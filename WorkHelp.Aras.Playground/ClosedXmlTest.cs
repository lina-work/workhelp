﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkHelp.Aras.Playground
{
    class ClosedXmlTest
    {
        public void Start()
        {
            ClosedXML.Excel.XLWorkbook workbook = new ClosedXML.Excel.XLWorkbook();
            ClosedXML.Excel.IXLWorksheet sheet = workbook.Worksheets.Add("報名總表");
            workbook.PageOptions.SetRowsToRepeatAtTop("$1:$1");
            workbook.PageOptions.ShowRowAndColumnHeadings = true;

            //重複標題列
            sheet.PageSetup.SetRowsToRepeatAtTop("$1:$1");
            //// Adding print areas
            //ws.PageSetup.PrintAreas.Add("A1:B2");
            //ws.PageSetup.PrintAreas.Add("D3:D5");

            //// Adding rows to repeat at top
            //ws.PageSetup.SetRowsToRepeatAtTop(1, 2);

            //// Adding columns to repeat at left
            //ws.PageSetup.SetColumnsToRepeatAtLeft(1, 2);

            //// Show gridlines
            //ws.PageSetup.ShowGridlines = true;

            //// Print in black and white
            //ws.PageSetup.BlackAndWhite = true;

            //// Print in draft quality
            //ws.PageSetup.DraftQuality = true;

            //// Show row and column headings
            //ws.PageSetup.ShowRowAndColumnHeadings = true;

            //// Set the page print order to over, then down
            //ws.PageSetup.PageOrder = XLPageOrderValues.OverThenDown;

            //// Place comments at the end of the sheet
            //ws.PageSetup.ShowComments = XLShowCommentsValues.AtEnd;

            //// Print errors as #N/A
            //ws.PageSetup.PrintErrorValue = XLPrintErrorValues.NA;

            workbook.SaveAs("SheetTab.xlsx");

            //凍結窗格
            //sheet.SheetView.FreezeRows();
       
            sheet.Column(0).Width = 10;
            sheet.Column(0).AdjustToContents();
            //第一欄隱藏
            sheet.Column(1).Hide();
        }

    }
}
