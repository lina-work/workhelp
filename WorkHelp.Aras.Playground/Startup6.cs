﻿using DocumentFormat.OpenXml.Drawing;
using DocumentFormat.OpenXml.Presentation;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml.Wordprocessing;
using Google.Apis.Auth;
using Google.Apis.Auth.OAuth2.Requests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using System.Xml.XPath;
using WorkHelp.Extension;

namespace WorkHelp.Aras.Playground.ArasModules
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class Startup6 : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
            目的: 成員列表
            日期: 2020-12-24 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]in_group_dashboard_mbrs";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "input dom: " + itmR.dom.InnerXml);

            string mode = itmR.getProperty("mode", "");
            string member_type = itmR.getProperty("type", "");

            //取得登入者資訊
            string sql = "SELECT * FROM IN_RESUME WITH(NOLOCK) WHERE in_user_id = N'" + inn.getUserID() + "'";
            Item itmLoginResume = inn.applySQL(sql);
            if (IsError(itmLoginResume))
            {
                throw new Exception("登入者履歷異常");
            }

            //要檢視的對象 Resume
            Item itmResumeView = GetTargetResume(CCO, strMethodName, inn, itmLoginResume, itmR);
            string view_resume_id = itmResumeView.getProperty("id", "");
            string view_member_type = itmResumeView.getProperty("in_member_type", "");

            //權限檢核
            Item itmPermit = inn.applyMethod("In_CheckIdentity", "<method>" + strMethodName + "</method><code>" + "ALL" + "</code>");
            bool isMeetingAdmin = itmPermit.getProperty("isMeetingAdmin", "") == "1";
            bool isCommittee = itmPermit.getProperty("isCommittee", "") == "1";

            if (isMeetingAdmin)
            {
                itmR.setProperty("CreatePaysBtn", "<button class='btn btn-primary' onclick='CreatePays_Click()'>產生繳費單</button>");
            }

            Item itmResumes = null;

            if (view_member_type == "asc" || view_member_type == "sys")
            {
                itmResumes = GetMembers(CCO, strMethodName, inn, "", member_type);
            }
            else if (view_member_type == "area_cmt" || view_member_type == "prjt_cmt")
            {
                itmResumes = GetMembers(CCO, strMethodName, inn, view_resume_id, member_type);
            }

            if (mode == "")
            {
                Query(CCO, strMethodName, inn, itmResumeView, itmResumes, itmPermit, itmR);
            }
            else if (mode == "xls")
            {
                Export(CCO, strMethodName, inn, itmResumeView, itmResumes, itmR);
            }
            return itmR;
        }

        #region 匯出
        private void Export(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmResumeView, Item itmResumes, Item itmReturn)
        {
            string member_type = itmReturn.getProperty("type", "");
            string view_name = itmResumeView.getProperty("in_name", "");

            string member_type_label = "";

            ClosedXML.Excel.XLWorkbook workbook = new ClosedXML.Excel.XLWorkbook();

            if (itmResumes == null || !IsError(itmResumes, isSingle: false))
            {
                switch (member_type)
                {
                    case "mbr":
                        member_type_label = "一般會員";
                        AppendMemberSheet(CCO, strMethodName, inn, workbook, member_type_label, itmResumes);
                        break;

                    case "vip_mbr":
                        member_type_label = "個人會員";
                        AppendMemberSheet(CCO, strMethodName, inn, workbook, member_type_label, itmResumes);
                        break;

                    case "vip_group":
                        member_type_label = "其他團體";
                        AppendGroupSheet(CCO, strMethodName, inn, workbook, member_type_label, itmResumes);
                        break;

                    case "gym":
                        member_type_label = "道館";
                        AppendGymSheet(CCO, strMethodName, inn, workbook, member_type_label, itmResumes);
                        break;

                    case "vip_gym":
                        member_type_label = "團體會員";
                        AppendGymSheet(CCO, strMethodName, inn, workbook, member_type_label, itmResumes);
                        break;

                    case "area_cmt":
                        member_type_label = "縣市委員會";
                        AppendCmtSheet(CCO, strMethodName, inn, workbook, member_type_label, itmResumes);
                        break;

                    default:
                        break;
                }

                itmReturn.setProperty("raw_count", itmResumes.getItemCount().ToString());
            }


            Item itmPath = GetExcelPath(CCO, strMethodName, inn, "export_path");

            string export_path = itmPath.getProperty("export_path", "").TrimEnd('\\');
            string ext_name = ".xlsx";
            string xls_name = member_type_label + DateTime.Now.ToString("MMdd_HHmmss");
            string xls_file = export_path + "\\" + xls_name + ext_name;
            string xls_url = xls_name + ext_name;

            workbook.SaveAs(xls_file);

            itmReturn.setProperty("xls_name", xls_url);
        }

        //附加個人會員 Sheet
        private void AppendMemberSheet(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, ClosedXML.Excel.XLWorkbook workbook, string sheet_name, Item itmResumes)
        {
            ClosedXML.Excel.IXLWorksheet sheet = workbook.Worksheets.Add(sheet_name);

            List<TField> fields = new List<TField>();
            fields.Add(new TField { property = "no", title = "編號", format = "" });
            fields.Add(new TField { property = "in_name", title = "姓名", format = "" });
            fields.Add(new TField { property = "in_gender", title = "性別", format = "" });
            fields.Add(new TField { property = "in_area", title = "出生地", format = "" });
            fields.Add(new TField { property = "in_sno", title = "身分證/居留證", format = "" });

            fields.Add(new TField { property = "in_resident_add", title = "戶籍地址", format = "" });
            fields.Add(new TField { property = "in_add", title = "通訊地址", });
            fields.Add(new TField { property = "in_email", title = "電子信箱", format = "" });

            fields.Add(new TField { property = "in_tel_1", title = "住家電話", format = "tel" });
            fields.Add(new TField { property = "in_tel", title = "行動電話", format = "tel" });
            fields.Add(new TField { property = "in_note", title = "備註", format = "" });

            fields.Add(new TField { property = "in_member_status", title = "會員狀態", format = "center" });
            fields.Add(new TField { property = "in_member_last_year", title = "最後繳費年度", format = "center" });
            fields.Add(new TField { property = "in_member_last_fee", title = "最後繳費金額", format = "$ #,##0" });
            fields.Add(new TField { property = "in_member_pay_no", title = "會費繳費單", format = "center" });
            fields.Add(new TField { property = "in_member_pay_amount", title = "會費金額", format = "$ #,##0" });

            fields.Add(new TField { property = "login_name", title = "系統帳號", format = "" });


            int wsRow = 1;
            int wsCol = 1;
            int field_count = fields.Count;

            for (int i = 0; i < field_count; i++)
            {
                TField field = fields[i];
                ClosedXML.Excel.IXLCell cell = sheet.Cell(wsRow, wsCol + i);
                SetHeadCell(cell, field.title);
            }
            wsRow++;

            int count = itmResumes.getItemCount();


            for (int i = 0; i < count; i++)
            {
                Item item = itmResumes.getItemByIndex(i);
                string in_add = item.getProperty("in_add", "");
                string in_add_code = item.getProperty("in_add_code", "");

                string in_resident_add = item.getProperty("in_resident_add", "");
                string in_resident_add_code = item.getProperty("in_resident_add_code", "");

                string in_email = item.getProperty("in_email", "");
                string in_reg_date = item.getProperty("in_reg_date", "");
                string in_change_date = item.getProperty("in_change_date", "");

                if (in_email == "na@na.n")
                {
                    item.setProperty("in_email", "");
                }

                item.setProperty("in_add", in_add_code + in_add);
                item.setProperty("in_resident_add", in_resident_add + in_resident_add_code);

                if (in_reg_date.Contains("1900"))
                {
                    item.setProperty("in_reg_date", "");
                }

                if (in_change_date.Contains("1900"))
                {
                    item.setProperty("in_change_date", "");
                }

                item.setProperty("no", (i + 1).ToString());

                SetItemCell(sheet, wsRow, wsCol, item, fields);

                wsRow++;
            }
        }

        //附加縣市委員會 Sheet
        private void AppendCmtSheet(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, ClosedXML.Excel.XLWorkbook workbook, string sheet_name, Item itmResumes)
        {
            ClosedXML.Excel.IXLWorksheet sheet = workbook.Worksheets.Add(sheet_name);

            List<TField> fields = new List<TField>();
            fields.Add(new TField { property = "no", title = "編號", format = "" });
            fields.Add(new TField { property = "in_name", title = "委員會", format = "" });
            fields.Add(new TField { property = "in_principal", title = "主委", format = "" });
            fields.Add(new TField { property = "in_head_coach", title = "總幹事", format = "" });
            fields.Add(new TField { property = "in_tel", title = "行動電話", format = "tel" });

            fields.Add(new TField { property = "in_emrg_contact1", title = "業務承辦人", format = "" });
            fields.Add(new TField { property = "in_emrg_tel1", title = "承辦人電話", format = "tel" });

            fields.Add(new TField { property = "in_add_code", title = "郵遞區號", format = "text" });
            fields.Add(new TField { property = "in_add", title = "通信地址", });

            fields.Add(new TField { property = "in_member_status", title = "會員狀態", format = "center" });
            fields.Add(new TField { property = "in_member_last_year", title = "最後繳費年度", format = "center" });

            fields.Add(new TField { property = "login_name", title = "系統帳號", format = "" });


            int wsRow = 1;
            int wsCol = 1;
            int field_count = fields.Count;

            for (int i = 0; i < field_count; i++)
            {
                TField field = fields[i];
                ClosedXML.Excel.IXLCell cell = sheet.Cell(wsRow, wsCol + i);
                SetHeadCell(cell, field.title);
            }
            wsRow++;

            int count = itmResumes.getItemCount();


            for (int i = 0; i < count; i++)
            {
                Item item = itmResumes.getItemByIndex(i);
                string in_add = item.getProperty("in_add", "");
                string in_add_code = item.getProperty("in_add_code", "");

                string in_resident_add = item.getProperty("in_resident_add", "");
                string in_resident_add_code = item.getProperty("in_resident_add_code", "");

                string in_email = item.getProperty("in_email", "");
                string in_reg_date = item.getProperty("in_reg_date", "");
                string in_change_date = item.getProperty("in_change_date", "");

                if (in_email == "na@na.n")
                {
                    item.setProperty("in_email", "");
                }

                item.setProperty("in_add", in_add_code + in_add);
                item.setProperty("in_resident_add", in_resident_add + in_resident_add_code);

                if (in_reg_date.Contains("1900"))
                {
                    item.setProperty("in_reg_date", "");
                }

                if (in_change_date.Contains("1900"))
                {
                    item.setProperty("in_change_date", "");
                }


                item.setProperty("no", (i + 1).ToString());

                SetItemCell(sheet, wsRow, wsCol, item, fields);

                wsRow++;
            }
        }

        //附加道館社團 Sheet
        private void AppendGymSheet(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, ClosedXML.Excel.XLWorkbook workbook, string sheet_name, Item itmResumes)
        {
            ClosedXML.Excel.IXLWorksheet sheet = workbook.Worksheets.Add(sheet_name);

            List<TField> fields = new List<TField>();
            fields.Add(new TField { property = "no", title = "編號", format = "" });
            fields.Add(new TField { property = "in_name", title = "設立名稱", format = "" });
            fields.Add(new TField { property = "in_principal", title = "負責人", format = "" });
            fields.Add(new TField { property = "in_head_coach", title = "總教練", format = "" });
            fields.Add(new TField { property = "in_assistant_coaches", title = "助理教練", format = "" });

            fields.Add(new TField { property = "in_resident_add", title = "道館地址", format = "" });
            fields.Add(new TField { property = "in_add", title = "通信地址", });

            fields.Add(new TField { property = "in_tel_1", title = "住家電話", format = "tel" });
            fields.Add(new TField { property = "in_tel_2", title = "道館電話", format = "tel" });
            fields.Add(new TField { property = "in_tel", title = "行動電話", format = "tel" });
            fields.Add(new TField { property = "in_fax", title = "傳真號碼", format = "tel" });

            fields.Add(new TField { property = "in_url", title = "網路地址", format = "" });
            fields.Add(new TField { property = "in_email", title = "電子信箱", format = "" });
            fields.Add(new TField { property = "in_note", title = "備註", format = "" });
            fields.Add(new TField { property = "in_email_exam", title = "發信狀況", format = "" });

            fields.Add(new TField { property = "in_member_unit", title = "社館", format = "" });
            fields.Add(new TField { property = "in_reg_date", title = "入會日期", format = "yyyy/MM/dd" });
            fields.Add(new TField { property = "in_change_date", title = "變更日期", format = "yyyy/MM/dd" });
            fields.Add(new TField { property = "in_manager_name", title = "所屬跆委會", format = "" });

            fields.Add(new TField { property = "in_member_status", title = "會員狀態", format = "center" });
            fields.Add(new TField { property = "in_member_last_year", title = "最後繳費年度", format = "center" });
            fields.Add(new TField { property = "in_member_last_fee", title = "最後繳費金額", format = "$ #,##0" });
            fields.Add(new TField { property = "in_member_pay_no", title = "會費繳費單", format = "center" });
            fields.Add(new TField { property = "in_member_pay_amount", title = "會費金額", format = "$ #,##0" });

            fields.Add(new TField { property = "login_name", title = "系統帳號", format = "" });


            int wsRow = 1;
            int wsCol = 1;
            int field_count = fields.Count;

            for (int i = 0; i < field_count; i++)
            {
                TField field = fields[i];
                ClosedXML.Excel.IXLCell cell = sheet.Cell(wsRow, wsCol + i);
                SetHeadCell(cell, field.title);
            }
            wsRow++;

            int count = itmResumes.getItemCount();


            for (int i = 0; i < count; i++)
            {
                Item item = itmResumes.getItemByIndex(i);
                string in_add = item.getProperty("in_add", "");
                string in_add_code = item.getProperty("in_add_code", "");

                string in_resident_add = item.getProperty("in_resident_add", "");
                string in_resident_add_code = item.getProperty("in_resident_add_code", "");

                string in_email = item.getProperty("in_email", "");
                string in_reg_date = item.getProperty("in_reg_date", "");
                string in_change_date = item.getProperty("in_change_date", "");

                if (in_email == "na@na.n")
                {
                    item.setProperty("in_email", "");
                }

                item.setProperty("in_add", in_add_code + in_add);
                item.setProperty("in_resident_add", in_resident_add + in_resident_add_code);

                if (in_reg_date.Contains("1900"))
                {
                    item.setProperty("in_reg_date", "");
                }

                if (in_change_date.Contains("1900"))
                {
                    item.setProperty("in_change_date", "");
                }


                item.setProperty("no", (i + 1).ToString());

                SetItemCell(sheet, wsRow, wsCol, item, fields);

                wsRow++;
            }
        }

        //附加其他團體會員 Sheet
        private void AppendGroupSheet(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, ClosedXML.Excel.XLWorkbook workbook, string sheet_name, Item itmResumes)
        {
            ClosedXML.Excel.IXLWorksheet sheet = workbook.Worksheets.Add(sheet_name);

            List<TField> fields = new List<TField>();
            fields.Add(new TField { property = "no", title = "編號", format = "" });
            fields.Add(new TField { property = "in_name", title = "團體會員名稱", format = "" });
            fields.Add(new TField { property = "in_principal", title = "法定代理人", format = "" });
            fields.Add(new TField { property = "in_emrg_contact1", title = "聯絡人", format = "" });
            fields.Add(new TField { property = "in_tel_1", title = "電話", format = "tel" });
            fields.Add(new TField { property = "in_add_code", title = "郵遞區號", format = "text" });
            fields.Add(new TField { property = "in_add", title = "團體會員地址", format = "" });
            fields.Add(new TField { property = "in_reg_date", title = "入會日期", format = "yyyy/MM/dd" });
            fields.Add(new TField { property = "in_member_status", title = "會員狀態", format = "center" });
            fields.Add(new TField { property = "in_member_last_year", title = "最後繳費年度", format = "center" });

            fields.Add(new TField { property = "login_name", title = "系統帳號", format = "" });


            int wsRow = 1;
            int wsCol = 1;
            int field_count = fields.Count;

            for (int i = 0; i < field_count; i++)
            {
                TField field = fields[i];
                ClosedXML.Excel.IXLCell cell = sheet.Cell(wsRow, wsCol + i);
                SetHeadCell(cell, field.title);
            }
            wsRow++;

            int count = itmResumes.getItemCount();


            for (int i = 0; i < count; i++)
            {
                Item item = itmResumes.getItemByIndex(i);
                string in_add = item.getProperty("in_add", "");
                string in_add_code = item.getProperty("in_add_code", "");
                string in_email = item.getProperty("in_email", "");
                string in_reg_date = item.getProperty("in_reg_date", "");

                if (in_email == "na@na.n")
                {
                    item.setProperty("in_email", "");
                }

                item.setProperty("in_add", in_add_code + in_add);

                if (in_reg_date.Contains("1900"))
                {
                    item.setProperty("in_reg_date", "");
                }

                item.setProperty("no", (i + 1).ToString());

                SetItemCell(sheet, wsRow, wsCol, item, fields);

                wsRow++;
            }
        }

        //設定物件與資料列
        private void SetItemCell(ClosedXML.Excel.IXLWorksheet sheet, int wsRow, int wsCol, Item item, List<TField> fields)
        {
            int count = fields.Count;
            for (int i = 0; i < count; i++)
            {
                TField field = fields[i];

                ClosedXML.Excel.IXLCell cell = sheet.Cell(wsRow, wsCol + i);

                if (field.property == "")
                {
                    SetBodyCell(cell, "", "");
                }
                else
                {
                    string value = item.getProperty(field.property, "");
                    SetBodyCell(cell, value, field.format);
                }
            }
        }

        //設定資料列
        private void SetBodyCell(ClosedXML.Excel.IXLCell cell, string value, string format)
        {
            switch (format)
            {
                case "center":
                    cell.Value = value;
                    cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    break;

                case "yyyy/MM/dd":
                case "yyyy/MM/dd HH:mm":
                    cell.Value = GetDateTimeValue(value, format, 8);
                    cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    break;

                case "$ #,##0":
                    cell.Value = value;
                    cell.Style.NumberFormat.Format = "$ #,##0";
                    break;

                case "tel":
                    if (value.StartsWith("09"))
                    {
                        cell.Value = value.Replace("-", "");
                        cell.Style.NumberFormat.Format = "0000-000-000";
                        cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    }
                    else
                    {
                        cell.Value = "'" + value;
                        cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    }
                    break;

                case "text":
                    cell.Value = "'" + value;
                    cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    break;

                default:
                    cell.Value = value;
                    break;
            }
            cell.Style.Border.SetRightBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetTopBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetBottomBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetLeftBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
        }

        ClosedXML.Excel.XLColor head_bg_color = ClosedXML.Excel.XLColor.FromHtml("#295C90");

        //設定標題列
        private void SetHeadCell(ClosedXML.Excel.IXLCell cell, string title)
        {
            cell.Value = title;

            cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
            cell.Style.Font.Bold = true;
            cell.DataType = ClosedXML.Excel.XLDataType.Text;
            cell.Style.Fill.BackgroundColor = head_bg_color;
            cell.Style.Font.FontColor = ClosedXML.Excel.XLColor.White;

            cell.Style.Border.SetRightBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetTopBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetBottomBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetLeftBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
        }
        private Item GetExcelPath(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string in_name)
        {
            Item itmResult = inn.newItem();

            //Excel 匯出路徑 (from 原創.設定參數)
            string sql = @"
            SELECT 
                TOP 1 t2.in_name AS 'variable', t1.in_name, t1.in_value 
            FROM 
                In_Variable_Detail t1 WITH(NOLOCK)
            INNER JOIN 
                In_Variable t2 WITH(NOLOCK) ON t2.ID = t1.SOURCE_ID 
            WHERE 
                t2.in_name = N'meeting_excel'  
                AND t1.in_name = N'{#in_name}'
            ";

            sql = sql.Replace("{#in_name}", in_name);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item item = inn.applySQL(sql);

            if (item.isError())
            {
                throw new Exception("未設定匯出路徑 _# " + in_name);
            }

            itmResult.setProperty("export_path", item.getProperty("in_value", ""));

            return itmResult;
        }
        #endregion 匯出

        private void Query(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmResumeView, Item itmResumes, Item itmPermit, Item itmReturn)
        {
            string member_type = itmReturn.getProperty("type", "");
            string view_name = itmResumeView.getProperty("in_name", "");

            string member_type_label = "";

            if (itmResumes == null || !IsError(itmResumes, isSingle: false))
            {
                switch (member_type)
                {
                    case "mbr":
                        member_type_label = "一般會員";
                        AppendMemberTable(CCO, strMethodName, inn, itmResumes, itmPermit, itmReturn);
                        break;

                    case "vip_mbr":
                        member_type_label = "個人會員";
                        AppendMemberTable(CCO, strMethodName, inn, itmResumes, itmPermit, itmReturn);
                        break;

                    case "vip_group":
                        member_type_label = "其他團體";
                        AppendGroupTable(CCO, strMethodName, inn, itmResumes, itmPermit, itmReturn);
                        break;

                    case "gym":
                        member_type_label = "道館";
                        AppendGymTable(CCO, strMethodName, inn, itmResumes, itmPermit, itmReturn);
                        break;

                    case "vip_gym":
                        member_type_label = "團體會員";
                        AppendGymTable(CCO, strMethodName, inn, itmResumes, itmPermit, itmReturn);
                        break;

                    case "area_cmt":
                        member_type_label = "縣市委員會";
                        AppendCmtTable(CCO, strMethodName, inn, itmResumes, itmPermit, itmReturn);
                        break;

                    case "prjt_cmt":
                        member_type_label = "專項委員會";
                        AppendCmtTable(CCO, strMethodName, inn, itmResumes, itmPermit, itmReturn);
                        break;

                    default:
                        break;
                }

                itmReturn.setProperty("raw_count", itmResumes.getItemCount().ToString());
            }

            itmReturn.setProperty("page_title", member_type_label);
            itmReturn.setProperty("page_sub_title", view_name);
        }

        //附加委員會 Table
        private void AppendCmtTable(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item items, Item itmPermit, Item itmReturn)
        {
            bool isMeetingAdmin = itmPermit.getProperty("isMeetingAdmin", "") == "1";
            bool isCommittee = itmPermit.getProperty("isCommittee", "") == "1";

            List<TField> fields = new List<TField>();
            fields.Add(new TField { property = "in_name", title = "委員會", css = "text-left", hide = true });
            fields.Add(new TField { property = "in_sno", title = "系統帳號", css = "text-left", hide = false });

            fields.Add(new TField { property = "in_link", title = "委員會", css = "text-left" });
            fields.Add(new TField { property = "in_principal", title = "主委", css = "text-left" });
            fields.Add(new TField { property = "in_head_coach", title = "總幹事", css = "text-left" });
            fields.Add(new TField { property = "in_tel", title = "行動電話", css = "text-left" });
            fields.Add(new TField { property = "in_emrg_contact1", title = "業務<br>承辦人", css = "text-left" });
            fields.Add(new TField { property = "in_emrg_tel1", title = "承辦人<br>電話", css = "text-left" });
            //fields.Add(new TField { property = "in_add_code", title = "郵遞<br>區號", css = "text-left" });
            //fields.Add(new TField { property = "in_add", title = "通信地址", css = "text-left",  });
            fields.Add(new TField { property = "in_member_status", title = "會員<br>狀態", css = "text-left" });
            fields.Add(new TField { property = "in_member_last_year", title = "最後<br>繳費年度", css = "text-left" });
            fields.Add(new TField { property = "in_pay_link", title = "會員年費", css = "text-center" });

            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            head.AppendLine("<thead>");
            foreach (var field in fields)
            {
                if (field.hide)
                {
                    head.AppendLine("<th class='text-center' data-field='" + field.property + "' data-sortable='true' data-visible='false'>" + field.title + "</th>");
                }
                else
                {
                    head.AppendLine("<th class='text-center' data-field='" + field.property + "' data-sortable='true'>" + field.title + "</th>");
                }
            }
            head.AppendLine("</thead>");

            int count = items.getItemCount(); ;

            body.AppendLine("<tbody>");
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                item.setProperty("in_link", GetDashboardLink(item));
                //item.setProperty("inn_funcs", GetFuncsLink(item));
                item.setProperty("in_pay_link", GetPayLink(item, isMeetingAdmin));

                body.AppendLine("<tr>");
                foreach (var field in fields)
                {
                    body.AppendLine("<td class='" + field.css + "' data-field='" + field.property + "' >" + field.GetStr(item) + "</td>");
                }
                body.AppendLine("</tr>");
            }
            body.AppendLine("</tbody>");


            string table_name = "resume_table";

            StringBuilder builder = new StringBuilder();
            builder.AppendLine(GetTableAttribute(table_name));
            builder.Append(head);
            builder.Append(body);
            builder.AppendLine("</table>");
            builder.AppendLine("<script>");
            builder.AppendLine("$('#" + table_name + "').bootstrapTable();");
            builder.AppendLine("if ($(window).width() <= 768) { $('#" + table_name + "').bootstrapTable('toggleView'); }");
            builder.AppendLine("</script>");

            itmReturn.setProperty("inn_table", builder.ToString());
        }

        //附加其他團體會員 Table
        private void AppendGroupTable(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item items, Item itmPermit, Item itmReturn)
        {
            bool isMeetingAdmin = itmPermit.getProperty("isMeetingAdmin", "") == "1";
            bool isCommittee = itmPermit.getProperty("isCommittee", "") == "1";

            List<TField> fields = new List<TField>();
            fields.Add(new TField { property = "in_name", title = "團體會員名稱", css = "text-left", hide = true });
            fields.Add(new TField { property = "in_sno", title = "系統帳號", css = "text-left", hide = false });

            fields.Add(new TField { property = "in_link", title = "團體會員名稱", css = "text-left" });
            fields.Add(new TField { property = "in_principal", title = "法定代理人", css = "text-left" });
            fields.Add(new TField { property = "in_emrg_contact1", title = "聯絡人", css = "text-left" });
            fields.Add(new TField { property = "in_tel_1", title = "電話", css = "text-left" });
            fields.Add(new TField { property = "in_add_code", title = "郵遞區號", css = "text-left" });
            fields.Add(new TField { property = "in_add", title = "團體會員地址", css = "text-left" });
            fields.Add(new TField { property = "in_member_status", title = "會員<br>狀態", css = "text-left" });
            fields.Add(new TField { property = "in_member_last_year", title = "最後<br>繳費年度", css = "text-left" });
            fields.Add(new TField { property = "in_pay_link", title = "會員年費", css = "text-center" });

            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            head.AppendLine("<thead>");
            foreach (var field in fields)
            {
                if (field.hide)
                {
                    head.AppendLine("<th class='text-center' data-field='" + field.property + "' data-sortable='true' data-visible='false'>" + field.title + "</th>");
                }
                else
                {
                    head.AppendLine("<th class='text-center' data-field='" + field.property + "' data-sortable='true'>" + field.title + "</th>");
                }
            }
            head.AppendLine("</thead>");

            int count = items.getItemCount(); ;

            body.AppendLine("<tbody>");
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                item.setProperty("in_link", GetDashboardLink(item));
                //item.setProperty("inn_funcs", GetFuncsLink(item));
                item.setProperty("in_pay_link", GetPayLink(item, isMeetingAdmin));

                body.AppendLine("<tr>");
                foreach (var field in fields)
                {
                    body.AppendLine("<td class='" + field.css + "' data-field='" + field.property + "' >" + field.GetStr(item) + "</td>");
                }
                body.AppendLine("</tr>");
            }
            body.AppendLine("</tbody>");


            string table_name = "resume_table";

            StringBuilder builder = new StringBuilder();
            builder.AppendLine(GetTableAttribute(table_name));
            builder.Append(head);
            builder.Append(body);
            builder.AppendLine("</table>");
            builder.AppendLine("<script>");
            builder.AppendLine("$('#" + table_name + "').bootstrapTable();");
            builder.AppendLine("if ($(window).width() <= 768) { $('#" + table_name + "').bootstrapTable('toggleView'); }");
            builder.AppendLine("</script>");

            itmReturn.setProperty("inn_table", builder.ToString());
        }

        //附加道館社團 Table
        private void AppendGymTable(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item items, Item itmPermit, Item itmReturn)
        {
            bool isMeetingAdmin = itmPermit.getProperty("isMeetingAdmin", "") == "1";
            bool isCommittee = itmPermit.getProperty("isCommittee", "") == "1";

            List<TField> fields = new List<TField>();
            fields.Add(new TField { property = "in_name", title = "設立名稱", css = "text-left", hide = true });
            fields.Add(new TField { property = "in_sno", title = "系統帳號", css = "text-left", hide = false });

            fields.Add(new TField { property = "in_link", title = "設立<br>名稱", css = "text-left" });
            fields.Add(new TField { property = "in_principal", title = "負責人", css = "text-left" });
            fields.Add(new TField { property = "in_head_coach", title = "總教練", css = "text-left" });
            // fields.Add(new TField { property = "in_assistant_coaches", title = "助理<br>教練", css = "text-left" });
            fields.Add(new TField { property = "in_member_unit", title = "社館", css = "text-left" });
            fields.Add(new TField { property = "in_manager_name", title = "所屬<br>跆委會", css = "text-left" });
            fields.Add(new TField { property = "in_tel", title = "行動<br>電話", css = "text-left" });
            // fields.Add(new TField { property = "in_add", title = "通信<br>地址", css = "text-left" });
            fields.Add(new TField { property = "in_email_exam", title = "發信<br>狀況", css = "text-left" });
            fields.Add(new TField { property = "in_member_status", title = "會員<br>狀態", css = "text-left" });
            fields.Add(new TField { property = "in_member_last_year", title = "最後<br>繳費年度", css = "text-left" });
            fields.Add(new TField { property = "in_pay_link", title = "會員年費", css = "text-center" });

            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            head.AppendLine("<thead>");
            foreach (var field in fields)
            {
                if (field.hide)
                {
                    head.AppendLine("<th class='text-center' data-field='" + field.property + "' data-sortable='true' data-visible='false'>" + field.title + "</th>");
                }
                else
                {
                    head.AppendLine("<th class='text-center' data-field='" + field.property + "' data-sortable='true'>" + field.title + "</th>");
                }
            }
            head.AppendLine("</thead>");

            int count = items.getItemCount(); ;

            body.AppendLine("<tbody>");
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                item.setProperty("in_link", GetDashboardLink(item));
                //item.setProperty("inn_funcs", GetFuncsLink(item));
                item.setProperty("in_pay_link", GetPayLink(item, isMeetingAdmin));

                body.AppendLine("<tr>");
                foreach (var field in fields)
                {
                    body.AppendLine("<td class='" + field.css + "' data-field='" + field.property + "' >" + field.GetStr(item) + "</td>");
                }
                body.AppendLine("</tr>");
            }
            body.AppendLine("</tbody>");


            string table_name = "resume_table";

            StringBuilder builder = new StringBuilder();
            builder.AppendLine(GetTableAttribute(table_name));
            builder.Append(head);
            builder.Append(body);
            builder.AppendLine("</table>");
            builder.AppendLine("<script>");
            builder.AppendLine("$('#" + table_name + "').bootstrapTable();");
            builder.AppendLine("if ($(window).width() <= 768) { $('#" + table_name + "').bootstrapTable('toggleView'); }");
            builder.AppendLine("</script>");

            itmReturn.setProperty("inn_table", builder.ToString());
        }

        //附加個人會員 Table
        private void AppendMemberTable(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item items, Item itmPermit, Item itmReturn)
        {
            bool isMeetingAdmin = itmPermit.getProperty("isMeetingAdmin", "") == "1";
            bool isCommittee = itmPermit.getProperty("isCommittee", "") == "1";

            List<TField> fields = new List<TField>();
            fields.Add(new TField { property = "in_name", title = "姓名", css = "text-left", hide = true });
            fields.Add(new TField { property = "in_sno", title = "系統帳號", css = "text-left", hide = true });

            fields.Add(new TField { property = "in_link", title = "姓名", css = "text-left" });
            fields.Add(new TField { property = "in_gender", title = "性別", css = "text-left" });
            fields.Add(new TField { property = "in_sno_display", title = "身分證號", css = "text-left" });
            fields.Add(new TField { property = "in_birth_display", title = "生日", css = "text-left" });
            fields.Add(new TField { property = "in_tel", title = "行動電話", css = "text-left" });
            // fields.Add(new TField { property = "in_add", title = "通信地址", css = "text-left" });
            fields.Add(new TField { property = "in_email_exam", title = "發信狀況", css = "text-left" });
            fields.Add(new TField { property = "in_member_status", title = "會員狀態", css = "text-left" });
            fields.Add(new TField { property = "in_degree_display", title = "段位", css = "text-left" });
            fields.Add(new TField { property = "in_member_last_year", title = "最後<br>繳費年度", css = "text-left" });
            fields.Add(new TField { property = "in_pay_link", title = "會員年費", css = "text-center" });

            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            head.AppendLine("<thead>");
            foreach (var field in fields)
            {
                if (field.hide)
                {
                    head.AppendLine("<th class='text-center' data-field='" + field.property + "' data-sortable='true' data-visible='false'>" + field.title + "</th>");
                }
                else
                {
                    head.AppendLine("<th class='text-center' data-field='" + field.property + "' data-sortable='true'>" + field.title + "</th>");
                }
            }
            head.AppendLine("</thead>");

            int count = items.getItemCount(); ;

            body.AppendLine("<tbody>");
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                item.setProperty("in_link", GetDashboardLink(item));
                item.setProperty("in_sno_display", GetSidDisplay(item));
                item.setProperty("in_birth_display", GetBirthDisplay(item));
                item.setProperty("in_degree_display", GetDegreeDisplay(item));
                item.setProperty("in_pay_link", GetPayLink(item, isMeetingAdmin));

                body.AppendLine("<tr>");
                foreach (var field in fields)
                {
                    body.AppendLine("<td class='" + field.css + "' data-field='" + field.property + "' >" + field.GetStr(item) + "</td>");
                }
                body.AppendLine("</tr>");
            }
            body.AppendLine("</tbody>");


            string table_name = "resume_table";

            StringBuilder builder = new StringBuilder();
            builder.AppendLine(GetTableAttribute(table_name));
            builder.Append(head);
            builder.Append(body);
            builder.AppendLine("</table>");
            builder.AppendLine("<script>");
            builder.AppendLine("$('#" + table_name + "').bootstrapTable();");
            builder.AppendLine("if ($(window).width() <= 768) { $('#" + table_name + "').bootstrapTable('toggleView'); }");
            builder.AppendLine("</script>");

            itmReturn.setProperty("inn_table", builder.ToString());
        }

        private string GetTableAttribute(string table_name)
        {
            return "<table id='" + table_name + "' class='table table-hover table-bordered table-rwd rwd rwdtable' "
                + " data-toggle='table' "
                + " data-click-to-select='true' "
                + " data-striped='false' "
                + " data-search='true' "
                + " data-pagination='true' "
                + " data-page-size='25' "
                + " data-show-pagination-switch='false'"
                + ">";
        }

        //取得成員
        private Item GetMembers(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string view_resume_id, string in_member_type)
        {
            string condition = view_resume_id != "" ? " AND t1.in_manager_org = '" + view_resume_id + "' " : "";
            string sql = @"
                SELECT 
	                t1.*
	                , t3.in_meeting AS 'pay_meeting'
	                , t3.pay_bool
                FROM 
	                IN_RESUME t1 WITH(NOLOCK)
				LEFT OUTER JOIN
					IN_RESUME t2 WITH(NOLOCK)
					ON t2.id = t1.in_manager_org
				LEFT OUTER JOIN
				    IN_MEETING_PAY t3 WITH(NOLOCK)
				    ON t3.item_number = t1.in_member_pay_no
                WHERE 
	                t1.in_member_role = 'sys_9999'
	                AND t1.in_member_type = '{#in_member_type}'
                    {#condition}
                ORDER BY 
                    t2.login_name
                    , t1.login_name
            ";

            sql = sql.Replace("{#in_member_type}", in_member_type)
                .Replace("{#condition}", condition);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return inn.applySQL(sql);

        }

        //取得檢視對象 Resume
        private Item GetTargetResume(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmLoginResume, Item itmReturn)
        {
            itmLoginResume.setType("In_Resume");
            itmLoginResume.setProperty("target_resume_id", itmReturn.getProperty("id", ""));
            itmLoginResume.setProperty("login_resume_id", itmLoginResume.getID());
            itmLoginResume.setProperty("login_member_type", itmLoginResume.getProperty("in_member_type", ""));
            return itmLoginResume.apply("In_Get_Target_Resume");
        }

        private class TField
        {
            public string property { get; set; }
            public string title { get; set; }
            public string css { get; set; }
            public string format { get; set; }
            public bool hide { get; set; }

            public string GetStr(Item item)
            {
                return item.getProperty(this.property, "");
            }
        }


        private string GetDashboardLink(Item itmResume)
        {
            string resume_id = itmResume.getProperty("id", "");
            string in_name = itmResume.getProperty("in_name", "");
            string in_org = itmResume.getProperty("in_org", "");

            if (in_org == "1")
            {
                //onclick=\"ajaxindicatorstart('載入中...');\"
                return "<a href='../pages/c.aspx?page=GymMemberView.html&method=In_Group_Resume_View&id=" + resume_id + "' >"
                    + in_name
                    + "</a>";
            }
            else
            {
                //onclick=\"ajaxindicatorstart('載入中...');\"
                return "<a href='../pages/c.aspx?page=SingleMemberView.html&method=In_Group_Resume_View&id=" + resume_id + "' >"
                    + in_name
                    + "</a>";
            }
        }

        private string GetFuncsLink(Item itmResume)
        {
            string in_sno = itmResume.getProperty("in_sno", "");

            return "<button class='btn btn-sm btn-primary' onclick='CreatePayment_Click(this)' data-sid='" + in_sno + "'>"
                + "產生繳費單"
                + "</button>";
        }

        private string GetPayLink(Item itmResume, bool isMeetingAdmin)
        {
            string in_sno = itmResume.getProperty("in_sno", "");
            string in_member_pay_no = itmResume.getProperty("in_member_pay_no", "");
            string in_member_pay_amount = itmResume.getProperty("in_member_pay_amount", "");
            string pay_meeting = itmResume.getProperty("pay_meeting", "");
            string pay_bool = itmResume.getProperty("pay_bool", "");

            if (in_member_pay_no != "")
            {
                string css = "btn-primary";
                if (pay_bool == "已繳費")
                {
                    css = "btn-success";
                }
                else if (pay_bool == "已取消")
                {
                    css = "btn-default";
                }

                return "<button class='btn " + css + " badge choseme edit_mode' "
                + " data-meeting='" + pay_meeting + "' data-no='" + in_member_pay_no + "' onclick='GoPayment_Click(this)'>"
                + " <i class='fa fa-dollar'></i> " + in_member_pay_amount + "</button>";
            }
            else if (isMeetingAdmin)
            {
                return "<button class='btn btn-sm btn-primary' onclick='CreatePayment_Click(this)' data-sid='" + in_sno + "'>"
                    + "產生繳費單"
                    + "</button>";
            }
            else
            {
                return "&nbsp;";
            }
        }

        private string GetSidDisplay(Item itmResume)
        {
            return GetSidDisplay(itmResume.getProperty("in_sno", ""));
        }

        //生日(外顯)
        private string GetBirthDisplay(Item itmResume)
        {
            string value = itmResume.getProperty("in_birth", "");

            if (value.Contains("1900") || value.Contains("1899"))
            {
                return "";
            }

            return GetDateTimeValue(value, "yyyy年MM月dd日", 8);
        }

        //身分證字號(外顯)
        private string GetSidDisplay(string in_sno)
        {
            if (in_sno.Length == 10)
            {
                //10碼(身分證處理)
                string in_sno_1 = in_sno.Substring(0, 2);
                string in_sno_2 = in_sno.Substring(6, 4);
                return in_sno_1 + "****" + in_sno_2;//A1****6789
            }
            else
            {
                return "";
            }
        }

        private string GetDegreeDisplay(Item itmResume)
        {
            return GetDegreeDisplay(itmResume.getProperty("in_degree", ""));
        }

        private string GetDegreeDisplay(string value)
        {
            switch (value)
            {
                case "": return "無段位";
                case "10": return "白帶";
                case "30": return "黃帶";
                case "40": return "黃綠帶";
                case "50": return "綠帶";
                case "60": return "綠藍帶";
                case "70": return "藍帶";
                case "80": return "藍紅帶";
                case "90": return "紅帶";
                case "100": return "紅黑帶";
                case "150": return "黑帶一品";
                case "250": return "黑帶二品";
                case "350": return "黑帶三品";
                case "1000": return "壹段";
                case "2000": return "貳段";
                case "3000": return "參段";
                case "4000": return "肆段";
                case "5000": return "伍段";
                case "6000": return "陸段";
                case "7000": return "柒段";
                case "8000": return "捌段";
                case "9000": return "玖段";

                case "11000": return "11";
                case "12000": return "12";
                case "13000": return "13";
                default: return "";
            }
        }

        private string GetDateTimeValue(string value, string format, int hours = 0)
        {
            if (value == "") return "";

            DateTime dt = DateTime.Now;
            if (DateTime.TryParse(value, out dt))
            {
                return dt.AddHours(hours).ToString(format);
            }
            else
            {
                return value;
            }
        }

        private bool IsError(Item item, bool isSingle = true)
        {
            if (item.isError() || item.getResult() == "")
            {
                return true;
            }
            else if (isSingle && item.getItemCount() != 1)
            {
                return true;
            }
            else if (item.getItemCount() <= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}