﻿using DocumentFormat.OpenXml.Drawing;
using DocumentFormat.OpenXml.Presentation;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml.Wordprocessing;
using Google.Apis.Auth;
using Google.Apis.Auth.OAuth2.Requests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using System.Xml.XPath;
using WorkHelp.Extension;

namespace WorkHelp.Aras.Playground.ArasModules
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class Startup5 : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
            目的: 快速產生資料
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]TEST_AUTO_CREATE_ORG_MAP";

            string sql = "SELECT * FROM IN_CLA_MEETING_USER WITH(NOLOCK) WHERE source_id = '789FA4BFBC744021AFA6429452F0FF91'";
            Item items = inn.applySQL(sql);
            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);

                Item itmIdentity = inn.newItem("Identity", "get");
                itmIdentity.setProperty("in_number", item.getProperty("in_sno", ""));
                itmIdentity = itmIdentity.apply();

                //此為申請館主之用,要把報名者加入 7AC8B33DA0F246DDA70BB244AB231E29 ACT_GymOwner
                Item itmMember = inn.newItem("Member", "merge");
                itmMember.setAttribute("where", "source_id='7AC8B33DA0F246DDA70BB244AB231E29' and related_id='" + itmIdentity.getID() + "'");
                itmMember.setProperty("source_id", "7AC8B33DA0F246DDA70BB244AB231E29");
                itmMember.setRelatedItem(itmIdentity);
                itmMember = itmMember.apply();
            }

            // int max = 210;
            // string item_type_name = "In_Org_Map";

            // for (int i = 0; i < max; i++)
            // {
            //     Item itmMap = inn.newItem(item_type_name, "add");
            //     itmMap.setProperty("in_stuff_b1", i.ToString().PadLeft(3, '0'));
            //     itmMap = itmMap.apply();

            // }

            return this;
        }
    }
}