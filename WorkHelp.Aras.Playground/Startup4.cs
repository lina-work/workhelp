﻿using DocumentFormat.OpenXml.Drawing;
using DocumentFormat.OpenXml.Presentation;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml.Wordprocessing;
using Google.Apis.Auth;
using Google.Apis.Auth.OAuth2.Requests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using System.Xml.XPath;
using WorkHelp.Extension;

namespace WorkHelp.Aras.Playground.ArasModules
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class Startup4 : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
            取得owned_by_id為當下登入使用者的會議並回傳。主要由c.aspx使用。

            回傳物件結構：
                <Item>
                    <!--本體啥都沒有-->
                    <Relationships>
                        <Item type="In_Cla_Meeting"/>
                        <Item type="Inn_Param"/> <!--傳入的parameter，來自In_Collect_PassengerParam-->
                    </Relationships>
                </Item>
            */

            //System.Diagnostics.Debugger.Break();
            //break cache
            
            Innovator inn = this.getInnovator();
            string strUserId = inn.getUserID();

            Item itmRst = inn.newItem();//用來乘載回傳資料的物件。

            Item itmCheckWithinTime = newItem("Method", "In_Cla_Check_MeetingFunctionValid");
            itmCheckWithinTime.setProperty("actiontype", "2");//基於頁面需求，強制指定為簽到退(2)

            Item itmParams = this.apply("In_Collect_PassengerParam");

            string MeetingHasLoginUser = "";
            string UserInResumeListSql = @"
                SELECT A.SOURCE_ID
                FROM [INNOVATOR].[In_Cla_Meeting_ResumeList] AS A JOIN [INNOVATOR].[IN_RESUME] AS B ON A.RELATED_ID = B.ID 
                WHERE B.IN_USER_ID = '{#Userid}'".Replace("{#Userid}", strUserId);

            Item LoginUserMeeting = inn.applySQL(UserInResumeListSql);
            for (int i = 0; i < LoginUserMeeting.getItemCount(); i++)
            {
                MeetingHasLoginUser += "'" + LoginUserMeeting.getItemByIndex(i).getProperty("source_id") + "'";
                if (i != (LoginUserMeeting.getItemCount() - 1))
                {
                    MeetingHasLoginUser += ",";

                }
            }

            string strUserName = inn.getItemById("User", inn.getUserID()).getProperty("keyed_name");
            string strSelectedMeetingID = this.getProperty("meeting_id");
            Item itmMeetings = inn.applyAML(@"
        <AML>
        	<Item type=""In_Cla_Meeting""  action=""get"" where='[In_Cla_Meeting].id!=[In_Cla_Meeting].in_refmeeting'>
        	<or>
        	   <or>
        		<owned_by_id>
        			<Item type=""Identity"" action=""get"">
        				<name>{#username}</name>
        				<is_aliase>1</is_aliase>
        			</Item>
        		</owned_by_id>
        		</or>
        		<or>
        		<managed_by_id>
        			<Item type=""Identity"" action=""get"">
        				<name>{#username}</name>
        				<is_aliase>1</is_aliase>
        			</Item>
        		</managed_by_id>
        		</or>
        		<or>
        		  <id conditio=""in"" inn_no_translate=""1"">
        			  '0B71B0A5652C49CCA8F48785EADC09FD','789FA4BFBC744021AFA6429452F0FF91','3B42985C4D6240E2A3CFF9A4760F1669'
        		  </id>
        		</or>
            </or>
			<in_current_activity condition='ne'></in_current_activity>
			<in_current_activity condition='is not null'></in_current_activity>
        	</Item>
        </AML>"
                    .Replace("{#username}", strUserName));

            if (!itmMeetings.isEmpty() && !itmMeetings.isError())
            {

                int mtCount = itmMeetings.getItemCount();
                for (int count = 0; count < mtCount; count++)
                {
                    Item tmpMeeting = itmMeetings.getItemByIndex(count);
                    itmCheckWithinTime.setProperty("meeting_id", tmpMeeting.getID());
                    bool withinTime = itmCheckWithinTime.apply().getResult() == "1"; //0 : 不在時間內, 1:在時間內
                    if (withinTime)
                    {
                        itmRst.addRelationship(tmpMeeting);
                    }
                    int paramCount = itmParams.getItemCount();
                    for (int pc = 0; pc < paramCount; pc++)
                    {
                        itmRst.addRelationship(itmParams.getItemByIndex(pc));
                    }
                }




            }

            itmRst.setProperty("inn_meeting_id", strSelectedMeetingID);


            return itmRst;





        }
    }
}