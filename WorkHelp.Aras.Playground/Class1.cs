﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkHelp.Aras.Playground
{
    public class Battle
    {
        public void Execute()
        {
            int player_count = 1096;

            List<TNode> tree1 = GetBaseTree(player_count);

            for(int i = 0; i < tree1.Count; i++)
            {
                TNode node = tree1[i];
                TNode fighter = node.FighterNode;

                if (fighter == null)
                {
                    Console.WriteLine($"階層: {node.Level}，編號: {node.Id}，籤號:{node.SignNo}({node.Cardinality})，失去對手: {node.Memo}");
                }
                else
                {
                    Console.WriteLine($"階層: {node.Level}，編號: {node.Id}，籤號:{node.SignNo}({node.Cardinality})，對戰: {fighter.SignNo}");

                }
            }
        }

        /// <summary>
        /// 建立基本節點
        /// </summary>
        private List<TNode> GetBaseTree(int max_no)
        {
            List<TNode> result = new List<TNode>();

            for (int no = 1; no <= max_no; no++)
            {
                TNode node = new TNode();

                node.Level = 1;
                node.Id = node.Level * 100 + result.Count + 1;

                node.IsEven = no % 2 == 0;

                node.SignNo = no;
                node.SignFoot = node.IsEven ? 2 : 1;

                AppendTreeNode(result, node);
            }
            return result;
        }

 

        /// <summary>
        /// 附加節點
        /// </summary>
        private void AppendTreeNode(List<TNode> tree, TNode node)
        {
            if (tree.Count == 0)
            {
                tree.Add(node);
            }
            else
            {
                int[] array = GetCardinality(node.SignNo);
                int logarithm = array[0];
                int cardinality = array[1];
                int sum = cardinality + 1;


                //尋找對手
                int target_no = sum - node.SignNo;
                int target_pos = -1;
                TNode target_node = null;
                for (int i = 0; i < tree.Count; i++)
                {
                    TNode x = tree[i];
                    if (x.SignNo == target_no)
                    {
                        target_pos = i;
                        target_node = x;
                    }
                }

                //找不到對手節點
                if (target_pos == -1) return;

                //掠奪對手
                if (target_node.FighterNode != null)
                {
                    var related_node = target_node.FighterNode;
                    related_node.FighterNode = null;
                    related_node.LossFighter = true;
                    related_node.Memo = node.SignNo + "/" + target_node.SignNo;
                }

                //將對手改為同輪次
                node.Cardinality = cardinality;
                node.FighterNode = target_node;
                node.LossFighter = false;
                node.Memo = "";

                target_node.FighterNode = node;
                target_node.Cardinality = cardinality;
                target_node.LossFighter = false;
                target_node.FighterNode.Memo = "";

                if (node.IsEven)
                {
                    tree.Insert(target_pos + 1, node);
                }
                else
                {
                    tree.Insert(target_pos, node);
                }
            }
        }

        /// <summary>
        /// 取得基數
        /// </summary>
        private int[] GetCardinality(int value)
        {
            if (value <= 2) return new int[] { 1, 2 };
            if (value <= 4) return new int[] { 2, 4 };
            if (value <= 8) return new int[] { 3, 8 };
            if (value <= 16) return new int[] { 4, 16 };
            if (value <= 32) return new int[] { 5, 32 };
            if (value <= 64) return new int[] { 6, 64 };
            if (value <= 128) return new int[] { 7, 128 };
            if (value <= 256) return new int[] { 8, 256 };
            if (value <= 512) return new int[] { 9, 512 };
            if (value <= 1024) return new int[] { 10, 1024 };
            if (value <= 2048) return new int[] { 11, 2048 };
            if (value <= 4096) return new int[] { 12, 4096 };
            return new int[] { 13, 8192 };
        }
    }

    /// <summary>
    /// 自訂節點類型
    /// </summary>
    public class TNode
    {
        /// <summary>
        /// 階層
        /// </summary>
        public int Level { get; set; }

        /// <summary>
        /// 編號
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 籤號
        /// </summary>
        public int SignNo { get; set; }

        /// <summary>
        /// 籤腳
        /// </summary>
        public int SignFoot { get; set; }

        /// <summary>
        /// 是否為偶數
        /// </summary>
        public bool IsEven { get; set; }

        /// <summary>
        /// 是否為選手
        /// </summary>
        public bool IsPlayer { get; set; }

        /// <summary>
        /// 親代編號
        /// </summary>
        public int ParentId { get; set; }

        /// <summary>
        /// 基數
        /// </summary>
        public int Cardinality { get; set; }

        /// <summary>
        /// 失去對手
        /// </summary>
        public bool LossFighter { get; set; }

        /// <summary>
        /// 對手
        /// </summary>
        public TNode FighterNode { get; set; }

        /// <summary>
        /// 註記
        /// </summary>
        public string Memo { get; set; }
    }

}
