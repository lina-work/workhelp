﻿using Spire.Doc;
using Spire.Doc.Fields;
using Spire.Xls;
using Spire.Xls.Collections;
using Spire.Xls.Core;

namespace WorkHelp.Aras.Playground
{
    public class SpireTest
    {
        private static void Start()
        {
            Workbook workbook = new Workbook();
            workbook.LoadFromFile("Sample.xlsx");
            //Worksheet worksheet = workbook.Worksheets[0];
            //worksheet.AllocatedRange.AutoFitColumns();
            //ColumnWidth
        }
        public static void Start1()
        {
            Workbook workbook = new Workbook();
            workbook.LoadFromFile(@"D:\Match_List_20201023_182052.xlsx");
            Worksheet sheet = workbook.Worksheets[0];
            Document doc = new Document();
            Table table = doc.AddSection().AddTable(true);
            table.ResetCells(sheet.LastRow, sheet.LastColumn);
            for (int r = 1; r <= sheet.LastRow; r++)
            {
                for (int c = 1; c <= sheet.LastColumn; c++)
                {
                    CellRange xCell = sheet.Range[r, c];
                    TableCell wCell = table.Rows[r - 1].Cells[c - 1];
                    //fill data to word table
                    TextRange textRange = wCell.AddParagraph().AppendText(xCell.NumberText);
                    //copy font and cell style from excel to word
                    CopyStyle(textRange, xCell, wCell);
                }
            }
            for (int i = 0; i < table.Rows.Count; i++)
            {
                for (int j = 0; j < table.Rows[i].Cells.Count; j++)
                {
                    table.Rows[i].Cells[j].Width = 60f;
                }
            }
            doc.SaveToFile("result.docx", Spire.Doc.FileFormat.Docx);
            System.Diagnostics.Process.Start("result.docx");
        }

        private static void CopyStyle(TextRange wTextRange, CellRange xCell, TableCell wCell)
        {
            ////copy font stlye
            //wTextRange.CharacterFormat.TextColor = xCell.Style.Font.Color;
            //wTextRange.CharacterFormat.FontSize = (float)xCell.Style.Font.Size;
            //wTextRange.CharacterFormat.FontName = xCell.Style.Font.FontName;
            //wTextRange.CharacterFormat.Bold = xCell.Style.Font.IsBold;
            //wTextRange.CharacterFormat.Italic = xCell.Style.Font.IsItalic;
            ////copy backcolor
            //wCell.CellFormat.BackColor = xCell.Style.Color;
            ////copy text alignment
            //switch (xCell.HorizontalAlignment)
            //{
            //    case HorizontalAlignType.Left:
            //        wTextRange.OwnerParagraph.Format.HorizontalAlignment = HorizontalAlignment.Left;
            //        break;
            //    case HorizontalAlignType.Center:
            //        wTextRange.OwnerParagraph.Format.HorizontalAlignment = HorizontalAlignment.Center;
            //        break;
            //    case HorizontalAlignType.Right:
            //        wTextRange.OwnerParagraph.Format.HorizontalAlignment = HorizontalAlignment.Right;
            //        break;
            //}
        }
    }
}