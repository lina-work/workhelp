﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace WorkHelp.Aras.Playground.ArasModules
{
    public class TConfigX
    {
        /// <summary>
        /// User id
        /// </summary>
        public string in_user_id { get; set; }

        /// <summary>
        /// Identity id
        /// </summary>
        public string identity_id { get; set; }



        /// <summary>
        /// 登入者所屬群組
        /// </summary>
        public string login_resume_group { get; set; }

        /// <summary>
        /// 登入者姓名
        /// </summary>
        public string login_resume_name { get; set; }

        /// <summary>
        /// 登入者身分證號
        /// </summary>
        public string login_resume_sno { get; set; }



        /// <summary>
        /// 繳費單 Identity id
        /// </summary>
        public string pay_identity_id { get; set; }

        /// <summary>
        /// 繳費單 建單者姓名
        /// </summary>
        public string pay_creator_name { get; set; }

        /// <summary>
        /// 繳費單 建單者身分證號
        /// </summary>
        public string pay_creator_sno { get; set; }

        /// <summary>
        /// 繳費單 建單者所屬群組
        /// </summary>
        public string pay_creator_group { get; set; }



        /// <summary>
        /// 是否為賽事活動
        /// </summary>
        public bool IsGame { get; set; }

        /// <summary>
        /// 是否為講習活動
        /// </summary>
        public bool IsClass { get; set; }

        /// <summary>
        /// 是否為盲報
        /// </summary>
        public bool IsAnonymous { get; set; }



        /// <summary>
        /// 活動 id
        /// </summary>
        public string MeetingId { get; set; }

        /// <summary>
        /// 登入者資訊
        /// </summary>
        public Item itmResume { get; set; }

        /// <summary>
        /// 活動資訊 (IN_MEETING OR IN_CLA_MEETING)
        /// </summary>
        public Item itmMeeting { get; set; }

        /// <summary>
        /// 時程資訊 (IN_MEETING_Functiontime OR IN_CLA_MEETING_Functiontime)
        /// </summary>
        public Item itmFunctiontime { get; set; }

        /// <summary>
        /// 與會者 sql
        /// </summary>
        public string MUserSql { get; set; }

        /// <summary>
        /// 單位繳費資料
        /// </summary>
        public Item in_org_excels { get; set; }

        /// <summary>
        /// 單位繳費資料
        /// </summary>
        public List<Item> OrgExcels { get; set; }

        /// <summary>
        /// Meeting ItemType
        /// </summary>
        public string MeetingName { get; set; }

        /// <summary>
        /// MeetingUser ItemType
        /// </summary>
        public string MeetingUserName { get; set; }

        /// <summary>
        /// MeetingFunction ItemType
        /// </summary>
        public string MeetingFunctiontimeName { get; set; }

        /// <summary>
        /// 欄位
        /// </summary>
        public string MeetingProperty { get; set; }

        /// <summary>
        /// 所屬單位
        /// </summary>
        public string in_current_org { get; set; }

        /// <summary>
        /// 發票抬頭
        /// </summary>
        public string invoice_up { get; set; }

        /// <summary>
        /// 統一編號
        /// </summary>
        public string uniform_numbers { get; set; }

        /// <summary>
        /// 所屬單位
        /// </summary>
        public string[] in_current_orgs { get; set; }

        /// <summary>
        /// 發票抬頭
        /// </summary>
        public string[] invoice_ups { get; set; }

        /// <summary>
        /// 統一編號
        /// </summary>
        public string[] uniform_numberss { get; set; }

        /// <summary>
        /// 活動編號
        /// </summary>
        public string in_meeting_code { get; set; }

        /// <summary>
        /// 收款單位編號
        /// </summary>
        public string in_receice_org_code { get; set; }

        /// <summary>
        /// 最後報名日期
        /// </summary>
        public string in_imf_date_e { get; set; }

        /// <summary>
        /// 最後繳費期限
        /// </summary>
        public string last_payment_date { get; set; }

        /// <summary>
        /// 最後繳費期限
        /// </summary>
        public string due_date { get; set; }

        /// <summary>
        /// 隊職員數
        /// </summary>
        public int in_staffs { get; set; }

        /// <summary>
        /// 選手人數
        /// </summary>
        public int in_players { get; set; }

        /// <summary>
        /// 項目總計
        /// </summary>
        public int in_items { get; set; }

        /// <summary>
        /// 費用
        /// </summary>
        public int in_expense { get; set; }

        /// <summary>
        /// 空白
        /// </summary>
        public string blank { get; set; }

        /// <summary>
        /// 備用碼(1)+運動代碼(0)
        /// </summary>
        public string codes { get; set; }

        /// <summary>
        /// 狀態(A>繳款人 B>委託單位)
        /// </summary>
        public string payment_mode { get; set; }

        /// <summary>
        /// 業主自訂編號
        /// </summary>
        public string index_number { get; set; }

        /// <summary>
        /// 條碼2
        /// </summary>
        public string barcode2 { get; set; }

        /// <summary>
        /// 臨櫃繳費虛擬帳號
        /// </summary>
        public string barcode2_1 { get; set; }

        /// <summary>
        /// 項目代碼
        /// </summary>
        public string project_code { get; set; }

        /// <summary>
        /// 條碼1
        /// </summary>
        public string B1 { get; set; }

        /// <summary>
        /// 條碼2
        /// </summary>
        public string B2 { get; set; }

        /// <summary>
        /// 條碼3
        /// </summary>
        public string B3 { get; set; }

        /// <summary>
        /// 當前時間
        /// </summary>
        public DateTime CurrentTime { get; set; }

        /// <summary>
        /// 建單時間
        /// </summary>
        public string AddTime { get; set; }
        

    }
}
