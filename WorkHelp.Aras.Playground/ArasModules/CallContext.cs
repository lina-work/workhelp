﻿using System;

namespace WorkHelp.Aras.Server.Core
{
    public class CallContext
    {
        public ContextState RequestState { get; set; }
        public CallContextUtilities Utilities { get; set; }
    }

    public class CallContextUtilities
    {
        public void WriteDebug(string method, string message)
        {
            Console.WriteLine($"=================== {method} :S =================== ");
            Console.WriteLine($"{message}");
            Console.WriteLine($"=================== {method} :E =================== ");
        }
    }

    public interface IContextState
    { }

    public class ContextState : IContextState
    { }
}
