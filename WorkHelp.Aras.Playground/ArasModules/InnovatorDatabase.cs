﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkHelp.Aras.Playground.ArasModules
{
    /// <summary>
    /// 資料庫連線
    /// </summary>
    public class InnovatorDatabase
    {
        /// <summary>
        /// 取得資料庫名稱
        /// </summary>
        /// <returns></returns>
        public string GetDatabaseName()
        {
            return "ACT";
        }
    }
}
