﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkHelp.Aras.Server.Security
{
    public class Permissions
    {
        public static Permissions Current = new Permissions
        {
            IdentitiesList = "792075B254024F0397F25A7D6399FA26,7AC8B33DA0F246DDA70BB244AB231E29,DEA651BC90CD4F51B732716F1611B0E9"
        };

        public string IdentitiesList { get; set; }

        /// <summary>
        /// 授權
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static bool GrantIdentity (Identity id)
        {
            return true;
        }

        /// <summary>
        /// 移除授權
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static bool RevokeIdentity(Identity id)
        {
            return true;
        }

    }
}
