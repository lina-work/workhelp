﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkHelp.Aras.Server.Security
{
    public class Identity
    {
        /// <summary>
        /// 名稱
        /// </summary>
        public string name { get; set; }

        public static Identity GetByName(string name)
        {
            return new Identity
            {
                name = name
            };
        }
    }
}
