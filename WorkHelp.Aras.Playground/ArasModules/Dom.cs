﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkHelp.Aras.Playground.ArasModules
{
    /// <summary>
    /// 文件物件模型
    /// </summary>
    public class Dom
    {
        /// <summary>
        /// 內部 Xml
        /// </summary>
        public string InnerXml { get; set; }

        /// <summary>
        /// 外部 Xml
        /// </summary>
        public string OuterXml { get; set; }

        
    }
}