﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using WorkHelp.Utilities;

namespace WorkHelp.Aras.Playground.ArasModules
{
    /// <summary>
    /// Arass Item
    /// </summary>
    public class Item
    {
        /// <summary>
        /// 物件類型名稱
        /// </summary>
        public string itemType { get; set; }

        /// <summary>
        /// 操作動作
        /// </summary>
        public string action { get; set; }

        /// <summary>
        /// 主鍵
        /// </summary>
        public string id { get; set; }

        /// <summary>
        /// 文件物件模型
        /// </summary>
        public Dom dom { get; set; }

        public Item newItem(string type, string action)
        {
            return new Item { };
        }

        /// <summary>
        /// 操作應用
        /// </summary>
        /// <returns>Arass Item</returns>
        public Item apply()
        {
            return new Item
            {
                dom = new Dom
                {
                    InnerXml = $"<AML><Item type='{this.itemType}' action='{this.action}'></AML>"
                }
            };
        }

        /// <summary>
        /// 操作應用
        /// </summary>
        /// <param name="action">操作動作</param>
        /// <returns>Arass Item</returns>
        public Item apply(string action)
        {
            return new Item
            {
                dom = new Dom
                {
                    InnerXml = $"<AML><Item type='{this.itemType}' action='{action}'></AML>"
                }
            };
        }

        /// <summary>
        /// 設定主鍵
        /// </summary>
        /// <param name="id">主鍵</param>
        /// <returns></returns>
        public void setID(string id)
        {
            this.id = id;
        }

        /// <summary>
        /// 設定操作動作
        /// </summary>
        /// <param name="action"></param>
        public void setAction(string action)
        {
            this.action = action;
        }

        /// <summary>
        /// 設定屬性值 (內部)
        /// </summary>
        /// <param name="property">屬性</param>
        /// <param name="value">值</param>
        /// <returns></returns>
        public void setProperty(string property, string value)
        {
        }

        /// <summary>
        /// 設定屬性值 (同列)
        /// </summary>
        /// <param name="attribute">屬性</param>
        /// <param name="value">值</param>
        /// <returns></returns>
        public void setAttribute(string attribute, string value)
        {
        }

        /// <summary>
        /// 取得屬性值
        /// </summary>
        /// <param name="conditionAttribute">條件屬性</param>
        /// <param name="conditionValue">條件值</param>
        /// <returns></returns>
        public string getPropertyAttribute(string conditionAttribute, string conditionValue)
        {
            return "假資料";
        }

        /// <summary>
        /// 取得屬性值
        /// </summary>
        /// <param name="conditionAttribute">條件屬性</param>
        /// <param name="conditionValue">條件值</param>
        /// <returns></returns>
        public string getPropertyAttribute(string conditionAttribute, string conditionValue, string defaultValue)
        {
            return "假資料";
        }

        /// <summary>
        /// 取得屬性值
        /// </summary>
        /// <param name="conditionProperty">條件屬性</param>
        /// <param name="conditionValue">條件值</param>
        /// <returns></returns>
        public string getProperty(string conditionProperty, string conditionValue)
        {
            if (conditionValue == "0")
            {
                return RandomUtility.RandomNumber(1, 50).ToString();
            }
            else
            {
                return "假資料";
            }
        }

        /// <summary>
        /// 取得屬性值
        /// </summary>
        /// <param name="conditionProperty">條件屬性</param>
        /// <returns></returns>
        public string getProperty(string conditionProperty)
        {
            return "假資料";
        }

        /// <summary>
        /// 取得屬性項目
        /// </summary>
        /// <param name="property">屬性</param>
        /// <returns></returns>
        public Item getPropertyItem(string property)
        {
            return new Item
            {
                itemType = this.itemType,
                action = this.action,
                dom = new Dom
                {
                    InnerXml = "Dom Xml Tree"
                }
            };
        }

        /// <summary>
        /// 回傳子項目筆數
        /// </summary>
        /// <returns></returns>
        public int getItemCount()
        {
            return 10;
        }

        /// <summary>
        /// 回傳子項目
        /// </summary>
        /// <param name="index">索引值</param>
        /// <returns></returns>
        public Item getItemByIndex(int index)
        {
            return new Item
            {
                itemType = this.itemType,
                action = this.action,
                dom = new Dom
                {
                    InnerXml = "用索引查找子項目"
                },
                node = null,
            };
        }

        public Item loadAML(string aml)
        {
            return new Item();
        }

        public string getRelatedItemID()
        {
            return string.Empty;
        }

        /// <summary>
        /// 加入 Relation Item
        /// </summary>
        /// <param name="item">Relation Item</param>
        public void addRelationship(Item item)
        {
        }

        /// <summary>
        /// 取得關聯項目集
        /// </summary>
        /// <param name="condition">條件式</param>
        /// <returns></returns>
        public Item getRelationships(string condition)
        {
            return new Item
            {
                itemType = this.itemType,
                action = this.action,
                dom = new Dom
                {
                    InnerXml = "關聯項目集"
                }
            };
        }
        /// <summary>
        /// 取得關聯項目
        /// </summary>
        /// <returns></returns>
        public Item getRelatedItem()
        {
            return new Item
            {
                itemType = this.itemType,
                action = this.action,
                dom = new Dom
                {
                    InnerXml = "關聯項目集"
                }
            };
        }

        /// <summary>
        /// 取得結果
        /// </summary>
        /// <returns></returns>
        public string getResult()
        {
            return string.Empty;
        }

        /// <summary>
        /// 取得 id
        /// </summary>
        /// <returns></returns>
        public string getID()
        {
            return System.Guid.NewGuid().ToString().Replace("-", string.Empty);
        }

        public Item getItemsByXPath(string path)
        {
            return new Item
            {
                itemType = this.itemType,
                action = this.action,
                dom = new Dom
                {
                    InnerXml = "path"
                }
            };
        }

        public bool isError()
        {
            return false;
        }

        public Innovator getInnovator()
        {
            return new Innovator();
        }

        public void setErrorDetail(string message)
        {

        }

        public void setType(string typeName)
        {
            this.itemType = typeName;
        }

        public void setRelatedItem(Item value)
        {

        }
        
        public bool isEmpty()
        {
            return false;
        }

        public string getErrorString()
        {
            return "error";
        }

        public string getErrorDetail()
        {
            return "error";
        }
        
        public XmlElement node { get; set; }

    }
}