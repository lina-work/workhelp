﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkHelp.Aras.Playground.ArasModules
{

    public class Innovator
    {
        /// <summary>
        /// 取得資料庫連線
        /// </summary>
        /// <returns></returns>
        public InnovatorDatabase getConnection()
        {
            return new InnovatorDatabase();
        }


        /// <summary>
        /// 新建一個項目
        /// </summary>
        /// <returns></returns>
        public Item newItem()
        {
            return new Item
            {
                dom = new Dom
                {
                    
                }
            };
        }

        /// <summary>
        /// 新建一個項目
        /// </summary>
        /// <param name="itemType">物件類型名稱</param>
        /// <param name="action">操作動作</param>
        /// <returns></returns>
        public Item newItem(string itemType, string action = "")
        {
            return new Item
            {
                itemType = itemType,
                action = action,
                dom = new Dom
                {
                    InnerXml = $"<AML><Item type='{itemType}' action='{action}'></AML>"
                }
            };
        }

        /// <summary>
        /// 新建項目結果
        /// </summary>
        /// <param name="command">指令</param>
        /// <returns></returns>
        public Item newResult(string command)
        {
            return new Item
            {
                itemType = string.Empty,
                action = string.Empty,
                dom = new Dom
                {
                    InnerXml = $"<AML><Item type='' action=''></AML>"
                }
            };
        }

        /// <summary>
        /// 操作應用
        /// </summary>
        /// <param name="aml">AML 字串</param>
        /// <returns>Arass Item</returns>
        public Item applyAML(string aml)
        {
            return new Item
            {
                dom = new Dom
                {
                    InnerXml = aml
                }
            };
        }

        /// <summary>
        /// 操作應用
        /// </summary>
        /// <param name="sql">sql 指令</param>
        /// <returns>Arass Item</returns>
        public Item applySQL(string sql)
        {
            WorkHelp.Logging.TLog.Watch(message: sql);

            return new Item
            {
                dom = new Dom
                {
                    InnerXml = sql
                }
            };
        }

        /// <summary>
        /// 調用 Method
        /// </summary>
        /// <param name="method">方法名稱</param>
        /// <param name="aml">Aml</param>
        /// <returns>Arass Item</returns>

        public Item applyMethod(string method, string aml)
        {
            return new Item
            {
                dom = new Dom { },
                node = null,
            };
        }

        public string getUserID()
        {
            return "userId";
        }

        public Item getItemById(string itemType, string id)
        {
            return new Item
            {
                dom = new Dom { },
                node = null,
            };
        }
        public Item getItemByKeyedName(string itemType, string keyedName)
        {
            return new Item
            {
                dom = new Dom { },
                node = null,
            };
        }
        public Item newError(string message)
        {
            return new Item
            {
                dom = new Dom { },
                node = null,
            };
        }
        public string getUserAliases()
        {
            return "";
        }
    }
}