﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class temp_Fix_SiteUrl : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]Fix_SiteUrl";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "input dom: " + itmR.dom.InnerXml);

            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            Item itmPermit = inn.applyMethod("In_CheckIdentity", "<method>" + strMethodName + "</method><code>" + "ALL" + "</code>");
            bool isMeetingAdmin = itmPermit.getProperty("isMeetingAdmin", "") == "1";

            if (!isMeetingAdmin) throw new Exception("無權限");

            string aml = "";
            string sql = "";

            sql = "SELECT * FROM IN_SiteType WITH(NOLOCK)";
            Item itmSiteTypes = inn.applySQL(sql);
            int type_count = itmSiteTypes.getItemCount();

            Dictionary<string, string> type_map = new Dictionary<string, string>();
            for (int i = 0; i < type_count; i++)
            {
                Item itmSiteType = itmSiteTypes.getItemByIndex(i);
                string type_id = itmSiteType.getProperty("id", "");
                string in_value = itmSiteType.getProperty("in_value", "");
                type_map.Add(in_value, type_id);
            }

            sql = "SELECT t1.*, t2.in_value FROM IN_SITEMAP t1 INNER JOIN IN_SITEMAIN t2 ON t2.id = t1.source_id";
            Item itmSites = inn.applySQL(sql);
            int count = itmSites.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item itmSite = itmSites.getItemByIndex(i);
                string in_value = itmSite.getProperty("in_value", "");
                string in_name1 = itmSite.getProperty("in_name1", "");
                string in_url1 = itmSite.getProperty("in_url1", "");
                string in_desc1 = itmSite.getProperty("in_desc1", "");
                string in_name2 = itmSite.getProperty("in_name2", "");
                string in_url2 = itmSite.getProperty("in_url2", "");
                string in_desc2 = itmSite.getProperty("in_desc2", "");
                string sort_order = itmSite.getProperty("sort_order", "");


                Item itmUrl = inn.newItem("IN_SiteUrl", "add");
                itmUrl.setProperty("in_name1", in_name1);
                itmUrl.setProperty("in_url1", in_url1);
                itmUrl.setProperty("in_desc1", in_desc1);
                itmUrl.setProperty("in_name2", in_name2);
                itmUrl.setProperty("in_url2", in_url2);
                itmUrl.setProperty("in_desc2", in_desc2);
                itmUrl = itmUrl.apply();

                string url_id = itmUrl.getID();
                string type_id = type_map.ContainsKey(in_value) ? type_map[in_value] : "";
                if (type_id == "")
                {
                    CCO.Utilities.WriteDebug(strMethodName, "查無網站類別：" + in_value);
                    continue;
                }

                Item itmUrlMap = inn.newItem("IN_SiteUrl_Map", "add");
                itmUrlMap.setProperty("source_id", url_id);
                itmUrlMap.setProperty("related_id", type_id);
                itmUrlMap.setProperty("sort_order", sort_order);
                itmUrlMap = itmUrlMap.apply();
            }

            return itmR;
        }
    }
}