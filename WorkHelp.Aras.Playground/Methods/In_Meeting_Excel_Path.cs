﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;
using System.Net;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class In_Meeting_Excel_Path : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            Innovator inn = this.getInnovator();

            string sql = @"
                SELECT 
	                t2.in_name AS 'variable'
	                , t1.in_name
	                , t1.in_value 
                FROM 
                    In_Variable_Detail t1 WITH(NOLOCK)
                INNER JOIN 
                    In_Variable t2 WITH(NOLOCK) 
	                ON t2.id = t1.source_id 
                WHERE 
                    t2.in_name = 'meeting_excel'  
                    AND t1.in_name IN ('export_path', '{#in_name}')
            ";

            sql = sql.Replace("{#in_name}", this.getProperty("in_name", ""));

            Item items = inn.applySQL(sql);
            string export_path = "";
            string template_path = "";
            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_name = item.getProperty("in_name", "");
                string in_value = item.getProperty("in_value", "");

                if (in_name == "export_path")
                {
                    export_path = in_value;
                    if (!export_path.EndsWith(@"\"))
                    {
                        export_path = export_path + @"\";
                    }
                }
                else
                {
                    template_path = in_value;
                }
            }

            Item itmR = inn.newItem();
            itmR.setProperty("export_path", export_path);
            itmR.setProperty("template_path", template_path);

            return itmR;
        }
    }
}