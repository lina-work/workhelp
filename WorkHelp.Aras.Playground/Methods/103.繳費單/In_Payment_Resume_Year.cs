﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;
using System.Net;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class In_Payment_Resume_Year : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
            目的: 年底產生會員年費繳費單
            日期: 
                - 2021/12/06: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]In_Payment_Resume_Year";

            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "input dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                InnH = _InnH,

                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
            };
            DateTime now = DateTime.Now;
            int m = now.Month;
            if (m == 12)
            {
                cfg.TargetWestYear = now.Year + 1;
                cfg.TargetTwYear = cfg.TargetWestYear - 1911;
            }
            else if (m == 1)
            {
                cfg.TargetWestYear = now.Year;
                cfg.TargetTwYear = cfg.TargetWestYear - 1911;
            }
            else
            {
                throw new Exception("限定於12月~1月期間產生");
            }

            //去年
            cfg.LastWestYear = cfg.TargetWestYear - 1;
            cfg.LastTwYear = cfg.TargetTwYear - 1;

            cfg.strMethodName_SQL = cfg.strMethodName + "_SQL";
            cfg.strMethodName_PRC = cfg.strMethodName + "_PRC";

            //清理會員狀態 2022 版本
            MemberStatusFor2022(cfg);

            //協會特權
            AssociationSpecial(cfg);

            //清理其他欄位 2022 版本
            ClearOtherProperty2022(cfg);

            //將會員數量統計 Log 下來
            LogSummary2022(cfg);

            ////開始產生會員年費繳費單
            //MergeYearPayments(cfg);

            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

            return itmR;
        }

        //開始產生會員年費繳費單
        private void MergeYearPayments(TConfig cfg)
        {
            string sql = @"
                SELECT 
	                t1.in_name
	                , t1.in_sno
	                , t1.in_member_type
	                , t1.in_member_status
	                , t1.in_pay_year3     AS 'Y2019'
	                , t1.in_pay_year2     AS 'Y2020'
	                , t1.in_pay_year1     AS 'Y2021'
	                , t1.in_pay_year4     AS 'Y2022'
	                , t2.in_name          AS 'manager_name'
	                , t2.in_sno           AS 'manager_sno'
	                , t2.in_manager_area  AS 'manager_area'
					, t3.item_number      AS 'target_year_paynumber'
                FROM 
	                IN_RESUME t1 WITH(NOLOCK)
                LEFT OUTER JOIN
	                IN_RESUME t2 WITH(NOLOCK)
	                ON t2.id = t1.in_manager_org
				LEFT OUTER JOIN
				(
					SELECT t11.item_number, t11.in_creator_sno 
					FROM IN_MEETING_PAY t11 WITH(NOLOCK) INNER JOIN IN_MEETING t12 WITH(NOLOCK) 
					ON t12.id = t11.in_meeting AND t12.in_meeting_type = 'payment' AND t12.in_annual = '{#tw_year}'
				) t3 ON t3.in_creator_sno = t1.in_sno
                WHERE 
	                ISNULL(t1.in_member_status, '') IN (N'合格會員', N'暫時停權')
	                AND ISNULL(t1.in_member_type, '') IN ('asc', 'area_cmt', 'vip_group', 'vip_gym', 'vip_mbr')
	                AND t1.in_sno NOT IN ('M001')
                ORDER BY 
	                t1.in_member_type
	                , t1.in_member_status
	                , t1.in_sno
            ";

            sql = sql.Replace("{#tw_year}", cfg.TargetTwYear.ToString());

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName_SQL, sql);

            Item items = cfg.inn.applySQL(sql);
            int count = items.getItemCount();
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName_PRC, "START _# rowCount: " + count);


            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_name = item.getProperty("in_name", "");
                string in_sno = item.getProperty("in_sno", "");
                string in_member_type = item.getProperty("in_member_type", "");
                string in_member_status = item.getProperty("in_member_status", "");
                string manager_name = item.getProperty("manager_name", "");
                string manager_sno = item.getProperty("manager_sno", "");
                string manager_area = item.getProperty("manager_area", "");
                string target_year_paynumber = item.getProperty("target_year_paynumber", "");

                string member = "";

                switch (in_member_type)
                {
                    case "area_cmt":
                        member = "縣市委員會";
                        break;

                    case "asc":
                        member = "協會人員";
                        break;

                    case "vip_group":
                        member = "其他團體";
                        break;

                    case "vip_gym":
                        member = "道館社團";
                        break;

                    case "vip_mbr":
                        member = "個人會員";
                        break;
                }

                string msg = "    - " + (i + 1) + ". [" + member + "]"
                    + in_name
                    + " (" + in_sno + ")"
                    + ": " + in_member_status
                    + "_# 年費繳費單: " + target_year_paynumber;

                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName_PRC, msg);

                if (target_year_paynumber != "")
                {
                    cfg.CCO.Utilities.WriteDebug(cfg.strMethodName_PRC, "        - 不產生年費繳費單");
                    continue;
                }

                //產生年費繳費單
                Item itmData = cfg.inn.newItem();
                itmData.setType("In_Resume");
                itmData.setProperty("in_sno", in_sno);
                itmData.setProperty("inn_new_apply", "");//非新註冊
                itmData.setProperty("inn_target_year", cfg.TargetWestYear.ToString());
                Item itmResult = itmData.apply("In_Payment_Resume");

                if (!itmResult.isError())
                {
                    cfg.CCO.Utilities.WriteDebug(cfg.strMethodName_PRC, "        - 產生年費繳費單: 成功");

                    //取消 110 年度的會員年費繳費單
                    CancelLastYearPayment(cfg, in_sno);
                }
                else
                {
                    cfg.CCO.Utilities.WriteDebug(cfg.strMethodName_PRC, "        - 產生年費繳費單: 失敗");
                }
            }

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName_PRC, "END");
        }

        //取消 110 年度的會員年費繳費單
        private void CancelLastYearPayment(TConfig cfg, string in_sno)
        {
            string last_tw_year = (cfg.LastWestYear - 1911).ToString();

            string sql = @"
                SELECT
	                t1.id
	                , t1.item_number
	                , t1.in_pay_amount_exp
	                , t2.in_title       AS 'mt_title'
	                , t2.in_member_type AS 'mt_member_type'
	                , t2.in_annual      AS 'mt_annual'
                FROM 
	                IN_MEETING_PAY t1 WITH(NOLOCK) 
                INNER JOIN
	                IN_MEETING t2 WITH(NOLOCK)
	                ON t2.id = t1.in_meeting
	                AND t2.in_meeting_type = 'payment'
                WHERE
	                t2.in_annual = '{#last_tw_year}'
	                AND t1.pay_bool = N'未繳費'
	                AND t1.in_creator_sno = '{#in_sno}'
            ";

            sql = sql.Replace("{#last_tw_year}", last_tw_year)
                .Replace("{#in_sno}", in_sno);

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName_SQL, sql);

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string mpid = item.getProperty("id", "");
                string item_number = item.getProperty("item_number", "");
                string mt_title = item.getProperty("mt_title", "");
                string mt_member_type = item.getProperty("mt_member_type", "");

                string msg = "        - " + (i + 1) + ". 取消繳費單(去年): "
                    + in_sno
                    + "|" + item_number
                    + "|" + mt_title
                    + "|" + mt_member_type;

                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName_PRC, msg);

                string sql_cancel = "UPDATE IN_MEETING_PAY SET pay_bool = N'已取消' WHERE id = '" + mpid + "'";
                Item itmSQL = cfg.inn.applySQL(sql_cancel);
            }
        }

        //將會員數量統計 Log 下來
        private void LogSummary2022(TConfig cfg)
        {
            string sql = @"
                SELECT in_member_type, in_member_status, in_member_unit, count(*) AS 'cnt' FROM IN_RESUME WITH(NOLOCK)
                WHERE ISNULL(in_member_status, '') IN (N'合格會員', N'暫時停權')
                AND ISNULL(in_member_type, '') NOT IN ('', 'vip_minority')
                GROUP BY in_member_type, in_member_status, in_member_unit
                ORDER BY in_member_type, in_member_status, in_member_unit
            ";

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName_SQL, sql);

            Item items = cfg.inn.applySQL(sql);
            int count = items.getItemCount();

            StringBuilder builder = new StringBuilder();
            builder.AppendLine("會員數量統計");

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_member_type = item.getProperty("in_member_type", "");
                string in_member_status = item.getProperty("in_member_status", "");
                string in_member_unit = item.getProperty("in_member_unit", "");
                string cnt = item.getProperty("cnt", "0");

                string member = "";

                switch (in_member_type)
                {
                    case "area_cmt":
                        member = "縣市委員會";
                        builder.AppendLine(member + "\t" + in_member_status + "\t" + "" + "\t" + cnt);
                        break;

                    case "asc":
                        member = "協會人員";
                        builder.AppendLine(member + "\t" + in_member_status + "\t" + "" + "\t" + cnt);
                        break;

                    case "vip_group":
                        member = "其他團體";
                        builder.AppendLine(member + "\t" + in_member_status + "\t" + "" + "\t" + cnt);
                        break;

                    case "vip_gym":
                        member = "道館社團";
                        builder.AppendLine(member + "\t" + in_member_status + "\t" + in_member_unit + "\t" + cnt);
                        break;

                    case "vip_mbr":
                        member = "個人會員";
                        builder.AppendLine(member + "\t" + in_member_status + "\t" + "" + "\t" + cnt);
                        break;
                }
            }

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName_PRC, builder.ToString());
        }

        //清理其他欄位 2022 版本
        private void ClearOtherProperty2022(TConfig cfg)
        {
            string sql = "";
            Item itmSQL = null;

            sql = @"UPDATE IN_RESUME SET in_member_type = 'vip_gym' WHERE in_sno = 'G02292'";
            itmSQL = cfg.inn.applySQL(sql);

            //統計才會正確
            sql = @"UPDATE IN_RESUME SET in_member_unit = '' WHERE in_member_unit IS NULL;";
            itmSQL = cfg.inn.applySQL(sql);
        }

        //協會特權
        private void AssociationSpecial(TConfig cfg)
        {
            string sql = "";
            Item itmSQL = null;

            //理事長
            sql = @"UPDATE IN_RESUME SET in_member_type = 'asc' WHERE in_sno = 'K121026406'";
            itmSQL = cfg.inn.applySQL(sql);

            //協會人員強制為合格會員
            sql = @"UPDATE IN_RESUME SET in_member_status = N'合格會員' WHERE in_member_type = 'asc'";
            itmSQL = cfg.inn.applySQL(sql);
        }

        //清理會員狀態 2022 版本
        private void MemberStatusFor2022(TConfig cfg)
        {
            string pro1 = "in_pay_year2";//Y-2
            string pro2 = "in_pay_year1";//Y-1

            //switch (cfg.TargetWestYear)
            //{
            //    case 2022:
            //        pro1 = "in_pay_year2"; //2020
            //        pro2 = "in_pay_year1"; //2021
            //        break;

            //    case 2023:
            //        pro1 = "in_pay_year1"; //2021
            //        pro2 = "in_pay_year4"; //2022
            //        break;

            //    case 2024:
            //        pro1 = "in_pay_year4"; //2022
            //        pro2 = "in_pay_year5"; //2023
            //        break;

            //    case 2025:
            //        pro1 = "in_pay_year5"; //2023
            //        pro2 = "in_pay_year6"; //2024
            //        break;

            //    case 2026:
            //        pro1 = "in_pay_year5"; //2023
            //        pro2 = "in_pay_year6"; //2024
            //        break;

            //    case 2027:
            //        pro1 = "in_pay_year5"; //2023
            //        pro2 = "in_pay_year6"; //2024
            //        break;

            //    case 2028:
            //        pro1 = "in_pay_year5"; //2023
            //        pro2 = "in_pay_year6"; //2024
            //        break;

            //    case 2029:
            //        pro1 = "in_pay_year5"; //2023
            //        pro2 = "in_pay_year6"; //2024
            //        break;

            //    case 2030:
            //        pro1 = "in_pay_year5"; //2023
            //        pro2 = "in_pay_year6"; //2024
            //        break;

            //    case 2031:
            //        pro1 = "in_pay_year5"; //2023
            //        pro2 = "in_pay_year6"; //2024
            //        break;

            //    default:
            //        throw new Exception("目前僅提供至 2025");
            //}

            string sql = "";
            Item itmSQL = null;

            sql = "UPDATE IN_RESUME SET " + pro1 + " = 0 WHERE " + pro1 + " IS NULL;";
            itmSQL = cfg.inn.applySQL(sql);

            //清除繳費年度狀態 NULL 值 - Y-2
            sql = "UPDATE IN_RESUME SET " + pro1 + " = 0 WHERE " + pro1 + " IS NULL;";
            itmSQL = cfg.inn.applySQL(sql);

            //清除繳費年度狀態 NULL 值 - Y-1
            sql = "UPDATE IN_RESUME SET " + pro2 + " = 0 WHERE " + pro2 + " IS NULL;";
            itmSQL = cfg.inn.applySQL(sql);

            sql = @"
-- Y-2、Y-1 皆未繳費，變更為【出會】
UPDATE IN_RESUME SET in_member_status = N'出會' WHERE {#pro1} = 0 AND {#pro2} = 0 
AND ISNULL(in_member_type, '') NOT IN (N'asc', N'sys')
AND ISNULL(in_member_status, '') NOT IN ('', N'暫時會員', N'審核未通過', N'除名', N'逝世')
            ";
            sql = sql.Replace("{#pro1}", pro1).Replace("{#pro2}", pro2);
            itmSQL = cfg.inn.applySQL(sql);

            sql = @"
-- Y-2、Y-1 只繳一年，變更為【暫時停權】
UPDATE IN_RESUME SET in_member_status = N'暫時停權' WHERE ({#pro1} = 1 OR {#pro2} = 1)
AND ISNULL(in_member_type, '') NOT IN (N'asc', N'sys')
AND ISNULL(in_member_status, '') NOT IN ('', N'暫時會員', N'審核未通過', N'除名', N'逝世')
            ";
            sql = sql.Replace("{#pro1}", pro1).Replace("{#pro2}", pro2);
            itmSQL = cfg.inn.applySQL(sql);

            sql = @"
-- Y-2、Y-1 皆已繳費，變更為【合格會員】
UPDATE IN_RESUME SET in_member_status = N'合格會員' WHERE {#pro1} = 1 AND {#pro2} = 1 
AND ISNULL(in_member_status, '') NOT IN ('', N'暫時會員', N'審核未通過', N'除名', N'逝世')
            ";
            sql = sql.Replace("{#pro1}", pro1).Replace("{#pro2}", pro2);
            itmSQL = cfg.inn.applySQL(sql);

            //去年入會
            sql = @"
UPDATE t1 SET in_apply_year = '{#LastWestYear}'
FROM
	IN_RESUME t1
INNER JOIN
(
	SELECT in_sno FROM IN_MEETING_USER 
	WHERE source_id IN 
	(
		'249FDB244E534EB0AA66C8E9C470E930'
		, '5F73936711E04DC799CB02587F4FF7E0'
		, '83B87AE0033640AA8DBA7AF2CF659479'
		, '38CAB90DF1274E048520A801948AC65C'
	)
	AND in_member_apply = N'是'
) t2 ON t2.in_sno = t1.in_sno
WHERE
    YEAR(CREATED_ON) = {#LastWestYear}
            ";
            sql = sql.Replace("{#LastWestYear}", cfg.LastWestYear.ToString());
            itmSQL = cfg.inn.applySQL(sql);

            sql = @"
-- 去年入會、去年已繳費，變更為【合格會員】
UPDATE IN_RESUME SET in_member_status = N'合格會員' WHERE in_apply_year = '{#LastWestYear}' AND {#pro2} = 1 
AND ISNULL(in_member_status, '') NOT IN ('', N'暫時會員', N'審核未通過', N'除名', N'逝世')
            ";
            sql = sql.Replace("{#pro1}", pro1).Replace("{#pro2}", pro2);
            itmSQL = cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public Innosoft.InnovatorHelper InnH { get; set; }

            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strMethodName_SQL { get; set; }
            public string strMethodName_PRC { get; set; }
            public string strUserId { get; set; }

            public int TargetWestYear { get; set; }
            public int LastWestYear { get; set; }

            public int TargetTwYear { get; set; }
            public int LastTwYear { get; set; }
        }
    }
}