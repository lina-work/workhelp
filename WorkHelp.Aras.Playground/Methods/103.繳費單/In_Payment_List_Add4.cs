﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class In_Payment_List_Add4 : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 產生繳費條碼(晉段專用)
                對象: 委員會
                描述: 委員會產生繳費單轉帳給協會
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]" + "In_Payment_List_Add4";

            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                strIdentityId = inn.getUserAliases(),

                blank = " ",
                codes = "10",
                in_meeting_type = "",
                in_meeting_code = "",
                in_receice_org_code = "",
                payment_mode = "A",
                project_code = "",
                index_number = "",
                barcode2 = "",
                barcode2_1 = "",
                in_imf_date_e = "",

                B1 = "",
                B2 = "",
                B3 = "",

                CurrentTime = System.DateTime.Today,
                AddTime = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss"),

                meeting_id = itmR.getProperty("meeting_id", "")
            };

            //應繳日期&最後繳費日期為[今日+7天](尚未確定範圍)
            cfg.due_date = cfg.CurrentTime.AddDays(7).ToString("yyyy-MM-ddTHH:mm:ss");//應繳日期
            cfg.last_payment_date = cfg.CurrentTime.AddDays(7).ToString("yyyy-MM-ddTHH:mm:ss");//最後收費日期

            cfg.MtType = "In_Cla_Meeting";
            cfg.MUserType = "In_Cla_Meeting_User";
            cfg.MFuncType = "In_Cla_Meeting_Functiontime";
            cfg.MtProperty = "in_cla_meeting";


            //設定權限角色
            Item itmPermit = inn.applyMethod("In_CheckIdentity", "<method>" + strMethodName + "</method><code>ALL</code>");
            cfg.isMeetingAdmin = itmPermit.getProperty("isMeetingAdmin", "") == "1";
            cfg.isCommittee = itmPermit.getProperty("isCommittee", "") == "1";
            cfg.isGymOwner = itmPermit.getProperty("isGymOwner", "") == "1";
            cfg.isGymAssistant = itmPermit.getProperty("isGymAssistant", "") == "1";


            //取得登入者資訊
            cfg.itmLogin = inn.applySQL("SELECT * FROM In_Resume WITH(NOLOCK) WHERE in_user_id = '" + cfg.strUserId + "'");
            cfg.login_resume_group = cfg.itmLogin.getProperty("in_group", "");//所屬單位
            cfg.login_resume_name = cfg.itmLogin.getProperty("in_name", "");//登入者姓名
            cfg.login_resume_sno = cfg.itmLogin.getProperty("in_sno", "");//登入者身分證號
            cfg.login_manager_name = cfg.itmLogin.getProperty("in_manager_name", "");//登入者所屬委員會

            //取得活動資料
            cfg.itmMeeting = inn.applyAML("<AML><Item type='" + cfg.MtType + "' action='get'><id>" + cfg.meeting_id + "</id></Item></AML>");
            cfg.in_meeting_code = cfg.itmMeeting.getProperty("item_number", "");//賽事編號
            cfg.in_meeting_type = cfg.itmMeeting.getProperty("in_meeting_type", "").ToLower();//活動類型
            cfg.in_receice_org_code = cfg.itmMeeting.getProperty("in_receice_org_code", "");//收款單位編號


            if (cfg.in_receice_org_code == "")
            {
                throw new Exception("Meeting 收款單位編號不可為空白");
            }
            else if (cfg.in_receice_org_code.Length != 6)
            {
                throw new Exception("收款單位編號格式錯誤");
            }

            //取得該[賽事的報名結束日]
            cfg.itmFunctiontime = inn.applyAML("<AML><Item type='" + cfg.MFuncType + "' action='get'><source_id>" + cfg.meeting_id + "</source_id><in_action>sheet1</in_action></Item></AML>");
            cfg.in_imf_date_e = cfg.itmFunctiontime.getProperty("in_date_e", "");//最後報名日期

            //如果[建單日期]與[賽事報名結束日期]差距天數小於7天 直接讓[最後繳費期限]訂在[報名結束日期]
            DateTime in_imf_date_e_dt = Convert.ToDateTime(cfg.in_imf_date_e);//最後報名日期
            TimeSpan ts = in_imf_date_e_dt - System.DateTime.Today;
            double days = ts.TotalDays;

            if (days < 7)
            {
                cfg.last_payment_date = cfg.in_imf_date_e;
                cfg.due_date = cfg.in_imf_date_e;
            }

            //繳費單歸屬
            cfg.pay_identity_id = cfg.strIdentityId;
            cfg.pay_creator_name = cfg.login_resume_name;
            cfg.pay_creator_sno = cfg.login_resume_sno;
            cfg.pay_creator_group = cfg.login_resume_group;
            cfg.pay_committee = cfg.login_resume_name;

            //取得統計摘要
            Item itmSummary = GetMUsersSummary(cfg);
            //取得報名者資料
            Item itmMUsers = GetMUsers(cfg);

            //新增繳費單與明細
            AddPayment(CCO, strMethodName, inn, cfg, itmSummary, itmMUsers, itmR);

            ////為這些報名者建立會員繳費單
            //AddResumePayment(CCO, strMethodName, inn, cfg, itmMUsers, itmR);

            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

            return itmR;
        }

        //統計摘要(理應只一筆資料)
        private Item GetMUsersSummary(TConfig cfg)
        {
            string sql = @"
                SELECT
                    in_committee AS 'in_current_org'
                    , sum(in_expense) AS 'expenses'
                    , count(*)        AS 'in_qty'
                FROM
                    {#MUserType} WITH(NOLOCK)
                WHERE
                    source_id = '{#meeting_id}'
                    AND in_committee = N'{#in_committee}'
                    AND ISNULL(IN_PAYNUMBER, '') = ''
                    AND ISNULL(IN_VERIFY_RESULT, '') = '1'
                GROUP BY
                    in_committee
            ";

            sql = sql.Replace("{#MUserType}", cfg.MUserType)
                .Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_committee}", cfg.login_resume_name);

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        //報名者資料
        private Item GetMUsers(TConfig cfg)
        {
            string sql = @"
                SELECT 
                    * 
                FROM 
                    {#MUserType} WITH(NOLOCK)
                WHERE
                    source_id = '{#meeting_id}'
                    AND in_committee = N'{#in_committee}'
                    AND ISNULL(IN_PAYNUMBER, '') = ''
                    AND ISNULL(IN_VERIFY_RESULT, '') = '1'
            ";

            sql = sql.Replace("{#MUserType}", cfg.MUserType)
                .Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_committee}", cfg.login_resume_name);

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// 新增繳費單與明細資料
        /// </summary>
        private void AddPayment(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, TConfig cfg, Item itmSummary, Item itmMUsers, Item itmReturn)
        {
            string aml = "";
            string sql = "";

            //預設為 10
            string codes = cfg.codes;

            //當收款單位編號長度為 6 碼
            if (cfg.in_receice_org_code.Length == 6)
            {
                codes = "";
            }

            string numbers = "";

            int muser_count = itmMUsers.getItemCount();

            //將最後收費日期&應繳日期轉型
            DateTime in_pay_date_exp1 = Convert.ToDateTime(cfg.last_payment_date.Split('T')[0]);
            DateTime in_pay_date_exp = Convert.ToDateTime(cfg.due_date.Split('T')[0]);

            //費用
            string xls_money = itmSummary.getProperty("expenses", "");
            string xls_current_org = itmSummary.getProperty("in_current_org", "");//所屬單位
            string xls_real_stuff = "0";
            string xls_real_player = muser_count.ToString();
            string xls_real_items = muser_count.ToString();

            //區分繳費模式(A>繳款人 B>委託單位)
            cfg.project_code = GetProjectCode(cfg.payment_mode, Int32.Parse(xls_money));

            //作廢
            string b2_1 = cfg.in_meeting_code.Substring(cfg.in_meeting_code.Length - 4);

            //業主自訂編號: [賽事/講習序號(後3碼)] + 自訂流水號(4碼)(預設為0001)
            //[運動賽事序號(後3碼)]
            string b2_2 = cfg.in_meeting_code.Substring(cfg.in_meeting_code.Length - 3);

            string index_number_prefix = b2_2.TrimStart('0');

            //找出該[賽事]繳費單最大的[業主自訂編號]
            sql = "SELECT MAX(index_number)";
            sql += " FROM In_Meeting_pay WITH(NOLOCK)";
            sql += " WHERE index_number LIKE '" + index_number_prefix + "%'";
            Item itmPay = inn.applySQL(sql);

            if (itmPay.getResult() == "")
            {
                cfg.barcode2 = b2_1 + "0001";
                cfg.barcode2_1 = b2_2 + "0001";
            }
            else
            {
                //若不是第一張單則將最大的[業主自訂編號]+1
                int number_next = Int32.Parse(itmPay.getProperty("column1", "0")) + 1;
                cfg.index_number = number_next.ToString();
                string suffix = cfg.index_number.Substring(cfg.index_number.Length - 4);

                cfg.barcode2 = b2_1 + suffix;
                cfg.barcode2_1 = b2_2 + suffix;
            }

            //這是超商繳費
            //第一碼(最後收費日期(DateTime),項目代碼(String))
            cfg.B1 = BarCode1_OutPut(in_pay_date_exp1, cfg.project_code);
            //第二碼(賽事編號(String),業者自訂編號(String))
            cfg.B2 = cfg.in_receice_org_code + cfg.barcode2_1;

            string checkhum = inn.applyMethod("In_GetRank_CheckSum", "<account>" + cfg.B2 + "</account><pay>" + xls_money + "</pay>").getResult();

            cfg.B2 = cfg.B2 + checkhum;

            //第三碼(應繳日期(DateTime),費用(String),一碼(String),二碼(String))
            cfg.B3 = BarCode3_OutPut(in_pay_date_exp, xls_money, cfg.B1, cfg.B2);

            //產生繳費單
            Item SingleNumber = inn.newItem("In_Meeting_pay", "add");

            SingleNumber.setProperty("owned_by_id", cfg.pay_identity_id);//建單者
            SingleNumber.setProperty("in_creator", cfg.pay_creator_name);//協助報名者
            SingleNumber.setProperty("in_creator_sno", cfg.pay_creator_sno);//協助報名者身分證字號
            SingleNumber.setProperty("in_group", cfg.pay_creator_group);//所屬群組
            SingleNumber.setProperty("in_committee", cfg.pay_committee);//所屬跆委會

            SingleNumber.setProperty("in_current_org", xls_current_org);//所屬單位
            SingleNumber.setProperty("in_pay_amount_exp", xls_money);//應繳金額

            SingleNumber.setProperty("in_pay_date_exp", cfg.due_date.Split('T')[0]);//應繳日期
            SingleNumber.setProperty("in_pay_date_exp1", cfg.last_payment_date.Split('T')[0]);//最後收費日期

            SingleNumber.setProperty("in_real_stuff", xls_real_stuff);//隊職員數
            SingleNumber.setProperty("in_real_player", xls_real_player);//選手人數
            SingleNumber.setProperty("in_real_items", xls_real_items);//項目總計

            SingleNumber.setProperty("in_code_1", cfg.B1);//條碼1
            SingleNumber.setProperty("in_code_2", "00" + cfg.B2);//條碼2
            SingleNumber.setProperty("in_code_3", cfg.B3);//條碼3
            SingleNumber.setProperty("pay_bool", "未繳費");//繳費狀態
            SingleNumber.setProperty("in_add_time", cfg.AddTime);//建單時間
            SingleNumber.setProperty("index_number", cfg.barcode2_1);//業主自訂編號

            SingleNumber.setProperty(cfg.MtProperty, cfg.meeting_id);

            Item documents = SingleNumber.apply();

            //取出該賽事的關聯繳費
            aml = "<AML>" +
                "<Item type='In_Meeting_pay' action='get'>" +
                "<" + cfg.MtProperty + ">" + cfg.meeting_id + "</" + cfg.MtProperty + ">" +
                "<id>" + documents.getProperty("id", "") + "</id>" +
                "</Item></AML>";

            Item SingleNumbers = inn.applyAML(aml);

            for (int i = 0; i < itmMUsers.getItemCount(); i++)
            {
                Item itmMUser = itmMUsers.getItemByIndex(i);
                string in_regdate = itmMUser.getProperty("in_regdate", "");
                in_regdate = GetDateTimeValue(in_regdate, "yyyy-MM-ddTHH:mm:ss", true);

                //新增到繳費資訊下
                Item in_meeting_news = inn.newItem("In_Meeting_news", "add");
                in_meeting_news.setProperty("in_ans_l3", itmMUser.getProperty("in_gameunit", ""));//組別彙整結果
                in_meeting_news.setProperty("in_pay_amount", itmMUser.getProperty("in_expense", ""));//應繳金額
                in_meeting_news.setProperty("in_name", itmMUser.getProperty("in_name", ""));//姓名
                in_meeting_news.setProperty("in_sno", itmMUser.getProperty("in_sno", ""));//身分證
                in_meeting_news.setProperty("in_creator", cfg.pay_creator_name);//協助報名者
                in_meeting_news.setProperty("in_creator_sno", cfg.pay_creator_sno);//協助報名者帳號
                in_meeting_news.setProperty("in_regdate", in_regdate);//報名日期

                //if (cfg.IsGame)
                //{
                in_meeting_news.setProperty("in_l1", itmMUser.getProperty("in_l1", ""));//第一層
                in_meeting_news.setProperty("in_l2", itmMUser.getProperty("in_l2", ""));//第二層
                in_meeting_news.setProperty("in_l3", itmMUser.getProperty("in_l3", ""));//第三層
                in_meeting_news.setProperty("in_index", itmMUser.getProperty("in_index", ""));//序號
                //in_meeting_news.setProperty("in_team_index", itmMUser.getProperty("in_team_index", ""));//隊伍序號
                in_meeting_news.setProperty("in_section_name", itmMUser.getProperty("in_section_name", ""));//組名
                in_meeting_news.setProperty("in_l1_sort", itmMUser.getProperty("in_l1_sort", ""));//第一層排序

                in_meeting_news.setProperty("in_muid", itmMUser.getProperty("id", ""));//與會者id

                SingleNumbers.addRelationship(in_meeting_news);
            }

            SingleNumbers.apply();

            //將[繳費單號]壓回去[與會者]
            for (int i = 0; i < itmMUsers.getItemCount(); i++)
            {
                Item itmMUser = itmMUsers.getItemByIndex(i);
                string muid = itmMUser.getProperty("id", "");

                sql = " UPDATE [" + cfg.MUserType + "] SET "
                    + "   in_paynumber = N'" + SingleNumbers.getProperty("item_number", "") + "'"
                    + " WHERE id = '" + muid + "'";

                //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

                Item UserNumber = inn.applySQL(sql);
            }
            
            itmReturn.setProperty("numbers", SingleNumbers.getProperty("item_number", ""));
        }

        private string GetProjectCode(string payment_mode, int money)
        {
            string project_code = "";

            switch (payment_mode)
            {
                case "A":
                    if (money <= 20000)
                    {
                        project_code = "6NR";
                    }
                    else if (money > 20000 && money <= 40000)
                    {
                        project_code = "6NS";
                    }
                    else if (money > 40000 && money <= 60000)
                    {
                        project_code = "6BD";
                    }
                    break;
                case "B":
                    if (money <= 20000)
                    {
                        project_code = "68Q";
                    }
                    else if (money > 20000 && money <= 40000)
                    {
                        project_code = "68R";
                    }
                    else if (money > 40000 && money <= 60000)
                    {
                        project_code = "6BF";
                    }
                    break;
            }

            return project_code;
        }

        //產生條碼**********************************
        int ichecksum1 = 0, ichecksum2 = 0;

        //判斷是否為英文或數字
        public bool IsNumberOREng(string str)
        {
            System.Text.RegularExpressions.Regex reg1 = new System.Text.RegularExpressions.Regex(@"^[A-Za-z0-9$+-.% ]+$");
            return reg1.IsMatch(str);

        }
        //判斷是否為數字
        public bool IsPositive_Number(string str)
        {
            System.Text.RegularExpressions.Regex reg1 = new System.Text.RegularExpressions.Regex(@"^[0-9]*[1-9][0-9]*$");
            return reg1.IsMatch(str);

        }
        //產生第一段條碼(代收期限,代收項目)
        public string BarCode1_OutPut(DateTime date, String num)
        {
            //確認代收項目
            bool bIsMatch;
            bIsMatch = IsNumberOREng(num.ToString());
            if (bIsMatch == false)
            {
                throw new System.ArgumentException("代收項目編號含有未定義之特殊字元。");
            }
            else
            {
                if (num.ToString().Length != 3)
                {
                    throw new System.ArgumentException("代收項目編號長度應為3。");
                }
            }
            if (date < DateTime.Today)
            {
                // throw new System.ArgumentException("代收期限不應小於今日。");
            }

            string strDate = string.Format("{0}{1}{2}", (date.Year - 1911).ToString().Substring(1, 2), date.Month.ToString("00"), date.Day.ToString("00"));
            return strDate + num;
        }
        //產生第二段條碼(業者自訂)
        public string BarCode2_OutPut(String num, String custom)
        {
            //確認業主自訂編號
            bool bIsMatch = false;
            bIsMatch = IsNumberOREng(custom.ToString());
            if (bIsMatch == false)
            {
                throw new System.ArgumentException("業主自訂編號含有未定義之特殊字元。");
            }
            else
            {
                if (custom.ToString().Length > 10)
                {
                    throw new System.ArgumentException("業主自訂編號超出字數限制。");
                }
            }

            string strCustom = custom.PadRight(10, '0');
            return "00" + num + strCustom;
        }
        //產生第三段條碼(應繳日,金額,第一碼,第二碼)
        public string BarCode3_OutPut(DateTime date, string strpay, String barcode1, String barcode2)
        {
            //確認金額
            bool bIsMatch = false;
            bIsMatch = IsPositive_Number(strpay);
            if (bIsMatch == false)
            {
                throw new System.ArgumentException("金額應為正整數。");
            }
            else
            {
                if (strpay.Length > 9)
                {
                    throw new System.ArgumentException("金額超過位數限制。");
                }
            }
            int pay = int.Parse(strpay);

            char[] cBarcode1, cBarcode2;
            char[] cDate, CMoney;
            string strCheckSum1, strCheckSum2;

            cBarcode1 = barcode1.ToUpper().ToCharArray(0, barcode1.Length);
            CharLoop(cBarcode1);
            cBarcode2 = barcode2.ToUpper().ToCharArray(0, barcode2.Length);
            CharLoop(cBarcode2);
            cDate = date.ToString("MMdd").ToCharArray(0, date.ToString("MMdd").Length);
            CharLoop(cDate);
            CMoney = pay.ToString().PadLeft(9, '0').ToCharArray(0, 9);
            CharLoop(CMoney);

            switch (ichecksum1)
            {
                case 0:
                    strCheckSum1 = "A";
                    break;
                case 10:
                    strCheckSum1 = "B";
                    break;
                default:
                    strCheckSum1 = ichecksum1.ToString();
                    break;
            }

            switch (ichecksum2)
            {
                case 0:
                    strCheckSum2 = "X";
                    break;
                case 10:
                    strCheckSum2 = "Y";
                    break;
                default:
                    strCheckSum2 = ichecksum2.ToString();
                    break;
            }
            ichecksum1 = 0;
            ichecksum2 = 0;
            return date.ToString("MMdd") + strCheckSum1 + strCheckSum2 + pay.ToString().PadLeft(9, '0');
        }

        private void CharLoop(char[] value)
        {
            for (int iCount = 0; iCount < value.Length; iCount++)
            {
                //偶數字串
                if (iCount % 2 > 0)
                {
                    ichecksum2 = GetCheckSum(value[iCount], ichecksum2);
                }
                //奇數字串
                else
                {
                    ichecksum1 = GetCheckSum(value[iCount], ichecksum1);
                }
            }
        }

        private int GetCheckSum(char value, int checksum)
        {
            if (Char.IsNumber(value))
            {
                //是數字
                return (checksum + ((int)value - 48)) % 11;
            }
            else
            {
                //是英文
                switch (value)
                {
                    case 'A':
                        return (checksum + 1) % 11;
                    case 'B':
                        return (checksum + 2) % 11;
                    case 'C':
                        return (checksum + 3) % 11;
                    case 'D':
                        return (checksum + 4) % 11;
                    case 'E':
                        return (checksum + 5) % 11;
                    case 'F':
                        return (checksum + 6) % 11;
                    case 'G':
                        return (checksum + 7) % 11;
                    case 'H':
                        return (checksum + 8) % 11;
                    case 'I':
                        return (checksum + 9) % 11;
                    case 'J':
                        return (checksum + 1) % 11;
                    case 'K':
                        return (checksum + 2) % 11;
                    case 'L':
                        return (checksum + 3) % 11;
                    case 'M':
                        return (checksum + 4) % 11;
                    case 'N':
                        return (checksum + 5) % 11;
                    case 'O':
                        return (checksum + 6) % 11;
                    case 'P':
                        return (checksum + 7) % 11;
                    case 'Q':
                        return (checksum + 8) % 11;
                    case 'R':
                        return (checksum + 9) % 11;
                    case 'S':
                        return (checksum + 2) % 11;
                    case 'T':
                        return (checksum + 3) % 11;
                    case 'U':
                        return (checksum + 4) % 11;
                    case 'V':
                        return (checksum + 5) % 11;
                    case 'W':
                        return (checksum + 6) % 11;
                    case 'X':
                        return (checksum + 7) % 11;
                    case 'Y':
                        return (checksum + 8) % 11;
                    case 'Z':
                        return (checksum + 9) % 11;
                    case '+':
                        return (checksum + 1) % 11;
                    case '%':
                        return (checksum + 2) % 11;
                    case '-':
                        return (checksum + 6) % 11;
                    case '.':
                        return (checksum + 7) % 11;
                    case ' ':
                        return (checksum + 8) % 11;
                    case '$':
                        return (checksum + 9) % 11;
                    case '/':
                        return (checksum + 0) % 11;
                    default:
                        return (checksum + 0) % 11;
                }
                return (checksum + ((((int)value - 65) % 9) + 1) % 11);
            }
        }

        /// <summary>
        /// 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }

            public bool isMeetingAdmin { get; set; }
            public bool isCommittee { get; set; }
            public bool isGymOwner { get; set; }
            public bool isGymAssistant { get; set; }


            public string meeting_id { get; set; }


            public Item itmLogin { get; set; }
            public string login_resume_group { get; set; }
            public string login_resume_name { get; set; }
            public string login_resume_sno { get; set; }
            public string login_manager_name { get; set; }


            /// <summary>
            /// 活動資訊 (IN_MEETING OR IN_CLA_MEETING)
            /// </summary>
            public Item itmMeeting { get; set; }

            /// <summary>
            /// 時程資訊 (IN_MEETING_Functiontime OR IN_CLA_MEETING_Functiontime)
            /// </summary>
            public Item itmFunctiontime { get; set; }

            /// <summary>
            /// 活動 ItemType
            /// </summary>
            public string MtType { get; set; }

            /// <summary>
            /// 與會者 ItemType
            /// </summary>
            public string MUserType { get; set; }

            /// <summary>
            /// 選項時程 ItemType
            /// </summary>
            public string MFuncType { get; set; }

            /// <summary>
            /// 繳費單 meeting id 欄位
            /// </summary>
            public string MtProperty { get; set; }



            /// <summary>
            /// 繳費單 Identity id
            /// </summary>
            public string pay_identity_id { get; set; }

            /// <summary>
            /// 繳費單 建單者姓名
            /// </summary>
            public string pay_creator_name { get; set; }

            /// <summary>
            /// 繳費單 建單者身分證號
            /// </summary>
            public string pay_creator_sno { get; set; }

            /// <summary>
            /// 繳費單 建單者所屬群組
            /// </summary>
            public string pay_creator_group { get; set; }

            /// <summary>
            /// 繳費單 建單者所屬委員會
            /// </summary>
            public string pay_committee { get; set; }


            /// <summary>
            /// 活動類型
            /// </summary>
            public string in_meeting_type { get; set; }

            /// <summary>
            /// 活動編號
            /// </summary>
            public string in_meeting_code { get; set; }

            /// <summary>
            /// 收款單位編號
            /// </summary>
            public string in_receice_org_code { get; set; }

            /// <summary>
            /// 最後報名日期
            /// </summary>
            public string in_imf_date_e { get; set; }

            /// <summary>
            /// 最後繳費期限
            /// </summary>
            public string last_payment_date { get; set; }

            /// <summary>
            /// 最後繳費期限
            /// </summary>
            public string due_date { get; set; }

            /// <summary>
            /// 隊職員數
            /// </summary>
            public int in_staffs { get; set; }

            /// <summary>
            /// 選手人數
            /// </summary>
            public int in_players { get; set; }

            /// <summary>
            /// 項目總計
            /// </summary>
            public int in_items { get; set; }

            /// <summary>
            /// 費用
            /// </summary>
            public int in_expense { get; set; }

            /// <summary>
            /// 空白
            /// </summary>
            public string blank { get; set; }

            /// <summary>
            /// 備用碼(1)+運動代碼(0)
            /// </summary>
            public string codes { get; set; }

            /// <summary>
            /// 狀態(A>繳款人 B>委託單位)
            /// </summary>
            public string payment_mode { get; set; }

            /// <summary>
            /// 業主自訂編號
            /// </summary>
            public string index_number { get; set; }

            /// <summary>
            /// 條碼2
            /// </summary>
            public string barcode2 { get; set; }

            /// <summary>
            /// 臨櫃繳費虛擬帳號
            /// </summary>
            public string barcode2_1 { get; set; }

            /// <summary>
            /// 項目代碼
            /// </summary>
            public string project_code { get; set; }

            /// <summary>
            /// 條碼1
            /// </summary>
            public string B1 { get; set; }

            /// <summary>
            /// 條碼2
            /// </summary>
            public string B2 { get; set; }

            /// <summary>
            /// 條碼3
            /// </summary>
            public string B3 { get; set; }

            /// <summary>
            /// 當前時間
            /// </summary>
            public DateTime CurrentTime { get; set; }

            /// <summary>
            /// 建單時間
            /// </summary>
            public string AddTime { get; set; }
        }

        private string GetDateTimeValue(string value, string format = "yyyy-MM-dd", bool bAdd8Hour = false)
        {
            if (value == "") return "";

            string day = value.Replace("/", "-");

            DateTime dt = DateTime.Now;
            if (DateTime.TryParse(day, out dt))
            {
                if (bAdd8Hour)
                {
                    return dt.AddHours(8).ToString(format);
                }
                else
                {
                    return dt.ToString(format);
                }
            }
            else
            {
                return value;
            }
        }
    }
}