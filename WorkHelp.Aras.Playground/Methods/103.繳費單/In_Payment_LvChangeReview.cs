﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;
using System.Net;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class In_Payment_LvChangeReview : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 審核[申請換組]的繳費單
                日期: 
                    - 2021-11-25 調整 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]In_Payment_LvChangeReview";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "input dom: " + itmR.dom.InnerXml);

            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            string sql = "";
            string strIdentityId = inn.getUserAliases();


            string news_id = itmR.getProperty("news_id", "");
            string in_verify_memo = itmR.getProperty("in_verify_memo", "");//審核說明
            string refund_type = itmR.getProperty("refund_type", "");//通過or不通過
            string now_time = DateTime.Now.AddHours(-8).ToString("yyyy-MM-ddTHH:mm:ss");//當前時間(SQL用-8H)

            //取得資訊
            sql = "SELECT * FROM IN_MEETING_NEWS WITH(NOLOCK) WHERE id = '" + news_id + "'";
            Item itmMeetingNew = inn.applySQL(sql);

            string pay_id = itmMeetingNew.getProperty("source_id", "");
            string in_l1 = itmMeetingNew.getProperty("in_l1", "");
            string in_l2 = itmMeetingNew.getProperty("in_l2", "");
            string in_l3 = itmMeetingNew.getProperty("in_l3", "");
            string in_index = itmMeetingNew.getProperty("in_index", "");

            string in_old_sections = itmMeetingNew.getProperty("in_section_name", "")
                + "-"
                + in_index;

            sql = "SELECT * FROM IN_MEETING_NEWS WITH(NOLOCK) WHERE source_id = '" + pay_id + "'"
                + " AND ISNULL(in_l1, '') = N'" + in_l1 + "'"
                + " AND ISNULL(in_l2, '') = N'" + in_l2 + "'"
                + " AND ISNULL(in_l3, '') = N'" + in_l3 + "'"
                + " AND ISNULL(in_index, '') = N'" + in_index + "'";

            Item items = inn.applySQL(sql);

            if (items.isError() || items.getItemCount() <= 0)
            {
                throw new Exception("繳費單明細不存在");
            }

            int count = items.getItemCount();
            bool need_update_muser = true;
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string current_id = item.getProperty("id", "");

                if (refund_type == "1")
                {
                    //通過
                    //更新[繳費資訊]的欄位
                    sql = "UPDATE IN_MEETING_NEWS SET"
                        + " in_verify_time = '" + now_time + "'"
                        + ", in_verify_by_id = '" + strIdentityId + "'"
                        + ", in_cancel_status = N'換組申請通過'"
                        + ", in_old_sections = N'" + in_old_sections + "'"
                        + " WHERE id = '" + current_id + "'";

                    Item itmUpdate = inn.applySQL(sql);

                    if (itmUpdate.isError())
                    {
                        need_update_muser = false;
                        throw new Exception("審核失敗");
                    }
                }
                else
                {
                    //不通過
                    //更新[繳費資訊]的欄位
                    sql = "UPDATE In_Meeting_news SET"
                        + " in_verify_time = '" + now_time + "'"
                        + ", in_verify_by_id = '" + strIdentityId + "'"
                        + ", in_verify_memo = N'" + in_verify_memo + "'"
                        + ", in_cancel_status = N'換組申請不通過'"
                        + " WHERE id = '" + current_id + "'";

                    Item itmUpdate = inn.applySQL(sql);
                    
                    need_update_muser = false;
                }
            }

            if (need_update_muser)
            {
                //更新與會者
                UpdateMeetingUser(CCO, strMethodName, inn, itmMeetingNew, itmR);
            }

            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

            return itmR;
        }

        //更新與會者
        private void UpdateMeetingUser(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmMeetingNew, Item itmReturn)
        {
            string in_new_levels = itmMeetingNew.getProperty("in_new_levels", "");
            string[] arr = in_new_levels.Split(new char[] { '@' }, StringSplitOptions.RemoveEmptyEntries);
            if (arr == null || arr.Length < 3)
            {
                throw new Exception("新組別錯誤");
            }

            TTeam entity = new TTeam 
            { 
                pay_id = itmMeetingNew.getProperty("source_id", ""),
                old_l1 = itmMeetingNew.getProperty("in_l1", ""),
                old_l2 = itmMeetingNew.getProperty("in_l2", ""),
                old_l3 = itmMeetingNew.getProperty("in_l3", ""),
                old_index = itmMeetingNew.getProperty("in_index", ""),

                new_l1 = arr[0],
                new_l2 = arr[1],
                new_l3 = arr[2],
            };
           
            string sql = "SELECT * FROM IN_MEETING_PAY WITH(NOLOCK) WHERE id = '" + entity.pay_id + "'";
            Item itmPay = inn.applySQL(sql);
            if (itmPay.isError() || itmPay.getResult() == "")
            {
                throw new Exception("繳費單不存在");
            }

            entity.meeting_id = itmPay.getProperty("in_meeting", "");
            entity.in_creator_sno = itmPay.getProperty("in_creator_sno", "");

            entity.new_section_name = string.Join("-", arr);
            entity.new_index = GetNewIndex(CCO, strMethodName, inn, entity);

            sql = "SELECT id, in_section_name, in_name, in_sno FROM IN_MEETING_USER WITH(NOLOCK)"
                + " WHERE source_id = '" + entity.meeting_id + "'"
                + " AND ISNULL(in_l1, '') = N'" + entity.old_l1 + "'"
                + " AND ISNULL(in_l2, '') = N'" + entity.old_l2 + "'"
                + " AND ISNULL(in_l3, '') = N'" + entity.old_l3 + "'"
                + " AND ISNULL(in_index, '') = N'" + entity.old_index + "'";

            CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item items = inn.applySQL(sql);

            if (items.isError() || items.getItemCount() <= 0)
            {
                throw new Exception("與會者不存在");
            }

            int count = items.getItemCount();

            for(int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string muid = item.getProperty("id", "");
                string in_section_name = item.getProperty("in_section_name", "");
                string in_name = item.getProperty("in_name", "");
                string in_sno = item.getProperty("in_sno", "");

                string sql_update = "UPDATE IN_MEETING_USER SET"
                    + " in_section_name = N'" + entity.new_section_name +"'"
                    + ", in_l1 = N'" + entity.new_l1 + "'"
                    + ", in_l2 = N'" + entity.new_l2 + "'"
                    + ", in_l3 = N'" + entity.new_l3 + "'"
                    + ", in_index = N'" + entity.new_index + "'"
                    + "WHERE id = '" + muid + "'";

                string exe_result = "執行換組成功";

                Item itmUpdate = inn.applySQL(sql_update);

                if (itmUpdate.isError() || itmUpdate.getResult() == "")
                {
                    exe_result = "執行換組失敗";
                }

                CCO.Utilities.WriteDebug(strMethodName + "_" + DateTime.Now.ToString("yyyyMMdd")
                    , exe_result + " | " + muid + ": " + in_name + "(" + in_sno + ") |"
                    + " OLD: " + entity.old_section_name + "(" + entity.old_index + ")"
                    + " NEW: " + entity.new_section_name + "(" + entity.new_index + ")"
                    + " sql: " + sql_update
                    );
            }
        }

        private string GetNewIndex(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, TTeam entity)
        {
            string result = "";

            string sql = "SELECT MAX(in_index) AS 'max_index' FROM IN_MEETING_USER WITH(NOLOCK)"
                + " WHERE source_id = '" + entity.meeting_id + "'"
                + " AND in_l1 = N'" + entity.new_l1 + "'"
                + "";

            Item itmData = inn.applySQL(sql);

            if (itmData.isError() || itmData.getResult() == "")
            {
                result = "00001";
            }
            else
            {
                string old_max_index = itmData.getProperty("max_index", "");
                if (old_max_index == "")
                {
                    result = "00001";
                }
                else
                {
                    int new_index = GetIntVal(old_max_index.TrimStart('0')) + 1;
                    result = new_index.ToString().PadLeft(5, '0');
                }
            }

            return result;
        }

        private class TTeam
        { 
            public string meeting_id { get; set; }
            public string pay_id { get; set; }
            public string in_creator_sno { get; set; }
            
            public string old_l1 { get; set; }
            public string old_l2 { get; set; }
            public string old_l3 { get; set; }
            public string old_index { get; set; }
            public string old_section_name { get; set; }

            public string new_l1 { get; set; }
            public string new_l2 { get; set; }
            public string new_l3 { get; set; }
            public string new_index { get; set; }
            public string new_section_name { get; set; }
        }


        private int GetIntVal(string value)
        {
            if (value == "" || value == "0") return 0;

            int result = 0;
            int.TryParse(value, out result);
            return result;
        }


    }
}