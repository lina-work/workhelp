﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class In_Payment_List_View : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
            目的: 繳費單檢視(不含功能)
            日期: 2020-08-18 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]In_Payment_List_View";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "this: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                login_user_id = inn.getUserID(),
                meeting_id = itmR.getProperty("meeting_id", ""),
                meeting_type = itmR.getProperty("meeting_type", ""),
                pay_status = itmR.getProperty("pay_status", ""),
                exe_type = itmR.getProperty("exe_type", ""),
                mode = itmR.getProperty("mode", ""),
                resume_id = itmR.getProperty("id", ""),

                mtName = "IN_MEETING",
                muName = "IN_MEETING_USER",
                msName = "IN_MEETING_SURVEYS",
                mtProperty = "in_meeting",
                svName = "IN_SURVEY",
                soName = "IN_SURVEY_OPTION",
            };

            if (cfg.mode == "cla")
            {
                cfg.mtName = "IN_CLA_MEETING";
                cfg.muName = "IN_CLA_MEETING_USER";
                cfg.msName = "IN_CLA_MEETING_SURVEYS";
                cfg.mtProperty = "in_cla_meeting";
                cfg.svName = "IN_CLA_SURVEY";
                cfg.soName = "IN_CLA_SURVEY_OPTION";
            }

            if (cfg.meeting_id == "")
            {
                throw new Exception("活動 id 不得為空白");
            }
            if (cfg.meeting_type == "")
            {
                throw new Exception("活動類型 不得為空白");
            }
            if (cfg.pay_status == "")
            {
                throw new Exception("繳費狀態 不得為空白");
            }

            //取得登入者權限
            cfg.itmPermit = inn.applyMethod("In_CheckIdentity", "<method>" + strMethodName + "</method><code>ALL</code>");
            cfg.isMeetingAdmin = cfg.itmPermit.getProperty("isMeetingAdmin", "") == "1";
            //取得登入者資訊
            cfg.itmLoginResume = inn.applySQL("SELECT * FROM IN_RESUME WITH(NOLOCK) WHERE in_user_id = N'" + cfg.login_user_id + "'");
            if (IsError(cfg.itmLoginResume))
            {
                throw new Exception("登入者履歷異常");
            }

            //要檢視的對象 Resume
            cfg.itmResumeView = GetTargetResume(cfg);

            //取得活動資訊
            cfg.itmMeeting = GetMeeting(cfg);

            itmR.setProperty("id", cfg.itmMeeting.getProperty("id", ""));
            itmR.setProperty("in_title", cfg.itmMeeting.getProperty("in_title", ""));
            itmR.setProperty("isMeetingAdmin", cfg.itmPermit.getProperty("isMeetingAdmin", ""));

            string show_paymode = "item_show_1";
            string show_usermode = "item_show_0";
            string show_xlsmode = "item_show_0";

            if (cfg.exe_type == "paylist")
            {
                show_paymode = "item_show_0";
                show_usermode = "item_show_0";
                show_xlsmode = "item_show_1";
            }

            switch (cfg.pay_status)
            {
                case "unconfirmed":
                    itmR.setProperty("inn_sub_title", "尚未產生繳費單");
                    itmR.setProperty("show_paynumber", "0");
                    itmR.setProperty("show_paymode", "item_show_0");
                    itmR.setProperty("show_usermode", "item_show_0");
                    itmR.setProperty("show_xlsmode", "item_show_0");
                    itmR.setProperty("show_refund", "0");
                    break;

                case "unpaid":
                    itmR.setProperty("inn_sub_title", "未付款繳費單");
                    itmR.setProperty("show_paynumber", "1");
                    itmR.setProperty("show_paymode", show_paymode);
                    itmR.setProperty("show_usermode", show_usermode);
                    itmR.setProperty("show_xlsmode", show_xlsmode);
                    itmR.setProperty("show_refund", "0");
                    break;

                case "paid":
                    itmR.setProperty("inn_sub_title", "已付款繳費單");
                    itmR.setProperty("show_paynumber", "1");
                    itmR.setProperty("show_paymode", show_paymode);
                    itmR.setProperty("show_usermode", show_usermode);
                    itmR.setProperty("show_xlsmode", show_xlsmode);
                    itmR.setProperty("show_refund", "1");
                    break;

                default:
                    break;
            }

            if (cfg.exe_type == "")
            {
                //分組模式
                UserMode(cfg, itmR);
            }
            else if (cfg.exe_type == "paylist")
            {
                //繳費單模式
                PayMode(cfg, itmR);
            }
            else if (cfg.exe_type == "xls")
            {
                //匯出 XLS
                XlsMode(cfg, itmR);
            }

            return itmR;
        }

        #region XLS

        //匯出 Excel
        private void XlsMode(TConfig cfg, Item itmReturn)
        {
            string pay_bool = "未繳費";
            if (cfg.pay_status == "paid")
            {
                pay_bool = "已繳費";
            }

            string in_sno = cfg.itmResumeView.getProperty("in_sno", "");
            //string in_group = cfg.itmResumeView.getProperty("in_group", "");

            Item items = null;
            if (cfg.isMeetingAdmin)
            {
                items = GetAdminMeetingPayList(cfg, pay_bool);
            }
            else
            {
                items = GetOrgMeetingPayList(cfg, in_sno, pay_bool);
            }

            if (IsError(items, isSingle: false))
            {
                itmReturn.setProperty("inn_tables", "查無資料");
            }
            else
            {
                ClosedXML.Excel.XLWorkbook workbook = new ClosedXML.Excel.XLWorkbook();

                AppendXlsTable(cfg, workbook, items, itmReturn);

                string in_title = cfg.itmMeeting.getProperty("in_title", "");

                Item itmPath = GetExcelPath(cfg, "export_path");

                string export_path = itmPath.getProperty("export_path", "").TrimEnd('\\');
                string ext_name = ".xlsx";
                string xls_name = in_title + "_" + DateTime.Now.ToString("yyyyMMdd_HHmmss");
                string xls_file = export_path + "\\" + xls_name + ext_name;
                string xls_url = xls_name + ext_name;

                workbook.SaveAs(xls_file);

                itmReturn.setProperty("xls_name", xls_url);
            }
        }
        private void AppendXlsTable(TConfig cfg, ClosedXML.Excel.XLWorkbook workbook, Item items, Item itmReturn)
        {
            ClosedXML.Excel.IXLWorksheet sheet = workbook.Worksheets.Add("對帳單");

            //凍結窗格
            sheet.SheetView.Freeze(1, 1);

            bool show_refund = itmReturn.getProperty("show_refund", "0") == "1";

            List<TXlsCol> fields = new List<TXlsCol>();
            fields.Add(new TXlsCol { Title = "項次", Property = "inn_no", Format = "center" });
            fields.Add(new TXlsCol { Title = "繳費單號", Property = "item_number", Format = "" });
            fields.Add(new TXlsCol { Title = "條碼2", Property = "in_code_2", Format = "text" });
            fields.Add(new TXlsCol { Title = "所屬單位", Property = "in_current_org", Format = "" });
            fields.Add(new TXlsCol { Title = "建立日期", Property = "created_on", Format = "yyyy/MM/dd" });
            fields.Add(new TXlsCol { Title = "應收金額", Property = "in_pay_amount_exp", Format = "$ #,##0" });
            fields.Add(new TXlsCol { Title = "應繳日期", Property = "in_pay_date_exp", Format = "yyyy/MM/dd" });
            fields.Add(new TXlsCol { Title = "實收金額", Property = "in_pay_amount_real", Format = "$ #,##0" });
            fields.Add(new TXlsCol { Title = "實繳日期", Property = "in_pay_date_real", Format = "yyyy/MM/dd" });
            if (show_refund)
            {
                fields.Add(new TXlsCol { Title = "退款金額", Property = "in_refund_amount", Format = "$ #,##0" });
                fields.Add(new TXlsCol { Title = "最後金額", Property = "inn_total", Format = "$ #,##0" });
            }
            fields.Add(new TXlsCol { Title = "發票抬頭", Property = "invoice_up", Format = "newline" });
            fields.Add(new TXlsCol { Title = "統一編號", Property = "uniform_numbers", Format = "newline" });

            int wsRow = 1;
            int wsCol = 1;
            int field_count = fields.Count;

            for (int i = 0; i < field_count; i++)
            {
                TXlsCol field = fields[i];
                SetHeadCell(sheet.Cell(wsRow, wsCol + i), field.Title);
            }
            wsRow++;

            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_pay_amount_exp = item.getProperty("in_pay_amount_exp", "");
                string in_pay_amount_real = item.getProperty("in_pay_amount_real", "");

                string inn_no = (i + 1).ToString();
                string inn_pay_amount_exp = GetDollarValue(in_pay_amount_exp);
                string inn_pay_amount_real = GetDollarValue(in_pay_amount_real);
                string inn_pay_date_exp = GetDateTimeValue(item.getProperty("in_pay_date_exp", ""), "yyyy-MM-dd", true);
                string inn_pay_date_real = GetDateTimeValue(item.getProperty("in_pay_date_real", ""), "yyyy-MM-dd", false);
                string inn_created_on = GetDateTimeValue(item.getProperty("created_on", ""), "yyyy-MM-dd", true);

                item.setProperty("in_code_2", GetCodeMask(item.getProperty("in_code_2", "")));

                item.setProperty("inn_no", inn_no);
                item.setProperty("inn_created_on", inn_created_on);
                item.setProperty("inn_pay_amount_exp", inn_pay_amount_exp);
                item.setProperty("inn_pay_date_exp", inn_pay_date_exp);
                item.setProperty("inn_pay_amount_real", inn_pay_amount_real);
                item.setProperty("inn_pay_date_real", inn_pay_date_real);

                string in_refund_amount = item.getProperty("in_refund_amount", "");
                string inn_total = GetNewTotal(in_pay_amount_exp, in_refund_amount).ToString();
                item.setProperty("inn_total", inn_total);

                SetItemCell(sheet, wsRow, wsCol, item, fields);
                wsRow++;
            }
            for (int i = 0; i < field_count; i++)
            {
                sheet.Column(wsCol + i).AdjustToContents();
            }
        }

        //設定物件與資料列
        private void SetItemCell(ClosedXML.Excel.IXLWorksheet sheet, int wsRow, int wsCol, Item item, List<TXlsCol> fields)
        {
            int field_count = fields.Count;
            for (int i = 0; i < field_count; i++)
            {
                TXlsCol field = fields[i];
                string field_name = field.Property;
                string field_format = field.Format;

                ClosedXML.Excel.IXLCell cell = sheet.Cell(wsRow, wsCol + i);

                if (field_name == "")
                {
                    SetBodyCell(cell, "", field_format);
                }
                else
                {
                    string value = item.getProperty(field_name, "");
                    SetBodyCell(cell, value, field_format);
                }
            }
        }

        //設定資料列
        private void SetBodyCell(ClosedXML.Excel.IXLCell cell, string value, string format)
        {
            switch (format)
            {
                case "center":
                    cell.Value = value;
                    cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    break;

                case "yyyy/MM/dd":
                case "yyyy/MM/dd HH:mm":
                    cell.Value = GetDateTimeValue(value, format);
                    cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    break;

                case "$ #,##0":
                    cell.Value = value;
                    cell.Style.NumberFormat.Format = "$ #,##0";
                    break;

                case "text":
                    cell.Value = "'" + value;
                    cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    break;

                case "newline":
                    cell.Value = "'" + StrToLines(value, Environment.NewLine);
                    cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    break;

                default:
                    cell.Value = value;
                    break;
            }
            cell.Style.Alignment.Vertical = ClosedXML.Excel.XLAlignmentVerticalValues.Center;
            cell.Style.Border.SetRightBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetTopBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetBottomBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetLeftBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
        }

        //轉換日期
        private string GetDateTimeValue(string value, string format)
        {
            if (value == "") return "";
            DateTime dt = Convert.ToDateTime(value);
            return dt.ToString(format);
        }

        ClosedXML.Excel.XLColor head_bg_color = ClosedXML.Excel.XLColor.FromHtml("#295C90");

        //設定標題列
        private void SetHeadCell(ClosedXML.Excel.IXLCell cell, string title)
        {
            cell.Value = title;

            cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
            cell.Style.Font.Bold = true;
            cell.DataType = ClosedXML.Excel.XLDataType.Text;
            cell.Style.Fill.BackgroundColor = head_bg_color;
            cell.Style.Font.FontColor = ClosedXML.Excel.XLColor.White;

            cell.Style.Border.SetRightBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetTopBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetBottomBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetLeftBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
        }

        //資料 Cell 上色
        private void SetBodyCellColor(ClosedXML.Excel.IXLWorksheet sheet, int wsRow, int wsCol_s, int wsCol_e, ClosedXML.Excel.XLColor color)
        {
            for (int i = wsCol_s; i <= wsCol_e; i++)
            {
                ClosedXML.Excel.IXLCell cell = sheet.Cell(wsRow, i);
                cell.Style.Fill.BackgroundColor = color;
            }
        }

        private class TXlsCol
        {
            public string Title { get; set; }
            public string Property { get; set; }
            public string Format { get; set; }
        }

        private Item GetExcelPath(TConfig cfg, string in_name)
        {
            Item itmResult = cfg.inn.newItem();

            //Excel 匯出路徑 (from 原創.設定參數)
            string sql = @"
                SELECT 
                    TOP 1 t2.in_name AS 'variable', t1.in_name, t1.in_value 
                FROM 
                    In_Variable_Detail t1 WITH(NOLOCK)
                INNER JOIN 
                    In_Variable t2 WITH(NOLOCK) ON t2.ID = t1.SOURCE_ID 
                WHERE 
                    t2.in_name = N'meeting_excel'  
                    AND t1.in_name = N'{#in_name}'
            ";

            sql = sql.Replace("{#in_name}", in_name);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item item = cfg.inn.applySQL(sql);

            if (item.isError())
            {
                throw new Exception("未設定匯出路徑 _# " + in_name);
            }

            itmResult.setProperty("export_path", item.getProperty("in_value", ""));

            return itmResult;
        }
        #endregion

        #region 繳費單模式

        //繳費單模式
        private void PayMode(TConfig cfg, Item itmReturn)
        {
            string pay_bool = "未繳費";
            if (cfg.pay_status == "paid")
            {
                pay_bool = "已繳費";
            }

            string in_sno = cfg.itmResumeView.getProperty("in_sno", "");

            Item items = null;
            if (cfg.isMeetingAdmin)
            {
                items = GetAdminMeetingPayList(cfg, pay_bool);
            }
            else
            {
                items = GetOrgMeetingPayList(cfg, in_sno, pay_bool);
            }

            if (IsError(items, isSingle: false))
            {
                itmReturn.setProperty("inn_tables", "查無資料");
            }
            else
            {
                AppendPayTable(items, itmReturn);
            }
        }

        private void AppendPayTable(Item items, Item itmReturn)
        {
            string table_name = "pay_table";
            bool show_refund = itmReturn.getProperty("show_refund", "0") == "1";

            StringBuilder head = new StringBuilder();
            head.AppendLine("<thead>");
            head.AppendLine("    <th class='text-center' data-field='inn_no' data-sortable='false'>項次</th>");
            head.AppendLine("    <th class='text-center' data-field='item_number' data-sortable='true'>繳費單號</th>");
            head.AppendLine("    <th class='text-center' data-field='in_code_2' data-sortable='true'>條碼2</th>");
            head.AppendLine("    <th class='text-center' data-field='in_current_org' data-sortable='true'>所屬單位</th>");
            head.AppendLine("    <th class='text-center' data-field='inn_created_on' data-sortable='true'>建立日期</th>");
            head.AppendLine("    <th class='text-center' data-field='inn_pay_amount_exp' data-sortable='true'>應收金額</th>");
            head.AppendLine("    <th class='text-center' data-field='inn_pay_date_exp' data-sortable='true'>應繳日期</th>");
            head.AppendLine("    <th class='text-center' data-field='inn_pay_amount_real' data-sortable='false'>實收金額</th>");
            head.AppendLine("    <th class='text-center' data-field='inn_pay_date_real' data-sortable='true'>實繳日期</th>");
            if (show_refund)
            {
                head.AppendLine("    <th class='text-center' data-field='in_refund_amount' data-sortable='true'>退款金額</th>");
                head.AppendLine("    <th class='text-center' data-field='in_total' data-sortable='true'>最後金額</th>");
            }
            head.AppendLine("    <th class='text-center' data-field='invoice_up' data-sortable='false'>發票抬頭</th>");
            head.AppendLine("    <th class='text-center' data-field='uniform_numbers' data-sortable='false'>統一編號</th>");
            head.AppendLine("</thead>");

            int count = items.getItemCount();

            StringBuilder body = new StringBuilder();
            body.AppendLine("<tbody>");
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_pay_amount_exp = item.getProperty("in_pay_amount_exp", "");
                string in_pay_amount_real = item.getProperty("in_pay_amount_real", "");

                string inn_no = (i + 1).ToString();
                string item_number = GetPayNumberLink(item.getProperty("item_number", ""));
                string in_code_2 = GetCodeMask(item.getProperty("in_code_2", ""));
                string in_current_org = item.getProperty("in_current_org", "");

                string inn_pay_amount_exp = GetDollarValue(in_pay_amount_exp);
                string inn_pay_amount_real = GetDollarValue(in_pay_amount_real);

                string inn_pay_date_exp = GetDateTimeValue(item.getProperty("in_pay_date_exp", ""), "yyyy-MM-dd", true);
                string inn_pay_date_real = GetDateTimeValue(item.getProperty("in_pay_date_real", ""), "yyyy-MM-dd", false);
                string invoice_up = item.getProperty("invoice_up", "");
                string uniform_numbers = item.getProperty("uniform_numbers", "");
                string inn_created_on = GetDateTimeValue(item.getProperty("created_on", ""), "yyyy-MM-dd", true);

                string in_refund_amount = item.getProperty("in_refund_amount", "");

                string inn_refund = GetDollarValue(in_refund_amount);
                string inn_total = GetNewTotal(in_pay_amount_exp, in_refund_amount).ToString("####,####");

                body.AppendLine("<tr>");
                body.AppendLine("        <td class='text-center' data-field='inn_no' >" + inn_no + "</td>");
                body.AppendLine("        <td class='text-left' data-field='item_number' >" + item_number + "</td>");
                body.AppendLine("        <td class='text-left' data-field='in_code_2' >" + in_code_2 + "</td>");
                body.AppendLine("        <td class='text-left' data-field='in_current_org' >" + in_current_org + "</td>");
                body.AppendLine("        <td class='text-center' data-field='inn_created_on' >" + inn_created_on + "</td>");
                body.AppendLine("        <td class='text-right' data-field='inn_pay_amount_exp' >" + inn_pay_amount_exp + "</td>");
                body.AppendLine("        <td class='text-center' data-field='inn_pay_date_exp' >" + inn_pay_date_exp + "</td>");
                body.AppendLine("        <td class='warning text-right' data-field='inn_pay_amount_real' >" + inn_pay_amount_real + "</td>");
                body.AppendLine("        <td class='warning text-center' data-field='inn_pay_date_real' >" + inn_pay_date_real + "</td>");

                if (show_refund)
                {
                    body.AppendLine("    <td class='danger text-right' data-field='in_refund_amount'>" + inn_refund + "</td>");
                    body.AppendLine("    <td class='danger text-right' data-field='in_total'>" + inn_total + "</td>");
                }

                body.AppendLine("        <td class='text-left' data-field='invoice_up' >" + StrToLines(invoice_up, "<br>") + "</td>");
                body.AppendLine("        <td class='text-left' data-field='uniform_numbers' >" + StrToLines(uniform_numbers, "<br>") + "</td>");
                body.AppendLine("</tr>");
            }
            body.AppendLine("</tbody>");

            StringBuilder builder = new StringBuilder();
            builder.AppendLine(GetTableAttribute(table_name));
            builder.Append(head);
            builder.Append(body);
            builder.AppendLine("</table>");

            itmReturn.setProperty("inn_tables", builder.ToString());
            itmReturn.setProperty("inn_names", table_name);
        }

        private string StrToLines(string value, string newLine)
        {
            string[] arr = value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            if (arr == null && arr.Length == 0) return "";
            return string.Join(newLine, arr);
        }
        #endregion 繳費單模式
        //名冊模式
        private void UserMode(TConfig cfg, Item itmReturn)
        {
            bool isMeetingAdmin = cfg.itmPermit.getProperty("isMeetingAdmin", "") == "1";
            string in_sno = cfg.itmResumeView.getProperty("in_sno", "");
            //string in_group = cfg.itmResumeView.getProperty("in_group", "");

            Item items = null;

            switch (cfg.pay_status)
            {
                case "unconfirmed":
                    if (isMeetingAdmin)
                    {
                        items = GetAdminMeetingUsers(cfg);
                    }
                    else
                    {
                        items = GetOrgMeetingUsers(cfg, in_sno);
                    }
                    itmReturn.setProperty("inn_sub_title", "尚未產生繳費單");
                    itmReturn.setProperty("show_paynumber", "0");
                    break;

                case "unpaid":
                    if (isMeetingAdmin)
                    {
                        items = GetAdminMeetingPays(cfg, "未繳費");
                    }
                    else
                    {
                        items = GetOrgMeetingPays(cfg, in_sno, "未繳費");
                    }
                    itmReturn.setProperty("inn_sub_title", "未付款繳費單");
                    itmReturn.setProperty("show_paynumber", "1");
                    break;

                case "paid":
                    if (isMeetingAdmin)
                    {
                        items = GetAdminMeetingPays(cfg, "已繳費");
                    }
                    else
                    {
                        items = GetOrgMeetingPays(cfg, in_sno, "已繳費");
                    }
                    itmReturn.setProperty("inn_sub_title", "已付款繳費單");
                    itmReturn.setProperty("show_paynumber", "1");
                    break;
                default:
                    break;
            }

            switch (cfg.meeting_type)
            {
                case "seminar":
                    itmReturn.setProperty("name_head", "報名學員");
                    itmReturn.setProperty("section_head", "項目");
                    itmReturn.setProperty("team_head", "報名人數");
                    itmReturn.setProperty("show_team", "0");
                    itmReturn.setProperty("show_section", "0");
                    itmReturn.setProperty("show_birth", "0");
                    break;

                case "payment":
                    itmReturn.setProperty("name_head", "會員");
                    itmReturn.setProperty("section_head", "項目");
                    itmReturn.setProperty("team_head", "報名人數");
                    itmReturn.setProperty("show_team", "0");
                    itmReturn.setProperty("show_section", "1");
                    itmReturn.setProperty("show_birth", "0");
                    break;

                default:
                    itmReturn.setProperty("name_head", "選手");
                    itmReturn.setProperty("section_head", "組別");
                    itmReturn.setProperty("team_head", "隊員數");
                    itmReturn.setProperty("show_team", "1");
                    itmReturn.setProperty("show_section", "1");
                    itmReturn.setProperty("show_birth", "0");
                    break;
            }

            var l1_options = GetL1Options(cfg);
            var map = MapDctionary(items, l1_options);
            AppendTables(map, itmReturn);
        }

        #region 存取資料

        //取得檢視對象 Resume
        private Item GetTargetResume(TConfig cfg)
        {
            cfg.itmLoginResume.setType("In_Resume");
            cfg.itmLoginResume.setProperty("target_resume_id", cfg.resume_id);
            cfg.itmLoginResume.setProperty("login_resume_id", cfg.itmLoginResume.getID());
            cfg.itmLoginResume.setProperty("login_member_type", cfg.itmLoginResume.getProperty("in_member_type", ""));
            return cfg.itmLoginResume.apply("In_Get_Target_Resume");
        }

        //取得賽事資料
        private Item GetMeeting(TConfig cfg)
        {
            string aml = @"
                <AML>
                  <Item type='{#mtName}' action='get' id='{#meeting_id}' select='keyed_name,in_title,in_meeting_type'>
                  </Item>
                </AML>
                ";

            aml = aml.Replace("{#mtName}", cfg.mtName)
                .Replace("{#meeting_id}", cfg.meeting_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "aml: " + aml);
            Item itmMeeting = cfg.inn.applyAML(aml);

            if (itmMeeting.isError())
            {
                throw new Exception("取得賽事資料發生錯誤");
            }

            return itmMeeting;
        }

        //取得與會者 (未確認)
        private Item GetAdminMeetingUsers(TConfig cfg)
        {
            string sql = @"
                SELECT
                    t1.*
                    , t2.in_tel AS 'in_creator_tel'
                FROM
                    {#muName} t1 WITH(NOLOCK)
                LEFT OUTER JOIN
                    IN_RESUME t2 WITH(NOLOCK) ON t2.in_sno = t1.in_creator_sno
                WHERE
                    t1.source_id = '{#meeting_id}'
                    AND ISNULL(t1.in_paynumber, '') = ''
                    AND ISNULL(t1.in_creator_sno, '') <> ''
                    AND ISNULL(t1.in_l1, '') <> N'隊職員'
                ORDER BY
                    t1.in_creator_sno
                    , t1.in_l1
                    , t1.in_l2
                    , t1.in_l3
                    , t1.in_index
                ";

            sql = sql.Replace("{#muName}", cfg.muName)
                .Replace("{#meeting_id}", cfg.meeting_id);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item items = cfg.inn.applySQL(sql);

            if (IsError(items, isSingle: false))
            {
                //throw new Exception("取得與會者資料發生錯誤");
            }

            return items;
        }

        //取得繳費單與會者 (未繳費、已繳費)
        private Item GetAdminMeetingPays(TConfig cfg, string pay_bool)
        {
            string sql = @"
                SELECT
                    t1.item_number
                    , t1.pay_bool
                    , t1.in_pay_amount_exp
                    , t1.in_pay_amount_real
                    , t1.in_group
                    , t1.in_current_org
                    , t2.in_l1
                    , t2.in_l2
                    , t2.in_l3
                    , t2.in_index
                    , t2.in_name
                    , t2.in_sno
                    , t2.in_pay_amount AS 'in_expense'
                    , t2.in_creator
                    , t2.in_creator_sno
                    , t2.in_cancel_status
                    , t3.in_tel AS 'in_creator_tel'
                FROM
                    IN_MEETING_PAY t1 WITH(NOLOCK)
                INNER JOIN 
                    IN_MEETING_NEWS t2 WITH(NOLOCK)
                    ON t2.source_id = t1.id
                LEFT OUTER JOIN
                    IN_RESUME t3 WITH(NOLOCK)
                    ON t3.in_sno = t2.in_creator_sno
                WHERE
                    t1.{#mtProperty} = '{#meeting_id}'
                    AND t1.pay_bool = N'{#pay_bool}'
                    AND ISNULL(t2.in_muid, '') <> ''
                ORDER BY
                    t2.in_creator_sno
                    , t1.item_number
                    , t2.in_l1
                    , t2.in_l2
                    , t2.in_l3
                    , t2.in_index
            ";

            sql = sql.Replace("{#mtProperty}", cfg.mtProperty)
                .Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#pay_bool}", pay_bool); ;

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item items = cfg.inn.applySQL(sql);

            if (IsError(items, isSingle: false))
            {
                throw new Exception("取得繳費單資料發生錯誤");
            }

            return items;
        }

        //取得與會者 (未確認)
        private Item GetOrgMeetingUsers(TConfig cfg, string in_creator_sno)
        {
            string sql = @"
                SELECT
                    t1.*
                    , t2.in_tel AS 'in_creator_tel'
                FROM
                    {#muName} t1 WITH(NOLOCK)
                LEFT OUTER JOIN
                    IN_RESUME t2 WITH(NOLOCK) ON t2.in_sno = t1.in_creator_sno
                WHERE
                    t1.source_id = '{#meeting_id}'
                    AND ISNULL(t1.in_l1, '') <> N'隊職員'
                    AND ISNULL(t1.in_creator_sno, '') = '{#in_creator_sno}'
                    AND ISNULL(t1.in_paynumber, '') = ''
                ORDER BY
                    t1.in_creator_sno
                    , t1.in_l1
                    , t1.in_l2
                    , t1.in_l3
                    , t1.in_index
                ";

            sql = sql.Replace("{#muName}", cfg.muName)
                .Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_creator_sno}", in_creator_sno);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item items = cfg.inn.applySQL(sql);

            if (IsError(items, isSingle: false))
            {
                throw new Exception("取得與會者資料發生錯誤");
            }

            return items;
        }


        //取得繳費單與會者 (未繳費、已繳費)
        private Item GetOrgMeetingPays(TConfig cfg, string in_creator_sno, string pay_bool)
        {
            string sql = @"
                SELECT
                    t1.item_number
                    , t1.pay_bool
                    , t1.in_pay_amount_exp
                    , t1.in_pay_amount_real
                    , t1.in_group
                    , t1.in_current_org
                    , t1.in_creator
                    , t1.in_creator_sno
                    , t2.in_l1
                    , t2.in_l2
                    , t2.in_l3
                    , t2.in_index
                    , t2.in_name
                    , t2.in_sno
                    , t2.in_pay_amount AS 'in_expense'
                    , t2.in_cancel_status
                    , t3.in_tel AS 'in_creator_tel'
                FROM
                    IN_MEETING_PAY t1 WITH(NOLOCK)
                INNER JOIN 
                    IN_MEETING_NEWS t2 WITH(NOLOCK)
                    ON t2.source_id = t1.id
                LEFT OUTER JOIN
                    IN_RESUME t3 WITH(NOLOCK)
                    ON t3.in_sno = t1.in_creator_sno
                WHERE
                    t1.{#mtProperty} = '{#meeting_id}'
                    AND t1.in_creator_sno = N'{#in_creator_sno}'
                    AND t1.pay_bool = N'{#pay_bool}'
                    AND ISNULL(t2.in_muid, '') <> ''
                ORDER BY
                    t1.item_number
                    , t2.in_l1
                    , t2.in_l2
                    , t2.in_l3
                    , t2.in_index
             ";

            sql = sql.Replace("{#mtProperty}", cfg.mtProperty)
                .Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_creator_sno}", in_creator_sno)
                .Replace("{#pay_bool}", pay_bool);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item items = cfg.inn.applySQL(sql);

            if (IsError(items, isSingle: false))
            {
                throw new Exception("取得繳費單資料發生錯誤");
            }

            return items;
        }

        private Dictionary<string, Item> GetL1Options(TConfig cfg)
        {
            string sql = @"
                SELECT
                    t3.in_value
                    , t3.in_label
                    , t3.in_expense_value
                    , t3.in_extend_value
                    , t3.sort_order
                FROM 
                    {#msName} t1 WITH(NOLOCK)
                INNER JOIN 
                    {#svName} t2 WITH(NOLOCK)
                    ON t2.id = t1.related_id
                INNER JOIN
                    {#soName} t3 WITH(NOLOCK)
                    ON t3.source_id = t1.related_id
                WHERE
                    t1.source_id = '{#meeting_id}'
                    AND t2.in_property = 'in_l1'
                ";

            sql = sql.Replace("{#msName}", cfg.msName)
                .Replace("{#svName}", cfg.svName)
                .Replace("{#soName}", cfg.soName)
                .Replace("{#meeting_id}", cfg.meeting_id);
            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item items = cfg.inn.applySQL(sql);

            Dictionary<string, Item> entities = new Dictionary<string, Item>();
            if (IsError(items, isSingle: false))
            {
                return entities;
            }

            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_value = item.getProperty("in_value", "");

                if (entities.ContainsKey(in_value))
                {
                    //異常
                }
                else
                {
                    entities.Add(in_value, item);
                }
            }
            return entities;
        }


        private Item GetAdminMeetingPayList(TConfig cfg, string pay_bool)
        {
            string sql = @"
                SELECT
                    t1.*
                FROM
                    IN_MEETING_PAY t1 WITH(NOLOCK)
                WHERE
                    {#mtProperty} = '{#meeting_id}'
                    AND pay_bool = N'{#pay_bool}'    
                ";

            sql = sql.Replace("{#mtProperty}", cfg.mtProperty)
                .Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#pay_bool}", pay_bool);

            return cfg.inn.applySQL(sql);
        }

        private Item GetOrgMeetingPayList(TConfig cfg, string in_creator_sno, string pay_bool)
        {
            string sql = @"
                SELECT
                    t1.*
                FROM
                    IN_MEETING_PAY t1 WITH(NOLOCK)
                WHERE
                    {#mtProperty} = '{#meeting_id}'
                    AND in_creator_sno = N'{#in_creator_sno}'
                    AND pay_bool = N'{#pay_bool}'    
                ";

            sql = sql.Replace("{#mtProperty}", cfg.mtProperty)
                .Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_creator_sno}", in_creator_sno)
                .Replace("{#pay_bool}", pay_bool);

            return cfg.inn.applySQL(sql);
        }

        #endregion

        private void AppendTables(Dictionary<string, TGroup> entities, Item itmReturn)
        {
            StringBuilder builder = new StringBuilder();

            int i = 1;
            List<string> tables = new List<string>();

            foreach (KeyValuePair<string, TGroup> kv in entities)
            {
                var in_l1 = kv.Key;
                var entity = kv.Value;
                var player_list = entity.Players;
                var team_player_list = entity.TeamPlayers;

                var table_name = "user_table_" + i.ToString();
                tables.Add("#" + table_name);

                var team_count = entity.TeamPlayers.Count;
                var in_title = in_l1 + "(共 " + team_count + " 人)";
                if (entity.IsMultiPlayer)
                {
                    in_title = in_l1 + "(共 " + team_count + " 隊)";
                }

                builder.AppendLine("<div>");
                builder.AppendLine("  <h3> " + in_title + " </h3>");

                if (entity.IsMultiPlayer)
                {
                    AppendTeamTable(builder, table_name, team_player_list, itmReturn);
                }
                else
                {
                    AppendPlayerTable(builder, table_name, player_list, itmReturn);
                }

                builder.AppendLine("</div>");
            }
            itmReturn.setProperty("inn_tables", builder.ToString());
            itmReturn.setProperty("inn_names", string.Join(", ", tables));
        }

        private void AppendTeamTable(StringBuilder builder, string table_name, Dictionary<string, List<Item>> dictionary, Item itmReturn)
        {
            string name_head = itmReturn.getProperty("name_head", "");
            string section_head = itmReturn.getProperty("section_head", "");
            string team_head = itmReturn.getProperty("team_head", "");
            bool show_team = itmReturn.getProperty("show_team", "") == "1";
            bool show_section = itmReturn.getProperty("show_section", "") == "1";
            bool show_birth = itmReturn.getProperty("show_birth", "") == "1";
            bool show_paynumber = itmReturn.getProperty("show_paynumber", "") == "1";
            bool show_refund = itmReturn.getProperty("show_refund", "") == "1";

            StringBuilder head = new StringBuilder();
            head.AppendLine("<thead>");
            head.AppendLine("    <th class='text-center' data-field='inn_no' data-sortable='false'>項次</th>");
            head.AppendLine("    <th class='text-center' data-field='in_current_org' data-sortable='true'>所屬單位</th>");

            //隊別
            if (show_team)
            {
                head.AppendLine("    <th class='text-center' data-field='in_team' data-sortable='true'>隊別</th>");
            }
            //人數
            head.AppendLine("    <th class='text-center' data-field='inn_player_count' data-sortable='true'>" + team_head + "</th>");

            //組別
            if (show_section)
            {
                head.AppendLine("    <th class='text-center' data-field='in_section_name' data-sortable='true'>" + section_head + "</th>");
            }
            //序號
            head.AppendLine("    <th class='text-center' data-field='in_index' data-sortable='true'>序號</th>");

            head.AppendLine("    <th class='text-center' data-field='in_expense' data-sortable='true'>費用</th>");
            if (show_paynumber)
            {
                head.AppendLine("    <th class='text-center' data-field='item_number' data-sortable='true'>繳費單號</th>");
            }
            if (show_refund)
            {
                head.AppendLine("    <th class='text-center' data-field='in_cancel_status' data-sortable='true'>報名狀態</th>");
            }
            head.AppendLine("    <th class='text-center' data-field='in_creator' data-sortable='true'>協助報名者<br>姓名</th>");
            head.AppendLine("    <th class='text-center' data-field='in_creator_tel' data-sortable='true'>協助報名者<br>電話</th>");
            head.AppendLine("</thead>");

            StringBuilder body = new StringBuilder();

            int i = 0;
            body.AppendLine("<tbody>");
            foreach (KeyValuePair<string, List<Item>> kv in dictionary)
            {
                var key = kv.Key;
                var list = kv.Value;
                var player_count = list.Count;

                Item item = list[0];
                string inn_no = (i + 1).ToString();
                string in_current_org = item.getProperty("in_current_org", "");
                string in_team = item.getProperty("in_team", "");
                string in_section_name = GetDollarValue(item.getProperty("in_section_name", ""));
                string inn_player_count = player_count.ToString();
                string in_expense = GetDollarValue(item.getProperty("in_expense", ""));
                string in_index = item.getProperty("in_index", "");
                string in_creator = item.getProperty("in_creator", "");
                string in_creator_tel = item.getProperty("in_creator_tel", "");
                string in_cancel_status = item.getProperty("in_cancel_status", "");
                string in_cancel_display = GetCancelDisplay(in_cancel_status);

                string item_number = GetPayNumberLink(item.getProperty("item_number", ""));

                body.AppendLine("<tr>");
                body.AppendLine("        <td class='text-left' data-field='inn_no' >" + inn_no + "</td>");
                body.AppendLine("        <td class='text-left' data-field='in_current_org' >" + in_current_org + "</td>");

                //隊別
                if (show_team)
                {
                    body.AppendLine("        <td class='text-left' data-field='in_team' >" + in_team + "</td>");
                }
                //人數
                body.AppendLine("        <td class='text-left' data-field='inn_player_count' >" + inn_player_count + "</td>");

                //組別
                if (show_section)
                {
                    body.AppendLine("        <td class='text-left' data-field='in_section_name' >" + in_section_name + "</td>");
                }
                //序號
                body.AppendLine("        <td class='text-left' data-field='in_index' >" + in_index + "</td>");

                body.AppendLine("        <td class='text-left' data-field='in_expense' >" + in_expense + "</td>");
                if (show_paynumber)
                {
                    body.AppendLine("        <td class='text-left' data-field='item_number' >" + item_number + "</td>");
                }

                if (show_refund)
                {
                    body.AppendLine("        <td class='warning text-center' data-field='in_cancel_status' >" + in_cancel_display + "</td>");
                }

                body.AppendLine("        <td class='text-left' data-field='in_creator' >" + in_creator + "</td>");
                body.AppendLine("        <td class='text-left' data-field='in_creator_tel' >" + in_creator_tel + "</td>");
                body.AppendLine("</tr>");

                i++;
            }
            body.AppendLine("</tbody>");

            builder.AppendLine(GetTableAttribute(table_name));
            builder.Append(head);
            builder.Append(body);
            builder.AppendLine("</table>");
        }

        private void AppendPlayerTable(StringBuilder builder, string table_name, List<Item> list, Item itmReturn)
        {
            string name_head = itmReturn.getProperty("name_head", "");
            string section_head = itmReturn.getProperty("section_head", "");
            string team_head = itmReturn.getProperty("team_head", "");
            bool show_team = itmReturn.getProperty("show_team", "") == "1";
            bool show_section = itmReturn.getProperty("show_section", "") == "1";
            bool show_birth = itmReturn.getProperty("show_birth", "") == "1";
            bool show_paynumber = itmReturn.getProperty("show_paynumber", "") == "1";
            bool show_refund = itmReturn.getProperty("show_refund", "") == "1";

            StringBuilder head = new StringBuilder();
            head.AppendLine("<thead>");
            head.AppendLine("    <th class='text-center' data-field='inn_no' data-sortable='true'>項次</th>");
            head.AppendLine("    <th class='text-center' data-field='in_current_org' data-sortable='true'>所屬單位</th>");
            head.AppendLine("    <th class='text-center' data-field='in_name' data-sortable='true'>" + name_head + "</th>");
            head.AppendLine("    <th class='text-center' data-field='in_sno' data-sortable='true'>身分證號</th>");

            if (show_birth)
            {
                head.AppendLine("    <th class='text-center' data-field='in_birth' data-sortable='true'>西元生日</th>");
            }

            //組別
            if (show_section)
            {
                head.AppendLine("    <th class='text-center' data-field='in_section_name' data-sortable='true'>" + section_head + "</th>");
            }
            //序號
            head.AppendLine("    <th class='text-center' data-field='in_index' data-sortable='true'>序號</th>");

            head.AppendLine("    <th class='text-center' data-field='in_expense' data-sortable='true'>費用</th>");
            if (show_paynumber)
            {
                head.AppendLine("    <th class='text-center' data-field='item_number' data-sortable='true'>繳費單號</th>");
            }
            if (show_refund)
            {
                head.AppendLine("    <th class='text-center' data-field='in_cancel_status' data-sortable='true'>報名狀態</th>");
            }

            head.AppendLine("    <th class='text-center' data-field='in_creator' data-sortable='true'>協助報名者<br>姓名</th>");
            head.AppendLine("    <th class='text-center' data-field='in_creator_tel' data-sortable='true'>協助報名者<br>電話</th>");
            head.AppendLine("</thead>");

            StringBuilder body = new StringBuilder();
            int count = list.Count;

            body.AppendLine("<tbody>");
            for (int i = 0; i < count; i++)
            {
                Item item = list[i];
                string inn_no = (i + 1).ToString();
                string in_current_org = item.getProperty("in_current_org", "");
                string in_name = item.getProperty("in_name", "");
                string in_sno = item.getProperty("in_sno", "");
                string in_birth = GetDateTimeValue(item.getProperty("in_birth", ""), "yyyy-MM-dd", true);
                string in_expense = GetDollarValue(item.getProperty("in_expense", ""));
                string in_section_name = GetDollarValue(item.getProperty("in_section_name", ""));
                string in_index = item.getProperty("in_index", "");
                string in_creator = item.getProperty("in_creator", "");
                string in_creator_sno = item.getProperty("in_creator_sno", "");
                string in_creator_tel = item.getProperty("in_creator_tel", "");

                string in_cancel_status = item.getProperty("in_cancel_status", "");
                string in_cancel_display = GetCancelDisplay(in_cancel_status);

                string item_number = GetPayNumberLink(item.getProperty("item_number", ""));

                body.AppendLine("<tr>");
                body.AppendLine("        <td class='text-left' data-field='inn_no' >" + inn_no + "</td>");
                body.AppendLine("        <td class='text-left' data-field='in_current_org' >" + in_current_org + "</td>");
                body.AppendLine("        <td class='text-left' data-field='in_name' >" + in_name + "</td>");
                body.AppendLine("        <td class='text-left' data-field='in_sno' >" + in_sno + "</td>");

                if (show_birth)
                {
                    body.AppendLine("        <td class='text-left' data-field='in_birth' >" + in_birth + "</td>");
                }

                //組別
                if (show_section)
                {
                    body.AppendLine("        <td class='text-left' data-field='in_section_name' >" + in_section_name + "</td>");
                }
                //序號
                body.AppendLine("        <td class='text-left' data-field='in_index' >" + in_index + "</td>");

                //費用
                body.AppendLine("        <td class='text-left' data-field='in_expense' >" + in_expense + "</td>");
                //繳費單號
                if (show_paynumber)
                {
                    body.AppendLine("        <td class='text-left' data-field='item_number' >" + item_number + "</td>");
                }

                if (show_refund)
                {
                    body.AppendLine("        <td class='warning text-center' data-field='in_cancel_status' >" + in_cancel_display + "</td>");
                }

                body.AppendLine("        <td class='text-left' data-field='in_creator' >" + in_creator + "</td>");
                body.AppendLine("        <td class='text-left' data-field='in_creator_tel' >" + in_creator_tel + "</td>");
                body.AppendLine("</tr>");
            }
            body.AppendLine("</tbody>");

            builder.AppendLine(GetTableAttribute(table_name));
            builder.Append(head);
            builder.Append(body);
            builder.AppendLine("</table>");
        }

        private string GetTableAttribute(string table_name)
        {
            return "<table id='" + table_name + "' class='table table-hover table-bordered table-rwd rwd rwdtable' data-toggle='table' data-click-to-select='true' data-striped='false' data-search='true' data-pagination='true' data-show-pagination-switch='false'>";
        }

        private string GetPayNumberLink(string item_number)
        {
            return "<a href='javascript:void()' onclick='ItemNumber_Click(\"" + item_number + "\")'>" + item_number + "</a>";
        }

        private string GetDollarValue(string value)
        {
            int amount = 0;
            if (Int32.TryParse(value, out amount))
            {
                return amount.ToString("####,####");
            }
            else
            {
                return value;
            }
        }

        private int GetNewTotal(string in_amount, string in_refund)
        {
            int amount = 0;
            int refund = 0;

            Int32.TryParse(in_amount, out amount);
            Int32.TryParse(in_refund, out refund);

            return (amount - refund);
        }

        private string GetDateTimeValue(string value, string format = "yyyy-MM-dd", bool bAdd8Hour = false)
        {
            if (value == "")
            {
                return "";
            }

            string day = value.Replace("/", "-");

            DateTime dt = DateTime.Now;
            if (DateTime.TryParse(day, out dt))
            {
                if (bAdd8Hour)
                {
                    return dt.AddHours(8).ToString(format);
                }
                else
                {
                    return dt.ToString(format);
                }
            }
            else
            {
                return value;
            }
        }

        private string GetCancelDisplay(string value)
        {
            switch (value)
            {
                case "退費申請中": return "退費申請中";
                case "變更申請中": return "變更申請中";

                case "申請取消通過": return "X";
                case "申請取消不通過": return "O";

                case "申請變更通過": return "O";
                case "申請變更不通過": return "O";

                case "變更後取消": return "X";
                case "人員變更": return "O";

                default: return "O";
            }
        }

        private Dictionary<string, TGroup> MapDctionary(Item items, Dictionary<string, Item> l1_options)
        {
            Dictionary<string, TGroup> entities = new Dictionary<string, TGroup>();
            if (items == null)
            {
                return entities;
            }

            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_l1 = item.getProperty("in_l1", "");
                string in_l2 = item.getProperty("in_l2", "");
                string in_l3 = item.getProperty("in_l3", "");
                string in_index = item.getProperty("in_index", "");
                string in_section_name = item.getProperty("in_section_name", "");
                string team_key = in_l1 + "-" + in_l2 + "-" + in_l3 + "-" + in_index;

                if (in_section_name == "")
                {
                    if (in_l1 != "" && in_l2 != "" && in_l3 != "")
                    {
                        in_section_name = in_l1 + "-" + in_l2 + "-" + in_l3;
                    }
                    else if (in_l1 != "" && in_l2 != "")
                    {
                        in_section_name = in_l1 + "-" + in_l2;
                    }
                    else if (in_l1 != "")
                    {
                        in_section_name = in_l1;
                    }

                    item.setProperty("in_section_name", in_section_name);
                }
                TGroup entity = null;
                if (entities.ContainsKey(in_l1))
                {
                    entity = entities[in_l1];
                }
                else
                {
                    entity = new TGroup
                    {
                        IsMultiPlayer = IsMultiPlayer(l1_options, in_l1),
                        Players = new List<Item>(),
                        TeamPlayers = new Dictionary<string, List<Item>>()
                    };
                    entities.Add(in_l1, entity);
                }

                entity.Players.Add(item);

                List<Item> team_players = null;
                if (entity.TeamPlayers.ContainsKey(team_key))
                {
                    team_players = entity.TeamPlayers[team_key];
                }
                else
                {
                    team_players = new List<Item>();
                    entity.TeamPlayers.Add(team_key, team_players);
                }
                team_players.Add(item);
            }

            return entities;
        }

        private bool IsMultiPlayer(Dictionary<string, Item> l1_options, string in_value)
        {
            if (!l1_options.ContainsKey(in_value))
            {
                return false;
            }

            Item item = l1_options[in_value];
            string in_extend_value = item.getProperty("in_extend_value", "");

            if (!in_extend_value.Contains("in_limit"))
            {
                return false;
            }
            else if (in_extend_value.Contains("in_limit:1~1"))
            {
                return false;
            }
            else
            {
                return true;
            }
        }


        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string login_user_id { get; set; }

            public string meeting_id { get; set; }
            public string meeting_type { get; set; }
            public string pay_status { get; set; }
            public string exe_type { get; set; }
            public string mode { get; set; }
            public string resume_id { get; set; }

            public Item itmPermit { get; set; }
            public Item itmLoginResume { get; set; }
            public Item itmResumeView { get; set; }
            public Item itmMeeting { get; set; }

            public string mtName { get; set; }
            public string muName { get; set; }
            public string msName { get; set; }
            public string mtProperty { get; set; }
            public string svName { get; set; }
            public string soName { get; set; }

            public bool isMeetingAdmin { get; set; }
        }

        private class TGroup
        {
            /// <summary>
            /// 是否為團體
            /// </summary>
            public bool IsMultiPlayer { get; set; }

            /// <summary>
            /// 選手清單
            /// </summary>
            public List<Item> Players { get; set; }

            /// <summary>
            /// 隊伍編號清單
            /// </summary>
            public Dictionary<string, List<Item>> TeamPlayers { get; set; }
        }


        private bool IsError(Item item, bool isSingle = true)
        {
            if (item.isError() || item.getResult() == "")
            {
                return true;
            }
            else if (isSingle && item.getItemCount() != 1)
            {
                return true;
            }
            else if (item.getItemCount() <= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //in_meeting_pay 條碼2 需加上遮罩
        private string GetCodeMask(string value)
        {
            if (value.Length == 16)
            {
                return value.Substring(0, 4) + "*******" + value.Substring(value.Length - 5, 5);
            }
            return "";

        }
    }
}