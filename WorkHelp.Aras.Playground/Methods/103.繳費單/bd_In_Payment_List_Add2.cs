﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class bd_In_Payment_List_Add2 : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
            目的: 產生繳費條碼 (無 Excel 版)
            日期: 2020-08-20 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]" + "In_Payment_List_Add2";

            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            string aml = "";
            string sql = "";
            string strError = "";
            string strUserId = inn.getUserID();
            string strIdentityId = inn.getUserAliases();

            _InnH.AddLog(strMethodName, "MethodSteps");

            Item itmR = this;
            // CCO.Utilities.WriteDebug(strMethodName, "dom: " + this.dom.InnerXml);

            //是否為協會手動建立繳費單
            Item itmPermit = inn.applyMethod("In_CheckIdentity", "<method>" + strMethodName + "</method><code>" + "ALL" + "</code>");
            bool isMeetingAdmin = itmPermit.getProperty("isMeetingAdmin", "") == "1";
            if (!isMeetingAdmin)
            {
                throw new Exception("無權限");
            }

            //取得賽事ID
            string meeting_id = this.getProperty("meeting_id", "");
            if (meeting_id == "")
            {
                throw new Exception("賽事 id 不得為空白");
            }

            //取得前台給的[所屬單位]
            string current_orgs = this.getProperty("current_orgs", "").Trim(',');
            string[] in_current_orgs = current_orgs.Split(',');

            //取得前台給的[抬頭發票]
            string invoice_up = this.getProperty("invoice_up", "");
            string[] invoice_ups = invoice_up.Split(',');

            //取得前台給的[統一編號]
            string uniform_numbers = this.getProperty("uniform_numbers", "");
            string[] uniform_numberss = uniform_numbers.Split(',');

            string resume_id = this.getProperty("resume_id", "");
            string target_group = this.getProperty("in_group", ""); //所屬單位

            //取得登入者資訊
            sql = "Select *";
            sql += " from In_Resume WITH(NOLOCK) ";
            sql += " where id = '" + resume_id + "'";
            Item In_Resume = inn.applySQL(sql);
            //CCO.Utilities.WriteDebug(strMethodName, sql);

            string target_resume_name = In_Resume.getProperty("in_name", "");//姓名
            string target_resume_sno = In_Resume.getProperty("in_sno", "");//身分證號

            //產生條碼
            string codes = "10"; //備用碼(1)+運動代碼(0)
            string Payment_mode = "A"; //狀態(A>繳款人 B>委託單位)
            string index_number = ""; //業主自訂編號
            string barcode2 = ""; //條碼2
            string barcode2_1 = ""; //臨櫃繳費虛擬帳號
            string Project_code = ""; //項目代碼
            string in_imf_date_e = ""; //最後報名日期
            string B1 = ""; //條碼1
            string B2 = ""; //條碼2
            string B3 = ""; //條碼3
            string blank = " ";

            //應繳日期&最後繳費日期為[今日+7天](尚未確定範圍)
            DateTime CurrentTime = System.DateTime.Today;
            string format = "yyyy-MM-ddTHH:mm:ss";
            string Add_time = DateTime.Now.ToString(format); //建單時間
            string Due_date = CurrentTime.AddDays(7).ToString(format);//應繳日期
            string Last_payment_date = CurrentTime.AddDays(7).ToString(format);//最後收費日期

            //*********************************************************************

            //取得該[賽事]
            aml = "<AML><Item type='In_Meeting' action='get'><id>#meeting_id</id></Item></AML>";
            aml = aml.Replace("#meeting_id", meeting_id);
            //CCO.Utilities.WriteDebug(strMethodName, "aml:" + aml);
            Item itmMeeting = inn.applyAML(aml);

            string in_meeting_code = itmMeeting.getProperty("item_number", ""); //賽事編號
            string in_receice_org_code = itmMeeting.getProperty("in_receice_org_code", ""); //收款單位編號

            if (in_receice_org_code == "")
            {
                throw new Exception("Meeting 收款單位編號不可為空白");
            }

            //當收款單位編號長度為 6 碼
            if (in_receice_org_code.Length == 6)
            {
                codes = "";//預設為 10
            }

            //取得該[賽事的報名結束日]
            aml = @"<AML><Item type='In_Meeting_Functiontime' action='get'><source_id>#meeting_id</source_id></Item></AML>";
            aml = aml.Replace("#meeting_id", meeting_id);
            Item In_Meeting_Functiontime = inn.applyAML(aml);

            in_imf_date_e = In_Meeting_Functiontime.getProperty("in_date_e", ""); //最後報名日期
            //如果[建單日期]與[賽事報名結束日期]差距天數小於7天 直接讓[最後繳費期限]訂在[報名結束日期]
            DateTime in_imf_date_e_dt = Convert.ToDateTime(in_imf_date_e); //最後報名日期
            TimeSpan ts = in_imf_date_e_dt - System.DateTime.Today;
            double days = ts.TotalDays;
            if (days < 7)
            {
                Last_payment_date = in_imf_date_e;
                Due_date = in_imf_date_e;
            }

            //lina 2021.03.10: 如果是會員繳費，日期壓當年年底
            int year = DateTime.Now.Year;
            Due_date = (new DateTime(year, 12, 31)).ToString(format); //應繳日期
            Last_payment_date = Due_date;


            Dictionary<string, TOrg> org_list = GetOrgSummary(CCO, strMethodName, inn, meeting_id, target_group);
            string numbers = "";

            int count_org = in_current_orgs.Length;

            for (int i = 0; i < count_org; i++)
            {
                string target_org = in_current_orgs[i].Trim();
                if (target_org == "")
                {
                    continue;
                }

                //查無資料
                if (!org_list.ContainsKey(target_org))
                {
                    continue;
                }

                TOrg entity = org_list[target_org];

                //非隊員的筆數 (一個人多項目)
                int user_count = entity.UserList.Count;

                //將最後收費日期&應繳日期轉型
                DateTime in_pay_date_exp1 = Convert.ToDateTime(Last_payment_date.Split('T')[0]);
                DateTime in_pay_date_exp = Convert.ToDateTime(Due_date.Split('T')[0]);

                //費用
                string expenses = entity.Amount.ToString();
                //隊職員數
                string stuffs = entity.StaffList.Count.ToString();
                //選手人數
                string players = entity.PlayerList.Count.ToString();
                //參賽項目總計
                string items = entity.ItemList.Count.ToString();

                //區分繳費模式(A>繳款人 B>委託單位)
                string project_code = GetProjectCode(Payment_mode, entity.Amount);

                //找出該[賽事]繳費單最大的[業主自訂編號]
                sql = "Select MAX(index_number)";
                sql += " from In_Meeting_pay";
                sql += " where in_meeting=N'" + meeting_id + "'";
                Item pay = inn.applySQL(sql);

                //如果為空代表 為該賽事第一張單
                if (pay.getResult() == "")
                {
                    barcode2 = codes + in_meeting_code.Substring(in_meeting_code.Length - 4) + "0001";
                    barcode2_1 = codes + in_meeting_code.Substring(in_meeting_code.Length - 3) + "0001";
                }
                else
                {
                    //若不是第一張單則將最大的[業主自訂編號]+1
                    //取出最大+1
                    int number_next = Int32.Parse(pay.getProperty("column1", "")) + 1;
                    index_number = number_next.ToString();
                    //[備用碼(1)運動代碼(0)] + [運動賽事序號(後4碼)]-[流水號(4)(最大+1)(取後4碼)]
                    barcode2 = codes + in_meeting_code.Substring(in_meeting_code.Length - 4) + index_number.Substring(index_number.Length - 4);
                    barcode2_1 = codes + in_meeting_code.Substring(in_meeting_code.Length - 3) + index_number.Substring(index_number.Length - 4);
                }

                //這是超商繳費
                //第一碼(最後收費日期(DateTime),項目代碼(String))
                B1 = BarCode1_OutPut(in_pay_date_exp1, project_code);
                //第二碼(賽事編號(String),業者自訂編號(String))

                B2 = in_receice_org_code + barcode2_1;

                string checkhum = inn.applyMethod("In_GetRank_CheckSum", "<account>" + B2 + "</account><pay>" + expenses + "</pay>").getResult();

                B2 = B2 + checkhum;

                //B2 = BarCode2_OutPut(in_receice_org_code,barcode2);
                //第三碼(應繳日期(DateTime),費用(String),一碼(String),二碼(String))
                B3 = BarCode3_OutPut(in_pay_date_exp, expenses, B1, B2);

                //產生繳費單
                Item SingleNumber = inn.newItem("In_Meeting_pay", "add");
                SingleNumber.setProperty("owned_by_id", strIdentityId);//建單者
                SingleNumber.setProperty("in_pay_amount_exp", expenses);//應繳金額
                SingleNumber.setProperty("in_pay_date_exp", Due_date.Split('T')[0]);//應繳日期
                SingleNumber.setProperty("in_pay_date_exp1", Last_payment_date.Split('T')[0]);//最後收費日期
                SingleNumber.setProperty("in_group", target_group);//所屬群組
                SingleNumber.setProperty("in_creator", target_resume_name);//協助報名者
                SingleNumber.setProperty("in_creator_sno", target_resume_sno);//協助報名者身分證字號
                SingleNumber.setProperty("in_current_org", target_org);//所屬單位
                SingleNumber.setProperty("in_real_stuff", stuffs);//隊職員數
                SingleNumber.setProperty("in_real_player", players);//選手人數
                SingleNumber.setProperty("in_real_items", items);//項目總計
                SingleNumber.setProperty("in_code_1", B1);//條碼1
                SingleNumber.setProperty("in_code_2", "00" + B2);//條碼2
                //SingleNumber.setProperty("in_code_2", B2.PadLeft(16,'0'));//條碼2
                SingleNumber.setProperty("in_code_3", B3);//條碼3
                SingleNumber.setProperty("pay_bool", "未繳費");//繳費狀態
                SingleNumber.setProperty("in_add_time", Add_time);//建單時間
                SingleNumber.setProperty("index_number", barcode2);//業主自訂編號
                //SingleNumber.setProperty("index_number", barcode2_1);//業主自訂編號
                //因著有可能[發票抬頭],[統一編號]為沒輸入的情況 不Trim(',')而是+1 之後開始取值
                SingleNumber.setProperty("invoice_up", invoice_ups[i + 1]);//發票抬頭
                SingleNumber.setProperty("uniform_numbers", uniform_numberss[i + 1]);//統一編號
                SingleNumber.setProperty("in_meeting", meeting_id);
                Item itmNewPayment = SingleNumber.apply();

                //取出該賽事的關聯繳費
                aml = "<AML>" +
                    "<Item type='In_Meeting_pay' action='get'>" +
                    "<in_meeting>" + meeting_id + "</in_meeting>" +
                    "<id>" + itmNewPayment.getProperty("id", "") + "</id>" +
                    "</Item></AML>";
                Item SingleNumbers = inn.applyAML(aml);

                for (int j = 0; j < user_count; j++)
                {
                    Item itmMeetingUser = entity.UserList[j];
                    string in_regdate = itmMeetingUser.getProperty("in_regdate", "");
                    in_regdate = GetDateTimeValue(in_regdate, "yyyy-MM-ddTHH:mm:ss", true);

                    //新增到繳費資訊下
                    Item in_meeting_news = inn.newItem("In_Meeting_news", "add");
                    in_meeting_news.setProperty("in_ans_l3", itmMeetingUser.getProperty("in_gameunit", ""));//組別彙整結果
                    in_meeting_news.setProperty("in_pay_amount", itmMeetingUser.getProperty("in_expense", ""));//應繳金額
                    in_meeting_news.setProperty("in_name", itmMeetingUser.getProperty("in_name", ""));//姓名
                    in_meeting_news.setProperty("in_sno", itmMeetingUser.getProperty("in_sno", ""));//身分證
                    in_meeting_news.setProperty("in_creator", itmMeetingUser.getProperty("in_creator", ""));//協助報名者
                    in_meeting_news.setProperty("in_creator_sno", itmMeetingUser.getProperty("in_creator_sno", ""));//協助報名者帳號
                    in_meeting_news.setProperty("in_regdate", in_regdate);//報名日期
                    in_meeting_news.setProperty("in_l1", itmMeetingUser.getProperty("in_l1", ""));//第一層
                    in_meeting_news.setProperty("in_l2", itmMeetingUser.getProperty("in_l2", ""));//第二層
                    in_meeting_news.setProperty("in_l3", itmMeetingUser.getProperty("in_l3", ""));//第三層
                    in_meeting_news.setProperty("in_index", itmMeetingUser.getProperty("in_index", ""));//序號
                    in_meeting_news.setProperty("in_team_index", itmMeetingUser.getProperty("in_team_index", ""));//隊伍序號
                    in_meeting_news.setProperty("in_l1_sort", itmMeetingUser.getProperty("in_l1_sort", ""));//第一層排序

                    in_meeting_news.setProperty("in_section_name", itmMeetingUser.getProperty("in_section_name", ""));//組名
                    in_meeting_news.setProperty("in_muid", itmMeetingUser.getProperty("id", ""));//與會者id

                    SingleNumbers.addRelationship(in_meeting_news);

                }
                SingleNumbers.apply();

                //將[繳費單號]壓回去[與會者]
                for (int j = 0; j < user_count; j++)
                {
                    Item itmMeetingUser = entity.UserList[j];

                    sql = "UPDATE [In_Meeting_User] SET ";
                    sql += "    in_paynumber = N'" + SingleNumbers.getProperty("item_number", "") + "'";
                    sql += " WHERE source_id = '" + meeting_id + "' AND in_group = N'" + target_group + "' AND in_current_org = N'" + target_org + "' AND ISNULL(in_paynumber, '') = ''";
                    //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);
                    Item UserNumber = inn.applySQL(sql);
                }
                numbers += "," + SingleNumbers.getProperty("item_number", "");
            }

            itmR.setProperty("numbers", numbers.Trim(','));

            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

            return itmR;
        }


        private string GetDateTimeValue(string value, string format = "yyyy-MM-dd", bool bAdd8Hour = false)
        {
            if (value == "")
            {
                return "";
            }

            string day = value.Replace("/", "-");

            DateTime dt = DateTime.Now;
            if (DateTime.TryParse(day, out dt))
            {
                if (bAdd8Hour)
                {
                    return dt.AddHours(8).ToString(format);
                }
                else
                {
                    return dt.ToString(format);
                }
            }
            else
            {
                return value;
            }
        }

        //取得所屬單位統計
        private Dictionary<string, TOrg> GetOrgSummary(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string meeting_id, string in_group)
        {
            string sql = @"
                SELECT
                    *
                FROM
                    IN_MEETING_USER WITH(NOLOCK)
                WHERE
                    source_id = '{#meeting_id}'
                    AND in_group = N'{#in_group}'
                    AND ISNULL(in_paynumber, '') = ''
                ";

            sql = sql.Replace("{#meeting_id}", meeting_id)
                .Replace("{#in_group}", in_group);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item items = inn.applySQL(sql);

            Dictionary<string, TOrg> entities = new Dictionary<string, TOrg>();

            if (IsError(items, isSingle: false))
            {
                CCO.Utilities.WriteDebug(strMethodName, "與會者查無資料");
                return entities;
            }

            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_current_org = item.getProperty("in_current_org", "");
                string id = item.getProperty("id", "");
                string in_l1 = item.getProperty("in_l1", "");
                string in_l2 = item.getProperty("in_l2", "");
                string in_l3 = item.getProperty("in_l3", "");
                string in_index = item.getProperty("in_index", "");
                string in_sno = item.getProperty("in_sno", "");
                string in_expense = item.getProperty("in_expense", "");
                string item_key = in_l1 + "-" + in_l2 + "-" + in_l3 + "-" + in_index;

                TOrg entity = null;
                if (entities.ContainsKey(in_current_org))
                {
                    entity = entities[in_current_org];
                }
                else
                {
                    entity = new TOrg
                    {
                        UserList = new List<Item>(),
                        StaffList = new Dictionary<string, Item>(),
                        PlayerList = new Dictionary<string, Item>(),
                        ItemList = new Dictionary<string, Item>(),
                        Amount = 0
                    };
                    entities.Add(in_current_org, entity);
                }

                if (in_l1 == "隊職員")
                {
                    entity.StaffList.Add(id, item);
                }
                else
                {
                    entity.UserList.Add(item);

                    if (!entity.PlayerList.ContainsKey(in_sno))
                    {
                        entity.PlayerList.Add(in_sno, item);
                    }

                    if (!entity.ItemList.ContainsKey(item_key))
                    {
                        entity.ItemList.Add(item_key, item);
                        entity.Amount += GetIntValue(in_expense);
                    }
                }
            }
            return entities;
        }

        private int GetIntValue(string value)
        {
            if (value == "")
            {
                return 0;
            }

            int result = 0;
            Int32.TryParse(value, out result);
            return result;
        }

        private class TOrg
        {
            public List<Item> UserList { get; set; }
            public Dictionary<string, Item> StaffList { get; set; }
            public Dictionary<string, Item> PlayerList { get; set; }
            public Dictionary<string, Item> ItemList { get; set; }
            public int Amount { get; set; }
        }

        private string GetProjectCode(string value, int money)
        {
            string result = "";

            //區分繳費模式(A>繳款人 B>委託單位)
            switch (value)
            {
                case "A":
                    if (money <= 20000)
                    {
                        result = "6NR";
                    }
                    else if (money > 20000 && money <= 40000)
                    {
                        result = "6NS";
                    }
                    else if (money > 40000 && money <= 60000)
                    {
                        result = "6BD";
                    }
                    break;
                case "B":
                    if (money <= 20000)
                    {
                        result = "68Q";
                    }
                    else if (money > 20000 && money <= 40000)
                    {
                        result = "68R";
                    }
                    else if (money > 40000 && money <= 60000)
                    {
                        result = "6BF";
                    }
                    break;
            }

            return result;
        }

        //產生條碼**********************************
        int ichecksum1 = 0, ichecksum2 = 0;

        //判斷是否為英文或數字
        public bool IsNumberOREng(string str)
        {
            System.Text.RegularExpressions.Regex reg1 = new System.Text.RegularExpressions.Regex(@"^[A-Za-z0-9$+-.% ]+$");
            return reg1.IsMatch(str);

        }

        //判斷是否為數字
        public bool IsPositive_Number(string str)
        {
            System.Text.RegularExpressions.Regex reg1 = new System.Text.RegularExpressions.Regex(@"^[0-9]*[1-9][0-9]*$");
            return reg1.IsMatch(str);
        }

        //產生第一段條碼(代收期限,代收項目)
        public string BarCode1_OutPut(DateTime date, String num)
        {
            //確認代收項目
            bool bIsMatch;
            bIsMatch = IsNumberOREng(num.ToString());
            if (bIsMatch == false)
            {
                throw new System.ArgumentException("代收項目編號含有未定義之特殊字元。");
            }
            else
            {
                if (num.ToString().Length != 3)
                {
                    throw new System.ArgumentException("代收項目編號長度應為3。");
                }
            }
            if (date < DateTime.Today)
            {
                // throw new System.ArgumentException("代收期限不應小於今日。");
            }

            string strDate = string.Format("{0}{1}{2}", (date.Year - 1911).ToString().Substring(1, 2), date.Month.ToString("00"), date.Day.ToString("00"));
            return strDate + num;
        }

        //產生第二段條碼(業者自訂)
        public string BarCode2_OutPut(String num, String custom)
        {
            //確認業主自訂編號
            bool bIsMatch = false;
            bIsMatch = IsNumberOREng(custom.ToString());
            if (bIsMatch == false)
            {
                throw new System.ArgumentException("業主自訂編號含有未定義之特殊字元。");
            }
            else
            {
                if (custom.ToString().Length > 10)
                {
                    throw new System.ArgumentException("業主自訂編號超出字數限制。");
                }
            }

            string strCustom = custom.PadRight(10, '0');
            return "00" + num + strCustom;
        }

        //產生第三段條碼(應繳日,金額,第一碼,第二碼)
        public string BarCode3_OutPut(DateTime date, string strpay, String barcode1, String barcode2)
        {
            //確認金額
            bool bIsMatch = false;
            bIsMatch = IsPositive_Number(strpay);
            if (bIsMatch == false)
            {
                throw new System.ArgumentException("金額應為正整數。");
            }
            else
            {
                if (strpay.Length > 9)
                {
                    throw new System.ArgumentException("金額超過位數限制。");
                }
            }
            int pay = int.Parse(strpay);

            char[] cBarcode1, cBarcode2;
            char[] cDate, CMoney;
            string strCheckSum1, strCheckSum2;

            cBarcode1 = barcode1.ToUpper().ToCharArray(0, barcode1.Length);
            CharLoop(cBarcode1);
            cBarcode2 = barcode2.ToUpper().ToCharArray(0, barcode2.Length);
            CharLoop(cBarcode2);
            cDate = date.ToString("MMdd").ToCharArray(0, date.ToString("MMdd").Length);
            CharLoop(cDate);
            CMoney = pay.ToString().PadLeft(9, '0').ToCharArray(0, 9);
            CharLoop(CMoney);

            switch (ichecksum1)
            {
                case 0:
                    strCheckSum1 = "A";
                    break;
                case 10:
                    strCheckSum1 = "B";
                    break;
                default:
                    strCheckSum1 = ichecksum1.ToString();
                    break;
            }

            switch (ichecksum2)
            {
                case 0:
                    strCheckSum2 = "X";
                    break;
                case 10:
                    strCheckSum2 = "Y";
                    break;
                default:
                    strCheckSum2 = ichecksum2.ToString();
                    break;
            }
            ichecksum1 = 0;
            ichecksum2 = 0;
            return date.ToString("MMdd") + strCheckSum1 + strCheckSum2 + pay.ToString().PadLeft(9, '0');
        }

        private void CharLoop(char[] value)
        {
            for (int iCount = 0; iCount < value.Length; iCount++)
            {
                //偶數字串
                if (iCount % 2 > 0)
                {
                    ichecksum2 = GetCheckSum(value[iCount], ichecksum2);
                }
                //奇數字串
                else
                {
                    ichecksum1 = GetCheckSum(value[iCount], ichecksum1);
                }
            }
        }

        private int GetCheckSum(char value, int checksum)
        {
            if (Char.IsNumber(value))
            {
                //是數字
                return (checksum + ((int)value - 48)) % 11;
            }
            else
            {
                //是英文
                switch (value)
                {
                    case 'A':
                        return (checksum + 1) % 11;
                    case 'B':
                        return (checksum + 2) % 11;
                    case 'C':
                        return (checksum + 3) % 11;
                    case 'D':
                        return (checksum + 4) % 11;
                    case 'E':
                        return (checksum + 5) % 11;
                    case 'F':
                        return (checksum + 6) % 11;
                    case 'G':
                        return (checksum + 7) % 11;
                    case 'H':
                        return (checksum + 8) % 11;
                    case 'I':
                        return (checksum + 9) % 11;
                    case 'J':
                        return (checksum + 1) % 11;
                    case 'K':
                        return (checksum + 2) % 11;
                    case 'L':
                        return (checksum + 3) % 11;
                    case 'M':
                        return (checksum + 4) % 11;
                    case 'N':
                        return (checksum + 5) % 11;
                    case 'O':
                        return (checksum + 6) % 11;
                    case 'P':
                        return (checksum + 7) % 11;
                    case 'Q':
                        return (checksum + 8) % 11;
                    case 'R':
                        return (checksum + 9) % 11;
                    case 'S':
                        return (checksum + 2) % 11;
                    case 'T':
                        return (checksum + 3) % 11;
                    case 'U':
                        return (checksum + 4) % 11;
                    case 'V':
                        return (checksum + 5) % 11;
                    case 'W':
                        return (checksum + 6) % 11;
                    case 'X':
                        return (checksum + 7) % 11;
                    case 'Y':
                        return (checksum + 8) % 11;
                    case 'Z':
                        return (checksum + 9) % 11;
                    case '+':
                        return (checksum + 1) % 11;
                    case '%':
                        return (checksum + 2) % 11;
                    case '-':
                        return (checksum + 6) % 11;
                    case '.':
                        return (checksum + 7) % 11;
                    case ' ':
                        return (checksum + 8) % 11;
                    case '$':
                        return (checksum + 9) % 11;
                    case '/':
                        return (checksum + 0) % 11;
                    default:
                        return (checksum + 0) % 11;
                }
                return (checksum + ((((int)value - 65) % 9) + 1) % 11);
            }
        }

        private bool IsError(Item item, bool isSingle = true)
        {
            if (item.isError() || item.getResult() == "")
            {
                return true;
            }
            else if (isSingle && item.getItemCount() != 1)
            {
                return true;
            }
            else if (item.getItemCount() <= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }
}