﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;
using System.Net;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class In_Payment_DetailList : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的:列出繳費單的詳細資訊(選手 組別 金額)
                日誌: 
                    - 2022-02-09: 團體計費 (lina)
                    - 2021-11-24: 調整 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]In_Payment_DetailList";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "input dom: " + itmR.dom.InnerXml);

            string aml = "";
            string sql = "";
            string strError = "";
            string strUserId = inn.getUserID();

            //取得賽事ID
            string meeting_id = this.getProperty("meeting_id", "");
            //取得前台給的繳費單號
            string item_number = this.getProperty("item_number", "");
            //模式
            string mode = this.getProperty("mode", "");

            //取得繳費單(依據[繳費單號],[賽事ID])
            aml = @"<AML>
    	        <Item type='In_Meeting_pay' action='get'>
                    <item_number>#item_number</item_number>
    		        </Item>
    	        </AML>";
            aml = aml.Replace("#item_number", item_number);

            Item itmMeetingPay = inn.applyAML(aml);
            if (itmMeetingPay.isError() || itmMeetingPay.getItemCount() != 1)
            {
                itmR.setProperty("item_number", item_number + " 查無資料");
                return itmR;
            }

            string in_meeting = itmMeetingPay.getProperty("in_meeting", "");
            string in_cla_meeting = itmMeetingPay.getProperty("in_cla_meeting", "");
            string muName = "IN_MEETING_USER";
            string mtProperty = "in_meeting";
            if (in_cla_meeting != "")
            {
                muName = "IN_CLA_MEETING_USER";
                mtProperty = "in_cla_meeting";
            }

            aml = "<AML>" +
                "<Item type='" + mtProperty + "' action='get' id='" + meeting_id + "'>" +
                "</Item></AML>";
            Item itmMeeting = inn.applyAML(aml);

            string meeting_name = itmMeeting.getProperty("in_title", "");
            string in_member_type = itmMeeting.getProperty("in_member_type", "");
            bool is_team_bill = in_member_type == "團隊計費";

            //取得登入者資訊
            sql = "Select *";
            sql += " from In_Resume WITH(NOLOCK)";
            sql += " where in_user_id='" + strUserId + "'";
            Item In_Resume = inn.applySQL(sql);

            string pay_id = itmMeetingPay.getProperty("id", "");
            string in_pay_amount_exp = GetDollarValue(itmMeetingPay.getProperty("in_pay_amount_exp", "0"));
            string in_pay_amount_real = GetDollarValue(itmMeetingPay.getProperty("in_pay_amount_real", "0"));

            string in_pay_date_exp = GetDateTimeValue(itmMeetingPay.getProperty("in_pay_date_exp", ""), "yyyy-MM-dd");
            string in_pay_date_real = GetDateTimeValue(itmMeetingPay.getProperty("in_pay_date_real", ""), "yyyy-MM-dd");

            itmMeetingPay.setProperty("inn_pay_amount_exp", in_pay_amount_exp);
            itmMeetingPay.setProperty("inn_pay_amount_real", in_pay_amount_real);
            itmMeetingPay.setProperty("inn_pay_date_exp", in_pay_date_exp);
            itmMeetingPay.setProperty("inn_pay_date_real", in_pay_date_real);

            itmMeetingPay.setProperty("inn_collection_date", GetDateTimeValue(itmMeetingPay.getProperty("in_collection_date", ""), "yyyy-MM-dd"));
            itmMeetingPay.setProperty("inn_credit_date", GetDateTimeValue(itmMeetingPay.getProperty("in_credit_date", ""), "yyyy-MM-dd"));
            string agency = itmMeetingPay.getProperty("in_collection_agency", "");
            string pay_bool = itmMeetingPay.getProperty("pay_bool", "");
            switch (agency.Trim())
            {
                case "7111111":
                    agency = "7-11";
                    break;
                case "TFM":
                    agency = "全家";
                    break;
                case "HILIFE":
                    agency = "萊爾富";
                    break;
                case "OKCVS":
                    agency = "OK";
                    break;
                case "":
                    if (pay_bool == "未繳費")
                    {
                        agency = "未繳費";
                    }
                    else
                    {
                        agency = "匯款/轉帳";
                    }

                    break;
            }
            itmMeetingPay.setProperty("inn_collection_agency", agency);


            itmR.addRelationship(itmMeetingPay);

            itmR.setProperty("pay_id", pay_id);

            //取得繳費單下的成員
            sql = @"
                SELECT
                    t2.in_current_org
                    , t1.in_section_name
                    , t1.in_index
                    , t1.in_name
                    , t1.in_sno
                    , t1.in_ans_l3
                    , t1.in_pay_amount AS 'in_expense'
                    , t1.in_cancel_status
                    , t1.in_refund_amount
                    , t1.in_new_levels
                    , t2.pay_bool
                    , t3.id AS 'muid'
                    , t3.in_sno_display
                FROM
                    IN_MEETING_NEWS t1 WITH(NOLOCK)
                INNER JOIN
                    IN_MEETING_PAY t2 WITH(NOLOCK)
                    ON t2.id = t1.source_id
                LEFT OUTER JOIN
                    {#muName} t3 WITH(NOLOCK)
                    ON t3.id = t1.in_muid
                WHERE
                    t2.id = '{#pay_id}'
                ORDER BY
                    t1.in_section_name
                    , t1.in_index
                ";

            sql = sql.Replace("{#muName}", muName)
                .Replace("{#pay_id}", pay_id);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item itmMeetingNews = inn.applySQL(sql);

            int count = itmMeetingNews.getItemCount();

            string in_section_name = "";
            string in_index = "";
            for (int i = 0; i < count; i++)
            {
                Item itmMeetingNew = itmMeetingNews.getItemByIndex(i);
                string muid = itmMeetingNew.getProperty("muid", "");
                string in_sno = itmMeetingNew.getProperty("in_sno", "");
                string in_sno_display = itmMeetingNew.getProperty("in_sno_display", "");
                string in_expense = GetDollarValue(itmMeetingNew.getProperty("in_expense", ""));
                string in_refund = GetDollarValue(itmMeetingNew.getProperty("in_refund_amount", ""));
                string in_cancel_status = itmMeetingNew.getProperty("in_cancel_status", "");
                string in_new_levels = itmMeetingNew.getProperty("in_new_levels", "");

                //狀態為[通過]的對外顯示[已退費]
                if (in_cancel_status == "通過")
                {
                    in_cancel_status = "已退費";
                }
                else if (in_cancel_status == "換組申請通過")
                {
                    in_cancel_status += "：<span style='color: red'>" + in_new_levels.Replace("@", "-") + "</span>";
                }



                itmMeetingNew.setType("detail_list");
                if (muid == "")
                {
                    itmMeetingNew.setProperty("registry_class", "danger");
                    itmMeetingNew.setProperty("registry_bool", "X");
                    itmMeetingNew.setProperty("in_sno_display", GetInSnoDisplay(in_sno));
                }
                else
                {
                    itmMeetingNew.setProperty("registry_class", "");
                    itmMeetingNew.setProperty("registry_bool", "O");
                }
                itmMeetingNew.setProperty("inn_no", (i + 1).ToString());
                if (in_section_name == itmMeetingNew.getProperty("in_section_name", "") && in_index == itmMeetingNew.getProperty("in_index", ""))
                {
                    itmMeetingNew.setProperty("in_expense", "");
                    itmMeetingNew.setProperty("in_refund", "");
                }
                else
                {
                    itmMeetingNew.setProperty("in_expense", in_expense);
                    itmMeetingNew.setProperty("in_refund", in_refund);
                }


                itmMeetingNew.setProperty("in_cancel_status", in_cancel_status);
                itmMeetingNew.setProperty("id", muid);

                if (is_team_bill)
                {
                    itmMeetingNew.setProperty("in_expense", in_member_type);
                }

                itmR.addRelationship(itmMeetingNew);

                in_section_name = itmMeetingNew.getProperty("in_section_name", "");
                in_index = itmMeetingNew.getProperty("in_index", "");

            }

            itmR.setProperty("meeting_name", meeting_name);//賽事名稱
            itmR.setProperty("id", meeting_id);//賽事ID
            itmR.setProperty("pay_number", item_number);//繳費單號

            if (mode == "apply")
            {
                AppendPayGroup(CCO, strMethodName, inn, itmR);
            }
            else
            {
                itmR.setProperty("group_hide", "item_show_0");
            }

            bool isAdmin = false;

            sql = @"
                SELECT
                    t3.id
                    , t3.name
                    , t1.in_number
                FROM
                    [IDENTITY] t1 WITH(NOLOCK)
                INNER JOIN
                    [MEMBER] t2 WITH(NOLOCK)
                    ON t2.related_id = t1.id
                INNER JOIN
                    [IDENTITY] t3 WITH(NOLOCK)
                    ON t3.id = t2.source_id
                WHERE
                    t1.in_user = '{#strUserId}'
                    AND t3.name in (N'Administrator', N'MeetingAdmin', N'ACT_Committee','ACT_ASC_Accounting')
            ";

            sql = sql.Replace("{#strUserId}", strUserId);
            Item itmIdentities = inn.applySQL(sql);

            if (!itmIdentities.isError() && itmIdentities.getItemCount() > 0)
            {
                for (int i = 0; i < itmIdentities.getItemCount(); i++)
                {
                    Item itmIdentity = itmIdentities.getItemByIndex(i);
                    string idt_name = itmIdentity.getProperty("name", "").ToLower();
                    switch (idt_name)
                    {
                        case "administrator":
                        case "act_asc_accounting":
                        case "meetingadmin":
                            isAdmin = true;
                            break;

                        default:
                            break;
                    }
                }
            }

            string review_tab = "item_show_0";

            review_tab = isAdmin ? "item_show_1" : "item_show_0";

            itmR.setProperty("review_tab", review_tab);

            return itmR;

        }

        private void AppendPayGroup(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            var list = MapPayGroup(CCO, strMethodName, inn, itmReturn);

            for (int i = 0; i < list.Count; i++)
            {
                var entity = list[i];
                Item item = inn.newItem();
                item.setType("inn_pay_group");
                item.setProperty("inn_no", entity.no);
                item.setProperty("in_section_name", entity.in_section_name);
                item.setProperty("in_index", entity.in_index);
                item.setProperty("in_expense", entity.in_pay_amount);
                item.setProperty("in_creator", entity.in_creator);
                item.setProperty("in_cancel_time", entity.in_cancel_time);
                item.setProperty("in_cancel_status", entity.in_cancel_status);
                item.setProperty("in_verify_memo", entity.in_verify_memo);
                item.setProperty("in_names", GetNames(entity));
                itmReturn.addRelationship(item);
            }
        }

        private string GetNames(TPay entity)
        {
            return string.Join(",", entity.Details.Select(x => x.in_name));
        }

        private List<TPay> MapPayGroup(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            List<TPay> result = new List<TPay>();

            string pay_id = itmReturn.getProperty("pay_id", "");

            string sql = @"
            SELECT 
	            * 
            FROM 
	            IN_MEETING_NEWS WITH(NOLOCK)
            WHERE
	            source_id = '{#pay_id}'
            ORDER BY
	            in_section_name
	            , in_index
            ";

            sql = sql.Replace("{#pay_id}", pay_id);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item items = inn.applySQL(sql);

            if (items.isError() || items.getItemCount() <= 0)
            {
                return result;
            }

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_section_name = item.getProperty("in_section_name", "");
                string in_index = item.getProperty("in_index", "");
                string key = in_section_name + "_" + in_index;

                TPay obj = result.Find(x => x.key == key);

                if (obj == null)
                {
                    obj = new TPay
                    {
                        key = key,
                        no = (result.Count + 1).ToString(),
                        in_section_name = in_section_name,
                        in_index = in_index,
                        in_pay_amount = item.getProperty("in_pay_amount", "0"),
                        in_creator = item.getProperty("in_creator", ""),
                        in_cancel_time = item.getProperty("in_cancel_time", ""),
                        in_cancel_status = item.getProperty("in_cancel_status", ""),
                        in_verify_memo = item.getProperty("in_verify_memo", ""),
                        Details = new List<TDetail>(),
                        Value = item,
                    };
                    result.Add(obj);
                }

                TDetail detail = new TDetail
                {
                    in_name = item.getProperty("in_name", ""),
                    Value = item,
                };

                obj.Details.Add(detail);
            }
            return result;
        }

        private class TPay
        {
            public string key { get; set; }
            public string no { get; set; }
            public string in_section_name { get; set; }
            public string in_index { get; set; }
            public string in_pay_amount { get; set; }
            public string in_creator { get; set; }
            public string in_cancel_time { get; set; }
            public string in_cancel_status { get; set; }
            public string in_verify_memo { get; set; }
            public List<TDetail> Details { get; set; }
            public Item Value { get; set; }
        }

        private class TDetail
        {
            public string in_name { get; set; }
            public Item Value { get; set; }
        }


        //取得身分證號隱碼
        private string GetInSnoDisplay(string in_sno)
        {
            if (in_sno.Length == 10)
            {
                //10碼(身分證處理)
                string in_sno_1 = in_sno.Substring(0, 2);
                string in_sno_2 = in_sno.Substring(6, 4);

                //A1****6789
                return in_sno_1 + " * ***" + in_sno_2;
            }
            else if (in_sno.Length == 18)
            {
                //18碼(身分證處理-大陸)
                string in_sno_1 = in_sno.Substring(0, 6);
                string in_sno_2 = in_sno.Substring(14, 3);

                //110102********888X
                return in_sno_1 + "********" + in_sno_2 + "X";
            }
            else
            {
                return in_sno;
            }
        }

        private string GetDateTimeValue(string value, string format)
        {
            if (value == "")
            {
                return "";
            }

            DateTime dt = DateTime.Now;
            if (DateTime.TryParse(value, out dt))
            {
                return dt.ToString(format);
            }
            else
            {
                return value;
            }
        }

        private string GetDollarValue(string value)
        {
            if (value == "" || value == "0") return "0";

            int amount = 0;
            if (Int32.TryParse(value, out amount))
            {
                return amount.ToString("####,####");
            }
            else
            {
                return value;
            }
        }
    }
}