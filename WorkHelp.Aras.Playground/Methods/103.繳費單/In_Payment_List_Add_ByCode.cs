﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class In_Payment_List_Add_ByCode : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
            目的: 
                - 產生繳費條碼(指定團體會員編號)
            規格:
                - 團體會員編號作為尾綴
                - 繳費期限為當年年底
            日期: 
                - 2021-03-19 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]" + "In_Payment_List_Add_ByCode";

            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            string aml = "";
            string sql = "";
            string strError = "";
            string strUserId = inn.getUserID();
            string strIdentityId = inn.getUserAliases();

            _InnH.AddLog(strMethodName, "MethodSteps");

            Item itmR = this;
            // CCO.Utilities.WriteDebug(strMethodName, "dom: " + this.dom.InnerXml);

            //指定賽事 ID
            string meeting_id = "81913AC62F444DD0870D5AF39110DCC1";

            //產生條碼
            string codes = "10"; //備用碼(1)+運動代碼(0)
            string Payment_mode = "A"; //狀態(A>繳款人 B>委託單位)
            string index_number = ""; //業主自訂編號
            string barcode2 = ""; //條碼2
            string barcode2_1 = ""; //臨櫃繳費虛擬帳號
            string Project_code = ""; //項目代碼
            string in_imf_date_e = ""; //最後報名日期
            string B1 = ""; //條碼1
            string B2 = ""; //條碼2
            string B3 = ""; //條碼3
            string blank = " ";

            //應繳日期&最後繳費日期為[今日+7天](尚未確定範圍)
            DateTime CurrentTime = System.DateTime.Today;
            int year = CurrentTime.Year;
            string format = "yyyy-MM-ddTHH:mm:ss";
            string Add_time = DateTime.Now.ToString(format); //建單時間
            string Due_date = Due_date = (new DateTime(year, 12, 31)).ToString(format); //應繳日期
            string Last_payment_date = Due_date;
            DateTime in_pay_date_exp1 = Convert.ToDateTime(Last_payment_date.Split('T')[0]);
            DateTime in_pay_date_exp = Convert.ToDateTime(Due_date.Split('T')[0]);
            //*********************************************************************

            //賽事處理
            aml = "<AML><Item type='In_Meeting' action='get'><id>#meeting_id</id></Item></AML>";
            aml = aml.Replace("#meeting_id", meeting_id);
            Item itmMeeting = inn.applyAML(aml);

            string in_meeting_code = itmMeeting.getProperty("item_number", ""); //賽事編號
            string in_receice_org_code = itmMeeting.getProperty("in_receice_org_code", ""); //收款單位編號
            string in_meeting_type = itmMeeting.getProperty("in_meeting_type", ""); //活動類型
            string in_annual = itmMeeting.getProperty("in_annual", ""); //年度(民國)

            if (in_receice_org_code == "")
            {
                throw new Exception("Meeting 收款單位編號不可為空白");
            }

            //當收款單位編號長度為 6 碼
            if (in_receice_org_code.Length == 6)
            {
                codes = "";//預設為 10
            }

            //*********************************************************************

            //與會者
            Item itmMUsers = inn.applySQL("SELECT * FROM AA_In_MUser_20210319 WITH(NOLOCK)");

            int count = itmMUsers.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item itmMUser = itmMUsers.getItemByIndex(i);
                string in_stuff_b1 = itmMUser.getProperty("in_stuff_b1", "");
                string in_group = itmMUser.getProperty("in_group", "");
                string in_current_org = itmMUser.getProperty("in_current_org", "");
                string in_name = itmMUser.getProperty("in_name", "");
                string in_sno = itmMUser.getProperty("in_sno", "");
                string in_l1 = itmMUser.getProperty("in_l1", "");
                string in_l2 = itmMUser.getProperty("in_l2", "");
                string in_l3 = itmMUser.getProperty("in_l3", "");
                string in_index = itmMUser.getProperty("in_index", "");
                string in_expense = itmMUser.getProperty("in_expense", "0"); 
                string in_section_name = itmMUser.getProperty("in_section_name", "0"); 
                string in_gameunit = itmMUser.getProperty("in_gameunit", "0"); 
                string in_creator = itmMUser.getProperty("in_creator", "lwu001");
                string in_creator_sno = itmMUser.getProperty("in_creator_sno", "lwu001");

                int amount = GetIntValue(in_expense);
              
                //費用
                string expenses = in_expense.ToString();
                //隊職員數
                string stuffs = "1";
                //選手人數
                string players = "1";
                //參賽項目總計
                string items = "1";

                //區分繳費模式(A>繳款人 B>委託單位)
                string project_code = GetProjectCode(Payment_mode, amount);

                //作廢
                string b2_1 = in_meeting_code.Substring(in_meeting_code.Length - 4);

                //業主自訂編號: [賽事/講習序號(後3碼)] + 自訂流水號(4碼)(預設為0001)
                //[運動賽事序號(後3碼)]
                string b2_2 = in_meeting_code.Substring(in_meeting_code.Length - 3);

                string index_number_prefix = b2_2.TrimStart('0');

                barcode2 = b2_1 + "0" + in_stuff_b1;
                barcode2_1 = b2_2 + "0" + in_stuff_b1;

                //這是超商繳費
                //第一碼(最後收費日期(DateTime),項目代碼(String))
                B1 = BarCode1_OutPut(in_pay_date_exp1, project_code);
                //第二碼(賽事編號(String),業者自訂編號(String))

                B2 = in_receice_org_code + barcode2_1;

                string checkhum = inn.applyMethod("In_GetRank_CheckSum", "<account>" + B2 + "</account><pay>" + expenses + "</pay>").getResult();

                B2 = B2 + checkhum;

                //B2 = BarCode2_OutPut(in_receice_org_code,barcode2);
                //第三碼(應繳日期(DateTime),費用(String),一碼(String),二碼(String))
                B3 = BarCode3_OutPut(in_pay_date_exp, expenses, B1, B2);

                //產生繳費單
                Item SingleNumber = inn.newItem("In_Meeting_pay", "add");
                SingleNumber.setProperty("owned_by_id", strIdentityId);//建單者
                SingleNumber.setProperty("in_pay_amount_exp", expenses);//應繳金額
                SingleNumber.setProperty("in_pay_date_exp", Due_date.Split('T')[0]);//應繳日期
                SingleNumber.setProperty("in_pay_date_exp1", Last_payment_date.Split('T')[0]);//最後收費日期
                SingleNumber.setProperty("in_group", in_group);//所屬群組
                SingleNumber.setProperty("in_creator", in_creator);//協助報名者
                SingleNumber.setProperty("in_creator_sno", in_creator_sno);//協助報名者身分證字號
                SingleNumber.setProperty("in_current_org", in_current_org);//所屬單位
                SingleNumber.setProperty("in_real_stuff", stuffs);//隊職員數
                SingleNumber.setProperty("in_real_player", players);//選手人數
                SingleNumber.setProperty("in_real_items", items);//項目總計
                SingleNumber.setProperty("in_code_1", B1);//條碼1
                SingleNumber.setProperty("in_code_2", "00" + B2);//條碼2
                //SingleNumber.setProperty("in_code_2", B2.PadLeft(16,'0'));//條碼2
                SingleNumber.setProperty("in_code_3", B3);//條碼3
                SingleNumber.setProperty("pay_bool", "未繳費");//繳費狀態
                SingleNumber.setProperty("in_add_time", Add_time);//建單時間
                //SingleNumber.setProperty("index_number", barcode2);//業主自訂編號
                SingleNumber.setProperty("index_number", barcode2_1);//業主自訂編號
                //因著有可能[發票抬頭],[統一編號]為沒輸入的情況 不Trim(',')而是+1 之後開始取值
                SingleNumber.setProperty("invoice_up", in_group);//發票抬頭
                SingleNumber.setProperty("uniform_numbers", "");//統一編號
                SingleNumber.setProperty("in_meeting", meeting_id);
                Item itmNewPayment = SingleNumber.apply();

                //取出該賽事的關聯繳費
                aml = "<AML>" +
                    "<Item type='In_Meeting_pay' action='get'>" +
                    "<in_meeting>" + meeting_id + "</in_meeting>" +
                    "<id>" + itmNewPayment.getProperty("id", "") + "</id>" +
                    "</Item></AML>";
                Item SingleNumbers = inn.applyAML(aml);


                string in_regdate = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");

                //新增到繳費資訊下
                Item in_meeting_news = inn.newItem("In_Meeting_news", "add");
                in_meeting_news.setProperty("in_ans_l3", in_gameunit);//組別彙整結果
                in_meeting_news.setProperty("in_pay_amount", in_expense);//應繳金額
                in_meeting_news.setProperty("in_name", in_name);//姓名
                in_meeting_news.setProperty("in_sno", in_sno);//身分證
                in_meeting_news.setProperty("in_creator", in_creator);//協助報名者
                in_meeting_news.setProperty("in_creator_sno", in_creator_sno);//協助報名者帳號
                in_meeting_news.setProperty("in_regdate", in_regdate);//報名日期
                in_meeting_news.setProperty("in_l1", in_l1);//第一層
                in_meeting_news.setProperty("in_l2", in_l2);//第二層
                in_meeting_news.setProperty("in_l3", in_l3);//第三層
                in_meeting_news.setProperty("in_index", in_index);//序號
                in_meeting_news.setProperty("in_section_name", in_section_name);//組名

                SingleNumbers.addRelationship(in_meeting_news);

                SingleNumbers.apply();
            }

            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

            return itmR;
        }

        private string GetProjectCode(string value, int money)
        {
            string result = "";

            //區分繳費模式(A>繳款人 B>委託單位)
            switch (value)
            {
                case "A":
                    if (money <= 20000)
                    {
                        result = "6NR";
                    }
                    else if (money > 20000 && money <= 40000)
                    {
                        result = "6NS";
                    }
                    else if (money > 40000 && money <= 60000)
                    {
                        result = "6BD";
                    }
                    break;
                case "B":
                    if (money <= 20000)
                    {
                        result = "68Q";
                    }
                    else if (money > 20000 && money <= 40000)
                    {
                        result = "68R";
                    }
                    else if (money > 40000 && money <= 60000)
                    {
                        result = "6BF";
                    }
                    break;
            }

            return result;
        }

        //產生條碼**********************************
        int ichecksum1 = 0, ichecksum2 = 0;

        //判斷是否為英文或數字
        public bool IsNumberOREng(string str)
        {
            System.Text.RegularExpressions.Regex reg1 = new System.Text.RegularExpressions.Regex(@"^[A-Za-z0-9$+-.% ]+$");
            return reg1.IsMatch(str);

        }

        //判斷是否為數字
        public bool IsPositive_Number(string str)
        {
            System.Text.RegularExpressions.Regex reg1 = new System.Text.RegularExpressions.Regex(@"^[0-9]*[1-9][0-9]*$");
            return reg1.IsMatch(str);
        }

        //產生第一段條碼(代收期限,代收項目)
        public string BarCode1_OutPut(DateTime date, String num)
        {
            //確認代收項目
            bool bIsMatch;
            bIsMatch = IsNumberOREng(num.ToString());
            if (bIsMatch == false)
            {
                throw new System.ArgumentException("代收項目編號含有未定義之特殊字元。");
            }
            else
            {
                if (num.ToString().Length != 3)
                {
                    throw new System.ArgumentException("代收項目編號長度應為3。");
                }
            }
            if (date < DateTime.Today)
            {
                // throw new System.ArgumentException("代收期限不應小於今日。");
            }

            string strDate = string.Format("{0}{1}{2}", (date.Year - 1911).ToString().Substring(1, 2), date.Month.ToString("00"), date.Day.ToString("00"));
            return strDate + num;
        }

        //產生第二段條碼(業者自訂)
        public string BarCode2_OutPut(String num, String custom)
        {
            //確認業主自訂編號
            bool bIsMatch = false;
            bIsMatch = IsNumberOREng(custom.ToString());
            if (bIsMatch == false)
            {
                throw new System.ArgumentException("業主自訂編號含有未定義之特殊字元。");
            }
            else
            {
                if (custom.ToString().Length > 10)
                {
                    throw new System.ArgumentException("業主自訂編號超出字數限制。");
                }
            }

            string strCustom = custom.PadRight(10, '0');
            return "00" + num + strCustom;
        }

        //產生第三段條碼(應繳日,金額,第一碼,第二碼)
        public string BarCode3_OutPut(DateTime date, string strpay, String barcode1, String barcode2)
        {
            //確認金額
            bool bIsMatch = false;
            bIsMatch = IsPositive_Number(strpay);
            if (bIsMatch == false)
            {
                throw new System.ArgumentException("金額應為正整數。");
            }
            else
            {
                if (strpay.Length > 9)
                {
                    throw new System.ArgumentException("金額超過位數限制。");
                }
            }
            int pay = int.Parse(strpay);

            char[] cBarcode1, cBarcode2;
            char[] cDate, CMoney;
            string strCheckSum1, strCheckSum2;

            cBarcode1 = barcode1.ToUpper().ToCharArray(0, barcode1.Length);
            CharLoop(cBarcode1);
            cBarcode2 = barcode2.ToUpper().ToCharArray(0, barcode2.Length);
            CharLoop(cBarcode2);
            cDate = date.ToString("MMdd").ToCharArray(0, date.ToString("MMdd").Length);
            CharLoop(cDate);
            CMoney = pay.ToString().PadLeft(9, '0').ToCharArray(0, 9);
            CharLoop(CMoney);

            switch (ichecksum1)
            {
                case 0:
                    strCheckSum1 = "A";
                    break;
                case 10:
                    strCheckSum1 = "B";
                    break;
                default:
                    strCheckSum1 = ichecksum1.ToString();
                    break;
            }

            switch (ichecksum2)
            {
                case 0:
                    strCheckSum2 = "X";
                    break;
                case 10:
                    strCheckSum2 = "Y";
                    break;
                default:
                    strCheckSum2 = ichecksum2.ToString();
                    break;
            }
            ichecksum1 = 0;
            ichecksum2 = 0;
            return date.ToString("MMdd") + strCheckSum1 + strCheckSum2 + pay.ToString().PadLeft(9, '0');
        }

        private void CharLoop(char[] value)
        {
            for (int iCount = 0; iCount < value.Length; iCount++)
            {
                //偶數字串
                if (iCount % 2 > 0)
                {
                    ichecksum2 = GetCheckSum(value[iCount], ichecksum2);
                }
                //奇數字串
                else
                {
                    ichecksum1 = GetCheckSum(value[iCount], ichecksum1);
                }
            }
        }

        private int GetCheckSum(char value, int checksum)
        {
            if (Char.IsNumber(value))
            {
                //是數字
                return (checksum + ((int)value - 48)) % 11;
            }
            else
            {
                //是英文
                switch (value)
                {
                    case 'A':
                        return (checksum + 1) % 11;
                    case 'B':
                        return (checksum + 2) % 11;
                    case 'C':
                        return (checksum + 3) % 11;
                    case 'D':
                        return (checksum + 4) % 11;
                    case 'E':
                        return (checksum + 5) % 11;
                    case 'F':
                        return (checksum + 6) % 11;
                    case 'G':
                        return (checksum + 7) % 11;
                    case 'H':
                        return (checksum + 8) % 11;
                    case 'I':
                        return (checksum + 9) % 11;
                    case 'J':
                        return (checksum + 1) % 11;
                    case 'K':
                        return (checksum + 2) % 11;
                    case 'L':
                        return (checksum + 3) % 11;
                    case 'M':
                        return (checksum + 4) % 11;
                    case 'N':
                        return (checksum + 5) % 11;
                    case 'O':
                        return (checksum + 6) % 11;
                    case 'P':
                        return (checksum + 7) % 11;
                    case 'Q':
                        return (checksum + 8) % 11;
                    case 'R':
                        return (checksum + 9) % 11;
                    case 'S':
                        return (checksum + 2) % 11;
                    case 'T':
                        return (checksum + 3) % 11;
                    case 'U':
                        return (checksum + 4) % 11;
                    case 'V':
                        return (checksum + 5) % 11;
                    case 'W':
                        return (checksum + 6) % 11;
                    case 'X':
                        return (checksum + 7) % 11;
                    case 'Y':
                        return (checksum + 8) % 11;
                    case 'Z':
                        return (checksum + 9) % 11;
                    case '+':
                        return (checksum + 1) % 11;
                    case '%':
                        return (checksum + 2) % 11;
                    case '-':
                        return (checksum + 6) % 11;
                    case '.':
                        return (checksum + 7) % 11;
                    case ' ':
                        return (checksum + 8) % 11;
                    case '$':
                        return (checksum + 9) % 11;
                    case '/':
                        return (checksum + 0) % 11;
                    default:
                        return (checksum + 0) % 11;
                }
                return (checksum + ((((int)value - 65) % 9) + 1) % 11);
            }
        }


        private string GetDateTimeValue(string value, string format = "yyyy-MM-dd", int hours = 0)
        {
            if (value == "") return "";

            DateTime dt = DateTime.Now;
            if (DateTime.TryParse(value.Replace("/", "-"), out dt))
            {
                return dt.AddHours(hours).ToString(format);
            }
            else
            {
                return value;
            }
        }

        private int GetIntValue(string value)
        {
            if (value == "" || value == "0") return 0;

            int result = 0;
            Int32.TryParse(value, out result);
            return result;
        }

    }
}