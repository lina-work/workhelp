﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;
using System.Net;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class In_Payment_Barcode_Print : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 繳費單列印
                日期: 
                    - 2022-02-09: 團體計費 (lina)
                    - 2021-02-19: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]In_Payment_Barcode_Print";

            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            Item itmR = this;
            string strUserId = inn.getUserID();
            //取得登入者權限
            Item itmPermit = inn.applyMethod("In_CheckIdentity", "<method>" + strMethodName + "</method><code>ALL</code>");
            bool isMeetingAdmin = itmPermit.getProperty("isMeetingAdmin", "") == "1";
            bool isCommittee = itmPermit.getProperty("isCommittee", "") == "1";
            bool isGymOwner = itmPermit.getProperty("isGymOwner", "") == "1";
            bool isGymAssistant = itmPermit.getProperty("isGymAssistant", "") == "1";

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                inn = inn,
                urlPath = itmR.getProperty("urlPath", ""),
                meeting_excel_parameter = itmR.getProperty("path", ""),
                doc_type = itmR.getProperty("doc_type", ""),
                doc_name = "",

                scene = itmR.getProperty("scene", ""),
                mode = itmR.getProperty("mode", ""),
                meeting_id = itmR.getProperty("meeting_id", ""),
                in_sno = itmR.getProperty("in_sno", ""),
            };

            cfg.mt_type = "in_meeting";
            cfg.muser_type = "in_meeting_user";
            if (cfg.mode == "cla")
            {
                cfg.mt_type = "in_cla_meeting";
                cfg.muser_type = "in_cla_meeting_user";
            }

            cfg.itmMeeting = cfg.inn.applySQL("SELECT * FROM " + cfg.mt_type + " WHERE id = '" + cfg.meeting_id + "'");

            //盲報
            bool isNoReg = false;
            if (cfg.in_sno != "")
            {
                isNoReg = true;
            }

            string sql = "";
            string sqlFilter = "";
            Item itmPayResults;

            if (!isNoReg)
            {
                //取得登入者資訊
                sql = "Select *";
                sql += " from In_Resume WITH(NOLOCK)";
                sql += " where in_user_id = '" + strUserId + "'";
                Item In_Resume = inn.applySQL(sql);

                if (isGymOwner)
                {
                    //道館主(看自己道館的人) 道館助理(看自己道館的人)
                    sqlFilter = " AND t1.in_creator_sno = N'" + In_Resume.getProperty("in_sno", "") + "'";
                }

                //取得未付款繳費單資訊
                itmPayResults = GetPayResult(cfg, sqlFilter);
            }
            else
            {
                sqlFilter = " AND (t1.in_creator_sno = N'" + cfg.in_sno + "' OR t1.item_number = N'" + itmR.getProperty("paynumbers", "") + "')";
                //取得未付款繳費單資訊
                itmPayResults = GetPayResult(cfg, sqlFilter);
            }

            if (itmPayResults.isError() || itmPayResults.getItemCount() < 0)
            {
                throw new Exception("目前無未繳費資料，無法列印");

            }

            cfg.doc_name = cfg.itmMeeting.getProperty("in_title", "") + "_繳費單";

            cfg.Export = GetExportInfo(cfg);

            Spire.Xls.Workbook workbook = new Spire.Xls.Workbook();
            workbook.LoadFromFile(cfg.Export.Source);
            Spire.Xls.Worksheet sheetTemplate = workbook.Worksheets[0];

            //附加繳費單
            AppendPaymentReport(workbook, sheetTemplate, cfg, itmPayResults, isNoReg);

            //移除樣板 sheet
            sheetTemplate.Remove();

            //cfg.doc_type = "";
            switch (cfg.doc_type)
            {
                case "PDF":
                    workbook.SaveToFile(cfg.Export.Target.Replace("xlsx", "pdf"), Spire.Xls.FileFormat.PDF);
                    itmR.setProperty("xls_name", cfg.Export.Url.Replace("xlsx", "pdf"));
                    break;

                default:
                    workbook.SaveToFile(cfg.Export.Target, Spire.Xls.ExcelVersion.Version2010);
                    itmR.setProperty("xls_name", cfg.Export.Url);
                    break;

            }

            //把身分換回來
            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

            return itmR;
        }

        private Item GetPayResult(TConfig cfg, string sqlFilter)
        {
            //如要濾掉已上傳照片需加條件in_pay_photo is null

            string sql = @"
                SELECT 
	                t1.item_number
	                , t1.in_code_1
	                , t1.in_code_2
	                , t1.in_code_3
	                , t1.in_pay_amount_exp
	                , t1.in_pay_date_exp1
	                , t1.in_creator
	                , t2.in_title
	                , t2.in_bank_name
                FROM 
	                IN_MEETING_PAY t1 WITH(NOLOCK)
                JOIN 
	                {#meeting_type} t2 WITH(NOLOCK)
	                ON t2.id = t1.{#meeting_type}
                WHERE 
	                t1.{#meeting_type} = '{#meeting_id}' 
	                AND t1.pay_bool = N'未繳費' {#sqlFilter}
                ORDER BY 
	                t1.pay_bool DESC
	                , t1.item_number
            ";

            sql = sql.Replace("{#meeting_type}", cfg.mt_type)
                    .Replace("{#meeting_id}", cfg.meeting_id)
                    .Replace("{#sqlFilter}", sqlFilter);

            return cfg.inn.applySQL(sql);
        }

        private void AppendPaymentReport(Spire.Xls.Workbook workbook, Spire.Xls.Worksheet sheetTemplate, TConfig cfg, Item itmPayResults, bool isNoReg)
        {
            Spire.Xls.Worksheet sheet = workbook.CreateEmptySheet();

            string in_member_type = cfg.itmMeeting.getProperty("in_member_type", "");
            bool is_team_bill = in_member_type == "團隊計費";

            for (int i = 0; i < itmPayResults.getItemCount(); i++)
            {
                Item itmPayResult = itmPayResults.getItemByIndex(i);

                var barcode_1 = GenerateCode39(itmPayResult.getProperty("in_code_1", ""), 250, 55);
                var barcode_2 = GenerateCode39(itmPayResult.getProperty("in_code_2", ""), 310, 55);
                var barcode_3 = GenerateCode39(itmPayResult.getProperty("in_code_3", ""), 310, 55);

                string in_title = itmPayResult.getProperty("in_title", "");
                string item_number = itmPayResult.getProperty("item_number", "");
                string payDate = GetDateTimeValue(itmPayResult.getProperty("in_pay_date_exp1", ""), "yyyy/MM/dd", 8);
                string chinese_expense = NumberToCHS(itmPayResult.getProperty("in_pay_amount_exp", ""));
                string expense = "$" + Convert.ToInt32(itmPayResult.getProperty("in_pay_amount_exp", "")).ToString("N0");
                string in_bank_name = itmPayResult.getProperty("in_bank_name", "");
                string in_creator = itmPayResult.getProperty("in_creator", "");

                string parameter_str = "&meeting_id=" + cfg.meeting_id + "&paynumbers =" + item_number + "&player_group =" + in_creator;

                var qrcode = GenerateQrCode(cfg.urlPath + parameter_str, 110, 110);

                if (i % 2 == 1)
                {
                    Spire.Xls.CellRange range1;
                    range1 = sheet.Range[1, 1, 21, sheet.LastColumn];

                    sheet.Copy(range1, sheet.Range[23, 1]);
                    for (int k = 1; k <= 21; k++)
                    {
                        for (int j = 1; j <= 11; j++)
                        {
                            sheet.Range[k + 22, j].Style = sheet.Range[k, j].Style;
                        }
                    }

                    var imgTitle = sheet.Pictures.Add(23, 3, sheet.Pictures[0].Picture);
                    imgTitle.Rotation = -90;
                    imgTitle.Width = 65;
                    imgTitle.Height = 50;
                    imgTitle.LeftColumnOffset = 300;
                    sheet.Range["K25"].Text = item_number;
                    sheet.Range["B26"].Text = "活動名稱：" + in_title;
                    sheet.Range["C36"].Text = in_bank_name;
                    sheet.Range["C37"].Text = chinese_expense;
                    sheet.Range["F37"].Text = expense;
                    sheet.Range["D42"].Text = payDate;

                    //繳費明細
                    Item itmPayDetailResults = GetPayDetailResult(cfg, item_number);
                    for (int j = 0; j < 7; j++)
                    {
                        sheet.Range["B" + (28 + j)].Text = "";
                        sheet.Range["F" + (28 + j)].Text = "";
                    }

                    for (int j = 0; j < itmPayDetailResults.getItemCount(); j++)
                    {
                        Item itmPayDetailResult = itmPayDetailResults.getItemByIndex(j);
                        var in_l1 = itmPayDetailResult.getProperty("in_l1", "");
                        var in_expense = GetIntVal(itmPayDetailResult.getProperty("in_expense", "0"));
                        var qty = GetIntVal(itmPayDetailResult.getProperty("qty", "0"));

                        string str_expense = "$" + in_expense.ToString("N0");
                        string str_sum = "$" + (in_expense * qty).ToString("N0");

                        if (is_team_bill)
                        {
                            str_expense = "";
                            str_sum = "";
                        }

                        sheet.Range["B" + (28 + j)].Text = in_l1 + " " + str_expense + " X " + qty.ToString();
                        sheet.Range["F" + (28 + j)].Text = str_sum;
                    }

                    //超商條碼1
                    var pic1 = sheet.Pictures.Add(6 + 22, 7, barcode_1);
                    pic1.LeftColumnOffset = 500;
                    pic1.TopRowOffset = 400;

                    //銀行條碼
                    var pic2 = sheet.Pictures.Add(16 + 22, 2, barcode_2);

                    pic2.LeftColumnOffset = 60;
                    pic2.TopRowOffset = 400;

                    //超商條碼2
                    pic2 = sheet.Pictures.Add(9 + 22, 7, barcode_2);
                    pic2.LeftColumnOffset = 40;
                    pic2.TopRowOffset = 400;

                    //超商條碼3
                    var pic3 = sheet.Pictures.Add(12 + 22, 7, barcode_3);
                    pic3.LeftColumnOffset = 40;
                    pic3.TopRowOffset = 400;

                    if (!isNoReg)
                    {
                        //qrcode
                        var pic4 = sheet.Pictures.Add(16 + 22, 11, qrcode);
                        pic4.LeftColumnOffset = 100;
                    }
                    else
                    {
                        sheet.Range["K" + (15 + 22)].Text = "";
                        sheet.Range["G" + (16 + 22)].Text = "";
                    }

                    sheet.PageSetup.FitToPagesWide = 1;
                    sheet.PageSetup.FitToPagesTall = 1;
                    sheet.PageSetup.TopMargin = 0.3;
                    sheet.PageSetup.LeftMargin = 0.5;
                    sheet.PageSetup.RightMargin = 0.5;
                    sheet.PageSetup.BottomMargin = 0.5;
                    //適合頁面
                    workbook.ConverterSetting.SheetFitToPage = true;
                    if (i + 1 < itmPayResults.getItemCount())
                    {
                        sheet = workbook.CreateEmptySheet();
                    }
                }
                else
                {

                    sheet.CopyFrom(sheetTemplate);
                    sheet.Name = "繳費單號" + i.ToString(); ;
                    sheet.Range["K3"].Text = item_number;
                    sheet.Range["B4"].Text = "活動名稱：" + in_title;
                    sheet.Range["C14"].Text = in_bank_name;
                    sheet.Range["C15"].Text = chinese_expense;
                    sheet.Range["F15"].Text = expense;
                    sheet.Range["D20"].Text = payDate;

                    //繳費明細
                    Item itmPayDetailResults = GetPayDetailResult(cfg, item_number);

                    for (int j = 0; j < 7; j++)
                    {
                        sheet.Range["B" + (6 + j)].Text = "";
                        sheet.Range["F" + (6 + j)].Text = "";
                    }

                    for (int j = 0; j < itmPayDetailResults.getItemCount(); j++)
                    {
                        Item itmPayDetailResult = itmPayDetailResults.getItemByIndex(j);
                        var in_l1 = itmPayDetailResult.getProperty("in_l1", "");
                        var in_expense = GetIntVal(itmPayDetailResult.getProperty("in_expense", "0"));
                        var qty = GetIntVal(itmPayDetailResult.getProperty("qty", "0"));

                        string str_expense = "$" + in_expense.ToString("N0");
                        string str_sum = "$" + (in_expense * qty).ToString("N0");

                        if (is_team_bill)
                        {
                            str_expense = "";
                            str_sum = "";
                        }

                        sheet.Range["B" + (6 + j)].Text = in_l1 + " " + str_expense + " X " + qty.ToString();
                        sheet.Range["F" + (6 + j)].Text = str_sum;
                    }

                    //超商條碼1
                    var pic1 = sheet.Pictures.Add(6, 7, barcode_1);
                    pic1.LeftColumnOffset = 500;
                    pic1.TopRowOffset = 400;

                    //銀行條碼
                    var pic2 = sheet.Pictures.Add(16, 2, barcode_2);

                    pic2.LeftColumnOffset = 60;
                    pic2.TopRowOffset = 400;

                    //超商條碼2
                    pic2 = sheet.Pictures.Add(9, 7, barcode_2);
                    pic2.LeftColumnOffset = 40;
                    pic2.TopRowOffset = 400;

                    //超商條碼3
                    var pic3 = sheet.Pictures.Add(12, 7, barcode_3);
                    pic3.LeftColumnOffset = 40;
                    pic3.TopRowOffset = 400;

                    if (!isNoReg)
                    {
                        //qrcode
                        var pic4 = sheet.Pictures.Add(16, 11, qrcode);
                        pic4.LeftColumnOffset = 100;
                    }
                    else
                    {
                        sheet.Range["K15"].Text = "";
                        sheet.Range["G16"].Text = "";
                    }

                    if (i + 1 == itmPayResults.getItemCount())
                    {
                        sheet.PageSetup.FitToPagesWide = 1;
                        sheet.PageSetup.FitToPagesTall = 1;
                        sheet.PageSetup.TopMargin = 0.3;
                        sheet.PageSetup.LeftMargin = 0.5;
                        sheet.PageSetup.RightMargin = 0.5;
                        sheet.PageSetup.BottomMargin = 0.5;
                        //適合頁面
                        workbook.ConverterSetting.SheetFitToPage = false;
                    }
                }
            }
        }

        #region 存取資料

        private Item GetPayDetailResult(TConfig cfg, string item_number)
        {
            string sql = @"
                SELECT in_l1, in_expense, count(*) AS qty
                FROM {#meeting_type} WITH (NOLOCK)
                WHERE IN_PAYNUMBER = '{#item_number}'
                GROUP BY in_l1, in_expense
            ";
            sql = sql.Replace("{#meeting_type}", cfg.muser_type)
                    .Replace("{#item_number}", item_number);

            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// 取得匯出設定資訊
        /// </summary>
        private TExport GetExportInfo(TConfig cfg)
        {
            string guid = System.Guid.NewGuid().ToString().Substring(0, 6).ToUpper()
                + "_" + DateTime.Now.ToString("yyyyMMddHHmmssfff");

            Item itmPath = GetDocPaths(cfg, cfg.meeting_excel_parameter);

            string export_path = itmPath.getProperty("export_path", "").TrimEnd('\\');
            string ext_name = ".xlsx";
            string doc_name = cfg.doc_name + "_" + guid;

            string doc_file = export_path + "\\" + doc_name + ext_name;
            string doc_url = doc_name + ext_name;

            return new TExport
            {
                Source = itmPath.getProperty("template_path", ""),
                Target = doc_file,
                Url = doc_url,
            };
        }

        /// <summary>
        /// 取得樣板與匯出路徑
        /// </summary>
        private Item GetDocPaths(TConfig cfg, string in_name)
        {
            Item itmResult = cfg.inn.newItem();

            //Excel 匯出路徑 (from 原創.設定參數)
            string sql = @"
                SELECT 
                    t2.in_name AS 'variable'
                    , t1.in_name
                    , t1.in_value 
                FROM 
                    In_Variable_Detail t1 WITH(NOLOCK)
                INNER JOIN 
                    In_Variable t2 WITH(NOLOCK) 
                    ON t2.ID = t1.SOURCE_ID 
                WHERE 
                    t2.in_name = N'meeting_excel'  
                    AND t1.in_name IN (N'export_path', N'{#in_name}')
            ";

            sql = sql.Replace("{#in_name}", in_name);

            //cfg.CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item items = cfg.inn.applySQL(sql);

            if (items.isError() || items.getItemCount() < 1)
            {
                throw new Exception("未設定樣板與匯出路徑 _# " + in_name);
            }

            for (int i = 0; i < items.getItemCount(); i++)
            {
                Item item = items.getItemByIndex(i);
                string xls_name = item.getProperty("in_name", "");
                string xls_value = item.getProperty("in_value", "");

                if (xls_name == "export_path")
                {
                    itmResult.setProperty("export_path", xls_value);
                }
                else
                {
                    itmResult.setProperty("template_path", xls_value);
                }
            }

            return itmResult;
        }

        #endregion 存取資料

        #region 資料模型

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public Innovator inn { get; set; }

            /// <summary>
            /// 範本參數
            /// </summary>
            public string meeting_excel_parameter { get; set; }

            /// <summary>
            /// 網址
            /// </summary>
            public string urlPath { get; set; }

            //外部輸入區

            /// <summary>
            /// cla 講習
            /// </summary>
            public string mode { get; set; }

            /// <summary>
            /// 場景
            /// </summary>
            public string scene { get; set; }

            public string in_sno { get; set; }

            /// <summary>
            /// 活動 ItemType
            /// </summary>
            public string mt_type { get; set; }

            /// <summary>
            /// 與會者 ItemType
            /// </summary>
            public string muser_type { get; set; }

            /// <summary>
            /// 活動 id
            /// </summary>
            public string meeting_id { get; set; }

            /// <summary>
            /// 匯出類型
            /// </summary>
            public string doc_type { get; set; }

            /// <summary>
            /// 匯出名稱
            /// </summary>
            public string doc_name { get; set; }

            public TExport Export { get; set; }

            public Item itmMeeting { get; set; }

        }

        /// <summary>
        /// 匯出資料模型
        /// </summary>
        private class TExport
        {
            /// <summary>
            /// 樣板來源完整路徑
            /// </summary>
            public string Source { get; set; }

            /// <summary>
            /// 完整檔案路徑與名稱
            /// </summary>
            public string Target { get; set; }

            /// <summary>
            /// 檔案網址
            /// </summary>
            public string Url { get; set; }
        }

        #endregion

        #region zxing

        /// <summary>
        /// 生成二維碼
        /// </summary>
        /// <param name="text">內容</param>
        /// <param name="width">寬度</param>
        /// <param name="height">高度</param>
        /// <returns></returns>
        private System.Drawing.Bitmap GenerateQrCode(string text, int width = 100, int height = 100)
        {
            var writer = new ZXing.BarcodeWriter
            {
                Format = ZXing.BarcodeFormat.QR_CODE,
                Options = new ZXing.QrCode.QrCodeEncodingOptions //設定大小
                {
                    Width = width,
                    Height = height,
                    Margin = 0, //設定二維碼的邊距,單位不是固定畫素
                    DisableECI = true,//設定內容編碼
                    CharacterSet = "UTF-8",
                    //ErrorCorrection = ZXing.QrCode.Internal.ErrorCorrectionLevel.M
                },
            };

            return writer.Write(text);
        }

        /// <summary>
        /// 生成一維條形碼
        /// </summary>
        /// <param name="text">內容</param>
        /// <param name="width">寬度</param>
        /// <param name="height">高度</param>
        /// <returns></returns>
        private System.Drawing.Bitmap GenerateCode39(string text, int width = 100, int height = 100)
        {
            var writer = new ZXing.BarcodeWriter
            {
                Format = ZXing.BarcodeFormat.CODE_39,
                Options = new ZXing.Common.EncodingOptions
                {
                    Width = width,
                    Height = height,
                    Margin = 2, //設定二維碼的邊距,單位不是固定畫素
                },
            };

            return writer.Write(text);
        }

        /// <summary>
        /// 生成帶Logo的二維碼
        /// </summary>
        /// <param name="text">內容</param>
        /// <param name="width">寬度</param>
        /// <param name="height">高度</param>
        private System.Drawing.Bitmap Generate3(string text, int width = 100, int height = 100)
        {
            //Logo 圖片
            string logoPath = System.AppDomain.CurrentDomain.BaseDirectory + @"\img\logo.png";
            var logo = new System.Drawing.Bitmap(logoPath);

            //構造二維碼寫碼器
            var writer = new ZXing.MultiFormatWriter();
            Dictionary<ZXing.EncodeHintType, object> hint = new Dictionary<ZXing.EncodeHintType, object>();
            hint.Add(ZXing.EncodeHintType.CHARACTER_SET, "UTF-8");
            hint.Add(ZXing.EncodeHintType.ERROR_CORRECTION, ZXing.QrCode.Internal.ErrorCorrectionLevel.H);
            //hint.Add(EncodeHintType.MARGIN, 2);//舊版本不起作用，需要手動去除白邊

            //生成二維碼
            var bm = writer.encode(text, ZXing.BarcodeFormat.QR_CODE, width + 30, height + 30, hint);
            bm = deleteWhite(bm);

            var barcodeWriter = new ZXing.BarcodeWriter();
            var map = barcodeWriter.Write(bm);

            //獲取二維碼實際尺寸（去掉二維碼兩邊空白後的實際尺寸）
            int[] rectangle = bm.getEnclosingRectangle();

            //計算插入圖片的大小和位置
            int middleW = Math.Min((int)(rectangle[2] / 3), logo.Width);
            int middleH = Math.Min((int)(rectangle[3] / 3), logo.Height);
            int middleL = (map.Width - middleW) / 2;
            int middleT = (map.Height - middleH) / 2;

            var bmpimg = new System.Drawing.Bitmap(map.Width, map.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

            using (var g = System.Drawing.Graphics.FromImage(bmpimg))
            {
                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                g.DrawImage(map, 0, 0, width, height);
                //白底將二維碼插入圖片
                g.FillRectangle(System.Drawing.Brushes.White, middleL, middleT, middleW, middleH);
                g.DrawImage(logo, middleL, middleT, middleW, middleH);
            }
            return bmpimg;
        }

        /// <summary>
        /// 刪除預設對應的空白
        /// </summary>
        private ZXing.Common.BitMatrix deleteWhite(ZXing.Common.BitMatrix matrix)
        {
            int[] rec = matrix.getEnclosingRectangle();
            int resWidth = rec[2] + 1;
            int resHeight = rec[3] + 1;

            var resMatrix = new ZXing.Common.BitMatrix(resWidth, resHeight);
            resMatrix.clear();
            for (int i = 0; i < resWidth; i++)
            {
                for (int j = 0; j < resHeight; j++)
                {
                    if (matrix[i + rec[0], j + rec[1]])
                        resMatrix[i, j] = true;
                }
            }
            return resMatrix;
        }

        #endregion zxing

        private string GetDateTimeValue(string value, string format, int hours = 0)
        {
            if (value == "") return "";

            DateTime dt = DateTime.Now;
            if (DateTime.TryParse(value, out dt))
            {
                return dt.AddHours(hours).ToString(format);
            }
            else
            {
                return value;
            }
        }

        private int GetIntVal(string value, int def = 0)
        {
            int result = def;
            Int32.TryParse(value, out result);
            return result;
        }

        //數字轉中文大寫
        public static string NumberToCHS(string Val)
        {
            string result = "";

            string CN_ZERO = "零";
            string CN_ONE = "壹";
            string CN_TWO = "貳";
            string CN_THREE = "參";
            string CN_FOUR = "肆";
            string CN_FIVE = "伍";
            string CN_SIX = "陸";
            string CN_SEVEN = "柒";
            string CN_EIGHT = "捌";
            string CN_NINE = "玖";
            string CN_TEN = "拾";
            string CN_HUNDRED = "佰";
            string CN_THOUSAND = "仟";
            string CN_TEN_THOUSAND = "萬";
            string CN_HUNDRED_MILLION = "億";
            //string CN_SYMBOL = "$";
            string CN_SYMBOL = "";
            string CN_DOLLAR = "元";
            string CN_TEN_CENT = "角";
            string CN_CENT = "分";
            string CN_INTEGER = "整";
            string[] aDigits = new string[] { CN_ZERO, CN_ONE, CN_TWO, CN_THREE, CN_FOUR, CN_FIVE, CN_SIX, CN_SEVEN, CN_EIGHT, CN_NINE };
            string[] aRadices = new string[] { "", CN_TEN, CN_HUNDRED, CN_THOUSAND };
            string[] aBigRadices = new string[] { "", CN_TEN_THOUSAND, CN_HUNDRED_MILLION };
            string[] aDecimals = new string[] { CN_TEN_CENT, CN_CENT };

            string[] aParts = null;
            string sInt = "";
            string sDcm = "";

            int iInt = 0;
            int iDcm = 0; // Represent decimal part of digit number.  

            int zeroCount = int.MinValue;
            int i = int.MinValue;
            int p = int.MinValue;
            int d = int.MinValue;

            int quotient = 0;
            int modulus = 0;

            aParts = Val.Split('.');
            if (aParts.Length > 1)
            {
                sInt = aParts[0];
                sDcm = aParts[1];
                if (sDcm.Length > 2) sDcm = sDcm.Substring(0, 2);
            }
            else
            {
                sInt = aParts[0];
                sDcm = "";
            }

            iInt = Convert.ToInt32(sInt);

            if (iInt > 0)
            {
                zeroCount = 0;
                for (i = 0; i < sInt.Length; i++)
                {
                    p = sInt.Length - i - 1;
                    d = Convert.ToInt32(sInt.Substring(i, 1));

                    quotient = p / 4;
                    modulus = p % 4;
                    if (d == 0)
                    {
                        zeroCount++;
                    }
                    else
                    {
                        if (zeroCount > 0)
                        {
                            result += aDigits[0];
                        }
                        zeroCount = 0;
                        result += aDigits[(int)(d)] + aRadices[modulus];
                    }
                    if (modulus == 0 && zeroCount < 4)
                    {
                        result += aBigRadices[quotient];
                    }
                }
                result += CN_DOLLAR;
            }
            if (sDcm.Length > 0)
            {
                for (i = 0; i < sDcm.Length; i++)
                {
                    d = Convert.ToInt32(sDcm.Substring(i, 1));
                    if (d != 0)
                    {
                        result += aDigits[(int)(d)] + aDecimals[i];
                    }
                }
            }
            if (result.Length == 0)
            {
                result = CN_ZERO + CN_DOLLAR;
            }
            if (sDcm.Length == 0)
            {
                result += CN_INTEGER;
            }

            result = CN_SYMBOL + result;

            return result;
        }
    }
}