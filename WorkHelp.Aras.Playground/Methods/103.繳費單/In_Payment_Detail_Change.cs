﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;
using System.Net;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class In_Payment_Detail_Change : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
            目的: 申請換組
            日期: 
                - 2021-11-25 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]In_Payment_Detail_Change";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "input dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                strIdentityId = inn.getUserAliases(),

                in_paynumber = itmR.getProperty("in_paynumber", ""),
                in_section_name = itmR.getProperty("in_section_name", ""),
                in_index = itmR.getProperty("in_index", ""),
                in_names = itmR.getProperty("in_names", ""),
                scene = itmR.getProperty("scene", ""),
            };

            switch (cfg.scene)
            {
                case "modal":
                    Modal(cfg, itmR);
                    break;

                case "save":
                    Save(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void Save(TConfig cfg, Item itmReturn)
        {
            if (cfg.in_paynumber == "")
            {
                throw new Exception("繳費單號不可為空值");
            }

            string new_in_l1 = itmReturn.getProperty("new_in_l1", "");
            string new_in_l2 = itmReturn.getProperty("new_in_l2", "");
            string new_in_l3 = itmReturn.getProperty("new_in_l3", "");
            string in_apply_reason = itmReturn.getProperty("in_apply_reason", "");

            if (string.IsNullOrWhiteSpace(in_apply_reason))
            {
                throw new Exception("申請原因不可為空值");
            }

            List<string> list = new List<string>
            {
                new_in_l1,
                new_in_l2,
                new_in_l3,
            };

            bool has_error = false;
            foreach(var str in list)
            {
                if (str == "" || str == "請選擇")
                {
                    has_error = true;
                    break;
                }
            }

            if (has_error)
            {
                throw new Exception("新組別或量級資料錯誤");
            }

            string in_new_sections = string.Join("@", list);

            string aml = "<item_number>" + cfg.in_paynumber + "</item_number>"
                + "<pay_section_name>" + cfg.in_section_name + "</pay_section_name>"
                + "<pay_index>" + cfg.in_index + "</pay_index>"
                + "<pay_type>" + "換組" + "</pay_type>"
                + "<in_cancel_memo>" + in_apply_reason + "</in_cancel_memo>"
                + "<in_new_sections>" + in_new_sections + "</in_new_sections>"
                + "";

            Item itmMethodResult = cfg.inn.applyMethod("In_Payment_ApplyRefund", aml);
        }

        //跳窗
        private void Modal(TConfig cfg, Item itmReturn)
        {
            string sql = "";

            if (cfg.in_paynumber == "")
            {
                throw new Exception("繳費單號不可為空值");
            }

            //繳費單
            sql = "SELECT TOP 1 * FROM IN_MEETING_PAY WITH(NOLOCK) WHERE item_number = '" + cfg.in_paynumber + "'";
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);
            Item itmPay = cfg.inn.applySQL(sql);
            if (itmPay.isError() || itmPay.getResult() == "")
            {
                throw new Exception("繳費單資料發生錯誤");
            }

            string pay_id = itmPay.getProperty("id", "");

            //繳費單明細
            sql = @"
                SELECT * FROM IN_MEETING_NEWS WITH(NOLOCK)
                WHERE source_id = '{#pay_id}'
                AND in_section_name = N'{#in_section_name}'
                AND in_index = '{#in_index}'
            ";

            sql = sql.Replace("{#pay_id}", pay_id)
                .Replace("{#in_section_name}", cfg.in_section_name)
                .Replace("{#in_index}", cfg.in_index);

            Item itmPayDetails = cfg.inn.applySQL(sql);
            if (itmPayDetails.isError() || itmPayDetails.getResult() == "")
            {
                throw new Exception("繳費單明細發生錯誤");
            }

            if (CanApply(cfg, itmPayDetails))
            {
                itmReturn.setProperty("apply_button", "<button type='button' class='btn btn-success' onclick='Apply_Click()'>送出申請</button>");
                //throw new Exception("該項無法申請換組");
            }

            ////與會者
            //sql = @"
            //    SELECT * FROM IN_MEETING_USER WITH(NOLOCK)
            //    WHERE in_paynumber = '{#in_paynumber}'
            //    AND in_section_name = N'{#in_section_name}'
            //    AND in_index = '{#in_index}'
            //";

            //sql = sql.Replace("{#in_paynumber}", cfg.in_paynumber)
            //    .Replace("{#in_section_name}", cfg.in_section_name)
            //    .Replace("{#in_index}", cfg.in_index);

            //Item itmMUsers = cfg.inn.applySQL(sql);
            //if (itmMUsers.isError() || itmMUsers.getResult() == "")
            //{
            //    throw new Exception("與會者發生錯誤");
            //}

            Item itmFirst = itmPayDetails.getItemByIndex(0);

            //設定回傳資料
            itmReturn.setProperty("in_current_org", itmPay.getProperty("in_current_org", ""));
            itmReturn.setProperty("in_l1", itmFirst.getProperty("in_l1", ""));
            itmReturn.setProperty("in_l2", itmFirst.getProperty("in_l2", ""));
            itmReturn.setProperty("in_l3", itmFirst.getProperty("in_l3", ""));
            itmReturn.setProperty("in_cancel_memo", itmFirst.getProperty("in_cancel_memo", ""));
            itmReturn.setProperty("in_new_sections", itmFirst.getProperty("in_new_sections", " "));

            //附加三階選項
            AppendLevels(cfg, itmPay, itmReturn);
        }

        //附加細項相關資訊
        private void AppendLevels(TConfig cfg, Item itmPay, Item itmReturn)
        {
            string meeting_id = itmPay.getProperty("in_meeting", "");
            string mt_type = "IN_MEETING";
            string mtsv_type = "IN_MEETING_SURVEYS";
            string sv_type = "IN_SURVEY";
            string svo_type = "IN_SURVEY_OPTION";

            if (meeting_id == "")
            {
                meeting_id = itmPay.getProperty("in_cla_meeting", "");
                mt_type = "IN_CLA_MEETING";
                mtsv_type = "IN_CLA_MEETING_SURVEYS";
                sv_type = "IN_CLA_SURVEY";
                svo_type = "IN_CLA_SURVEY_OPTION";
            }

            string sql = @"
                SELECT
	                t2.id AS 'survey_id'
	                , t2.in_property
	                , t2.in_questions
                FROM
	                {#mtsv_type} t1 WITH(NOLOCK)
                INNER JOIN
	                {#sv_type} t2 WITH(NOLOCK)
	                ON t2.id = t1.related_id
                WHERE
	                t1.source_id = '{#meeting_id}'
	                AND t2.in_property IN ('in_l1', 'in_l2', 'in_l3')
                ORDER BY
	                t2.in_property
            ";

            sql = sql.Replace("{#mtsv_type}", mtsv_type)
                .Replace("{#sv_type}", sv_type)
                .Replace("{#meeting_id}", meeting_id);

            Item itmData = cfg.inn.newItem();

            List<string> ls = new List<string>();
            Item items = cfg.inn.applySQL(sql);
            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string survey_id = item.getProperty("survey_id", "");
                string in_property = item.getProperty("in_property", "");
                string in_questions = item.getProperty("in_questions", "");
                ls.Add(in_property);

                itmData.setProperty(in_property + "_id", survey_id);
            }

            itmData.setProperty("in_level_array", string.Join(",", ls));

            AppendOptions(cfg, itmData, itmReturn);
        }

        //附加細項相關資訊
        private void AppendOptions(TConfig cfg, Item itmData, Item itmReturn)
        {
            string in_level_array = itmData.getProperty("in_level_array", "");

            Item items = null;
            if (in_level_array.Contains("in_l3"))
            {
                items = GetOptions123(cfg, itmData);
            }
            else if (in_level_array.Contains("in_l2"))
            {
                items = GetOptions12(cfg, itmData);
            }
            else if (in_level_array.Contains("in_l1"))
            {
                items = GetOptions1(cfg, itmData);
            }

            if (items == null || items.isError() || items.getItemCount() <= 0)
            {
                return;
            }

            List<TNode> nodes = new List<TNode>();

            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                TNode l1 = AddAndGetNode(nodes, item, "in_l1");
                TNode l2 = AddAndGetNode(l1.Nodes, item, "in_l2");
                TNode l3 = AddAndGetNode(l2.Nodes, item, "in_l3");
            }

            itmReturn.setProperty("in_level_json", Newtonsoft.Json.JsonConvert.SerializeObject(nodes));
        }

        private TNode AddAndGetNode(List<TNode> nodes, Item item, string lv)
        {
            string value = item.getProperty(lv + "_value", "");

            TNode search = nodes.Find(x => x.Val == value);

            if (search == null)
            {
                search = new TNode
                {
                    Lv = lv,
                    Val = value,
                    Lbl = item.getProperty(lv + "_label", ""),
                    Ext = item.getProperty(lv + "_extend_value", ""),
                    Nodes = new List<TNode>()
                };
                nodes.Add(search);
            }

            return search;
        }

        private Item GetOptions1(TConfig cfg, Item itmData)
        {
            string in_l1_id = itmData.getProperty("in_l1_id", "");

            string sql = @"
                SELECT
                    t1.in_value           AS 'in_l1_value'
                    , t1.in_label         AS 'in_l1_label'
                    , t1.sort_order       AS 'in_l1_sort_order'
                    , t1.in_extend_value  AS 'in_l1_extend_value'
                FROM 
                    IN_SURVEY_OPTION t1 WITH(NOLOCK)
                WHERE
                    t1.SOURCE_ID = '{#in_l1_id}'
                ORDER BY
                    t1.SORT_ORDER
            ";

            sql = sql.Replace("{#in_l1_id}", in_l1_id);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        private Item GetOptions12(TConfig cfg, Item itmData)
        {
            string in_l1_id = itmData.getProperty("in_l1_id", "");
            string in_l2_id = itmData.getProperty("in_l2_id", "");

            string sql = @"
                SELECT
                    t1.in_value           AS 'in_l1_value'
                    , t1.in_label         AS 'in_l1_label'
                    , t1.sort_order       AS 'in_l1_sort_order'
                    , t1.in_extend_value  AS 'in_l1_extend_value'
                    , t2.in_value         AS 'in_l2_value'
                    , t2.in_label         AS 'in_l2_label'
                    , t2.sort_order       AS 'in_l2_sort_order'
                    , t2.in_extend_value  AS 'in_l2_extend_value'
                FROM 
                    IN_SURVEY_OPTION t1 WITH(NOLOCK)
                LEFT OUTER JOIN (
                        SELECT IN_FILTER, IN_VALUE, IN_LABEL, SORT_ORDER, IN_EXPENSE_VALUE, IN_EXTEND_VALUE FROM IN_SURVEY_OPTION WITH(NOLOCK) WHERE SOURCE_ID = '{#in_l2_id}'
                    ) t2 ON t2.IN_FILTER = t1.IN_VALUE
                WHERE
                    t1.SOURCE_ID = '{#in_l1_id}'
                ORDER BY
                    t1.SORT_ORDER
                    , t2.SORT_ORDER
            ";

            sql = sql.Replace("{#in_l1_id}", in_l1_id)
                .Replace("{#in_l2_id}", in_l2_id);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        private Item GetOptions123(TConfig cfg, Item itmData)
        {
            string in_l1_id = itmData.getProperty("in_l1_id", "");
            string in_l2_id = itmData.getProperty("in_l2_id", "");
            string in_l3_id = itmData.getProperty("in_l3_id", "");

            string sql = @"
                SELECT
                    t1.in_value           AS 'in_l1_value'
                    , t1.in_label         AS 'in_l1_label'
                    , t1.sort_order       AS 'in_l1_sort_order'
                    , t1.in_extend_value  AS 'in_l1_extend_value'
                    , t2.in_value         AS 'in_l2_value'
                    , t2.in_label         AS 'in_l2_label'
                    , t2.sort_order       AS 'in_l2_sort_order'
                    , t2.in_extend_value  AS 'in_l2_extend_value'
                    , t3.in_value         AS 'in_l3_value'
                    , t3.in_label         AS 'in_l3_label'
                    , t3.sort_order       AS 'in_l3_sort_order'
                    , t3.in_extend_value  AS 'in_l3_extend_value'
                FROM 
                    IN_SURVEY_OPTION t1 WITH(NOLOCK)
                LEFT OUTER JOIN (
                        SELECT IN_FILTER, IN_VALUE, IN_LABEL, SORT_ORDER, IN_EXPENSE_VALUE, IN_EXTEND_VALUE FROM IN_SURVEY_OPTION WITH(NOLOCK) WHERE SOURCE_ID = '{#in_l2_id}'
                    ) t2 ON t2.IN_FILTER = t1.IN_VALUE
                LEFT OUTER JOIN (
                        SELECT IN_FILTER, IN_VALUE, IN_LABEL, SORT_ORDER, IN_EXPENSE_VALUE, IN_EXTEND_VALUE FROM IN_SURVEY_OPTION WITH(NOLOCK) WHERE SOURCE_ID = '{#in_l3_id}'
                    ) t3 ON t3.IN_FILTER = t2.IN_VALUE
                WHERE
                    t1.SOURCE_ID = '{#in_l1_id}'
                ORDER BY
                    t1.SORT_ORDER
                    , t2.SORT_ORDER
                    , t3.SORT_ORDER
            ";

            sql = sql.Replace("{#in_l1_id}", in_l1_id)
                .Replace("{#in_l2_id}", in_l2_id)
                .Replace("{#in_l3_id}", in_l3_id);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        private bool CanApply(TConfig cfg, Item items)
        {
            bool can_apply = true;

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_cancel_time = item.getProperty("in_cancel_time", "");

                if (in_cancel_time != "")
                {
                    can_apply = false;
                    break;
                }
            }

            return can_apply;
        }

        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }

            public string in_paynumber { get; set; }
            public string in_section_name { get; set; }
            public string in_index { get; set; }
            public string in_names { get; set; }
            public string scene { get; set; }
        }

        private class TNode
        {
            public string Lv { get; set; }
            public string Val { get; set; }
            public string Lbl { get; set; }
            public string Ext { get; set; }
            public List<TNode> Nodes { get; set; }
        }

    }
}