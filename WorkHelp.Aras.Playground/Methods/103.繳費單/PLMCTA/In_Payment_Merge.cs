﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;
using System.Net;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class In_Payment_Merge : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 繳費單合併
                日期: 
                    - 2022-01-13 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]In_Payment_Merge";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "this: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                strIdentityId = inn.getUserAliases(),

                code = itmR.getProperty("code", ""),
                scene = itmR.getProperty("scene", ""),
            };

            //取得登入者權限
            Item itmPermit = inn.applyMethod("In_CheckIdentity", "<method>" + strMethodName + "</method><code>ALL</code>");
            cfg.isMeetingAdmin = itmPermit.getProperty("isMeetingAdmin", "") == "1";
            cfg.isCommittee = itmPermit.getProperty("isCommittee", "") == "1";
            cfg.isGymOwner = itmPermit.getProperty("isGymOwner", "") == "1";
            cfg.isGymAssistant = itmPermit.getProperty("isGymAssistant", "") == "1";

            //取得登入者資訊
            Item itmResume = inn.newItem("In_Resume", "get");
            itmResume.setProperty("in_user_id", cfg.strUserId);
            cfg.LoginResume = itmResume.apply();

            //取得活動資料
            string sql = "SELECT TOP 1 * FROM IN_MEETING WITH(NOLOCK)"
                + " WHERE in_meeting_type = 'merge'"
                + " AND in_receice_org_code = '" + cfg.code + "'"
                + " ORDER BY in_date_s DESC";

            cfg.itmMeeting = cfg.inn.applySQL(sql);
            if (cfg.itmMeeting.isError() || cfg.itmMeeting.getResult() == "")
            {
                throw new Exception("合併 Meeting 未設定");
            }

            switch (cfg.scene)
            {
                case "merge":
                    Merge(cfg, itmR);
                    break;

                case "cancel":
                    Cancel(cfg, itmR);
                    break;

                default:
                    itmR.setProperty("inn_resume_email", cfg.LoginResume.getProperty("in_email", " "));

                    Page(cfg, itmR);
                    break;
            }

            return itmR;
        }

        //取消合併
        private void Cancel(TConfig cfg, Item itmReturn)
        {
            string id = itmReturn.getProperty("id", "");

            Item itmPay = cfg.inn.newItem("IN_MEETING_PAY", "get");
            itmPay.setProperty("id", id);
            itmPay = itmPay.apply();

            if (itmPay.isError() || itmPay.getResult() == "")
            {
                throw new Exception("查無繳費單");
            }

            string pay_bool = itmPay.getProperty("pay_bool", "");
            if (pay_bool != "未繳費")
            {
                throw new Exception("繳費單狀態 = " + pay_bool + "，不可取消合併");
            }

            string sql = @"
                SELECT 
	                t2.* 
                FROM 
	                IN_MEETING_NEWS t1 WITH(NOLOCK) 
                INNER JOIN 
	                IN_MEETING_PAY t2 WITH(NOLOCK) 
	                ON t2.item_number = t1.in_l1 
                WHERE 
	                t1.source_id = '{#id}'
            ";

            sql = sql.Replace("{#id}", id);

            Item itmDetails = cfg.inn.applySQL(sql);

            if (itmDetails.isError() || itmDetails.getResult() == "")
            {
                throw new Exception("查無繳費單明細");
            }

            bool is_error = false;
            int count = itmDetails.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item itmDetail = itmDetails.getItemByIndex(i);
                string detail_pay_bool = itmDetail.getProperty("pay_bool", "");

                if (detail_pay_bool != "合併")
                {
                    is_error = true;
                    break;
                }
            }

            if (is_error)
            {
                throw new Exception("被合併的繳費單狀態已變更，不可取消合併");
            }

            for (int i = 0; i < count; i++)
            {
                Item itmDetail = itmDetails.getItemByIndex(i);
                string detail_pay_id = itmDetail.getProperty("id", "");

                string sql_upd = "UPDATE IN_MEETING_PAY SET"
                    + " pay_bool = N'未繳費'"
                    + " WHERE id = '" + detail_pay_id + "'";

                Item itmDetailResult = cfg.inn.applySQL(sql_upd);

                if (itmDetailResult.isError())
                {
                    throw new Exception("被合併的繳費單狀態復原失敗");
                }
            }

            string sql_upd2 = "UPDATE IN_MEETING_PAY SET"
                    + " pay_bool = N'已取消'"
                    + " WHERE id = '" + id + "'";

            Item itmPayResult = cfg.inn.applySQL(sql_upd2);

            if (itmPayResult.isError())
            {
                throw new Exception("取消合併失敗");
            }
        }

        //合併繳費單
        private void Merge(TConfig cfg, Item itmReturn)
        {
            string ids = itmReturn.getProperty("ids", "");
            if (ids == "") throw new Exception("請勾選繳費單");

            string[] arr = ids.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            if (arr == null || arr.Length == 0) throw new Exception("請勾選繳費單");

            List<TPay> list = GetPaymentList(cfg, is_merge: false);
            AppendGymPayList(cfg, list, is_merge: false);
            var merge_list = list.FindAll(x => arr.Contains(x.item_number));
            if (merge_list == null || merge_list.Count != arr.Length)
            {
                throw new Exception("繳費單狀態已變更，請刷新頁面後重新進行合併");
            }

            var param = GetPayParam(cfg);

            //新增繳費單與明細
            AddPayment(cfg, param, merge_list);

            //變更繳費單狀態
            UpdPayment(cfg, merge_list);

            //發送繳費通知
            SendPaymentEmail(cfg, itmReturn);
        }

        private void SendPaymentEmail(TConfig cfg, Item itmReturn)
        {
            Item itmEmail = cfg.inn.newItem();
            itmEmail.setType("In_Meeting_Pay");
            itmEmail.setProperty("itemtype", "In_Meeting");
            itmEmail.setProperty("id", cfg.itmMeeting.getProperty("id", ""));
            itmEmail.setProperty("resume_sno", cfg.LoginResume.getProperty("in_sno", ""));
            itmEmail.setProperty("new_creator_email", itmReturn.getProperty("new_creator_email", ""));
            itmEmail.setProperty("email_name", "PayEmailMerge");
            Item itmEmailResult = itmEmail.apply("In_Send_MeetingPay");
        }

        private void UpdPayment(TConfig cfg, List<TPay> pay_list)
        {
            for (int i = 0; i < pay_list.Count; i++)
            {
                var pay = pay_list[i];

                string sql = "UPDATE IN_MEETING_PAY SET pay_bool = N'合併' WHERE id = '" + pay.id + "'";

                Item itmResult = cfg.inn.applySQL(sql);

                if (itmResult.isError())
                {
                    throw new Exception("繳費單變更狀態(合併)發生錯誤");
                }
            }
        }

        /// <summary>
        /// 新增繳費單與明細資料
        /// </summary>
        private void AddPayment(TConfig cfg, TPayParam param, List<TPay> pay_list)
        {
            string aml = "";
            string sql = "";

            //預設為 10
            string codes = param.codes;

            //當收款單位編號長度為 6 碼
            if (param.in_receice_org_code.Length == 6)
            {
                codes = "";
            }

            string numbers = "";

            int muser_count = pay_list.Count;
            int total_amount = pay_list.Sum(x => x.pay_amount_exp);
            DateTime max_pay_date_exp = pay_list.Max(x => x.pay_date_exp);

            //將最後收費日期&應繳日期轉型
            DateTime in_pay_date_exp1 = max_pay_date_exp;
            DateTime in_pay_date_exp = max_pay_date_exp;
            string due_date = in_pay_date_exp.ToString("yyyy-MM-dd");

            //費用
            string xls_money = total_amount.ToString();
            string xls_current_org = cfg.LoginResume.getProperty("in_current_org", "");//所屬單位
            string xls_real_stuff = "0";
            string xls_real_player = muser_count.ToString();
            string xls_real_items = muser_count.ToString();

            //區分繳費模式(A>繳款人 B>委託單位)
            param.project_code = GetProjectCode(param.payment_mode, Int32.Parse(xls_money));

            //作廢
            string b2_1 = param.in_meeting_code.Substring(param.in_meeting_code.Length - 4);

            //業主自訂編號: [賽事/講習序號(後3碼)] + 自訂流水號(4碼)(預設為0001)
            //[運動賽事序號(後3碼)]
            string b2_2 = param.in_meeting_code.Substring(param.in_meeting_code.Length - 3);

            string index_number_prefix = b2_2.TrimStart('0');

            //找出該[賽事]繳費單最大的[業主自訂編號]
            sql = "SELECT MAX(index_number)";
            sql += " FROM In_Meeting_pay WITH(NOLOCK)";
            sql += " WHERE index_number LIKE '" + index_number_prefix + "%'";
            Item itmIndexNumber = cfg.inn.applySQL(sql);

            if (itmIndexNumber.getResult() == "")
            {
                param.barcode2 = b2_1 + "0001";
                param.barcode2_1 = b2_2 + "0001";
            }
            else
            {
                //若不是第一張單則將最大的[業主自訂編號]+1
                int number_next = Int32.Parse(itmIndexNumber.getProperty("column1", "0")) + 1;
                param.index_number = number_next.ToString();
                string suffix = param.index_number.Substring(param.index_number.Length - 4);

                param.barcode2 = b2_1 + suffix;
                param.barcode2_1 = b2_2 + suffix;
            }

            //這是超商繳費
            //第一碼(最後收費日期(DateTime),項目代碼(String))
            param.B1 = BarCode1_OutPut(in_pay_date_exp1, param.project_code);
            //第二碼(賽事編號(String),業者自訂編號(String))
            param.B2 = param.in_receice_org_code + param.barcode2_1;

            string checkhum = cfg.inn.applyMethod("In_GetRank_CheckSum", "<account>" + param.B2 + "</account><pay>" + xls_money + "</pay>").getResult();

            param.B2 = param.B2 + checkhum;

            //第三碼(應繳日期(DateTime),費用(String),一碼(String),二碼(String))
            param.B3 = BarCode3_OutPut(in_pay_date_exp, xls_money, param.B1, param.B2);

            //產生繳費單
            Item SingleNumber = cfg.inn.newItem("In_Meeting_pay", "add");

            SingleNumber.setProperty("owned_by_id", cfg.strIdentityId);//建單者
            SingleNumber.setProperty("in_creator", cfg.LoginResume.getProperty("in_name", ""));//協助報名者
            SingleNumber.setProperty("in_creator_sno", cfg.LoginResume.getProperty("in_sno", ""));//協助報名者身分證字號
            SingleNumber.setProperty("in_group", cfg.LoginResume.getProperty("in_group", ""));//所屬群組
            SingleNumber.setProperty("in_committee", "");//所屬跆委會

            SingleNumber.setProperty("in_current_org", cfg.LoginResume.getProperty("in_current_org", ""));//所屬單位
            SingleNumber.setProperty("in_pay_amount_exp", xls_money);//應繳金額

            SingleNumber.setProperty("in_pay_date_exp", due_date);//應繳日期
            SingleNumber.setProperty("in_pay_date_exp1", due_date);//最後收費日期

            SingleNumber.setProperty("in_real_stuff", xls_real_stuff);//隊職員數
            SingleNumber.setProperty("in_real_player", xls_real_player);//選手人數
            SingleNumber.setProperty("in_real_items", xls_real_items);//項目總計

            SingleNumber.setProperty("in_code_1", param.B1);//條碼1
            SingleNumber.setProperty("in_code_2", "00" + param.B2);//條碼2
            SingleNumber.setProperty("in_code_3", param.B3);//條碼3
            SingleNumber.setProperty("pay_bool", "未繳費");//繳費狀態
            SingleNumber.setProperty("in_add_time", param.AddTime);//建單時間
            SingleNumber.setProperty("index_number", param.barcode2_1);//業主自訂編號

            SingleNumber.setProperty("in_meeting", param.meeting_id);

            Item documents = SingleNumber.apply();

            //取出該賽事的關聯繳費
            aml = "<AML>" +
                "<Item type='In_Meeting_pay' action='get'>" +
                "<id>" + documents.getProperty("id", "") + "</id>" +
                "</Item></AML>";

            Item SingleNumbers = cfg.inn.applyAML(aml);

            for (int i = 0; i < pay_list.Count; i++)
            {
                var in_index = (i + 1).ToString().PadLeft(5, '0');

                var entity = pay_list[i];

                //新增到繳費資訊下
                Item in_meeting_news = cfg.inn.newItem("In_Meeting_news", "add");
                in_meeting_news.setProperty("in_ans_l3", entity.mt_title);//組別彙整結果
                in_meeting_news.setProperty("in_pay_amount", entity.in_pay_amount_exp);//應繳金額
                in_meeting_news.setProperty("in_name", entity.item_number);//姓名
                in_meeting_news.setProperty("in_sno", entity.in_code_2);//身分證
                in_meeting_news.setProperty("in_creator", cfg.LoginResume.getProperty("in_name", ""));//協助報名者
                in_meeting_news.setProperty("in_creator_sno", cfg.LoginResume.getProperty("in_sno", ""));//協助報名者帳號
                in_meeting_news.setProperty("in_regdate", "");//報名日期

                in_meeting_news.setProperty("in_l1", entity.item_number);//第一層
                in_meeting_news.setProperty("in_l2", entity.mt_title);//第二層
                in_meeting_news.setProperty("in_l3", "");//第三層
                in_meeting_news.setProperty("in_index", in_index);//序號

                in_meeting_news.setProperty("in_section_name", entity.mt_title);//組名
                in_meeting_news.setProperty("in_l1_sort", "0");//第一層排序

                in_meeting_news.setProperty("in_muid", "");//與會者id

                SingleNumbers.addRelationship(in_meeting_news);
            }

            Item itmResult = SingleNumbers.apply();

            if (itmResult.isError())
            {
                throw new Exception("繳費單明細新增發生錯誤");
            }
        }

        private TPayParam GetPayParam(TConfig cfg)
        {
            var param = new TPayParam
            {
                blank = " ",
                codes = "10",
                in_meeting_type = "",
                in_meeting_code = "",
                in_receice_org_code = "",
                payment_mode = "A",
                project_code = "",
                index_number = "",
                barcode2 = "",
                barcode2_1 = "",
                in_imf_date_e = "",

                B1 = "",
                B2 = "",
                B3 = "",

                CurrentTime = System.DateTime.Today,
                AddTime = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss"),
            };


            param.meeting_id = cfg.itmMeeting.getProperty("id", "");//賽事編號
            param.in_meeting_code = cfg.itmMeeting.getProperty("item_number", "");//賽事編號
            param.in_meeting_type = cfg.itmMeeting.getProperty("in_meeting_type", "").ToLower();//活動類型
            param.in_receice_org_code = cfg.itmMeeting.getProperty("in_receice_org_code", "");//收款單位編號

            if (param.in_receice_org_code == "")
            {
                throw new Exception("收款單位編號不可為空白");
            }
            else if (param.in_receice_org_code.Length != 6)
            {
                throw new Exception("收款單位編號格式錯誤");
            }

            //應繳日期&最後繳費日期為[今日+7天](尚未確定範圍)
            param.due_date = param.CurrentTime.AddDays(7).ToString("yyyy-MM-ddTHH:mm:ss");//應繳日期
            param.last_payment_date = param.CurrentTime.AddDays(7).ToString("yyyy-MM-ddTHH:mm:ss");//最後收費日期

            return param;
        }

        #region 繳費單模組
        private string GetProjectCode(string payment_mode, int money)
        {
            string project_code = "";

            switch (payment_mode)
            {
                case "A":
                    if (money <= 20000)
                    {
                        project_code = "6NR";
                    }
                    else if (money > 20000 && money <= 40000)
                    {
                        project_code = "6NS";
                    }
                    else if (money > 40000 && money <= 60000)
                    {
                        project_code = "6BD";
                    }
                    break;
                case "B":
                    if (money <= 20000)
                    {
                        project_code = "68Q";
                    }
                    else if (money > 20000 && money <= 40000)
                    {
                        project_code = "68R";
                    }
                    else if (money > 40000 && money <= 60000)
                    {
                        project_code = "6BF";
                    }
                    break;
            }

            return project_code;
        }

        //產生條碼**********************************
        int ichecksum1 = 0, ichecksum2 = 0;

        //判斷是否為英文或數字
        public bool IsNumberOREng(string str)
        {
            System.Text.RegularExpressions.Regex reg1 = new System.Text.RegularExpressions.Regex(@"^[A-Za-z0-9$+-.% ]+$");
            return reg1.IsMatch(str);

        }
        //判斷是否為數字
        public bool IsPositive_Number(string str)
        {
            System.Text.RegularExpressions.Regex reg1 = new System.Text.RegularExpressions.Regex(@"^[0-9]*[1-9][0-9]*$");
            return reg1.IsMatch(str);

        }

        //產生第一段條碼(代收期限,代收項目)
        public string BarCode1_OutPut(DateTime date, String num)
        {
            //確認代收項目
            bool bIsMatch;
            bIsMatch = IsNumberOREng(num.ToString());
            if (bIsMatch == false)
            {
                throw new System.ArgumentException("代收項目編號含有未定義之特殊字元。");
            }
            else
            {
                if (num.ToString().Length != 3)
                {
                    throw new System.ArgumentException("代收項目編號長度應為3。");
                }
            }
            if (date < DateTime.Today)
            {
                // throw new System.ArgumentException("代收期限不應小於今日。");
            }

            string strDate = string.Format("{0}{1}{2}", (date.Year - 1911).ToString().Substring(1, 2), date.Month.ToString("00"), date.Day.ToString("00"));
            return strDate + num;
        }

        //產生第二段條碼(業者自訂)
        public string BarCode2_OutPut(String num, String custom)
        {
            //確認業主自訂編號
            bool bIsMatch = false;
            bIsMatch = IsNumberOREng(custom.ToString());
            if (bIsMatch == false)
            {
                throw new System.ArgumentException("業主自訂編號含有未定義之特殊字元。");
            }
            else
            {
                if (custom.ToString().Length > 10)
                {
                    throw new System.ArgumentException("業主自訂編號超出字數限制。");
                }
            }

            string strCustom = custom.PadRight(10, '0');
            return "00" + num + strCustom;
        }

        //產生第三段條碼(應繳日,金額,第一碼,第二碼)
        public string BarCode3_OutPut(DateTime date, string strpay, String barcode1, String barcode2)
        {
            //確認金額
            bool bIsMatch = false;

            if (strpay != "0")
            {
                bIsMatch = IsPositive_Number(strpay);
                if (bIsMatch == false)
                {
                    throw new System.ArgumentException("金額應為正整數。");
                }
                else
                {
                    if (strpay.Length > 9)
                    {
                        throw new System.ArgumentException("金額超過位數限制。");
                    }
                }
            }

            int pay = int.Parse(strpay);

            char[] cBarcode1, cBarcode2;
            char[] cDate, CMoney;
            string strCheckSum1, strCheckSum2;

            cBarcode1 = barcode1.ToUpper().ToCharArray(0, barcode1.Length);
            CharLoop(cBarcode1);
            cBarcode2 = barcode2.ToUpper().ToCharArray(0, barcode2.Length);
            CharLoop(cBarcode2);
            cDate = date.ToString("MMdd").ToCharArray(0, date.ToString("MMdd").Length);
            CharLoop(cDate);
            CMoney = pay.ToString().PadLeft(9, '0').ToCharArray(0, 9);
            CharLoop(CMoney);

            switch (ichecksum1)
            {
                case 0:
                    strCheckSum1 = "A";
                    break;
                case 10:
                    strCheckSum1 = "B";
                    break;
                default:
                    strCheckSum1 = ichecksum1.ToString();
                    break;
            }

            switch (ichecksum2)
            {
                case 0:
                    strCheckSum2 = "X";
                    break;
                case 10:
                    strCheckSum2 = "Y";
                    break;
                default:
                    strCheckSum2 = ichecksum2.ToString();
                    break;
            }
            ichecksum1 = 0;
            ichecksum2 = 0;
            return date.ToString("MMdd") + strCheckSum1 + strCheckSum2 + pay.ToString().PadLeft(9, '0');
        }

        private void CharLoop(char[] value)
        {
            for (int iCount = 0; iCount < value.Length; iCount++)
            {
                //偶數字串
                if (iCount % 2 > 0)
                {
                    ichecksum2 = GetCheckSum(value[iCount], ichecksum2);
                }
                //奇數字串
                else
                {
                    ichecksum1 = GetCheckSum(value[iCount], ichecksum1);
                }
            }
        }

        private int GetCheckSum(char value, int checksum)
        {
            if (Char.IsNumber(value))
            {
                //是數字
                return (checksum + ((int)value - 48)) % 11;
            }
            else
            {
                //是英文
                switch (value)
                {
                    case 'A':
                        return (checksum + 1) % 11;
                    case 'B':
                        return (checksum + 2) % 11;
                    case 'C':
                        return (checksum + 3) % 11;
                    case 'D':
                        return (checksum + 4) % 11;
                    case 'E':
                        return (checksum + 5) % 11;
                    case 'F':
                        return (checksum + 6) % 11;
                    case 'G':
                        return (checksum + 7) % 11;
                    case 'H':
                        return (checksum + 8) % 11;
                    case 'I':
                        return (checksum + 9) % 11;
                    case 'J':
                        return (checksum + 1) % 11;
                    case 'K':
                        return (checksum + 2) % 11;
                    case 'L':
                        return (checksum + 3) % 11;
                    case 'M':
                        return (checksum + 4) % 11;
                    case 'N':
                        return (checksum + 5) % 11;
                    case 'O':
                        return (checksum + 6) % 11;
                    case 'P':
                        return (checksum + 7) % 11;
                    case 'Q':
                        return (checksum + 8) % 11;
                    case 'R':
                        return (checksum + 9) % 11;
                    case 'S':
                        return (checksum + 2) % 11;
                    case 'T':
                        return (checksum + 3) % 11;
                    case 'U':
                        return (checksum + 4) % 11;
                    case 'V':
                        return (checksum + 5) % 11;
                    case 'W':
                        return (checksum + 6) % 11;
                    case 'X':
                        return (checksum + 7) % 11;
                    case 'Y':
                        return (checksum + 8) % 11;
                    case 'Z':
                        return (checksum + 9) % 11;
                    case '+':
                        return (checksum + 1) % 11;
                    case '%':
                        return (checksum + 2) % 11;
                    case '-':
                        return (checksum + 6) % 11;
                    case '.':
                        return (checksum + 7) % 11;
                    case ' ':
                        return (checksum + 8) % 11;
                    case '$':
                        return (checksum + 9) % 11;
                    case '/':
                        return (checksum + 0) % 11;
                    default:
                        return (checksum + 0) % 11;
                }
                return (checksum + ((((int)value - 65) % 9) + 1) % 11);
            }
        }
        #endregion 繳費單模組

        #region 頁面

        private void Page(TConfig cfg, Item itmReturn)
        {
            List<TPay> list1 = GetPaymentList(cfg, is_merge: false);
            AppendGymPayList(cfg, list1, is_merge: false);
            string table_contens1 = TabelContents(cfg, list1, "payment_table", is_merge: false);
            itmReturn.setProperty("inn_table", table_contens1);

            List<TPay> list2 = GetPaymentList(cfg, is_merge: true);
            AppendGymPayList(cfg, list2, is_merge: true);
            string table_contens2 = TabelContents(cfg, list2, "merge_table", is_merge: true);
            itmReturn.setProperty("inn_table2", table_contens2);
        }

        private string TabelContents(TConfig cfg, List<TPay> list, string table_name, bool is_merge = false)
        {
            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            head.Append("<thead>");
            head.Append("<tr>");
            if (!is_merge)
            {
                head.Append("  <th class='text-center' data-field='selected'  data-checkbox='true'></th>");
            }
            head.Append("  <th class='text-center' data-field='item_number'>繳費單號</th>");
            head.Append("  <th class='text-center' data-field='mt_name'>項目名稱</th>");
            head.Append("  <th class='text-center' data-field='in_code_2'>條碼2</th>");
            head.Append("  <th class='text-center' data-field='pay_bool'>繳費狀態</th>");
            head.Append("  <th class='text-center' data-field='pay_amount'>金額</th>");
            head.Append("  <th class='text-center' data-field='in_current_org'>所屬單位</th>");
            head.Append("  <th class='text-center' data-field='in_real_items'>項目數量</th>");

            if (!is_merge)
            {
                head.Append("  <th class='text-center' data-field='in_real_player'>選手人數</th>");
            }
            else
            {
                head.Append("  <th class='text-center' data-field='inn_func'>功能</th>");
            }

            head.Append("</tr>");
            head.Append("</thead>");

            body.Append("<tbody>");

            for (int i = 0; i < list.Count; i++)
            {
                var entity = list[i];
                Item item = entity.Value;

                body.Append("<tr>");
                if (!is_merge)
                {
                    body.Append("  <td class='text-center' data-field='selected'></td>");
                }
                body.Append("  <td class='text-center' data-field='item_number'>" + item.getProperty("item_number", "") + "</td>");
                body.Append("  <td class='text-left' data-field='mt_name'>" + entity.mt_title + "</td>");
                body.Append("  <td class='text-left' data-field='in_code_2'>" + item.getProperty("in_code_2", "") + "</td>");
                body.Append("  <td class='text-center' data-field='pay_bool'>" + GetPayStatus(cfg, entity) + "</td>");
                body.Append("  <td class='text-right' data-field='pay_amount'>" + entity.pay_amount_exp.ToString("N0") + "</td>");
                body.Append("  <td class='text-center' data-field='in_current_org'>" + item.getProperty("in_current_org", "") + "</td>");
                body.Append("  <td class='text-right' data-field='in_real_items'>" + item.getProperty("in_real_items", "") + "</td>");

                if (!is_merge)
                {
                    body.Append("  <td class='text-right' data-field='in_real_player'>" + item.getProperty("in_real_player", "") + "</td>");
                }
                else
                {
                    body.Append("  <td class='text-center' data-field='inn_func'>" + GetButtons(cfg, entity) + "</td>");
                }

                body.Append("</tr>");
            }
            body.Append("</tbody>");

            StringBuilder builder = new StringBuilder();
            builder.AppendLine(GetTableAttribute(table_name));
            builder.Append(head);
            builder.Append(body);
            builder.AppendLine("</table>");
            builder.AppendLine("<script>");
            builder.AppendLine("$('#" + table_name + "').bootstrapTable();");
            builder.AppendLine("if ($(window).width() <= 768) { $('#" + table_name + "').bootstrapTable('toggleView'); }");
            builder.AppendLine("</script>");
            return builder.ToString();
        }

        private string GetButtons(TConfig cfg, TPay entity)
        {
            string result = "<button class='btn btn-sm btn-danger'"
                + " data-id='" + entity.id + "'"
                + " onclick='CancelMerge_Click(this)'"
                + ">取消合併</button>";
            return result;
        }
        private string GetPayStatus(TConfig cfg, TPay entity)
        {
            string href = "";

            if (entity.is_game)
            {
                href = "../pages/c.aspx"
                    + "?page=detail_list_n1.html"
                    + "&method=In_Payment_DetailList"
                    + "&meeting_id=" + entity.mt_id
                    + "&item_number=" + entity.item_number;
            }
            else
            {
                href = "../pages/c.aspx"
                    + "?page=Cla_detail_list_n1.html"
                    + "&method=In_Cla_Payment_DetailList"
                    + "&meeting_id=" + entity.mt_id
                    + "&item_number=" + entity.item_number;
            }

            return "<a target='_blank' href='" + href + "'>"
                + "<span style='color:red;'><i class='fa fa-list-alt'></i>未繳費</span>"
                + "</a>";
        }

        private string GetTableAttribute(string table_name)
        {
            return "<table id='" + table_name + "' class='table table-hover table-bordered table-rwd rwd rwdtable' "
                + " data-toggle='table' "
                + " data-click-to-select='true' "
                + " data-select-item-name='selectItem'"
                + " data-striped='false' "
                + " data-pagination='false' "
                + " data-show-pagination-switch='false'"
                + ">";
        }

        #endregion 頁面

        private List<TPay> GetPaymentList(TConfig cfg, bool is_merge = false)
        {

            string sql = @"
                SELECT 
	                t1.*
	                , t2.in_title
                    , t2.in_receice_org_code
                    , t2.in_meeting_type
	                , t3.in_title            AS 'cla_title'
	                , t3.in_receice_org_code AS 'cla_receice_org_code'
                    , t3.in_meeting_type     AS 'cla_meeting_type'
                FROM
	                IN_MEETING_PAY t1 WITH(NOLOCK)
                LEFT OUTER JOIN
	                IN_MEETING t2 WITH(NOLOCK)
	                ON t2.id = t1.in_meeting
                LEFT OUTER JOIN
	                IN_CLA_MEETING t3 WITH(NOLOCK)
	                ON t3.id = t1.in_cla_meeting
                WHERE
	                t1.in_creator_sno = '{#in_creator_sno}'
	                AND t1.pay_bool = N'未繳費'
                ORDER BY
	                t1.item_number
            ";

            sql = sql.Replace("{#in_creator_sno}", cfg.LoginResume.getProperty("in_sno", ""));

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item items = cfg.inn.applySQL(sql);

            List<TPay> list = new List<TPay>();

            AppendPayList(cfg, list, items, is_merge);

            return list;
        }

        //附加轄下道館繳費單
        private void AppendGymPayList(TConfig cfg, List<TPay> list, bool is_merge = false)
        {
            string sql = @"
                SELECT 
	                t1.*
	                , t2.in_title
                    , t2.in_receice_org_code
                    , t2.in_meeting_type
	                , t3.in_title            AS 'cla_title'
	                , t3.in_receice_org_code AS 'cla_receice_org_code'
                    , t3.in_meeting_type     AS 'cla_meeting_type'
                FROM
	                IN_MEETING_PAY t1 WITH(NOLOCK)
                LEFT OUTER JOIN
	                IN_MEETING t2 WITH(NOLOCK)
	                ON t2.id = t1.in_meeting
                LEFT OUTER JOIN
	                IN_CLA_MEETING t3 WITH(NOLOCK)
	                ON t3.id = t1.in_cla_meeting
                INNER JOIN
                    IN_RESUME t11 WITH(NOLOCK)
                    ON t11.in_sno = t1.in_creator_sno
                WHERE
	                t11.in_principal_sno = '{#in_creator_sno}'
	                AND t1.pay_bool = N'未繳費'
                ORDER BY
	                t1.item_number
            ";

            sql = sql.Replace("{#in_creator_sno}", cfg.LoginResume.getProperty("in_sno", ""));

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item items = cfg.inn.applySQL(sql);

            AppendPayList(cfg, list, items, is_merge);
        }

        private void AppendPayList(TConfig cfg, List<TPay> list, Item items, bool is_merge)
        {
            string in_receice_org_code = cfg.itmMeeting.getProperty("in_receice_org_code", "");

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_meeting = item.getProperty("in_meeting", "");

                var entity = new TPay
                {
                    id = item.getProperty("id", ""),
                    item_number = item.getProperty("item_number", ""),
                    in_code_2 = item.getProperty("in_code_2", ""),
                    in_pay_amount_exp = item.getProperty("in_pay_amount_exp", "0"),
                    in_pay_date_exp = item.getProperty("in_pay_date_exp", ""),
                    Value = item,
                };

                entity.pay_amount_exp = GetIntVal(entity.in_pay_amount_exp);
                entity.pay_date_exp = GetDateTime(entity.in_pay_date_exp);

                if (in_meeting != "")
                {
                    entity.mt_id = item.getProperty("in_meeting", "");
                    entity.mt_title = item.getProperty("in_title", "");
                    entity.mt_receice_org_code = item.getProperty("in_receice_org_code", "");
                    entity.mt_type = item.getProperty("in_meeting_type", "");
                    entity.is_game = true;
                }
                else
                {
                    entity.mt_id = item.getProperty("in_cla_meeting", "");
                    entity.mt_title = item.getProperty("cla_title", "");
                    entity.mt_receice_org_code = item.getProperty("cla_receice_org_code", "");
                    entity.mt_type = item.getProperty("cla_meeting_type", "");
                    entity.is_game = false;
                }

                //收款單位編號 必須與 Meeting 吻合
                if (entity.mt_receice_org_code != in_receice_org_code)
                {
                    continue;
                }

                if (is_merge)
                {
                    //合併類型的繳費單
                    if (entity.mt_type == "merge")
                    {
                        list.Add(entity);
                    }
                }
                else
                {
                    if (entity.mt_type != "merge")
                    {
                        list.Add(entity);
                    }
                }
            }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }

            public string code { get; set; }
            public string scene { get; set; }

            public bool isMeetingAdmin { get; set; }
            public bool isCommittee { get; set; }
            public bool isGymOwner { get; set; }
            public bool isGymAssistant { get; set; }

            public Item itmMeeting { get; set; }
            public Item LoginResume { get; set; }
        }

        private class TPayParam
        {
            /// <summary>
            /// 活動 id
            /// </summary>
            public string meeting_id { get; set; }

            /// <summary>
            /// 活動類型
            /// </summary>
            public string in_meeting_type { get; set; }

            /// <summary>
            /// 活動編號
            /// </summary>
            public string in_meeting_code { get; set; }

            /// <summary>
            /// 收款單位編號
            /// </summary>
            public string in_receice_org_code { get; set; }

            /// <summary>
            /// 最後報名日期
            /// </summary>
            public string in_imf_date_e { get; set; }

            /// <summary>
            /// 最後繳費期限
            /// </summary>
            public string last_payment_date { get; set; }

            /// <summary>
            /// 最後繳費期限
            /// </summary>
            public string due_date { get; set; }

            /// <summary>
            /// 隊職員數
            /// </summary>
            public int in_staffs { get; set; }

            /// <summary>
            /// 選手人數
            /// </summary>
            public int in_players { get; set; }

            /// <summary>
            /// 項目總計
            /// </summary>
            public int in_items { get; set; }

            /// <summary>
            /// 費用
            /// </summary>
            public int in_expense { get; set; }

            /// <summary>
            /// 空白
            /// </summary>
            public string blank { get; set; }

            /// <summary>
            /// 備用碼(1)+運動代碼(0)
            /// </summary>
            public string codes { get; set; }

            /// <summary>
            /// 狀態(A>繳款人 B>委託單位)
            /// </summary>
            public string payment_mode { get; set; }

            /// <summary>
            /// 業主自訂編號
            /// </summary>
            public string index_number { get; set; }

            /// <summary>
            /// 條碼2
            /// </summary>
            public string barcode2 { get; set; }

            /// <summary>
            /// 臨櫃繳費虛擬帳號
            /// </summary>
            public string barcode2_1 { get; set; }

            /// <summary>
            /// 項目代碼
            /// </summary>
            public string project_code { get; set; }

            /// <summary>
            /// 條碼1
            /// </summary>
            public string B1 { get; set; }

            /// <summary>
            /// 條碼2
            /// </summary>
            public string B2 { get; set; }

            /// <summary>
            /// 條碼3
            /// </summary>
            public string B3 { get; set; }

            /// <summary>
            /// 當前時間
            /// </summary>
            public DateTime CurrentTime { get; set; }

            /// <summary>
            /// 建單時間
            /// </summary>
            public string AddTime { get; set; }
        }

        private class TPay
        {
            public string id { get; set; }
            public string item_number { get; set; }
            public string in_code_2 { get; set; }
            public string in_pay_amount_exp { get; set; }
            public string in_pay_date_exp { get; set; }

            public int pay_amount_exp { get; set; }
            public DateTime pay_date_exp { get; set; }

            public bool is_game { get; set; }
            public string mt_id { get; set; }
            public string mt_title { get; set; }
            public string mt_type { get; set; }
            public string mt_receice_org_code { get; set; }

            public Item Value { get; set; }
        }

        private DateTime GetDateTime(string value, int hours = 8)
        {
            if (value == "") return DateTime.MinValue;

            DateTime result = DateTime.MinValue;
            DateTime.TryParse(value, out result);
            return result.AddHours(hours);
        }

        private int GetIntVal(string value)
        {
            if (value == "" || value == "0") return 0;
            int result = 0;
            int.TryParse(value, out result);

            return result;
        }
    }
}