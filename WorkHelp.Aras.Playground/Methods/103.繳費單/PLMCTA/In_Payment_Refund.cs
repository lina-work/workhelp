﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;
using System.Net;

namespace WorkHelp.Aras.Playground.Methods.PLMCTA
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class In_Payment_Refund : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 退費申請
                頁面: 
                    - detail_list_refund.html
                    - detail_list_refund_modal.html
                輸入: 
                    - meeting_id
                    - item_number
                    - scene
                日期:
                    - 2022-01-24 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]" + "In_Payment_Refund";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                strIdentityId = inn.getUserAliases(),

                item_number = itmR.getProperty("item_number", ""),
                scene = itmR.getProperty("scene", ""),
            };

            cfg.itmLogin = cfg.inn.applySQL("SELECT * FROM IN_RESUME WITH(NOLOCK) WHERE in_user_id = '" + cfg.strUserId + "'");

            cfg.itmMtPay = cfg.inn.newItem("In_Meeting_Pay", "get");
            cfg.itmMtPay.setProperty("item_number", cfg.item_number);
            cfg.itmMtPay = cfg.itmMtPay.apply();

            if (cfg.itmMtPay.isError() || cfg.itmMtPay.getResult() == "")
            {
                throw new Exception("查無繳費單資料");
            }

            cfg.pay_id = cfg.itmMtPay.getProperty("id", "");
            cfg.in_meeting = cfg.itmMtPay.getProperty("in_meeting", "");
            cfg.in_cla_meeting = cfg.itmMtPay.getProperty("in_cla_meeting", "");

            cfg.MtType = "IN_MEETING";
            cfg.MtId = cfg.in_meeting;
            cfg.MtMode = "";
            if (cfg.in_cla_meeting != "")
            {
                cfg.MtType = "IN_CLA_MEETING";
                cfg.MtId = cfg.in_cla_meeting;
                cfg.MtMode = "cla";
            }

            cfg.itmMeeting = cfg.inn.applySQL("SELECT in_title, in_member_type FROM " + cfg.MtType + " WITH(NOLOCK) WHERE id = '" + cfg.MtId + "'");
            cfg.in_member_type = cfg.itmMeeting.getProperty("in_member_type", "");
            cfg.is_team_bill = cfg.in_member_type == "團隊計費";

            switch (cfg.scene)
            {
                case "verify_page"://頁面: 各組審核
                    AssVerifyPage(cfg, itmR, is_sect: true, is_fd: false);
                    itmR.setProperty("hide_ass_box", "item_show_1");
                    itmR.setProperty("hide_fd_box", "item_show_0");
                    break;

                case "fd_page"://頁面: 財務審核
                    AssVerifyPage(cfg, itmR, is_sect: false, is_fd: true);
                    itmR.setProperty("hide_ass_box", "item_show_0");
                    itmR.setProperty("hide_fd_box", "item_show_1");
                    if (cfg.is_team_bill)
                    {
                        itmR.setProperty("hide_team_detail_btn", "item_show_1");
                    }
                    else
                    {
                        itmR.setProperty("hide_team_detail_btn", "item_show_0");
                    }
                    break;

                case "verify"://各組審核
                    AssVerify(cfg, itmR);
                    break;

                case "fd_verify"://財務審核 (FD: Finance Department)
                    FinanceVerify(cfg, itmR);
                    break;

                case "apply":
                    //更新繳費單明細
                    UpdateMtNews(cfg, itmR);
                    //建立退費申請書
                    MergeRefund(cfg, itmR);
                    break;

                case "edit_modal"://跳窗: 申請 or 修改
                    EditModal(cfg, itmR);
                    break;
            }

            return itmR;
        }

        //財務審核
        private void FinanceVerify(TConfig cfg, Item itmReturn)
        {
            string now_time = System.DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
            string in_amount_real = itmReturn.getProperty("in_amount_real", "0");
            string in_amount_real_tw = itmReturn.getProperty("in_amount_real_tw", "");
            string in_amount_real_photo = itmReturn.getProperty("in_amount_real_photo", "");

            string in_fd_ver_result = itmReturn.getProperty("in_fd_ver_result", "");
            string in_fd_ver_memo = itmReturn.getProperty("in_fd_ver_memo", "");

            Item itmMtRefund = cfg.inn.newItem("In_Meeting_Refund", "merge");
            itmMtRefund.setAttribute("where", "source_id = '" + cfg.pay_id + "'");
            itmMtRefund.setProperty("in_amount_real", in_amount_real);
            itmMtRefund.setProperty("in_amount_real_tw", in_amount_real_tw);
            itmMtRefund.setProperty("in_amount_real_photo", in_amount_real_photo);
            itmMtRefund.setProperty("in_fd_ver_time", now_time);
            itmMtRefund.setProperty("in_fd_ver_result", in_fd_ver_result);
            itmMtRefund.setProperty("in_fd_ver_memo", in_fd_ver_memo);
            itmMtRefund.setProperty("in_fd_ver_by_id", cfg.strIdentityId);

            Item itmResult = itmMtRefund.apply();

            if (itmResult.isError())
            {
                throw new Exception("財務審核發生錯誤");
            }

            string in_pay_amount_real = cfg.itmMtPay.getProperty("in_pay_amount_real", "0");
            decimal pay_amount_real = GetDcmVal(in_pay_amount_real);
            decimal ref_amount_real = GetDcmVal(in_amount_real);
            decimal fee = pay_amount_real - ref_amount_real;
            if (fee < 0) fee = 0;


            string sql = "UPDATE IN_MEETING_PAY SET"
                + " in_refund_amount = '" + ref_amount_real + "'"
                + ", in_fee_amount = '" + fee + "'"
                + " WHERE id = '" + cfg.pay_id + "'";

            Item itmMtPayResult = cfg.inn.applySQL(sql);

            if (itmMtPayResult.isError())
            {
                throw new Exception("更新繳費單退款金額發生錯誤");
            }
        }

        private void AssVerify(TConfig cfg, Item itmReturn)
        {
            string now_time = System.DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
            string in_ass_ver_result = itmReturn.getProperty("in_ass_ver_result", "");
            string in_ass_ver_memo = itmReturn.getProperty("in_ass_ver_memo", "");

            Item itmMtRefund = cfg.inn.newItem("In_Meeting_Refund", "merge");
            itmMtRefund.setAttribute("where", "source_id = '" + cfg.pay_id + "'");
            itmMtRefund.setProperty("in_ass_ver_time", now_time);
            itmMtRefund.setProperty("in_ass_ver_result", in_ass_ver_result);
            itmMtRefund.setProperty("in_ass_ver_memo", in_ass_ver_memo);
            itmMtRefund.setProperty("in_ass_ver_by_id", cfg.strIdentityId);

            Item itmResult = itmMtRefund.apply();

            if (itmResult.isError())
            {
                throw new Exception("建立退費申請書發生錯誤");
            }

            string sql = "SELECT * FROM IN_MEETING_NEWS WITH(NOLOCK)"
                + " WHERE source_id = '" + cfg.pay_id + "'"
                + " AND in_cancel_status = N'退費申請中'"
                + " ORDER BY in_section_name, in_index";

            Item itmDetails = cfg.inn.applySQL(sql);
            int count = itmDetails.getItemCount();
            string last_key = "";

            for (int i = 0; i < count; i++)
            {
                Item itmDetail = itmDetails.getItemByIndex(i);
                string in_section_name = itmDetail.getProperty("in_section_name", "");
                string in_index = itmDetail.getProperty("in_index", "");
                string in_pay_amount = itmDetail.getProperty("in_pay_amount", "0");
                string key = in_section_name + "-" + in_index;

                if (key != last_key)
                {
                    last_key = key;
                    itmDetail.setType("IN_MEETING_NEWS");
                    itmDetail.setProperty("news_id", itmDetail.getProperty("id", ""));
                    itmDetail.setProperty("meeting_id", cfg.MtId);
                    itmDetail.setProperty("in_verify_memo", in_ass_ver_memo);
                    itmDetail.setProperty("refund_type", in_ass_ver_result);
                    itmDetail.setProperty("r_amt", in_pay_amount);
                    itmDetail.setProperty("mode", cfg.MtMode);
                    Item itmDataResult = itmDetail.apply("In_Payment_RefundReview");
                }
            }
        }

        //頁面: 各組審核
        private void AssVerifyPage(TConfig cfg, Item itmReturn, bool is_sect = false, bool is_fd = false)
        {
            EditModal(cfg, itmReturn, is_sect, is_fd);

            string sql = @"
                SELECT * FROM IN_MEETING_NEWS WITH(NOLOCK) 
                WHERE source_id = '{#pay_id}'
                AND in_cancel_status IN (N'退費申請中', N'退費申請通過', N'退費申請不通過')
                ORDER BY in_cancel_time, in_l1, in_index
            ";

            sql = sql.Replace("{#pay_id}", cfg.pay_id);

            var items = cfg.inn.applySQL(sql);

            var list = MapSectList(cfg, items);

            for (int i = 0; i < list.Count; i++)
            {
                var entity = list[i];

                var item = entity.Value;
                item.setType("inn_detail");
                item.setProperty("in_names", string.Join(", ", entity.names));

                if (cfg.is_team_bill)
                {
                    item.setProperty("in_pay_amount", cfg.in_member_type);
                }

                itmReturn.addRelationship(item);
            }
        }

        private List<TSect> MapSectList(TConfig cfg, Item items)
        {
            List<TSect> result = new List<TSect>();

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_section_name = item.getProperty("in_section_name", "");
                string in_index = item.getProperty("in_index", "");
                string in_pay_amount = item.getProperty("in_pay_amount", "0");
                string in_cancel_status = item.getProperty("in_cancel_status", "0");
                string in_name = item.getProperty("in_name", "");

                string key = in_section_name + "-" + in_index;

                var sect = result.Find(x => x.key == key);

                if (sect == null)
                {
                    sect = new TSect
                    {
                        key = key,
                        in_section_name = in_index,
                        in_index = in_index,
                        in_pay_amount = in_pay_amount,
                        in_cancel_status = in_cancel_status,
                        names = new List<string>(),
                        Value = item,
                    };

                    result.Add(sect);
                }

                sect.names.Add(in_name);
            }

            return result;
        }

        private class TSect
        {
            public string key { get; set; }
            public string in_section_name { get; set; }
            public string in_index { get; set; }
            public string in_pay_amount { get; set; }
            public string in_cancel_status { get; set; }
            public List<string> names { get; set; }
            public Item Value { get; set; }
        }

        //建立退費申請書
        private void MergeRefund(TConfig cfg, Item itmReturn)
        {
            Item itmMtRefund = cfg.inn.newItem("In_Meeting_Refund", "merge");
            itmMtRefund.setAttribute("where", "source_id = '" + cfg.pay_id + "'");

            itmMtRefund.setProperty("source_id", cfg.pay_id);
            itmMtRefund.setProperty("in_name", itmReturn.getProperty("in_name", ""));
            itmMtRefund.setProperty("in_sno", itmReturn.getProperty("in_sno", ""));
            itmMtRefund.setProperty("in_tel", itmReturn.getProperty("in_tel", ""));
            //itmMtRefund.setProperty("in_name", cfg.itmLogin.getProperty("in_name", ""));
            //itmMtRefund.setProperty("in_sno", cfg.itmLogin.getProperty("in_sno", ""));
            //itmMtRefund.setProperty("in_tel", cfg.itmLogin.getProperty("in_tel", ""));

            itmMtRefund.setProperty("in_reason", itmReturn.getProperty("in_reason", ""));
            itmMtRefund.setProperty("in_amount_exp", GetTotalRefund(cfg));
            itmMtRefund.setProperty("in_bank_account_name", itmReturn.getProperty("in_bank_account_name", ""));
            itmMtRefund.setProperty("in_bank_account_sno", itmReturn.getProperty("in_bank_account_sno", ""));
            itmMtRefund.setProperty("in_bank_code", itmReturn.getProperty("in_bank_code", ""));
            itmMtRefund.setProperty("in_bank_account_number", itmReturn.getProperty("in_bank_account_number", ""));

            itmMtRefund.setProperty("in_bank_branch", itmReturn.getProperty("in_bank_branch", ""));
            itmMtRefund.setProperty("in_bank_post_office", itmReturn.getProperty("in_bank_post_office", ""));
            itmMtRefund.setProperty("in_bank_account_photo", itmReturn.getProperty("in_bank_account_photo", ""));

            itmMtRefund.setProperty("in_note", itmReturn.getProperty("in_note", ""));

            //資料重送，審核狀態變更為未審核
            itmMtRefund.setProperty("in_ass_ver_result", "");

            Item itmResult = itmMtRefund.apply();

            if (itmResult.isError())
            {
                throw new Exception("建立退費申請書發生錯誤");
            }
        }

        //更新繳費單明細
        private void UpdateMtNews(TConfig cfg, Item itmReturn)
        {
            string sql = "";

            string pay_section_name = itmReturn.getProperty("pay_section_name", "").Trim(',');//已選組別
            string[] pay_section_names = pay_section_name.Split(',');

            string pay_index = itmReturn.getProperty("pay_index", "").Trim(',');//序號
            string[] pay_indexs = pay_index.Split(',');

            string pay_type = itmReturn.getProperty("pay_type", "");//申請類型
            string in_cancel_status = pay_type + "申請中";
            string now_time = DateTime.Now.AddHours(-8).ToString("yyyy-MM-ddTHH:mm:ss");//當前時間(SQL用-8H)

            string in_cancel_memo = itmReturn.getProperty("in_cancel_memo", " ");//申請說明
            string in_new_levels = itmReturn.getProperty("in_new_levels", "");//新組別

            for (int i = 0; i < pay_section_names.Count(); i++)
            {
                //更新該張單底下的[繳費資訊]
                sql = "UPDATE IN_MEETING_NEWS SET"
                    + " in_cancel_status = N'" + in_cancel_status + "'"
                    + ", in_cancel_by_id = '" + cfg.strIdentityId + "'"
                    + ", in_cancel_time = '" + now_time + "'"
                    + ", in_cancel_memo = N'" + in_cancel_memo + "'"
                    + ", in_new_levels = N'" + in_new_levels + "'"
                    + " WHERE source_id = '" + cfg.pay_id + "'"
                    + " AND in_section_name = N'" + pay_section_names[i] + "'"
                    + " AND in_index = '" + pay_indexs[i] + "'";

                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

                Item itmResult = cfg.inn.applySQL(sql);

                if (itmResult.isError())
                {
                    throw new Exception("繳費單明細更新退費資料發生錯誤");
                }
            }
        }

        private string GetTotalRefund(TConfig cfg)
        {
            string in_member_type = cfg.itmMeeting.getProperty("in_member_type", "");

            if (in_member_type == "團隊計費")
            {
                return "0";
            }

            string sql = @"
                SELECT 
                	SUM(t1.in_pay_amount) AS 'total_refund'
                FROM
                (
                	SELECT DISTINCT in_index, in_pay_amount
                	FROM IN_MEETING_NEWS WITH(NOLOCK)
                	WHERE source_id = '{#pay_id}'
                	AND in_cancel_status IN (N'退費申請中', N'退費申請通過')
                ) t1
            ";

            sql = sql.Replace("{#pay_id}", cfg.pay_id);

            Item itmData = cfg.inn.applySQL(sql);

            if (itmData.isError() || itmData.getResult() == "")
            {
                return "0";
            }
            else
            {
                string total_refund = itmData.getProperty("total_refund", "0");
                return total_refund;
            }
        }

        //跳窗: 申請 or 修改
        private void EditModal(TConfig cfg, Item itmReturn, bool is_sect = false, bool is_fd = false)
        {
            //目前僅一筆
            Item itmRefund = cfg.inn.newItem("In_Meeting_Refund", "get");
            itmRefund.setProperty("source_id", cfg.pay_id);
            itmRefund = itmRefund.apply();

            if (!itmRefund.isError() && itmRefund.getResult() != "")
            {
                //已有申請資料
                itmReturn.setProperty("in_name", itmRefund.getProperty("in_name", ""));
                itmReturn.setProperty("in_sno", itmRefund.getProperty("in_sno", ""));
                itmReturn.setProperty("in_tel", itmRefund.getProperty("in_tel", ""));

                itmReturn.setProperty("in_reason", itmRefund.getProperty("in_reason", ""));
                itmReturn.setProperty("in_amount_exp", itmRefund.getProperty("in_amount_exp", ""));
                itmReturn.setProperty("in_bank_account_name", itmRefund.getProperty("in_bank_account_name", ""));
                itmReturn.setProperty("in_bank_account_sno", itmRefund.getProperty("in_bank_account_sno", ""));
                itmReturn.setProperty("in_bank_code", itmRefund.getProperty("in_bank_code", ""));
                itmReturn.setProperty("in_bank_account_number", itmRefund.getProperty("in_bank_account_number", ""));

                itmReturn.setProperty("in_bank_branch", itmRefund.getProperty("in_bank_branch", ""));
                itmReturn.setProperty("in_bank_post_office", itmRefund.getProperty("in_bank_post_office", ""));
                itmReturn.setProperty("in_bank_account_photo", itmRefund.getProperty("in_bank_account_photo", ""));
                itmReturn.setProperty("in_note", itmRefund.getProperty("in_note", ""));

                if (is_sect)
                {
                    itmReturn.setProperty("in_ass_ver_result", itmRefund.getProperty("in_ass_ver_result", ""));
                    itmReturn.setProperty("in_ass_ver_memo", itmRefund.getProperty("in_ass_ver_memo", ""));
                }

                if (is_fd)
                {
                    itmReturn.setProperty("in_amount_real", itmRefund.getProperty("in_amount_real", ""));
                    itmReturn.setProperty("in_amount_real_tw", itmRefund.getProperty("in_amount_real_tw", ""));
                    itmReturn.setProperty("in_amount_real_photo", itmRefund.getProperty("in_amount_real_photo", ""));

                    itmReturn.setProperty("in_fd_ver_result", itmRefund.getProperty("in_fd_ver_result", ""));
                    itmReturn.setProperty("in_fd_ver_memo", itmRefund.getProperty("in_fd_ver_memo", ""));
                }
            }
            else
            {
                //抓取登入者
                itmReturn.setProperty("in_name", cfg.itmLogin.getProperty("in_name", ""));
                itmReturn.setProperty("in_sno", cfg.itmLogin.getProperty("in_sno", ""));
                itmReturn.setProperty("in_tel", cfg.itmLogin.getProperty("in_tel", ""));
                itmReturn.setProperty("in_bank_account_name", cfg.itmLogin.getProperty("in_name", ""));
                itmReturn.setProperty("in_bank_account_sno", cfg.itmLogin.getProperty("in_sno", ""));
            }

            //活動名稱
            itmReturn.setProperty("mt_title", cfg.itmMeeting.getProperty("in_title", ""));
            //登入者 id
            itmReturn.setProperty("resume_id", cfg.itmLogin.getProperty("id", ""));

        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }

            public string item_number { get; set; }
            public string scene { get; set; }

            public string MtType { get; set; }
            public string MtId { get; set; }
            public string MtMode { get; set; }

            public Item itmLogin { get; set; }
            public Item itmMeeting { get; set; }
            public Item itmMtPay { get; set; }

            public string pay_id { get; set; }
            public string in_meeting { get; set; }
            public string in_cla_meeting { get; set; }

            public string in_member_type { get; set; }
            public bool is_team_bill { get; set; }
        }

        private decimal GetDcmVal(string value)
        {
            decimal result = 0;
            decimal.TryParse(value, out result);
            return result;
        }
    }
}