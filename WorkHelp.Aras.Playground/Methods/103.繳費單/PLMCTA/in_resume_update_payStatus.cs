﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;
using System.Net;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class in_resume_update_payStatus : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的:更新繳費狀態 相關
                日期:
                    - 2022-01-13 合併繳費單 (lina)
                    - 2021-10-05 調整會員狀態 (lina)
                    - 2021-09-28 17:18 創建 (David)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            // method名稱
            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]in_resume_update_payStatus";

            // 暫時增加權限
            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "this: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                strIdentityId = inn.getUserAliases(),

                id = itmR.getProperty("id", ""),
                model = itmR.getProperty("model", ""),// 模式 未繳費pay/已繳費 payed
                in_pay_date_real = itmR.getProperty("in_pay_date_real", ""),// 繳費日期 yyy(民國年)mmdd
            };

            // cfg.id = "02B15FD1E36548678BA37799E4E08059";
            // cfg.model = "";
            // cfg.in_pay_date_real = "1110118";

            Item itmPay = cfg.inn.newItem("In_Meeting_Pay", "get");
            itmPay.setProperty("id", cfg.id);
            itmPay = itmPay.apply();

            if (itmPay.isError() || itmPay.getResult() == "")
            {
                return itmR;
            }

            string meeting_id = itmPay.getProperty("in_meeting", "");
            if (meeting_id == "")
            {
                return itmR;
            }

            Item itmMeeting = cfg.inn.applySQL("SELECT in_meeting_type FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + meeting_id + "'");
            if (itmMeeting.isError() || itmMeeting.getResult() == "")
            {
                return itmR;
            }

            string in_meeting_type = itmMeeting.getProperty("in_meeting_type", "").Trim().ToLower();

            switch (in_meeting_type)
            {
                case "merge":
                    MergePayment(cfg, itmPay, itmR);
                    break;

                case "payment":
                    YearPayment(cfg, itmR);
                    break;
            }

            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

            return itmR;
        }

        private void MergePayment(TConfig cfg, Item itmPay, Item itmReturn)
        {
            string parent_pay_no = itmPay.getProperty("item_number", "");

            var aml_pay_date_real = itmPay.getProperty("in_pay_date_real", "");
            var dtm_pay_date_real = GetDateTime(aml_pay_date_real, -8);

            //1110121
            var tw_pay_date_real = (dtm_pay_date_real.Year - 1911).ToString().PadLeft(3, '0')
                + dtm_pay_date_real.Month.ToString().PadLeft(2, '0')
                + dtm_pay_date_real.Day.ToString().PadLeft(2, '0');

            //2022-01-21 10:37:22
            var str_pay_date_real = dtm_pay_date_real.ToString("yyyy-MM-dd HH:mm:ss");
            
            string sql = @"
                SELECT 
	                t2.id
	                , t2.pay_bool
	                , t2.in_pay_amount_exp
	                , t2.item_number
                    , t11.in_meeting_type AS 'game_type'
                    , t12.in_meeting_type AS 'class_type'
                FROM 
	                IN_MEETING_NEWS t1 WITH(NOLOCK) 
                INNER JOIN 
	                IN_MEETING_PAY t2 WITH(NOLOCK) 
	                ON t2.item_number = t1.in_l1 
                LEFT OUTER JOIN
                    IN_MEETING t11 WITH(NOLOCK)
                    ON t11.id = t2.in_meeting 
                LEFT OUTER JOIN
                    IN_CLA_MEETING t12 WITH(NOLOCK)
                    ON t12.id = t2.in_cla_meeting
                WHERE 
	                t1.source_id = '{#id}'
            ";

            sql = sql.Replace("{#id}", cfg.id);

            Item items = cfg.inn.applySQL(sql);

            if (items.isError() || items.getResult() == "")
            {
                return;
            }

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string child_pay_id = item.getProperty("id", "");
                string child_pay_no = item.getProperty("item_number", "");
                string child_pay_bool = item.getProperty("pay_bool", "");
                string child_game_type = item.getProperty("game_type", "");

                if (child_pay_bool != "合併")
                {
                    continue;
                }

                //更新被合併的繳費單
                UpdChildPay(cfg, item, parent_pay_no, child_pay_id, child_pay_no, aml_pay_date_real);

                //年費連動
                LinkResumePayYear(cfg, parent_pay_no, child_pay_id, child_pay_no, child_game_type, tw_pay_date_real);

                //更新與會者繳費時間
                UpdMeetingUser(cfg, parent_pay_no, child_pay_no, str_pay_date_real);
            }
        }

        //更新被合併的繳費單
        private void UpdChildPay(TConfig cfg, Item item, string parent_pay_no, string child_pay_id, string child_pay_no, string aml_pay_date_real)
        {
            Item itmOld = cfg.inn.newItem("IN_MEETING_PAY", "merge");
            itmOld.setAttribute("where", "id='" + child_pay_id + "'");
            itmOld.setProperty("id", child_pay_id);
            itmOld.setProperty("pay_bool", "已繳費");
            itmOld.setProperty("in_pay_amount_real", item.getProperty("in_pay_amount_exp", "0"));
            itmOld.setProperty("in_pay_date_real", aml_pay_date_real);
            itmOld.setProperty("in_write_note", "合併繳費: " + parent_pay_no);
            itmOld = itmOld.apply();

            if (itmOld.isError())
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName
                    , "被合併的繳費單狀態變更失敗: " + parent_pay_no + "|" + child_pay_no);
            }
        }

        //年費連動
        private void LinkResumePayYear(TConfig cfg, string parent_pay_no, string child_pay_id, string child_pay_no, string child_game_type, string tw_pay_date_real)
        {
            if (child_game_type != "payment") return;
            
            Item itmData = cfg.inn.newItem("IN_MEETING_PAY");
            itmData.setProperty("id", child_pay_id);
            itmData.setProperty("model", "");
            itmData.setProperty("in_pay_date_real", tw_pay_date_real);

            Item itmMethodResult = itmData.apply("in_resume_update_payStatus");
            
            if (itmMethodResult.isError())
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName
                    , "年費連動失敗 pid: " + parent_pay_no + "|" + child_pay_no);
            }
        }

        //更新與會者繳費時間
        private void UpdMeetingUser(TConfig cfg, string parent_pay_no, string child_pay_no, string str_pay_date_real)
        {
            string sql_upd = "UPDATE IN_MEETING_USER SET"
                + " in_paytime = '" + str_pay_date_real + "'"
                + " WHERE in_paynumber = '" + child_pay_no + "'"
                + " AND ISNULL(in_paytime, '') = ''";

            Item itmUpd = cfg.inn.applySQL(sql_upd);

            if (itmUpd.isError())
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName
                    , "被合併與會者的繳費時間變更失敗: " + parent_pay_no + "|" + child_pay_no);
            }
        }

        private void YearPayment(TConfig cfg, Item itmReturn)
        {
            try
            {
                //繳費資訊
                Item ItemPayListInf = GetPayInfo(cfg);
                //更新 會員繳納資訊
                Update_In_Resume_Pay(cfg, ItemPayListInf, cfg.in_pay_date_real);
                //更新 學員履歷 年度繳費欄位
                Update_In_Resume_YearsPayStatus(cfg, ItemPayListInf, cfg.in_pay_date_real);
                //更新 學員履歷 其他欄位判斷
                Update_In_Resume_OtherColumns(cfg, ItemPayListInf);
            }
            catch (Exception ex)
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "dom: " + itmReturn.dom.InnerXml);
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, ex.ToString());
            }
        }

        private Item GetPayInfo(TConfig cfg)
        {
            string sql = @"
                SELECT
                    t1.item_number              AS 'item_number'
                    , t1.in_pay_amount_exp      AS 'pay_total_mount'
                    , t2.in_l2                  AS 'pay_year'
                    , t2.in_sno                 AS 'in_sno'
                    , t2.in_pay_amount          AS 'pay_mount'
                    , t2.in_ans_l3              AS 'pay_group'
                    , t3.in_meeting_type        AS 'pay_type'
                    , t4.id                     AS 'in_resume_id'
	                , t4.in_name                AS 'in_resume_name'
                    , t4.in_member_type         AS 'resume_member_type'
                    , t4.in_member_status       AS 'resume_member_status'
                FROM 
                    IN_MEETING_PAY t1 WITH(NOLOCK)
                LEFT OUTER JOIN
                    IN_MEETING_NEWS t2 WITH(NOLOCK)
                    ON t2.source_id = t1.id
                INNER JOIN 
                    IN_MEETING t3 WITH(NOLOCK)
                    ON t3.id = t1.in_meeting
                LEFT OUTER JOIN 
                    IN_RESUME t4 WITH(NOLOCK)
	                on t4.in_sno = t2.in_sno
                WHERE 
                    t1.id = '{#pay_id}'
            ";

            sql = sql.Replace("{#pay_id}", cfg.id);

            // cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql:" + sql);

            return cfg.inn.applySQL(sql);
        }

        private Item GetInResumePayInfo(TConfig cfg, string in_resume_id)
        {
            string sql = string.Format(@"
                SELECT 
                    t2.*
                FROM
                    IN_RESUME t1 WITH(NOLOCK)
                LEFT JOIN In_Resume_Pay t2 WITH(NOLOCK)
                    ON t2.source_id = t1.id
                WHERE t1.id = '{0}'
            "
                , in_resume_id
            );

            // cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql:" + sql);

            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// 更新 會員繳納資訊
        /// </summary>
        private void Update_In_Resume_Pay(TConfig cfg, Item itmDetails, string In_pay_date_real)
        {
            int count = itmDetails.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item itmDetail = itmDetails.getItemByIndex(i);
                DateTime pay_date = GetSiWanYear(In_pay_date_real);         // 繳費日期
                string item_number = itmDetail.getProperty("item_number", "");    // 繳費單號
                string pay_year = itmDetail.getProperty("pay_year", "");       // 繳費年度(西元)
                string in_sno = itmDetail.getProperty("in_sno", "");         // 身分證
                string pay_mount = itmDetail.getProperty("pay_mount", "");      // 繳費金額
                string pay_group = itmDetail.getProperty("pay_group", "");      // 繳費組別
                string pay_type = itmDetail.getProperty("pay_type", "");       // 繳費類別
                string in_resume_id = itmDetail.getProperty("in_resume_id", "");   // 講師履歷ID
                string in_resume_name = itmDetail.getProperty("in_resume_name", ""); // 講師履歷 姓名


                string whereConditionString = string.Format(
                    @"
            In_Resume_Pay.[source_id]='{0}' and In_Resume_Pay.[in_year]='{1}' and In_Resume_Pay.[in_paynumber]='{2}' {3}
            "
                    , in_resume_id //講師履歷ID
                    , pay_year //繳費年度
                    , item_number //學員編號
                    , pay_group.Contains("入會費") ? "and In_Resume_Pay.[in_l1]='入會'" : "and ISNULL(In_Resume_Pay.[in_l1], '')=''" //判斷是否有入會費 相關
                );

                string aml = string.Format(
                    @"
            <AML>
                <Item type='In_Resume_Pay' action='merge' where=""{0}"">
                <source_id>{1}</source_id>
                <in_date>{2}</in_date>
                <in_year>{3}</in_year>
                <in_amount>{4}</in_amount>
                <in_paynumber>{5}</in_paynumber>
                {6}
                </Item>
            </AML>

            "
                    , whereConditionString
                    , in_resume_id //講師履歷ID
                    , pay_date.ToString("yyyy-MM-dd") + "T" + pay_date.ToString("HH:mm:ss")//繳費日期
                    , pay_year //繳費年度 
                    , pay_mount //繳費金額
                    , item_number //繳費單號
                    , pay_group.Contains("入會費") ? "<in_l1>入會</in_l1>" : "" //判斷是否有入會費 相關
                );

                // cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, aml);
                Item result = cfg.inn.applyAML(aml);
                // cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, result.dom.InnerXml);

            }
        }

        /// <summary>
        /// 更新 學員履歷 年度時間
        /// </summary>
        private void Update_In_Resume_YearsPayStatus(TConfig cfg, Item itmDetails, string In_pay_date_real)
        {
            int count = itmDetails.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item itmDetail = itmDetails.getItemByIndex(i);
                DateTime pay_date = GetSiWanYear(In_pay_date_real);// 繳費日期
                string item_number = itmDetail.getProperty("item_number", "");//繳費單號
                string pay_year = itmDetail.getProperty("pay_year", "");//繳費年度(西元)
                string in_sno = itmDetail.getProperty("in_sno", "");//身分證
                string pay_mount = itmDetail.getProperty("pay_mount", "");//繳費金額
                string pay_group = itmDetail.getProperty("pay_group", "");      // 繳費組別
                string pay_type = itmDetail.getProperty("pay_type", "");//繳費類別
                string in_resume_id = itmDetail.getProperty("in_resume_id", "");//講師履歷ID
                string in_resume_name = itmDetail.getProperty("in_resume_name", "");//講師履歷 姓名

                //更新欄位
                string year_col = GetYearTo_InResume_ColumnName(pay_year);

                string sql = string.Format(@"UPDATE IN_RESUME SET {1} = '{2}', in_email_exam = N'良好' WHERE id = '{0}';"
                    , in_resume_id //講師履歷ID
                    , year_col
                    , "1" //Update value 
                );

                Item result = cfg.inn.applySQL(sql);
            }
        }

        /// <summary>
        /// 更新 學員履歷 其他欄位判斷
        /// </summary>
        private void Update_In_Resume_OtherColumns(TConfig cfg, Item itmDetails)
        {
            int count = itmDetails.getItemCount();

            List<string> YearsBox = new List<string>();//已繳費年度 + 繳費單繳費年度
            string in_resume_id = ""; // 講師履歷ID
            string pay_total_mount = ""; // 應繳金額
            string item_number = ""; // 繳費單號
            string in_sno = ""; // 身分證
            string in_member_last_year = ""; // 最後繳費年度
            string resume_member_status = ""; // 會員狀態
            string resume_member_type = ""; // 會員類型

            string new_member_status = ""; // 新會員狀態

            bool isHavePaymentData = false;

            //先判斷是否可執行 並且取得必要資訊
            for (int i = 0; i < count; i++)
            {
                Item itmDetail = itmDetails.getItemByIndex(i);
                string pay_type = itmDetail.getProperty("pay_type", "");//繳費類別

                isHavePaymentData = true;
                in_resume_id = itmDetail.getProperty("in_resume_id", "");    // 講師履歷ID
                pay_total_mount = itmDetail.getProperty("pay_total_mount", ""); // 應繳金額
                item_number = itmDetail.getProperty("item_number", "");     // 繳費單號
                in_sno = itmDetail.getProperty("in_sno", "");          // 身分證
                resume_member_status = itmDetail.getProperty("resume_member_status", ""); // Resume 會員狀態
                resume_member_type = itmDetail.getProperty("resume_member_type", ""); // Resume 會員類ㄒㄧㄥ
            }

            //如果繳費單都沒有會費相關的狀態 直接跳離
            if (!isHavePaymentData)
            {
                return;
            }

            //空字串 跳離
            if (in_resume_id.Equals(""))
            {
                return;
            }

            //取得原有的Insume
            Item ItemInResumePayInf = GetInResumePayInfo(cfg, in_resume_id);
            int ItemInResumePayInfCount = ItemInResumePayInf.getItemCount();

            //必須可以查到講師履歷相關資料
            if (0 == ItemInResumePayInfCount)
            {
                return;
            }
            //處理講師履歷已繳費年度
            for (int i = 0; i < ItemInResumePayInfCount; i++)
            {
                Item iData = ItemInResumePayInf.getItemByIndex(i);
                string in_year = iData.getProperty("in_year", "");//繳費年度(西元)
                if (!in_year.Equals(""))
                    YearsBox.Add(in_year);
            }


            //處理繳費單 繳費年度
            for (int i = 0; i < count; i++)
            {
                Item itmDetail = itmDetails.getItemByIndex(i);
                string pay_type = itmDetail.getProperty("pay_type", ""); // 繳費類別
                string pay_year = itmDetail.getProperty("pay_year", ""); // 繳費年度(西元)

                YearsBox.Add(pay_year);
            }

            //刪除重複值
            YearsBox = YearsBox.Distinct().ToList();

            //算出最大繳費年度
            in_member_last_year = YearsBox.Max();

            string sql = "";
            bool is_one_year = CheckIsOneYearPassMemeber(in_sno);

            if (resume_member_status == "暫時會員")
            {
                //不更改會員狀態
                sql = "UPDATE IN_RESUME SET"
                    + " in_member_pay_no = '" + item_number + "'"
                    + ", in_member_last_year = '" + in_member_last_year + "'"
                    + ", in_member_pay_amount = '" + pay_total_mount + "'"
                    + " WHERE"
                    + " id = '" + in_resume_id + "'";
            }
            else if (CheckMemberQualification(YearsBox, resume_member_type, is_one_year))
            {
                //有繳費資格
                // CCO.Utilities.WriteDebug(strMethodName, "有繳費資格");

                new_member_status = "合格會員";

                sql = string.Format(
                    @"
                UPDATE IN_RESUME SET
                    in_member_pay_no = '{2}', in_member_last_year = '{3}', in_member_pay_amount = '{1}', in_member_status = N'{4}'
                WHERE id = '{0}';
            "
                    , in_resume_id         // 講師履歷ID
                    , pay_total_mount      // 應繳金額
                    , item_number          // 繳費單號
                    , in_member_last_year  // 最後繳費年度
                    , new_member_status     // 會員狀態
                );
            }
            else
            {
                sql = string.Format(
                    @"
                UPDATE IN_RESUME SET
                    in_member_pay_no = '{2}', in_member_last_year = '{3}', in_member_pay_amount = '{1}'
                WHERE id = '{0}';
            "
                    , in_resume_id         // 講師履歷ID
                    , pay_total_mount      // 應繳金額
                    , item_number          // 繳費單號
                    , in_member_last_year  // 最後繳費年度
                    , new_member_status    // 會員狀態
                );
            }

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item result = cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// 判斷是否有合格會員的資格
        /// </summary>
        /// <param name="YearsBox"></param>
        /// <param name="in_member_type">會員類型</param>
        /// <param name="isOneYearCondition">是否為[合格會員 一年判斷]</param>
        /// <returns></returns>
        private bool CheckMemberQualification(List<string> YearsBox, string in_member_type, bool isOneYearCondition)
        {
            bool Result = false;

            bool checkYear1 = false;
            bool checkYear2 = false;
            bool checkYear3 = false;

            DateTime thisYear = DateTime.Today;
            string year1 = (thisYear.Year - 1).ToString();
            string year2 = (thisYear.Year - 2).ToString();
            string year3 = (thisYear.Year - 3).ToString();

            foreach (var value in YearsBox)
            {
                // CCO.Utilities.WriteDebug(strMethodName, "YearsBox 比對年分:" + value);
                if (value.Equals(year1)) checkYear1 = true;
                if (value.Equals(year2)) checkYear2 = true;
                if (value.Equals(year3)) checkYear3 = true;
            }

            if (in_member_type == "vip_gym")
            {
                //團體會員只看近兩年
                checkYear3 = true;
            }

            if (isOneYearCondition)
            {
                ;//此邏輯 不採用 in 2021/10/01
                 // //是否有 合格會員的資格 直接判斷 近一年要繳費
                 // if(checkYear )
                 // {
                 //     Result = true;
                 // }
            }
            else
            {
                //是否有 合格會員的資格 直接判斷 近兩年要繳費
                if (checkYear1 && checkYear2 && checkYear3)
                {
                    Result = true;
                }
            }

            return Result;
        }

        /// <summary>
        /// 判斷是否為 判斷一年繳費的 會員 
        /// </summary>
        /// <param name="in_sno">身分證</param>
        /// <returns>ture一年/false其他</returns>
        private bool CheckIsOneYearPassMemeber(string in_sno)
        {
            bool result = false;

            if (in_sno.Length == 6)
            {
                if (in_sno.Substring(0, 1).ToUpper().Equals("T"))
                {
                    result = true;
                }
            }
            return result;
        }

        /// <summary>
        /// 回傳西元年度 對應講師履歷 年度欄位
        /// </summary>
        /// <param name="rYear"></param>
        /// <returns></returns>
        private string GetYearTo_InResume_ColumnName(string rYear)
        {
            string returnStr = "";
            switch (rYear)
            {
                case "2021":
                    returnStr = "in_pay_year1";
                    break;
                case "2020":
                    returnStr = "in_pay_year2";
                    break;
                case "2019":
                    returnStr = "in_pay_year3";
                    break;
                case "2022":
                    returnStr = "in_pay_year4";
                    break;
                case "2023":
                    returnStr = "in_pay_year5";
                    break;
                case "2024":
                    returnStr = "in_pay_year6";
                    break;
                case "2025":
                    returnStr = "in_pay_year7";
                    break;
                case "2026":
                    returnStr = "in_pay_year8";
                    break;
                case "2027":
                    returnStr = "in_pay_year9";
                    break;
                case "2028":
                    returnStr = "in_pay_year10";
                    break;
                case "2029":
                    returnStr = "in_pay_year11";
                    break;
                case "2030":
                    returnStr = "in_pay_year12";
                    break;
                case "2031":
                    returnStr = "in_pay_year13";
                    break;

                default:
                    returnStr = "in_pay_year1";
                    break;
            }
            return returnStr;
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }

            public string id { get; set; }

            public string model { get; set; }

            /// <summary>
            /// 民國年月日 yyy/MM/dd
            /// </summary>
            public string in_pay_date_real { get; set; }
        }

        private DateTime GetSiWanYear(string rYear)
        {
            //將民國年轉換成西元年
            DateTime dt = DateTime.ParseExact(rYear, "yyyMMdd", CultureInfo.InvariantCulture).AddYears(1911);
            return dt;
        }

        private DateTime GetDateTime(string value, int hours)
        {
            DateTime dt = DateTime.Now;
            DateTime.TryParse(value, out dt);
            return dt.AddHours(hours);
        }
    }
}