﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;

namespace WorkHelp.Aras.Playground.Methods.PLMCTA
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class In_Payment_List_Add : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的:產生繳費條碼(賽事+課程)
                日期: 
                    - 2022-02-09 團體計費 (lina)
                    - 2022-01-12 寄發 e-mail (lina)
            */

            System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]" + "In_Payment_List_Add";

            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            //轉換為組態
            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strDailMethod = "[" + strDatabaseName + "]" + "user_event_" + DateTime.Now.ToString("yyyy-MM-dd"),
                strUserId = inn.getUserID(),
                HasCreatedPayment = false,
            };

            MapConfig(cfg, itmR);

            //組態繫結資料
            BindItem(cfg, itmR);
            if (cfg.in_receice_org_code == "")
            {
                throw new Exception("Meeting 收款單位編號不可為空白");
            }

            //新增繳費單與明細
            if (cfg.is_team_bill)
            {
                AddTeamPayment(cfg, itmR);
            }
            else
            {
                AddPayment(cfg, itmR);
            }

            if (cfg.HasCreatedPayment && cfg.new_creator_email != "")
            {
                //確實建立繳費單，並且前台填入 e-mail
                SendPaymentEmail(cfg, itmR);
            }

            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

            return itmR;
        }

        private void SendPaymentEmail(TConfig cfg, Item itmReturn)
        {
            Item itmEmail = cfg.inn.newItem();
            itmEmail.setType("In_Meeting_Pay");
            itmEmail.setProperty("itemtype", cfg.MeetingName);
            itmEmail.setProperty("id", cfg.MeetingId);
            itmEmail.setProperty("resume_sno", cfg.pay_creator_sno);
            itmEmail.setProperty("new_creator_email", cfg.new_creator_email);
            Item itmEmailResult = itmEmail.apply("In_Send_MeetingPay");
        }

        /// <summary>
        /// 轉換為組態
        /// </summary>
        private void MapConfig(TConfig cfg, Item itmReturn)
        {
            cfg.in_current_org = itmReturn.getProperty("current_orgs", "").Trim(',');
            cfg.invoice_up = itmReturn.getProperty("invoice_up", "");
            cfg.uniform_numbers = itmReturn.getProperty("uniform_numbers", "");
            cfg.pay_creator_sno = itmReturn.getProperty("in_creator_sno", "");
            cfg.new_creator_email = itmReturn.getProperty("new_creator_email", "");

            cfg.blank = " ";
            cfg.codes = "10";
            cfg.in_meeting_type = "";
            cfg.in_meeting_code = "";
            cfg.in_receice_org_code = "";
            cfg.payment_mode = "A";
            cfg.project_code = "";
            cfg.index_number = "";
            cfg.barcode2 = "";
            cfg.barcode2_1 = "";
            cfg.in_imf_date_e = "";

            cfg.B1 = "";
            cfg.B2 = "";
            cfg.B3 = "";

            cfg.CurrentTime = System.DateTime.Today;
            cfg.AddTime = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");

            //應繳日期&最後繳費日期為[今日+7天](尚未確定範圍)
            cfg.due_date = cfg.CurrentTime.AddDays(7).ToString("yyyy-MM-ddTHH:mm:ss");//應繳日期
            cfg.last_payment_date = cfg.CurrentTime.AddDays(7).ToString("yyyy-MM-ddTHH:mm:ss");//最後收費日期

            //取得課程盲報
            string cla_meeting_id = itmReturn.getProperty("cla_meeting_id", "");
            //取得課程實名制
            string cla_meeting_id_group = itmReturn.getProperty("cla_meeting_id_group", "");
            //取得賽事ID
            string meeting_id = itmReturn.getProperty("meeting_id", "");

            if (!string.IsNullOrWhiteSpace(cla_meeting_id))
            {
                cfg.IsGame = false;
                cfg.IsAnonymous = true;
                cfg.IsClass = true;
                cfg.MeetingId = cla_meeting_id;

            }
            else if (!string.IsNullOrWhiteSpace(cla_meeting_id_group))
            {
                cfg.IsGame = false;
                cfg.IsAnonymous = false;
                cfg.IsClass = true;

                cfg.MeetingId = cla_meeting_id_group;
            }
            if (!string.IsNullOrWhiteSpace(meeting_id))
            {
                cfg.IsGame = true;
                cfg.IsAnonymous = false;
                cfg.IsClass = false;

                cfg.MeetingId = meeting_id;
            }

            if (cfg.IsGame)
            {
                cfg.MeetingName = "In_Meeting";
                cfg.MeetingUserName = "In_Meeting_User";
                cfg.MeetingFunctiontimeName = "In_Meeting_Functiontime";
                cfg.MeetingProperty = "in_meeting";
            }
            else
            {
                cfg.MeetingName = "In_Cla_Meeting";
                cfg.MeetingUserName = "In_Cla_Meeting_User";
                cfg.MeetingFunctiontimeName = "In_Cla_Meeting_Functiontime";
                cfg.MeetingProperty = "in_cla_meeting";
            }

            cfg.in_current_orgs = cfg.in_current_org.Split(',');
            cfg.invoice_ups = cfg.invoice_up.Split(',');
            cfg.uniform_numberss = cfg.uniform_numbers.Split(',');
        }

        /// <summary>
        /// 組態繫結資料
        /// </summary>
        private void BindItem(TConfig cfg, Item itmReturn)
        {
            string aml = "";
            string sql = "";

            //設定權限角色
            Item itmPermit = cfg.inn.applyMethod("In_CheckIdentity", "<method>" + cfg.strMethodName + "</method><code>ALL</code>");
            cfg.isMeetingAdmin = itmPermit.getProperty("isMeetingAdmin", "") == "1";
            cfg.isCommittee = itmPermit.getProperty("isCommittee", "") == "1";
            cfg.isGymOwner = itmPermit.getProperty("isGymOwner", "") == "1";
            cfg.isGymAssistant = itmPermit.getProperty("isGymAssistant", "") == "1";

            if (cfg.strUserId != "")
            {
                //取得登入者資訊
                sql = "SELECT * FROM In_Resume WITH(NOLOCK) WHERE in_user_id = '" + cfg.strUserId + "'";
                //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

                cfg.itmResume = cfg.inn.applySQL(sql);
                cfg.login_resume_group = cfg.itmResume.getProperty("in_group", "");//所屬單位
                cfg.login_resume_name = cfg.itmResume.getProperty("in_name", "");//登入者姓名
                cfg.login_resume_sno = cfg.itmResume.getProperty("in_sno", "");//登入者身分證號
                cfg.login_resume_email = cfg.itmResume.getProperty("in_email", "");//登入者 e-mail
                cfg.login_manager_name = cfg.itmResume.getProperty("in_manager_name", "");//登入者所屬委員會
            }


            //取得活動資料(賽事 or 講習)
            aml = @"<AML><Item type='" + cfg.MeetingName + "' action='get'><id>" + cfg.MeetingId + "</id></Item></AML>";
            //CCO.Utilities.WriteDebug(strMethodName, "aml: " + aml);

            cfg.itmMeeting = cfg.inn.applyAML(aml);
            cfg.in_meeting_code = cfg.itmMeeting.getProperty("item_number", "");//賽事編號
            cfg.in_meeting_title = cfg.itmMeeting.getProperty("in_title", "");//賽事名稱
            cfg.in_meeting_type = cfg.itmMeeting.getProperty("in_meeting_type", "").ToLower();//活動類型
            cfg.in_receice_org_code = cfg.itmMeeting.getProperty("in_receice_org_code", "");//收款單位編號
            cfg.in_member_type = cfg.itmMeeting.getProperty("in_member_type", "");//特殊費用類型
            cfg.in_course_fees = cfg.itmMeeting.getProperty("in_course_fees", "0");//每隊報名費

            //團隊計費
            cfg.is_team_bill = cfg.in_member_type == "團隊計費";
            if (cfg.is_team_bill)
            {
                cfg.course_fees = GetIntVal(cfg.in_course_fees);
            }

            if (cfg.in_receice_org_code.Length != 6)
            {
                throw new Exception("收款單位編號格式錯誤");
            }

            switch (cfg.in_meeting_type)
            {
                case "registry":
                    cfg.MeetingType = MeetingTypeEnum.Registry;
                    break;

                case "degree":
                    cfg.MeetingType = MeetingTypeEnum.Degree;
                    break;

                case "seminar":
                    cfg.MeetingType = MeetingTypeEnum.Seminar;
                    break;

                case "game":
                    cfg.MeetingType = MeetingTypeEnum.Game;
                    break;

                case "payment":
                    cfg.MeetingType = MeetingTypeEnum.Payment;
                    break;

                case "other":
                    cfg.MeetingType = MeetingTypeEnum.Other;
                    break;

                default:
                    cfg.MeetingType = MeetingTypeEnum.None;
                    break;
            }

            if (cfg.MeetingType == MeetingTypeEnum.None)
            {
                throw new Exception("未設定活動類型");
            }

            if (cfg.IsAnonymous)
            {
                string identity_id = itmReturn.getProperty("isIndId", "");
                string in_name = itmReturn.getProperty("in_name", "");//姓名(單位簡稱)
                string in_sno = itmReturn.getProperty("in_sno", "");//身分證字號
                //CCO.Utilities.WriteDebug(strMethodName, "盲報 identity:" + identity_id);
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "盲報 dom: " + itmReturn.dom.InnerXml);

                Item itmResume = cfg.inn.newItem("In_Resume", "get");
                itmResume.setProperty("owned_by_id", identity_id);
                itmResume = itmResume.apply();

                //盲報
                cfg.pay_identity_id = identity_id;
                cfg.pay_creator_name = in_name;
                cfg.pay_creator_sno = in_sno;
                cfg.pay_creator_group = itmResume.getProperty("in_group", "");
                cfg.pay_committee = itmResume.getProperty("in_manager_name", "");
            }
            else
            {
                cfg.pay_identity_id = cfg.identity_id;
                cfg.pay_creator_name = cfg.login_resume_name;
                // cfg.pay_creator_sno = cfg.login_resume_sno;
                cfg.pay_creator_group = cfg.login_resume_group;

                if (cfg.MeetingType == MeetingTypeEnum.Degree && cfg.isCommittee)
                {
                    cfg.pay_committee = cfg.login_resume_name;
                }
                else
                {
                    cfg.pay_committee = cfg.login_manager_name;
                }
            }

            //取得該[賽事的報名結束日]
            aml = @"<AML><Item type='" + cfg.MeetingFunctiontimeName + "' action='get'><source_id>" + cfg.MeetingId + "</source_id><in_action>sheet1</in_action></Item></AML>";
            //CCO.Utilities.WriteDebug(strMethodName, "aml: " + aml);

            cfg.itmFunctiontime = cfg.inn.applyAML(aml);
            cfg.in_imf_date_e = cfg.itmFunctiontime.getProperty("in_date_e", "");//最後報名日期

            //如果[建單日期]與[賽事報名結束日期]差距天數小於7天 直接讓[最後繳費期限]訂在[報名結束日期]
            DateTime in_imf_date_e_dt = Convert.ToDateTime(cfg.in_imf_date_e);//最後報名日期
            TimeSpan ts = in_imf_date_e_dt - System.DateTime.Today;
            double days = ts.TotalDays;

            if (days < 7)
            {
                cfg.last_payment_date = cfg.in_imf_date_e;
                cfg.due_date = cfg.in_imf_date_e;
            }

            //設定與會者資料
            if (cfg.IsAnonymous)
            {
                //盲報
                cfg.MUserSql = "SELECT * FROM " + cfg.MeetingUserName + " WITH(NOLOCK)"
                    + " WHERE source_id = '" + cfg.MeetingId + "'"
                    + " AND in_name = '" + cfg.pay_creator_name + "'"
                    + " AND in_sno = '" + cfg.pay_creator_sno + "'"
                    + " AND ISNULL(in_paynumber, '') = ''";
            }
            else
            {
                if (cfg.MeetingType == MeetingTypeEnum.Degree && cfg.isCommittee)
                {
                    cfg.MUserSql = "SELECT * FROM " + cfg.MeetingUserName + " WITH(NOLOCK)"
                        + " WHERE source_id = '" + cfg.MeetingId + "'"
                        + " AND in_committee = N'" + cfg.pay_committee + "'"
                        + " AND ISNULL(in_paynumber, '') = ''"
                        + " AND ISNULL(IN_VERIFY_RESULT, '') = '1'";
                }
                else
                {
                    cfg.MUserSql = "SELECT * FROM " + cfg.MeetingUserName + " WITH(NOLOCK)"
                        + " WHERE source_id = '" + cfg.MeetingId + "'"
                        + " AND in_creator_sno = N'" + cfg.pay_creator_sno + "'"
                        + " AND in_current_org = N'{#xls_current_org}'"
                        + " AND ISNULL(in_paynumber, '') = ''";
                }
            }

            //設定 in_org_excels
            if (cfg.IsClass)
            {
                if (cfg.IsAnonymous)
                {
                    //盲報
                    Item itmOrg = cfg.inn.newItem();
                    itmOrg.setProperty("expenses", cfg.itmMeeting.getProperty("in_course_fees", "0"));
                    itmOrg.setProperty("in_current_org", "");
                    itmOrg.setProperty("in_real_stuff", "0");
                    itmOrg.setProperty("in_real_player", "0");
                    itmOrg.setProperty("in_real_items", "1");

                    cfg.OrgExcels = new List<Item>();
                    cfg.OrgExcels.Add(itmOrg);
                }
                else
                {
                    if (cfg.MeetingType == MeetingTypeEnum.Degree && cfg.isCommittee)
                    {
                        //晉段
                        sql = @"
                            SELECT 
                                in_committee AS 'in_current_org'
                                , sum(in_expense) AS 'expenses'
                                , count(*)        AS 'in_qty'
                            FROM 
                                in_cla_meeting_user WITH(NOLOCK)
                            WHERE 
                                source_id = '{#meeting_id}' 
                                AND ISNULL(IN_PAYNUMBER, '') = ''
                                AND ISNULL(IN_VERIFY_RESULT, '') = '1'
                            GROUP BY 
                                in_committee
                        ";
                    }
                    else
                    {
                        //講習
                        sql = @"
                            SELECT 
                                in_current_org
                                , sum(in_expense) AS 'expenses'
                                , count(*)        AS 'in_qty'
                            FROM 
                                in_cla_meeting_user WITH(NOLOCK)
                            WHERE 
                                source_id = '{#meeting_id}' 
                                AND in_creator_sno = '{#in_creator_sno}'
                                AND ISNULL(IN_PAYNUMBER, '') = ''
                            GROUP BY 
                                in_current_org"
                        ;
                    }

                    sql = sql.Replace("{#meeting_id}", cfg.MeetingId)
                        .Replace("{#in_creator_sno}", cfg.pay_creator_sno);

                    cfg.in_org_excels = cfg.inn.applySQL(sql);
                    cfg.OrgExcels = MapList(cfg.in_org_excels);
                }
            }
            else if (cfg.is_team_bill)
            {
                //團隊計費
            }
            else
            {
                //呼叫excel method取得計算後的人數 項目數 費用
                itmReturn.setProperty("meeting_id", cfg.MeetingId);
                itmReturn.setProperty("export_type", "pay");
                Item in_org_excels = itmReturn.apply("in_meeting_export_excel_gym");
                //CCO.Utilities.WriteDebug(strMethodName, "in_org_excels: " + in_org_excels.dom.InnerXml);

                cfg.in_org_excels = in_org_excels.getRelationships("in_org_excel");
                cfg.OrgExcels = MapList(cfg.in_org_excels);
            }
        }

        /// <summary>
        /// 新增繳費單與明細資料
        /// </summary>
        private void AddPayment(TConfig cfg, Item itmReturn)
        {
            string aml = "";
            string sql = "";

            int count_org = cfg.in_current_orgs.Length;
            int count_org_excel = cfg.OrgExcels.Count;
            //CCO.Utilities.WriteDebug(strMethodName, "count_org_excel: " + count_org_excel.ToString());

            //預設為 10
            string codes = cfg.codes;

            //當收款單位編號長度為 6 碼
            if (cfg.in_receice_org_code.Length == 6)
            {
                codes = "";
            }

            string numbers = "";
            for (int c = 0; c < count_org; c++)
            {
                string target_org = cfg.in_current_orgs[c].Trim();

                string sql_qry = cfg.MUserSql.Replace("{#xls_current_org}", target_org);
                //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);
                Item itmMUsers = cfg.inn.applySQL(sql_qry);

                //建立繳費單號之前檢查是否有已有[繳費單號]
                HasPayNums(itmMUsers);

                //取出計算後的結果
                for (int o = 0; o < cfg.OrgExcels.Count; o++)
                {
                    Item in_org_excel = cfg.OrgExcels[o];
                    string excel_in_current_org = in_org_excel.getProperty("in_current_org", "");
                    string _in_current_org = cfg.in_current_orgs[c];

                    if (cfg.IsAnonymous)
                    {
                        excel_in_current_org = _in_current_org;
                    }

                    if (excel_in_current_org == _in_current_org)
                    {
                        //將最後收費日期&應繳日期轉型
                        DateTime in_pay_date_exp1 = Convert.ToDateTime(cfg.last_payment_date.Split('T')[0]);
                        DateTime in_pay_date_exp = Convert.ToDateTime(cfg.due_date.Split('T')[0]);

                        //費用
                        string xls_money = in_org_excel.getProperty("expenses", "");
                        string xls_current_org = in_org_excel.getProperty("in_current_org", "");//所屬單位
                        string xls_real_stuff = in_org_excel.getProperty("stuffs", "");//隊職員數
                        string xls_real_player = in_org_excel.getProperty("players", "");//選手人數
                        string xls_real_items = in_org_excel.getProperty("items", "");//項目總計

                        //lina, 2021-11-23, 規格變更: 可產生費用為 0 的繳費單
                        //若計算金額為0 則不需要產出繳費單
                        // if(xls_money == "0")
                        // {
                        //     break;
                        // }
                        if (xls_money == "")
                        {
                            break;
                        }

                        //區分繳費模式(A>繳款人 B>委託單位)
                        cfg.project_code = GetProjectCode(cfg.payment_mode, Int32.Parse(xls_money));

                        //作廢
                        string b2_1 = cfg.in_meeting_code.Substring(cfg.in_meeting_code.Length - 4);

                        //業主自訂編號: [賽事/講習序號(後3碼)] + 自訂流水號(4碼)(預設為0001)
                        //[運動賽事序號(後3碼)]
                        string b2_2 = cfg.in_meeting_code.Substring(cfg.in_meeting_code.Length - 3);

                        string index_number_prefix = b2_2.TrimStart('0');

                        //找出該[賽事]繳費單最大的[業主自訂編號]
                        sql = "SELECT MAX(index_number)";
                        sql += " FROM In_Meeting_pay WITH(NOLOCK)";
                        sql += " WHERE index_number LIKE '" + index_number_prefix + "%'";
                        Item itmPay = cfg.inn.applySQL(sql);

                        if (itmPay.getResult() == "")
                        {
                            cfg.barcode2 = b2_1 + "0001";
                            cfg.barcode2_1 = b2_2 + "0001";
                        }
                        else
                        {
                            //若不是第一張單則將最大的[業主自訂編號]+1
                            int number_next = Int32.Parse(itmPay.getProperty("column1", "0")) + 1;
                            cfg.index_number = number_next.ToString();
                            string suffix = cfg.index_number.Substring(cfg.index_number.Length - 4);

                            cfg.barcode2 = b2_1 + suffix;
                            cfg.barcode2_1 = b2_2 + suffix;
                        }

                        //這是超商繳費
                        //第一碼(最後收費日期(DateTime),項目代碼(String))
                        cfg.B1 = BarCode1_OutPut(in_pay_date_exp1, cfg.project_code);
                        //第二碼(賽事編號(String),業者自訂編號(String))
                        cfg.B2 = cfg.in_receice_org_code + cfg.barcode2_1;

                        string checkhum = cfg.inn.applyMethod("In_GetRank_CheckSum", "<account>" + cfg.B2 + "</account><pay>" + xls_money + "</pay>").getResult();

                        cfg.B2 = cfg.B2 + checkhum;

                        //第三碼(應繳日期(DateTime),費用(String),一碼(String),二碼(String))
                        cfg.B3 = BarCode3_OutPut(in_pay_date_exp, xls_money, cfg.B1, cfg.B2);

                        //產生繳費單
                        Item SingleNumber = cfg.inn.newItem("In_Meeting_pay", "add");

                        SingleNumber.setProperty("owned_by_id", cfg.pay_identity_id);//建單者
                        SingleNumber.setProperty("in_creator", cfg.pay_creator_name);//協助報名者
                        SingleNumber.setProperty("in_creator_sno", cfg.pay_creator_sno);//協助報名者身分證字號
                        SingleNumber.setProperty("in_group", cfg.pay_creator_group);//所屬群組
                        SingleNumber.setProperty("in_committee", cfg.pay_committee);//所屬跆委會

                        SingleNumber.setProperty("in_current_org", xls_current_org);//所屬單位
                        SingleNumber.setProperty("in_pay_amount_exp", xls_money);//應繳金額

                        SingleNumber.setProperty("in_pay_date_exp", cfg.due_date.Split('T')[0]);//應繳日期
                        SingleNumber.setProperty("in_pay_date_exp1", cfg.last_payment_date.Split('T')[0]);//最後收費日期

                        SingleNumber.setProperty("in_real_stuff", xls_real_stuff);//隊職員數
                        SingleNumber.setProperty("in_real_player", xls_real_player);//選手人數
                        SingleNumber.setProperty("in_real_items", xls_real_items);//項目總計

                        SingleNumber.setProperty("in_code_1", cfg.B1);//條碼1
                        SingleNumber.setProperty("in_code_2", "00" + cfg.B2);//條碼2
                        SingleNumber.setProperty("in_code_3", cfg.B3);//條碼3
                        SingleNumber.setProperty("pay_bool", "未繳費");//繳費狀態
                        SingleNumber.setProperty("in_add_time", cfg.AddTime);//建單時間
                        SingleNumber.setProperty("index_number", cfg.barcode2_1);//業主自訂編號

                        SingleNumber.setProperty(cfg.MeetingProperty, cfg.MeetingId);
                        Item documents = SingleNumber.apply();

                        //取出該賽事的關聯繳費
                        aml = "<AML>" +
                            "<Item type='In_Meeting_pay' action='get'>" +
                            "<" + cfg.MeetingProperty + ">" + cfg.MeetingId + "</" + cfg.MeetingProperty + ">" +
                            "<id>" + documents.getProperty("id", "") + "</id>" +
                            "</Item></AML>";
                        Item SingleNumbers = cfg.inn.applyAML(aml);

                        for (int i = 0; i < itmMUsers.getItemCount(); i++)
                        {
                            Item itmMUser = itmMUsers.getItemByIndex(i);
                            string in_regdate = itmMUser.getProperty("in_regdate", "");
                            in_regdate = GetDateTimeValue(in_regdate, "yyyy-MM-ddTHH:mm:ss", true);

                            if (itmMUser.getProperty("in_paynumber", "") == "")
                            {
                                //新增到繳費資訊下
                                Item in_meeting_news = cfg.inn.newItem("In_Meeting_news", "add");
                                in_meeting_news.setProperty("in_ans_l3", itmMUser.getProperty("in_gameunit", ""));//組別彙整結果
                                in_meeting_news.setProperty("in_pay_amount", itmMUser.getProperty("in_expense", ""));//應繳金額
                                in_meeting_news.setProperty("in_name", itmMUser.getProperty("in_name", ""));//姓名
                                in_meeting_news.setProperty("in_sno", itmMUser.getProperty("in_sno", ""));//身分證
                                in_meeting_news.setProperty("in_creator", cfg.pay_creator_name);//協助報名者
                                in_meeting_news.setProperty("in_creator_sno", cfg.pay_creator_sno);//協助報名者帳號
                                in_meeting_news.setProperty("in_regdate", in_regdate);//報名日期

                                //if (cfg.IsGame)
                                //{
                                in_meeting_news.setProperty("in_l1", itmMUser.getProperty("in_l1", ""));//第一層
                                in_meeting_news.setProperty("in_l2", itmMUser.getProperty("in_l2", ""));//第二層
                                in_meeting_news.setProperty("in_l3", itmMUser.getProperty("in_l3", ""));//第三層
                                in_meeting_news.setProperty("in_index", itmMUser.getProperty("in_index", ""));//序號
                                //in_meeting_news.setProperty("in_team_index", itmMUser.getProperty("in_team_index", ""));//隊伍序號
                                in_meeting_news.setProperty("in_section_name", itmMUser.getProperty("in_section_name", ""));//組名
                                in_meeting_news.setProperty("in_l1_sort", itmMUser.getProperty("in_l1_sort", ""));//第一層排序

                                in_meeting_news.setProperty("in_muid", itmMUser.getProperty("id", ""));//與會者id

                                SingleNumbers.addRelationship(in_meeting_news);
                            }
                        }

                        SingleNumbers.apply();

                        //確實建立繳費單
                        cfg.HasCreatedPayment = true;

                        //將[繳費單號]壓回去[與會者]
                        for (int i = 0; i < itmMUsers.getItemCount(); i++)
                        {
                            Item itmMUser = itmMUsers.getItemByIndex(i);
                            string muid = itmMUser.getProperty("id", "");

                            sql = "UPDATE [" + cfg.MeetingUserName + "] SET "
                                + "   in_paynumber = N'" + SingleNumbers.getProperty("item_number", "") + "'"
                                + " WHERE source_id = '" + cfg.MeetingId + "'"
                                + " AND id = '" + muid + "'";

                            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

                            Item UserNumber = cfg.inn.applySQL(sql);

                            //紀錄 log
                            AppendLog(cfg, SingleNumbers, itmMUser);
                        }

                        numbers += "," + SingleNumbers.getProperty("item_number", "");
                    }
                }
            }

            itmReturn.setProperty("numbers", numbers.Trim(','));
        }

        private List<Item> MapList(Item items)
        {
            List<Item> result = new List<Item>();
            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                result.Add(item);
            }
            return result;
        }

        //建立繳費單號之前檢查是否有已有[繳費單號]
        private bool HasPayNums(Item itmMUsers)
        {
            for (int i = 0; i < itmMUsers.getItemCount(); i++)
            {
                Item itmMUser = itmMUsers.getItemByIndex(i);
                string in_paynumber = itmMUser.getProperty("in_paynumber", "");
                if (in_paynumber != "")
                {
                    throw new Exception("所選擇之單位已有繳費單號");
                }
            }

            return true;
        }

        #region 團隊計費

        /// <summary>
        /// 新增繳費單與明細資料
        /// </summary>
        private void AddTeamPayment(TConfig cfg, Item itmReturn)
        {
            var pay_numbers = new List<string>();
            var org_sum_map = MUsersToOrgs(cfg);

            for (int i = 0; i < cfg.in_current_orgs.Length; i++)
            {
                var target_org = cfg.in_current_orgs[i].Trim();

                if (!org_sum_map.ContainsKey(target_org))
                {
                    cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "AddTeamPayment Not Exists _# " + target_org);
                    continue;
                }

                var sql_qry = cfg.MUserSql.Replace("{#xls_current_org}", target_org);
                //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

                //取得與會者
                var itmMUsers = cfg.inn.applySQL(sql_qry);

                //取得所屬單位
                var org = org_sum_map[target_org];

                //該單位未產生繳費單款項
                OrgWaitPayAmount(cfg, org);

                //產生繳費單(團隊計費)
                AddTeamPayment(cfg, pay_numbers, itmMUsers, org);
            }

            itmReturn.setProperty("numbers", string.Join(",", pay_numbers));
        }

        //產生繳費單(團隊計費)
        private void AddTeamPayment(TConfig cfg, List<string> pay_numbers, Item itmMUsers, TOrg org)
        {
            string sql = "";
            string aml = "";

            //將最後收費日期&應繳日期轉型
            DateTime in_pay_date_exp1 = Convert.ToDateTime(cfg.last_payment_date.Split('T')[0]);
            DateTime in_pay_date_exp = Convert.ToDateTime(cfg.due_date.Split('T')[0]);

            //區分繳費模式(A>繳款人 B>委託單位)
            cfg.project_code = GetProjectCode(cfg.payment_mode, org.wait_amount);

            //作廢
            string b2_1 = cfg.in_meeting_code.Substring(cfg.in_meeting_code.Length - 4);

            //業主自訂編號: [賽事/講習序號(後3碼)] + 自訂流水號(4碼)(預設為0001)
            //[運動賽事序號(後3碼)]
            string b2_2 = cfg.in_meeting_code.Substring(cfg.in_meeting_code.Length - 3);

            string index_number_prefix = b2_2.TrimStart('0');

            //找出該[賽事]繳費單最大的[業主自訂編號]
            sql = "SELECT MAX(index_number)";
            sql += " FROM In_Meeting_pay WITH(NOLOCK)";
            sql += " WHERE index_number LIKE '" + index_number_prefix + "%'";
            Item itmPay = cfg.inn.applySQL(sql);

            if (itmPay.getResult() == "")
            {
                cfg.barcode2 = b2_1 + "0001";
                cfg.barcode2_1 = b2_2 + "0001";
            }
            else
            {
                //若不是第一張單則將最大的[業主自訂編號]+1
                int number_next = GetIntVal(itmPay.getProperty("column1", "0")) + 1;
                cfg.index_number = number_next.ToString();
                string suffix = cfg.index_number.Substring(cfg.index_number.Length - 4);

                cfg.barcode2 = b2_1 + suffix;
                cfg.barcode2_1 = b2_2 + suffix;
            }

            //這是超商繳費
            //第一碼(最後收費日期(DateTime),項目代碼(String))
            cfg.B1 = BarCode1_OutPut(in_pay_date_exp1, cfg.project_code);
            //第二碼(賽事編號(String),業者自訂編號(String))
            cfg.B2 = cfg.in_receice_org_code + cfg.barcode2_1;

            string checkhum = cfg.inn.applyMethod("In_GetRank_CheckSum", "<account>" + cfg.B2 + "</account><pay>" + org.strpay + "</pay>").getResult();

            cfg.B2 = cfg.B2 + checkhum;

            //第三碼(應繳日期(DateTime),費用(String),一碼(String),二碼(String))
            cfg.B3 = BarCode3_OutPut(in_pay_date_exp, org.strpay, cfg.B1, cfg.B2);

            //產生繳費單
            Item SingleNumber = cfg.inn.newItem("In_Meeting_pay", "add");

            SingleNumber.setProperty("owned_by_id", cfg.pay_identity_id);//建單者
            SingleNumber.setProperty("in_creator", cfg.pay_creator_name);//協助報名者
            SingleNumber.setProperty("in_creator_sno", cfg.pay_creator_sno);//協助報名者身分證字號
            SingleNumber.setProperty("in_group", cfg.pay_creator_group);//所屬群組
            SingleNumber.setProperty("in_committee", cfg.pay_committee);//所屬跆委會

            SingleNumber.setProperty("in_current_org", org.name);//所屬單位
            SingleNumber.setProperty("in_pay_amount_exp", org.strpay);//應繳金額

            SingleNumber.setProperty("in_pay_date_exp", cfg.due_date.Split('T')[0]);//應繳日期
            SingleNumber.setProperty("in_pay_date_exp1", cfg.last_payment_date.Split('T')[0]);//最後收費日期

            SingleNumber.setProperty("in_real_stuff", org.info.nopay_staff_count.ToString());//隊職員數
            SingleNumber.setProperty("in_real_player", org.info.nopay_player_count.ToString());//選手人數
            SingleNumber.setProperty("in_real_items", org.info.nopay_item_count.ToString());//項目總計

            SingleNumber.setProperty("in_code_1", cfg.B1);//條碼1
            SingleNumber.setProperty("in_code_2", "00" + cfg.B2);//條碼2
            SingleNumber.setProperty("in_code_3", cfg.B3);//條碼3
            SingleNumber.setProperty("pay_bool", "未繳費");//繳費狀態
            SingleNumber.setProperty("in_add_time", cfg.AddTime);//建單時間
            SingleNumber.setProperty("index_number", cfg.barcode2_1);//業主自訂編號

            SingleNumber.setProperty(cfg.MeetingProperty, cfg.MeetingId);
            Item documents = SingleNumber.apply();

            //取出該賽事的關聯繳費
            aml = "<AML>" +
                "<Item type='In_Meeting_pay' action='get'>" +
                "<" + cfg.MeetingProperty + ">" + cfg.MeetingId + "</" + cfg.MeetingProperty + ">" +
                "<id>" + documents.getProperty("id", "") + "</id>" +
                "</Item></AML>";
            Item SingleNumbers = cfg.inn.applyAML(aml);

            for (int i = 0; i < itmMUsers.getItemCount(); i++)
            {
                Item itmMUser = itmMUsers.getItemByIndex(i);
                string in_regdate = itmMUser.getProperty("in_regdate", "");
                in_regdate = GetDateTimeValue(in_regdate, "yyyy-MM-ddTHH:mm:ss", true);

                if (itmMUser.getProperty("in_paynumber", "") == "")
                {
                    //新增到繳費資訊下
                    Item in_meeting_news = cfg.inn.newItem("In_Meeting_news", "add");
                    in_meeting_news.setProperty("in_ans_l3", itmMUser.getProperty("in_gameunit", ""));//組別彙整結果
                    in_meeting_news.setProperty("in_pay_amount", itmMUser.getProperty("in_expense", ""));//應繳金額
                    in_meeting_news.setProperty("in_name", itmMUser.getProperty("in_name", ""));//姓名
                    in_meeting_news.setProperty("in_sno", itmMUser.getProperty("in_sno", ""));//身分證
                    in_meeting_news.setProperty("in_creator", cfg.pay_creator_name);//協助報名者
                    in_meeting_news.setProperty("in_creator_sno", cfg.pay_creator_sno);//協助報名者帳號
                    in_meeting_news.setProperty("in_regdate", in_regdate);//報名日期

                    //if (cfg.IsGame)
                    //{
                    in_meeting_news.setProperty("in_l1", itmMUser.getProperty("in_l1", ""));//第一層
                    in_meeting_news.setProperty("in_l2", itmMUser.getProperty("in_l2", ""));//第二層
                    in_meeting_news.setProperty("in_l3", itmMUser.getProperty("in_l3", ""));//第三層
                    in_meeting_news.setProperty("in_index", itmMUser.getProperty("in_index", ""));//序號
                                                                                                  //in_meeting_news.setProperty("in_team_index", itmMUser.getProperty("in_team_index", ""));//隊伍序號
                    in_meeting_news.setProperty("in_section_name", itmMUser.getProperty("in_section_name", ""));//組名
                    in_meeting_news.setProperty("in_l1_sort", itmMUser.getProperty("in_l1_sort", ""));//第一層排序

                    in_meeting_news.setProperty("in_muid", itmMUser.getProperty("id", ""));//與會者id

                    SingleNumbers.addRelationship(in_meeting_news);
                }
            }

            SingleNumbers.apply();

            //確實建立繳費單
            cfg.HasCreatedPayment = true;

            //將[繳費單號]壓回去[與會者]
            for (int i = 0; i < itmMUsers.getItemCount(); i++)
            {
                Item itmMUser = itmMUsers.getItemByIndex(i);
                string muid = itmMUser.getProperty("id", "");

                sql = "UPDATE [" + cfg.MeetingUserName + "] SET "
                    + "   in_paynumber = N'" + SingleNumbers.getProperty("item_number", "") + "'"
                    + " WHERE id = '" + muid + "'";

                //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

                Item itmUpdResult = cfg.inn.applySQL(sql);

                //紀錄 log
                AppendLog(cfg, SingleNumbers, itmMUser);
            }

            pay_numbers.Add(SingleNumbers.getProperty("item_number", ""));
        }

        //該單位未產生繳費單款項
        private void OrgWaitPayAmount(TConfig cfg, TOrg org)
        {
            string sql = @"
            SELECT 
	            ISNULL(SUM(in_pay_amount_exp), 0) AS 'in_filed_amount'
            FROM
	            IN_MEETING_PAY WITH(NOLOCK)
            WHERE
	            in_meeting = '{#meeting_id}'
	            AND in_creator_sno = '{#in_creator_sno}'
	            AND in_current_org = N'{#in_current_org}'
	            AND pay_bool IN (N'已繳費', N'未繳費')
            ";

            sql = sql.Replace("{#meeting_id}", cfg.MeetingId)
                .Replace("{#in_creator_sno}", cfg.pay_creator_sno)
                .Replace("{#in_current_org}", org.name);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            //該單位 已產生繳費單款項
            org.filed_amount = 0;
            Item itmOrgAmount = cfg.inn.applySQL(sql);
            if (!itmOrgAmount.isError() && itmOrgAmount.getResult() != "")
            {
                string in_filed_amount = itmOrgAmount.getProperty("in_filed_amount", "0");
                org.filed_amount = GetIntVal(in_filed_amount);
            }

            //該單位 應付款項
            org.bill_amount = org.msum.bill_amount + org.wsum.bill_amount;

            //該單位 未產生繳費單款項
            org.wait_amount = org.bill_amount - org.filed_amount;

            if (org.wait_amount < 0)
            {
                org.wait_amount = 0;
            }

            org.strpay = org.wait_amount.ToString();
        }

        private Dictionary<string, TOrg> MUsersToOrgs(TConfig cfg)
        {
            Dictionary<string, TOrg> result = new Dictionary<string, TOrg>();

            string sql = "SELECT * FROM " + cfg.MeetingUserName + " WITH(NOLOCK)"
                + " WHERE source_id = '" + cfg.MeetingId + "'"
                + " AND in_creator_sno = '" + cfg.pay_creator_sno + "'";

            Item items = cfg.inn.applySQL(sql);
            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                TMUser muser = MapMUser(cfg, item);
                string key = muser.in_current_org;

                TOrg org = null;
                if (result.ContainsKey(key))
                {
                    org = result[key];
                }
                else
                {
                    org = MapOrg(cfg, muser);
                    result.Add(key, org);
                }

                //數量統計
                ResetPaySum(cfg, muser, org);

                //男女分隊統計
                TGenderSum gender = muser.in_gender == "男" ? org.msum : org.wsum;
                gender.expense += muser.expense;
                gender.list.Add(item);

                if (gender.expense > cfg.course_fees)
                {
                    gender.bill_amount = cfg.course_fees;
                }
                else
                {
                    gender.bill_amount = gender.expense;
                }
            }

            return result;
        }

        private void ResetPaySum(TConfig cfg, TMUser muser, TOrg org)
        {
            var info = org.info;
            var list = org.nopay_item_list;

            if (muser.is_staff)
            {
                info.total_staff_count++;
            }
            else
            {
                info.total_player_count++;
            }

            //未產生繳費單
            if (!muser.has_paynumber)
            {
                if (muser.is_staff)
                {
                    info.nopay_staff_count++;
                }
                else
                {
                    info.nopay_player_count++;
                }

                if (!list.Contains(muser.item_key))
                {
                    list.Add(muser.item_key);
                    info.nopay_item_count++;
                }
            }
        }

        private TOrg MapOrg(TConfig cfg, TMUser muser)
        {
            TOrg result = new TOrg
            {
                name = muser.in_current_org,
                bill_amount = 0,
                msum = new TGenderSum { in_gender = "男", list = new List<Item>() },
                wsum = new TGenderSum { in_gender = "女", list = new List<Item>() },
                info = new TPaySum(),
                nopay_item_list = new List<string>(),
            };

            return result;
        }

        private TMUser MapMUser(TConfig cfg, Item item)
        {
            TMUser result = new TMUser
            {
                id = item.getProperty("id", ""),
                in_gender = item.getProperty("in_gender", ""),
                in_creator_sno = item.getProperty("in_creator_sno", ""),
                in_current_org = item.getProperty("in_current_org", "").Trim(),
                in_l1 = item.getProperty("in_l1", ""),
                in_l2 = item.getProperty("in_l2", ""),
                in_l3 = item.getProperty("in_l3", ""),
                in_index = item.getProperty("in_index", ""),
                in_expense = item.getProperty("in_expense", "0"),
                in_paynumber = item.getProperty("in_paynumber", ""),
            };

            result.expense = GetIntVal(result.in_expense);
            result.is_staff = result.in_l1 == "隊職員";
            result.has_paynumber = result.in_paynumber != "";

            result.item_key = result.in_l1 + result.in_l2 + result.in_l3 + result.in_index;

            return result;
        }


        private class TOrg
        {
            public string name { get; set; }
            public int bill_amount { get; set; }
            public int filed_amount { get; set; }
            public int wait_amount { get; set; }
            public string strpay { get; set; }

            public TGenderSum msum { get; set; }
            public TGenderSum wsum { get; set; }

            public TPaySum info { get; set; }
            public List<string> nopay_item_list { get; set; }
        }

        private class TGenderSum
        {
            public string in_gender { get; set; }
            public int expense { get; set; }
            public int bill_amount { get; set; }
            public List<Item> list { get; set; }
        }

        private class TMUser
        {
            public string id { get; set; }
            public string in_creator_sno { get; set; }
            public string in_current_org { get; set; }
            public string in_gender { get; set; }
            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }
            public string in_index { get; set; }
            public string in_expense { get; set; }
            public string in_paynumber { get; set; }

            public int expense { get; set; }
            public bool is_staff { get; set; }
            public bool has_paynumber { get; set; }
            public string item_key { get; set; }
        }

        private class TPaySum
        {
            public int total_player_count { get; set; }
            public int total_staff_count { get; set; }
            public int total_item_count { get; set; }

            public int nopay_player_count { get; set; }
            public int nopay_staff_count { get; set; }
            public int nopay_item_count { get; set; }
        }
        #endregion 團隊計費

        #region 共用
        private void AppendLog(TConfig cfg, Item SingleNumbers, Item itmMUser)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("【產生繳費單】" + Environment.NewLine);
            builder.Append("活動名稱：" + cfg.in_meeting_title + Environment.NewLine);
            builder.Append("繳費單號：" + SingleNumbers.getProperty("item_number", "") + Environment.NewLine);
            builder.Append("報名項目：" + itmMUser.getProperty("in_l1", "") + itmMUser.getProperty("in_l2", "") + itmMUser.getProperty("in_l3", "") + Environment.NewLine);
            builder.Append("登入者：" + cfg.login_resume_sno + " " + cfg.login_resume_name + Environment.NewLine);
            builder.Append("條碼2：" + SingleNumbers.getProperty("in_code_2") + Environment.NewLine);

            cfg.CCO.Utilities.WriteDebug(cfg.strDailMethod, builder.ToString());
        }

        private string GetProjectCode(string payment_mode, int money)
        {
            string project_code = "";

            switch (payment_mode)
            {
                case "A":
                    if (money <= 20000)
                    {
                        project_code = "6NR";
                    }
                    else if (money > 20000 && money <= 40000)
                    {
                        project_code = "6NS";
                    }
                    else if (money > 40000 && money <= 60000)
                    {
                        project_code = "6BD";
                    }
                    break;
                case "B":
                    if (money <= 20000)
                    {
                        project_code = "68Q";
                    }
                    else if (money > 20000 && money <= 40000)
                    {
                        project_code = "68R";
                    }
                    else if (money > 40000 && money <= 60000)
                    {
                        project_code = "6BF";
                    }
                    break;
            }

            return project_code;
        }

        //產生條碼**********************************
        int ichecksum1 = 0, ichecksum2 = 0;

        //判斷是否為英文或數字
        public bool IsNumberOREng(string str)
        {
            System.Text.RegularExpressions.Regex reg1 = new System.Text.RegularExpressions.Regex(@"^[A-Za-z0-9$+-.% ]+$");
            return reg1.IsMatch(str);

        }
        //判斷是否為數字
        public bool IsPositive_Number(string str)
        {

            System.Text.RegularExpressions.Regex reg1 = new System.Text.RegularExpressions.Regex(@"^[0-9]*[1-9][0-9]*$");
            return reg1.IsMatch(str);

        }
        //產生第一段條碼(代收期限,代收項目)
        public string BarCode1_OutPut(DateTime date, String num)
        {
            //確認代收項目
            bool bIsMatch;
            bIsMatch = IsNumberOREng(num.ToString());
            if (bIsMatch == false)
            {
                throw new System.ArgumentException("代收項目編號含有未定義之特殊字元。");
            }
            else
            {
                if (num.ToString().Length != 3)
                {
                    throw new System.ArgumentException("代收項目編號長度應為3。");
                }
            }
            if (date < DateTime.Today)
            {
                // throw new System.ArgumentException("代收期限不應小於今日。");
            }

            string strDate = string.Format("{0}{1}{2}", (date.Year - 1911).ToString().Substring(1, 2), date.Month.ToString("00"), date.Day.ToString("00"));
            return strDate + num;
        }
        //產生第二段條碼(業者自訂)
        public string BarCode2_OutPut(String num, String custom)
        {
            //確認業主自訂編號
            bool bIsMatch = false;
            bIsMatch = IsNumberOREng(custom.ToString());
            if (bIsMatch == false)
            {
                throw new System.ArgumentException("業主自訂編號含有未定義之特殊字元。");
            }
            else
            {
                if (custom.ToString().Length > 10)
                {
                    throw new System.ArgumentException("業主自訂編號超出字數限制。");
                }
            }

            string strCustom = custom.PadRight(10, '0');
            return "00" + num + strCustom;
        }
        //產生第三段條碼(應繳日,金額,第一碼,第二碼)
        public string BarCode3_OutPut(DateTime date, string strpay, String barcode1, String barcode2)
        {
            //確認金額
            bool bIsMatch = false;

            if (strpay != "0")
            {
                bIsMatch = IsPositive_Number(strpay);
                if (bIsMatch == false)
                {
                    throw new System.ArgumentException("金額應為正整數。");
                }
                else
                {
                    if (strpay.Length > 9)
                    {
                        throw new System.ArgumentException("金額超過位數限制。");
                    }
                }
            }

            int pay = int.Parse(strpay);

            char[] cBarcode1, cBarcode2;
            char[] cDate, CMoney;
            string strCheckSum1, strCheckSum2;

            cBarcode1 = barcode1.ToUpper().ToCharArray(0, barcode1.Length);
            CharLoop(cBarcode1);
            cBarcode2 = barcode2.ToUpper().ToCharArray(0, barcode2.Length);
            CharLoop(cBarcode2);
            cDate = date.ToString("MMdd").ToCharArray(0, date.ToString("MMdd").Length);
            CharLoop(cDate);
            CMoney = pay.ToString().PadLeft(9, '0').ToCharArray(0, 9);
            CharLoop(CMoney);

            switch (ichecksum1)
            {
                case 0:
                    strCheckSum1 = "A";
                    break;
                case 10:
                    strCheckSum1 = "B";
                    break;
                default:
                    strCheckSum1 = ichecksum1.ToString();
                    break;
            }

            switch (ichecksum2)
            {
                case 0:
                    strCheckSum2 = "X";
                    break;
                case 10:
                    strCheckSum2 = "Y";
                    break;
                default:
                    strCheckSum2 = ichecksum2.ToString();
                    break;
            }
            ichecksum1 = 0;
            ichecksum2 = 0;
            return date.ToString("MMdd") + strCheckSum1 + strCheckSum2 + pay.ToString().PadLeft(9, '0');
        }

        private void CharLoop(char[] value)
        {
            for (int iCount = 0; iCount < value.Length; iCount++)
            {
                //偶數字串
                if (iCount % 2 > 0)
                {
                    ichecksum2 = GetCheckSum(value[iCount], ichecksum2);
                }
                //奇數字串
                else
                {
                    ichecksum1 = GetCheckSum(value[iCount], ichecksum1);
                }
            }
        }

        private int GetCheckSum(char value, int checksum)
        {
            if (Char.IsNumber(value))
            {
                //是數字
                return (checksum + ((int)value - 48)) % 11;
            }
            else
            {
                //是英文
                switch (value)
                {
                    case 'A':
                        return (checksum + 1) % 11;
                    case 'B':
                        return (checksum + 2) % 11;
                    case 'C':
                        return (checksum + 3) % 11;
                    case 'D':
                        return (checksum + 4) % 11;
                    case 'E':
                        return (checksum + 5) % 11;
                    case 'F':
                        return (checksum + 6) % 11;
                    case 'G':
                        return (checksum + 7) % 11;
                    case 'H':
                        return (checksum + 8) % 11;
                    case 'I':
                        return (checksum + 9) % 11;
                    case 'J':
                        return (checksum + 1) % 11;
                    case 'K':
                        return (checksum + 2) % 11;
                    case 'L':
                        return (checksum + 3) % 11;
                    case 'M':
                        return (checksum + 4) % 11;
                    case 'N':
                        return (checksum + 5) % 11;
                    case 'O':
                        return (checksum + 6) % 11;
                    case 'P':
                        return (checksum + 7) % 11;
                    case 'Q':
                        return (checksum + 8) % 11;
                    case 'R':
                        return (checksum + 9) % 11;
                    case 'S':
                        return (checksum + 2) % 11;
                    case 'T':
                        return (checksum + 3) % 11;
                    case 'U':
                        return (checksum + 4) % 11;
                    case 'V':
                        return (checksum + 5) % 11;
                    case 'W':
                        return (checksum + 6) % 11;
                    case 'X':
                        return (checksum + 7) % 11;
                    case 'Y':
                        return (checksum + 8) % 11;
                    case 'Z':
                        return (checksum + 9) % 11;
                    case '+':
                        return (checksum + 1) % 11;
                    case '%':
                        return (checksum + 2) % 11;
                    case '-':
                        return (checksum + 6) % 11;
                    case '.':
                        return (checksum + 7) % 11;
                    case ' ':
                        return (checksum + 8) % 11;
                    case '$':
                        return (checksum + 9) % 11;
                    case '/':
                        return (checksum + 0) % 11;
                    default:
                        return (checksum + 0) % 11;
                }
                return (checksum + ((((int)value - 65) % 9) + 1) % 11);
            }
        }
        #endregion 共用

        /// <summary>
        /// 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strDailMethod { get; set; }
            public string strUserId { get; set; }

            /// <summary>
            /// Identity id
            /// </summary>
            public string identity_id { get; set; }

            /// <summary>
            /// 協會
            /// </summary>
            public bool isMeetingAdmin { get; set; }

            /// <summary>
            /// 縣市委員會
            /// </summary>
            public bool isCommittee { get; set; }

            /// <summary>
            /// 道館社團
            /// </summary>
            public bool isGymOwner { get; set; }

            /// <summary>
            /// 道館助理
            /// </summary>
            public bool isGymAssistant { get; set; }

            /// <summary>
            /// 登入者所屬群組
            /// </summary>
            public string login_resume_group { get; set; }

            /// <summary>
            /// 登入者姓名
            /// </summary>
            public string login_resume_name { get; set; }

            /// <summary>
            /// 登入者身分證號
            /// </summary>
            public string login_resume_sno { get; set; }

            /// <summary>
            /// 登入者 email
            /// </summary>
            public string login_resume_email { get; set; }

            /// <summary>
            /// 登入者所屬委員會
            /// </summary>
            public string login_manager_name { get; set; }

            /// <summary>
            /// 登入者新 email
            /// </summary>
            public string new_creator_email { get; set; }

            /// <summary>
            /// 繳費單 Identity id
            /// </summary>
            public string pay_identity_id { get; set; }

            /// <summary>
            /// 繳費單 建單者姓名
            /// </summary>
            public string pay_creator_name { get; set; }

            /// <summary>
            /// 繳費單 建單者身分證號
            /// </summary>
            public string pay_creator_sno { get; set; }

            /// <summary>
            /// 繳費單 建單者所屬群組
            /// </summary>
            public string pay_creator_group { get; set; }

            /// <summary>
            /// 繳費單 建單者所屬委員會
            /// </summary>
            public string pay_committee { get; set; }



            /// <summary>
            /// 是否為賽事活動
            /// </summary>
            public bool IsGame { get; set; }

            /// <summary>
            /// 是否為講習活動 (講習、晉段)
            /// </summary>
            public bool IsClass { get; set; }

            /// <summary>
            /// 是否為盲報
            /// </summary>
            public bool IsAnonymous { get; set; }



            /// <summary>
            /// 活動 id
            /// </summary>
            public string MeetingId { get; set; }

            /// <summary>
            /// 登入者資訊
            /// </summary>
            public Item itmResume { get; set; }

            /// <summary>
            /// 活動資訊 (IN_MEETING OR IN_CLA_MEETING)
            /// </summary>
            public Item itmMeeting { get; set; }

            /// <summary>
            /// 時程資訊 (IN_MEETING_Functiontime OR IN_CLA_MEETING_Functiontime)
            /// </summary>
            public Item itmFunctiontime { get; set; }

            /// <summary>
            /// 與會者 sql
            /// </summary>
            public string MUserSql { get; set; }

            /// <summary>
            /// 單位繳費資料
            /// </summary>
            public Item in_org_excels { get; set; }

            /// <summary>
            /// 單位繳費資料
            /// </summary>
            public List<Item> OrgExcels { get; set; }

            /// <summary>
            /// Meeting ItemType
            /// </summary>
            public string MeetingName { get; set; }

            /// <summary>
            /// MeetingUser ItemType
            /// </summary>
            public string MeetingUserName { get; set; }

            /// <summary>
            /// MeetingFunction ItemType
            /// </summary>
            public string MeetingFunctiontimeName { get; set; }

            /// <summary>
            /// 欄位
            /// </summary>
            public string MeetingProperty { get; set; }

            /// <summary>
            /// 所屬單位
            /// </summary>
            public string in_current_org { get; set; }

            /// <summary>
            /// 發票抬頭
            /// </summary>
            public string invoice_up { get; set; }

            /// <summary>
            /// 統一編號
            /// </summary>
            public string uniform_numbers { get; set; }

            /// <summary>
            /// 所屬單位
            /// </summary>
            public string[] in_current_orgs { get; set; }

            /// <summary>
            /// 發票抬頭
            /// </summary>
            public string[] invoice_ups { get; set; }

            /// <summary>
            /// 統一編號
            /// </summary>
            public string[] uniform_numberss { get; set; }

            /// <summary>
            /// 活動編號
            /// </summary>
            public string in_meeting_code { get; set; }

            /// <summary>
            /// 活動名稱
            /// </summary>
            public string in_meeting_title { get; set; }

            /// <summary>
            /// 活動類型
            /// </summary>
            public string in_meeting_type { get; set; }

            /// <summary>
            /// 活動類型
            /// </summary>
            public MeetingTypeEnum MeetingType { get; set; }

            /// <summary>
            /// 收款單位編號
            /// </summary>
            public string in_receice_org_code { get; set; }

            /// <summary>
            /// 最後報名日期
            /// </summary>
            public string in_imf_date_e { get; set; }

            /// <summary>
            /// 最後繳費期限
            /// </summary>
            public string last_payment_date { get; set; }

            /// <summary>
            /// 最後繳費期限
            /// </summary>
            public string due_date { get; set; }

            /// <summary>
            /// 隊職員數
            /// </summary>
            public int in_staffs { get; set; }

            /// <summary>
            /// 選手人數
            /// </summary>
            public int in_players { get; set; }

            /// <summary>
            /// 項目總計
            /// </summary>
            public int in_items { get; set; }

            /// <summary>
            /// 費用
            /// </summary>
            public int in_expense { get; set; }

            /// <summary>
            /// 空白
            /// </summary>
            public string blank { get; set; }

            /// <summary>
            /// 備用碼(1)+運動代碼(0)
            /// </summary>
            public string codes { get; set; }

            /// <summary>
            /// 狀態(A>繳款人 B>委託單位)
            /// </summary>
            public string payment_mode { get; set; }

            /// <summary>
            /// 業主自訂編號
            /// </summary>
            public string index_number { get; set; }

            /// <summary>
            /// 條碼2
            /// </summary>
            public string barcode2 { get; set; }

            /// <summary>
            /// 臨櫃繳費虛擬帳號
            /// </summary>
            public string barcode2_1 { get; set; }

            /// <summary>
            /// 項目代碼
            /// </summary>
            public string project_code { get; set; }

            /// <summary>
            /// 條碼1
            /// </summary>
            public string B1 { get; set; }

            /// <summary>
            /// 條碼2
            /// </summary>
            public string B2 { get; set; }

            /// <summary>
            /// 條碼3
            /// </summary>
            public string B3 { get; set; }

            /// <summary>
            /// 當前時間
            /// </summary>
            public DateTime CurrentTime { get; set; }

            /// <summary>
            /// 建單時間
            /// </summary>
            public string AddTime { get; set; }

            /// <summary>
            /// 是否建立繳費單
            /// </summary>
            public bool HasCreatedPayment { get; set; }

            /// <summary>
            /// 特殊費用類型
            /// </summary>
            public string in_member_type { get; set; }

            /// <summary>
            /// 是否為團隊計費
            /// </summary>
            public bool is_team_bill { get; set; }

            /// <summary>
            /// 每隊報名費
            /// </summary>
            public string in_course_fees { get; set; }

            /// <summary>
            /// 每隊報名費
            /// </summary>
            public int course_fees { get; set; }
        }

        /// <summary>
        /// 活動類型
        /// </summary>
        private enum MeetingTypeEnum
        {
            /// <summary>
            /// 未設定
            /// </summary>
            None = 0,
            /// <summary>
            /// 註冊
            /// </summary>
            Registry = 100,
            /// <summary>
            /// 晉段
            /// </summary>
            Degree = 200,
            /// <summary>
            /// 講習
            /// </summary>
            Seminar = 300,
            /// <summary>
            /// 比賽
            /// </summary>
            Game = 400,
            /// <summary>
            /// 會員年費
            /// </summary>
            Payment = 500,
            /// <summary>
            /// 其他
            /// </summary>
            Other = 900,
        }

        private string GetDateTimeValue(string value, string format = "yyyy-MM-dd", bool bAdd8Hour = false)
        {
            if (value == "") return "";

            string day = value.Replace("/", "-");

            DateTime dt = DateTime.Now;
            if (DateTime.TryParse(day, out dt))
            {
                if (bAdd8Hour)
                {
                    return dt.AddHours(8).ToString(format);
                }
                else
                {
                    return dt.ToString(format);
                }
            }
            else
            {
                return value;
            }
        }

        private int GetIntVal(string value, int def = 0)
        {
            if (value == "" || value == "0") return 0;

            int result = def;
            Int32.TryParse(value, out result);
            return result;
        }
    }
}