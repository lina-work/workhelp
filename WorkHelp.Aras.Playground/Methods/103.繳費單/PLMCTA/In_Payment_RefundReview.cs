﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;
using System.Net;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class In_Payment_RefundReview : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
            目的: 審核[申請退費]的繳費單
            日期: 
                - 2022-01-21 調用重算報名人數 (lina)
                - 2021-09-27 創建 (David)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]In_Payment_RefundReview";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            string aml = "";
            string sql = "";
            string strError = "";

            //string strUserId = inn.getUserID();
            string strIdentityId = inn.getUserAliases();

            string news_id = this.getProperty("news_id", "");
            string meeting_id = this.getProperty("meeting_id", "");//meeting_id
            string now_time = DateTime.Now.AddHours(-8).ToString("yyyy-MM-ddTHH:mm:ss");//當前時間(SQL用-8H)
            string in_verify_memo = this.getProperty("in_verify_memo", "");//審核說明
            string refund_type = this.getProperty("refund_type", "");//通過or不通過
            string r_amt = this.getProperty("r_amt", "");//退款金額
            string mode = this.getProperty("mode");

            //取得資訊
            sql = "SELECT * FROM IN_MEETING_NEWS WITH(NOLOCK) WHERE id = '" + news_id + "'";
            Item itmMeetingNew = inn.applySQL(sql);

            string pay_id = itmMeetingNew.getProperty("source_id", "");
            string in_l1 = itmMeetingNew.getProperty("in_l1", "");
            string in_l2 = itmMeetingNew.getProperty("in_l2", "");
            string in_l3 = itmMeetingNew.getProperty("in_l3", "");
            string in_index = itmMeetingNew.getProperty("in_index", "");

            sql = "SELECT * FROM IN_MEETING_PAY WITH(NOLOCK) WHERE id = '" + pay_id + "'";
            Item itmMeetingPay = inn.applySQL(sql);

            string in_meeting = itmMeetingPay.getProperty("in_meeting", "");
            string in_cla_meeting = itmMeetingPay.getProperty("in_cla_meeting", "");

            //取得該退費項
            sql = "SELECT * FROM IN_MEETING_NEWS WITH(NOLOCK) WHERE source_id = '" + pay_id + "'"
                + " AND ISNULL(in_l1, '') = N'" + in_l1 + "'"
                + " AND ISNULL(in_l2, '') = N'" + in_l2 + "'"
                + " AND ISNULL(in_l3, '') = N'" + in_l3 + "'"
                + " AND ISNULL(in_index, '') = N'" + in_index + "'";

            Item items = inn.applySQL(sql);

            if (items.isError() || items.getItemCount() <= 0)
            {
                throw new Exception("繳費單明細不存在");
            }

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string current_id = item.getProperty("id", "");
                string in_muid = item.getProperty("in_muid", "");

                if (refund_type == "1")
                {
                    //通過
                    sql = @"UPDATE IN_MEETING_NEWS SET"
                        + " in_verify_time = '" + now_time + "'"
                        + ", in_verify_by_id = '" + strIdentityId + "'"
                        + ", in_cancel_status = N'退費申請通過'"
                        + ", in_refund_amount = '"+ r_amt + "'"
                        + " WHERE id = '"+ current_id + "'";

                    inn.applySQL(sql);

                    //移除[申請退費的學員的報名資料](In_Meeting_User)
                    if (in_muid == "")
                    {
                        continue;
                    }
                    
                    if (mode == "cla")
                    {
                        //增加學員編號判斷: 已有學員編號，不可刪除
                        if (canDeleteStatus(CCO, inn, strMethodName, in_muid))
                        {
                            //講習(原刪除邏輯)
                            sql = "DELETE FROM IN_CLA_MEETING_USER WHERE id = '" + in_muid + "'";
                            inn.applySQL(sql);

                            //重新計算正備取數量
                            Item Meeting = inn.getItemById("In_Cla_Meeting", in_cla_meeting);
                            Meeting = Meeting.apply("In_Cla_Update_MeetingOnMUEdit");
                        }
                        else
                        {
                            //取消報名 更新資料
                            UpdateData(CCO, inn, strMethodName, in_muid);
                        }
                    }
                    else
                    {
                        //賽事
                        sql = "DELETE FROM IN_MEETING_USER WHERE id = '" + in_muid + "'";
                        inn.applySQL(sql);

                        //重新計算正備取數量
                        Item Meeting = inn.getItemById("In_Meeting", in_meeting);
                        Meeting = Meeting.apply("In_Update_MeetingOnMUEdit");
                    }
                }
                else
                {
                    //不通過
                    sql = @"UPDATE IN_MEETING_NEWS SET"
                        + " in_verify_time = '" + now_time + "'"
                        + ", in_verify_by_id = '" + strIdentityId + "'"
                        + ", in_verify_memo = N'" + in_verify_memo + "'"
                        + ", in_cancel_status = N'退費申請不通過'"
                        + " WHERE id = '" + current_id + "'";

                    inn.applySQL(sql);
                }
            }

            if (refund_type == "1")
            {
                //更新[繳費單]的欄位
                string in_pay_amount_exp = itmMeetingPay.getProperty("in_pay_amount_exp", "0");
                string in_fee_amount = itmMeetingPay.getProperty("in_fee_amount", "0");
                string in_refund_amount = itmMeetingPay.getProperty("in_refund_amount", "0");

                string in_pay_amount = itmMeetingNew.getProperty("in_pay_amount", "0");
                //調整自訂輸入金額 Alan
                string in_refund_current = r_amt;

                int pay_amount_exp = 0;
                int refund_amount = 0;
                int refund_current = 0;
                int refund_total = 0;
                int pay_amount = 0;
                int fee_amount_exp = 0;
                int fee_total = 0;

                Int32.TryParse(in_pay_amount_exp, out pay_amount_exp); //應繳金額
                Int32.TryParse(in_refund_amount, out refund_amount); //已退金額
                Int32.TryParse(in_pay_amount, out pay_amount); //本次應繳金額
                Int32.TryParse(in_refund_current, out refund_current); //本次退款金額
                refund_total = refund_amount + refund_current; //合計退款總額
                Int32.TryParse(in_fee_amount, out fee_amount_exp); //已收手續費

                fee_total = fee_amount_exp + pay_amount - refund_current; //合計手續費
                if (refund_total > pay_amount_exp)
                {
                    throw new Exception("退款金額異常");
                }

                sql = "UPDATE IN_MEETING_PAY SET"
                    + " in_refund_amount = '" + refund_total + "'"
                    + ", in_fee_amount = '" + fee_total + "'"
                    + " WHERE id = '" + pay_id + "'";

                inn.applySQL(sql);
            }

            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

            return itmR;
        }

        private void UpdateData(Aras.Server.Core.CallContext CCO, Innovator inn, string strMethodName, string strMuid)
        {
            string updateSql = "";
            // CCO.Utilities.WriteDebug(strMethodName, "Run this Update!");

            //需要更新 
            //IN_CLA_MEETING_USER in_note VALUE:取消報名
            //In_Cla_Meeting_Resume in_year_note VALUE:未到
            //In_Cla_Meeting_Resume in_note VALUE:取消報名

            updateSql = string.Format("UPDATE IN_CLA_MEETING_USER SET {0} = N'{1}', {2} = '{3}' WHERE id = '{4}';"
                , "in_note"
                , "取消報名"
                , "in_registration_status"
                , "1"
                , strMuid
            );

            inn.applySQL(updateSql);

            updateSql = string.Format("UPDATE IN_CLA_MEETING_RESUME SET {0} = N'{1}', {2} = N'{3}',{4} = N'{5}' WHERE in_user = '{6}';"
                , "in_note"
                , "取消報名"
                , "in_year_note"
                , "未到"
                , "in_valid_type"
                , "更改報名"
                , strMuid
            );
            inn.applySQL(updateSql);
        }

        /// <summary>
        /// 用學員編號判斷是否可以刪除 ,沒有學員編號(刪除) 有學員編號 更新狀態
        /// </summary>
        private bool canDeleteStatus(Aras.Server.Core.CallContext CCO, Innovator inn, string strMethodName, string strMuid)
        {
            string sql = "SELECT t1.in_name_num FROM IN_CLA_MEETING_USER t1 WITH(NOLOCK) WHERE t1.id = '"+ strMuid + "'";
                
            Item itmSql = inn.applySQL(sql);

            string in_name_num = itmSql.getProperty("in_name_num", "");

            //沒有學員編號
            return in_name_num.Equals("");

        }
    }
}