﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;

namespace WorkHelp.Aras.Playground.Methods.PLMCTA
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class In_Payment_List_Barcode : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的:列出繳費條碼
                日期: 
                    - 2022-02-09 團體計費 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]In_Payment_List_Barcode";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "input dom: " + itmR.dom.InnerXml);

            string aml = "";
            string sql = "";
            string strError = "";
            string strUserId = inn.getUserID();
            //string strIdentityId = inn.getUserAliases();

            _InnH.AddLog(strMethodName, "MethodSteps");

            //取得登入者權限
            Item itmPermit = inn.applyMethod("In_CheckIdentity", "<method>" + strMethodName + "</method><code>ALL</code>");
            bool isMeetingAdmin = itmPermit.getProperty("isMeetingAdmin", "") == "1";
            bool isGymOwner = itmPermit.getProperty("isGymOwner", "") == "1";
            bool isGymAssistant = itmPermit.getProperty("isGymAssistant", "") == "1";

            itmR.setProperty("inn_meeting_admin", itmPermit.getProperty("isMeetingAdmin", ""));

            Item code = null;

            //取得前台給的賽事ID
            string meeting_id = this.getProperty("meeting_id", "").Trim();

            if (meeting_id == "")
            {
                throw new Exception("賽事 id 不得為空白");
            }

            var cols = new List<string> 
            {
                "in_title",
                "in_url",
                "in_register_url",
                "in_bank_1",
                "in_bank_2",
                "in_bank_account",
                "in_bank_name",
                "in_pay_mode",
                "in_pay_qrcode_photo1",
                "in_pay_qrcode_photo2",
                "in_meeting_type",
                "in_member_type",
                "in_course_fees",
            };

            var fields = string.Join(",", cols);

            aml = "<AML>" +
                "<Item type='in_meeting' action='get' id='" + meeting_id + "' select='" + fields + "'>" +
                "</Item></AML>";

            Item itmMeeting = inn.applyAML(aml);
            string in_bank_name = itmMeeting.getProperty("in_bank_name", "");

            //2020.10.08補上銀行名稱+帳號(無土銀串接)
            string in_bank_1 = itmMeeting.getProperty("in_bank_1", "");//銀行名稱
            string in_bank_account = itmMeeting.getProperty("in_bank_account", "");//銀行帳號
            string in_pay_mode = itmMeeting.getProperty("in_pay_mode", "");//付款方式

            //取得登入者資訊
            sql = "Select *";
            sql += " from In_Resume WITH(NOLOCK)";
            sql += " where in_user_id = '" + strUserId + "'";
            Item In_Resume = inn.applySQL(sql);

            string SQL_OnlyMyGym = "";
            string in_current_org = In_Resume.getProperty("in_current_org", "");
            string in_creator_sno = In_Resume.getProperty("in_sno", "");
            string in_member_type = In_Resume.getProperty("in_member_type", "");

            //取得前台給的繳費單號
            string Pay_number = this.getProperty("paynumbers", "").Trim(',');
            string All_Pay_number = Pay_number;
            if (Pay_number.Contains(","))
            {
                string[] pay_arr = Pay_number.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                if (pay_arr != null && pay_arr.Length > 0)
                {
                    //多張繳費單時，預設為第一張
                    Pay_number = pay_arr[0];
                }
            }

            //取得前台給的所屬群組
            string in_group = this.getProperty("player_group", "");

            itmR.setProperty("player_current_org", in_current_org);

            if (isMeetingAdmin)
            {
                if (in_member_type == "vip_mbr" || in_member_type == "vip_minority")
                {
                    //協會成員並且為個人會員: 只看自己的單
                    SQL_OnlyMyGym = " AND in_creator_sno = N'" + in_creator_sno + "'";
                }
                else
                {
                    SQL_OnlyMyGym = "";
                }
            }
            else
            {
                //道館主(看自己道館的人) 道館助理(看自己道館的人)
                SQL_OnlyMyGym = " AND in_creator_sno = N'" + in_creator_sno + "'";
            }


            //取得該賽事[已繳費],[未繳費]的繳費單

            sql = @"
                SELECT 
                    item_number
                    , in_current_org
                    , pay_bool 
                FROM 
                    IN_MEETING_PAY WITH(NOLOCK)
                WHERE 
                    in_meeting = '{#in_meeting}' 
                    AND pay_bool IN (N'已繳費', N'未繳費') 
                    {#SQL_OnlyMyGym}             
                ORDER BY 
                    pay_bool DESC
                    , item_number
            ";

            sql = sql.Replace("{#in_meeting}", meeting_id)
                    .Replace("{#SQL_OnlyMyGym}", SQL_OnlyMyGym)
                    .Replace("{#Pay_number}", Pay_number);

            Item In_Meeting_pays = inn.applySQL(sql);

            if (In_Meeting_pays.isError() || In_Meeting_pays.getResult() == "")
            {
                itmR.setProperty("in_error", "X");
                return itmR;
            }

            //2020.10.29(計算此登入者目前有幾張未繳款的單子並帶出下一張的資訊)
            sql = "SELECT * FROM IN_MEETING_PAY WITH(NOLOCK)"
                + " WHERE in_meeting = '" + meeting_id + "'"
                + " AND pay_bool = N'未繳費'" + SQL_OnlyMyGym;

            Item unpaid_sum = inn.applySQL(sql);

            int pay_count = unpaid_sum.getItemCount();
            if (pay_count < 0)
            {
                pay_count = 0;
            }

            itmR.setProperty("unpaid_sum", "共" + pay_count + "張尚未繳款");//未繳款的單數

            string next_item_number = "";//下一張單號
            string next_in_current_org = "";//下一張單位
            string next_status = "";

            string last_item_number = "";//上一張單號
            string last_in_current_org = "";//上一張單位
            string last_status = "";

            //如果單號為空 帶表示第一次進來 因此直接給予該賽事第一張的單號
            if (Pay_number == "")
            {
                Pay_number = In_Meeting_pays.getItemByIndex(0).getProperty("item_number", "");

                itmR.setProperty("inn_item_number", In_Meeting_pays.getItemByIndex(0).getProperty("item_number", ""));
                itmR.setProperty("inn_current_org", In_Meeting_pays.getItemByIndex(0).getProperty("in_current_org", ""));
            }
            else
            {
                itmR.setProperty("inn_item_number", Pay_number);
                itmR.setProperty("inn_current_org", in_group);
                itmR.setProperty("inn_creator_sno", in_creator_sno);
            }


            bool a = false;
            for (int i = 0; i < In_Meeting_pays.getItemCount(); i++)
            {
                //取得下一張的資訊
                Item In_Meeting_pay = In_Meeting_pays.getItemByIndex(i);

                if (a)
                {
                    next_item_number = In_Meeting_pay.getProperty("item_number", "") + ",";
                    next_in_current_org = In_Meeting_pay.getProperty("in_current_org", "") + ",";
                    next_status = "(" + In_Meeting_pay.getProperty("pay_bool", "").Substring(0, 2) + ")";
                    a = false;
                }

                if (In_Meeting_pay.getProperty("item_number", "") == Pay_number)
                    a = true;
            }

            bool b = false;
            for (int i = In_Meeting_pays.getItemCount() - 1; i >= 0; i--)
            {
                //取得上一張的資訊
                Item In_Meeting_pay = In_Meeting_pays.getItemByIndex(i);

                if (b)
                {
                    last_item_number = In_Meeting_pay.getProperty("item_number", "") + ",";
                    last_in_current_org = In_Meeting_pay.getProperty("in_current_org", "") + ",";
                    last_status = "(" + In_Meeting_pay.getProperty("pay_bool", "").Substring(0, 2) + ")";
                    b = false;
                }

                if (In_Meeting_pay.getProperty("item_number", "") == Pay_number)
                    b = true;
            }

            string[] Pay_numbers = Pay_number.Split(',');
            //CCO.Utilities.WriteDebug("In_Payment_List_Barcode", "Pay_number:"+Pay_number);


            //丟出下一張的資訊
            itmR.setProperty("next_item_numbe", next_item_number.Trim(','));//下一個單位
            itmR.setProperty("next_current_org", next_in_current_org.Trim(','));//下一個繳費單號
            itmR.setProperty("next_status", next_status);
            if (next_status == "(已繳)")
            {
                itmR.setProperty("next_type", "success");
            }
            else
            {
                itmR.setProperty("next_type", "warning");
            }

            //丟出上一張的資訊
            itmR.setProperty("last_item_numbe", last_item_number.Trim(','));//下一個單位
            itmR.setProperty("last_current_org", last_in_current_org.Trim(','));//下一個繳費單號
            itmR.setProperty("last_status", last_status);
            if (last_status == "(已繳)")
            {
                itmR.setProperty("last_type", "success");
            }
            else
            {
                itmR.setProperty("last_type", "warning");
            }


            //應繳日期&最後繳費日期為[今日+7天]
            DateTime CurrentTime = System.DateTime.Today;
            string AfterDay = CurrentTime.AddDays(7).ToString("yyyy-MM-ddTHH:mm:ss");

            string in_pay_photo = "";
            //依著切出來的單號數量跑
            for (int s = 0; s < Pay_numbers.Count(); s++)
            {
                //依據[賽事ID],[所屬群組],[繳費單號]取得該賽事下的繳費單
                aml = "<AML>" +
                    "<Item type='In_Meeting_pay' action='get'>" +
                    "<in_meeting>" + meeting_id + "</in_meeting>" +
                    "<item_number>" + Pay_numbers[s] + "</item_number>" +
                    "</Item></AML>";
                Item Meeting_pays = inn.applyAML(aml);

                for (int i = 0; i < Meeting_pays.getItemCount(); i++)
                {
                    Item Meeting_pay = Meeting_pays.getItemByIndex(i);
                    string in_pay_date_exp1 = Meeting_pay.getProperty("in_pay_date_exp1", "").Split('T')[0];

                    code = inn.newItem("BarCode");
                    code.setProperty("in_pay_date_exp1", in_pay_date_exp1.Replace('-', '/'));//繳費期限
                    code.setProperty("in_pay_amount_exp", Meeting_pay.getProperty("in_pay_amount_exp", "0"));//繳費金額
                    code.setProperty("in_group", Meeting_pay.getProperty("in_group", ""));//所屬群組
                    code.setProperty("in_current_org", Meeting_pay.getProperty("in_current_org", ""));//所屬單位
                    code.setProperty("in_creator_sno", Meeting_pay.getProperty("in_creator_sno", ""));//協助報名者
                    code.setProperty("in_code_1", Meeting_pay.getProperty("in_code_1", ""));//條碼1


                    //lina 2021.01.22: 為了處理 in_code_2 打星號導致 js 出錯而改寫 _# START:
                    string inn_code_2_barcode = "";
                    string inn_code_2_display = "";
                    if (in_pay_mode == "3")
                    {
                        string in_code_2 = Meeting_pay.getProperty("in_code_2", "");
                        inn_code_2_barcode = "<svg id='barcode2' class='barcode' jsbarcode-format='auto' jsbarcode-value='" + in_code_2 + "' jsbarcode-width='1' jsbarcode-height='70' jsbarcode-textmargin='10' jsbarcode-fontoptions='bold' style='width: 100%'></svg>";
                        inn_code_2_display = in_code_2;
                    }
                    else
                    {
                        inn_code_2_display = GetCodeMask(Meeting_pay.getProperty("in_code_2", ""));
                    }
                    code.setProperty("inn_code_2_barcode", inn_code_2_barcode);//條碼2 (如果秀出3條碼可能會出錯)
                    code.setProperty("in_code_2", inn_code_2_display);//條碼2
                    //lina 2021.01.22: 為了處理 in_code_2 打星號導致 js 出錯而改寫 _# END.

                    //code.setProperty("in_code_2",GetCodeMask(Meeting_pay.getProperty("in_code_2","")));//條碼2(如果秀出3條碼可能會出錯)
                    code.setProperty("in_code_3", Meeting_pay.getProperty("in_code_3", ""));//條碼3
                    code.setProperty("in_real_stuff", Meeting_pay.getProperty("in_real_stuff", ""));//隊職員數
                    code.setProperty("in_real_player", Meeting_pay.getProperty("in_real_player", ""));//選手人數
                    code.setProperty("in_real_items", Meeting_pay.getProperty("in_real_items", ""));//參賽總計
                    code.setProperty("item_number", Meeting_pay.getProperty("item_number", ""));//繳費編號
                    code.setProperty("in_pay_amount1", Meeting_pay.getProperty("in_pay_amount_real", ""));//已繳費用
                    code.setProperty("index_number", Meeting_pay.getProperty("index_number", ""));// 業主自訂編號
                    code.setProperty("pay_bool", Meeting_pay.getProperty("pay_bool", ""));// 繳費狀態
                    code.setProperty("invoice_up", Meeting_pay.getProperty("invoice_up", ""));// 抬頭發票
                    code.setProperty("uniform_numbers", Meeting_pay.getProperty("uniform_numbers", ""));// 統一編號
                    if (in_bank_name == "")
                    {
                        in_bank_name = "-";
                    }
                    code.setProperty("in_bank_name", in_bank_name);//戶名
                    code.setProperty("in_creator", Meeting_pay.getProperty("in_creator", ""));//繳費者姓名
                                                                                              //2020.10.08補上銀行名稱+帳號(無土銀串接)
                    code.setProperty("in_bank_1", in_bank_1);//銀行名稱
                    code.setProperty("in_bank_account", in_bank_account);//銀行帳號

                    in_pay_photo = Meeting_pay.getProperty("in_pay_photo", "");//繳費照片

                    if (Meeting_pay.getProperty("pay_bool", "") == "已繳費")
                    {
                        //若狀態為[已繳費]拋出一個特徵 讓前端變化
                        itmR.setProperty("check_pay_bool", "O");
                    }

                    //mark 第一張繳費單
                    if (itmR.getProperty("first_paynumber", "") == "")
                    {
                        itmR.setProperty("first_paynumber", code.getProperty("item_number", ""));
                    }

                    itmR.addRelationship(code);
                }
            }

            itmR.setProperty("in_pay_photo", in_pay_photo);//繳費照片

            //附加賽事、付款相關資訊
            AppendMeetingInfo(itmMeeting, itmR);

            //附加付款明細資訊
            AppendPayDetail(CCO, strMethodName, inn, itmMeeting, Pay_numbers, itmR);

            //隱藏流程，以及會導到其他頁面的功能
            string flow = this.getProperty("flow", "").Trim();
            itmR.setProperty("hide_flow", flow == "hide" ? "item_show_0" : "");

            return itmR;

        }

        //附加賽事、付款相關資訊
        private void AppendMeetingInfo(Item itmMeeting, Item itmReturn)
        {
            string in_title = itmMeeting.getProperty("in_title", "");
            string in_bank_1 = itmMeeting.getProperty("in_bank_1", "");
            string in_bank_2 = itmMeeting.getProperty("in_bank_2", "");
            string in_bank_account = itmMeeting.getProperty("in_bank_account", "");
            string in_bank_name = itmMeeting.getProperty("in_bank_name", "");

            string in_pay_mode = itmMeeting.getProperty("in_pay_mode", "");
            string in_pay_qrcode_photo1 = itmMeeting.getProperty("in_pay_qrcode_photo1", "");
            string in_pay_qrcode_photo2 = itmMeeting.getProperty("in_pay_qrcode_photo2", "");
            if (in_pay_mode == "")
            {    //預設給 2: 產生繳費單，無金流串接
                in_pay_mode = "2";
            }

            string in_url = itmMeeting.getProperty("in_url", "");
            string in_register_url = itmMeeting.getProperty("in_register_url", "");

            string virtual_account_atm = "item_show_0";
            string virtual_account_kiosk = "item_show_0";
            string real_account_atm = "item_show_0";
            string qrcode_account = "item_show_0";

            switch (in_pay_mode)
            {
                case "1": //純報名，無繳費單
                    break;

                case "2": //繳費，無金流模式
                    real_account_atm = "";
                    break;

                case "3": //繳費，土銀模式
                    virtual_account_atm = "";
                    virtual_account_kiosk = "";
                    break;

                case "4": //QrCode 模式
                    qrcode_account = "";
                    break;

                default: //未設定
                    break;
            }

            itmReturn.setProperty("meeting_name", in_title);
            itmReturn.setProperty("in_bank_1", in_bank_1);//銀行名稱
            itmReturn.setProperty("in_bank_2", in_bank_2);//銀行帳號
            itmReturn.setProperty("in_bank_account", in_bank_account);//銀行分行
            itmReturn.setProperty("in_bank_name", in_bank_name);//銀行戶名

            itmReturn.setProperty("in_pay_mode", in_pay_mode);
            itmReturn.setProperty("in_pay_qrcode_photo1", in_pay_qrcode_photo1);
            itmReturn.setProperty("in_pay_qrcode_photo2", in_pay_qrcode_photo2);

            itmReturn.setProperty("virtual_account_atm", virtual_account_atm);
            itmReturn.setProperty("virtual_account_kiosk", virtual_account_kiosk);
            itmReturn.setProperty("real_account_atm", real_account_atm);
            itmReturn.setProperty("qrcode_account", qrcode_account);

            itmReturn.setProperty("in_title", in_title);
            itmReturn.setProperty("in_url", in_url);
            itmReturn.setProperty("in_register_url", in_register_url);
        }

        private void AppendPayDetail(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmMeeting, string[] pay_numbers, Item itmReturn)
        {
            string meeting_id = itmMeeting.getProperty("id", "");
            string in_meeting_type = itmMeeting.getProperty("in_meeting_type", "");
            string in_member_type = itmMeeting.getProperty("in_member_type", "");
            string vu_name = in_meeting_type == "payment" ? "VU_MEETING_PAY_DETAIL2" : "VU_MEETING_PAY_DETAIL";
            bool is_team_bill = in_member_type == "團隊計費";

            string sql = @"
                SELECT
                    t2.item_name
                    , t2.item_amount
                    , t2.item_count
                    , t2.item_amount * t2.item_count AS 'item_subtotal'
                FROM
                    IN_MEETING_PAY t1 WITH(NOLOCK)
                INNER JOIN
                    {#vu_name} t2
                    on t2.source_id = t1.id
                WHERE
                    t1.in_meeting = '{#meeting_id}'
                    AND t1.item_number = N'{#item_number}'
                ORDER BY
                    t1.item_number
                    , t2.item_name
            ";

            for (int i = 0; i < pay_numbers.Count(); i++)
            {
                string in_paynumber = pay_numbers[i];
                if (in_paynumber == "")
                {
                    continue;
                }

                string temp_sql = sql.Replace("{#meeting_id}", meeting_id)
                    .Replace("{#vu_name}", vu_name)
                    .Replace("{#item_number}", in_paynumber);

                //CCO.Utilities.WriteDebug(strMethodName, "temp_sql: " + temp_sql);

                Item items = inn.applySQL(temp_sql);
                if (items.isError() || items.getItemCount() <= 0)
                {
                    CCO.Utilities.WriteDebug(strMethodName, "in_paynumber: " + in_paynumber + " 明細資料發生錯誤 _# sql: " + temp_sql);
                    continue;
                }

                int total_amount = 0;

                int count = items.getItemCount();
                for (int j = 0; j < count; j++)
                {
                    Item item = items.getItemByIndex(j);
                    string item_name = item.getProperty("item_name", "");
                    string item_amount = item.getProperty("item_amount", "");
                    string item_count = item.getProperty("item_count", "");
                    string item_subtotal = item.getProperty("item_subtotal", "");

                    total_amount += GetIntValue(item_subtotal);

                    item.setType("inn_meeting_user");
                    item.setProperty("group_name", item_name);
                    item.setProperty("group_count", item_count);
                    item.setProperty("inn_group_expense", GetMoneyFormat(item_amount));
                    item.setProperty("inn_group_amount", GetMoneyFormat(item_subtotal));

                    if(is_team_bill)
                    {
                        item.setProperty("inn_group_expense", "--");
                        item.setProperty("inn_group_amount", "團隊計費");
                    }

                    itmReturn.addRelationship(item);
                }

                itmReturn.setProperty("inn_total_amount", total_amount.ToString("###,###"));
            }
        }

        private int GetIntValue(string value)
        {
            int result = 0;
            if (Int32.TryParse(value, out result))
            {
                return result;
            }
            return 0;
        }

        private string GetMoneyFormat(string value)
        {
            if (value == "" || value == "0")
            {
                return "0";
            }

            int result = 0;
            if (Int32.TryParse(value, out result))
            {
                return result.ToString("###,###");
            }
            else
            {
                return "0";
            }
        }

        //in_meeting_pay 條碼2 需加上遮罩
        private string GetCodeMask(string value)
        {
            if (value.Length == 16)
            {
                return value.Substring(0, 4) + "*******" + value.Substring(value.Length - 5, 5);
            }
            return "";
        }
    }
}