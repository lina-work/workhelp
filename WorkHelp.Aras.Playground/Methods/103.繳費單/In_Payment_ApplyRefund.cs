﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;
using System.Net;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class In_Payment_ApplyRefund : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;            
            
            /*
                目的: 繳費單退費申請
                日期: 
                    - 2021-11-25 調整 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]In_Payment_ApplyRefund";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "input dom: " + itmR.dom.InnerXml);

            string sql = "";
            string strUserId = inn.getUserID();
            string strIdentityId = inn.getUserAliases();

            string item_number = itmR.getProperty("item_number", "");//繳費單號

            string pay_section_name = itmR.getProperty("pay_section_name", "").Trim(',');//已選組別
            string[] pay_section_names = pay_section_name.Split(',');

            string pay_index = itmR.getProperty("pay_index", "").Trim(',');//序號
            string[] pay_indexs = pay_index.Split(',');

            string pay_type = itmR.getProperty("pay_type", "");//申請類型
            string in_cancel_status = pay_type + "申請中";
            string now_time = DateTime.Now.AddHours(-8).ToString("yyyy-MM-ddTHH:mm:ss");//當前時間(SQL用-8H)

            string in_cancel_memo = itmR.getProperty("in_cancel_memo", " ");//申請說明
            string in_new_levels = itmR.getProperty("in_new_levels", "");//新組別

            //取得[繳費單]的ID, 退款金額
            sql = "SELECT id, in_refund_amount FROM IN_MEETING_PAY WITH(NOLOCK) WHERE item_number = '" + item_number + "'";
            Item itmPay = inn.applySQL(sql);
            string pay_id = itmPay.getProperty("id", "");

            for (int i = 0; i < pay_section_names.Count(); i++)
            {
                //更新該張單底下的[繳費資訊]
                sql = "UPDATE IN_MEETING_NEWS SET"
                    + " in_cancel_status = N'" + in_cancel_status + "'"
                    + ", in_cancel_by_id = '" + strIdentityId + "'"
                    + ", in_cancel_time = '" + now_time + "'"
                    + ", in_cancel_memo = N'" + in_cancel_memo + "'"
                    + ", in_new_levels = N'" + in_new_levels + "'"
                    + " WHERE source_id = '" + pay_id + "'"
                    + " AND in_section_name = N'" + pay_section_names[i] + "'"
                    + " AND in_index = '" + pay_indexs[i] + "'";

                CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

                inn.applySQL(sql);
            }

            return itmR;
        }

    }
}