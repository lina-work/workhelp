﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class In_Send_MeetingPay : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 繳費單 e-mail
                輸入:
                    - itemtype: In_Meeting OR In_Cla_Meeting
                    - id: 賽事 id OR 課程 id
                    - resume_id: 講師履歷 id
                    - new_creator_email: 新填寫的 e-mail
                日期: 
                    - 2021-01-12 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]In_Send_MeetingPay";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "input dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),

                itemtype = itmR.getProperty("itemtype", ""),
                id = itmR.getProperty("id", ""),
                resume_id = itmR.getProperty("resume_id", ""),
                new_creator_email = itmR.getProperty("new_creator_email", ""),
            };

            string sql = "";

            //iplm 站台網址
            cfg.iplm_url = GetVariable(cfg, "app_url").Trim('/');
            //是否為課程
            cfg.is_seminar = cfg.itemtype.ToLower() == "in_cla_meeting";

            List<string> mt_cols = new List<string>
            {
                "in_title",
                "in_url",
                "in_register_url",
                "in_address",
                "ISNULL(in_place",
                "in_address) AS 'in_place'",
                "in_date_s",
                "in_date_e",
                "in_mail",
                "in_customer_contact",
                "in_rmbn",
            };

            sql = "SELECT " + string.Join(", ", mt_cols) + " FROM " + cfg.itemtype + " WITH(NOLOCK) WHERE id = '" + cfg.id + "'";
            cfg.itmMeeting = cfg.inn.applySQL(sql);

            sql = "SELECT in_name FROM IN_RESUME WITH(NOLOCK) WHERE resume_id = '" + cfg.resume_id + "'";
            cfg.itmResume = cfg.inn.applySQL(sql);

            cfg.email_name = "PayEmail";
            sql = "SELECT * FROM EMAIL_MESSAGE WITH(NOLOCK) WHERE [name] = '"+ cfg.email_name + "'";
            cfg.itmEmail = cfg.inn.applySQL(sql);

            cfg.mt_title = cfg.itmMeeting.getProperty("in_title", "");
            cfg.mt_contact = cfg.itmMeeting.getProperty("in_customer_contact", "");
            cfg.mt_email = cfg.itmMeeting.getProperty("in_mail", "");

            cfg.resume_name = cfg.itmResume.getProperty("in_name", "");

            var fields = GetFields(cfg);

            return itmR;
        }

        private void SendEmail(TConfig cfg, List<TField> fields)
        {
            var MyFrom = new System.Net.Mail.MailAddress(cfg.mt_email, cfg.mt_contact);
            var MyTo = new System.Net.Mail.MailAddress(cfg.new_creator_email, cfg.resume_name);
            var myMail = new System.Net.Mail.MailMessage(MyFrom, MyTo);

            string NewBody = cfg.itmEmail.getProperty("body_html", "");
            string NewSubject = cfg.itmEmail.getProperty("subject", "");

            //內文處理
            StringBuilder build = new StringBuilder(NewBody);
            foreach (var field in fields)
            {
                build.Replace(field.parm, field.value);
            }

            //標題處理
            string email_body = build.ToString();
            string email_subject = NewSubject.Replace("@MeetingTitle", cfg.mt_title);

            myMail.Subject = email_subject;
            myMail.SubjectEncoding = System.Text.Encoding.UTF8;

            myMail.Body = email_body;
            myMail.BodyEncoding = System.Text.Encoding.UTF8;

            myMail.IsBodyHtml = true;

            try
            {
                cfg.CCO.Email.SetupSmtpMailServerAndSend(myMail);

                //新增發送紀錄
                AddRecord(cfg, email_subject, email_body, "已發送");
            }
            catch(Exception ex)
            {
                //新增發送紀錄
                AddRecord(cfg, email_subject, email_body, "發送失敗");
            }
        }

        //新增發送紀錄
        private void AddRecord(TConfig cfg, string subject, string body, string status)
        {
            Item itmNew = cfg.inn.newItem("In_Resume_Mail", "add");
            itmNew.setProperty("source_id", cfg.resume_id);
            itmNew.setProperty("in_type", cfg.email_name);
            itmNew.setProperty("in_subject", subject);
            itmNew.setProperty("in_body", body);
            itmNew.setProperty("in_status", status);
            Item itmNewResult = itmNew.apply();

            if (itmNewResult.isError())
            {
                throw new Exception("紀錄發生錯誤");
            }
        }

        private List<TField> GetFields(TConfig cfg)
        {

            List<TField> fields = new List<TField>();
            fields.Add(new TField { parm = "@ResumeName", prop = "in_name", item = cfg.itmResume });
            fields.Add(new TField { parm = "@MeetingTitle", prop = "in_title", item = cfg.itmMeeting });
            fields.Add(new TField { parm = "@MeetingAddress", prop = "in_address", item = cfg.itmMeeting });
            fields.Add(new TField { parm = "@MeetingPlace", prop = "in_place", item = cfg.itmMeeting });
            fields.Add(new TField { parm = "@MeetingContract", prop = "in_customer_contact", item = cfg.itmMeeting });
            fields.Add(new TField { parm = "@MeetingEmail", prop = "in_mail", item = cfg.itmMeeting });
            fields.Add(new TField { parm = "@MeetingTel", prop = "in_rmbn", item = cfg.itmMeeting });

            fields.Add(new TField { parm = "@MeetingStartDate", prop = "in_date_s", getVal = MeetingDate, });
            fields.Add(new TField { parm = "@MeetingEndDate", prop = "in_date_e", getVal = MeetingDate, });

            fields.Add(new TField { parm = "@MeetingInfoUrl", getVal = MeetingInfoUrl, });
            fields.Add(new TField { parm = "@payUrl", getVal = MeetingPay, });

            foreach (var field in fields)
            {
                string value = "";

                if (field.getVal != null)
                {
                    value = field.getVal(cfg, field);
                }
                else if (field.prop != "" && field.item != null)
                {
                    value = field.item.getProperty(field.prop, "");
                }

                field.value = value;
            }

            return fields;
        }

        private string MeetingDate(TConfig cfg, TField field)
        {
            var dt = GetDateTime(field.item.getProperty(field.prop, ""));
            if (dt == DateTime.MinValue)
            {
                return "";
            }

            return dt.AddHours(8).ToString("yyyy/MM/dd HH:mm");
        }

        private string MeetingInfoUrl(TConfig cfg, TField field)
        {
            string page = cfg.itmMeeting.getProperty("in_url", "");
            string method = cfg.is_seminar ? "In_Cla_MeetingUserResponse" : "In_MeetingUserResponse";

            return cfg.iplm_url
                + "/pages/b.aspx"
                + "?page=" + page
                + "&method=" + method
                + "&in_meetingid=" + cfg.id;
        }

        private string MeetingPay(TConfig cfg, TField field)
        {
            string page = cfg.is_seminar ? "cla_barcode_list.html" : "barcode_list.html";
            string method = cfg.is_seminar ? "In_Cla_Payment_List_Barcode_V1" : "In_Payment_List_Barcode";

            return cfg.iplm_url
                + "/pages/c.aspx"
                + "?page=" + page
                + "&method=" + method
                + "&meeting_id=" + cfg.id;
        }

        private string GetVariable(TConfig cfg, string in_name)
        {
            Item item = cfg.inn.applySQL("SELECT TOP 1 in_value FROM IN_VARIABLE WITH(NOLOCK) WHERE in_name = '" + in_name + "'");
            if (item.isError() || item.getResult() == "")
            {
                throw new Exception("查無 " + in_name + " 站台網址");
            }
            return item.getProperty("in_value", "").TrimEnd('/');
        }

        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }

            public string iplm_url { get; set; }
            
            public string itemtype { get; set; }
            public string id { get; set; }
            public string resume_id { get; set; }
            public string new_creator_email { get; set; }

            public string email_name { get; set; }

            public Item itmMeeting { get; set; }
            public Item itmResume { get; set; }
            public Item itmEmail { get; set; }

            public bool is_seminar { get; set; }

            public string mt_title { get; set; }
            public string mt_contact { get; set; }
            public string mt_email { get; set; }

            public string resume_name { get; set; }
        }

        private class TField
        { 
            public string parm { get; set; }
            public string prop { get; set; }
            public string value { get; set; }
            public Item item { get; set; }
            public Func<TConfig, TField, string> getVal { get; set; }
        }

        private int GetIntVal(string value, int def = 0)
        {
            int result = def;
            Int32.TryParse(value, out result);
            return result;
        }

        private DateTime GetDateTime(string value)
        {
            DateTime result = DateTime.MinValue;
            DateTime.TryParse(value, out result);
            return result;
        }
    }
}