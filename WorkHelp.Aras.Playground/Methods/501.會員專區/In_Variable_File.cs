﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;
using System.Net;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class In_Variable_File : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 
                    套版檔案上傳
                日期: 
                    - 2021-10-09: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]In_Variable_File";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, this.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                InnH = _InnH,

                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                id = itmR.getProperty("id", ""),
                in_name = itmR.getProperty("in_name", ""),
                scene = itmR.getProperty("scene", ""),
            };

            switch (cfg.scene)
            {
                case "modal":
                    Modal(cfg, itmR);
                    break;

                case "upload":
                    Upload(cfg, itmR);
                    break;

                case "remove":
                    Remove(cfg, itmR);
                    break;

                case "page":
                    Page(cfg, itmR);
                    break;

                default:
                    Path(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void Path(TConfig cfg, Item itmReturn)
        {
            string sql = @"
                SELECT 
	                t2.in_name AS 'variable'
	                , t1.in_name
	                , t1.in_value 
	                , t1.in_file
					, t3.filename
                FROM 
                    In_Variable_Detail t1 WITH(NOLOCK)
                INNER JOIN 
                    In_Variable t2 WITH(NOLOCK) 
	                ON t2.id = t1.source_id 
                LEFT OUTER JOIN
                    [File] t3 WITH(NOLOCK)
                    ON t3.id = t1.in_file
                WHERE 
                    t2.in_name = 'meeting_excel'  
                    AND t1.in_name IN ('export_path', '{#in_name}')
            ";

            sql = sql.Replace("{#in_name}", this.getProperty("in_name", ""));

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_name = item.getProperty("in_name", "");
                string in_value = item.getProperty("in_value", "");
                string in_file = item.getProperty("in_file", "");
                string filename = item.getProperty("filename", "");

                if (in_name == "export_path")
                {
                    string export_path = in_value;
                    if (!export_path.EndsWith(@"\"))
                    {
                        export_path = export_path + @"\";
                    }
                    itmReturn.setProperty("export_path", export_path);
                    itmReturn.setProperty("export_path_fid", in_file);
                    itmReturn.setProperty("export_path_fname", filename);
                }
                else
                {
                    itmReturn.setProperty("template_path", in_value);
                    itmReturn.setProperty("template_path_fid", in_file);
                    itmReturn.setProperty("template_path_fname", filename);
                }
            }

            string told = itmReturn.getProperty("template_path", "");
            string tfid = itmReturn.getProperty("template_path_fid", "");
            string tfname = itmReturn.getProperty("template_path_fname", "");

            if (tfid != "" && tfname != "")
            {
                string tfvault = VaultPath(cfg, tfid, tfname);
                if (System.IO.File.Exists(tfvault))
                {
                    itmReturn.setProperty("template_path", tfvault);
                    itmReturn.setProperty("template_path_old", told);
                    itmReturn.setProperty("template_path_vault", tfvault);
                }
            }
        }

        private string VaultPath(TConfig cfg, string fileid, string filename)
        {
            //路徑切出來
            string id_1 = fileid.Substring(0, 1);
            string id_2 = fileid.Substring(1, 2);
            string id_3 = fileid.Substring(3, 29);

            string path = @"C:\Aras\Vault\"
                + cfg.strDatabaseName + @"\"
                + id_1 + @"\"
                + id_2 + @"\"
                + id_3 + @"\"
                + filename;

            return path;
        }

        private void Remove(TConfig cfg, Item itmReturn)
        {
            itmReturn.setProperty("in_file", "");
            Upload(cfg, itmReturn);
        }

        private void Upload(TConfig cfg, Item itmReturn)
        {
            string id = itmReturn.getProperty("id", "");
            string in_file = itmReturn.getProperty("in_file", "");

            string sql = "UPDATE In_Variable_detail SET"
                + "  in_file = '" + in_file + "'"
                + " WHERE id = '" + id + "'";

            Item itmSQL = cfg.inn.applySQL(sql);

            if (itmSQL.isError())
            {
                throw new Exception("執行失敗");
            }
        }

        private void Modal(TConfig cfg, Item itmReturn)
        {
            string sql = @"
                SELECT 
	                t1.*
                FROM 
	                In_Variable_detail t1 WITH(NOLOCK)
                LEFT OUTER JOIN 
                    [FILE] t2 WITH(NOLOCK)
                    ON t2.id = t1.in_file
                WHERE
	                t1.id = '{#id}'
            ";

            sql = sql.Replace("{#id}", cfg.id);

            Item item = cfg.inn.applySQL(sql);

            if (item.isError() || item.getResult() == "")
            {
                throw new Exception("查無資料");
            }

            itmReturn.setProperty("in_name", item.getProperty("in_name", ""));
            itmReturn.setProperty("in_value", item.getProperty("in_value", ""));
            itmReturn.setProperty("in_description", item.getProperty("in_description", ""));
            itmReturn.setProperty("old_file", item.getProperty("in_file", ""));
        }

        private void Page(TConfig cfg, Item itmReturn)
        {
            string sql = @"
                SELECT 
	                t1.id            AS 'vid'
	                , t1.in_name     AS 'vname' 
	                , t2.id
	                , t2.in_name
	                , t2.in_value
	                , t2.in_description
	                , t2.in_file
                FROM 
	                In_Variable t1 WITH(NOLOCK)
                INNER JOIN 
	                In_Variable_detail t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                WHERE
	                t1.in_name = 'meeting_excel'
	                AND t2.in_name in ('certificatembr_path')
            ";

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_value = item.getProperty("in_value", "");
                string in_description = item.getProperty("in_description", "");
                string in_file = item.getProperty("in_file", "");

                string inn_url = in_value.ToLower().Replace(@"c:\site\cta_git\", "../").Replace(@"\", "/");

                if (in_file != "")
                {
                    in_description = "<div class='form-inline'>"
                        + "<label>" + in_description + "</label>"
                        + "&nbsp;"
                        + "<a style='display: initial' target='_blank' href='../DownloadMeetingFile.aspx?fileid=" + in_file + "'>"
                        + "<i class='fa fa-cloud-download'></i>"
                        + "</a>"
                        + "</div>";
                }

                item.setType("inn_file");
                item.setProperty("no", (i + 1).ToString());
                item.setProperty("inn_url", inn_url);
                item.setProperty("inn_desc", in_description);
                itmReturn.addRelationship(item);
            }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public Innosoft.InnovatorHelper InnH { get; set; }

            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }

            public string in_name { get; set; }
            public string id { get; set; }
            public string scene { get; set; }
        }
    }
}