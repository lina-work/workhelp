﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class In_UpdateUser_Identity_Card : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的:將 User 的登入帳號欄位更新
                位置: onAfterAdd, onAfterUpdate
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]In_UpdateUser_Identity_Card";

            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            string sql = "";
            string aml = "";


            //檢查格式A~Z(1) 0~9(9)(台灣身分證)
            var regex = new System.Text.RegularExpressions.Regex("^[A-Z]{1}[0-9]{9}$");
            //檢查格式A~Z(1)A~Z(2) 0~9(8)(居留證)
            var pat = new System.Text.RegularExpressions.Regex("^[A-Z]{1}[A-Z]{1}[0-9]{8}$");
            //檢查格式A~Z(1)A~Z(2) 0~9(6)(外國護照)
            var pas = new System.Text.RegularExpressions.Regex("^[A-Z]{1}[A-Z]{1}[0-9]{6}$");

            //判定類型
            string id_type = ""; //taiwanid,passport,bbb,cc

            string userID = this.getProperty("in_user_id", "");
            aml = @"<AML><Item type='User' action='get'><id>#userID</id></Item></AML>";
            aml = aml.Replace("#userID", userID);
            Item iUser = inn.applyAML(aml);

            Item itmIdentity = inn.newItem("Identity", "get");
            itmIdentity.setProperty("in_user", userID);
            itmIdentity = itmIdentity.apply();
            string strIdentityId = itmIdentity.getID();



            string old_login_name = iUser.getProperty("login_name", "");//修改之前的登入帳號(丟給前台顯示唯獨欄位-舊身分證)
                                                                        //this.setProperty("old_login_name",old_login_name);
            string new_login_name = this.getProperty("new_login_name", "");//修改之後的登入帳號(更新之前拿來比對)

            string[] Special_text = new string[] { "(", "?", "=", ".", "*", "[", "@", "#", "$", "%", "^", "&", ".", "+", "=", "]", ")", " " };//特殊字元

            string b_text = new_login_name.Trim().TrimEnd(" ".ToCharArray());





            //長度判定
            if (b_text.Length == 4)//學校代碼
            {
                id_type = "school_code";
            }
            else if (b_text.Length == 6)//學校代碼
            {
                id_type = "school_code";
            }
            else if (b_text.Length == 8)//外國護照
            {
                id_type = "passport";
            }
            else if (b_text.Length == 10)//身分證or居留證
            {
                if (regex.IsMatch(b_text))//身分證
                {
                    id_type = "identity_card";
                }
                else if (pat.IsMatch(b_text))//居留證
                {
                    id_type = "resident_permit";
                }
                else
                {
                    //不符合以上兩個格式
                    throw new Exception("身分證or居留證_格式錯誤");
                    //strError += "身分格式錯誤" + "\n";
                }
            }
            else
            {
                //非6,8,10碼
                throw new Exception("輸入格式錯誤,請確認字數");
                //strError += "輸入格式錯誤,請確認字數" + "\n";
            }



            switch (id_type)
            {
                case "school_code"://學校代碼
                    this.setProperty("state", "ok");
                    this.setProperty("message", "此為學校代碼");
                    break;
                case "passport"://外國護照

                    if (!pas.IsMatch(b_text))//身分證
                    {
                        //strError += "護照格式錯誤" + "\n";
                        throw new Exception("護照格式錯誤");
                    }
                    else
                    {
                        this.setProperty("state", "ok");
                        this.setProperty("message", "此為外國護照");
                    }
                    break;
                case "identity_card"://身分證
                                     //存放檢查碼之外的數字
                    int[] seed = new int[10];
                    //字母陣列
                    string[] charMapping = new string[] { "A", "B", "C", "D", "E", "F", "G", "H", "J", "K", "L", "M", "N", "P", "Q", "R", "S", "T", "U", "V", "X", "Y", "W", "Z", "I", "O" };

                    string target = b_text.Substring(0, 1);
                    string gender = "";

                    for (int index = 0; index < charMapping.Length; index++)
                    {
                        if (charMapping[index] == target)
                        {
                            index += 10;
                            seed[0] = index / 10;
                            seed[1] = (index % 10) * 9;
                            break;
                        }
                    }
                    for (int index = 2; index < 10; index++)
                    {
                        seed[index] = Convert.ToInt32(b_text.Substring(index - 1, 1)) * (10 - index);
                    }
                    //檢查是否為正確的身分證(依據身分證檢查規則)
                    if ((10 - (seed.Sum() % 10)) % 10 != Convert.ToInt32(b_text.Substring(9, 1)))
                    {
                        this.setProperty("state", "ng");
                        this.setProperty("message", "請輸入正確身分證");
                        //strError += "請輸入正確身分證" + "\n";
                        throw new Exception("請輸入正確身分證");
                    }
                    else
                    {
                        this.setProperty("state", "ok");
                        this.setProperty("message", "身分證號碼正確");
                    }
                    break;
                case "resident_permit"://居留證

                    string sex = "";
                    string nationality = "";
                    char[] strArr = b_text.ToCharArray(); // 字串轉成char陣列

                    int verifyNum = 0;
                    int[] pidResidentFirstInt = { 1, 10, 9, 8, 7, 6, 5, 4, 9, 3, 2, 2, 11, 10, 8, 9, 8, 7, 6, 5, 4, 3, 11, 3, 12, 10 };
                    char[] pidCharArray = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };
                    // 第一碼
                    verifyNum += pidResidentFirstInt[Array.BinarySearch(pidCharArray, strArr[0])];
                    // 原居留證第二碼英文字應轉換為10~33，並僅取個位數*6，這裡直接取[(個位數*6) mod 10]
                    int[] pidResidentSecondInt = { 0, 8, 6, 4, 2, 0, 8, 6, 2, 4, 2, 0, 8, 6, 0, 4, 2, 0, 8, 6, 4, 2, 6, 0, 8, 4 };
                    // 第二碼
                    verifyNum += pidResidentSecondInt[Array.BinarySearch(pidCharArray, strArr[1])];
                    // 第三~八碼
                    for (int i = 2, j = 7; i < 9; i++, j--)
                    {
                        verifyNum += Convert.ToInt32(strArr[i].ToString(), 10) * j;
                    }
                    // 檢查碼
                    verifyNum = (10 - (verifyNum % 10)) % 10;
                    bool ok = verifyNum == Convert.ToInt32(strArr[9].ToString(), 10);
                    if (ok)
                    {
                        // 判斷性別 & 國籍
                        sex = "男";
                        if (strArr[1] == 'B' || strArr[1] == 'D') sex = "女";
                        nationality = "外籍人士";
                        if (strArr[1] == 'A' || strArr[1] == 'B') nationality += "(臺灣地區無戶籍國民、大陸地區人民、港澳居民)";
                        {
                            this.setProperty("state", "ok");
                            this.setProperty("message", "此為居留證");
                        }
                    }
                    else
                    {
                        this.setProperty("state", "ng");
                        this.setProperty("message", "請輸入正確居留證");
                        //strError += "請輸入正確居留證" + "\n";
                        throw new Exception("請輸入正確居留證");
                    }
                    break;
            }

            //檢查是否有特殊字元存在
            foreach (string _text in Special_text)
            {
                if (new_login_name.Contains(_text))
                {
                    throw new Exception("身分證包含空白與或特殊字元");
                }
            }

            // //更新之前先找查詢新的帳號是否已在資料庫
            // Item itmUsers = inn.newItem("User","get");
            // itmUsers =  itmUsers.apply();
            // for(int i = 0;i < itmUsers.getItemCount();i++)
            // {
            //     Item itmUser = itmUsers.getItemByIndex(i);
            //     if(itmUser.getProperty("login_name","") == new_login_name)//判斷新輸入的是否存在
            //     {
            //         this.setProperty("error","該身分證已存在");
            //         //this.setProperty("test","1");
            //         throw new Exception("該身分證已存在");
            //     }
            // }

            Item itmUsers = inn.applySQL("SELECT id FROM [USER] WITH(NOLOCK) WHERE login_name = '" + new_login_name + "'");
            if (itmUsers.isError())
            {
                throw new Exception("檢查新帳號發生錯誤");
            }
            else if (itmUsers.getResult() != "")
            {
                this.setProperty("error", "該身分證已存在");
                throw new Exception("該身分證已存在");
            }

            Item itmMUsers = inn.applySQL("SELECT id FROM IN_MEETING_USER WITH(NOLOCK) WHERE in_mail = '" + new_login_name + "'");
            if (itmMUsers.isError())
            {
                throw new Exception("檢查註冊資料發生錯誤");
            }
            else if (itmMUsers.getResult() != "")
            {
                this.setProperty("error", "該身分證已註冊");
                throw new Exception("該身分證已註冊");
            }

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                strIdentityId = strIdentityId,

                old_login_name = old_login_name,
                new_login_name = new_login_name,
                user_id = iUser.getID(),
                user_last_name = iUser.getProperty("last_name", ""),

            };

            //身分證上遮罩
            cfg.new_sno_display = GetSidDisplay(cfg.new_login_name);

            UpdateMeetings(cfg, "賽事", "");
            UpdateMeetings(cfg, "課程", "_CLA");
            UpdateOther(cfg);

            this.setProperty("success", "身分證更改成功");
            //this.setProperty("test","0");
            //this.setProperty("","0");//是否導向的依據 0不導向1導向

            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

            return this;
        }

        private void UpdateMeetings(TConfig cfg, string title, string mode)
        {
            string sql = "";
            string action = "";
            Item itmSQL = null;

            /////////////////////////////////////////////////////////////

            action = "[" + title + "]更新 被報名資料: ";

            sql = @"
                UPDATE IN{#mode}_MEETING_USER SET
                    in_sno = '{#new_login_name}'
                    , in_mail = REPLACE(in_mail, '{#old_login_name}', '{#new_login_name}')
                    , in_sno_display = '{#in_sno_display}'
                WHERE 
                    in_sno = '{#old_login_name}'
            ";

            sql = sql.Replace("{#mode}", mode)
                .Replace("{#new_login_name}", cfg.new_login_name)
                .Replace("{#in_sno_display}", cfg.new_sno_display)
                .Replace("{#old_login_name}", cfg.old_login_name);

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, action + sql);
            itmSQL = cfg.inn.applySQL(sql);
            if (itmSQL.isError()) throw new Exception(action + "發生錯誤");

            /////////////////////////////////////////////////////////////

            action = "[" + title + "]更新 協助報名者: ";

            sql = @"
                UPDATE IN{#mode}_MEETING_USER SET
                    in_creator_sno = '{#new_login_name}'
                WHERE 
                    in_creator_sno = '{#old_login_name}'
            ";

            sql = sql.Replace("{#mode}", mode)
                .Replace("{#new_login_name}", cfg.new_login_name)
                .Replace("{#old_login_name}", cfg.old_login_name);

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, action + sql);
            itmSQL = cfg.inn.applySQL(sql);
            if (itmSQL.isError()) throw new Exception(action + "發生錯誤");

            /////////////////////////////////////////////////////////////

            action = "[" + title + "]更新 工作人員: ";

            sql = @"
                UPDATE IN{#mode}_MEETING_STAFF SET
                    in_sno = '{#new_login_name}'
                WHERE 
                    in_sno = '{#old_login_name}'
            ";

            sql = sql.Replace("{#mode}", mode)
                .Replace("{#new_login_name}", cfg.new_login_name)
                .Replace("{#old_login_name}", cfg.old_login_name);

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, action + sql);
            itmSQL = cfg.inn.applySQL(sql);
            if (itmSQL.isError()) throw new Exception(action + "發生錯誤");

            /////////////////////////////////////////////////////////////

            action = "[" + title + "]更新 已結束報名單位列表: ";

            sql = @"
                UPDATE IN{#mode}_MEETING_GYMLIST SET
                    in_creator_sno = '{#new_login_name}'
                WHERE 
                    in_creator_sno = '{#old_login_name}'
            ";

            sql = sql.Replace("{#mode}", mode)
                .Replace("{#new_login_name}", cfg.new_login_name)
                .Replace("{#old_login_name}", cfg.old_login_name);

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, action + sql);
            itmSQL = cfg.inn.applySQL(sql);
            if (itmSQL.isError()) throw new Exception(action + "發生錯誤");
        }

        private void UpdateOther(TConfig cfg)
        {
            string sql = "";
            string action = "";
            Item itmSQL = null;

            /////////////////////////////////////////////////////////////

            action = "[其他]更新 繳費單: ";

            sql = @"
                UPDATE IN_MEETING_PAY SET
                    in_creator_sno = '{#new_login_name}'
                WHERE 
                    in_creator_sno = '{#old_login_name}'
            ";

            sql = sql.Replace("{#new_login_name}", cfg.new_login_name)
                .Replace("{#old_login_name}", cfg.old_login_name);

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, action + sql);
            itmSQL = cfg.inn.applySQL(sql);
            if (itmSQL.isError()) throw new Exception(action + "發生錯誤");

            /////////////////////////////////////////////////////////////

            action = "[其他]更新 繳費單明細(協助報名者): ";

            sql = @"
                UPDATE IN_MEETING_NEWS SET
                    in_creator_sno = '{#new_login_name}'
                WHERE 
                    in_creator_sno = '{#old_login_name}'
            ";

            sql = sql.Replace("{#new_login_name}", cfg.new_login_name)
                .Replace("{#old_login_name}", cfg.old_login_name);

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, action + sql);
            itmSQL = cfg.inn.applySQL(sql);
            if (itmSQL.isError()) throw new Exception(action + "發生錯誤");

            /////////////////////////////////////////////////////////////

            action = "[其他]更新 繳費單明細(被報名者): ";

            sql = @"
                UPDATE IN_MEETING_NEWS SET
                    in_sno = '{#new_login_name}'
                WHERE 
                    in_sno = '{#old_login_name}'
            ";

            sql = sql.Replace("{#new_login_name}", cfg.new_login_name)
                .Replace("{#old_login_name}", cfg.old_login_name);

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, action + sql);
            itmSQL = cfg.inn.applySQL(sql);
            if (itmSQL.isError()) throw new Exception(action + "發生錯誤");

            /////////////////////////////////////////////////////////////

            action = "[其他]更新 Identity(角色): ";

            //用Identity的ID為依據,取得Identity
            sql = "Update [Identity] set ";
            sql += "in_number='" + cfg.new_login_name + "'";
            sql += ",keyed_name=N'" + cfg.new_login_name + " " + cfg.user_last_name + "'";
            sql += ",name=N'" + cfg.new_login_name + " " + cfg.user_last_name + "'";
            sql += " where id='" + cfg.strIdentityId + "'";

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, action + sql);
            itmSQL = cfg.inn.applySQL(sql);
            if (itmSQL.isError()) throw new Exception(action + "發生錯誤");

            /////////////////////////////////////////////////////////////

            action = "[其他]更新 In_Resume(講師履歷): ";

            sql = "Update [In_Resume] set ";
            sql += "in_sno='" + cfg.new_login_name + "'";
            sql += ",login_name='" + cfg.new_login_name + "'";
            sql += " where in_user_id='" + cfg.user_id + "'";

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, action + sql);
            itmSQL = cfg.inn.applySQL(sql);
            if (itmSQL.isError()) throw new Exception(action + "發生錯誤");

            /////////////////////////////////////////////////////////////

            action = "[其他]更新 User(使用者): ";

            sql = "Update [User] set ";
            sql += "login_name='" + cfg.new_login_name + "'";
            sql += ",user_no='" + cfg.new_login_name + "'";
            sql += ",first_name='" + cfg.new_login_name + "'";
            sql += ",keyed_name=N'" + cfg.new_login_name + " " + cfg.user_last_name + "'";
            sql += " where id='" + cfg.user_id + "'";

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, action + sql);
            itmSQL = cfg.inn.applySQL(sql);
            if (itmSQL.isError()) throw new Exception(action + "發生錯誤");
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }

            public string old_login_name { get; set; }
            public string new_login_name { get; set; }
            public string new_sno_display { get; set; }

            public string user_id { get; set; }
            public string user_last_name { get; set; }
        }



        //身分證字號(外顯)
        private string GetSidDisplay(string in_sno)
        {
            if (in_sno.Length == 10)
            {
                //10碼(身分證處理)
                string in_sno_1 = in_sno.Substring(0, 2);
                string in_sno_2 = in_sno.Substring(6, 4);
                return in_sno_1 + "****" + in_sno_2;//A1****6789
            }
            else
            {
                return "";
            }
        }
    }
}