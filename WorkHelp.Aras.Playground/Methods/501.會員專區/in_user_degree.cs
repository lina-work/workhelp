﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class in_user_degree : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
            目的: 晉段會員資料
            日期: 2021-03-23 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]in_user_degree";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "input dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                scene = itmR.getProperty("scene", ""),
                in_committee = itmR.getProperty("in_committee", ""),
                in_degree = itmR.getProperty("in_degree", ""),
                in_filter = itmR.getProperty("in_filter", ""),
            };

            //取得登入者資訊
            cfg.itmLogin = inn.applySQL("SELECT * FROM IN_RESUME WITH(NOLOCK) WHERE in_user_id = N'" + cfg.strUserId + "'");
            if (cfg.itmLogin.isError() || cfg.itmLogin.getResult() == "")
            {
                throw new Exception("登入者履歷異常");
            }

            string in_is_admin = cfg.itmLogin.getProperty("in_is_admin", "");

            //權限檢核
            cfg.itmPermit = inn.applyMethod("In_CheckIdentity", "<method>" + strMethodName + "</method><code>" + "ALL" + "</code>");
            cfg.isMeetingAdmin = cfg.itmPermit.getProperty("isMeetingAdmin", "") == "1";

            if (cfg.isMeetingAdmin || in_is_admin == "1")
            {
                Query(cfg, itmR);
            }

            return itmR;
        }

        //查詢
        private void Query(TConfig cfg, Item itmReturn)
        {
            Item itmResumes = null;
            if (cfg.scene == "")
            {
                return;
            }
            else
            {
                itmResumes = GetResumes(cfg);
            }

            int count = itmResumes.getItemCount();

            List<TField> fields = new List<TField>();
            fields.Add(new TField { title = "姓名", property = "in_name" });
            fields.Add(new TField { title = "性別", property = "in_gender" });
            fields.Add(new TField { title = "西元生日", property = "in_birth", format = "yyyy-MM-dd", getValue = BirthDay });
            fields.Add(new TField { title = "身分證號", property = "in_sno", getValue = SnoDisplay });
            fields.Add(new TField { title = "電子郵件", property = "in_email", getValue = ClearEmail });
            fields.Add(new TField { title = "發信狀況", property = "in_email_exam" });
            fields.Add(new TField { title = "段位", property = "in_degree", getValue = DegreeLabel });
            fields.Add(new TField { title = "所屬委員會", property = "committee_short_name" });
            fields.Add(new TField { title = "所屬道館", property = "in_current_org", css = "text-left" });
            fields.Add(new TField { title = "功能", getValue = FuncButtonns });

            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            head.AppendLine("<thead>");
            foreach (var field in fields)
            {
                head.AppendLine("<th class='text-center' data-field='" + field.property + "' data-sortable='true'>" + field.title + "</th>");
            }
            head.AppendLine("</thead>");


            body.AppendLine("<tbody>");
            for (int i = 0; i < count; i++)
            {
                Item itmResume = itmResumes.getItemByIndex(i);
                //string id = item.getProperty("id", "");
                //string in_name = item.getProperty("in_name", "");
                //string in_org = item.getProperty("in_org", "");
                //item.setProperty("in_link", GetDashboardStaffLink(id, in_name, in_org));

                body.AppendLine("<tr>");
                foreach (var field in fields)
                {
                    string value = GetValue(cfg, itmResume, field);
                    body.AppendLine("<td class='" + field.css + "' data-field='" + field.property + "' >" + value + "</td>");
                }
                body.AppendLine("</tr>");
            }
            body.AppendLine("</tbody>");


            string table_name = "resume_table";

            StringBuilder builder = new StringBuilder();
            builder.AppendLine(GetTableAttribute(table_name, "#member_toolbar"));
            builder.Append(head);
            builder.Append(body);
            builder.AppendLine("</table>");
            builder.AppendLine("<script>");
            builder.AppendLine("$('#" + table_name + "').bootstrapTable();");
            builder.AppendLine("if ($(window).width() <= 768) { $('#" + table_name + "').bootstrapTable('toggleView'); }");
            builder.AppendLine("</script>");

            itmReturn.setProperty("inn_table", builder.ToString());
        }

        private string GetValue(TConfig cfg, Item item, TField field)
        {
            if (field.getValue == null)
            {
                if (field.property == "")
                {
                    return "";
                }
                else
                {
                    return item.getProperty(field.property, "");
                }
            }
            else
            {
                return field.getValue(cfg, item, field);
            }
        }

        private Item GetResumes(TConfig cfg)
        {
            List<string> conditions = new List<string>();

            if (cfg.in_committee != "")
            {
                conditions.Add("AND t1.in_manager_name = N'" + cfg.in_committee + "'");
            }

            if (cfg.in_degree != "")
            {
                if (cfg.in_degree == "0")
                {
                    conditions.Add("AND ISNULL(t1.in_degree, '') = ''");

                }
                else
                {
                    conditions.Add("AND ISNULL(t1.in_degree, '') = '" + cfg.in_degree + "'");
                }
            }

            if (cfg.in_filter != "")
            {
                List<string> or_cond = new List<string>();
                or_cond.Add("(t1.in_name LIKE '%" + cfg.in_filter + "%')");
                or_cond.Add("(t1.in_sno LIKE '%" + cfg.in_filter + "%')");
                or_cond.Add("(t1.in_gender LIKE '%" + cfg.in_filter + "%')");
                or_cond.Add("(t1.in_birth LIKE '%" + cfg.in_filter + "%')");
                or_cond.Add("(t1.in_tel LIKE '%" + cfg.in_filter + "%')");
                or_cond.Add("(t1.in_current_org LIKE '%" + cfg.in_filter + "%')");
                or_cond.Add("(t1.in_email LIKE '%" + cfg.in_filter + "%')");
                conditions.Add("AND ( " + string.Join(" OR ", or_cond) + " )");
            }

            string cond = string.Join("\r\n                    ", conditions);

            string sql = @"
                SELECT 
                    t1.id
                    , t1.in_name
                    , t1.in_gender
                    , t1.in_birth
                    , t1.in_sno
                    , t1.in_email
                    , t1.in_email_exam
                    , t1.in_current_org
                    , t1.in_degree
                    , ISNULL(t2.in_short_org, t1.in_name) AS 'committee_short_name'
                FROM
                    IN_RESUME t1 WITH(NOLOCK)
                LEFT OUTER JOIN
                    IN_RESUME t2 WITH(NOLOCK)
                    ON t2.id = t1.in_manager_org
                WHERE
                    t1.in_org = 0
                    {#cond}
                ORDER BY
                    t1.in_degree DESC
                    , t1.in_birth
            ";

            sql = sql.Replace("{#cond}", cond);

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }

            public string scene { get; set; }
            public string in_committee { get; set; }
            public string in_degree { get; set; }
            public string in_filter { get; set; }

            public Item itmLogin { get; set; }
            public Item itmPermit { get; set; }

            public bool isMeetingAdmin { get; set; }
        }

        private class TField
        {
            public string title { get; set; }
            public string property { get; set; }
            public string format { get; set; }
            public string css { get; set; }
            public Func<TConfig, Item, TField, string> getValue { get; set; }
        }

        private string FuncButtonns(TConfig cfg, Item item, TField field)
        {
            string resume_id = item.getProperty("id", "");
            if (resume_id == "") return "";

            return "<button class='btn btn-sm btn-primary' data-rid='" + resume_id + "' onclick='ResumeView_Click(this)'>檢視</button>";
        }

        private string SnoDisplay(TConfig cfg, Item item, TField field)
        {
            string in_sno = item.getProperty(field.property, "");
            //處理[身分證字號(外顯)]
            if (in_sno.Length == 10)
            {
                //10碼(身分證處理)
                string in_sno_1 = in_sno.Substring(0, 2);
                string in_sno_2 = in_sno.Substring(6, 4);
                return in_sno_1 + "****" + in_sno_2;//A1****6789
            }
            else if (in_sno.Length == 18)
            {
                //18碼(身分證處理-大陸)
                string in_sno_1 = in_sno.Substring(0, 6);
                string in_sno_2 = in_sno.Substring(14, 3);
                return in_sno_1 + "********" + in_sno_2 + "X";//110102********888X
            }
            else
            {
                //不屬於前兩者(身分證)可能為學校代碼
                return in_sno;
            }
        }

        private string DegreeLabel(TConfig cfg, Item item, TField field)
        {
            string value = item.getProperty(field.property, "");
            switch (value)
            {
                case "": return "無段位";
                case "10": return "白帶(10)";
                case "30": return "黃帶(8)";
                case "40": return "黃綠帶(7)";
                case "50": return "綠帶(6)";
                case "60": return "綠藍帶(5)";
                case "70": return "藍帶(4)";
                case "80": return "藍紅帶(3)";
                case "90": return "紅帶(2)";
                case "100": return "紅黑帶(1)";
                case "150": return "黑帶一品";
                case "250": return "黑帶二品";
                case "350": return "黑帶三品";
                case "1000": return "壹段";
                case "2000": return "貳段";
                case "3000": return "參段";
                case "4000": return "肆段";
                case "5000": return "伍段";
                case "6000": return "陸段";
                case "7000": return "柒段";
                case "8000": return "捌段";
                case "9000": return "玖段";
                case "11000": return "POOM-11";
                case "12000": return "POOM-12";
                case "13000": return "POOM-13";

                default: return value + "(未設定)";
            }
        }

        /// <summary>
        /// 清除 NA mail
        /// </summary>
        private string ClearEmail(TConfig cfg, Item item, TField field)
        {
            string value = item.getProperty(field.property, "");
            if (value.Contains("na@na.n"))
            {
                return "";
            }
            else
            {
                return value;
            }
        }

        /// <summary>
        /// 生日
        /// </summary>
        private string BirthDay(TConfig cfg, Item item, TField field)
        {
            string value = item.getProperty(field.property, "");
            if (value.Contains("1900") || value.Contains("1899"))
            {
                return "";
            }
            else
            {
                return GetDateTimeByFormat(value, field.format, hours: 8);
            }
        }

        /// <summary>
        /// 轉為民國年
        /// </summary>
        private string MapChineseYear(TConfig cfg, Item item, TField field)
        {
            string value = item.getProperty(field.property, "");
            if (value == "")
            {
                return "";
            }
            else
            {
                int result = GetIntVal(value) - 1911;
                return result.ToString();
            }
        }

        private string ClearMoney(TConfig cfg, Item item, TField field)
        {
            string value = item.getProperty(field.property, "");
            if (value == "" || value == "0")
            {
                return "0";
            }
            else
            {
                int result = GetIntVal(value);
                //0會被清空字串
                return result.ToString("###,###");
            }
        }

        /// <summary>
        /// 轉為是 or 否
        /// </summary>
        private string GetYN(TConfig cfg, Item item, TField field)
        {
            string value = item.getProperty(field.property, "");

            if (value == "0")
            {
                return "否";
            }
            else if (value == "1")
            {
                return "是";
            }
            else
            {
                return "";
            }
        }

        private string GetDegreeDisplay(TConfig cfg, Item item, TField field)
        {
            string value = item.getProperty(field.property, "");

            switch (value)
            {
                case "": return "無段位";
                case "10": return "白帶";
                case "30": return "黃帶";
                case "40": return "黃綠帶";
                case "50": return "綠帶";
                case "60": return "綠藍帶";
                case "70": return "藍帶";
                case "80": return "藍紅帶";
                case "90": return "紅帶";
                case "100": return "紅黑帶";
                case "150": return "黑帶一品";
                case "250": return "黑帶二品";
                case "350": return "黑帶三品";
                case "1000": return "壹段";
                case "2000": return "貳段";
                case "3000": return "參段";
                case "4000": return "肆段";
                case "5000": return "伍段";
                case "6000": return "陸段";
                case "7000": return "柒段";
                case "8000": return "捌段";
                case "9000": return "玖段";

                case "11000": return "11";
                case "12000": return "12";
                case "13000": return "13";
                default: return "";
            }
        }

        private string GetTableAttribute(string table_name, string toolbar_name)
        {
            return "<table id='" + table_name + "' class='table table-hover table-bordered table-rwd rwd rwdtable' "
                + " data-toggle='table' "
                + " data-toolbar='" + toolbar_name + "' "
                + " data-click-to-select='true' "
                + " data-striped='false' "
                + " data-search='false' "
                + " data-pagination='true' "
                + " data-page-size='10' "
                + " data-show-pagination-switch='false'"
                + ">";
        }

        private string GetDateTimeByFormat(string value, string format, int hours = 0)
        {
            if (value == "") return "";

            DateTime dt = DateTime.Now;
            if (DateTime.TryParse(value.Replace("/", "-"), out dt))
            {
                return dt.AddHours(hours).ToString(format);
            }
            else
            {
                return value;
            }
        }

        private int GetIntVal(string value, int def = 0)
        {
            int result = def;
            Int32.TryParse(value, out result);
            return result;
        }
    }
}