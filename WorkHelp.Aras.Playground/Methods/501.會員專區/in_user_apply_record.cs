﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class in_user_apply_record : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
            目的: 各類申請歷史記錄
            輸入: 
               - meeting id
               - 查詢條件
            日誌:
               - 2021.04.29 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]in_user_apply_record";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                InnH = _InnH,

                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),

                meeting_id = "A9F45C0EFD944ADAB1D7975D8C2E09C3",
                mode = itmR.getProperty("mode", ""),
                scene = itmR.getProperty("scene", ""),

                MtName = "IN_MEETING",
                MUserName = "IN_MEETING_USER",
            };


            Item itmIdentity = cfg.inn.newItem("Identity", "get");
            itmIdentity.setProperty("in_user", cfg.strUserId);
            itmIdentity = itmIdentity.apply();
            cfg.strIdentityId = itmIdentity.getID();

            //取得登入者權限
            Item itmPermit = inn.applyMethod("In_CheckIdentity", "<method>" + strMethodName + "</method><code>ALL</code>");
            cfg.isMeetingAdmin = itmPermit.getProperty("isMeetingAdmin", "") == "1";
            cfg.isCommittee = itmPermit.getProperty("isCommittee", "") == "1";
            cfg.isGymOwner = itmPermit.getProperty("isGymOwner", "") == "1";
            cfg.isGymAssistant = itmPermit.getProperty("isGymAssistant", "") == "1";

            if (cfg.mode == "cla")
            {
                cfg.MtName = "IN_CLA_MEETING";
                cfg.MUserName = "IN_CLA_MEETING_USER";
            }

            if (cfg.meeting_id == "")
            {
                return itmR;
            }

            if (cfg.scene == "")
            {
                cfg.itmMeeting = cfg.inn.applySQL("SELECT in_title FROM " + cfg.MtName + " WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
                cfg.mt_title = cfg.itmMeeting.getProperty("in_title", "");

                AppendTabs(cfg, itmR);
            }

            itmR.setProperty("in_title", cfg.mt_title);

            return itmR;
        }

        private void AppendTabs(TConfig cfg, Item itmReturn)
        {
            List<TTab> tabs = new List<TTab>();
            tabs.Add(new TTab { id = "muser_register", title = "會員申請", GetListFunc = GetListRegister, AppendListAction = AppendRegistrList, type = TabEnum.Register });
            tabs.Add(new TTab { id = "muser_change", title = "資料變更", GetListFunc = GetListChange, AppendListAction = AppendRegistrList, type = TabEnum.Change });
            tabs.Add(new TTab { id = "muser_reissue",  title = "證照補發", GetListFunc = GetListReissue, AppendListAction = AppendRegistrList, type = TabEnum.Reissue });
            tabs.Add(new TTab { id = "muser_cert", title = "證書上傳", GetListFunc = GetListCert, AppendListAction = AppendRegistrList, type = TabEnum.Cert });

            string active = "active";
            foreach (var tab in tabs)
            {
                FixTab(cfg, tab);

                if (!tab.is_error && tab.count > 0)
                {
                    tab.active = active;
                    active = "";
                }
            }

            StringBuilder builder = new StringBuilder();

            builder.Append("<ul class='nav nav-pills' style='justify-content: start ;' id='pills-tab' role='tablist'>");
            AppendTabHeads(cfg, tabs, builder);
            builder.Append("</ul>");

            builder.Append("<div class='tab-content' id='pills-tabContent'>");
            AppendTabContents(cfg, tabs, builder);
            builder.Append("</div>");

            itmReturn.setProperty("inn_tabs", builder.ToString());
        }

        private void FixTab(TConfig cfg, TTab tab)
        {
            tab.active = "";

            tab.li_id = "li_" + tab.id;
            tab.a_id = "a_" + tab.id;
            tab.content_id = "box_" + tab.id;
            tab.folder_id = "folder_" + tab.id;
            tab.toggle_id = "toggle_" + tab.id;
            tab.table_id = "table_" + tab.id;
            tab.items = tab.GetListFunc(cfg, tab);

            tab.is_error = tab.items.isError();

            tab.count = !tab.is_error && tab.items.getResult() != ""
                ? tab.items.getItemCount()
                : 0;
        }

        private void AppendTabHeads(TConfig cfg, List<TTab> tabs, StringBuilder builder)
        {
            foreach (var tab in tabs)
            {
                if (tab.is_error) continue;
                if (tab.count <= 0) continue;

                builder.Append("<li id='" + tab.li_id + "' class='nav-item " + tab.active + "'>");
                builder.Append("<a id='" + tab.a_id + "' href='#" + tab.content_id + "' class='nav-link' data-toggle='pill' role='tab' aria-controls='pills-home' aria-selected='true' onclick='StoreTab_Click(this)'>");
                builder.Append(tab.title + "(" + tab.count + ")");
                builder.Append("</a>");
                builder.Append("</li>");
            }
        }

        private void AppendTabContents(TConfig cfg, List<TTab> tabs, StringBuilder builder)
        {
            foreach (var tab in tabs)
            {
                if (tab.is_error) continue;
                if (tab.count <= 0) continue;

                builder.Append("<div class='tab-pane fade in " + tab.active + "' id='" + tab.content_id + "' role='tabpanel'>");
                builder.Append("    <div class='showsheet_ clearfix'>");
                //builder.Append("        <div class='box-header'>");
                //builder.Append("            <h4 class='box-header with-border'>");
                //builder.Append(tab.title);
                //builder.Append("                <small>(0)");
                //builder.Append("                </small>");
                //builder.Append("            </h4>");
                //builder.Append("            <div class='box-tools pull-right'>");
                //builder.Append("                <button type='button' class='btn btn-box-tool fold_type' data-widget='collapse' style='width: 40px; height: 20px;'>");
                //builder.Append("                    <a id='" + tab.toggle_id + "' class='accordion-toggle box-title form-box-title fa fa-plus' style='font-size: 25px;' data-toggle='collapse' data-parent='#accordion' href='#" + tab.folder_id + "'>");
                //builder.Append("                    </a>");
                //builder.Append("                </button>");
                //builder.Append("            </div>");
                //builder.Append("        </div>");

                //builder.Append("        <div id='" + tab.folder_id + "' class='collapse'>");
                builder.Append("        <div id='" + tab.folder_id + "'>");
                builder.Append("            <div class='float-btn clearfix'>");
                builder.Append("            </div>");

                tab.AppendListAction(cfg, tab, builder);

                builder.Append("        </div>");


                builder.Append("    </div>");
                builder.Append("</div>");
            }
        }

        private void AppendRegistrList(TConfig cfg, TTab tab, StringBuilder builder)
        {
            List<TField> fields = new List<TField>();
            fields.Add(new TField { title = "項次", property = "no" });

            if (tab.type == TabEnum.Register)
            {
                fields.Add(new TField { title = "類別", defval = "會員類" });
                fields.Add(new TField { title = "申請項目", getVal = GetApplyInL2 });
            }
            else if (tab.type == TabEnum.Reissue)
            {
                fields.Add(new TField { title = "類別", property = "in_l1" });
                fields.Add(new TField { title = "申請項目", getVal = GetPayStatusIcon });
            }
            else
            {
                fields.Add(new TField { title = "類別", property = "in_l1" });
                fields.Add(new TField { title = "申請項目", property = "in_l2" });
            }
            
            fields.Add(new TField { title = "申請日期", getVal = GetApplyDate});
            fields.Add(new TField { title = "申請者", getVal = GetApplyUser });
            fields.Add(new TField { title = "審核狀態", getVal = GetVerifyDisplay });
            fields.Add(new TField { title = "審核說明", property = "in_ass_ver_memo", hcss = "col-sm-3" });
            fields.Add(new TField { title = "修改狀態", getVal = GetChangeStatus});

            if (tab.type == TabEnum.Register)
            {
                fields.Add(new TField { title = "功能", getVal = GetVerifyButton2 });
            }
            else
            {
                fields.Add(new TField { title = "功能", getVal = GetVerifyButton });
            }

            Dictionary<string, TTable> map = MapMUsers(tab.items);

            //分組 Table
            foreach (var kv in map)
            {
                var table = kv.Value;
                table.id = tab.table_id + "_" + table.code;
                AppendTable(cfg, tab, table, fields, builder);
            }
        }

        private void AppendTable(TConfig cfg, TTab tab, TTable table, List<TField> fields, StringBuilder builder)
        {
            var list = table.list;

            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();


            head.Append("<thead>");
            head.Append("  <tr>");
            for (int i = 0; i < fields.Count; i++)
            {
                TField field = fields[i];
                head.Append("    <th class='" + field.hcss + "'>" + field.title + "</th>");
            }
            head.Append("  </tr>");
            head.Append("</thead>");


            body.Append("<tbody>");
            for (int i = 0; i < list.Count; i++)
            {
                Item item = list[i];
                item.setProperty("no", (i + 1).ToString());

                AppendRow(cfg, tab, fields, item, body);
            }
            body.Append("</tbody>");

            builder.Append("<h4 class='box-header with-border'>" + table.title + " (" + list.Count + ")</h4>");
            builder.Append("<table id='" + table.id + "' class='table table-hover table-bordered table-rwd rwd'>");
            builder.Append(head);
            builder.Append(body);
            builder.Append("</table>");
        }

        private void AppendRow(TConfig cfg, TTab tab, List<TField> fields, Item item, StringBuilder body)
        {
            string id = item.getProperty("id", "");
            string in_l1 = item.getProperty("in_l1", "");
            string in_l2 = item.getProperty("in_l2", "");
            string in_paynumber = item.getProperty("in_paynumber", "");
            string resume_id = item.getProperty("resume_id", "");
            string resume_member_type = item.getProperty("resume_member_type", "");
            string resume_member_role = item.getProperty("resume_member_role", "");

            string verify = tab.type == TabEnum.Register
                ? "verify"
                : "";

            body.Append("<tr");
            body.Append(" data-id='" + id + "'");
            body.Append(" data-l1='" + in_l1 + "'");
            body.Append(" data-l2='" + in_l2 + "'");
            body.Append(" data-payno='" + in_paynumber + "'");
            body.Append(" data-rid='" + resume_id + "'");
            body.Append(" data-type='" + resume_member_type + "'");
            body.Append(" data-role='" + resume_member_role + "'");
            body.Append(" data-scene='" + verify  + "'");
            body.Append(" >");

            for (int i = 0; i < fields.Count; i++)
            {
                TField field = fields[i];
                string value = "";
                if (field.getVal != null)
                {
                    value = field.getVal(cfg, tab, field, item);
                }
                else if (!string.IsNullOrEmpty(field.property))
                {
                    value = item.getProperty(field.property, "");
                }
                else if (!string.IsNullOrEmpty(field.defval))
                {
                    value = field.defval;
                }

                body.Append("<td>" + value + "</td>");
            }
            body.Append("</tr>");
        }

        //會員申請
        private Item GetListRegister(TConfig cfg, TTab tab)
        {
            string member_condition = "";

            if (!cfg.isMeetingAdmin)
            {
                string chairman_id = "B8F2DA2ED4BF4AC1885270084185C557";//理事長
                string secretary_id = "05292A9E596C4C9DB734B2DC3E247A60";//秘書長(含副秘書長)
                string admin_id = "7833055F8A3E44948E4E1F70F4F6C4A5";//行政組
                string degree_id = "1FDEB413DD614E24841DF5559A955695";//晉段組
                string login_identity_list = Aras.Server.Security.Permissions.Current.IdentitiesList;

                //if (cfg.CCO.Permissions.IdentityListHasId(login_identity_list, chairman_id))
                //{
                //    member_condition = "";
                //}
                //else if (cfg.CCO.Permissions.IdentityListHasId(login_identity_list, secretary_id))
                //{
                //    member_condition = "";
                //}
                //else if (cfg.CCO.Permissions.IdentityListHasId(login_identity_list, admin_id))
                //{
                //    member_condition = "AND in_member_type in ('u_mbr', 'vip_mbr', 'vip_group')";
                //}
                //else if (cfg.CCO.Permissions.IdentityListHasId(login_identity_list, degree_id))
                //{
                //    member_condition = "AND in_member_type in ('gym', 'vip_gym')";
                //}
                //else
                //{
                //    member_condition = "AND ISNULL(in_member_type, '') = ''";
                //}
            }

            string sql = @"
                SELECT 
	                t1.*
	                , t2.id             AS 'resume_id'
	                , t2.in_name		AS 'resume_name'
	                , t2.in_member_type AS 'resume_member_type'
	                , t2.in_member_role AS 'resume_member_role'
	                , t2.in_member_unit AS 'resume_member_unit'
                FROM 
	                IN_MEETING_USER t1 WITH(NOLOCK) 
                INNER JOIN
	                IN_RESUME t2 WITH(NOLOCK)
	                ON t2.in_sno = t1.in_sno
                WHERE 
	                t1.source_id in 
                    (
                        '249FDB244E534EB0AA66C8E9C470E930'
                        , '5F73936711E04DC799CB02587F4FF7E0'
                        , '83B87AE0033640AA8DBA7AF2CF659479'
                        , '38CAB90DF1274E048520A801948AC65C'
                    )
	                AND t1.in_member_apply = N'是'
                    {#member_condition}
                ORDER BY
	                t2.in_member_type
	                , t2.in_member_unit
	                , t1.created_on
            ";

            sql = sql.Replace("{#member_condition}", member_condition);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);

        }

        //資料變更
        private Item GetListChange(TConfig cfg, TTab tab)
        {
            string sql = @"                SELECT 
	                t1.*
	                , t2.id             AS 'resume_id'
	                , t2.in_name		AS 'resume_name'
	                , t2.in_member_type AS 'resume_member_type'
	                , t2.in_member_role AS 'resume_member_role'
	                , t2.in_member_unit AS 'resume_member_unit'
                FROM 
	                IN_MEETING_USER t1 WITH(NOLOCK) 
                INNER JOIN
	                IN_RESUME t2 WITH(NOLOCK)
	                ON t2.in_sno = t1.in_sno
                WHERE 
					t1.in_l2 LIKE N'%資料變更%'
	                AND t1.in_ass_ver_identity IN (SELECT source_id FROM MEMBER WITH(NOLOCK) WHERE related_id = '{#identity_id}')
                ORDER BY
	                LEN(t1.in_sno)
	                , t2.in_member_unit
                    , t2.in_member_type
	                , t1.in_sno
            ";

            sql = sql.Replace("{#identity_id}", cfg.strIdentityId);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        //證件補發
        private Item GetListReissue(TConfig cfg, TTab tab)
        {
            string sql = @"                SELECT 
	                t1.*
	                , t2.id             AS 'resume_id'
	                , t2.in_name		AS 'resume_name'
	                , t2.in_member_type AS 'resume_member_type'
	                , t2.in_member_role AS 'resume_member_role'
	                , t2.in_member_unit AS 'resume_member_unit'
                    , t3.in_pay_amount_real
                    , t3.in_pay_photo
                    , t3.in_return_mark
                    , t3.in_collection_agency
                FROM 
	                IN_MEETING_USER t1 WITH(NOLOCK) 
                INNER JOIN
	                IN_RESUME t2 WITH(NOLOCK)
	                ON t2.in_sno = t1.in_sno
                LEFT OUTER JOIN
                    IN_MEETING_PAY t3 WITH(NOLOCK)
                    ON t3.in_meeting = t1.source_id
                    AND t3.item_number = t1.in_paynumber
                WHERE 
					t1.in_l2 LIKE N'%補發%'
	                AND t1.in_ass_ver_identity IN (SELECT source_id FROM MEMBER WITH(NOLOCK) WHERE related_id = '{#identity_id}')
                ORDER BY
	                t1.in_l2
	                , t2.in_sno
            ";

            sql = sql.Replace("{#identity_id}", cfg.strIdentityId);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        //證書上傳
        private Item GetListCert(TConfig cfg, TTab tab)
        {
            string sql = @"                SELECT 
	                t1.*
	                , t2.id             AS 'resume_id'
	                , t2.in_name		AS 'resume_name'
	                , t2.in_member_type AS 'resume_member_type'
	                , t2.in_member_role AS 'resume_member_role'
	                , t2.in_member_unit AS 'resume_member_unit'
                FROM 
	                IN_MEETING_USER t1 WITH(NOLOCK) 
                INNER JOIN
	                IN_RESUME t2 WITH(NOLOCK)
	                ON t2.in_sno = t1.in_sno
                WHERE 
					t1.in_l2 LIKE N'%證書上傳%'
	                AND t1.in_ass_ver_identity IN (SELECT source_id FROM MEMBER WITH(NOLOCK) WHERE related_id = '{#identity_id}')
                ORDER BY
	                t1.in_l2
	                , t2.in_sno
            ";

            sql = sql.Replace("{#identity_id}", cfg.strIdentityId);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        //未審核
        private Item GetListNoVerify(TConfig cfg, TTab tab)
        {
            string sql = @"
                SELECT 
                	t1.*
                	, t2.in_name AS 'resume_name'
                FROM 
                	{#MUserName} t1 WITH(NOLOCK)
                INNER JOIN
                    IN_RESUME t2 WITH(NOLOCK)
                    ON t1.in_sno = t2.in_sno
                WHERE
                	t1.source_id = '{#meeting_id}'
                	AND ISNULL(t1.in_ass_ver_result, '') = ''
	                AND t1.in_ass_ver_identity IN (SELECT source_id FROM MEMBER WHERE related_id = '{#identity_id}')
                ORDER BY
                	t1.in_l1
                	, t1.in_l2
                	, t1.in_l3
                	, t1.in_degree DESC
                	, t1.in_birth
            ";

            sql = sql.Replace("{#MUserName}", cfg.MUserName)
                .Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#identity_id}", cfg.strIdentityId)
                .Replace("{#in_note_state}", "official");

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        //已通過
        private Item GetListAgree(TConfig cfg, TTab tab)
        {
            string sql = @"
                SELECT 
                	t1.*
                	, t2.in_name AS 'resume_name'
                FROM 
                	{#MUserName} t1 WITH(NOLOCK)
                INNER JOIN
                    IN_RESUME t2 WITH(NOLOCK)
                    ON t1.in_sno = t2.in_sno
                WHERE
                	t1.source_id = '{#meeting_id}'
                	AND ISNULL(t1.in_ass_ver_result, '') <> ''
					AND t1.in_ass_ver_result = '1'
	                AND t1.in_ass_ver_identity IN (SELECT source_id FROM MEMBER WHERE related_id = '{#identity_id}')
                ORDER BY
                	t1.in_l1
                	, t1.in_l2
                	, t1.in_l3
                	, t1.in_degree DESC
                	, t1.in_birth
            ";

            sql = sql.Replace("{#MUserName}", cfg.MUserName)
                .Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#identity_id}", cfg.strIdentityId)
                .Replace("{#in_note_state}", "waiting");

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        //未通過
        private Item GetListReject(TConfig cfg, TTab tab)
        {
            string sql = @"
                SELECT 
                	t1.*
                	, t2.in_name AS 'resume_name'
                FROM 
                	{#MUserName} t1 WITH(NOLOCK)
                INNER JOIN
                    IN_RESUME t2 WITH(NOLOCK)
                    ON t1.in_sno = t2.in_sno
                WHERE
                	t1.source_id = '{#meeting_id}'
                	AND ISNULL(t1.in_ass_ver_result, '') <> ''
					AND t1.in_ass_ver_result = '0'
	                AND t1.in_ass_ver_identity IN (SELECT source_id FROM MEMBER WHERE related_id = '{#identity_id}')
                ORDER BY
                	t1.in_l1
                	, t1.in_l2
                	, t1.in_l3
                	, t1.in_degree DESC
                	, t1.in_birth
            ";

            sql = sql.Replace("{#MUserName}", cfg.MUserName)
                .Replace("{#identity_id}", cfg.strIdentityId)
                .Replace("{#meeting_id}", cfg.meeting_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        private Dictionary<string, TTable> MapMUsers(Item items)
        {
            Dictionary<string, TTable> map = new Dictionary<string, TTable>();

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_ass_ver_result = item.getProperty("in_ass_ver_result", "");
                string key = "list1";
                string title = "未審核";
                if (in_ass_ver_result == "1")
                {
                    key = "list2";
                    title = "審核通過";
                }
                else if (in_ass_ver_result == "0")
                {
                    key = "list3";
                    title = "審核未通過";
                }

                TTable table = null;
                if (map.ContainsKey(key))
                {
                    table = map[key];
                }
                else
                {
                    table = new TTable
                    {
                        code = (map.Count + 1).ToString(),
                        key = key,
                        title = title,
                        list = new List<Item>(),
                    };
                    map.Add(key, table);
                }

                table.list.Add(item);
            }

            var orderbyMap = map.OrderBy(key => key.Key);
            return orderbyMap.ToDictionary((keyItem) => keyItem.Key, (valueItem) => valueItem.Value);
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public Innosoft.InnovatorHelper InnH { get; set; }

            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }

            public string meeting_id { get; set; }
            public string mode { get; set; }
            public string scene { get; set; }

            public string MtName { get; set; }
            public string MUserName { get; set; }

            public Item itmMeeting { get; set; }
            public string mt_title { get; set; }

            public bool isMeetingAdmin { get; set; }
            public bool isCommittee { get; set; }
            public bool isGymOwner { get; set; }
            public bool isGymAssistant { get; set; }
        }

        private enum TabEnum
        {
            /// <summary>
            /// 會員申請
            /// </summary>
            Register = 1,
            /// <summary>
            /// 資料變更
            /// </summary>
            Change = 2,
            /// <summary>
            /// 證照補發
            /// </summary>
            Reissue = 3,
            /// <summary>
            /// 證書上傳
            /// </summary>
            Cert = 4
        }

        private class TTab
        {
            public string id { get; set; }
            public string active { get; set; }
            public string title { get; set; }

            public string li_id { get; set; }
            public string a_id { get; set; }
            public string content_id { get; set; }
            public string folder_id { get; set; }
            public string toggle_id { get; set; }
            public string table_id { get; set; }

            public bool is_reject { get; set; }

            public Item items { get; set; }
            public bool is_error { get; set; }
            public int count { get; set; }

            public Func<TConfig, TTab, Item> GetListFunc { get; set; }
            public Action<TConfig, TTab, StringBuilder> AppendListAction { get; set; }

            public TabEnum type { get; set; }
        }

        private class TTable
        {
            public string code { get; set; }
            public string key { get; set; }
            public string title { get; set; }
            public string id { get; set; }
            public List<Item> list { get; set; }
        }

        private class TField
        {
            public string title { get; set; }
            public string property { get; set; }
            public string hcss { get; set; }
            public string css { get; set; }
            public string format { get; set; }
            public string defval { get; set; }
            public Func<TConfig, TTab, TField, Item, string> getVal { get; set; }
        }

        private string GetApplyDate(TConfig cfg, TTab tab, TField field, Item item)
        {
            string in_regdate = item.getProperty("in_regdate", "");
            return GetDateFormat(in_regdate, "yyyy年MM月dd日", 8);
        }

        private string GetApplyUser(TConfig cfg, TTab tab, TField field, Item item)
        {
            string in_name = item.getProperty("resume_name", "");
            string in_sno_display = item.getProperty("in_sno_display", "");
            string link = "<a onclick='go_member(this)'>" + in_sno_display + "</a>";
            return in_name + "(" + link + ")";
        }

        private string GetApplyInL2(TConfig cfg, TTab tab, TField field, Item item)
        {
            string result = "會員申請";
            string resume_member_type = item.getProperty("resume_member_type", "");
            string resume_member_unit = item.getProperty("resume_member_unit", "");
            switch (resume_member_type)
            {
                case "vip_gym":
                    if (resume_member_unit == "館")
                    {
                        result = "會員申請(道館)";
                    }
                    else if (resume_member_unit == "社")
                    {
                        result = "會員申請(學校社團)";
                    }
                    break;

                case "reg":
                    result = "會員申請(晉段會員)";
                    break;

                case "vip_mbr":
                    result = "會員申請(個人會員)";
                    break;

                case "vip_group":
                    result = "會員申請(一般團體)";
                    break;

                default:
                    break;
            }

            return result;
        }

        private string GetVerifyDisplay(TConfig cfg, TTab tab, TField field, Item item)
        {
            string in_ass_ver_result = item.getProperty("in_ass_ver_result", "");
            if (in_ass_ver_result == "")
            {
                //return "<span style='color: #2C4198; cursor: pointer;'><i class='fa fa-list-alt'></i> 審核中 </span>";
                return "<span style='color: #2C4198; '> 審核中 </span>";
            }
            else if (in_ass_ver_result == "0")
            {
                //return "<span style='color: red; cursor: pointer;'><i class='fa fa-list-alt'></i> 不通過 </span>";
                return "<span style='color: red; '> 不通過 </span>";
            }
            else
            {
                //return "<span style='color: green; cursor: pointer;'><i class='fa fa-list-alt'></i> 審核通過 </span>";
                return "<span style='color: green; '> 審核通過 </span>";
            }
        }

        private string GetVerifyButton(TConfig cfg, TTab tab, TField field, Item item)
        {
            return "<button class='btn btn-sm btn-primary' onclick='ApplyVerify_Click(this)'>審核</button>";
        }

        private string GetVerifyButton2(TConfig cfg, TTab tab, TField field, Item item)
        {
            return "<button class='btn btn-sm btn-primary' onclick='go_member(this)'>審核</button>";
        }

        private string GetChangeStatus(TConfig cfg, TTab tab, TField field, Item item)
        {
            string in_is_changed = item.getProperty("in_is_changed", "");
            return in_is_changed == "1"
                ? "已修改"
                : "";
        }

        private string GetPayStatusIcon(TConfig cfg, TTab tab, TField field, Item item)
        {
            StringBuilder builder = new StringBuilder();

            string[] properties = new string[]
            {
                "in_paynumber", //繳費單號
                "in_pay_amount_real", //實際收款金額
                "in_pay_photo", //上傳繳費照片(繳費收據)
                "in_return_mark", //審核退回說明
                "in_collection_agency", //代收機構
            };

            foreach (string property in properties)
            {
                builder.Append("<" + property + ">" + item.getProperty(property, "") + "</" + property + ">");
            }
            //未確認
            builder.Append("<option>1</option>");

            Item itmResult = cfg.inn.applyMethod("In_PayType_Icon", builder.ToString());

            string in_pay_type = itmResult.getProperty("in_pay_type", "");

            return item.getProperty("in_l2", "") + " " + in_pay_type;
        }

        private string GetDateFormat(string value, string format, int hours = 0)
        {
            try
            {
                return DateTime.Parse(value.Replace("/", "-")).AddHours(hours).ToString(format);
            }
            catch
            {
                return "";
            }
        }

    }
}