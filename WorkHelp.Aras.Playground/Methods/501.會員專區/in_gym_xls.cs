﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class in_gym_xls : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 
                    團體會員清冊
                輸出: 
                    PDF
                重點: 
                    - 需 System.Draw.dll (建議一併引用 System.Drawing.Common.dll)
                    - 需 Spire.Xls 系統 dll
                    - Spire.Xls 需 license
                    - 請於 method-config.xml 引用 DLL
                日期: 
                    2021-03-17: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]in_gym_xls";

            Item itmR = this;

            TSheet cfg = new TSheet
            {
                WsRow = 4,
                WsCol = 0,
                Heads = new string[] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" },
                Font = "細明體",
                SheetCount = 1,
                Sheets = new List<string>(),
                scene = itmR.getProperty("scene", ""),
                in_member_status = itmR.getProperty("in_member_status", ""),
                export_type = itmR.getProperty("scene", ""),
            };

            TXls xinfo = GetExcelInfo(cfg, CCO, strMethodName, inn, itmR);

            Spire.Xls.Workbook workbook = new Spire.Xls.Workbook();
            workbook.LoadFromFile(xinfo.xls_source);
            Spire.Xls.Worksheet sheetTemplate = workbook.Worksheets[0];

            Item itmCommittees = inn.applySQL("SELECT * FROM IN_RESUME WITH(NOLOCK) WHERE in_member_type = 'area_cmt' AND in_member_role = 'sys_9999'　ORDER BY in_sno ");
            int count = itmCommittees.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item itmCommittee = itmCommittees.getItemByIndex(i);
                AppendSheets(cfg, CCO, strMethodName, inn, workbook, sheetTemplate, itmCommittee);
            }

            //移除樣板 sheet
            sheetTemplate.Remove();

            string export_type = "PDF";

            switch (export_type)
            {
                case "PDF":
                    workbook.SaveToFile(xinfo.xls_file.Replace("xlsx", "pdf"), Spire.Xls.FileFormat.PDF);
                    itmR.setProperty("xls_name", xinfo.xls_url.Replace("xlsx", "pdf"));
                    break;

                default:
                    workbook.SaveToFile(xinfo.xls_file, Spire.Xls.ExcelVersion.Version2010);
                    itmR.setProperty("xls_name", xinfo.xls_url);
                    break;
            }
            return itmR;
        }

        private void AppendSheets(TSheet cfg
            , Aras.Server.Core.CallContext CCO
            , string strMethodName
            , Innovator inn
            , Spire.Xls.Workbook workbook
            , Spire.Xls.Worksheet sheetTemplate
            , Item itmCommittee)
        {
            string committee_resume_id = itmCommittee.getProperty("id", "");
            string committee_name = itmCommittee.getProperty("in_name", "");
            string committee_short_name = itmCommittee.getProperty("in_short_org", "");

            string sql = @"
                SELECT 
                    t1.* 
	                , t2.in_pay_amount_exp
	                , t2.in_code_2
	                , t2.pay_bool
                FROM 
	                IN_RESUME t1 WITH(NOLOCK) 
                LEFT OUTER JOIN
	                IN_MEETING_PAY t2 WITH(NOLOCK)
	                ON t2.item_number = t1.in_member_pay_no
                WHERE 
	                t1.in_member_type = 'vip_gym' 
	                AND t1.in_member_role = 'sys_9999'　
	                AND t1.in_manager_org = '{#resume_id}' 
	                AND in_email_exam IN (N'良好', N'停止寄')
	                --AND t1.in_member_status = '{#in_member_status}' 
                ORDER BY 
	                t1.in_sno
            ";

            sql = sql.Replace("{#resume_id}", committee_resume_id)
                .Replace("{#in_member_status}", cfg.in_member_status); ;

            Item itmGyms = inn.applySQL(sql);

            int count = itmGyms.getItemCount();

            List<TField> fields = new List<TField>();
            fields.Add(new TField { title = "縣市委員會", property = "in_committee_name" });
            fields.Add(new TField { title = "設立名稱 ", property = "in_name" });
            fields.Add(new TField { title = "負責人姓名 ", property = "in_principal" });
            fields.Add(new TField { title = "教練姓名 ", property = "in_head_coach" });
            fields.Add(new TField { title = "道館地址 ", property = "in_resident_add" });
            fields.Add(new TField { title = "道館電話 ", property = "in_tel_2", format = "tel" });
            fields.Add(new TField { title = "系統帳號 ", property = "sno_display" });
            // fields.Add(new TField { title = "會費狀態 ", property = "pay_display" });
            //fields.Add(new TField { title = "發信狀況 ", property = "in_email_exam" });

            Spire.Xls.Worksheet sheet = workbook.CreateEmptySheet();
            sheet.CopyFrom(sheetTemplate);
            sheet.Name = committee_short_name;

            sheet.Range["A2"].Text = committee_name;
            sheet.Range["A2"].Style.Font.FontName = cfg.Font;

            int wsRow = cfg.WsRow;
            int wsCol = cfg.WsCol;

            for (int i = 0; i < count; i++)
            {
                Item itmGym = itmGyms.getItemByIndex(i);
                string in_member_status = itmGym.getProperty("in_member_status", "");
                string sno_display = "";
                string pay_display = "";

                bool is_export = true;
                switch (in_member_status)
                {
                    case "暫時會員":
                    case "合格會員":
                        sno_display = itmGym.getProperty("in_sno", "");
                        pay_display = itmGym.getProperty("pay_bool", "");
                        break;

                    case "出會":
                        sno_display = itmGym.getProperty("in_sno", "");
                        pay_display = itmGym.getProperty("pay_bool", "");
                        break;

                    case "暫時停權":
                    case "除名":
                    case "審核未通過":
                        sno_display = itmGym.getProperty("in_sno", "");
                        pay_display = itmGym.getProperty("pay_bool", "");
                        break;

                    default:
                        is_export = false;
                        break;
                }

                if (!is_export)
                {
                    continue;
                }

                itmGym.setProperty("in_committee_name", committee_short_name);
                itmGym.setProperty("sno_display", sno_display);
                itmGym.setProperty("pay_display", pay_display);

                SetItemCell(sheet, cfg, wsRow, wsCol, itmGym, fields);

                wsRow++;
            }
        }
        #region 查詢資料

        /// <summary>
        /// 取得匯出路徑
        /// </summary>
        private Item GetXlsPaths(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string in_name)
        {
            Item itmResult = inn.newItem();

            //Excel 匯出路徑 (from 原創.設定參數)
            string sql = @"
                SELECT 
                    t2.in_name AS 'variable', t1.in_name, t1.in_value 
                FROM 
                    In_Variable_Detail t1 WITH(NOLOCK)
                INNER JOIN 
                    In_Variable t2 WITH(NOLOCK) ON t2.ID = t1.SOURCE_ID 
                WHERE 
                    t2.in_name = N'meeting_excel'  
                    AND t1.in_name IN (N'export_path', N'{#in_name}')
                ";

            sql = sql.Replace("{#in_name}", in_name);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item items = inn.applySQL(sql);

            if (items.isError() || items.getItemCount() < 1)
            {
                throw new Exception("未設定匯出路徑 _# " + in_name);
            }

            for (int i = 0; i < items.getItemCount(); i++)
            {
                Item item = items.getItemByIndex(i);
                string xls_name = item.getProperty("in_name", "");
                string xls_value = item.getProperty("in_value", "");

                if (xls_name == "export_path")
                {
                    itmResult.setProperty("export_path", xls_value);
                }
                else
                {
                    itmResult.setProperty("template_path", xls_value);
                }
            }

            return itmResult;
        }

        private TXls GetExcelInfo(TSheet cfg, Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            Item itmPath = GetXlsPaths(CCO, strMethodName, inn, "gym_list_path");

            string export_path = itmPath.getProperty("export_path", "").TrimEnd('\\');
            string ext_name = ".xlsx";

            string xls_name = cfg.in_member_status == ""
                ? "中華民國跆拳道協會" + " - " + "團體會員帳號清冊_" + DateTime.Now.ToString("MMdd_HHmmss")
                : "中華民國跆拳道協會" + " - " + "團體會員帳號清冊(" + cfg.in_member_status + ")_" + DateTime.Now.ToString("MMdd_HHmmss");

            string xls_file = export_path + "\\" + xls_name + ext_name;
            string xls_url = xls_name + ext_name;

            return new TXls
            {
                xls_source = itmPath.getProperty("template_path", ""),
                xls_file = xls_file,
                xls_url = xls_url,
            };
        }

        #endregion 查詢資料

        #region Excel

        //設定物件與資料列
        private void SetItemCell(Spire.Xls.Worksheet sheet, TSheet cfg, int wsRow, int wsCol, Item item, List<TField> fields)
        {
            for (int i = 0; i < fields.Count; i++)
            {
                var field = fields[i];
                var value = "";
                var position = cfg.Heads[wsCol + i] + wsRow;

                if (field.getValue != null)
                {
                    value = field.getValue(cfg, item, field);
                }
                else if (field.property != "")
                {
                    value = item.getProperty(field.property, "");
                }

                Spire.Xls.CellRange range = sheet.Range[position];
                SetBodyCell(range, cfg, value, field.format);
            }
        }

        //設定資料列
        private void SetBodyCell(Spire.Xls.CellRange range, TSheet cfg, string value = "", string format = "")
        {
            switch (format)
            {
                case "center":
                    range.Text = value;
                    range.Style.Font.FontName = cfg.Font;
                    break;

                case "yyyy/MM/dd":
                case "yyyy/MM/dd HH:mm":
                    range.Text = GetDateTimeValue(value, format);
                    range.Style.Font.FontName = cfg.Font;
                    break;

                case "$ #,##0":
                    range.Text = value;
                    range.Style.Font.FontName = cfg.Font;
                    break;

                case "text":
                    range.Text = value;
                    range.Style.Font.FontName = cfg.Font;
                    break;

                default:
                    range.Text = value;
                    range.Style.Font.FontName = cfg.Font;
                    break;
            }

            range.Style.Borders[Spire.Xls.BordersLineType.EdgeTop].LineStyle = Spire.Xls.LineStyleType.Thin;
            range.Style.Borders[Spire.Xls.BordersLineType.EdgeRight].LineStyle = Spire.Xls.LineStyleType.Thin;
            range.Style.Borders[Spire.Xls.BordersLineType.EdgeBottom].LineStyle = Spire.Xls.LineStyleType.Thin;
            range.Style.Borders[Spire.Xls.BordersLineType.EdgeLeft].LineStyle = Spire.Xls.LineStyleType.Thin;
        }

        #endregion

        private class TSheet
        {
            public int WsRow { get; set; }

            public int WsCol { get; set; }

            public string[] Heads { get; set; }

            public string Font { get; set; }

            public int SheetCount { get; set; }

            public List<string> Sheets { get; set; }

            public string scene { get; set; }

            public string in_member_status { get; set; }

            public string export_type { get; set; }
        }

        private class TXls
        {
            public string xls_source { get; set; }

            public string xls_file { get; set; }

            public string xls_url { get; set; }
        }

        private class TField
        {
            public string title { get; set; }
            public string property { get; set; }
            public string format { get; set; }
            public string defv { get; set; }

            public Func<TSheet, Item, TField, string> getValue { get; set; }
        }

        //轉換日期
        private string GetDateTimeValue(string value, string format)
        {
            if (value == "") return "";
            DateTime dt = Convert.ToDateTime(value);
            return dt.ToString(format);
        }

        private int GetIntVal(string value, int def = 0)
        {
            int result = def;
            Int32.TryParse(value, out result);
            return result;
        }
    }
}
  