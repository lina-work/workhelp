﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class in_user_apply : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
            目的: 各類申請
            日期: 
                - 2021/03/10: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]in_user_apply";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "input dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                muid = itmR.getProperty("muid", ""),
                in_l1 = itmR.getProperty("in_l1", ""),
                in_l2 = itmR.getProperty("in_l2", ""),
                scene = itmR.getProperty("scene", ""),
            };

            cfg.itmLogin = inn.applySQL("SELECT * FROM IN_RESUME WITH(NOLOCK) WHERE in_user_id = '" + cfg.strUserId + "'");
            cfg.login_member_type = cfg.itmLogin.getProperty("in_member_type", "");
            cfg.login_sno = cfg.itmLogin.getProperty("in_sno", "");

            if (cfg.muid != "")
            {
                cfg.itmMUser = inn.applySQL("SELECT * FROM IN_MEETING_USER WITH(NOLOCK) WHERE id = '" + cfg.muid + "'");
                cfg.meeting_id = cfg.itmMUser.getProperty("source_id", "");
                cfg.in_l1 = cfg.itmMUser.getProperty("in_l1", "");
                cfg.in_l2 = cfg.itmMUser.getProperty("in_l2", "");

                string in_ass_ver_result = cfg.itmMUser.getProperty("in_ass_ver_result", "");
                string in_ass_ver_memo = cfg.itmMUser.getProperty("in_ass_ver_memo", "");

                string verify_display = "審核說明";
                if (in_ass_ver_result == "1") verify_display = "審核說明 (審核通過)";
                else if (in_ass_ver_result == "0") verify_display = "審核說明 (審核不通過)";

                itmR.setProperty("inn_verify_display", verify_display);
                itmR.setProperty("inn_verify_memo", in_ass_ver_memo);
            }

            cfg.itmMeeting = inn.applySQL("SELECT * FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            cfg.itmOption = GetSurveyOption(cfg);
            cfg.option_list_id = cfg.itmOption.getProperty("in_list", "");

            if (cfg.scene == "")
            {
                //進入申請頁
                Query(cfg, itmR);
                itmR.setProperty("muid", " ");
            }
            else if (cfg.scene == "apply")
            {
                //送出申請
                Apply(cfg, itmR);
            }
            else if (cfg.scene == "modal")
            {
                //明細 or 審核 Modal
                Modal(cfg, itmR);
            }
            else if (cfg.scene == "edit_page")
            {
                //進入修改頁
                EditPage(cfg, itmR);
            }
            else if (cfg.scene == "edit")
            {
                //修改申請
                Edit(cfg, itmR);
            }

            return itmR;
        }

        //申請說明
        private void Modal(TConfig cfg, Item itmReturn)
        {
            string in_creator_sno = cfg.itmMUser.getProperty("in_creator_sno", "");
            string in_ass_ver_identity = cfg.itmMUser.getProperty("in_ass_ver_identity", "");

            itmReturn.setProperty("meeting_id", cfg.meeting_id);
            itmReturn.setProperty("in_l1", cfg.itmMUser.getProperty("in_l1", ""));
            itmReturn.setProperty("in_l2", cfg.itmMUser.getProperty("in_l2", ""));
            itmReturn.setProperty("in_note", cfg.itmMUser.getProperty("in_note", ""));
            itmReturn.setProperty("in_creator", cfg.itmMUser.getProperty("in_creator", ""));
            itmReturn.setProperty("in_creator_sno", in_creator_sno);
            itmReturn.setProperty("inn_regdate", GetDateFormat(cfg.itmMUser, "in_regdate", "yyyy年MM月dd日", 8));


            if (in_creator_sno != cfg.login_sno)
            {
                Item itmIdentity = cfg.inn.newItem("Identity", "get");
                itmIdentity.setProperty("in_user", cfg.strUserId);
                itmIdentity = itmIdentity.apply();
                string identity_id = itmIdentity.getID();

                //該筆資料的審核角色成員中是否加入了登入者
                string sql = "SELECT * FROM MEMBER WITH(NOLOCK) WHERE source_id = '{#source_id}' AND related_id = '{#related_id}'";
                sql = sql.Replace("{#source_id}", in_ass_ver_identity)
                    .Replace("{#related_id}", identity_id);
                Item itmMember = cfg.inn.applySQL(sql);
                if (itmMember.isError() || itmMember.getResult() == "")
                {
                    return;
                }
                else
                {
                    itmReturn.setProperty("role", "manager");
                }
            }

            string sql2 = "SELECT * FROM IN_MEETING_USERLOG WITH(NOLOCK) WHERE in_user = '" + cfg.muid + "' ORDER BY sort_order";
            // cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql2);
            Item itmMULogs = cfg.inn.applySQL(sql2);

            int count = itmMULogs.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item itmMULog = itmMULogs.getItemByIndex(i);
                string in_title = itmMULog.getProperty("in_title", "");
                string in_property = itmMULog.getProperty("in_property", "");
                string old_value = itmMULog.getProperty("old_value", "");
                string new_value = itmMULog.getProperty("new_value", "");

                bool is_add = true;

                switch (in_property)
                {
                    case "in_file1":
                        itmReturn.setProperty("in_file1", new_value);
                        is_add = false;
                        break;

                    case "in_file2":
                        itmReturn.setProperty("in_file2", new_value);
                        is_add = false;
                        break;

                    case "in_file3":
                        itmReturn.setProperty("in_file3", new_value);
                        is_add = false;
                        break;

                    case "in_birth":
                        old_value = GetDateTimeByFormat(old_value, "yyyy-MM-dd", hours: 8);
                        new_value = GetDateTimeByFormat(new_value, "yyyy-MM-dd", hours: 8);
                        break;
                }

                if (is_add)
                {
                    Item item = cfg.inn.newItem();
                    item.setType("inn_item");
                    item.setProperty("title", in_title);
                    item.setProperty("property", in_property);
                    item.setProperty("old_value", old_value);
                    item.setProperty("new_value", new_value);
                    itmReturn.addRelationship(item);
                }
            }
        }

        //送出申請
        private void Apply(TConfig cfg, Item itmReturn)
        {
            List<TField> fields = MapFields(cfg, cfg.option_list_id);

            Item itmSOption = GetSurveyOption(cfg);
            string in_expense_value = itmSOption.getProperty("in_expense_value", "0");
            if (in_expense_value == "") in_expense_value = "0";
            string in_ass_ver_identity = GetVerifyIdentity(cfg, itmSOption);

            string in_group = cfg.itmLogin.getProperty("in_group", "");
            string in_current_org = cfg.itmLogin.getProperty("in_current_org", "");

            string in_creator = cfg.itmLogin.getProperty("in_name", "");
            string in_creator_sno = cfg.itmLogin.getProperty("in_sno", "");

            string in_l1 = cfg.in_l1;
            string in_l2 = cfg.in_l2;
            string in_l3 = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");

            string in_mail = in_creator_sno + "-" + in_l1 + "-" + in_l2 + "-" + in_l3;
            string in_section_name = in_l1 + "-" + in_l2;
            string in_gameunit = in_l2;

            string in_note = itmReturn.getProperty("in_note");

            //建立與會者
            Item itmMUser = cfg.inn.newItem("In_Meeting_User");
            itmMUser.setProperty("source_id", cfg.meeting_id);
            itmMUser.setProperty("in_mail", in_mail);//唯一
            itmMUser.setProperty("in_sno", in_creator_sno);
            itmMUser.setProperty("in_sno_display", GetSnoDisplay(in_creator_sno));
            itmMUser.setProperty("in_creator", in_creator);
            itmMUser.setProperty("in_creator_sno", in_creator_sno);

            itmMUser.setProperty("in_group", in_group);
            itmMUser.setProperty("in_current_org", in_current_org);
            itmMUser.setProperty("in_section_name", in_section_name);
            itmMUser.setProperty("in_gameunit", in_gameunit);
            itmMUser.setProperty("in_l1", in_l1);
            itmMUser.setProperty("in_l2", in_l2);
            itmMUser.setProperty("in_l3", in_l3);
            itmMUser.setProperty("in_note", in_note);

            //預設費用為0
            itmMUser.setProperty("in_expense", in_expense_value);
            //預設身分為選手
            itmMUser.setProperty("in_role", "player");
            //預設錄取為正取
            itmMUser.setProperty("in_note_state", "official");
            //申請時間
            itmMUser.setProperty("in_regdate", System.DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss"));
            //協會審核角色
            itmMUser.setProperty("in_ass_ver_identity", in_ass_ver_identity);

            itmMUser = itmMUser.apply("add");

            if (itmMUser.isError())
            {
                throw new Exception("申請失敗");
            }

            foreach (var field in fields)
            {
                string old_value = "";
                string new_value = "";
                switch (field.property)
                {
                    case "in_birth":
                        old_value = BirthDay(cfg, cfg.itmLogin, field);
                        new_value = BirthDayFromUI(cfg, itmReturn, field);
                        break;

                    case "in_email":
                        old_value = Email(cfg, cfg.itmLogin, field);
                        new_value = itmReturn.getProperty(field.property, "");
                        break;

                    default:
                        old_value = cfg.itmLogin.getProperty(field.property, "");
                        new_value = itmReturn.getProperty(field.property, "");
                        break;
                }

                if (new_value == "")
                {
                    continue;
                }

                //與會者日誌
                Item itmMULog = cfg.inn.newItem("In_Meeting_UserLog");
                itmMULog.setProperty("source_id", cfg.meeting_id);
                itmMULog.setProperty("in_user", itmMUser.getID());
                itmMULog.setProperty("in_title", field.title);
                itmMULog.setProperty("in_property", field.property);
                itmMULog.setProperty("sort_order", field.sort_order);
                itmMULog.setProperty("old_value", old_value);
                itmMULog.setProperty("new_value", new_value);
                itmMULog = itmMULog.apply("add");

                if (itmMULog.isError())
                {
                    throw new Exception("申請失敗");
                }
            }
        }

        //送出申請
        private void Edit(TConfig cfg, Item itmReturn)
        {
            //刪除舊資料
            string sql = "DELETE FROM IN_MEETING_USERLOG WHERE in_user = '" + cfg.muid + "'";
            Item itmSQL = cfg.inn.applySQL(sql);

            List<TField> fields = MapFields(cfg, cfg.option_list_id);

            foreach (var field in fields)
            {
                string old_value = "";
                string new_value = "";
                switch (field.property)
                {
                    case "in_birth":
                        old_value = BirthDay(cfg, cfg.itmLogin, field);
                        new_value = BirthDayFromUI(cfg, itmReturn, field);
                        break;

                    case "in_email":
                        old_value = Email(cfg, cfg.itmLogin, field);
                        new_value = itmReturn.getProperty(field.property, "");
                        break;

                    default:
                        old_value = cfg.itmLogin.getProperty(field.property, "");
                        new_value = itmReturn.getProperty(field.property, "");
                        break;
                }

                if (new_value == "")
                {
                    continue;
                }

                //與會者日誌
                Item itmMULog = cfg.inn.newItem("In_Meeting_UserLog");
                itmMULog.setProperty("source_id", cfg.meeting_id);
                itmMULog.setProperty("in_user", cfg.muid);
                itmMULog.setProperty("in_title", field.title);
                itmMULog.setProperty("in_property", field.property);
                itmMULog.setProperty("sort_order", field.sort_order);
                itmMULog.setProperty("old_value", old_value);
                itmMULog.setProperty("new_value", new_value);
                itmMULog = itmMULog.apply("add");

                if (itmMULog.isError())
                {
                    throw new Exception("修改失敗");
                }
            }

            //變更審核狀態
            sql = "UPDATE IN_MEETING_USER SET in_ass_ver_result = NULL WHERE id = '" + cfg.muid + "'";
            itmSQL = cfg.inn.applySQL(sql);
        }

        private Dictionary<string, Item> MapMUserLog(TConfig cfg)
        {
            string sql2 = "SELECT * FROM IN_MEETING_USERLOG WITH(NOLOCK) WHERE in_user = '" + cfg.muid + "' ORDER BY sort_order";

            // cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql2);

            Item itmMULogs = cfg.inn.applySQL(sql2);

            int count = itmMULogs.getItemCount();

            Dictionary<string, Item> map = new Dictionary<string, Item>();

            for (int i = 0; i < count; i++)
            {
                Item itmMULog = itmMULogs.getItemByIndex(i);
                string in_property = itmMULog.getProperty("in_property", "");
                if (map.ContainsKey(in_property))
                {
                    //異常
                }
                else
                {
                    map.Add(in_property, itmMULog);
                }
            }

            return map;
        }

        //進入修改頁
        private void EditPage(TConfig cfg, Item itmReturn)
        {
            string in_l2 = cfg.itmMUser.getProperty("in_l2", "");
            if (in_l2.Contains("資料變更"))
            {
                //有新舊值
                EditPage1(cfg, itmReturn);
            }
            else
            {
                EditPage2(cfg, itmReturn);
            }
        }

        //進入修改頁(資料變更)
        private void EditPage1(TConfig cfg, Item itmReturn)
        {
            itmReturn.setProperty("login_resume_id", cfg.itmLogin.getProperty("id", ""));
            itmReturn.setProperty("login_resume_sno", cfg.itmLogin.getProperty("in_sno", ""));

            itmReturn.setProperty("in_note", cfg.itmMUser.getProperty("in_note", ""));

            List<TField> fields = MapFields(cfg, cfg.option_list_id);
            Dictionary<string, Item> map = MapMUserLog(cfg);

            foreach (var field in fields)
            {
                TCtrl ctrl = new TCtrl
                {
                    is_add = true,
                    title = field.title,
                    property = field.property,
                    placeholder = field.title,
                    value = "",
                    new_value = "",
                    onchange = "",
                };

                string new_value = "";
                if (map.ContainsKey(ctrl.property))
                {
                    Item itmMULog = map[ctrl.property];
                    new_value = itmMULog.getProperty("new_value", "");
                }

                switch (field.property)
                {
                    case "in_file1":
                    case "in_file2":
                    case "in_file3":
                        ctrl.is_add = false;
                        break;

                    case "in_official_no":
                        ctrl.placeholder = "ex: 中跆爐字第1090000236號";
                        ctrl.new_value = new_value;
                        break;

                    case "in_effective_start":
                    case "in_effective_end":
                        ctrl.placeholder = "2021/01/01";
                        ctrl.onchange = "onchange='ih.addChangedClass(this)'";
                        ctrl.new_value = new_value;
                        break;

                    case "in_birth":
                        ctrl.value = BirthDay(cfg, cfg.itmLogin, field);
                        ctrl.new_value = BirthDay(new_value);
                        break;

                    case "in_email":
                        ctrl.value = Email(cfg, cfg.itmLogin, field);
                        ctrl.new_value = Email(new_value);
                        break;

                    default:
                        ctrl.value = cfg.itmLogin.getProperty(field.property, "");
                        ctrl.new_value = new_value;
                        break;
                }

                if (ctrl.is_add)
                {
                    Item item = cfg.inn.newItem();
                    item.setType("inn_item");
                    item.setProperty("title", ctrl.title);
                    item.setProperty("value", ctrl.value);
                    item.setProperty("new_value", ctrl.new_value);
                    item.setProperty("property", ctrl.property);
                    item.setProperty("placeholder", ctrl.placeholder);
                    item.setProperty("onchange", ctrl.onchange);
                    itmReturn.addRelationship(item);
                }
            }
        }

        //進入修改頁
        private void EditPage2(TConfig cfg, Item itmReturn)
        {
            itmReturn.setProperty("login_resume_id", cfg.itmLogin.getProperty("id", ""));
            itmReturn.setProperty("login_resume_sno", cfg.itmLogin.getProperty("in_sno", ""));

            itmReturn.setProperty("in_note", cfg.itmMUser.getProperty("in_note", ""));

            List<TField> fields = MapFields(cfg, cfg.option_list_id);
            Dictionary<string, Item> map = MapMUserLog(cfg);

            foreach (var field in fields)
            {
                TCtrl ctrl = new TCtrl
                {
                    is_add = true,
                    title = field.title,
                    property = field.property,
                    placeholder = field.title,
                    value = "",
                    onchange = "",
                };

                string value = "";
                if (map.ContainsKey(ctrl.property))
                {
                    Item itmMULog = map[ctrl.property];
                    value = itmMULog.getProperty("new_value", "");
                }

                switch (field.property)
                {
                    case "in_file1":
                    case "in_file2":
                    case "in_file3":
                        ctrl.is_add = false;
                        itmReturn.setProperty(field.property, value);
                        break;

                    case "in_official_no":
                        ctrl.value = value;
                        ctrl.placeholder = "ex: 中跆爐字第1090000236號";
                        break;

                    case "in_effective_start":
                    case "in_effective_end":
                        ctrl.value = ShortDay(value);
                        ctrl.placeholder = "2021/01/01";
                        ctrl.onchange = "onchange='ih.addChangedClass(this)'";
                        break;

                    case "in_birth":
                        ctrl.value = BirthDay(value);
                        break;

                    case "in_email":
                        ctrl.value = Email(value);
                        break;

                    default:
                        ctrl.value = value;
                        break;
                }

                if (ctrl.is_add)
                {
                    Item item = cfg.inn.newItem();
                    item.setType("inn_item");
                    item.setProperty("title", ctrl.title);
                    item.setProperty("value", ctrl.value);
                    item.setProperty("property", ctrl.property);
                    item.setProperty("placeholder", ctrl.placeholder);
                    item.setProperty("onchange", ctrl.onchange);
                    itmReturn.addRelationship(item);
                }
            }
        }

        //查詢
        private void Query(TConfig cfg, Item itmReturn)
        {
            itmReturn.setProperty("login_resume_id", cfg.itmLogin.getProperty("id", ""));
            itmReturn.setProperty("login_resume_sno", cfg.itmLogin.getProperty("in_sno", ""));

            List<TField> fields = MapFields(cfg, cfg.option_list_id);

            foreach (var field in fields)
            {
                TCtrl ctrl = new TCtrl
                {
                    is_add = true,
                    title = field.title,
                    property = field.property,
                    placeholder = field.title,
                    value = "",
                    onchange = "",
                };

                switch (field.property)
                {
                    case "in_file1":
                    case "in_file2":
                    case "in_file3":
                        ctrl.is_add = false;
                        break;

                    case "in_official_no":
                        ctrl.placeholder = "ex: 中跆爐字第1090000236號";
                        break;

                    case "in_effective_start":
                    case "in_effective_end":
                        ctrl.placeholder = "2021/01/01";
                        ctrl.onchange = "onchange='ih.addChangedClass(this)'";
                        break;

                    case "in_birth":
                        ctrl.value = BirthDay(cfg, cfg.itmLogin, field);
                        break;

                    case "in_email":
                        ctrl.value = Email(cfg, cfg.itmLogin, field);
                        break;

                    default:
                        ctrl.value = cfg.itmLogin.getProperty(field.property, "");
                        break;
                }

                if (ctrl.is_add)
                {
                    Item item = cfg.inn.newItem();
                    item.setType("inn_item");
                    item.setProperty("title", ctrl.title);
                    item.setProperty("value", ctrl.value);
                    item.setProperty("property", ctrl.property);
                    item.setProperty("placeholder", ctrl.placeholder);
                    item.setProperty("onchange", ctrl.onchange);
                    itmReturn.addRelationship(item);
                }
            }
        }

        //取得問項
        private Item GetSurveyOption(TConfig cfg)
        {
            string sql = @"
                SELECT 
	                TOP 1 * 
                FROM 
	                IN_SURVEY_OPTION WITH(NOLOCK)
                WHERE 
	                source_id IN 
	                (
		                SELECT t2.id FROM IN_MEETING_SURVEYS t1 WITH(NOLOCK) INNER JOIN IN_SURVEY t2 WITH(NOLOCK) ON t2.id = t1.related_id 
		                WHERE t1.source_id = '{#meeting_id}' AND t2.in_property = N'in_l2'
	                )
					AND in_filter = N'{#in_l1}'
					AND in_value = N'{#in_l2}'
	            ORDER BY
	                sort_order
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_l1}", cfg.in_l1)
                .Replace("{#in_l2}", cfg.in_l2);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item item = cfg.inn.applySQL(sql);

            if (item.isError() || item.getResult() == "" || item.getItemCount() != 1)
            {
                return cfg.inn.newItem();
            }
            else
            {
                return item;
            }
        }

        //取得屬性清單
        private List<TField> MapFields(TConfig cfg, string list_id)
        {
            string sql = "SELECT * FROM [VALUE] WITH(NOLOCK) WHERE source_id = '{#list_id}' ORDER BY sort_order";
            sql = sql.Replace("{#list_id}", list_id);
            Item items = cfg.inn.applySQL(sql);
            int count = items.getItemCount();

            List<TField> fields = new List<TField>();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                fields.Add(new TField
                {
                    property = item.getProperty("value", ""),
                    title = item.getProperty("label_zt", ""),
                    sort_order = item.getProperty("sort_order", ""),
                });
            }
            return fields;
        }


        //取得審核角色 id
        private string GetVerifyIdentity(TConfig cfg, Item itmSOption)
        {
            //ACT_ASC_Degree
            string in_verify_identity = itmSOption.getProperty("in_verify_identity", "");

            string sql = "SELECT TOP 1 * FROM [IDENTITY] WHERE [name] = N'" + in_verify_identity + "'";

            Item item = cfg.inn.applySQL(sql);

            if (item.isError() || item.getResult() == "" || item.getItemCount() != 1)
            {
                return "";
            }
            else
            {
                return item.getProperty("id", "");
            }
        }

        private class TField
        {
            public string title { get; set; }
            public string property { get; set; }
            public string sort_order { get; set; }
        }

        private class TCtrl
        {
            public bool is_add { get; set; }
            public string title { get; set; }
            public string property { get; set; }
            public string placeholder { get; set; }
            public string value { get; set; }
            public string onchange { get; set; }
            public string new_value { get; set; }
        }

        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }

            public string meeting_id { get; set; }
            public string muid { get; set; }
            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string scene { get; set; }

            public Item itmLogin { get; set; }
            public Item itmMeeting { get; set; }
            public Item itmMUser { get; set; }
            public Item itmOption { get; set; }
            public string option_list_id { get; set; }

            public string login_member_type { get; set; }
            public string login_sno { get; set; }

        }

        private string GetSnoDisplay(string in_sno)
        {
            //處理[身分證字號(外顯)]
            if (in_sno.Length == 10)
            {
                //10碼(身分證處理)
                string in_sno_1 = in_sno.Substring(0, 2);
                string in_sno_2 = in_sno.Substring(6, 4);
                return in_sno_1 + "****" + in_sno_2;//A1****6789
            }
            else if (in_sno.Length == 18)
            {
                //18碼(身分證處理-大陸)
                string in_sno_1 = in_sno.Substring(0, 6);
                string in_sno_2 = in_sno.Substring(14, 3);
                return in_sno_1 + "********" + in_sno_2 + "X";//110102********888X
            }
            else
            {
                //不屬於前兩者(身分證)可能為學校代碼
                return in_sno;
            }
        }

        /// <summary>
        /// 生日
        /// </summary>
        private string BirthDay(TConfig cfg, Item item, TField field)
        {
            return BirthDay(item.getProperty(field.property, ""));
        }

        /// <summary>
        /// 生日
        /// </summary>
        private string BirthDay(string value)
        {
            if (value.Contains("1900") || value.Contains("1899"))
            {
                return "";
            }
            else
            {
                return GetDateTimeByFormat(value, "yyyy-MM-dd", hours: 8);
            }
        }

        /// <summary>
        /// 日期
        /// </summary>
        private string ShortDay(TConfig cfg, Item item, TField field)
        {
            return ShortDay(item.getProperty(field.property, ""));
        }

        /// <summary>
        /// 日期
        /// </summary>
        private string ShortDay(string value)
        {
            return GetDateTimeByFormat(value, "yyyy-MM-dd", hours: 8);
        }

        /// <summary>
        /// 生日
        /// </summary>
        private string BirthDayFromUI(TConfig cfg, Item item, TField field)
        {
            string value = item.getProperty(field.property, "");
            return GetDateTimeByFormat(value, "yyyy-MM-ddTHH:mm:ss");
        }

        /// <summary>
        /// 清除 NA mail
        /// </summary>
        private string Email(TConfig cfg, Item item, TField field)
        {
            return Email(item.getProperty(field.property, ""));
        }

        /// <summary>
        /// 清除 NA mail
        /// </summary>
        private string Email(string value)
        {
            if (value.Contains("na@na.n"))
            {
                return "";
            }
            else
            {
                return value;
            }
        }

        private string GetDateFormat(Item item, string property_name, string format, int hours = 0)
        {
            string value = item.getProperty(property_name, "");
            if (value == "") return "";

            try
            {
                return DateTime.Parse(value).AddHours(hours).ToString(format);
            }
            catch
            {
                return "";
            }
        }

        private string GetDateTimeByFormat(string value, string format, int hours = 0)
        {
            if (value == "") return "";

            DateTime dt = DateTime.Now;
            if (DateTime.TryParse(value.Replace("/", "-"), out dt))
            {
                return dt.AddHours(hours).ToString(format);
            }
            else
            {
                return value;
            }
        }
    }
}