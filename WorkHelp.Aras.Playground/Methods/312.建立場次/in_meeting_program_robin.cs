﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;
using System.Net;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class in_meeting_program_robin : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 循環賽
                日期: 
                    2020-11-20: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]in_meeting_program_robin";

            Item itmR = this;

            string meeting_id = itmR.getProperty("meeting_id", "");
            string program_id = itmR.getProperty("program_id", "");
            string battle_type = itmR.getProperty("battle_type", "");

            if (meeting_id == "" || program_id == "" || battle_type == "")
            {
                throw new Exception("參數錯誤");
            }

            //檢查頁面權限
            Item itmPermit = inn.applyMethod("In_CheckIdentity", "<method>" + strMethodName + "</method><code>" + "ALL" + "</code>");
            bool isMeetingAdmin = itmPermit.getProperty("isMeetingAdmin", "") == "1";
            bool isMeetingLeader = itmPermit.getProperty("isMeetingLeader", "") == "1";
            bool can_view = isMeetingAdmin || isMeetingLeader;
            if (!can_view)
            {
                itmR.setProperty("error_message", "您無權限瀏覽此頁面");
                return itmR;
            }

            Item itmMeeting = GetMeeting(CCO, strMethodName, inn, meeting_id);
            if (itmMeeting.isError())
            {
                throw new Exception("賽事資料錯誤");
            }

            Item itmProgram = GetProgram(CCO, strMethodName, inn, program_id);
            if (itmMeeting.isError())
            {
                throw new Exception("賽程組別資料錯誤");
            }

            //更新組別賽制
            UpdateProgram(CCO, strMethodName, inn, itmProgram, battle_type);

            //清除場次資料
            RemoveEvents(CCO, strMethodName, inn, itmProgram);

            bool need_next = false;

            if (battle_type == "SingleRoundRobin")
            {
                //新增單循環場次資料
                AddCycleEvents(CCO, strMethodName, inn, itmProgram);
                need_next = true;
            }
            else if (battle_type == "DoubleRoundRobin")
            {
                //新增雙循環場次資料
                AddDoubleCycleEvents(CCO, strMethodName, inn, itmProgram);
                need_next = true;
            }

            if (need_next)
            {
                //新增賽事分組名單
                AddEventDetails(CCO, strMethodName, inn, itmMeeting, itmProgram);

                //建立子場次
                string in_l1 = itmProgram.getProperty("in_l1", "");
                string in_sub_event = itmProgram.getProperty("in_sub_event", "0");

                if (in_l1 == "團體組" && in_sub_event != "" && in_sub_event != "0")
                {
                    GenerateSubEvents(CCO, strMethodName, inn, itmProgram);
                }

                //更新場地編號
                AddEventSite(CCO, strMethodName, inn, itmMeeting, itmProgram);

                //重算場次數
                RecountEvents(CCO, strMethodName, inn, itmProgram);

                //修補隊伍的量級序號
                FixTeamSectionNo(CCO, strMethodName, inn, itmProgram);
            }

            return itmR;
        }

        //重算場次數
        private void RecountEvents(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmProgram)
        {
            string sql = "";
            Item itmSQL = null;

            string program_id = itmProgram.getProperty("id", "");

            sql = @"
                UPDATE t1 SET
	                t1.in_event_count = t2.cnt
                FROM
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
                (
	                SELECT
		                source_id
		                , count(*) AS 'cnt'
	                FROM
		                IN_MEETING_PEVENT WITH(NOLOCK)
	                WHERE
		                source_id = '{#program_id}'
		                AND ISNULL(in_tree_no, 0) > 0
		                AND ISNULL(in_win_status, '') NOT IN ('cancel', 'nofight', 'bypass')
	                GROUP BY
		                source_id
                ) t2
                ON t2.source_id = t1.id
            ";

            sql = sql.Replace("{#program_id}", program_id);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            itmSQL = inn.applySQL(sql);

            if (itmSQL.isError()) throw new Exception("發生錯誤: 重算場次數");

        }

        //建立子場次
        private void GenerateSubEvents(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmProgram)
        {
            Item item = inn.newItem();
            item.setType("In_Meeting_Program");
            item.setProperty("meeting_id", itmProgram.getProperty("in_meeting", ""));
            item.setProperty("program_id", itmProgram.getProperty("id", ""));
            item.apply("in_meeting_pevent_sub");
        }

        /// <summary>
        /// 更新場地編號
        /// </summary>
        private void AddEventSite(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmMeeting, Item itmProgram)
        {
            string program_id = itmProgram.getProperty("id", "");

            string sql = @"
				 UPDATE t1 SET
             	    t1.in_date_key = t3.in_date_key
             	    , t1.in_site = t4.id
             	    , t1.in_site_code = t4.in_code
             	    , t1.in_tree_state = 0
                 FROM
             	    IN_MEETING_PEVENT t1
                 INNER JOIN
                    IN_MEETING_PROGRAM t2
                    ON t2.id = t1.source_id
                 INNER JOIN
             	    IN_MEETING_ALLOCATION t3
             	    ON t3.in_program = t2.id
                 INNER JOIN
             	    IN_MEETING_SITE t4
             	    ON t4.id = t3.in_site
                 WHERE
                    t2.id = '{#program_id}'
            ";

            sql = sql.Replace("{#program_id}", program_id);

            inn.applySQL(sql);
        }

        /// <summary>
        /// 更新組別賽制
        /// </summary>
        private void UpdateProgram(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmProgram, string in_battle_type)
        {
            string program_id = itmProgram.getProperty("id", "");

            string sql = "UPDATE IN_MEETING_PROGRAM SET"
                + " in_battle_type = N'" + in_battle_type + "'"
                + " , in_tiebreaker = N''"
                + " , in_challenge = N''"
                + " WHERE id = '" + program_id + "'";

            Item itmSQL = inn.applySQL(sql);

            if (itmSQL.isError())
            {
                throw new Exception("更新組別賽制發生錯誤");
            }
        }

        /// <summary>
        /// 清除場次資料
        /// </summary>
        private void RemoveEvents(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmProgram, bool is_delete_pteam = false)
        {
            Item itmData = inn.newItem("In_Meeting_Program");
            itmData.setProperty("program_id", itmProgram.getProperty("id", ""));
            itmData.apply("in_meeting_program_remove_all");
        }

        /// <summary>
        /// 修正隊伍量級序號
        /// </summary>
        private void FixTeamSectionNo(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmProgram)
        {
            string program_id = itmProgram.getProperty("id", "");

            string sql = "UPDATE IN_MEETING_PTEAM SET in_section_no = in_sign_no WHERE source_id = '" + program_id + "'";

            inn.applySQL(sql);
        }

        /// <summary>
        /// 建立單循環比賽場次
        /// </summary>
        private void AddCycleEvents(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmProgram)
        {
            string in_meeting = itmProgram.getProperty("in_meeting", "");
            string program_id = itmProgram.getProperty("id", "");
            string in_fight_day = itmProgram.getProperty("in_fight_day", "");
            string in_site = itmProgram.getProperty("in_site", "");

            int team_count = GetIntVal(itmProgram.getProperty("in_team_count", "0"));

            var event_cycles = GetSingleCycles(team_count);
            int event_count = event_cycles.Count;

            if (event_count == 0)
            {
                return;
                //throw new Exception("單循環賽資料錯誤");
            }

            string tree_code = "M";
            string tree_name = "main";
            int tree_sort = 100;
            int current_round = 1;

            for (int i = 1; i <= event_count; i++)
            {
                TCycle cycle = event_cycles[i - 1];
                //回合序號
                int current_round_id = i;
                //樹圖序號(空間)
                string in_tree_id = GetTreeId(tree_code, current_round, current_round_id);

                TRobin robin = GetSRTreeNo(team_count, cycle.Val, i);

                TEvent evt = new TEvent
                {
                    in_meeting = in_meeting,
                    source_id = program_id,
                    in_date_key = in_fight_day,
                    in_site = in_site,

                    in_tree_name = tree_name,
                    in_tree_sort = tree_sort,
                    in_tree_id = in_tree_id,
                    in_tree_no = robin.TreeNo.ToString(),

                    in_round = current_round,
                    in_round_code = event_count,
                    in_round_id = current_round_id,

                    in_sign_code = event_count,
                    in_sign_no = i,

                    in_detail_ns = robin.SignNos,

                    in_robin_key = cycle.Key,
                };

                ApplyEvent(CCO, strMethodName, inn, evt);

                if (team_count == 2)
                {
                    //三戰兩勝(2)
                    CloneFor3Battle2Win(CCO, strMethodName, inn, evt, "M102", "2", 2);
                    //三戰兩勝(3)
                    CloneFor3Battle2Win(CCO, strMethodName, inn, evt, "M103", "3", 3);
                }
            }
        }

        //三戰兩勝加賽
        private void CloneFor3Battle2Win(
            Aras.Server.Core.CallContext CCO
            , string strMethodName
            , Innovator inn
            , TEvent evt
            , string in_tree_id
            , string in_tree_no
            , int in_round
            )
        {
            evt.in_tree_id = in_tree_id;
            evt.in_tree_no = in_tree_no;
            evt.in_round = in_round;
            ApplyEvent(CCO, strMethodName, inn, evt);
        }

        /// <summary>
        /// 建立雙循環比賽場次
        /// </summary>
        private void AddDoubleCycleEvents(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmProgram)
        {
            string in_meeting = itmProgram.getProperty("in_meeting", "");
            string program_id = itmProgram.getProperty("id", "");
            string in_fight_day = itmProgram.getProperty("in_fight_day", "");
            string in_site = itmProgram.getProperty("in_site", "");
            int team_count = GetIntVal(itmProgram.getProperty("in_team_count", ""));

            var event_cycles = GetDoubleCycles(team_count);
            int event_count = event_cycles.Count;

            if (event_count == 0)
            {
                throw new Exception("雙循環賽資料錯誤");
            }

            string tree_code = "M";
            string tree_name = "main";
            int tree_sort = 100;
            int current_round = 1;

            for (int i = 1; i <= event_count; i++)
            {
                TCycle cycle = event_cycles[i - 1];
                //回合序號
                int current_round_id = i;
                //樹圖序號(空間)
                string in_tree_id = GetTreeId(tree_code, current_round, current_round_id);
                //對戰籤號清單
                string in_detail_ns = cycle.Key;

                TEvent evt = new TEvent
                {
                    in_meeting = in_meeting,
                    source_id = program_id,
                    in_date_key = in_fight_day,
                    in_site = in_site,

                    in_tree_name = tree_name,
                    in_tree_sort = tree_sort,
                    in_tree_id = in_tree_id,
                    in_tree_no = i.ToString(),

                    in_round = current_round,
                    in_round_code = event_count,
                    in_round_id = current_round_id,

                    in_sign_code = event_count,
                    in_sign_no = i,

                    in_detail_ns = in_detail_ns,
                };

                ApplyEvent(CCO, strMethodName, inn, evt);
            }
        }

        /// <summary>
        /// 新增比賽場次
        /// </summary>
        private void ApplyEvent(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, TEvent evt)
        {
            Item itmEvent = inn.newItem("In_Meeting_PEvent", "add");
            itmEvent.setProperty("in_meeting", evt.in_meeting);
            itmEvent.setProperty("source_id", evt.source_id);
            itmEvent.setProperty("in_date_key", evt.in_date_key);
            itmEvent.setProperty("in_site", evt.in_site);

            itmEvent.setProperty("in_tree_name", evt.in_tree_name);
            itmEvent.setProperty("in_tree_sort", evt.in_tree_sort.ToString());
            itmEvent.setProperty("in_tree_id", evt.in_tree_id);
            itmEvent.setProperty("in_tree_no", evt.in_tree_no);

            itmEvent.setProperty("in_round", evt.in_round.ToString());
            itmEvent.setProperty("in_round_code", evt.in_round_code.ToString());
            itmEvent.setProperty("in_round_id", evt.in_round_id.ToString());

            itmEvent.setProperty("in_sign_code", evt.in_sign_code.ToString());
            itmEvent.setProperty("in_sign_no", evt.in_sign_no.ToString());

            itmEvent.setProperty("in_detail_ns", evt.in_detail_ns);
            itmEvent.setProperty("in_robin_key", evt.in_robin_key);


            itmEvent = itmEvent.apply();

            if (itmEvent.isError())
            {
                throw new Exception("建立賽事組別場次發生失敗");
            }

            evt.id = itmEvent.getID();
        }

        /// <summary>
        /// 新增比賽分組
        /// </summary>
        private void AddEventDetails(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmMeeting, Item itmProgram)
        {
            string program_id = itmProgram.getProperty("id", "");

            Item itmEvents = inn.applySQL("SELECT * FROM IN_MEETING_PEVENT WITH(NOLOCK) WHERE source_id = '" + program_id + "' ");

            int count = itmEvents.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item itmEvent = itmEvents.getItemByIndex(i);

                string event_id = itmEvent.getProperty("id", "");
                string in_detail_ns = itmEvent.getProperty("in_detail_ns", "");

                string[] ns = in_detail_ns.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                if (ns == null || ns.Length <= 1)
                {
                    throw new Exception("對戰籤號清單錯誤");
                }

                for (int j = 1; j <= ns.Length; j++)
                {
                    string in_sign_no = "";
                    string in_sign_foot = "";
                    string in_target_no = "";
                    string in_target_foot = "";

                    if (j % 2 != 0)
                    {
                        in_sign_no = ns[0];
                        in_sign_foot = "1";
                        in_target_no = ns[1];
                        in_target_foot = "2";
                    }
                    else
                    {
                        in_sign_no = ns[1];
                        in_sign_foot = "2";
                        in_target_no = ns[0];
                        in_target_foot = "1";
                    }

                    Item itmDetail = inn.newItem("In_Meeting_PEvent_Detail", "add");
                    itmDetail.setProperty("source_id", event_id);
                    itmDetail.setProperty("in_sign_foot", in_sign_foot);
                    itmDetail.setProperty("in_sign_no", in_sign_no);

                    itmDetail.setProperty("in_target_no", in_target_no);
                    itmDetail.setProperty("in_target_foot", in_target_foot);
                    itmDetail = itmDetail.apply();
                }
            }
        }

        /// <summary>
        /// 取得單循環對戰列表
        /// </summary>
        private List<TCycle> GetSingleCycles(int team_count)
        {
            List<TCycle> list = new List<TCycle>();
            for (int y = 1; y <= team_count; y++)
            {
                for (int x = 2; x <= team_count; x++)
                {
                    if (y == x)
                    {
                        continue;
                    }

                    TCycle cycle = GetSingleCycle(y, x);
                    if (!list.Exists(e => e.Key == cycle.Key))
                    {
                        list.Add(cycle);
                    }
                }
            }
            return list;
        }

        /// <summary>
        /// 取得雙循環對戰列表
        /// </summary>
        private List<TCycle> GetDoubleCycles(int team_count)
        {
            List<TCycle> list = new List<TCycle>();
            for (int y = 1; y <= team_count; y++)
            {
                for (int x = 1; x <= team_count; x++)
                {
                    if (y == x)
                    {
                        continue;
                    }

                    TCycle cycle = new TCycle
                    {
                        Key = y + "," + x,
                        No1 = y,
                        No2 = x,
                        Val = y + "-" + x,
                    };
                    list.Add(cycle);
                }
            }
            return list;
        }

        /// <summary>
        /// 取得單循環對戰
        /// </summary>
        private TCycle GetSingleCycle(int y, int x)
        {
            if (y < x)
            {
                return new TCycle
                {
                    Key = y + "," + x,
                    No1 = y,
                    No2 = x,
                    Val = y + "-" + x,
                };
            }
            else
            {
                return new TCycle
                {
                    Key = x + "," + y,
                    No1 = x,
                    No2 = y,
                    Val = x + "-" + y,
                };
            }
        }

        /// <summary>
        /// 取得樹圖序號
        /// </summary>
        private string GetTreeId(string code, int round, int id)
        {
            return code + round + id.ToString().PadLeft(2, '0');
        }

        /// <summary>
        /// 取得樹圖序號
        /// </summary>
        private TRobin GetSRTreeNo(int team_count, string yx, int i)
        {
            switch (team_count)
            {
                case 1:
                case 2:
                    return GetSRTreeNo1(yx);

                case 3:
                    return GetSRTreeNo3(yx);

                case 4:
                    return GetSRTreeNo4(yx);

                case 5:
                    return GetSRTreeNo5(yx);

                default:
                    return new TRobin { TreeNo = i, SignNos = "" };
            }
        }

        /// <summary>
        /// 取得樹圖序號
        /// </summary>
        private TRobin GetSRTreeNo1(string yx)
        {
            return new TRobin
            {
                TreeNo = 1,
                SignNos = "1,2",
            };
        }

        /// <summary>
        /// 取得樹圖序號
        /// </summary>
        private TRobin GetSRTreeNo3(string yx)
        {
            switch (yx)
            {
                case "1-2": return new TRobin { TreeNo = 1, SignNos = "2,1" };
                case "2-3": return new TRobin { TreeNo = 2, SignNos = "3,2" };
                case "1-3": return new TRobin { TreeNo = 3, SignNos = "1,3" };
                default: return new TRobin { TreeNo = 0, SignNos = "" };
            }
        }

        /// <summary>
        /// 取得樹圖序號
        /// </summary>
        private TRobin GetSRTreeNo4(string yx)
        {
            switch (yx)
            {
                case "1-2": return new TRobin { TreeNo = 1, SignNos = "2,1" };
                case "3-4": return new TRobin { TreeNo = 2, SignNos = "4,3" };
                case "1-3": return new TRobin { TreeNo = 3, SignNos = "3,1" };
                case "2-4": return new TRobin { TreeNo = 4, SignNos = "4,2" };
                case "1-4": return new TRobin { TreeNo = 5, SignNos = "4,1" };
                case "2-3": return new TRobin { TreeNo = 6, SignNos = "3,2" };
                default: return new TRobin { TreeNo = 0, SignNos = "" };
            }
        }

        /// <summary>
        /// 取得樹圖序號
        /// </summary>
        private TRobin GetSRTreeNo5(string yx)
        {
            switch (yx)
            {
                case "1-2": return new TRobin { TreeNo = 1, SignNos = "2,1" };
                case "3-4": return new TRobin { TreeNo = 2, SignNos = "4,3" };
                case "1-5": return new TRobin { TreeNo = 3, SignNos = "5,1" };
                case "2-3": return new TRobin { TreeNo = 4, SignNos = "3,2" };
                case "4-5": return new TRobin { TreeNo = 5, SignNos = "5,4" };
                case "1-3": return new TRobin { TreeNo = 6, SignNos = "3,1" };
                case "2-4": return new TRobin { TreeNo = 7, SignNos = "4,2" };
                case "3-5": return new TRobin { TreeNo = 8, SignNos = "5,3" };
                case "1-4": return new TRobin { TreeNo = 9, SignNos = "4,1" };
                case "2-5": return new TRobin { TreeNo = 10, SignNos = "5,2" };
                default: return new TRobin { TreeNo = 0, SignNos = "" };
            }
        }

        //取得賽事資訊
        private Item GetMeeting(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string meeting_id)
        {
            string aml = @"
        <AML>
            <Item type='In_Meeting' action='get' id='{#meeting_id}' select='in_title,in_battle_type,in_surface_code'>
            </Item>
        </AML>
        ".Replace("{#meeting_id}", meeting_id);

            return inn.applyAML(aml);
        }

        //取得賽程組別資訊
        private Item GetProgram(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string program_id)
        {
            string sql = @"
        SELECT
            *
        FROM
            IN_MEETING_PROGRAM WITH(NOLOCK)
        WHERE
            id = '{#program_id}'
        ";

            sql = sql.Replace("{#program_id}", program_id);

            return inn.applySQL(sql);
        }

        //取得 children 賽程組別資訊
        private Item GetChildrenPrograms(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string program_id)
        {
            string sql = @"
        SELECT
            *
        FROM
            IN_MEETING_PROGRAM WITH(NOLOCK)
        WHERE
            in_program = '{#program_id}'
        ";

            sql = sql.Replace("{#program_id}", program_id);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return inn.applySQL(sql);
        }

        /// <summary>
        /// 場次資料結構
        /// </summary>
        private class TEvent
        {
            /// <summary>
            /// 賽程 id
            /// </summary>
            public string in_meeting { get; set; }

            /// <summary>
            /// 賽程組別 id
            /// </summary>
            public string source_id { get; set; }

            /// <summary>
            /// 賽程場次 id
            /// </summary>
            public string id { get; set; }

            /// <summary>
            /// 樹圖名稱
            /// </summary>
            public string in_tree_name { get; set; }

            /// <summary>
            /// 樹圖排序
            /// </summary>
            public int in_tree_sort { get; set; }

            /// <summary>
            /// 樹圖序號(空間)
            /// </summary>
            public string in_tree_id { get; set; }

            /// <summary>
            /// 樹圖序號(時間)
            /// </summary>
            public string in_tree_no { get; set; }

            /// <summary>
            /// 回合
            /// </summary>
            public int in_round { get; set; }

            /// <summary>
            /// 回合代碼
            /// </summary>
            public int in_round_code { get; set; }

            /// <summary>
            /// 回合序號
            /// </summary>
            public int in_round_id { get; set; }

            /// <summary>
            /// 場次籤數
            /// </summary>
            public int in_sign_code { get; set; }

            /// <summary>
            /// 場次籤號
            /// </summary>
            public int in_sign_no { get; set; }

            /// <summary>
            /// 對戰籤號清單
            /// </summary>
            public string in_detail_ns { get; set; }

            /// <summary>
            /// 循環賽鍵
            /// </summary>
            public string in_robin_key { get; set; }

            /// <summary>
            /// 比賽日期
            /// </summary>
            public string in_date_key { get; set; }

            /// <summary>
            /// 比賽場地 id
            /// </summary>
            public string in_site { get; set; }
        }

        private class TCycle
        {
            public string Key { get; set; }
            public int No1 { get; set; }
            public int No2 { get; set; }
            public string Val { get; set; }
        }

        private class TRobin
        {
            public int TreeNo { get; set; }
            public string SignNos { get; set; }
        }

        private int GetIntVal(string value, int def = 0)
        {
            int result = def;
            Int32.TryParse(value, out result);
            return result;
        }
    }
}