﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class In_Cla_MeetingRegistryResult : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 提供報名清單
                位置: In_MeetingRegistryResult.html
                做法:
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]" + "In_Cla_MeetingRegistryResult";

            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            string aml = "";
            string sql = "";
            string strError = "";
            string strUserId = inn.getUserID();

            DateTime CurrentTime = System.DateTime.Today;

            _InnH.AddLog(strMethodName, "MethodSteps");

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "input dom: " + itmR.dom.InnerXml);

            string meeting_id = itmR.getProperty("meeting_id");
            string pay_number = itmR.getProperty("pay_number", ""); //取得[繳費單號]
            string org_photo = itmR.getProperty("org_photo", "");

            //協助報名者身分證號
            string in_creator_sno = "";
            //所屬群組
            string gym_group = "";
            //賽事狀態或單位狀態是否已關閉
            string gym_is_closed = "";
            string gym_really_closed = "";

            //委員會可看到該委員會下社團報名
            string userType = "";
            string userResumeID = "";

            Item itmPermit = inn.applyMethod("In_CheckIdentity", "<method>" + strMethodName + "</method><code>" + "ALL" + "</code>");
            bool isMeetingAdmin = itmPermit.getProperty("isMeetingAdmin", "") == "1";
            bool isCommittee = itmPermit.getProperty("isCommittee", "") == "1";
            bool isGymOwner = itmPermit.getProperty("isGymOwner", "") == "1";
            bool isGymAssistant = itmPermit.getProperty("isGymAssistant", "") == "1";

            //無權限
            if (!isMeetingAdmin && !isCommittee && !isGymOwner && !isGymAssistant)
            {
                return itmR;
            }

            //登入者資訊
            Item itmLogin = inn.newItem("In_Resume", "get");
            itmLogin.setProperty("in_user_id", strUserId);
            itmLogin = itmLogin.apply();

            //登入者錯誤
            if (itmLogin.isError())
            {
                return itmR;
            }

            userResumeID = itmLogin.getProperty("id", "");
            userType = itmLogin.getProperty("in_member_type", "");
            in_creator_sno = itmLogin.getProperty("in_sno", "");
            gym_group = itmLogin.getProperty("in_group", "");


            //賽事資訊
            aml = "<AML><Item type='In_Cla_Meeting' action='get' id='" + meeting_id + "'></Item></AML>";
            Item itmMeeting = inn.applyAML(aml);

            //報名開始時間
            string in_state_time_start = itmMeeting.getProperty("in_state_time_start", CurrentTime.ToString("yyyy-MM-dd"));
            //報名結束時間
            string in_state_time_end = itmMeeting.getProperty("in_state_time_end", CurrentTime.ToString("yyyy-MM-dd"));
            //已額滿
            string in_isfull = itmMeeting.getProperty("in_isfull", "");
            //活動類型
            string in_meeting_type = itmMeeting.getProperty("in_meeting_type", ""); 


            //是否為共同講師
            bool open = GetUserResumeListStatus(inn, meeting_id, strUserId);
            if (open) isMeetingAdmin = true;

            itmPermit.setProperty("isCoInstructor", open ? "1" : "0");

            bool is_gym = (isGymOwner || isGymAssistant);
            if (isMeetingAdmin || open) is_gym = false;

            if (is_gym)
            {
                gym_is_closed = "0";
                gym_really_closed = "0";

                sql = "SELECT id FROM In_Cla_Meeting_Gymlist WITH(NOLOCK) WHERE source_id = '{#meeting_id}' AND in_creator_sno = N'{#in_creator_sno}'";
                sql = sql.Replace("{#meeting_id}", meeting_id)
                    .Replace("{#in_creator_sno}", in_creator_sno);

                Item itmMeetingGymlist = inn.applySQL(sql);

                if (itmMeetingGymlist.getResult() != "")
                {
                    gym_is_closed = "1";
                    gym_really_closed = "1";
                }
            }


            //附加條件
            string group_condition = "";

            if (isMeetingAdmin)
            {
                //協會帳號可看所有報名人員

            }
            else if (isCommittee)
            {
                if (in_meeting_type == "degree")
                {
                    //委員會帳號可看該區所有報名人員
                    //alan 模稜兩可 in_manager_org
                    //  group_condition = "AND in_manager_org = N'" + userResumeID + "'";
                    group_condition = "AND t5.in_manager_org = N'" + userResumeID + "'";
                }
                else
                {
                    group_condition = "AND in_creator_sno = N'" + in_creator_sno + "'";
                }
            }
            else
            {
                group_condition = "AND in_creator_sno = N'" + in_creator_sno + "'";
            }

            string paynumber_condition = pay_number != "" ? "AND ISNULL(in_paynumber, '') = N'" + pay_number + "'" : "";

            DateTime dtNow = System.DateTime.Now;
            DateTime meeting_time_s = Convert.ToDateTime(in_state_time_start);//將開始時間轉型
            DateTime meeting_time_e = Convert.ToDateTime(in_state_time_end);//將結束時間轉型

            //賽事狀態判定(丟給前台)
            //尚未開始
            if (dtNow < meeting_time_s)//今日<開始
            {
                gym_is_closed = "1";
            }
            //額滿
            else if (in_isfull == "1")//已額滿 == 1
            {
                gym_is_closed = "1";
            }
            //報名中
            else if (dtNow < meeting_time_e && dtNow > meeting_time_s)//今日<結束&&今日>開始
            {

            }
            //報名截止
            else if (dtNow > meeting_time_e)//今日>結束
            {
                gym_is_closed = "1";
            }

            //單位編輯模式
            Item itmOrgMode = GetOrgMode(inn, org_photo);
            Item itmSurvey = this;
            itmSurvey.setProperty("meeting_id", meeting_id);
            itmSurvey.setProperty("group_condition", group_condition);
            itmSurvey.setProperty("paynumber_condition", paynumber_condition);
            itmSurvey.setProperty("gym_is_closed", gym_is_closed);

            //與會者
            AppendMeetingUsers(CCO, strMethodName, inn, itmPermit, itmMeeting, itmSurvey, itmOrgMode, itmR);

            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

            return itmR;
        }

        //與會者
        private void AppendMeetingUsers(Aras.Server.Core.CallContext CCO
            , string strMethodName
            , Innovator inn
            , Item itmPermit
            , Item itmMeeting
            , Item itmSurvey
            , Item itmOrgMode
            , Item itmReturn)
        {
            string sql = "";

            bool isMeetingAdmin = itmPermit.getProperty("isMeetingAdmin", "") == "1";

            string meeting_id = itmMeeting.getProperty("id", "");
            string in_date_s = itmMeeting.getProperty("in_date_s", "");//活動開始時間
            string in_meeting_type = itmMeeting.getProperty("in_meeting_type", "");
            string in_pay_mode = itmMeeting.getProperty("in_pay_mode", "");

            string in_isfull = itmReturn.getProperty("in_isfull", "");
            string gym_really_closed = itmReturn.getProperty("gym_really_closed", "");

            string gym_is_closed = itmSurvey.getProperty("gym_is_closed", "");

            string group_condition = itmSurvey.getProperty("group_condition", "").Replace("AND in_creator_sno", "AND t1.in_creator_sno");
            string paynumber_condition = itmSurvey.getProperty("paynumber_condition", "");

            string org_upload_mode = itmOrgMode.getProperty("org_upload_mode", "");
            string org_text_mode = itmOrgMode.getProperty("org_text_mode", "");

            string hide_section_col = "data-visible=\"false\"";
            string hide_index_col = "data-visible=\"false\"";
            string hide_degree_col = "data-visible=\"false\"";
            string hide_note_col = "data-visible=\"false\"";
            string hide_pay_col = "data-visible=\"false\"";    //繳費狀態
            string hide_refund_col = "data-visible=\"false\""; //退費申請
            string hide_cert_col = "data-visible=\"false\"";   //資料狀態
            string hide_verify_col = "data-visible=\"false\""; //審核資格狀態

            //無金流模式、土銀模式、QrCode 模式
            if (in_pay_mode == "2" || in_pay_mode == "3" || in_pay_mode == "4")
            {
                hide_pay_col = "";
            }

            switch (in_meeting_type)
            {
                case "degree":
                    hide_degree_col = "";
                    hide_cert_col = "";
                    hide_verify_col = "";
                    //晉段強制關閉【繳費狀態】欄位
                    hide_pay_col = "data-visible=\"false\"";
                    break;

                case "seminar":
                    hide_section_col = "";
                    hide_index_col = "";
                    hide_note_col = "";
                    //hide_refund_col = "";
                    hide_verify_col = "";
                    break;

                default:
                    hide_section_col = "";
                    hide_index_col = "";
                    hide_note_col = "";
                    //hide_refund_col = "";
                    break;

            }

            //申請退費僅允許在活動開始前一週之前可以
            bool can_refund = true;
            DateTime dtNow = System.DateTime.Now;
            DateTime meeting_time_s = Convert.ToDateTime(in_date_s).AddDays(-7);//活動開始時間
            if (dtNow >= meeting_time_s)
            {
                //已過可退費時間
                can_refund = false;
                hide_refund_col = "data-visible=\"false\""; //退費申請
            }


            sql = @"
                SELECT
                    t1.*
                    , t3.in_pay_amount_real
                    , t3.in_pay_photo
                    , t3.in_return_mark
                    , t3.in_collection_agency
                    , t4.in_cancel_status
                    , ISNULL(t6.in_short_org, t1.in_committee) AS 'committee_short_name'
                FROM
                    IN_CLA_MEETING_USER t1 WITH(NOLOCK)
                LEFT OUTER JOIN
                    IN_MEETING_PAY t3 WITH(NOLOCK)
                    ON t3.in_cla_meeting = t1.source_id
                    AND t3.item_number = t1.in_paynumber
                LEFT OUTER JOIN
                    IN_MEETING_NEWS t4 WITH(NOLOCK)
                    ON t4.source_id = t3.id
                    AND t4.in_muid = t1.id
                LEFT OUTER JOIN 
	                IN_RESUME t5 WITH(NOLOCK)
	                ON t5.in_sno = t1.in_creator_sno
                LEFT OUTER JOIN 
	                IN_RESUME t6 WITH(NOLOCK)
	                ON t6.in_name = t1.in_committee 
	                AND t6.in_member_type = 'area_cmt'
	                AND t6.in_member_role = 'sys_9999'
                WHERE
                    t1.source_id = '{#meeting_id}'
                    AND t1.in_note_state = N'official'
                    {#group_condition}
                    {#paynumber_condition}
                ORDER BY
                    t1.in_degree DESC
                    , t1.in_birth
            ";

            sql = sql.Replace("{#meeting_id}", meeting_id)
                .Replace("{#group_condition}", group_condition)
                .Replace("{#paynumber_condition}", paynumber_condition);
            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item items = inn.applySQL(sql);
            int count = items.getItemCount();

            string number = "";

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_group = item.getProperty("in_group", "");
                string in_l1 = item.getProperty("in_l1", "");
                string in_l1_short = in_l1.Replace("跆拳道教練講習", "");

                string in_paynumber = item.getProperty("in_paynumber", "");
                string in_paytime = item.getProperty("in_paytime", "");
                string in_pay_photo = item.getProperty("in_pay_photo", "");

                string in_return_mark = item.getProperty("in_return_mark", "");
                string in_cancel_status = item.getProperty("in_cancel_status", "");

                string in_cert_valid = item.getProperty("in_cert_valid", "");
                string in_verify_result = item.getProperty("in_verify_result", "");
                string in_ass_ver_result = item.getProperty("in_ass_ver_result", "");
                bool is_reject = in_verify_result == "0" || in_ass_ver_result == "0";

                if (can_refund && in_cancel_status != "")
                {
                    //標題列要出現退款申請欄位 (預設隱藏)
                    hide_refund_col = "";
                }

                string inn_keywords = "," + in_l1 + ",";

                item.setType("inn_meeting_user");
                item.setProperty("number", in_paynumber);
                item.setProperty("group", in_group);
                item.setProperty("meeting_id", meeting_id);
                item.setProperty("inn_keywords", inn_keywords);
                item.setProperty("inn_has_stuffs", "0");
                item.setProperty("inn_gym_is_closed", gym_is_closed);
                item.setProperty("in_l1_short", in_l1_short);
                item.setProperty("beaten", "備註");//是badminton丟出備註
                item.setProperty("org_upload_mode", org_upload_mode);
                item.setProperty("org_text_mode", org_text_mode);
                item.setProperty("in_verify_mode", itmMeeting.getProperty("in_verify_mode", ""));

                //繳費狀態
                if (hide_pay_col == "")
                {
                    item.setProperty("pay_number", GetPayStatusIcon(inn, item));
                }
                else
                {
                    item.setProperty("pay_number", "");
                }

                //資料狀態
                if (hide_cert_col == "")
                {
                    item.setProperty("inn_cert_valid", GetCertStatus(in_cert_valid));
                }
                else
                {
                    item.setProperty("inn_cert_valid", "");
                }

                //審核資料狀態
                if (hide_verify_col == "")
                {
                    item.setProperty("inn_verify_display", GetVerifyDisplay(inn, item));
                }

                string hide_edit = "item_show_0";
                string hide_delete = "item_show_0";
                string hide_view = "item_show_0";
                string hide_refund = "item_show_0";
                string hide_exchange = "item_show_0";

                //額滿，但未關閉單位
                if (gym_really_closed != "1" && in_isfull == "1")
                {
                    if (in_paytime != "")
                    {
                        hide_view = "";
                        if (in_cancel_status == "變更申請通過")
                        {
                            hide_exchange = "";
                        }
                        else
                        {
                            hide_refund = "";
                        }
                    }
                    else if (in_paynumber != "")
                    {
                        if (is_reject)
                        {
                            //退回可修改
                            hide_edit = "";
                        }
                        else
                        {
                            //可檢視
                            hide_view = "";
                        }
                    }
                    else
                    {
                        hide_edit = "";
                        hide_delete = "";
                    }
                }
                else if (gym_is_closed == "1")
                {
                    hide_view = "";
                    if (in_paytime != "")
                    {
                        if (in_cancel_status == "變更申請通過")
                        {
                            hide_exchange = "";
                        }
                        else
                        {
                            hide_refund = "";
                        }
                    }
                    else
                    {
                        hide_view = "item_show_0";
                        hide_edit = "";
                        hide_delete = "";
                    }
                }
                else if (in_paytime != "")
                {
                    //已繳費
                    hide_view = "";
                    if (in_cancel_status == "變更申請通過")
                    {
                        hide_exchange = "";
                    }
                    else
                    {
                        hide_refund = "";
                    }
                }
                else if (in_paynumber != "")
                {
                    if (is_reject)
                    {
                        //退回可修改
                        hide_edit = "";
                    }
                    else
                    {
                        //有繳費單未繳款
                        hide_view = "";
                    }
                }
                else
                {
                    hide_edit = "";
                    hide_delete = "";
                }

                if (!can_refund)
                {
                    //不可退費申請
                    hide_refund = "item_show_0";
                }

                item.setProperty("hide_edit", hide_edit);
                item.setProperty("hide_delete", hide_delete);
                item.setProperty("hide_view", hide_view);
                item.setProperty("hide_refund", hide_refund);
                item.setProperty("hide_exchange", hide_exchange);

                itmReturn.addRelationship(item);
            }

            itmReturn.setProperty("muser_count", count > 0 ? count.ToString() : "0");

            itmReturn.setProperty("hide_section_col", hide_section_col);
            itmReturn.setProperty("hide_index_col", hide_index_col);
            itmReturn.setProperty("hide_degree_col", hide_degree_col);
            itmReturn.setProperty("hide_note_col", hide_note_col);
            itmReturn.setProperty("hide_pay_col", hide_pay_col);
            
            itmReturn.setProperty("hide_refund_col", hide_refund_col);
            itmReturn.setProperty("hide_cert_col", hide_cert_col);
            itmReturn.setProperty("hide_verify_col", hide_verify_col);
        }

        //取得共同講師 (判斷現在的登入者是否為此課程的共同講師)
        private bool GetUserResumeListStatus(Innovator inn, string meeting_id, string strUserId)
        {
            string sql = @"
SELECT
    t2.id
FROM 
    In_Cla_Meeting_Resumelist t1 WITH(NOLOCK) 
INNER JOIN 
    IN_RESUME t2 WITH(NOLOCK) 
    ON t2.id = t1.related_id    
WHERE 
    t1.source_id = '{#meeting_id}' 
    AND t2.in_user_id = '{#in_user_id}';    
";

            sql = sql.Replace("{#meeting_id}", meeting_id)
                .Replace("{#in_user_id}", strUserId);

            Item item = inn.applySQL(sql);

            if (item.getResult() != "")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //取得單位編輯模式
        private Item GetOrgMode(Innovator inn, string org_photo)
        {
            Item item = inn.newItem();
            if (org_photo == "1")
            {
                item.setProperty("org_upload_mode", "item_show_1");
                item.setProperty("org_text_mode", "item_show_0");
            }
            else
            {
                item.setProperty("org_upload_mode", "item_show_0");
                item.setProperty("org_text_mode", "item_show_1");
            }
            return item;
        }

        private string GetPayStatusIcon(Innovator inn, Item item)
        {
            StringBuilder builder = new StringBuilder();

            string[] properties = new string[]
            {
        "in_paynumber", //繳費單號
        "in_pay_amount_real", //實際收款金額
        "in_pay_photo", //上傳繳費照片(繳費收據)
        "in_return_mark", //審核退回說明
        "in_collection_agency", //代收機構
            };

            foreach (string property in properties)
            {
                builder.Append("<" + property + ">" + item.getProperty(property, "") + "</" + property + ">");
            }

            Item itmResult = inn.applyMethod("In_PayType_Icon", builder.ToString());

            return itmResult.getProperty("in_pay_type", "");
        }

        //資料狀態
        private string GetCertStatus(string value)
        {
            if (value == "1")
            {
                return "<span style='color: green'>已上傳</span>";
            }
            else if (value == "0")
            {
                return "<span style='color: red'>資料不齊</span>";
            }
            else
            {
                return "<span style='color: #2C4198'>未檢查</span>";
            }
        }

        private string GetVerifyDisplay(Innovator inn, Item item)
        {
            StringBuilder builder = new StringBuilder();

            string[] properties = new string[]
            {
                "in_verify_mode", //活動審核模式
                "in_verify_result", //委員會審核結果
                "in_verify_memo", //委員會審核說明
                "in_ass_ver_result", //協會審核結果
                "in_ass_ver_memo", //協會審核說明
            };

            foreach (string property in properties)
            {
                builder.Append("<" + property + ">" + item.getProperty(property, "") + "</" + property + ">");
            }

            Item itmResult = inn.applyMethod("In_Verify_Message", builder.ToString());

            return itmResult.getProperty("in_verify_display", "");

        }
    }
}