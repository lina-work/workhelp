﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;
using System.Net;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class In_Cla_Get_Meeting_Register : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                In_Cla_Get_Meeting_Register
                用來取得 MeetingRegistry.html 系列檔案所需要的資料，由b.aspx使用
                傳入參數：
	                meeting_id 	: 會議的id
	                muid		: 會議使用者的id，非必要，若有傳入，視為「修改使用者報名資料」模式。額外回傳使用者所填答的問卷。
	                continous 	: 是否為連續報名模式，字串true或false，預設false
                回傳物件結構:見下方主查詢AML
                額外回傳:
	                inn_is_active  	:會議是否仍可報名
	                inn_isFull		:會議是否已額滿
	                inn_reg_on_scene:是否為現場報名
	                inn_modify_mode	:是否為修改使用者報名資料模式
	                inn_muid		:有傳入使用者id時，將之回寫並帶下去
                由itmRst乘載所有回傳資料。
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]In_Cla_Get_Meeting_Register";

            Item itmRst, itmMeeting;

            string meetingid = this.getProperty("meeting_id", "no_data");
            string strMuid = this.getProperty("muid", "no_data");
            string strIsFull = "false";                                     //是否已額滿
            string strIsActive = "false";                                       //是否仍可報名
            string strRegOnScene = "false";                                 //是否為現場報名
            string strModifyMode = "false";                                 //是否為修改使用者報名資料模式
            string strContinousMode = this.getProperty("continous", "false");   //是否為連續報名模式
            int intSysPrepare, intRealPrepare, intSysTaking, intRealTaking;

            string strUserId = inn.getUserID();
            string strIdentityId = inn.getUserAliases();

            string sql = "";

            //主查詢AML
            string strGetMeetingAML = @"
<AML>
	<Item type='In_Cla_Meeting' action='get' id='{#meeting_id}'>
		<Relationships>
			<Item type='In_Cla_Meeting_Surveys' action='get' orderBy='[In_Cla_Meeting_Surveys].sort_order'>
				<in_surveytype>1</in_surveytype>
			</Item>
			<Item type='In_Cla_Meeting_File' action='get'/>
			<Item type='In_Cla_Meeting_FunctionTime' select='top 1 in_date_s,in_date_e,in_action' action='get'>
				<in_date_s condition='lt'>{#now}</in_date_s>
				<in_date_e condition='gt'>{#now}</in_date_e>
				<in_action condition='in'>'sheet1','2'</in_action>
			</Item>
				{#muquery}
		</Relationships>
	</Item>
</AML>"
            .Replace("{#meeting_id}", meetingid)
            .Replace("{#now}", System.DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss"));

            string muQuery = @"
<Item type='In_Cla_Meeting_Surveys_Result' action='get'>
	<in_participant>{#muid}</in_participant>
<Item>".Replace("{#muid}", strMuid);

            //假如有傳入muid，將{#muquery}replace成取得使用者報名問卷的AML，如果沒有則取代為空白。
            if (strMuid != "no_data")
            {
                strModifyMode = "true";
                string strMuQuery = @"
<Item type='In_Cla_Meeting_Surveys_Result' aciton='get' orderBy='sort_order'>
	<in_participant>{#muid}</in_participant>
</Item>".Replace("{#muid}", strMuid);
                strGetMeetingAML = strGetMeetingAML.Replace("{#muquery}", strMuQuery);
            }
            else
            {
                strGetMeetingAML = strGetMeetingAML.Replace("{#muquery}", "");
            }

            itmMeeting = inn.applyAML(strGetMeetingAML);

            //取得登入者權限
            Item itmPermit = inn.applyMethod("In_CheckIdentity", "<method>" + strMethodName + "</method><code>ALL</code>");
            bool isMeetingAdmin = itmPermit.getProperty("isMeetingAdmin", "") == "1";
            bool isCommittee = itmPermit.getProperty("isCommittee", "") == "1";
            bool isGymOwner = itmPermit.getProperty("isGymOwner", "") == "1";
            bool isGymAssistant = itmPermit.getProperty("isGymAssistant", "") == "1";

            //登入者
            Item itmLogin = inn.newItem("In_Resume", "get");
            itmLogin.setProperty("in_user_id", strUserId);
            itmLogin = itmLogin.apply();


            //是否為共同講師
            bool open = GetUserResumeListStatus(inn, meetingid, strUserId);
            if (open) isMeetingAdmin = true;


            //晉段
            string in_meeting_type = itmMeeting.getProperty("in_meeting_type", "");
            string login_sno = itmLogin.getProperty("in_sno", "");
            if (!isMeetingAdmin && in_meeting_type == "degree")
            {
                isMeetingAdmin = GetDegreeAdmin(inn, login_sno);
            }

            //透過 In_MeetingFunctionTime 檢查是仍可報名及是否為現場報名
            Item itmFunctiontimes = itmMeeting.getRelationships("In_Cla_Meeting_FunctionTime");
            int intFunctionCount = itmFunctiontimes.getItemCount();
            for (int mfc = 0; mfc < intFunctionCount; mfc++)
            {
                Item itmFunctiontime = itmFunctiontimes.getItemByIndex(mfc);
                switch (itmFunctiontime.getProperty("in_action", ""))
                {
                    case "sheet1":
                        if (strIsActive == "true") { break; }
                        else { strIsActive = "true"; break; }
                    case "2":
                        if (strRegOnScene == "true") { break; }
                        else { strRegOnScene = "true"; break; }
                }
                if (strIsActive == "true" && strRegOnScene == "true") { break; }
            }

            //2021-11-26 繳費注意事項設定抓取in_note_pay欄位或是取預設文字
            string strPayNotice = itmMeeting.getProperty("in_note_pay", "");

            //因為資料都在In_Survey上，在這裡將 In_Survey 直接放到 relationship 裡面。         
            Item itmSurveys = itmMeeting.getRelationships("In_Cla_Meeting_Surveys");
            int surcount = itmSurveys.getItemCount();
            for (int ui = 0; ui < surcount; ui++)
            {
                Item itmMeetingSurvey = itmSurveys.getItemByIndex(ui);
                Item itmTmpSurvey = itmMeetingSurvey.getRelatedItem();
                string in_client_remove = itmTmpSurvey.getProperty("in_client_remove", "");
                if (in_client_remove == "1")
                {
                    continue;
                }
                string QuestionTitle = itmTmpSurvey.getProperty("in_questions", "");
                string strSort = itmMeetingSurvey.getProperty("in_sort_order", "");
                if (strSort != "") { strSort += "."; }
                QuestionTitle = strSort + QuestionTitle;
                itmTmpSurvey.setProperty("in_questions", QuestionTitle);
                itmMeeting.addRelationship(itmTmpSurvey);
            }

            intSysPrepare = Int32.Parse(itmMeeting.getProperty("in_prepare", "0"));
            intSysTaking = Int32.Parse(itmMeeting.getProperty("in_taking", "0"));
            intRealPrepare = Int32.Parse(itmMeeting.getProperty("in_real_prepare", "0"));
            intRealTaking = Int32.Parse(itmMeeting.getProperty("in_real_taking", "0"));

            if (intRealPrepare >= intSysPrepare && intRealTaking >= intSysTaking) { strIsFull = "true"; }

            itmRst = itmMeeting;

            //組裝回傳物件
            itmRst.setProperty("in_date_s", System.DateTime.Parse(itmRst.getProperty("in_date_s")).ToString("yyyy/MM/dd HH:mm"));
            itmRst.setProperty("in_date_e", System.DateTime.Parse(itmRst.getProperty("in_date_e")).ToString("yyyy/MM/dd HH:mm"));
            itmRst.setProperty("inn_is_active", strIsActive);
            itmRst.setProperty("inn_is_full", strIsFull);
            itmRst.setProperty("inn_register_on_scene", strRegOnScene);
            itmRst.setProperty("inn_modify_mode", strModifyMode);
            itmRst.setProperty("inn_continous_mode", strContinousMode);
            itmRst.setProperty("inn_muid", strMuid);

            string no_pay = "";
            string show_pay_amount = "";

            if (isMeetingAdmin)
            {
                itmRst.setProperty("inn_admin_flow", "");
                itmRst.setProperty("inn_committee_flow", "item_show_0");
                itmRst.setProperty("inn_user_flow", "item_show_0");
                itmRst.setProperty("inn_meeting_admin", "1");
            }
            else if (isCommittee)
            {
                itmRst.setProperty("inn_admin_flow", "item_show_0");
                itmRst.setProperty("inn_committee_flow", "");
                itmRst.setProperty("inn_user_flow", "item_show_0");
                itmRst.setProperty("inn_meeting_admin", "1");
            }
            else
            {
                itmRst.setProperty("inn_admin_flow", "item_show_0");
                itmRst.setProperty("inn_committee_flow", "item_show_0");
                itmRst.setProperty("inn_user_flow", "");
                itmRst.setProperty("inn_meeting_admin", "0");

                if (in_meeting_type == "degree")
                {
                    no_pay = "1";
                    show_pay_amount = "item_show_0";
                }
            }

            itmRst.setProperty("show_pay_amount", show_pay_amount);
            itmRst.setProperty("no_pay", no_pay);

            //CCO.Utilities.WriteDebug("In_Cla_Get_Meeting_Register", "this: " + this.dom.InnerXml);
            //CCO.Utilities.WriteDebug("In_Cla_Get_Meeting_Register", "strUserId: " + strUserId);
            //CCO.Utilities.WriteDebug("In_Cla_Get_Meeting_Register", "strIdentityId: " + strIdentityId);

            //登入者資訊區
            itmRst.setProperty("inn_struserid", strUserId);
            itmRst.setProperty("inn_stridentityid", strIdentityId);


            string in_creator_sno = ""; //協助報名者身分證號
            string in_group = "";//所屬群組

            if (!itmLogin.isError() && itmLogin.getItemCount() == 1)
            {
                in_creator_sno = itmLogin.getProperty("in_sno", "");
                in_group = itmLogin.getProperty("in_group", "");
                string in_member_type = itmLogin.getProperty("in_member_type", "");

                itmRst.setProperty("login_name", itmLogin.getProperty("login_name", ""));
                itmRst.setProperty("inn_name", itmLogin.getProperty("in_name", ""));
                itmRst.setProperty("inn_current_org", itmLogin.getProperty("in_current_org", ""));
                itmRst.setProperty("inn_group", in_group);
                itmRst.setProperty("inn_tel", itmLogin.getProperty("in_tel", " "));
                itmRst.setProperty("inn_email", itmLogin.getProperty("in_email", " "));

                itmRst.setProperty("user_id", strUserId);
                itmRst.setProperty("identity_id", strIdentityId);
                itmRst.setProperty("resume_id", itmLogin.getProperty("id", ""));
                itmRst.setProperty("in_group", itmLogin.getProperty("in_group", ""));
                itmRst.setProperty("in_current_org", itmLogin.getProperty("in_current_org", ""));
                itmRst.setProperty("in_name", itmLogin.getProperty("in_name", ""));
                itmRst.setProperty("in_sno", itmLogin.getProperty("in_sno", ""));

                string inn_gym_has_pay = GetGymHasPay(CCO, strMethodName, inn, meetingid, itmLogin.getProperty("in_sno", ""));
                itmRst.setProperty("inn_gym_has_pay", inn_gym_has_pay);

                //lina 2021.01.04 增加區域檢查 _# START:
                Item itmData = inn.newItem("In_Meeting_User");
                itmData.setProperty("inn_type", "In_Cla_Meeting");
                itmData.setProperty("inn_stype", "In_Cla_Meeting_Area");
                itmData.setProperty("inn_id", meetingid);
                itmData.setProperty("inn_mt_type", in_meeting_type);
                itmData.setProperty("inn_member_status", itmLogin.getProperty("in_member_status", ""));
                itmData.setProperty("inn_member_type", itmLogin.getProperty("in_member_type", ""));
                itmData.setProperty("inn_manager_area", itmLogin.getProperty("in_manager_area", ""));
                itmData.setProperty("inn_manager_org", itmLogin.getProperty("in_manager_org", ""));
                Item itmDataResult = itmData.apply("In_Register_Check");
                //throw new Exception();
                itmRst.setProperty("permission_reject_msg", itmDataResult.getProperty("message", ""));
                itmRst.setProperty("resume_manager_name", itmLogin.getProperty("in_manager_name", ""));
                //lina 2021.01.04 增加區域檢查 _# END.
            }

            Item itmVariable = inn.getItemByKeyedName("In_Variable", "meeting_game_name");
            string meeting_name = itmVariable.getProperty("in_value", "");
            itmRst.setProperty("meeting_game_name", meeting_name); //DB名稱


            bool isGymClosed = false;
            if (isGymOwner || isGymAssistant)
            {
                sql = "SELECT id FROM In_Cla_Meeting_Gymlist WITH(NOLOCK) WHERE source_id = '" + meetingid + "' AND in_creator_sno = N'" + in_creator_sno + "'";
                Item MeetingGymList = inn.applySQL(sql);
                if (MeetingGymList.getResult() != "")
                {
                    isGymClosed = true;
                }
            }

            if (isGymClosed)
            {
                itmRst.setProperty("inn_gym_is_closed", "1");
                itmRst.setProperty("inn_gym_real_closed", "1");
            }
            else
            {
                itmRst.setProperty("inn_gym_is_closed", "0");
                itmRst.setProperty("inn_gym_real_closed", "0");
            }


            DateTime CurrentTime = System.DateTime.Today;
            string in_date_s = itmMeeting.getProperty("in_state_time_start", CurrentTime.ToString("yyyy-MM-dd"));//開始時間
            string in_state_time = itmMeeting.getProperty("in_state_time_end", CurrentTime.ToString("yyyy-MM-dd"));//結束時間

            DateTime Meeting_Time_s = Convert.ToDateTime(in_date_s);//將開始時間轉型
            DateTime Meeting_Time_e = Convert.ToDateTime(in_state_time);//將結束時間轉型

            //賽事狀態判定(丟給前台)

            //尚未開始
            if (System.DateTime.Now < Meeting_Time_s)//今日<開始
            {
                itmMeeting.setProperty("inn_gym_is_closed", "1");
                itmRst.setProperty("inn_gym_is_closed", "1");
                itmRst.setProperty("inn_gym_real_closed", "1");
            }
            //額滿
            else if (itmMeeting.getProperty("in_isfull", "") == "1")//已額滿 == 1
            {
                itmMeeting.setProperty("inn_gym_is_closed", "1");
                itmRst.setProperty("in_isfull", "1");
                itmRst.setProperty("inn_gym_is_closed", "1");
                itmRst.setProperty("inn_gym_real_closed", "0");
            }
            //報名中
            else if (System.DateTime.Now < Meeting_Time_e && System.DateTime.Now > Meeting_Time_s)//今日<結束&&今日>開始
            {

            }
            //報名截止
            else if (System.DateTime.Now > Meeting_Time_e)//今日>結束
            {
                itmMeeting.setProperty("inn_gym_is_closed", "1");
                itmRst.setProperty("inn_gym_is_closed", "1");
                itmRst.setProperty("inn_gym_real_closed", "1");
            }

            Item itmVariable2 = inn.getItemByKeyedName("In_Variable", "in_meeting_account_verify");
            string meeting_account_verify = itmVariable2.getProperty("in_value", "");
            itmRst.setProperty("in_meeting_account_verify", meeting_account_verify);//審核關卡

            if (isMeetingAdmin)
            {
                itmRst.setProperty("inn_gym_is_closed", "2");//無關是否送出&報名結束 都要可以看到繳費按鈕
            }

            //2021-11-26 繳費注意事項改取值
            if (itmRst.getProperty("inn_gym_is_closed", "") == "1")
            {
                itmRst.setProperty("closed_mode", "");
            }
            else
            {
                itmRst.setProperty("closed_mode", "item_show_0");
            }
            itmRst.setProperty("inn_pay_notice", strPayNotice);

            //統計報名費用
            AppendAmount(CCO, strMethodName, inn, in_creator_sno, itmPermit, itmRst);

            //附加報名問項連動
            AppendSurveyEvent(CCO, strMethodName, inn, meetingid, itmRst);

            //問項限制人數
            AppendLimit(CCO, strMethodName, inn, meetingid, itmRst);

            string in_current_org = itmLogin.getProperty("in_current_org", "");

            //問項限制單位人數
            AppendLimitCurrent(CCO, strMethodName, inn, meetingid, itmRst, in_current_org);


            //費用統計區
            // itmRst.setProperty("in_unconfirmed", "0");
            // itmRst.setProperty("in_unpaid_exp", "0");
            // itmRst.setProperty("in_paid", "0");

            //附加控制狀態
            AppendStatus(itmRst);

            return itmRst;
        }


        //附加控制狀態
        private void AppendStatus(Item itmReturn)
        {
            string in_pay_mode = itmReturn.getProperty("in_pay_mode", "");
            string in_isfull = itmReturn.getProperty("in_isfull", "");
            string inn_gym_is_closed = itmReturn.getProperty("inn_gym_is_closed", "");
            string inn_gym_real_closed = itmReturn.getProperty("inn_gym_real_closed", "");

            //2020.09.21 繳費注意事項改為抓欄位(in_note_pay)
            string inn_pay_notice = itmReturn.getProperty("in_note_pay", "");
            //string inn_pay_notice = "報名資料填寫完成後，請勾選下方『我已確認報名資料填寫完成，並進行繳費』，點擊『線上繳費 GO』前往繳費。";

            string inn_cbx_contetnt = "我已確認報名資料填寫完成，並進行繳費";
            string inn_btn_contetnt1 = "請確認報名完成";
            string inn_btn_contetnt2 = "線上繳費 GO";
            string inn_closing_message = "結束報名並產生繳費單";
            string inn_closed_message = "已送出本次報名資訊並產生繳費單";

            string closed_mode = "item_show_0";
            string pay_box_class = "";
            string reg_box_class = "";
            string cfm_box_class = "";

            switch (in_pay_mode)
            {
                case "1": //報名，不產生繳費單
                    inn_pay_notice = "報名資料填寫完成後，請勾選下方『我已確認報名資料填寫完成』，點擊『確認送出』後完成報名。";
                    inn_cbx_contetnt = "我已確認報名資料填寫完成";
                    inn_btn_contetnt2 = "確認送出";
                    inn_closing_message = "結束報名";
                    inn_closed_message = "已送出本次報名資訊";
                    pay_box_class = "item_show_0";
                    break;

                case "2": //繳費，無金流模式
                case "3": //繳費，土銀模式
                case "4": //繳費，QrCode 模式
                    break;

                default:
                    break;
            }

            switch (inn_gym_is_closed)
            {
                case "0": //可報名
                    pay_box_class = "item_show_0";
                    break;

                case "1": //關閉
                    closed_mode = "";
                    reg_box_class = "item_show_0";
                    cfm_box_class = "item_show_0";
                    break;

                case "2": //admin
                    pay_box_class = "";
                    reg_box_class = "";
                    cfm_box_class = "";
                    break;

                default:
                    break;
            }

            if (inn_gym_real_closed != "1" && in_isfull == "1")
            {
                cfm_box_class = "";
            }

            // itmReturn.setProperty("closed_mode", closed_mode);
            // itmReturn.setProperty("reg_box", reg_box_class);
            // itmReturn.setProperty("pay_box", pay_box_class);
            // itmReturn.setProperty("cfm_box", cfm_box_class);

            // itmReturn.setProperty("inn_pay_notice", inn_pay_notice);
            // itmReturn.setProperty("inn_cbx_contetnt", inn_cbx_contetnt);
            // itmReturn.setProperty("inn_btn_contetnt1", inn_btn_contetnt1);
            // itmReturn.setProperty("inn_btn_contetnt2", inn_btn_contetnt2);
            itmReturn.setProperty("inn_closing_message", inn_closing_message);
            itmReturn.setProperty("inn_closed_message", inn_closed_message);
        }

        private void AppendLimit(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string meetingid, Item itmRst)
        {
            string sql = @"  SELECT in_property, IN_LABEL, in_admission, in_preparation 
                    FROM innovator.IN_Cla_MEETING_SURVEYS AS T1 WITH(NOLOCK)
                    JOIN  innovator.IN_CLA_SURVEY AS T2 WITH(NOLOCK)
                    ON T1.RELATED_ID = T2.id
                    JOIN innovator.IN_Cla_SURVEY_OPTION AS T3 WITH(NOLOCK)
                    ON T2.id = T3.SOURCE_ID
                    WHERE T1.SOURCE_ID ='{#meetingid}' and ISNULL(in_admission,-1) >= 0
                    ORDER BY in_property";
            sql = sql.Replace("{#meetingid}", meetingid);

            Item itmSurverys = inn.applySQL(sql);
            if (itmSurverys.getItemCount() > 0 && !itmSurverys.isError())
            {
                string in_property = "";
                string old_property = "";
                string list = "{"; //正取
                string rlist = "{"; //已報名正取
                string plist = "{"; //備取
                string rplist = "{"; //已報名正取
                string registeredQty = "";
                string regPrepQty = "";
                int c = 0;
                for (int i = 0; i < itmSurverys.getItemCount(); i++)
                {
                    Item itmSurvery = itmSurverys.getItemByIndex(i);
                    in_property = itmSurvery.getProperty("in_property", "");
                    if (in_property != old_property)
                    {
                        if (c != 0)
                        {
                            list = list.Substring(0, list.LastIndexOf(",")) + "},";
                            rlist = rlist.Substring(0, rlist.LastIndexOf(",")) + "},";
                            plist = plist.Substring(0, plist.LastIndexOf(",")) + "},";
                            rplist = rplist.Substring(0, rplist.LastIndexOf(",")) + "},";
                        }
                        list += "\"" + in_property + "\": {";
                        rlist += "\"" + in_property + "\": {";
                        plist += "\"" + in_property + "\": {";
                        rplist += "\"" + in_property + "\": {";
                        old_property = in_property;
                        c = 0;
                    }
                    string in_label = itmSurvery.getProperty("in_label", "");
                    string in_admission = itmSurvery.getProperty("in_admission", "");
                    string in_preparation = itmSurvery.getProperty("in_preparation", "");

                    registeredQty = AppendRegistered(CCO, strMethodName, inn, in_property, in_label, meetingid, itmRst, "official");
                    regPrepQty = AppendRegistered(CCO, strMethodName, inn, in_property, in_label, meetingid, itmRst, "waiting");
                    list += "\"" + in_label + "\":" + "\"" + in_admission + "\",";
                    rlist += "\"" + in_label + "\":" + "\"" + registeredQty + "\",";
                    plist += "\"" + in_label + "\":" + "\"" + in_preparation + "\",";
                    rplist += "\"" + in_label + "\":" + "\"" + regPrepQty + "\",";
                    c++;

                }

                if (c != 0)
                {
                    list = list.Substring(0, list.LastIndexOf(",")) + "}";
                    rlist = rlist.Substring(0, rlist.LastIndexOf(",")) + "}";
                    plist = plist.Substring(0, plist.LastIndexOf(",")) + "}";
                    rplist = rplist.Substring(0, rplist.LastIndexOf(",")) + "}";
                }

                list += "}";
                rlist += "}";
                plist += "}";
                rplist += "}";
                itmRst.setProperty("admissionField", list);
                itmRst.setProperty("registeredField", rlist);
                itmRst.setProperty("preparationField", plist);
                itmRst.setProperty("regPrepField", rplist);
            }
        }

        private void AppendLimitCurrent(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string meetingid, Item itmRst, string in_current_org)
        {
            string sql = @"  SELECT in_property, IN_LABEL, in_limit_current
                    FROM innovator.IN_Cla_MEETING_SURVEYS AS T1 WITH(NOLOCK)
                    JOIN  innovator.IN_CLA_SURVEY AS T2 WITH(NOLOCK)
                    ON T1.RELATED_ID = T2.id
                    JOIN innovator.IN_Cla_SURVEY_OPTION AS T3 WITH(NOLOCK)
                    ON T2.id = T3.SOURCE_ID
                    WHERE T1.SOURCE_ID ='{#meetingid}' and ISNULL(in_limit_current,-1) >= 0
                    ORDER BY in_property";
            sql = sql.Replace("{#meetingid}", meetingid);

            Item itmSurverys = inn.applySQL(sql);
            if (itmSurverys.getItemCount() > 0 && !itmSurverys.isError())
            {
                string in_property = "";
                string old_property = "";
                string list = "{";
                string rlist = "{";
                string registeredQty = "";
                int c = 0;
                for (int i = 0; i < itmSurverys.getItemCount(); i++)
                {
                    Item itmSurvery = itmSurverys.getItemByIndex(i);
                    in_property = itmSurvery.getProperty("in_property", "");
                    if (in_property != old_property)
                    {
                        if (c != 0)
                        {
                            list = list.Substring(0, list.LastIndexOf(",")) + "},";
                            rlist = rlist.Substring(0, rlist.LastIndexOf(",")) + "},";
                        }
                        list += "\"" + in_property + "\": {";
                        rlist += "\"" + in_property + "\": {";
                        old_property = in_property;
                        c = 0;
                    }
                    string in_label = itmSurvery.getProperty("in_label", "");
                    string in_limit_current = itmSurvery.getProperty("in_limit_current", "");

                    registeredQty = AppendRegisteredCurrent(CCO, strMethodName, inn, in_property, in_label, meetingid, itmRst, in_current_org);
                    list += "\"" + in_label + "\":" + "\"" + in_limit_current + "\",";
                    rlist += "\"" + in_label + "\":" + "\"" + registeredQty + "\",";
                    c++;

                }

                if (c != 0)
                {
                    list = list.Substring(0, list.LastIndexOf(",")) + "}";
                    rlist = rlist.Substring(0, rlist.LastIndexOf(",")) + "}";
                }

                list += "}";
                rlist += "}";
                itmRst.setProperty("currentField", list);
                itmRst.setProperty("regCurrField", rlist);
            }
        }


        private string AppendRegistered(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string in_property, string in_label, string meetingid, Item itmRst, string type)
        {
            string sql = @" SELECT COUNT(*) as registered_qty FROM in_cla_meeting_user WITH(NOLOCK)
                    WHERE SOURCE_ID ='{#meetingid}' AND IN_NOTE_STATE ='{#type}' AND {#in_property} = N'{#in_label}'";

            sql = sql.Replace("{#meetingid}", meetingid)
                    .Replace("{#in_property}", in_property)
                    .Replace("{#in_label}", in_label)
                    .Replace("{#type}", type);

            Item itmQty = inn.applySQL(sql);

            string result = "";
            if (itmQty.getItemCount() > 0 && !itmQty.isError())
            {
                result = itmQty.getProperty("registered_qty", "");
            }
            return result;
        }

        private string AppendRegisteredCurrent(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string in_property, string in_label, string meetingid, Item itmRst, string in_current_org)
        {
            string sql = @" SELECT COUNT(*) as registered_qty FROM in_cla_meeting_user WITH(NOLOCK)
                    WHERE SOURCE_ID ='{#meetingid}' AND in_current_org ='{#in_current_org}' AND IN_NOTE_STATE IN ('official','waiting') AND {#in_property} = N'{#in_label}'";

            sql = sql.Replace("{#meetingid}", meetingid)
                    .Replace("{#in_property}", in_property)
                    .Replace("{#in_label}", in_label)
                    .Replace("{#in_current_org}", in_current_org);

            Item itmQty = inn.applySQL(sql);

            string result = "";
            if (itmQty.getItemCount() > 0 && !itmQty.isError())
            {
                result = itmQty.getProperty("registered_qty", "");
            }
            return result;
        }

        private string GetDollarValue(string value)
        {
            int amount = 0;

            if (Int32.TryParse(value, out amount))
            {
                return amount.ToString("###,##0");
            }
            else
            {
                return value;
            }
        }
        //統計報名費用
        private void AppendAmount(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string in_creator_sno, Item itmPermit, Item itmReturn)
        {
            string meeting_id = itmReturn.getProperty("id", "");

            bool isMeetingAdmin = itmPermit.getProperty("isMeetingAdmin", "") == "1";
            bool isGymOwner = itmPermit.getProperty("isGymOwner", "") == "1";
            bool isGymAssistant = itmPermit.getProperty("isGymAssistant", "") == "1";
            string inn_gym_is_closed = itmPermit.getProperty("inn_gym_is_closed", "");

            if (!isMeetingAdmin && !isGymOwner && !isGymAssistant)
            {
                CCO.Utilities.WriteDebug(strMethodName, "無權限");
                return; //無權限
            }

            string group_condition = "AND in_creator_sno = N'" + in_creator_sno + "'";
            if (isMeetingAdmin || inn_gym_is_closed == "2")
            {
                group_condition = "";
            }

            List<Item> list = GetPayList(CCO, strMethodName, inn, meeting_id, group_condition);

            string in_unconfirm_exp = "0";
            string in_unpaid_exp = "0";
            string in_paid_exp = "0";
            string in_paid_real = "0";

            string in_un_total = "0";

            int unconfirm_exp = 0;
            int unpaid_exp = 0;

            for (int i = 0; i < list.Count; i++)
            {
                Item item = list[i];
                string pay_bool = item.getProperty("pay_bool", "");
                string subtotal_exp = item.getProperty("subtotal_exp", "");
                string subtotal_real = item.getProperty("subtotal_real", "");

                switch (pay_bool)
                {
                    case "未確認":
                        in_unconfirm_exp = subtotal_exp;
                        break;

                    case "未繳費":
                        in_unpaid_exp = subtotal_exp;
                        break;

                    case "已繳費":
                        in_paid_exp = subtotal_exp;
                        in_paid_real = subtotal_real;
                        break;

                    case "已取消":
                        break;

                    default:
                        break;
                }
            }

            Int32.TryParse(in_unconfirm_exp, out unconfirm_exp);
            Int32.TryParse(in_unpaid_exp, out unpaid_exp);
            in_un_total = (unconfirm_exp + unpaid_exp).ToString();

            //已繳(繳費單的實際收款金額)
            itmReturn.setProperty("in_paid", GetDollarValue(in_paid_real));

            //費用總計: 未產生繳費單(未產生的總額) + 已產生未繳費(繳費單的應繳金額)
            itmReturn.setProperty("in_unpaid", GetDollarValue(in_un_total));

            itmReturn.setProperty("in_unconfirmed", GetDollarValue(in_unconfirm_exp));
            itmReturn.setProperty("in_unpaid_exp", GetDollarValue(in_unpaid_exp));

        }
        private List<Item> GetPayList(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string meeting_id, string group_condition)
        {
            List<Item> list = new List<Item>();
            MergeAmounts(list, GetUserAmounts(CCO, strMethodName, inn, meeting_id, group_condition));
            MergeAmounts(list, GetPayAmounts(CCO, strMethodName, inn, meeting_id, group_condition));
            return list;
        }


        //取得晉段管理人員
        private bool GetDegreeAdmin(Innovator inn, string in_sno)
        {
            string sql = @"
                SELECT
                    t3.id
                    , t3.name
                    , t1.in_number
                FROM
                    [IDENTITY] t1 WITH(NOLOCK)
                INNER JOIN
                    [MEMBER] t2 WITH(NOLOCK)
                    ON t2.related_id = t1.id
                INNER JOIN
                    [IDENTITY] t3 WITH(NOLOCK)
                    ON t3.id = t2.source_id
                WHERE
                    t1.in_number = '{#in_sno}' 
        			AND t3.name = 'ACT_ASC_Degree'   
            ";

            sql = sql.Replace("{#in_sno}", in_sno);

            Item item = inn.applySQL(sql);

            if (item.getResult() != "")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private Item GetUserAmounts(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string meeting_id, string group_condition)
        {
            //未確認未繳費
            string sql = @"
SELECT
    source_id         AS 'meeting_id'
    , '未確認'        AS 'pay_bool'
    , SUM(in_expense) AS 'subtotal_exp'
    , SUM(in_expense) AS 'subtotal_real'
FROM
(
    SELECT
        DISTINCT source_id
        , in_creator_sno
        , in_current_org
        , in_expense
        , in_number
    FROM
        IN_Cla_MEETING_USER WITH(NOLOCK)
    WHERE
        source_id = '{#meeting_id}'
        {#group_condition}
        AND ISNULL(IN_PAYNUMBER, '') = ''
) t1
GROUP BY
    source_id
";

            sql = sql.Replace("{#meeting_id}", meeting_id)
                .Replace("{#group_condition}", group_condition);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql); 

            return inn.applySQL(sql);
        }

        private Item GetPayAmounts(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string meeting_id, string group_condition)
        {
            //已確認未繳費、已繳費(需扣除已退費)、已取消
            string sql = @"
SELECT
    in_cla_meeting                       AS 'meeting_id'
    , pay_bool
    , ISNULL(SUM(in_pay_amount_exp), 0)  AS 'subtotal_exp' 
    , SUM(ISNULL(in_pay_amount_exp, 0) - ISNULL(in_refund_amount, 0)) AS 'subtotal_real' 
FROM 
    IN_MEETING_PAY WITH(NOLOCK)
WHERE 
    IN_CLA_MEETING = '{#meeting_id}' 
    {#group_condition}
GROUP BY 
    in_cla_meeting, 
    pay_bool
";

            sql = sql.Replace("{#meeting_id}", meeting_id)
                .Replace("{#group_condition}", group_condition);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql); 

            return inn.applySQL(sql);

        }

        private void MergeAmounts(List<Item> list, Item items)
        {
            if (items.isError() || items.getItemCount() <= 0)
            {
                return;
            }

            for (int i = 0; i < items.getItemCount(); i++)
            {
                list.Add(items.getItemByIndex(i));
            }
        }

        //取得共同講師 (判斷現在的登入者是否為此課程的共同講師)
        private bool GetUserResumeListStatus(Innovator inn, string meeting_id, string strUserId)
        {
            string sql = @"
SELECT
    t2.id
FROM 
    In_Cla_Meeting_Resumelist t1 WITH(NOLOCK) 
INNER JOIN 
    IN_RESUME t2 WITH(NOLOCK) 
    ON t2.id = t1.related_id    
WHERE 
    t1.source_id = '{#meeting_id}' 
    AND t2.in_user_id = '{#in_user_id}';    
";

            sql = sql.Replace("{#meeting_id}", meeting_id)
                .Replace("{#in_user_id}", strUserId);

            Item item = inn.applySQL(sql);

            if (item.getResult() != "")
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        //單位是否有繳費單
        private string GetGymHasPay(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string meeting_id, string in_creator_sno)
        {
            string sql = @"
SELECT
    TOP 1 id
FROM
    IN_MEETING_PAY WITH(NOLOCK)
WHERE
    in_cla_meeting = '{#meeting_id}'
    AND in_creator_sno = '{#in_creator_sno}'
";

            sql = sql.Replace("{#meeting_id}", meeting_id)
                .Replace("{#in_creator_sno}", in_creator_sno);

            Item item = inn.applySQL(sql);

            if (item.isError() || item.getItemCount() <= 0 || item.getResult() == "")
            {
                return "0";
            }
            else
            {
                return "1";
            }
        }
        //附加報名問項連動
        private void AppendSurveyEvent(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string meeting_id, Item itmReturn)
        {
            string sql = @"
SELECT
    t2.id
    , t2.in_questions
    , t2.in_question_type
    , t2.in_property
    , t2.in_client_remove
    , t2.in_user_template
    , t3.in_value
    , t3.in_action
    , t3.in_target
    , t4.in_question_type AS 'in_target_type'
    , t4.in_client_remove AS 'in_target_remove'
    , t4.in_user_template AS 'in_target_body'
FROM
    IN_CLA_MEETING_SURVEYS t1 WITH(NOLOCK)
INNER JOIN
    IN_CLA_SURVEY t2 WITH(NOLOCK)
    ON t2.id = t1.related_id
INNER JOIN
    IN_CLA_SURVEY_EVENT t3 WITH(NOLOCK)
    ON t3.source_id = t1.related_id
INNER JOIN
    IN_CLA_SURVEY t4 WITH(NOLOCK)
    ON t4.id = t3.in_target
WHERE
    t1.source_id = '{#meeting_id}'
";

            sql = sql.Replace("{#meeting_id}", meeting_id);

            Item items = inn.applySQL(sql);

            if (items.isError() || items.getItemCount() <= 0)
            {
                return;
            }

            List<TEvent> list = new List<TEvent>();

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string id = item.getProperty("id", "");
                string in_questions = item.getProperty("in_questions", "");
                string in_question_type = item.getProperty("in_question_type", "");
                string in_property = item.getProperty("in_property", "");
                string in_value = item.getProperty("in_value", "");
                string in_action = item.getProperty("in_action", "");
                string in_client_remove = item.getProperty("in_client_remove", "");
                string in_user_template = item.getProperty("in_user_template", "");

                string in_target = item.getProperty("in_target", "");
                string in_target_type = item.getProperty("in_target_type", "");
                string in_target_remove = item.getProperty("in_target_remove", "");
                string in_target_body = item.getProperty("in_target_body", "");

                string inn_src_ctrl = GetFormCtrl(in_question_type);
                string inn_src_form = GetFormType(in_client_remove, in_user_template);

                string inn_tgt_ctrl = GetFormCtrl(in_target_type);
                string inn_tgt_form = GetFormType(in_target_remove, in_target_body);

                string key = id + "-" + in_value;

                if (inn_src_form == "" || inn_tgt_form == "")
                {
                    continue;
                }

                TEvent search = list.Find(x => x.key == key);

                if (search == null)
                {
                    search = new TEvent
                    {
                        key = key,
                        mufield = in_property,
                        value = in_value,
                        action = in_action,
                        ctrl = inn_src_ctrl,
                        form = inn_src_form,
                        targets = new List<TEventTarget>()
                    };
                    list.Add(search);
                }

                TEventTarget t = new TEventTarget
                {
                    name = in_target.Trim(),
                    ctrl = inn_tgt_ctrl,
                    form = inn_tgt_form,
                };

                search.targets.Add(t);

            }

            itmReturn.setProperty("survey_event_json", Newtonsoft.Json.JsonConvert.SerializeObject(list));
        }

        //取得控件類型
        private string GetFormCtrl(string in_question_type)
        {
            switch (in_question_type)
            {
                case "single_value_dropdown": return "select";

                default: return "input";
            }
        }

        //取得表頭表身類型
        private string GetFormType(string in_client_remove, string in_user_template)
        {
            if (in_client_remove == "1" && in_user_template == "1")
            {
                //表身欄位
                return "b";
            }
            else if (in_client_remove != "1")
            {
                //表頭欄位
                return "h";
            }
            return "";
        }

        private class TEvent
        {
            public string key { get; set; }
            public string mufield { get; set; }
            public string value { get; set; }
            public string action { get; set; }
            public string ctrl { get; set; }
            public string form { get; set; }

            public List<TEventTarget> targets { get; set; }
        }

        private class TEventTarget
        {
            public string name { get; set; }
            public string ctrl { get; set; }
            public string form { get; set; }
        }
    }
}