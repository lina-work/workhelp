﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class In_Cla_Meeting_Print : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
            目的: 報名清冊
            輸入: 
               - meeting id
            日誌:
               - 2020.09.16 改寫 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]In_Cla_Meeting_Print";

            string aml = "";
            string sql = "";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                in_sno_n = itmR.getProperty("in_sno_n", ""),

                mode = itmR.getProperty("mode", ""),
                verify = itmR.getProperty("verify", ""),
                hide_pay_amount = true,
            };

            //取得賽事資訊
            cfg.itmMeeting = cfg.inn.applySQL("SELECT * FROM IN_CLA_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");

            if (cfg.itmMeeting.isError() || cfg.itmMeeting.getResult() == "")
            {
                itmR.setProperty("permission_type", "1");
                itmR.setProperty("permission_message", "查無賽事資訊");
                return itmR;
            }

            cfg.in_title = cfg.itmMeeting.getProperty("in_title", "");
            cfg.in_meeting_type = cfg.itmMeeting.getProperty("in_meeting_type", "");
            cfg.in_pay_mode = cfg.itmMeeting.getProperty("in_pay_mode", "");
            cfg.in_verify_mode = cfg.itmMeeting.getProperty("in_verify_mode", "");
            cfg.in_allowed_reapply = cfg.itmMeeting.getProperty("in_allowed_reapply", "");
            cfg.in_date_s = cfg.itmMeeting.getProperty("in_date_s", "");

            //登入者資訊
            cfg.itmLogin = null;
            cfg.isAnonymous = cfg.in_sno_n != "";
            if (cfg.isAnonymous)
            {
                //盲報 (匿名報名)
                cfg.itmLogin = inn.applySQL("SELECT * FROM In_Cla_Meeting_User WITH(NOLOCK)"
                    + " WHERE source_id ='" + cfg.meeting_id + "'"
                    + " AND in_sno = '" + cfg.in_sno_n + "'");
                cfg.isMeetingAdmin = false;
            }
            else
            {
                cfg.itmLogin = cfg.inn.applySQL("SELECT * FROM IN_RESUME WITH(NOLOCK) WHERE in_user_id = '" + cfg.strUserId + "'");
            }

            if (cfg.itmLogin.isError() || cfg.itmLogin.getResult() == "")
            {
                itmR.setProperty("permission_type", "1");
                itmR.setProperty("permission_message", "您無權限瀏覽此頁面");
                return itmR;
            }

            cfg.login_sno = cfg.itmLogin.getProperty("in_sno", "");
            cfg.login_name = cfg.itmLogin.getProperty("in_name", "");
            cfg.login_member_type = cfg.itmLogin.getProperty("in_member_type", "");

            //取得登入者權限
            cfg.itmPermit = inn.applyMethod("In_CheckIdentity", "<method>" + strMethodName + "</method><code>ALL</code>");
            cfg.isMeetingAdmin = cfg.itmPermit.getProperty("isMeetingAdmin", "") == "1";
            cfg.isCommittee = cfg.itmPermit.getProperty("isCommittee", "") == "1";
            cfg.isGymOwner = cfg.itmPermit.getProperty("isGymOwner", "") == "1";
            cfg.isGymAssistant = cfg.itmPermit.getProperty("isGymAssistant", "") == "1";

            //是否為共同講師
            bool open = GetUserResumeListStatus(cfg.inn, cfg.meeting_id, cfg.strUserId);
            if (open) cfg.isMeetingAdmin = true;

            if (!cfg.isMeetingAdmin && cfg.in_meeting_type == "degree")
            {
                Item itmRoleResult = cfg.inn.applyMethod("In_Association_Role"
                    , "<in_sno>" + cfg.login_sno + "</in_sno>"
                    + "<idt_names>ACT_ASC_Degree</idt_names>");

                cfg.isMeetingAdmin = itmRoleResult.getProperty("inn_result", "") == "1";
            }

            //檢查登入者權限
            if (!cfg.isMeetingAdmin && !cfg.isCommittee && !cfg.isGymOwner && !cfg.isGymAssistant)
            {
                itmR.setProperty("permission_type", "1");
                itmR.setProperty("permission_message", "您無權限瀏覽此頁面");
                return itmR;
            }
            
            
            itmR.setProperty("in_meeting_type", cfg.in_meeting_type);

            //設定流程
            if (cfg.isMeetingAdmin)
            {
                itmR.setProperty("inn_admin_flow", "");
                itmR.setProperty("inn_committee_flow", "item_show_0");
                itmR.setProperty("inn_user_flow", "item_show_0");
                itmR.setProperty("inn_meeting_admin", "1");
                cfg.hide_pay_amount = false;
            }
            else if (cfg.isCommittee)
            {
                itmR.setProperty("inn_admin_flow", "item_show_0");
                itmR.setProperty("inn_committee_flow", "");
                itmR.setProperty("inn_user_flow", "item_show_0");
                itmR.setProperty("inn_meeting_admin", "1");
                cfg.hide_pay_amount = false;
            }
            else
            {
                itmR.setProperty("inn_admin_flow", "item_show_0");
                itmR.setProperty("inn_committee_flow", "item_show_0");
                itmR.setProperty("inn_user_flow", "");
                itmR.setProperty("inn_meeting_admin", "0");

            }

            if (cfg.in_meeting_type == "degree")
            {
                cfg.hide_pay_amount = true;
            }
            else
            {
                cfg.hide_pay_amount = false;
            }

            if (cfg.hide_pay_amount)
            {
                itmR.setProperty("no_pay", "1");
                itmR.setProperty("show_pay_amount", "item_show_0");
                itmR.setProperty("show_pay_col", "data-visible=\"false\"");
            }
            else
            {
                itmR.setProperty("no_pay", "");
                itmR.setProperty("show_pay_amount", "");
                itmR.setProperty("show_pay_col", "data-visible=\"true\"");
            }


            //取得單位結束報名狀態
            Item itmGymClosed = GetMeetingGymList(CCO, strMethodName, inn, cfg.meeting_id, cfg.login_sno);
            if (!itmGymClosed.isError() && itmGymClosed.getItemCount() > 0)
            {
                itmR.setProperty("inn_gym_is_closed", "1");
                itmR.setProperty("inn_gym_real_closed", "1");
            }

            //取得與會者資訊
            cfg.itmMUsers = GetMeetingUsers(cfg);
            //取得問卷資訊
            cfg.lstSurveys = GetMeetingSurveys(cfg);

            if (cfg.mode == "pdf")
            {
                exportPDF(cfg, itmR);
                return itmR;
            }


            TBox box = MapBox(cfg);
            //有未確認的報名人員
            itmR.setProperty("inn_gym_un_confirmed", box.has_non_confirmed ? "1" : "0");
            //已產生過繳費單
            itmR.setProperty("inn_gym_has_payment", box.has_payment ? "1" : "0");
            if (box.has_payment && !box.has_non_confirmed)
            {
                //有繳費單但沒有未確認的報名人員
                //視為關閉狀態
                itmR.setProperty("inn_gym_is_closed", "1");
            }

            //附加賽事資訊
            AppendMeetingInfo(cfg, itmR);

            //附加登入者資訊
            AppendLoginInfo(cfg, itmR);

            //附加統計按鈕狀態
            AppendBtnStatus(cfg, itmR);

            //附加注意事項
            AppendNotices(cfg, itmR);

            //報名清單
            AppendSummaryTable(cfg, box, itmR);
            AppendPlayerTables(cfg, box, itmR);

            return itmR;
        }

        //匯出PDF
        private void exportPDF(TConfig cfg, Item itmR)
        {
            string variable_name = "degree_check_path";
            Item itmXls = cfg.inn.applyMethod("In_Variable_File", "<in_name>" + variable_name + "</in_name>");
            string export_path = itmXls.getProperty("export_path", "").TrimEnd('\\');
            string template_path = itmXls.getProperty("template_path", "");

            string guid = System.Guid.NewGuid().ToString().Substring(0, 6).ToUpper()
                + "_" + DateTime.Now.ToString("yyyyMMddHHmmssfff");

            string pdfName = cfg.in_title + "_晉段審核名冊_" + guid;
            string pdfFile = export_path + "\\" + pdfName + ".pdf";

            Spire.Xls.Workbook book = new Spire.Xls.Workbook();
            book.LoadFromFile(template_path);
            Spire.Xls.Worksheet sheetTemplate = book.Worksheets[0];

            AppendSheetDegree(cfg, book, sheetTemplate);

            sheetTemplate.Remove();

            book.SaveToFile(pdfFile, Spire.Xls.FileFormat.PDF);

            itmR.setProperty("pdf_name", pdfName + ".pdf");
        }

        private void AppendSheetDegree(TConfig cfg, Spire.Xls.Workbook book, Spire.Xls.Worksheet sheetTemplate)
        {
            string old_l1 = "";
            string in_l1 = "";
            string manager_name = "";
            int page = 0;
            int j = 1;
            int k = 0;
            int all_total_exp = 0;


            for (int i = 0; i < cfg.itmMUsers.getItemCount(); i++)
            {
                Spire.Xls.Worksheet sheet;
                Item itmMUser = cfg.itmMUsers.getItemByIndex(i);
                Item itmMUserNext;
                string in_l1_next = "";
                if (i + 1 < cfg.itmMUsers.getItemCount())
                {
                    itmMUserNext = cfg.itmMUsers.getItemByIndex(i + 1);
                    in_l1_next = itmMUserNext.getProperty("in_l1", "");
                }


                in_l1 = itmMUser.getProperty("in_l1", "");
                if (in_l1 != old_l1 || j % 10 == 1)
                {
                    if (in_l1 != old_l1)
                    {
                        j = 1;
                    }
                    sheet = book.CreateEmptySheet();
                    page++;
                    sheet.CopyFrom(sheetTemplate);
                    sheet.Name = i.ToString() + "test";
                    sheet.PageSetup.FitToPagesWide = 1;
                    sheet.PageSetup.FitToPagesTall = 1;
                    sheet.PageSetup.TopMargin = 0.3;
                    sheet.PageSetup.LeftMargin = 0.5;
                    sheet.PageSetup.RightMargin = 0.5;
                    sheet.PageSetup.BottomMargin = 0.5;
                    book.ConverterSetting.SheetFitToPage = true;


                    manager_name = itmMUser.getProperty("committee_short_name", "");
                    DateTime sTime = Convert.ToDateTime(cfg.in_date_s);

                    sheet.Range["A1"].Text = manager_name + "申請參加" + cfg.in_title + "人員審核名冊";
                    sheet.Range["C2"].Text = in_l1;
                    sheet.Range["E2"].Text = itmMUser.getProperty("in_manager_area", "");
                    sheet.Range["G2"].Text = convertCDay(sTime, "content");
                    old_l1 = in_l1;

                }

                sheet = book.Worksheets[page];
                string in_name = itmMUser.getProperty("in_name", "");
                string in_en_name = itmMUser.getProperty("in_en_name", "");
                string in_current_org = itmMUser.getProperty("in_current_org", "");
                string in_stuff_c1 = itmMUser.getProperty("in_stuff_c1", "");
                int in_expense = Convert.ToInt32(itmMUser.getProperty("in_expense", ""));
                int total_expense = 0;
                string in_verify_memo = itmMUser.getProperty("in_verify_memo", "");
                string in_verify_result = itmMUser.getProperty("in_verify_result", "");
                string in_ass_ver_result = itmMUser.getProperty("in_ass_ver_result", "");
                string note = "";

                // if(in_verify_result =="0")
                // {
                //     note ="協會退回";
                // }
                // else
                // {
                //     if(in_ass_ver_result =="1")
                //     {
                //         note ="審核通過";
                //     }
                //     else
                //     {
                //         note ="待協會審核";
                //     }
                // }

                if (j % 10 == 0)
                {
                    k = 10;
                }
                else
                {
                    k = j % 10;
                }

                sheet.Range["A" + (5 + k)].Text = j.ToString();
                sheet.Range["B" + (5 + k)].Text = in_name;
                // sheet.Range["C"+(5 + k )].Text = in_en_name;
                sheet.Range["C" + (5 + k)].Text = in_current_org;
                sheet.Range["E" + (5 + k)].Text = in_stuff_c1;
                //lina 2022.01.19 要求隱藏金額
                sheet.Range["F" + (5 + k)].Text = "";//GetMoney(in_expense);
                total_expense = in_expense;
                //lina 2022.01.19 要求隱藏金額
                sheet.Range["i" + (5 + k)].Text = "";//GetMoney(total_expense); //小計
                sheet.Range["J" + (5 + k)].Text = in_verify_memo;
                sheet.Range["L" + (5 + k)].Text = note;
                all_total_exp += total_expense;
                if (i == cfg.itmMUsers.getItemCount() - 1 || in_l1_next != in_l1)
                {
                    if (j % 10 == 0)
                    {
                        sheet = book.CreateEmptySheet();
                        page++;
                        sheet.CopyFrom(sheetTemplate);
                        sheet.Name = i.ToString() + "test";
                        sheet.PageSetup.FitToPagesWide = 1;
                        sheet.PageSetup.FitToPagesTall = 1;
                        sheet.PageSetup.TopMargin = 0.3;
                        sheet.PageSetup.LeftMargin = 0.5;
                        sheet.PageSetup.RightMargin = 0.5;
                        sheet.PageSetup.BottomMargin = 0.5;
                        book.ConverterSetting.SheetFitToPage = true;


                        manager_name = itmMUser.getProperty("committee_short_name", "");
                        DateTime sTime = Convert.ToDateTime(cfg.in_date_s);

                        sheet.Range["A1"].Text = manager_name + "申請參加" + cfg.in_title + "人員審核名冊";
                        sheet.Range["C2"].Text = in_l1;
                        sheet.Range["E2"].Text = itmMUser.getProperty("in_manager_area", "");
                        sheet.Range["G2"].Text = convertCDay(sTime, "content");
                    }

                    sheet.Merge(sheet.Range["A" + (5 + 10)], sheet.Range["B" + (5 + 10)]);
                    sheet.Range["A" + (5 + 10)].Text = in_l1 + "參加人數總計";
                    sheet.Range["C" + (5 + 10)].Text = j.ToString() + "人";

                    sheet.Range["E" + (5 + 10)].Text = "晉段總金額";
                    sheet.Merge(sheet.Range["F" + (5 + 10)], sheet.Range["G" + (5 + 10)]);
                    //lina 2022.01.19 要求隱藏金額
                    sheet.Range["F" + (5 + 10)].Text = "";//GetMoney(all_total_exp);

                    sheet.Merge(sheet.Range["H" + (5 + 10)], sheet.Range["I" + (5 + 10)]);
                    sheet.Range["H" + (5 + 10)].Text = "縣市委員會" + Environment.NewLine + "承辦人簽章";
                    sheet.Range["A15"].RowHeight = 30;
                    all_total_exp = 0;
                }

                j++;
            }
        }

        private static string convertCDay(DateTime CDay, string type)
        {
            string y = (CDay.Year - 1911).ToString();
            string m = CDay.Month.ToString("00");
            string d = CDay.Day.ToString("00");
            string returnStr = "";
            if (type == "footer")
            {
                returnStr = " 中華民國　" + y + "　年　" + m + "　月　" + d + "　日";
            }
            else
            {
                returnStr = y + "." + m + "." + d;
            }

            return returnStr;
        }


        //取得共同講師 (判斷現在的登入者是否為此課程的共同講師)
        private bool GetUserResumeListStatus(Innovator inn, string meeting_id, string strUserId)
        {
            string sql = @"
        SELECT
            t2.id
        FROM 
            In_Cla_Meeting_Resumelist t1 WITH(NOLOCK) 
        INNER JOIN 
            IN_RESUME t2 WITH(NOLOCK) 
            ON t2.id = t1.related_id    
        WHERE 
            t1.source_id = '{#meeting_id}' 
            AND t2.in_user_id = '{#in_user_id}';    
        ";

            sql = sql.Replace("{#meeting_id}", meeting_id)
                .Replace("{#in_user_id}", strUserId);

            Item item = inn.applySQL(sql);

            if (item.getResult() != "")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //報名清單
        private void AppendSummaryTable(TConfig cfg, TBox box, Item itmReturn)
        {
            itmReturn.setProperty("head_2", "所屬單位");
            // itmReturn.setProperty("head_4", "隊職員數");
            itmReturn.setProperty("head_5", "報名人數");
            // itmReturn.setProperty("head_6", "參賽項目總計");
            itmReturn.setProperty("head_11", "報名費用");

            itmReturn.setProperty("hide_head_2", "");
            // itmReturn.setProperty("hide_head_4", "");
            itmReturn.setProperty("hide_head_5", "");
            // itmReturn.setProperty("hide_head_6", "");
            itmReturn.setProperty("hide_head_11", "");

            var sort_dictionary = box.Orgs.OrderBy(e => e.Value.name);

            foreach (var kv in sort_dictionary)
            {
                var org = kv.Value;

                Item item = cfg.inn.newItem("coverpage");

                item.setProperty("col_2", org.name);
                // item.setProperty("col_4", summary.StaffCount.ToString());
                item.setProperty("col_5", org.total_player.ToString());
                // item.setProperty("col_6", summary.TeamCount.ToString());
                item.setProperty("col_11", GetMoney(org.total_amount));

                itmReturn.addRelationship(item);
            }

            //報名總費用
            itmReturn.setProperty("total_amount", GetMoney(box.total_amount));
        }


        private void AppendPlayerTables(TConfig cfg, TBox box, Item itmReturn)
        {
            StringBuilder builder = new StringBuilder();

            foreach (var kv in box.Items)
            {
                var sheet = kv.Value;
                AppendPlayerTable(builder, cfg, box, sheet);
            }

            itmReturn.setProperty("inn_tables", builder.ToString());
        }

        private void AppendPlayerTable(StringBuilder builder, TConfig cfg, TBox box, TSheet sheet)
        {
            string player_count = "  (" + sheet.Players.Count.ToString() + ")";
            builder.Append("<div class=\"showsheet_" + sheet.id + " page-block\">");
            builder.Append("  <h2 class=\"text-center\">" + cfg.in_title + "</h2>");

            if (cfg.hide_pay_amount)
            {
                //隱藏金額
                builder.Append("  <h4 class=\"sheet-title\">" + sheet.name + player_count + " </h4>");
            }
            else
            {
                builder.Append("  <h4 class=\"sheet-title\">" + sheet.name + player_count
                    + "<span>報名費總計: " + GetMoney(sheet.total_amount) + "</span> </h4>");
            }

            builder.Append("  <table id=\"" + sheet.table_id + "\" class=\"table table-hover table-bordered table-rwd rwd\">");

            switch (cfg.in_meeting_type)
            {
                case "degree":
                    AppendDegreeTable(builder, cfg, box, sheet);
                    break;

                default:
                    AppendSeminarTable(builder, cfg, box, sheet);
                    break;
            }

            builder.Append("  </table>");
            // builder.Append("  <script>");
            // builder.Append("      $('#" + table.Name + "').bootstrapTable();");
            // builder.Append("      if($(window).width() <= 768) { $('#" + table.Name + "').bootstrapTable('toggleView');}");
            // builder.Append("  </script>");
            builder.Append("  <div style='height: 25px;'> </div>");//2020.10.06 補了間隔 讓其可以蓋章
            builder.Append("  <div class=\"pull-left signature\">" + box.sign0 + "</div>");
            builder.Append("  <div class=\"pull-left signature\">" + box.sign1 + "</div>");
            builder.Append("  <div class=\"pull-left signature\">" + box.sign2 + "</div>");
            builder.Append("  <div class=\"pull-left signature\">" + box.sign3 + "</div>");
            builder.Append("</div>");
        }

        //附加講習名單
        private void AppendSeminarTable(StringBuilder builder, TConfig cfg, TBox box, TSheet sheet)
        {
            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            int field_count = cfg.lstSurveys.Count;

            head.Append("<thead>");
            head.Append("  <tr>");

            for (int i = 0; i < field_count; i++)
            {
                Item itmSurvey = cfg.lstSurveys[i];
                string in_questions = itmSurvey.getProperty("in_questions", "");
                head.Append("    <th class=\"mailbox-subject search-field\" data-sortable=\"true\">" + in_questions + "</th>");
            }
            head.Append("    <th class=\"mailbox-subject search-field\" data-sortable=\"true\">審核狀態</th>");
            head.Append("    <th class=\"mailbox-subject search-field\" data-sortable=\"true\">報名費用</th>");
            head.Append("    <th class=\"mailbox-subject search-field\" data-sortable=\"true\">繳費狀態</th>");
            head.Append("  </tr>");
            head.Append("</thead>");

            body.Append("<tbody>");
            int no = 1;
            foreach (var kv in sheet.Players)
            {
                var player = kv.Value;
                var row = player.Value;

                body.Append("  <tr>");
                for (int i = 0; i < field_count; i++)
                {
                    Item itmSurvey = cfg.lstSurveys[i];
                    string in_property = itmSurvey.getProperty("in_property", "");
                    string in_question_type = itmSurvey.getProperty("in_question_type", "").ToLower(); ;

                    switch (in_question_type)
                    {
                        case "date":
                            body.Append("<td>" + GetDateTimeVal(row.getProperty(in_property, ""), "yyyy/MM/dd", hours: 8) + "</td>");
                            break;

                        default:
                            body.Append("<td>" + GetRowVal(row, in_property) + "</td>");
                            break;
                    }
                }

                string in_pay_display = GetPayStatusIcon(cfg.inn, row);
                string in_verify_display = GetVerifyDisplay(cfg.inn, row);

                body.Append("<td class=\"text-center\">" + in_verify_display + "</td>");
                body.Append("<td class=\"text-center\">" + GetMoney(player.amount) + "</td>");
                body.Append("<td class=\"text-center\">" + in_pay_display + "</td>");
                body.Append("</tr>");

                no++;
            }
            body.Append("</tbody>");

            builder.Append(head);
            builder.Append(body);
        }

        private string GetRowVal(Item row, string in_property)
        {
            string value = "";
            switch (in_property)
            {
                case "in_sno":
                    value = GetSidDisplay(row.getProperty(in_property, ""));
                    break;

                case "in_committee":
                    value = row.getProperty("committee_short_name", "");
                    break;


                default:
                    value = row.getProperty(in_property, "");
                    break;
            }
            return value;
        }

        //附加晉段申請名單
        private void AppendDegreeTable(StringBuilder builder, TConfig cfg, TBox box, TSheet sheet)
        {
            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            int field_count = cfg.lstSurveys.Count;

            head.Append("<thead>");
            head.Append("  <tr>");

            for (int i = 0; i < field_count; i++)
            {
                Item itmSurvey = cfg.lstSurveys[i];
                string in_questions = itmSurvey.getProperty("in_questions", "");
                head.Append("    <th class=\"mailbox-subject search-field\" data-sortable=\"true\">" + in_questions + "</th>");
            }
            head.Append("    <th class=\"mailbox-subject search-field\" data-sortable=\"true\">審核狀態</th>");
            head.Append("  </tr>");
            head.Append("</thead>");

            body.Append("<tbody>");
            int no = 1;
            foreach (var kv in sheet.Players)
            {
                var player = kv.Value;
                var row = player.Value;

                body.Append("  <tr>");
                for (int i = 0; i < field_count; i++)
                {
                    Item itmSurvey = cfg.lstSurveys[i];
                    string in_property = itmSurvey.getProperty("in_property", "");
                    string in_question_type = itmSurvey.getProperty("in_question_type", "").ToLower(); ;

                    switch (in_question_type)
                    {
                        case "date":
                            body.Append("<td>" + GetDateTimeVal(row.getProperty(in_property, ""), "yyyy/MM/dd", hours: 8) + "</td>");
                            break;

                        default:
                            if (in_property == "in_sno")
                            {
                                body.Append("<td>" + GetSidDisplay(row.getProperty(in_property, "")) + "</td>");
                            }
                            else
                            {
                                body.Append("<td>" + row.getProperty(in_property, "") + "</td>");
                            }
                            break;
                    }
                }

                string in_verify_display = GetVerifyDisplay(cfg.inn, row);
                body.Append("<td class=\"text-center\">" + in_verify_display + "</td>");
                body.Append("</tr>");

                no++;
            }
            body.Append("</tbody>");

            builder.Append(head);
            builder.Append(body);
        }

        private TBox MapBox(TConfig cfg)
        {
            TBox box = new TBox
            {
                has_non_confirmed = false,
                has_payment = false,
                total_player = 0,
                total_amount = 0,
                Orgs = new Dictionary<string, TSheet>(),
                Items = new Dictionary<string, TSheet>(),
                Players = new Dictionary<string, TPlayer>(),
            };

            Item itmPdf = cfg.inn.newItem();
            string in_pdf = cfg.itmMeeting.getProperty("in_pdf", "");
            if (in_pdf != "")
            {
                string[] pdf_params = in_pdf.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                if (pdf_params != null && pdf_params.Length > 0)
                {
                    for (int i = 0; i < pdf_params.Length; i++)
                    {
                        itmPdf.setProperty("sign" + i, pdf_params[i]);
                    }
                }
            }

            box.sign0 = itmPdf.getProperty("sign0", "");
            box.sign1 = itmPdf.getProperty("sign1", "");
            box.sign2 = itmPdf.getProperty("sign2", "");
            box.sign3 = itmPdf.getProperty("sign3", "");


            int count = cfg.itmMUsers.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item itmMUser = cfg.itmMUsers.getItemByIndex(i);
                string in_l1 = itmMUser.getProperty("in_l1", "");
                string in_l2 = itmMUser.getProperty("in_l2", "");
                string in_l3 = itmMUser.getProperty("in_l3", "");
                string in_index = itmMUser.getProperty("in_index", "");
                string in_creator_sno = itmMUser.getProperty("in_creator_sno", "");
                string in_current_org = itmMUser.getProperty("in_current_org", "");
                string in_paynumber = itmMUser.getProperty("in_paynumber", "");
                string pay_bool = itmMUser.getProperty("pay_bool", "");

                string in_expense = itmMUser.getProperty("in_expense", "0");
                int amount = GetIntVal(in_expense);

                string team_key = string.Join("-", new string[]
                {
                    in_l1,
                    in_l2,
                    in_l3,
                    in_index,
                    in_creator_sno,
                });

                if (!box.has_non_confirmed && pay_bool == "")
                {
                    box.has_non_confirmed = true;
                }

                if (!box.has_payment && in_paynumber != "")
                {
                    box.has_payment = true;
                }

                TPlayer player = new TPlayer
                {
                    team_key = team_key,
                    Value = itmMUser,
                    in_expense = in_expense,
                    amount = amount,
                };

                AddSheetPlayer(box.Orgs, in_current_org, player);
                AddSheetPlayer(box.Items, in_l1, player);

                if (!box.Players.ContainsKey(team_key))
                {
                    box.total_player++;
                    box.total_amount += player.amount;
                    box.Players.Add(team_key, player);
                }
            }

            return box;
        }

        private void AddSheetPlayer(Dictionary<string, TSheet> map, string key, TPlayer player)
        {
            TSheet sheet = null;
            if (map.ContainsKey(key))
            {
                sheet = map[key];
            }
            else
            {
                sheet = new TSheet
                {
                    id = (map.Count + 1).ToString(),
                    name = key,
                    total_player = 0,
                    total_amount = 0,
                    Players = new Dictionary<string, TPlayer>(),
                };

                sheet.table_id = "tb_" + sheet.id;

                map.Add(key, sheet);
            }

            if (!sheet.Players.ContainsKey(player.team_key))
            {
                sheet.total_player++;
                sheet.total_amount += player.amount;
                sheet.Players.Add(player.team_key, player);
            }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }

            public string meeting_id { get; set; }
            public string in_sno_n { get; set; }
            public string verify { get; set; }
            public string exe_type { get; set; }
            public string mode { get; set; }

            public Item itmPermit { get; set; }
            public Item itmLogin { get; set; }
            public Item itmMeeting { get; set; }
            public Item itmMUsers { get; set; }
            public List<Item> lstSurveys { get; set; }

            public bool isMeetingAdmin { get; set; }
            public bool isCommittee { get; set; }
            public bool isGymOwner { get; set; }
            public bool isGymAssistant { get; set; }
            public bool isAnonymous { get; set; }

            public string login_name { get; set; }
            public string login_sno { get; set; }
            public string login_member_type { get; set; }
            public string login_member_status { get; set; }

            /// <summary>
            /// 活動名稱
            /// </summary>
            public string in_title { get; set; }
            /// <summary>
            /// 活動類型
            /// </summary>
            public string in_meeting_type { get; set; }
            /// <summary>
            /// 付款方式
            /// </summary>
            public string in_pay_mode { get; set; }
            /// <summary>
            /// 審核方式
            /// </summary>
            public string in_verify_mode { get; set; }
            /// <summary>
            /// 是否允許再報
            /// </summary>
            public string in_allowed_reapply { get; set; }
            public string in_date_s { get; set; }

            //隱藏金額相關資訊
            public bool hide_pay_amount { get; set; }


        }

        private class TBox
        {
            public string sign0 { get; set; }
            public string sign1 { get; set; }
            public string sign2 { get; set; }
            public string sign3 { get; set; }

            public bool has_non_confirmed { get; set; }
            public bool has_payment { get; set; }
            public int total_player { get; set; }
            public int total_amount { get; set; }
            public Dictionary<string, TSheet> Orgs { get; set; }
            public Dictionary<string, TSheet> Items { get; set; }
            public Dictionary<string, TPlayer> Players { get; set; }
        }

        private class TSheet
        {
            public string id { get; set; }
            public string table_id { get; set; }
            public string name { get; set; }
            public int total_player { get; set; }
            public int total_amount { get; set; }
            public Dictionary<string, TPlayer> Players { get; set; }
        }

        private class TPlayer
        {
            public Item Value { get; set; }
            public string team_key { get; set; }
            public string in_expense { get; set; }
            public int amount { get; set; }
        }

        //附加統計按鈕狀態
        private void AppendBtnStatus(TConfig cfg, Item itmReturn)
        {
            string inn_gym_is_closed = itmReturn.getProperty("inn_gym_is_closed", "");
            string inn_gym_real_closed = itmReturn.getProperty("inn_gym_real_closed", "");

            string hide_confirm_box = "";
            string hide_payment_btn = "item_show_0";
            string hide_item_statistics = "item_show_0";
            string hide_org_statistics = "item_show_0";

            if (cfg.isMeetingAdmin)
            {
                hide_confirm_box = "";
                hide_payment_btn = "";

                switch (cfg.in_meeting_type)
                {
                    case "degree":
                    case "seminar":
                        break;

                    default:
                        if (cfg.exe_type == "item")
                        {
                            hide_org_statistics = "";
                        }
                        else
                        {
                            hide_item_statistics = "";
                        }
                        break;
                }
            }
            else
            {
                if (inn_gym_is_closed == "1")
                {
                    hide_confirm_box = "item_show_0";
                    if (cfg.in_pay_mode == "1")
                    {
                        hide_payment_btn = "item_show_0";
                    }
                    else
                    {
                        hide_payment_btn = "";
                    }
                }
                else
                {
                    hide_confirm_box = "";
                }
            }

            itmReturn.setProperty("hide_confirm_box", hide_confirm_box);
            itmReturn.setProperty("hide_payment_btn", hide_payment_btn);

            itmReturn.setProperty("hide_item_statistics", hide_item_statistics);
            itmReturn.setProperty("hide_org_statistics", hide_org_statistics);
        }

        //附加注意事項
        private void AppendNotices(TConfig cfg, Item itmReturn)
        {
            //有未確認的報名人員
            string inn_gym_un_confirmed = itmReturn.getProperty("inn_gym_un_confirmed", "");
            //已產生過繳費單
            string inn_gym_has_payment = itmReturn.getProperty("inn_gym_has_payment", "");

            string inn_cbx_contetnt = "我已確認報名資料無誤，並進行繳費";
            string inn_btn_contetnt1 = "請確認報名完成";
            string inn_btn_contetnt2 = "線上繳費 GO";
            string inn_closing_message = "結束報名並產生繳費單";
            string inn_closed_message = "已送出本次報名資訊並產生繳費單";

            switch (cfg.in_pay_mode)
            {
                case "1": //報名，不產生繳費單
                    inn_cbx_contetnt = "我已確認報名資料填寫完成";
                    inn_btn_contetnt2 = "確認送出";
                    inn_closing_message = "結束報名";
                    inn_closed_message = "已送出本次報名資訊";
                    break;

                case "2": //繳費，無金流模式
                case "3": //繳費，土銀模式
                case "4": //繳費，QrCode 模式
                    break;

                default:
                    break;
            }

            if (cfg.in_pay_mode != "1" && cfg.in_allowed_reapply == "1" && inn_gym_un_confirmed == "1" && inn_gym_has_payment == "1")
            {
                //允許再報，有產生過繳費單，又有未確認的報名資料
                inn_cbx_contetnt = "我已確認再報資料填寫完成";
                inn_btn_contetnt1 = "請確認再報完成";
                inn_btn_contetnt2 = "產生繳費單";
            }

            itmReturn.setProperty("inn_cbx_contetnt", inn_cbx_contetnt);
            itmReturn.setProperty("inn_btn_contetnt1", inn_btn_contetnt1);
            itmReturn.setProperty("inn_btn_contetnt2", inn_btn_contetnt2);
            itmReturn.setProperty("inn_closing_message", inn_closing_message);
            itmReturn.setProperty("inn_closed_message", inn_closed_message);
        }

        //附加賽事資訊
        private void AppendMeetingInfo(TConfig cfg, Item itmReturn)
        {
            Item itmMeeting = cfg.itmMeeting;

            itmReturn.setProperty("meeting_name", itmMeeting.getProperty("in_title", ""));
            itmReturn.setProperty("in_title", itmMeeting.getProperty("in_title", ""));
            itmReturn.setProperty("in_customer_tel", itmMeeting.getProperty("in_customer_tel", "").Replace("(請加上@)", ""));
            itmReturn.setProperty("in_url", itmMeeting.getProperty("in_url", ""));

            itmReturn.setProperty("in_register_url", itmMeeting.getProperty("in_register_url", ""));
            itmReturn.setProperty("inn_meeting_admin", cfg.isMeetingAdmin ? "1" : "0");
            itmReturn.setProperty("inn_excel_method", cfg.isMeetingAdmin ? "in_meeting_export_excel_admin" : "in_meeting_export_excel_gym");

            itmReturn.setProperty("in_pay_mode", itmMeeting.getProperty("in_pay_mode", ""));

            itmReturn.setProperty("in_bank_1", itmMeeting.getProperty("in_bank_1", ""));
            itmReturn.setProperty("in_bank_2", itmMeeting.getProperty("in_bank_2", ""));
            itmReturn.setProperty("in_bank_name", itmMeeting.getProperty("in_bank_name", ""));
            itmReturn.setProperty("in_bank_account", itmMeeting.getProperty("in_bank_account", ""));
            itmReturn.setProperty("in_bank_note", itmMeeting.getProperty("in_bank_note", ""));
        }

        //附加登入者資訊
        private void AppendLoginInfo(TConfig cfg, Item itmReturn)
        {
            Item itmResume = cfg.itmLogin;

            itmReturn.setProperty("in_sno", itmResume.getProperty("in_sno", ""));
            itmReturn.setProperty("in_group", itmResume.getProperty("in_group", ""));
            itmReturn.setProperty("in_name", itmResume.getProperty("in_name", ""));
            itmReturn.setProperty("in_tel", itmResume.getProperty("in_tel", ""));
            itmReturn.setProperty("in_add", itmResume.getProperty("in_add", ""));
            itmReturn.setProperty("total_amount", itmResume.getProperty("in_expense", ""));

            itmReturn.setProperty("print_datetime", DateTime.Now.ToString("yyyy/MM/dd HH:mm"));
            itmReturn.setProperty("print_user", itmResume.getProperty("in_name", ""));

            itmReturn.setProperty("inn_email", itmResume.getProperty("in_email", " "));
        }
        #region 存取資料

        //取得報名人員
        private Item GetMeetingUsers(TConfig cfg)
        {
            string filter_condition = "";

            if (!cfg.isMeetingAdmin)
            {
                switch (cfg.login_member_type)
                {
                    case "area_cmt": //縣市委員會
                        filter_condition = "AND t1.in_committee = N'" + cfg.login_name + "'";

                        break;

                    default:
                        if (cfg.isAnonymous)
                        {
                            filter_condition = "AND t1.in_sno = N'" + cfg.login_sno + "'";
                        }
                        else
                        {
                            filter_condition = "AND t1.in_creator_sno = N'" + cfg.login_sno + "'";
                        }
                        break;
                }
            }

            string orderFilter = "";
            if (cfg.in_meeting_type == "degree")
            {
                orderFilter = "t1.in_degree DESC, t1.in_birth";
            }
            else
            {
                orderFilter = " t1.in_l1, t1.in_degree DESC";
            }

            if (cfg.verify == "1")
            {
                filter_condition += " AND ISNULL(t1.in_verify_result, '') = '1'";
            }

            string sql = "";

            sql = @"
                SELECT
                    t1.*
                    , t2.in_tel AS 'in_creator_tel'
                    , t2.in_add AS 'in_creator_add'
                    , t3.pay_bool
                    , t3.in_pay_amount_real
                    , t3.in_pay_photo
                    , t3.in_return_mark
                    , t3.in_collection_agency
                    , ISNULL(t4.in_short_org, t1.in_committee) AS 'committee_short_name'
                    , '{#in_verify_mode}' AS 'in_verify_mode'
                    , t4.in_manager_area
                FROM
                    IN_CLA_MEETING_USER t1 WITH(NOLOCK)
                LEFT OUTER JOIN
                    IN_RESUME t2 WITH(NOLOCK)
                    ON t2.in_sno = t1.in_creator_sno
                LEFT OUTER JOIN
                    IN_MEETING_PAY t3 WITH(NOLOCK)
                    ON t3.in_cla_meeting = t1.source_id
                    AND t3.item_number = t1.in_paynumber
                LEFT OUTER JOIN
                    IN_RESUME t4 WITH(NOLOCK)
                    ON t4.in_name = t1.in_committee 
                    AND t4.in_member_type = 'area_cmt' 
                    AND t4.in_member_role = 'sys_9999' 
                WHERE
                    t1.source_id = '{#meeting_id}'
                    {#filter_condition}
                ORDER BY
                    {#orderFilter}
                    , t1.in_birth
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#filter_condition}", filter_condition)
                .Replace("{#in_verify_mode}", cfg.in_verify_mode)
                .Replace("{#orderFilter}", orderFilter);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        //取得報名問項
        private List<Item> GetMeetingSurveys(TConfig cfg)
        {
            string sql = "";

            sql = @"
                SELECT
                    t1.in_sort_order
                    , t1.in_excel_sort
                    , t2.in_questions
                    , t2.in_question_type
                    , t2.in_property
                FROM
                    IN_CLA_MEETING_SURVEYS t1 WITH(NOLOCK)
                INNER JOIN
                    IN_CLA_SURVEY t2 WITH(NOLOCK)
                    ON t2.id = t1.related_id
                WHERE
                    t1.source_id = '{#meeting_id}'
                    AND t1.in_surveytype = '1'
                    AND ISNULL(t2.in_property, '') <> ''
                    AND t1.in_excel_sort > 0
                ORDER BY
                    t1.in_excel_sort
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item itmSurveys = cfg.inn.applySQL(sql);

            List<Item> list = new List<Item>();
            int count = itmSurveys.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item itmSurvey = itmSurveys.getItemByIndex(i);
                string in_excel_sort = itmSurvey.getProperty("in_excel_sort", "");

                if (in_excel_sort == "" || in_excel_sort == "0")
                {
                    continue;
                }

                list.Add(itmSurvey);
            }
            return list;
        }

        //取得單位結束報名資料
        private Item GetMeetingGymList(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string meeting_id, string in_creator_sno)
        {
            string sql = @"
                SELECT
                    *
                FROM
                    IN_CLA_MEETING_GYMLIST WITH(NOLOCK)
                WHERE
                    source_id = '{#meeting_id}'
                    AND in_creator_sno = N'{#in_creator_sno}'
            ";

            sql = sql.Replace("{#meeting_id}", meeting_id)
                .Replace("{#in_creator_sno}", in_creator_sno);

            return inn.applySQL(sql);
        }

        #endregion 存取資料

        private string ClearValue(string value)
        {
            if (value.Contains("."))
            {
                return value.Split('.').Last().Trim();
            }
            return value.Trim();
        }

        //身分證字號(外顯)
        private string GetSidDisplay(string in_sno)
        {
            if (in_sno.Length == 10)
            {
                //10碼(身分證處理)
                string in_sno_1 = in_sno.Substring(0, 2);
                string in_sno_2 = in_sno.Substring(6, 4);
                return in_sno_1 + "****" + in_sno_2;//A1****6789
            }
            else
            {
                return "";
            }
        }

        private string GetPayStatusIcon(Innovator inn, Item item)
        {
            StringBuilder builder = new StringBuilder();

            string[] properties = new string[]
            {
                "in_paynumber", //繳費單號
                "in_pay_amount_real", //實際收款金額
                "in_pay_photo", //上傳繳費照片(繳費收據)
                "in_return_mark", //審核退回說明
                "in_collection_agency", //代收機構
            };

            foreach (string property in properties)
            {
                builder.Append("<" + property + ">" + item.getProperty(property, "") + "</" + property + ">");
            }
            //未確認
            builder.Append("<option>1</option>");

            Item itmResult = inn.applyMethod("In_PayType_Icon", builder.ToString());

            return itmResult.getProperty("in_pay_type", "");
        }

        private string GetVerifyDisplay(Innovator inn, Item item)
        {
            StringBuilder builder = new StringBuilder();

            string[] properties = new string[]
            {
                "in_verify_mode", //活動審核模式
                "in_verify_result", //委員會審核結果
                "in_verify_memo", //委員會審核說明
                "in_ass_ver_result", //協會審核結果
                "in_ass_ver_memo", //協會審核說明
            };

            foreach (string property in properties)
            {
                builder.Append("<" + property + ">" + item.getProperty(property, "") + "</" + property + ">");
            }

            Item itmResult = inn.applyMethod("In_Verify_Message", builder.ToString());

            return itmResult.getProperty("in_verify_display", "");
        }

        private string GetMoney(string value)
        {
            if (value == "" || value == "0") return "0";

            int result = GetIntVal(value);
            //0會被清空字串
            return result.ToString("###,###");
        }

        private string GetMoney(int value)
        {
            if (value == 0) return "0";
            //0會被清空字串
            return value.ToString("###,###");
        }

        private int GetIntVal(string value)
        {
            if (string.IsNullOrWhiteSpace(value)) return 0;
            int result = 0;
            Int32.TryParse(value, out result);
            return result;
        }

        private string GetDateTimeVal(string value, string format, int hours = 0)
        {
            if (value == "") return "";

            DateTime result = DateTime.MinValue;
            if (DateTime.TryParse(value, out result))
            {
                return result.AddHours(hours).ToString(format);
            }
            else
            {
                return "";
            }
        }
    }
}
