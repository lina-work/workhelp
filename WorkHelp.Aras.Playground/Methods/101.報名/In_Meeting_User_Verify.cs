﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class In_Meeting_User_Verify : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
            目的: 報名資料審核(賽事講習共用)
            日期: 2021-01-11 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]In_Meeting_User_Verify";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "input dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                muid = itmR.getProperty("muid", ""),
                resumeid = itmR.getProperty("resumeid", ""),
                scene = itmR.getProperty("scene", ""),
                mode = itmR.getProperty("mode", ""),
                role = itmR.getProperty("role", ""),
                muid_list = itmR.getProperty("users_id", ""),
                resumeid_list = itmR.getProperty("users_rid", ""),
                recover = itmR.getProperty("recover", ""),

                MtType = "IN_MEETING",
                MUserType = "IN_MEETING_USER",
                MCertType = "IN_MEETING_CERTIFICATE",
                MSvType = "IN_MEETING_SURVEYS",
                SvType = "IN_SURVEY",
            };

            if (cfg.mode == "cla")
            {
                cfg.MtType = "IN_CLA_MEETING";
                cfg.MUserType = "IN_CLA_MEETING_USER";
                cfg.MCertType = "IN_CLA_MEETING_CERTIFICATE";
                cfg.MSvType = "IN_CLA_MEETING_SURVEYS";
                cfg.SvType = "IN_CLA_SURVEY";
            }

            Item itmPermit = inn.applyMethod("In_CheckIdentity", "<method>" + strMethodName + "</method><code>ALL</code>");
            cfg.isMeetingAdmin = itmPermit.getProperty("isMeetingAdmin", "") == "1";
            cfg.isCommittee = itmPermit.getProperty("isCommittee", "") == "1";

            // //取得登入者權限繳角色
            // if (!cfg.isMeetingAdmin && !cfg.isCommittee)
            // {
            //     return itmR;
            // }

            //取得登入者資訊
            cfg.itmLoginResume = inn.applySQL("SELECT * FROM IN_RESUME WITH(NOLOCK) WHERE in_user_id = '" + cfg.strUserId + "'");
            if (IsError(cfg.itmLoginResume))
            {
                return itmR;
            }

            //取得活動資訊
            string cols = cfg.MtType == "IN_CLA_MEETING"
                ? "in_title, in_meeting_type, in_verify_mode, in_seminar_type, in_level"
                : "in_title, in_meeting_type, in_verify_mode, in_seminar_type";

            cfg.itmMeeting = inn.applySQL("SELECT " + cols + " FROM " + cfg.MtType + " WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            if (IsError(cfg.itmMeeting))
            {
                return itmR;
            }

            cfg.mt_title = cfg.itmMeeting.getProperty("in_title", "");
            cfg.in_meeting_type = cfg.itmMeeting.getProperty("in_meeting_type", "");
            cfg.in_verify_mode = cfg.itmMeeting.getProperty("in_verify_mode", "");
            cfg.in_seminar_type = cfg.itmMeeting.getProperty("in_seminar_type", "");

            //取得登入者權限繳角色
            if (!cfg.isMeetingAdmin && cfg.in_meeting_type == "degree")
            {
                Item itmRoleResult = inn.applyMethod("In_Association_Role"
                    , "<in_sno>" + cfg.itmLoginResume.getProperty("in_sno", "") + "</in_sno>"
                    + "<idt_names>ACT_ASC_Degree</idt_names>");

                cfg.isMeetingAdmin = itmRoleResult.getProperty("inn_result", "") == "1";
            }

            if (cfg.isCommittee && cfg.in_verify_mode != "3")
            {
                //該活動縣市委員會並無審核權限
                return itmR;
            }

            //批次審核
            if (cfg.scene == "verify" && cfg.muid_list != "")
            {
                string[] muid_arr = cfg.muid_list.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                foreach (string muid in muid_arr)
                {
                    Item itmMUser = GetMUser(cfg.CCO, cfg.strMethodName, cfg.inn, cfg.MUserType, muid);
                    Verify(cfg, muid, itmR);
                    //覆蓋新資料
                    CoverData(cfg, cfg.muid, itmMUser, itmR);
                }
            }
            else
            {
                //要檢視的與會者
                cfg.itmMUser = GetMUser(cfg.CCO, cfg.strMethodName, cfg.inn, cfg.MUserType, cfg.muid);
                if (IsError(cfg.itmMUser))
                {
                    return itmR;
                }

                //要檢視的個人履歷
                string muser_sno = cfg.itmMUser.getProperty("in_sno", "");
                if (muser_sno == "")
                {
                    return itmR;
                }

                cfg.itmResumeView = inn.applySQL("SELECT TOP 1 * FROM IN_RESUME WITH(NOLOCK) WHERE in_sno = '" + muser_sno + "'");
                if (IsError(cfg.itmResumeView))
                {
                    return itmR;
                }

                if (cfg.isCommittee)
                {
                    //要檢視的對象所屬委員會
                    string muser_committee = cfg.itmMUser.getProperty("in_committee", "");
                    string login_committee = cfg.itmLoginResume.getProperty("in_name", "");

                    if (muser_committee != login_committee)
                    {
                        return itmR;
                    }
                }

                if (cfg.scene == "")
                {
                    //查詢
                    Query(cfg, itmR);

                    //【還原為未審核】Button 是否呈現
                    itmR.setProperty("hide_recover", cfg.recover == "1" ? "" : "item_show_0");
                }
                else if (cfg.scene == "verify")
                {
                    //單一審核
                    Verify(cfg, cfg.muid, itmR);
                    //覆蓋新資料
                    CoverData(cfg, cfg.muid, cfg.itmMUser, itmR);
                }
                else if (cfg.scene == "recover")
                {
                    Recover(cfg, itmR);//復原
                }
            }

            SetExtendProperty(cfg, itmR);

            return itmR;
        }

        private void SetExtendProperty(TConfig cfg, Item itmReturn)
        {
            string sql = @"
                SELECT
                	t2.in_questions
                FROM
                	{#msv_type} t1 WITH(NOLOCK)
                INNER JOIN
                	{#sv_type} t2 WITH(NOLOCK)
                	ON t2.id = t1.related_id
                WHERE
                	t1.source_id = '{#meeting_id}'
                	AND t1.in_surveytype = '1'
                	AND t2.in_property = 'in_exe_a1'
            ";

            sql = sql.Replace("{#msv_type}", cfg.MSvType)
                .Replace("{#sv_type}", cfg.SvType)
                .Replace("{#meeting_id}", cfg.meeting_id);

            Item item = cfg.inn.applySQL(sql);

            if (item.isError() || item.getResult() == "")
            {
                itmReturn.setProperty("hide_mt_level", "item_show_0");
            }
            else
            {
                itmReturn.setProperty("hide_mt_level", "item_show_1");
                itmReturn.setProperty("mt_level_survey", item.getProperty("in_questions", "").Replace("您", ""));
            }

            string in_exe_a8 = itmReturn.getProperty("in_exe_a8", "");
            if (in_exe_a8.Contains("T00:00:00"))
            {
                in_exe_a8 = in_exe_a8.Replace("T00:00:00", "");
                itmReturn.setProperty("in_exe_a8", in_exe_a8);
            }
        }

        //覆蓋新資料
        private void CoverData(TConfig cfg, string muid, Item itmMUser, Item itmReturn)
        {
            string meeting_id = cfg.meeting_id;
            string in_meeting_type = cfg.in_meeting_type;
            string in_sno = itmMUser.getProperty("in_sno", "");
            string in_l1 = itmMUser.getProperty("in_l1", "");
            string in_l2 = itmMUser.getProperty("in_l2", "");

            //原審核狀態
            string old_verify_result = itmMUser.getProperty("in_ass_ver_result", "");
            //新審核狀態
            string new_verify_result = itmReturn.getProperty("in_verify_result", "");

            if (meeting_id != "A9F45C0EFD944ADAB1D7975D8C2E09C3")
            {
                //非各類申請，不往下處理
                return;
            }

            string sql = "SELECT * FROM IN_MEETING_USERLOG WITH(NOLOCK) WHERE source_id = '" + meeting_id + "' AND in_user = '" + muid + "'";
            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);
            Item itmMULogs = cfg.inn.applySQL(sql);
            if (itmMULogs.isError() || itmMULogs.getResult() == "")
            {
                //查無變更資料
                return;
            }

            //審核通過
            bool need_update = new_verify_result == "1";
            //將值復原 (審核通過變為不通過)
            bool need_recover = (new_verify_result == "0" && old_verify_result == "1");
            //須更新 Resume
            bool need_update_resume = in_l2.Contains("資料變更");
            bool is_seminar = in_l2.Contains("講習結業");

            if (need_update_resume)
            {
                string sql_cols = "";
                if (need_update)
                {
                    sql_cols = string.Join(", ", GetCols(itmMULogs, false));
                }
                else if (need_recover)
                {
                    sql_cols = string.Join(", ", GetCols(itmMULogs, true));
                }
                if (sql_cols != "")
                {

                    string sql_update = "UPDATE IN_RESUME SET " + sql_cols + " WHERE in_sno = '" + in_sno + "'";
                    //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql_update: " + sql_update);
                    Item itmSQL = cfg.inn.applySQL(sql_update);

                    if (itmSQL.isError())
                    {
                        throw new Exception("變更資料發生錯誤");
                    }
                }
            }

            if (is_seminar)
            {
                //處理裁判或教練講習資料
                if (need_update)
                {
                    //新增裁判或教練講習資料(含年度)
                    AppendResumeService(cfg, itmMUser, itmMULogs);
                }
                else if (need_recover)
                {
                    //移除裁判或教練講習資料(含年度)
                    RemoveResumeService(cfg, itmMUser, itmMULogs);
                }
            }
        }

        //移除裁判或教練講習資料
        private void RemoveResumeService(TConfig cfg, Item itmMUser, Item itmMULogs)
        {
            string sql = "";
            Dictionary<string, TLog> map = MapMUserLogs(itmMULogs);

            string muid = itmMUser.getProperty("id", "");
            string mu_sno = itmMUser.getProperty("in_sno", "");
            string in_l2 = itmMUser.getProperty("in_l2", "");

            string target_sno = MUserLogValue(map, "in_sno", mu_sno);

            sql = "SELECT * FROM IN_RESUME WITH(NOLOCK) WHERE in_sno = '" + target_sno + "'";
            Item itmResume = cfg.inn.applySQL(sql);
            if (itmResume.isError() || itmResume.getResult() == "")
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "查無講師資料: " + sql + ", muid: " + muid);
                return;
            }

            string resume_id = itmResume.getProperty("id", "");

            string typeName = "";
            string propName = "";
            string actTitle = "";
            string in_seminar_type = "";

            if (in_l2.Contains("裁判講習"))
            {
                typeName = "In_Resume_Referee";
                propName = "in_user";
                actTitle = "裁判服務";
                in_seminar_type = "referee";
            }
            else if (in_l2.Contains("教練講習"))
            {
                typeName = "In_Resume_Coach";
                propName = "in_user";
                actTitle = "教練服務";
                in_seminar_type = "coach";
            }

            sql = "SELECT * FROM " + typeName + " WITH(NOLOCK) WHERE source_id = '" + resume_id + "' AND " + propName + " = '" + muid + "'";
            Item itmService = cfg.inn.applySQL(sql);
            if (itmService.isError())
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, actTitle + "查無資料: " + sql);
                return;
            }

            Item itmDEL = null;

            string in_service = itmService.getProperty("id", "");

            sql = "DELETE FROM IN_RESUME_YEARS WHERE source_id = '" + resume_id + "' AND in_service = '" + in_service + "'";
            itmDEL = cfg.inn.applySQL(sql);
            if (itmDEL.isError())
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "年度記錄刪除失敗: " + sql);
                return;
            }

            sql = "DELETE FROM " + typeName + " WHERE id = '" + in_service + "'";
            itmDEL = cfg.inn.applySQL(sql);
            if (itmDEL.isError())
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, actTitle + "刪除失敗: " + sql);
                return;
            }

        }

        //新增裁判或教練講習資料
        private void AppendResumeService(TConfig cfg, Item itmMUser, Item itmMULogs)
        {
            string sql = "";

            Dictionary<string, TLog> map = MapMUserLogs(itmMULogs);

            string muid = itmMUser.getProperty("id", "");
            string mu_sno = itmMUser.getProperty("in_sno", "");
            string in_l2 = itmMUser.getProperty("in_l2", "");

            string target_sno = MUserLogValue(map, "in_sno", mu_sno);


            Item itmResume = cfg.inn.newItem("In_Resume", "get");
            itmResume.setProperty("in_sno", target_sno);
            itmResume = itmResume.apply();

            if (itmResume.isError() || itmResume.getResult() == "")
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "查無講師資料: " + sql + ", muid: " + muid);
                return;
            }

            string resume_id = itmResume.getProperty("id", "");

            string typeName = "";
            string propName = "";
            string actTitle = "";
            string in_seminar_type = "";

            if (in_l2.Contains("裁判講習"))
            {
                typeName = "In_Resume_Referee";
                propName = "in_user";
                actTitle = "裁判服務";
                in_seminar_type = "referee";
            }
            else if (in_l2.Contains("教練講習"))
            {
                typeName = "In_Resume_Coach";
                propName = "in_user";
                actTitle = "教練服務";
                in_seminar_type = "coach";
            }


            string in_date = System.DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
            string in_west_year = TwYearToWest(map);
            string in_level = MUserLogValue(map, "in_certificate_level");
            string in_echelon = MUserLogValue(map, "in_certificate_echelon");
            string in_certificate_no = MUserLogValue(map, "in_certificate_no");
            string in_has_muser = "1";

            //服務
            string str_where = "source_id = '" + resume_id + "' AND " + propName + " = '" + muid + "'";

            Item itmService = cfg.inn.newItem(typeName, "merge");
            itmService.setAttribute("where", str_where);
            itmService.setProperty("source_id", resume_id);
            itmService.setProperty(propName, muid);
            itmService.setProperty("in_date", in_date);
            itmService.setProperty("in_year", in_west_year);
            itmService.setProperty("in_level", in_level);
            itmService.setProperty("in_echelon", in_echelon);
            itmService.setProperty("in_certificate_no", in_certificate_no);
            itmService.setProperty("in_note", in_l2);
            itmService.setProperty("in_has_muser", in_has_muser);
            itmService.setRelatedItem(itmResume);
            itmService = itmService.apply();

            if (itmService.isError())
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, actTitle + "存檔失敗: " + itmService.dom.InnerXml);
                return;
            }

            string in_service = itmService.getProperty("id", "");

            //年度
            str_where = "source_id = '" + resume_id + "' AND in_service = '" + in_service + "'";

            Item itmYear = cfg.inn.newItem("In_Resume_Years", "merge");
            itmYear.setAttribute("where", str_where);
            itmYear.setProperty("source_id", resume_id);
            itmYear.setProperty("in_service", in_service);
            itmYear.setProperty("in_type", in_seminar_type);
            itmYear.setProperty("in_date", in_date);
            itmYear.setProperty("in_year", in_west_year);
            itmYear.setProperty("in_note", "各類申請");
            itmYear.setRelatedItem(itmResume);
            itmYear = itmYear.apply();

            if (itmYear.isError())
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "年度記錄存檔失敗: " + itmYear.dom.InnerXml);
                return;
            }

            if (in_l2 == "國際裁判講習結業證書上傳")
            {
                if (in_certificate_no != "")
                {
                    string sql_update = "UPDATE IN_RESUME SET in_gl_referee_id = '" + in_certificate_no + "' WHERE id = '" + resume_id + "'";
                    Item itmSQL = cfg.inn.applySQL(sql_update);
                    if (itmSQL.isError())
                    {
                        cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "更新國際裁判證號: " + itmSQL.dom.InnerXml);
                    }
                }
            }

        }

        private string TwYearToWest(Dictionary<string, TLog> map)
        {
            string property = "in_tw_year";
            if (!map.ContainsKey(property)) return "";

            var in_tw_year = map[property].new_value;
            var tw_year = GetIntVal(in_tw_year);
            var in_west_year = (tw_year + 1911).ToString();
            return in_west_year;
        }

        private string MUserLogValue(Dictionary<string, TLog> map, string property, string def = "")
        {
            return map.ContainsKey(property) ? map[property].new_value : def;
        }

        private Dictionary<string, TLog> MapMUserLogs(Item itmMULogs)
        {
            Dictionary<string, TLog> map = new Dictionary<string, TLog>();

            int count = itmMULogs.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item itmMULog = itmMULogs.getItemByIndex(i);
                string in_property = itmMULog.getProperty("in_property", "");
                string new_value = itmMULog.getProperty("new_value", "");
                string old_value = itmMULog.getProperty("old_value", "");
                if (!map.ContainsKey(in_property))
                {
                    map.Add(in_property, new TLog
                    {
                        in_property = in_property,
                        new_value = new_value,
                        old_value = old_value,
                        Value = itmMULog,
                    });
                }
            }

            return map;
        }

        private class TLog
        {
            public string in_property { get; set; }
            public string new_value { get; set; }
            public string old_value { get; set; }
            public Item Value { get; set; }
        }

        private List<string> GetCols(Item itmMULogs, bool is_recover)
        {
            List<string> cols = new List<string>();

            int count = itmMULogs.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item itmMULog = itmMULogs.getItemByIndex(i);
                string in_property = itmMULog.getProperty("in_property", "");
                string old_value = itmMULog.getProperty("old_value", "");
                string new_value = itmMULog.getProperty("new_value", "");

                switch (in_property)
                {
                    case "in_birth":
                        old_value = GetDateTimeByFormat(old_value, "yyyy-MM-ddTHH:mm:ss");
                        new_value = GetDateTimeByFormat(new_value, "yyyy-MM-ddTHH:mm:ss");
                        break;
                }

                if (is_recover)
                {
                    cols.Add(in_property + " = N'" + old_value + "'");
                }
                else
                {
                    cols.Add(in_property + " = N'" + new_value + "'");
                }
            }

            return cols;
        }

        //還原為未審核
        private void Recover(TConfig cfg, Item itmReturn)
        {
            string sql = "";

            string muid = cfg.itmMUser.getProperty("id", "");
            Item itmE = cfg.inn.newItem(cfg.MUserType, "edit");
            //協會與委員會審核切開
            if (cfg.role == "ass" || cfg.role == "seminar" || cfg.role == "degree")
            {
                itmE.setAttribute("where", "id ='" + muid + "'");
                itmE.setProperty("in_ass_ver_result", "");
                itmE.setProperty("in_ass_ver_memo", "");
                itmE.setProperty("in_ass_ver_time", "");
                itmE.setProperty("in_ass_ver_by_id", "");
            }
            else
            {
                itmE.setAttribute("where", "id ='" + muid + "'");
                itmE.setProperty("in_verify_result", "");
                itmE.setProperty("in_verify_memo", "");
                itmE.setProperty("in_verify_time", "");
                itmE.setProperty("in_verify_by_id", "");
            }

            Item itmSQL = itmE.apply();

            if (itmSQL.isError())
            {
                throw new Exception("復原失敗");
            }
        }

        //審核
        private void Verify(TConfig cfg, string muid, Item itmReturn)
        {
            string in_verify_result = itmReturn.getProperty("in_verify_result", "");
            string in_verify_memo = itmReturn.getProperty("in_verify_memo", "").Trim();
            string in_verify_time = System.DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
            string in_verify_by_id = cfg.inn.getUserAliases();

            if (in_verify_result == "")
            {
                throw new Exception("請填寫審核結果");
            }
            else if (in_verify_result == "0" && in_verify_memo == "")
            {
                throw new Exception("請填寫不通過說明");
            }

            Item itmE = cfg.inn.newItem(cfg.MUserType, "edit");

            //協會與委員會審核切開
            if (cfg.role == "ass" || cfg.role == "seminar" || cfg.role == "degree")
            {
                itmE.setAttribute("where", "id ='" + muid + "'");
                itmE.setProperty("in_ass_ver_result", in_verify_result);
                itmE.setProperty("in_ass_ver_memo", in_verify_memo);
                itmE.setProperty("in_ass_ver_time", in_verify_time);
                itmE.setProperty("in_ass_ver_by_id", in_verify_by_id);
                itmE.setProperty("in_is_changed", "0");
                if (in_verify_result == "0")
                {
                    itmE.setProperty("in_cert_valid", "0");
                }
            }
            else
            {
                itmE.setAttribute("where", "id ='" + muid + "'");
                itmE.setProperty("in_verify_result", in_verify_result);
                itmE.setProperty("in_verify_memo", in_verify_memo);
                itmE.setProperty("in_verify_time", in_verify_time);
                itmE.setProperty("in_verify_by_id", in_verify_by_id);
                if (in_verify_result == "0")
                {
                    itmE.setProperty("in_cert_valid", "0");
                }
            }

            Item itmUpd = itmE.apply();

            if (itmUpd.isError())
            {
                throw new Exception("審核失敗");
            }
        }

        //查詢
        private void Query(TConfig cfg, Item itmReturn)
        {
            //複製全部 property
            var nodes = cfg.itmMUser.node.ChildNodes;
            foreach (XmlNode node in nodes)
            {
                string property = node.Name;
                itmReturn.setProperty(property, cfg.itmMUser.getProperty(property, ""));
            }

            //需改變格式的欄位
            List<TField> fields = new List<TField>();
            fields.Add(new TField { property = "in_birth", format = "yyyy年MM月dd日", need_clear = true, ClearValue = Clear1900 });
            fields.Add(new TField { property = "in_email", need_clear = true, ClearValue = ClearEmail });
            fields.Add(new TField { property = "in_exe_a2", format = "yyyy年MM月dd日", need_clear = true, ClearValue = Clear1900 });
            fields.Add(new TField { property = "in_verify_time", format = "yyyy年MM月dd日", need_clear = true, ClearValue = Clear1900 });
            fields.Add(new TField { property = "in_date", format = "yyyy年MM月dd日", need_clear = true, ClearValue = Clear1900 });

            if (cfg.role == "seminar")
            {
                itmReturn.setProperty("resume_title", "講習人員審核");
                itmReturn.setProperty("tab_activity", "item_show_0");
                itmReturn.setProperty("tab_pro_re", "item_show_0");
                itmReturn.setProperty("tab_seminar", "active item_show_1");
                itmReturn.setProperty("tab_seminar_re", "active item_show_1");
                itmReturn.setProperty("tab_cer_re", "item_show_1");
                itmReturn.setProperty("cont_seminar", "active");
                itmReturn.setProperty("in_verify_memo", cfg.itmMUser.getProperty("in_ass_ver_memo", ""));
            }
            else
            {
                itmReturn.setProperty("resume_title", "晉段人員審核");
                itmReturn.setProperty("tab_activity", "active item_show_1");
                itmReturn.setProperty("tab_pro_re", "item_show_1");
                itmReturn.setProperty("tab_seminar", "item_show_0");
                itmReturn.setProperty("tab_seminar_re", "item_show_0");
                itmReturn.setProperty("tab_cer_re", "item_show_1");
                itmReturn.setProperty("cont_act", "active");
            }

            //複寫 value
            OverrideValue(itmReturn, cfg.itmMUser, fields);

            itmReturn.setProperty("inn_verify_result", GetVerifyResult(cfg.itmMUser, cfg.role));

            if (cfg.role == "ass" || cfg.role == "seminar" || cfg.role == "degree")
            {
                itmReturn.setProperty("in_verify_memo", cfg.itmMUser.getProperty("in_ass_ver_memo", ""));
            }

            //晉段記錄
            AppendResumePromotion(cfg, cfg.itmResumeView.getProperty("id", ""), itmReturn);

            //證照列表
            AppendResumeCertificate(cfg, cfg.itmResumeView.getProperty("id", ""), itmReturn);

            //講習記錄列表
            if (cfg.in_meeting_type == "seminar")
            {
                //講習記錄列表
                AppendResumeSeminar(cfg, cfg.itmResumeView.getProperty("id", ""), itmReturn);
            }


            itmReturn.setProperty("resume_degree_display", GetDegreeDisplay(cfg.itmResumeView.getProperty("in_degree", "")));
            itmReturn.setProperty("resume_degree_id", cfg.itmResumeView.getProperty("in_degree_id", ""));
            itmReturn.setProperty("resume_gl_degree_id", cfg.itmResumeView.getProperty("in_gl_degree_id", ""));

            //證件驗證結果
            AppendCertCheck(cfg, cfg.itmMUser, cfg.itmResumeView.getProperty("id", ""), itmReturn);
        }

        private string GetVerifyResult(Item itmMUser, string role)
        {
            string in_verify_result = "";
            if (role == "ass" || role == "seminar")
            {
                in_verify_result = itmMUser.getProperty("in_ass_ver_result", "");
            }
            else
            {
                in_verify_result = itmMUser.getProperty("in_verify_result", "");
            }
            if (in_verify_result == "")
            {
                return "";
            }
            else if (in_verify_result == "1")
            {
                return "(通過)";
            }
            else
            {
                return "(不通過)";
            }
        }

        private string GetDegreeDisplay(string value)
        {
            switch (value)
            {
                case "": return "無段位";
                case "10": return "白帶";
                case "30": return "黃帶";
                case "40": return "黃綠帶";
                case "50": return "綠帶";
                case "60": return "綠藍帶";
                case "70": return "藍帶";
                case "80": return "藍紅帶";
                case "90": return "紅帶";
                case "100": return "紅黑帶";
                case "150": return "黑帶一品";
                case "250": return "黑帶二品";
                case "350": return "黑帶三品";
                case "1000": return "壹段";
                case "2000": return "貳段";
                case "3000": return "參段";
                case "4000": return "肆段";
                case "5000": return "伍段";
                case "6000": return "陸段";
                case "7000": return "柒段";
                case "8000": return "捌段";
                case "9000": return "玖段";

                case "11000": return "11";
                case "12000": return "12";
                case "13000": return "13";
                default: return "";
            }
        }

        //下拉選單
        private void AppendMenu(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string list_name, string type_name, Item itmReturn)
        {
            Item items = GetValues(CCO, strMethodName, inn, list_name: list_name);
            AppendMenu(CCO, strMethodName, inn, items, type_name, itmReturn);
        }

        //下拉選單
        private void AppendMenu(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item items, string type_name, Item itmReturn)
        {
            if (items.isError() || items.getItemCount() <= 0)
            {
                return;
            }

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);

                item.setType(type_name);
                item.setProperty("inn_label", item.getProperty("label", ""));
                item.setProperty("inn_value", item.getProperty("value", ""));
                itmReturn.addRelationship(item);
            }
        }

        //會費繳納現況
        private void AppendResumePay(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string resume_id, Item itmReturn)
        {
            List<TField> fields = new List<TField>();
            fields.Add(new TField { property = "id" });
            fields.Add(new TField { property = "in_date", format = "yyyy年MM月dd日", need_clear = true, ClearValue = Clear1900 });
            fields.Add(new TField { property = "in_year", need_clear = true, ClearValue = MapChineseYear });
            fields.Add(new TField { property = "in_amount", need_clear = true, ClearValue = ClearMoney });
            fields.Add(new TField { property = "in_note" });

            Item items = GetResumePays(CCO, strMethodName, inn, resume_id);

            if (items.isError() || items.getItemCount() <= 0)
            {
                return;
            }

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item source = items.getItemByIndex(i);

                Item item = inn.newItem();
                item.setType("inn_pay");
                OverrideValue(item, source, fields);
                itmReturn.addRelationship(item);
            }
        }

        //晉段記錄
        private void AppendResumePromotion(TConfig cfg, string resume_id, Item itmReturn)
        {
            List<TField> fields = new List<TField>();
            fields.Add(new TField { property = "id" });
            fields.Add(new TField { property = "in_shift_no" });
            fields.Add(new TField { property = "in_manager_area" });
            fields.Add(new TField { property = "in_apply_degree", need_clear = true, ClearValue = GetDegreeDisplay });
            fields.Add(new TField { property = "in_date", format = "yyyy年MM月dd日", need_clear = true, ClearValue = Clear1900 });
            fields.Add(new TField { property = "in_is_global", need_clear = true, ClearValue = GetYN });
            fields.Add(new TField { property = "in_note" });

            Item items = GetResumePromotions(cfg.CCO, cfg.strMethodName, cfg.inn, resume_id);

            if (items.isError() || items.getItemCount() <= 0)
            {
                return;
            }

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item source = items.getItemByIndex(i);

                Item item = cfg.inn.newItem();
                item.setType("inn_promotion");
                OverrideValue(item, source, fields);
                itmReturn.addRelationship(item);
            }
        }

        //證照列表
        private void AppendResumeCertificate(TConfig cfg, string resume_id, Item itmReturn)
        {
            List<TField> fields = new List<TField>();
            fields.Add(new TField { property = "id" });
            fields.Add(new TField { property = "in_type" });
            fields.Add(new TField { property = "in_certificate_no" });
            fields.Add(new TField { property = "in_certificate_name" });
            fields.Add(new TField { property = "hide_rotate" });

            var date_properties = new List<string> { "in_effective_start", "in_effective" };
            fields.Add(new TField { property = "inn_effective", format = "yyyy年MM月dd日", need_clear = true, ClearValue = DateRange, properties = date_properties });

            var file_properties = new List<string> { "in_file1", "in_file2" };
            fields.Add(new TField { property = "inn_files", need_clear = true, ClearValue = DownloadFiles, properties = file_properties });

            Item itmRCerts = GetResumeCertificates(cfg, resume_id);
            Item itmMCerts = GetMeetingCertificates(cfg);

            List<Item> cert_list = MapCerts(cfg, itmMCerts, itmRCerts, itmReturn);

            if (cert_list == null || cert_list.Count == 0)
            {
                return;
            }

            int count = cert_list.Count;

            for (int i = 0; i < count; i++)
            {
                Item source = cert_list[i];

                Item item = cfg.inn.newItem();
                item.setType("inn_certificate");
                OverrideValue(item, source, fields);
                itmReturn.addRelationship(item);
            }
        }

        //講習記錄列表
        private void AppendResumeSeminar(TConfig cfg, string resume_id, Item itmReturn)
        {
            List<TField> fields = new List<TField>();
            fields.Add(new TField { property = "id" });
            fields.Add(new TField { property = "in_year", need_clear = true, ClearValue = MapChineseYear });
            fields.Add(new TField { property = "in_level" });
            fields.Add(new TField { property = "in_echelon" });
            fields.Add(new TField { property = "in_know_score" });
            fields.Add(new TField { property = "in_tech_score" });
            fields.Add(new TField { property = "in_evaluation" });
            fields.Add(new TField { property = "in_note" });
            fields.Add(new TField { property = "in_certificate", need_clear = true, ClearValue = CheckBox });

            string item_type = "";
            string in_type = "";
            switch (cfg.in_seminar_type)
            {
                case "coach":
                    item_type = "In_Resume_Coach";
                    in_type = "coach";
                    break;

                case "coach_enr":
                    item_type = "In_Resume_Coach";
                    in_type = "coach";
                    break;

                case "referee":
                    item_type = "In_Resume_Referee";
                    in_type = "referee";
                    break;

                case "poomsae":
                    item_type = "In_Resume_Poomsae";
                    in_type = "poomsae";
                    break;

                default:
                    if (cfg.mt_title.Contains("裁判"))
                    {
                        item_type = "In_Resume_Referee";
                        in_type = "referee";
                    }
                    else
                    {
                        item_type = "In_Resume_Coach";
                        in_type = "coach";
                    }
                    break;
            }

            Item items = null;
            int count = 0;

            string sql = @"SELECT * FROM {#item_type} WITH (NOLOCK) WHERE source_id = '{#resume_id}' ORDER BY in_year DESC, in_date DESC";
            sql = sql.Replace("{#resume_id}", resume_id).Replace("{#item_type}", item_type);
            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);
            items = cfg.inn.applySQL(sql);
            count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item source = items.getItemByIndex(i);

                Item item = cfg.inn.newItem();
                item.setType("inn_seminar");
                OverrideValue(item, source, fields);
                itmReturn.addRelationship(item);
            }

            List<string> list = new List<string>();

            sql = @"SELECT * FROM IN_RESUME_YEARS WITH (NOLOCK) WHERE source_id = '{#resume_id}' AND in_type = '{#in_type}' ORDER BY in_year, in_date";
            sql = sql.Replace("{#resume_id}", resume_id).Replace("{#in_type}", in_type);
            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);
            items = cfg.inn.applySQL(sql);
            count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item source = items.getItemByIndex(i);
                string in_year = source.getProperty("in_year", "");
                string in_tw_year = (GetIntVal(in_year) - 1911).ToString();
                list.Add(in_tw_year);
            }

            itmReturn.setProperty("inn_seminar_years", GetYearTable(list));
        }

        private string GetYearTable(List<string> source)
        {
            List<string> list = FilterYearList(source, 5);

            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            head.Append("<thead>");
            head.Append("  <tr>");

            body.Append("<tbody>");
            body.Append("  <tr>");

            for (int i = 0; i < list.Count; i++)
            {
                head.Append("<th class='col-sm-2 text-center'>" + (i + 1) + "</th>");
                body.Append("<td >" + list[i] + "</td>");
            }

            head.Append("  </tr>");
            head.Append("</thead>");

            body.Append("  </tr>");
            body.Append("</tbody>");

            StringBuilder builder = new StringBuilder();
            builder.Append("<table class='table table-bordered'>");
            builder.Append(head);
            builder.Append(body);
            builder.Append("</table>");
            return builder.ToString();
        }

        private List<string> FilterYearList(List<string> list, int max_count)
        {
            List<string> result = new List<string>();
            if (list.Count <= max_count)
            {
                for (int i = 0; i < max_count; i++)
                {
                    if (i >= list.Count)
                    {
                        result.Add(" ");
                    }
                    else
                    {
                        result.Add(list[i]);
                    }
                }
            }
            else if (list.Count > max_count)
            {
                int min = list.Count - max_count;
                for (int i = min; i < list.Count; i++)
                {
                    result.Add(list[i]);
                }
            }
            return result;
        }

        private Item GetMUser(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string MUserType, string muid)
        {
            string sql = @"
                SELECT 
                    t1.*
                    , ISNULL(t2.in_short_org, t1.in_committee) AS 'committee_short_name'
                FROM 
                    {#MUserType} t1 WITH(NOLOCK)
                LEFT OUTER JOIN 
                    IN_RESUME t2 WITH(NOLOCK)
                    ON t2.in_name = t1.in_committee 
                    AND t2.in_member_type = 'area_cmt' 
                    AND t2.in_member_role = 'sys_9999'
                WHERE 
                    t1.id = '{#muid}'
            ";

            sql = sql.Replace("{#MUserType}", MUserType)
                .Replace("{#muid}", muid);

            return inn.applySQL(sql);
        }

        //跆委會(專項與地區)
        private Item GetCommittees(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn)
        {
            string sql = @"
                SELECT
                    id AS 'value'
                    , in_name AS 'label'
                FROM
                    IN_RESUME WITH (NOLOCK) 
                WHERE
                    (IN_MEMBER_TYPE = 'area_cmt')
                    OR 
                    (IN_MEMBER_TYPE = N'prjt_cmt' AND login_name IN (N'A100', N'A101') )
                ORDER BY 
                    login_name
            ";

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return inn.applySQL(sql);
        }

        //會員狀態選項
        private Item GetMStatuses(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn)
        {
            string sql = @"
                SELECT
                	value AS 'value'
                	, label_zt AS 'label'
                FROM [list] AS T1 WITH (NOLOCK)
                JOIN [VALUE] AS T2 WITH (NOLOCK)
                ON T1.id =T2.SOURCE_ID
                WHERE name ='In_Member_Status'
                ORDER BY SORT_ORDER
            ";

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return inn.applySQL(sql);
        }

        //取得委員會下的道館資訊
        private Item GetGyms(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string in_manager_name)
        {
            string sql = @"
                SELECT 
                    id, in_name, in_org, in_name, in_head_coach, in_principal
                FROM IN_RESUME WITH (NOLOCK)
                WHERE
                    in_manager_name = N'{#in_manager_name}' AND in_email_exam= N'良好' AND in_member_type =N'vip_gym' 
            ";
            sql = sql.Replace("{#in_manager_name}", in_manager_name);

            return inn.applySQL(sql);
        }

        //取得委員會下的成員資訊
        private Item GetStaffs(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string in_manager_name)
        {
            string sql = @"
                SELECT 
                    id, in_name, in_org, in_name,in_member_type
                FROM IN_RESUME WITH (NOLOCK)
                WHERE
                    in_manager_name = N'{#in_manager_name}' AND in_member_type IN (N'area_chm',N'area_dg')
                ORDER BY  
                (CASE in_member_type
                    WHEN 'area_dg' THEN 1
                    WHEN 'area_chm' THEN 2
                END)
            ";
            sql = sql.Replace("{#in_manager_name}", in_manager_name);

            return inn.applySQL(sql);
        }

        //所屬地區
        private Item GetAreas(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn)
        {
            string sql = @"
                SELECT 
                    value AS 'value'
                    , value AS 'label'
                FROM 
                    [list] AS T1 WITH (NOLOCK)
                JOIN 
                    [value] AS T2 WITH (NOLOCK)
                ON T1.id =T2.SOURCE_ID
                WHERE 
                    name ='in_area_list'
                ORDER BY 
                    SORT_ORDER
            ";

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return inn.applySQL(sql);
        }

        //取得下拉選單
        private Item GetValues(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string list_name)
        {
            string sql = @"
                SELECT
                    t2.value
			        , t2.label_zt AS 'label'
                FROM
                    [LIST] t1 WITH(NOLOCK)
		        INNER JOIN
			        [VALUE] t2 WITH(NOLOCK)
			        ON t2.source_id = t1.id
                WHERE
			        t1.name = N'{#list_name}'
		        ORDER BY
			        t2.sort_order
            ";

            sql = sql.Replace("{#list_name}", list_name);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return inn.applySQL(sql);
        }

        //會費繳納現況
        private Item GetResumePays(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string resume_id)
        {
            string sql = @"
                SELECT
                    *
                FROM
                    IN_RESUME_PAY WITH (NOLOCK) 
                WHERE
                    source_id = '{#resume_id}'
                ORDER BY 
                    in_year
                    , in_date
            ";

            sql = sql.Replace("{#resume_id}", resume_id);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return inn.applySQL(sql);
        }

        //晉段記錄
        private Item GetResumePromotions(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string resume_id)
        {
            string sql = @"
                SELECT
                    *
                FROM
                    IN_RESUME_PROMOTION WITH (NOLOCK) 
                WHERE
                    source_id = '{#resume_id}'
                ORDER BY 
                    in_date
            ";

            sql = sql.Replace("{#resume_id}", resume_id);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return inn.applySQL(sql);
        }

        //證照列表
        private Item GetResumeCertificates(TConfig cfg, string resume_id)
        {
            string sql = @"
                SELECT
                    *
                FROM
                    IN_RESUME_CERTIFICATE WITH (NOLOCK) 
                WHERE
                    source_id = '{#resume_id}'
                ORDER BY 
                    in_phototype
            ";

            sql = sql.Replace("{#resume_id}", resume_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        private Item GetMeetingCertificates(TConfig cfg)
        {
            string sql = @"
                SELECT
	                t1.in_type
	                , t1.in_country
	                , t1.in_level
	                , t1.is_sides
	                , t2.label
                FROM 
	                {#mtc_type} t1 WITH(NOLOCK)
                LEFT OUTER JOIN
                (
	                SELECT 
		                t12.value
		                , t12.label_zt AS 'label'
	                FROM
		                [LIST] t11 WITH(NOLOCK)
	                INNER JOIN
		                [VALUE] t12 WITH(NOLOCK)
		                ON t12.source_id = t11.id
	                WHERE
		                t11.name = 'In_Meeting_CertificateType'
                ) t2 ON t2.value = t1.in_type
                WHERE
                    t1.source_id = '{#meeting_id}'
                ORDER BY
                    t1.in_type
            ";

            sql = sql.Replace("{#mtc_type}", cfg.MCertType)
                .Replace("{#meeting_id}", cfg.meeting_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        private List<Item> MapCerts(TConfig cfg, Item itmMCerts, Item itmRCerts, Item itmReturn)
        {
            List<Item> result = new List<Item>();

            var mcert_map = ItemsToDic(itmMCerts, CertKey_Meeting);
            var rcert_map = ItemsToDic(itmRCerts, CertKey_Resume);

            foreach (var kv in mcert_map)
            {
                var itmMCert = kv.Value;

                var key = kv.Key;
                var text = CertText(itmMCert);

                if (rcert_map.ContainsKey(key))
                {
                    Item itmRCert = rcert_map[key];
                    itmRCert.setProperty("in_type", text);
                    itmRCert.setProperty("hide_rotate", " ");
                    result.Add(itmRCert);
                }
                else
                {
                    Item itmEmpty = cfg.inn.newItem();
                    itmEmpty.setProperty("in_type", text);

                    if (key == "in_photo0")
                    {
                        //大頭照
                        itmEmpty.setProperty("in_file1", itmReturn.getProperty("in_photo1", ""));
                    }
                    else
                    {
                        itmEmpty.setProperty("hide_rotate", "item_show_0");
                    }

                    result.Add(itmEmpty);
                }
            }

            return result;
        }

        private string CertText(Item item)
        {
            string label = item.getProperty("label", "");
            string in_country = item.getProperty("in_country", "").ToUpper();

            if (in_country == "TW" && !label.Contains("國內"))
            {
                label = "國內" + label;
            }
            else if (in_country == "GL" && !label.Contains("國外"))
            {
                label = "國外" + label;
            }

            return label;
        }

        private Dictionary<string, Item> ItemsToDic(Item items, Func<Item, string> getKey)
        {
            Dictionary<string, Item> result = new Dictionary<string, Item>();

            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string key = getKey(item);
                if (!result.ContainsKey(key))
                {
                    result.Add(key, item);
                }
            }

            return result;
        }

        private string CertKey_Resume(Item item)
        {
            return item.getProperty("in_photokey", "").ToLower();
        }

        private string CertKey_Meeting(Item item)
        {
            string in_type = item.getProperty("in_type", "");
            string in_country = item.getProperty("in_country", "").ToUpper();
            string in_level = item.getProperty("in_level", "");
            string is_sides = item.getProperty("is_sides", "");

            string key = "in_photo" + in_type + in_country;
            return key.ToLower();
        }

        private void OverrideValue(Item itmTarget, Item itmSource, List<TField> fields)
        {
            foreach (var field in fields)
            {
                if (field.need_clear)
                {
                    itmTarget.setProperty(field.property, field.ClearValue(itmSource, field));
                }
                else
                {
                    itmTarget.setProperty(field.property, itmSource.getProperty(field.property, ""));
                }
            }
        }

        /// <summary>
        /// 清除 NA mail
        /// </summary>
        private string ClearEmail(Item item, TField field)
        {
            string value = item.getProperty(field.property, "");
            if (value.Contains("na@na.n"))
            {
                return "";
            }
            else
            {
                return value;
            }
        }

        /// <summary>
        /// 清除 1900 的值
        /// </summary>
        private string Clear1900(Item item, TField field)
        {
            string value = item.getProperty(field.property, "");
            if (value.Contains("1900"))
            {
                return "";
            }
            else
            {
                return GetDateTimeByFormat(value, field.format, hours: 8);
            }
        }

        /// <summary>
        /// 日期1~日期2 (properties 限定兩個值)
        /// </summary>
        private string DateRange(Item item, TField field)
        {
            string v1 = item.getProperty(field.properties[0], "");
            string v2 = item.getProperty(field.properties[1], "");

            string r1 = GetDateTimeByFormat(v1, field.format, hours: 8);
            string r2 = GetDateTimeByFormat(v2, field.format, hours: 8);

            if (r1 == "" && r2 == "")
            {
                return "";
            }
            else
            {
                return r1 + "~" + r2;
            }
        }

        /// <summary>
        /// 下載檔案
        /// </summary>
        private string DownloadFiles(Item item, TField field)
        {
            StringBuilder builder = new StringBuilder();
            foreach (string property in field.properties)
            {
                string value = item.getProperty(property, "");
                if (value != "")
                {
                    if (builder.Length > 0) builder.Append(" ");
                    builder.Append("<a><i class='fa fa-picture-o' onclick='CertFile_Click(this)' data-photo='" + value + "'></i></a>");
                }
            }

            return builder.ToString();
        }

        /// <summary>
        /// 轉為民國年
        /// </summary>
        private string MapChineseYear(Item item, TField field)
        {
            string value = item.getProperty(field.property, "");
            if (value == "")
            {
                return "";
            }
            else
            {
                int result = GetIntVal(value) - 1911;
                return result.ToString();
            }
        }

        /// <summary>
        /// Checkbox
        /// </summary>
        private string CheckBox(Item item, TField field)
        {
            string value = item.getProperty(field.property, "");
            if (value == "1")
            {
                return "checked='checked'";
            }
            else
            {
                return "";
            }
        }

        /// <summary>
        /// 轉為是 or 否
        /// </summary>
        private string GetYN(Item item, TField field)
        {
            string value = item.getProperty(field.property, "");

            if (value == "0")
            {
                return "否";
            }
            else if (value == "1")
            {
                return "是";
            }
            else
            {
                return "";
            }
        }

        private string GetDegreeDisplay(Item item, TField field)
        {
            return GetDegreeDisplay(item.getProperty(field.property, ""));
        }

        private string ClearMoney(Item item, TField field)
        {
            string value = item.getProperty(field.property, "");
            if (value == "" || value == "0")
            {
                return "0";
            }
            else
            {
                int result = GetIntVal(value);
                //0會被清空字串
                return result.ToString("###,###");
            }
        }

        private string GetDateTimeByFormat(string value, string format, int hours = 0)
        {
            if (value == "") return "";

            DateTime dt = DateTime.Now;
            if (DateTime.TryParse(value, out dt))
            {
                return dt.AddHours(hours).ToString(format);
            }
            else
            {
                return value;
            }
        }

        private string GetSubtotal(string count, string amount)
        {
            int quantity = 0;
            int price = 0;
            if (!int.TryParse(count, out quantity) || !int.TryParse(amount, out price))
            {
                return "0";
            }
            return (quantity * price).ToString("###,###");
        }

        private bool IsError(Item item, bool isSingle = true)
        {
            if (item.isError() || item.getResult() == "")
            {
                return true;
            }
            else if (isSingle && item.getItemCount() != 1)
            {
                return true;
            }
            else if (item.getItemCount() <= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //附加道館社團 Table
        private void AppendGymTable(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item items, Item itmReturn)
        {
            List<TField> fields = new List<TField>();
            fields.Add(new TField { property = "in_link", title = "設立名稱", css = "text-left" });
            fields.Add(new TField { property = "in_principal", title = "負責人", css = "text-left" });
            fields.Add(new TField { property = "in_head_coach", title = "總教練", css = "text-left" });

            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            head.AppendLine("<thead>");
            foreach (var field in fields)
            {
                head.AppendLine("<th class='text-center' data-field='" + field.property + "' data-sortable='true'>" + field.title + "</th>");
            }
            head.AppendLine("</thead>");

            int count = items.getItemCount(); ;

            body.AppendLine("<tbody>");
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string id = item.getProperty("id", "");
                string in_name = item.getProperty("in_name", "");
                string in_org = item.getProperty("in_org", "");
                string in_member_type = item.getProperty("in_member_type", "");
                item.setProperty("in_link", GetDashboardLink(id, in_name, in_org, in_member_type));

                body.AppendLine("<tr>");
                foreach (var field in fields)
                {
                    body.AppendLine("<td class='" + field.css + "' data-field='" + field.property + "' >" + field.GetStr(item) + "</td>");
                }
                body.AppendLine("</tr>");
            }
            body.AppendLine("</tbody>");


            string table_name = "resume_table";

            StringBuilder builder = new StringBuilder();
            builder.AppendLine(GetTableAttribute(table_name));
            builder.Append(head);
            builder.Append(body);
            builder.AppendLine("</table>");
            builder.AppendLine("<script>");
            builder.AppendLine("$('#" + table_name + "').bootstrapTable();");
            builder.AppendLine("if ($(window).width() <= 768) { $('#" + table_name + "').bootstrapTable('toggleView'); }");
            builder.AppendLine("</script>");

            itmReturn.setProperty("inn_GymTable", builder.ToString());

        }

        //附加委員會成員 Table
        private void AppendStaffTable(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item items, Item itmReturn)
        {
            List<TField> fields = new List<TField>();
            fields.Add(new TField { property = "in_link", title = "姓名", css = "text-left" });
            fields.Add(new TField { property = "in_staff_title", title = "職稱", css = "text-left" });

            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            head.AppendLine("<thead>");
            foreach (var field in fields)
            {
                head.AppendLine("<th class='text-center' data-field='" + field.property + "' data-sortable='true'>" + field.title + "</th>");
            }
            head.AppendLine("</thead>");

            int count = items.getItemCount(); ;

            body.AppendLine("<tbody>");
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string id = item.getProperty("id", "");
                string in_name = item.getProperty("in_name", "");
                string in_org = item.getProperty("in_org", "");
                string in_member_type = item.getProperty("in_member_type", "");
                item.setProperty("in_link", GetDashboardStaffLink(id, in_name, in_org));
                if (in_member_type == "area_dg")
                {
                    in_member_type = "總幹事";
                }
                else if (in_member_type == "area_chm")
                {
                    in_member_type = "主任委員";
                }
                item.setProperty("in_staff_title", in_member_type);
                body.AppendLine("<tr>");
                foreach (var field in fields)
                {
                    body.AppendLine("<td class='" + field.css + "' data-field='" + field.property + "' >" + field.GetStr(item) + "</td>");
                }
                body.AppendLine("</tr>");
            }
            body.AppendLine("</tbody>");


            string table_name = "resume_table";

            StringBuilder builder = new StringBuilder();
            builder.AppendLine(GetTableAttribute(table_name));
            builder.Append(head);
            builder.Append(body);
            builder.AppendLine("</table>");
            builder.AppendLine("<script>");
            builder.AppendLine("$('#" + table_name + "').bootstrapTable();");
            builder.AppendLine("if ($(window).width() <= 768) { $('#" + table_name + "').bootstrapTable('toggleView'); }");
            builder.AppendLine("</script>");

            itmReturn.setProperty("inn_StaffTable", builder.ToString());

        }

        //委員會成員
        private string GetDashboardStaffLink(string resume_id, string in_name, string in_org)
        {
            return "<a target='_blank' href='../pages/c.aspx?page=ManagerStaffView.html&method=In_Group_Resume_View&id=" + resume_id + "' >"
                   + in_name
                   + "</a>";
        }

        private string GetDashboardLink(string resume_id, string in_name, string in_org, string in_member_type)
        {
            if (in_org == "1")
            {
                if (in_member_type == "vip_group")
                {
                    return "<a href='../pages/c.aspx?page=GroupMemberView.html&method=In_Group_Resume_View&id=" + resume_id + "' >"
                        + in_name
                        + "</a>";
                }
                else
                {
                    //onclick=\"ajaxindicatorstart('載入中...');\"
                    return "<a target='_blank' href='../pages/c.aspx?page=GymMemberView.html&method=In_Group_Resume_View&id=" + resume_id + "' >"
                        + in_name
                        + "</a>";
                }
            }
            else
            {
                //onclick=\"ajaxindicatorstart('載入中...');\"
                return "<a target='_blank' href='../pages/c.aspx?page=SingleMemberView.html&method=In_Group_Resume_View&id=" + resume_id + "' >"
                    + in_name
                    + "</a>";
                // return "<a target='_blank' href='../pages/c.aspx?page=UserResume.html&method=in_user_resume&id=" + resume_id + "' >"
                //     + in_name
                //     + "</a>";
            }
        }

        private string GetTableAttribute(string table_name)
        {
            return "<table id='" + table_name + "' class='table table-hover table-bordered table-rwd rwd rwdtable' "
                + " data-toggle='table' "
                + " data-click-to-select='true' "
                + " data-striped='false' "
                + " data-search='true' "
                 + " data-pagination='true' "
                 + " data-show-pagination-switch='false'"
                + ">";
        }

        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }


            public string meeting_id { get; set; }
            public string muid { get; set; }
            public string resumeid { get; set; }
            public string scene { get; set; }
            public string mode { get; set; }
            public string role { get; set; }

            public string muid_list { get; set; }
            public string resumeid_list { get; set; }
            public string recover { get; set; }


            public bool isMeetingAdmin { get; set; }
            public bool isCommittee { get; set; }

            public Item itmLoginResume { get; set; }
            public Item itmMeeting { get; set; }
            public Item itmResumeView { get; set; }
            public Item itmMUser { get; set; }

            public string MtType { get; set; }
            public string MUserType { get; set; }
            public string MCertType { get; set; }
            public string MSvType { get; set; }
            public string SvType { get; set; }

            public string mt_title { get; set; }
            public string in_meeting_type { get; set; }
            public string in_verify_mode { get; set; }
            public string in_seminar_type { get; set; }

        }

        private class TField
        {
            public string title { get; set; }
            public string property { get; set; }
            public string format { get; set; }
            public string css { get; set; }
            public bool need_clear { get; set; }
            public List<string> properties { get; set; }
            public Func<Item, TField, string> ClearValue { get; set; }
            public string GetStr(Item item)
            {
                return item.getProperty(this.property, "");
            }
        }
        #region 證件驗證結果

        //證件驗證結果
        private void AppendCertCheck(TConfig cfg, Item itmMUser, string resume_id, Item itmReturn)
        {
            if (resume_id == "") return;

            string muser_resume_btn = "<a class='btn btn-sm btn-primary' target='_blank'"
                + " href='c.aspx?page=SingleMemberView.html&method=In_Group_Resume_View&id=" + resume_id + "'>個人資料</a>";

            itmReturn.setProperty("muser_resume_btn", muser_resume_btn);


            //取得證照設定規則
            var itmRules = GetRules(cfg);
            //轉化規則
            var rules = MapRules(cfg, itmRules);
            //取得與會者證照資料
            var itmMUserCertficates = GetMUserCertficates(cfg, itmMUser);
            //轉換與會者證照資料
            var cert_map = ConvertMap(itmMUserCertficates);
            //轉換與會者
            var users = MapMUsers(itmMUser, cert_map);
            //執行證照驗證
            var result = RunCheck(cfg, rules, users);

            if (result.message != "")
            {
                itmReturn.setProperty("cert_verify_message", result.message);
            }
        }

        private List<TMUser> MapMUsers(Item itmMUsers, Dictionary<string, Dictionary<string, TCert>> cert_map)
        {
            List<TMUser> entities = new List<TMUser>();

            int count = itmMUsers.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item itmMUser = itmMUsers.getItemByIndex(i);
                string muid = itmMUser.getProperty("id", "");
                string in_sno = itmMUser.getProperty("in_sno", "").ToUpper();
                string in_name = itmMUser.getProperty("in_name", "");
                string in_cert_valid = itmMUser.getProperty("in_cert_valid", "");

                TMUser entity = new TMUser
                {
                    muid = muid,
                    in_sno = in_sno,
                    in_name = in_name,
                    Value = itmMUser,
                    in_cert_valid = in_cert_valid,
                    new_cert_valid = "1",
                };

                if (cert_map.ContainsKey(in_sno))
                {
                    entity.Certificates = cert_map[in_sno];
                }
                else
                {
                    entity.Certificates = new Dictionary<string, TCert>();
                }

                entities.Add(entity);
            }

            return entities;
        }

        /// <summary>
        /// 執行證照驗證
        /// </summary>
        private TResult RunCheck(TConfig cfg, List<TCert> rules, List<TMUser> users)
        {
            TResult result = new TResult
            {
                first_muid = "",
                first_sno = "",
            };

            var builder = new StringBuilder();

            foreach (var rule in rules)
            {
                var messages = new List<string>();
                var un_upload_count = 0;

                foreach (var user in users)
                {
                    bool is_ok = IsOk(rule, user);

                    if (!is_ok)
                    {
                        user.new_cert_valid = "0";
                        un_upload_count++;
                        messages.Add("姓名:[" + user.in_name + "] 身分證字號:[" + user.in_sno + "]");

                        if (result.first_muid == "")
                        {
                            result.first_muid = user.muid;
                            result.first_sno = user.in_sno;
                        }
                    }
                }

                if (un_upload_count > 0)
                {
                    builder.AppendLine("<span style='color: red ; background-color: yellow;'>"
                        + "<b>" + rule.label + "</b></span>"
                        + "</br>");
                }
            }

            ////將驗證結果更新至與會者
            //foreach (var user in users)
            //{
            //    if (user.new_cert_valid != user.in_cert_valid)
            //    {
            //        string sql = "UPDATE " + cfg.muser_type + " SET in_cert_valid = '" + user.new_cert_valid + "' WHERE id = '" + user.muid + "'";
            //        cfg.inn.applySQL(sql);
            //    }
            //}

            //string sql_staff = "UPDATE " + cfg.muser_type + " SET in_cert_valid = '1' WHERE source_id = '" + cfg.meeting_id + "' AND in_l1 = N'隊職員' AND ISNULL(in_sno, '') = ''";
            ////cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql_staff: " + sql_staff);
            //cfg.inn.applySQL(sql_staff);

            result.message = builder.ToString();

            return result;
        }

        private bool IsOk(TCert rule, TMUser user)
        {
            //不卡控
            if (rule.is_bypass)
            {
                return true;
            }

            bool need_check = false;
            if (rule.PreMatchFuncs.Count == 0)
            {
                need_check = true;
            }
            else
            {
                foreach (var func in rule.PreMatchFuncs)
                {
                    bool is_match = func(rule, user.Value);
                    if (is_match)
                    {
                        need_check = true;
                        break;
                    }
                }
            }

            bool is_ok = true;

            if (need_check)
            {
                if (rule.is_headshot)
                {
                    if (user.Value.getProperty("in_photo1", "") == "")
                    {
                        //查無大頭照
                        is_ok = false;
                    }
                }
                else if (!user.Certificates.ContainsKey(rule.key))
                {
                    //查無這張證照
                    is_ok = false;
                }
                else
                {
                    TCert ucert = user.Certificates[rule.key];
                    foreach (var property in rule.file_property_list)
                    {
                        //這張證照的檔案之一不存在
                        if (ucert.Value.getProperty(property, "").Trim() == "")
                        {
                            is_ok = false;
                        }
                    }
                }
            }

            return is_ok;
        }

        private List<TCert> MapRules(TConfig cfg, Item itmRules)
        {
            List<TCert> list = new List<TCert>();

            int count = itmRules.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item itmRule = itmRules.getItemByIndex(i);
                TCert cert = new TCert
                {
                    label = itmRule.getProperty("label", ""),
                    in_type = itmRule.getProperty("in_type", ""),
                    in_country = itmRule.getProperty("in_country", ""),
                    in_level = itmRule.getProperty("in_level", ""),
                    in_limit = itmRule.getProperty("in_limit", ""),
                    is_sides = itmRule.getProperty("is_sides", ""),
                    is_enable = itmRule.getProperty("is_enable", ""),
                    in_property = itmRule.getProperty("in_property", ""),
                    in_value = itmRule.getProperty("in_value", ""),
                    in_operator = itmRule.getProperty("in_operator", ""),
                    in_require = itmRule.getProperty("in_require", ""),

                    file_property_list = new List<string>(),
                    PreMatchFuncs = new List<Func<TCert, Item, bool>>(),
                };

                if (cert.in_country == "TW" && !cert.label.Contains("國內"))
                {
                    cert.label = "國內" + cert.label;
                }
                else if (cert.in_country == "GL" && !cert.label.Contains("國外"))
                {
                    cert.label = "國外" + cert.label;
                }

                if (cert.in_type == "" || cert.is_enable != "1")
                {
                    continue;
                }

                if (cert.in_type == "0")
                {
                    cert.is_headshot = true;
                }

                cert.key = cert.in_type + cert.in_country + cert.in_level;
                cert.key = cert.key.ToLower();

                if (cert.is_sides == "1")
                {
                    //正反面
                    cert.file_property_list.Add("in_file1");
                    cert.file_property_list.Add("in_file2");
                }
                else
                {
                    //正面
                    cert.file_property_list.Add("in_file1");
                }

                switch (cert.in_limit)
                {
                    case "0":
                        cert.is_bypass = true;
                        break;

                    case "1":
                        cert.PreMatchFuncs.Add(IsPlayer);
                        break;

                    case "2":
                        cert.PreMatchFuncs.Add(IsPlayer);
                        cert.PreMatchFuncs.Add(IsTutorship);
                        break;

                    default:
                        break;
                }

                switch (cert.in_require)
                {
                    case "1": //必填證書編號
                        cert.file_property_list.Add("in_certificate_no");
                        break;

                    case "2"://必填證書編號+期限
                        cert.file_property_list.Add("in_certificate_no");
                        cert.file_property_list.Add("in_effective_start");
                        cert.file_property_list.Add("in_effective_end");
                        break;

                    default:
                        break;
                }

                if (cert.in_property != "" && cert.in_value != "")
                {
                    cert.PreMatchFuncs.Add(NeedCheck);
                }

                list.Add(cert);
            }
            return list;
        }

        private bool NeedCheck(TCert rule, Item item)
        {
            string v = item.getProperty(rule.in_property, "");
            if (v == "") return false;

            switch (rule.in_property)
            {
                case "in_birth":
                    return CompareDtm(v, rule.in_value, rule.in_operator, bAdd8Hours: true);

                default:
                    return CompareStr(v, rule.in_value, rule.in_operator);
            }
        }

        //字串比較
        private bool CompareStr(string a, string b, string op)
        {
            switch (op)
            {
                case "eq": return a == b; //於
                case "ne": return a != b; //不等於
                case "contains": return a.Contains(b); //包含
                default: return false; //未設定 or 其他
            }
        }

        //整數比較
        private bool CompareInt(string a, string b, string op)
        {
            var v1 = GetIntVal(a);
            var v2 = GetIntVal(b);

            switch (op)
            {
                case "eq": return v1 == v2; //於
                case "ne": return v1 != v2; //不等於
                case "gt": return v1 > v2;  //大於
                case "ge": return v1 >= v2; //大於等於
                case "lt": return v1 < v2; //小於
                case "le": return v1 <= v2; //小於等於
                case "contains": return (v1 & v2) == v2; //包含(位元算法)
                default: return false; //未設定
            }
        }

        //日期比較
        private bool CompareDtm(string a, string b, string op, bool bAdd8Hours = false)
        {
            var d1 = GetDateTime(a);
            var d2 = GetDateTime(b);

            if (bAdd8Hours)
            {
                d1 = d1.AddHours(8);
            }

            switch (op)
            {
                case "eq": return d1 == d2; //於
                case "ne": return d1 != d2; //不等於
                case "gt": return d1 > d2;  //大於
                case "ge": return d1 >= d2; //大於等於
                case "lt": return d1 < d2; //小於
                case "le": return d1 <= d2; //小於等於
                case "contains": return d1 == d2; //包含
                default: return false; //未設定
            }
        }

        private bool IsPlayer(TCert rule, Item item)
        {
            string in_l1 = item.getProperty("in_l1", "");
            return in_l1 != "隊職員";
        }

        private bool IsTutorship(TCert rule, Item item)
        {
            string in_l2 = item.getProperty("in_l2", "");
            return in_l2 == "教練";
        }

        private Dictionary<string, Dictionary<string, TCert>> ConvertMap(Item items)
        {
            Dictionary<string, Dictionary<string, TCert>> map = new Dictionary<string, Dictionary<string, TCert>>();
            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_type = item.getProperty("in_type", "");
                string in_phototype = item.getProperty("in_phototype", "");

                if (in_phototype == "")
                {
                    continue;
                }

                string user_key = item.getProperty("in_sno", "").ToUpper();
                string type_key = GetTypeKeyFromResume(in_phototype);

                if (type_key == "")
                {
                    continue;
                }

                Dictionary<string, TCert> sub = null;
                if (map.ContainsKey(user_key))
                {
                    sub = map[user_key];
                }
                else
                {
                    sub = new Dictionary<string, TCert>();
                    map.Add(user_key, sub);
                }

                TCert cert = new TCert
                {
                    key = type_key,
                    name = in_type,
                    Value = item,
                };

                if (sub.ContainsKey(type_key))
                {
                    //異常
                }
                else
                {
                    sub.Add(type_key, cert);
                }
            }
            return map;
        }

        private string GetTypeKeyFromResume(string value)
        {
            //in_photo7twa_p01,in_photo7twa_n01
            //return 7twa (裁判證-國內-A級)
            if (!value.Contains("in_photo"))
            {
                return "";
            }

            string v = value.Replace("in_photo", "");

            if (string.IsNullOrWhiteSpace(v))
            {
                return "";
            }

            return v.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries).First();
        }

        /// <summary>
        /// 取得證照設定(卡控規則)
        /// </summary>
        private Item GetRules(TConfig cfg)
        {
            string sql = @"
                SELECT
	                t1.*
	                , t2.label
                FROM 
	                {#mcert_type} t1 WITH(NOLOCK)
                LEFT OUTER JOIN
                (
	                SELECT
		                t12.value
		                , t12.label_zt   AS 'label'
		                , t12.sort_order AS 'sort_order'
	                FROM
		                [LIST] t11 WITH(NOLOCK)
	                INNER JOIN
		                [VALUE] t12 WITH(NOLOCK)
		                ON t12.source_id = t11.id
	                WHERE
		                t11.name = N'In_Meeting_CertificateType'
                ) t2
	                ON t2.value = t1.in_type
                WHERE 
	                t1.source_id = '{#meeting_id}'
                ORDER BY
	                t2.sort_order
            ";

            sql = sql.Replace("{#mcert_type}", cfg.MCertType)
                .Replace("{#meeting_id}", cfg.meeting_id);

            return cfg.inn.applySQL(sql);
        }


        /// <summary>
        /// 取得與會者證照(IN_RESUME_CERTIFICATE)
        /// </summary>
        private Item GetMUserCertficates(TConfig cfg, Item itmMUser)
        {
            string in_sno = itmMUser.getProperty("in_sno", "");
            string sql = @"SELECT t2.in_sno, t1.* FROM IN_RESUME_CERTIFICATE t1 WITH(NOLOCK) INNER JOIN IN_RESUME t2 WITH(NOLOCK) ON t2.id = t1.source_id WHERE t2.in_sno = '" + in_sno + "'";
            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);
            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// 與會者
        /// </summary>
        private class TMUser
        {
            /// <summary>
            /// 與會者 id
            /// </summary>
            public string muid { get; set; }

            /// <summary>
            /// 姓名
            /// </summary>
            public string in_name { get; set; }

            /// <summary>
            /// 身分證號
            /// </summary>
            public string in_sno { get; set; }

            /// <summary>
            /// 證照驗證通過
            /// </summary>
            public string in_cert_valid { get; set; }

            /// <summary>
            /// 證照驗證通過 (本次新值)
            /// </summary>
            public string new_cert_valid { get; set; }

            /// <summary>
            /// 資料來源
            /// </summary>
            public Item Value { get; set; }

            /// <summary>
            /// 證照列表
            /// </summary>
            public Dictionary<string, TCert> Certificates { get; set; }
        }

        /// <summary>
        /// 證照
        /// </summary>
        private class TCert
        {
            /// <summary>
            /// 鍵
            /// </summary>
            public string key { get; set; }

            /// <summary>
            /// 證照名稱 (In_Resume_Certificate.in_type)
            /// </summary>
            public string name { get; set; }

            /// <summary>
            /// 證照類型名稱 (下拉選單 In_Meeting_CertificateType label_zt)
            /// </summary>
            public string label { get; set; }

            /// <summary>
            /// 資料來源
            /// </summary>
            public Item Value { get; set; }

            /// <summary>
            /// 證照類別 (0: 大頭照、1: 身分證、2: 良民證、3: 學歷、4: 戶籍、5: 健康檢查、6: 級段證、7: 裁判證、8: 教練證)
            /// </summary>
            public string in_type { get; set; }

            /// <summary>
            /// 國家別 (TW: 國內、GL: 國外)
            /// </summary>
            public string in_country { get; set; }

            /// <summary>
            /// 級別 (1-9、A-C)
            /// </summary>
            public string in_level { get; set; }

            /// <summary>
            /// 卡控 (0: 不限制、1: 選手必須上傳、2: 選手與教練必須上傳、3: 所有人員必須上傳)
            /// </summary>
            public string in_limit { get; set; }

            /// <summary>
            /// 有正反面
            /// </summary>
            public string is_sides { get; set; }

            /// <summary>
            /// 是否啟用
            /// </summary>
            public string is_enable { get; set; }

            /// <summary>
            /// 篩選欄位 (In_Meeting_User)
            /// </summary>
            public string in_property { get; set; }

            /// <summary>
            /// 比較子
            /// </summary>
            public string in_operator { get; set; }

            /// <summary>
            /// 篩選值(符合者進行卡控判斷)
            /// </summary>
            public string in_value { get; set; }

            /// <summary>
            /// 必填項目
            /// </summary>
            public string in_require { get; set; }

            /// <summary>
            /// 不卡控
            /// </summary>
            public bool is_bypass { get; set; }

            /// <summary>
            /// 大頭照卡控
            /// </summary>
            public bool is_headshot { get; set; }

            /// <summary>
            /// 有正反面就會有兩筆 (in_file1，in_file2)
            /// </summary>
            public List<string> file_property_list { get; set; }

            /// <summary>
            /// 先成立條件才卡控 (無資料代表直接卡控) (其中一個成立就卡控)
            /// </summary>
            public List<Func<TCert, Item, bool>> PreMatchFuncs { get; set; }
        }

        private class TResult
        {
            public string message { get; set; }
            public string first_muid { get; set; }
            public string first_sno { get; set; }
        }

        #endregion 證件驗證結果


        private int GetIntVal(string value, int def = 0)
        {
            int result = def;
            Int32.TryParse(value, out result);
            return result;
        }

        private DateTime GetDateTime(string value)
        {
            DateTime result = DateTime.MinValue;
            DateTime.TryParse(value, out result);
            return result;
        }
    }
}