﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;
using System.Net;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class in_meeting_program_options2 : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 
                    取得三階選單 json
                輸入: 
                    meeting_id
                日期: 
                    2020-10-30: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]in_meeting_program_options2";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            string meeting_id = itmR.getProperty("meeting_id", "");

            if (meeting_id == "")
            {
                throw new Exception("賽事 id 不得為空白");
            }

            Item itmLvInfo = GetLevelInfo(CCO, strMethodName, inn, meeting_id);

            //附加細項相關資訊
            AppendOptions(CCO, strMethodName, inn, itmLvInfo, itmR);

            return itmR;
        }

        private Item GetLevelInfo(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string meeting_id)
        {
            string sql = @"
                SELECT 
	                t2.id
	                , t2.in_property
                FROM
	                IN_MEETING_SURVEYS t1 WITH(NOLOCK)
                INNER JOIN 
	                IN_SURVEY t2 WITH(NOLOCK)
	                ON t2.id = t1.related_id
                WHERE 
	                t1.source_id = '{#meeting_id}' 
	                AND t2.in_property IN ('in_l1', 'in_l2', 'in_l3')
                ORDER BY
	                t2.in_property
            ";

            sql = sql.Replace("{#meeting_id}", meeting_id);

            Item items = inn.applySQL(sql);

            int count = items.getItemCount();

            Item itmResult = inn.newItem();

            List<string> lvs = new List<string>();
            
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string id = item.getProperty("id", "");
                string in_property = item.getProperty("in_property", "");

                lvs.Add(in_property);
                
                itmResult.setProperty(in_property + "_id", id);
            }

            itmResult.setProperty("in_level_array", string.Join(",", lvs));

            return itmResult;
        }

        //附加細項相關資訊
        private void AppendOptions(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmLvInfo, Item itmReturn)
        {
            string in_level_array = itmLvInfo.getProperty("in_level_array", "");

            Item items = null;
            if (in_level_array.Contains("in_l3"))
            {
                items = GetOptions123(CCO, strMethodName, inn, itmLvInfo);
            }
            else if (in_level_array.Contains("in_l2"))
            {
                items = GetOptions12(CCO, strMethodName, inn, itmLvInfo);
            }
            else if (in_level_array.Contains("in_l1"))
            {
                items = GetOptions1(CCO, strMethodName, inn, itmLvInfo);
            }

            if (items == null || items.isError() || items.getItemCount() <= 0)
            {
                return;
            }

            List<TNode> nodes = new List<TNode>();

            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                TNode l1 = AddAndGetNode(nodes, item, "in_l1");
                TNode l2 = AddAndGetNode(l1.Nodes, item, "in_l2");
                TNode l3 = AddAndGetNode(l2.Nodes, item, "in_l3");
            }

            itmReturn.setProperty("in_level_json", Newtonsoft.Json.JsonConvert.SerializeObject(nodes));
        }

        private TNode AddAndGetNode(List<TNode> nodes, Item item, string lv)
        {
            string value = item.getProperty(lv + "_value", "");

            TNode search = nodes.Find(x => x.Val == value);

            if (search == null)
            {
                search = new TNode
                {
                    Lv = lv,
                    Val = value,
                    Lbl = item.getProperty(lv + "_label", ""),
                    Ext = item.getProperty(lv + "_extend_value", ""),
                    Nodes = new List<TNode>()
                };
                nodes.Add(search);
            }

            return search;
        }

        private class TNode
        {
            public string Lv { get; set; }
            public string Val { get; set; }
            public string Lbl { get; set; }
            public string Ext { get; set; }
            public List<TNode> Nodes { get; set; }
        }

        private Item GetOptions1(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmLvInfo)
        {
            string in_l1_id = itmLvInfo.getProperty("in_l1_id", "");

            string sql = @"
                SELECT
                    t1.in_value           AS 'in_l1_value'
                    , t1.in_label         AS 'in_l1_label'
                    , t1.sort_order       AS 'in_l1_sort_order'
                    , t1.in_extend_value  AS 'in_l1_extend_value'
                FROM 
                    IN_SURVEY_OPTION t1 WITH(NOLOCK)
                WHERE
                    t1.SOURCE_ID = '{#in_l1_id}'
                ORDER BY
                    t1.SORT_ORDER
            ";

            sql = sql.Replace("{#in_l1_id}", in_l1_id);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return inn.applySQL(sql);
        }

        private Item GetOptions12(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmLvInfo)
        {
            string in_l1_id = itmLvInfo.getProperty("in_l1_id", "");
            string in_l2_id = itmLvInfo.getProperty("in_l2_id", "");

            string sql = @"
                SELECT
                    t1.in_value           AS 'in_l1_value'
                    , t1.in_label         AS 'in_l1_label'
                    , t1.sort_order       AS 'in_l1_sort_order'
                    , t1.in_extend_value  AS 'in_l1_extend_value'
                    , t2.in_value         AS 'in_l2_value'
                    , t2.in_label         AS 'in_l2_label'
                    , t2.sort_order       AS 'in_l2_sort_order'
                    , t2.in_extend_value  AS 'in_l2_extend_value'
                FROM 
                    IN_SURVEY_OPTION t1 WITH(NOLOCK)
                LEFT OUTER JOIN (
                        SELECT IN_FILTER, IN_VALUE, IN_LABEL, SORT_ORDER, IN_EXPENSE_VALUE, IN_EXTEND_VALUE FROM IN_SURVEY_OPTION WITH(NOLOCK) WHERE SOURCE_ID = '{#in_l2_id}'
                    ) t2 ON t2.IN_FILTER = t1.IN_VALUE
                WHERE
                    t1.SOURCE_ID = '{#in_l1_id}'
                ORDER BY
                    t1.SORT_ORDER
                    , t2.SORT_ORDER
            ";

            sql = sql.Replace("{#in_l1_id}", in_l1_id)
                .Replace("{#in_l2_id}", in_l2_id);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return inn.applySQL(sql);
        }

        private Item GetOptions123(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmLvInfo)
        {
            string in_l1_id = itmLvInfo.getProperty("in_l1_id", "");
            string in_l2_id = itmLvInfo.getProperty("in_l2_id", "");
            string in_l3_id = itmLvInfo.getProperty("in_l3_id", "");

            string sql = @"
                SELECT
                    t1.in_value           AS 'in_l1_value'
                    , t1.in_label         AS 'in_l1_label'
                    , t1.sort_order       AS 'in_l1_sort_order'
                    , t1.in_extend_value  AS 'in_l1_extend_value'
                    , t2.in_value         AS 'in_l2_value'
                    , t2.in_label         AS 'in_l2_label'
                    , t2.sort_order       AS 'in_l2_sort_order'
                    , t2.in_extend_value  AS 'in_l2_extend_value'
                    , t3.in_value         AS 'in_l3_value'
                    , t3.in_label         AS 'in_l3_label'
                    , t3.sort_order       AS 'in_l3_sort_order'
                    , t3.in_extend_value  AS 'in_l3_extend_value'
                FROM 
                    IN_SURVEY_OPTION t1 WITH(NOLOCK)
                LEFT OUTER JOIN (
                        SELECT IN_FILTER, IN_VALUE, IN_LABEL, SORT_ORDER, IN_EXPENSE_VALUE, IN_EXTEND_VALUE FROM IN_SURVEY_OPTION WITH(NOLOCK) WHERE SOURCE_ID = '{#in_l2_id}'
                    ) t2 ON t2.IN_FILTER = t1.IN_VALUE
                LEFT OUTER JOIN (
                        SELECT IN_FILTER, IN_VALUE, IN_LABEL, SORT_ORDER, IN_EXPENSE_VALUE, IN_EXTEND_VALUE FROM IN_SURVEY_OPTION WITH(NOLOCK) WHERE SOURCE_ID = '{#in_l3_id}'
                    ) t3 ON t3.IN_FILTER = t2.IN_VALUE
                WHERE
                    t1.SOURCE_ID = '{#in_l1_id}'
                ORDER BY
                    t1.SORT_ORDER
                    , t2.SORT_ORDER
                    , t3.SORT_ORDER
            ";

            sql = sql.Replace("{#in_l1_id}", in_l1_id)
                .Replace("{#in_l2_id}", in_l2_id)
                .Replace("{#in_l3_id}", in_l3_id);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return inn.applySQL(sql);
        }

        private int GetIntVal(string value, int def = 0)
        {
            int result = def;
            Int32.TryParse(value, out result);
            return result;
        }
    }
}