﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;
using System.Net;

namespace WorkHelp.Aras.Playground.Methods.PLMCTA
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class In_Register_Check : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
            目的: 驗證登入者資訊
            卡控:
                - 所屬區域
                - 會員狀態
            範例: 
                Item itmData = inn.newItem("In_Meeting_User");
                itmData.setProperty("inn_type", "In_Cla_Meeting");
                itmData.setProperty("inn_stype", "In_Cla_Meeting_Area");
                itmData.setProperty("inn_id", meetingid);

                itmData.setProperty("inn_mt_type", in_meeting_type);
                itmData.setProperty("mt_member_type", in_meeting_type);

                itmData.setProperty("inn_member_status", itmLogin.getProperty("in_member_status", ""));
                itmData.setProperty("inn_member_type", itmLogin.getProperty("in_member_type", ""));
                itmData.setProperty("inn_manager_area", itmLogin.getProperty("in_manager_area", ""));
                itmData.setProperty("inn_manager_org", itmLogin.getProperty("in_manager_org", ""));
                Item itmDataResult = itmData.apply("In_Register_Check");
            日期: 
                - 2022-01-17: 增加晉段卡控【合格會員】
                - 2021-01-04: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]In_Register_Check";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, this.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                inn = inn,

                inn_type = itmR.getProperty("inn_type", ""),
                inn_id = itmR.getProperty("inn_id", ""),
                
                inn_mt_type = itmR.getProperty("inn_mt_type", ""),
                mt_member_type = itmR.getProperty("mt_member_type", ""),

                inn_stype = itmR.getProperty("inn_stype", ""),
                inn_member_type = itmR.getProperty("inn_member_type", ""),
                inn_member_status = itmR.getProperty("inn_member_status", ""),
                inn_manager_area = itmR.getProperty("inn_manager_area", ""),
                inn_manager_org = itmR.getProperty("inn_manager_org", ""),
            };

            if (cfg.inn_member_type != "asc" && cfg.inn_member_type != "sys")
            {
                if (cfg.inn_mt_type == "degree")
                {
                    //lina 2022.01.17 合格會員才可報名名 _# START:
                    if (cfg.inn_member_status != "合格會員")
                    {
                        itmR.setProperty("message", "非合格會員，無法報名 <br> (請繳清會費後再行報名)");
                    }
                    else
                    {
                        Item itmAreas = GetMeetingAreas(cfg);
                        itmR.setProperty("message", CheckArea(cfg, itmAreas));
                    }
                    //lina 2022.01.17 合格會員才可報名名 _# END.
                }
                else if (cfg.inn_mt_type == "payment" && cfg.mt_member_type == "團體會員")
                {
                    if (cfg.inn_member_type != "vip_mbr")
                    {
                        itmR.setProperty("message", "非團體會員，無法報名 <br>");
                    }
                }
                else
                {
                    //檢查區域設定
                    Item itmAreas = GetMeetingAreas(cfg);
                    itmR.setProperty("message", CheckArea(cfg, itmAreas));
                }
            }

            return itmR;
        }

        /// <summary>
        /// 檢查區域設定
        /// </summary>
        private string CheckArea(TConfig cfg, Item itmAreas)
        {
            string result = "";

            int count = itmAreas.getItemCount();

            if (itmAreas.isError())
            {
                result = "區域設定資料錯誤";
            }
            else if (count > 0)
            {
                bool allow_register = false;
                for (int i = 0; i < count; i++)
                {
                    Item itmArea = itmAreas.getItemByIndex(i);
                    string in_area = itmArea.getProperty("in_area", "");
                    string is_allow = itmArea.getProperty("is_allow", "");
                    if (is_allow == "1" && in_area == cfg.inn_manager_area)
                    {
                        allow_register = true;
                    }
                }
                if (!allow_register)
                {
                    result = "所屬區域不可報名";
                }
            }

            return result;
        }

        /// <summary>
        /// 取得區域設定
        /// </summary>
        private Item GetMeetingAreas(TConfig cfg)
        {
            string sql = @"
                SELECT
                    t2.*
                FROM
                    {#meeting_type} t1 WITH(NOLOCK)
                INNER JOIN
                    {#meeting_stype} t2 WITH(NOLOCK)
                    ON t2.source_id = t1.id
                WHERE
                    t1.id = '{#meeting_id}'
            ";

            sql = sql.Replace("{#meeting_type}", cfg.inn_type)
                .Replace("{#meeting_stype}", cfg.inn_stype)
                .Replace("{#meeting_id}", cfg.inn_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public Innovator inn { get; set; }

            /// <summary>
            /// 活動 物件類型
            /// </summary>
            public string inn_type { get; set; }
            /// <summary>
            /// 活動區域設定 物件類型
            /// </summary>
            public string inn_stype { get; set; }
            /// <summary>
            /// 活動 id
            /// </summary>
            public string inn_id { get; set; }
            /// <summary>
            /// 活動 類型
            /// </summary>
            public string inn_mt_type { get; set; }
            /// <summary>
            /// 會費繳納-會員類型
            /// </summary>
            public string mt_member_type { get; set; }
            /// <summary>
            /// 登入者會員類型
            /// </summary>
            public string inn_member_type { get; set; }
            /// <summary>
            /// 登入者會員狀態
            /// </summary>
            public string inn_member_status { get; set; }
            /// <summary>
            /// 登入者所屬地區
            /// </summary>
            public string inn_manager_area { get; set; }
            /// <summary>
            /// 登入者上層單位 Resume id
            /// </summary>
            public string inn_manager_org { get; set; }
        }

    }
}
