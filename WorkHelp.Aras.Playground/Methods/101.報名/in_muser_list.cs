﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;
using System.Net;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class in_muser_list : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 換組
                日期: 
                    - 2021-10-29 創建 (Lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]in_muser_list";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,

                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                scene = itmR.getProperty("scene", ""),
            };

            string sql = "SELECT id, in_title, in_url, in_register_url FROM IN_MEETING WITH(NOLOCK)"
                + " WHERE id = '" + cfg.meeting_id + "'";
            cfg.itmMeeting = inn.applySQL(sql);

            switch(cfg.scene)
            {
                case "AAA":
                    break;

                default:
                    MeetingPage(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void MeetingPage(TConfig cfg, Item itmReturn)
        {
            itmReturn.setProperty("in_title", cfg.itmMeeting.getProperty("in_title", ""));
            itmReturn.setProperty("in_url", cfg.itmMeeting.getProperty("in_url", ""));
            itmReturn.setProperty("in_register_url", cfg.itmMeeting.getProperty("in_register_url", ""));


            //附加選單資訊
            Item itmJson = cfg.inn.newItem("In_Meeting");
            itmJson.setProperty("meeting_id", cfg.meeting_id);
            itmJson = itmJson.apply("in_meeting_program_options");
            itmReturn.setProperty("in_level_json", itmJson.getProperty("json", ""));
            itmReturn.setProperty("in_level_count", itmJson.getProperty("total", ""));
        }

        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string meeting_id { get; set; }
            public string scene { get; set; }
            public Item itmMeeting { get; set; }
        }
    }
}