﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;
using System.Net;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class In_Checking : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 檢查學校代碼,身分證,居留證,護照
                說明:
                    - 區分單位帳號與個人帳號
                輸入:
                    - In_Resume 欄位 (無法動態給欄位與值，一定要 In_Resume 有的 Property)
                輸出:
                    - 輸入的值
                位置:
                    - In_Resume 新增與修改
                日誌:
                    - 2020.09.18 改寫 (lina)
                注意事項:
                    - 因提供快速建立單位註冊，單位帳號與名稱檢查邏輯有異動時，需修改In_Meeting_InsCurrent_Validate
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]In_Checking";

            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "this dom: " + itmR.dom.InnerXml);

            string in_sno = itmR.getProperty("in_sno", "");
            string meeting_id = itmR.getProperty("in_meeting", "");
            string in_member_type = itmR.getProperty("in_member_type", "");
            string in_member_role = itmR.getProperty("in_member_role", "");

            //單位註冊 meeting_id 陣列
            string[] org_arr = new string[]
            {
        "83B87AE0033640AA8DBA7AF2CF659479", //道館、訓練站
        "38CAB90DF1274E048520A801948AC65C", //學校社團
        "5F73936711E04DC799CB02587F4FF7E0", //一般團體
            };

            bool is_gym_registry = org_arr.Contains(meeting_id); //單位
            bool is_own_registry = meeting_id == "249FDB244E534EB0AA66C8E9C470E930"; //個人

            //lina 2021.01.26 for 縣市委員會成員特殊帳號設計
            string[] bypass_roles = new string[] { "cmt_8100", "cmt_8200" };
            bool is_bypass = in_member_type == "area_cmt" && bypass_roles.Contains(in_member_role);

            if (is_bypass)
            {
                //跳過證號驗證
                return itmR;
            }


            if (in_sno.Contains(" ") || in_sno.Contains("　"))
            {
                throw new Exception("身分證字號有空白,請修正!");
                //strError += "身分證字號有空白,請修正!" + "\n";
            }

            if (is_gym_registry)
            {
                //驗證單位帳號
                //throw new Exception ("驗證單位帳號");
                CCO.Utilities.WriteDebug(strMethodName, "註冊單位帳號: " + in_sno);
                CheckOrgAccount(CCO, strMethodName, inn, itmR);
            }
            else if (is_own_registry)
            {
                //驗證個人帳號
                //throw new Exception ("驗證個人帳號");
                CCO.Utilities.WriteDebug(strMethodName, "註冊個人帳號: " + in_sno);
                CheckOwnAccount(CCO, strMethodName, inn, itmR);
            }
            else
            {
                //throw new Exception ("一般報名(個人帳號驗證模式)");
                CCO.Utilities.WriteDebug(strMethodName, "一般報名(個人帳號驗證模式): " + in_sno);
                CheckOwnAccount(CCO, strMethodName, inn, itmR);
            }

            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

            //throw new Exception("STOP (流程不要往下)");

            return itmR;
        }

        //驗證單位帳號
        private void CheckOrgAccount(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            List<string> errs = new List<string>();

            //In_Resume 帳號
            string identity_card = itmReturn.getProperty("in_sno", "").ToUpper();
            //In_Resume 所屬單位
            string in_current_org = itmReturn.getProperty("in_current_org", "");

            //In_Resume 帳號長度
            int id_len = identity_card.Length;

            //單位帳號格式 A~Z 0~9(9)(8~12位英數字)
            var custom_regex = new System.Text.RegularExpressions.Regex("^[A-Za-z0-9]{8,12}$");

            //判定類型
            string id_type = "";

            //長度判定
            if (id_len == 4) //學校代碼
            {
                id_type = "school_code";
            }
            else if (id_len == 6) //學校代碼
            {
                id_type = "school_code";
            }
            else if (id_len >= 8 && id_len <= 12)
            {
                if (custom_regex.IsMatch(identity_card))
                {
                    if (!IsAllNumOrAllLetter(identity_card))
                    {
                        id_type = "custom_code";
                    }
                    else
                    {
                        errs.Add("單位帳號格式必須為英文與數字組合");
                    }
                }
                else
                {
                    errs.Add("單位帳號格式錯誤");
                }
            }
            else
            {
                errs.Add("單位帳號格式錯誤，請重新檢查");
            }

            switch (id_type)
            {
                case "school_code": //學校代碼
                    itmReturn.setProperty("state", "ok");
                    itmReturn.setProperty("message", "此為學校代碼");
                    break;

                case "custom_code": //單位帳號
                    itmReturn.setProperty("state", "ok");
                    itmReturn.setProperty("message", "此為單位帳號");
                    break;
            }

            //檢查所屬單位特殊字元
            CheckCurrentOrg(errs, in_current_org);

            if (errs.Count > 0)
            {
                throw new Exception(string.Join(" <br> ", errs));
            }
        }

        //驗證個人帳號
        private void CheckOwnAccount(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            List<string> errs = new List<string>();

            //In_Resume 帳號
            string identity_card = itmReturn.getProperty("in_sno", "").ToUpper();
            //In_Resume 性別
            string in_gender = itmReturn.getProperty("in_gender", "");
            //In_Resume 西元生日
            string in_birth = itmReturn.getProperty("in_birth", "").Split('T')[0];
            //In_Resume 所屬單位
            string in_current_org = itmReturn.getProperty("in_current_org", "");

            DateTime CurrentTime = System.DateTime.Today;

            //In_Resume 帳號長度
            int id_len = identity_card.Length;

            //檢查格式A~Z(1) 0~9(9)(台灣身分證)
            var regex = new System.Text.RegularExpressions.Regex("^[A-Z]{1}[0-9]{9}$");
            //檢查格式A~Z(1)A~Z(2) 0~9(8)(居留證)
            var pat = new System.Text.RegularExpressions.Regex("^[A-Z]{1}[A-Z]{1}[0-9]{8}$");
            //檢查格式A~Z(1)3~0(2) 0~9(8)(居留證)
            var pat2 = new System.Text.RegularExpressions.Regex("^[A-Z]{1}[89]{1}[0-9]{8}$");
            //檢查格式A~Z(1)A~Z(2) 0~9(6)(外國護照)
            var pas = new System.Text.RegularExpressions.Regex("^[A-Z]{1}[A-Z]{1}[0-9]{6}$");

            //判定類型
            string id_type = ""; //taiwanid,passport,bbb,cc

            if (id_len == 8) //外國護照
            {
                id_type = "passport";
            }
            else if (id_len == 10) //身分證 or 居留證
            {
                if (pat2.IsMatch(identity_card)) //居留證
                {
                    id_type = "resident_permit2";
                }
                else if (regex.IsMatch(identity_card)) //身分證
                {
                    id_type = "identity_card";
                }
                else if (pat.IsMatch(identity_card)) //居留證
                {
                    id_type = "resident_permit";
                }
                // else if (identity_card.Substring(4,1) =="M") //縣市協會成員無身分證
                // {
                //     id_type = "CommitteeStaff";
                // }
                else
                {
                    //不符合以上兩個格式
                    //throw new Exception("身分證_居留證_格式錯誤");
                    errs.Add("證號格式錯誤");
                }
            }
            else if (id_len >= 11 && id_len <= 12)//港澳居留
            {
                id_type = "HKM_code";
            }
            else if (id_len >= 18 && id_len <= 20) //中國身分證or居留證
            {
                id_type = "china_code";
            }
            else
            {
                errs.Add("個人帳號格式錯誤，請重新檢查");
            }

            string gender = ""; //性別

            switch (id_type)
            {
                case "china_code": //中國身分證
                    CheckIDCards(identity_card); //18碼驗證

                    //檢查[性別]是否與身分證相符
                    if (Int32.Parse(identity_card.Substring(16, 1)) % 2 == 1)
                    {
                        //奇數為男生
                        gender = "男";
                    }
                    else
                    {
                        //奇數為女生
                        gender = "女";
                    }

                    if (in_gender != gender)
                    {
                        throw new Exception("所選擇之性別與身分證不相符");
                    }

                    //檢查[生日]是否與身分證相符
                    CCO.Utilities.WriteDebug("In_Checking", "生日切割:" + in_birth.Replace("-", ""));
                    CCO.Utilities.WriteDebug("In_Checking", "大陸生日:" + identity_card.Substring(6, 8));

                    if (in_birth.Replace("-", "") != identity_card.Substring(6, 8))
                    {
                        throw new Exception("所輸入之生日與身分證不相符");
                    }

                    itmReturn.setProperty("state", "ok");
                    itmReturn.setProperty("message", "此為中國身分證");
                    break;

                case "passport": //外國護照
                    if (!pas.IsMatch(identity_card)) //身分證
                    {
                        errs.Add("護照格式錯誤");
                    }
                    else
                    {
                        itmReturn.setProperty("state", "ok");
                        itmReturn.setProperty("message", "此為外國護照");
                    }
                    break;

                case "identity_card":

                    //存放檢查碼之外的數字
                    int[] seed = new int[10];
                    //字母陣列
                    string[] charMapping = new string[] { "A", "B", "C", "D", "E", "F", "G", "H", "J", "K", "L", "M", "N", "P", "Q", "R", "S", "T", "U", "V", "X", "Y", "W", "Z", "I", "O" };

                    string target = identity_card.Substring(0, 1);

                    //依據身分證第2碼判定性別
                    if (identity_card.Substring(1, 1) == "1")
                    {
                        gender = "男";
                        itmReturn.setProperty("inn_gender", "1");
                    }
                    else if (identity_card.Substring(1, 1) == "2")
                    {
                        gender = "女";
                        itmReturn.setProperty("inn_gender", "2");
                    }

                    if (in_gender != gender)
                    {
                        errs.Add("性別驗證錯誤");
                    }

                    for (int index = 0; index < charMapping.Length; index++)
                    {
                        if (charMapping[index] == target)
                        {
                            index += 10;
                            seed[0] = index / 10;
                            seed[1] = (index % 10) * 9;
                            break;
                        }
                    }
                    for (int index = 2; index < 10; index++)
                    {
                        seed[index] = Convert.ToInt32(identity_card.Substring(index - 1, 1)) * (10 - index);
                    }
                    //檢查是否為正確的身分證(依據身分證檢查規則)
                    if ((10 - (seed.Sum() % 10)) % 10 != Convert.ToInt32(identity_card.Substring(9, 1)))
                    {
                        itmReturn.setProperty("state", "ng");
                        itmReturn.setProperty("message", "請輸入正確身分證");
                        errs.Add("請輸入正確身分證");
                    }
                    else
                    {
                        itmReturn.setProperty("state", "ok");
                        itmReturn.setProperty("message", "身分證號碼正確");
                    }
                    break;

                case "resident_permit": //居留證

                    string sex = "";
                    string nationality = "";
                    char[] strArr = identity_card.ToCharArray(); // 字串轉成char陣列

                    int verifyNum = 0;
                    int[] pidResidentFirstInt = { 1, 10, 9, 8, 7, 6, 5, 4, 9, 3, 2, 2, 11, 10, 8, 9, 8, 7, 6, 5, 4, 3, 11, 3, 12, 10 };
                    char[] pidCharArray = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };
                    // 第一碼
                    verifyNum += pidResidentFirstInt[Array.BinarySearch(pidCharArray, strArr[0])];
                    // 原居留證第二碼英文字應轉換為10~33，並僅取個位數*6，這裡直接取[(個位數*6) mod 10]
                    int[] pidResidentSecondInt = { 0, 8, 6, 4, 2, 0, 8, 6, 2, 4, 2, 0, 8, 6, 0, 4, 2, 0, 8, 6, 4, 2, 6, 0, 8, 4 };
                    // 第二碼
                    verifyNum += pidResidentSecondInt[Array.BinarySearch(pidCharArray, strArr[1])];
                    // 第三~八碼
                    for (int i = 2, j = 7; i < 9; i++, j--)
                    {
                        verifyNum += Convert.ToInt32(strArr[i].ToString(), 10) * j;
                    }
                    // 檢查碼
                    verifyNum = (10 - (verifyNum % 10)) % 10;
                    bool ok = verifyNum == Convert.ToInt32(strArr[9].ToString(), 10);
                    if (ok)
                    {
                        // 判斷性別 & 國籍
                        sex = "男";
                        if (strArr[1] == 'B' || strArr[1] == 'D') sex = "女";
                        nationality = "外籍人士";
                        if (strArr[1] == 'A' || strArr[1] == 'B') nationality += "(臺灣地區無戶籍國民、大陸地區人民、港澳居民)";
                        {
                            itmReturn.setProperty("state", "ok");
                            itmReturn.setProperty("message", "此為居留證");
                        }
                    }
                    else
                    {
                        itmReturn.setProperty("state", "ng");
                        itmReturn.setProperty("message", "請輸入正確居留證");
                        errs.Add("請輸入正確居留證");
                    }
                    break;

                case "resident_permit2": //居留證
                    var resident_result = CheckResidentSno(identity_card, itmReturn);
                    if (resident_result.errs.Count == 0)
                    {
                        itmReturn.setProperty("state", "ok");
                        itmReturn.setProperty("message", "此為居留證(new)");
                    }
                    else
                    {
                        errs.AddRange(resident_result.errs);
                    }
                    break;

                case "HKM_code": //港澳居留
                    itmReturn.setProperty("state", "ok");
                    itmReturn.setProperty("message", "此为港澳居留证");
                    break;
            }

            //生日定判定
            switch (id_type)
            {
                case "china_code": //中國身分證
                    itmReturn.setProperty("province_code", identity_card.Substring(0, 2)); //省份碼
                    itmReturn.setProperty("gender_code", identity_card.Substring(6, 8)); //生日
                    itmReturn.setProperty("birth_code", identity_card.Substring(16, 1)); //性別碼
                                                                                         //CCO.Utilities.WriteDebug("In_Checking", "省份碼:" + identity_card.Substring(0,2) + ",生日:" + identity_card.Substring(6,8) + ",性別碼:" + identity_card.Substring(16,1));
                    itmReturn.setProperty("state", "ok");
                    itmReturn.setProperty("message", "此為中國身分證");
                    break;

                case "resident_permit": //居留證
                case "resident_permit2": //居留證-格式2
                case "passport": //外國護照
                case "identity_card": //身分證
                case "HKM_code"://港澳居留证

                    //取得生日
                    string birth = itmReturn.getProperty("in_birth", CurrentTime.ToString("yyyy/MM/dd"));
                    //取得現在時間
                    string nowtime = DateTime.Now.ToString("yyyy/MM/dd");

                    DateTime dt1 = DateTime.Parse(birth); //接收的生日
                    DateTime dt2 = DateTime.Parse(nowtime); //當天的日期

                    TimeSpan ts = dt2.Subtract(dt1); //比較
                    double day = ts.TotalDays; //生日到當天的天數
                    int d = Convert.ToInt16(Math.Floor(day / 365));

                    string cccc = CurrentTime.AddYears(-5).ToString("yyyy/MM/dd");

                    itmReturn.setProperty("NowTime", cccc);

                    //年齡小於10歲
                    if (d < 3)
                    {
                        //itmReturn.setProperty("state","ng");
                        itmReturn.setProperty("age", "小於三歲");
                        errs.Add("生日驗證錯誤!" + "輸入生日為" + "(" + birth.Split('T')[0] + ")" + "未達可報名年齡");
                    }
                    else
                    {
                        //itmReturn.setProperty("state","ok");
                        itmReturn.setProperty("age", "大於三歲");
                    }
                    break;
            }

            //檢查所屬單位特殊字元
            CheckCurrentOrg(errs, in_current_org);

            if (errs.Count > 0)
            {
                throw new Exception(string.Join(" <br> ", errs));
            }
        }

        //檢查所屬單位是否有特殊字元存在
        private void CheckCurrentOrg(List<string> errs, string in_current_org)
        {
            string[] Special_text = new string[] { "?", "=", ".", "*", "[", "@", "#", "$", "%", "^", "&", ".", "+", "-", "]", " ", "/", @"\" }; //特殊字元

            if (!string.IsNullOrWhiteSpace(in_current_org))
            {
                foreach (string _text in Special_text)
                {
                    if (in_current_org.Contains(_text))
                    {
                        errs.Add("所屬單位包含特殊字元");
                        break;
                    }
                }
            }
        }

        //大陸身分證驗證(18碼)
        private bool CheckIDCards(string Id)
        {
            int intLen = Id.Length;
            long n = 0;

            if (intLen == 18)
            {
                if (long.TryParse(Id.Remove(17), out n) == false || n < Math.Pow(10, 16) || long.TryParse(Id.Replace('x', '0').Replace('X', '0'), out n) == false)
                {
                    //strError += "數字格式錯誤" + "\n";
                    throw new Exception("數字格式錯誤");
                    //return false;//数字验证
                }
                string address = "11x22x35x44x53x12x23x36x45x54x13x31x37x46x61x14x32x41x50x62x15x33x42x51x63x21x34x43x52x64x65x71x81x82x91";
                if (address.IndexOf(Id.Remove(2)) == -1)
                {
                    //strError += "省份驗證錯誤" + "\n";
                    throw new Exception("省份驗證錯誤");
                    //return false;//省份验证
                }
                string birth = Id.Substring(6, 8).Insert(6, "-").Insert(4, "-");
                DateTime time = new DateTime();
                if (DateTime.TryParse(birth, out time) == false)
                {
                    //strError += "生日驗證錯誤" + "\n";
                    throw new Exception("生日驗證錯誤");
                    //return false;//生日验证
                }
                string[] arrVarifyCode = ("1,0,x,9,8,7,6,5,4,3,2").Split(',');
                string[] Wi = ("7,9,10,5,8,4,2,1,6,3,7,9,10,5,8,4,2").Split(',');
                char[] Ai = Id.Remove(17).ToCharArray();
                int sum = 0;
                for (int i = 0; i < 17; i++)
                {
                    sum += int.Parse(Wi[i]) * int.Parse(Ai[i].ToString());
                }
                int y = -1;
                Math.DivRem(sum, 11, out y);
                if (arrVarifyCode[y] != Id.Substring(17, 1).ToLower())
                {
                    //strError += "校驗碼錯誤" + "\n";
                    throw new Exception("校驗碼錯誤");
                    //return false;//校验码验证
                }
                return true; //符合GB11643-1999標準
            }
            else if (intLen == 15)
            {
                //舊身分證為15碼 為了驗證故轉成18碼
                return CheckIDCards(Per15To18(Id));
            }
            else
            {
                //strError += "身分證碼數錯誤" + "\n";
                throw new Exception("身分證碼數錯誤");
                //return false;//位数不对
            }
        }

        //15碼轉18碼(舊身分證為15碼 驗證須轉18碼)
        private string Per15To18(string perIDSrc)
        {
            int iS = 0;
            //加權因子常數
            int[] iW = new int[] { 7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2 };
            //校驗碼常數
            string LastCode = "10X98765432";
            //新身分證號
            string perIDNew;
            perIDNew = perIDSrc.Substring(0, 6);
            //填在第6位及第7位填上'1','9'兩個數字
            perIDNew += "19";
            perIDNew += perIDSrc.Substring(6, 9);

            //進行加權求和
            for (int i = 0; i < 17; i++)
            {
                iS += int.Parse(perIDNew.Substring(i, 1)) * iW[i];
            }

            //取模運算,得到模值
            int iY = iS % 11;
            //從LastCode中取得已模為索引號的值,加到身分證的最後一位,即為新身分證號
            perIDNew += LastCode.Substring(iY, 1);
            return perIDNew;

        }
        
        private class TPerson
        { 
            public string gender { get; set; }
            public List<string> errs { get; set; }
        }

        private TPerson CheckResidentSno(string identity_card, Item itmReturn)
        {
            var result = new TPerson 
            {
                gender = "",
                errs = new List<string>(),
            };

            //In_Resume 性別
            string in_gender = itmReturn.getProperty("in_gender", "");


            //存放檢查碼之外的數字
            int[] seed = new int[10];
            //字母陣列
            string[] charMapping = new string[] { "A", "B", "C", "D", "E", "F", "G", "H", "J", "K", "L", "M", "N", "P", "Q", "R", "S", "T", "U", "V", "X", "Y", "W", "Z", "I", "O" };

            string target = identity_card.Substring(0, 1);

            //依據身分證第2碼判定性別
            if (identity_card.Substring(1, 1) == "8")
            {
                result.gender = "男";
                itmReturn.setProperty("inn_gender", "1");
            }
            else if (identity_card.Substring(1, 1) == "2")
            {
                result.gender = "女";
                itmReturn.setProperty("inn_gender", "2");
            }

            if (in_gender != result.gender)
            {
                result.errs.Add("性別驗證錯誤");
            }

            for (int index = 0; index < charMapping.Length; index++)
            {
                if (charMapping[index] == target)
                {
                    index += 10;
                    seed[0] = index / 10;
                    seed[1] = (index % 10) * 9;
                    break;
                }
            }
            for (int index = 2; index < 10; index++)
            {
                seed[index] = Convert.ToInt32(identity_card.Substring(index - 1, 1)) * (10 - index);
            }
            //檢查是否為正確的身分證(依據身分證檢查規則)
            if ((10 - (seed.Sum() % 10)) % 10 != Convert.ToInt32(identity_card.Substring(9, 1)))
            {
                itmReturn.setProperty("state", "ng");
                itmReturn.setProperty("message", "請輸入正確身分證");
                result.errs.Add("請輸入正確身分證");
            }
            else
            {
                itmReturn.setProperty("state", "ok");
                itmReturn.setProperty("message", "身分證號碼正確");
            }
            return result;
        }

        /// <summary>
        /// 檢查帳號是否全為數字或全為字母
        /// </summary>
        private bool IsAllNumOrAllLetter(string value)
        {
            int len = value.Length;

            int num_count = 0;
            int letter_count = 0;

            foreach (char c in value)
            {
                if (char.IsNumber(c)) num_count++;
                if (char.IsLetter(c)) letter_count++;
            }

            if (num_count == len || letter_count == len)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}