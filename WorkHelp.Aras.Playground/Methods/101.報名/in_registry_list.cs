﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class in_registry_list : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
            目的: 報名結果
            輸入: 
               - meeting id
               - 查詢條件
            日誌:
               - 2021.04.29 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]in_registry_list";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                InnH = _InnH,

                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                mode = itmR.getProperty("mode", ""),
                scene = itmR.getProperty("scene", ""),

                MtName = "IN_MEETING",
                MUserName = "IN_MEETING_USER",
            };

            if (cfg.mode == "cla")
            {
                cfg.MtName = "IN_CLA_MEETING";
                cfg.MUserName = "IN_CLA_MEETING_USER";
            }

            if (cfg.meeting_id == "")
            {
                return itmR;
            }

            if (cfg.scene == "")
            {
                cfg.itmMeeting = cfg.inn.applySQL("SELECT in_title, in_meeting_type FROM " + cfg.MtName + " WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
                cfg.in_title = cfg.itmMeeting.getProperty("in_title", "");
                cfg.in_meeting_type = cfg.itmMeeting.getProperty("in_meeting_type", "");
                cfg.mt_type = GetMtType(cfg.in_meeting_type);

                AppendTabs(cfg, itmR);
            }

            itmR.setProperty("in_title", cfg.in_title);

            return itmR;
        }

        private MTType GetMtType(string value)
        {
            switch (value)
            {
                case "game": return MTType.Game;
                case "seminar": return MTType.Seminar;
                case "degree": return MTType.Degree;
                default: return MTType.None;
            }
        }

        private void AppendTabs(TConfig cfg, Item itmReturn)
        {
            List<TTab> tabs = new List<TTab>();
            switch (cfg.mt_type)
            {
                case MTType.Game:
                    tabs.Add(new TTab { id = "muser_noverify", active = "active", title = "未審核", GetListFunc = GetListNoVerify, AppendListAction = AppendGameList });
                    tabs.Add(new TTab { id = "muser_agree", active = "", title = "審核通過", GetListFunc = GetListAgree, AppendListAction = AppendGameList });
                    tabs.Add(new TTab { id = "muser_reject", active = "", title = "審核未通過", GetListFunc = GetListReject, AppendListAction = AppendGameList, is_reject = true });
                    break;

                case MTType.Seminar:
                    tabs.Add(new TTab { id = "muser_noverify", active = "active", title = "未審核", GetListFunc = GetListNoVerify, AppendListAction = AppendSeminarList });
                    tabs.Add(new TTab { id = "muser_agree", active = "", title = "審核通過", GetListFunc = GetListAgree, AppendListAction = AppendSeminarList });
                    tabs.Add(new TTab { id = "muser_reject", active = "", title = "審核未通過", GetListFunc = GetListReject, AppendListAction = AppendSeminarList, is_reject = true });
                    break;

                case MTType.Degree:
                    tabs.Add(new TTab { id = "muser_noverify", active = "active", title = "未審核", GetListFunc = GetListNoVerify, AppendListAction = AppendDegreeList });
                    tabs.Add(new TTab { id = "muser_agree", active = "", title = "審核通過", GetListFunc = GetListAgree, AppendListAction = AppendDegreeList });
                    tabs.Add(new TTab { id = "muser_reject", active = "", title = "審核未通過", GetListFunc = GetListReject, AppendListAction = AppendDegreeList, is_reject = true });
                    break;

                default:
                    tabs.Add(new TTab { id = "muser_noverify", active = "active", title = "未審核", GetListFunc = GetListNoVerify, AppendListAction = AppendSeminarList });
                    tabs.Add(new TTab { id = "muser_agree", active = "", title = "審核通過", GetListFunc = GetListAgree, AppendListAction = AppendSeminarList });
                    tabs.Add(new TTab { id = "muser_reject", active = "", title = "審核未通過", GetListFunc = GetListReject, AppendListAction = AppendSeminarList, is_reject = true });
                    break;
            }

            foreach (var tab in tabs)
            {
                FixTab(cfg, tab);
            }

            StringBuilder builder = new StringBuilder();

            builder.Append("<ul class='nav nav-pills' style='justify-content: start ;' id='pills-tab' role='tablist'>");
            AppendTabHeads(cfg, tabs, builder);
            builder.Append("</ul>");

            builder.Append("<div class='tab-content' id='pills-tabContent'>");
            AppendTabContents(cfg, tabs, builder);
            builder.Append("</div>");

            itmReturn.setProperty("inn_tabs", builder.ToString());
        }

        private void AppendTabHeads(TConfig cfg, List<TTab> tabs, StringBuilder builder)
        {
            foreach (var tab in tabs)
            {
                if (tab.is_error) continue;

                builder.Append("<li id='" + tab.li_id + "' class='nav-item " + tab.active + "'>");
                builder.Append("<a id='" + tab.a_id + "' href='#" + tab.content_id + "' class='nav-link' data-toggle='pill' role='tab' aria-controls='pills-home' aria-selected='true' onclick='StoreTab_Click(this)'>");
                builder.Append(tab.title + "(" + tab.count + ")");
                builder.Append("</a>");
                builder.Append("</li>");
            }
        }

        private void AppendTabContents(TConfig cfg, List<TTab> tabs, StringBuilder builder)
        {
            foreach (var tab in tabs)
            {
                if (tab.is_error) continue;

                builder.Append("<div class='tab-pane fade in " + tab.active + "' id='" + tab.content_id + "' role='tabpanel'>");
                builder.Append("    <div class='showsheet_ clearfix'>");
                //builder.Append("        <div class='box-header'>");
                //builder.Append("            <h4 class='box-header with-border'>");
                //builder.Append(tab.title);
                //builder.Append("                <small>(0)");
                //builder.Append("                </small>");
                //builder.Append("            </h4>");
                //builder.Append("            <div class='box-tools pull-right'>");
                //builder.Append("                <button type='button' class='btn btn-box-tool fold_type' data-widget='collapse' style='width: 40px; height: 20px;'>");
                //builder.Append("                    <a id='" + tab.toggle_id + "' class='accordion-toggle box-title form-box-title fa fa-plus' style='font-size: 25px;' data-toggle='collapse' data-parent='#accordion' href='#" + tab.folder_id + "'>");
                //builder.Append("                    </a>");
                //builder.Append("                </button>");
                //builder.Append("            </div>");
                //builder.Append("        </div>");

                //builder.Append("        <div id='" + tab.folder_id + "' class='collapse'>");
                builder.Append("        <div id='" + tab.folder_id + "'>");
                builder.Append("            <div class='float-btn clearfix'>");
                builder.Append("            </div>");

                tab.AppendListAction(cfg, tab, builder);

                builder.Append("        </div>");


                builder.Append("    </div>");
                builder.Append("</div>");
            }
        }

        #region 比賽

        private void AppendGameList(TConfig cfg, TTab tab, StringBuilder builder)
        {
            Dictionary<string, List<TMUser>> map = MapMUsers(tab.items);

            var no = 1;
            foreach (var kv in map)
            {
                var source = kv.Value;
                var list = source.OrderBy(x => x.sort_order).ToList();

                var table = new TTable
                {
                    id = tab.table_id + "_" + no,
                    title = kv.Key,
                    count = list.Count,
                };

                AppendGameTable(cfg, tab, table, list, builder);

                no++;
            }
        }

        private void AppendGameTable(TConfig cfg, TTab tab, TTable table, List<TMUser> list, StringBuilder builder)
        {
            List<TField> fields = new List<TField>();
            fields.Add(new TField { title = "序號", property = "no" });
            fields.Add(new TField { title = "所屬單位", property = "in_current_org" });
            fields.Add(new TField { title = "姓名", property = "in_name" });
            fields.Add(new TField { title = "身分證字號", property = "in_sno_display" });

            if (table.title == "隊職員")
            {
                fields.Add(new TField { title = "項目", property = "in_l1" });
                fields.Add(new TField { title = "職務", property = "in_l2" });
            }
            else
            {
                fields.Add(new TField { title = "競賽項目", property = "in_l1" });
                fields.Add(new TField { title = "競賽組別", property = "in_l2" });
                fields.Add(new TField { title = "競賽分級", property = "in_l3" });
            }

            fields.Add(new TField { title = "公假單位", property = "in_official_leave_org" });

            if (tab.is_reject)
            {
                fields.Add(new TField { title = "審核說明", property = "" });//in_ass_ver_memo
            }

            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();


            head.Append("<thead>");
            head.Append("  <tr>");
            for (int i = 0; i < fields.Count; i++)
            {
                TField field = fields[i];
                head.Append("    <th>" + field.title + "</th>");
            }
            head.Append("  </tr>");
            head.Append("</thead>");


            body.Append("<tbody>");
            for (int i = 0; i < list.Count; i++)
            {
                TMUser musr = list[i];
                Item item = musr.Value;
                item.setProperty("no", (i + 1).ToString());

                AppendRow(cfg, tab, fields, item, body);
            }
            body.Append("</tbody>");

            builder.Append("<h4 class='box-header with-border'>" + table.title + " (" + table.count + ")</h4>");
            builder.Append("<table id='" + table.id + "' class='table table-hover table-bordered table-rwd rwd'>");
            builder.Append(head);
            builder.Append(body);
            builder.Append("</table>");
        }

        #endregion 比賽

        #region 講習

        private void AppendSeminarList(TConfig cfg, TTab tab, StringBuilder builder)
        {
            List<TField> fields = new List<TField>();
            fields.Add(new TField { title = "序號", property = "no" });
            fields.Add(new TField { title = "項目", property = "in_l1" });
            fields.Add(new TField { title = "所屬單位", property = "in_current_org" });
            fields.Add(new TField { title = "姓名", property = "in_name" });
            fields.Add(new TField { title = "身分證字號", property = "in_sno_display" });

            if (tab.is_reject)
            {
                fields.Add(new TField { title = "審核說明", property = "" });//in_ass_ver_memo
            }

            Dictionary<string, List<TMUser>> map = MapMUsers(tab.items);

            var no = 1;
            foreach (var kv in map)
            {
                var source = kv.Value;
                var list = source.OrderBy(x => x.sort_order).ToList();

                var table = new TTable
                {
                    id = tab.table_id + "_" + no,
                    title = kv.Key,
                    count = list.Count,
                };

                AppendTable(cfg, tab, table, fields, list, builder);

                no++;
            }
        }
        #endregion 講習

        #region 晉段

        private void AppendDegreeList(TConfig cfg, TTab tab, StringBuilder builder)
        {
            List<TField> fields = new List<TField>();
            fields.Add(new TField { title = "序號", property = "no" });
            fields.Add(new TField { title = "所屬委員會", property = "committee_short_name" });
            fields.Add(new TField { title = "所屬單位", property = "in_current_org" });
            fields.Add(new TField { title = "姓名", property = "in_name" });
            fields.Add(new TField { title = "身分證字號", property = "in_sno_display" });
            fields.Add(new TField { title = "申請段位", property = "in_l1" });

            if (tab.is_reject)
            {
                fields.Add(new TField { title = "審核說明", property = "" });//in_ass_ver_memo
            }

            Dictionary<string, List<TMUser>> map = MapMUsers(tab.items);

            var no = 1;
            foreach (var kv in map)
            {
                var source = kv.Value;
                var list = source.OrderBy(x => x.sort_order).ToList();

                var table = new TTable
                {
                    id = tab.table_id + "_" + no,
                    title = kv.Key,
                    count = list.Count,
                };

                AppendTable(cfg, tab, table, fields, list, builder);

                no++;
            }
        }
        #endregion 晉段


        private void AppendTable(TConfig cfg, TTab tab, TTable table, List<TField> fields, List<TMUser> list, StringBuilder builder)
        {
            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();


            head.Append("<thead>");
            head.Append("  <tr>");
            for (int i = 0; i < fields.Count; i++)
            {
                TField field = fields[i];
                head.Append("    <th>" + field.title + "</th>");
            }
            head.Append("  </tr>");
            head.Append("</thead>");


            body.Append("<tbody>");
            for (int i = 0; i < list.Count; i++)
            {
                TMUser musr = list[i];
                Item item = musr.Value;
                item.setProperty("no", (i + 1).ToString());

                AppendRow(cfg, tab, fields, item, body);
            }
            body.Append("</tbody>");

            builder.Append("<h4 class='box-header with-border'>" + table.title + " (" + table.count + ")</h4>");
            builder.Append("<table id='" + table.id + "' class='table table-hover table-bordered table-rwd rwd'>");
            builder.Append(head);
            builder.Append(body);
            builder.Append("</table>");
        }

        private void AppendRow(TConfig cfg, TTab tab, List<TField> fields, Item item, StringBuilder body)
        {
            body.Append("<tr>");
            for (int i = 0; i < fields.Count; i++)
            {
                TField field = fields[i];
                string value = "";
                if (field.getVal != null)
                {
                    value = field.getVal(cfg, tab, field, item);
                }
                else if (field.property != "")
                {
                    value = item.getProperty(field.property, "");
                }

                body.Append("<td>" + value + "</td>");
            }
            body.Append("</tr>");
        }

        private void FixTab(TConfig cfg, TTab tab)
        {
            tab.li_id = "li_" + tab.id;
            tab.a_id = "a_" + tab.id;
            tab.content_id = "box_" + tab.id;
            tab.folder_id = "folder_" + tab.id;
            tab.toggle_id = "toggle_" + tab.id;
            tab.table_id = "table_" + tab.id;
            tab.items = tab.GetListFunc(cfg, tab);

            tab.is_error = tab.items.isError();

            tab.count = !tab.is_error && tab.items.getResult() != ""
                ? tab.items.getItemCount()
                : 0;
        }

        private Item GetListNoVerify(TConfig cfg, TTab tab)
        {
            return GetList(cfg, tab, "");
        }

        private Item GetListAgree(TConfig cfg, TTab tab)
        {
            return GetList(cfg, tab, "1");
        }

        private Item GetListReject(TConfig cfg, TTab tab)
        {
            return GetList(cfg, tab, "0");
        }

        private Item GetList(TConfig cfg, TTab tab, string in_ass_ver_result)
        {
            string sql = "";
            string ver_condition = "";
            if (in_ass_ver_result == "")
            {
                ver_condition = "AND ISNULL(t1.in_ass_ver_result, '') = ''";
            }
            else
            {
                ver_condition = "AND ISNULL(t1.in_ass_ver_result, '') = '" + in_ass_ver_result + "'";
            }

            switch (cfg.mt_type)
            {
                case MTType.Game:
                    sql = @"
                        SELECT 
                	        t1.*
                        FROM 
                	        {#MUserName} t1 WITH(NOLOCK)
                        WHERE
                	        t1.source_id = '{#meeting_id}'
                	        {#ver_condition}
                        ORDER BY
                	        t1.in_l1
                	        , t1.in_l2
                	        , t1.in_l3
                	        , t1.in_current_org
                	        , t1.in_birth
                    ";
                    break;

                case MTType.Seminar:
                    sql = @"
                        SELECT 
                	        t1.*
                        FROM 
                	        {#MUserName} t1 WITH(NOLOCK)
                        WHERE
                	        t1.source_id = '{#meeting_id}'
                	        {#ver_condition}
                        ORDER BY
                	        t1.in_l1
                	        , t1.in_l2
                	        , t1.in_l3
                	        , t1.in_degree DESC
                	        , t1.in_birth
                    ";
                    break;

                case MTType.Degree:
                    sql = @"
                        SELECT 
                	        t1.*
                            , t2.in_short_org AS 'committee_short_name'
                        FROM 
                	        {#MUserName} t1 WITH(NOLOCK)
                        LEFT OUTER JOIN
                            IN_RESUME t2 WiTH(NOLOCK)
                            ON t2.in_name = t1.in_committee AND t2.in_member_type = 'area_cmt' AND t2.in_member_role = 'sys_9999'
                        WHERE
                	        t1.source_id = '{#meeting_id}'
                	        {#ver_condition}
                        ORDER BY
                	        t1.in_l1
                	        , t1.in_l2
                	        , t1.in_l3
                	        , t1.in_degree DESC
                	        , t1.in_birth
                    ";
                    break;

                default:
                    sql = @"
                        SELECT 
                	        t1.*
                        FROM 
                	        {#MUserName} t1 WITH(NOLOCK)
                        WHERE
                	        t1.source_id = '{#meeting_id}'
                	        {#ver_condition}
                        ORDER BY
                	        t1.in_l1
                	        , t1.in_l2
                	        , t1.in_l3
                	        , t1.in_degree DESC
                	        , t1.in_birth
                    ";
                    break;
            }


            sql = sql.Replace("{#MUserName}", cfg.MUserName)
                .Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#ver_condition}", ver_condition);
            //.Replace("{#in_note_state}", "official");

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        private Dictionary<string, List<TMUser>> MapMUsers(Item items)
        {
            Dictionary<string, List<TMUser>> map = new Dictionary<string, List<TMUser>>();

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string key = item.getProperty("in_l1", "");
                string in_l2 = item.getProperty("in_l2", "");
                int sort_order = 100;

                switch (in_l2)
                {
                    case "領隊":
                        sort_order = 200;
                        break;
                    case "管理":
                        sort_order = 300;
                        break;
                    case "教練":
                        sort_order = 400;
                        break;
                    case "男子教練":
                        sort_order = 410;
                        break;
                    case "女子教練":
                        sort_order = 420;
                        break;
                }

                List<TMUser> list = null;
                if (map.ContainsKey(key))
                {
                    list = map[key];
                }
                else
                {
                    list = new List<TMUser>();
                    map.Add(key, list);
                }

                var muser = new TMUser
                {
                    sort_order = sort_order,
                    Value = item,
                };

                list.Add(muser);
            }
            return map;
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public Innosoft.InnovatorHelper InnH { get; set; }

            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }

            public string meeting_id { get; set; }
            public string mode { get; set; }
            public string scene { get; set; }

            public string MtName { get; set; }
            public string MUserName { get; set; }

            public Item itmMeeting { get; set; }
            public string in_title { get; set; }
            public string in_meeting_type { get; set; }

            public MTType mt_type { get; set; }
        }

        private enum MTType
        {
            None = 0,
            Game = 100,
            Seminar = 200,
            Degree = 300,
        }

        private class TTab
        {
            public string id { get; set; }
            public string active { get; set; }
            public string title { get; set; }

            public string li_id { get; set; }
            public string a_id { get; set; }
            public string content_id { get; set; }
            public string folder_id { get; set; }
            public string toggle_id { get; set; }
            public string table_id { get; set; }

            public bool is_reject { get; set; }

            public Item items { get; set; }
            public bool is_error { get; set; }
            public int count { get; set; }

            public Func<TConfig, TTab, Item> GetListFunc { get; set; }
            public Action<TConfig, TTab, StringBuilder> AppendListAction { get; set; }
        }

        private class TTable
        {
            public string id { get; set; }
            public string title { get; set; }
            public int count { get; set; }
        }

        private class TField
        {
            public string title { get; set; }
            public string property { get; set; }
            public string css { get; set; }
            public string format { get; set; }
            public string defval { get; set; }
            public Func<TConfig, TTab, TField, Item, string> getVal { get; set; }
        }

        private class TMUser
        {
            public int sort_order { get; set; }
            public Item Value { get; set; }
        }
    }
}