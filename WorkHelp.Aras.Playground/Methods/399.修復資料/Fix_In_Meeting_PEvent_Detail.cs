﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;
using System.Net;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class Fix_In_Meeting_PEvent_Detail : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
            目的: 修復 In_Meeting_PEvent_Detail
            輸入: 
               - meeting id
            日誌:
               - 2020.09.16 改寫 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]Fix_In_Meeting_PEvent_Detail";

            string aml = "";
            string sql = "";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            sql = @"
                SELECT
	                t1.in_meeting
	                , t1.in_l1
	                , t1.in_l2
	                , t1.in_l3
	                , t1.in_name
	                , t1.in_name2
	                , t2.id AS 'event_id'
	                , t2.in_tree_id
	                , t3.id AS 'detail_id'
	                , t3.in_sign_foot
	                , t3.in_sign_no
	                , t3.in_player_org
	                , t3.in_player_team
	                , t3.in_player_name
	                , t3.in_player_sno
                FROM
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PEVENT t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t3 WITH(NOLOCK)
	                ON t3.source_id = t2.id
                WHERE
	                t1.in_l1 = N'團體組'
	                AND ISNULL(t2.in_type, '') = 's'
	                AND ISNULL(t3.in_player_name, '') NOT IN ('', N'沒有選手')
            ";

            Item items = inn.applySQL(sql);

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string meeting_id = item.getProperty("in_meeting", "");
                string event_id = item.getProperty("event_id", "");
                string detail_id = item.getProperty("detail_id", "");
                string in_sign_foot = item.getProperty("in_sign_foot", "");
                string in_player_org = item.getProperty("in_player_org", "");
                string in_player_team = item.getProperty("in_player_team", "");
                string in_player_name = item.getProperty("in_player_name", "");

                if (in_player_name == "")
                {
                    continue;
                }

                Item itmMUser = GetMUser(inn, meeting_id, in_player_org, in_player_name);
                string in_player_sno = itmMUser.getProperty("in_sno", "");

                if (in_player_sno == "")
                {
                    continue;
                }


                string sql_upd1 = "UPDATE IN_MEETING_PEVENT_DETAIL SET"
                    + " in_player_sno = '" + in_player_sno + "'"
                    + " WHERE id = '" + detail_id + "'";

                Item itmUpd1 = inn.applySQL(sql_upd1);

                if (itmUpd1.isError())
                {
                    throw new Exception("修正身分證號發生錯誤");
                }

                string target_foot = in_sign_foot == "1" ? "2" : "1";

                string sql_upd2 = "UPDATE IN_MEETING_PEVENT_DETAIL SET"
                    + " in_target_org = N'" + in_player_org + "'"
                    + ", in_target_team = N'" + in_player_team + "'"
                    + ", in_target_name = N'" + in_player_name + "'"
                    + ", in_target_sno = N'" + in_player_sno + "'"
                    + " WHERE source_id = '" + event_id + "'"
                    + " AND in_sign_foot = '" + target_foot + "'";

                Item itmUpd2 = inn.applySQL(sql_upd2);

                if (itmUpd2.isError())
                {
                    throw new Exception("修正對手資料發生錯誤");
                }
            }

            return itmR;
        }

        private Item GetMUser(Innovator inn, string meeting_id, string in_short_org, string in_name)
        {
            string sql = "SELECT TOP 1 in_name, in_sno, in_short_org FROM IN_MEETING_USER WITH(NOLOCK)"
                + " WHERE source_id = '" + meeting_id + "'"
                + " AND in_short_org = N'" + in_short_org + "'"
                + " AND in_name = N'" + in_name + "'";

            Item itmMUser = inn.applySQL(sql);

            if (itmMUser.isError() || itmMUser.getResult() == "")
            {
                return inn.newItem();
            }
            else
            {
                return itmMUser;
            }
        }
    }
}