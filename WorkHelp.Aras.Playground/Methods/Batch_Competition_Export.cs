﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;
using System.Net;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class Batch_Competition_Export : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string sql = @"
                SELECT 
                    TOP 10 * from IN_MEETING_PROGRAM WITH(NOLOCK) 
                WHERE 
                    in_meeting = '4420520723C44A70B5F312C38B238128'
                    AND in_l1 <> N'格式組'
                ORDER BY 
                in_sort_order
            ";

            Item items = inn.applySQL(sql);

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                item.setType("IN_MEETING_PROGRAM");
                item.apply("In_Meeting_Competition_Export");
            }

            return this;
        }
    }
}