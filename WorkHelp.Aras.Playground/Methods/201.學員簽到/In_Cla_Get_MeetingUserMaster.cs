﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;
using System.Net;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class In_Cla_Get_MeetingUserMaster : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

			/*
				In_Cla_Get_MeetingUserMaster，取 得MeetingUserMaster.html 所需要的資料，由b.aspx使用
				傳入參數：
					muid : 會議使用者的id
				回傳物件結構：
				<In_Cla_Meeting>
					<Relationships>
						<In_Cla_Meeting_User>
						<In_Cla_Meeting_Agenda>
						<In_Cla_Meeting_Document>
						<inn_questiontype>
							<value/>
							<label/>
						</inn_quesitontype>
					</Relationships>
				<In_Cla_Meeting>

			*/

			/*增加容錯效果:
			1.沒有 muid
			2.id查不到資料
			3.非正取
			4.超過登入時間
			*/

			Item itmRst;//用來乘載回傳資料的物件
			Innovator inn = this.getInnovator();
			string muid = this.getProperty("muid", "no_data");

			if (muid == "no_data")
			{
				itmRst = inn.newItem();
				itmRst.loadAML(@"<AML><Item><inn_allowed>無學員資料,無法檢視本頁面</inn_allowed><to_url>http://www.sfast.org/</to_url></Item></AML>");
				return itmRst;
			}

			//Item itmMeetingUser=inn.getItemById("In_Cla_Meeting_User",muid);
			string aml = "<AML>";
			aml += "<Item type='In_Cla_Meeting_User' action='get'>";
			aml += "<id>" + muid + "</id>";
			aml += "</Item></AML>";
			Item itmMeetingUser = inn.applyAML(aml);

			if (itmMeetingUser.isError())
			{
				itmRst = inn.newItem();
				itmRst.loadAML(@"<AML><Item><inn_allowed>資料錯誤,無法檢視本頁面</inn_allowed><to_url>http://www.sfast.org/</to_url></Item></AML>");
				return itmRst;
			}

			if (itmMeetingUser.getProperty("in_note_state", "cancel") != "official")
			{
				itmRst = inn.newItem();
				itmRst.loadAML(@"<AML><Item><inn_allowed>您不是本課程正取學員,無法檢視本頁面</inn_allowed><to_url>http://www.sfast.org/</to_url></Item></AML>");
				return itmRst;
			}

			string sql = @"SELECT in_name_num FROM In_Cla_Meeting_Technical
            WHERE in_user ='{#muid}'";
			sql = sql.Replace("{#muid}", muid);
			Item itmSql = inn.applySQL(sql);
			if (!itmSql.isError() && itmSql.getItemCount() > 0)
			{
				itmMeetingUser.setProperty("in_number", itmSql.getProperty("in_name_num"));
			}

			//先判斷是否已超過登入時間
			aml = "<AML>";
			aml += "<Item type='In_Cla_Meeting_FunctionTime' action='get' select='in_date_e,source_id(in_url)'>";
			aml += "<source_id>" + itmMeetingUser.getProperty("source_id") + "</source_id>";
			aml += "<in_action>mulogin</in_action>";
			aml += "</Item></AML>";

			Item ItmMULoginTime = inn.applyAML(aml);

			DateTime MuLoginTime = DateTime.Parse(ItmMULoginTime.getProperty("in_date_e", ""));
			if (MuLoginTime < System.DateTime.Now)
			{
				string str_url = "./b.aspx?page={#in_url}&method=In_Cla_MeetingUserResponse&in_meetingid={#meeting_id}";
				str_url = str_url.Replace("{#in_url}", ItmMULoginTime.getPropertyItem("source_id").getProperty("in_url"));
				str_url = str_url.Replace("{#meeting_id}", ItmMULoginTime.getProperty("source_id"));

				itmRst = inn.newItem();
				itmRst.loadAML(@"<AML><Item><inn_allowed>已超過登入時間,無法檢視本頁面</inn_allowed><to_url><![CDATA[" + str_url + "]]></to_url></Item></AML>");
				return itmRst;
			}

			string strMeetingQueryAML = @"<AML>
								<Item type='In_Cla_Meeting' action='get' id='{#meeting_id}' select='in_title,in_date_s,in_date_e'>
									<Relationships>
										<Item type='In_Cla_Meeting_File2' action='get'/>
										<Item type='In_Cla_Meeting_Agenda' action='get'/>
										<Item type='In_Cla_Meeting_Record' action='get' orderBy='in_date'>
											<in_participant>{#muid}</in_participant>
										</Item>
										<Item type='In_Cla_Meeting_Resume' action='get'>
											<in_user>{#muid}</in_user>
										</Item>
									</Relationships>
								</Item>
							</AML>"
										.Replace("{#muid}", muid)
										.Replace("{#meeting_id}", itmMeetingUser.getProperty("source_id"));

			//取得survey的部分
			/*
			string strGetSurveySQL=@"select in_action,substring(in_action,6,1) 'value',in_date_s,in_date_e,
										case 
											when in_action='sheet3' then '技術考由考官操作'
											when exists(select top 1 id from In_Cla_Meeting_Surveys_Result where in_surveytype=substring(in_action,6,1) and in_participant='{#muid}')
												then '已填寫過'
											when '{#now}' between in_date_s and in_date_e 
												then '可填寫'
												else '已過填寫期間' 
											end as inn_active_status
										from In_Cla_Meeting_FunctionTime where source_id = '{#meeting_id}' and in_action like 'sheet%'  order By in_date_s,sort_order
										"
										.Replace("{#muid}",muid)
										.Replace("{#meeting_id}",itmMeetingUser.getProperty("source_id"))
										.Replace("{#now}",System.DateTime.Now.AddHours(-8).ToString("yyyy-MM-ddTHH:mm:ss"));
			*/
			string strGetSurveySQL = @"select in_action,in_purpose,in_date_s,in_date_e,
							case 
								when in_action='sheet3' then '技術考由考官操作'
								when exists(select top 1 id from In_Cla_Meeting_Surveys_Result where concat('sheet',in_surveytype)=in_action and in_participant='{#muid}')
									then '已填寫過'
								when '{#now}' between in_date_s and in_date_e 
									then '可填寫'
									else '已過填寫期間' 
								end as inn_active_status
							from In_Cla_Meeting_FunctionTime where source_id = '{#meeting_id}' and in_action like 'sheet%'  order By in_date_s,sort_order
							"
										.Replace("{#muid}", muid)
										.Replace("{#meeting_id}", itmMeetingUser.getProperty("source_id"))
										.Replace("{#now}", System.DateTime.Now.AddHours(-8).ToString("yyyy-MM-ddTHH:mm:ss"));






			string strGetLabelsAML = @"<AML>
				<Item type='Value' action='get' select='label,value'>
					<source_id>
						<Item type='List' action='get' where="" name='in_meeting_action' ""/>
					</source_id>
					<value condition='like'>sheet%</value>
				</Item>
			</AML>";

			Item itmFunctiontimeLabels = inn.applyAML(strGetLabelsAML);
			Item meeting = inn.applyAML(strMeetingQueryAML);
			Item itmSurveyAvail = inn.applySQL(strGetSurveySQL);

			//多試卷,試卷名稱要抓 in_purpose
			sql = "";
			sql = "select in_action,in_purpose from In_Cla_Meeting_FunctionTime where source_id='" + itmMeetingUser.getProperty("source_id") + "' and in_action like 'sheet%' and in_action!='sheet1' order By in_action ";
			Item FuncTimes = inn.applySQL(sql);

			meeting = meeting.apply("In_Cla_Has_MeetingSurveys");
			meeting.addRelationship(itmMeetingUser);
			int intAvailCount = itmSurveyAvail.getItemCount();
			for (int c = 0; c < intAvailCount; c++)
			{
				Item itmSurveyAvailTmp = itmSurveyAvail.getItemByIndex(c);
				string strValue = itmSurveyAvailTmp.getProperty("in_action"); //sheet1, sheet2, sheet3....
				int labelCount = itmFunctiontimeLabels.getItemCount();
				if (strValue.Contains("sheet"))
				{
					//Item FuncTime  = FuncTimes.getItemsByXPath("//Item[in_action='" + strValue + "']");
					itmSurveyAvailTmp.setProperty("label", itmSurveyAvailTmp.getProperty("in_purpose", "")); //問卷1, 問卷2...
				}
				else
				{
					for (int lc = 0; lc < labelCount; lc++)
					{
						Item itmTmpLabel = itmFunctiontimeLabels.getItemByIndex(lc);
						if (itmTmpLabel.getProperty("value") == strValue)
						{
							itmSurveyAvailTmp.setProperty("label", itmTmpLabel.getProperty("label")); //簽到,技術考....
							break;
						}
					}
				}

				//lina 2020.08.04 停用報名表檢視功能 (沒有問卷資料) _# start:
				if (strValue == "sheet1")
				{
					itmSurveyAvailTmp.setProperty("inn_disabled", "disabled");
				}
				//lina 2020.08.04 停用報名表檢視功能 (沒有問卷資料) _# end.


				itmSurveyAvailTmp.setAttribute("type", "inn_questiontype");
				itmSurveyAvailTmp.setProperty("inn_sdate_s", System.DateTime.Parse(itmSurveyAvailTmp.getProperty("in_date_s")).AddHours(8).ToString("yyyy/MM/dd HH:mm"));
				itmSurveyAvailTmp.setProperty("inn_sdate_e", System.DateTime.Parse(itmSurveyAvailTmp.getProperty("in_date_e")).AddHours(8).ToString("yyyy/MM/dd HH:mm"));


				itmSurveyAvailTmp.setProperty("value", itmSurveyAvailTmp.getProperty("in_action", "").Replace("sheet", ""));


				meeting.addRelationship(itmSurveyAvailTmp);
			}

			//動態圈圈
			Item itmResume = meeting.getRelationships("In_Cla_Meeting_Resume");
			int intResumeCount = itmResume.getItemCount();
			//string[21] stat_list={"in_stat_0","in_stat_1","in_stat_4","in_stat_2","in_stat_3","in_stat_5"};
			Dictionary<string, string> in_stats = new Dictionary<string, string>(); //時程選項(FuncTime) 與 in_stat_x 的對應關係
			in_stats.Add("sheet4", "in_stat_2"); //滿意度調查
			in_stats.Add("sheet5", "in_stat_3"); //筆試
			in_stats.Add("sheet3", "in_stat_4"); //技術考
			in_stats.Add("sheet2", "in_stat_5"); //事前問卷

			for (int i = 6; i < 21; i++)
			{
				in_stats.Add("sheet" + i.ToString(), "in_stat_" + i.ToString());
			}

			if (intResumeCount == 1)
			{
				Item itmTmpResume = itmResume.getItemByIndex(0);

				//先補上 合格 與 簽到
				string strRecord = "";
				Item FuncTime = inn.newItem();
				FuncTime.setProperty("in_purpose", "合格");

				FuncTime.setType("FuncTime");
				strRecord = itmTmpResume.getProperty("in_stat_0", "");
				switch (strRecord)
				{
					case "O":
						FuncTime.setProperty("in_stat_judge", "100");
						break;
					case "X":
						FuncTime.setProperty("in_stat_judge", "0");
						break;
				}
				meeting.addRelationship(FuncTime);


				FuncTime = inn.newItem();
				FuncTime.setType("FuncTime");
				FuncTime.setProperty("in_purpose", "簽到");
				strRecord = itmTmpResume.getProperty("in_stat_1", "");
				switch (strRecord)
				{
					case "O":
						FuncTime.setProperty("in_stat_judge", "100");
						break;
					case "X":
						FuncTime.setProperty("in_stat_judge", "0");
						break;
				}
				meeting.addRelationship(FuncTime);

				foreach (var in_stat in in_stats)
				{
					//先判斷是否包含於選項時程內

					FuncTime = inn.newItem();
					FuncTime = FuncTimes.getItemsByXPath("//Item[in_action='" + in_stat.Key + "']");
					if (FuncTime.getItemCount() == 0)
						continue;


					strRecord = itmTmpResume.getProperty(in_stat.Value, "");
					FuncTime.setType("FuncTime");
					switch (strRecord)
					{
						case "O":
							FuncTime.setProperty("in_stat_judge", "100");
							break;
						case "X":
							FuncTime.setProperty("in_stat_judge", "0");
							break;
					}

					meeting.addRelationship(FuncTime);

				}


			}
			else
			{
				throw new Exception("本頁面僅支援單一學員檢視成績");
			}

			/*
			for(int ir=0;ir<intResumeCount;ir++){
				Item itmTmpResume=itmResume.getItemByIndex(ir);

				foreach(string stat in stat_list){
					string strShowOrNot=stat.Replace("stat","show");
					string strRecord=itmTmpResume.getProperty(stat,"");
					itmTmpResume.setProperty(strShowOrNot,meeting.getProperty(strShowOrNot));
					switch(strRecord){
						case "O":
							itmTmpResume.setProperty(stat+"_judge","100");	break;
						case "X":
							itmTmpResume.setProperty(stat+"_judge","0");break;}	
				}
			}
			*/


			Item itmMFile2 = meeting.getRelationships("In_Cla_Meeting_File2");
			int intFile2Count = itmMFile2.getItemCount();
			for (int f = 0; f < intFile2Count; f++)
			{
				Item itmMF = itmMFile2.getItemByIndex(f);
				if (itmMF.getRelatedItem() == null) { continue; }
				itmMF.setProperty("related_id.mimetype", itmMF.getRelatedItem().getProperty("mimetype"));
				itmMF.setProperty("related_id.keyed_name", itmMF.getRelatedItem().getProperty("keyed_name"));

			}

			itmRst = meeting;
			itmRst.setProperty("in_date_s", System.DateTime.Parse(itmRst.getProperty("in_date_s")).ToString("yyyy/MM/dd HH:mm"));
			itmRst.setProperty("in_date_e", System.DateTime.Parse(itmRst.getProperty("in_date_e")).ToString("yyyy/MM/dd HH:mm"));
			itmRst.setProperty("muid", muid);
			itmRst.setProperty("inn_allowed", "1");

			return itmRst;

        }
    }
}