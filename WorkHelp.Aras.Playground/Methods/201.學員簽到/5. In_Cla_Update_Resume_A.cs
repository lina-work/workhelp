﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;
using System.Net;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class In_Cla_Update_Resume_A : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            //System.Diagnostics.Debugger.Break();
            Innovator inn = this.getInnovator();
            string meetingid = this.getProperty("source_id", "");
            string meetinguserid = this.getID();
            string[] mysurveytype;//術科、滿意度、學習後評量
            string sql = "", replacesql = "";
            Item MeetingResume;

            Item Meeting = inn.getItemById("In_Cla_Meeting", meetingid);
            //組出有問卷的問卷類型的陣列
            Meeting = Meeting.apply("In_Cla_Has_MeetingSurveys");
            int surveycounts = Convert.ToInt32(Meeting.getProperty("Inn_allsurveycount", "0"));
            string hassurvey = "";
            for (int i = 2; i <= surveycounts; i++)
            {
                if (Meeting.getProperty("in_show_" + i, "") == "1")
                {
                    hassurvey += (i.ToString() + ",");
                }
            }
            mysurveytype = hassurvey.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            //簽到簽退
            Item itmRecord = inn.newItem("In_Cla_Meeting_Record", "get");
            itmRecord.setProperty("source_id", meetingid);
            itmRecord.setProperty("in_participant", meetinguserid);
            itmRecord = itmRecord.apply();

            if (itmRecord.isError())
            {
                sql = "update In_Cla_Meeting_Resume set in_stat_1 = 'X' where source_id='" + meetingid + "' and in_user='" + meetinguserid + "'";
                MeetingResume = inn.applySQL(sql);
            }
            else
            {
                sql = "update In_Cla_Meeting_Resume set in_stat_1 = 'O' where source_id='" + meetingid + "' and in_user='" + meetinguserid + "'";
                MeetingResume = inn.applySQL(sql);
            }

            //問卷結果
            for (int i = 0; i < mysurveytype.Length; i++)
            {
                string successsql = "", failsql = "";
                Item SurveyResult = inn.newItem("In_Cla_Meeting_Surveys_Result", "get");
                SurveyResult.setProperty("source_id", meetingid);
                SurveyResult.setProperty("in_participant", meetinguserid);
                SurveyResult.setProperty("in_surveytype", mysurveytype[i]);

                switch (mysurveytype[i])
                {
                    case "2":
                        successsql = "update In_Cla_Meeting_Resume set in_stat_5 = 'O' where source_id='" + meetingid + "' and in_user='" + meetinguserid + "'";
                        failsql = "update In_Cla_Meeting_Resume set in_stat_5 = 'X' where source_id='" + meetingid + "' and in_user='" + meetinguserid + "'";
                        replacesql += "AND IN_STAT_5 = 'O' ";
                        break;
                    case "3"://技術考試
                        successsql = "update In_Cla_Meeting_Resume set in_stat_4 = 'O' where source_id='" + meetingid + "' and in_user='" + meetinguserid + "'";
                        failsql = "update In_Cla_Meeting_Resume set in_stat_4 = 'X' where source_id='" + meetingid + "' and in_user='" + meetinguserid + "'";
                        replacesql += "AND IN_STAT_4 = 'O' ";
                        break;
                    case "4"://滿意度
                        successsql = "update In_Cla_Meeting_Resume set in_stat_2 = 'O' where source_id='" + meetingid + "' and in_user='" + meetinguserid + "'";
                        failsql = "update In_Cla_Meeting_Resume set in_stat_2 = 'X' where source_id='" + meetingid + "' and in_user='" + meetinguserid + "'";
                        replacesql += "AND IN_STAT_2 = 'O' ";
                        break;
                    case "5"://學習後評量
                        successsql = "update In_Cla_Meeting_Resume set in_stat_3 = 'O' where source_id='" + meetingid + "' and in_user='" + meetinguserid + "'";
                        failsql = "update In_Cla_Meeting_Resume set in_stat_3 = 'X' where source_id='" + meetingid + "' and in_user='" + meetinguserid + "'";
                        replacesql += "AND IN_STAT_3 = 'O' ";
                        break;
                    default:
                        successsql = "update In_Cla_Meeting_Resume set in_stat_" + mysurveytype[i] + " = 'O' where source_id='" + meetingid + "' and in_user='" + meetinguserid + "'";
                        failsql = "update In_Cla_Meeting_Resume set in_stat_" + mysurveytype[i] + " = 'X' where source_id='" + meetingid + "' and in_user='" + meetinguserid + "'";
                        replacesql += "AND IN_STAT_" + mysurveytype[i] + " = 'O' ";
                        break;
                }
                SurveyResult = SurveyResult.apply();
                if (SurveyResult.isError())
                {
                    MeetingResume = inn.applySQL(failsql);
                }
                else
                {

                    MeetingResume = inn.applySQL(successsql);
                }
            }

            //可領證 先設成'X'
            sql = @"UPDATE In_Cla_Meeting_Resume
        SET IN_STAT_0 = 'X'
        WHERE SOURCE_ID = '[@meetingid]' AND IN_USER = '[@meetinguserid]'"
            .Replace("[@meetingid]", meetingid)
            .Replace("[@meetinguserid]", meetinguserid);
            MeetingResume = inn.applySQL(sql);

            //因著replacesql去組出條件來更新可領證的欄位
            sql = @"UPDATE In_Cla_Meeting_Resume
        SET IN_STAT_0 = 'O'
        WHERE SOURCE_ID = '[@meetingid]' AND IN_USER = '[@meetinguserid]' 
        AND IN_STAT_1 = 'O' [@replacesql]"
                        .Replace("[@replacesql]", replacesql)
                        .Replace("[@meetingid]", meetingid)
                        .Replace("[@meetinguserid]", meetinguserid);
            MeetingResume = inn.applySQL(sql);
            CCO.Utilities.WriteDebug("alantest_0628", sql);

            return inn.newResult("");

        }
    }
}