﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;
using System.Net;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class In_Cla_Update_Resume : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            //System.Diagnostics.Debugger.Break();
            Innovator inn = this.getInnovator();
            string meetinguserid = this.getProperty("meetinguserid", "");

            Item MeetingUser = inn.getItemById("In_Cla_Meeting_User", meetinguserid);
            string meetingid = MeetingUser.getProperty("source_id", "");
            Item Meeting = inn.getItemById("In_Cla_Meeting", meetingid);
            string resumerule = Meeting.getProperty("in_updateresume_rule", "");
            Item itmRule = MeetingUser;//meetinguser
            switch (resumerule)
            {
                case "In_Cla_Update_Resume_A":

                    itmRule = itmRule.apply(resumerule);
                    break;

            }


            return inn.newResult("");
        }
    }
}