﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;
using System.Net;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class In_Cla_Check_Meetingfunctionvalid : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                檢查指定會議的指定動作在當下是否可被執行，內部呼叫。以 Result 方式傳回結果。
                回傳值：
                     0 : 不在開放時間內
                     1 : 不在開放時間內
            */

            Innovator inn = this.getInnovator();
            string strMeetingId = this.getProperty("meeting_id", "");  //指定的meeting_id
            string strActionType = this.getProperty("actiontype", ""); //指定的actiontype

            //傳入參數檢查
            if (strMeetingId == "no_data" || strActionType == "no_data")
            {
                throw new Exception("missing \"meeting_id\"(value given={#meeting_id}) or \"actiontype\"(value given={#actiontype}) parameter"
                                    .Replace("{#meeting_id}", strMeetingId)
                                    .Replace("{#actiontype}", strActionType)
                                    );
            }

            string strCurrentTime = System.DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
            Item itmMeetingFunctionTime = inn.applyAML(@"
                        <AML>
                            <Item type=""In_Cla_Meeting_FunctionTime"" action=""get"" maxRecords=""1"">
                                <source_id>{#meeting_id}</source_id>
                                <in_action>{#actiontype}</in_action>
                                <in_date_s condition=""lt"">{#currentTime}</in_date_s>
                                <in_date_e condition=""gt"">{#currentTime}</in_date_e>
                            </Item>
                        </AML>"
                                    .Replace("{#actiontype}", strActionType)
                                    .Replace("{#currentTime}", strCurrentTime)
                                    .Replace("{#meeting_id}", strMeetingId)
                                    );

            if (!itmMeetingFunctionTime.isError() && !itmMeetingFunctionTime.isEmpty())
            {
                return inn.newResult("1");
            }
            else
            {
                return inn.newResult("0");
            }

        }
    }
}