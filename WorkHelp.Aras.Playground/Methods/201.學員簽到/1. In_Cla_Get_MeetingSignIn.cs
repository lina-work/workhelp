﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;
using System.Net;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class In_Cla_Get_MeetingSignIn : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
            取得owned_by_id為當下登入使用者的會議並回傳。主要由c.aspx使用。

            回傳物件結構：
                <Item>
                    <!--本體啥都沒有-->
                    <Relationships>
                        <Item type="In_Cla_Meeting"/>
                        <Item type="Inn_Param"/> <!--傳入的parameter，來自In_Collect_PassengerParam-->
                    </Relationships>
                </Item>
            */

            //break cache
            System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]In_Cla_Get_MeetingSignIn";

            Item itmRst = inn.newItem();//用來乘載回傳資料的物件。
            Item itmCheckWithinTime = newItem("Method", "In_Cla_Check_MeetingFunctionValid");
            itmCheckWithinTime.setProperty("actiontype", "2");//基於頁面需求，強制指定為簽到退(2)

            Item itmParams = this.apply("In_Collect_PassengerParam");

            string strUserName = inn.getItemById("User", inn.getUserID()).getProperty("keyed_name");
            string strSelectedMeetingID = this.getProperty("meeting_id");

            var _cache = System.Runtime.Caching.MemoryCache.Default;

            Item itmMeetings = inn.applyAML(@"
        <AML>
        	<Item type=""In_Cla_Meeting""  action=""get"" where='[In_Cla_Meeting].id!=[In_Cla_Meeting].in_refmeeting'>
        	<or>
        	   <or>
        		<owned_by_id>
        			<Item type=""Identity"" action=""get"">
        				<name>{#username}</name>
        				<is_aliase>1</is_aliase>
        			</Item>
        		</owned_by_id>
        		</or>
        		<or>
        		<managed_by_id>
        			<Item type=""Identity"" action=""get"">
        				<name>{#username}</name>
        				<is_aliase>1</is_aliase>
        			</Item>
        		</managed_by_id>
        		</or>
        		<or>
        		<in_owner>
        			<Item type=""Identity"" action=""get"">
        				<name>{#username}</name>
        				<is_aliase>1</is_aliase>
        			</Item>
        		</in_owner>
        		</or>
            </or>
			<in_current_activity condition='ne'></in_current_activity>
			<in_current_activity condition='is not null'></in_current_activity>
        	</Item>
        </AML>"
                    .Replace("{#username}", strUserName));


            //2021-06-15 為了         新北市跆拳道協會 110 年度 C 級跆拳道裁判講習會 ,寫死
            // if(!itmMeetings.isError() || itmMeetings.getItemCount()==0)
            // {
            //     //AE0925E88023487BAB2831EEBD2C645A : IMS-000575
            //     //F68B1D07E28947BD89A6F2EFBAC8C179 : IMS-000548
            //     itmMeetings = inn.getItemById("In_Cla_Meeting","F68B1D07E28947BD89A6F2EFBAC8C179");
            // }


            if (!itmMeetings.isEmpty() && !itmMeetings.isError())
            {

                int mtCount = itmMeetings.getItemCount();
                for (int count = 0; count < mtCount; count++)
                {
                    Item tmpMeeting = itmMeetings.getItemByIndex(count);
                    itmCheckWithinTime.setProperty("meeting_id", tmpMeeting.getID());
                    bool withinTime = itmCheckWithinTime.apply().getResult() == "1"; //0 : 不在時間內, 1:在時間內
                    if (withinTime)
                    {
                        itmRst.addRelationship(tmpMeeting);
                    }
                    int paramCount = itmParams.getItemCount();
                    for (int pc = 0; pc < paramCount; pc++)
                    {
                        itmRst.addRelationship(itmParams.getItemByIndex(pc));
                    }
                }




            }

            itmRst.setProperty("inn_meeting_id", strSelectedMeetingID);

            return itmRst;
        }
    }
}