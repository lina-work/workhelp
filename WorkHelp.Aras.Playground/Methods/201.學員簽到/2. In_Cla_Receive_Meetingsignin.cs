﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;
using System.Net;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class In_Cla_Receive_Meetingsignin : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            //方法：In_Cla_Receive_Meetingsignin
            /*
                接收簽到請求，假如可以簽到，增加一筆簽到記錄，假如不行，回傳錯誤。由InnoJSON.ashx的SimpleJSON模式使用。
                除了可以遷到外同時檢查傳入的使用者是否屬於欲簽到的會議，如果使用者非屬該會議，回傳錯誤。
                這個方法會在response傳回四種狀態： error,success,userIsCanceled,userIsWaiting。
                        error:表示未處理的錯誤，詳情請看message
                        success:情請求成功，通常不會有額外的mesage
                        userIsCanceled,userIsWaiting:欲簽到的人員是取消或備取狀態，主要供現場報名系統使用，若不採用現場報名，可以只判斷是否為success。
                接收參數：
                        meeting_id:欲簽到的會議，用於檢查會議使用者是否屬於該會議。
                        value:會議使用者的判斷值，內容依據ientifier而變化。
                        identifier: 用來取得會議使用者的依據，預設為in_mail，
                        actiontype:動作類型，暫時預設為2(簽到簽退)
                        sign_on_scene : 字串true或false是否為現場簽到。
            */

            Innovator inn = this.getInnovator();
            Item itmFunctionAvailable;
            Item itmMeetingUser;
            Item itmRst = inn.newItem();//乘載回傳用資料的物件。
            Item itmSinginRecord;
            string strMeetingId = this.getProperty("meeting_id", "no_data");
            string strIdentifier = this.getProperty("identifier", "in_mail");
            string strValue = this.getProperty("value", "no_data");
            string strActionType = this.getProperty("actiontype", "no_data");
            string strSigninOnScene = this.getProperty("sign_on_scene", "false");

            string sql = "";

            //是否現場簽到，字串true或false，如果為true，則直接增加會議的報名人數(假設會議已滿)
            if (strValue == "no_data" || strMeetingId == "no_data") { return inn.newError("missing parameter \"value\" or \"meeting_id\""); }

            string strCurrentTime = System.DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
            itmMeetingUser = inn.applyAML(@"
    <AML>
        <Item type='In_Cla_Meeting_User' action='get' select='in_name,source_id,id,in_note_state'>
          <{#identifier}>{#value}</{#identifier}>
          <source_id>{#meeting_id}</source_id>
        </Item>
    </AML>"
                .Replace("{#identifier}", strIdentifier)
                .Replace("{#value}", strValue)
                .Replace("{#meeting_id}", strMeetingId)
                );

            if (itmMeetingUser.isError() || itmMeetingUser.isEmpty())
            {
                itmRst.loadAML(@"
                <Item>
                    <response>error</response>
                    <message>找不到[{#strValue}]使用者，請檢查輸入的資料。</message>
                </Item>".Replace("{#strValue}", strValue));
                return itmRst;
            }


            //將In_Get_MeetingFunctionTimes 的必要參數設定到自己身上並用自身去apply(In_Check_MeetingFunctionValid)
            this.setProperty("meeting_id", itmMeetingUser.getProperty("source_id"));
            this.setProperty("actiontype", strActionType);
            itmFunctionAvailable = this.apply("In_Cla_Check_Meetingfunctionvalid");

            //▼修改檢查會議可否簽到的邏輯
            string strCheckMeetingFunctionAML = @"<AML>
										<Item type='In_Cla_Meeting_FunctionTime' action='get' select='id,in_type,in_date_s,in_date_e,in_purpose,in_note'>
										    <in_action>2</in_action>
											<source_id>{#meeting_id}</source_id>
											<in_date_s condition='lt'>{#today}</in_date_s>
											<in_date_e condition='gt'>{#today}</in_date_e>
										</Item>
									</AML>"
                                                .Replace("{#meeting_id}", strMeetingId)
                                                .Replace("{#today}", strCurrentTime);

            Item itmCanSign = inn.applyAML(strCheckMeetingFunctionAML);

            if (itmCanSign.isError() || itmCanSign.isEmpty() || itmCanSign.getItemCount() == 0)
            {
                itmRst.loadAML(@"
                    <Item>
                        <response>error</response>
                        <message>尚未開放簽到</message>
                    </Item>");
                return itmRst;
            }

            //判斷是否已經是新的一輪簽到: 
            if (itmCanSign.getProperty("in_note", "") == "")
            {
                sql = "Update [In_Cla_Meeting_Resume] set in_date_s=null,in_stat_1='X' where source_id='" + strMeetingId + "'";
                inn.applySQL(sql);

                sql = "Update [In_Cla_Meeting_FunctionTime] set in_note='y' where id='" + itmCanSign.getID() + "'";
                inn.applySQL(sql);
            }

            //▲修改檢查會議可否簽到的邏輯
            //確定會議仍可簽到後，處理in_note_state不為official且非現場簽到的狀況，如果非正取立刻跳回詢問是否要轉正取或現場報名。
            if (strSigninOnScene == "false")
            {
                switch (itmMeetingUser.getProperty("in_note_state", "cancel"))
                {
                    case "cancel":
                        itmRst.loadAML(@"
					<Item>
						<response>userIsCanceled</response>
						<message>[{#user_name}]已被取消報名，是否調整為正取?</message>
					</Item>".Replace("{#user_name}", itmMeetingUser.getProperty("in_name")));
                        return itmRst;
                    case "waiting":
                        itmRst.loadAML(@"
					<Item>
						<response>userIsWaiting</response>
						<message>[{#user_name}]為備取狀態，是否調整為正取?</message>
					</Item>".Replace("{#user_name}", itmMeetingUser.getProperty("in_name")));
                        return itmRst;
                }
            }

            if (strSigninOnScene == "true")
            {
                //如果為現場簽到狀態，開始更新會議並準備簽到
                //假如會議未滿，直接將使用者設定為正取，假如會議已滿，回傳錯誤。
                Item itmMeeting = inn.getItemById("In_Cla_Meeting", itmMeetingUser.getProperty("source_id"));
                int intSysTaking = Int32.Parse(itmMeeting.getProperty("in_taking", "0"));
                int intRealTaking = Int32.Parse(itmMeeting.getProperty("in_real_taking", "0"));
                //如果正取人數不足，正取人數+1
                if (intSysTaking < intRealTaking)
                {
                    itmRst.loadAML(@"
					<Item>
						<response>error</response>
						<message>正取人數已額滿，無法繼續簽到。</message>
					</Item>");
                    return itmRst;
                }

                itmMeetingUser.setProperty("in_note_state", "official");
                itmMeetingUser.setAction("edit");
                itmMeetingUser = itmMeetingUser.apply();
                if (itmMeetingUser.isError())
                {
                    itmRst.loadAML(@"
					<Item>
						<response>error</response>
						<message>更新[{#user_name}]的登記狀況發生問題</message>
					</Item>".Replace("{#user_name}", itmMeetingUser.getProperty("in_name")));
                    return itmRst;
                }
                itmMeetingUser.setProperty("in_note_state", "official");
                itmMeetingUser.setAction("edit");
                itmMeetingUser = itmMeetingUser.apply();
                if (itmMeetingUser.isError())
                {
                    itmRst.loadAML(@"
					<Item>
						<response>error</response>
						<message>更新[{#user_name}]的登記狀況發生問題</message>
					</Item>".Replace("{#user_name}", itmMeetingUser.getProperty("in_name")));
                    return itmRst;
                }
                itmMeeting.setAction("get");
                itmMeeting = itmMeeting.apply();
                itmMeeting.apply("In_Cla_Update_MeetingOnMUEdit");
            }


            //檢查使用者是否已簽到過
            Item itmCurrentSigning = itmCanSign.getItemByIndex(0);

            itmSinginRecord = inn.applyAML(@"
                <AML>
                    <Item type=""In_Cla_Meeting_Record"" action=""get"">
                        <source_id>{#meeting_id}</source_id>
                        <in_participant>{#muid}</in_participant>
						<in_date condition='gt'>{#sign_in_start}</in_date>
						<in_date condition='lt'>{#sign_in_end}</in_date>
                    </Item>
                </AML>"
                            .Replace("{#meeting_id}", strMeetingId)
                            .Replace("{#sign_in_start}", itmCurrentSigning.getProperty("in_date_s"))
                            .Replace("{#sign_in_end}", itmCurrentSigning.getProperty("in_date_e"))
                            .Replace("{#muid}", itmMeetingUser.getID())
                            );

            if (!itmSinginRecord.isError() && !itmSinginRecord.isEmpty())
            {
                itmRst.loadAML(@"
                <Item>
                    <response>success</response>
                    <message>[{#user_name}]已簽到過，無須再簽到</message>
                </Item>".Replace("{#user_name}", itmMeetingUser.getProperty("in_name")));
                return itmRst;
            }



            Item itmSignIn = inn.applyAML(@"
        <AML>
            <Item type=""In_Cla_Meeting_Record"" action=""add"">
                <source_id>{#meeting_id}</source_id>
				<in_purpose>{#purpose}</in_purpose>
                <in_participant>{#muid}</in_participant>
                <in_date>{#currentTime}</in_date>
            </Item>
            <Item type=""In_Cla_Meeting_Resume"" action=""edit"" where=""source_id='{#meeting_id}' and in_user='{#muid}'"">
                <in_date_s>{#currentTime}</in_date_s>
            </Item>
        </AML>"
                    .Replace("{#meeting_id}", strMeetingId)
                    .Replace("{#muid}", itmMeetingUser.getID())
                    .Replace("{#currentTime}", strCurrentTime)
                    .Replace("{#purpose}", itmCanSign.getProperty("in_purpose", ""))
                    );
            //增加學員履歷的紀錄
            inn.applyAML(@"<AML>
                <Item type=""In_Cla_Meeting_Resume"" action=""edit"" where=""source_id='{#meeting_id}' and in_user='{#muid}'"">
                    <in_date_s>{#currentTime}</in_date_s>
                </Item>
            </AML>"
                .Replace("{#meeting_id}", strMeetingId)
                .Replace("{#sign_in_type}", itmCurrentSigning.getProperty("in_type", ""))
                .Replace("{#muid}", itmMeetingUser.getID())
                .Replace("{#currentTime}", strCurrentTime)

                );

            /*
            簽到成功後去更新該學員的學員履歷的簽到欄位
            */
            string meetinguserid = itmMeetingUser.getProperty("id", "");
            Item itmMeetingUser1 = inn.newItem("In_Cla_Meeting_User");
            itmMeetingUser1.setProperty("meetinguserid", meetinguserid);
            itmMeetingUser1 = itmMeetingUser1.apply("In_Cla_Update_Resume");



            if (itmSignIn.isError() || itmSignIn.isEmpty())
            {
                itmRst.loadAML(@"
                <Item>
                    <response>error</response>
                    <message>[{#user_name}]簽到錯誤</message>
                    <data>{#error_data}</data>
                </Item>".Replace("{#error_data}", itmSignIn.dom.InnerXml)
                                    .Replace("{#user_name}", itmMeetingUser.getProperty("in_name")));
                return itmRst;
            }
            else
            {
                itmRst.loadAML(@"
                <Item>
                    <response>success</response>
                    <message>[{#user_name}]簽到退成功</message>
                </Item>".Replace("{#user_name}", itmMeetingUser.getProperty("in_name")));
                return itmRst;

            }

        }
    }
}