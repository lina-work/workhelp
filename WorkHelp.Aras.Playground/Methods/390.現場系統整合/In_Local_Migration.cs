﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;
using System.Net;

namespace WorkHelp.Aras.Playground.Methods.Local
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class In_Local_Migration : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
            目的: 現場系統整合
            輸入: meeting_id
            日期: 
                2021-09-28: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]In_Local_Migration";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "input dom: " + itmR.dom.InnerXml);

            string folder = "judo_" + itmR.getProperty("in_date", "").Replace("-", "");
            string file = folder + ".zip";
            string acs_file = @"C:\site\judo\pages\AccessIO\Judo.mdb";
            string zip_file = @"C:\site\judo\tempvault\meeting_access\" + file;

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,

                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                AccessConnStr = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + acs_file + ";",

                meeting_id = itmR.getProperty("meeting_id", ""),
                in_date_key = itmR.getProperty("in_date", ""),
            };

            //清除選手資料
            ClearTable(cfg, "Player");
            //新增選手資料
            AppendPlayers(cfg);

            //清除場次資料
            ClearTable(cfg, "Schedule");
            //新增場次資料
            AppendSchedules(cfg);

            //刪除既有檔案
            if (System.IO.File.Exists(zip_file))
            {
                System.IO.File.Delete(zip_file);
            }

            //壓縮資料庫
            using (var zip = new Ionic.Zip.ZipFile(zip_file, System.Text.Encoding.UTF8))
            {
                zip.AddFile(acs_file, folder);
                zip.Comment = "This zip was created at " + System.DateTime.Now.ToString("G");
                zip.Save();
            }

            itmR.setProperty("xls_name", file);

            return itmR;
        }

        /// <summary>
        /// 新增場次資料
        /// </summary>
        private void AppendSchedules(TConfig cfg)
        {
            string sql = @"
                SELECT 
	               t1.item_number               AS 'mnumber'
	                , t2.in_name2               AS 'program_name2'
	                , t5.in_code				AS 'site_code'
	                , t5.in_name				AS 'site_name'
	                , t3.id                     AS 'event_id'
	                , t3.in_tree_name
	                , t3.in_tree_no
	                , t3.in_date_key	    	AS 'mday'
	                , t4.in_sign_foot
	                , t4.in_sign_no
	                , t11.map_short_org         AS 'player_abbr'
	                , t11.in_name               AS 'player_name'
                FROM
	                IN_MEETING t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PROGRAM t2 WITH(NOLOCK)
	                ON t2.in_meeting = t1.id
                INNER JOIN
	                IN_MEETING_PEVENT t3 WITH(NOLOCK)
	                ON t3.source_id = t2.id
                INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t4 WITH(NOLOCK)
	                ON t4.source_id = t3.id
                INNER JOIN
	                IN_MEETING_SITE t5 WITH(NOLOCK)
	                ON t5.id = t3.in_site
                LEFT OUTER JOIN
	                VU_MEETING_PTEAM t11 WITH(NOLOCK)
	                ON t11.source_id = t2.id
	                AND t11.in_sign_no = t4.in_sign_no
                WHERE
	                t1.id = '{#meeting_id}'
	                AND t3.in_date_key = '{#in_date_key}'
	                AND ISNULL(t3.in_tree_no, 0) <> 0
	                AND t3.in_tree_name NOT IN ('rank56', 'rank78')
                ORDER BY
	                t5.in_code
	                , t3.in_tree_no
	                , t4.in_sign_foot
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_date_key}", cfg.in_date_key);
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);
            Item items = cfg.inn.applySQL(sql);
            int count = items.getItemCount();
            if (count <= 0) return;

            List<TEvent> list = new List<TEvent>();

            for (int i = 0; i < count; i++)
            {
                Item itmEvent = items.getItemByIndex(i);
                string id = itmEvent.getProperty("event_id", "");
                string in_sign_foot = itmEvent.getProperty("in_sign_foot", "");

                TEvent evt = list.Find(x => x.Id == id);
                if (evt == null)
                {
                    evt = new TEvent
                    {
                        Id = id,
                        Value = itmEvent,
                    };
                    list.Add(evt);
                }

                if (in_sign_foot == "1")
                {
                    evt.Foot1 = itmEvent;
                }
                else
                {
                    evt.Foot2 = itmEvent;
                }
            }

            List<string> sql_list = new List<string>();
            for (int i = 0; i < list.Count; i++)
            {
                var evt = list[i];
                if (evt.Foot1 == null) evt.Foot1 = cfg.inn.newItem();
                if (evt.Foot2 == null) evt.Foot2 = cfg.inn.newItem();

                Item item = evt.Value;
                string site_code = item.getProperty("site_code", "");
                string site_english = GetSiteCodeEnglish(site_code);

                List<TField> fields = new List<TField>();
                fields.Add(new TField { Prop = "site", Val = site_english });
                fields.Add(new TField { Prop = "eround", Val = item.getProperty("in_tree_no", "") });
                fields.Add(new TField { Prop = "eweight", Val = item.getProperty("program_name2", "") });
                fields.Add(new TField { Prop = "whited", Val = evt.Foot1.getProperty("player_abbr", "") });
                fields.Add(new TField { Prop = "whiten", Val = evt.Foot1.getProperty("player_name", "") });
                fields.Add(new TField { Prop = "blued", Val = evt.Foot2.getProperty("player_abbr", "") });
                fields.Add(new TField { Prop = "bluen", Val = evt.Foot2.getProperty("player_name", "") });
                fields.Add(new TField { Prop = "whitei", Val = "" });
                fields.Add(new TField { Prop = "bluei", Val = "" });
                fields.Add(new TField { Prop = "whitew", Val = "" });
                fields.Add(new TField { Prop = "bluew", Val = "" });
                fields.Add(new TField { Prop = "whites", Val = "" });
                fields.Add(new TField { Prop = "blues", Val = "" });
                fields.Add(new TField { Prop = "win", Val = "" });
                fields.Add(new TField { Prop = "updateflg", Val = "" });
                fields.Add(new TField { Prop = "mnumber", Val = item.getProperty("mnumber", "") });
                fields.Add(new TField { Prop = "mday", Val = item.getProperty("mday", "") });


                string cols = string.Join(", ", fields.Select(x => "[" + x.Prop + "]"));
                string vals = string.Join(", ", fields.Select(x => "'" + x.Val + "'"));
                string sql_insert = "INSERT INTO [Schedule] (" + cols + ") VALUES(" + vals + ")";

                //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "insert sql: " + sql_insert);
                sql_list.Add(sql_insert);
            }
            ExecuteNonQuery(cfg, sql_list, cfg.AccessConnStr);
        }

        private string GetSiteCodeEnglish(string value)
        {
            switch (value)
            {
                case "1": return "A";
                case "2": return "B";
                case "3": return "C";
                case "4": return "D";
                case "5": return "E";
                case "6": return "F";
                case "7": return "G";
                case "8": return "H";
                case "9": return "I";
                case "10": return "J";
                case "11": return "K";
                case "12": return "L";
                case "13": return "M";
                case "14": return "N";
                case "15": return "O";
                case "16": return "P";
                case "17": return "Q";
                case "18": return "R";
                case "19": return "S";
                case "20": return "T";
                case "21": return "U";
                case "22": return "V";
                case "23": return "W";
                case "24": return "X";
                case "25": return "Y";
                case "26": return "Z";
                default: return "";
            }
        }

        private class TEvent
        {
            /// <summary>
            /// 編號
            /// </summary>
            public string Id { get; set; }

            /// <summary>
            /// 場次資料
            /// </summary>
            public Item Value { get; set; }

            /// <summary>
            /// 白方
            /// </summary>
            public Item Foot1 { get; set; }

            /// <summary>
            /// 藍方
            /// </summary>
            public Item Foot2 { get; set; }
        }

        /// <summary>
        /// 新增選手資料
        /// </summary>
        private void AppendPlayers(TConfig cfg)
        {
            string sql = @"
                SELECT 
					t1.item_number								AS 'mnumber'
	                , REPLACE(t2.in_l1, N'組', '') + t2.in_l2	AS 'gender'
	                , N'第' + t2.in_n3 + N'級'					AS 'weight'
	                , t11.map_short_org							AS 'abbr'
	                , t11.in_name								AS 'name'
                FROM
	                IN_MEETING t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PROGRAM t2 WITH(NOLOCK)
					ON t2.in_meeting = t1.id
                INNER JOIN
	                VU_MEETING_PTEAM t11 WITH(NOLOCK)
	                ON t11.source_id = t2.id
                WHERE
	                t1.id = '{#meeting_id}'
                ORDER BY
	                t2.in_sort_order
	                , t11.in_short_org
	                , t11.in_team
	                , t11.in_sno
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);
            Item items = cfg.inn.applySQL(sql);
            int count = items.getItemCount();
            if (count <= 0) return;

            List<string> sql_list = new List<string>();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                List<TField> fields = new List<TField>();
                fields.Add(new TField { Prop = "gender", Val = item.getProperty("gender", "") });
                fields.Add(new TField { Prop = "weight", Val = item.getProperty("weight", "") });
                fields.Add(new TField { Prop = "abbr", Val = item.getProperty("abbr", "") });
                fields.Add(new TField { Prop = "name", Val = item.getProperty("name", "") });
                fields.Add(new TField { Prop = "mnumber", Val = item.getProperty("mnumber", "") });

                string cols = string.Join(", ", fields.Select(x => "[" + x.Prop + "]"));
                string vals = string.Join(", ", fields.Select(x => "'" + x.Val + "'"));
                string sql_insert = "INSERT INTO [Player] (" + cols + ") VALUES(" + vals + ")";

                //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "insert sql: " + sql_insert);
                sql_list.Add(sql_insert);
            }
            ExecuteNonQuery(cfg, sql_list, cfg.AccessConnStr);
        }

        private class TField
        {
            public string Prop { get; set; }
            public string Val { get; set; }
        }

        /// <summary>
        /// 清除資料表資料
        /// </summary>
        private void ClearTable(TConfig cfg, string table)
        {
            string sql = "DELETE FROM [" + table + "]";
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);
            ExecuteNonQuery(cfg, sql, cfg.AccessConnStr);
        }

        /// <summary>
        /// 存取 Oledb
        /// </summary>
        /// <param name="selectCommandText">執行指令</param>
        /// <param name="connectionString">連線字串</param>
        /// <returns></returns>
        private int ExecuteNonQuery(TConfig cfg, List<string> sql_list, string connectionString)
        {
            int total = 0;

            using (var conn = new System.Data.OleDb.OleDbConnection(connectionString))
            {
                try
                {
                    conn.Open();
                    foreach(var sql in sql_list)
                    {
                        var cmd = new System.Data.OleDb.OleDbCommand(sql, conn);
                        int result = cmd.ExecuteNonQuery();
                        if (result >= 0) total += result;
                    }
                    conn.Close();
                }
                catch (System.Exception ex)
                {
                    cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "ex: " + ex.Message);
                }
            }

            return total;
        }

        /// <summary>
        /// 存取 Oledb
        /// </summary>
        /// <param name="selectCommandText">執行指令</param>
        /// <param name="connectionString">連線字串</param>
        /// <returns></returns>
        private int ExecuteNonQuery(TConfig cfg, string selectCommandText, string connectionString)
        {
            int result = 0;

            using (var conn = new System.Data.OleDb.OleDbConnection(connectionString))
            {
                try
                {
                    var cmd = new System.Data.OleDb.OleDbCommand(selectCommandText, conn);
                    conn.Open();
                    result = cmd.ExecuteNonQuery();
                    conn.Close();
                }
                catch (System.Exception ex)
                {
                    cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "ex: " + ex.Message);
                }
            }

            return result;
        }

        /// <summary>
        /// 存取 Oledb
        /// </summary>
        /// <param name="connectionString">連線字串</param>
        /// <param name="selectCommandText">執行指令</param>
        /// <returns></returns>
        private System.Data.DataTable GetDataTable(string connectionString, string selectCommandText)
        {
            try
            {
                System.Data.DataTable Result = null;
                using (System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection(connectionString))
                {
                    using (System.Data.OleDb.OleDbDataAdapter da = new System.Data.OleDb.OleDbDataAdapter(selectCommandText, conn))
                    {
                        try
                        {
                            Result = new System.Data.DataTable();
                            da.Fill(Result);
                        }
                        catch (System.Exception ex)
                        {
                            Result = null;
                        }
                    }
                }
                return Result;
            }
            catch (System.Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }

            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }

            public string meeting_id { get; set; }
            public string in_date_key { get; set; }

            public string AccessConnStr { get; set; }
        }
    }
}