﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;
using System.Net;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class In_Local_Connect : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: signalr 雙向溝通
                輸入: 
                    - UserName: 說話者
                    - Message: 訊息
                日期: 
                    - 2022-01-05: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]In_Local_Connect";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, this.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                InnH = _InnH,

                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
            };

            //Api
            cfg.api_url = GetVariable(cfg, "api_url");



            return itmR;
        }

        private void Talk(TConfig cfg, string message)
        {
            try
            {
                string fullurl = cfg.api_url + "/talk";
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, fullurl);

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(fullurl);
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";

                var postParams = System.Web.HttpUtility.ParseQueryString(string.Empty);
                postParams.Add("name", "Aras");
                postParams.Add("message", message);

                byte[] byteArray = Encoding.UTF8.GetBytes(postParams.ToString());
                using (Stream reqStream = request.GetRequestStream())
                {
                    reqStream.Write(byteArray, 0, byteArray.Length);
                }//end using

                //API回傳的字串
                string responseStr = "";
                //發出Request
                using (WebResponse response = request.GetResponse())
                {
                    using (StreamReader sr = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
                    {
                        responseStr = sr.ReadToEnd();
                    }//end using  
                }

                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, responseStr);
            }
            catch (Exception ex)
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, ex.Message);
            }
        }

        private string GetVariable(TConfig cfg, string in_name)
        {
            Item item = cfg.inn.applySQL("SELECT TOP 1 in_value FROM IN_VARIABLE WITH(NOLOCK) WHERE in_name = '" + in_name + "'");
            if (item.isError() || item.getResult() == "")
            {
                throw new Exception("查無 " + in_name + " 站台網址");
            }
            return item.getProperty("in_value", "").TrimEnd('/');
        }


        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public Innosoft.InnovatorHelper InnH { get; set; }

            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }

            public string api_url { get; set; }
        }

    }
}