﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;
using System.Net;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class In_Cla_Meeting_Promotion : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 晉段測驗成績
                輸入: 
                    - meeting_id
                    - scene
                日期:
                    - 2022-01-20 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]" + "In_Cla_Meeting_Promotion";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),

                meeting_id = itmR.getProperty("itemid", ""),
                scene = itmR.getProperty("scene", ""),

                doc_name = itmR.getProperty("doc_name", ""),
                CharSet = GetCharSet(),
                font_size = 12,
                row_height = 24,
            };

            if (cfg.meeting_id == "")
            {
                cfg.meeting_id = itmR.getProperty("meeting_id", "");
            }

            List<string> cols = new List<string>
            {
                "keyed_name",
                "in_title",
                "in_echelon",
                "in_annual",
                "in_seminar_type",
            };

            string sql = "SELECT " + string.Join(", ", cols) + " FROM IN_CLA_MEETING WITH(NOLOCK)"
                + " WHERE id = '" + cfg.meeting_id + "'";

            cfg.itmMeeting = cfg.inn.applySQL(sql);
            cfg.mt_title = cfg.itmMeeting.getProperty("in_title", "");

            switch (cfg.scene)
            {
                case "refresh"://更新報名資料
                    Refresh(cfg, itmR);
                    break;

                case "update"://更新晉段測驗成績
                    Update(cfg, itmR);
                    break;

                case "file_modal"://跳窗: 上傳晉段卡照片
                    FileModal(cfg, itmR);
                    break;

                case "new_modal"://跳窗: 新增補測人員
                    NewModal(cfg, itmR);
                    break;

                case "edit_modal"://跳窗: 修改補測人員
                    EditModal(cfg, itmR);
                    break;

                case "import_modal"://跳窗: 調入人員資料
                    ImportModal(cfg, itmR);
                    break;

                case "upload"://上傳晉段卡照片
                    FileUpload(cfg, itmR);
                    break;

                case "merge"://更新補測人員
                    MergeMakeup(cfg, itmR);
                    break;

                case "remove"://移除補測人員
                    RemoveMakeup(cfg, itmR);
                    break;

                case "all_pass"://全部合格
                    AllPass(cfg, itmR);
                    break;

                case "promotion"://登錄晉段紀錄
                    ResumePromotion(cfg, itmR);
                    break;

                case "excel"://下載資料
                    ExportExcel(cfg, itmR);
                    break;

                default:
                    Page(cfg, itmR);
                    break;
            }

            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

            return itmR;
        }

        #region 下載資料

        private void ExportExcel(TConfig cfg, Item itmReturn)
        {
            Spire.Xls.Workbook workbook = new Spire.Xls.Workbook();

            switch (cfg.doc_name)
            {
                case "測驗成績總表":
                    cfg.ext_type = "xlsx";
                    AppendScoreSheet(cfg, workbook);
                    break;

                case "補測名單":
                    cfg.ext_type = "pdf";
                    AppendMakeupSheets(cfg, workbook);
                    break;

                default:
                    cfg.ext_type = "xlsx";
                    AppendScoreSheet(cfg, workbook);
                    break;
            }

            //移除空 sheet
            workbook.Worksheets[0].Remove();
            workbook.Worksheets[0].Remove();
            workbook.Worksheets[0].Remove();

            Item itmXls = cfg.inn.applyMethod("In_Variable_File", "<in_name></in_name>");
            string export_path = itmXls.getProperty("export_path", "");

            string guid = System.Guid.NewGuid().ToString().Substring(0, 6).ToUpper()
                + "_" + DateTime.Now.ToString("yyyyMMddHHmmssfff");

            string ext_name = "." + cfg.ext_type;
            string xls_name = cfg.itmMeeting.getProperty("in_title", "") + "_" + cfg.doc_name + "_" + guid;
            string xls_file = export_path + "\\" + xls_name + ext_name;

            if (cfg.ext_type == "pdf")
            {
                workbook.SaveToFile(xls_file, Spire.Xls.FileFormat.PDF);
            }
            else
            {
                workbook.SaveToFile(xls_file, Spire.Xls.ExcelVersion.Version2013);
            }

            itmReturn.setProperty("xls_name", xls_name + ext_name);
        }

        //補測名單
        private void AppendMakeupSheets(TConfig cfg, Spire.Xls.Workbook workbook)
        {
            int mt_echelon = GetIntVal(cfg.itmMeeting.getProperty("in_echelon", "0"));

            List<TField> fields = new List<TField>();
            fields.Add(new TField { ci = 1, title = "編號", property = "no", css = TCss.Center, width = 5 });
            fields.Add(new TField { ci = 2, title = "段位", property = "in_degree", css = TCss.Center, width = 6 });
            fields.Add(new TField { ci = 3, title = "中文姓名", property = "in_name", css = TCss.Center, width = 10 });
            fields.Add(new TField { ci = 4, title = "所屬教練", property = "in_stuff_c1", css = TCss.Center, width = 10 });

            int ci = 5;
            for (int i = 4; i > 0; i--)
            {
                //往前推四個梯次
                int echelon = mt_echelon - i;
                fields.Add(new TField { ci = ci, title = echelon.ToString(), property = "ech_" + echelon, css = TCss.None, width = 10 });
                ci++;
            }

            fields.Add(new TField { ci = ci, title = "備註", property = "in_score_note", css = TCss.None, width = 10 });

            MapCharSet(cfg, fields);

            Item items = GetMeetingPromotions(cfg, filter_makeup: true, order_by_degree: true);
            List<TCommittee> committees = MapCommittees(cfg, items);

            for (int i = 0; i < committees.Count; i++)
            {
                TCommittee committee = committees[i];
                AppendMakeupSheet(cfg, workbook, fields, committee);
            }
        }

        //補測名單
        private void AppendMakeupSheet(TConfig cfg, Spire.Xls.Workbook workbook, List<TField> fields, TCommittee committee)
        {
            Spire.Xls.Worksheet sheet = workbook.CreateEmptySheet();
            sheet.Name = committee.city;

            //頁面配置-邊界
            sheet.PageSetup.TopMargin = 0.5;
            // sheet.PageSetup.BottomMargin = 0.5;
            // sheet.PageSetup.LeftMargin = 0.5;
            // sheet.PageSetup.RightMargin = 0.5;

            //列印標題
            sheet.PageSetup.PrintTitleRows = "$1:$2";

            var last_field = fields.Last();

            string sheet_title = cfg.mt_title.Replace("晉段申請", "補測名單")
                + "    " + committee.city;

            //活動標題
            var mt_mr = sheet.Range["A1:" + last_field.cs + "1"];
            mt_mr.Merge();
            mt_mr.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
            mt_mr.Style.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;
            mt_mr.Style.Font.Size = 16;
            //mt_mr.Style.Font.IsBold = true;
            mt_mr.RowHeight = 40;
            mt_mr.Text = sheet_title;


            int mnRow = 2;
            int wsRow = 2;

            //標題列
            SetHeadRow(cfg, sheet, wsRow, fields);
            wsRow++;

            ////凍結窗格
            //sheet.FreezePanes(wsRow, fields.Last().ci + 1);

            int count = committee.items.Count;

            for (int i = 0; i < count; i++)
            {
                var item = committee.items[i];
                string in_degree = item.getProperty("in_degree", "");

                item.setProperty("no", (i + 1).ToString());
                item.setProperty("in_degree", in_degree.Replace("000", ""));

                SetItemCell(cfg, sheet, wsRow, item, fields);

                wsRow++;
            }

            //設定格線
            SetRangeBorder(sheet, fields, mnRow, wsRow);

            //設定欄寬
            SetColumnWidth(sheet, fields);
        }


        //測驗成績總表
        private void AppendScoreSheet(TConfig cfg, Spire.Xls.Workbook workbook)
        {
            Spire.Xls.Worksheet sheet = workbook.CreateEmptySheet();
            sheet.Name = cfg.doc_name;

            //頁面配置-邊界
            // sheet.PageSetup.TopMargin = 0.5;
            // sheet.PageSetup.BottomMargin = 0.5;
            // sheet.PageSetup.LeftMargin = 0.5;
            // sheet.PageSetup.RightMargin = 0.5;

            //列印標題
            sheet.PageSetup.PrintTitleRows = "$1:$2";


            List<TField> fields = new List<TField>();
            fields.Add(new TField { title = "編號", property = "no", css = TCss.Center, width = 5 });
            fields.Add(new TField { title = "區別", property = "in_area", css = TCss.None, width = 8 });
            fields.Add(new TField { title = "中文姓名", property = "in_name", css = TCss.None, width = 10 });
            fields.Add(new TField { title = "段位", property = "in_l1", css = TCss.None, width = 8 });
            fields.Add(new TField { title = "所屬教練", property = "in_stuff_c1", css = TCss.None, width = 10 });
            fields.Add(new TField { title = "所屬道館", property = "in_gym", css = TCss.None, width = 15 });
            fields.Add(new TField { title = "所屬委員會", property = "in_committee_short", css = TCss.None, width = 15 });
            fields.Add(new TField { title = "足技", property = "in_score_foot", css = TCss.Center, width = 5 });
            fields.Add(new TField { title = "型", property = "in_score_poomsae", css = TCss.Center, width = 5 });
            fields.Add(new TField { title = "對練", property = "in_score_sparring", css = TCss.Center, width = 5 });
            fields.Add(new TField { title = "擊破", property = "in_score_beat", css = TCss.Center, width = 5 });
            fields.Add(new TField { title = "補測", property = "makeup_yn", css = TCss.Center, width = 5 });
            fields.Add(new TField { title = "補測項目", property = "in_makeup_note", css = TCss.None, width = 20 });

            MapCharSetAndIndex(cfg, fields);

            //最後欄
            var last_field = fields.Last();

            //活動標題
            var mt_mr = sheet.Range["A1:" + last_field.cs + "1"];
            mt_mr.Merge();
            mt_mr.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
            mt_mr.Style.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;
            mt_mr.Text = cfg.mt_title;
            mt_mr.Style.Font.Size = 16;
            //mt_mr.Style.Font.IsBold = true;
            mt_mr.RowHeight = 40;


            int mnRow = 2;
            int wsRow = 2;

            //標題列
            SetHeadRow(cfg, sheet, wsRow, fields);
            wsRow++;

            //凍結窗格
            sheet.FreezePanes(wsRow, last_field.ci + 1);

            Item items = GetMeetingPromotions(cfg);
            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                string in_makeup_yn = item.getProperty("in_makeup_yn", "");
                string makeup_yn = in_makeup_yn == "1" ? "是" : "";

                item.setProperty("no", (i + 1).ToString());
                item.setProperty("makeup_yn", makeup_yn);

                SetItemCell(cfg, sheet, wsRow, item, fields);

                wsRow++;
            }

            //設定格線
            SetRangeBorder(sheet, fields, mnRow, wsRow);

            //設定欄寬
            SetColumnWidth(sheet, fields);
        }

        private List<TCommittee> MapCommittees(TConfig cfg, Item items)
        {
            List<TCommittee> result = new List<TCommittee>();

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                string in_committee = item.getProperty("in_committee", "");
                string in_committee_sno = item.getProperty("in_committee_sno", "");
                string in_committee_short = item.getProperty("in_committee_short", "");

                string city = in_committee_short.Replace("跆委會", "")
                    .Replace("跆協會", "");

                TCommittee committee = result.Find(x => x.in_sno == in_committee_sno);
                if (committee == null)
                {
                    committee = new TCommittee
                    {
                        in_name = in_committee,
                        in_sno = in_committee_sno,
                        in_short_name = in_committee_short,
                        city = city,
                        items = new List<Item>(),
                    };

                    result.Add(committee);
                }

                committee.items.Add(item);
            }

            return result.OrderBy(x => x.in_sno).ToList();
        }

        private class TCommittee
        {
            public string in_name { get; set; }
            public string in_sno { get; set; }
            public string in_short_name { get; set; }
            public string city { get; set; }

            public List<Item> items { get; set; }
        }

        private class TField
        {
            public string title { get; set; }
            public string property { get; set; }
            public TCss css { get; set; }
            public int ci { get; set; }
            public string cs { get; set; }
            public int width { get; set; }
            public Func<TConfig, TField, Item, string> getValue { get; set; }

            public bool is_merge { get; set; }
        }

        private enum TCss
        {
            None = 0,
            Center = 10,
            Text = 100,
            Number = 200,
            Money = 300,
            Day = 400,
        }

        private string[] GetCharSet()
        {
            return new string[] { "", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
        }

        //設定標題列
        private void SetHeadRow(TConfig cfg, Spire.Xls.Worksheet sheet, int wsRow, List<TField> fields, bool is_bold = false)
        {
            foreach (var field in fields)
            {
                string cr = field.cs + wsRow;
                SetHeadCell(cfg, sheet, cr, field.title, is_bold);
            }

            var row_mr = sheet.Range["A" + wsRow + ":" + fields.Last().cs + wsRow];
            row_mr.RowHeight = cfg.row_height;
            row_mr.Style.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;
        }

        //設定標題列
        private void SetHeadCell(TConfig cfg, Spire.Xls.Worksheet sheet, string cr, string value, bool is_bold = false)
        {
            var range = sheet.Range[cr];
            range.Text = value;
            range.Style.Font.IsBold = is_bold;
            range.Style.Font.Size = 12;
            //range.Style.Font.Color = System.Drawing.Color.White;
            //range.Style.Color = System.Drawing.Color.FromArgb(41, 92, 144);//#295C90
            range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
        }

        //設定資料列
        private void SetItemCell(TConfig cfg, Spire.Xls.Worksheet sheet, int wsRow, Item item, List<TField> fields)
        {
            foreach (var field in fields)
            {
                var cr = field.cs + wsRow;
                var va = "";

                if (field.getValue != null)
                {
                    va = field.getValue(cfg, field, item);
                }
                else if (field.property != "")
                {
                    va = item.getProperty(field.property, "");
                }

                SetCell(cfg, sheet, cr, va, field.css);
            }

            var row_mr = sheet.Range["A" + wsRow + ":" + fields.Last().cs + wsRow];
            row_mr.RowHeight = cfg.row_height;
            row_mr.Style.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;
        }

        //設定資料格
        private void SetCell(TConfig cfg, Spire.Xls.Worksheet sheet, string cr, string value, TCss css)
        {
            var range = sheet.Range[cr];
            range.Style.Font.Size = cfg.font_size;

            switch (css)
            {
                case TCss.None:
                    range.Text = value;
                    break;
                case TCss.Center:
                    range.Text = value;
                    range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                    break;
                case TCss.Text:
                    range.Text = "'" + value;
                    range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                    break;
                case TCss.Number:
                    range.NumberValue = GetIntVal(value);
                    range.NumberFormat = "0";
                    range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Right;
                    break;
                case TCss.Money:
                    range.NumberValue = GetIntVal(value);
                    range.NumberFormat = "$ #,##0";
                    break;
                case TCss.Day:
                    range.Text = DateTimeStr(value, "yyyy/MM/dd");
                    range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                    break;
            }
        }

        //設定格線
        private void SetRangeBorder(Spire.Xls.Worksheet sheet, string pos)
        {
            var range = sheet.Range[pos];
            range.BorderInside(Spire.Xls.LineStyleType.Thin, System.Drawing.Color.Black);
            range.BorderAround(Spire.Xls.LineStyleType.Thin, System.Drawing.Color.Black);
        }

        //設定格線
        private void SetRangeBorder(Spire.Xls.Worksheet sheet, List<TField> fields, int mnRow, int wsRow)
        {
            var tbl_ps = fields.First().cs + mnRow;
            var tbl_pe = fields.Last().cs + (wsRow - 1);
            SetRangeBorder(sheet, tbl_ps + ":" + tbl_pe);
        }

        private void MapCharSet(TConfig cfg, List<TField> fields)
        {
            foreach (var field in fields)
            {
                field.cs = cfg.CharSet[field.ci];
            }
        }

        private void MapCharSetAndIndex(TConfig cfg, List<TField> fields)
        {
            for (int i = 0; i < fields.Count; i++)
            {
                var field = fields[i];
                field.ci = i + 1;
                field.cs = cfg.CharSet[field.ci];
            }
        }

        private void AutoFit(Spire.Xls.Worksheet sheet, List<TField> fields)
        {
            foreach (var field in fields)
            {
                sheet.Columns[field.ci - 1].AutoFitColumns();
            }
        }

        private void SetColumnWidth(Spire.Xls.Worksheet sheet, List<TField> fields)
        {
            foreach (var field in fields)
            {
                sheet.Columns[field.ci - 1].ColumnWidth = field.width;
            }
        }
        #endregion 下載資料

        //全部合格
        private void AllPass(TConfig cfg, Item itmReturn)
        {
            string sql = "UPDATE IN_CLA_MEETING_PROMOTION SET"
                + " in_score_state = N'合格'"
                + ", in_score_foot = 'O'"
                + ", in_score_poomsae = 'O'"
                + ", in_score_sparring = 'O'"
                + ", in_score_beat = 'O'"
                + " WHERE source_id = '" + cfg.meeting_id + "'"
                + "";

            Item itmResult = cfg.inn.applySQL(sql);

            if (itmResult.isError())
            {
                throw new Exception("執行全部合格發生錯誤");
            }
        }

        //登錄晉段紀錄
        private void ResumePromotion(TConfig cfg, Item itmReturn)
        {
            string mt_annual = cfg.itmMeeting.getProperty("in_annual", "");
            string mt_seminar_type = cfg.itmMeeting.getProperty("in_seminar_type", "");

            string mt_date = itmReturn.getProperty("in_date", "");

            if (mt_annual == "") throw new Exception("未設定晉段民國年度");
            if (mt_seminar_type == "") throw new Exception("未設定晉段國內國際類型");
            if (mt_date == "") throw new Exception("請輸入晉升日期");

            string promotion_day = GetPromotionDay(cfg, mt_annual, mt_date);
            string in_is_global = mt_seminar_type == "degree_gl" ? "1" : "0";

            string sql = "SELECT * FROM IN_CLA_MEETING_PROMOTION WITH(NOLOCK)"
                + " WHERE source_id = '" + cfg.meeting_id + "'";

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_score_foot = item.getProperty("in_score_foot", "");
                string in_score_poomsae = item.getProperty("in_score_poomsae", "");
                string in_score_sparring = item.getProperty("in_score_sparring", "");
                string in_score_beat = item.getProperty("in_score_beat", "");
                string in_score_state = item.getProperty("in_score_state", "");

                bool is_qualified = false;

                switch (in_score_state)
                {
                    case "":
                        is_qualified = in_score_foot == "O"
                            && in_score_poomsae == "O"
                            && in_score_sparring == "O"
                            && in_score_beat == "O";
                        break;

                    case "合格":
                        is_qualified = true;
                        break;

                    default:
                        is_qualified = false;
                        break;

                }
                if (is_qualified)
                {
                    //更新晉段紀錄
                    MergeResumePromotion(cfg, item, promotion_day, in_is_global);
                }
                else
                {
                    //移除晉段紀錄
                    RemoveResumePromotion(cfg, item);
                }
            }
        }

        //移除晉段紀錄
        private void RemoveResumePromotion(TConfig cfg, Item item)
        {
            string in_cla_promotion = item.getProperty("id", "");

            Item itmRPro = cfg.inn.newItem("In_Resume_Promotion", "delete");
            itmRPro.setAttribute("where", "in_cla_promotion = '" + in_cla_promotion + "'");
            itmRPro = itmRPro.apply();

            if (itmRPro.isError())
            {
                //throw new Exception("移除晉段紀錄發生錯誤");
            }
        }

        //更新晉段紀錄
        private void MergeResumePromotion(TConfig cfg, Item item, string promotion_day, string in_is_global)
        {
            string in_cla_promotion = item.getProperty("id", "");
            string in_makeup_yn = item.getProperty("in_makeup_yn", "");
            string resume_id = item.getProperty("related_id", "");

            Item itmRPro = cfg.inn.newItem("In_Resume_Promotion", "merge");
            itmRPro.setAttribute("where", "in_cla_promotion = '" + in_cla_promotion + "'");

            itmRPro.setProperty("source_id", resume_id);
            itmRPro.setProperty("in_cla_meeting", cfg.meeting_id);
            itmRPro.setProperty("in_cla_promotion", in_cla_promotion);
            itmRPro.setProperty("in_echelon", item.getProperty("in_echelon", ""));
            itmRPro.setProperty("in_manager_area", item.getProperty("in_area", ""));
            itmRPro.setProperty("in_date", promotion_day);
            itmRPro.setProperty("in_apply_degree", item.getProperty("in_degree", ""));
            itmRPro.setProperty("in_is_global", in_is_global);
            itmRPro.setProperty("in_score_photo", item.getProperty("in_score_photo", ""));

            if (in_makeup_yn == "1")
            {
                itmRPro.setProperty("in_note", "補測通過");
            }

            itmRPro.setProperty("in_sno", item.getProperty("in_sno", ""));

            itmRPro = itmRPro.apply();

            if (itmRPro.isError())
            {
                throw new Exception("更新晉段紀錄發生錯誤");
            }
        }

        //刪除補測人員
        private void RemoveMakeup(TConfig cfg, Item itmReturn)
        {
            string id = itmReturn.getProperty("id", "");

            string sql = "DELETE FROM IN_CLA_MEETING_PROMOTION"
                + " WHERE id = '" + id + "'"
                + " AND in_makeup_yn = 1";

            Item itmResult = cfg.inn.applySQL(sql);

            if (itmReturn.isError())
            {
                throw new Exception("刪除失敗");
            }
        }

        //跳窗: 新增補測人員
        private void NewModal(TConfig cfg, Item itmReturn)
        {
            itmReturn.setProperty("crud", "create");
        }

        //跳窗: 修改補測人員
        private void EditModal(TConfig cfg, Item itmReturn)
        {
            string id = itmReturn.getProperty("id", "");

            Item itmPromotion = cfg.inn.getItemById("In_Cla_Meeting_Promotion", id);
            if (itmPromotion.isError() || itmPromotion.getResult() == "")
            {
                throw new Exception("查無測驗紀錄");
            }

            string resume_id = itmPromotion.getProperty("related_id", "");
            Item itmResume = cfg.inn.getItemById("In_Resume", resume_id);

            SetUserModal(cfg, itmResume, itmReturn);

            itmReturn.setProperty("in_area", itmPromotion.getProperty("in_area", ""));
            itmReturn.setProperty("in_l1", itmPromotion.getProperty("in_l1", ""));
            itmReturn.setProperty("in_num", itmPromotion.getProperty("in_num", ""));
            itmReturn.setProperty("in_serial", itmPromotion.getProperty("in_serial", ""));
            itmReturn.setProperty("in_makeup_note", itmPromotion.getProperty("in_makeup_note", ""));

            itmReturn.setProperty("in_committee", itmPromotion.getProperty("in_committee", ""));
            itmReturn.setProperty("in_committee_sno", itmPromotion.getProperty("in_committee_sno", ""));
            itmReturn.setProperty("in_committee_short", itmPromotion.getProperty("in_committee_short", ""));

            itmReturn.setProperty("crud", "update");
        }

        //編輯補測人員
        private void MergeMakeup(TConfig cfg, Item itmReturn)
        {
            string mt_title = cfg.itmMeeting.getProperty("in_title", "");
            string mt_annual = cfg.itmMeeting.getProperty("in_annual", "");
            string mt_echelon = cfg.itmMeeting.getProperty("in_echelon", "");

            string id = itmReturn.getProperty("id", "");
            string crud = itmReturn.getProperty("crud", "");
            string resume_id = itmReturn.getProperty("resume_id", "");
            string in_name = itmReturn.getProperty("in_name", "");
            string in_sno = itmReturn.getProperty("in_sno", "");

            string in_gym = itmReturn.getProperty("in_gym", "");
            string in_gym_sno = itmReturn.getProperty("in_gym_sno", "");

            string in_committee = itmReturn.getProperty("in_committee", "");
            string in_committee_short = itmReturn.getProperty("in_committee_short", "");
            string in_committee_sno = itmReturn.getProperty("in_committee_sno", "");

            string in_num = itmReturn.getProperty("in_num", "");
            string in_serial = itmReturn.getProperty("in_serial", "");
            string in_area = itmReturn.getProperty("in_area", "");
            string in_l1 = itmReturn.getProperty("in_l1", "");
            string in_makeup_note = itmReturn.getProperty("in_makeup_note", "");

            if (resume_id == "")
            {
                throw new Exception("人員資料錯誤");
            }

            Item itmResume = cfg.inn.getItemById("In_Resume", resume_id);
            if (itmResume.isError())
            {
                throw new Exception("查無人員資料");
            }

            //過去無報名資料，因此以晉段卡號為鍵值
            string in_key = in_num;
            string sql = "";

            Item itmOld = null;

            if (crud == "create")
            {
                //新增 - 資料防呆
                sql = "SELECT id FROM In_Cla_Meeting_Promotion WITH(NOLOCK)"
                + " WHERE in_key = '" + in_key + "'"
                + " AND in_serial = '" + in_serial + "'"
                + "";

                itmOld = cfg.inn.applySQL(sql);
                if (!itmOld.isError() && itmOld.getResult() != "")
                {
                    throw new Exception("該晉段卡已有第 " + in_serial + " 次測驗紀錄");
                }

                sql = "SELECT id FROM In_Cla_Meeting_Promotion WITH(NOLOCK)"
                   + " WHERE source_id = '" + cfg.meeting_id + "'"
                   + " AND in_sno = '" + in_sno + "'"
                   + "";

                itmOld = cfg.inn.applySQL(sql);
                if (!itmOld.isError() && itmOld.getResult() != "")
                {
                    throw new Exception("【" + mt_title + "】已有 " + in_name + " 報名資料");
                }
            }
            else if (crud == "update")
            {
                //修改 - 資料防呆
                itmOld = cfg.inn.getItemById("In_Cla_Meeting_Promotion", id);
                if (itmOld.isError() || itmOld.getResult() == "")
                {
                    throw new Exception("查無測驗紀錄");
                }
            }
            else
            {
                throw new Exception("參數錯誤");
            }

            Item itmMAreas = GetMeetingAreas(cfg);
            var area_map = MapItem(itmMAreas, "in_area");

            Item itmBelts = GetBelts(cfg);
            var belt_map = MapItem(itmBelts, "label");


            Item itmMPromotion = cfg.inn.newItem("In_Cla_Meeting_Promotion", "merge");
            if (crud == "create")
            {
                itmMPromotion.setAttribute("where", "in_key = '" + in_key + "' AND in_serial = " + in_serial);
            }
            else if (crud == "update")
            {
                itmMPromotion.setAttribute("where", "id = '" + id + "'");
            }

            itmMPromotion.setProperty("in_key", in_key);
            itmMPromotion.setProperty("in_serial", in_serial);
            itmMPromotion.setProperty("in_num", in_num);
            itmMPromotion.setProperty("in_makeup_yn", "0");
            itmMPromotion.setProperty("in_makeup_note", "");

            itmMPromotion.setProperty("source_id", cfg.meeting_id);
            itmMPromotion.setProperty("related_id", resume_id);
            itmMPromotion.setProperty("in_user", "");

            itmMPromotion.setProperty("in_annual", mt_annual);
            itmMPromotion.setProperty("in_echelon", mt_echelon);

            itmMPromotion.setProperty("in_name", itmResume.getProperty("in_name", ""));
            itmMPromotion.setProperty("in_sno", itmResume.getProperty("in_sno", ""));
            itmMPromotion.setProperty("in_birth", itmResume.getProperty("in_birth", ""));
            itmMPromotion.setProperty("in_gender", itmResume.getProperty("in_gender", ""));

            itmMPromotion.setProperty("in_stuff_c1", itmResume.getProperty("in_stuff_c1", ""));
            itmMPromotion.setProperty("in_stuff_c1_sno", "");

            itmMPromotion.setProperty("in_gym", in_gym);
            itmMPromotion.setProperty("in_gym_sno", in_gym_sno);

            itmMPromotion.setProperty("in_committee", in_committee);
            itmMPromotion.setProperty("in_committee_sno", in_committee_sno);
            itmMPromotion.setProperty("in_committee_short", in_committee_short);

            if (area_map.ContainsKey(in_area))
            {
                Item itmMArea = area_map[in_area];
                itmMPromotion.setProperty("in_area", itmMArea.getProperty("in_area", ""));
                itmMPromotion.setProperty("in_area_sort", itmMArea.getProperty("in_sort_order", ""));
                itmMPromotion.setProperty("in_location", itmMArea.getProperty("in_location", ""));
                itmMPromotion.setProperty("in_place", itmMArea.getProperty("in_place", ""));
                itmMPromotion.setProperty("in_date", DateTimeStr(itmMArea.getProperty("in_date", "")));
                itmMPromotion.setProperty("in_area_id", itmMArea.getProperty("id", ""));
            }
            else
            {
                itmMPromotion.setProperty("in_area", in_area);
                itmMPromotion.setProperty("in_area_sort", "100");
                itmMPromotion.setProperty("in_location", "");
                itmMPromotion.setProperty("in_place", "");
                itmMPromotion.setProperty("in_date", "");
                itmMPromotion.setProperty("in_area_id", "");
            }

            if (belt_map.ContainsKey(in_l1))
            {
                Item itmBelt = belt_map[in_l1];
                itmMPromotion.setProperty("in_l1", in_l1);
                itmMPromotion.setProperty("in_degree", itmBelt.getProperty("value", ""));
            }
            else
            {
                itmMPromotion.setProperty("in_l1", in_l1);
                itmMPromotion.setProperty("in_degree", "0");
            }

            itmMPromotion.setProperty("in_makeup_yn", "1");
            itmMPromotion.setProperty("in_makeup_note", in_makeup_note);

            Item itmResult = itmMPromotion.apply();

            if (itmResult.isError())
            {
                throw new Exception("補測人員新增失敗");
            }
        }

        //載入
        private void ImportModal(TConfig cfg, Item itmReturn)
        {
            string in_sno = itmReturn.getProperty("in_sno", "");
            if (in_sno == "") return;

            Item itmResume = cfg.inn.newItem("In_Resume", "get");
            itmResume.setProperty("in_sno", in_sno);
            itmResume = itmResume.apply();

            SetUserModal(cfg, itmResume, itmReturn);

            itmReturn.setProperty("crud", "create");
        }

        //載入
        private void SetUserModal(TConfig cfg, Item itmResume, Item itmReturn)
        {
            if (itmResume.isError() || itmResume.getResult() == "")
            {
                throw new Exception("查無人員資料");
            }

            itmReturn.setProperty("resume_id", itmResume.getProperty("id", ""));
            itmReturn.setProperty("in_name", itmResume.getProperty("in_name", ""));
            itmReturn.setProperty("in_en_name", itmResume.getProperty("in_en_name", ""));
            itmReturn.setProperty("in_stuff_c1", itmResume.getProperty("in_stuff_c1", ""));
            itmReturn.setProperty("inn_sno", itmResume.getProperty("in_sno", ""));

            //所屬單位
            string in_current_org = itmResume.getProperty("in_current_org", "");

            //所屬道館資料
            Item itmGym = cfg.inn.newItem("In_Resume", "get");
            itmGym.setProperty("in_name", in_current_org);
            itmGym.setProperty("in_member_type", "vip_gym");
            itmGym = itmGym.apply();

            if (itmGym.isError() || itmGym.getResult() == "")
            {
                itmGym = cfg.inn.newItem();
                itmReturn.setProperty("in_gym", itmGym.getProperty("in_current_org", ""));
                itmReturn.setProperty("in_gym_sno", " ");
            }
            else
            {
                itmReturn.setProperty("in_gym", itmGym.getProperty("in_name", ""));
                itmReturn.setProperty("in_gym_sno", itmGym.getProperty("in_sno", ""));
            }


            //所屬委員會 resume_id
            string in_manager_org = itmGym.getProperty("in_manager_org", "");

            //所屬委員會
            Item itmCommittee = cfg.inn.newItem("In_Resume", "get");
            itmCommittee.setProperty("id", in_manager_org);
            itmCommittee.setProperty("in_member_type", "area_cmt");
            itmCommittee = itmCommittee.apply();

            if (itmCommittee.isError() || itmCommittee.getResult() == "")
            {
                itmReturn.setProperty("in_committee", " ");
                itmReturn.setProperty("in_committee_short", " ");
                itmReturn.setProperty("in_committee_sno", " ");
            }
            else
            {
                itmReturn.setProperty("in_committee", itmCommittee.getProperty("in_name", ""));
                itmReturn.setProperty("in_committee_short", itmCommittee.getProperty("in_short_org", ""));
                itmReturn.setProperty("in_committee_sno", itmCommittee.getProperty("in_sno", ""));
            }
        }

        //上傳
        private void FileUpload(TConfig cfg, Item itmReturn)
        {
            string id = itmReturn.getProperty("id", "");
            string in_file = itmReturn.getProperty("in_file", "");

            Item item = cfg.inn.newItem("IN_CLA_MEETING_PROMOTION", "merge");
            item.setAttribute("where", "id = '" + id + "'");
            item.setProperty("in_score_photo", in_file);
            Item itmResult = item.apply();

            if (itmResult.isError())
            {
                throw new Exception("上傳失敗，請稍後再試");
            }
        }

        //跳窗
        private void FileModal(TConfig cfg, Item itmReturn)
        {
            string id = itmReturn.getProperty("id", "");
            Item item = cfg.inn.getItemById("IN_CLA_MEETING_PROMOTION", id);

            if (item.isError())
            {
                return;
            }

            itmReturn.setProperty("in_name", item.getProperty("in_name", ""));
            itmReturn.setProperty("in_echelon", item.getProperty("in_echelon", ""));
            itmReturn.setProperty("in_serial", item.getProperty("in_serial", ""));
            itmReturn.setProperty("old_file", item.getProperty("in_score_photo", ""));
            itmReturn.setProperty("old_file_ext", " ");
        }

        //刷新資料
        private void Refresh(TConfig cfg, Item itmReturn)
        {
            MergeMeetingPromotions(cfg);
        }

        //更新資料
        private void Update(TConfig cfg, Item itmReturn)
        {
            string promotion_id = itmReturn.getProperty("promotion_id", "");
            string property = itmReturn.getProperty("property", "");
            string value = itmReturn.getProperty("value", "");

            Item itmOld = cfg.inn.newItem("In_Cla_Meeting_Promotion", "merge");
            itmOld.setAttribute("where", "id = '" + promotion_id + "'");

            if (property == "in_score_state" && value == "合格")
            {
                itmOld.setProperty("in_score_foot", "O");
                itmOld.setProperty("in_score_poomsae", "O");
                itmOld.setProperty("in_score_sparring", "O");
                itmOld.setProperty("in_score_beat", "O");
                itmOld.setProperty("in_score_state", "合格");
            }
            else
            {
                itmOld.setProperty(property, value);
                if (value == "X")
                {
                    itmOld.setProperty("in_score_state", "須補測");
                }
            }

            Item itmResult = itmOld = itmOld.apply();

            if (itmResult.isError())
            {
                throw new Exception("更新失敗");
            }
        }

        //頁面
        private void Page(TConfig cfg, Item itmReturn)
        {
            itmReturn.setProperty("in_title", cfg.itmMeeting.getProperty("in_title", ""));
            itmReturn.setProperty("keyed_name", cfg.itmMeeting.getProperty("keyed_name", ""));

            Item items = GetMeetingPromotions(cfg);
            if (items.isError() || items.getResult() == "")
            {
                return;
            }

            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_name = item.getProperty("in_name", "");
                string in_score_photo = item.getProperty("in_score_photo", "");
                string in_makeup_yn = item.getProperty("in_makeup_yn", "");
                string resume_id = item.getProperty("related_id", "");

                string score_photo = "晉段卡<i class='fa fa-cloud-upload'></i>";
                if (in_score_photo != "")
                {
                    score_photo = "晉段卡<i class='fa fa-file-photo-o'></i>";
                }

                string name = in_name;
                if (in_makeup_yn == "1")
                {
                    name = in_name + "<a href='javascript:void()' onclick='UpdateModal_Click(this)'> (補) </a>";
                }
                name += " <a target='_blank' href='c.aspx?page=SingleMemberView.html&method=In_Group_Resume_View&id=" + resume_id + "'"
                + " > <i class='fa fa-user'><i/> </a>";

                item.setType("In_Cla_Meeting_Promotion");
                item.setProperty("inn_name", name);
                item.setProperty("inn_photo", score_photo);
                itmReturn.addRelationship(item);
            }
        }

        private void MergeMeetingPromotions(TConfig cfg)
        {
            string mt_title = cfg.itmMeeting.getProperty("in_title", "");
            string mt_annual = cfg.itmMeeting.getProperty("in_annual", "");
            string mt_echelon = cfg.itmMeeting.getProperty("in_echelon", "");

            Item itmMUsers = GetMeetingUsers(cfg);

            Item itmMAreas = GetMeetingAreas(cfg);
            var area_map = MapItem(itmMAreas, "in_area");

            Item itmBelts = GetBelts(cfg);
            var belt_map = MapItem(itmBelts, "label");

            int count = itmMUsers.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item itmMUser = itmMUsers.getItemByIndex(i);

                string in_serial = "1";

                string muid = itmMUser.getProperty("id", "");
                string in_l1 = itmMUser.getProperty("in_l1", "");
                string in_degree_area = itmMUser.getProperty("in_degree_area", "");
                string resume_id = itmMUser.getProperty("resume_id", "");

                Item itmMPromotion = cfg.inn.newItem("In_Cla_Meeting_Promotion", "merge");
                itmMPromotion.setAttribute("where", "in_key = '" + muid + "' AND in_serial = " + in_serial);

                itmMPromotion.setProperty("in_key", muid);
                itmMPromotion.setProperty("in_serial", in_serial);
                itmMPromotion.setProperty("in_makeup_yn", "0");
                itmMPromotion.setProperty("in_makeup_note", "");

                itmMPromotion.setProperty("source_id", cfg.meeting_id);
                itmMPromotion.setProperty("related_id", resume_id);
                itmMPromotion.setProperty("in_user", muid);

                itmMPromotion.setProperty("in_annual", mt_annual);
                itmMPromotion.setProperty("in_echelon", mt_echelon);

                itmMPromotion.setProperty("in_name", itmMUser.getProperty("in_name", ""));
                itmMPromotion.setProperty("in_sno", itmMUser.getProperty("in_sno", ""));
                itmMPromotion.setProperty("in_birth", DateTimeStr(itmMUser.getProperty("in_birth", ""), hours: 8));
                itmMPromotion.setProperty("in_gender", itmMUser.getProperty("in_gender", ""));
                itmMPromotion.setProperty("in_stuff_c1", itmMUser.getProperty("in_stuff_c1", ""));
                itmMPromotion.setProperty("in_stuff_c1_sno", "");

                itmMPromotion.setProperty("in_gym", itmMUser.getProperty("in_current_org", ""));
                itmMPromotion.setProperty("in_gym_sno", itmMUser.getProperty("in_creator_sno", ""));

                itmMPromotion.setProperty("in_committee", itmMUser.getProperty("in_committee", ""));
                itmMPromotion.setProperty("in_committee_sno", itmMUser.getProperty("committee_sno", ""));
                itmMPromotion.setProperty("in_committee_short", itmMUser.getProperty("committee_short_name", ""));

                if (area_map.ContainsKey(in_degree_area))
                {
                    Item itmMArea = area_map[in_degree_area];
                    itmMPromotion.setProperty("in_area", itmMArea.getProperty("in_area", ""));
                    itmMPromotion.setProperty("in_area_sort", itmMArea.getProperty("in_sort_order", ""));
                    itmMPromotion.setProperty("in_location", itmMArea.getProperty("in_location", ""));
                    itmMPromotion.setProperty("in_place", itmMArea.getProperty("in_place", ""));
                    itmMPromotion.setProperty("in_date", DateTimeStr(itmMArea.getProperty("in_date", "")));
                    itmMPromotion.setProperty("in_area_id", itmMArea.getProperty("id", ""));
                }
                else
                {
                    itmMPromotion.setProperty("in_area", in_degree_area);
                    itmMPromotion.setProperty("in_area_sort", "100");
                    itmMPromotion.setProperty("in_location", "");
                    itmMPromotion.setProperty("in_place", "");
                    itmMPromotion.setProperty("in_date", "");
                    itmMPromotion.setProperty("in_area_id", "");
                }

                if (belt_map.ContainsKey(in_l1))
                {
                    Item itmBelt = belt_map[in_l1];
                    itmMPromotion.setProperty("in_l1", in_l1);
                    itmMPromotion.setProperty("in_degree", itmBelt.getProperty("value", ""));
                }
                else
                {
                    itmMPromotion.setProperty("in_l1", in_l1);
                    itmMPromotion.setProperty("in_degree", "0");
                }

                Item itmResult = itmMPromotion.apply();

                if (itmResult.isError())
                {
                    throw new Exception("建立 晉段測驗成績 發生錯誤");
                }
            }
        }

        private Dictionary<string, Item> MapItem(Item items, string key_pro)
        {
            Dictionary<string, Item> map = new Dictionary<string, Item>();

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string key_val = item.getProperty(key_pro, "");

                if (!map.ContainsKey(key_val))
                {
                    map.Add(key_val, item);
                }
            }

            return map;
        }

        private Item GetBelts(TConfig cfg)
        {
            string sql = @"
                SELECT
	                t2.value
	                , t2.label_zt AS 'label'
                FROM
	                [LIST] t1 WITH(NOLOCK)
                INNER JOIN
	                [VALUE] t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                WHERE
	                t1.name = 'In_Black_Belt_List'
                ORDER BY
	                t2.sort_order
            ";

            return cfg.inn.applySQL(sql);
        }

        private Item GetMeetingAreas(TConfig cfg)
        {
            string sql = "SELECT * FROM IN_CLA_MEETING_AREA WITH(NOLOCK)"
                + " WHERE source_id = '{#meeting_id}'";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);
            return cfg.inn.applySQL(sql);
        }

        private Item GetMeetingPromotions(TConfig cfg, bool filter_makeup = false, bool order_by_degree = false)
        {
            string sql = "SELECT * FROM IN_CLA_MEETING_PROMOTION WITH(NOLOCK)"
                + " WHERE source_id = '{#meeting_id}'";

            if (filter_makeup)
            {
                sql += " AND in_makeup_yn = 1";
            }

            if (order_by_degree)
            {
                sql += " ORDER BY in_degree, in_birth";
            }
            else
            {
                sql += " ORDER BY in_area_sort, in_degree DESC, in_birth";
            }

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);
            return cfg.inn.applySQL(sql);
        }

        private Item GetMeetingUsers(TConfig cfg)
        {
            string sql = @"
                SELECT
	                t1.id               AS 'mt_id'
	                , t1.in_annual      AS 'mt_annual'
	                , t1.in_echelon     AS 'mt_echelon'
	                , t2.*
                    , t11.in_sno        AS 'committee_sno'
                    , t11.in_short_org  AS 'committee_short_name'
                    , t12.id            AS 'resume_id'
                FROM
	                IN_CLA_MEETING t1 WITH(NOLOCK)
                INNER JOIN
	                IN_CLA_MEETING_USER t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                INNER JOIN
	                IN_RESUME t11 WITH(NOLOCK)
	                ON t11.in_name = t2.in_committee
                    AND t11.in_member_type = 'area_cmt' 
                    AND t11.in_member_role = 'sys_9999'
                INNER JOIN 
                    IN_RESUME t12 WITH(NOLOCK)
                    ON t12.in_sno = t2.in_sno
                WHERE
	                t1.id = '{#meeting_id}'
	                AND ISNULL(t2.in_ass_ver_result, '') = '1'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }

            public string meeting_id { get; set; }
            public string scene { get; set; }

            public Item itmMeeting { get; set; }

            public string mt_title { get; set; }
            public string doc_name { get; set; }
            public string ext_type { get; set; }
            public string[] CharSet { get; set; }
            public int font_size { get; set; }
            public int row_height { get; set; }
        }

        private string GetPromotionDay(TConfig cfg, string mt_annual, string mt_date)
        {
            string result = "";

            int west_year = GetIntVal(mt_annual) + 1911;

            switch (mt_date)
            {
                case "03-30":
                    result = west_year + "-03-29T16:00:00";
                    break;
                case "03-31":
                    result = west_year + "-03-30T16:00:00";
                    break;
                case "06-30":
                    result = west_year + "-06-29T16:00:00";
                    break;
                case "09-30":
                    result = west_year + "-09-29T16:00:00";
                    break;
                case "12-30":
                    result = west_year + "-12-29T16:00:00";
                    break;
                case "12-31":
                    result = west_year + "-12-30T16:00:00";
                    break;
            }

            return result;
        }

        private string DateTimeStr(string value, string format = "yyyy-MM-ddTHH:mm:ss", int hours = 8)
        {
            if (value == "") return "";

            DateTime dt = DateTime.Now;
            if (DateTime.TryParse(value, out dt))
            {
                return dt.AddHours(hours).ToString(format);
            }
            else
            {
                return value;
            }
        }

        private int GetIntVal(string value)
        {
            int result = 0;
            int.TryParse(value, out result);
            return result;
        }
    }
}