﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;
using System.Net;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class In_Site_Page_GetTemplate : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            Innovator inn = this.getInnovator();

            string id = this.getProperty("id", "");
            string in_page = this.getProperty("in_page", "");
            string in_method = this.getProperty("in_method", "");
            string in_meeting = this.getProperty("in_meeting", "");

            Item itmAppUrl = inn.applySQL("SELECT in_name, in_value FROM IN_VARIABLE t1 WITH(NOLOCK) WHERE t1.in_name = 'app_url'");
            string app_url = itmAppUrl.getProperty("in_value", "");

            string in_source_url = "";
            string in_template = "";
            
            switch (in_method.ToUpper())
            {
                case "IN_CLA_GET_MEETINGUSERLOGIN":
                    in_source_url = app_url.TrimEnd('/') + "/" + "b.aspx?page=" + in_page + "&method=" + in_method + "&meeting_id=" + in_meeting + "&scene=generate";
                    in_template = RequestContents(in_source_url);
                    break;
            }

            Item itmSitePage = inn.newItem("", "merge");
            itmSitePage.setAttribute("where", "id='"+ id + "'");
            itmSitePage.setProperty("in_source_url", in_source_url);
            itmSitePage.setProperty("in_template", in_template);

            return this;
        }

        private string RequestContents(string target_url)
        {
            try
            {
                HttpWebRequest request = HttpWebRequest.Create(target_url) as HttpWebRequest;
                request.Method = "GET";
                request.Timeout = 30000;

                string contents = "";

                // 取得回應資料
                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    using (var sr = new System.IO.StreamReader(response.GetResponseStream()))
                    {
                        contents = sr.ReadToEnd();
                    }
                }

                return contents;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }
}