﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;
using System.Net;

namespace WorkHelp.Aras.Playground.Methods.Cta
{
	/// <summary>
	/// 啟動
	/// </summary>
	public class In_Cla_Get_MeetingUserLogin : Item
	{
		/// <summary>
		/// 編程啟動點 (Code 在此撰寫)
		/// </summary>
		public Item Run()
		{
			Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
			Aras.Server.Core.IContextState RequestState = CCO.RequestState;

			//若有傳入指定會議id: meeting_id，會傳回inn_is_active，指定的會議是否仍在報名期間內。若無傳則無條件傳回false。

			//System.Diagnostics.Debugger.Break();

			Innovator inn = this.getInnovator();
			string strDatabaseName = inn.getConnection().GetDatabaseName();
			string strMethodName = "[" + strDatabaseName + "]In_Cla_Get_MeetingUserLogin";

			//CCO.Utilities.WriteDebug(strMethodName, "input dom: " + this.dom.InnerXml);

			TConfig cfg = new TConfig
			{
				CCO = CCO,
				inn = inn,
				strDatabaseName = strDatabaseName,
				strMethodName = strMethodName,
				meeting_id = this.getProperty("meeting_id", "no_data"),
				caching = System.Runtime.Caching.MemoryCache.Default,
				policy = new System.Runtime.Caching.CacheItemPolicy(),
			};
			cfg.policy.SlidingExpiration = TimeSpan.FromSeconds(60);

			Item result = inn.newItem();

			cfg.itmMeeting = GetMeeting(cfg);
			string is_function = GetIsFunction(cfg);
			string pk_label = GetPKLabel(cfg);

			result.setProperty("inn_meeting_id", cfg.meeting_id);
			result.setProperty("inn_is_active", is_function);
			result.setProperty("inn_in_url", cfg.itmMeeting.getProperty("in_url", ""));

			result.setProperty("in_url", cfg.itmMeeting.getProperty("in_url", ""));
			result.setProperty("in_register_url", cfg.itmMeeting.getProperty("in_register_url", ""));
			result.setProperty("inn_primary_field_name", pk_label);

			result.addRelationship(cfg.itmMeeting);

			return result;
		}

		private Item GetMeeting(TConfig cfg)
        {
			string key = cfg.meeting_id + "_MS";
			
			Item val = cfg.caching.Contains(key)
				? cfg.caching.Get(key) as Item
				: null;

			if (val == null)
            {
				val = cfg.inn.newItem("In_Cla_Meeting", "get");
				val.setAttribute("select", "id,keyed_name,in_title,in_url,in_primary_field_name,in_register_url");
				val.setID(cfg.meeting_id);
				val = val.apply();

				cfg.caching.Set(key, val, cfg.policy);
			}

			return val;
		}

		private string GetIsFunction(TConfig cfg)
		{
			string key = cfg.meeting_id + "_FT";

			string val = cfg.caching.Contains(key)
				? (string)cfg.caching.Get(key)
				: string.Empty;

			if (string.IsNullOrWhiteSpace(val))
            {
				string aml = @"<AML>
						<Item type=""In_Cla_Meeting_FunctionTime"" action=""get"" select=""id"">
							<source_id>{#meeting_id}</source_id>
							<in_action>mulogin</in_action>
							<in_date_s condition=""lt"">{#today}</in_date_s>
							<in_date_e condition=""gt"">{#today}</in_date_e >
						</Item>
					</AML>";

				aml = aml.Replace("{#meeting_id}", cfg.meeting_id)
							.Replace("{#today}", System.DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss"));

				Item items = cfg.inn.applyAML(aml);

				if (items.isError() || items.getItemCount() <= 0)
				{
					val = "false";
				}
				else
				{
					val = "true";
				}

				cfg.caching.Set(key, val, cfg.policy);
			}

			return val;
		}

		private string GetPKLabel(TConfig cfg)
		{
			string key = cfg.meeting_id + "_PKLBL";

			string val = cfg.caching.Contains(key)
				? (string)cfg.caching.Get(key)
				: string.Empty;

			if (string.IsNullOrWhiteSpace(val))
            {
				string in_primary_field_name = cfg.itmMeeting.getProperty("in_primary_field_name", "");

				if (in_primary_field_name != "")
				{
					//In_Meeting_PKey
					string aml = @"
						<AML>
							<Item type='Value' select='label' action='get'>
								<source_id>
									<Item type='List' action='get'>
										<name>In_Meeting_PKey</name>
									</Item>
								</source_id>
								<value>{#meeting_p_key}</value>
							</Item>
						</AML>";
					
					aml = aml.Replace("{#meeting_p_key}", in_primary_field_name);

					Item item = cfg.inn.applyAML(aml);

					val = item.getProperty("label", "");

					cfg.caching.Set(key, val, cfg.policy);
				}
			}

			return val;
		}

		/// <summary>
		/// 組態
		/// </summary>
		private class TConfig
		{
			public Aras.Server.Core.CallContext CCO { get; set; }
			public Innovator inn { get; set; }
			public string strDatabaseName { get; set; }
			public string strMethodName { get; set; } 
			public string meeting_id { get; set; }
			public Item itmMeeting { get; set; }
			public System.Runtime.Caching.MemoryCache caching { get; set; }
			public System.Runtime.Caching.CacheItemPolicy policy { get; set; }
		}
	}
}