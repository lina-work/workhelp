﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;
using System.Net;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class In_Cla_CheckSurveyAns : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                1.取得會議Id、問卷類型、學員Id
                2.以meetingid取得這筆會議的學員履歷
                3.以學員履歷的in_user來取得每筆學員id(meetinguserid)
                4.以meetingid、meetinguserid、surveytype取得符合這筆會議的每筆學員的問卷類型的問卷結果
                5.對問卷結果的每題題目與對應的標準答案做比較
            */

            /*
                這個 method 的 this 將會是 In_Cla_Meeting
                所以可以抓到這個 In_Cla_Meeting 的所有 meeting_user_resume, 然後逐條取得 meeting_user_resume的 user, 帶入 MeetingUserId
            */

            System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]In_Cla_CheckSurveyAns";

            string aml = "";
            string sql = "";
            string meetingid = this.getProperty("meeting_id", "");
            string surveytype = this.getProperty("in_surveytype", "");
            string meetinguserid = this.getProperty("muid", "");

            /*
            if(surveytype!="1" && surveytype!="4")
            {
                 Item itmMeetingUser1 = inn.newItem("In_Cla_Meeting_User");
                itmMeetingUser1.setProperty("meetinguserid", meetinguserid);
                itmMeetingUser1 = itmMeetingUser1.apply("In_Update_Resume");
                return inn.newResult("此問卷無算分數功能!");
            }
            */

            /*
            switch (surveytype)
            {
                case "2":
                case "3":
                case "5":
                    break;
                default:
                    Item itmMeetingUser1 = inn.newItem("In_Cla_Meeting_User");
                    itmMeetingUser1.setProperty("meetinguserid", meetinguserid);
                    itmMeetingUser1 = itmMeetingUser1.apply("In_Update_Resume");
                    return inn.newResult("此問卷無算分數功能!");
            }
            */
            /*
            string meetingid = "9C473A25FCFE4E028DCA45449DC2538D";
            string surveytype = "1";
            string meetinguserid = "A9BC0DDA582A411FA0476952CEE610F6";
            */

            Item itmMUser = inn.newItem("In_Cla_Meeting_User", "get");
            itmMUser.setProperty("source_id", meetingid);
            itmMUser.setProperty("id", meetinguserid);
            itmMUser = itmMUser.apply(sql);

            if (itmMUser.getItemCount() == -1 || itmMUser.getResult() == "")
            {
                return inn.newError("本課程無該學員");
            }

            string ResultAns = "";//答案 
            string SurveyAns = "";//標準答案 

            Item itmMResume = inn.newItem("In_Cla_Meeting_Resume", "get");
            //itmMResume.setProperty("source_id", meetingid);
            itmMResume.setProperty("in_user", meetinguserid);
            itmMResume = itmMResume.apply();
            if (itmMResume.getItemCount() == 0)
            {
                string MeetingUserName = itmMUser.getProperty("in_name", "no_data");
                return inn.newError("參加者" + MeetingUserName + "沒有學員履歷!");
            }

            //string surveyresultaml = @"<AML>
            //        <Item type='In_Cla_Meeting_Surveys_Result' action='get' orderBy='KEYED_NAME'>
            //            <SOURCE_ID>[@meetingid]</SOURCE_ID>
            //            <IN_SURVEYTYPE>[@surveytype]</IN_SURVEYTYPE>
            //            <IN_PARTICIPANT>[@meetinguserid]</IN_PARTICIPANT>
            //        </Item>
            //    </AML>"
            //    .Replace("[@meetingid]", meetingid)
            //    .Replace("[@meetinguserid]", meetinguserid)
            //    .Replace("[@surveytype]", surveytype);
            //Item MeetingSurveyResult = inn.applyAML(surveyresultaml);

            //int Count = MeetingSurveyResult.getItemCount();

            ////	這裡宣告一個　總分數　SurveyScore
            //double SurveyScores = 0;

            //aml = "<AML>" +
            //    "<Item type='In_Cla_Meeting_Surveys' action='get' related_expand='0'>" +
            //    "<source_id>" + meetingid + "</source_id>" +
            //    "<in_surveytype>" + surveytype + "</in_surveytype>" +
            //    "</Item></AML>";

            //Item In_Meeting_Surveys = inn.applyAML(aml);

            //for (int i = 0; i < Count; i++)
            //{
            //    Item SurveyResult = MeetingSurveyResult.getItemByIndex(i);
            //    Item Survey = SurveyResult.getRelatedItem();
            //    double SurveyScore = 0;//每題題目的試題分數
            //    Item MeetingSurveyItem = In_Meeting_Surveys.getItemsByXPath("//Item[related_id='" + Survey.getID() + "']");

            //    if (MeetingSurveyItem.getItemCount() == 1)
            //    {
            //        SurveyScore = GetDbl(MeetingSurveyItem.getProperty("in_score", "-1"));
            //    }

            //    if (SurveyScore == -1)
            //    {
            //        //如果是-1,代表沒設定配分,就拿 題目原本的分數來用,然後順便更新
            //        SurveyScore = GetDbl(Survey.getProperty("in_score", "0"));
            //        sql = "Update In_Cla_Meeting_Surveys SET in_score = " + SurveyScore
            //            + " WHERE source_id = '" + meetingid + "' AND related_id = '" + Survey.getID() + "' AND in_surveytype = '" + surveytype + "'";
            //        CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);
            //        inn.applySQL(sql);
            //    }

            //    SurveyAns = Survey.getProperty("in_answer", "");
            //    ResultAns = SurveyResult.getProperty("in_answer", "");

            //    SurveyAns = SurveyAns.Replace(" ", "");
            //    ResultAns = ResultAns.Replace(" ", "");

            //    /*
            //        這裡要判斷SurveyAns與ResultAns是否相等，若字串相同，則更新 In_Cla_Meeting_Surveys_Result 的正確與否欄位為＂１＂否則為０
            //        這裡同時可以計算各個 In_Cla_Meeting_Surveys_Result 的分數，然後累加到 SurveyScore
            //    */

            //    if (string.Compare(SurveyAns, ResultAns, false) == 0)
            //    {
            //        SurveyResult.setProperty("in_sysresult", "1");
            //        SurveyScores += SurveyScore;
            //    }
            //    else
            //    {
            //        SurveyResult.setProperty("in_sysresult", "0");
            //    }

            //    switch (SurveyAns)//如果標準答案為空，則代表不用計算分數
            //    {
            //        case " ":
            //        case "":
            //        case "#evaluator":
            //            SurveyResult.setProperty("in_sysresult", "");
            //            break;
            //    }

            //    SurveyResult = SurveyResult.apply("edit");
            //}

            sql = @"
                SELECT
	                t1.id			            AS 'mtsr_id'
	                , t1.in_answer	            AS 'mtsr_answer'
	                , t2.id			            AS 'mtsv_id'
	                , ISNULL(t2.in_score, -1)	AS 'mtsv_score'
	                , t3.in_answer	            AS 'survey_answer'
	                , t3.in_score	            AS 'survey_score'
                FROM
	                IN_CLA_MEETING_SURVEYS_RESULT t1 WITH(NOLOCK)
                INNER JOIN 
	                IN_CLA_MEETING_SURVEYS t2 WITH(NOLOCK)
	                ON t2.source_id = t1.source_id
	                AND t2.related_id = t1.related_id
                INNER JOIN 
	                IN_CLA_SURVEY t3 WITH(NOLOCK)
	                ON t3.id = t1.related_id
                WHERE
	                t1.source_id = '{#meeting_id}'
	                AND t1.in_surveytype = '{#surveytype}'
	                AND t1.in_participant = '{#muid}'
                ORDER BY 
	                t1.keyed_name
            ";

            sql = sql.Replace("{#meeting_id}", meetingid)
                .Replace("{#surveytype}", surveytype)
                .Replace("{#muid}", meetinguserid);
            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);
            Item itmMSResults = inn.applySQL(sql);
            int count = itmMSResults.getItemCount();
            double SurveyScores = 0;

            for (int i = 0; i < count; i ++)
            {
                Item itmMSResult = itmMSResults.getItemByIndex(i);
                string mtsr_id = itmMSResult.getProperty("mtsr_id", "");
                string mtsr_answer = itmMSResult.getProperty("mtsr_answer", "").Replace(" ", "");
                
                string mtsv_id = itmMSResult.getProperty("mtsv_id", "");
                double mtsv_score = GetDbl(itmMSResult.getProperty("mtsv_score", "-1"));

                string survey_answer = itmMSResult.getProperty("survey_answer", "").Replace(" ", "");
                double survey_score = GetDbl(itmMSResult.getProperty("survey_score", "-1"));

                double SurveyScore = mtsv_score;

                if (mtsv_score == -1)
                {
                    //如果是-1,代表沒設定配分,就拿 題目原本的分數來用,然後順便更新
                    sql = "Update In_Cla_Meeting_Surveys SET in_score = " + survey_score + " WHERE id = '" + mtsv_id + "'";
                    CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);
                    inn.applySQL(sql);

                    SurveyScore = survey_score;
                }

                string in_sysresult = "";
                if (string.Compare(survey_answer, mtsr_answer, false) == 0)
                {
                    in_sysresult = "1";
                    SurveyScores += SurveyScore;
                }
                else
                {
                    in_sysresult = "0";
                }

                switch (SurveyAns)
                {
                    //如果標準答案為空，則代表不用計算分數
                    case " ":
                    case "":
                    case "#evaluator":
                        in_sysresult = "";
                        break;
                }

                string mtsr_sql = "UPDATE In_Cla_Meeting_Surveys_Result SET in_sysresult = '" + in_sysresult + "'"
                    + " WHERE id = '" + mtsr_id + "'";
                //CCO.Utilities.WriteDebug(strMethodName, "sql: " + mtsr_sql);
                inn.applySQL(mtsr_sql);
            }

            /*
                單一人員的所有問題都跑完之後,也獲得了 SurveyScore 總分數　SurveyScore
                將 SurveyScore 總分更新到 meeting_user_resume 的 分數欄位上
            */

            if (surveytype == "3")
            {
                itmMResume.setProperty("in_technical", SurveyScores.ToString());//將分數回寫到學員履歷的術科分數
            }

            itmMResume.setProperty("in_score_o_" + surveytype, SurveyScores.ToString());//將分數回寫到學員履歷的原始分數


            /*
            switch(surveytype)
            {
                case "3":
                    MeetingResumes.setProperty("in_technical", SurveyScores.ToString());//將分數回寫到學員履歷的術科分數
                    break;
                case "5":
                    MeetingResumes.setProperty("in_score_o", SurveyScores.ToString());//將分數回寫到學員履歷的原始分數
                    break;
            }
            */

            itmMResume = itmMResume.apply("edit");

            //這樣METHOD執行完之後,就會算出這個 meeting 的所有上課學員的surveytype=5的考卷分數更新到個人的 meeting_user_resume 分數欄位內
            itmMUser.setProperty("meetinguserid", meetinguserid);
            itmMUser = itmMUser.apply("In_Cla_Update_Resume");

            return inn.newResult(SurveyScores.ToString());
        }

        private double GetDbl(string value, double defV = 0)
        {
            double result = 0;
            if (double.TryParse(value, out result))
            {
                return result;
            }
            else
            {
                return defV;
            }
        }
    }
}