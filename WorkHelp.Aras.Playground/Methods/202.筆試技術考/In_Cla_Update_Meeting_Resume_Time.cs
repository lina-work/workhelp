﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;
using System.Net;

namespace WorkHelp.Aras.Playground.Methods.Cta
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class In_Cla_Update_Meeting_Resume_Time : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
            目的: 更新學員學習履歷時間
            說明: 
                1. 一般回答模式: 更新開始時間 (該問卷必須無結束時間)
                2. 送出問卷答案: 更新結束時間，並計算秒數
            輸入: 
               - meeting id
               - muid
               - surveytype
               - time_mode
            日誌:
               - 2021.09.14 改寫 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]In_Cla_Update_Meeting_Resume_Time";

            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            //CCO.Utilities.WriteDebug(strMethodName, "input dom: " + this.dom.InnerXml);

            string meetingid = this.getProperty("meeting_id", "");
            string muid = this.getProperty("muid", "");
            string surveytype = this.getProperty("surveytype", "");
            string time_mode = this.getProperty("time_mode", "").ToLower();

            string field_name_s = "in_time_s_" + surveytype; //開始
            string field_name_e = "in_time_e_" + surveytype; //結束
            string field_name_t = "in_time_t_" + surveytype; //總秒數

            DateTime now = System.DateTime.Now.AddHours(-8);
            string now_value = now.ToString("yyyy-MM-ddTHH:mm:ss");

            Item itmMResume = inn.newItem("In_Cla_Meeting_Resume", "get");
            itmMResume.setProperty("in_user", muid);
            itmMResume = itmMResume.apply();

            if (itmMResume.isError())
            {
                throw new Exception("學員履歷資料發生錯誤");
            }

            string mrid = itmMResume.getProperty("id", "");
            string start_time_value = itmMResume.getProperty(field_name_s, "");
            string end_time_value = itmMResume.getProperty(field_name_e, "");

            //CCO.Utilities.WriteDebug(strMethodName, "field_name_s = " + field_name_s + ", start_time_value = " + start_time_value);
            //CCO.Utilities.WriteDebug(strMethodName, "field_name_e = " + field_name_e + ", end_time_value = " + end_time_value);

            if (end_time_value != "")
            {
                return this;
                //throw new Exception("已有結束時間，不更新開始與結束時間");
            }

            if (time_mode == "e" && start_time_value == "")
            {
                throw new Exception("查無開始時間");
            }

            string sql = "";
            if (time_mode == "s")
            {
                sql = "UPDATE In_Cla_Meeting_Resume SET " + field_name_s + " = '" + now_value + "'"
                    + " WHERE id = '" + mrid + "'";
            }
            else if (time_mode == "e")
            {
                string second_value = "";
                DateTime start_time = Convert.ToDateTime(start_time_value).AddHours(-8);
                if (now > start_time)
                {
                    TimeSpan ts = now - start_time;
                    second_value = Convert.ToInt32(ts.TotalSeconds).ToString();
                }

                sql = "UPDATE In_Cla_Meeting_Resume SET " + field_name_e + " = '" + now_value + "', " + field_name_t + " = '" + second_value + "'"
                    + " WHERE id = '" + mrid + "'";
            }

            if (sql == "")
            {
                throw new Exception("SQL 指令異常");
            }

            //CCO.Utilities.WriteDebug(strMethodName, "sql =  " + sql);

            Item itmR = inn.applySQL(sql);

            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

            return itmR;
        }
    }
}