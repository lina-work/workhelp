﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;
using System.Net;

namespace WorkHelp.Aras.Playground.Methods.Cta
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class In_Cla_Receive_MeetingSurveyAML : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                這個方法主要用來接收回傳的問卷結果。由InnoJSON.ashx呼叫。使用標準InnoJSON格式。
                會先檢查該使用者是否有填寫過問卷，若填寫過，傳回訊號。
                若未填寫過，add問卷並且以applySQL方式設定 In_Cla_Meeting 本體的統計數據需要更新。

                預期的傳入資料結構：

                <AML>
                    <Item type="Method" action="In_Cla_Add_MeetingSurveys">
                        <meeting_id></meeting_id>
                        <muid><muid>
                        <in_survey_type><in_survey_type>
                        <results>
                            <Item type="In_Cla_Meeting_Surveys_Result" action="add">
                                <source_id></source_id>
                                <in_answer></in_answer>
                                <related_id></related_id>
                            </Item>
                        </results>
                    </Item>
                </AML>
            */

            /*
            2019-12-10 如果該題型允許重複答到對,則立即計算分數,若不及格,則刪掉答題記錄,讓前端再答一次
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]In_Cla_Receive_MeetingSurveyAML";

            string strMeetingId = this.getProperty("meeting_id", "");
            string strMuid = this.getProperty("muid", "");
            string strSurveytype = this.getProperty("surveytype", "");

            //檢查必要參數是否均有傳入
            if (string.IsNullOrWhiteSpace(strMeetingId) 
                || string.IsNullOrWhiteSpace(strMuid) 
                || string.IsNullOrWhiteSpace(strSurveytype)) 
            { 
                return inn.newError("missing key parameter meeting_id,muid or surveytype"); 
            }

            string sql = "";

            Item itmRst = inn.newItem(); //乘載回傳用資訊的物件

            string strCheckSurveyExistSQL = @"SELECT TOP 1 id FROM In_Cla_Meeting_Surveys_Result WITH(NOLOCK)
									WHERE source_id = '{#meeting_id}' AND in_surveytype = '{#surveytype}' AND in_participant = '{#muid}'"
                                            .Replace("{#muid}", strMuid)
                                            .Replace("{#meeting_id}", strMeetingId)
                                            .Replace("{#surveytype}", strSurveytype);

            //標準回傳物件結構
            string strResponseToLoadAML = @"
								<Item>
									<response>{#response}</response>
									<message>{#message}</message>
								</Item>";

            Item itmCheckResult = inn.applySQL(strCheckSurveyExistSQL);

            if (itmCheckResult.getItemCount() != -1)
            {
                string checkResultFailedAML = strResponseToLoadAML
                                            .Replace("{#response}", "error")
                                            .Replace("{#message}", "您已填過此問卷。");
                itmRst.loadAML(checkResultFailedAML);
                return itmRst;
            }

            if (this.node.SelectSingleNode("results/Item").InnerXml.Trim() == "")
            {
                string resultEmptyAML = strResponseToLoadAML
                                            .Replace("{#response}", "error")
                                            .Replace("{#message}", "未傳入任何答案。");
                itmRst.loadAML(resultEmptyAML);
                return itmRst;
            }

            string strAddSurveyAML = this.node.SelectSingleNode("results").InnerXml;

            Item itmAddSurveys = inn.applyAML("<AML>" + strAddSurveyAML + "</AML>");


            //2019-12-10 如果該題型允許重複答到對,則立即計算分數,若不及格,則刪掉答題記錄,讓前端再答一次
            string ReturnType = "";
            if (itmAddSurveys.isError())
            {
                ReturnType = "error";
            }
            else
            {
                sql = "SELECT in_is_must_pass, i n_pass_score FROM In_Cla_Meeting_FunctionTime WITH(NOLOCK) WHERE source_id = '" + strMeetingId + "' AND in_action = 'sheet" + strSurveytype + "'";
                Item In_Meeting_Functiontime = inn.applySQL(sql);

                if (In_Meeting_Functiontime.getProperty("in_is_must_pass", "0") == "1")
                {
                    Item Inn_Result = inn.newItem("In_Cla_Meeting");
                    Inn_Result.setProperty("meeting_id", strMeetingId);
                    Inn_Result.setProperty("muid", strMuid);
                    Inn_Result.setProperty("in_surveytype", strSurveytype);
                    Inn_Result.setProperty("meeting_type", "In_Cla_Meeting");
                    Inn_Result = Inn_Result.apply("In_CheckSurveyAns");

                    string strSurveyScore = Inn_Result.getResult();
                    string srtPassScore = In_Meeting_Functiontime.getProperty("in_pass_score", "0");
                    double dblSurveyScore = GetDbl(strSurveyScore);
                    double dblPassScore = GetDbl(srtPassScore);

                    if (dblSurveyScore < dblPassScore)
                    {
                        ReturnType = "no_pass";
                    }
                    else
                    {
                        ReturnType = "ok";
                    }
                }
                else
                {
                    ReturnType = "ok";
                }
            }

            switch (ReturnType)
            {
                case "error":
                    throw new Exception("err:寫入問卷資料發生錯誤，請洽系統管理人員");
                    itmRst.loadAML(strResponseToLoadAML
                                .Replace("{#response}", "error")
                                .Replace("{#message}", "寫入問卷資料發生錯誤，請洽系統管理人員")
                                );
                    itmRst.setProperty("data", "errorString : " + itmAddSurveys.getErrorString() + "errorDetail : " + itmAddSurveys.getErrorDetail());
                    break;
                case "no_pass":
                    throw new Exception("no_pass:您本次測驗未達合格標準,請重新作答");
                    itmRst.loadAML(strResponseToLoadAML
                                .Replace("{#response}", "no_pass")
                                .Replace("{#message}", "您本次測驗未達合格標準,請重新作答")
                                );
                    itmRst.setProperty("data", "errorString : " + itmAddSurveys.getErrorString() + "errorDetail : " + itmAddSurveys.getErrorDetail());
                    sql = "DELETE  FROM In_Cla_Meeting_Surveys_Result WHERE source_id = '" + strMeetingId + "' AND in_surveytype = '" + strSurveytype + "' AND in_participant = '" + strMuid + "'";
                    inn.applySQL(sql);
                    break;

                default:
                    itmRst.loadAML(strResponseToLoadAML
                                .Replace("{#response}", "success")
                                .Replace("{#message}", "問卷上傳成功")
                                );
                    
                    string strSetNeedUpdateSQL = "UPDATE In_Cla_Meeting SET in_statistics_uptodate = '0' WHERE id = '{#meeting_id}'".Replace("{#meeting_id}", strMeetingId);
                    inn.applySQL(strSetNeedUpdateSQL);
                    
                    break;
            }

            return itmRst;
        }

        private double GetDbl(string value)
        {
            double result = 0;
            double.TryParse(value, out result);
            return result;
        }
    }
}