﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;
using System.Net;

namespace WorkHelp.Aras.Playground.Methods.Cta
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class In_Cla_Get_MeetingSurveyAndAnswer : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            //本方法由b.aspx使用。
            //傳入參數： 
            //  meeting_id=會議的id
            //  surveytype : 要求的問卷的類型(ex:事前問卷、事後問卷...)
            //  status : 問卷狀態(1:期間內且未填寫、2:已超時未填寫，基本上不會出現、3:已填寫)，這個參數主要是寫在網頁上回到前端時讓javascript辨別狀態用，會被寫成：inn_sur_sta/  tus。
            //  muid : 關聯的會議使用者id
            //本方法只負責取出題目及答案，確認是否在期間內由 In_Cla_Check_MeetingSurveyActive 進行。取得此連結時應當已進行過確認。

            //string meeting_id=this.getProperty("meeting_id","6908863025B74A008F8A8C1A88E2FBBF");
            //string surveytype=this.getProperty("surveytype","2");
            //string status=this.getProperty("status","1");//主要是要寫回去給javascript判別狀態用，查詢沒有使用到這個值。
            string muid = this.getProperty("muid", "");
            string meeting_id = this.getProperty("meeting_id");
            string surveytype = this.getProperty("surveytype");
            string muEmail = this.getProperty("email", "");
            string status = this.getProperty("status");


            Innovator inn = this.getInnovator();

            if ((meeting_id == "" || muEmail == "") && muid == "")
            {
                return inn.newError("Missing required parameter \"muid\" or \"muEmail\" and \"meeting_id\" ");
            }

            Item result = inn.newItem();
            Item meetingUser;

            if (muid != "")
            {
                meetingUser = inn.getItemById("In_Cla_Meeting_User", muid);
            }
            else
            {
                meetingUser = inn.newItem("In_Cla_Meeting_User", "get");
                meetingUser.setProperty("source_id", meeting_id);
                meetingUser.setProperty("in_mail", muEmail);
                meetingUser = meetingUser.apply();
                muid = meetingUser.getID();

            }



            result.setAttribute("type", "In_Cla_Meeting");
            result.setAttribute("action", "get");
            result.setID(meeting_id);
            result = result.apply();

            Item surveys = inn.newItem();
            surveys.setAttribute("type", "In_Cla_Meeting_Surveys_Result");
            surveys.setAttribute("action", "get");
            surveys.setProperty("source_id", meeting_id);
            surveys.setProperty("in_surveytype", surveytype);
            surveys.setAttribute("orderBy", "[In_Cla_Meeting_Surveys_Result].sort_order");
            surveys.setProperty("in_participant", muid);
            surveys = surveys.apply();

            int sCount = surveys.getItemCount();

            //取得每個題目的題號
            string strGetInSortOrderAML = @"<AML>
								<Item type='In_Cla_Meeting_Surveys' action='get'>
									<source_id>{#meeting_id}</source_id>
									<in_surveytype>{#surveytype}</in_surveytype>
									<related_id>{#surveyid}</related_id>
								</Item>
								</AML>";

            List<string> deduplicate = new List<string>();

            for (int k = 0; k < sCount; k++)
            {
                Item tmp = surveys.getItemByIndex(k);
                string in_sysresult = tmp.getProperty("in_sysresult");

                Item answer = inn.newItem();
                answer.setAttribute("type", "Inn_Cla_Meeting_Surveys_Result");
                answer.setProperty("in_answer", tmp.getProperty("in_answer"));
                answer.setProperty("inn_question_id", tmp.getRelatedItemID());
                answer.setProperty("in_sysresult", in_sysresult);

                //答錯
                if (in_sysresult == "0" && in_sysresult != " ")
                {
                    Item insurvey = tmp.getRelatedItem();
                    string model_answer = insurvey.getProperty("in_answer", "no_data"); //標準答案
                    answer.setProperty("model_answer", model_answer);
                }

                //如果是影片型的題目,則一律是正確答案
                if (tmp.getRelatedItem().getProperty("in_question_type", "") == "video")
                {
                    answer.setProperty("in_sysresult", "1");
                }

                result.addRelationship(answer);
                if (deduplicate.Contains(tmp.getRelatedItemID())) { continue; }
                else
                {
                    Item itmSortOrder = inn.newItem("In_Cla_Meeting_Surveys", "get");

                    itmSortOrder.setProperty("source_id", meeting_id);
                    itmSortOrder.setProperty("in_surveytype", surveytype);
                    itmSortOrder.setProperty("related_id", tmp.getRelatedItemID());
                    itmSortOrder.setAttribute("maxRecords", "1");
                    itmSortOrder = itmSortOrder.apply();
                    Item ja = tmp.getRelatedItem();
                    if (!itmSortOrder.isError() && !itmSortOrder.isEmpty() && itmSortOrder.getItemCount() == 1)
                    {
                        string in_so = itmSortOrder.getProperty("in_sort_order");
                        ja.setProperty("in_questions", itmSortOrder.getProperty("in_sort_order") + "." + ja.getProperty("in_questions"));
                    }
                    result.addRelationship(ja);
                    deduplicate.Add(tmp.getRelatedItemID());

                }
            }

            if (sCount == 0)
            {
                Item itmFakeAnswer = inn.newItem("In_Cla_Survey");
                itmFakeAnswer.setProperty("in_question_type", "single_text");
                result.addRelationship(itmFakeAnswer);
                result.setProperty("inn_no_surveyresult_found", "false");
            }


            string strMuNoteState = meetingUser.getProperty("in_note_state");

            switch (strMuNoteState)
            {
                case "official": strMuNoteState = "正取"; break;
                case "waiting": strMuNoteState = "備取"; break;
                case "canvel": strMuNoteState = "取消"; break;
                default: strMuNoteState = ""; break;
            }

            result.setProperty("inn_mu_name", meetingUser.getProperty("in_name"));
            result.setProperty("inn_mu_notestate", strMuNoteState);

            string sql = @"SELECT in_name_num FROM IN_CLA_MEETING_TECHNICAL WITH(NOLOCK) 
                WHERE source_id = '{#meeting_id}' in_user = '{#muid}'";

            sql = sql.Replace("{#meeting_id}", meeting_id)
                    .Replace("{#muid}", muid);

            Item itmSql = inn.applySQL(sql);
            if (!itmSql.isError() && itmSql.getItemCount() > 0)
            {
                result.setProperty("inn_mu_number", itmSql.getProperty("in_name_num"));
            }
            else
            {
                result.setProperty("inn_mu_number", meetingUser.getProperty("in_number"));
            }

            result.setProperty("inn_label", this.getProperty("label"));
            result.setProperty("in_surveytype", surveytype);
            result.setProperty("muid", muid);

            string aml = "";
            aml = "<AML>";
            aml += "<Item type='Value' action='get'>";
            aml += "<source_id><Item type='List' action='get'><name>in_Surveytype</name></Item></source_id>";
            aml += "<value>" + surveytype + "</value>";
            aml += "</Item></AML>";
            Item SurveyTypeValue = inn.applyAML(aml);
            string SurveyTypeLabel = "會議問卷";
            if (SurveyTypeValue.getItemCount() == 1)
                SurveyTypeLabel = SurveyTypeValue.getProperty("label");

            result.setProperty("inn_muid", muid);
            result.setProperty("inn_survey_label", SurveyTypeLabel);


            //CCO.Utilities.WriteDebug("In_Cla_Get_MeetingSurveyAndAnswer", result.dom.InnerXml);

            return result;

        }
    }
}