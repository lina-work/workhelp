﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;
using System.Net;

namespace WorkHelp.Aras.Playground.Methods.Cta
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class In_Cla_Get_MeetingSurveyTechnique : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
			//方法名稱：In_Cla_Get_MeetingSurveyTechnique
			/*
			由c.aspx呼叫，用來取得技術考的 In_Meeting_Survey_Techniqeu.html 及技術考所需要的資料。
			使用參數：
				data_mode:要取得考試入口(主考官掃學員身分證畫面)，或是考試頁面，必要參數，無預設值
					可用選項：
						entrance:取得考試入口	
						exam:取得考試問卷頁面，此狀態下需要額外資料
				surveytype:試卷的in_surveytype，必要參數，未傳時使用預設值
				id_no:考生的身分證號碼，用來尋找考生本人，data_mode=exam時必要

			任務流程：
				檢查必要參數
				檢查考試是否開放
				依照data_mode取得資料並放入itmRst，若data_mode錯誤傳送錯誤訊息
				回傳
			*/

			Innovator inn = this.getInnovator();
			string strDataMode = this.getProperty("data_mode", "no_data");
			string strSurveytype = this.getProperty("surveytype", "no_data");
			string strFunctoinTimeType = this.getProperty("type", "");
			string strMeetingId = this.getProperty("meeting_id", "no_data");
			string strEvaluatorName = this.getProperty("evaluator", "no_data");

			//檢查必要參數


			string strMeetingUserIdno = this.getProperty("muidno", "no_data");//考生id
			if (strDataMode == "no_data") { return inn.newError("missing required parameter \"data_mode\""); }
			//檢查entrance模式下的必要參數
			if (strDataMode == "entrance")
			{
				if (strSurveytype == "no_data") { return inn.newError("missing required parameter \"data_mode\"  or \"surveytype\" , data_mode=entrance"); }
			}
			//檢查exam模式下的必要參數
			if (strDataMode == "exam")
			{
				if (strMeetingId == "no_data" || strMeetingUserIdno == "no_data" || strEvaluatorName == "no_data") { return inn.newError("missing required parameter \"meeting_id\",\"muidno\" or \"evaluator\" , data_mode=entrance"); }
			}

			Item itmRst;//用來乘載回傳資料的物件

			//依照data_mode取得資料並放入itmRst，除出現錯誤或特殊情況外，一律在最後一航return itmRst。
			switch (strDataMode)
			{

				case "entrance":
					itmRst = inn.newItem();
					//用來取得自己作為共同講師的會議id的AML
					string strGetMeetingTeacherAML = @"
				<AML>
					<Item type='In_Cla_Meeting_Resumelist' action='get' select='source_id'>
						<related_id>
							<Item type='In_Resume' action='get'>
								<in_user_id>{#userid}</in_user_id>
							</Item>
						</related_id>
					</Item>
				</AML>"
							.Replace("{#userid}", inn.getUserID());
					StringBuilder stbCommonTeacheredMeeting = new StringBuilder();
					string strCommonTeacheredMeeting = "";
					//處理共同講師
					Item itmCommonTeacherMeeting = inn.applyAML(strGetMeetingTeacherAML);

					if (!itmCommonTeacherMeeting.isError() && !itmCommonTeacherMeeting.isError())
					{
						int intCTM = itmCommonTeacherMeeting.getItemCount();
						for (int ict = 0; ict < intCTM; ict++)
						{
							Item itmTmpCTM = itmCommonTeacherMeeting.getItemByIndex(ict);
							stbCommonTeacheredMeeting.Append("'" + itmTmpCTM.getProperty("source_id") + "',");
						}
						strCommonTeacheredMeeting = stbCommonTeacheredMeeting.ToString().TrimEnd(new Char[] { ',' });
					}
					if (strCommonTeacheredMeeting != "")
					{
						strCommonTeacheredMeeting = "<id condition='in'>" + strCommonTeacheredMeeting + "</id>";
					}
					string strGetEvaluateMeetingAML = @"
										<AML>
											<Item type='In_Cla_Meeting' action='get' select='id,in_title,item_number,keyed_name,in_current_activity'>
											<or>
												{#common_teachered_meetings}
												<owned_by_id condition='in'>{#identitylist}</owned_by_id>
												<managed_by_id condition='in'>{#identitylist}</managed_by_id>				
											</or>
												<in_current_activity condition='ne'></in_current_activity>
												<in_current_activity condition='is not null'></in_current_activity>
												<state>start</state>
												<Relationships>
													<Item type='In_Cla_Meeting_FunctionTime' action='get' select='top 1 id'>
														<in_date_s condition='lt'>{#date_now}</in_date_s>
														<in_date_e condition='gt'>{#date_now}</in_date_e>
														<in_action>sheet3</in_action>
													</Item>
												</Relationships>
											</Item>
										</AML>
										"
													.Replace("{#common_teachered_meetings}", strCommonTeacheredMeeting)
													.Replace("{#identitylist}", Aras.Server.Security.Permissions.Current.IdentitiesList)
													.Replace("{#date_now}", System.DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss"));


					Item itmEvalMeetings = inn.applyAML(strGetEvaluateMeetingAML);
					if (!itmEvalMeetings.isError() && !itmEvalMeetings.isEmpty())
					{

						int intEMeetingCount = itmEvalMeetings.getItemCount();
						for (int p = 0; p < intEMeetingCount; p++)
						{
							Item itmTmpEM = itmEvalMeetings.getItemByIndex(p);
							if (itmTmpEM.getRelationships("In_Cla_Meeting_FunctionTime").getItemCount() <= 0) { continue; }
							itmTmpEM.setType("Inn_Evaluate_Meeting");
							itmRst.addRelationship(itmTmpEM);
						}
					}

					itmRst.setProperty("inn_technique_surveytype", strSurveytype);
					return itmRst;
				case "exam":
					itmRst = inn.newItem();
					string strMeetingUserPrimaryKey = "in_mail";//存放身份證字號的property名稱，暫時為in_mail
					string strGetMeetingUserWithIDnoAML = @"<AML>
												<Item type='In_Cla_Meeting_User' action='get' select='id,keyed_name,in_number,in_name'>
													<{#mainkey}>{#idno}</{#mainkey}>
													<source_id>{#meeting_id}</source_id>
												</Item>
											</AML>"
														.Replace("{#mainkey}", strMeetingUserPrimaryKey)
														.Replace("{#idno}", strMeetingUserIdno)
														.Replace("{#meeting_id}", strMeetingId);
					Item itmMeetingUser = inn.applyAML(strGetMeetingUserWithIDnoAML);

					if (itmMeetingUser.isError() || itmMeetingUser.isEmpty())
					{
						itmRst.setProperty("inn_mu_exist", "false");
						//因為舊型系統如果會在這邊出現Null錯誤，所以需要塞一個假資料給他。
						Item itmFakeSurvey = inn.newItem("In_Cla_Survey");
						itmFakeSurvey.setProperty("in_question_type", "single_text");
						itmRst.addRelationship(itmFakeSurvey);
						return itmRst;
					}
					else
					{
						itmRst.setProperty("inn_mu_exist", "true");
					}
					string strCheckSurveyAvailableAML = @"
													<Item type='Method' action='In_Cla_Check_MeetingSurveyActive'>
														<meeting_id>{#meeting_id}</meeting_id>
														<muid>{#muid}</muid>
														<surveytype>{#surveytype}</surveytype>
													</Item>
												"
														.Replace("{#meeting_id}", strMeetingId)
														.Replace("{#muid}", itmMeetingUser.getID())
														.Replace("{#surveytype}", strSurveytype);
					Item itmSurveyAnswered = inn.newItem();
					itmSurveyAnswered.loadAML(strCheckSurveyAvailableAML);
					if (itmSurveyAnswered.apply().getResult() != "1")
					{
						//因為舊型系統如果會在這邊出現Null錯誤，所以需要塞一個假資料給他。
						Item itmFakeSurvey = inn.newItem("In_Cla_Survey");
						itmFakeSurvey.setProperty("in_question_type", "single_text");
						itmRst.addRelationship(itmFakeSurvey);

						itmRst.setProperty("inn_survey_answered", "false");
						return itmRst;
					}

					string strGetSurveySheetAML = @"<AML>
										<Item type='In_Cla_Meeting_Surveys' action='get' orderBy='sort_order' related_expand='1' select='related_id,sort_order,id,in_sort_order'>
											<source_id>{#meeting_id}</source_id>
											<in_surveytype>{#surveytype}</in_surveytype>
										</Item>
									</AML>
									"
												.Replace("{#meeting_id}", strMeetingId)
												.Replace("{#surveytype}", strSurveytype);

					if (itmMeetingUser.isError() || itmMeetingUser.isEmpty()) { break; }
					Item itmMeetingSurveys = inn.applyAML(strGetSurveySheetAML);
					if (itmMeetingSurveys.isError() || itmMeetingSurveys.isEmpty()) { break; }
					int intMeetingSurveyCount = itmMeetingSurveys.getItemCount();
					itmRst.setProperty("inn_meeting_user_id", itmMeetingUser.getID());
					for (int u = 0; u < intMeetingSurveyCount; u++)
					{
						Item itmMeetingSurvey = itmMeetingSurveys.getItemByIndex(u);
						Item itmTmpSurvey = itmMeetingSurvey.getRelatedItem();
						Item itmTmpTechSurvey = inn.newItem();
						string strAns = itmTmpSurvey.getProperty("in_answer", "");
						//處理考官部分的客製
						if (strAns == "#evaluator")
						{
							//strEvaluatorName
							//itmTmpSurvey.setProperty("in_answer",itmEvaluator.getProperty("keyed_name"));
							itmTmpSurvey.setProperty("in_answer", strEvaluatorName);
							itmTmpSurvey.setProperty("inn_answerable", "false");
						}
						itmTmpSurvey.setProperty("in_questions", itmMeetingSurvey.getProperty("in_sort_order", "") + "." + itmTmpSurvey.getProperty("in_questions"));
						itmTmpTechSurvey.loadAML(itmTmpSurvey.node.OuterXml);
						itmTmpTechSurvey.setType("Inn_Cla_Technique_Survey");
						itmRst.addRelationship(itmTmpSurvey);
						itmRst.addRelationship(itmTmpTechSurvey);
					}


					string strGetSheetLabelAML = @"
        <AML>
            <Item type='Value' action='get'>
                <source_id><Item type='List' action='get'><name>in_Surveytype</name></Item></source_id>
                <value>{#surveytype}</value>
            </Item>
        </AML>
        ".Replace("{#surveytype}", strSurveytype);
					Item itmSurveyTypeLabel = inn.applyAML(strGetSheetLabelAML);
					string SurveyTypeLabel = "會議問卷";
					if (itmSurveyTypeLabel.getItemCount() == 1) { SurveyTypeLabel = itmSurveyTypeLabel.getProperty("label"); }
					itmRst.setProperty("inn_muid", itmMeetingUser.getID());
					itmRst.setProperty("inn_mu_name", itmMeetingUser.getProperty("in_name", ""));
					itmRst.setProperty("inn_mu_number", itmMeetingUser.getProperty("in_number", ""));
					itmRst.setProperty("inn_survey_label", SurveyTypeLabel);
					break;
				default: return inn.newError("unknown \"data_mode\":" + strDataMode);
			}


			return itmRst;


		}
	}
}