﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class In_SiteMap : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
            目的: 原創網站地圖
            日期: 
                - 2021-02-26 改版 (lina)
                - 2020-08-21 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]" + "in_sitemap";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                ssn_loginid = itmR.getProperty("ssn_loginid", ""),
                scene = itmR.getProperty("scene", ""),
                mode = itmR.getProperty("mode", ""),
                tag = itmR.getProperty("tag", ""),
            };

            if (cfg.ssn_loginid == "lwu001")
            {
                cfg.IsAdmin = true;
                itmR.setProperty("ssn_loginid", cfg.ssn_loginid);
            }

            if (cfg.scene == "")
            {
                Query(cfg, itmR);
            }
            else if (cfg.scene == "type")
            {
                QueryType(cfg, itmR);
            }
            else if (cfg.scene == "save" && cfg.IsAdmin)
            {
                Save(cfg, itmR);
            }
            else if (cfg.scene == "update_sort" && cfg.IsAdmin)
            {
                UpdateSort(cfg, itmR);
            }

            return itmR;
        }

        //更新排序
        private void UpdateSort(TConfig cfg, Item itmReturn)
        {
            string map_id = itmReturn.getProperty("map_id", "");
            string sort_order = itmReturn.getProperty("sort_order", "");
            string chk_sort_order = GetIntValue(sort_order).ToString();
            if (chk_sort_order != sort_order) throw new Exception("排序必須為整數");

            Item itmUpdate = cfg.inn.newItem("IN_SITEURL_MAP", "merge");
            itmUpdate.setAttribute("where", "id='" + map_id + "'");
            itmUpdate.setProperty("id", map_id);
            itmUpdate.setProperty("sort_order", sort_order);
            itmUpdate = itmUpdate.apply();

            if (itmUpdate.isError() || itmUpdate.getResult() == "")
            {
                throw new Exception("更新排序發生錯誤");
            }
        }

        //儲存
        private void Save(TConfig cfg, Item itmReturn)
        {
            string url_id = itmReturn.getProperty("id", "");

            Item itmOld = cfg.inn.newItem("In_SiteUrl", "get");
            itmOld.setProperty("id", url_id);
            itmOld = itmOld.apply();

            if (itmOld.isError() || itmOld.getResult() == "")
            {
                throw new Exception("存取網址資訊發生錯誤");
            }

            Item itmUpdate = cfg.inn.newItem("In_SiteUrl", "merge");
            itmUpdate.setAttribute("where", "id='" + url_id + "'");
            itmUpdate.setProperty("id", url_id);
            itmUpdate.setProperty("in_name1", itmReturn.getProperty("in_name1", ""));
            itmUpdate.setProperty("in_url1", itmReturn.getProperty("in_url1", ""));
            itmUpdate.setProperty("in_desc1", itmReturn.getProperty("in_desc1", ""));
            itmUpdate.setProperty("in_name2", itmReturn.getProperty("in_name2", ""));
            itmUpdate.setProperty("in_url2", itmReturn.getProperty("in_url2", ""));
            itmUpdate.setProperty("in_desc2", itmReturn.getProperty("in_desc2", ""));
            itmUpdate.setProperty("in_tags", itmReturn.getProperty("in_tags", ""));
            itmUpdate = itmUpdate.apply();

            if (itmUpdate.isError() || itmUpdate.getResult() == "")
            {
                throw new Exception("更新網址資訊發生錯誤");
            }
        }

        //查詢
        private void QueryType(TConfig cfg, Item itmReturn)
        {
            string type_id = itmReturn.getProperty("id", "");
            Item itmType = cfg.inn.applySQL("SELECT * FROM IN_SITETYPE WITH(NOLOCK) WHERE id = '" + type_id + "'");
            itmReturn.setProperty("type_id", itmType.getProperty("id", ""));
            itmReturn.setProperty("type_name", itmType.getProperty("in_value", ""));

            string sql = @"
                SELECT
                	t1.id AS 'map_id'
                	, t1.sort_order
                	, t2.*
                FROM 
                	IN_SITEURL_MAP t1 
                INNER JOIN 
                	IN_SITEURL t2 
                	ON t2.id = t1.source_id 
                WHERE 
                	t1.related_id = '{#type_id}'
                ORDER BY
                	t1.sort_order
            ";

            sql = sql.Replace("{#type_id}", type_id);

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                item.setType("inn_url");
                itmReturn.addRelationship(item);
            }
        }


        //查詢
        private void Query(TConfig cfg, Item itmReturn)
        {
            cfg.IsEdit = cfg.mode == "edit";

            var itmSites = GetSites(cfg);
            //轉換
            var map = MapSites(cfg, itmSites);

            //附加網站
            AppendSiteMain(cfg, map, itmReturn);

            //附加標籤
            AppendSiteTags(cfg, itmReturn);

            if (cfg.IsAdmin)
            {
                itmReturn.setProperty("hide_edit_btn", cfg.IsEdit ? "item_show_0" : "");
                itmReturn.setProperty("hide_edit_view", cfg.IsEdit ? "" : "item_show_0");
            }
            else
            {
                itmReturn.setProperty("hide_edit_btn", "item_show_0");
                itmReturn.setProperty("hide_edit_view", "item_show_0");
            }
        }

        //附加標籤
        private void AppendSiteTags(TConfig cfg, Item itmReturn)
        {
            var tags = GetAllTags(cfg);

            var selected_tag = itmReturn.getProperty("tag", "");

            foreach (var tag in tags)
            {
                Item itmTag = cfg.inn.newItem();
                itmTag.setType("inn_tag");
                itmTag.setProperty("value", tag);
                itmTag.setProperty("selected", tag == selected_tag ? "selected" : "");
                itmReturn.addRelationship(itmTag);
            }
        }

        //附加網站
        private void AppendSiteMain(TConfig cfg, Dictionary<string, TSiteType> map, Item itmReturn)
        {
            List<string> table_names = new List<string>();

            StringBuilder builder = new StringBuilder();

            foreach (var kv in map)
            {
                var key = kv.Key;
                var entity = kv.Value;

                var table_name = "tb_" + key;
                table_names.Add("#" + table_name);

                builder.AppendLine("<section id=\"" + entity.Id + "\">");
                builder.AppendLine("  <h2>");
                builder.AppendLine(entity.Name);
                if (cfg.IsEdit)
                {
                    builder.AppendLine("<small><a class='btn btn-sm btn-success' target='_blank' href='b.aspx?page=Innosoft_SiteType.html&method=in_sitemap&id=" + entity.Id + "&scene=type'>修改</a>");
                }
                builder.AppendLine("  </h2>");
                builder.AppendLine("  <div class='table-responsive'>");
                AppendSiteMap(cfg, builder, table_name, entity.SiteList);
                builder.AppendLine("  </div>");
                builder.AppendLine("</section>");
            }

            itmReturn.setProperty("inn_tables", builder.ToString());
            itmReturn.setProperty("inn_names", string.Join(", ", table_names));
        }

        private void AppendSiteMap(TConfig cfg, StringBuilder builder, string table_name, List<TSiteUrl> sites)
        {
            int count = sites.Count;
            int max_index = sites.Count - 1;

            int code = 4;
            int row = count / code;
            if (count % code != 0)
            {
                row = row + 1;
            }

            StringBuilder head = new StringBuilder();
            head.AppendLine("<thead>");
            head.AppendLine("    <th class='text-center' data-field='fd_0' >A</th>");
            head.AppendLine("    <th class='text-center' data-field='fd_1' >B</th>");
            head.AppendLine("    <th class='text-center' data-field='fd_2' >C</th>");
            head.AppendLine("    <th class='text-center' data-field='fd_3' >D</th>");
            head.AppendLine("</thead>");

            StringBuilder body = new StringBuilder();
            body.AppendLine("<tbody>");
            for (int i = 0; i < row; i++)
            {
                body.AppendLine("<tr>");

                for (int j = 0; j < code; j++)
                {
                    int index = i * code + j;
                    string field_name = "fd_" + j.ToString();

                    if (index > max_index)
                    {
                        body.AppendLine("<td class='text-left' data-field='" + field_name + "'>&nbsp;</td>");
                    }
                    else
                    {
                        var site = sites[index];
                        body.AppendLine("<td class='text-left' data-field='" + field_name + "'>" + GetSiteLink(cfg, site) + "</td>");
                    }
                }
                body.AppendLine("</tr>");
            }
            body.AppendLine("</tbody>");

            builder.AppendLine(GetTableAttribute(table_name));
            builder.Append(head);
            builder.Append(body);
            builder.AppendLine("</table>");
        }

        private string GetTableAttribute(string table_name)
        {
            return "<table id='" + table_name + "' class='table table-striped table-hover table-bordered' data-toggle='table' data-click-to-select='true' data-striped='false' data-search='false' data-pagination='false' data-show-pagination-switch='false' data-show-header='false'>";
        }

        private string GetSiteLink(TConfig cfg, TSiteUrl site)
        {
            var item = site.Value;
            string id = item.getProperty("id", "");
            string in_name1 = item.getProperty("in_name1", "");
            string in_url1 = item.getProperty("in_url1", "");
            string in_desc1 = item.getProperty("in_desc1", "");
            string in_name2 = item.getProperty("in_name2", "");
            string in_url2 = item.getProperty("in_url2", "");
            string in_desc2 = item.getProperty("in_desc2", "");

            string result = "";
            if (cfg.IsEdit)
            {
                result += "<small class='pull-right label bg-green edit-label' data-id='" + id + "' onclick='Edit_Click(this)'>修改</small>";
            }
            result += GetAhref(cfg, in_name1, in_url1, in_desc1);
            result += GetAhref(cfg, in_name2, in_url2, in_desc2);
            result += GetTag(cfg, site);

            return result;
        }

        private string GetTag(TConfig cfg, TSiteUrl site)
        {
            if (site.badges.Count == 0) return "";

            string badge = "<p>";
            foreach (var tag in site.badges)
            {
                badge += "<small class='label bg-blue'>" + tag + "</small>\n";
            }
            badge += "</p>";
            return badge;
        }

        private string GetAhref(TConfig cfg, string in_name, string in_url, string in_desc)
        {
            string link = "";
            if (in_name != "")
            {
                if (in_url != "")
                {
                    link += "<p>";
                    link += "  <a href='" + in_url + "' target='_blank'>" + in_name + "</a>";
                    if (in_desc != "")
                    {
                        link += "<br />" + in_desc;
                    }
                    link += "</p>";
                }
                else
                {
                    link += "<p>";
                    link += in_name;
                    if (in_desc != "")
                    {
                        link += "<br />" + in_desc;
                    }
                    link += "</p>";
                }
            }
            return link;
        }

        private Item GetSites(TConfig cfg)
        {
            string tag_condition = cfg.tag == ""
                ? ""
                : "AND t1.in_tags LIKE N'%" + cfg.tag + "%'";

            string sql = @"
                SELECT
                    t3.id AS 'type_id'
                    , t3.in_value AS 'type_name'
	                , t1.*
                FROM
	                IN_SiteUrl t1 WITH(NOLOCK)
                INNER JOIN
	                IN_SiteUrl_Map t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                INNER JOIN
	                In_SiteType t3 WITH(NOLOCK)
	                ON t3.id = t2.related_id
                WHERE
                    t1.in_closed = 0
                    {#tag_condition}
                ORDER BY
	                t3.sort_order
	                , t2.sort_order
            ";

            sql = sql.Replace("{#tag_condition}", tag_condition);

            return cfg.inn.applySQL(sql);
        }

        private Dictionary<string, TSiteType> MapSites(TConfig cfg, Item items)
        {
            var map = new Dictionary<string, TSiteType>();

            if (IsError(items, isSingle: false))
            {
                return map;
            }

            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string key = item.getProperty("type_id", "");
                string name = item.getProperty("type_name", "");
                string in_tags = item.getProperty("in_tags", "");


                TSiteType entity = null;
                if (map.ContainsKey(key))
                {
                    entity = map[key];
                }
                else
                {
                    entity = new TSiteType
                    {
                        Id = key,
                        Name = name,
                        SiteList = new List<TSiteUrl>(),
                    };
                    map.Add(key, entity);
                }

                TSiteUrl url = new TSiteUrl
                {
                    Value = item,
                };

                string[] tag_arr = in_tags.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                if (tag_arr == null || tag_arr.Length == 0)
                {
                    url.badges = new List<string>();
                }
                else
                {
                    url.badges = tag_arr.ToList();
                }

                entity.SiteList.Add(url);
            }

            return map;
        }

        private List<string> GetAllTags(TConfig cfg)
        {
            List<string> tags = new List<string>();
            tags.Add("全部");

            Item itmSiteUrls = cfg.inn.applySQL("SELECT DISTINCT in_tags FROM IN_SITEURL WITH(NOLOCK) WHERE ISNULL(in_tags, '') <> ''");
            int count = itmSiteUrls.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item itmSiteUrl = itmSiteUrls.getItemByIndex(i);
                string in_tags = itmSiteUrl.getProperty("in_tags", "");
                string[] tag_arr = in_tags.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                if (tag_arr == null || tag_arr.Length == 0) continue;
                foreach (var tag in tag_arr)
                {
                    if (!tags.Contains(tag))
                    {
                        tags.Add(tag);
                    }
                }
            }
            return tags;
        }

        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string ssn_loginid { get; set; }

            public string scene { get; set; }
            public string mode { get; set; }
            public string tag { get; set; }

            public bool IsAdmin { get; set; }
            public bool IsEdit { get; set; }
        }

        private class TSiteType
        {
            public string Id { get; set; }
            public string Name { get; set; }
            public List<TSiteUrl> SiteList { get; set; }
        }

        private class TSiteUrl
        {
            public Item Value { get; set; }
            public string in_tags { get; set; }
            public List<string> badges { get; set; }
        }

        private bool IsError(Item item, bool isSingle = true)
        {
            if (item.isError() || item.getResult() == "")
            {
                return true;
            }
            else if (isSingle && item.getItemCount() != 1)
            {
                return true;
            }
            else if (item.getItemCount() <= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        private int GetIntValue(string value)
        {
            int result = 0;
            if (Int32.TryParse(value, out result))
            {
                return result;
            }
            else
            {
                return 0;
            }
        }
    }
}