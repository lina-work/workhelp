﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;
using System.Net;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class in_meeting_program_preview_robin : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 循環賽預覽
                日期: 
                    2020-11-24: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]in_meeting_program_preview_robin";

            Item itmProgram = this;
            Item itmR = inn.newItem();
            //CCO.Utilities.WriteDebug(strMethodName, this.dom.InnerXml);

            string meeting_id = itmProgram.getProperty("meeting_id", "");
            string program_id = itmProgram.getProperty("program_id", "");
            string eno = itmProgram.getProperty("eno", "");

            this.GLOBAL_EVENT_EDIT = eno == "edit";

            //附加比賽選手
            Item itmPlayers = GetPlayers(CCO, strMethodName, inn, meeting_id, program_id);

            //附加場次
            Item itmEvents = GetEventPlayers(CCO, strMethodName, inn, meeting_id, program_id);

            Dictionary<string, TStatistics> map = MapTeam(itmEvents);

            if (!itmPlayers.isError())
            {
                AppendCycleEvent(CCO, strMethodName, inn, map, itmProgram, itmPlayers, itmEvents, itmR);
            }

            return itmR;
        }

        //循環賽
        private void AppendCycleEvent(Aras.Server.Core.CallContext CCO
            , string strMethodName
            , Innovator inn
            , Dictionary<string, TStatistics> map
            , Item itmProgram
            , Item itmPlayers
            , Item itmEvents
            , Item itmReturn)
        {

            var model = new TCycleModel();
            model.program_id = itmProgram.getProperty("id", "");
            model.in_battle_type = itmProgram.getProperty("in_battle_type", "");
            model.is_double = model.in_battle_type == "DoubleRoundRobin";
            model.player_map = ConvertMap(CCO, strMethodName, inn, itmPlayers, "in_sign_no");
            model.event_map = ConvertMap2(CCO, strMethodName, inn, itmEvents, "in_robin_key");

            if (model.player_map.Count == 0 || model.event_map.Count == 0)
            {
                return;
            }

            model.team_count = GetIntVal(itmProgram.getProperty("in_team_count", ""));
            model.row_title = model.is_double ? "循環二＼循環一" : "第一循環";
            model.itmPlayers = itmPlayers;
            model.itmEvents = itmEvents;

            var builder = new StringBuilder();

            //循環賽表格
            AppendCycelTable(CCO, strMethodName, inn, map, model, builder);

            //對戰表格
            AppendFightTable(CCO, strMethodName, inn, model, builder);

            itmReturn.setProperty("bracket_table", builder.ToString());
        }

        private List<TEvent> MapEvents(TCycleModel model)
        {
            List<TEvent> list = new List<TEvent>();

            int count = model.itmEvents.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = model.itmEvents.getItemByIndex(i);
                string event_id = item.getProperty("event_id", "");
                string in_sign_foot = item.getProperty("in_sign_foot", "");
                string in_tree_no = item.getProperty("in_tree_no", "0");
                var evt = list.Find(x => x.Id == event_id);

                if (evt == null)
                {
                    evt = new TEvent
                    {
                        Id = event_id,
                        Value = item,
                        TreeNo = GetIntVal(in_tree_no),
                    };
                    list.Add(evt);
                }

                if (in_sign_foot == "1")
                {
                    evt.itmFoot1 = item;
                }
                else
                {
                    evt.itmFoot2 = item;
                }
            }

            return list.OrderBy(x => x.TreeNo).ToList();
        }

        private void AppendFightTable(Aras.Server.Core.CallContext CCO
            , string strMethodName
            , Innovator inn
            , TCycleModel model
            , StringBuilder builder)
        {

            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            head.AppendLine("<thead>");
            head.AppendLine("  <tr>");
            head.AppendLine("    <td class='text-center' rowspan='2'>場地</td>");
            head.AppendLine("    <td class='text-center' rowspan='2'>場次</td>");
            head.AppendLine("    <td class='text-center white_group' rowspan='2'>白方<br>籤號</td>");
            head.AppendLine("    <td class='text-center white_group' rowspan='2'>白方選手</td>");
            head.AppendLine("    <td class='text-center white_group' rowspan='1' colspan='3'>得分</td>");
            head.AppendLine("    <td class='text-center white_group' rowspan='2'>得分</td>");
            head.AppendLine("    <td class='text-center' rowspan='2'>比賽時間</td>");
            head.AppendLine("    <td class='text-center blue_group' rowspan='2'>得分</td>");
            head.AppendLine("    <td class='text-center blue_group' rowspan='1' colspan='3'>得分</td>");
            head.AppendLine("    <td class='text-center blue_group' rowspan='2'>藍方選手</td>");
            head.AppendLine("    <td class='text-center blue_group' rowspan='2'>藍方<br>籤號</td>");
            head.AppendLine("  </tr>");
            head.AppendLine("  <tr>");
            head.AppendLine("    <td class='text-center white_group'>I</td>");
            head.AppendLine("    <td class='text-center white_group'>W</td>");
            head.AppendLine("    <td class='text-center white_group'>S</td>");
            head.AppendLine("    <td class='text-center blue_group'>I</td>");
            head.AppendLine("    <td class='text-center blue_group'>W</td>");
            head.AppendLine("    <td class='text-center blue_group'>S</td>");
            head.AppendLine("  </tr>");
            head.AppendLine("</thead>");

            var list = MapEvents(model);

            body.AppendLine("<tbody>");

            for (int i = 0; i < list.Count; i++)
            {
                var evt = list[i];
                if (evt.itmFoot1 == null) evt.itmFoot1 = inn.newItem();
                if (evt.itmFoot2 == null) evt.itmFoot2 = inn.newItem();

                evt.Foot1 = MapFoot(evt.itmFoot1);
                evt.Foot2 = MapFoot(evt.itmFoot2);

                string site_name = evt.Value.getProperty("site_name", "");
                string in_win_local_time = evt.Value.getProperty("in_win_local_time", "");

                site_name = site_name.Replace("第", "").Replace("場地", "");

                body.AppendLine("<tr>");
                body.AppendLine("    <td class='text-center'>" + site_name + "</td>");
                body.AppendLine("    <td class='text-center'>" + evt.TreeNo + "</td>");

                body.AppendLine("    <td class='text-center white_group'>" + evt.Foot1.display_no + "</td>");
                body.AppendLine("    <td class='text-center white_group'>" + evt.Foot1.display_name + "</td>");
                body.AppendLine("    <td class='text-center white_group'>" + evt.Foot1.score_i + "</td>");
                body.AppendLine("    <td class='text-center white_group'>" + evt.Foot1.score_w + "</td>");
                body.AppendLine("    <td class='text-center white_group'>" + evt.Foot1.score_s + "</td>");
                body.AppendLine("    <td class='text-center white_group'>" + evt.Foot1.score_points + "</td>");

                body.AppendLine("    <td class='text-center'>" + in_win_local_time + "</td>");

                body.AppendLine("    <td class='text-center blue_group'>" + evt.Foot2.score_points + "</td>");
                body.AppendLine("    <td class='text-center blue_group'>" + evt.Foot2.score_i + "</td>");
                body.AppendLine("    <td class='text-center blue_group'>" + evt.Foot2.score_w + "</td>");
                body.AppendLine("    <td class='text-center blue_group'>" + evt.Foot2.score_s + "</td>");
                body.AppendLine("    <td class='text-center blue_group'>" + evt.Foot2.display_name + "</td>");
                body.AppendLine("    <td class='text-center blue_group'>" + evt.Foot2.display_no + "</td>");

                body.AppendLine("</tr>");
            }

            body.AppendLine("</tbody>");

            var table_name = "table_fight_" + model.program_id;

            builder.AppendLine("</div>");
            builder.AppendLine("<div class='container' style='margin-top: 30px;'>");
            builder.AppendLine("<table id='" + table_name + "' class='table table-hover table-bordered table-rwd rwd rwdtable match-table'>");
            builder.Append(head);
            builder.Append(body);
            builder.AppendLine("</table>");
        }

        private void AppendCycelTable(Aras.Server.Core.CallContext CCO
            , string strMethodName
            , Innovator inn
            , Dictionary<string, TStatistics> map
            , TCycleModel model
            , StringBuilder builder)
        {
            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();


            head.AppendLine("<thead>");
            head.AppendLine("<tr>");
            head.AppendLine("<th class='text-center'>籤號</th>");
            head.AppendLine("<th class='text-center'>" + model.row_title + "</th>");
            for (int x = 1; x <= model.team_count; x++)
            {
                var itmPlayer = FindItem(inn, model.player_map, x.ToString());
                head.AppendLine("<th class='text-center' style='min-width: 60px'>" + GetCycleNameInfo(itmPlayer, isAddNo: true) + "</th>");
            }
            head.AppendLine("<th class='text-center'>勝場</th>");
            head.AppendLine("<th class='text-center'>積分</th>");
            head.AppendLine("<th class='text-center'>名次</th>");
            head.AppendLine("</tr>");
            head.AppendLine("</thead>");

            int count = model.itmPlayers.getItemCount();

            body.AppendLine("<tbody>");

            for (int y = 1; y <= model.team_count; y++)
            {
                Item itmPlayer = FindItem(inn, model.player_map, y.ToString());
                string in_sign_no = itmPlayer.getProperty("in_sign_no", "");
                string check_class = GetCheckClass(itmPlayer);
                TStatistics statistics = FindStatistics(map, in_sign_no);

                body.AppendLine("<tr>");
                body.AppendLine("<td class='text-center'>" + GetCycleSignNoInfo(itmPlayer) + "</td>");
                body.AppendLine("<td class='text-center " + check_class + "'>" + GetCycleNameInfo(itmPlayer) + "</td>");
                for (int x = 1; x <= model.team_count; x++)
                {
                    var event_key = model.is_double ? GetDoubleCycleKey(y, x) : GetSingleCycleKey(y, x);
                    var event_items = FindItems(inn, model.event_map, event_key);
                    var event_first = event_items.First();

                    var in_tree_id = event_first.getProperty("in_tree_id", "");

                    if (in_tree_id != "")
                    {
                        var in_tree_no = event_first.getProperty("in_tree_no", "");
                        var in_site_allocate = event_first.getProperty("in_site_allocate", "");
                        var in_site_id = event_first.getProperty("in_site_id", "");
                        var in_win_sign_no = event_first.getProperty("in_win_sign_no", "");

                        var entity = default(TDetail);
                        if (in_win_sign_no != "")
                        {
                            entity = GetBattleResult(event_items);
                        }
                        else
                        {
                            entity = GetBattleInitial(event_items);
                        }

                        var event_link = GetRowIdLink(model.program_id, "", in_tree_id, in_site_id, entity.score_div, entity.btn_class);

                        body.AppendLine("<td class='text-center'>" + entity.event_div + "<br>" + event_link + "</td>");

                        model.event_map.Remove(event_key);
                    }
                    else
                    {
                        body.AppendLine("<td class='text-center disable-evet'>&nbsp;</td>");
                    }
                }
                body.AppendLine("<td class='text-center' data-title='勝場'>" + statistics.total_wins + "</td>");
                body.AppendLine("<td class='text-center' data-title='積分'>" + statistics.total_points + "</td>");
                body.AppendLine("<td class='text-center' data-title='名次'>" + itmPlayer.getProperty("in_final_rank", "") + "</td>");
                body.AppendLine("</tr>");
            }
            body.AppendLine("</tbody>");

            var table_name = "table_" + model.program_id;

            builder.AppendLine("<table id='" + table_name + "' class='table table-hover table-bordered table-rwd rwd rwdtable match-table'>");
            builder.Append(head);
            builder.Append(body);
            builder.AppendLine("</table>");

            // builder.AppendLine("<script>");
            // builder.AppendLine("$('#" + table_name + "').bootstrapTable({});");

            // builder.AppendLine("if($(window).width() <= 768) {");
            // builder.AppendLine("    $('#" + table_name + "').bootstrapTable('toggleView');");
            // builder.AppendLine("}");

            // builder.AppendLine("</script>");
        }

        //是否場次編號可編輯模式
        private bool GLOBAL_EVENT_EDIT = false;

        /// <summary>
        /// 統計隊伍積分與勝場
        /// </summary>
        private Dictionary<string, TStatistics> MapTeam(Item itmEvents)
        {
            Dictionary<string, TStatistics> map = new Dictionary<string, TStatistics>();
            int count = itmEvents.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item itmEvent = itmEvents.getItemByIndex(i);
                string in_sign_no = itmEvent.getProperty("in_sign_no", "");
                string in_status = itmEvent.getProperty("in_status", "");
                string in_points = itmEvent.getProperty("in_points", "0");
                int points = GetIntVal(in_points);

                TStatistics entity = null;
                if (map.ContainsKey(in_sign_no))
                {
                    entity = map[in_sign_no];
                }
                else
                {
                    entity = new TStatistics
                    {
                        in_sign_no = in_sign_no,
                        total_points = 0,
                        total_wins = 0,
                    };
                    map.Add(in_sign_no, entity);
                }

                entity.total_points += points;

                if (in_status == "1")
                {
                    entity.total_wins++;
                }

            }
            return map;
        }

        private TStatistics FindStatistics(Dictionary<string, TStatistics> map, string in_sign_no)
        {
            if (map.ContainsKey(in_sign_no))
            {
                return map[in_sign_no];
            }
            else
            {
                return new TStatistics
                {
                    in_sign_no = in_sign_no,
                    total_points = 0,
                    total_wins = 0
                };
            }
        }


        private string GetRowIdLink(string pid, string is_allocate, string rid, string sid, string tno, string btn_class = "")
        {
            if (GLOBAL_EVENT_EDIT)
            {
                return GetRowIdLinkEdit(pid, is_allocate, rid, sid, tno, btn_class);
            }
            else
            {
                return GetRowIdLinkView(pid, is_allocate, rid, sid, tno, btn_class);
            }
        }

        private string GetRowIdLinkView(string pid, string is_allocate, string rid, string sid, string tno, string btn_class = "")
        {
            string current_class = btn_class == "" ? "event-btn" : btn_class;

            if (is_allocate == "1")
            {
                return "<a class='" + current_class + "' href='javascript:void()' onclick='Event_Click(this)' data-pid='" + pid + "' data-rid='" + rid + "'>" + sid + "</a>";
            }
            else if (tno == "0")
            {
                return "<a class='no-event-btn' href='javascript:void()' onclick='Event_Click(this)' data-pid='" + pid + "' data-rid='" + rid + "'>　</a>";
                //return "&nbsp;";
            }
            else if (tno != "")
            {
                string val = "&nbsp;" + tno + "&nbsp;";

                return "<a class='" + current_class + "' href='javascript:void()' onclick='Event_Click(this)' data-pid='" + pid + "' data-rid='" + rid + "'>" + val + "</a>";
            }
            else
            {
                return "<a class='no-event-btn' href='javascript:void()' onclick='Event_Click(this)' data-pid='" + pid + "' data-rid='" + rid + "'>　</a>";
                //return "&nbsp;";
            }
        }

        private string GetRowIdLinkEdit(string pid, string is_allocate, string rid, string sid, string tno, string btn_class = "")
        {
            if (is_allocate == "1")
            {
                return "<input type='text' value='" + sid + "' class='event-input'"
                + " data-pid='" + pid + "' data-rid='" + rid + "' data-old='" + sid + "'"
                + " onkeyup='Event_KeyUp(this)'"
                + " ondblclick='Event_Click(this)'"
                + " >";
            }
            else if (tno != "")
            {
                return "<input type='text' value='" + tno + "' class='event-input'"
                + " data-pid='" + pid + "' data-rid='" + rid + "' data-old='" + tno + "'"
                + " onkeyup='Event_KeyUp(this)'"
                + " ondblclick='Event_Click(this)'"
                + " >";
            }
            else
            {
                return "<input type='text' value='NA' class='event-input'"
                + " data-pid='" + pid + "' data-rid='" + rid + "' data-old='" + tno + "'"
                + " onkeyup='Event_KeyUp(this)'"
                + " ondblclick='Event_Click(this)'"
                + " >";
            }
        }


        // private string GetRowIdLink(string pid, string is_allocate, string rid, string sid, string tno, string btn_class = "")
        // {
        //     string current_class = btn_class == "" ? "event-btn" : btn_class;

        //     if (is_allocate == "1")
        //     {
        //         return "<a class='" + current_class + "' href='javascript:void()' onclick='Event_Click(this)' data-pid='" + pid + "' data-rid='" + rid + "'>" + sid + "</a>";
        //     }
        //     else if (tno != "")
        //     {
        //         string val = "&nbsp;" + tno + "&nbsp;";

        //         return "<a class='" + current_class + "' href='javascript:void()' onclick='Event_Click(this)' data-pid='" + pid + "' data-rid='" + rid + "'>" + val + "</a>";
        //     }
        //     else
        //     {
        //         return "&nbsp;";
        //     }
        // }

        private string GetCheckClass(Item itmPlayer)
        {
            string in_check_result = itmPlayer.getProperty("in_check_result", "");

            switch (in_check_result)
            {
                case "1": return "btn-success";
                case "0": return "btn-danger";
                default: return "";
            }
        }

        private TDetail GetBattleResult(List<Item> items)
        {
            TDetail result = new TDetail
            {
                event_div = "",
                score_div = "",
                btn_class = "",
            };

            if (items.Count < 2)
            {
                return result;
            }

            Item item1 = items[0];
            Item item2 = items[1];

            string in_tree_no = item1.getProperty("in_tree_no", "");
            string in_win_sign_no = item1.getProperty("in_win_sign_no", "");
            string in_sign_foot = item1.getProperty("in_sign_foot", "");

            string f1_sign_no = item1.getProperty("in_sign_no", "");
            string f2_sign_no = item2.getProperty("in_sign_no", "");

            if (f1_sign_no == in_win_sign_no) f1_sign_no += "(win)";
            if (f2_sign_no == in_win_sign_no) f2_sign_no += "(win)";

            if (in_sign_foot == "1")
            {
                result.btn_class = "event-btn2";
                result.event_div = " <span class='event-btn'>&nbsp;" + in_tree_no + " </span>&nbsp;" + f1_sign_no + " vs " + f2_sign_no;
                result.score_div = item1.getProperty("in_points", "0") + ":" + item2.getProperty("in_points", "0");
            }
            else
            {
                result.btn_class = "event-btn2";
                result.event_div = " <span class='event-btn'>&nbsp;" + in_tree_no + " </span>&nbsp;" + f2_sign_no + " vs " + f1_sign_no;
                result.score_div = item2.getProperty("in_points", "0") + ":" + item1.getProperty("in_points", "0");
            }

            return result;
        }

        private TDetail GetBattleInitial(List<Item> items)
        {
            TDetail result = new TDetail
            {
                event_div = "",
                score_div = "",
                btn_class = "",
            };

            if (items.Count < 2)
            {
                return result;
            }

            Item item1 = items[0];
            Item item2 = items[1];

            string in_tree_no = item1.getProperty("in_tree_no", "");
            string in_win_sign_no = item1.getProperty("in_win_sign_no", "");
            string in_sign_foot = item1.getProperty("in_sign_foot", "");

            string f1_sign_no = item1.getProperty("in_sign_no", "");
            string f2_sign_no = item2.getProperty("in_sign_no", "");

            if (f1_sign_no == in_win_sign_no) f1_sign_no += "(win)";
            if (f2_sign_no == in_win_sign_no) f2_sign_no += "(win)";

            if (in_sign_foot == "1")
            {
                result.event_div = f1_sign_no + " vs " + f2_sign_no;
                result.score_div = in_tree_no;
            }
            else
            {
                result.event_div = f2_sign_no + " vs " + f1_sign_no;
                result.score_div = in_tree_no;
            }

            return result;
        }

        private string GetCycleSignNoInfo(Item item)
        {
            return item.getProperty("in_sign_no", "");
        }

        private string GetCycleNameInfo(Item item, bool isAddNo = false)
        {
            string in_l1 = item.getProperty("in_l1", "");
            string in_name = item.getProperty("in_name", "");
            string in_section_no = item.getProperty("in_section_no", "");
            string map_short_org = item.getProperty("map_short_org", "");

            if (in_l1 == "團體組")
            {
                if (in_name == "")
                {
                    return map_short_org;
                }
                else
                {
                    return map_short_org + "(" + in_name + ")";
                }
            }
            else if (isAddNo)
            {
                return in_section_no + "<br>" + map_short_org + "<br>" + in_name;

            }
            else
            {
                return map_short_org + "<br>" + in_name;
            }
        }

        /// <summary>
        /// 轉換為字典
        /// </summary>
        private Dictionary<string, Item> ConvertMap(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item items, string property)
        {
            Dictionary<string, Item> map = new Dictionary<string, Item>();

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string key = item.getProperty(property, "");

                if (map.ContainsKey(key))
                {
                    //throw new Exception("鍵值重覆");
                }
                else
                {
                    map.Add(key, item);
                }
            }
            return map;
        }
        /// <summary>
        /// 轉換為字典
        /// </summary>
        private Dictionary<string, List<Item>> ConvertMap2(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item items, string property)
        {
            Dictionary<string, List<Item>> map = new Dictionary<string, List<Item>>();

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string key = item.getProperty(property, "");

                List<Item> list = null;
                if (map.ContainsKey(key))
                {
                    list = map[key];
                }
                else
                {
                    list = new List<Item>();
                    map.Add(key, list);
                }
                list.Add(item);
            }
            return map;
        }

        /// <summary>
        /// 找出項目
        /// </summary>
        private Item FindItem(Innovator inn, Dictionary<string, Item> map, string key)
        {
            if (map.ContainsKey(key))
            {
                return map[key];
            }
            else
            {
                return inn.newItem();
            }
        }

        /// <summary>
        /// 找出項目清單
        /// </summary>
        private List<Item> FindItems(Innovator inn, Dictionary<string, List<Item>> map, string key)
        {
            if (map.ContainsKey(key))
            {
                return map[key];
            }
            else
            {
                List<Item> list = new List<Item>();
                list.Add(inn.newItem());
                list.Add(inn.newItem());
                return list;
            }
        }

        /// <summary>
        /// 取得單循環對戰鍵值
        /// </summary>
        private string GetSingleCycleKey(int y, int x)
        {
            if (y < x)
            {
                return y + "," + x;
            }
            else
            {
                return x + "," + y;
            }
        }

        /// <summary>
        /// 取得雙循環對戰鍵值
        /// </summary>
        private string GetDoubleCycleKey(int y, int x)
        {
            return y + "," + x;
        }

        //取得選手
        private Item GetPlayers(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string meeting_id, string program_id)
        {
            string sql = @"
                SELECT
                    t1.in_sign_no
                    , t1.in_section_no
                    , t1.in_check_result
                    , t1.in_current_org
                    , t1.map_short_org
                    , t1.in_name
                    , t1.in_sno
                    , t1.in_final_rank
                    , t2.in_l1
                FROM
                    VU_MEETING_PTEAM t1 WITH(NOLOCK)
                INNER JOIN
                    IN_MEETING_PROGRAM t2 WITH(NOLOCK)
                    ON t2.id = t1.source_id
                WHERE
                    t1.source_id = '{#program_id}'
                    --AND ISNULL(t1.in_sign_no, '') <> ''
                ORDER BY
                    CAST(ISNULL(t1.in_sign_no, '0') AS INT)
            ";

            sql = sql.Replace("{#program_id}", program_id);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return inn.applySQL(sql);
        }

        //取得賽事組別場次
        private Item GetEventPlayers(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string meeting_id, string program_id)
        {
            string sql = @"
                SELECT
                    t1.id                   AS 'event_id'
                    , t1.in_tree_name
                    , t1.in_tree_id
                    , t1.in_tree_no
                    , t1.in_round
                    , t1.in_round_id
                    , t1.in_round_code
                    , t1.in_next_win
                    , t1.in_next_foot_win
                    , t1.in_next_lose
                    , t1.in_next_foot_lose
                    , t1.in_detail_ns
                    , t1.in_robin_key
                    , t1.in_win_status
                    , t1.in_win_sign_no
                    , t1.in_win_local_time
                    , t2.id                 AS 'detail_id'
                    , t2.in_sign_foot
                    , t2.in_sign_no
                    , t2.in_sign_bypass
                    , t2.in_target_no
                    , t2.in_status
                    , t2.in_points
                    , t2.in_points_type
                    , t3.in_name            AS 'site_name'
                    , t11.id                AS 'mdid'
                    , t11.in_current_org
                    , t11.in_short_org
                    , t11.map_short_org
                    , t11.in_name
                    , t11.in_names
                    , t11.in_sno
                    , t11.in_org_teams
                    , t11.in_section_no
                    , t11.in_seeds
                    , t11.in_check_result
                FROM 
                    IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
                    IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK)
                    ON t2.source_id = t1.id
                LEFT OUTER JOIN
                    IN_MEETING_SITE t3 WITH(NOLOCK)
                    ON t3.id = t1.in_site
                LEFT OUTER JOIN
                    VU_MEETING_PTEAM t11 WITH(NOLOCK)
                    ON t11.source_id = t1.source_id
                    AND ISNULL(t11.in_sign_no, '') <> ''
                    AND t11.in_sign_no = t2.in_sign_no
                WHERE
                    t1.source_id = '{#program_id}'
                ORDER BY
                    t1.in_tree_sort
                    , t1.in_round
                    , t1.in_round_id
                    , t2.in_sign_foot
            ";

            sql = sql.Replace("{#meeting_id}", meeting_id)
                .Replace("{#program_id}", program_id);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return inn.applySQL(sql);
        }

        private class TDetail
        {
            public string event_div { get; set; }
            public string score_div { get; set; }
            public string btn_class { get; set; }
        }

        private class TStatistics
        {
            /// <summary>
            /// 籤號
            /// </summary>
            public string in_sign_no { get; set; }

            /// <summary>
            /// 總積分
            /// </summary>
            public int total_points { get; set; }

            /// <summary>
            /// 總勝場
            /// </summary>
            public int total_wins { get; set; }
        }

        private class TCycleModel
        {
            public string program_id { get; set; }
            public string in_battle_type { get; set; }
            public bool is_double { get; set; }
            public Dictionary<string, Item> player_map { get; set; }
            public Dictionary<string, List<Item>> event_map { get; set; }
            public int team_count { get; set; }
            public string row_title { get; set; }
            public Item itmPlayers { get; set; }
            public Item itmEvents { get; set; }
        }

        private class TEvent
        {
            public string Id { get; set; }
            public int TreeNo { get; set; }
            public Item Value { get; set; }
            public Item itmFoot1 { get; set; }
            public Item itmFoot2 { get; set; }
            public TFoot Foot1 { get; set; }
            public TFoot Foot2 { get; set; }
        }

        private TFoot MapFoot(Item item)
        {
            var result = new TFoot();

            string in_section_no = item.getProperty("in_section_no", "");
            string org = item.getProperty("map_short_org", "");
            string name = item.getProperty("in_name", "");
            string in_points = item.getProperty("in_points", "");
            string in_correct_count = item.getProperty("in_correct_count", "");

            if (in_points.Length == 2)
            {
                result.score_i = in_points[0].ToString();
                result.score_w = in_points[1].ToString();
            }
            else if (in_points.Length == 1)
            {
                result.score_i = "";
                result.score_w = in_points;
            }
            else
            {
                result.score_i = "";
                result.score_w = "";
            }

            result.display_no = in_section_no;
            result.display_name = org + "<br>" + name;

            result.score_points = in_points;
            result.score_s = in_correct_count;

            return result;
        }

        private class TFoot
        {
            public string display_no { get; set; }
            public string display_name { get; set; }
            public string score_i { get; set; }
            public string score_w { get; set; }
            public string score_s { get; set; }
            public string score_points { get; set; }
        }


        private int GetIntVal(string value, int def = 0)
        {
            if (value == "") return def;
            int result = def;
            Int32.TryParse(value, out result);
            return result;
        }
    }
}