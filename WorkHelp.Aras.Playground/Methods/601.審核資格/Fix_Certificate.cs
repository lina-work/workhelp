﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;
using System.Net;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class Fix_Certificate : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]Fix_Certificate";

            Item itmR = this;

            string sql = "";

            sql = "UPDATE IN_RESUME_REFEREE SET in_level = '' WHERE in_level IS NULL";
            inn.applySQL(sql);

            sql = "UPDATE IN_RESUME_REFEREE SET in_echelon = '' WHERE in_echelon IS NULL";
            inn.applySQL(sql);



            sql = "SELECT * FROM AAA_RESUME_REFEREE";

            Item items = inn.applySQL(sql);

            int count = items.getItemCount();

            List<TCol> cols = new List<TCol>();
            cols.Add(new TCol { prop = "source_id" });
            cols.Add(new TCol { prop = "in_date", getVal = GetDate });
            cols.Add(new TCol { prop = "in_year" });
            cols.Add(new TCol { prop = "in_level" });
            cols.Add(new TCol { prop = "in_echelon" });
            cols.Add(new TCol { prop = "in_know_score" });
            cols.Add(new TCol { prop = "in_tech_score" });
            cols.Add(new TCol { prop = "in_evaluation" });
            cols.Add(new TCol { prop = "in_note" });
            cols.Add(new TCol { prop = "in_certificate" });
            cols.Add(new TCol { prop = "in_certificate_no" });
            cols.Add(new TCol { prop = "in_hours" });
            //cols.Add(new TCol { prop = "in_has_muser" });
            //cols.Add(new TCol { prop = "in_user" });
            //cols.Add(new TCol { prop = "in_cla_user" });

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string resume_id = item.getProperty("resume_id", "");
                string in_year = item.getProperty("in_year", "");
                string in_level = item.getProperty("in_level", "");
                string in_echelon = item.getProperty("in_echelon", "");

                Item itmData = inn.newItem("In_Resume_Referee", "merge");

                string condition = "source_id = '" + resume_id + "'"
                    + " AND in_year = '" + in_year + "'"
                    + " AND in_level = '" + in_level + "'"
                    + " AND in_echelon = '" + in_echelon + "'"
                    + "";

                itmData.setAttribute("where", condition);
                itmData.setProperty("source_id", resume_id);

                foreach(var col in cols)
                {
                    string value = "";
                    if (col.getVal == null)
                    {
                        value = item.getProperty(col.prop, "");
                    }
                    else
                    {
                        value = col.getVal(item, col);
                    }

                    itmData.setProperty(col.prop, value);

                    Item itmResult = itmData.apply();

                    if (itmResult.isError())
                    {
                        throw new Exception("修正失敗");
                    }
                }
            }

            return itmR;
        }
    
        private string GetDate(Item item, TCol col)
        {
            return DateTimeStr(item.getProperty(col.prop));
        }

        private class TCol
        {
            public string prop { get; set; }
            public Func<Item, TCol, string> getVal { get; set; }
        }

        private string DateTimeStr(string value, string format = "yyyy-MM-ddTHH:mm:ss")
        {
            DateTime result = DateTime.MinValue;
            if (DateTime.TryParse(value, out result))
            {
                return result.AddHours(8).ToString(format);
            }
            return "";
        }
    }
}