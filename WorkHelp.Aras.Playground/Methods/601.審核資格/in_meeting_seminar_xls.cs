﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class in_meeting_seminar_xls : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 講習名冊(待審核)
                日期: 2021-03-25: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]in_meeting_seminar_xls";

            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                year_count = 10,
            };

            if (cfg.meeting_id == "")
            {
                throw new Exception("活動 id 不得為空白");
            }

            //取得活動資料
            cfg.itmMeeting = cfg.inn.applySQL("SELECT id, in_title, in_seminar_type FROM IN_CLA_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            cfg.in_seminar_type = cfg.itmMeeting.getProperty("in_seminar_type", "");
            //取得問項資料
            //cfg.itmMSurveys = GetMeetingSuverys(cfg);
            //取得服務年度資料
            Dictionary<string, TResume> year_map = MapYears(cfg, GetResumeYears(cfg));
            //取得與會者資料
            Dictionary<string, TTable> map = MapMUsers(cfg, GetMeetingUsers(cfg));

            //取得匯出資料
            TExport export = GetExport(cfg);

            ClosedXML.Excel.XLWorkbook workbook = new ClosedXML.Excel.XLWorkbook();
            
            foreach(var kv in map)
            {
                var table = kv.Value;

                //與會者附加服務年度資料
                MatchMUserYears(cfg, table.List, year_map);

                AppendSheet(workbook, cfg, table);
            }

            workbook.SaveAs(export.xls_file);

            itmR.setProperty("xls_name", export.xls_url);

            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

            return itmR;
        }

        private void MatchMUserYears(TConfig cfg, List<TMUser> list, Dictionary<string, TResume> year_map)
        {
            foreach(var muser in list)
            {
                Item item = muser.Value;
                if (!year_map.ContainsKey(muser.resume_id)) continue;
                
                var resume = year_map[muser.resume_id];
                for(int i = 0; i < resume.TargetYears.Count; i++)
                {
                    string property = "y" + i;
                    item.setProperty(property, resume.TargetYears[i]);
                }
            }
        }

        //報名總表
        private void AppendSheet(ClosedXML.Excel.XLWorkbook workbook, TConfig cfg, TTable table)
        {
            ClosedXML.Excel.IXLWorksheet sheet = workbook.Worksheets.Add(table.Name);

            List<TField> fields = new List<TField>();
            fields.Add(new TField { title = "姓名", property = "in_name", format = "center", width = 15 });
            fields.Add(new TField { title = "性別", property = "in_gender", format = "center", width = 6 });
            fields.Add(new TField { title = "出生年月日", property = "in_birth", getValue = BirthDay, format = "yyyy/MM/dd", width = 12 });
            fields.Add(new TField { title = "身分證字號", property = "in_sno", format = "center", width = 15 });
            fields.Add(new TField { title = "聯絡電話", property = "in_tel", format = "tel", width = 15 });
            fields.Add(new TField { title = "通訊地址" + Environment.NewLine + "郵遞區號", property = "in_add_code", format = "text", width = 10 });
            fields.Add(new TField { title = "通訊地址", property = "in_add", width = 30 });
            fields.Add(new TField { title = "段位", property = "in_degree_label", format = "center", width = 10 });
            fields.Add(new TField { title = "段位證號", property = "in_degree_id", format = "text", width = 15 });
            fields.Add(new TField { title = "教練證證號" + Environment.NewLine + "(若從未講習之人員請填無)", property = "in_exe_a2", format = "text", width = 30 });
            fields.Add(new TField { title = "發證日期", property = "in_exe_a8", format = "yyyy/MM/dd", width = 10 });
            fields.Add(new TField { title = "所屬委員會", property = "committee_short_name", width = 15 });
            fields.Add(new TField { title = "最高學歷", property = "in_education", width = 15 });
            fields.Add(new TField { title = "現任職務" + Environment.NewLine + "(若無，請填無)", property = "in_work_org", width = 20 });
            fields.Add(new TField { title = "公假單位" + Environment.NewLine + "(若無公假單位請填寫無)", property = "in_official_leave_org", width = 25 });
            fields.Add(new TField { title = "所屬道館", property = "in_current_org", width = 15 });
            fields.Add(new TField { title = "報名級別", property = "in_l1", getValue = ClearL1, format = "center", width = 10 });
            fields.Add(new TField { title = "新訓或晉升", property = "in_retraining", width = 15 });
            fields.Add(new TField { title = "飲食", property = "in_lunch", format = "center", width = 10 });
            fields.Add(new TField { title = "是否需要" + Environment.NewLine + "代訂住宿", format = "center", property = "in_booking", width = 10 });
            fields.Add(new TField { title = "代訂住宿" + Environment.NewLine + "同住人姓名", property = "in_booking_note", width = 20 });

            //fields.Add(new TField { title = "教級編號", property = "", width = 20 });
            for(int i = 0; i < cfg.year_count;i++)
            {
                string property = "y" + i;
                fields.Add(new TField { title = "Y"+(i+1), property = property, width = 6 });
            }

            int wsRow = 1;
            int wsCol = 1;

            for (int i = 0; i < fields.Count; i++)
            {
                var field = fields[i];
                ClosedXML.Excel.IXLCell cell = sheet.Cell(wsRow, wsCol + i);
                SetHeadCell(cell, field.title);
            }
            wsRow++;

            //凍結窗格
            sheet.SheetView.FreezeRows(wsRow - 1);

            for(int i = 0; i < table.List.Count; i++)
            {
                var item = table.List[i].Value;
                SetItemCell(sheet, wsRow, wsCol, cfg, item, fields);
                wsRow++;
            }

            for (int i = 0; i < fields.Count; i++)
            {
                //sheet.Column(wsCol + i).AdjustToContents();
                sheet.Column(wsCol + i).Width = fields[i].width;
            }
        }

        private string ClearL1(TConfig cfg, Item item, TField field)
        {
            string value = item.getProperty(field.property, "");
            return value.Replace("跆拳道教練講習", "");
        }

        /// <summary>
        /// 清除 NA mail
        /// </summary>
        private string ClearEmail(TConfig cfg, Item item, TField field)
        {
            string value = item.getProperty(field.property, "");
            if (value.Contains("na@na.n"))
            {
                return "";
            }
            else
            {
                return value;
            }
        }

        /// <summary>
        /// 生日
        /// </summary>
        private string BirthDay(TConfig cfg, Item item, TField field)
        {
            string value = item.getProperty(field.property, "");
            if (value.Contains("1900") || value.Contains("1899"))
            {
                return "";
            }
            else
            {
                return GetDateTimeValue(value, field.format, hours: 8);
            }
        }

        
        private Dictionary<string, TResume> MapYears(TConfig cfg, Item items)
        {
            Dictionary<string, TResume> map = new Dictionary<string, TResume>();

            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string resume_id = item.getProperty("resume_id", "");
                string in_year = item.getProperty("in_year", "");
                int west_year = GetIntVal(in_year);
                int tw_year = west_year - 1911;

                TResume resume = null;
                if (map.ContainsKey(resume_id))
                {
                    resume = map[resume_id];
                }
                else
                {
                    resume = new TResume
                    {
                        resume_id = resume_id,
                        SourceYears = new List<string>(),
                        TargetYears = new List<string>(),
                    };
                    map.Add(resume_id, resume);
                }

                resume.SourceYears.Add(tw_year.ToString());
            }

            foreach(var kv in map)
            {
                var resume = kv.Value;
                var src_list = resume.SourceYears;
                var src_count = resume.SourceYears.Count;
                var tgt_list = resume.TargetYears;
                var tgt_count = cfg.year_count;

                if (src_count <= tgt_count)
                {
                    for (int i = 0; i < tgt_count; i++)
                    {
                        if (i >= src_count)
                        {
                            tgt_list.Add(" ");
                        }
                        else
                        {
                            tgt_list.Add(src_list[i]);
                        }
                    }
                }
                else if (src_count > tgt_count)
                {
                    int min = src_count - tgt_count;
                    for (int i = min; i < src_count; i++)
                    {
                        tgt_list.Add(src_list[i]);
                    }
                }
            }

            return map;
        }

        private Dictionary<string, TTable> MapMUsers(TConfig cfg, Item items)
        {
            Dictionary<string, TTable> map = new Dictionary<string, TTable>();

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item itmMUser = items.getItemByIndex(i);

                TMUser muser = new TMUser
                {
                    in_name = itmMUser.getProperty("in_name", ""),
                    in_l1 = itmMUser.getProperty("in_l1", ""),
                    resume_id = itmMUser.getProperty("resume_id", ""),
                    Value = itmMUser,
                };

                TTable table = null;
                if (map.ContainsKey(muser.in_l1))
                {
                    table = map[muser.in_l1];
                }
                else
                {
                    table = new TTable
                    {
                        Name = muser.in_l1,
                        Value = itmMUser,
                        List = new List<TMUser>(),
                    };
                    map.Add(muser.in_l1, table);
                }
                table.List.Add(muser);
            }
            return map;
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }

            public string meeting_id { get; set; }
            public int year_count { get; set; }

            public Item itmMeeting { get; set; }
            public Item itmMSurveys { get; set; }

            public string in_seminar_type { get; set; }
        }

        private class TTable
        {
            public string Name { get; set; }
            public Item Value { get; set; }
            public List<TMUser> List { get; set; }
        }

        private class TResume
        {
            public string resume_id { get; set; }
            public List<string> SourceYears { get; set; }
            public List<string> TargetYears { get; set; }
        }

        private class TMUser
        {
            public string in_name { get; set; }
            public string in_l1 { get; set; }
            public string resume_id { get; set; }

            public Item Value { get; set; }

            /// <summary>
            /// 服務紀錄列表
            /// </summary>
            public List<Item> itmServices { get; set; }
        }

        private class TExport
        { 
            public string xls_file { get; set; }
            public string xls_url { get; set; }
        }

        private class TField
        {
            public string title { get; set; }
            public string property { get; set; }
            public string css { get; set; }
            public string format { get; set; }
            public int width { get; set; }
            public Func<TConfig, Item, TField, string> getValue { get; set; }
        }

        private TExport GetExport(TConfig cfg)
        {
            string in_title = cfg.itmMeeting.getProperty("in_title", "");

            Item itmPath = GetExcelPath(cfg.CCO, cfg.strMethodName, cfg.inn, "export_path");

            string export_path = itmPath.getProperty("export_path", "").TrimEnd('\\');
            string ext_name = ".xlsx";
            string xls_name = in_title + "_" + "待審核名冊" + "_" + DateTime.Now.ToString("MMdd_HHmmss");
            string xls_file = export_path + "\\" + xls_name + ext_name;
            string xls_url = xls_name + ext_name;

            return new TExport
            {
                xls_file = xls_file,
                xls_url = xls_url,
            };
        }

        private Item GetExcelPath(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string in_name)
        {
            Item itmResult = inn.newItem();

            //Excel 匯出路徑 (from 原創.設定參數)
            string sql = @"
                SELECT 
                    TOP 1 t2.in_name AS 'variable', t1.in_name, t1.in_value 
                FROM 
                    In_Variable_Detail t1 WITH(NOLOCK)
                INNER JOIN 
                    In_Variable t2 WITH(NOLOCK) ON t2.ID = t1.SOURCE_ID 
                WHERE 
                    t2.in_name = N'meeting_excel'  
                    AND t1.in_name = N'{#in_name}'
            ";

            sql = sql.Replace("{#in_name}", in_name);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item item = inn.applySQL(sql);

            if (item.isError())
            {
                throw new Exception("未設定匯出路徑 _# " + in_name);
            }

            itmResult.setProperty("export_path", item.getProperty("in_value", ""));

            return itmResult;
        }


        //設定物件與資料列
        private void SetItemCell(ClosedXML.Excel.IXLWorksheet sheet, int wsRow, int wsCol, TConfig cfg, Item item, List<TField> fields)
        {
            for (int i = 0; i < fields.Count; i++)
            {
                var field = fields[i];

                ClosedXML.Excel.IXLCell cell = sheet.Cell(wsRow, wsCol + i);
                string value = "";
                if (field.getValue != null)
                {
                    value = field.getValue(cfg, item, field);
                }
                else if (field.property != "")
                {
                    value = item.getProperty(field.property, "");
                }
                SetBodyCell(cell, value, field.format);
            }
        }

        //設定資料列
        private void SetBodyCell(ClosedXML.Excel.IXLCell cell, string value, string format)
        {
            switch (format)
            {
                case "center":
                    cell.Value = value;
                    cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    break;

                case "yyyy/MM/dd":
                case "yyyy/MM/dd HH:mm":
                    cell.Value = GetDateTimeValue(value, format);
                    cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    break;

                case "$ #,##0":
                    cell.Value = value;
                    cell.Style.NumberFormat.Format = "$ #,##0";
                    break;

                case "text":
                    cell.Value = "'" + value;
                    cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    break;

                case "text-left":
                    cell.Value = "'" + value;
                    cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Left;
                    break;

                case "tel":
                    if (value.StartsWith("09"))
                    {
                        cell.Value = value.Replace("-", "");
                        cell.Style.NumberFormat.Format = "0000-000-000";
                        cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    }
                    else
                    {
                        cell.Value = "'" + value;
                        cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    }
                    break;

                default:
                    cell.Value = value;
                    break;
            }
            cell.Style.Alignment.Vertical = ClosedXML.Excel.XLAlignmentVerticalValues.Center;
            //自動換行
            cell.Style.Alignment.WrapText = false;

            cell.Style.Border.SetRightBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetTopBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetBottomBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetLeftBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
        }

        ClosedXML.Excel.XLColor head_bg_color = ClosedXML.Excel.XLColor.FromHtml("#295C90");

        //設定標題列
        private void SetHeadCell(ClosedXML.Excel.IXLCell cell, string title)
        {
            cell.Value = title;

            cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
            cell.Style.Alignment.Vertical = ClosedXML.Excel.XLAlignmentVerticalValues.Center;
            //自動換行
            cell.Style.Alignment.WrapText = true;

            cell.Style.Font.Bold = true;
            cell.DataType = ClosedXML.Excel.XLDataType.Text;
            cell.Style.Fill.BackgroundColor = head_bg_color;
            cell.Style.Font.FontColor = ClosedXML.Excel.XLColor.White;

            cell.Style.Border.SetRightBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetTopBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetBottomBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetLeftBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);

        }

        //資料 Cell 上色
        private void SetBodyCellColor(ClosedXML.Excel.IXLWorksheet sheet, int wsRow, int wsCol_s, int wsCol_e, ClosedXML.Excel.XLColor color)
        {
            for (int i = wsCol_s; i <= wsCol_e; i++)
            {
                ClosedXML.Excel.IXLCell cell = sheet.Cell(wsRow, i);
                cell.Style.Fill.BackgroundColor = color;
            }
        }

        /// <summary>
        /// 轉為一階字典清單
        /// </summary>
        private Dictionary<string, List<Item>> MapToDicList1(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item items, string property1)
        {
            Dictionary<string, List<Item>> dicitonary = new Dictionary<string, List<Item>>();

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i); ;
                string key1 = item.getProperty(property1, "");

                List<Item> lv1 = null;
                if (dicitonary.ContainsKey(key1))
                {
                    lv1 = dicitonary[key1];
                }
                else
                {
                    lv1 = new List<Item>();
                    dicitonary.Add(key1, lv1);
                }

                lv1.Add(item);
            }

            return dicitonary;
        }

        /// <summary>
        /// 轉為三階資料字典
        /// </summary>
        private Dictionary<string, Dictionary<string, Dictionary<string, List<Item>>>> MapToDictionary3(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item items, string[] properties)
        {
            Dictionary<string, Dictionary<string, Dictionary<string, List<Item>>>> dicitonary = new Dictionary<string, Dictionary<string, Dictionary<string, List<Item>>>>();

            string property1 = properties[0];
            string property2 = properties[1];
            string property3 = properties[2];

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i); ;
                string key1 = item.getProperty(property1, "");
                string key2 = item.getProperty(property2, "");
                string key3 = item.getProperty(property3, "");

                Dictionary<string, Dictionary<string, List<Item>>> lv1 = null;
                if (dicitonary.ContainsKey(key1))
                {
                    lv1 = dicitonary[key1];
                }
                else
                {
                    lv1 = new Dictionary<string, Dictionary<string, List<Item>>>();
                    dicitonary.Add(key1, lv1);
                }

                Dictionary<string, List<Item>> lv2 = null;
                if (lv1.ContainsKey(key2))
                {
                    lv2 = lv1[key2];
                }
                else
                {
                    lv2 = new Dictionary<string, List<Item>>();
                    lv1.Add(key2, lv2);
                }


                List<Item> lv3 = null;
                if (lv2.ContainsKey(key3))
                {
                    lv3 = lv2[key3];
                }
                else
                {
                    lv3 = new List<Item>();
                    lv2.Add(key3, lv3);
                }

                lv3.Add(item);
            }

            return dicitonary;
        }
        
        /// <summary>
        /// 取得與會者資料
        /// </summary>
        private Item GetMeetingUsers(TConfig cfg)
        {
            string sql = @"
                SELECT 
	                t1.*
	                , t2.pay_bool
	                , t3.id	AS 'resume_id'
                    , ISNULL(t11.in_short_org, t1.in_committee) AS 'committee_short_name'
                FROM 
	                IN_CLA_MEETING_USER t1 WITH(NOLOCK)
                LEFT OUTER JOIN
	                IN_MEETING_PAY t2 WITH(NOLOCK)
	                ON t2.item_number = t1.in_paynumber
                INNER JOIN
	                IN_RESUME t3 WITH(NOLOCK)
	                ON t3.in_sno = t1.in_sno
                LEFT OUTER JOIN
	                IN_RESUME t11 WITH(NOLOCK)
	                ON t11.IN_NAME = t1.IN_COMMITTEE AND t11.in_member_type = 'area_cmt' AND t11.in_member_role = 'sys_9999'
                LEFT OUTER JOIN
	                IN_RESUME t12 WITH(NOLOCK)
	                ON t11.in_sno = t1.in_creator_sno
                WHERE
	                t1.source_id = '{#meeting_id}'
                ORDER BY
	                t1.in_l1
	                , t1.in_degree DESC
	                , t1.in_birth
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item items = cfg.inn.applySQL(sql);

            if (items.isError() || items.getItemCount() <= 0)
            {
                throw new Exception("查無與會者資料");
            }

            return items;
        }

        /// <summary>
        /// 取得問項資料
        /// </summary>
        private Item GetMeetingSuverys(TConfig cfg)
        {
            string sql = @"
                SELECT 
	                in_questions
	                , in_question_type
	                , in_property
                FROM 
	                IN_CLA_MEETING_SURVEYS t1 WITH(NOLOCK)
                INNER JOIN
	                IN_CLA_SURVEY t2 WITH(NOLOCK)
	                ON t2.id = t1.related_id
                WHERE
	                t1.source_id = '{#meeting_id}'
	                AND t1.in_surveytype = '1'
                ORDER BY
	                t1.sort_order
            "; 
            
            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item items = cfg.inn.applySQL(sql);

            if (items.isError() || items.getItemCount() <= 0)
            {
                throw new Exception("查無問項資料");
            }

            return items;
        }

        /// <summary>
        /// 取得服務年度
        /// </summary>
        private Item GetResumeYears(TConfig cfg)
        {
            string sql = @"
                SELECT
	                t2.id AS 'resume_id'
	                , t3.in_year
                FROM
	                IN_CLA_MEETING_USER t1 WITH(NOLOCK)
                INNER JOIN
	                IN_RESUME t2 WITH(NOLOCK)
	                ON t2.in_sno = t1.in_sno
                INNER JOIN
	                IN_RESUME_YEARS t3 WITH(NOLOCK)
	                ON t3.source_id = t2.id
                WHERE
	                t1.source_id = '{#meeting_id}'
	                AND t3.in_type = '{#in_seminar_type}'
                ORDER BY
                    t2.id
                    , t3.in_year
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_seminar_type}", cfg.in_seminar_type);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item items = cfg.inn.applySQL(sql);

            if (items.isError() || items.getItemCount() <= 0)
            {
                throw new Exception("查無問項資料");
            }

            return items;
        }

        private string GetDateTimeValue(string value, string format, int hours = 0)
        {
            if (value == "") return "";

            DateTime dt = DateTime.Now;
            if (DateTime.TryParse(value.Replace("/", "-"), out dt))
            {
                return dt.AddHours(hours).ToString(format);
            }
            else
            {
                return value;
            }
        }

        private int GetIntVal(string value, int def = 0)
        {
            int result = def;
            Int32.TryParse(value, out result);
            return result;
        }
    }
}