﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class In_Cla_Promotion_List_Verify : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 協會晉段審核
                日誌:
                    - 2022.01.18 移除繳費單關聯-協會審核 (lina)
                    - 2021.01.08 協會審核(Alan)
                    - 2021.09.27(David)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]In_Cla_Promotion_List_Verify";

            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            string aml = "";
            string sql = "";
            string strError = "";
            string strUserId = inn.getUserID();

            _InnH.AddLog(strMethodName, "MethodSteps");

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                mode = itmR.getProperty("mode", ""),
                s_type = itmR.getProperty("s_type", ""),//測試-顯示未繳費的單子
                method_type = "",
            };

            var col_list = new List<string>
            {
                "in_title",
                "in_address",
                "in_customer",
                "in_url",
                "in_register_url",
                "in_need_receipt",
                "in_meeting_type",
                "in_echelon",
                "in_decree",
                "in_date_s",
                "in_date_e",
                "in_level",
                "in_seminar_type",
            };

            var cols = string.Join(", ", col_list);

            aml = "<AML>" +
                "<Item type='in_cla_meeting' action='get' id='" + cfg.meeting_id + "' select='" + cols + "'>" +
                "</Item></AML>";
            cfg.itmMeeting = inn.applyAML(aml);

            cfg.in_title = cfg.itmMeeting.getProperty("in_title", "");
            cfg.in_address = cfg.itmMeeting.getProperty("in_address", "");
            cfg.in_customer = cfg.itmMeeting.getProperty("in_customer", "");
            cfg.in_url = cfg.itmMeeting.getProperty("in_url", "");
            cfg.in_register_url = cfg.itmMeeting.getProperty("in_register_url", "");
            cfg.in_meeting_type = cfg.itmMeeting.getProperty("in_meeting_type", "");
            cfg.in_level = cfg.itmMeeting.getProperty("in_level", "");
            cfg.in_need_receipt = cfg.itmMeeting.getProperty("in_need_receipt", "");
            cfg.in_echelon = cfg.itmMeeting.getProperty("in_echelon", "");
            cfg.in_decree = cfg.itmMeeting.getProperty("in_decree", "");
            cfg.in_date_s = cfg.itmMeeting.getProperty("in_date_s", "");
            cfg.in_date_e = cfg.itmMeeting.getProperty("in_date_e", "");
            cfg.in_seminar_type = cfg.itmMeeting.getProperty("in_seminar_type", "");

            //取得登入者資訊
            sql = "SELECT *";
            sql += " FROM In_Resume WITH(NOLOCK)";
            sql += " WHERE in_user_id = '" + cfg.strUserId + "'";
            cfg.loginResume = inn.applySQL(sql);
            cfg.login_resume_id = cfg.loginResume.getProperty("id", "");
            cfg.login_group = cfg.loginResume.getProperty("in_group", "");//所屬單位
            cfg.login_sno = cfg.loginResume.getProperty("in_sno", "");//協助報名者身分證號
            cfg.login_name = cfg.loginResume.getProperty("in_name", "");//
            cfg.login_member_type = cfg.loginResume.getProperty("in_member_type", "");
            cfg.login_member_role = cfg.loginResume.getProperty("in_member_role", "");

            Item itmPermit = inn.applyMethod("In_CheckIdentity", "<method>" + strMethodName + "</method><code>ALL</code>");
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmPermit.dom.InnerXml);
            cfg.isMeetingAdmin = itmPermit.getProperty("isMeetingAdmin", "") == "1";
            cfg.isCommittee = itmPermit.getProperty("isCommittee", "") == "1";
            cfg.isGymOwner = itmPermit.getProperty("isGymOwner", "") == "1";
            cfg.isGymAssistant = itmPermit.getProperty("isGymAssistant", "") == "1";

            //是否為共同講師
            bool open = GetUserResumeListStatus(cfg.inn, cfg.meeting_id, cfg.strUserId);
            if (open) cfg.isMeetingAdmin = true;

            cfg.can_review = cfg.isMeetingAdmin;

            //晉段
            if (!cfg.can_review && cfg.in_meeting_type == "degree")
            {
                Item itmRoleResult = inn.applyMethod("In_Association_Role"
                    , "<in_sno>" + cfg.login_sno + "</in_sno>"
                    + "<idt_names>ACT_ASC_Degree</idt_names>");

                cfg.can_review = itmRoleResult.getProperty("inn_result", "") == "1";
            }

            if (!cfg.can_review)
            {
                itmR.setProperty("permission_type", "1");
                itmR.setProperty("permission_message", "您無權限瀏覽此頁面");
                return itmR;
            }


            if (cfg.in_meeting_type == "degree")
            {
                //晉段功能
                if (cfg.mode == "xls")
                {
                    string committee_sno = itmR.getProperty("committee_sno", "");
                    ExportDegreeXlsx(cfg, itmR, committee_sno);
                }
                else
                {
                    //協會審查晉段人員清單
                    AppendPromotionView(cfg, itmR);
                }
            }
            else if (cfg.in_meeting_type == "seminar")
            {
                //講習功能
                if (cfg.mode == "xlsSeminar")
                {
                    ExportSeminar(cfg, itmR);
                }
                else
                {
                    //協會審查講習人員清單
                    AppendSeminarViev(cfg, itmR);
                }
            }

            itmR.setProperty("meeting_name", cfg.in_title);
            itmR.setProperty("id", cfg.meeting_id);
            itmR.setProperty("player_group", cfg.login_group);
            itmR.setProperty("method_type", cfg.method_type);
            itmR.setProperty("inn_meeting_admin", cfg.isMeetingAdmin ? "1" : "0");
            itmR.setProperty("inn_excel_method", cfg.method_type);


            itmR.setProperty("in_title", cfg.in_title);
            itmR.setProperty("in_url", cfg.in_url);
            itmR.setProperty("in_register_url", cfg.in_register_url);
            itmR.setProperty("in_group", cfg.login_group);

            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

            return itmR;
        }

        //取得共同講師 (判斷現在的登入者是否為此課程的共同講師)
        private bool GetUserResumeListStatus(Innovator inn, string meeting_id, string strUserId)
        {
            string sql = @"
                SELECT
                    t2.id
                FROM 
                    In_Cla_Meeting_Resumelist t1 WITH(NOLOCK) 
                INNER JOIN 
                    IN_RESUME t2 WITH(NOLOCK) 
                    ON t2.id = t1.related_id    
                WHERE 
                    t1.source_id = '{#meeting_id}' 
                    AND t2.in_user_id = '{#in_user_id}';    
            ";

            sql = sql.Replace("{#meeting_id}", meeting_id)
                .Replace("{#in_user_id}", strUserId);

            Item item = inn.applySQL(sql);

            if (item.getResult() != "")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //匯出晉段人員名冊
        private void ExportDegreeXlsx(TConfig cfg, Item itmReturn, string committee_sno)
        {
            string committee_condition = committee_sno != ""
                ? "AND t2.in_sno = '" + committee_sno + "'"
                : "";

            string sql = @"
                SELECT 
                    DISTINCT t2.in_sno
                    , t1.in_committee AS 'in_name'
                FROM 
                    IN_CLA_MEETING_USER t1 WITH(NOLOCK) 
                LEFT OUTER JOIN
                    IN_RESUME t2 WITH(NOLOCK)
                    ON t2.in_name = t1.in_committee 
                    AND t2.in_member_type = 'area_cmt'
                    AND t2.in_name <> N'無'
                LEFT OUTER JOIN
                    IN_MEETING_PAY t3 WITH(NOLOCK)
                    ON t3.item_number = t1.in_paynumber
                WHERE 
                    t1.source_id = '{#meeting_id}'
                    {#committee_condition}
                    AND ISNULL(t1.in_verify_result, '') = '1'
                    AND t1.in_ass_ver_time IS NOT NULL
                    AND ISNULL(t1.in_ass_ver_result, '') = '1' 
                ORDER BY
                    t2.in_sno
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#committee_condition}", committee_condition);

            Item itmCommittees = cfg.inn.applySQL(sql);
            if (itmCommittees.isError() || itmCommittees.getItemCount() < 0)
            {
                throw new Exception("目前無資料，無法匯出");
            }


            string variable_name = "promotion_path";
            Item itmXls = cfg.inn.applyMethod("In_Variable_File", "<in_name>" + variable_name + "</in_name>");

            string export_path = itmXls.getProperty("export_path", "").TrimEnd('\\');
            string template_path = itmXls.getProperty("template_path", "");

            ClosedXML.Excel.XLWorkbook workbook = new ClosedXML.Excel.XLWorkbook(template_path);
            ClosedXML.Excel.IXLWorksheet sheetTemplate = workbook.Worksheet(1);

            int count = itmCommittees.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item itmCommittee = itmCommittees.getItemByIndex(i);
                string in_sno = itmCommittee.getProperty("in_sno", "");
                string in_name = itmCommittee.getProperty("in_name", "");

                //複製樣板
                sheetTemplate.CopyTo(in_name);
                //取得新 Sheet
                ClosedXML.Excel.IXLWorksheet sheet = workbook.Worksheet(2 + i);

                AppendDegreeSheet(workbook, sheet, cfg, in_sno, in_name);
            }

            //移除樣板
            sheetTemplate.Delete();

            Item itmPath = GetExcelPath(cfg.CCO, cfg.strMethodName, cfg.inn, "export_path");

            string ext_name = ".xlsx";

            string xls_name = count == 1
                ? "晉段名冊_" + itmCommittees.getItemByIndex(0).getProperty("in_name", "") + "_" + DateTime.Now.ToString("MMdd_HHmmss")
                : "晉段名冊_" + cfg.in_title + "_" + DateTime.Now.ToString("MMdd_HHmmss");

            string xls_file = export_path + "\\" + xls_name + ext_name;
            string xls_url = xls_name + ext_name;

            workbook.SaveAs(xls_file);

            itmReturn.setProperty("xls_name", xls_url);
        }

        private void AppendDegreeSheet(ClosedXML.Excel.XLWorkbook workbook, ClosedXML.Excel.IXLWorksheet sheet, TConfig cfg, string committee_sno, string committee_name)
        {
            string sql = @"
                SELECT 
                    t2.id
                    , t2.in_name_num
                    , t2.in_sno
                    , t2.in_ass_ver_memo
                    , t2.in_l1
                    , t2.in_committee
                    , t2.in_name
                    , t2.in_en_name
                    , t2.in_sno_display
                    , dateadd(hour, 8, t2.in_birth) AS 'in_birth'
                    , t2.in_gender
                    , t2.in_guardian
                    , t2.in_degree_id
                    , t2.in_gl_degree_id 
                    , t4.id AS 'rid'
                FROM 
                    IN_CLA_MEETING t1 WITH (NOLOCK)
                INNER JOIN 
                    IN_CLA_MEETING_USER t2 WITH (NOLOCK)
                    ON t2.source_id = t1.id
                INNER JOIN 
                    IN_RESUME t4 WITH (NOLOCK)
                    ON t4.in_sno = t2.in_sno
                LEFT OUTER JOIN
                    IN_MEETING_PAY t11 WITH(NOLOCK)
                    ON t11.item_number = t2.in_paynumber
                INNER JOIN
                    IN_RESUME AS t12 WITH (NOLOCK)
                    ON t12.in_name = t2.in_committee 
                    AND t12.in_member_type = 'area_cmt' 
                    AND t12.in_name <> N'無'
                WHERE 
                    t1.id = '{#meeting_id}' 
                    AND t1.in_meeting_type = 'degree'
                    AND ISNULL(t2.in_verify_result, '') = '1' 
                    AND t2.in_ass_ver_time IS NOT NULL
                    AND ISNULL(t2.in_ass_ver_result, '') = '1' 
                    AND t12.in_sno = N'{#committee_sno}'
                ORDER BY 
                    t2.in_degree DESC
                    , t2.in_birth
            ";

            sql = sql.Replace("{#committee_sno}", committee_sno)
                      .Replace("{#meeting_id}", cfg.meeting_id);

            Item items = cfg.inn.applySQL(sql);

            if (items.isError() || items.getItemCount() < 0)
            {
                //throw new Exception("目前無資料，無法匯出");
                return;
            }

            sql = @"
                SELECT 
                    t1.in_degree
                    , t1.in_l1
                    , count(*) as qty
                FROM 
                    IN_CLA_MEETING_USER AS t1 WITH (NOLOCK)
                LEFT OUTER JOIN 
                    IN_MEETING_PAY t2 WITH (NOLOCK)
                    ON t2.item_number = t1.in_paynumber
                INNER JOIN
                    IN_RESUME t3 WITH (NOLOCK)
                    ON t3.in_name = t2.in_committee 
                    AND t3.in_member_type = 'area_cmt' 
                    AND t3.in_name <> N'無'
                WHERE 
                    t1.source_id = '{#meeting_id}' 
                    AND ISNULL(t1.in_verify_result, '') = '1'
                    AND t1.in_ass_ver_time IS NOT NULL
                    AND ISNULL(t1.in_ass_ver_result, '') = '1' 
                    AND t1.in_l1 LIKE '%段%' 
                    AND t3.in_sno = N'{#committee_sno}'
                GROUP BY 
                    t1.in_degree 
                    , t1.in_l1
                ORDER BY 
                    t1.in_degree DESC
					, t1.in_l1
            ";

            sql = sql.Replace("{#committee_sno}", committee_sno)
                      .Replace("{#meeting_id}", cfg.meeting_id);

            Item prolists = cfg.inn.applySQL(sql);
            string pro_list = "";
            int totalCount = 0;

            for (int i = 0; i < prolists.getItemCount(); i++)
            {
                Item prolist = prolists.getItemByIndex(i);
                pro_list += "  ";
                pro_list += prolist.getProperty("in_l1", "");
                pro_list += prolist.getProperty("qty", "") + "張";
                totalCount += Int32.Parse(prolist.getProperty("qty", ""));
            }

            pro_list += "    ";
            pro_list += "共計";
            pro_list += totalCount.ToString() + "張";


            ClosedXML.Excel.IXLCell foundCell;

            string[] ClassFields = { "in_title", "pro_list" };

            Item itmTitle = cfg.inn.newItem("title");

            string in_title = "中華民國跆拳道協會" + committee_name + "第" + cfg.in_echelon + "梯次晉段名冊";

            itmTitle.setProperty("in_title", in_title);
            itmTitle.setProperty("pro_list", pro_list);

            for (int i = 0; i < ClassFields.Length; i++)
            {
                foundCell = sheet.Search("{#" + ClassFields[i] + "}", CompareOptions.OrdinalIgnoreCase).FirstOrDefault();
                if (foundCell != null)
                {
                    string FieldValue = itmTitle.getProperty(ClassFields[i], "");
                    SetCellTextValue(ref foundCell, FieldValue);
                }
            }

            List<TField> fields = new List<TField>();
            fields.Add(new TField { property = "no", title = "編號", format = "center" });
            fields.Add(new TField { property = "in_l1", title = "段位", format = "center" });
            fields.Add(new TField { property = "in_name", title = "中文姓名", format = "center" });
            fields.Add(new TField { property = "in_en_name", title = "英文姓名", format = "center" });
            fields.Add(new TField { property = "in_sno", title = "身分證字號", format = "center" });
            fields.Add(new TField { property = "in_birth", title = "出生日期", format = "yyyy/MM/dd" });
            fields.Add(new TField { property = "in_gender", title = "性別", format = "center" });
            fields.Add(new TField { property = "in_guardian", title = "現任教練", format = "center" });
            fields.Add(new TField { property = "in_degree_id", title = "國內證號", format = "center" });
            fields.Add(new TField { property = "in_gl_degree_id", title = "國際證號", format = "center" });
            fields.Add(new TField { property = "in_note", title = "備註", format = "" });

            int wsRow = 3;
            int wsCol = 1;
            int field_count = fields.Count;

            for (int i = 0; i < field_count; i++)
            {
                TField field = fields[i];
                ClosedXML.Excel.IXLCell cell = sheet.Cell(wsRow, wsCol + i);
                SetHeadCell(cell, field.title);
            }
            wsRow++;

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_name_num = item.getProperty("in_name_num", "");//學員編號
                item.setProperty("no", (i + 1).ToString());
                SetItemCell(sheet, wsRow, wsCol, item, fields);

                wsRow++;
            }
        }

        //匯出講習人員名冊
        private void ExportSeminar(TConfig cfg, Item itmReturn)
        {
            string in_photokey = cfg.in_title.Contains("教練")
                ? "in_photo8" + cfg.in_level
                : "in_photo7" + cfg.in_level; //裁判

            string sql = @"
                SELECT 
                    T1.id
                    , T1.in_name_num
                    , T1.in_name
                    , T1.in_en_name
                    , T1.in_sno
                    , T1.in_gender
                    , dateadd(hour, 8, T1.in_birth) AS 'in_birth'
                    , T1.in_tel
                    , T1.in_add 
                    , T1.in_exe_a1
                    , T1.in_exe_a2
                    , T1.in_exe_a5
                    , T1.in_exe_a6
                    , T1.in_education
                    , T1.in_title
                    , T1.in_guardian
                    , T1.in_l1
                    , T1.in_degree_id
                    , T1.in_gl_degree_id 
                    , T1.in_sno_display
                    , T1.in_ass_ver_memo
                    , T1.in_note
                    , T2.id AS 'rid'
                    , ISNULL(T1.in_exe_a2 , T3.in_certificate_no) AS 'in_certificate_no'
                    , dateadd(hour, 8, T3.in_effective_start)     AS 'in_effective_start'
                FROM 
                    IN_CLA_MEETING_USER AS T1 WITH (NOLOCK)
                JOIN IN_RESUME AS T2 WITH (NOLOCK)
                    ON T1.in_sno = T2.in_sno
                LEFT OUTER JOIN
                    IN_RESUME_CERTIFICATE T3 WITH(NOLOCK)
                    ON T3.source_id = t2.id
                    AND T3.in_photokey = '{#in_photokey}'
                WHERE 
                    T1.SOURCE_ID = N'{#meeting_id}'  
                    --AND T1.in_paytime IS NOT NULL
                    --AND T1.in_ass_ver_time IS NOT NULL
                    --AND ISNULL(T1.in_ass_ver_result, '') = '1' 
                ORDER BY 
                    T1.in_name_num ASC
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_photokey}", in_photokey);

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item items = cfg.inn.applySQL(sql);

            if (items.isError() || items.getItemCount() < 0)
            {
                throw new Exception("目前無資料，無法匯出");
            }

            string aml = "";
            //開始整理EXCEL Template
            aml = "<AML>" +
                "<Item type='In_Variable' action='get'>" +
                "<in_name>meeting_excel</in_name>" +
                "<Relationships>" +
                "<Item type='In_Variable_Detail' action='get'/>" +
                "</Relationships>" +
                "</Item></AML>";
            Item Vairable = cfg.inn.applyAML(aml);

            string Template_Path = "";
            string Export_Path = "";
            for (int i = 0; i < Vairable.getRelationships("In_Variable_Detail").getItemCount(); i++)
            {
                Item In_Variable_Detail = Vairable.getRelationships("In_Variable_Detail").getItemByIndex(i);
                if (In_Variable_Detail.getProperty("in_name", "") == "seminar_path")
                    Template_Path = In_Variable_Detail.getProperty("in_value", "");

                if (In_Variable_Detail.getProperty("in_name", "") == "export_path")
                    Export_Path = In_Variable_Detail.getProperty("in_value", "");
                if (!Export_Path.EndsWith(@"\"))
                    Export_Path = Export_Path + @"\";
            }

            ClosedXML.Excel.XLWorkbook workbook = new ClosedXML.Excel.XLWorkbook(Template_Path);
            ClosedXML.Excel.IXLWorksheet sheet = workbook.Worksheet(1);

            ClosedXML.Excel.IXLCell foundCell;

            List<TField> fields = new List<TField>();
            fields.Add(new TField { property = "no", title = "編號", format = "center" });
            fields.Add(new TField { property = "in_name", title = "中文姓名", format = "center" });
            fields.Add(new TField { property = "in_gender", title = "性別", format = "center" });
            fields.Add(new TField { property = "in_birth", title = "出生日期", format = "yyyy/MM/dd" });
            fields.Add(new TField { property = "in_sno", title = "身分證字號", format = "center" });
            fields.Add(new TField { property = "in_e1", title = "是否合格", format = "center" }); //暫無
            fields.Add(new TField { property = "in_effective_start", title = "原始發照日", format = "yyyy/MM/dd" }); //證照列表
            fields.Add(new TField { property = "in_certificate_no", title = "證書字號", format = "text" }); //證照列表 OR in_exe_a5
            fields.Add(new TField { property = "in_education", title = "最高學歷", format = "" });
            fields.Add(new TField { property = "in_title", title = "現任職務", format = "" });
            fields.Add(new TField { property = "in_tel", title = "手機電話", format = "tel" });
            fields.Add(new TField { property = "in_shipping_add", title = "通訊地址", format = "text-left" }); //郵遞區號+證件寄送地址
            fields.Add(new TField { property = "in_note", title = "備註", format = "" });

            string level = cfg.in_level;
            if (!level.Contains("級"))
            {
                level += "級";
            }

            DateTime sTime = Convert.ToDateTime(cfg.in_date_s);
            DateTime eTime = Convert.ToDateTime(cfg.in_date_e);

            string semTime = sTime.ToString("MM/dd") + "~" + eTime.ToString("MM/dd");
            string semAdd = cfg.in_address;

            ClosedXML.Excel.IXLCell cell = sheet.Cell(4, 3);
            SetBodyCell(cell, cfg.in_customer, "center");         //單位名稱

            cell = sheet.Cell(2, 2);
            SetBodyCell(cell, cfg.in_title + "資料", "title");  //表題

            cell = sheet.Cell(4, 4);
            SetBodyCell(cell, "跆拳道", "center");           //運動種類
            cell = sheet.Cell(4, 6);
            SetBodyCell(cell, level, "center");             //講習會級別
            cell = sheet.Cell(4, 7);
            SetBodyCell(cell, semTime, "center");             //講習會時間
            cell = sheet.Cell(4, 8);
            SetBodyCell(cell, semAdd, "center");            //講習會地點
            cell = sheet.Cell(4, 11);
            SetBodyCell(cell, cfg.in_decree, "center");     //核備文號




            int wsRow = 7;
            int wsCol = 2;

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string rev_add = ClearEmpty(item.getProperty("in_exe_a5", ""));
                string rev_code = ClearEmpty(item.getProperty("in_exe_a6", ""));
                string in_add = ClearEmpty(item.getProperty("in_add", ""));
                string in_resident_add = ClearEmpty(item.getProperty("in_resident_add", ""));
                string in_name_num = ClearEmpty(item.getProperty("in_name_num", ""));//學員編號


                if (in_add == "") in_add = in_resident_add;

                string in_shipping_add = rev_add == ""
                    ? in_add
                    : rev_code + rev_add;

                // item.setProperty("no", (i + 1).ToString());
                item.setProperty("no", in_name_num);
                item.setProperty("in_shipping_add", in_shipping_add);
                SetItemCell(sheet, wsRow, wsCol, item, fields);

                wsRow++;
            }
            cell = sheet.Cell(4, 9);
            SetBodyCell(cell, count.ToString(), "center");     //參加人數
            Item itmPath = GetExcelPath(cfg.CCO, cfg.strMethodName, cfg.inn, "export_path");

            string export_path = itmPath.getProperty("export_path", "").TrimEnd('\\');
            string ext_name = ".xlsx";
            string xls_name = cfg.in_title + "_講習資料_" + DateTime.Now.ToString("MMdd_HHmmss");
            string xls_file = export_path + "\\" + xls_name + ext_name;
            string xls_url = xls_name + ext_name;

            workbook.SaveAs(xls_file);

            itmReturn.setProperty("xls_name", xls_url);
        }

        private string ClearEmpty(string value)
        {
            if (value == "無") return "";
            return value;
        }

        //審查晉段人員清單
        private void AppendPromotionView(TConfig cfg, Item itmReturn)
        {
            var items = GetDegreeMUsers(cfg);
            var list = MapCommittee(cfg, items);
            var contents = GetDegreeContents(cfg, list);
            itmReturn.setProperty("inn_verify_list", contents);
        }

        private string GetDegreeContents(TConfig cfg, List<TCommittee> list)
        {
            List<TField> fields = new List<TField>();
            fields.Add(new TField { property = "in_name", title = "中文姓名", css = "text-left" });
            fields.Add(new TField { property = "in_en_name", title = "英文姓名", css = "text-left" });
            fields.Add(new TField { property = "in_sno_display", title = "身分證號", css = "text-left" });
            fields.Add(new TField { property = "in_birth", title = "出生日", css = "text-left" });
            fields.Add(new TField { property = "in_gender", title = "性別", css = "text-left" });
            fields.Add(new TField { property = "in_guardian", title = "現任教練", css = "text-left" });
            fields.Add(new TField { property = "in_l1", title = "申請", css = "text-left" });
            fields.Add(new TField { property = "in_degree_id", title = "國內證號", css = "text-left" });
            fields.Add(new TField { property = "in_gl_degree_id", title = "國際證號", css = "text-left" });

            StringBuilder tabs = new StringBuilder();
            StringBuilder body = new StringBuilder();

            for (int i = 0; i < list.Count; i++)
            {
                string active = i == 0 ? "active" : "";
                string active2 = i == 0 ? "active in" : "";

                var cmt = list[i];
                tabs.Append("<li id='" + cmt.ctrl_li_id + "' class='nav-item " + active + "'>");
                tabs.Append("  <a id='" + cmt.ctrl_a_id + "'");
                tabs.Append("	href='#" + cmt.ctrl_pn_id + "'");
                tabs.Append("   class='nav-link' role='tab' data-toggle='pill'");
                tabs.Append("	aria-controls='pills-home' aria-selected='true' onclick='StoreTab_Click(this)'>");
                tabs.Append("	" + cmt.city);
                tabs.Append("  </a>");
                tabs.Append("</li>");

                body.Append("<div class='tab-pane fade " + active2 + "' id='" + cmt.ctrl_pn_id + "' role='tabpanel' >");
                body.Append("  <div class='container bg-white-hw'>");
                foreach (var dbox in cmt.DegreeList)
                {
                    AppendDegreeSubBox(cfg, cmt, dbox, fields, body);
                }

                AppendDegreeSubBox(cfg, cmt, cmt.AcsAgreeList, fields, body);
                AppendDegreeSubBox(cfg, cmt, cmt.AcsRejectList, fields, body);
                AppendDegreeSubBox(cfg, cmt, cmt.UnVerList, fields, body);
                AppendDegreeSubBox(cfg, cmt, cmt.RejectList, fields, body);

                body.Append("  </div>");
                body.Append("</div>");
            }

            StringBuilder builder = new StringBuilder();
            builder.Append("<ul class='nav nav-pills' style='justify-content: start ;' id='pills-tab' role='tablist'>");
            builder.Append(tabs);
            builder.Append("</ul>");

            builder.Append("<div class='tab-content' id='pills-tabContent'>");
            builder.Append(body);
            builder.Append("</div>");
            return builder.ToString();
        }

        private void AppendDegreeSubBox(TConfig cfg, TCommittee cmt, TBox bx, List<TField> fields, StringBuilder container)
        {
            if (bx.itmMUsers.Count <= 0) return;

            var table = AppendDegreeTable(cfg, cmt, bx, fields);

            container.Append("<div class='main clearfix'>");

            //Box-Header
            container.Append("  <div class='box-header'>");
            container.Append("    <h4 class='box-header with-border'>");
            container.Append("      " + bx.title);
            container.Append("      <small>(人數：" + bx.itmMUsers.Count + "名)</small>");
            if (bx.is_agree)
            {
                container.Append(" <button class='btn btn-primary' onclick=\"ExportXls_Click('" + cmt.sno + "', 'ass')\">匯出晉段名冊</button>");
            }
            container.Append("    </h4>");

            container.Append("    <div class='box-tools pull-right'>");
            container.Append("      <button type='button' class='btn btn-box-tool fold_type' style='width: 40px; height: 20px;'>");
            container.Append("        <a class='accordion-toggle box-title form-box-title fa fa-minus' style='font-size: 25px;'");
            container.Append("          data-toggle='collapse' data-parent='#accordion'");
            container.Append("          href='#" + bx.ctrl_fold_id + "'></a>");
            container.Append("      </button>");
            container.Append("    </div>");
            container.Append("  </div>");

            //Box-Body
            container.Append("  <div id='" + bx.ctrl_fold_id + "' class='collapse in'>");
            container.Append(table);
            container.Append("  </div>");
            container.Append("</div>");
        }

        private StringBuilder AppendDegreeTable(TConfig cfg, TCommittee cmt, TBox bx, List<TField> fields)
        {
            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            head.AppendLine("<thead>");
            head.AppendLine("<tr>");
            foreach (var field in fields)
            {
                if (field.hide)
                {
                    head.AppendLine("<th class='mailbox-name' data-field='" + field.property + "' data-sortable='true' data-visible='false'>" + field.title + "</th>");
                }
                else
                {
                    head.AppendLine("<th class='mailbox-name' data-field='" + field.property + "' data-sortable='true'>" + field.title + "</th>");
                }
            }

            if (bx.is_reject)
            {
                head.AppendLine("<th class='mailbox-name' data-field='inn_ver_memo' data-sortable='true'>審核不通過</th>");
            }

            if (bx.can_verify)
            {
                head.AppendLine("<th class='mailbox-name' data-field='功能' data-sortable='false'>功能</th>");
            }

            head.AppendLine("</tr>");
            head.AppendLine("</thead>");

            int count = bx.itmMUsers.Count;

            body.AppendLine("<tbody>");
            for (int i = 0; i < count; i++)
            {
                Item item = bx.itmMUsers[i];
                string id = item.getProperty("id", "");
                string in_birth = item.getProperty("in_birth", "");
                string in_name = item.getProperty("in_name", "");
                string in_sno_display = item.getProperty("in_sno_display", "");
                string rid = item.getProperty("resume_id", "");
                string nid = item.getProperty("nid", "");
                string in_pay_amount = item.getProperty("in_pay_amount", "0");

                item.setProperty("in_birth", DateTimeStr(in_birth));
                item.setProperty("in_name", "<a onclick=\"show_PhotoUploadModal('" + id + "', '" + in_sno_display + "', 'view')\"><i class='fa fa-picture-o in_in_photo1'>" + in_name + "</i></a>");

                body.AppendLine("<tr data-id='" + id + "' data-rid='" + rid + "' data-amount='" + in_pay_amount + "' data-nid='" + nid + "'>");

                foreach (var field in fields)
                {
                    body.AppendLine("<td class='" + field.css + "' data-field='" + field.property + "' >" + field.GetStr(item) + "</td>");
                }

                if (bx.is_reject)
                {
                    body.AppendLine("<td class='text-left' data-field='inn_ver_memo' >" + item.getProperty("inn_ver_memo", "") + "</td>");
                }

                if (bx.can_verify)
                {
                    body.AppendLine("  <td>");
                    body.AppendLine("    <button class='btn btn-primary' onclick=\"show_verifyModal(this,'degree')\">審核</button>");
                    body.AppendLine("    <button class='btn btn-danger' onclick=\"cancelUser(this,'degree')\">刪除</button>");
                    body.AppendLine("  </td>");

                }

                body.AppendLine("</tr>");
            }

            body.AppendLine("</tbody>");

            string table_name = bx.ctrl_table_id;

            StringBuilder builder = new StringBuilder();
            builder.AppendLine(GetTableAttribute(table_name));
            builder.Append(head);
            builder.Append(body);
            builder.AppendLine("</table>");
            builder.AppendLine("<script>");
            builder.AppendLine("$('#" + table_name + "').bootstrapTable();");
            builder.AppendLine("if ($(window).width() <= 768) { $('#" + table_name + "').bootstrapTable('toggleView'); }");
            builder.AppendLine("</script>");

            return builder;
        }

        private List<TCommittee> MapCommittee(TConfig cfg, Item items)
        {
            var result = new List<TCommittee>();

            var count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var in_committee = item.getProperty("in_committee", "");
                var committee_sno = item.getProperty("committee_sno", "");

                var in_verify_result = item.getProperty("in_verify_result", "");
                var in_verify_memo = item.getProperty("in_verify_memo", "");

                var in_ass_ver_result = item.getProperty("in_ass_ver_result", "");
                var in_ass_ver_memo = item.getProperty("in_ass_ver_memo", "");

                var cmt = result.Find(x => x.sno == committee_sno);
                if (cmt == null)
                {
                    cmt = new TCommittee
                    {
                        sno = committee_sno,
                        name = item.getProperty("in_committee", ""),
                        short_name = item.getProperty("committee_shor_name", ""),
                        city = item.getProperty("committee_city", ""),

                        DegreeList = new List<TBox>(),
                        UnVerList = new TBox { code = "cmt_unver", title = in_committee + "-" + "未進行審核", itmMUsers = new List<Item>(), can_verify = false },
                        RejectList = new TBox { code = "cmt_reject", title = in_committee + "-" + "審核不通過", itmMUsers = new List<Item>(), can_verify = false, is_reject = true },

                        AcsAgreeList = new TBox { code = "acs_agree", title = in_committee + "-" + "協會審核通過", itmMUsers = new List<Item>(), can_verify = true, is_agree = true },
                        AcsRejectList = new TBox { code = "acs_reject", title = in_committee + "-" + "協會審核不通過", itmMUsers = new List<Item>(), can_verify = true, is_reject = true },
                    };

                    cmt.ctrl_li_id = "cmt_li_" + cmt.sno;
                    cmt.ctrl_pn_id = "cmt_pn_" + cmt.sno;
                    cmt.ctrl_a_id = "cmt_a_" + cmt.sno;

                    BindDegreeCtrlId(cmt, cmt.UnVerList);
                    BindDegreeCtrlId(cmt, cmt.RejectList);
                    BindDegreeCtrlId(cmt, cmt.AcsAgreeList);
                    BindDegreeCtrlId(cmt, cmt.AcsRejectList);

                    result.Add(cmt);
                }

                if (in_ass_ver_result == "")
                {
                    //協會未審核
                    if (in_verify_result == "1")
                    {
                        //委員會審核-通過
                        AppendDegreeWaiting(cfg, cmt, item);
                    }
                    else if (in_verify_result == "0")
                    {
                        //委員會審核-未通過
                        cmt.RejectList.itmMUsers.Add(item);
                        item.setProperty("inn_ver_memo", in_verify_memo);
                    }
                    else
                    {
                        //委員會審核-未審核
                        cmt.UnVerList.itmMUsers.Add(item);
                    }
                }
                else if (in_ass_ver_result == "1")
                {
                    //協會審核-通過
                    cmt.AcsAgreeList.itmMUsers.Add(item);
                }
                else if (in_ass_ver_result == "0")
                {
                    //協會審核-未通過
                    cmt.AcsRejectList.itmMUsers.Add(item);
                    item.setProperty("inn_ver_memo", in_ass_ver_memo);
                }
            }

            //用委員會帳號排序
            return result.OrderBy(x => x.sno).ToList();
        }

        private void AppendDegreeWaiting(TConfig cfg, TCommittee cmt, Item item)
        {
            var in_l1 = item.getProperty("in_l1", "");
            var in_l1_code = item.getProperty("in_l1_code", "0");
            var apply_dgr_value = GetIntVal(in_l1_code); ;

            var degree_box = cmt.DegreeList.Find(x => x.apply_dgr_value == apply_dgr_value);

            if (degree_box == null)
            {
                degree_box = new TBox
                {
                    code = "cmt_" + in_l1_code,
                    apply_dgr_value = apply_dgr_value,
                    apply_dgr_label = in_l1,
                    itmMUsers = new List<Item>(),
                    can_verify = true,
                    is_reject = false,
                };

                degree_box.title = cmt.name + "-" + "晉段審核" + "-" + in_l1;

                BindDegreeCtrlId(cmt, degree_box);

                cmt.DegreeList.Add(degree_box);
            }

            degree_box.itmMUsers.Add(item);
        }

        private void BindDegreeCtrlId(TCommittee cmt, TBox bx)
        {
            bx.ctrl_fold_id = "cmt_box_fold_" + cmt.sno + "_" + bx.code;
            bx.ctrl_table_id = "cmt_box_tbl_" + cmt.sno + "_" + bx.code;
        }

        private string GetDegreeTitle(int degree)
        {
            switch (degree)
            {
                case 1000: return "壹段";
                case 2000: return "貳段";
                case 3000: return "參段";
                case 4000: return "肆段";
                case 5000: return "伍段";
                case 6000: return "陸段";
                case 7000: return "柒段";
                case 8000: return "捌段";
                case 9000: return "玖段";
                default: return "異常";
            }
        }

        private Item GetDegreeMUsers(TConfig cfg)
        {
            string sql = @"
                SELECT
	                t1.in_meeting_type
	                , t2.id
	                , t2.in_committee
	                , t2.in_name
	                , t2.in_en_name
	                , t2.in_sno
	                , t2.in_sno_display
	                , dateadd(hour, 8, t2.in_birth) AS 'in_birth'
	                , t2.in_gender
	                , t2.in_guardian
	                , t2.in_l1
	                , t5.value AS 'in_l1_code'
	                , t2.in_degree
	                , t2.in_degree_id
	                , t2.in_gl_degree_id
	                , t2.in_verify_time
	                , t2.in_verify_result
	                , t2.in_verify_memo
	                , t2.in_ass_ver_time
	                , t2.in_ass_ver_result
	                , t2.in_ass_ver_memo
	                , t3.id           AS 'resume_id'
	                , t4.in_sno       AS 'committee_sno'
	                , t4.in_short_org AS 'committee_shor_name'
	                , t4.in_area      AS 'committee_city'
                FROM
	                IN_CLA_MEETING t1 WITH(NOLOCK)
                INNER JOIN
	                IN_CLA_MEETING_USER t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                INNER JOIN
	                IN_RESUME t3 WITH(NOLOCK)
	                ON t3.in_sno = t2.in_sno
                INNER JOIN
	                IN_RESUME t4 WITH(NOLOCK)
	                ON t4.in_name = t2.in_committee
	                AND t4.in_member_type = 'area_cmt'
				INNER JOIN
				(
					SELECT t12.value, t12.label_zt AS 'label'
					FROM [List] t11 WITH(NOLOCK) 
					INNER JOIN [Value] t12 WITH(NOLOCK) ON t12.source_id = t11.id
					WHERE t11.name = 'In_Black_Belt_List'
				) t5 ON t5.label = t2.in_l1
                WHERE
	                t1.id = '{#meeting_id}'
                ORDER BY
                    t2.in_degree DESC
                    , t2.in_birth
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        //審查講習人員清單
        private void AppendSeminarViev(TConfig cfg, Item itmReturn)
        {
            string sql = "";

            sql = @"
                SELECT T4.id AS rid,
                    T1.id,
                    T1.IN_L1,
                    T1.IN_NAME,
                    T1.IN_EN_NAME,
                    T1.IN_SNO_DISPLAY,
                    dateadd(hour, 8, T1.IN_BIRTH) AS IN_BIRTH,
                    T1.IN_GENDER,
                    T1.IN_GUARDIAN,
                    T1.IN_DEGREE,
                    T1.IN_DEGREE_LABEL,
                    T1.IN_DEGREE_ID,
                    T1.IN_GL_DEGREE_ID,
                    T1.IN_CURRENT_ORG,
                    T1.IN_CERT_VALID,
                    T2.Pay_bool,
                    T5.IN_PAY_AMOUNT,
                    ISNULL(T3.in_short_org, T1.IN_COMMITTEE) AS 'committee_short_name',
                    T5.id AS nid
                FROM 
                    IN_CLA_MEETING_USER AS T1 WITH (NOLOCK)
                LEFT OUTER JOIN 
                    IN_MEETING_PAY AS T2 WITH (NOLOCK) ON T2.item_number = T1.in_paynumber
                LEFT OUTER JOIN  
                    IN_RESUME AS T3 WITH (NOLOCK) ON T3.IN_NAME = T1.IN_COMMITTEE AND T3.in_member_type = 'area_cmt' AND t3.in_member_role = 'sys_9999'
                JOIN 
                    IN_RESUME AS T4 WITH (NOLOCK) ON T1.in_sno = T4.in_sno
                LEFT JOIN 
                    In_Meeting_news AS T5 WITH (NOLOCK) ON T5.source_id = T2.id AND T5.in_sno = t1.in_sno AND T5.in_l1 = t1.in_l1
                WHERE 
                    T1.source_id = '{#meeting_id}'
                    AND ISNULL(T1.in_ass_ver_result, '') = ''
                ORDER BY 
                    T1.IN_L1
                    , T1.IN_DEGREE DESC
                    , T1.IN_BIRTH
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);
            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);
            Item itmMUsers = cfg.inn.applySQL(sql);
            int count = itmMUsers.getItemCount();
            Dictionary<string, List<Item>> map = new Dictionary<string, List<Item>>();
            for (int i = 0; i < count; i++)
            {
                Item itmMUser = itmMUsers.getItemByIndex(i);
                string in_l1 = itmMUser.getProperty("in_l1", "");
                List<Item> list = null;
                if (map.ContainsKey(in_l1))
                {
                    list = map[in_l1];
                }
                else
                {
                    list = new List<Item>();
                    map.Add(in_l1, list);
                }
                list.Add(itmMUser);
            }

            string verifyStr = "";
            string table_name = "";
            if (map.Count > 0)
            {
                int i = 0;
                foreach (var kv in map)
                {
                    List<Item> items = kv.Value;
                    Item itmStage = items[0];

                    string stage = itmStage.getProperty("in_l1", "");
                    table_name = "In_Seminar_list_" + i.ToString();

                    verifyStr += "<div class=\"container bg-white-hw\">";
                    verifyStr += "<div class=\"main clearfix\">";
                    verifyStr += "<div class=\"box-header\">";
                    verifyStr += "<h4 class=\"box-header with-border\">" + "待審核-" + stage + "<small>({count_" + i.ToString() + "}) " + SeminarXlsBtn(cfg, itmStage) + "</small></h4>";
                    verifyStr += "<div class=\"box-tools pull-right\">";
                    verifyStr += "<button type=\"button\" class=\"btn btn-box-tool fold_type\" data-widget=\"collapse\" style=\"width: 40px; height: 20px;\">";
                    verifyStr += "<a class=\"accordion-toggle box-title form-box-title fa fa-minus\" style=\"font-size: 25px;\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#" + table_name + "_fold\"></a>";
                    verifyStr += "</button></div></div>";
                    verifyStr += "<div id=\"" + table_name + "_fold\" class=\"collapse in\">";
                    verifyStr += "<div class=\"float-btn clearfix\">";
                    verifyStr += "<button id=\"select_b_all" + i.ToString() + "\" class=\"btn btn-success btnAll\" tablename =\"" + table_name + "\" >全選</button>&emsp;";
                    verifyStr += "<button id=\"unselect_b_all" + i.ToString() + "\" class=\"btn btn-default btnUnAll\" tablename =\"" + table_name + "\">取消全選</button>&emsp;";
                    verifyStr += "<button id=\"reviewPromotion" + i.ToString() + "\" class=\"btn btn-primary  btnRePro\" tablename =\"" + table_name + "\">審核已勾選</button>";

                    verifyStr += "</div>";

                    //待審核講習人員清單
                    verifyStr = AppendSeminarStage(stage, cfg, items, table_name, verifyStr, i.ToString());
                    verifyStr += "</div></div></div>";

                    i++;
                }
            }

            //已審核人員清單(通過)
            verifyStr = verifySeminarList(cfg, verifyStr, "1");
            verifyStr += "</div></div></div>";

            //已審核人員清單(不通過)
            verifyStr = verifySeminarList(cfg, verifyStr, "0");
            verifyStr += "</div></div></div>";
            verifyStr += "</div>";

            itmReturn.setProperty("inn_verify_list", verifyStr);
        }

        private string SeminarXlsBtn(TConfig cfg, Item itmMUser)
        {
            string result = "<button class='btn btn-sm btn-primary'"
                + " data-type='" + cfg.in_meeting_type + "' data-stype='" + cfg.in_seminar_type + "'"
                + " data-verify='' onclick='ExportReisterXls(this)'>匯出待審核名冊</button>";

            result += " <button class='btn btn-sm btn-warning'"
                            + " data-type='" + cfg.in_meeting_type + "' data-stype='" + cfg.in_seminar_type + "'"
                            + " onclick='BatchVerify(this)'>一鍵檢查附件</button>";

            return result;
        }

        private string SeminarXlsBtn2(TConfig cfg)
        {
            return "<button class='btn btn-sm btn-primary' data-type='" + cfg.in_meeting_type + "' data-stype='" + cfg.in_seminar_type + "' data-verify='0' onclick='ExportReisterXls(this)'>匯出未通過名冊</button>";
        }

        //附加晉段人員清單 Table
        private string AppendDegreeStage(string stage, TConfig cfg, string table_name, string in_committee, string verifyStr, string num)
        {
            string sql = @"
                SELECT
                    t2.id
                    , t2.in_sno
                    , t2.in_l1
                    , t2.in_committee
                    , t2.in_name
                    , t2.in_en_name
                    , t2.in_sno_display
                    , dateadd(hour, 8, t2.in_birth) AS 'in_birth'
                    , t2.in_gender
                    , t2.in_guardian
                    , t2.in_degree
                    , t2.in_degree_id
                    , t2.in_gl_degree_id
                    , t3.id                         AS 'rid'
                FROM 
                    IN_CLA_MEETING t1 WITH (NOLOCK)
                INNER JOIN
                    IN_CLA_MEETING_USER t2 WITH (NOLOCK)
                    ON t2.source_id = t1.id
                LEFT OUTER JOIN
                    IN_RESUME t3 WITH(NOLOCK)
                    ON t3.in_sno = t2.in_sno
                LEFT OUTER JOIN
                    IN_MEETING_PAY t4 WITH(NOLOCK)
                    ON t4.item_number = t2.in_paynumber
                WHERE 
                    t1.id = '{#meeting_id}'
                    AND t2.in_committee = N'{#in_committee}'
                    AND t2.in_l1 = N'{#stage}'
                    AND ISNULL(t2.in_verify_result, '') = '1' 
                    AND ISNULL(t2.in_ass_ver_result, '') = '' 
                ORDER BY 
                    t2.in_degree DESC
                    , t2.in_birth
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_committee}", in_committee)
                .Replace("{#stage}", stage);


            Item items = cfg.inn.applySQL(sql);

            List<TField> fields = new List<TField>();
            fields.Add(new TField { property = "in_name", title = "中文姓名", css = "text-left" });
            fields.Add(new TField { property = "in_en_name", title = "英文姓名", css = "text-left" });
            fields.Add(new TField { property = "in_sno_display", title = "身分證號", css = "text-left" });
            fields.Add(new TField { property = "in_birth", title = "出生日", css = "text-left" });
            fields.Add(new TField { property = "in_gender", title = "性別", css = "text-left" });
            fields.Add(new TField { property = "in_guardian", title = "現任教練", css = "text-left" });
            fields.Add(new TField { property = "in_degree_id", title = "國內證號", css = "text-left" });
            fields.Add(new TField { property = "in_gl_degree_id", title = "國際證號", css = "text-left" });
            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            head.AppendLine("<thead>");
            // head.AppendLine("<th></th>");
            foreach (var field in fields)
            {
                if (field.hide)
                {
                    head.AppendLine("<th class='mailbox-name' data-field='" + field.property + "' data-sortable='true' data-visible='false'>" + field.title + "</th>");
                }
                else
                {
                    head.AppendLine("<th class='mailbox-name' data-field='" + field.property + "' data-sortable='true'>" + field.title + "</th>");
                }
            }
            head.AppendLine("<th class=\"mailbox-name\" data-field=\"功能\" data-sortable=\"false\">功能</th>");

            head.AppendLine("</thead>");

            int count = items.getItemCount();
            if (count <= 0)
            {
                count = 0;
            }
            verifyStr = verifyStr.Replace("{count_" + num + "}", "人數：" + count.ToString() + "名");
            body.AppendLine("<tbody>");
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string id = item.getProperty("id", "");
                string in_birth = item.getProperty("in_birth", "");
                string in_name = item.getProperty("in_name", "");
                string in_sno_display = item.getProperty("in_sno_display", "");

                string rid = item.getProperty("rid", "");

                string pay_bool = item.getProperty("pay_bool", "");
                string in_pay_amount = item.getProperty("in_pay_amount", "");

                string nid = item.getProperty("nid", "");

                item.setProperty("in_birth", Convert.ToDateTime(in_birth).ToString("yyyy/MM/dd"));
                item.setProperty("in_name", "<a onclick=\"show_PhotoUploadModal('" + id + "', '" + in_sno_display + "', 'view')\"><i class=\"fa fa-picture-o in_in_photo1\">" + in_name + "</i></a>");

                body.AppendLine("<tr data-id=\"" + id + "\" data-rid=\"" + rid + "\" data-amount=\"" + in_pay_amount + "\" data-nid=\"" + nid + "\">");
                // body.AppendLine("<td><label class=\"checkbox\"><input type=\"checkbox\" value=\"\" data-toggle=\"checkbox\" class=\"custom-checkbox\"/><span class=\"icons\"><span class=\"icon-unchecked\"></span><span class=\"icon-checked\"></span></span></label></td>");
                foreach (var field in fields)
                {
                    body.AppendLine("<td class='" + field.css + "' data-field='" + field.property + "' >" + field.GetStr(item) + "</td>");
                }
                // body.AppendLine("<td><button class=\"btn btn-primary\"><i class=\"fa fa-eye\"></i>檢視</button>&emsp;<button class=\"btn btn-danger\"><i class=\"fa fa-times\"></i>不通過</button></td>");

                if (pay_bool != "已繳費")
                {
                    body.AppendLine("<td><button class=\"btn btn-primary\" onclick=\"show_verifyModal(this,'degree')\">審核</button>&nbsp;<button class=\"btn btn-danger\" onclick=\"cancelUser(this,'seminar')\">刪除</button></td>");
                }
                else
                {
                    body.AppendLine("<td><button class=\"btn btn-primary\" onclick=\"show_verifyModal(this,'degree')\">審核</button>&nbsp;<button class=\"btn btn-default\" onclick=\"refund_review(this,'seminar')\">退費</button></td>");
                }
                body.AppendLine("</tr>");
            }

            body.AppendLine("</tbody>");


            StringBuilder builder = new StringBuilder();
            builder.AppendLine(GetTableAttribute(table_name));
            builder.Append(head);
            builder.Append(body);
            builder.AppendLine("</table>");
            builder.AppendLine("<script>");
            builder.AppendLine("$('#" + table_name + "').bootstrapTable();");
            builder.AppendLine("if ($(window).width() <= 768) { $('#" + table_name + "').bootstrapTable('toggleView'); }");
            builder.AppendLine("</script>");

            return verifyStr + builder.ToString();
        }

        //已審核晉段人員清單 Table
        private string verifyList(TConfig cfg, string committee_sno, string in_committee, string verifyStr, string verResult)
        {
            string mode = "";

            if (verResult == "1")
            {
                mode = "通過";
            }
            else
            {
                mode = "不通過";
            }

            string sql = @"
                SELECT 
                    t2.id
                    , t2.in_name
                    , t2.in_en_name
                    , t2.in_gender
                    , dateadd(hour, 8, t2.in_birth) AS 'in_birth'
                    , t2.in_degree
                    , t2.in_degree_id
                    , t2.in_gl_degree_id
                    , t2.in_sno_display
                    , t2.in_l1
                    , t2.in_committee
                    , t2.in_ass_ver_memo
                    , t2.in_guardian
                    , t11.pay_bool
                    , t3.id                          AS 'rid'
                FROM 
                    IN_CLA_MEETING t1 WITH(NOLOCK)
                INNER JOIN
                    IN_CLA_MEETING_USER t2 WITH(NOLOCK)
                    ON t2.source_id = t1.id
                INNER JOIN
                    IN_RESUME t3 WITH(NOLOCK)
                    ON t3.in_sno = t2.in_sno
                LEFT OUTER JOIN
                    IN_MEETING_PAY t11 WITH(NOLOCK)
                    ON t11.item_number = t2.in_paynumber
                LEFT OUTER JOIN
                    IN_RESUME t12 WITH(NOLOCK)
                    ON t12.in_name = t2.in_committee
                WHERE 
                    t1.id = '{#meeting_id}' 
                    AND t1.in_meeting_type = 'degree'
                    AND ISNULL(t2.in_verify_result, '') = '1' 
                    AND t2.in_ass_ver_time IS NOT NULL 
                    AND ISNULL(t2.in_ass_ver_result, '') = '{#verResult}'
                    AND t2.in_committee = N'{#in_committee}'
                order by
                    t2.in_degree DESC
                    , t2.in_birth
            ";

            sql = sql.Replace("{#in_committee}", in_committee)
                      .Replace("{#meeting_id}", cfg.meeting_id)
                      .Replace("{#verResult}", verResult);
            Item items = cfg.inn.applySQL(sql);

            List<TField> fields = new List<TField>();
            fields.Add(new TField { property = "in_l1", title = "段位", css = "text-left" });
            fields.Add(new TField { property = "in_name", title = "中文姓名", css = "text-left" });
            fields.Add(new TField { property = "in_en_name", title = "英文姓名", css = "text-left" });
            fields.Add(new TField { property = "in_sno_display", title = "身分證號", css = "text-left" });
            fields.Add(new TField { property = "in_birth", title = "出生日", css = "text-left" });
            fields.Add(new TField { property = "in_gender", title = "性別", css = "text-left" });
            fields.Add(new TField { property = "in_guardian", title = "現任教練", css = "text-left" });
            fields.Add(new TField { property = "in_degree_id", title = "國內證號", css = "text-left" });
            fields.Add(new TField { property = "in_gl_degree_id", title = "國際證號", css = "text-left" });
            fields.Add(new TField { property = "pay_status", title = "繳費狀態", css = "text-left" });
            if (verResult == "0")
            {
                fields.Add(new TField { property = "in_ass_ver_memo", title = "審核不通過", css = "text-left text-ver" });
            }
            verifyStr += "<div class=\"container bg-white-hw\">";
            verifyStr += "<div class=\"main clearfix\">";
            verifyStr += "<div class=\"box-header\">";

            verifyStr += "<h4 class=\"box-header with-border\">" + in_committee + "-晉段審核" + mode + "<small>({count_ver})</small>&emsp;";
            if (verResult == "1")
            {
                verifyStr += "<button id=\"select_b_all\" class=\"btn btn-primary\" onclick='ExportXls_Click(\"" + committee_sno + "\",\"ass\")'>匯出晉段名冊</button>";
            }
            verifyStr += "</h4><div class=\"box-tools pull-right\">";
            verifyStr += "<button type=\"button\" class=\"btn btn-box-tool fold_type\" data-widget=\"collapse\" style=\"width: 40px; height: 20px;\">";
            verifyStr += "<a class=\"accordion-toggle box-title form-box-title fa fa-minus\" style=\"font-size: 25px;\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#" + committee_sno + verResult + "_fold\"></a>";
            verifyStr += "</button></div></div>";
            verifyStr += "<div id=\"" + committee_sno + verResult + "_fold\" class=\"collapse in\">";

            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            head.AppendLine("<thead>");
            // head.AppendLine("<th></th>");
            foreach (var field in fields)
            {
                if (field.hide)
                {
                    head.AppendLine("<th class='mailbox-name' data-field='" + field.property + "' data-sortable='true' data-visible='false'>" + field.title + "</th>");
                }
                else
                {
                    head.AppendLine("<th class='mailbox-name' data-field='" + field.property + "' data-sortable='true'>" + field.title + "</th>");
                }
            }
            head.AppendLine("<th class=\"mailbox-name\" data-field=\"功能\" data-sortable=\"false\">功能</th>");

            head.AppendLine("</thead>");

            int count = items.getItemCount();
            if (count <= 0)
            {
                count = 0;
            }
            verifyStr = verifyStr.Replace("{count_ver}", "人數：" + count.ToString() + "名");
            body.AppendLine("<tbody>");
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_birth = item.getProperty("in_birth", "");
                string in_name = item.getProperty("in_name", "");
                string id = item.getProperty("id", "");
                string in_sno_display = item.getProperty("in_sno_display", "");
                string rid = item.getProperty("rid", "");
                item.setProperty("pay_status", GetPayStatus(item.getProperty("pay_bool", "")));
                item.setProperty("in_birth", Convert.ToDateTime(in_birth).ToString("yyyy/MM/dd"));
                item.setProperty("in_name", "<a onclick=\"show_PhotoUploadModal('" + id + "', '" + in_sno_display + "', 'view')\"><i class=\"fa fa-picture-o in_in_photo1\">" + in_name + "</i></a>");

                body.AppendLine("<tr data-id=\"" + id + "\" data-rid=\"" + rid + "\">");
                // body.AppendLine("<td><label class=\"checkbox\"><input type=\"checkbox\" value=\"\" data-toggle=\"checkbox\" class=\"custom-checkbox\"/><span class=\"icons\"><span class=\"icon-unchecked\"></span><span class=\"icon-checked\"></span></span></label></td>");
                foreach (var field in fields)
                {
                    body.AppendLine("<td class='" + field.css + "' data-field='" + field.property + "' >" + field.GetStr(item) + "</td>");
                }
                body.AppendLine("<td><button class=\"btn btn-primary\" onclick=\"show_verifyModal(this,'ass')\">審核</button></td>");
                body.AppendLine("</tr>");
            }

            body.AppendLine("</tbody>");


            StringBuilder builder = new StringBuilder();
            builder.AppendLine(GetTableAttribute(committee_sno + "_ver"));
            builder.Append(head);
            builder.Append(body);
            builder.AppendLine("</table>");
            builder.AppendLine("<script>");
            builder.AppendLine("$('#" + committee_sno + "_ver" + "').bootstrapTable();");
            builder.AppendLine("if ($(window).width() <= 768) { $('#" + committee_sno + "_ver" + "').bootstrapTable('toggleView'); }");
            builder.AppendLine("</script>");

            return verifyStr + builder.ToString();
        }

        //已審核講習人員清單 Table
        private string verifySeminarList(TConfig cfg, string verifyStr, string verResult)
        {

            string mode = "";
            if (verResult == "1")
            {
                mode = "審核通過";
            }
            else
            {
                mode = "審核不通過";
            }

            string sql = @"
                SELECT T4.id AS rid,
                    T1.id,
                    T1.in_ass_ver_memo,
                    T1.IN_L1,
                    T2.IN_COMMITTEE,
                    T1.IN_NAME,
                    T1.IN_EN_NAME,
                    T1.IN_SNO_DISPLAY,
                    dateadd(hour, 8, T1.IN_BIRTH) AS IN_BIRTH,
                    T1.IN_GENDER,
                    T1.IN_GUARDIAN,
                    T1.IN_DEGREE,
                    T1.IN_DEGREE_LABEL,
                    T1.IN_DEGREE_ID,
                    T1.IN_GL_DEGREE_ID,
                    T2.PAY_BOOL,
                    T1.in_current_org,
                    ISNULL(T3.in_short_org, T1.IN_COMMITTEE) AS 'committee_short_name'
                FROM IN_CLA_MEETING_USER AS T1 WITH (NOLOCK)
                    LEFT OUTER JOIN IN_MEETING_PAY AS T2 WITH (NOLOCK) ON T2.item_number = T1.in_paynumber
                    LEFT OUTER JOIN IN_RESUME AS T3 WITH (NOLOCK) ON T1.IN_COMMITTEE = T3.IN_NAME AND T3.in_member_type = 'area_cmt' AND T3.in_member_role = 'sys_9999'
                    JOIN IN_RESUME AS T4 WITH (NOLOCK) ON T1.in_sno = T4.in_sno
                WHERE T1.source_id = N'{#meeting_id}'
                    AND T1.in_ass_ver_time is not null
                    AND ISNULL(T1.in_ass_ver_result, '') = '{#verResult}'
                ORDER BY T1.IN_DEGREE DESC,
                    T1.in_birth
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                      .Replace("{#verResult}", verResult);

            Item items = cfg.inn.applySQL(sql);

            List<TField> fields = new List<TField>();
            // fields.Add(new TField { property = "in_l1", title = "項目", css = "text-left" });
            fields.Add(new TField { property = "committee_short_name", title = "所屬委員會", css = "text-left" });
            fields.Add(new TField { property = "in_current_org", title = "所屬單位", css = "text-left" });
            fields.Add(new TField { property = "in_name", title = "中文姓名", css = "text-left" });
            fields.Add(new TField { property = "in_sno_display", title = "身分證號", css = "text-left" });
            fields.Add(new TField { property = "in_birth", title = "出生日", css = "text-left" });
            fields.Add(new TField { property = "in_gender", title = "性別", css = "text-left" });
            fields.Add(new TField { property = "in_degree_label", title = "段位", css = "text-left" });
            fields.Add(new TField { property = "pay_status", title = "繳費狀態", css = "text-left" });
            if (verResult == "0")
            {
                fields.Add(new TField { property = "in_ass_ver_memo", title = "審核不通過", css = "text-left text-ver" });
            }
            verifyStr += "<div class=\"container bg-white-hw\">";
            verifyStr += "<div class=\"main clearfix\">";
            verifyStr += "<div class=\"box-header\">";

            verifyStr += "<h4 class=\"box-header with-border\">" + mode + "<small>({count_ver})</small>&emsp;";
            if (verResult == "1")
            {
                verifyStr += "<button id=\"select_b_all\" class=\"btn btn-primary\" onclick='ExportXls_Click(\"" + cfg.meeting_id + "\",\"seminar\")'>匯出講習清冊</button>";
            }
            else if (verResult == "0")
            {
                verifyStr += SeminarXlsBtn2(cfg);
            }
            verifyStr += "</h4><div class=\"box-tools pull-right\">";
            verifyStr += "<button type=\"button\" class=\"btn btn-box-tool fold_type\" data-widget=\"collapse\" style=\"width: 40px; height: 20px;\">";
            verifyStr += "<a class=\"accordion-toggle box-title form-box-title fa fa-minus\" style=\"font-size: 25px;\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#" + cfg.meeting_id + verResult + "_fold\"></a>";
            verifyStr += "</button></div></div>";
            verifyStr += "<div id=\"" + cfg.meeting_id + verResult + "_fold\" class=\"collapse in\">";

            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            head.AppendLine("<thead>");
            // head.AppendLine("<th></th>");
            foreach (var field in fields)
            {
                if (field.hide)
                {
                    head.AppendLine("<th class='mailbox-name' data-field='" + field.property + "' data-sortable='true' data-visible='false'>" + field.title + "</th>");
                }
                else
                {
                    head.AppendLine("<th class='mailbox-name' data-field='" + field.property + "' data-sortable='true'>" + field.title + "</th>");
                }
            }
            head.AppendLine("<th class=\"mailbox-name\" data-field=\"功能\" data-sortable=\"false\">功能</th>");

            head.AppendLine("</thead>");

            int count = items.getItemCount();
            if (count <= 0)
            {
                count = 0;
            }
            verifyStr = verifyStr.Replace("{count_ver}", "人數：" + count.ToString() + "名");
            body.AppendLine("<tbody>");
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_birth = item.getProperty("in_birth", "");
                string in_name = item.getProperty("in_name", "");
                string id = item.getProperty("id", "");
                string in_sno_display = item.getProperty("in_sno_display", "");
                string rid = item.getProperty("rid", "");
                item.setProperty("pay_status", GetPayStatus(item.getProperty("pay_bool", "")));
                item.setProperty("in_birth", Convert.ToDateTime(in_birth).ToString("yyyy/MM/dd"));
                item.setProperty("in_name", "<a onclick=\"show_PhotoUploadModal('" + id + "', '" + in_sno_display + "', 'view')\"><i class=\"fa fa-picture-o in_in_photo1\">" + in_name + "</i></a>");

                body.AppendLine("<tr data-id=\"" + id + "\" data-rid=\"" + rid + "\">");
                // body.AppendLine("<td><label class=\"checkbox\"><input type=\"checkbox\" value=\"\" data-toggle=\"checkbox\" class=\"custom-checkbox\"/><span class=\"icons\"><span class=\"icon-unchecked\"></span><span class=\"icon-checked\"></span></span></label></td>");
                foreach (var field in fields)
                {
                    body.AppendLine("<td class='" + field.css + "' data-field='" + field.property + "' >" + field.GetStr(item) + "</td>");
                }
                body.AppendLine("<td><button class=\"btn btn-primary\" onclick=\"show_verifyModal(this,'seminar')\">審核</button></td>");
                body.AppendLine("</tr>");
            }

            body.AppendLine("</tbody>");


            StringBuilder builder = new StringBuilder();
            builder.AppendLine(GetTableAttribute(cfg.meeting_id + "_ver"));
            builder.Append(head);
            builder.Append(body);
            builder.AppendLine("</table>");
            builder.AppendLine("<script>");
            builder.AppendLine("$('#" + cfg.meeting_id + "_ver" + "').bootstrapTable();");
            builder.AppendLine("if ($(window).width() <= 768) { $('#" + cfg.meeting_id + "_ver" + "').bootstrapTable('toggleView'); }");
            builder.AppendLine("</script>");


            return verifyStr + builder.ToString();

        }

        //附加講習人員清單 Table
        private string AppendSeminarStage(string stage, TConfig cfg, List<Item> items, string table_name, string verifyStr, string num)
        {
            List<TField> fields = new List<TField>();
            fields.Add(new TField { property = "committee_short_name", title = "所屬委員會", css = "text-left" });
            fields.Add(new TField { property = "in_current_org", title = "所屬單位", css = "text-left" });
            fields.Add(new TField { property = "in_name", title = "中文姓名", css = "text-left" });
            fields.Add(new TField { property = "in_sno_display", title = "身分證號", css = "text-left" });
            fields.Add(new TField { property = "in_birth", title = "出生日", css = "text-left" });
            fields.Add(new TField { property = "in_gender", title = "性別", css = "text-left" });
            fields.Add(new TField { property = "in_degree_label", title = "段位", css = "text-left" });

            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            head.AppendLine("<thead>");
            head.AppendLine("<th></th>");
            foreach (var field in fields)
            {
                if (field.hide)
                {
                    head.AppendLine("<th class='mailbox-name' data-field='" + field.property + "' data-sortable='true' data-visible='false'>" + field.title + "</th>");
                }
                else
                {
                    head.AppendLine("<th class='mailbox-name' data-field='" + field.property + "' data-sortable='true'>" + field.title + "</th>");
                }
            }
            head.AppendLine("<th class=\"mailbox-name\" data-field=\"功能\" data-sortable=\"false\">功能</th>");

            head.AppendLine("</thead>");

            int count = items.Count;
            if (count <= 0)
            {
                count = 0;
            }
            verifyStr = verifyStr.Replace("{count_" + num + "}", "人數：" + count.ToString() + "名");
            body.AppendLine("<tbody>");
            for (int i = 0; i < count; i++)
            {
                Item item = items[i];
                // string id = item.getProperty("id", "");
                string in_birth = item.getProperty("in_birth", "");
                string in_name = item.getProperty("in_name", "");
                string id = item.getProperty("id", "");
                string in_sno_display = item.getProperty("in_sno_display", "");
                string rid = item.getProperty("rid", "");
                string pay_bool = item.getProperty("pay_bool", "");
                string in_pay_amount = item.getProperty("in_pay_amount", "");
                string nid = item.getProperty("nid", "");


                item.setProperty("in_birth", Convert.ToDateTime(in_birth).ToString("yyyy/MM/dd"));
                item.setProperty("in_name", NameInfo(item));

                body.AppendLine("<tr data-id=\"" + id + "\" data-rid=\"" + rid + "\" data-amount=\"" + in_pay_amount + "\" data-nid=\"" + nid + "\">");
                body.AppendLine("<td><label class=\"checkbox\"><input type=\"checkbox\" value=\"\" data-toggle=\"checkbox\" class=\"custom-checkbox\"/><span class=\"icons\"><span class=\"icon-unchecked\"></span><span class=\"icon-checked\"></span></span></label></td>");

                foreach (var field in fields)
                {
                    body.AppendLine("<td class='" + field.css + "' data-field='" + field.property + "' >" + field.GetStr(item) + "</td>");
                }
                // body.AppendLine("<td><button class=\"btn btn-primary\"><i class=\"fa fa-eye\"></i>檢視</button>&emsp;<button class=\"btn btn-danger\"><i class=\"fa fa-times\"></i>不通過</button></td>");
                if (pay_bool != "已繳費")
                {
                    body.AppendLine("<td><button class=\"btn btn-primary\" onclick=\"show_verifyModal(this,'seminar')\">審核</button>&nbsp;<button class=\"btn btn-danger\" onclick=\"cancelUser(this,'seminar')\">刪除</button></td>");
                }
                else
                {
                    body.AppendLine("<td><button class=\"btn btn-primary\" onclick=\"show_verifyModal(this,'seminar')\">審核</button>&nbsp;<button class=\"btn btn-default\" onclick=\"refund_review(this,'seminar')\">退費</button></td>");
                }

                body.AppendLine("</tr>");
            }

            body.AppendLine("</tbody>");


            StringBuilder builder = new StringBuilder();
            builder.AppendLine(GetTableAttribute(table_name));
            builder.Append(head);
            builder.Append(body);
            builder.AppendLine("</table>");
            builder.AppendLine("<script>");
            builder.AppendLine("$('#" + table_name + "').bootstrapTable();");
            builder.AppendLine("if ($(window).width() <= 768) { $('#" + table_name + "').bootstrapTable('toggleView'); }");
            builder.AppendLine("</script>");

            return verifyStr + builder.ToString();
        }

        private string NameInfo(Item item)
        {
            string id = item.getProperty("id", "");
            string in_sno_display = item.getProperty("in_sno_display", "");
            string in_name = item.getProperty("in_name", "");
            string in_cert_valid = item.getProperty("in_cert_valid", "");

            string cert_icon = "";
            switch (in_cert_valid)
            {
                case "1":
                    cert_icon = "<i class='fa fa-check' style='color:#00A64E'></i>";
                    break;
                case "0":
                    cert_icon = "<i class='fa fa-exclamation' style='color:red'></i>";
                    break;
                default:
                    cert_icon = "<i class='fa fa-question' style='color:red'></i>";
                    break;
            }

            return "<a onclick=\"show_PhotoUploadModal('" + id + "', '" + in_sno_display + "', 'view')\">"
            + "<i class='fa fa-picture-o in_in_photo1'>" + in_name + "</i>"
            + "</a> "
            + cert_icon;
        }

        private class TField
        {
            public string property { get; set; }
            public string title { get; set; }
            public string css { get; set; }
            public string format { get; set; }
            public bool hide { get; set; }

            public string GetStr(Item item)
            {
                return item.getProperty(this.property, "");
            }

        }

        private string GetTableAttribute(string table_name)
        {
            return "<table id='" + table_name + "'"
                + " data-toggle='table' "
                + " data-click-to-select='true' "
                + " data-striped='false' "
                + " data-search-align='left' "
                + " data-sort-stable='true' "
                + " data-search='true' "
                + ">";
        }

        //設定物件與資料列
        private void SetItemCell(ClosedXML.Excel.IXLWorksheet sheet, int wsRow, int wsCol, Item item, List<TField> fields)
        {
            int count = fields.Count;
            for (int i = 0; i < count; i++)
            {
                TField field = fields[i];

                ClosedXML.Excel.IXLCell cell = sheet.Cell(wsRow, wsCol + i);

                if (field.property == "")
                {
                    SetBodyCell(cell, "", "");
                }
                else
                {
                    string value = item.getProperty(field.property, "");
                    SetBodyCell(cell, value, field.format);
                }
            }
        }

        private Item GetExcelPath(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string in_name)
        {
            Item itmResult = inn.newItem();

            //Excel 匯出路徑 (from 原創.設定參數)
            string sql = @"
            SELECT 
                TOP 1 t2.in_name AS 'variable', t1.in_name, t1.in_value 
            FROM 
                In_Variable_Detail t1 WITH(NOLOCK)
            INNER JOIN 
                In_Variable t2 WITH(NOLOCK) ON t2.ID = t1.SOURCE_ID 
            WHERE 
                t2.in_name = N'meeting_excel'  
                AND t1.in_name = N'{#in_name}'
            ";

            sql = sql.Replace("{#in_name}", in_name);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item item = inn.applySQL(sql);

            if (item.isError())
            {
                throw new Exception("未設定匯出路徑 _# " + in_name);
            }

            itmResult.setProperty("export_path", item.getProperty("in_value", ""));

            return itmResult;
        }
        private void SetCellTextValue(ref ClosedXML.Excel.IXLCell Cell, string TextValue)
        {

            DateTime dtTmp;
            double dblTmp;


            if (DateTime.TryParse(TextValue, out dtTmp))
            {
                Cell.Value = "'" + TextValue;
            }
            else if (double.TryParse(TextValue, out dblTmp))
            {
                Cell.Value = "'" + TextValue;
            }
            else
            {
                Cell.Value = TextValue;
            }
            Cell.DataType = ClosedXML.Excel.XLDataType.Text;
        }

        //設定資料列
        private void SetBodyCell(ClosedXML.Excel.IXLCell cell, string value, string format)
        {
            switch (format)
            {
                case "center":
                    cell.Value = "'" + value;
                    cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    break;

                case "yyyy/MM/dd":
                case "yyyy/MM/dd HH:mm":
                    cell.Value = GetDateTimeValue(value, format, 8);
                    cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    break;

                case "$ #,##0":
                    cell.Value = value;
                    cell.Style.NumberFormat.Format = "$ #,##0";
                    break;

                case "tel":
                    if (value.StartsWith("09"))
                    {
                        cell.Value = value.Replace("-", "");
                        cell.Style.NumberFormat.Format = "0000-000-000";
                        cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    }
                    else
                    {
                        cell.Value = "'" + value;
                        cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    }
                    break;

                case "text":
                    cell.Value = "'" + value;
                    cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    break;

                case "text-left":
                    cell.Value = "'" + value;
                    cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Left;
                    break;

                default:
                    cell.Value = value;
                    break;
            }

            if (format != "title")
            {
                cell.Style.Border.SetRightBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
                cell.Style.Border.SetTopBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
                cell.Style.Border.SetBottomBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
                cell.Style.Border.SetLeftBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            }
        }

        private string GetDateTimeValue(string value, string format, int hours = 0)
        {
            if (value == "") return "";

            DateTime dt = DateTime.Now;
            if (DateTime.TryParse(value, out dt))
            {
                return dt.AddHours(hours).ToString(format);
            }
            else
            {
                return value;
            }
        }

        private string GetPayStatus(string pay_bool)
        {
            if (pay_bool == "已繳費")
            {
                return "<span style='color: green;'> <i class='fa fa-list-alt'></i> 已繳費</span>";
            }
            else if (pay_bool == "未繳費")
            {
                return "<span style='color: red;'> <i class='fa fa-list-alt'></i> 未繳費</span>";
            }
            else
            {
                return pay_bool;
            }
        }

        ClosedXML.Excel.XLColor head_bg_color = ClosedXML.Excel.XLColor.FromHtml("#295C90");

        //設定標題列
        private void SetHeadCell(ClosedXML.Excel.IXLCell cell, string title)
        {
            cell.Value = title;

            cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
            cell.Style.Font.Bold = true;
            cell.DataType = ClosedXML.Excel.XLDataType.Text;
            cell.Style.Fill.BackgroundColor = head_bg_color;
            cell.Style.Font.FontColor = ClosedXML.Excel.XLColor.White;

            cell.Style.Border.SetRightBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetTopBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetBottomBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetLeftBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }

            //外部輸入區

            /// <summary>
            /// 活動 id
            /// </summary>
            public string meeting_id { get; set; }

            /// <summary>
            /// 模式 (cla 為課程)
            /// </summary>
            public string mode { get; set; }

            /// <summary>
            /// 測試-顯示未繳費的單子
            /// </summary>
            public string s_type { get; set; }



            //權限區

            /// <summary>
            /// 最高管理者
            /// </summary>
            public bool isMeetingAdmin { get; set; }

            /// <summary>
            /// 地區管理者
            /// </summary>
            public bool isCommittee { get; set; }

            /// <summary>
            /// 道館主
            /// </summary>
            public bool isGymOwner { get; set; }

            /// <summary>
            /// 道館協助
            /// </summary>
            public bool isGymAssistant { get; set; }

            /// <summary>
            /// 可以審核
            /// </summary>
            public bool can_review { get; set; }



            //登入者資訊區

            /// <summary>
            /// 登入者 Resume 資料
            /// </summary>
            public Item loginResume { get; set; }

            /// <summary>
            /// 登入者 Resume id
            /// </summary>
            public string login_resume_id { get; set; }

            /// <summary>
            /// 登入者 會員類型
            /// </summary>
            public string login_member_type { get; set; }

            /// <summary>
            /// 登入者 會員角色
            /// </summary>
            public string login_member_role { get; set; }

            /// <summary>
            /// 登入者 所屬群組
            /// </summary>
            public string login_group { get; set; }

            /// <summary>
            /// 登入者 身分證號
            /// </summary>
            public string login_sno { get; set; }

            /// <summary>
            /// 登入者 姓名
            /// </summary>
            public string login_name { get; set; }


            // Meeting 區

            /// <summary>
            /// Meeting 資料
            /// </summary>
            public Item itmMeeting { get; set; }

            /// <summary>
            /// 活動名稱
            /// </summary>
            public string in_title { get; set; }

            /// <summary>
            /// 活動地址
            /// </summary>
            public string in_address { get; set; }

            /// <summary>
            /// 客戶名稱
            /// </summary>
            public string in_customer { get; set; }

            /// <summary>
            /// 專屬展示網址
            /// </summary>
            public string in_url { get; set; }

            /// <summary>
            /// 專屬報名網址
            /// </summary>
            public string in_register_url { get; set; }

            /// <summary>
            /// 活動類型
            /// </summary>
            public string in_meeting_type { get; set; }

            /// <summary>
            /// 活動級別
            /// </summary>
            public string in_level { get; set; }

            /// <summary>
            /// 需下載收據功能
            /// </summary>
            public string in_need_receipt { get; set; }

            /// <summary>
            /// 梯次
            /// </summary>
            public string in_echelon { get; set; }
            /// <summary>
            /// 文號
            /// </summary>
            public string in_decree { get; set; }
            /// <summary>
            /// 課程起
            /// </summary>
            public string in_date_s { get; set; }
            /// <summary>
            /// 課程迄
            /// </summary>
            public string in_date_e { get; set; }
            /// <summary>
            /// 講習類型
            /// </summary>
            public string in_seminar_type { get; set; }

            public string SQL_OnlyMyGym { get; set; }
            public string group { get; set; }
            public string open_function { get; set; }
            public string method_type { get; set; }
        }

        private class TCommittee
        {
            public string sno { get; set; }
            public string name { get; set; }
            public string short_name { get; set; }
            public string city { get; set; }

            public List<TBox> DegreeList { get; set; }
            public TBox UnVerList { get; set; }
            public TBox RejectList { get; set; }

            public TBox AcsAgreeList { get; set; }
            public TBox AcsRejectList { get; set; }

            public string ctrl_li_id { get; set; }
            public string ctrl_pn_id { get; set; }
            public string ctrl_a_id { get; set; }
        }

        private class TBox
        {
            public string code { get; set; }
            public string title { get; set; }

            public string ctrl_fold_id { get; set; }
            public string ctrl_table_id { get; set; }

            public int apply_dgr_value { get; set; }
            public string apply_dgr_label { get; set; }

            public List<Item> itmMUsers { get; set; }

            public bool is_agree { get; set; }
            public bool is_reject { get; set; }
            public bool can_verify { get; set; }
        }

        private string DateTimeStr(string value, string format = "yyyy/MM/dd")
        {
            DateTime result = DateTime.MinValue;
            DateTime.TryParse(value, out result);
            return result.ToString(format);
        }

        private int GetIntVal(string value, int def = 0)
        {
            int result = def;
            Int32.TryParse(value, out result);
            return result;
        }
    }
}
