﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;
using System.Net;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class In_CTA_VipMember_Pay : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]In_CTA_VipMember_Pay";

            string sql = "SELECT * FROM IN_CTA_RESUME_PAY_20210524 WITH(NOLOCK) WHERE T23 = 'C221470007'";
            
            Item items = inn.applySQL(sql);

            int count = items.getItemCount();
            
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string resume_id = item.getProperty("resume_id", "");

                string sql_delete = "DELETE FROM IN_RESUME_PAY WHERE source_id = '"+ resume_id + "' AND ISNULL(in_paynumber, '') = ''";
                Item itmDelete = inn.applySQL(sql_delete);

                string in_year = "2018";
                string in_old_payno = GetOldPayNo(item.getProperty("t3", ""));//單據流水號
                string in_date = "";
                string in_note = item.getProperty("t4", "");//繳費情形(已交)
                AddResumePay(CCO, strMethodName, inn, resume_id, in_year, in_date, in_old_payno, in_note);

                in_year = "2019";
                in_old_payno = GetOldPayNo(item.getProperty("t5", ""));//單據流水號
                in_date = GetWestDay(CCO, strMethodName, inn, item.getProperty("t6", ""));//繳費情形
                in_note = "";
                AddResumePay(CCO, strMethodName, inn, resume_id, in_year, in_date, in_old_payno, in_note);

                in_year = "2020";
                in_old_payno = GetOldPayNo(item.getProperty("t7", ""));//單據流水號
                in_date = GetWestDay(CCO, strMethodName, inn, item.getProperty("t8", ""));//繳費情形
                in_note = "";
                AddResumePay(CCO, strMethodName, inn, resume_id, in_year, in_date, in_old_payno, in_note);

                in_year = "2021";
                in_old_payno = GetOldPayNo(item.getProperty("t9", ""));//單據流水號
                in_date = GetWestDay(CCO, strMethodName, inn, item.getProperty("t10", ""));//繳費情形
                in_note = "";
                AddResumePay(CCO, strMethodName, inn, resume_id, in_year, in_date, in_old_payno, in_note);
            }
            return this;
        }

        private string GetOldPayNo(string value)
        {
            return value.Length >= 3 ? value.PadLeft(6, '0') : value;
        }

        private string GetWestDay(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string tw_day)
        {
            if (tw_day == "") return "";

            string[] arr = tw_day.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
            if (arr == null || arr.Length != 3) return "";

            var y = GetIntVal(arr[0]);
            var m = GetIntVal(arr[1]);
            var d = GetIntVal(arr[2]);

            if (y < 150) y = y + 1911;

            CCO.Utilities.WriteDebug(strMethodName, "source: " + tw_day
                + ", y: " + y
                + ", m: " + m
                + ", d: " + d);

            var dt = new DateTime(y, m, d);
            return dt.ToString("yyyy-MM-ddTHH:mm:ss");
        }

        private int GetIntVal(string value)
        {
            if (value == "" || value == "0") return 0;
            int result = 0;
            int.TryParse(value, out result);
            return result;
        }

        private void AddResumePay(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string resume_id, string in_year, string in_date, string in_old_payno, string in_note)
        {
            if (in_old_payno == "" && in_date == "" && in_note == "") return;

            Item itmRPay = inn.newItem("In_Resume_Pay");
            itmRPay.setProperty("source_id", resume_id);
            itmRPay.setProperty("in_year", in_year);
            itmRPay.setProperty("in_date", in_date);
            itmRPay.setProperty("in_amount", "");
            itmRPay.setProperty("in_old_payno", in_old_payno);
            itmRPay.setProperty("in_note", in_note);

            itmRPay = itmRPay.apply("add");

        }
    }
}