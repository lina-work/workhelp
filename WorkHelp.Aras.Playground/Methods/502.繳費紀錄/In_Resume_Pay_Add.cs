﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class In_Resume_Pay_Add : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
            目的: 會費繳納現況
            日期: 2021-03-22 創建 (lina)
            */

            System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]In_Resume_Pay_Add";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "input dom: " + itmR.dom.InnerXml);

            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            Item itmPermit = inn.applyMethod("In_CheckIdentity", "<method>" + strMethodName + "</method><code>" + "ALL" + "</code>");
            bool isMeetingAdmin = itmPermit.getProperty("isMeetingAdmin", "") == "1";

            if (!isMeetingAdmin) throw new Exception("無權限");

            string aml = "";
            string sql = "";

            sql = @"
                SELECT 
	                id
	                , in_annual			AS 'in_tw_year'
	                , YEAR(GETDATE())	AS 'in_west_year' 
                FROM 
	                IN_MEETING WITH(NOLOCK)
                WHERE 
	                in_meeting_type = 'payment' 
	                AND in_annual = (YEAR(GETDATE()) - 1911)
            ";

            Item itmMeetings = inn.applySQL(sql);

            int meeting_count = itmMeetings.getItemCount();

            for (int i = 0; i < meeting_count; i++)
            {
                Item itmMeeting = itmMeetings.getItemByIndex(i);
                AddResumePay(CCO, strMethodName, inn, itmMeeting);
            }

            //把身分換回來
            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

            return itmR;
        }

        private void AddResumePay(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmMeeting)
        {
            string meeting_id = itmMeeting.getProperty("id", "");
            string in_tw_year = itmMeeting.getProperty("in_tw_year", "");
            string in_west_year = itmMeeting.getProperty("in_west_year", "");

            string sql = @"
                SELECT
                    t1.id
	                , t1.in_creator
	                , t1.in_creator_sno
	                , t1.item_number
	                , t1.money_transfer_10
	                , t1.in_collection_agency
	                , t1.in_pay_amount_exp
	                , t1.in_pay_amount_real
	                , CONVERT(NVARCHAR, t1.in_pay_date_real, 126) AS 'in_pay_date_real'
	                , CONVERT(NVARCHAR, t1.in_collection_date, 126) AS 'in_collection_date'
	                , t2.id AS 'resume_id'
                FROM 
	                IN_MEETING_PAY t1 WITH(NOLOCK)
                INNER JOIN
	                IN_RESUME t2 WITH(NOLOCK)
	                ON t2.in_sno = t1.in_creator_sno
                WHERE 
	                t1.in_meeting = '{#meeting_id}' 
	                AND t1.pay_bool = N'已繳費'
                    AND t1.item_number = 'IMP-00007319'
            ";

            sql = sql.Replace("{#meeting_id}", meeting_id);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item itmMPays = inn.applySQL(sql);

            Dictionary<string, List<Item>> map = GetMPayDetail(CCO, strMethodName, inn, meeting_id);

            int pay_count = itmMPays.getItemCount();

            for (int i = 0; i < pay_count; i++)
            {
                Item itmMPay = itmMPays.getItemByIndex(i);

                var in_pay_date_real = MapDay(itmMPay.getProperty("in_pay_date_real", ""));
                var tw_day = (in_pay_date_real.Year - 1911).ToString().PadLeft(3, '0')
                    + "/" + in_pay_date_real.Month
                    + "/" + in_pay_date_real.Day;

                Item itmData = inn.newItem();
                itmData.setType("IN_MEETING_PAY");
                itmData.setProperty(itmMPay.getProperty("id", ""), "");
                itmData.setProperty(in_pay_date_real.ToString("yyyy/MM/dd"), "");
                itmData.apply("in_resume_update_payStatus");

                //string id = itmMPay.getProperty("id", "");
                //if (map.ContainsKey(id))
                //{
                //    List<Item> list = map[id];
                //    for (int j = 0; j < list.Count; j++)
                //    {
                //        Item itmDetail = list[j];
                //        AppendResumePay(CCO, strMethodName, inn, itmMPay, itmDetail);
                //    }
                //}
            }
        }

        private DateTime MapDay(string value)
        {
            DateTime result = DateTime.MinValue;
            if (DateTime.TryParse(value, out result))
            {
                return result;
            }
            else
            {
                return DateTime.MinValue;
            }
        }

        private void AppendResumePay(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmMPay, Item itmDetail)
        {
            string item_number = itmMPay.getProperty("item_number", "");
            string resume_id = itmMPay.getProperty("resume_id", "");
            string money_transfer_10 = itmMPay.getProperty("money_transfer_10", "");
            //代收機構
            string in_collection_agency = itmMPay.getProperty("in_collection_agency", "");

            string in_pay_date_real = itmMPay.getProperty("in_pay_date_real", "");
            string in_collection_date = itmMPay.getProperty("in_collection_date", "");

            string in_pay_amount_exp = itmMPay.getProperty("in_pay_amount_exp", "");
            string in_pay_amount_real = itmMPay.getProperty("in_pay_amount_real", "");

            string in_pay_date = "";
            string in_pay_amount = "";
            string in_note = "";

            if (in_pay_date_real != "")
            {
                in_pay_date = in_pay_date_real;
                in_pay_amount = in_pay_amount_real;
                in_note = "";
            }
            else
            {
                in_pay_date = in_collection_date;
                in_pay_amount = in_pay_amount_exp;
                in_note = "代收";
            }

            //入會註記
            string in_l1 = itmDetail.getProperty("in_l1", "");

            //年度
            string in_west_year = itmDetail.getProperty("in_l2", "");
            string in_pay_ycol = "";
            string in_pay_yval = "1";
            switch (in_west_year)
            {
                case "2019":
                    in_pay_ycol = "in_pay_year3";
                    break;

                case "2020":
                    in_pay_ycol = "in_pay_year2";
                    break;

                case "2021":
                    in_pay_ycol = "in_pay_year1";
                    break;

                default:
                    CCO.Utilities.WriteDebug(strMethodName, "item_number: " + item_number);
                    break;
            }

            Item itmRPay = inn.newItem("In_Resume_Pay");

            itmRPay.setAttribute("where", "source_id = '" + resume_id + "'"
                + " AND in_year = '" + in_west_year + "'"
                + " AND in_paynumber = '" + item_number + "'");

            itmRPay.setProperty("source_id", resume_id);
            itmRPay.setProperty("in_year", in_west_year);
            itmRPay.setProperty("in_date", in_pay_date);
            itmRPay.setProperty("in_amount", in_pay_amount);
            itmRPay.setProperty("in_note", in_note);
            itmRPay.setProperty("in_paynumber", item_number);

            itmRPay = itmRPay.apply("merge");

            if (itmRPay.isError())
            {
                CCO.Utilities.WriteDebug(strMethodName, "In_Resume_Pay merge error _# dom: " + itmRPay.dom.InnerXml);
            }
            else if (in_pay_ycol != "")
            {
                string sql_update = "UPDATE IN_RESUME SET in_member_last_year = '{#in_west_year}', {#year_col} = {#year_val} WHERE id = '{#resume_id}'";

                sql_update = sql_update.Replace("{#resume_id}", resume_id)
                    .Replace("{#in_west_year}", in_west_year)
                    .Replace("{#year_col}", in_pay_ycol)
                    .Replace("{#year_val}", in_pay_yval);

                //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql_update);

                inn.applySQL(sql_update);
            }
        }

        //取得繳費單明細
        private Dictionary<string, List<Item>> GetMPayDetail(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string meeting_id)
        {
            Dictionary<string, List<Item>> map = new Dictionary<string, List<Item>>();

            string sql = @"
                SELECT
	                t2.*
                FROM 
	                IN_MEETING_PAY t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_NEWS t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                WHERE
	                t1.in_meeting = '{#meeting_id}'
	                AND t1.pay_bool = N'已繳費'
            ";

            sql = sql.Replace("{#meeting_id}", meeting_id);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item items = inn.applySQL(sql);

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string source_id = item.getProperty("source_id", "");

                List<Item> list = null;

                if (map.ContainsKey(source_id))
                {
                    list = map[source_id];
                }
                else
                {
                    list = new List<Item>();
                    map.Add(source_id, list);
                }

                list.Add(item);
            }

            return map;
        }
    }
}