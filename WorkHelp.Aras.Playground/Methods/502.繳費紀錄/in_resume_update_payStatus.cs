﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;
using System.Net;

namespace WorkHelp.Aras.Playground.Methods.Pay
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class AAA_Empty : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的:更新繳費狀態 相關
                日期:
                    - 2021-10-05 調整會員狀態 (Lina)
                    - 2021-09-28 17:18 創建 (David)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            // method名稱
            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]in_resume_update_payStatus";

            // 暫時增加權限
            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            string aml = "";
            string sql = "";

            Item itmR = this;
            string str_r = "";
            Item rst = inn.newItem();

            // 系統使用者User ID
            string strUserId = inn.getUserID();
            // 系統使用者Identity ID
            string strIdentityId = inn.getUserAliases();

            string strItemTypeLabel = "";
            string queryItemType = "";

            try
            {

                // string In_Meeting_pay_id = "00595DC0383E402E91C4DF641B252737"; //測試用 
                string In_Meeting_pay_id = this.getProperty("id", "");
                string model = this.getProperty("model", "");            // 模式 未繳費pay/已繳費 payed
                string In_pay_date_real = this.getProperty("in_pay_date_real", ""); // 繳費日期 yyy(民國年)mmdd
                // CCO.Utilities.WriteDebug(strMethodName, "In_Meeting_pay_id:" + In_Meeting_pay_id);

                // if(model.ToLower().Trim().Equals("pay"))
                // {
                //     CCO.Utilities.WriteDebug(strMethodName, "is 未繳費模式");
                // }
                // else if(model.ToLower().Trim().Equals("payed")){
                //     CCO.Utilities.WriteDebug(strMethodName, "is 繳費模式");
                // }
                // else{
                //     CCO.Utilities.WriteDebug(strMethodName, "is other模式");
                // }

                // Item itmMPay = inn.applySQL("SELECT * FROM IN_MEETING_PAY WITH(NOLOCK) WHERE id = '" + In_Meeting_pay_id + "'");
                // if (!itmMPay.isError())
                // {
                //     string in_paynumber = itmMPay.getProperty("item_number", "");
                //     string sql_delete = "DELETE FROM IN_RESUME_PAY WHERE ISNULL(in_paynumber, '') = '" + in_paynumber + "'";
                //     //CCO.Utilities.WriteDebug(strMethodName, sql_delete);
                //     inn.applySQL(sql_delete);
                // }    

                //繳費資訊
                Item ItemPayListInf = GetPayInf(CCO, inn, strMethodName, In_Meeting_pay_id);

                Update_In_Resume_Pay(CCO, inn, strMethodName, ItemPayListInf, In_pay_date_real);//更新 會員繳納資訊

                Update_In_Resume_YearsPayStatus(CCO, inn, strMethodName, ItemPayListInf, In_pay_date_real);//更新 學員履歷 年度繳費欄位
                Update_In_Resume_OtherColumns(CCO, inn, strMethodName, ItemPayListInf);//更新 學員履歷 其他欄位判斷

            }
            catch (Exception ex)
            {
                CCO.Utilities.WriteDebug(strMethodName, ex.ToString());
                string strFullMethodInfo = "[" + this.getType() + "]." + this.getID() + ":" + strMethodName;
                string strError = ex.Message + "\n";
                if (aml != "")
                    strError += "無法執行AML:" + aml + "\n";
                if (sql != "")
                    strError += "無法執行SQL:" + sql + "\n";
                string strErrorDetail = "";
                strErrorDetail = strFullMethodInfo + "\n" + strError + "\n" + ex.ToString() + "\n" + ex.StackTrace.ToString();
                _InnH.AddLog(strErrorDetail, "Error");
                strError = strError.Replace("\n", "</br>");
                throw new Exception(_InnH.Translate(strError));
            }
            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);
            return itmR;
        }


        private Item GetPayInf(Aras.Server.Core.CallContext CCO, Innovator inn, string strMethodName, string In_Meeting_pay_id)
        {
            string sql = @"
                SELECT
                    t1.ITEM_NUMBER AS 'item_number'
                    , t1.in_pay_amount_exp as 'pay_total_mount'
                    , t2.IN_L2 AS 'pay_year'
                    , t2.IN_SNO AS 'in_sno'
                    , t2.IN_PAY_AMOUNT as 'pay_mount'
                    , t2.IN_ANS_L3 as 'pay_group'
                    , t3.IN_MEETING_TYPE AS 'pay_type'
                    , t4.id as 'in_resume_id'
	                , t4.IN_NAME as 'in_resume_name'
                    , t4.in_member_status AS 'resume_member_status'
                FROM 
                    In_Meeting_pay t1 WITH(NOLOCK)
                LEFT JOIN
                    In_Meeting_news t2 WITH(NOLOCK)
                    ON t1.id = t2.SOURCE_ID
                JOIN 
                    In_Meeting t3 with(nolock)
                    ON t1.in_meeting = t3.id
                LEFT JOIN 
                    IN_RESUME t4 WITH(NOLOCK)
	                on t2.IN_SNO = t4.IN_SNO
                WHERE 
                    t1.id = '{#pay_id}'
            ";

            sql = sql.Replace("{#pay_id}", In_Meeting_pay_id);
            // CCO.Utilities.WriteDebug(strMethodName, "sql:" + sql);

            Item itmReturn = inn.applySQL(sql);
            
            return itmReturn;
        }


        private Item GetInResumePayInf(Aras.Server.Core.CallContext CCO, Innovator inn, string strMethodName, string in_resume_id)
        {
            string sql = string.Format(
                @"
        SELECT 
            T2.*
        FROM
            IN_RESUME T1 WITH(NOLOCK)
        LEFT JOIN In_Resume_Pay T2 WITH(NOLOCK)
            ON T1.id = T2.SOURCE_ID
        WHERE T1.id='{0}'
        "
                , in_resume_id
            );
            // CCO.Utilities.WriteDebug(strMethodName, "sql:" + sql);
            Item itmReturn = inn.applySQL(sql);
            return itmReturn;
        }


        /// <summary>
        /// 更新 會員繳納資訊
        /// </summary>
        /// <param name="CCO"></param>
        /// <param name="inn"></param>
        /// <param name="strMethodName"></param>
        /// <param name="ItemData">繳費單資料</param>
        /// <param name="In_pay_date_real">繳費日期</param>
        private void Update_In_Resume_Pay(Aras.Server.Core.CallContext CCO, Innovator inn, string strMethodName, Item ItemData, string In_pay_date_real)
        {
            int DataCount = ItemData.getItemCount();

            for (int i = 0; i < DataCount; i++)
            {
                Item iData = ItemData.getItemByIndex(i);
                DateTime pay_date = GetSiWanYear(In_pay_date_real);         // 繳費日期
                string item_number = iData.getProperty("item_number", "");    // 繳費單號
                string pay_year = iData.getProperty("pay_year", "");       // 繳費年度(西元)
                string in_sno = iData.getProperty("in_sno", "");         // 身分證
                string pay_mount = iData.getProperty("pay_mount", "");      // 繳費金額
                string pay_group = iData.getProperty("pay_group", "");      // 繳費組別
                string pay_type = iData.getProperty("pay_type", "");       // 繳費類別
                string in_resume_id = iData.getProperty("in_resume_id", "");   // 講師履歷ID
                string in_resume_name = iData.getProperty("in_resume_name", ""); // 講師履歷 姓名

                //須為會費
                if (!pay_type.Trim().ToLower().Equals("payment"))
                    continue;

                string whereConditionString = string.Format(
                    @"
            In_Resume_Pay.[source_id]='{0}' and In_Resume_Pay.[in_year]='{1}' and In_Resume_Pay.[in_paynumber]='{2}' {3}
            "
                    , in_resume_id //講師履歷ID
                    , pay_year //繳費年度
                    , item_number //學員編號
                    , pay_group.Contains("入會費") ? "and In_Resume_Pay.[in_l1]='入會'" : "and ISNULL(In_Resume_Pay.[in_l1], '')=''" //判斷是否有入會費 相關
                );

                string aml = string.Format(
                    @"
            <AML>
                <Item type='In_Resume_Pay' action='merge' where=""{0}"">
                <source_id>{1}</source_id>
                <in_date>{2}</in_date>
                <in_year>{3}</in_year>
                <in_amount>{4}</in_amount>
                <in_paynumber>{5}</in_paynumber>
                {6}
                </Item>
            </AML>

            "
                    , whereConditionString
                    , in_resume_id //講師履歷ID
                    , pay_date.ToString("yyyy-MM-dd") + "T" + pay_date.ToString("HH:mm:ss")//繳費日期
                    , pay_year //繳費年度 
                    , pay_mount //繳費金額
                    , item_number //繳費單號
                    , pay_group.Contains("入會費") ? "<in_l1>入會</in_l1>" : "" //判斷是否有入會費 相關
                );

                // CCO.Utilities.WriteDebug(strMethodName, aml);
                Item result = inn.applyAML(aml);
                // CCO.Utilities.WriteDebug(strMethodName, result.dom.InnerXml);

            }
        }

        /// <summary>
        /// 更新 學員履歷 年度時間
        /// </summary>
        /// <param name="CCO"></param>
        /// <param name="inn"></param>
        /// <param name="strMethodName"></param>
        /// <param name="ItemData">繳費單資料</param>
        /// <param name="In_pay_date_real">繳費日期</param>
        private void Update_In_Resume_YearsPayStatus(Aras.Server.Core.CallContext CCO, Innovator inn, string strMethodName, Item ItemData, string In_pay_date_real)
        {
            int DataCount = ItemData.getItemCount();

            for (int i = 0; i < DataCount; i++)
            {
                Item iData = ItemData.getItemByIndex(i);
                DateTime pay_date = GetSiWanYear(In_pay_date_real);// 繳費日期
                string item_number = iData.getProperty("item_number", "");//繳費單號
                string pay_year = iData.getProperty("pay_year", "");//繳費年度(西元)
                string in_sno = iData.getProperty("in_sno", "");//身分證
                string pay_mount = iData.getProperty("pay_mount", "");//繳費金額
                string pay_group = iData.getProperty("pay_group", "");      // 繳費組別
                string pay_type = iData.getProperty("pay_type", "");//繳費類別
                string in_resume_id = iData.getProperty("in_resume_id", "");//講師履歷ID
                string in_resume_name = iData.getProperty("in_resume_name", "");//講師履歷 姓名

                //須為會費
                if (!pay_type.Trim().ToLower().Equals("payment"))
                    continue;


                string sql = string.Format(
                    @"
                UPDATE IN_RESUME
                SET {1} = '{2}'
                WHERE id='{0}';
            "
                    , in_resume_id //講師履歷ID
                    , GetYearTo_InResume_ColumnName(pay_year) //更新欄位
                    , "1" //Update value 

                );

                // CCO.Utilities.WriteDebug(strMethodName, sql);
                Item result = inn.applySQL(sql);
                // CCO.Utilities.WriteDebug(strMethodName, result.dom.InnerXml);
            }
        }


        /// <summary>
        /// 更新 學員履歷 其他欄位判斷
        /// </summary>
        /// <param name="CCO"></param>
        /// <param name="inn"></param>
        /// <param name="strMethodName"></param>
        /// <param name="ItemData">繳費單資料</param>
        private void Update_In_Resume_OtherColumns(Aras.Server.Core.CallContext CCO, Innovator inn, string strMethodName, Item ItemData)
        {
            int DataCount = ItemData.getItemCount();

            List<string> YearsBox = new List<string>();//已繳費年度 + 繳費單繳費年度
            string in_resume_id = ""; // 講師履歷ID
            string pay_total_mount = ""; // 應繳金額
            string item_number = ""; // 繳費單號
            string in_sno = ""; // 身分證
            string in_member_last_year = ""; // 最後繳費年度
            string in_member_status = ""; // 會員狀態
            string resume_member_status = ""; // 會員狀態


            bool isHavePaymentData = false;

            //先判斷是否可執行 並且取得必要資訊
            for (int i = 0; i < DataCount; i++)
            {
                Item iData = ItemData.getItemByIndex(i);
                string pay_type = iData.getProperty("pay_type", "");//繳費類別
                                                                    //須為會費
                if (!pay_type.Trim().ToLower().Equals("payment"))
                    continue;

                isHavePaymentData = true;
                in_resume_id = iData.getProperty("in_resume_id", "");    // 講師履歷ID
                pay_total_mount = iData.getProperty("pay_total_mount", ""); // 應繳金額
                item_number = iData.getProperty("item_number", "");     // 繳費單號
                in_sno = iData.getProperty("in_sno", "");          // 身分證
                resume_member_status = iData.getProperty("resume_member_status", ""); // Resume 會員狀態
            }

            //如果繳費單都沒有會費相關的狀態 直接跳離
            if (!isHavePaymentData)
                return;

            //空字串 跳離
            if (in_resume_id.Equals(""))
                return;

            //取得原有的Insume
            Item ItemInResumePayInf = GetInResumePayInf(CCO, inn, strMethodName, in_resume_id);
            int ItemInResumePayInfCount = ItemInResumePayInf.getItemCount();
            //必須可以查到講師履歷相關資料
            if (0 == ItemInResumePayInfCount)
                return;

            //處理講師履歷已繳費年度
            for (int i = 0; i < ItemInResumePayInfCount; i++)
            {
                Item iData = ItemInResumePayInf.getItemByIndex(i);
                string in_year = iData.getProperty("in_year", "");//繳費年度(西元)
                if (!in_year.Equals(""))
                    YearsBox.Add(in_year);
            }


            //處理繳費單 繳費年度
            for (int i = 0; i < DataCount; i++)
            {
                Item iData = ItemData.getItemByIndex(i);
                string pay_type = iData.getProperty("pay_type", ""); // 繳費類別
                string pay_year = iData.getProperty("pay_year", ""); // 繳費年度(西元)
                                                                     //須為會費
                if (!pay_type.Trim().ToLower().Equals("payment"))
                    continue;
                YearsBox.Add(pay_year);
            }

            //刪除重複值
            YearsBox = YearsBox.Distinct().ToList();
            // foreach (var Value in YearsBox)
            // {
            //     CCO.Utilities.WriteDebug(strMethodName, "YearsBox Value: " + Value);
            // }
            //算出最大繳費年度
            in_member_last_year = YearsBox.Max();

            string sql = "";
            bool is_one_year = CheckIsOneYearPassMemeber(in_sno);
            
            if (resume_member_status == "暫時會員")
            {
                //不更改會員狀態
                sql = "UPDATE IN_RESUME SET"
                    + " in_member_pay_no = '" + item_number + "'"
                    + ", in_member_last_year = '" + in_member_last_year + "'"
                    + ", in_member_pay_amount = '" + pay_total_mount + "'"
                    + " WHERE"
                    + " id = '" + in_resume_id + "'";
            }
            else if (CheckMemberQualification(YearsBox, is_one_year))
            {
                //有繳費資格
                // CCO.Utilities.WriteDebug(strMethodName, "有繳費資格");

                in_member_status = "合格會員";
                sql = string.Format(
                    @"
                Update IN_RESUME
                set IN_MEMBER_PAY_NO='{2}', IN_MEMBER_LAST_YEAR ='{3}', IN_MEMBER_PAY_AMOUNT ='{1}', In_Member_Status='{4}'
                where id = '{0}';
            "
                    , in_resume_id         // 講師履歷ID
                    , pay_total_mount      // 應繳金額
                    , item_number          // 繳費單號
                    , in_member_last_year  // 最後繳費年度
                    , in_member_status     // 會員狀態
                );
            }
            else
            {
                sql = string.Format(
                    @"
                Update IN_RESUME
                set IN_MEMBER_PAY_NO='{2}', IN_MEMBER_LAST_YEAR ='{3}', IN_MEMBER_PAY_AMOUNT ='{1}'
                where id = '{0}';
            "
                    , in_resume_id         // 講師履歷ID
                    , pay_total_mount      // 應繳金額
                    , item_number          // 繳費單號
                    , in_member_last_year  // 最後繳費年度
                    , in_member_status     // 會員狀態
                );
            }

            CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);
            
            Item result = inn.applySQL(sql);
        }

        /// <summary>
        /// 判斷是否有合格會員的資格
        /// </summary>
        /// <param name="YearsBox"></param>
        /// <param name="isOneYearCondition">是否為[合格會員 一年判斷]</param>
        /// <returns></returns>
        private bool CheckMemberQualification(List<string> YearsBox, bool isOneYearCondition)
        {
            bool Result = false;

            bool checkYear = false;
            bool checkPre1Year = false;

            DateTime thisYear = DateTime.Today;
            string rYear = thisYear.Year.ToString();
            string rPre1Year = (thisYear.Year - 1).ToString();
            // CCO.Utilities.WriteDebug(strMethodName, "系統年分:" + rYear);
            // CCO.Utilities.WriteDebug(strMethodName, "系統年分:" + rPre1Year);

            foreach (var value in YearsBox)
            {
                // CCO.Utilities.WriteDebug(strMethodName, "YearsBox 比對年分:" + value);
                if (value.Equals(rYear))
                    checkYear = true;
                if (value.Equals(rPre1Year))
                    checkPre1Year = true;
            }

            if (isOneYearCondition)
            {
                ;//此邏輯 不採用 in 2021/10/01
                 // //是否有 合格會員的資格 直接判斷 近一年要繳費
                 // if(checkYear )
                 // {
                 //     Result = true;
                 // }
            }
            else
            {
                //是否有 合格會員的資格 直接判斷 近兩年要繳費
                if (checkYear && checkPre1Year)
                {
                    Result = true;
                }
            }

            return Result;
        }

        /// <summary>
        /// 判斷是否為 判斷一年繳費的 會員 
        /// </summary>
        /// <param name="in_sno">身分證</param>
        /// <returns>ture一年/false其他</returns>
        private bool CheckIsOneYearPassMemeber(string in_sno)
        {
            bool result = false;

            if (6 == in_sno.Length)
            {
                if (in_sno.Substring(0, 1).ToUpper().Equals("T"))
                {
                    result = true;
                }
            }
            return result;
        }

        /// <summary>
        /// 回傳西元年度 對應講師履歷 年度欄位
        /// </summary>
        /// <param name="rYear"></param>
        /// <returns></returns>
        private string GetYearTo_InResume_ColumnName(string rYear)
        {
            string returnStr = "";
            switch (rYear)
            {
                case "2021":
                    returnStr = "in_pay_year1";
                    break;
                case "2020":
                    returnStr = "in_pay_year2";
                    break;
                case "2019":
                    returnStr = "in_pay_year3";
                    break;
                case "2022":
                    returnStr = "in_pay_year4";
                    break;
                case "2023":
                    returnStr = "in_pay_year5";
                    break;
                case "2024":
                    returnStr = "in_pay_year6";
                    break;
                case "2025":
                    returnStr = "in_pay_year7";
                    break;
                case "2026":
                    returnStr = "in_pay_year8";
                    break;
                case "2027":
                    returnStr = "in_pay_year9";
                    break;
                case "2028":
                    returnStr = "in_pay_year10";
                    break;
                case "2029":
                    returnStr = "in_pay_year11";
                    break;
                case "2030":
                    returnStr = "in_pay_year12";
                    break;
                case "2031":
                    returnStr = "in_pay_year13";
                    break;

                default:
                    returnStr = "in_pay_year1";
                    break;
            }
            return returnStr;
        }

        private DateTime GetSiWanYear(string rYear)
        {
            //將民國年轉換成西元年
            DateTime dt = DateTime.ParseExact(rYear, "yyyMMdd", CultureInfo.InvariantCulture).AddYears(1911);
            return dt;

        }
    }
}