﻿using System;
using System.Collections.Generic;
using WorkHelp.Aras.Playground.ArasModules;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class In_Meeting_ExportUser_Excel : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
			目的: 
				- 產生可供匯入與會者的 Excel 模板
			說明:
				1.產生Sheet(Current)，欄位 登入帳號 單位名稱 聯絡地址 聯絡電話
				2.產生Sheet(help)，提供欄位說明
				3.產生xlsx檔案
			輸入:
				- meeting_id
			輸出: 
				- 與會者匯入檔.xlsx
			位置: 
				- In_Meeting_Current_Import.html
			日期: 
				2021-03-09 改寫 (Lina)
			*/

            System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]Fix_MemberIdentity";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "input dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                mode = itmR.getProperty("mode", ""),

                mtName = "IN_MEETING",
                muName = "IN_MEETING_USER",
                msName = "IN_MEETING_SURVEYS",
                mtProperty = "in_meeting",
                svName = "IN_SURVEY",
                soName = "IN_SURVEY_OPTION",
            };

            if (cfg.mode == "cla")
            {
                cfg.mtName = "IN_CLA_MEETING";
                cfg.muName = "IN_CLA_MEETING_USER";
                cfg.msName = "IN_CLA_MEETING_SURVEYS";
                cfg.mtProperty = "in_cla_meeting";
                cfg.svName = "IN_CLA_SURVEY";
                cfg.soName = "IN_CLA_SURVEY_OPTION";
            }

            if (cfg.meeting_id == "")
            {
                throw new Exception("活動 id 不可空白");
            }

            cfg.itmMeeting = cfg.inn.applySQL("SELECT * FROM " + cfg.mtName + " WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");

            TExport export = GetExportInfo(cfg);
            List<TField> fields = GetFields(cfg);

            ClosedXML.Excel.XLWorkbook wb = new ClosedXML.Excel.XLWorkbook(export.TemplatePath);
            //附加與會者 Sheet
            AppendMUserSheet(wb, cfg, fields);
            //附加說明 Sheet
            AppendHelpheet(wb, cfg, fields);
            wb.SaveAs(export.OutputPath);

            itmR.setProperty("xls_name", export.OutputUrl);

            return itmR;

        }

        private void AppendMUserSheet(ClosedXML.Excel.XLWorkbook wb, TConfig cfg, List<TField> fields)
        {
            ClosedXML.Excel.IXLWorksheet sheet = wb.Worksheet(1);

            int wsRow = 1;
            int wsCol = 1;

            for (int i = 0; i < fields.Count; i++)
            {
                TField field = fields[i];

                ClosedXML.Excel.IXLCell cell_property = sheet.Cell(wsRow, wsCol + i);
                ClosedXML.Excel.IXLCell cell_title = sheet.Cell(wsRow + 1, wsCol + i);

                cell_property.Value = field.property;
                cell_property.Value = field.title;
            }
        }

        private void AppendHelpheet(ClosedXML.Excel.XLWorkbook wb, TConfig cfg, List<TField> fields)
        {
            ClosedXML.Excel.IXLWorksheet sheet = wb.Worksheet(2);

            int wsRow = 3;

            for (int i = 0; i < fields.Count; i++)
            {
                TField field = fields[i];

                ClosedXML.Excel.IXLCell cell_title = sheet.Cell(wsRow + i, 1);
                ClosedXML.Excel.IXLCell cell_help = sheet.Cell(wsRow + i, 2);

                cell_title.Value = field.title;
                cell_title.Style.Alignment.WrapText = true;

                cell_help.Value = field.help;
                cell_help.Style.Alignment.WrapText = true;
            }
        }

        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }

            public string meeting_id { get; set; }
            public string mode { get; set; }

            public string mtName { get; set; }
            public string muName { get; set; }
            public string msName { get; set; }
            public string mtProperty { get; set; }
            public string svName { get; set; }
            public string soName { get; set; }

            public Item itmMeeting { get; set; }

        }

        private class TExport
        {
            public string TemplatePath { get; set; }

            public string FileFold { get; set; }
            public string FileName { get; set; }
            public string FileExt { get; set; }

            public string OutputPath { get; set; }
            public string OutputUrl { get; set; }
        }

        private class TField
        {
            public string survey_id { get; set; }

            public string property { get; set; }
            public string title { get; set; }
            public string type { get; set; }
            public string options { get; set; }
            public string help { get; set; }
        }

        private List<TField> GetFields(TConfig cfg)
        {
            string sql = @"
				SELECT 
					t2.*
				FROM 
					{#msName} t1 WITH(NOLOCK)
				INNER JOIN 
					{#svName} t2 WITH(NOLOCK) ON t2.id = t1.related_id
				WHERE 
					t1.source_id = '{#meeting_id}'
					AND ISNULL(t2.in_property, '') NOT IN ('', 'in_group', 'in_creator', 'in_creator_sno', 'in_index')
				ORDER by 
					t1.sort_order
			";

            sql = sql.Replace("{#msName}", cfg.msName)
                .Replace("{#svName}", cfg.svName)
                .Replace("{#meeting_id}", cfg.meeting_id);

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();

            List<TField> list = new List<TField>();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string survey_id = item.getProperty("id", "");
                string in_property = item.getProperty("in_property", "");
                string in_questions = item.getProperty("in_questions", "");
                string in_question_type = item.getProperty("in_question_type", "");
                string in_selectoption = item.getProperty("in_selectoption", "");

                TField field = new TField
                {
                    survey_id = survey_id,
                    property = in_property,
                    title = in_questions,
                    type = in_question_type,
                    options = in_selectoption,
                };

                switch (field.type)
                {
                    case "single_value":
                    case "single_value_dropdown":
                        field.help = "必須是以下備選項目其中之一:\n" + GetOptions(cfg, field);
                        break;

                    case "date":
                        field.help = "日期格式,例如:2019/05/30 08:12:00\n亦可為系統格式:2019-05-30T08:12:00";
                        break;

                    case "single_tel_text":
                        field.help = "輸入文字,區碼-電話,例如:02-12345678";
                        break;

                    default:
                        field.help = "輸入文字";
                        break;
                }

                switch (field.property)
                {
                    case "in_sno":
                        field.help = "輸入文字,長度[4][6][8-12],格式必須為英文與數字組合";
                        break;

                    case "in_birth":
                        field.help = "日期格式,例如:2019/05/30 \n亦可為系統格式:2019-05-30";
                        break;

                    case "in_tel":
                        field.help = "輸入文字,手機號碼,例如:0900-123456";
                        break;

                    case "in_tel_1":
                        field.help = "輸入文字,區碼-電話,例如:02-12345678";
                        break;
                }

                list.Add(field);
            }

            return list;
        }

        private string GetOptions(TConfig cfg, TField field)
        {
            List<string> list = new List<string>();
            if (field.options != "")
            {
                string[] arr = field.options.Split(new char[] { '@' }, StringSplitOptions.RemoveEmptyEntries);
                if (arr != null && arr.Length > 0)
                {
                    foreach (var option in arr)
                    {
                        var value = option.Trim();
                        if (value!= "")
                        { 
                            list.Add(value);
                        }
                    }
                }
            }
            else
            {
                string sql = @"SELECT * FROM " + cfg.soName + " WITH(NOLOCK) WHERE source_id = '" + field.survey_id + "' ORDER BY sort_order";
                Item items = cfg.inn.applySQL(sql);
                int count = items.getItemCount();
                for (int i = 0; i < count; i++)
                {
                    Item item = items.getItemByIndex(i);
                    string in_filter = item.getProperty("in_filter", "").Trim();
                    string in_value = item.getProperty("in_value", "").Trim();
                    string in_label = item.getProperty("in_label", "").Trim();

                    var value = in_value.Trim();
                    if (value != "")
                    {
                        list.Add(value);
                    }
                }
            }

            return string.Join(", ", list);
        }

        private TExport GetExportInfo(TConfig cfg)
        {
            //開始整理EXCEL Template
            string aml = "<AML>" +
                "<Item type='In_Variable' action='get'>" +
                "<in_name>meeting_excel</in_name>" +
                "<Relationships>" +
                "<Item type='In_Variable_Detail' action='get'/>" +
                "</Relationships>" +
                "</Item></AML>";

            Item Vairable = cfg.inn.applyAML(aml);

            string Template_Path = "";
            string Export_Path = "";

            for (int i = 0; i < Vairable.getRelationships("In_Variable_Detail").getItemCount(); i++)
            {
                Item In_Variable_Detail = Vairable.getRelationships("In_Variable_Detail").getItemByIndex(i);
                if (In_Variable_Detail.getProperty("in_name", "") == "current_template_path")
                    Template_Path = In_Variable_Detail.getProperty("in_value", "");

                if (In_Variable_Detail.getProperty("in_name", "") == "export_path")
                    Export_Path = In_Variable_Detail.getProperty("in_value", "");
                if (!Export_Path.EndsWith(@"\"))
                    Export_Path = Export_Path + @"\";
            }

            TExport export = new TExport
            {
                TemplatePath = Template_Path,
                FileFold = Export_Path,
                FileName = "與會者匯入檔",
                FileExt = ".xlsx"
            };

            export.OutputPath = export.FileFold.TrimEnd('\\') + "\\" + export.FileName + export.FileExt;
            export.OutputUrl = export.FileName + export.FileExt;

            return export;
        }
    }
}