﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;
using System.Net;

namespace WorkHelp.Aras.Playground.Methods.PlmCta
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class In_Meeting_InsUser : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: excel匯入與會者
                說明: 快速建立與會者資訊
                輸入: 
                輸出:
                位置: In_Meeting_User_Import.html
                做法:
                日期: 
                    - 2021/09/22 個人帳號補刀 (Lina)
                    - 2021/03/10 創建 (Alan)
            */

            //System.Diagnostics.Debugger.Break();

            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]In_Meeting_InsUser";

            _InnH.AddLog(strMethodName, "MethodSteps");
            Item itmR = this;

            //登入者 Resume
            Item itmLoginResume = inn.newItem("In_Resume", "get");
            itmLoginResume.setProperty("in_user_id", inn.getUserID());
            itmLoginResume = itmLoginResume.apply();

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                strIndId = inn.getUserAliases(),
                login_resume_id = itmLoginResume.getProperty("id", ""),
                login_resume_current_org = itmLoginResume.getProperty("in_current_org", ""),
                login_resume_group = itmLoginResume.getProperty("in_group", ""),
                in_creator = itmLoginResume.getProperty("in_name", ""),
                meeting_id = itmR.getProperty("meeting_id", ""),
                mode = itmR.getProperty("mode", ""),
                in_sno = itmR.getProperty("in_sno", ""),
                isPayAdd = itmR.getProperty("isPayAdd", ""),
                needUpdateResume = itmR.getProperty("in_update_resume", "") == "是",

                mtName = "IN_MEETING",
                muName = "IN_MEETING_USER",
                msName = "IN_MEETING_SURVEYS",
                mtProperty = "in_meeting",
                svName = "IN_SURVEY",
                soName = "IN_SURVEY_OPTION",
            };

            if (cfg.mode == "cla")
            {
                cfg.mtName = "IN_CLA_MEETING";
                cfg.muName = "IN_CLA_MEETING_USER";
                cfg.msName = "IN_CLA_MEETING_SURVEYS";
                cfg.mtProperty = "in_cla_meeting";
                cfg.svName = "IN_CLA_SURVEY";
                cfg.soName = "IN_CLA_SURVEY_OPTION";
            }

            if (cfg.meeting_id == "")
            {
                throw new Exception("活動 id 不可空白");
            }

            //個人帳號註冊
            cfg.isOneRegister = cfg.meeting_id == "249FDB244E534EB0AA66C8E9C470E930";

            if (cfg.isOneRegister)
            {
                Item itmMUser = inn.newItem("In_Meeting_User", "get");
                itmMUser.setProperty("source_id", cfg.meeting_id);
                itmMUser.setProperty("in_sno", itmR.getProperty("in_sno", ""));
                itmMUser = itmMUser.apply();

                //新增與會者
                if (itmMUser.isError() || itmMUser.getResult() == "")
                {
                    InsUserFields(cfg, itmR);
                }

                if (cfg.needUpdateResume)
                {
                    FixResumeProperty(cfg, itmR);
                }
            }
            else
            {
                //新增與會者
                InsUserFields(cfg, itmR);
            }

            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);
            return itmR;

        }

        private void InsUserFields(TConfig cfg, Item itmR)
        {
            string sql = @"
		        SELECT 
			        t2.*
		        FROM 
			        {#msName} t1 WITH(NOLOCK)
		        INNER JOIN 
			        {#svName} t2 WITH(NOLOCK) ON t2.id = t1.related_id
		        WHERE 
			        t1.source_id = '{#meeting_id}'
			        AND ISNULL(t2.in_property, '') NOT IN ('', 'in_creator', 'in_creator_sno')
		        ORDER by 
			        t1.sort_order
	        ";

            sql = sql.Replace("{#msName}", cfg.msName)
                .Replace("{#svName}", cfg.svName)
                .Replace("{#meeting_id}", cfg.meeting_id);

            Item items = cfg.inn.applySQL(sql);
            int count = items.getItemCount();
            
            string parameter = "";
            string in_l1 = itmR.getProperty("in_l1", "");
            string in_l2 = itmR.getProperty("in_l2", "");
            string in_l3 = itmR.getProperty("in_l3", "");
            string in_current_org = itmR.getProperty("in_current_org", "");
            
            if (in_current_org == "")
            {
                in_current_org = cfg.login_resume_current_org;
            }

            string register_section_name = "";
            register_section_name += in_l1;
            if (in_l2 != "")
            {
                register_section_name += "-" + in_l2;
            }
            if (in_l3 != "")
            {
                register_section_name += "-" + in_l3;
            }

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string survey_id = item.getProperty("id", "");
                string in_property = item.getProperty("in_property", ""); //欄位屬性
                string in_questions = item.getProperty("in_questions", ""); //欄位名稱
                string in_question_type = item.getProperty("in_question_type", "");
                string in_selectoption = item.getProperty("in_selectoption", "");
                string in_request = item.getProperty("in_request", ""); //必填欄位
                string ins_val = itmR.getProperty(in_property, "").Trim();


                if (in_question_type == "date")
                {
                    DateTime DateVal = Convert.ToDateTime(ins_val);
                    ins_val = DateVal.ToString("yyyy-MM-ddTHH:mm:ss");
                }

                parameter += survey_id + "=" + ins_val + "&";
            }

            //共用
            Item mUser = cfg.inn.newItem("Method");
            mUser.setProperty("OnlyInsMUser", "0");

            string in_creator = itmR.getProperty("in_creator", "");
            string in_creator_sno = cfg.in_sno;
            if (in_creator != "")
            {
                cfg.in_creator = in_creator;
                cfg.in_sno = itmR.getProperty("in_creator_sno", "");
                in_creator_sno = itmR.getProperty("in_creator_sno", "");
                mUser.setProperty("current_user_name_sno", cfg.in_sno);
            }
            else
            {

                mUser.setProperty("isUserId", cfg.strUserId);
                Item CurrentUser = cfg.inn.getItemById("User", cfg.strUserId);
                //用偷藏的ID取得當前登入者
                cfg.in_creator = CurrentUser.getProperty("last_name", "");//協助報名者
                in_creator_sno = CurrentUser.getProperty("login_name", "");//協助報名者帳號
            }

            mUser.setProperty("isIndId", cfg.strIndId);
            mUser.setProperty("meeting_id", cfg.meeting_id);
            mUser.setProperty("method", "register_meeting");
            mUser.setProperty("login_resume_id", cfg.login_resume_id);
            mUser.setProperty("login_resume_current_org", cfg.login_resume_current_org);
            mUser.setProperty("login_resume_group", cfg.login_resume_group);
            //團體須研究 client_user_index in_index
            mUser.setProperty("client_user_index", "1");
            mUser.setProperty("in_index", "");
            mUser.setProperty("su_creator", in_creator);
            mUser.setProperty("su_creator_sno", in_creator_sno);

            if (cfg.mode == "cla")
            {
                parameter += "22531BA9D5284BBF96C8E648F8E257E9=";
                mUser.setProperty("parameter", parameter);
                mUser.setProperty("agent_id", "x");
                mUser.setProperty("surveytype", "1a");
                mUser.setProperty("mode", cfg.mode);
                mUser.setProperty("email", cfg.in_sno);

                itmR = mUser.apply("In_Cla_Meeting_Register");

                //自動產生繳費單
                if (cfg.isPayAdd == "1")
                {
                    //為了配合 In_Payment_List_Add AML 查詢，一定要把 繳費單號  更新成空白
                    //否則繳費單號一定產生失敗!!!!!!!!

                    string sql_update = @"UPDATE IN_CLA_MEETING_USER SET in_paynumber = N'' WHERE source_id = '" + cfg.meeting_id + "' AND in_paynumber IS NULL AND in_sno = '" + cfg.in_sno + "'";
                    cfg.inn.applySQL(sql_update);

                    Item itmResume = cfg.inn.applySQL("SELECT * FROM IN_RESUME WITH(NOLOCK) WHERE in_sno = '" + in_creator_sno + "'");

                    string resume_id = itmResume.getProperty("id", "");
                    string in_group = itmResume.getProperty("in_group", "");

                    // Item itmMClose = cfg.inn.newItem();
                    // itmMClose.setType("In_Meeting_GymList");
                    // itmMClose.setProperty("meeting_id", cfg.meeting_id);
                    // itmMClose.setProperty("in_group", in_group);
                    // Item itmMCloseResult = itmMClose.apply("In_Close_GymReg");

                    Item itmMPay = cfg.inn.newItem();
                    itmMPay.setType("In_Meeting_Pay");
                    itmMPay.setProperty("cla_meeting_id_group", cfg.meeting_id);
                    itmMPay.setProperty("in_group", in_group);
                    itmMPay.setProperty("in_creator_sno", in_creator_sno);
                    itmMPay.setProperty("current_orgs", "," + in_current_org);
                    itmMPay.setProperty("invoice_up", ",");
                    itmMPay.setProperty("uniform_numbers", ",");

                    Item itmMPayResult = itmMPay.apply("In_Payment_List_Add");
                }

            }
            else
            {
                parameter += "8466E57C08F64D8BB9AADF99A4F5C844=";
                mUser.setProperty("parameter", parameter);
                mUser.setProperty("in_locked", "1");
                mUser.setProperty("surveytype", "1");
                mUser.setProperty("in_creator", cfg.in_creator);
                mUser.setProperty("in_locked_id", cfg.in_sno);
                mUser.setProperty("in_l1", in_l1);
                mUser.setProperty("email", cfg.in_sno + "-" + register_section_name);
                mUser.setProperty("register_section_name", register_section_name);
                itmR = mUser.apply("In_meeting_register");

                //自動產生繳費單
                if (cfg.isPayAdd == "1")
                {
                    //為了配合 In_Payment_List_Add AML 查詢，一定要把 繳費單號  更新成空白
                    //否則繳費單號一定產生失敗!!!!!!!!

                    string sql_update = @"UPDATE IN_MEETING_USER SET in_paynumber = N'' WHERE source_id = '" + cfg.meeting_id + "' AND in_paynumber IS NULL AND in_sno = '" + cfg.in_sno + "'";
                    cfg.inn.applySQL(sql_update);

                    Item itmResume = cfg.inn.applySQL("SELECT * FROM IN_RESUME WITH(NOLOCK) WHERE in_sno = '" + in_creator_sno + "'");

                    string resume_id = itmResume.getProperty("id", "");
                    string in_group = itmResume.getProperty("in_group", "");

                    // Item itmMClose = cfg.inn.newItem();
                    // itmMClose.setType("In_Meeting_GymList");
                    // itmMClose.setProperty("meeting_id", cfg.meeting_id);
                    // itmMClose.setProperty("in_group", in_group);
                    // Item itmMCloseResult = itmMClose.apply("In_Close_GymReg");

                    Item itmMPay = cfg.inn.newItem();
                    itmMPay.setType("In_Meeting_Pay");
                    itmMPay.setProperty("meeting_id", cfg.meeting_id);
                    itmMPay.setProperty("in_group", in_group);
                    itmMPay.setProperty("in_creator_sno", in_creator_sno);
                    itmMPay.setProperty("resume_id", resume_id);
                    itmMPay.setProperty("current_orgs", "," + in_current_org);
                    itmMPay.setProperty("invoice_up", ",");
                    itmMPay.setProperty("uniform_numbers", ",");
                    Item itmMPayResult = itmMPay.apply("In_Payment_List_Add2");
                }
            }
        }

        //針對【個人帳號】匯入進行補刀
        private void FixResumeProperty(TConfig cfg, Item itmReturn)
        {
            //會員類型
            string in_member_type = GetMemberType(cfg, itmReturn.getProperty("in_member_type", "vip_mbr"));
            string in_member_status = itmReturn.getProperty("in_member_status", "暫時會員");
            string in_member_role = "sys_9999";

            //段位
            string in_degree_label = itmReturn.getProperty("in_degree_label", "");
            string in_degree = GetDegree(cfg, in_degree_label);

            //所屬委員會
            string in_committee = itmReturn.getProperty("in_committee", "");
            Item itmCommittee = GetCommittee(cfg, in_committee);
            if (itmCommittee.isError())
            {
                itmCommittee = cfg.inn.newItem();
            }

            //有值才更新
            Dictionary<string, string> map = new Dictionary<string, string>();
            map.Add("in_member_type", in_member_type);
            map.Add("in_member_role", in_member_role); 
            map.Add("in_member_status", in_member_status); 
            map.Add("in_degree", in_degree); 

            AddKV(map, "in_email", itmReturn);
            AddKV(map, "in_tel", itmReturn); 
            AddKV(map, "in_tel_1", itmReturn);
            AddKV(map, "in_add", itmReturn);
            AddKV(map, "in_resident_add", itmReturn);
            AddKV(map, "in_guardian", itmReturn);
            AddKV(map, "in_area", itmReturn);

            AddKV(map, "in_education", itmReturn);
            AddKV(map, "in_work_history", itmReturn);
            AddKV(map, "in_work_org", itmReturn);
            AddKV(map, "in_title", itmReturn);
            
            AddKV(map, "in_degree_label", itmReturn); 
            AddKV(map, "in_degree_id", itmReturn);
            AddKV(map, "in_stuff_c1", itmReturn);
            AddKV(map, "in_stuff_c2", itmReturn);

            AddKV(map, "in_group", itmReturn);
            AddKV(map, "in_current_org", itmReturn);

            map.Add("in_manager_name", in_committee);
            map.Add("in_manager_org", itmCommittee.getProperty("id", ""));
            map.Add("in_manager_area", itmCommittee.getProperty("in_manager_area", ""));

            StringBuilder builder = new StringBuilder();
            foreach(var kv in map)
            {
                if (builder.Length > 0) builder.Append(", ");
                builder.Append(kv.Key + " = '" + kv.Value +"'");
            }

            string sql = "UPDATE IN_RESUME SET " + builder.ToString() + " WHERE in_sno = '" + cfg.in_sno + "'";
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);
            Item itmUpdate = cfg.inn.applySQL(sql);

            if (itmUpdate.isError())
            {
                throw new Exception("【個人帳號匯入】修補 Resume 資料發生錯誤");
            }
        }

        //會員類型處理
        private string GetMemberType(TConfig cfg, string in_member_type)
        {
            switch (in_member_type)
            {
                case "個人會員": return "vip_mbr";
                case "準會員": return "vip_minority";
                case "": return "vip_mbr";

                default: return in_member_type;
            }
        }

        //段位數值與標籤處理
        private string GetDegree(TConfig cfg, string in_degree_label)
        {
            Item itmData = cfg.inn.newItem("In_Meeting_User");
            itmData.setProperty("in_degree", "");
            itmData.setProperty("in_degree_label", in_degree_label);

            Item itmDegreeRslt = itmData.apply("In_Degree_Value");
            return itmDegreeRslt.getProperty("in_degree", "");
        }

        //所屬委員會處理
        private Item GetCommittee(TConfig cfg, string in_committee)
        {
            string sql = "SELECT id, in_manager_area FROM IN_RESUME WITH(NOLOCK) WHERE in_member_type = 'area_cmt' AND in_member_role = 'sys_9999' AND in_name = N'{#in_name}'";
            sql = sql.Replace("{#in_name}", in_committee);
            return cfg.inn.applySQL(sql);
        }

        private void AddKV(Dictionary<string, string> map, string key, Item item)
        {
            string value = item.getProperty(key, "");
            if (value != "")
            {
                map.Add(key, value);
            }
        }

        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIndId { get; set; }
            public string login_resume_id { get; set; }
            public string login_resume_current_org { get; set; }
            public string login_resume_group { get; set; }
            public string meeting_id { get; set; }
            public string mode { get; set; }
            public string in_sno { get; set; }
            public string in_creator { get; set; }
            public string isPayAdd { get; set; }
            public bool needUpdateResume { get; set; }
            public bool isOneRegister { get; set; }
            
            public string mtName { get; set; }
            public string muName { get; set; }
            public string msName { get; set; }
            public string mtProperty { get; set; }
            public string svName { get; set; }
            public string soName { get; set; }

            public Item itmInsUsers { get; set; }
        }
    }
}