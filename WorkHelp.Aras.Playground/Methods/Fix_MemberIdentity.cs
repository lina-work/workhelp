﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class Fix_MemberIdentity : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]Fix_MemberIdentity";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "input dom: " + itmR.dom.InnerXml);

            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            Item itmPermit = inn.applyMethod("In_CheckIdentity", "<method>" + strMethodName + "</method><code>" + "ALL" + "</code>");
            bool isMeetingAdmin = itmPermit.getProperty("isMeetingAdmin", "") == "1";

            if (!isMeetingAdmin) throw new Exception("無權限");

            string aml = "";
            string sql = "";

            sql = "SELECT * FROM [IDENTITY] WITH(NOLOCK) WHERE [name] = 'ACT_VipGym'";
            Item itmIdentityVipGym = inn.applySQL(sql);
            string source_id = itmIdentityVipGym.getProperty("id", "");

            //刪除成員
            aml = @"<AML><Item type='Member' action='delete' where=""source_id='{#identity_id}' ""/></AML>";
            aml = aml.Replace("{#identity_id}", source_id);
            inn.applyAML(aml);

            //新增成員
            sql = "SELECT * FROM IN_RESUME WITH(NOLOCK) WHERE in_member_type = 'vip_gym'  AND in_member_role = 'sys_9999'";
            Item itmResumes = inn.applySQL(sql);
            int count = itmResumes.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item itmResume = itmResumes.getItemByIndex(i);
                string user_id = itmResume.getProperty("in_user_id", "");

                Item itmIdentity = inn.newItem("Identity", "get");
                itmIdentity.setProperty("in_user", user_id);
                itmIdentity = itmIdentity.apply();

                //MeetingUser
                Item itmMember = inn.newItem("Member", "add");
                itmMember.setProperty("source_id", source_id);
                itmMember.setRelatedItem(itmIdentity);
                itmMember = itmMember.apply();
            }

            return itmR;
        }
    }
}