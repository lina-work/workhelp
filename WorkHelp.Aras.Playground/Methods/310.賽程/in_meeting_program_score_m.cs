﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class in_meeting_program_score_m : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
            目的: 登錄成績介面
            日期: 
                2021-04-19: 切割創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]in_meeting_program_score_m";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "input dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserAliases(),
                strIdentityId = inn.getUserAliases(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                program_id = itmR.getProperty("program_id", ""),
                tree_id = itmR.getProperty("id", ""),
                scene = itmR.getProperty("scene", ""),
            };

            if (cfg.meeting_id == "")
            {
                itmR.setProperty("err_msg", "賽事 id 不得為空白");
                return itmR;
            }

            if (cfg.program_id == "")
            {
                itmR.setProperty("err_msg", "組別 id 不得為空白");
                return itmR;
            }

            if (cfg.tree_id == "")
            {
                itmR.setProperty("err_msg", "場次 id 不得為空白");
                return itmR;
            }

            string sql = "SELECT * FROM IN_MEETING_PEVENT WITH(NOLOCK) WHERE source_id = '" + cfg.program_id + "' AND in_tree_id = '" + cfg.tree_id + "'";
            Item itmQueryEvent = cfg.inn.applySQL(sql);
            if (itmQueryEvent.isError() || itmQueryEvent.getResult() == "")
            {
                itmR.setProperty("err_msg", "查無場次資料");
                return itmR;
            }

            Item itmTargetEvent = null;
            string target_tree_no = "";

            switch (cfg.scene)
            {
                case "last"://上一場
                    Item itmLastNo = OneSiteLastEvent(cfg, itmQueryEvent);
                    target_tree_no = itmLastNo.getProperty("target_tree_no", "");
                    if (target_tree_no == "")
                    {
                        itmR.setProperty("err_msg", "已無上一場資料");
                    }
                    else
                    {
                        itmTargetEvent = GetSiteEvent(cfg, itmQueryEvent, target_tree_no);
                    }
                    break;

                case "next"://下一場
                    Item itmNextNo = OneSiteNextEvent(cfg, itmQueryEvent);
                    target_tree_no = itmNextNo.getProperty("target_tree_no", "");
                    if (target_tree_no == "")
                    {
                        itmR.setProperty("err_msg", "已無下一場資料");
                    }
                    else
                    {
                        itmTargetEvent = GetSiteEvent(cfg, itmQueryEvent, target_tree_no);
                    }
                    break;

                default://查詢當場
                    itmTargetEvent = itmQueryEvent;
                    break;
            }

            Modal(cfg, itmTargetEvent, target_tree_no, itmR);

            return itmR;
        }

        private Item GetSiteEvent(TConfig cfg, Item itmQueryEvent, string target_tree_no)
        {
            string in_date_key = itmQueryEvent.getProperty("in_date_key", "");
            string in_site = itmQueryEvent.getProperty("in_site", "");

            string sql = @"
                SELECT 
	                *
                FROM
	                IN_MEETING_PEVENT WITH(NOLOCK)
                WHERE
	                in_date_key = '{#in_date_key}'
	                AND in_site = '{#in_site}'
	                AND in_tree_no = {#in_tree_no}
            ";

            sql = sql.Replace("{#in_date_key}", in_date_key)
                .Replace("{#in_site}", in_site)
                .Replace("{#in_tree_no}", target_tree_no);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item item = cfg.inn.applySQL(sql);
            int count = item.getItemCount();
            string result = item.getResult();

            return item;
        }

        private Item OneSiteLastEvent(TConfig cfg, Item itmQueryEvent)
        {
            string in_date_key = itmQueryEvent.getProperty("in_date_key", "");
            string in_site = itmQueryEvent.getProperty("in_site", "");
            string in_tree_no = itmQueryEvent.getProperty("in_tree_no", "");

            string sql = @"
                SELECT 
	                Max(in_tree_no) AS 'target_tree_no'
                FROM
	                IN_MEETING_PEVENT WITH(NOLOCK)
                WHERE
	                in_date_key = '{#in_date_key}'
	                AND in_site = '{#in_site}'
	                AND in_tree_no < {#in_tree_no}
	                AND in_tree_no <> 0
            ";

            sql = sql.Replace("{#in_date_key}", in_date_key)
                .Replace("{#in_site}", in_site)
                .Replace("{#in_tree_no}", in_tree_no);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        private Item OneSiteNextEvent(TConfig cfg, Item itmQueryEvent)
        {
            string in_date_key = itmQueryEvent.getProperty("in_date_key", "");
            string in_site = itmQueryEvent.getProperty("in_site", "");
            string in_tree_no = itmQueryEvent.getProperty("in_tree_no", "");

            string sql = @"
                SELECT 
	                MIN(in_tree_no) AS 'target_tree_no'
                FROM
	                IN_MEETING_PEVENT WITH(NOLOCK)
                WHERE
	                in_date_key = '{#in_date_key}'
	                AND in_site = '{#in_site}'
	                AND in_tree_no > {#in_tree_no}
	                AND in_tree_no <> 0
            ";

            sql = sql.Replace("{#in_date_key}", in_date_key)
                .Replace("{#in_site}", in_site)
                .Replace("{#in_tree_no}", in_tree_no);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        private void Modal(TConfig cfg, Item itmEvent, string target_tree_no, Item itmReturn)
        {
            if (itmEvent.isError())
            {
                itmReturn.setProperty("err_msg", "系統忙碌中");
                return;
            }

            if (itmEvent.getResult() == "")
            {
                itmReturn.setProperty("err_msg", "查無場次資料");
                return;
            }

            if (itmEvent.getItemCount() > 1)
            {
                itmReturn.setProperty("err_msg", "場次編號重複: " + target_tree_no);
                return;
            }

            string program_id = itmEvent.getProperty("source_id", "");
            string in_tree_id = itmEvent.getProperty("in_tree_id", "");

            Item itmProgram = cfg.inn.applySQL("SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + program_id + "'");

            Item items = GetEventDetails(cfg, program_id, in_tree_id);
            int count = items.getItemCount();
            if (count != 2) throw new Exception("對戰明細錯誤");

            string hide_points = ""; //積分
            string hide_scores = ""; //成績

            //string program_battle_type = cfg.itmProgram.getProperty("in_battle_type", "");
            //switch (program_battle_type)
            //{
            //    case "SingleRoundRobin":
            //    case "DoubleRoundRobin":
            //    case "GroupSRoundRobin":
            //    case "GroupDRoundRobin":
            //        hide_scores = "item_show_0";
            //        break;

            //    default:
            //        hide_points = "item_show_0";
            //        break;
            //}


            //組別
            itmReturn.setProperty("program_id", itmProgram.getProperty("id", ""));
            itmReturn.setProperty("program_display", itmProgram.getProperty("in_display", ""));
            itmReturn.setProperty("program_name2", itmProgram.getProperty("in_name2", ""));
            itmReturn.setProperty("program_name3", itmProgram.getProperty("in_name3", ""));

            itmReturn.setProperty("hide_scores", hide_scores);
            itmReturn.setProperty("hide_points", hide_points);

            //場次
            itmReturn.setProperty("event_id", itmEvent.getProperty("id", ""));
            itmReturn.setProperty("event_status", EventStatus(itmEvent.getProperty("in_win_status", "")));
            itmReturn.setProperty("in_tree_name", itmEvent.getProperty("in_tree_name", ""));
            itmReturn.setProperty("in_tree_no", itmEvent.getProperty("in_tree_no", ""));
            itmReturn.setProperty("in_tree_id", itmEvent.getProperty("in_tree_id", ""));
            itmReturn.setProperty("in_tree_alias", itmEvent.getProperty("in_tree_alias", ""));
            itmReturn.setProperty("in_site", itmEvent.getProperty("in_site", ""));
            itmReturn.setProperty("in_date_key", itmEvent.getProperty("in_date_key", ""));

            itmReturn.setProperty("in_round", itmEvent.getProperty("in_round", ""));

            SetModalItem(cfg, items.getItemByIndex(0), "f1", itmReturn);
            SetModalItem(cfg, items.getItemByIndex(1), "f2", itmReturn);
        }

        private void SetModalItem(TConfig cfg, Item source, string pfx, Item itmReturn)
        {
            string in_team_players = source.getProperty("in_team_players", "");
            bool is_signle = in_team_players == "1";

            itmReturn.setProperty(pfx + "_detail_id", source.getProperty("detail_id", ""));
            itmReturn.setProperty(pfx + "_photo", ShowPhoto(source, is_signle));
            itmReturn.setProperty(pfx + "_name", source.getProperty("in_name", ""));
            itmReturn.setProperty(pfx + "_sno", source.getProperty("in_sno", ""));
            itmReturn.setProperty(pfx + "_short_org", source.getProperty("map_short_org", ""));
            itmReturn.setProperty(pfx + "_show_name", ShowName(source, is_signle));
            itmReturn.setProperty(pfx + "_sign_no", source.getProperty("in_section_no", ""));
            itmReturn.setProperty(pfx + "_check", CheckStatus(source));
            itmReturn.setProperty(pfx + "_status", WinStatus(source));

            itmReturn.setProperty(pfx + "_correct_count", source.getProperty("in_correct_count", ""));

            string in_points = source.getProperty("in_points", "");
            string in_points_text = source.getProperty("in_points_text", "");
            if (in_points == "" || in_points == "0")
            {
                itmReturn.setProperty(pfx + "_points_1", "");
                itmReturn.setProperty(pfx + "_points_2", "");
                itmReturn.setProperty(pfx + "_points_3", "0");
            }
            else if (in_points == "1")
            {
                itmReturn.setProperty(pfx + "_points_1", "");
                itmReturn.setProperty(pfx + "_points_2", "");
                itmReturn.setProperty(pfx + "_points_3", "1");
            }
            else if (in_points == "10")
            {
                itmReturn.setProperty(pfx + "_points_1", "1");
                itmReturn.setProperty(pfx + "_points_2", "0");
                itmReturn.setProperty(pfx + "_points_3", "");
            }
            itmReturn.setProperty(pfx + "_points_text", in_points_text);


            string in_sign_no = source.getProperty("in_sign_no", "");
            string in_sign_bypass = source.getProperty("in_sign_bypass", "");
            if (in_sign_no == "" || in_sign_no == "0" || in_sign_bypass == "1")
            {
                itmReturn.setProperty(pfx + "_hide", "item_show_0");
            }
        }

        //檢錄 or DQ 狀態
        private string CheckStatus(Item source)
        {
            string in_check_result = source.getProperty("in_check_result", "");
            string in_check = "";
            if (in_check_result == "1")
            {
                in_check = "btn-success";
            }
            else if (in_check_result == "0")
            {
                in_check = "btn-danger";
            }
            return in_check;
        }

        //姓名
        private string ShowName(Item source, bool is_signle)
        {
            string in_name = source.getProperty("in_name", "");
            string in_team = source.getProperty("in_team", "");
            string map_short_org = source.getProperty("map_short_org", "");

            string in_show_name = in_name;

            if (!is_signle)
            {
                if (in_team == "")
                {
                    in_show_name = map_short_org;
                }
                else
                {
                    in_show_name = map_short_org + " " + in_team + "隊";
                }
            }

            return in_show_name;
        }

        //大頭照
        private string ShowPhoto(Item source, bool is_signle)
        {
            string in_name = source.getProperty("in_name", "");
            string resume_photo = source.getProperty("resume_photo", "");
            string org_photo = source.getProperty("org_photo", "");
            return resume_photo;
        }

        //取得選手勝敗呈現
        private string WinStatus(Item source)
        {
            string value = source.getProperty("in_status", "");

            switch (value)
            {
                case "1": return "<span class='team_score_b'>1</span>";
                case "0": return "<span class='team_score_c'>0</span>";
                default: return "<span class='team_score_a'>0</span>";
            }
        }

        //取得場次勝敗狀態
        private string EventStatus(string value)
        {
            switch (value)
            {
                case "cancel": return "(本場次取消)";

                case "nofight": return "(一方取消)";

                case "bypass": return "(輪空)";

                default: return "";
            }
        }

        private Item GetEventDetails(TConfig cfg, string program_id, string tree_id)
        {
            string sql = @"
                SELECT
                    t2.id AS 'detail_id'
                    , t2.in_sign_no
                    , t2.in_target_no
                    , t2.in_status
                    , t2.in_points
                    , t2.in_points_type
                    , t2.in_points_text
                    , t2.in_correct_count
                    , t3.id AS 'team_id'
                    , t3.in_current_org
                    , t3.in_short_org
                    , t3.map_short_org
                    , t3.in_team
                    , t3.in_name
                    , t3.in_sno
                    , t3.in_section_no
                    , t3.in_seeds
                    , t3.in_check_result
                    , t3.in_check_status
                    , t3.in_team_players
                    , t11.in_photo  AS 'resume_photo'
                FROM
                    IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
                    IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK)
                    ON t2.source_id = t1.id
                LEFT OUTER JOIN
                    VU_MEETING_PTEAM t3 WITH(NOLOCK)
                    ON t3.source_id = t1.source_id
                    AND t3.in_sign_no = t2.in_sign_no
                LEFT OUTER JOIN
                    IN_RESUME t11 WITH(NOLOCK)
                    ON t11.in_sno = t3.in_sno
                WHERE
                    t1.source_id = '{#program_id}'
                    AND t1.in_tree_id = '{#in_tree_id}'
                ORDER BY
                    t2.in_sign_foot
            ";

            sql = sql.Replace("{#program_id}", program_id)
                .Replace("{#in_tree_id}", tree_id);

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }

            public string meeting_id { get; set; }
            public string program_id { get; set; }
            public string tree_id { get; set; }
            public string scene { get; set; }

        }
    }
}