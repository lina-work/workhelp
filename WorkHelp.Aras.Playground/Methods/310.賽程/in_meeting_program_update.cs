﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;
using System.Net;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class in_meeting_program_update : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 更新組別資料
                輸入: 
                    - program_id
                    - property
                    - value
                日期: 
                    - 2021-12-15: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]in_meeting_program_update";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, this.dom.InnerXml);

            string program_id = itmR.getProperty("program_id", "");
            string property = itmR.getProperty("property", "");
            string value = itmR.getProperty("value", "");

            if (program_id == "" || property == "")
            {
                throw new Exception("參數錯誤");
            }

            string sql = "UPDATE IN_MEETING_PROGRAM SET property = '" + value + "' WHERE id = '" + program_id + "'";
            
            Item itmUpd = inn.applySQL(sql);
            
            if (itmUpd.isError() || itmUpd.getResult() == "")
            {
                throw new Exception("更新失敗");
            }

            return itmR;
        }
    }
}