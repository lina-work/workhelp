﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;
using System.Net;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class in_meeting_bulletin_word : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 下載獎狀
                輸入: meeting_id、team_id
                日期: 
                    2021-11-09: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]in_meeting_bulletin_word";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, this.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                InnH = _InnH,

                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                program_id = itmR.getProperty("program_id", ""),
                team_id = itmR.getProperty("team_id", ""),
                scene = itmR.getProperty("scene", ""),
            };

            switch (cfg.scene)
            {
                case "top5":
                    Top5Players(cfg, itmR);
                    break;

                default:
                    OnePlayer(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void Top5Players(TConfig cfg, Item itmReturn)
        {
            cfg.itmMeeting = cfg.inn.applySQL("SELECT in_title FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            if (cfg.itmMeeting.isError() || cfg.itmMeeting.getResult() == "")
            {
                throw new Exception("賽事資訊錯誤");
            }

            cfg.itmProgram = cfg.inn.applySQL("SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + cfg.program_id + "'");
            if (cfg.itmProgram.isError() || cfg.itmProgram.getResult() == "")
            {
                throw new Exception("組別資訊錯誤");
            }

            string sql = @"
                SELECT * FROM VU_MEETING_PTEAM 
                WHERE source_id = '{#program_id}'
                AND ISNULL(in_final_rank, 999) > 0 
                AND ISNULL(in_final_rank, 999) <= 5
                ORDER BY in_show_rank
            ";
            
            sql = sql.Replace("{#program_id}", cfg.program_id);
            Item itmPTeams = cfg.inn.applySQL(sql);
            if (itmPTeams.isError() || itmPTeams.getResult() == "")
            {
                throw new Exception("隊伍資訊錯誤");
            }

            cfg.InL1 = cfg.itmProgram.getProperty("in_l1", "");
            cfg.InL2 = cfg.itmProgram.getProperty("in_l2", "");
            cfg.InL3 = cfg.itmProgram.getProperty("in_l3", "");
            cfg.IsTeamBattle = cfg.InL1 == "團體組";
            cfg.WeightDisplay = GetWeightDisplay(cfg);

            TExport exp = ExportInfo(cfg, "rank_medal_path", cfg.itmProgram.getProperty("in_name2", ""));

            List<TOrgPlayer> list = new List<TOrgPlayer>();

            int team_count = itmPTeams.getItemCount();
            for(int i = 0; i < team_count; i++)
            {
                Item itmPTeam = itmPTeams.getItemByIndex(i);

                var in_short_org = itmPTeam.getProperty("in_short_org", "");
                var in_name = itmPTeam.getProperty("in_name", "");
                var in_final_rank = itmPTeam.getProperty("in_final_rank", "");
                var in_show_rank = itmPTeam.getProperty("in_show_rank", "");
                var in_names = itmPTeam.getProperty("in_names", "");

                var nms = in_names.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                if (nms != null && nms.Length > 0)
                {
                    foreach (var nm in nms)
                    {
                        var entity = new TOrgPlayer
                        {
                            OrgName = in_short_org,
                            PlayerName = nm,
                            FinalRank = in_final_rank,
                            ShowRank = in_show_rank,
                        };
                        list.Add(entity);
                    }
                }
            }

            foreach (var entity in list)
            {
                entity.FullName = entity.OrgName + "  " + entity.PlayerName;
                entity.RankDisplay = GetRankDisplay(cfg, entity.ShowRank);
            }

            //載入模板
            using (var doc = Xceed.Words.NET.DocX.Load(exp.template_Path))
            {
                int end_idx = list.Count - 1;
                for (int i = 0; i < list.Count; i++)
                {
                    AppendWordTable(cfg, doc, list[i], i == end_idx);
                }

                doc.Tables[0].Remove();
                doc.RemoveParagraphAt(0);

                doc.SaveAs(exp.File);
            }

            itmReturn.setProperty("xls_name", exp.Url);
        }

        private void OnePlayer(TConfig cfg, Item itmReturn)
        {
            cfg.itmMeeting = cfg.inn.applySQL("SELECT in_title FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            if (cfg.itmMeeting.isError() || cfg.itmMeeting.getResult() == "")
            {
                throw new Exception("賽事資訊錯誤");
            }

            Item itmPTeam = cfg.inn.applySQL("SELECT * FROM VU_MEETING_PTEAM WITH(NOLOCK) WHERE id = '" + cfg.team_id + "'");
            if (itmPTeam.isError() || itmPTeam.getResult() == "")
            {
                throw new Exception("隊伍資訊錯誤");
            }

            cfg.program_id = itmPTeam.getProperty("source_id", "");
            cfg.itmProgram = cfg.inn.applySQL("SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + cfg.program_id + "'");
            if (cfg.itmProgram.isError() || cfg.itmProgram.getResult() == "")
            {
                throw new Exception("組別資訊錯誤");
            }

            string in_name2 = cfg.itmProgram.getProperty("in_name2", "");

            cfg.InL1 = cfg.itmProgram.getProperty("in_l1", "");
            cfg.InL2 = cfg.itmProgram.getProperty("in_l2", "");
            cfg.InL3 = cfg.itmProgram.getProperty("in_l3", "");
            cfg.IsTeamBattle = cfg.InL1 == "團體組";
            cfg.WeightDisplay = GetWeightDisplay(cfg);

            List<TOrgPlayer> list = new List<TOrgPlayer>();
            var in_short_org = itmPTeam.getProperty("in_short_org", "");
            var in_final_rank = itmPTeam.getProperty("in_final_rank", "0");
            var in_show_rank = itmPTeam.getProperty("in_show_rank", "0");

            TExport exp = ExportInfo(cfg, "rank_medal_path", in_name2 + "_" + in_short_org);

            string in_l1 = cfg.itmProgram.getProperty("in_l1", "");
            if (in_l1 == "團體組")
            {
                var in_names = itmPTeam.getProperty("in_names", "");
                var nms = in_names.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                if (nms != null && nms.Length > 0)
                {
                    foreach (var nm in nms)
                    {
                        var entity = new TOrgPlayer
                        {  
                            OrgName = in_short_org,
                            PlayerName = nm,
                            FinalRank = in_final_rank,
                            ShowRank = in_show_rank,
                        };
                        list.Add(entity);
                    }
                }
            }

            if (list.Count == 0)
            {
                var player = new TOrgPlayer
                {
                    OrgName = in_short_org,
                    PlayerName = itmPTeam.getProperty("in_name", ""),
                    FinalRank = in_final_rank,
                    ShowRank = in_show_rank,
                };
                list.Add(player);
            }

            foreach (var entity in list)
            {
                entity.FullName = entity.OrgName + "  " + entity.PlayerName;
                entity.RankDisplay = GetRankDisplay(cfg, entity.ShowRank);
            }

            //載入模板
            using (var doc = Xceed.Words.NET.DocX.Load(exp.template_Path))
            {
                int end_idx = list.Count - 1;
                for (int i = 0; i < list.Count; i++)
                {
                    AppendWordTable(cfg, doc, list[i], i == end_idx);
                }

                doc.Tables[0].Remove();
                doc.RemoveParagraphAt(0);

                doc.SaveAs(exp.File);
            }

            itmReturn.setProperty("xls_name", exp.Url);
        }
        
        private void AppendWordTable(TConfig cfg, Xceed.Words.NET.DocX doc, TOrgPlayer player, bool is_end)
        {
            DateTime now = DateTime.Now;
            string tw_year = (now.Year - 1911).ToString();
            string m = now.Month.ToString().PadLeft(2, '0');
            string d = now.Day.ToString().PadLeft(2, '0');

            string pname = "";

            if (cfg.IsTeamBattle)
            {
                pname = cfg.itmProgram.getProperty("in_name2", "")
                    .Replace("組", "組 ")
                    .Replace("團體賽", "團體組")
                    + " "
                    + player.RankDisplay;
            }
            else
            {
                pname = cfg.itmProgram.getProperty("in_name2", "")
                    .Replace("組", "組 ")
                    .Replace("級", "量級")
                    + " "
                    + cfg.WeightDisplay
                    + " "
                    + player.RankDisplay;
            }

            //var p = doc.InsertParagraph();

            //取得模板表格
            var template_table = doc.Tables[0];

            var table = doc.InsertTable(template_table);

            var name_cell = table.Rows[5].Cells[0];
            name_cell.ReplaceText("[$player_name$]", player.FullName);

            var year_cell = table.Rows[6].Cells[0];
            year_cell.ReplaceText("[$year$]", tw_year);

            var program_cell = table.Rows[7].Cells[1];
            program_cell.ReplaceText("[$program_name$]", pname);

            var date_cell_1 = table.Rows[9].Cells[1];
            date_cell_1.ReplaceText("[$y$]", tw_year);

            var date_cell_2 = table.Rows[9].Cells[3];
            date_cell_2.ReplaceText("[$m$]", m);

            var date_cell_3 = table.Rows[9].Cells[4];
            date_cell_3.ReplaceText("[$d$]", d);

            if (!is_end)
            {
                var pend = doc.InsertParagraph();
                pend.InsertPageBreakAfterSelf();
            }
        }


        private TExport ExportInfo(TConfig cfg, string in_name, string file_title)
        {
            Item itmXls = cfg.inn.applyMethod("In_Meeting_Excel_Path", "<in_name>" + in_name + "</in_name>");

            var result = new TExport
            {
                template_Path = itmXls.getProperty("template_path", ""),
                export_Path = itmXls.getProperty("export_path", ""),
            };

            string pg_title = cfg.itmProgram.getProperty("in_name2", "");

            string ext_name = ".docx";
            string doc_name = pg_title + "_" + file_title + "_獎狀_" + DateTime.Now.ToString("yyyyMMdd_HHmmss");

            string export_path = result.export_Path.TrimEnd('\\');
            string doc_file = export_path + "\\" + doc_name + ext_name;
            string doc_url = doc_name + ext_name;

            result.File = doc_file;
            result.Url = doc_url;

            return result;
        }

        private string GetRankDisplay(TConfig cfg, string in_show_rank)
        {
            switch (in_show_rank)
            {
                case "1": return "冠軍";
                case "2": return "亞軍";
                case "3": return "季軍";
                case "4": return "第四名";
                case "5": return "第五名";
                case "6": return "第六名";
                case "7": return "第七名";
                case "8": return "第八名";
                default: return "無名次";
            }
        }

        private string GetWeightDisplay(TConfig cfg)
        {
            if (cfg.IsTeamBattle) return "";

            string[] arr = cfg.InL3.Split(new char[] { '：' }, StringSplitOptions.RemoveEmptyEntries);
            if (arr == null || arr.Length == 0)
            {
                return "";
            }

            return arr.Last().ToUpper();
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public Innosoft.InnovatorHelper InnH { get; set; }

            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }

            public string meeting_id { get; set; }
            public string program_id { get; set; }
            public string team_id { get; set; }
            public string scene { get; set; }

            public Item itmMeeting { get; set; }
            public Item itmProgram { get; set; }
            //public Item itmPTeam { get; set; }
            
            public string InL1 { get; set; }
            public string InL2 { get; set; }
            public string InL3 { get; set; }
            public bool IsTeamBattle { get; set; }
            public string WeightDisplay { get; set; }
        }

        private class TExport
        {
            public string template_Path { get; set; }
            public string export_Path { get; set; }
            public string File { get; set; }
            public string Url { get; set; }
        }

        private class TOrgPlayer
        {
            public string OrgName { get; set; }
            public string PlayerName { get; set; }
            public string FullName { get; set; }
            public string FinalRank { get; set; }
            public string ShowRank { get; set; }
            public string RankDisplay { get; set; }
        }
    }
}