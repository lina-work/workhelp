﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class in_meeting_program_spotcheck : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
 目的: 抽磅
 輸入: 
    - meeting id
    - in_date_key
 日誌:
    - 2021.09.07 改為不透過 XLSX 版 (lina)
    - 2021.05.06 創建 (lina)
 */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]in_meeting_program_spotcheck";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            //檢查頁面權限
            Item itmPermit = inn.applyMethod("In_CheckIdentity", "<method>" + strMethodName + "</method><code>" + "ALL" + "</code>");
            bool isMeetingAdmin = itmPermit.getProperty("isMeetingAdmin", "") == "1";
            bool isMeetingLeader = itmPermit.getProperty("isMeetingLeader", "") == "1";
            bool can_view = isMeetingAdmin || isMeetingLeader;
            if (!can_view)
            {
                itmR.setProperty("error_message", "您無權限瀏覽此頁面");
                return itmR;
            }

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                InnH = _InnH,

                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                program_id = itmR.getProperty("program_id", ""),
                muid = itmR.getProperty("muid", ""),
                in_date = itmR.getProperty("in_date", ""),
                in_time = itmR.getProperty("in_time", ""),
                scene = itmR.getProperty("scene", ""),
            };

            if (cfg.scene == "")
            {
                //頁面與選單
                Page(cfg, itmR);
            }
            else if (cfg.scene == "registry_page")
            {
                //抽磅登錄頁面
                RegistryPage(cfg, itmR);
            }
            else if (cfg.scene == "players")
            {
                //參賽選手列表
                BindData(cfg);
                Table(cfg, itmR);
            }
            else if (cfg.scene == "straw")
            {
                //抽磅

                BindData(cfg);
                //從 UI 取得數值
                CheckSpotValue(cfg, itmR);

                //if (cfg.weight.spot_max >= 999)
                //{
                //    throw new Exception("該量級無須抽磅");
                //}

                Straw(cfg, itmR);
            }
            else if (cfg.scene == "cancel")
            {
                //取消抽磅
                Cancel(cfg, itmR);
            }
            else if (cfg.scene == "registry")
            {
                //抽磅登錄
                EditSpotWeight(cfg, itmR);
            }
            else if (cfg.scene == "status")
            {
                //變更狀態
                EditSpotStatus(cfg, itmR);
            }
            else if (cfg.scene == "change_player")
            {
                //變更抽磅人員
                ChangeStrawPlayer(cfg, itmR);
            }
            else if (cfg.scene == "pdf")
            {
                //下載抽磅單
                DownloadPDF(cfg, itmR);
            }

            return itmR;
        }

        //變更抽磅人員
        private void ChangeStrawPlayer(TConfig cfg, Item itmReturn)
        {
            string muid = itmReturn.getProperty("muid", "");
            string in_spot_checked = itmReturn.getProperty("in_spot_checked", "");

            //繫結資料
            BindData(cfg);
            //從 UI 取得數值
            CheckSpotValue(cfg, itmReturn);

            string sql = "";

            if (in_spot_checked == "1")
            {
                //列入抽磅

                //將抽磅人數、上下限更新至該組
                UpdateProgram(cfg, is_clear: false);

                //將該組體重上下限更為一致
                sql = "UPDATE IN_MEETING_USER SET"
                    + " in_spot_min = '" + cfg.weight.in_spot_min + "'"
                    + ", in_spot_max = '" + cfg.weight.in_spot_max + "'"
                    + " WHERE"
                    + " source_id = '" + cfg.meeting_id + "'"
                    + " AND in_l1 = N'" + cfg.in_l1 + "'"
                    + " AND in_l2 = N'" + cfg.in_l2 + "'"
                    + " AND in_l3 = N'" + cfg.in_l3 + "'"
                    + " AND ISNULL(in_spot_checked, '') = '1'"
                    + "";

                //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

                cfg.inn.applySQL(sql);

                //將該人員列入抽磅
                sql = "UPDATE IN_MEETING_USER SET"
                    + " in_spot_min = '" + cfg.weight.in_spot_min + "'"
                    + ", in_spot_max = '" + cfg.weight.in_spot_max + "'"
                    + ", in_spot_no = '0'"
                    + ", in_spot_checked = '1'"
                    + ", in_spot_weight = NULL"
                    + ", in_spot_result = NULL"
                    + ", in_spot_time = NULL"
                    + " WHERE"
                    + "  id = '" + muid + "'"
                    + "";

                //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

                cfg.inn.applySQL(sql);
            }
            else
            {
                //將該人員移出抽磅
                sql = "UPDATE IN_MEETING_USER SET"
                    + " in_spot_min = NULL"
                    + ", in_spot_max = NULL"
                    + ", in_spot_no = NULL"
                    + ", in_spot_checked = '0'"
                    + ", in_spot_weight = NULL"
                    + ", in_spot_result = NULL"
                    + ", in_spot_time = NULL"
                    + " WHERE"
                    + "  id = '" + muid + "'"
                    + "";

                //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

                cfg.inn.applySQL(sql);
            }

            //重算組別的抽磅人數
            sql = @"
                UPDATE t1 SET
	                t1.in_spot_count = t2.spot_count
                FROM
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
	                (
	                SELECT
		                t11.id
		                , count(t12.id) AS 'spot_count'
	                FROM
		                IN_MEETING_PROGRAM t11 WITH(NOLOCK)
	                INNER JOIN
		                IN_MEETING_USER t12 WITH(NOLOCK)
		                ON t12.source_id = t11.in_meeting
		                AND ISNULL(t12.in_l1, '')  = t11.in_l1
		                AND ISNULL(t12.in_l2, '')  = t11.in_l2
		                AND ISNULL(t12.in_l3, '')  = t11.in_l3
	                WHERE
		                t11.id = '{#program_id}'
		                AND ISNULL(t12.in_spot_checked, '') = '1'
	                GROUP BY
		                t11.id
                ) t2 ON t2.id = t1.id
                WHERE
                    t1.id = '{#program_id}'
            ";

            sql = sql.Replace("{#program_id}", cfg.program_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            cfg.inn.applySQL(sql);
        }

        //取消抽磅
        private void Cancel(TConfig cfg, Item itmReturn)
        {
            string sql = @"
                UPDATE t1 SET 
                	IN_SPOT_CHECKED = 0
                FROM
                	IN_MEETING_USER t1 WITH(NOLOCK)
                INNER JOIN
                	IN_MEETING_PROGRAM t2 WITH(NOLOCK)
                	ON t2.in_meeting = t1.source_id
                	AND t2.in_l1 = t1.in_l1
                	AND t2.in_l2 = t1.in_l2
                	AND t2.in_l3 = t1.in_l3
                WHERE
                	t1.source_id = '{#meeting_id}'
                	AND t2.id = '{#program_id}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#program_id}", cfg.program_id);

            Item itmSQL = cfg.inn.applySQL(sql);

            if (itmSQL.isError())
            {
                throw new Exception("取消抽磅發生錯誤#1");
            }

            //將抽磅人數、上下限更新至該組
            UpdateProgram(cfg, is_clear: true);
        }

        //將抽磅人數、上下限更新至該組
        private void UpdateProgram(TConfig cfg, bool is_clear = false)
        {
            string sql = "";

            if (is_clear)
            {
                sql = @"
                    UPDATE IN_MEETING_PROGRAM SET 
                	    in_spot_count = NULL
                	    , in_spot_min = NULL
                	    , in_spot_max = NULL
                    WHERE
                	    id = '{#program_id}'
                ";

                sql = sql.Replace("{#program_id}", cfg.program_id);
            }
            else
            {

                sql = @"
                     UPDATE IN_MEETING_PROGRAM SET 
                         in_spot_count = '{#in_spot_count}'
                         , in_spot_min = '{#in_spot_min}'
                         , in_spot_max = '{#in_spot_max}'
                     WHERE 
                         id = '{#program_id}'
                 ";

                sql = sql.Replace("{#program_id}", cfg.program_id)
                    .Replace("{#in_spot_count}", cfg.spot_count.ToString())
                    .Replace("{#in_spot_min}", cfg.weight.in_spot_min)
                    .Replace("{#in_spot_max}", cfg.weight.in_spot_max);
            }

            Item itmSQL = cfg.inn.applySQL(sql);

            if (itmSQL.isError())
            {
                throw new Exception("更新組別抽磅資料發生錯誤");
            }
        }

        #region PDF
        private void DownloadPDF(TConfig cfg, Item itmReturn)
        {
            string sql = "SELECT in_title FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'";
            cfg.itmMeeting = cfg.inn.applySQL(sql);
            cfg.in_title = cfg.itmMeeting.getProperty("in_title", "");

            TSheet sheetCfg = new TSheet
            {
                WsRow = 6,
                WsCol = 0,
                Heads = new string[] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" },
                Font = "標楷體",
                SheetCount = 1,
                Sheets = new List<string>()
            };

            List<TTab> tabs = new List<TTab>();
            tabs.Add(new TTab { id = "muser_male", active = "active", title = "男生組", in_gender = "男", GetListFunc = GetSpotCheckList, GetGroupFunc = GetGroupStr });
            tabs.Add(new TTab { id = "muser_female", active = "", title = "女生組", in_gender = "女", GetListFunc = GetSpotCheckList, GetGroupFunc = GetGroupStr });

            int total_count = 0;
            foreach (var tab in tabs)
            {
                FixTab(cfg, tab);
                tab.groups = tab.GetGroupFunc(cfg, tab);
                total_count += tab.count;
            }

            if (total_count == 0)
            {
                throw new Exception("無抽磅資料");
            }


            cfg.day = GetDayInfo(cfg);
            cfg.xls = GetSpotCheckPDFInfo(cfg);

            Spire.Xls.Workbook workbook = new Spire.Xls.Workbook();
            workbook.LoadFromFile(cfg.xls.source);
            Spire.Xls.Worksheet sheetTemplate = workbook.Worksheets[0];

            foreach (var tab in tabs)
            {
                if (tab.count == 0) continue;
                AppendSheets(workbook, sheetTemplate, cfg, sheetCfg, tab);
            }

            //移除樣板 sheet
            sheetTemplate.Remove();

            string export_type = "PDF";

            switch (export_type)
            {
                case "PDF":
                    workbook.SaveToFile(cfg.xls.file.Replace("xlsx", "pdf"), Spire.Xls.FileFormat.PDF);
                    itmReturn.setProperty("xls_name", cfg.xls.url.Replace("xlsx", "pdf"));
                    break;

                default:
                    workbook.SaveToFile(cfg.xls.file, Spire.Xls.ExcelVersion.Version2010);
                    itmReturn.setProperty("xls_name", cfg.xls.url);
                    break;
            }
        }

        private void AppendSheets(
            Spire.Xls.Workbook workbook
            , Spire.Xls.Worksheet sheetTemplate
            , TConfig cfg
            , TSheet sheetCfg
            , TTab tab)
        {

            List<TField> fields = new List<TField>();
            fields.Add(new TField { title = "No.", property = "no", css = "text-center" });
            fields.Add(new TField { title = "姓名", property = "in_name", css = "text-left" });
            fields.Add(new TField { title = "參賽單位", property = "map_short_org", css = "text-left" });
            fields.Add(new TField { title = "量級", property = "program_sname", css = "text-center" });
            fields.Add(new TField { title = "準體重", property = "in_spot_weight", css = "text-right" });
            fields.Add(new TField { title = "選手簽名" });
            fields.Add(new TField { title = "抽磅體重上限", getVal = GetSpotMaxWeight2 });

            Spire.Xls.Worksheet sheet = workbook.CreateEmptySheet();
            sheet.CopyFrom(sheetTemplate);
            sheet.Name = tab.title;

            //sheet.Range["A1"].Style.Font.FontName = sheetCfg.Font;

            sheet.Range["A1"].Text = cfg.in_title + " 抽磅表 Day " + cfg.day.day_no + " " + tab.in_gender;
            sheet.Range["A2"].Text = "● " + tab.title + " " + tab.groups;
            sheet.Range["A3"].Text = "● 日期：" + cfg.day.contents;
            sheet.Range["A4"].Text = "● 抽磅體重上限超重才需簽名，時間到未到者請劃掉";

            int wsRow = sheetCfg.WsRow;
            int wsCol = sheetCfg.WsCol;

            List<Item> list = MapList(cfg, tab.items, true, 12);
            int count = list.Count;
            for (int i = 0; i < count; i++)
            {
                Item item = list[i];
                SetItemCell(sheet, cfg, sheetCfg, wsRow, wsCol, item, fields);
                wsRow++;
            }

            //string pace_pos = "D" + (wsRow);
            //sheet.Range[pace_pos].RowHeight = 10;

            //string sign_pos = "D" + (wsRow + 1);
            //sheet.Range[sign_pos].Text = "裁判簽名：";
            //sheet.Range[sign_pos].Style.Font.FontName = sheetCfg.Font;
            //sheet.Range[sign_pos].Style.Font.IsBold = true;

            //string line_pos_s = "E" + (wsRow + 1);
            //string line_pos_e = "G" + (wsRow + 1);
            //Spire.Xls.CellRange range = sheet.Range[line_pos_s + ":" + line_pos_e];
            //range.Merge();
            //range.Style.Borders[Spire.Xls.BordersLineType.EdgeBottom].LineStyle = Spire.Xls.LineStyleType.Thin;
        }

        private List<Item> MapList(TConfig cfg, Item items, bool is_fullt, int full_count)
        {
            List<Item> list = new List<Item>();

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                item.setProperty("no", (i + 1).ToString());
                list.Add(item);
            }

            if (is_fullt)
            {
                if ((count % full_count) > 0)
                {
                    int a = count % full_count;
                    int f = full_count - a;
                    for (int i = 0; i < f; i++)
                    {
                        list.Add(cfg.inn.newItem());
                    }
                }
            }

            return list;
        }

        private TXls GetSpotCheckPDFInfo(TConfig cfg)
        {
            Item itmPath = GetExcelPath(cfg, "spotcheck_list_path");

            string export_path = itmPath.getProperty("export_path", "").TrimEnd('\\');
            string ext_name = ".xlsx";

            string xls_name = cfg.in_title + " - " + cfg.in_date + "_" + DateTime.Now.ToString("MMdd_HHmmss");

            return new TXls
            {
                file = export_path + "\\" + xls_name + ext_name,
                url = xls_name + ext_name,
                source = itmPath.getProperty("template_path", ""),
            };
        }

        //設定物件與資料列
        private void SetItemCell(Spire.Xls.Worksheet sheet, TConfig cfg, TSheet sheetCfg, int wsRow, int wsCol, Item item, List<TField> fields)
        {
            for (int i = 0; i < fields.Count; i++)
            {
                var field = fields[i];
                var value = "";
                var position = sheetCfg.Heads[wsCol + i] + wsRow;

                if (field.getVal != null)
                {
                    value = field.getVal(cfg, field, item);
                }
                else if (!string.IsNullOrWhiteSpace(field.property))
                {
                    value = item.getProperty(field.property, "");
                }

                Spire.Xls.CellRange range = sheet.Range[position];
                SetBodyCell(range, sheetCfg, value, field.format);
            }
        }

        //設定資料列
        private void SetBodyCell(Spire.Xls.CellRange range, TSheet cfg, string value = "", string format = "")
        {
            switch (format)
            {
                case "center":
                    range.Text = value;
                    range.Style.Font.FontName = cfg.Font;
                    break;

                case "yyyy/MM/dd":
                case "yyyy/MM/dd HH:mm":
                    range.Text = GetDateTimeValue(value, format);
                    range.Style.Font.FontName = cfg.Font;
                    break;

                case "$ #,##0":
                    range.Text = value;
                    range.Style.Font.FontName = cfg.Font;
                    break;

                case "text":
                    range.Text = value;
                    range.Style.Font.FontName = cfg.Font;
                    break;

                default:
                    range.Text = value;
                    range.Style.Font.FontName = cfg.Font;
                    break;
            }

            range.Style.Borders[Spire.Xls.BordersLineType.EdgeTop].LineStyle = Spire.Xls.LineStyleType.Thin;
            range.Style.Borders[Spire.Xls.BordersLineType.EdgeRight].LineStyle = Spire.Xls.LineStyleType.Thin;
            range.Style.Borders[Spire.Xls.BordersLineType.EdgeBottom].LineStyle = Spire.Xls.LineStyleType.Thin;
            range.Style.Borders[Spire.Xls.BordersLineType.EdgeLeft].LineStyle = Spire.Xls.LineStyleType.Thin;
        }
        #endregion PDF

        private void EditSpotStatus(TConfig cfg, Item itmReturn)
        {
            string in_spot_status = itmReturn.getProperty("in_spot_status", "");

            string sql = "SELECT * FROM IN_MEETING_USER WITH(NOLOCK) WHERE id = '" + cfg.muid + "'";
            Item itmMUser = cfg.inn.applySQL(sql);
            string in_sno = itmMUser.getProperty("in_sno", "");
            string in_spot_weight = itmMUser.getProperty("in_spot_weight", "");
            string in_spot_min = itmMUser.getProperty("in_spot_min", "");
            string in_spot_max = itmMUser.getProperty("in_spot_max", "");

            //decimal weight = GetDecimal(in_spot_weight);
            //decimal w_min = GetDecimal(in_spot_min);
            //decimal w_max = GetDecimal(in_spot_max);

            ////防呆
            //if (weight > w_min && weight <= w_max)
            //{
            //    if (in_spot_status != "on")
            //    {
            //        throw new Exception("異常");
            //    }
            //}

            string w_check = "";
            string w_status = in_spot_status;
            string w_message = "";
            string w_show_status = "";
            string w_time = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");

            switch (in_spot_status)
            {
                case "on":
                    w_check = "1";
                    w_message = "";
                    w_show_status = "<font color='green'><b>抽磅合格</b></font>";
                    break;

                case "off":
                    w_check = "0";
                    w_show_status = "<font color='red'><b>DQ(抽磅未到)</b></font>";
                    break;

                case "leave":
                    w_check = "0";
                    w_show_status = "<font color='red'><b>DQ(抽磅請假)</b></font>";
                    break;

                case "dq":
                    w_check = "0";
                    w_show_status = "<font color='red'><b>DQ(公斤不符)</b></font>";
                    break;
            }

            sql = "UPDATE IN_MEETING_USER SET "
                + "   in_spot_result = '" + w_check + "'"
                + " , in_spot_time = '" + w_time + "'"
                + " , in_spot_status = '" + w_status + "'"
                + " WHERE id = '" + cfg.muid + "'";

            Item itmSQL = cfg.inn.applySQL(sql);

            if (itmSQL.isError())
            {
                w_message = "登錄失敗!<BR><BR>系統忙碌中請稍後再試!";
            }

            //成績串接
            Item itmScore = cfg.inn.newItem("In_Meeting_PEvent_Detail");
            itmScore.setProperty("mode", "rollcall");
            itmScore.setProperty("meeting_id", cfg.meeting_id);

            itmScore.setProperty("in_l1", itmMUser.getProperty("in_l1", ""));
            itmScore.setProperty("in_l2", itmMUser.getProperty("in_l2", ""));
            itmScore.setProperty("in_l3", itmMUser.getProperty("in_l3", ""));
            itmScore.setProperty("in_sno", itmMUser.getProperty("in_sno", ""));
            itmScore.setProperty("w_check", w_check);
            itmScore.setProperty("w_status", w_status);
            itmScore.setProperty("w_type", "spot");
            itmScore.setProperty("w_value", in_spot_weight);
            Item itmScoreResult = itmScore.apply("in_meeting_program_score");

            itmReturn.setProperty("w_message", w_message);
            itmReturn.setProperty("w_show_status", w_show_status);
        }

        private void EditSpotWeight(TConfig cfg, Item itmReturn)
        {
            string in_spot_weight = itmReturn.getProperty("in_spot_weight", "");
            decimal weight = GetDecimal(in_spot_weight);

            string sql = "SELECT * FROM IN_MEETING_USER WITH(NOLOCK) WHERE id = '" + cfg.muid + "'";
            Item itmMUser = cfg.inn.applySQL(sql);
            string in_sno = itmMUser.getProperty("in_sno", "");
            string in_spot_min = itmMUser.getProperty("in_spot_min", "");
            string in_spot_max = itmMUser.getProperty("in_spot_max", "");

            decimal w_min = GetDecimal(in_spot_min);
            decimal w_max = GetDecimal(in_spot_max);

            string w_check = "";
            string w_status = "";
            string w_message = "";
            string w_show_status = "";
            string w_time = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");

            if (weight > w_min && weight <= w_max)
            {
                w_check = "1";
                w_status = "on";
                w_message = "";
                w_show_status = "<font color='green'><b>抽磅合格</b></font>";
            }
            else
            {
                w_check = "0";
                w_status = "dq";
                w_message = "登錄成功!<BR><BR>體重與項目限制不符!";
                w_show_status = "<font color='red'><b>DQ(公斤不符)</b></font>";
            }

            sql = "UPDATE IN_MEETING_USER SET "
                + "  in_spot_weight = '" + in_spot_weight + "'"
                + " , in_spot_result = '" + w_check + "'"
                + " , in_spot_time = '" + w_time + "'"
                + " , in_spot_status = '" + w_status + "'"
                + " WHERE id = '" + cfg.muid + "'";

            Item itmSQL = cfg.inn.applySQL(sql);

            if (itmSQL.isError())
            {
                w_message = "登錄失敗!<BR><BR>系統忙碌中請稍後再試!";
            }

            //成績串接
            Item itmScore = cfg.inn.newItem("In_Meeting_PEvent_Detail");
            itmScore.setProperty("mode", "rollcall");
            itmScore.setProperty("meeting_id", cfg.meeting_id);

            itmScore.setProperty("in_l1", itmMUser.getProperty("in_l1", ""));
            itmScore.setProperty("in_l2", itmMUser.getProperty("in_l2", ""));
            itmScore.setProperty("in_l3", itmMUser.getProperty("in_l3", ""));
            itmScore.setProperty("in_sno", itmMUser.getProperty("in_sno", ""));
            itmScore.setProperty("w_check", w_check);
            itmScore.setProperty("w_status", w_status);
            itmScore.setProperty("w_type", "spot");
            itmScore.setProperty("w_value", in_spot_weight);
            Item itmScoreResult = itmScore.apply("in_meeting_program_score");

            itmReturn.setProperty("w_message", w_message);
            itmReturn.setProperty("w_show_status", w_show_status);
        }

        private void BindData(TConfig cfg)
        {
            string sql = "SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + cfg.program_id + "'";
            cfg.itmProgram = cfg.inn.applySQL(sql);
            cfg.in_name3 = cfg.itmProgram.getProperty("in_name3", "");
            cfg.in_l1 = cfg.itmProgram.getProperty("in_l1", "");
            cfg.in_l2 = cfg.itmProgram.getProperty("in_l2", "");
            cfg.in_l3 = cfg.itmProgram.getProperty("in_l3", "");
            cfg.in_sort_order = cfg.itmProgram.getProperty("in_sort_order", "");
            cfg.in_round_code = cfg.itmProgram.getProperty("in_round_code", "");
            cfg.in_team_count = cfg.itmProgram.getProperty("in_team_count", "");
            cfg.round_code = GetInt32(cfg.in_round_code);
            cfg.team_count = GetInt32(cfg.in_team_count);

            string in_spot_count = cfg.itmProgram.getProperty("in_spot_count", "0");
            if (in_spot_count != "" && in_spot_count != "0")
            {
                cfg.spot_count = GetInt32(in_spot_count);
            }
            else
            {
                cfg.spot_count = GetSpotCount(cfg.team_count);
            }

            cfg.itmSurvey = GetSurvey(cfg, "in_l3");
            cfg.survey_id = cfg.itmSurvey.getProperty("id", "");

            cfg.itmOption = GetOption(cfg);
            cfg.in_extend_value2 = cfg.itmOption.getProperty("in_extend_value2", "");

            cfg.weight = GetWeight(cfg.in_extend_value2);

            string in_spot_min = cfg.itmProgram.getProperty("in_spot_min", "0");
            string in_spot_max = cfg.itmProgram.getProperty("in_spot_max", "0");
            if (in_spot_max != "" && in_spot_max != "0")
            {
                var w = cfg.weight;
                w.in_spot_min = in_spot_min;
                w.in_spot_max = in_spot_max;

                w.spot_min = GetDecimal(w.in_spot_min);
                w.spot_max = GetDecimal(w.in_spot_max);
                w.spot_range = w.spot_min.ToString() + "-" + w.spot_max.ToString();
            }
        }

        private void CheckSpotValue(TConfig cfg, Item itmReturn)
        {
            string new_spot_count = itmReturn.getProperty("new_spot_count", "0");
            string new_spot_min = itmReturn.getProperty("new_spot_min", "0");
            string new_spot_max = itmReturn.getProperty("new_spot_max", "0");

            var spot_count = GetInt32(new_spot_count);
            var spot_min = GetDecimal(new_spot_min);
            var spot_max = GetDecimal(new_spot_max);

            if (spot_count > 0)
            {
                cfg.spot_count = spot_count;
            }
            if (spot_max > 0)
            {
                var w = cfg.weight;
                w.in_spot_min = new_spot_min;
                w.in_spot_max = new_spot_max;

                w.spot_min = GetDecimal(w.in_spot_min);
                w.spot_max = GetDecimal(w.in_spot_max);
                w.spot_range = w.spot_min.ToString() + "-" + w.spot_max.ToString();
            }
        }

        //抽磅
        private void Straw(TConfig cfg, Item itmReturn)
        {
            //復原因抽磅而 DQ 的人員
            string sql = @"
                UPDATE IN_MEETING_PTEAM SET 
                    in_check_result = NULL
                    , in_check_status = NULL
                WHERE 
                    source_id = '{#program_id}' 
                    AND ISNULL(in_weight_result, '') <> '0' 
                    AND ISNULL(in_check_status, '') = 'spot'
                    AND ISNULL(in_check_result, '') = '0'
            ";

            sql = sql.Replace("{#program_id}", cfg.program_id);
            cfg.inn.applySQL(sql);

            //將抽磅人數、上下限更新至該組
            UpdateProgram(cfg, is_clear: false);

            //抽磅組別
            Item itmTeams = GetMTeams(cfg);
            cfg.TeamMap = MapTeams(cfg, itmTeams);

            ////建立籤表 XLS
            //Item itmXlsResult = GenerateExcel(cfg, itmTeams);
            //cfg.xls_name = itmXlsResult.getProperty("xls_name", "");
            //cfg.xls_file = itmXlsResult.getProperty("xls_file", "");

            ////抽籤
            //RunStraw(cfg);

            //抽籤
            RunStrawNew(cfg);
        }

        #region 抽籤

        //整組抽籤
        private void RunStrawNew(TConfig cfg)
        {
            int[] ns = InnSport.Core.Utilities.TUtility.RandomArray(cfg.TeamMap.Count);
            int i = 0;

            foreach (var kv in cfg.TeamMap)
            {
                int spot_no = ns[i];

                var team = kv.Value;
                team.in_spot_no = spot_no.ToString();

                if (spot_no > 0 && spot_no <= cfg.spot_count)
                {
                    team.in_spot_checked = "1";
                }
                else
                {
                    team.in_spot_checked = "0";
                }

                team.in_index = team.Value.getProperty("in_index", "");
                team.in_creator_sno = team.Value.getProperty("in_creator_sno", "");

                string sql = "UPDATE IN_MEETING_USER SET"
                    + " in_spot_min = '" + cfg.weight.in_spot_min + "'"
                    + ", in_spot_max = '" + cfg.weight.in_spot_max + "'"
                    + ", in_spot_no = '" + team.in_spot_no + "'"
                    + ", in_spot_checked = '" + team.in_spot_checked + "'"
                    + ", in_spot_weight = NULL"
                    + ", in_spot_result = NULL"
                    + ", in_spot_time = NULL"
                    + " WHERE"
                    + " source_id = '" + cfg.meeting_id + "'"
                    + " AND in_l1 = N'" + cfg.in_l1 + "'"
                    + " AND in_l2 = N'" + cfg.in_l2 + "'"
                    + " AND in_l3 = N'" + cfg.in_l3 + "'"
                    + " AND in_index = N'" + team.in_index + "'"
                    + " AND in_creator_sno = N'" + team.in_creator_sno + "'"
                    + "";

                //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

                cfg.inn.applySQL(sql);

                i++;
            }
        }

        /// <summary>
        /// 轉換
        /// </summary>
        private Dictionary<string, TTeam> MapTeams(TConfig cfg, Item items)
        {
            Dictionary<string, TTeam> map = new Dictionary<string, TTeam>();
            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_team_index = item.getProperty("in_team_index", "");

                if (in_team_index == "") continue;
                if (map.ContainsKey(in_team_index)) continue;

                TTeam team = new TTeam
                {
                    in_spot_no = "",
                    Value = item
                };

                map.Add(in_team_index, team);
            }
            return map;
        }

        private Item GetExcelPath(TConfig cfg, string in_name)
        {
            Item itmResult = cfg.inn.newItem();

            //Excel 匯出路徑 (from 原創.設定參數)
            string sql = @"
                SELECT 
                    t2.in_name AS 'variable'
                    , t1.in_name
                    , t1.in_value 
                FROM 
                    In_Variable_Detail t1 WITH(NOLOCK)
                INNER JOIN 
                    In_Variable t2 WITH(NOLOCK) 
                    ON t2.id = t1.source_id 
                WHERE 
                    t2.in_name = N'meeting_excel'  
                    AND t1.in_name IN (N'export_path', N'{#in_name}')
            ";

            sql = sql.Replace("{#in_name}", in_name);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item items = cfg.inn.applySQL(sql);

            if (items.isError())
            {
                throw new Exception("未設定匯出路徑 _# " + in_name);
            }

            for (int i = 0; i < items.getItemCount(); i++)
            {
                Item item = items.getItemByIndex(i);
                string xls_name = item.getProperty("in_name", "");
                string xls_value = item.getProperty("in_value", "");

                if (xls_name == "export_path")
                {
                    itmResult.setProperty("export_path", xls_value);
                }
                else
                {
                    itmResult.setProperty("template_path", xls_value);
                }
            }

            return itmResult;
        }

        //取得賽程組別資訊(抽籤)
        private Item GetMTeams(TConfig cfg)
        {
            string sql = @"
                SELECT
                   t2.id            AS 'program_id'
                   , t2.in_name     AS 'program_name'
                   , t2.in_name2    AS 'program_name2'
                   , t2.in_name3    AS 'program_name3'
                   , t2.in_display AS 'program_display'
                   , t1.*
                FROM
                    VU_MEETING_PTEAM t1 WITH(NOLOCK)
                INNER JOIN
                    IN_MEETING_PROGRAM t2 WITH(NOLOCK)
                    ON t2.id = t1.source_id
                WHERE
                    t1.source_id = '{#program_id}'
                    AND ISNULL(t1.in_weight_result, '') NOT IN ('0')
                    AND ISNULL(t1.in_weight_message, '') = ''
                ORDER BY
                    t1.in_team_index
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#program_id}", cfg.program_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        #endregion 抽籤

        #region Table

        private void Table(TConfig cfg, Item itmReturn)
        {
            // string program_title = ""
            //     + "<h5>"
            //     + "參賽人數/隊數："
            //     + "<span class='program_highlight'>" + cfg.in_team_count + "</span>"
            //     + "預計抽磅人數："
            //     + "<span class='program_highlight'>" + cfg.spot_count + "</span>"
            //     + "抽磅體重上限："
            //     + "<span class='program_highlight'>" + cfg.weight.in_spot_max + "</span>"
            //     + "</h5>";

            // itmReturn.setProperty("program_title", program_title);

            itmReturn.setProperty("inn_team_count", cfg.in_team_count.ToString());
            itmReturn.setProperty("inn_spot_count", cfg.spot_count.ToString());
            itmReturn.setProperty("inn_spot_max", cfg.weight.in_spot_max);
            itmReturn.setProperty("inn_spot_min", cfg.weight.in_spot_min);

            itmReturn.setProperty("inn_table", GetTableContents(cfg));
        }

        private string GetTableContents(TConfig cfg)
        {
            Item itmMUsers = GetMUsers(cfg);

            List<TField> fields = new List<TField>();
            fields.Add(new TField { title = "No.", property = "view_no", css = "text-center" });
            fields.Add(new TField { title = "姓名", property = "in_name", css = "text-left inn_name" });
            fields.Add(new TField { title = "參賽單位", property = "map_short_org", css = "text-left" });
            fields.Add(new TField { title = "量級", property = "program_name3", css = "text-left" });
            fields.Add(new TField { title = "準體重", property = "in_weight", css = "text-right" });
            fields.Add(new TField { title = "過磅結果", getVal = GetWeightStatus, css = "text-center" });
            fields.Add(new TField { title = "籤號", property = "in_section_no", css = "text-center" });
            fields.Add(new TField { title = "抽磅", getVal = GetSpotCheckBox, css = "text-center" });
            fields.Add(new TField { title = "抽磅體重", property = "in_spot_weight", css = "text-right" });
            fields.Add(new TField { title = "抽磅結果", getVal = GetSpotResult, css = "text-center" });

            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();


            head.Append("<thead>");
            head.Append("  <tr>");
            for (int i = 0; i < fields.Count; i++)
            {
                TField field = fields[i];
                head.Append("    <th class='text-center'>" + field.title + "</th>");
            }
            head.Append("  </tr>");
            head.Append("</thead>");


            int count = itmMUsers.getItemCount();

            body.Append("<tbody>");
            for (int i = 0; i < count; i++)
            {
                Item item = itmMUsers.getItemByIndex(i);
                item.setProperty("view_no", (i + 1).ToString());

                AppendRow(cfg, fields, item, body);
            }
            body.Append("</tbody>");

            string table_id = "player_table";
            StringBuilder builder = new StringBuilder();
            builder.Append("<table id='" + table_id + "' data-toggle='table' class='table table-hover table-bordered table-rwd rwd rwdtable' data-search='true' data-search-align='left' data-sort-stable='true'>");
            builder.Append(head);
            builder.Append(body);
            builder.Append("</table>");
            builder.Append("<script>");
            builder.Append("    $('#" + table_id + "').bootstrapTable({});");
            builder.Append("    if($(window).width() <= 768) { $('#" + table_id + "').bootstrapTable('toggleView'); }");
            builder.Append("</script>");

            return builder.ToString();
        }

        private void AppendRow(TConfig cfg, List<TField> fields, Item item, StringBuilder body)
        {
            body.Append("<tr>");
            for (int i = 0; i < fields.Count; i++)
            {
                TField field = fields[i];
                string value = "";
                if (field.getVal != null)
                {
                    value = field.getVal(cfg, field, item);
                }
                else if (field.property != "")
                {
                    value = item.getProperty(field.property, "");
                }

                body.Append("<td class='" + field.css + "'>" + value + "</td>");
            }
            body.Append("</tr>");
        }

        private Item GetSurvey(TConfig cfg, string in_property)
        {
            string sql = @"
                SELECT
	                t2.*
                FROM 
	                IN_MEETING_SURVEYS t1 WITH(NOLOCK)
                INNER JOIN
	                IN_SURVEY t2 WITH(NOLOCK)
	                ON t2.id = t1.related_id
                WHERE
	                t1.source_id = '{#meeting_id}'
	                AND t2.in_property = '{#in_property}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_property}", in_property);

            return cfg.inn.applySQL(sql);
        }

        private Item GetOption(TConfig cfg)
        {
            string sql = @"
                SELECT
	                *
                FROM 
	                IN_SURVEY_OPTION WITH(NOLOCK)
                WHERE
	                source_id = '{#survey_id}'
	                AND in_grand_filter = N'{#in_l1}'
	                AND in_filter = N'{#in_l2}'
	                AND in_value = N'{#in_l3}'
            ";

            sql = sql.Replace("{#survey_id}", cfg.survey_id)
                .Replace("{#in_l1}", cfg.in_l1)
                .Replace("{#in_l2}", cfg.in_l2)
                .Replace("{#in_l3}", cfg.in_l3);

            return cfg.inn.applySQL(sql);
        }

        private Item GetMUsers(TConfig cfg)
        {
            //, ISNULL(t1.in_stuff_b1, '') + ISNULL(t2.in_short_org, t1.in_short_org) AS 'map_short_org'
            string sql = @"
                SELECT 
	                t1.*
	                , ISNULL(t2.in_short_org, t1.in_short_org) AS 'map_short_org'
					, t3.in_name3 AS 'program_name3'
                FROM 
	                IN_MEETING_USER t1 WITH(NOLOCK)
                LEFT OUTER JOIN
	                IN_ORG_MAP t2 WITH(NOLOCK)
	                ON t2.in_stuff_b1 = t1.in_stuff_b1
				INNER JOIN
					IN_MEETING_PROGRAM t3 WITH(NOLOCK)
					ON t3.in_meeting = t1.source_id
					AND t3.in_l1 = t1.in_l1
					AND t3.in_l2 = t1.in_l2
					AND t3.in_l3 = t1.in_l3
                WHERE 
	                t1.source_id = '{#meeting_id}'
	                AND t1.in_l1 = N'{#in_l1}'
	                AND t1.in_l2 = N'{#in_l2}'
	                AND t1.in_l3 = N'{#in_l3}'
                ORDER BY
                    t1.in_short_org
                    , t1.in_team
                    , t1.in_sno
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_l1}", cfg.in_l1)
                .Replace("{#in_l2}", cfg.in_l2)
                .Replace("{#in_l3}", cfg.in_l3);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }


        #endregion Table

        private void Page(TConfig cfg, Item itmReturn)
        {
            string sql = "SELECT in_title FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'";
            cfg.itmMeeting = cfg.inn.applySQL(sql);
            itmReturn.setProperty("in_title", cfg.itmMeeting.getProperty("in_title", ""));

            //日期選單
            AppendDateMenu(cfg, itmReturn);

            if (cfg.in_date != "")
            {
                //組別選單
                AppendProgramMenu(cfg, itmReturn);
                itmReturn.setProperty("hide_program", "");
            }
            else
            {
                itmReturn.setProperty("hide_program", "item_show_0");
            }
        }

        #region 抽磅登錄

        private void RegistryPage(TConfig cfg, Item itmReturn)
        {
            string sql = "SELECT in_title FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'";
            cfg.itmMeeting = cfg.inn.applySQL(sql);
            itmReturn.setProperty("in_title", cfg.itmMeeting.getProperty("in_title", ""));

            //日期選單
            AppendDateMenu(cfg, itmReturn);

            if (cfg.in_date != "")
            {
                //附加頁籤
                AppendTabs(cfg, itmReturn);
            }
        }

        private void AppendTabs(TConfig cfg, Item itmReturn)
        {
            List<TTab> tabs = new List<TTab>();
            tabs.Add(new TTab { id = "muser_male", active = "active", title = "男生組", in_gender = "男", GetListFunc = GetSpotCheckList, AppendListAction = AppendSpotCheckList });
            tabs.Add(new TTab { id = "muser_female", active = "", title = "女生組", in_gender = "女", GetListFunc = GetSpotCheckList, AppendListAction = AppendSpotCheckList });

            foreach (var tab in tabs)
            {
                FixTab(cfg, tab);
            }

            StringBuilder builder = new StringBuilder();

            builder.Append("<ul class='nav nav-pills' style='justify-content: start ;' id='pills-tab' role='tablist'>");
            AppendTabHeads(cfg, tabs, builder);
            builder.Append("</ul>");

            builder.Append("<div class='tab-content' id='pills-tabContent'>");
            AppendTabContents(cfg, tabs, builder);
            builder.Append("</div>");

            itmReturn.setProperty("inn_tabs", builder.ToString());
        }

        private void AppendTabHeads(TConfig cfg, List<TTab> tabs, StringBuilder builder)
        {
            foreach (var tab in tabs)
            {
                if (tab.is_error) continue;

                builder.Append("<li id='" + tab.li_id + "' class='nav-item " + tab.active + "'>");
                builder.Append("<a id='" + tab.a_id + "' href='#" + tab.content_id + "' class='nav-link' data-toggle='pill' role='tab' aria-controls='pills-home' aria-selected='true' onclick='StoreTab_Click(this)'>");
                builder.Append(tab.title + "(" + tab.count + ")");
                builder.Append("</a>");
                builder.Append("</li>");
            }
        }

        private void AppendTabContents(TConfig cfg, List<TTab> tabs, StringBuilder builder)
        {
            foreach (var tab in tabs)
            {
                if (tab.is_error) continue;

                builder.Append("<div class='tab-pane fade in " + tab.active + "' id='" + tab.content_id + "' role='tabpanel'>");
                builder.Append("    <div class='showsheet_ clearfix'>");
                //builder.Append("        <div class='box-header'>");
                //builder.Append("            <h4 class='box-header with-border'>");
                //builder.Append(tab.title);
                //builder.Append("                <small>(0)");
                //builder.Append("                </small>");
                //builder.Append("            </h4>");
                //builder.Append("            <div class='box-tools pull-right'>");
                //builder.Append("                <button type='button' class='btn btn-box-tool fold_type' data-widget='collapse' style='width: 40px; height: 20px;'>");
                //builder.Append("                    <a id='" + tab.toggle_id + "' class='accordion-toggle box-title form-box-title fa fa-plus' style='font-size: 25px;' data-toggle='collapse' data-parent='#accordion' href='#" + tab.folder_id + "'>");
                //builder.Append("                    </a>");
                //builder.Append("                </button>");
                //builder.Append("            </div>");
                //builder.Append("        </div>");

                //builder.Append("        <div id='" + tab.folder_id + "' class='collapse'>");
                builder.Append("        <div id='" + tab.folder_id + "'>");
                builder.Append("            <div class='float-btn clearfix'>");
                builder.Append("            </div>");

                tab.AppendListAction(cfg, tab, builder);

                builder.Append("        </div>");


                builder.Append("    </div>");
                builder.Append("</div>");
            }
        }

        private void AppendSpotCheckList(TConfig cfg, TTab tab, StringBuilder builder)
        {
            List<TField> fields = new List<TField>();
            fields.Add(new TField { title = "No.", property = "no", hcss = "text-center" });
            fields.Add(new TField { title = "姓名", property = "in_name", hcss = "col-md-2 text-center" });
            fields.Add(new TField { title = "參賽單位", property = "in_current_org", hcss = "col-md-2 text-center" });
            //fields.Add(new TField { title = "量級", getVal = GetShortProgramName, hcss = "text-center" });
            //fields.Add(new TField { title = "量級", property = "program_name3", hcss = "text-center" });
            fields.Add(new TField { title = "量級", getVal = GetProgramNameLink, hcss = "text-center" });
            fields.Add(new TField { title = "準體重", getVal = SpotWeightInput, hcss = "col-md-1 text-center" });
            fields.Add(new TField { title = "抽磅結果", getVal = GetSpotResult, hcss = "text-center" });
            fields.Add(new TField { title = "抽磅狀態", getVal = GetSpotStatus, hcss = "text-center" });
            fields.Add(new TField { title = "選手簽名", getVal = SignNameFunc, hcss = "text-center" });
            fields.Add(new TField { title = "抽磅體重<br>上限", getVal = GetSpotMaxWeight, hcss = "text-center" });

            var table = new TTable
            {
                id = tab.table_id,
                title = "",
                count = tab.items.getItemCount(),
            };

            AppendTable(cfg, tab, table, fields, tab.items, builder);

        }

        private void AppendTable(TConfig cfg, TTab tab, TTable table, List<TField> fields, Item items, StringBuilder builder)
        {
            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();


            head.Append("<thead>");
            head.Append("  <tr>");
            for (int i = 0; i < fields.Count; i++)
            {
                TField field = fields[i];
                head.Append("    <th class='" + field.hcss + "'>" + field.title + "</th>");
            }
            head.Append("  </tr>");
            head.Append("</thead>");

            int count = items.getItemCount();
            body.Append("<tbody>");
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                item.setProperty("no", (i + 1).ToString());

                AppendRow(cfg, tab, fields, item, body);
            }
            body.Append("</tbody>");

            //builder.Append("<h4 class='box-header with-border'>" + table.title + " (" + table.count + ")</h4>");
            builder.Append("<table id='" + table.id + "' class='table table-hover table-bordered table-rwd rwd'>");
            builder.Append(head);
            builder.Append(body);
            builder.Append("</table>");
            builder.Append("<script>");
            builder.Append("    $('#" + table.id + "').bootstrapTable({});");
            builder.Append("    if($(window).width() <= 768) { $('#" + table.id + "').bootstrapTable('toggleView'); }");
            builder.Append("</script>");
        }

        private void AppendRow(TConfig cfg, TTab tab, List<TField> fields, Item item, StringBuilder body)
        {
            body.Append("<tr>");
            for (int i = 0; i < fields.Count; i++)
            {
                TField field = fields[i];
                string value = "";
                if (field.getVal != null)
                {
                    value = field.getVal(cfg, field, item);
                }
                else if (field.property != "")
                {
                    value = item.getProperty(field.property, "");
                }

                body.Append("<td>" + value + "</td>");
            }
            body.Append("</tr>");
        }

        private void FixTab(TConfig cfg, TTab tab)
        {
            tab.li_id = "li_" + tab.id;
            tab.a_id = "a_" + tab.id;
            tab.content_id = "box_" + tab.id;
            tab.folder_id = "folder_" + tab.id;
            tab.toggle_id = "toggle_" + tab.id;
            tab.table_id = "table_" + tab.id;
            tab.items = tab.GetListFunc(cfg, tab);

            tab.is_error = tab.items.isError();

            tab.count = !tab.is_error && tab.items.getResult() != ""
                ? tab.items.getItemCount()
                : 0;
        }

        private Item GetSpotCheckList(TConfig cfg, TTab tab)
        {
            //, ISNULL(t3.in_stuff_b1, '') + ISNULL(t4.in_short_org, t3.in_short_org) AS 'map_short_org'
            string sql = @"
                SELECT
	                t2.id			    AS 'program_id'
	                , t2.in_name3	    AS 'program_name3'
	                , t2.in_short_name	AS 'program_sname'
	                , t3.*
	                , ISNULL(t4.in_short_org, t3.in_short_org) AS 'map_short_org'
                FROM
	                IN_MEETING_ALLOCATION t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PROGRAM t2 WITH(NOLOCK)
	                ON t2.id = t1.in_program
                INNER JOIN
	                IN_MEETING_USER t3 WITH(NOLOCK)
	                ON t3.source_id = t2.in_meeting
	                AND t3.in_l1 = t2.in_l1
	                AND t3.in_l2 = t2.in_l2
	                AND t3.in_l3 = t2.in_l3
				LEFT OUTER JOIN
					IN_ORG_MAP t4 WITH(NOLOCK)
					ON t4.in_stuff_b1 = t3.in_stuff_b1
                WHERE
	                t1.in_meeting = '{#meeting_id}'
	                AND t1.in_date_key = '{#in_date}'
	                AND t3.in_spot_checked = 1
                    AND t2.in_name3 LIKE '%{#in_gender}%'
                ORDER BY
	                t2.in_sort_order
	                , t3.in_spot_no
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_date}", cfg.in_date)
                .Replace("{#in_gender}", tab.in_gender);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        private string GetGroupStr(TConfig cfg, TTab tab)
        {
            string sql = @"
                SELECT 
                	DISTINCT t2.in_sort_order
                	, LEFT(ISNULL(t2.in_short_name, ''), 2)	AS 'group'
                	, ISNULL(t2.in_short_name, '')			AS 'in_short_name'
                FROM
                	IN_MEETING_ALLOCATION t1 WITH(NOLOCK)
                INNER JOIN
                	IN_MEETING_PROGRAM t2 WITH(NOLOCK)
                	ON t2.id = t1.in_program
                INNER JOIN
                	IN_MEETING_USER t3 WITH(NOLOCK)
                	ON t3.source_id = t2.in_meeting
                	AND t3.in_l1 = t2.in_l1
                	AND t3.in_l2 = t2.in_l2
                	AND t3.in_l3 = t2.in_l3
                WHERE
                	t1.in_meeting = '{#meeting_id}'
                	AND t1.in_date_key = '{#in_date}'
                	AND t3.in_spot_checked = 1
                	AND t2.in_short_name LIKE '%{#in_gender}%'
                ORDER BY
                	t2.in_sort_order
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_date}", cfg.in_date)
                .Replace("{#in_gender}", tab.in_gender);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();

            Dictionary<string, List<string>> map = new Dictionary<string, List<string>>();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string group = item.getProperty("group", "");
                string in_short_name = item.getProperty("in_short_name", "");
                string value = in_short_name.Replace(group, "");

                if (map.ContainsKey(group))
                {
                    map[group].Add(value);
                }
                else
                {
                    List<string> list = new List<string>();
                    list.Add(value);
                    map.Add(group, list);
                }
            }

            List<string> arr = new List<string>();

            foreach (var kv in map)
            {
                arr.Add("(" + kv.Key + string.Join("、", kv.Value) + ")");
            }

            return string.Join(" ", arr);
        }

        #endregion 抽磅登錄

        //日期選單
        private void AppendDateMenu(TConfig cfg, Item itmReturn)
        {
            string sql = @"
                SELECT 
	                DISTINCT t1.in_date_key
                FROM 
	                IN_MEETING_PEVENT t1 WITH(NOLOCK) 
                WHERE 
	                t1.in_meeting = '{#meeting_id}'
	                AND ISNULL(t1.in_date_key, '') <> ''
                ORDER BY 
	                t1.in_date_key
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item itmDays = cfg.inn.applySQL(sql);

            AppendItems(cfg, itmDays, "inn_date", "in_date_key", "in_date_key", itmReturn);
        }

        //組別選單
        private void AppendProgramMenu(TConfig cfg, Item itmReturn)
        {
            string sql = @"
                SELECT 
	                t2.id
	                , t2.in_name3 + ' (' + CAST(in_team_count AS VARCHAR) + ')' AS 'program_display'
                FROM
	                IN_MEETING_ALLOCATION t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PROGRAM t2 WITH(NOLOCK)
	                ON t2.id = t1.in_program
                WHERE
	                t1.in_meeting = '{#meeting_id}'
	                AND t1.in_date_key = '{#in_date}'
                ORDER BY
	                t2.in_sort_order
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_date}", cfg.in_date);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item itmPrograms = cfg.inn.applySQL(sql);

            AppendItems(cfg, itmPrograms, "inn_program", "id", "program_display", itmReturn);
        }

        //取得日期資料
        private TDay GetDayInfo(TConfig cfg)
        {
            string sql = @"
                SELECT t11.* FROM
                (
	                SELECT in_date_key, ROW_NUMBER() OVER (ORDER BY in_date_key) AS 'day_no' FROM
	                (
		                SELECT DISTINCT in_date_key FROM IN_MEETING_PEVENT WITH(NOLOCK) 
		                WHERE in_meeting = '{#meeting_id}'
						AND ISNULL(in_date_key, '') <> ''
	                ) t1
                ) t11
                WHERE t11.in_date_key = '{#in_date_key}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_date_key}", cfg.in_date);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item itmDay = cfg.inn.applySQL(sql);

            if (itmDay.isError() || itmDay.getResult() == "")
            {
                throw new Exception("查無比賽日期資料");
            }

            TDay day = new TDay
            {
                itmDay = itmDay,
                day_no = itmDay.getProperty("day_no", ""),
                tw_day = TwDay(cfg.in_date),
                spot_check_time = "pm " + cfg.in_time + ":00～" + cfg.in_time + ":45",
            };

            //110年4月20日（二） pm 08:00~08:45
            day.contents = day.tw_day + " " + day.spot_check_time;

            return day;
        }

        private void AppendItems(
            TConfig cfg
            , Item items
            , string type_name
            , string val_property
            , string lbl_property
            , Item itmReturn)
        {

            Item itmEmpty = cfg.inn.newItem();
            itmEmpty.setType(type_name);
            itmEmpty.setProperty("label", "請選擇");
            itmEmpty.setProperty("value", "");
            itmReturn.addRelationship(itmEmpty);

            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                item.setType(type_name);
                item.setProperty("value", item.getProperty(val_property, ""));
                item.setProperty("label", item.getProperty(lbl_property, ""));
                itmReturn.addRelationship(item);
            }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public Innosoft.InnovatorHelper InnH { get; set; }

            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }

            public string meeting_id { get; set; }
            public string program_id { get; set; }
            public string muid { get; set; }
            public string in_date { get; set; }
            public string in_time { get; set; }
            public string scene { get; set; }

            public Item itmMeeting { get; set; }
            public Item itmProgram { get; set; }
            public Item itmSurvey { get; set; }
            public Item itmOption { get; set; }

            public string in_title { get; set; }
            public string in_name3 { get; set; }
            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }
            public string in_sort_order { get; set; }
            public string in_round_code { get; set; }
            public string in_team_count { get; set; }

            public string survey_id { get; set; }
            public string in_extend_value2 { get; set; }

            public TWeight weight { get; set; }
            public TDay day { get; set; }
            public TXls xls { get; set; }

            public int round_code { get; set; }
            public int team_count { get; set; }
            public int spot_count { get; set; }

            public string xls_name { get; set; }
            public string xls_file { get; set; }

            public Dictionary<string, TTeam> TeamMap { get; set; }
        }

        private class TTab
        {
            public string id { get; set; }
            public string active { get; set; }
            public string title { get; set; }
            public string groups { get; set; }

            public string li_id { get; set; }
            public string a_id { get; set; }
            public string content_id { get; set; }
            public string folder_id { get; set; }
            public string toggle_id { get; set; }
            public string table_id { get; set; }

            public string in_gender { get; set; }

            public bool is_reject { get; set; }

            public Item items { get; set; }
            public bool is_error { get; set; }
            public int count { get; set; }

            public Func<TConfig, TTab, Item> GetListFunc { get; set; }
            public Func<TConfig, TTab, string> GetGroupFunc { get; set; }
            public Action<TConfig, TTab, StringBuilder> AppendListAction { get; set; }
        }

        private class TTable
        {
            public string id { get; set; }
            public string title { get; set; }
            public int count { get; set; }
        }

        private class TTeam
        {
            public Item Value { get; set; }
            public string in_spot_no { get; set; }
            public string in_spot_checked { get; set; }

            public string in_spot_weight { get; set; }
            public string in_spot_result { get; set; }
            public string in_spot_time { get; set; }

            //In_Meeting_User
            public string in_index { get; set; }
            public string in_creator_sno { get; set; }
        }

        private class TField
        {
            public string title { get; set; }
            public string property { get; set; }
            public string hcss { get; set; }
            public string css { get; set; }
            public string format { get; set; }
            public string defval { get; set; }
            public Func<TConfig, TField, Item, string> getVal { get; set; }
        }

        private class TXls
        {
            /// <summary>
            /// 樣板完整路徑
            /// </summary>
            public string source { get; set; }
            /// <summary>
            /// 檔案名稱
            /// </summary>
            public string file { get; set; }
            /// <summary>
            /// 檔案相對路徑
            /// </summary>
            public string url { get; set; }
        }

        private class TSheet
        {
            public int WsRow { get; set; }

            public int WsCol { get; set; }

            public string[] Heads { get; set; }

            public string Font { get; set; }

            public int SheetCount { get; set; }

            public List<string> Sheets { get; set; }

            public string export_type { get; set; }
        }

        private class TWeight
        {
            public string range { get; set; }
            public decimal min { get; set; }
            public decimal max { get; set; }

            //原上下限 + 5%
            public string spot_range { get; set; }
            public decimal spot_min { get; set; }
            public decimal spot_max { get; set; }

            public string in_spot_min { get; set; }
            public string in_spot_max { get; set; }
        }

        private class TDay
        {
            public Item itmDay { get; set; }
            public string day_no { get; set; }
            public string tw_day { get; set; }
            public string spot_check_time { get; set; }
            public string contents { get; set; }
        }

        private TWeight GetWeight(string range)
        {
            //0-60
            string[] arr = range.Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);
            if (arr == null || arr.Length < 2)
            {
                throw new Exception("未設定體重標準");
            }

            string v1 = arr[0];
            string v2 = arr[1];

            TWeight result = new TWeight
            {
                range = range,
                min = GetDecimal(v1),
                max = GetDecimal(v2),
            };

            if (result.min <= 0)
            {
                result.spot_min = 0;
            }
            else
            {
                result.spot_min = result.min - (result.min * 5) / 100;
            }

            if (result.max >= 999)
            {
                result.spot_max = 999;
            }
            else
            {
                result.spot_max = result.max + (result.max * 5) / 100;
            }

            result.spot_min = MapDecimal(result.spot_min);
            result.spot_max = MapDecimal(result.spot_max);
            result.spot_range = result.spot_min.ToString() + "-" + result.spot_max.ToString();

            result.in_spot_min = P1(result.spot_min);
            result.in_spot_max = P1(result.spot_max);

            return result;
        }

        private int GetSpotCount(int round_code)
        {
            if (round_code < 4)
            {
                return 1;
            }
            else if (round_code < 8)
            {
                return 2;
            }
            if (round_code < 16)
            {
                return 3;
            }
            else
            {
                return 4;
            }
        }

        private string SpotWeightInput(TConfig cfg, TField field, Item item)
        {
            string pid = item.getProperty("program_id", "");
            string muid = item.getProperty("id", "");
            string in_sno = item.getProperty("in_sno", "");
            string in_spot_weight = item.getProperty("in_spot_weight", "");

            return "<input type='number' class='eventKD'"
                + " id='" + in_sno + "'"
                + " value='" + in_spot_weight + "'"
                + " data-muid='" + muid + "'"
                + " data-pid='" + pid + "'"
                + " data-value='" + in_spot_weight + "'"
                + " >";
        }

        private string GetShortProgramName(TConfig cfg, TField field, Item item)
        {
            string program_name3 = item.getProperty("program_name3", "");

            string section_name = program_name3.Split(new char[] { '：' }, StringSplitOptions.RemoveEmptyEntries).FirstOrDefault();
            if (section_name == null) section_name = "";

            return section_name.Replace("中", "")
                .Replace("子", "")
                .Replace("生", "")
                .Replace("組", "")
                .Replace("第", "")
                .Replace("級", "");
        }

        private string GetProgramNameLink(TConfig cfg, TField field, Item item)
        {
            string pid = item.getProperty("program_id", "");
            string program_name3 = item.getProperty("program_name3", "");

            string section_name = program_name3.Split(new char[] { '：' }, StringSplitOptions.RemoveEmptyEntries).FirstOrDefault();
            if (section_name == null) section_name = "";

            return "<a href='javascript:void()' onclick='GoMatch(this)' data-pid='" + pid + "'>"
                + program_name3
                + "</a>";
        }

        private string GetWeightStatus(TConfig cfg, TField field, Item item)
        {
            string in_weight_status = item.getProperty("in_weight_status", "");
            string in_weight_result = item.getProperty("in_weight_result", "");

            switch (in_weight_status)
            {
                case "on": return "<span><font color='green'><b>過磅合格</b></font></span>";
                case "dq": return "<span><font color='red'><b>DQ(公斤不符)</b></font></span>";
                case "off": return "<span><font color='red'><b>DQ(未到)</b></font></span>";
                case "leave": return "<span><font color='red'><b>DQ(請假)</b></font></span>";
                default: return "<span><font color='green'><b>未過磅</b></font></span>";
            }

            // if (in_weight_result == "1")
            // {
            //     return "<font color='green'><b>過磅合格</b></font>";
            // }
            // else if (in_weight_result == "0")
            // {
            //     return "<font color='red'><b>DQ(公斤不符)</b></font>";
            // }
            // else
            // {
            //     return "<font color='red'><b>DQ(未過磅)</b></font>";
            // }
        }

        private string GetSpotResult(TConfig cfg, TField field, Item item)
        {
            string in_sno = item.getProperty("in_sno", "");
            string in_spot_checked = item.getProperty("in_spot_checked", "");
            string in_spot_result = item.getProperty("in_spot_result", "");
            string in_spot_time = item.getProperty("in_spot_time", "");
            string in_spot_status = item.getProperty("in_spot_status", "");

            string ctrl_id = in_sno + "_result";

            if (in_spot_checked != "1")
            {
                return "";
            }
            else
            {
                switch (in_spot_status)
                {
                    case "on": return "<span id='" + ctrl_id + "'><font color='green'><b>抽磅合格</b></font></span>";
                    case "dq": return "<span id='" + ctrl_id + "'><font color='red'><b>DQ(公斤不符)</b></font></span>";
                    case "off": return "<span id='" + ctrl_id + "'><font color='red'><b>DQ(抽磅未到)</b></font></span>";
                    case "leave": return "<span id='" + ctrl_id + "'><font color='red'><b>DQ(抽磅請假)</b></font></span>";
                    default: return "<span id='" + ctrl_id + "'><font color='green'><b> </b></font></span>";
                }
            }
        }

        private string GetSpotStatus(TConfig cfg, TField field, Item item)
        {
            string in_spot_status = item.getProperty("in_spot_status", "");
            string muid = item.getProperty("id", "");
            string in_sno = item.getProperty("in_sno", "");
            string in_weight_status = item.getProperty("in_weight_status", "");

            string options = "";
            switch (in_spot_status)
            {
                case "on":
                    options = "<option value=''>--</option>"
                        + "<option value='leave' style='background-color: yellow'>請假</option>"
                        + "<option value='off'   style='background-color: orange'>未到</option>"
                        + "<option value='dq'    style='background-color: #F8D7DA'>DQ</option>"
                        + "<option value='on'    style='background-color: white'  selected>抽磅合格</option>";
                    break;

                case "leave":
                    options = "<option value=''>--</option>"
                        + "<option value='leave' style='background-color: yellow' selected>請假</option>"
                        + "<option value='off'   style='background-color: orange'>未到</option>"
                        + "<option value='dq'    style='background-color: #F8D7DA'>DQ</option>"
                        + "<option value='on'    style='background-color: white'  >抽磅合格</option>";
                    break;

                case "off":
                    options = "<option value=''>--</option>"
                        + "<option value='leave' style='background-color: yellow'>請假</option>"
                        + "<option value='off'   style='background-color: orange' selected>未到</option>"
                        + "<option value='dq'    style='background-color: #F8D7DA'>DQ</option>"
                        + "<option value='on'    style='background-color: white'  >抽磅合格</option>";
                    break;

                case "dq":
                    options = "<option value=''>--</option>"
                        + "<option value='leave' style='background-color: yellow'>請假</option>"
                        + "<option value='off'   style='background-color: orange'>未到</option>"
                        + "<option value='dq'    style='background-color: #F8D7DA' selected>DQ</option>"
                        + "<option value='on'    style='background-color: white'  >抽磅合格</option>";
                    break;

                default:
                    options = "<option value=''>--</option>"
                        + "<option value='leave' style='background-color: yellow' >請假</option>"
                        + "<option value='off'   style='background-color: orange' >未到</option>"
                        + "<option value='dq'    style='background-color: #F8D7DA'>DQ</option>"
                        + "<option value='on'    style='background-color: white'  >抽磅合格</option>";
                    break;
            }

            StringBuilder builder = new StringBuilder();
            builder.Append("<select id='" + in_sno + "_status' class='form-control weight-status' data-id='" + muid + "' data-sno='" + in_sno + "' onchange='WeightStatus_Change(this)'>");
            builder.Append(options);
            builder.Append("</select>");

            return builder.ToString();
        }

        private string SignNameFunc(TConfig cfg, TField field, Item item)
        {
            string muid = item.getProperty("id", "");
            string in_spot_sign = item.getProperty("in_spot_sign", "");
            string result = "<button class='btn btn-sm btn-primary' data-muid='" + muid + "' onclick='GoSignName(this)'>簽名</button>";

            if (in_spot_sign != "")
            {
                result += " <button class='btn btn-sm btn-primary' data-fileid='" + in_spot_sign + "' onclick='ShowSignName(this)'><i class='fa fa-file-code-o'></i></button>";
            }
            return result;
        }

        private string GetSpotMaxWeight(TConfig cfg, TField field, Item item)
        {
            string in_spot_max = item.getProperty("in_spot_max", "");
            return "<span><b>" + in_spot_max + "kg</b></span>";
        }

        private string GetSpotMaxWeight2(TConfig cfg, TField field, Item item)
        {
            string in_spot_max = item.getProperty("in_spot_max", "");
            if (in_spot_max == "") return "";

            return in_spot_max + "kg";
        }

        private string GetSpotCheckBox(TConfig cfg, TField field, Item item)
        {
            string muid = item.getProperty("id", "");
            string in_spot_checked = item.getProperty("in_spot_checked", "");
            string ckd = "";

            if (in_spot_checked == "1")
            {
                ckd = "checked='checked'";
            }

            //return "<input class='form-check-input' type='checkbox' onclick='return false' " + ckd + " />";

            return "<input class='form-check-input' type='checkbox' data-muid='" + muid + "' onclick='ChangeStrawPlayer_Click(this)' " + ckd + " />";
        }

        //轉換日期
        private string GetDateTimeValue(string value, string format)
        {
            if (value == "") return "";
            DateTime dt = Convert.ToDateTime(value);
            return dt.ToString(format);
        }

        private string TwDay(string value)
        {
            if (value == "") return "";
            DateTime dt = Convert.ToDateTime(value);

            string day = (dt.Year - 1911) + "年"
                + dt.Month.ToString().PadLeft(2, '0') + "月"
                + dt.Day.ToString().PadLeft(2, '0') + "日";

            switch (dt.DayOfWeek)
            {
                case DayOfWeek.Sunday: return day + "（日）";
                case DayOfWeek.Monday: return day + "（一）";
                case DayOfWeek.Tuesday: return day + "（二）";
                case DayOfWeek.Wednesday: return day + "（三）";
                case DayOfWeek.Thursday: return day + "（四）";
                case DayOfWeek.Friday: return day + "（五）";
                case DayOfWeek.Saturday: return day + "（六）";
                default: return "";
            }
        }

        private int GetInt32(string value)
        {
            if (value == "" || value == "0") return 0;

            int result = 0;
            int.TryParse(value, out result);
            return result;
        }

        private decimal GetDecimal(string value)
        {
            if (value == "" || value == "0") return 0;
            if (value == "999") return 999;

            decimal result = 0;
            decimal.TryParse(value, out result);
            return result;
        }

        //取到小數點第一位
        private decimal MapDecimal(decimal value)
        {
            return Math.Round(value, 1, MidpointRounding.AwayFromZero);
        }

        private string P1(decimal value)
        {
            string result = value.ToString("0.#");
            if (!result.Contains('.')) result += ".0";
            return result;
        }
    }
}