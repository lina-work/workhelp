﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class in_meeting_program_remove_all : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
            目的: 清除組別相關資料
            輸入: meeting_id
            日期: 
                2020-12-01: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]in_meeting_program_remove_all";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "input dom: " + itmR.dom.InnerXml);

            string program_id = itmR.getProperty("program_id", "");
            string in_group_tag = itmR.getProperty("in_group_tag", "");

            string is_remove_team = itmR.getProperty("is_remove_team", "0");
            string is_remove_allocation = itmR.getProperty("is_remove_allocation", "0");

            string sql = "";

            sql = "SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + program_id + "'";
            Item itmParent = inn.applySQL(sql);

            sql = "SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE in_program = '" + program_id + "'";
            if (in_group_tag != "") sql += " AND in_group_tag = N'" + in_group_tag + "'";
            Item itmChildrens = inn.applySQL(sql);

            //刪除子組別
            for (int i = 0; i < itmChildrens.getItemCount(); i++)
            {
                string children_program = itmChildrens.getItemByIndex(i).getProperty("id", "");

                RemoveRelations(CCO, strMethodName, inn, itmChildrens, is_remove_team: "1", is_remove_allocation: "1");

                //刪除 children 組別
                inn.applySQL("DELETE FROM IN_MEETING_PROGRAM WHERE id = '" + children_program + "'");
            }

            RemoveRelations(CCO, strMethodName, inn, itmParent, is_remove_team: is_remove_team, is_remove_allocation: is_remove_allocation);

            return itmR;
        }

        private void RemoveRelations(Aras.Server.Core.CallContext CCO
            , string strMethodName
            , Innovator inn
            , Item itmProgram
            , string is_remove_team = "0"
            , string is_remove_allocation = "0")
        {
            string sql = "";
            Item itmSQL = null;

            string meeting_id = itmProgram.getProperty("in_meeting", "");
            string program_id = itmProgram.getProperty("id", "");
            string in_l1 = itmProgram.getProperty("in_l1", "");
            string in_l2 = itmProgram.getProperty("in_l2", "");
            string in_l3 = itmProgram.getProperty("in_l3", "");

            // //清除與會者檢錄資料
            // if (is_remove_team == "1")
            // {
            //     //含籤號與量級序號
            //     sql = @"UPDATE
            //             	IN_MEETING_USER
            //             SET
            //             	in_sign_no = ''
            //             	, in_section_no = ''
            //             	, in_rollcall_time = NULL 
            //             	, in_weight = NULL
            //             	, in_weight_createid = NULL 
            //             	, in_weight_createname = NULL 
            //             	, in_weight_time = NULL 
            //             	, in_weight_result = NULL 
            //             where
            //             	source_id = '{#meeting_id}'
            //             	AND ISNULL(in_l1, '') = N'{#in_l1}'
            //             	AND ISNULL(in_l2, '') = N'{#in_l2}'
            //             	AND ISNULL(in_l3, '') = N'{#in_l3}'
            //         ";
            // }
            // else
            // {
            //     //不含籤號與量級序號
            //     sql = @"UPDATE
            //             	IN_MEETING_USER
            //             SET
            //             	in_rollcall_time = NULL 
            //             	, in_weight = NULL
            //             	, in_weight_createid = NULL 
            //             	, in_weight_createname = NULL 
            //             	, in_weight_time = NULL 
            //             	, in_weight_result = NULL 
            //             where
            //             	source_id = '{#meeting_id}'
            //             	AND ISNULL(in_l1, '') = N'{#in_l1}'
            //             	AND ISNULL(in_l2, '') = N'{#in_l2}'
            //             	AND ISNULL(in_l3, '') = N'{#in_l3}'
            //         ";
            // }

            // sql = sql.Replace("{#meeting_id}", meeting_id)
            //     .Replace("{#in_l1}", in_l1)
            //     .Replace("{#in_l2}", in_l2)
            //     .Replace("{#in_l3}", in_l3);

            // //CCO.Utilities.WriteDebug(strMethodName, sql);

            // itmSQL = inn.applySQL(sql);



            //清除隊伍
            if (is_remove_team == "1")
            {
                //刪除隊伍
                sql = "DELETE FROM IN_MEETING_PTEAM WHERE source_id = '" + program_id + "'";
            }
            else
            {
                //清空名次與檢錄狀態
                sql = "UPDATE IN_MEETING_PTEAM SET"
                    + " in_final_rank = ''"
                    + ", in_check_result = ''"
                    + ", in_check_status = ''"
                    + ", in_rollcall_result = ''"
                    + ", in_weight_result = ''"
                    + ", in_weight_value = ''"
                    + " WHERE source_id = '" + program_id + "'";
            }

            //CCO.Utilities.WriteDebug(strMethodName, sql);

            itmSQL = inn.applySQL(sql);


            // 刪除明細
            sql = @"DELETE FROM IN_MEETING_PEVENT_DETAIL WHERE source_id IN (
                SELECT id FROM IN_MEETING_PEVENT WITH(NOLOCK) WHERE source_id = '{#program_id}'
            )";

            sql = sql.Replace("{#program_id}", program_id);

            //CCO.Utilities.WriteDebug(strMethodName, sql);

            itmSQL = inn.applySQL(sql);


            // 刪除場次
            sql = @"DELETE FROM IN_MEETING_PEVENT WHERE source_id = '{#program_id}'";

            sql = sql.Replace("{#program_id}", program_id);

            //CCO.Utilities.WriteDebug(strMethodName, sql);

            itmSQL = inn.applySQL(sql);


            if (is_remove_allocation == "1")
            {
                // 刪除場地分配
                sql = @"DELETE FROM IN_MEETING_ALLOCATION WHERE in_program = '{#program_id}'";

                sql = sql.Replace("{#program_id}", program_id);

                //CCO.Utilities.WriteDebug(strMethodName, sql);

                itmSQL = inn.applySQL(sql);
            }
        }
    }
 }