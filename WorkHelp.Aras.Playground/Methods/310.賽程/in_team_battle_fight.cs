﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;
using System.Net;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class in_team_battle_fight : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 團體戰-出賽、紀錄表
                日期: 
                    - 2021-10-13 創建 (Lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]in_team_battle_fight";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, this.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                InnH = _InnH,

                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                program_id = itmR.getProperty("program_id", ""),
                scene = itmR.getProperty("scene", ""),
            };

            switch (cfg.scene)
            {
                case "save":
                    Save(cfg, itmR);
                    break;

                case "fight_xlsx":
                    FightXlsx(cfg, itmR);
                    break;

                case "record_xlsx":
                    RecordXlsx(cfg, itmR);
                    break;

                default:
                    BattlePage(cfg, itmR);
                    break;
            }

            return itmR;
        }

        #region 團體出賽單

        //團體紀錄表 Excel
        private void RecordXlsx(TConfig cfg, Item itmReturn)
        {
            string sql = "";

            sql = "SELECT id, in_title FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'";
            cfg.itmMeeting = cfg.inn.applySQL(sql);

            sql = "SELECT TOP 1 * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + cfg.program_id + "'";
            cfg.itmProgram = cfg.inn.applySQL(sql);

            if (cfg.itmMeeting.isError() || cfg.itmMeeting.getResult() == ""
                || cfg.itmProgram.isError() || cfg.itmProgram.getResult() == "")
            {
                throw new Exception("賽程組別資料錯誤");
            }
            cfg.sect_start = GetIntVal(cfg.itmProgram.getProperty("in_sect_start", "0"));

            //場次資料
            var match = GetMatch(cfg, TMode.SubEvents, itmReturn);
            if (match == null || match.OrgList == null || match.Children == null)
            {
                throw new Exception("場次資料錯誤");
            }

            //子場次量級
            var sects = GetSectList(cfg, true);
            //匯出資料
            var export = ExportInfo(cfg, "team_record");

            ClosedXML.Excel.XLWorkbook workbook = new ClosedXML.Excel.XLWorkbook(export.template_Path);

            //團體出賽單
            var sheetEmpty = workbook.Worksheet(1);
            sheetEmpty.CopyTo(match.MatName);

            var sheet = workbook.Worksheet(workbook.Worksheets.Count);
            AppendRecordSheet(cfg, match, sects, sheet, itmReturn);

            workbook.Worksheets.Delete(1);

            string mt_title = cfg.itmMeeting.getProperty("in_title", "");
            string pg_title = cfg.itmProgram.getProperty("in_name2", "");
            string xlsName = mt_title + "_" + pg_title + "_紀錄表" + "_" + match.MatName;

            workbook.SaveAs(export.export_Path + xlsName + ".xlsx");

            itmReturn.setProperty("xls_name", xlsName + ".xlsx");
        }

        //團體紀錄表 Xlsx
        private void AppendRecordSheet(TConfig cfg, TMatch match, List<TSect> sects, ClosedXML.Excel.IXLWorksheet sheet, Item itmReturn)
        {
            string mt_title = cfg.itmMeeting.getProperty("in_title", "");
            string pg_title = cfg.itmProgram.getProperty("in_name2", "");

            SetHead(sheet.Cell(1, 1), mt_title, fontSize: 22);
            SetHead(sheet.Cell(2, 1), pg_title, fontSize: 22);
            SetNumHead(sheet.Cell(4, 3), match.SiteCode, fontSize: 22);
            SetNumHead(sheet.Cell(4, 7), match.TreeNo, fontSize: 22);
            SetNumHead(sheet.Cell(4, 13), match.InRoundSerial, fontSize: 22);

            int wsRow = 6;
            int wsCol = 0;

            string[] fight_arr = GetFightArr(sects.Count, cfg.sect_start, match.RoundSerial);

            for (int i = 0; i < sects.Count; i++)
            {
                var sect = sects[i];
                var sect_evt = match.Children.Find(x => x.SubId == sect.sub_id);
                if (sect_evt == null) continue;

                var fight_no = fight_arr[i];

                var f1 = sect_evt.Map["1"];
                var f2 = sect_evt.Map["2"];

                var n1 = f1.PlayerName == "" ? "沒有選手" : f1.PlayerName;
                var n2 = f2.PlayerName == "" ? "沒有選手" : f2.PlayerName;


                var v1 = f1.PlayerOrg + f1.PlayerTeam + Environment.NewLine + n1;
                var v2 = f2.PlayerOrg + f2.PlayerTeam + Environment.NewLine + n2;

                var weigth = sect.weight;
                //var weigth = sect.weight + Environment.NewLine + "(" + sect_evt.TreeNo + ")";

                //全國賽
                //SetCell(sheet.Cell(wsRow, wsCol + 1), fight_no);
                //中正盃、全柔錦
                SetNumCell(sheet.Cell(wsRow, wsCol + 1), sect_evt.TreeNo);
                SetCell(sheet.Cell(wsRow, wsCol + 2), sect.name, isBold: true);
                SetCell(sheet.Cell(wsRow, wsCol + 3), v1);
                SetCell(sheet.Cell(wsRow, wsCol + 14), v2);

                wsRow++;
                wsRow++;
            }
        }

        private string[] GetFightArr(int count, int start, int serial)
        {
            string[] result = new string[count + 1];

            if (start > 0)
            {
                int rno = start + (serial - 1);
                int sidx = rno - 1;
                int eidx = sidx + count;

                int no = 1;
                for (int i = sidx; i < eidx; i++)
                {
                    var idx = i % count;
                    result[idx] = no.ToString();
                    no++;
                }
            }
            else
            {
                for (int i = 0; i < count; i++)
                {
                    result[i] = "";
                }
            }

            result[count] = "";

            return result;
        }

        //團體出賽單 Excel
        private void FightXlsx(TConfig cfg, Item itmReturn)
        {
            string sql = "";

            sql = "SELECT id, in_title FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'";
            cfg.itmMeeting = cfg.inn.applySQL(sql);

            sql = "SELECT TOP 1 * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + cfg.program_id + "'";
            cfg.itmProgram = cfg.inn.applySQL(sql);

            if (cfg.itmMeeting.isError() || cfg.itmMeeting.getResult() == ""
                || cfg.itmProgram.isError() || cfg.itmProgram.getResult() == "")
            {
                throw new Exception("賽程組別資料錯誤");
            }

            cfg.sect_start = GetIntVal(cfg.itmProgram.getProperty("in_sect_start", "0"));

            string team_sign_no = itmReturn.getProperty("team_sign_no", "");

            //場次資料
            var match = GetMatch(cfg, TMode.OrgPlayers, itmReturn);

            //隊伍資料
            var rows = match.OrgList.FindAll(x => x.SignNo == team_sign_no);
            if (rows == null || rows.Count <= 0)
            {
                throw new Exception("隊伍資料錯誤");
            }

            //子場次量級
            var sects = GetSectList(cfg, true);


            var max_rcount = 0;
            for (int i = 0; i < rows.Count; i++)
            {
                var row = rows[i];
                for (int j = 0; j < sects.Count; j++)
                {
                    var sect = sects[j];
                    var sect_players = row.Players.FindAll(x => x.sub_id == sect.sub_id);
                    if (sect_players != null && sect_players.Count > max_rcount)
                    {
                        max_rcount = sect_players.Count;
                    }
                }
            }

            if (max_rcount > 7)
            {
                throw new Exception("報名人數異常");
            }

            bool is_max = false;
            string xls_parm_name = "team_fight";
            if (max_rcount == 7)
            {
                is_max = true;
                xls_parm_name = "team_fight7";
            }

            //匯出資料
            var export = ExportInfo(cfg, xls_parm_name);

            ClosedXML.Excel.XLWorkbook workbook = new ClosedXML.Excel.XLWorkbook(export.template_Path);

            for (int i = 0; i < rows.Count; i++)
            {
                var row = rows[i];
                if (row.OrgName == "")
                {
                    continue;
                }

                //團體出賽單
                var sheetEmpty = workbook.Worksheet(1);
                sheetEmpty.CopyTo(row.OrgName + row.TeamName);

                var sheet = workbook.Worksheet(workbook.Worksheets.Count);
                if (is_max)
                {
                    AppendFightSheet7(cfg, match, row, sects, sheet, itmReturn);
                }
                else
                {
                    AppendFightSheet6(cfg, match, row, sects, sheet, itmReturn);
                }
            }

            workbook.Worksheets.Delete(1);

            //string mt_title = cfg.itmMeeting.getProperty("in_title", "");
            // string in_fight_day = cfg.itmProgram.getProperty("in_fight_day", "");
            // string in_site_mat = cfg.itmProgram.getProperty("in_site_mat", "");
            string mt_title = match.MatName;
            string pg_title = cfg.itmProgram.getProperty("in_name2", "");
            string xlsName = mt_title + "_" + pg_title + "_出賽單";

            if (rows.Count == 1)
            {
                xlsName = mt_title + "_" + pg_title + "_出賽單" + "_" + rows[0].OrgName + rows[0].TeamName;
            }
            else if (rows.Count == 2)
            {
                xlsName = mt_title + "_" + pg_title + "_出賽單" + "_" + match.MatName;
            }

            workbook.SaveAs(export.export_Path + xlsName + ".xlsx");

            itmReturn.setProperty("xls_name", xlsName + ".xlsx");
        }


        //團體出賽單 Xlsx
        private void AppendFightSheet6(TConfig cfg, TMatch match, TOrg row, List<TSect> sects, ClosedXML.Excel.IXLWorksheet sheet, Item itmReturn)
        {
            string[] fight_arr = GetFightArr(sects.Count, cfg.sect_start, match.RoundSerial);

            var players = row.Players;

            SetHead(sheet.Cell(1, 1), cfg.itmMeeting.getProperty("in_title", ""), 18);
            SetHead(sheet.Cell(2, 1), cfg.itmProgram.getProperty("in_name2", ""), 16);

            SetNumHead(sheet.Cell(4, 2), match.SiteCode);
            SetNumHead(sheet.Cell(4, 4), match.TreeNo);
            //SetHead(sheet.Cell(4, 4), match.InRoundSerial);

            SetHead(sheet.Cell(4, 6), row.OrgName + row.TeamName, 16);
            //SetHead(sheet.Cell(6, 3), row.OrgName + row.TeamName);

            int wsRow = 7;
            int wsCol = 0;

            for (int i = 0; i < sects.Count; i++)
            {
                var sect = sects[i];
                var sect_players = players.FindAll(x => x.sub_id == sect.sub_id);

                string sect_name = sect.Value.getProperty("in_name", "");
                string sect_weight = sect.Value.getProperty("in_weight", "");
                string sect_ranges = sect.Value.getProperty("in_ranges", "");

                var fight_no = fight_arr[i];
                //四大賽: 全國賽
                //SetCell(sheet.Cell(wsRow, wsCol + 1), fight_no);
                //中正盃、全柔錦
                var sub_tree_no = match.TreeNo.ToString() + (i + 1).ToString().PadLeft(2, '0');
                SetNumCell(sheet.Cell(wsRow, wsCol + 1), sub_tree_no);
                SetCell(sheet.Cell(wsRow, wsCol + 2), sect_name, isBold: true);
                SetNumCell(sheet.Cell(wsRow, wsCol + 3), sect_weight, isBold: true);

                var p1 = sect_players.Count >= 1 ? sect_players[0] : GetEmptyPlayer(cfg);
                var p2 = sect_players.Count >= 2 ? sect_players[1] : GetEmptyPlayer(cfg);
                var p3 = sect_players.Count >= 3 ? sect_players[2] : GetEmptyPlayer(cfg);
                var p4 = sect_players.Count >= 4 ? sect_players[3] : GetEmptyPlayer(cfg);
                var p5 = sect_players.Count >= 5 ? sect_players[4] : GetEmptyPlayer(cfg);
                var p6 = sect_players.Count >= 6 ? sect_players[5] : GetEmptyPlayer(cfg);

                SetPlayerRowCells(cfg, sheet, wsRow, wsCol, sect, p1);
                wsRow++;

                SetPlayerRowCells(cfg, sheet, wsRow, wsCol, sect, p2);
                wsRow++;

                SetPlayerRowCells(cfg, sheet, wsRow, wsCol, sect, p3);
                wsRow++;

                SetPlayerRowCells(cfg, sheet, wsRow, wsCol, sect, p4);
                wsRow++;

                SetPlayerRowCells(cfg, sheet, wsRow, wsCol, sect, p5);
                wsRow++;

                SetPlayerRowCells(cfg, sheet, wsRow, wsCol, sect, p6);
                wsRow++;

                wsRow++;

                wsRow++;
            }
        }

        //團體出賽單 Xlsx
        private void AppendFightSheet7(TConfig cfg, TMatch match, TOrg row, List<TSect> sects, ClosedXML.Excel.IXLWorksheet sheet, Item itmReturn)
        {
            string[] fight_arr = GetFightArr(sects.Count, cfg.sect_start, match.RoundSerial);

            var players = row.Players;

            SetHead(sheet.Cell(1, 1), cfg.itmMeeting.getProperty("in_title", ""), 18);
            SetHead(sheet.Cell(2, 1), cfg.itmProgram.getProperty("in_name2", ""), 16);

            SetNumHead(sheet.Cell(4, 2), match.SiteCode);
            SetNumHead(sheet.Cell(4, 4), match.TreeNo);
            //SetHead(sheet.Cell(4, 4), match.InRoundSerial);

            SetHead(sheet.Cell(4, 6), row.OrgName + row.TeamName, 16);
            //SetHead(sheet.Cell(6, 3), row.OrgName + row.TeamName);

            int wsRow = 7;
            int wsCol = 0;

            for (int i = 0; i < sects.Count; i++)
            {
                var sect = sects[i];
                var sect_players = players.FindAll(x => x.sub_id == sect.sub_id);

                string sect_name = sect.Value.getProperty("in_name", "");
                string sect_weight = sect.Value.getProperty("in_weight", "");
                string sect_ranges = sect.Value.getProperty("in_ranges", "");

                var fight_no = fight_arr[i];
                //四大賽: 全國賽
                //SetCell(sheet.Cell(wsRow, wsCol + 1), fight_no);
                //中正盃、全柔錦
                var sub_tree_no = match.TreeNo.ToString() + (i + 1).ToString().PadLeft(2, '0');
                SetNumCell(sheet.Cell(wsRow, wsCol + 1), sub_tree_no);
                SetCell(sheet.Cell(wsRow, wsCol + 2), sect_name, isBold: true);
                SetNumCell(sheet.Cell(wsRow, wsCol + 3), sect_weight, isBold: true);

                var p1 = sect_players.Count >= 1 ? sect_players[0] : GetEmptyPlayer(cfg);
                var p2 = sect_players.Count >= 2 ? sect_players[1] : GetEmptyPlayer(cfg);
                var p3 = sect_players.Count >= 3 ? sect_players[2] : GetEmptyPlayer(cfg);
                var p4 = sect_players.Count >= 4 ? sect_players[3] : GetEmptyPlayer(cfg);
                var p5 = sect_players.Count >= 5 ? sect_players[4] : GetEmptyPlayer(cfg);
                var p6 = sect_players.Count >= 6 ? sect_players[5] : GetEmptyPlayer(cfg);
                var p7 = sect_players.Count >= 7 ? sect_players[6] : GetEmptyPlayer(cfg);

                SetPlayerRowCells(cfg, sheet, wsRow, wsCol, sect, p1);
                wsRow++;

                SetPlayerRowCells(cfg, sheet, wsRow, wsCol, sect, p2);
                wsRow++;

                SetPlayerRowCells(cfg, sheet, wsRow, wsCol, sect, p3);
                wsRow++;

                SetPlayerRowCells(cfg, sheet, wsRow, wsCol, sect, p4);
                wsRow++;

                SetPlayerRowCells(cfg, sheet, wsRow, wsCol, sect, p5);
                wsRow++;

                SetPlayerRowCells(cfg, sheet, wsRow, wsCol, sect, p6);
                wsRow++;

                SetPlayerRowCells(cfg, sheet, wsRow, wsCol, sect, p7);
                wsRow++;

                wsRow++;

                wsRow++;
            }
        }

        private void SetPlayerRowCells(TConfig cfg, ClosedXML.Excel.IXLWorksheet sheet, int wsRow, int wsCol, TSect sect, TMTeam p)
        {
            if (p.in_name == "")
            {
                SetCell(sheet.Cell(wsRow, wsCol + 5), "沒有選手");
                SetCell(sheet.Cell(wsRow, wsCol + 9), "");
            }
            else
            {
                SetCell(sheet.Cell(wsRow, wsCol + 5), p.in_name);
                SetCell(sheet.Cell(wsRow, wsCol + 9), GetPlayerSect(cfg, sect, p.in_sub_weight));
            }
        }

        private string GetPlayerSect(TConfig cfg, TSect sect, string weight)
        {
            var in_ranges = sect.Value.getProperty("in_ranges", "");
            if (in_ranges == "") return "";

            //var values = in_ranges + ",無";
            var values = in_ranges;
            var arr = values.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            if (arr == null || arr.Length == 0) return "";

            List<string> list = new List<string>();
            foreach (var v in arr)
            {
                if (v == weight)
                {
                    list.Add("■" + v);
                }
                else
                {
                    list.Add("□" + v);
                }
            }

            return string.Join(",", list);
        }

        ClosedXML.Excel.XLColor head_bg_color = ClosedXML.Excel.XLColor.FromHtml("#295C90");

        //設定標題列
        private void SetHead(ClosedXML.Excel.IXLCell cell, string title, int fontSize = 14)
        {
            cell.Value = title;

            cell.Style.Font.Bold = true;
            cell.DataType = ClosedXML.Excel.XLDataType.Text;
            cell.Style.Font.FontName = "標楷體";
            cell.Style.Font.SetFontSize(fontSize);

            // cell.Style.Fill.BackgroundColor = head_bg_color;
            // cell.Style.Font.FontColor = ClosedXML.Excel.XLColor.White;
            // cell.Style.Border.SetRightBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            // cell.Style.Border.SetTopBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            // cell.Style.Border.SetBottomBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            // cell.Style.Border.SetLeftBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
        }
        //設定標題列
        private void SetNumHead(ClosedXML.Excel.IXLCell cell, string title, int fontSize = 14)
        {
            cell.Value = title;

            cell.Style.Font.Bold = true;
            cell.DataType = ClosedXML.Excel.XLDataType.Text;
            cell.Style.Font.FontName = "Times New Roman";
            cell.Style.Font.SetFontSize(fontSize);

            // cell.Style.Fill.BackgroundColor = head_bg_color;
            // cell.Style.Font.FontColor = ClosedXML.Excel.XLColor.White;
            // cell.Style.Border.SetRightBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            // cell.Style.Border.SetTopBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            // cell.Style.Border.SetBottomBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            // cell.Style.Border.SetLeftBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
        }

        //設定標題列
        private void SetCell(ClosedXML.Excel.IXLCell cell, string title, bool isBold = false)
        {
            cell.Value = title;

            cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
            cell.Style.Font.Bold = isBold;
            cell.DataType = ClosedXML.Excel.XLDataType.Text;
            cell.Style.Font.FontName = "標楷體";

            // cell.Style.Font.SetFontSize(16);
            // cell.Style.Fill.BackgroundColor = head_bg_color;
            // cell.Style.Font.FontColor = ClosedXML.Excel.XLColor.White;
            // cell.Style.Border.SetRightBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            // cell.Style.Border.SetTopBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            // cell.Style.Border.SetBottomBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            // cell.Style.Border.SetLeftBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
        }

        //設定標題列
        private void SetNumCell(ClosedXML.Excel.IXLCell cell, string title, bool isBold = false)
        {
            cell.Value = title;

            cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
            cell.Style.Font.Bold = isBold;
            cell.DataType = ClosedXML.Excel.XLDataType.Text;
            cell.Style.Font.FontName = "Times New Roman";

            // cell.Style.Font.SetFontSize(16);
            // cell.Style.Fill.BackgroundColor = head_bg_color;
            // cell.Style.Font.FontColor = ClosedXML.Excel.XLColor.White;
            // cell.Style.Border.SetRightBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            // cell.Style.Border.SetTopBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            // cell.Style.Border.SetBottomBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            // cell.Style.Border.SetLeftBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
        }

        private TExport ExportInfo(TConfig cfg, string in_name)
        {
            TExport result = new TExport
            {
                template_Path = "",
                export_Path = "",
            };

            string aml = "<AML>" +
                "<Item type='In_Variable' action='get'>" +
                "<in_name>meeting_excel</in_name>" +
                "<Relationships>" +
                "<Item type='In_Variable_Detail' action='get'/>" +
                "</Relationships>" +
                "</Item></AML>";

            Item Vairable = cfg.inn.applyAML(aml);

            for (int i = 0; i < Vairable.getRelationships("In_Variable_Detail").getItemCount(); i++)
            {
                Item In_Variable_Detail = Vairable.getRelationships("In_Variable_Detail").getItemByIndex(i);
                if (In_Variable_Detail.getProperty("in_name", "") == in_name)
                    result.template_Path = In_Variable_Detail.getProperty("in_value", "");

                if (In_Variable_Detail.getProperty("in_name", "") == "export_path")
                    result.export_Path = In_Variable_Detail.getProperty("in_value", "");
                if (!result.export_Path.EndsWith(@"\"))
                    result.export_Path = result.export_Path + @"\";
            }

            return result;
        }

        private Item GetEventDetails(TConfig cfg, string event_id)
        {
            string sql = @"
                SELECT
	                t1.id               AS 'event_id'
	                , t1.in_tree_id
	                , t1.in_tree_no
	                , t1.in_round
	                , t1.in_round_serial
	                , t1.in_round_name
	                , t2.in_sign_foot
	                , t2.in_sign_no
	                , t11.id AS         'team_id'
	                , t11.in_short_org
	                , t11.in_team
	                , t11.in_name
	                , t11.in_sno
	                , t12.in_code AS 'site_code'
	                , t12.in_name AS 'site_name'
                FROM
	                IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                LEFT OUTER JOIN
	                VU_MEETING_PTEAM t11 WITH(NOLOCK)
	                ON t11.source_id = t1.source_id
	                AND t11.in_sign_no = t2.in_sign_no
                INNER JOIN
	                IN_MEETING_SITE t12 WITH(NOLOCK)
	                ON t12.id = t1.in_site
                WHERE
	                t1.id = '{#event_id}'
            ";

            sql = sql.Replace("{#event_id}", event_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        private TMatch GetMatch(TConfig cfg, TMode mode, Item itmReturn)
        {
            var result = new TMatch
            {
                EventId = itmReturn.getProperty("event_id", ""),
                OrgList = new List<TOrg>(),
                Children = new List<TSub>(),
            };

            Item items = GetEventDetails(cfg, result.EventId);
            if (items.isError() || items.getItemCount() <= 1)
            {
                throw new Exception("場次明細資料錯誤");
            }

            Item itmFirst = items.getItemByIndex(0);
            result.SiteCode = itmFirst.getProperty("site_code", "");
            result.SiteName = itmFirst.getProperty("site_name", "");
            result.TreeNo = itmFirst.getProperty("in_tree_no", "");
            result.InRoundSerial = itmFirst.getProperty("in_round_serial", "");
            result.RoundSerial = GetIntVal(result.InRoundSerial);
            result.MatName = "MAT" + result.SiteCode + "-" + result.TreeNo;

            int count = items.getItemCount();

            var f1 = new TOrg { SignFoot = "1" };
            var f2 = new TOrg { SignFoot = "2" };

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_sign_foot = item.getProperty("in_sign_foot", "");

                if (in_sign_foot == "1")
                {
                    f1.Value = item;
                }
                else if (in_sign_foot == "2")
                {
                    f2.Value = item;
                }
            }

            if (f1.Value == null) f1.Value = cfg.inn.newItem();
            if (f2.Value == null) f2.Value = cfg.inn.newItem();

            SetOrgValue(cfg, f1, f2, mode);
            SetOrgValue(cfg, f2, f1, mode);

            result.OrgList.Add(f1);
            result.OrgList.Add(f2);

            if (mode == TMode.SubEvents)
            {
                result.Children = GetSubEvents(cfg, result);
            }

            return result;
        }

        private List<TSub> GetSubEvents(TConfig cfg, TMatch match)
        {
            List<TSub> result = new List<TSub>();

            string sql = @"
                SELECT
	                t1.id		AS 'event_id'
	                , t1.in_tree_id
	                , t1.in_tree_no
	                , t1.in_sub_id
	                , t2.id		AS 'detail_id'
	                , t2.in_sign_foot
	                , t2.in_sign_no
	                , t2.in_player_org
	                , t2.in_player_team
	                , t2.in_player_name
	                , t2.in_player_sno
                FROM
	                IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                INNER JOIN
	                IN_MEETING_SITE t12 WITH(NOLOCK)
	                ON t12.id = t1.in_site
                WHERE
	                t1.in_parent = '{#event_id}'
				ORDER BY
					t1.in_sub_id
            ";

            sql = sql.Replace("{#event_id}", match.EventId);
            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var sub_id = GetIntVal(item.getProperty("in_sub_id", "0"));
                var in_sign_foot = item.getProperty("in_sign_foot", "");

                var child = result.Find(x => x.SubId == sub_id);
                if (child == null)
                {
                    child = new TSub
                    {
                        EventId = item.getProperty("event_id", ""),
                        TreeId = item.getProperty("in_tree_id", ""),
                        TreeNo = item.getProperty("in_tree_no", ""),
                        SubId = sub_id,
                        Value = item,
                        Map = new Dictionary<string, TPlayer>(),
                    };
                    result.Add(child);
                }

                var player = new TPlayer
                {
                    DetailId = item.getProperty("detail_id", ""),
                    SignFoot = in_sign_foot,
                    SignNo = item.getProperty("in_sign_no", ""),
                    PlayerOrg = item.getProperty("in_player_org", ""),
                    PlayerTeam = item.getProperty("in_player_team", ""),
                    PlayerName = item.getProperty("in_player_name", ""),
                    PlayerSno = item.getProperty("in_player_sno", ""),
                };

                child.Map.Add(in_sign_foot, player);
            }

            return result;
        }

        private void SetOrgValue(TConfig cfg, TOrg a, TOrg b, TMode mode)
        {
            a.OrgName = a.Value.getProperty("in_short_org", "");
            a.SignNo = a.Value.getProperty("in_sign_no", "");
            a.TeamId = a.Value.getProperty("team_id", "");
            a.TeamName = a.Value.getProperty("in_team", "");

            a.Enemy = b;
            a.EnemyName = b.Value.getProperty("in_short_org", "");

            if (mode == TMode.OrgPlayers && a.TeamId != "")
            {
                a.Players = GetPlayerList(cfg, a.TeamId);
            }
            else
            {
                a.Players = new List<TMTeam>();
            }
        }

        private List<TMTeam> GetPlayerList(TConfig cfg, string team_id)
        {
            List<TMTeam> list = new List<TMTeam>();

            string sql = "SELECT * FROM IN_MEETING_PTEAM WITH(NOLOCK)"
                    + " WHERE in_type = 's'"
                    + " AND in_parent = '" + team_id + "'"
                    + " AND ISNULL(in_weight_message, '') = ''"
                    + " AND ISNULL(in_name, '') <> ''"
                    + " ORDER BY in_sub_id, in_sub_serial";

            Item itmSubs = cfg.inn.applySQL(sql);

            int count = itmSubs.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = itmSubs.getItemByIndex(i);
                list.Add(GetMTeam(cfg, item));
            }

            return list;
        }

        //空隊伍
        private TMTeam GetEmptyPlayer(TConfig cfg)
        {
            return new TMTeam
            {
                in_name = "",
                in_sub_weight = "",
                in_weight_message = "",
                Value = cfg.inn.newItem(),
            };
        }

        private class TExport
        {
            public string template_Path { get; set; }
            public string export_Path { get; set; }
        }

        private enum TMode
        {
            None = 0,
            OrgPlayers = 1,
            SubEvents = 2,
        }

        private class TMatch
        {
            public string EventId { get; set; }
            public string SiteName { get; set; }
            public string SiteCode { get; set; }
            public string MatName { get; set; }
            public string TreeId { get; set; }
            public string TreeNo { get; set; }
            public string InRoundSerial { get; set; }
            public int RoundSerial { get; set; }

            public Item Value { get; set; }
            public List<TOrg> OrgList { get; set; }
            public List<TSub> Children { get; set; }
        }

        private class TSub
        {
            public string EventId { get; set; }
            public string TreeId { get; set; }
            public string TreeNo { get; set; }
            public int SubId { get; set; }
            public Item Value { get; set; }
            public Dictionary<string, TPlayer> Map { get; set; }
        }

        private class TPlayer
        {
            public string DetailId { get; set; }
            public string SignFoot { get; set; }
            public string SignNo { get; set; }
            public string PlayerOrg { get; set; }
            public string PlayerTeam { get; set; }
            public string PlayerName { get; set; }
            public string PlayerSno { get; set; }
        }

        private class TOrg
        {
            public string OrgName { get; set; }
            public string TeamId { get; set; }
            public string TeamName { get; set; }
            public string SignFoot { get; set; }
            public string SignNo { get; set; }

            public string EnemyName { get; set; }

            /// <summary>
            /// 我方
            /// </summary>
            public Item Value { get; set; }

            /// <summary>
            /// 選手
            /// </summary>
            public List<TMTeam> Players { get; set; }

            /// <summary>
            /// 對手
            /// </summary>
            public TOrg Enemy { get; set; }
        }

        #endregion 團體過磅單

        //儲存
        private void Save(TConfig cfg, Item itmReturn)
        {
            string did = itmReturn.getProperty("did", "");
            string old_name = itmReturn.getProperty("old_name", "");
            string new_name = itmReturn.getProperty("new_name", "");
            string new_sno = itmReturn.getProperty("new_sno", "");

            Item itmChild = GetEvtDtlById(cfg, did);
            if (itmChild.isError() || itmChild.getResult() == "")
            {
                throw new Exception("查無子場次明細資料");
            }

            string detail_id = itmChild.getProperty("detail_id", "");
            string in_parent = itmChild.getProperty("in_parent", "");
            string in_sub_id = itmChild.getProperty("in_sub_id", "");
            string in_sign_foot = itmChild.getProperty("in_sign_foot", "");

            Item itmParent = GetEvtDtlByFoot(cfg, in_parent, in_sign_foot);
            if (itmParent.isError() || itmParent.getResult() == "")
            {
                throw new Exception("查無主場次明細資料");
            }

            int sub_id = GetIntVal(in_sub_id);

            int sign_no = in_sign_foot == "1"
                    ? (10 + sub_id) * -1
                    : (20 + sub_id) * -1;


            string sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET "
                + " in_sign_no = '" + sign_no + "'"
                + " , in_player_org = N'" + itmParent.getProperty("player_org", "") + "'"
                + " , in_player_team = N'" + itmParent.getProperty("player_team", "") + "'"
                + " , in_player_name = N'" + new_name + "'"
                + " , in_player_sno = N'" + new_sno + "'"
                + " WHERE id = '" + detail_id + "'";

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "變更選手 from: " + old_name + " to: " + new_name
                + "，sql: " + sql);

            Item itmSQL = cfg.inn.applySQL(sql);

            if (itmSQL.isError())
            {
                throw new Exception("出賽選手變更失敗");
            }
        }

        private Item GetEvtDtlById(TConfig cfg, string did)
        {
            string sql = @"
                SELECT
	                t1.id           AS 'event_id'
                    , t1.in_type
                    , t1.in_parent
                    , t1.in_sub_id
	                , t2.id         AS 'detail_id'
                    , t2.in_sign_foot
                    , t2.in_sign_no
                FROM
	                IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                WHERE
	                t2.id = '{#did}'
            ";

            sql = sql.Replace("{#did}", did);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);

        }

        private Item GetEvtDtlByFoot(TConfig cfg, string eid, string foot)
        {
            string sql = @"
                SELECT
	                ISNULL(t11.map_short_org, t2.in_player_org) AS 'player_org'
	                , ISNULL(t11.in_team, t2.in_player_team)    AS 'player_team'
	                , ISNULL(t11.in_name, t2.in_player_name)    AS 'player_name'
	                , ISNULL(t11.in_sno, t2.in_player_sno)      AS 'player_sno'
                FROM
	                IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                LEFT OUTER JOIN
	                VU_MEETING_PTEAM t11 WITH(NOLOCK)
	                ON t11.source_id = t1.source_id
	                AND t11.in_sign_no = t2.in_sign_no
                WHERE
	                t1.id = '{#eid}'
	                AND t2.in_sign_foot = '{#foot}'
            ";

            sql = sql.Replace("{#eid}", eid)
                .Replace("{#foot}", foot);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        //頁面
        private void BattlePage(TConfig cfg, Item itmReturn)
        {
            //賽事資訊
            MeetingInfo(cfg);

            itmReturn.setProperty("in_title", cfg.itmMeeting.getProperty("in_title", ""));

            AppendProgramMenu(cfg, itmReturn);

            if (cfg.program_id != "")
            {
                cfg.itmProgram = cfg.inn.applySQL("SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + cfg.program_id + "'");

                string program_id = cfg.itmProgram.getProperty("id", "");
                itmReturn.setProperty("program_id", program_id);

                //隊伍
                var rows = RowsFromMTeam(cfg);
                //場次
                var evts = GetEvents(cfg);
                //繫結場次與隊伍
                BindEventRows(cfg, evts, rows);
                //對戰 Table
                BattleTable(cfg, evts, itmReturn);
            }
        }

        //附加組別選單
        private void AppendProgramMenu(TConfig cfg, Item itmReturn)
        {
            string sql = "SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE in_meeting = '" + cfg.meeting_id + "'"
                    + " AND in_l1 = N'團體組'"
                    + " ORDER BY in_sort_order";

            Item items = cfg.inn.applySQL(sql);

            string in_type = "inn_program";

            Item itmEmpty = cfg.inn.newItem(in_type);
            itmEmpty.setProperty("value", "");
            itmEmpty.setProperty("text", "請選擇");
            itmReturn.addRelationship(itmEmpty);

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string id = item.getProperty("id", "");
                string in_name2 = item.getProperty("in_name2", "");
                string in_team_count = item.getProperty("in_team_count", "0");

                item.setType(in_type);
                item.setProperty("value", id);
                item.setProperty("text", in_name2 + " (" + in_team_count + ")");
                itmReturn.addRelationship(item);

                if (count == 1 && i == 0)
                {
                    cfg.program_id = id;
                }
            }
        }

        //與會者隊伍
        private List<TRow> RowsFromMTeam(TConfig cfg)
        {
            //主隊伍資料
            string sql = @"SELECT * FROM IN_MEETING_PTEAM WITH(NOLOCK)"
                + " WHERE source_id = '" + cfg.itmProgram.getProperty("id", "")
                + "' ORDER BY in_stuff_b1, in_team, in_type, in_sub_id, in_sub_serial";

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            var itmMTeams = cfg.inn.applySQL(sql);

            List<TRow> rows = new List<TRow>();

            int count = itmMTeams.getItemCount();
            if (count <= 0) return rows;

            for (int i = 0; i < count; i++)
            {
                Item item = itmMTeams.getItemByIndex(i);
                string team_id = item.getProperty("id", "");
                string in_type = item.getProperty("in_type", "");
                string in_parent = item.getProperty("in_parent", "");
                string in_name = item.getProperty("in_name", "");

                var row = default(TRow);

                if (in_type == "p")
                {
                    row = new TRow
                    {
                        TeamId = team_id,
                        SortNo = item.getProperty("in_stuff_b1", ""),
                        OrgName = item.getProperty("in_short_org", ""),
                        TeamName = item.getProperty("in_team", ""),
                        InSignNo = item.getProperty("in_sign_no", ""),
                        InJudoNo = item.getProperty("in_judo_no", ""),
                        Parent = GetMTeam(cfg, item),
                        Children = new List<TMTeam>(),
                    };

                    row.SignNo = GetIntVal(row.InSignNo);

                    rows.Add(row);
                }
                else if (in_type == "s" && in_parent != "" && in_name != "")
                {
                    row = rows.Find(x => x.TeamId == in_parent);
                    if (row == null)
                    {
                        //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "隊伍資料異常(parent) dom: " + item.dom.InnerXml);
                    }
                    else
                    {
                        row.Children.Add(GetMTeam(cfg, item));
                    }
                }
                else
                {
                    //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "隊伍資料異常 dom: " + item.dom.InnerXml);
                }
            }

            return rows;
        }

        private TMTeam GetMTeam(TConfig cfg, Item item)
        {
            var result = new TMTeam
            {
                team_id = item.getProperty("id", ""),
                in_short_org = item.getProperty("in_short_org", ""),
                in_team = item.getProperty("in_team", ""),
                in_name = item.getProperty("in_name", ""),
                in_sno = item.getProperty("in_sno", ""),
                in_current_org = item.getProperty("in_current_org", ""),

                in_type = item.getProperty("in_type", ""),
                in_parent = item.getProperty("in_parent", ""),
                in_sub_sect = item.getProperty("in_sub_sect", ""),
                in_sub_id = item.getProperty("in_sub_id", ""),
                in_sub_serial = item.getProperty("in_sub_serial", ""),
                in_sub_weight = item.getProperty("in_sub_weight", ""),
                in_muser = item.getProperty("in_muser", ""),

                in_sign_no = item.getProperty("in_sign_no", ""),
                in_judo_no = item.getProperty("in_judo_no", ""),

                in_weight_message = item.getProperty("in_weight_message", ""),

                Value = item,
            };

            result.sign_no = GetIntVal(result.in_sign_no);
            result.sub_id = GetIntVal(result.in_sub_id);
            result.sub_serial = GetIntVal(result.in_sub_serial);

            return result;
        }

        //繫結場次與隊伍
        private void BindEventRows(TConfig cfg, List<TEvent> mevents, List<TRow> teams)
        {
            foreach (var mevent in mevents)
            {
                BindFootRow(cfg, mevent.Foots, "1", teams);
                BindFootRow(cfg, mevent.Foots, "2", teams);
            }
        }

        //繫結場次腳位與隊伍
        private void BindFootRow(TConfig cfg, Dictionary<string, TFoot> foots, string sign_foot, List<TRow> teams)
        {
            if (foots.ContainsKey(sign_foot))
            {
                var foot = foots[sign_foot];
                if (foot.sign_no > 0)
                {
                    foot.OrgTeam = teams.Find(x => x.SignNo == foot.sign_no);
                }

                if (foot.OrgTeam == null)
                {
                    foot.OrgTeam = new TRow
                    {
                        OrgName = "",
                        Parent = new TMTeam { },
                        Children = new List<TMTeam>(),
                    };
                }
            }
        }

        //表格箱
        private void BattleTable(TConfig cfg, List<TEvent> list, Item itmReturn)
        {
            StringBuilder builder = new StringBuilder();

            builder.Append("<div class='showsheet_ clearfix'>");
            builder.Append("    <div>");
            builder.Append("        <div class='float-btn clearfix'>");
            builder.Append("        </div>");

            if (cfg.scene == "score")
            {
                AppendScoreTable(cfg, list, builder);
            }
            else
            {
                AppendBattleTable(cfg, list, builder);
            }

            builder.Append("    </div>");

            builder.Append("</div>");

            itmReturn.setProperty("inn_tabs", builder.ToString());
        }

        private void AppendScoreTable(TConfig cfg, List<TEvent> list, StringBuilder builder)
        {
            var sects = GetSectList(cfg, false);

            List<TField> fields = new List<TField>();
            fields.Add(new TField { title = "場次", format = TFType.None, getValue = EventName, css = "treeStyle" });
            fields.Add(new TField { title = "單位", format = TFType.None, getValue = OrgName, css = "cellStyle" });
            fields.Add(new TField { title = "隊別", format = TFType.None, getValue = TeamName, css = "cellStyle" });
            fields.Add(new TField { title = "籤號", format = TFType.None, getValue = SignNo, css = "cellStyle" });
            for (int i = 0; i < sects.Count; i++)
            {
                var sect = sects[i];
                fields.Add(new TField { title = sect.name, format = TFType.None, getValue = ScoreVal, sect = sect, css = "cellStyle" });
            }
            fields.Add(new TField { title = "比數", format = TFType.None, getValue = SumVal, css = "cellStyle" });

            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            head.Append("<thead>");
            head.Append("  <tr>");
            for (int i = 0; i < fields.Count; i++)
            {
                var field = fields[i];
                head.Append("    <th class='text-center' rowspan='1' data-cell-style='" + field.css + "'>");
                head.Append(field.title);
                head.Append("    </th>");
            }
            head.Append("  </tr>");
            head.Append("</thead>");


            int count = list.Count;
            body.Append("<tbody>");
            for (int i = 0; i < count; i++)
            {
                var evt = list[i];

                var evt_field = fields[0];

                body.Append("  <tr>");
                body.Append("      <td rowspan='2' >");
                body.Append(evt_field.getValue(cfg, evt_field, evt, "", null));
                body.Append("      </td>");
                AppendTeamCols(cfg, fields, evt, "1", body);
                body.Append("  </tr>");

                body.Append("  <tr class='tree_row'>");
                AppendTeamCols(cfg, fields, evt, "2", body);
                body.Append("  </tr>");
            }
            body.Append("</tbody>");



            string tb_id = "team_battle_table";

            builder.Append("<table id='" + tb_id + "'"
                + " class='table table-hover table-bordered table-rwd rwd'"
                + " style='background-color: #fff;' "
                + " data-toggle='table' "
                + " data-search='false'"
                + " data-search-align='left'"
                + ">");

            builder.Append(head);
            builder.Append(body);
            builder.Append("</table>");

            //builder.Append("<script>");
            //builder.Append("    $('#" + tb_id + "').bootstrapTable({});");
            //builder.Append("    if($(window).width() <= 768) { $('#" + tb_id + "').bootstrapTable('toggleView'); }");
            //builder.Append("</script>");
        }

        private void AppendBattleTable(TConfig cfg, List<TEvent> list, StringBuilder builder)
        {
            var sects = GetSectList(cfg, false);

            List<TField> fields = new List<TField>();
            fields.Add(new TField { title = "場次", format = TFType.None, getValue = EventName, css = "treeStyle" });
            fields.Add(new TField { title = "單位", format = TFType.None, getValue = OrgName, css = "cellStyle" });
            fields.Add(new TField { title = "隊別", format = TFType.None, getValue = TeamName, css = "cellStyle" });
            fields.Add(new TField { title = "籤號", format = TFType.None, getValue = SignNo, css = "cellStyle" });
            for (int i = 0; i < sects.Count; i++)
            {
                var sect = sects[i];
                fields.Add(new TField { title = sect.name, format = TFType.None, getValue = CtrlVal, sect = sect, css = "cellStyle" });
            }
            fields.Add(new TField { title = "功能", format = TFType.None, getValue = BtnValue, css = "cellStyle" });

            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            head.Append("<thead>");
            head.Append("  <tr>");
            for (int i = 0; i < fields.Count; i++)
            {
                var field = fields[i];
                head.Append("    <th class='text-center' rowspan='1' data-cell-style='" + field.css + "'>");
                head.Append(field.title);
                head.Append("    </th>");
            }
            head.Append("  </tr>");
            head.Append("</thead>");


            int count = list.Count;
            body.Append("<tbody>");
            for (int i = 0; i < count; i++)
            {
                var evt = list[i];

                var evt_field = fields[0];

                body.Append("  <tr>");
                body.Append("      <td rowspan='2' >");
                body.Append(evt_field.getValue(cfg, evt_field, evt, "", null));
                body.Append("      </td>");
                AppendTeamCols(cfg, fields, evt, "1", body);
                body.Append("  </tr>");

                body.Append("  <tr class='tree_row'>");
                AppendTeamCols(cfg, fields, evt, "2", body);
                body.Append("  </tr>");
            }
            body.Append("</tbody>");



            string tb_id = "team_battle_table";

            builder.Append("<table id='" + tb_id + "'"
                + " class='table table-hover table-bordered table-rwd rwd'"
                + " style='background-color: #fff;' "
                + " data-toggle='table' "
                + " data-search='false'"
                + " data-search-align='left'"
                + ">");

            builder.Append(head);
            builder.Append(body);
            builder.Append("</table>");

            //builder.Append("<script>");
            //builder.Append("    $('#" + tb_id + "').bootstrapTable({});");
            //builder.Append("    if($(window).width() <= 768) { $('#" + tb_id + "').bootstrapTable('toggleView'); }");
            //builder.Append("</script>");
        }

        private void AppendTeamCols(TConfig cfg, List<TField> fields, TEvent evt, string foot, StringBuilder body)
        {
            var team = evt.Foots[foot].OrgTeam;

            for (int i = 1; i < fields.Count; i++)
            {
                var field = fields[i];
                body.Append("      <td >");
                if (team == null)
                {
                    body.Append("&nbsp;");
                }
                else
                {
                    body.Append(field.getValue(cfg, field, evt, foot, team));
                }
                body.Append("      </td>");
            }
        }

        private string EventName(TConfig cfg, TField field, TEvent evt, string foot, TRow team)
        {
            string mat = "MAT " + evt.site_code + "-" + evt.in_tree_no;

            string btn = "<button class='btn btn-sm btn-primary'"
                + " data-eid='" + evt.event_id + "'"
                + " onclick='ExportRecord(this)'>"
                + "紀錄表"
                + "</button>";

            return mat + "<br>" + btn;
        }

        private string OrgName(TConfig cfg, TField field, TEvent evt, string foot, TRow team)
        {
            return team.OrgName;
        }
        private string SignNo(TConfig cfg, TField field, TEvent evt, string foot, TRow team)
        {
            return team.InJudoNo;
        }

        private string TeamName(TConfig cfg, TField field, TEvent evt, string foot, TRow team)
        {
            return team.TeamName;
        }

        private string ScoreVal(TConfig cfg, TField field, TEvent evt, string foot, TRow team)
        {
            string points = "";
            var sect = field.sect;
            var child = evt.Children.Find(x => x.sub_id == sect.sub_id);
            if (child != null && child.Foots.ContainsKey(foot))
            {
                var f = child.Foots[foot];
                string in_status = f.Value.getProperty("in_status", "");
                string in_points = f.Value.getProperty("in_points", "0");
                string in_correct_count = f.Value.getProperty("in_correct_count", "0");
                string in_player_name = f.Value.getProperty("in_player_name", "");

                switch (in_status)
                {
                    case "1": return "<span class='team_score_b'>" + in_points + "</span>";
                    case "0":
                        if (in_correct_count == "3")
                        {
                            return "<span class='team_score_c'>" + in_points + " (S3)</span>";
                        }
                        else if (in_player_name == "")
                        {
                            return "<span class='team_score_c'>" + in_points + " (無)</span>";
                        }
                        else
                        {
                            return "<span class='team_score_c'>" + in_points + "</span>";
                        }
                    default: return "<span class='team_score_a'>&nbsp;</span>";
                }
            }
            return points;
        }

        private string SumVal(TConfig cfg, TField field, TEvent evt, string foot, TRow team)
        {
            var cnt = 0;

            foreach (var child in evt.Children)
            {
                if (child.Foots.ContainsKey(foot))
                {
                    var itmDetail = child.Foots[foot].Value;
                    var in_status = itmDetail.getProperty("in_status", "");
                    if (in_status == "1")
                    {
                        cnt++;
                    }
                }
            }

            return cnt.ToString();
        }

        private string BtnValue(TConfig cfg, TField field, TEvent evt, string foot, TRow team)
        {
            string pid = team.Parent.team_id;
            string eid = evt.event_id;
            string sign = team.InSignNo;

            if (team.SignNo <= 0)
            {
                return "&nbsp;";
            }

            return "<button class='btn btn-sm btn-primary'"
                + " data-eid='" + eid + "'"
                + " data-pid='" + pid + "'"
                + " data-sign='" + sign + "'"
                + " onclick='ExportXlsx(this)'>"
                + "出賽單"
                + "</button>";
        }

        private string CtrlVal(TConfig cfg, TField field, TEvent evt, string foot, TRow team)
        {
            var sect = field.sect;

            string sub = sect.sub_id.ToString();
            string pid = team.Parent.team_id;
            string sign = team.Parent.in_sign_no;
            string eid = evt.event_id;
            string did = "";
            string name = "";
            string sno = "";

            var child = evt.Children.Find(x => x.sub_id == sect.sub_id);
            if (child != null && child.Foots.ContainsKey(foot))
            {
                var f = child.Foots[foot];
                did = f.detail_id;
                name = f.Value.getProperty("in_player_name", "");
                sno = f.Value.getProperty("in_player_sno", "");
            }


            StringBuilder builder = new StringBuilder();
            builder.Append("<select class='form-control btn btn-default team-select'");
            builder.Append(" data-did='" + did + "'");
            builder.Append(" data-sub='" + sub + "'");
            builder.Append(" data-name='" + name + "'");
            builder.Append(" data-sign='" + sign + "'");
            builder.Append(" onchange='BattlePlayer_Change(this)' >");

            builder.Append("    <option value=''>請選擇</option>");
            builder.Append("    <option value='沒有選手'>沒有選手</option>");

            var players = default(List<TMTeam>);
            if (sect.sub_id == 7)
            {
                players = team.Children;
            }
            else
            {
                players = team.Children.FindAll(x => x.sub_id == sect.sub_id);
            }

            if (players != null)
            {
                foreach (var player in players)
                {
                    string v = player.in_name + player.in_weight_message;
                    builder.Append("    <option value='" + v + "' muid='" + player.in_muser + "'>" + v + "</option>");
                }
            }

            builder.Append("</select>");

            return builder.ToString();
        }

        private List<TEvent> GetEvents(TConfig cfg)
        {
            List<TEvent> main_evts = GetMainEvents(cfg);
            AppendSubEvents(cfg, main_evts);
            return main_evts;
        }

        private void AppendSubEvents(TConfig cfg, List<TEvent> main)
        {
            string sql = @"
                SELECT
	                t1.id AS 'event_id'
	                , t1.in_tree_name
	                , t1.in_tree_id
	                , t1.in_tree_no
	                , t1.in_tree_alias
                    , t1.in_win_sign_no
                    , t1.in_parent
                    , t1.in_sub_id
                    , t2.id AS 'detail_id'
                    , t2.in_sign_foot
	                , t2.in_sign_no
	                , t11.id                                        AS 'team_id'
	                , ISNULL(t11.map_short_org, t2.in_player_org)   AS 'in_player_org'
	                , ISNULL(t11.in_team, t2.in_player_team)        AS 'in_player_team'
	                , ISNULL(t11.in_name, t2.in_player_name)        AS 'in_player_name'
	                , ISNULL(t11.in_sno, t2.in_player_sno)          AS 'in_player_sno'
	                , t2.in_player_status
                    , t2.in_status
                    , t2.in_points
                    , t2.in_correct_count
                FROM
	                IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                LEFT OUTER JOIN
                    VU_MEETING_PTEAM t11
                    ON t11.source_id = t1.source_id
                    AND t11.in_sign_no = t2.in_sign_no
                WHERE
	                t1.source_id = '{#program_id}'
	                AND t1.in_tree_name = 'sub'
					AND ISNULL(t1.in_tree_no, 0) <> 0
                ORDER BY
	                t1.in_tree_no
	                , t2.in_sign_foot
            ";

            sql = sql.Replace("{#program_id}", cfg.itmProgram.getProperty("id", ""));

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item items = cfg.inn.applySQL(sql);
            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_parent = item.getProperty("in_parent", "");
                string event_id = item.getProperty("event_id", "");

                //父場次
                TEvent parent = main.Find(x => x.event_id == in_parent);
                if (parent == null)
                {
                    //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "error sql: " + sql);
                    continue;
                }

                TEvent evt = parent.Children.Find(x => x.event_id == event_id);

                if (evt == null)
                {
                    evt = new TEvent
                    {
                        event_id = item.getProperty("event_id", ""),
                        in_tree_name = item.getProperty("in_tree_name", ""),
                        in_tree_id = item.getProperty("in_tree_id", ""),
                        in_tree_no = item.getProperty("in_tree_no", ""),
                        in_sub_id = item.getProperty("in_sub_id", "0"),
                        in_win_sign_no = item.getProperty("in_win_sign_no", "0"),
                        Value = item,
                        Foots = new Dictionary<string, TFoot>(),
                        Children = new List<TEvent>(),
                    };

                    evt.sub_id = GetIntVal(evt.in_sub_id);

                    parent.Children.Add(evt);
                }

                string detail_id = item.getProperty("detail_id", "");
                string in_sign_foot = item.getProperty("in_sign_foot", "");
                string in_sign_no = item.getProperty("in_sign_no", "0");
                int sign_no = GetIntVal(in_sign_no);

                var foot = new TFoot
                {
                    detail_id = detail_id,
                    in_sign_foot = in_sign_foot,
                    in_sign_no = in_sign_no,
                    sign_no = sign_no,
                    Value = item,
                };

                evt.Foots.Add(in_sign_foot, foot);
            }
        }

        private List<TEvent> GetMainEvents(TConfig cfg)
        {
            List<TEvent> result = new List<TEvent>();

            string sql = @"
                SELECT
	                t1.id AS 'event_id'
	                , t1.in_tree_name
	                , t1.in_tree_id
	                , t1.in_tree_no
	                , t1.in_tree_alias
                    , t1.in_win_sign_no
                    , t2.in_sign_foot
	                , t2.in_sign_no
                    , t2.in_status
                    , t2.in_points
                    , t2.in_correct_count
                    , t12.in_name AS 'site_name'
                    , t12.in_code AS 'site_code'
                FROM
	                IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                LEFT OUTER JOIN
                    IN_MEETING_SITE t12
                    ON t12.id = t1.in_site
                WHERE
	                t1.source_id = '{#program_id}'
	                AND t1.in_tree_name NOT IN (N'sub')
	                AND ISNULL(t1.in_tree_no, 0) <> 0
                ORDER BY
	                t1.in_tree_no
	                , t12.in_code
	                , t2.in_sign_foot
            ";

            sql = sql.Replace("{#program_id}", cfg.itmProgram.getProperty("id", ""));

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item items = cfg.inn.applySQL(sql);
            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string event_id = item.getProperty("event_id", "");

                TEvent evt = result.Find(x => x.event_id == event_id);

                if (evt == null)
                {
                    evt = new TEvent
                    {
                        event_id = item.getProperty("event_id", ""),
                        in_tree_name = item.getProperty("in_tree_name", ""),
                        in_tree_id = item.getProperty("in_tree_id", ""),
                        in_tree_no = item.getProperty("in_tree_no", ""),
                        site_code = item.getProperty("site_code", ""),
                        site_name = item.getProperty("site_name", ""),
                        Value = item,
                        Foots = new Dictionary<string, TFoot>(),
                        Children = new List<TEvent>(),
                    };

                    result.Add(evt);
                }

                string in_sign_foot = item.getProperty("in_sign_foot", "");
                string in_sign_no = item.getProperty("in_sign_no", "0");
                int sign_no = GetIntVal(in_sign_no);

                var foot = new TFoot
                {
                    in_sign_foot = in_sign_foot,
                    in_sign_no = in_sign_no,
                    sign_no = sign_no,
                    Value = item,
                };

                evt.Foots.Add(in_sign_foot, foot);
            }
            return result;
        }


        //取得組別量級選單
        private List<TSect> GetSectList(TConfig cfg, bool is_filter)
        {
            var lstSect = GetMeetingSection(cfg, is_filter);

            var sects = new List<TSect>();

            for (int i = 0; i < lstSect.Count; i++)
            {
                Item itmSect = lstSect[i];
                sects.Add(new TSect
                {
                    sub_id = i + 1,
                    name = itmSect.getProperty("in_name", ""),
                    gender = itmSect.getProperty("in_gender", ""),
                    weight = itmSect.getProperty("in_weight", ""),
                    Value = itmSect,
                });
            }

            //sects.Add(new TSect { No = 7, Name = "代表戰" });

            return sects;
        }

        //取得團體賽量級清單
        private List<Item> GetMeetingSection(TConfig cfg, bool is_filter)
        {
            string program_id = cfg.program_id;

            List<Item> result = new List<Item>();

            string sql = "SELECT * FROM In_Meeting_PSect WITH(NOLOCK) WHERE in_program = '" + program_id + "'"
                + " ORDER BY in_sub_id";

            if (is_filter)
            {
                sql = "SELECT * FROM In_Meeting_PSect WITH(NOLOCK) WHERE in_program = '" + program_id + "'"
                + " AND in_name <> N'代表戰'"
                + " ORDER BY in_sub_id";
            }

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item items = cfg.inn.applySQL(sql);
            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                result.Add(items.getItemByIndex(i));
            }

            return result;
        }

        //賽事資訊
        private void MeetingInfo(TConfig cfg)
        {
            string sql = "SELECT id, in_title FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'";
            cfg.itmMeeting = cfg.inn.applySQL(sql);
            if (cfg.itmMeeting.isError() || cfg.itmMeeting.getResult() == "")
            {
                throw new Exception("賽事資料錯誤");
            }

            cfg.mt_title = cfg.itmMeeting.getProperty("in_title", "");
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public Innosoft.InnovatorHelper InnH { get; set; }

            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }

            public string meeting_id { get; set; }
            public string program_id { get; set; }
            public string scene { get; set; }

            public Item itmMeeting { get; set; }
            public Item itmProgram { get; set; }
            public string mt_title { get; set; }
            public int sect_start { get; set; }
        }

        private class TField
        {
            public string title { get; set; }
            public string css { get; set; }
            public TFType format { get; set; }
            public Func<TConfig, TField, TEvent, string, TRow, string> getValue { get; set; }
            public TSect sect { get; set; }
        }

        private class TSect
        {
            public int sub_id { get; set; }
            public string name { get; set; }
            public string gender { get; set; }
            public string weight { get; set; }

            public Item Value { get; set; }
        }

        private enum TFType
        {
            None = 0,
            Center = 1,
        }

        private class TEvent
        {
            public string event_id { get; set; }
            public string in_tree_name { get; set; }
            public string in_tree_id { get; set; }
            public string in_tree_no { get; set; }
            public string in_win_sign_no { get; set; }
            public string in_sub_id { get; set; }

            public string site_code { get; set; }
            public string site_name { get; set; }

            public Item Value { get; set; }

            public Dictionary<string, TFoot> Foots { get; set; }
            public List<TEvent> Children { get; set; }

            public int sub_id { get; set; }
        }

        private class TFoot
        {
            public string detail_id { get; set; }
            public string in_sign_foot { get; set; }
            public string in_sign_no { get; set; }
            public string in_judo_no { get; set; }
            public int sign_no { get; set; }
            public TRow OrgTeam { get; set; }
            public Item Value { get; set; }
        }

        private class TRow
        {
            public string TeamId { get; set; }
            public string SortNo { get; set; }
            public string OrgName { get; set; }
            public string TeamName { get; set; }
            public string InSignNo { get; set; }
            public string InJudoNo { get; set; }

            public TMTeam Parent { get; set; }
            public List<TMTeam> Children { get; set; }

            public int SignNo { get; set; }
        }

        private class TMTeam
        {
            public string team_id { get; set; }
            public string in_short_org { get; set; }
            public string in_team { get; set; }

            public string in_name { get; set; }
            public string in_sno { get; set; }
            public string in_current_org { get; set; }

            public string in_type { get; set; }
            public string in_parent { get; set; }
            public string in_sub_sect { get; set; }
            public string in_sub_id { get; set; }
            public string in_sub_serial { get; set; }
            public string in_sub_weight { get; set; }
            public string in_muser { get; set; }

            public string in_sign_no { get; set; }
            public string in_judo_no { get; set; }
            public string in_weight_message { get; set; }

            public int sign_no { get; set; }
            public int sub_id { get; set; }
            public int sub_serial { get; set; }

            public Item Value { get; set; }
        }

        private int GetIntVal(string value, int def = 0)
        {
            int result = def;
            Int32.TryParse(value, out result);
            return result;
        }
    }
}