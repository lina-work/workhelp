﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;
using System.Net;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class in_fix_site_no : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
            目的: 
                修獎牌戰場地場次
            日期: 
                - 2021-10-09: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]in_fix_site_no";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, this.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                InnH = _InnH,

                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),

                scene = itmR.getProperty("scene", ""),
                meeting_id = itmR.getProperty("meeting_id", ""),
                program_id = itmR.getProperty("program_id", ""),
                event_id = itmR.getProperty("event_id", ""),
            };

            cfg.meeting_id = "90A97E89B67142F9A163E86E283C21EC";
            cfg.in_date_key = "2021-10-09";

            var itmSites = GetSites(cfg);
            var site_map = MapSites(cfg, itmSites);

            //修復金牌戰
            FixRPC(cfg, site_map);
            //修復銅牌戰
            FixRPC(cfg, site_map);

            return itmR;
        }

        //修復銅牌戰
        private void FixRPC(TConfig cfg, Dictionary<string, Item> site_map)
        {
            string left_site_code = "1";
            string right_site_code = "2";
            if (!site_map.ContainsKey(left_site_code) || !site_map.ContainsKey(right_site_code))
            {
                throw new Exception("未包含該場次: 1, 2");
            }

            Item items = GetItems(cfg, "repechage", "4");

            int count = items.getItemCount();

            var left_evts = new List<TEvent>();
            var right_evts = new List<TEvent>();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string event_id = item.getProperty("event_id", "");
                string in_round_id = item.getProperty("in_round_id", "");

                string in_n1 = item.getProperty("in_n1", "");
                string in_n2 = item.getProperty("in_n2", "");
                string in_n3 = item.getProperty("in_n3", "");

                TEvent evt = new TEvent
                {
                    Value = item,
                    SortOrder = GetOrderNo(in_n1, in_n2, in_n3),
                    event_id = event_id,
                };

                if (in_round_id == "1")
                {
                    left_evts.Add(evt);
                }
                else if (in_round_id == "2")
                {
                    right_evts.Add(evt);
                }
            }

            //銅牌戰: 1XX (West)
            ReSort(cfg, site_map, left_evts, "1", "1");

            //銅牌戰: 1XX (East)
            ReSort(cfg, site_map, right_evts, "2", "1");
        }

        //修復金牌戰
        private void FixGold(TConfig cfg, Dictionary<string, Item> site_map)
        {
            var evts = new List<TEvent>();

            string gold_site_code = "2";
            if (!site_map.ContainsKey(gold_site_code))
            {
                throw new Exception("未包含該場次: " + gold_site_code);
            }

            Item items = GetItems(cfg, "main", "2");

            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string event_id = item.getProperty("event_id", "");
                string in_n1 = item.getProperty("in_n1", "");
                string in_n2 = item.getProperty("in_n2", "");
                string in_n3 = item.getProperty("in_n3", "");

                TEvent evt = new TEvent
                {
                    Value = item,
                    SortOrder = GetOrderNo(in_n1, in_n2, in_n3),
                    event_id = event_id,
                };

                evts.Add(evt);
            }

            //金牌戰: 2XX
            ReSort(cfg, site_map, evts, gold_site_code, "2");
        }

        private void ReSort(TConfig cfg, Dictionary<string, Item> site_map, List<TEvent> evts, string site_code, string prefix)
        {
            var sorted_list = evts.OrderBy(x => x.SortOrder).ToList();

            for (int i = 0; i < sorted_list.Count; i++)
            {
                var evt = sorted_list[i];
                var itmSite = site_map[site_code];
                string site_id = itmSite.getProperty("id", "");

                string sub = (i + 1).ToString().PadLeft(2, '0');
                string in_tree_no = prefix + sub;

                string sql_update = "UPDATE IN_MEETING_PEVENT SET"
                    + " in_site = '" + site_id + "'"
                    + ", in_tree_no = '" + in_tree_no + "'"
                    + " WHERE id = '" + evt.event_id + "'";

                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql_update);

                Item itmSQL = cfg.inn.applySQL(sql_update);
            }
        }

        private class TEvent
        {
            public Item Value { get; set; }
            public int SortOrder { get; set; }
            public string event_id { get; set; }
        }

        private Dictionary<string, Item> MapSites(TConfig cfg, Item items)
        {
            Dictionary<string, Item> result = new Dictionary<string, Item>();

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_code = item.getProperty("in_code", "");
                if (result.ContainsKey(in_code)) continue;
                result.Add(in_code, item);
            }

            return result;
        }
        private Item GetSites(TConfig cfg)
        {
            string sql = @"SELECT id, in_code, in_name FROM IN_MEETING_SITE WITH(NOLOCK) WHERE in_meeting = '{#meeting_id}'";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            return cfg.inn.applySQL(sql);
        }

        private Item GetItems(TConfig cfg, string in_tree_name, string in_round_code)
        {
            string sql = @"
                SELECT
                    t1.in_n1
	                , t1.in_n2
	                , t1.in_n3
	                , t1.in_battle_type
	                , t2.id                 AS 'event_id'
	                , t2.in_tree_name
	                , t2.in_tree_id
	                , t2.in_round
	                , t2.in_round_id
	                , t2.in_round_code
	                , t2.in_tree_no
                FROM
                    IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
                    IN_MEETING_PEVENT t2 WITH(NOLOCK)
                    ON t2.source_id = t1.id
                WHERE
                    t1.in_meeting = '{#meeting_id}'
                    AND t2.in_date_key = '{#in_date_key}'
                    AND t1.in_battle_type IN('JudoTopFour')
	                AND t2.in_tree_name = '{#in_tree_name}'
                    AND t2.in_round_code = {#in_round_code}
            ";

            //in_round_code: 2 = 決賽

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_date_key}", cfg.in_date_key)
                .Replace("{#in_tree_name}", in_tree_name)
                .Replace("{#in_round_code}", in_round_code);

            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public Innosoft.InnovatorHelper InnH { get; set; }

            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }

            public string meeting_id { get; set; }
            public string program_id { get; set; }
            public string event_id { get; set; }
            public string in_date_key { get; set; }
            public string scene { get; set; }

            public Item itmMeeting { get; set; }
            public string in_title { get; set; }
            public string mt_battle_type { get; set; }
            public int mt_robin_player { get; set; }
        }


        private int GetOrderNo(string in_n1, string in_n2, string in_n3)
        {
            int result = 1;
            switch (in_n1)
            {
                case "一":
                    result = result * 10000;
                    break;
                case "公":
                    result = result * 20000;
                    break;
                default:
                    result = result * 30000;
                    break;
            }

            switch (in_n2)
            {
                case "女":
                    result += 1;
                    break;

                case "男":
                    result += 2;
                    break;

                default:
                    break;
            }

            switch (in_n3)
            {
                case "一":
                    result += 100;
                    break;

                case "二":
                    result += 200;
                    break;

                case "三":
                    result += 300;
                    break;

                case "四":
                    result += 400;
                    break;

                case "五":
                    result += 500;
                    break;

                case "六":
                    result += 600;
                    break;

                case "七":
                    result += 700;
                    break;

                case "八":
                    result += 800;
                    break;

                case "九":
                    result += 900;
                    break;

                case "十":
                    result += 1000;
                    break;

                default:
                    break;
            }
            return result;
        }
    }
}