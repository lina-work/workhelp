﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class in_meeting_program_team_fix2 : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
            目的: 修補隊伍量級序號(柔道版)
            日期: 
                2020-11-17: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]in_meeting_program_team_fix2";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, this.dom.InnerXml);

            string meeting_id = itmR.getProperty("meeting_id", "");
            string program_id = itmR.getProperty("program_id", "");

            string sql = "SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE in_meeting = '" + meeting_id + "'";

            if (program_id != "")
            {
                sql += " AND id = '" + program_id + "'";
            }

            Item itmPrograms = inn.applySQL(sql);

            int count = itmPrograms.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item itmProgram = itmPrograms.getItemByIndex(i);

                FixTeamSectionNo(CCO, strMethodName, inn, itmProgram);
            }

            return itmR;
        }

        private void FixTeamSectionNo(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmProgram)
        {
            string meeting_id = itmProgram.getProperty("in_meeting", "");
            string program_id = itmProgram.getProperty("id", "");
            string in_battle_type = itmProgram.getProperty("in_battle_type", "");
            string in_team_count = itmProgram.getProperty("in_team_count", "");
            string in_round_code = itmProgram.getProperty("in_round_code", "");


            Item itmTeams = inn.applySQL("SELECT * FROM IN_MEETING_PTEAM WITH(NOLOCK) WHERE source_id = '" + program_id + "'");
            int count = itmTeams.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item itmTeam = itmTeams.getItemByIndex(i);
                string team_id = itmTeam.getProperty("id", "");
                string in_sign_no = itmTeam.getProperty("in_sign_no", "");
                string old_section_no = itmTeam.getProperty("in_section_no", "");


                Item itmData = inn.newItem();
                itmData.setType("In_Meeting_PTeam");
                itmData.setProperty("in_battle_type", in_battle_type);
                itmData.setProperty("in_team_count", in_team_count);
                itmData.setProperty("in_round_code", in_round_code);
                itmData.setProperty("is_tkd_to_judo", "1");
                itmData.setProperty("value", in_sign_no);
                Item itmResult = itmData.apply("In_Judo_SignNo");

                string new_section_no = itmResult.getProperty("map_no", "");

                
                if (new_section_no != old_section_no)
                {
                    string sql = "UPDATE IN_MEETING_PTEAM SET in_section_no = '" + new_section_no + "' WHERE id = '" + team_id + "'";
                    Item itmSQL = inn.applySQL(sql);
                    if (itmSQL.isError()) throw new Exception("error");
                }
            }
        }
    }
}