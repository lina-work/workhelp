﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class in_meeting_pteam : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 
                    參賽隊伍
                日期: 
                    2021-05-13: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]in_meeting_pteam";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                InnH = _InnH,

                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                program_id = itmR.getProperty("program_id", ""),
                team_id = itmR.getProperty("team_id", ""),
                scene = itmR.getProperty("scene", ""),
            };

            if (cfg.scene == "")
            {
                //頁面
                Page(cfg, itmR);
            }
            else if (cfg.scene == "remove")
            {
                //刪除與會者與隊伍
                RemovePTeam(cfg, cfg.program_id, cfg.team_id, itmR);
            }
            else if (cfg.scene == "modal")
            {
                //【新增】跳窗
                Modal(cfg, itmR);
            }
            else if (cfg.scene == "modal2")
            {
                //【換組】跳窗
                Modal2(cfg, itmR);
                itmR.setProperty("modal_title", "換組");
                itmR.setProperty("hide_change_button", "");
                itmR.setProperty("hide_append_button", "item_show_0");
            }
            else if (cfg.scene == "modal3")
            {
                //【加報】跳窗
                Modal2(cfg, itmR);
                itmR.setProperty("modal_title", "加報");
                itmR.setProperty("hide_change_button", "item_show_0");
                itmR.setProperty("hide_append_button", "");
            }
            else if (cfg.scene == "add")
            {
                //新增
                AddPTeam(cfg, cfg.program_id, itmR);
            }
            else if (cfg.scene == "change")
            {
                //換組
                ChangePTeam(cfg, itmR, need_remove: true);
            }
            else if (cfg.scene == "append")
            {
                //加報
                ChangePTeam(cfg, itmR, need_remove: false);
            }
            return itmR;
        }

        #region 異動參賽者

        //換組
        private void ChangePTeam(TConfig cfg, Item itmReturn, bool need_remove = true)
        {
            Item itmPTeam = cfg.inn.applySQL("SELECT * FROM IN_MEETING_PTEAM WITH(NOLOCK) WHERE id = '" + cfg.team_id + "'");
            string old_program_id = itmPTeam.getProperty("source_id", "");
            string old_in_index = itmPTeam.getProperty("in_index", "");
            string old_in_creator_sno = itmPTeam.getProperty("in_creator_sno", "");

            Item itmOldProgram = cfg.inn.applySQL("SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + old_program_id + "'");
            string old_in_l1 = itmOldProgram.getProperty("in_l1", "");
            string old_in_l2 = itmOldProgram.getProperty("in_l2", "");
            string old_in_l3 = itmOldProgram.getProperty("in_l3", "");

            string sql = "SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK)"
                + " WHERE in_meeting = '" + cfg.meeting_id + "'"
                + " AND in_l1 = N'" + itmReturn.getProperty("new_in_l1", "") + "'"
                + " AND in_l2 = N'" + itmReturn.getProperty("new_in_l2", "") + "'"
                + " AND in_l3 = N'" + itmReturn.getProperty("new_in_l3", "") + "'";
            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item itmNewProgram = cfg.inn.applySQL(sql);
            string new_program_id = itmNewProgram.getProperty("id", "");
            string new_in_l1 = itmNewProgram.getProperty("in_l1", "");
            string new_in_l2 = itmNewProgram.getProperty("in_l2", "");
            string new_in_l3 = itmNewProgram.getProperty("in_l3", "");

            Item itmOldMUser = GetMUser(cfg, old_in_l1, old_in_l2, old_in_l3, old_in_index, old_in_creator_sno);
            string in_birth = GetDateTimeValue(itmOldMUser.getProperty("in_birth", ""), "yyyy-MM-ddTHH:mm:ss", 8);
            string in_paynumber = itmOldMUser.getProperty("in_paynumber", "");
            string in_paytime = GetDateTimeValue(itmOldMUser.getProperty("in_paytime", ""), "yyyy-MM-ddTHH:mm:ss", 8);

            TTeam team = new TTeam
            {
                program_id = new_program_id,
                in_l1 = new_in_l1,
                in_l2 = new_in_l2,
                in_l3 = new_in_l3,

                in_name = itmOldMUser.getProperty("in_name", ""),
                in_sno = itmOldMUser.getProperty("in_sno", ""),
                in_gender = itmOldMUser.getProperty("in_gender", ""),
                in_birth = in_birth,
                in_group = itmOldMUser.getProperty("in_group", ""),
                in_current_org = itmOldMUser.getProperty("in_current_org", ""),
                in_short_org = itmOldMUser.getProperty("in_short_org", ""),
                in_stuff_b1 = itmOldMUser.getProperty("in_stuff_b1", ""),
                in_stuff_c1 = itmOldMUser.getProperty("in_stuff_c1", ""),
                in_team = itmOldMUser.getProperty("in_team", ""),

                in_creator = itmOldMUser.getProperty("in_creator", ""),
                in_creator_sno= itmOldMUser.getProperty("in_creator_sno", ""),

                in_paynumber = in_paynumber,
                in_paytime = in_paytime,
                in_org = "0",
            };

            //修復團體會員編號與單位簡稱
            FixShortOrg(cfg, team);

            //新增參賽者
            AddPTeam(cfg, team);

            if (need_remove)
            {
                //移除參賽者
                RemovePTeam(cfg, old_program_id, cfg.team_id, itmReturn);
            }
        }

        //新增參賽者(含與會者)
        private void AddPTeam(TConfig cfg, string program_id, Item itmReturn)
        {
            Item itmProgram = cfg.inn.applySQL("SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + program_id + "'");

            TTeam team = new TTeam
            {
                program_id = program_id,
                in_l1 = itmProgram.getProperty("in_l1", ""),
                in_l2 = itmProgram.getProperty("in_l2", ""),
                in_l3 = itmProgram.getProperty("in_l3", ""),

                in_name = itmReturn.getProperty("in_name", ""),
                in_sno = itmReturn.getProperty("in_sno", ""),
                in_gender = itmReturn.getProperty("in_gender", ""),
                in_birth = itmReturn.getProperty("in_birth", ""),
                in_current_org = itmReturn.getProperty("in_current_org", ""),
                in_stuff_b1 = itmReturn.getProperty("in_stuff_b1", ""),
                in_short_org = itmReturn.getProperty("in_short_org", ""),
                in_team = itmReturn.getProperty("in_team", ""),
            };


            Item itmParent = cfg.inn.applySQL("SELECT * FROM IN_RESUME WITH(NOLOCK) WHERE in_user_id = '" + cfg.strUserId + "'");
            team.isUserId = itmParent.getProperty("in_user_id", "");
            team.isIndId = itmParent.getProperty("owned_by_id", "");
            team.in_group = itmParent.getProperty("in_group", "");
            team.in_creator = itmParent.getProperty("in_name", "");
            team.in_creator_sno = itmParent.getProperty("in_sno", "");
            team.in_org = "0";

            team.in_stuff_c1 = "";

            //修復團體會員編號與單位簡稱
            FixShortOrg(cfg, team);

            AddPTeam(cfg, team);
        }

        private void FixShortOrg(TConfig cfg, TTeam team)
        {
            bool no_stuff_b1 = team.in_stuff_b1 == "";
            bool no_short_org = team.in_short_org == "";

            if (no_stuff_b1 || no_short_org)
            {
                string in_stuff_b1 = team.in_stuff_b1;
                if (no_stuff_b1 && team.in_current_org.Length > 3)
                {
                    in_stuff_b1 = team.in_current_org.Substring(0, 3);
                }

                Item itmOrg = cfg.inn.applySQL("SELECT TOP 1 * FROM IN_ORG_MAP WITH(NOLOCK) WHERE in_stuff_b1 = '" + in_stuff_b1 + "'");
                if (!itmOrg.isError() && itmOrg.getResult() != "")
                {
                    team.map_stuff_b1 = itmOrg.getProperty("in_stuff_b1", "");
                    team.map_short_org = itmOrg.getProperty("in_short_org", "");
                }
            }
            else
            {
                team.map_stuff_b1 = team.in_stuff_b1;
                team.map_short_org = team.in_short_org;
            }
        }

        //新增參賽者(含與會者)
        private void AddPTeam(TConfig cfg, TTeam team)
        {
            if (team.in_paytime.Contains("1900-01-01"))
            {
                team.in_paytime = "NULL";
            }
            else
            {
                team.in_paytime = "'" + team.in_paytime + "'";
            }

            TMSuvery ms_entity = MeetingSurveyEntity(cfg);
            team.in_mail = GetMail(cfg, ms_entity.pk_list, team);

            string sql = "SELECT * FROM IN_MEETING_USER WITH(NOLOCK)"
                + " WHERE source_id  = '" + cfg.meeting_id + "'"
                + " AND in_mail  = N'" + team.in_mail + "'";

            Item itmData = cfg.inn.applySQL(sql);

            if (!itmData.isError() && itmData.getResult() != "")
            {
                throw new Exception(ms_entity.pk_msg);
            }

            Item itmEntity = MapEntity(cfg, team);

            //新建與會者
            Item itmMUser = NewRegister(cfg, team, itmEntity, ms_entity.List);
            itmEntity.setProperty("muid", itmMUser.getProperty("id", ""));
            itmEntity.setProperty("in_index", itmMUser.getProperty("in_index", ""));

            string sql_muser = "";
            string muser_stuff_b1 = itmMUser.getProperty("in_stuff_b1", "");
            string muser_short_org = itmMUser.getProperty("in_short_org", "");

            if (muser_stuff_b1 == "" || muser_short_org == "")
            {
                sql_muser = "UPDATE IN_MEETING_USER SET"
                    + " in_weight_result = NULL"
                    + ", in_sign_no = ''"
                    + ", in_section_no = ''"
                    + ", in_spot_checked = NULL"
                    + ", in_creator = N'" + team.in_creator + "'"
                    + ", in_creator_sno = N'" + team.in_creator_sno + "'"
                    + ", in_paynumber = '" + team.in_paynumber + "'"
                    + ", in_paytime = " + team.in_paytime + ""
                    + ", in_stuff_c1 = N'" + team.in_stuff_c1 + "'"
                    + ", in_stuff_b1 = N'" + team.map_stuff_b1 + "'"
                    + ", in_short_org = N'" + team.map_short_org + "'"
                    + " WHERE id = '" + itmEntity.getProperty("muid", "") + "'";
            }
            else
            {
                sql_muser = "UPDATE IN_MEETING_USER SET"
                    + " in_weight_result = NULL"
                    + ", in_sign_no = ''"
                    + ", in_section_no = ''"
                    + ", in_spot_checked = NULL"
                    + ", in_creator = N'" + team.in_creator + "'"
                    + ", in_creator_sno = N'" + team.in_creator_sno + "'"
                    + ", in_paynumber = '" + team.in_paynumber + "'"
                    + ", in_paytime = " + team.in_paytime
                    + ", in_stuff_c1 = N'" + team.in_stuff_c1 + "'"
                    + " WHERE id = '" + itmEntity.getProperty("muid", "") + "'";
            }

            cfg.inn.applySQL(sql_muser);

            //新建組別隊伍
            NewTeam(cfg, itmEntity);

            //重設組別隊伍欄位 (含重建場次)
            ResetProgram(cfg, team.in_l1, team.in_l2, team.in_l3);

            //統計單位隊伍數量
            UpdateOrgTeams(cfg, team.program_id);

            //更新與會者 No
            UpdateMUserShowNo(cfg, team.in_l1, team.in_l2, team.in_l3);

            //更新隊伍 No
            UpdatePTeamShowNo(cfg, team.program_id);
        }

        private Item MapEntity(TConfig cfg, TTeam team)
        {
            Item item = cfg.inn.newItem();
            item.setProperty("program_id", team.program_id);
            item.setProperty("in_l1", team.in_l1);
            item.setProperty("in_l2", team.in_l2);
            item.setProperty("in_l3", team.in_l3);

            item.setProperty("in_name", team.in_name);
            item.setProperty("in_sno", team.in_sno);
            item.setProperty("in_gender", team.in_gender);
            item.setProperty("in_birth", team.in_birth);
            item.setProperty("in_current_org", team.in_current_org);
            item.setProperty("in_stuff_b1", team.map_stuff_b1);
            item.setProperty("in_short_org", team.map_short_org);
            item.setProperty("in_team", team.in_team);

            item.setProperty("in_group", team.in_group);
            item.setProperty("in_creator", team.in_creator);
            item.setProperty("in_creator_sno", team.in_creator_sno);

            item.setProperty("in_mail", team.in_mail);
            item.setProperty("in_org", team.in_org);
            item.setProperty("in_index", team.in_index);

            return item;
        }

        /// <summary>
        /// 新建組別隊伍
        /// </summary>
        private void NewTeam(TConfig cfg, Item itmEntity)
        {
            string in_team_index = GetTeamIndex(cfg);
            string in_team_key = string.Join("-", new string[]
            {
                itmEntity.getProperty("in_l1", ""),
                itmEntity.getProperty("in_l2", ""),
                itmEntity.getProperty("in_l3", ""),
                itmEntity.getProperty("in_index", ""),
                itmEntity.getProperty("in_creator_sno", ""),
            });

            string in_stuff_b1 = itmEntity.getProperty("in_stuff_b1", "");
            string in_short_org = itmEntity.getProperty("in_short_org", "");

            Item itmPTeam = cfg.inn.newItem("In_Meeting_PTeam");
            itmPTeam.setProperty("in_meeting", cfg.meeting_id);
            itmPTeam.setProperty("source_id", itmEntity.getProperty("program_id", ""));
            itmPTeam.setProperty("in_team_index", in_team_index);
            itmPTeam.setProperty("in_team_key", in_team_key);

            itmPTeam.setProperty("in_index", itmEntity.getProperty("in_index", ""));
            itmPTeam.setProperty("in_group", itmEntity.getProperty("in_group", ""));
            itmPTeam.setProperty("in_current_org", itmEntity.getProperty("in_current_org", ""));
            itmPTeam.setProperty("in_stuff_b1", itmEntity.getProperty("in_stuff_b1", ""));
            itmPTeam.setProperty("in_short_org", itmEntity.getProperty("in_short_org", ""));
            itmPTeam.setProperty("in_team", itmEntity.getProperty("in_team", ""));
            itmPTeam.setProperty("in_team_players", "1");
            itmPTeam.setProperty("in_creator_sno", itmEntity.getProperty("in_creator_sno", ""));
            itmPTeam.setProperty("in_team", itmEntity.getProperty("in_team", ""));
            itmPTeam.setProperty("in_type", "p");

            if (in_stuff_b1 != "")
            {
                itmPTeam.setProperty("in_stuff_b1", in_stuff_b1);
            }

            if (in_short_org == "")
            {
                itmPTeam.setProperty("in_short_org", itmEntity.getProperty("in_current_org", ""));
            }
            else
            {
                itmPTeam.setProperty("in_short_org", itmEntity.getProperty("in_short_org", ""));
            }


            itmPTeam.setProperty("in_name", itmEntity.getProperty("in_name", ""));
            itmPTeam.setProperty("in_sno", itmEntity.getProperty("in_sno", ""));
            itmPTeam.setProperty("in_names", itmEntity.getProperty("in_name", ""));


            itmPTeam.setProperty("in_is_sync", "0");
            itmPTeam.setProperty("in_sign_no", "");
            itmPTeam.setProperty("in_section_no", "");
            itmPTeam.setProperty("in_check_result", "");
            itmPTeam.setProperty("in_check_status", "");
            itmPTeam.setProperty("in_weight_result", "");
            itmPTeam.setProperty("in_weight_value", "");

            itmPTeam = itmPTeam.apply("add");
        }

        private string GetTeamIndex(TConfig cfg)
        {
            string sql = "SELECT MAX(in_team_index) AS 'max_team_index' FROM IN_MEETING_PTEAM WITH(NOLOCK)"
                + " WHERE in_meeting = '" + cfg.meeting_id + "'";

            Item itmNo = cfg.inn.applySQL(sql);

            string first_team_index = "00001";

            if (itmNo.isError()) throw new Exception("查找參賽隊伍序號發生錯誤");
            if (itmNo.getResult() == "") return first_team_index;

            string max_team_index = itmNo.getProperty("max_team_index", "0").Trim('0');
            if (max_team_index == "") return first_team_index;

            int new_team_index = GetIntVal(max_team_index) + 1;
            return new_team_index.ToString().PadLeft(5, '0');
        }

        /// <summary>
        /// 新建與會者 (註冊模式)
        /// </summary>
        private Item NewRegister(TConfig cfg, TTeam team, Item itmEntity, List<Item> lstSurveys)
        {
            string survey_type = "1";
            string method = "register_meeting";

            string parameters = GetParamValues(lstSurveys, itmEntity);

            string aml = ""
                + "<method>" + method + "</method>"
                + "<meeting_id>" + cfg.meeting_id + "</meeting_id>"
                + "<surveytype>" + survey_type + "</surveytype>"
                + "<agent_id>" + "</agent_id>"
                + "<isUserId>" + team.isUserId + "</isUserId>"
                + "<isIndId>" + team.isIndId + "</isIndId>"
                + "<email>" + team.in_mail + "</email>"
                + "<in_org>" + team.in_org + "</in_org>"
                + "<parameter>" + parameters + "</parameter>"
                + "";

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "aml: " + aml);

            Item itmMUser = cfg.inn.applyMethod("In_meeting_register", aml);

            if (itmMUser.isError())
            {
                throw new Exception("創建成員帳號發生錯誤");
            }

            return itmMUser;
        }

        /// <summary>
        /// 取得問項清單
        /// </summary>
        private TMSuvery MeetingSurveyEntity(TConfig cfg)
        {
            string sql = @"
                SELECT
                    t3.id
                    , t3.in_property
                    , t3.in_is_pkey
                FROM
                    IN_MEETING t1 WITH(NOLOCK)
                INNER JOIN
                    IN_MEETING_SURVEYS t2 WITH(NOLOCK) ON t2.source_id = t1.id
                INNER JOIN
                    IN_SURVEY t3 WITH(NOLOCK) ON t3.id = t2.related_id
                WHERE
                    t1.id = '{#meeting_id}'
                    AND ISNULL(in_property, '') <> ''
                ORDER BY
                    t2.sort_order
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            Item items = cfg.inn.applySQL(sql);

            if (items.isError() || items.getItemCount() <= 0)
            {
                throw new Exception("取得問項清單發生錯誤 _# sql: " + sql);
            }

            TMSuvery result = new TMSuvery
            {
                List = new List<Item>(),
                pk_list = new List<string>(),
                pk_arr = "",
                pk_msg = "",
            };

            result.pk_list.Add("in_sno");

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_property = item.getProperty("in_property", "");
                string in_is_pkey = item.getProperty("in_is_pkey", "");

                result.List.Add(item);
                if (in_is_pkey == "1")
                {
                    result.pk_arr += in_property + ",";
                    if (!result.pk_list.Contains(in_property))
                    {
                        result.pk_list.Add(in_property);
                    }
                }
            }

            if (result.pk_arr.Contains("in_l3"))
            {
                result.pk_msg = "該組別量級已報過名";
            }
            else if (result.pk_arr.Contains("in_l2"))
            {
                result.pk_msg = "該組別已報過名";
            }
            else if (result.pk_arr.Contains("in_l1"))
            {
                result.pk_msg = "該項目已報過名";
            }

            return result;
        }

        private class TMSuvery
        {
            public List<Item> List { get; set; }
            public List<string> pk_list { get; set; }
            public string pk_arr { get; set; }
            public string pk_msg { get; set; }
        }

        /// <summary>
        /// 組成 Url 參數
        /// </summary>
        /// <param name="list">問項清單</param>
        /// <param name="itmEntity">與會者資料</param>
        private string GetParamValues(List<Item> list, Item itmEntity)
        {
            string result = "";
            int count = list.Count;
            for (int i = 0; i < count; i++)
            {
                Item item = list[i];
                string id = item.getProperty("id", "");
                string property = item.getProperty("in_property", "");

                string value = itmEntity.getProperty(property, "");

                if (value == "")
                {
                    continue;
                }

                if (property == "in_birth")
                {
                    value = GetDateTimeValue(value, "yyyy-MM-dd");
                }

                if (result != "")
                {
                    result += "&amp;";
                }

                result += id + "=" + value;
            }
            return result;
        }

        #endregion 異動參賽者

        //隊伍跳窗(新增)
        private void Modal(TConfig cfg, Item itmReturn)
        {
            Item itmProgram = cfg.inn.applySQL("SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + cfg.program_id + "'");
            itmReturn.setProperty("in_l1", itmProgram.getProperty("in_l1", ""));
            itmReturn.setProperty("in_l2", itmProgram.getProperty("in_l2", ""));
            itmReturn.setProperty("in_l3", itmProgram.getProperty("in_l3", ""));
        }

        //隊伍跳窗(換組)
        private void Modal2(TConfig cfg, Item itmReturn)
        {
            Item itmProgram = cfg.inn.applySQL("SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + cfg.program_id + "'");
            itmReturn.setProperty("in_l1", itmProgram.getProperty("in_l1", ""));
            itmReturn.setProperty("in_l2", itmProgram.getProperty("in_l2", ""));
            itmReturn.setProperty("in_l3", itmProgram.getProperty("in_l3", ""));

            Item itmPTeam = cfg.inn.applySQL("SELECT * FROM VU_MEETING_PTEAM WITH(NOLOCK) WHERE id = '" + cfg.team_id + "'");
            itmReturn.setProperty("in_name", itmPTeam.getProperty("in_name", ""));
            itmReturn.setProperty("map_short_org", itmPTeam.getProperty("map_short_org", ""));

            //三階選單
            Item itmJson = cfg.inn.newItem("In_Meeting");
            itmJson.setProperty("meeting_id", cfg.meeting_id);
            itmJson = itmJson.apply("in_meeting_program_options");
            itmReturn.setProperty("in_level_json", itmJson.getProperty("json", ""));
        }

        //刪除與會者與隊伍
        private void RemovePTeam(TConfig cfg, string program_id, string team_id, Item itmReturn)
        {
            Item itmProgram = cfg.inn.applySQL("SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + program_id + "'");
            string in_l1 = itmProgram.getProperty("in_l1", "");
            string in_l2 = itmProgram.getProperty("in_l2", "");
            string in_l3 = itmProgram.getProperty("in_l3", "");

            Item itmPTeam = cfg.inn.applySQL("SELECT * FROM IN_MEETING_PTEAM WITH(NOLOCK) WHERE id = '" + team_id + "'");
            string in_index = itmPTeam.getProperty("in_index", "");
            string in_creator_sno = itmPTeam.getProperty("in_creator_sno", "");

            Item itmMUser = GetMUser(cfg, in_l1, in_l2, in_l3, in_index, in_creator_sno);

            bool has_muser = true;
            if (itmMUser.isError())
            {
                throw new Exception("系統異常");
            }
            else if (itmMUser.getResult() == "")
            {
                has_muser = false;
            }

            string aml = "";
            Item itmSQL = null;

            if (has_muser)
            {
                string muid = itmMUser.getProperty("id", "");

                //刪除問卷結果
                aml = @"<AML>
                            <Item type='In_Meeting_Surveys_result' action='delete' where=""in_participant='{#muid}' and in_surveytype='1' ""/>
                        </AML>"
                    .Replace("{#muid}", muid);
                itmSQL = cfg.inn.applyAML(aml);

                //刪除學員履歷
                aml = "<AML>" +
                            "<Item type='in_meeting_resume' action='delete' where=\"in_user='" + muid + "'\">" +
                      "</Item></AML>";
                itmSQL = cfg.inn.applyAML(aml);

                //刪除簽到記錄
                aml = "<AML>" +
                            "<Item type='in_meeting_record' action='delete' where=\"in_participant='" + muid + "'\">" +
                       "</Item></AML>";
                itmSQL = cfg.inn.applyAML(aml);

                //刪除與會者
                aml = "<AML>" +
                          "<Item type='in_meeting_user' action='delete' id='" + muid + "'>" +
                      "</Item></AML>";
                itmSQL = cfg.inn.applyAML(aml);
            }

            //刪除隊伍
            aml = "<AML>" +
                      "<Item type='in_meeting_pteam' action='delete' id='" + cfg.team_id + "'>" +
                  "</Item></AML>";
            itmSQL = cfg.inn.applyAML(aml);

            //重設組別隊伍欄位 (含重建場次)
            ResetProgram(cfg, in_l1, in_l2, in_l3);

            //統計單位隊伍數量
            UpdateOrgTeams(cfg, program_id);

            //更新與會者 No
            UpdateMUserShowNo(cfg, in_l1, in_l2, in_l3);

            //更新單位隊伍 No
            UpdatePTeamShowNo(cfg, program_id);
        }

        //重設組別隊伍欄位
        private void ResetProgram(TConfig cfg, string in_l1, string in_l2, string in_l3)
        {
            Item itmData = cfg.inn.newItem();
            itmData.setType("In_Meeting_Program");
            itmData.setProperty("scene", "");
            itmData.setProperty("meeting_id", cfg.meeting_id);
            itmData.setProperty("l1_value", in_l1);
            itmData.setProperty("l2_value", in_l2);
            itmData.setProperty("l3_value", in_l3);
            itmData.setProperty("no_straw", "1");
            itmData.apply("in_meeting_straw");
        }

        //更新與會者 No
        private void UpdateMUserShowNo(TConfig cfg, string in_l1, string in_l2, string in_l3)
        {
            string sql = @"
                UPDATE t1 SET
	                t1.in_show_no = t2.rno
	                , t1.in_section_no = t2.rno
                FROM 
	                IN_MEETING_USER t1 WITH(NOLOCK)
                INNER JOIN
                (
	                SELECT
		                id
						, in_name
						, in_sno
		                , ROW_NUMBER() OVER (PARTITION BY in_l1, in_l2, in_l3 ORDER BY in_stuff_b1, in_team, in_sno) AS 'rno'
	                FROM
		                IN_MEETING_USER WITH(NOLOCK)
	                WHERE
		                source_id = '{#meeting_id}'
    	                AND in_l1 = N'{#in_l1}'
    	                AND in_l2 = N'{#in_l2}'
    	                AND in_l3 = N'{#in_l3}'
                ) t2
                ON t2.id = t1.id
                WHERE
	                t1.source_id = '{#meeting_id}'
	                AND t1.in_l1 = N'{#in_l1}'
	                AND t1.in_l2 = N'{#in_l2}'
	                AND t1.in_l3 = N'{#in_l3}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_l1}", in_l1)
                .Replace("{#in_l2}", in_l2)
                .Replace("{#in_l3}", in_l3);

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item itmSQL = cfg.inn.applySQL(sql);
        }

        //更新單位隊伍 No
        private void UpdatePTeamShowNo(TConfig cfg, string program_id)
        {
            string sql = @"
                UPDATE t1 SET
	                t1.in_show_no = t2.rno
                FROM 
	                IN_MEETING_PTEAM t1 WITH(NOLOCK)
                INNER JOIN
                (
	                SELECT
		                id
		                , ROW_NUMBER() OVER (PARTITION BY source_id ORDER BY in_stuff_b1, in_team, in_sno) AS 'rno'
	                FROM
		                IN_MEETING_PTEAM WITH(NOLOCK)
	                WHERE
		                source_id = '{#program_id}'
                ) t2
                ON t2.id = t1.id
                WHERE
	                t1.source_id = '{#program_id}'
            ";

            sql = sql.Replace("{#program_id}", program_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item itmSQL = cfg.inn.applySQL(sql);
        }

        //更新單位隊伍數
        private void UpdateOrgTeams(TConfig cfg, string program_id)
        {
            string sql = @"
                UPDATE
                    t1
                SET
                    t1.in_org_teams = t2.in_org_teams
                FROM
                    IN_MEETING_PTEAM t1 WITH(NOLOCK)
                LEFT OUTER JOIN
                (
                    SELECT 
                        source_id
                        , in_current_org
                        , count(id) AS 'in_org_teams'
                    FROM
                        IN_MEETING_PTEAM WITH(NOLOCK)
                    GROUP BY
                        source_id
                        , in_current_org
                ) t2 
                    ON t2.source_id = t1.source_id
                    AND t2.in_current_org = t1.in_current_org
                WHERE
                    t1.source_id = '{#program_id}'
            ";

            sql = sql.Replace("{#program_id}", program_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item itmSQL = cfg.inn.applySQL(sql);
        }

        private Item GetMUser(TConfig cfg, string in_l1, string in_l2, string in_l3, string in_index, string in_creator_sno)
        {
            string sql = @"
                SELECT 
	                *
                FROM
	                IN_MEETING_USER WITH(NOLOCK)
                WHERE
	                source_id = '{#meeting_id}'
	                AND ISNULL(in_l1, '') = N'{#in_l1}'
	                AND ISNULL(in_l2, '') = N'{#in_l2}'
	                AND ISNULL(in_l3, '') = N'{#in_l3}'
	                AND ISNULL(in_index, '') = N'{#in_index}'
	                AND ISNULL(in_creator_sno, '') = N'{#in_creator_sno}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_l1}", in_l1)
                .Replace("{#in_l2}", in_l2)
                .Replace("{#in_l3}", in_l3)
                .Replace("{#in_index}", in_index)
                .Replace("{#in_creator_sno}", in_creator_sno);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        //頁面
        private void Page(TConfig cfg, Item itmReturn)
        {
            Item itmMeeting = cfg.inn.applySQL("SELECT in_title FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            itmReturn.setProperty("in_title", itmMeeting.getProperty("in_title", " "));

            string sql = "SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + cfg.program_id + "'";
            if (cfg.program_id == "")
            {
                sql = "SELECT TOP 1 * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE in_meeting = '" + cfg.meeting_id + "' ORDER BY in_sort_order";
            }

            Item itmProgram = cfg.inn.applySQL(sql);
            cfg.program_id = itmProgram.getProperty("id", "");
            itmReturn.setProperty("program_id", cfg.program_id);
            itmReturn.setProperty("in_l1", itmProgram.getProperty("in_l1", ""));
            itmReturn.setProperty("in_l2", itmProgram.getProperty("in_l2", ""));
            itmReturn.setProperty("in_l3", itmProgram.getProperty("in_l3", ""));
            itmReturn.setProperty("in_name", itmProgram.getProperty("in_name", ""));
            itmReturn.setProperty("in_name2", itmProgram.getProperty("in_name2", ""));
            itmReturn.setProperty("in_name3", itmProgram.getProperty("in_name3", " "));
            itmReturn.setProperty("in_short_name", itmProgram.getProperty("in_short_name", " "));
            itmReturn.setProperty("in_sort_order", itmProgram.getProperty("in_sort_order", " "));
            itmReturn.setProperty("in_team_count", itmProgram.getProperty("in_team_count", " "));
            itmReturn.setProperty("in_round_code", itmProgram.getProperty("in_round_code", " "));
            itmReturn.setProperty("in_round_count", itmProgram.getProperty("in_round_count", " "));

            Item itmTeams = cfg.inn.applySQL("SELECT * FROM VU_MEETING_PTEAM WITH(NOLOCK) WHERE source_id = '" + cfg.program_id + "' ORDER BY in_show_no");
            int count = itmTeams.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item itmTeam = itmTeams.getItemByIndex(i);
                itmTeam.setType("inn_team");
                itmTeam.setProperty("program_name3", itmProgram.getProperty("in_name3", " "));
                itmTeam.setProperty("program_short_name", itmProgram.getProperty("in_short_name", " "));
                itmReturn.addRelationship(itmTeam);
            }

            //三階選單
            Item itmJson = cfg.inn.newItem("In_Meeting");
            itmJson.setProperty("meeting_id", cfg.meeting_id);
            itmJson.setProperty("need_id", "1");
            itmJson = itmJson.apply("in_meeting_program_options");
            itmReturn.setProperty("in_level_json", itmJson.getProperty("json", ""));
            itmReturn.setProperty("in_level_count", itmJson.getProperty("total", ""));
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public Innosoft.InnovatorHelper InnH { get; set; }

            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }

            public string meeting_id { get; set; }
            public string program_id { get; set; }
            public string team_id { get; set; }
            public string scene { get; set; }

            public Item itmMeeting { get; set; }
            public string in_title { get; set; }
            public string mt_battle_type { get; set; }
            public int mt_robin_player { get; set; }
        }

        private class TTeam
        {
            public string program_id { get; set; }
            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }

            public string in_name { get; set; }
            public string in_sno { get; set; }
            public string in_gender { get; set; }
            public string in_birth { get; set; }
            public string in_current_org { get; set; }
            public string in_stuff_b1 { get; set; }
            public string in_stuff_c1 { get; set; }
            public string in_short_org { get; set; }
            public string in_team { get; set; }

            public string in_group { get; set; }
            public string in_creator { get; set; }
            public string in_creator_sno { get; set; }

            public string in_mail { get; set; }
            public string in_org { get; set; }
            public string in_index { get; set; }

            public string isUserId { get; set; }
            public string isIndId { get; set; }

            public string in_paynumber { get; set; }
            public string in_paytime { get; set; }

            public string map_stuff_b1 { get; set; }
            public string map_short_org { get; set; }
        }

        private string GetMail(TConfig cfg, List<string> properties, TTeam team)
        {
            Item item = cfg.inn.newItem();
            item.setProperty("in_sno", team.in_sno);
            item.setProperty("in_l1", team.in_l1);
            item.setProperty("in_l2", team.in_l2);
            item.setProperty("in_l3", team.in_l3);

            List<string> values = new List<string>();
            properties.ForEach(x =>
            {
                values.Add(item.getProperty(x, ""));
            });
            return string.Join("-", values);
        }

        private string GetDateTimeValue(string value, string format, int hours = 0)
        {
            if (value == "") return "";
            DateTime result = DateTime.MinValue;
            DateTime.TryParse(value, out result);
            return result.AddHours(hours).ToString(format);
        }

        private int GetIntVal(string value)
        {
            if (value == "" || value == "0") return 0;

            int result = 0;
            int.TryParse(value, out result);
            return result;
        }
    }
}