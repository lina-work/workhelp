﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;
using System.Net;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class in_team_battle_psect : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 團體戰-級別
                日期: 
                    - 2021-11-01 創建 (Lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]in_team_battle_psect";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, this.dom.InnerXml);

            string sql = "SELECT * FROM AAA_MT_PSECT WITH(NOLOCK)";
            Item items = inn.applySQL(sql);
            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_meeting = item.getProperty("in_meeting", "");
                string in_program = item.getProperty("in_program", "");
                string in_sub_id = item.getProperty("in_sub_id", "");

                Item itmPSect = inn.newItem("IN_MEETING_PSECT");
                
                itmPSect.setAttribute("where", "in_program = '" + in_program + "'"
                    + " AND in_sub_id = '" + in_sub_id + "'");
                
                itmPSect.setProperty("in_meeting", in_meeting);
                itmPSect.setProperty("in_program", in_program);
                itmPSect.setProperty("in_sub_id", in_sub_id);

                itmPSect.setProperty("in_name", item.getProperty("in_name", ""));
                itmPSect.setProperty("in_gender", item.getProperty("in_gender", ""));
                itmPSect.setProperty("in_weight", item.getProperty("in_weight", ""));
                itmPSect.setProperty("in_ranges", item.getProperty("in_ranges", ""));
                itmPSect.setProperty("in_note", item.getProperty("in_note", ""));

                Item itmResult = itmPSect.apply("merge");
                if (itmResult.isError())
                {
                    throw new Exception("新增團體賽組別失敗");
                }
            }

            return this;
        }
    }
}