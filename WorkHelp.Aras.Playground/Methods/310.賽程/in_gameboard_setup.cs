﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class in_gameboard_setup : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 競賽看板設定
                日期: 2021/04/30: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]in_gameboard_setup";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            string sql = "";
            string meeting_id = itmR.getProperty("meeting_id", "");

            sql = "SELECT in_title FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + meeting_id + "'";
            Item itmMeeting = inn.applySQL(sql);
            itmR.setProperty("in_title", itmMeeting.getProperty("in_title", ""));

            sql = @"
                SELECT 
	                DISTINCT t1.in_date_key
                FROM 
	                IN_MEETING_PEVENT t1 WITH(NOLOCK) 
                WHERE 
	                t1.in_meeting = '{#meeting_id}'
	                AND ISNULL(t1.in_date_key, '') <> ''
                ORDER BY 
	                t1.in_date_key
            ";

            sql = sql.Replace("{#meeting_id}", meeting_id);
            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);
            Item itmDays = inn.applySQL(sql);


            sql = @"
                SELECT 
	                DISTINCT t1.in_site
	                , t2.in_code
	                , t2.in_name
                FROM 
	                IN_MEETING_PEVENT t1 WITH(NOLOCK) 
                INNER JOIN 
	                IN_MEETING_SITE t2 WITH(NOLOCK) 
	                ON t2.id = t1.in_site 
                WHERE 
	                t1.in_meeting = '{#meeting_id}' 
                ORDER BY 
	                t2.in_code
            ";

            sql = sql.Replace("{#meeting_id}", meeting_id);
            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);
            Item itmSites = inn.applySQL(sql);

            //日期選單
            AppendItems(CCO, inn, strMethodName, itmDays, "inn_date", "in_date_key", "in_date_key", itmR);

            //場地選單
            AppendItems(CCO, inn, strMethodName, itmSites, "inn_site", "in_site", "in_name", itmR);

            return itmR;
        }

        private void AppendItems(Aras.Server.Core.CallContext CCO
            , Innovator inn
            , string strMethodName
            , Item items
            , string type_name
            , string val_property
            , string lbl_property
            , Item itmReturn)
        {
            if (type_name == "inn_date")
            {
                Item itmEmpty = inn.newItem();
                itmEmpty.setType(type_name);
                itmEmpty.setProperty("label", "請選擇");
                itmEmpty.setProperty("value", "");
                itmReturn.addRelationship(itmEmpty);
            }

            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                item.setType(type_name);
                item.setProperty("value", item.getProperty(val_property, ""));
                item.setProperty("label", item.getProperty(lbl_property, ""));
                itmReturn.addRelationship(item);
            }
        }
    }
}