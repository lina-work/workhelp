﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;
using System.Net;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class in_meeting_program_resort : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
            目的: 
                修獎牌戰場地場次
            日期: 
                - 2021-10-09: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]in_fix_site_no";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, this.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                InnH = _InnH,

                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                meeting_id = itmR.getProperty("meeting_id", ""),
            };

            cfg.meeting_id = "90A97E89B67142F9A163E86E283C21EC";

            Item items = inn.applySQL("SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE in_meeting = '" + cfg.meeting_id + "'");

            int count = items.getItemCount();

            var list = new List<TProgram>();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string id = item.getProperty("id", "");
                string in_n1 = item.getProperty("in_n1", "");
                string in_n2 = item.getProperty("in_n2", "");
                string in_n3 = item.getProperty("in_n3", "");

                var entity = new TProgram
                {
                    Id = id,
                    Value = item,
                    SortOrder = GetOrderNo(in_n1, in_n2, in_n3),
                };

                list.Add(entity);
            }

            for (int i = 0; i < list.Count; i++)
            {
                var entity = list[i];

                string sql_update = "UPDATE In_Meeting_Program SET"
                    + " in_modal_sort = '" + entity.SortOrder + "'"
                    + " WHERE id = '" + entity.Id + "'";

                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql_update);

                Item itmSQL = cfg.inn.applySQL(sql_update);
            }

            return itmR;
        }


        private class TProgram
        {
            public string Id { get; set; }
            public Item Value { get; set; }
            public int SortOrder { get; set; }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public Innosoft.InnovatorHelper InnH { get; set; }

            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }

            public string meeting_id { get; set; }
        }


        private int GetOrderNo(string in_n1, string in_n2, string in_n3)
        {
            int result = 1;
            switch (in_n1)
            {
                case "一":
                    result = result * 10000;
                    break;
                case "公":
                    result = result * 20000;
                    break;
                default:
                    result = result * 30000;
                    break;
            }

            switch (in_n2)
            {
                case "女":
                    result += 1;
                    break;

                case "男":
                    result += 2;
                    break;

                default:
                    break;
            }

            switch (in_n3)
            {
                case "一":
                    result += 100;
                    break;

                case "二":
                    result += 200;
                    break;

                case "三":
                    result += 300;
                    break;

                case "四":
                    result += 400;
                    break;

                case "五":
                    result += 500;
                    break;

                case "六":
                    result += 600;
                    break;

                case "七":
                    result += 700;
                    break;

                case "八":
                    result += 800;
                    break;

                case "九":
                    result += 900;
                    break;

                case "十":
                    result += 1000;
                    break;

                default:
                    break;
            }
            return result;
        }
    }
}