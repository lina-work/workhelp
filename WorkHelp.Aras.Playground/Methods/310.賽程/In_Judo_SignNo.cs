﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class In_Judo_SignNo : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]In_Judo_SignNo";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "input dom: " + itmR.dom.InnerXml);

            string in_battle_type = itmR.getProperty("in_battle_type", "");
            string in_team_count = itmR.getProperty("in_team_count", "");
            string in_round_code = itmR.getProperty("in_round_code", "");
            string is_tkd_to_judo = itmR.getProperty("is_tkd_to_judo", "");
            string value = itmR.getProperty("value", "");

            string map_no = "";

            if (in_team_count == "1")
            {
                map_no = "1";
            }
            else
            {
                if (is_tkd_to_judo == "1")
                {
                    //跆拳道籤號轉柔道籤號
                    map_no = TkdNoToJudoNo(in_battle_type, in_round_code, value);
                }
                else
                {
                    //柔道籤號轉跆拳道籤號
                    map_no = JudoNoToTkdNo(in_battle_type, in_round_code, value);
                }
            }

            itmR.setProperty("map_no", map_no);

            return itmR;
        }

        #region Judo 轉 TKD

        private string JudoNoToTkdNo(string in_battle_type, string in_round_code, string value)
        {
            Func<string, string> mapNo = MapNo;

            if (in_battle_type.Contains("Robin"))
            {
                mapNo = MapNo;
            }
            else
            {
                switch (in_round_code)
                {
                    case "2": mapNo = MapTKDNo_2; break;
                    case "4": mapNo = MapTKDNo_4; break;
                    case "8": mapNo = MapTKDNo_8; break;
                    case "16": mapNo = MapTKDNo_16; break;
                    case "32": mapNo = MapTKDNo_32; break;
                    case "64": mapNo = MapTKDNo_64; break;
                    case "128": mapNo = MapTKDNo_128; break;
                    default: throw new Exception("請補 256 人的對照表");
                        //default: break;
                }

            }
            return mapNo(value);
        }

        private string MapTKDNo_2(string value)
        {
            switch (value)
            {
                case "1": return "1";
                case "2": return "2";
                default: return "";
            }
        }

        private string MapTKDNo_4(string value)
        {
            switch (value)
            {
                case "1": return "1";
                case "3": return "4";
                case "2": return "3";
                case "4": return "2";
                default: return "";
            }
        }

        private string MapTKDNo_8(string value)
        {
            switch (value)
            {
                case "1": return "1";
                case "5": return "8";
                case "3": return "5";
                case "7": return "4";
                case "2": return "3";
                case "6": return "6";
                case "4": return "7";
                case "8": return "2";
                default: return "";
            }
        }

        private string MapTKDNo_16(string value)
        {
            switch (value)
            {
                case "1": return "1";
                case "9": return "16";
                case "5": return "9";
                case "13": return "8";
                case "3": return "5";
                case "11": return "12";
                case "7": return "13";
                case "15": return "4";
                case "2": return "3";
                case "10": return "14";
                case "6": return "11";
                case "14": return "6";
                case "4": return "7";
                case "12": return "10";
                case "8": return "15";
                case "16": return "2";
                default: return "";
            }
        }

        private string MapTKDNo_32(string value)
        {
            switch (value)
            {
                case "1": return "1";
                case "17": return "32";
                case "9": return "17";
                case "25": return "16";
                case "5": return "9";
                case "21": return "24";
                case "13": return "25";
                case "29": return "8";
                case "3": return "5";
                case "19": return "28";
                case "11": return "21";
                case "27": return "12";
                case "7": return "13";
                case "23": return "20";
                case "15": return "29";
                case "31": return "4";

                case "2": return "3";
                case "18": return "30";
                case "10": return "19";
                case "26": return "14";
                case "6": return "11";
                case "22": return "22";
                case "14": return "27";
                case "30": return "6";
                case "4": return "7";
                case "20": return "26";
                case "12": return "23";
                case "28": return "10";
                case "8": return "15";
                case "24": return "18";
                case "16": return "31";
                case "32": return "2";

                default: return "";
            }
        }

        private string MapTKDNo_64(string value)
        {
            switch (value)
            {
                case "1": return "1";
                case "33": return "64";
                case "17": return "33";
                case "49": return "32";
                case "9": return "17";
                case "41": return "48";
                case "25": return "49";
                case "57": return "16";

                case "5": return "9";
                case "37": return "56";
                case "21": return "41";
                case "53": return "24";
                case "13": return "25";
                case "45": return "40";
                case "29": return "57";
                case "61": return "8";

                case "3": return "5";
                case "35": return "60";
                case "19": return "37";
                case "51": return "28";
                case "11": return "21";
                case "43": return "44";
                case "27": return "53";
                case "59": return "12";

                case "7": return "13";
                case "39": return "52";
                case "23": return "45";
                case "55": return "20";
                case "15": return "29";
                case "47": return "36";
                case "31": return "61";
                case "63": return "4";

                case "2": return "3";
                case "34": return "62";
                case "18": return "35";
                case "50": return "30";
                case "10": return "19";
                case "42": return "46";
                case "26": return "51";
                case "58": return "14";

                case "6": return "11";
                case "38": return "54";
                case "22": return "43";
                case "54": return "22";
                case "14": return "27";
                case "46": return "38";
                case "30": return "59";
                case "62": return "6";

                case "4": return "7";
                case "36": return "58";
                case "20": return "39";
                case "52": return "26";
                case "12": return "23";
                case "44": return "42";
                case "28": return "55";
                case "60": return "10";

                case "8": return "15";
                case "40": return "50";
                case "24": return "47";
                case "56": return "18";
                case "16": return "31";
                case "48": return "34";
                case "32": return "63";
                case "64": return "2";
                default: return "";
            }
        }

        private string MapTKDNo_128(string value)
        {
            switch (value)
            {
                case "1": return "1";
                case "65": return "128";
                case "33": return "65";
                case "97": return "64";
                case "17": return "33";
                case "81": return "96";
                case "49": return "97";
                case "113": return "32";
                case "9": return "17";
                case "73": return "112";
                case "41": return "81";
                case "105": return "48";
                case "25": return "49";
                case "89": return "80";
                case "57": return "113";
                case "121": return "16";
                case "5": return "9";
                case "69": return "120";
                case "37": return "73";
                case "101": return "56";
                case "21": return "41";
                case "85": return "88";
                case "53": return "105";
                case "117": return "24";
                case "13": return "25";
                case "77": return "104";
                case "45": return "89";
                case "109": return "40";
                case "29": return "57";
                case "93": return "72";
                case "61": return "121";
                case "125": return "8";
                case "3": return "5";
                case "67": return "124";
                case "35": return "69";
                case "99": return "60";
                case "19": return "37";
                case "83": return "92";
                case "51": return "101";
                case "115": return "28";
                case "11": return "21";
                case "75": return "108";
                case "43": return "85";
                case "107": return "44";
                case "27": return "53";
                case "91": return "76";
                case "59": return "117";
                case "123": return "12";
                case "7": return "13";
                case "71": return "116";
                case "39": return "77";
                case "103": return "52";
                case "23": return "45";
                case "87": return "84";
                case "55": return "109";
                case "119": return "20";
                case "15": return "29";
                case "79": return "100";
                case "47": return "93";
                case "111": return "36";
                case "31": return "61";
                case "95": return "68";
                case "63": return "125";
                case "127": return "4";
                case "2": return "3";
                case "66": return "126";
                case "34": return "67";
                case "98": return "62";
                case "18": return "35";
                case "82": return "94";
                case "50": return "99";
                case "114": return "30";
                case "10": return "19";
                case "74": return "110";
                case "42": return "83";
                case "106": return "46";
                case "26": return "51";
                case "90": return "78";
                case "58": return "115";
                case "122": return "14";
                case "6": return "11";
                case "70": return "118";
                case "38": return "75";
                case "102": return "54";
                case "22": return "43";
                case "86": return "86";
                case "54": return "107";
                case "118": return "22";
                case "14": return "27";
                case "78": return "102";
                case "46": return "91";
                case "110": return "38";
                case "30": return "59";
                case "94": return "70";
                case "62": return "123";
                case "126": return "6";
                case "4": return "7";
                case "68": return "122";
                case "36": return "71";
                case "100": return "58";
                case "20": return "39";
                case "84": return "90";
                case "52": return "103";
                case "116": return "26";
                case "12": return "23";
                case "76": return "106";
                case "44": return "87";
                case "108": return "42";
                case "28": return "55";
                case "92": return "74";
                case "60": return "119";
                case "124": return "10";
                case "8": return "15";
                case "72": return "114";
                case "40": return "79";
                case "104": return "50";
                case "24": return "47";
                case "88": return "82";
                case "56": return "111";
                case "120": return "18";
                case "16": return "31";
                case "80": return "98";
                case "48": return "95";
                case "112": return "34";
                case "32": return "63";
                case "96": return "66";
                case "64": return "127";
                case "128": return "2";

                default: return "";
            }
        }
        #endregion Judo 轉 TKD

        #region TKD 轉 Judo

        private string TkdNoToJudoNo(string in_battle_type, string in_round_code, string value)
        {
            Func<string, string> mapNo = MapNo;

            if (in_battle_type.Contains("Robin"))
            {
                mapNo = MapNo;
            }
            else
            {
                switch (in_round_code)
                {
                    case "2": mapNo = MapJudoNo_2; break;
                    case "4": mapNo = MapJudoNo_4; break;
                    case "8": mapNo = MapJudoNo_8; break;
                    case "16": mapNo = MapJudoNo_16; break;
                    case "32": mapNo = MapJudoNo_32; break;
                    case "64": mapNo = MapJudoNo_64; break;
                    case "128": mapNo = MapJudoNo_128; break;
                    default: throw new Exception("請補 256 人的對照表");
                        //default: break;
                }

            }
            return mapNo(value);
        }

        private string MapJudoNo_2(string value)
        {
            switch (value)
            {
                case "1": return "1";
                case "2": return "2";
                default: return "";
            }
        }

        private string MapJudoNo_4(string value)
        {
            switch (value)
            {
                case "1": return "1";
                case "4": return "3";
                case "3": return "2";
                case "2": return "4";
                default: return "";
            }
        }

        private string MapJudoNo_8(string value)
        {
            switch (value)
            {
                case "1": return "1";
                case "8": return "5";
                case "5": return "3";
                case "4": return "7";
                case "3": return "2";
                case "6": return "6";
                case "7": return "4";
                case "2": return "8";
                default: return "";
            }
        }

        private string MapJudoNo_16(string value)
        {
            switch (value)
            {
                case "1": return "1";
                case "16": return "9";
                case "9": return "5";
                case "8": return "13";
                case "5": return "3";
                case "12": return "11";
                case "13": return "7";
                case "4": return "15";
                case "3": return "2";
                case "14": return "10";
                case "11": return "6";
                case "6": return "14";
                case "7": return "4";
                case "10": return "12";
                case "15": return "8";
                case "2": return "16";
                default: return "";
            }
        }

        private string MapJudoNo_32(string value)
        {
            switch (value)
            {
                case "1": return "1";
                case "32": return "17";
                case "17": return "9";
                case "16": return "25";
                case "9": return "5";
                case "24": return "21";
                case "25": return "13";
                case "8": return "29";
                case "5": return "3";
                case "28": return "19";
                case "21": return "11";
                case "12": return "27";
                case "13": return "7";
                case "20": return "23";
                case "29": return "15";
                case "4": return "31";

                case "3": return "2";
                case "30": return "18";
                case "19": return "10";
                case "14": return "26";
                case "11": return "6";
                case "22": return "22";
                case "27": return "14";
                case "6": return "30";
                case "7": return "4";
                case "26": return "20";
                case "23": return "12";
                case "10": return "28";
                case "15": return "8";
                case "18": return "24";
                case "31": return "16";
                case "2": return "32";
                default: return "";
            }
        }

        private string MapJudoNo_64(string value)
        {
            switch (value)
            {
                case "1": return "1";
                case "64": return "33";
                case "33": return "17";
                case "32": return "49";
                case "17": return "9";
                case "48": return "41";
                case "49": return "25";
                case "16": return "57";

                case "9": return "5";
                case "56": return "37";
                case "41": return "21";
                case "24": return "53";
                case "25": return "13";
                case "40": return "45";
                case "57": return "29";
                case "8": return "61";

                case "5": return "3";
                case "60": return "35";
                case "37": return "19";
                case "28": return "51";
                case "21": return "11";
                case "44": return "43";
                case "53": return "27";
                case "12": return "59";

                case "13": return "7";
                case "52": return "39";
                case "45": return "23";
                case "20": return "55";
                case "29": return "15";
                case "36": return "47";
                case "61": return "31";
                case "4": return "63";

                case "3": return "2";
                case "62": return "34";
                case "35": return "18";
                case "30": return "50";
                case "19": return "10";
                case "46": return "42";
                case "51": return "26";
                case "14": return "58";

                case "11": return "6";
                case "54": return "38";
                case "43": return "22";
                case "22": return "54";
                case "27": return "14";
                case "38": return "46";
                case "59": return "30";
                case "6": return "62";

                case "7": return "4";
                case "58": return "36";
                case "39": return "20";
                case "26": return "52";
                case "23": return "12";
                case "42": return "44";
                case "55": return "28";
                case "10": return "60";

                case "15": return "8";
                case "50": return "40";
                case "47": return "24";
                case "18": return "56";
                case "31": return "16";
                case "34": return "48";
                case "63": return "32";
                case "2": return "64";

                default: return "";
            }
        }

        private string MapJudoNo_128(string value)
        {
            switch (value)
            {
                case "1": return "1";
                case "128": return "65";
                case "65": return "33";
                case "64": return "97";
                case "33": return "17";
                case "96": return "81";
                case "97": return "49";
                case "32": return "113";
                case "17": return "9";
                case "112": return "73";
                case "81": return "41";
                case "48": return "105";
                case "49": return "25";
                case "80": return "89";
                case "113": return "57";
                case "16": return "121";
                case "9": return "5";
                case "120": return "69";
                case "73": return "37";
                case "56": return "101";
                case "41": return "21";
                case "88": return "85";
                case "105": return "53";
                case "24": return "117";
                case "25": return "13";
                case "104": return "77";
                case "89": return "45";
                case "40": return "109";
                case "57": return "29";
                case "72": return "93";
                case "121": return "61";
                case "8": return "125";
                case "5": return "3";
                case "124": return "67";
                case "69": return "35";
                case "60": return "99";
                case "37": return "19";
                case "92": return "83";
                case "101": return "51";
                case "28": return "115";
                case "21": return "11";
                case "108": return "75";
                case "85": return "43";
                case "44": return "107";
                case "53": return "27";
                case "76": return "91";
                case "117": return "59";
                case "12": return "123";
                case "13": return "7";
                case "116": return "71";
                case "77": return "39";
                case "52": return "103";
                case "45": return "23";
                case "84": return "87";
                case "109": return "55";
                case "20": return "119";
                case "29": return "15";
                case "100": return "79";
                case "93": return "47";
                case "36": return "111";
                case "61": return "31";
                case "68": return "95";
                case "125": return "63";
                case "4": return "127";
                case "3": return "2";
                case "126": return "66";
                case "67": return "34";
                case "62": return "98";
                case "35": return "18";
                case "94": return "82";
                case "99": return "50";
                case "30": return "114";
                case "19": return "10";
                case "110": return "74";
                case "83": return "42";
                case "46": return "106";
                case "51": return "26";
                case "78": return "90";
                case "115": return "58";
                case "14": return "122";
                case "11": return "6";
                case "118": return "70";
                case "75": return "38";
                case "54": return "102";
                case "43": return "22";
                case "86": return "86";
                case "107": return "54";
                case "22": return "118";
                case "27": return "14";
                case "102": return "78";
                case "91": return "46";
                case "38": return "110";
                case "59": return "30";
                case "70": return "94";
                case "123": return "62";
                case "6": return "126";
                case "7": return "4";
                case "122": return "68";
                case "71": return "36";
                case "58": return "100";
                case "39": return "20";
                case "90": return "84";
                case "103": return "52";
                case "26": return "116";
                case "23": return "12";
                case "106": return "76";
                case "87": return "44";
                case "42": return "108";
                case "55": return "28";
                case "74": return "92";
                case "119": return "60";
                case "10": return "124";
                case "15": return "8";
                case "114": return "72";
                case "79": return "40";
                case "50": return "104";
                case "47": return "24";
                case "82": return "88";
                case "111": return "56";
                case "18": return "120";
                case "31": return "16";
                case "98": return "80";
                case "95": return "48";
                case "34": return "112";
                case "63": return "32";
                case "66": return "96";
                case "127": return "64";
                case "2": return "128";

                default: return "";
            }
        }
        #endregion TKD 轉 Judo


        private string MapNo(string value)
        {
            return value;
        }
    }
}