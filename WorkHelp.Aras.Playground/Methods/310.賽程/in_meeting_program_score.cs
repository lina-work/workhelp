﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class in_meeting_program_score : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 登錄成績
                日期: 
                    2021-09-28: 加入現場同步 (lina)
                    2020-11-25: 加入積分功能 (lina)
                    2020-10-28: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]in_meeting_program_score";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, this.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                InnH = _InnH,

                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                mode = itmR.getProperty("mode", ""),
            };

            //取得登入者資訊
            string sql = "SELECT * FROM IN_RESUME WITH(NOLOCK) WHERE in_user_id = N'" + cfg.strUserId + "'";
            cfg.itmLogin = inn.applySQL(sql);
            if (cfg.itmLogin.isError() || cfg.itmLogin.getItemCount() != 1)
            {
                throw new Exception("登入者資料異常");
            }

            if (cfg.meeting_id == "")
            {
                itmR.setProperty("error_message", "賽事 id 不得為空白");
                return itmR;
            }

            switch (cfg.mode)
            {
                case "upgrade": //自動勝出
                    AutoUpgrade(cfg, itmR);
                    break;

                case "rollcall": //檢錄
                    RollCall(cfg, itmR);
                    break;

                case "score": //登錄成績
                    EnterScore(cfg, itmR);
                    break;

                case "sync": //現場同步
                    EnterSync(cfg, itmR);
                    break;

                case "fight": //籤腳非輪空
                    Fight(cfg, itmR);
                    break;

                case "bypass": //籤腳輪空
                    ByPass(cfg, itmR);
                    break;

                case "clear": //籤腳清空
                    ClearFoot(cfg, itmR);
                    break;

                case "dq_upgrade": //自動勝出
                    AutoUpgradeDQ(cfg, itmR);
                    break;

                default:
                    break;
            }

            return itmR;
        }

        /// <summary>
        /// 籤腳清空
        /// </summary>
        private void ClearFoot(TConfig cfg, Item itmReturn)
        {
            string sql = "";
            Item itmSQL = null;

            string meeting_id = itmReturn.getProperty("meeting_id", "");
            string program_id = itmReturn.getProperty("program_id", "");
            string tree_id = itmReturn.getProperty("tree_id", "");
            string in_sign_foot = itmReturn.getProperty("in_sign_foot", "");
            string target_sign_foot = in_sign_foot == "1" ? "2" : "1";

            sql = "SELECT * FROM IN_MEETING_PEVENT WITH(NOLOCK) WHERE source_id = '" + program_id + "' AND in_tree_id = '" + tree_id + "'";
            Item itmEvent = cfg.inn.applySQL(sql);
            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "event count: " + itmEvent.getItemCount().ToString());

            if (itmEvent.isError() || itmEvent.getItemCount() != 1)
            {
                throw new Exception("場次資料錯誤");
            }

            string event_id = itmEvent.getProperty("id", "");

            sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET in_sign_no = NULL WHERE source_id = '" + event_id + "' AND in_sign_foot = '" + in_sign_foot + "'";
            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);
            itmSQL = cfg.inn.applySQL(sql);

            sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET in_target_no = NULL WHERE source_id = '" + event_id + "' AND in_sign_foot = '" + target_sign_foot + "'";
            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);
            itmSQL = cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// 籤腳輪空
        /// </summary>
        private void ByPass(TConfig cfg, Item itmReturn)
        {
            string sql = "";
            Item itmSQL = null;

            string meeting_id = itmReturn.getProperty("meeting_id", "");
            string program_id = itmReturn.getProperty("program_id", "");
            string tree_id = itmReturn.getProperty("tree_id", "");
            string in_sign_foot = itmReturn.getProperty("in_sign_foot", "");

            sql = "SELECT * FROM IN_MEETING_PEVENT WITH(NOLOCK) WHERE source_id = '" + program_id + "' AND in_tree_id = '" + tree_id + "'";
            Item itmEvent = cfg.inn.applySQL(sql);
            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "event count: " + itmEvent.getItemCount().ToString());

            if (itmEvent.isError() || itmEvent.getItemCount() != 1)
            {
                throw new Exception("場次資料錯誤");
            }

            string event_id = itmEvent.getProperty("id", "");
            string in_next_win = itmEvent.getProperty("in_next_win", "");
            string in_next_foot_win = itmEvent.getProperty("in_next_foot_win", "");
            string in_next_target_foot = in_next_foot_win == "1" ? "2" : "1";

            sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET in_sign_bypass = '1', in_status = '' WHERE source_id = '" + event_id + "' AND in_sign_foot = '" + in_sign_foot + "'";
            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);
            itmSQL = cfg.inn.applySQL(sql);


            sql = "SELECT * FROM IN_MEETING_PEVENT_DETAIL WITH(NOLOCK) WHERE source_id = '" + event_id + "' AND in_sign_foot = '1'";
            Item itmDetail1 = cfg.inn.applySQL(sql);

            if (itmDetail1.isError() || itmDetail1.getItemCount() != 1)
            {
                throw new Exception("場次對戰資料錯誤(1)");
            }


            sql = "SELECT * FROM IN_MEETING_PEVENT_DETAIL WITH(NOLOCK) WHERE source_id = '" + event_id + "' AND in_sign_foot = '2'";
            Item itmDetail2 = cfg.inn.applySQL(sql);

            if (itmDetail2.isError() || itmDetail2.getItemCount() != 1)
            {
                throw new Exception("場次對戰資料錯誤(2)");
            }

            string sign_bypass1 = itmDetail1.getProperty("in_sign_bypass", "");
            string sign_bypass2 = itmDetail2.getProperty("in_sign_bypass", "");

            string in_bypass_foot = "";
            string in_bypass_status = "1"; //整數，0 = 不排, 1 = 無輪空, 2 = 單腳輪空(上場次), 8 兩腳皆輪空(上場次)

            if (sign_bypass1 == "1" && sign_bypass2 == "1")
            {
                in_bypass_foot = "";
                in_bypass_status = "0";
            }
            else if (sign_bypass1 == "1")
            {
                in_bypass_foot = "1";
                in_bypass_status = "0";
            }
            else if (sign_bypass2 == "1")
            {
                in_bypass_foot = "2";
                in_bypass_status = "0";
            }
            else
            {
                in_bypass_status = "1";
            }

            sql = "UPDATE IN_MEETING_PEVENT SET"
                + " in_bypass_foot = '" + in_bypass_foot + "'"
                + " , in_bypass_status = '" + in_bypass_status + "'"
                + " WHERE id = '" + event_id + "'";
            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);
            itmSQL = cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// 非輪空
        /// </summary>
        private void Fight(TConfig cfg, Item itmReturn)
        {
            string sql = "";
            Item itmSQL = null;

            string meeting_id = itmReturn.getProperty("meeting_id", "");
            string program_id = itmReturn.getProperty("program_id", "");
            string tree_id = itmReturn.getProperty("tree_id", "");
            string in_sign_foot = itmReturn.getProperty("in_sign_foot", "");

            sql = "SELECT * FROM IN_MEETING_PEVENT WITH(NOLOCK) WHERE source_id = '" + program_id + "' AND in_tree_id = '" + tree_id + "'";
            Item itmEvent = cfg.inn.applySQL(sql);
            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "event count: " + itmEvent.getItemCount().ToString());

            if (itmEvent.isError() || itmEvent.getItemCount() != 1)
            {
                throw new Exception("場次資料錯誤");
            }

            string event_id = itmEvent.getProperty("id", "");
            string in_next_win = itmEvent.getProperty("in_next_win", "");
            string in_next_foot_win = itmEvent.getProperty("in_next_foot_win", "");
            string in_next_target_foot = in_next_foot_win == "1" ? "2" : "1";

            sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET in_sign_bypass = '0', in_status = NULL WHERE source_id = '" + event_id + "' AND in_sign_foot = '" + in_sign_foot + "'";
            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);
            itmSQL = cfg.inn.applySQL(sql);

            sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET in_sign_no = '' WHERE source_id = '" + in_next_win + "' AND in_sign_foot = '" + in_next_foot_win + "'";
            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);
            itmSQL = cfg.inn.applySQL(sql);

            sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET in_target_no = '' WHERE source_id = '" + in_next_win + "' AND in_sign_foot = '" + in_next_target_foot + "'";
            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);
            itmSQL = cfg.inn.applySQL(sql);

            sql = "UPDATE IN_MEETING_PEVENT SET"
                + " in_bypass_foot = NULL"
                + " , in_bypass_status = '1'"
                + " , in_win_status = NULL"
                + " , in_win_sign_no = NULL"
                + " , in_win_time = NULL"
                + " , in_win_creator = NULL"
                + " , in_win_creator_sno = NULL"
                + " WHERE id = '" + event_id + "'";
            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);
            itmSQL = cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// 登錄成績(積分模式)
        /// </summary>
        private void EnterScore(TConfig cfg, Item itmReturn)
        {
            TScore entity = new TScore
            {
                meeting_id = itmReturn.getProperty("meeting_id", ""),
                program_id = itmReturn.getProperty("program_id", ""),
                event_id = itmReturn.getProperty("event_id", ""),
                detail_id = itmReturn.getProperty("detail_id", ""),
                points_type = itmReturn.getProperty("points_type", ""),
                points_text = itmReturn.getProperty("points_text", ""),
                team_name = itmReturn.getProperty("team_name", ""),
                target_detail_id = itmReturn.getProperty("target_detail_id", ""),
                equipment_sno = itmReturn.getProperty("equipment_sno", ""),
                status_enum = StatusEnum.None,
                in_note = "",
            };

            List<string> err = new List<string>();
            if (entity.meeting_id == "") err.Add("賽事 id 不得為空白");
            if (entity.program_id == "") err.Add("組別 id 不得為空白");
            if (entity.event_id == "") err.Add("場次 id 不得為空白");
            if (entity.detail_id == "") err.Add("對戰 id 不得為空白");
            if (entity.points_type == "") err.Add("成績 不得為空白");
            if (entity.target_detail_id == "") err.Add("對戰 id 不得為空白");

            entity.Value = GetDetail(cfg, entity.detail_id);
            entity.TargetValue = GetDetail(cfg, entity.target_detail_id);

            entity.in_sign_no = entity.Value.getProperty("in_sign_no", "");
            entity.old_points = GetIntVal(entity.Value.getProperty("in_points", "0"));
            entity.old_correct_count = GetIntVal(entity.Value.getProperty("in_correct_count", "0"));

            //設定場次資訊
            SetEventInfo(entity);
            //設定積分資訊
            SetPointsInfo(entity);

            //更新場次備註
            cfg.inn.applySQL("UPDATE IN_MEETING_PEVENT SET in_note = '" + entity.in_note + "' WHERE id = '" + entity.event_id + "'");

            EnterScore(cfg, entity, itmReturn);
        }


        /// <summary>
        /// 登錄成績(現場同步模式)
        /// </summary>
        private void EnterSync(TConfig cfg, Item itmReturn)
        {
            TScore entity = new TScore
            {
                meeting_id = itmReturn.getProperty("meeting_id", ""),
                program_id = itmReturn.getProperty("program_id", ""),
                event_id = itmReturn.getProperty("event_id", ""),
                detail_id = itmReturn.getProperty("detail_id", ""),
                target_detail_id = itmReturn.getProperty("target_detail_id", ""),
            };

            List<string> err = new List<string>();
            if (entity.meeting_id == "") err.Add("賽事 id 不得為空白");
            if (entity.program_id == "") err.Add("組別 id 不得為空白");
            if (entity.event_id == "") err.Add("場次 id 不得為空白");
            if (entity.detail_id == "") err.Add("對戰 id 不得為空白");
            if (entity.target_detail_id == "") err.Add("對戰 id 不得為空白");

            entity.Value = GetDetail(cfg, entity.detail_id);
            entity.TargetValue = GetDetail(cfg, entity.target_detail_id);

            entity.in_sign_no = entity.Value.getProperty("in_sign_no", "");
            entity.old_points = GetIntVal(entity.Value.getProperty("in_points", "0"));
            entity.old_correct_count = GetIntVal(entity.Value.getProperty("in_correct_count", "0"));

            //設定場次資訊
            SetEventInfo(entity);

            //登錄成績之後
            AfterScore(cfg, entity);
        }

        /// <summary>
        /// 設定場次資訊
        /// </summary>
        private void SetEventInfo(TScore entity)
        {
            Item source = entity.Value;
            entity.parent_program = source.getProperty("parent_program", "");
            entity.parent_battle_type = source.getProperty("parent_battle_type", "");
            entity.parent_group_rank = source.getProperty("parent_group_rank", "");
            entity.parent_tiebreakere = source.getProperty("parent_tiebreakere", "");

            entity.pg_battle_type = source.getProperty("program_battle_type", "");
            entity.pg_rank_type = source.getProperty("program_rank_type", "");
            entity.pg_team_count = source.getProperty("program_team_count", "0");
            entity.pg_fight_time = source.getProperty("program_fight_time", "0");

            entity.team_count = GetIntVal(entity.pg_team_count);
            entity.fight_time = GetIntVal(entity.pg_fight_time);

            entity.BattleMap = GetBattleMap(entity.pg_battle_type, entity.team_count);

            entity.in_tree_name = source.getProperty("in_tree_name", "");
            entity.in_tree_id = source.getProperty("in_tree_id", "");
            entity.in_round_code = source.getProperty("in_round_code", "0");
            entity.round_code = GetIntVal(entity.in_round_code);

            switch (entity.in_tree_name)
            {
                case "main":
                    entity.is_main = true;
                    break;

                case "repechage":
                    entity.is_rpc = true;
                    break;
            }

            switch (entity.in_tree_id)
            {
                case "CA01"://挑戰賽-3挑戰2
                    entity.is_ca01 = true;
                    break;

                case "CA02"://挑戰賽-2挑戰1
                    entity.is_ca02 = true;
                    break;

                case "CB01"://挑戰賽-2挑戰1成功，1再挑戰
                    entity.is_cb01 = true;
                    break;
            }

            if (entity.is_main)
            {
                entity.is_main_eight = entity.round_code == 8; //八強賽
                entity.is_main_semi = entity.round_code == 4; //準決賽
                entity.is_main_final = entity.round_code == 2; //決賽
            }

            if (entity.is_rpc)
            {
                entity.is_rpc_final = entity.round_code == 2; //四柱復活賽-決賽
            }
        }

        /// <summary>
        /// 設定積分資訊
        /// </summary>
        private void SetPointsInfo(TScore entity)
        {
            switch (entity.points_type)
            {
                case "HWIN": //勝出
                    entity.new_correct_count = entity.old_correct_count;
                    entity.status_enum = StatusEnum.Win;
                    entity.in_points_type = entity.points_type;
                    entity.status = "1";

                    if (entity.old_points == 0)
                    {
                        entity.in_points_text = "直接勝出";
                        entity.in_points = "0";
                        entity.in_note = "1";
                    }
                    else if (entity.old_points == 1)
                    {
                        //半勝勝出
                        entity.in_points_text = "一個半勝勝出";
                        entity.in_points = "1";
                        entity.in_note = "2";
                    }
                    else
                    {
                        entity.in_points_text = "其他勝出";
                        entity.in_points = entity.old_points.ToString();
                        entity.in_note = "9";
                    }
                    break;

                case "10": //全勝
                    entity.new_correct_count = entity.old_correct_count;
                    entity.status_enum = StatusEnum.Win;
                    entity.in_points_type = entity.points_type;
                    entity.status = "1";

                    if (entity.old_points == 0)
                    {
                        entity.in_points_text = "全勝";
                        entity.in_points = "10";
                        entity.in_note = "11";
                    }
                    else if (entity.old_points == 1)
                    {
                        entity.in_points_text = "半勝+全勝";
                        entity.in_points = "11";
                        entity.in_note = "12";
                    }
                    else
                    {
                        entity.in_points_text = "其他勝出";
                        entity.in_points = entity.old_points.ToString();
                        entity.in_note = "19";
                    }
                    break;

                case "1": //半勝
                    entity.new_correct_count = entity.old_correct_count;
                    if (entity.old_points == 0)
                    {
                        entity.status = "";
                        entity.in_points_type = entity.points_type;
                        entity.in_points_text = "一個半勝";
                        entity.in_points = "1";
                        entity.in_note = "21";
                    }
                    else if (entity.old_points == 1)
                    {
                        //兩個半勝合計一個全勝
                        entity.status_enum = StatusEnum.Win;
                        entity.status = "1";
                        entity.in_points_type = entity.points_type;
                        entity.in_points_text = "兩個半勝勝出";
                        entity.in_points = "10";
                        entity.in_note = "22";
                    }
                    else if (entity.old_points == 10)
                    {
                        entity.status_enum = StatusEnum.Win;
                        entity.status = "1";
                        entity.in_points_type = entity.points_type;
                        entity.in_points_text = "全勝+半勝";
                        entity.in_points = "11";
                        entity.in_note = "23";
                    }
                    else
                    {
                        entity.status_enum = StatusEnum.Win;
                        entity.status = "1";
                        entity.in_points_type = entity.points_type;
                        entity.in_points_text = "其他半勝";
                        entity.in_points = entity.old_points.ToString();
                        entity.in_note = "29";
                    }
                    break;

                case "0": //指導
                    entity.in_points = entity.old_points.ToString();
                    entity.in_points_text = entity.points_text;
                    entity.in_points_type = entity.points_type;
                    entity.new_correct_count = entity.old_correct_count + 1;
                    entity.status = "";
                    if (entity.new_correct_count >= 3)
                    {
                        entity.status_enum = StatusEnum.Correct;
                        entity.correct_msg = "指導滿次數，對方勝出";
                        entity.in_points_text = "因指導落敗";
                        entity.in_note = "31";
                    }
                    break;

                case "Initial": //歸零
                    entity.status_enum = StatusEnum.Initial;
                    entity.status = "";
                    entity.in_points = "0";
                    entity.in_points_text = entity.points_text;
                    entity.in_points_type = entity.points_type;
                    entity.new_correct_count = 0;
                    entity.in_note = "";
                    break;

                case "DQ": //失格
                    entity.new_correct_count = entity.old_correct_count;
                    entity.in_points = entity.old_points.ToString();
                    entity.status_enum = StatusEnum.DQ;
                    entity.status = "0";
                    entity.in_points_text = entity.points_text;
                    entity.in_points_type = entity.points_type;
                    entity.in_note = "91";

                    string target_points_type = entity.TargetValue.getProperty("in_points_type", "");
                    if (target_points_type == "DQ")
                    {
                        //雙方DQ
                        entity.in_note = "92";
                    }
                    break;
            }
        }

        /// <summary>
        /// 登錄成績(積分模式)
        /// </summary>
        private void EnterScore(TConfig cfg, TScore entity, Item itmReturn)
        {
            string sql = "";

            if (entity.status_enum == StatusEnum.Correct)
            {
                //因指導勝出，對方分數加10
                int target_old_points = GetIntVal(entity.TargetValue.getProperty("in_points", "0"));
                int target_new_points = target_old_points;

                if (target_old_points == 0)
                {
                    target_new_points = 10;
                }
                else if (target_old_points == 1)
                {
                    target_new_points = 11;
                }

                //對手勝出
                sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET"
                    + " in_sign_action = N'勝出'"
                    + " , in_points_text = N'因指導勝出'"
                    + " , in_points = " + target_new_points
                    + " WHERE id = '" + entity.target_detail_id + "'";
                Item itmSQL1 = cfg.inn.applySQL(sql);

                //我方因指導滿次數落敗
                sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET"
                    + " in_sign_action = N''"
                    + " , in_points_text = N'因指導落敗'"
                    + " , in_correct_count = N'" + entity.new_correct_count + "'"
                    + " WHERE id = '" + entity.detail_id + "'";
                Item itmSQL2 = cfg.inn.applySQL(sql);

                //登錄成績後
                AfterScore(cfg, entity);

                itmReturn.setProperty("correct_message", entity.correct_msg);
                itmReturn.setProperty("is_go_next", "1");
            }
            else if (entity.status_enum == StatusEnum.DQ)
            {
                //更新隊伍資料
                sql = "UPDATE IN_MEETING_PTEAM SET"
                    + " in_check_result = '0'"
                    + " , in_check_status = N'失格'"
                    + " WHERE source_id = '" + entity.program_id + "'"
                    + " AND in_sign_no = '" + entity.in_sign_no + "'";

                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "對戰失格 _# sql: " + sql);

                cfg.inn.applySQL(sql);

                //我方失格
                sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET"
                    + " in_sign_action = N'失格'"
                    + " , in_points = N'" + entity.in_points + "'"
                    + " , in_points_type = N'" + entity.in_points_type + "'"
                    + " , in_points_text = N'" + entity.in_points_text + "'"
                    + " WHERE id = '" + entity.detail_id + "'";
                Item itmSQL1 = cfg.inn.applySQL(sql);

                //對手不戰而勝
                sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET"
                    + " in_sign_action = N''"
                    + " WHERE id = '" + entity.target_detail_id + "'";
                Item itmSQL2 = cfg.inn.applySQL(sql);

                //登錄成績後
                AfterScore(cfg, entity);

                itmReturn.setProperty("is_go_next", "1");
            }
            else if (entity.status_enum == StatusEnum.Win)
            {
                //我方勝出
                sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET"
                    + " in_sign_action = N'勝出'"
                    + " , in_points = N'" + entity.in_points + "'"
                    + " , in_points_type = N'" + entity.in_points_type + "'"
                    + " , in_points_text = N'" + entity.in_points_text + "'"
                    + " WHERE id = '" + entity.detail_id + "'";
                Item itmSQL1 = cfg.inn.applySQL(sql);

                //對手戰敗
                sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET"
                    + " in_sign_action = N''"
                    + " WHERE id = '" + entity.target_detail_id + "'";
                Item itmSQL2 = cfg.inn.applySQL(sql);

                //登錄成績後
                AfterScore(cfg, entity);

                itmReturn.setProperty("is_go_next", "1");
            }
            else if (entity.points_type == "CHK")
            {
                //0129檢錄
                string chkInfo = "";
                chkInfo += "<in_equipment_sno>" + entity.equipment_sno + "</in_equipment_sno>";
                chkInfo += "<meeting_id>" + entity.meeting_id + "</meeting_id>";
                chkInfo += "<data_mode>entrance</data_mode>";
                chkInfo += "<rollcall>1</rollcall>";
                cfg.inn.applyMethod("In_Meeting_GetCheckIdByEqNo", chkInfo);

                AfterScore(cfg, entity);
            }
            else
            {
                //只更新我方狀態
                sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET"
                    + " in_sign_action = N''"
                    + " , in_points = N'" + entity.in_points + "'"
                    + " , in_points_type = N'" + entity.in_points_type + "'"
                    + " , in_points_text = N'" + entity.in_points_text + "'"
                    + " , in_correct_count = " + entity.new_correct_count + ""
                    + " WHERE id = '" + entity.detail_id + "'";
                cfg.inn.applySQL(sql);

                //我方歸零，並且我方為勝出狀態
                if (entity.status_enum == StatusEnum.Initial && entity.Value.getProperty("in_status", "") == "1")
                {
                    AfterScore(cfg, entity);
                }
            }
        }

        /// <summary>
        /// 登錄成績(積分模式)
        /// </summary>
        private void AfterScore(TConfig cfg, TScore entity)
        {
            //刷新成績(遞迴)
            ScoreRecursive(cfg, entity.event_id);

            var bmap = entity.BattleMap;

            //四柱復活賽
            if ((bmap.isRepechage || bmap.isChallenge) && (entity.is_main_eight || entity.is_main_semi))
            {
                //復活賽自動晉級
                Item items1 = GetByPassEventsRpc(cfg, entity.program_id, "1");
                AutoUpgradeRpc(cfg, entity, items1);

                Item items2 = GetByPassEventsRpc(cfg, entity.program_id, "2");
                AutoUpgradeRpc(cfg, entity, items2);
            }

            //挑戰賽
            if (bmap.isChallenge)
            {
                //if (entity.is_main_final || entity.is_rpc_final)
                //{
                //    //將123名掛至挑戰賽
                //    LinkChallengeAEvent(cfg, entity.program_id);
                //}

                if (entity.is_ca02)
                {
                    //如果挑戰賽的勝方不為 main 的勝方
                    LinkChallengeBEvent(cfg, entity.program_id);
                }
            }

            //循環賽
            if (bmap.isRobin)
            {
                //無加賽
                if (entity.parent_tiebreakere == "")
                {
                    //總結成績
                    SummaryResults(cfg, entity);
                }
                else
                {
                    //總結成績
                    TiebreakereResults(cfg, entity);
                }
            }
        }

        //復活賽自動晉級
        private void AutoUpgradeRpc(TConfig cfg, TScore entity, Item items)
        {
            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string current_event_id = item.getProperty("event_id", "");

                //刷新成績(遞迴)
                ScoreRecursive(cfg, current_event_id);
            }
        }

        #region 循環賽

        /// <summary>
        /// 總結成績
        /// </summary>
        private void TiebreakereResults(TConfig cfg, TScore entity)
        {
            //附加場次
            Item itmEventCount = GetEventCount(cfg, entity.program_id, is_parent: false);
            if (itmEventCount.isError() || itmEventCount.getItemCount() != 1)
            {
                return;
            }

            //剩餘
            int event_count = GetIntVal(itmEventCount.getProperty("event_count", ""));

            if (event_count == 0)
            {
                UpdateRobinRank(cfg, entity);

                Item itmEventCount2 = GetEventCount(cfg, entity.parent_program, is_parent: true);
                if (itmEventCount2.isError() || itmEventCount2.getItemCount() != 1)
                {
                    return;
                }
                else
                {
                    int event_count2 = GetIntVal(itmEventCount2.getProperty("event_count", ""));
                    if (event_count2 == 0)
                    {
                        AutoGenerateTiebreaker(cfg, entity);
                    }
                }
            }
        }

        #region 循環賽結算成績


        /// <summary>
        /// 總結成績
        /// </summary>
        private void SummaryResults(TConfig cfg, TScore entity)
        {
            var evts = GetRobinEvents(cfg, entity);
            var robin = MapRobin(cfg, entity, evts);

            if (entity.team_count == 2)
            {
                //三戰兩勝

                if (robin.finished_count <= 1)
                {
                    //已進行一場
                    return;
                }
                else if (robin.finished_count == 2)
                {
                    //已進行兩場

                    if (robin.players[0].win_count == 2)
                    {
                        //一人勝出兩場，取消第三場
                        robin.players[0].rank = 1;
                        robin.players[1].rank = 2;
                        SetRobinRanks(cfg, entity, robin.players, need_cancel_3rd: true);
                    }
                    else
                    {
                        //各贏一場: 復原名次與第三場
                        RecoverTwoFight3Rd(cfg, entity);
                    }
                }
                else
                {
                    //已進行三場
                    robin.players[0].rank = 1;
                    robin.players[1].rank = 2;
                    SetRobinRanks(cfg, entity, robin.players, need_cancel_3rd: false);
                }
            }
            else if (robin.event_count == robin.finished_count)
            {
                //已盡數完賽

                int no = 1;
                int rank = 1;

                var last = default(TRobinPlayer);
                for (int i = 0; i < robin.players.Count; i++)
                {
                    var current = robin.players[i];
                    current.rank = 0;

                    if (last == null)
                    {
                        rank = GetRankByNo(no);
                    }
                    else
                    {
                        if (current.win_count == last.win_count
                            && current.win_points == last.win_points
                            && current.win_times == last.win_times
                            && current.in_weight_value == last.in_weight_value)
                        {
                            rank = last.rank;
                        }
                        else
                        {
                            rank = GetRankByNo(no);
                        }
                    }

                    current.rank = rank;
                    no++;
                    last = current;
                }

                ////該組取名次
                //int group_ranks = entity.team_count - 2;
                //var rank_list = robin.players.FindAll(x => x.rank > 0 && x.rank <= group_ranks);
                //SetRobinRanks(cfg, entity, rank_list, need_cancel_3rd: false);

                //lina 2022.01.20: 取消依人數取名次算法
                SetRobinRanks(cfg, entity, robin.players, need_cancel_3rd: false);
            }
        }

        private int GetRankByNo(int no)
        {
            switch (no)
            {
                case 1:
                    return 1;
                case 2:
                    return 2;
                case 3:
                case 4:
                    return 3;
                case 5:
                case 6:
                    return 5;
                case 7:
                case 8:
                    return 7;
                default:
                    return no;
            }
        }

        private TRobin MapRobin(TConfig cfg, TScore entity, List<TEvent> evts)
        {
            var finished_count = 0;
            var players = new List<TRobinPlayer>();

            for (int i = 0; i < evts.Count; i++)
            {
                var evt = evts[i];
                if (evt.n1 == null) evt.n1 = new TDetail { value = cfg.inn.newItem() };
                if (evt.n2 == null) evt.n2 = new TDetail { value = cfg.inn.newItem() };

                var itmEvent = evt.value;
                var itmFoot1 = evt.n1.value;
                var itmFoot2 = evt.n2.value;
                var f1_sign_no = itmFoot1.getProperty("in_sign_no", "");
                var f2_sign_no = itmFoot2.getProperty("in_sign_no", "");

                AppendRobinPlayer(players, f1_sign_no);
                AppendRobinPlayer(players, f2_sign_no);

                string in_win_sign_no = itmEvent.getProperty("in_win_sign_no", "");
                if (in_win_sign_no == "" || in_win_sign_no == "0")
                {
                    continue;
                }

                finished_count++;


                var winner = players.Find(x => x.in_sign_no == in_win_sign_no);
                if (winner == null)
                {
                    cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "循環戰資料錯誤");
                }

                var itmTarget = default(Item);
                if (in_win_sign_no == f1_sign_no)
                {
                    itmTarget = itmFoot1;
                }
                else if (in_win_sign_no == f2_sign_no)
                {
                    itmTarget = itmFoot2;
                }
                else
                {
                    cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "三戰兩勝 勝出者籤號異常" + entity.program_id);
                    continue;
                }

                string in_points = itmTarget.getProperty("in_points", "0");
                string in_win_local_time = itmTarget.getProperty("in_win_local_time", "").Trim();
                string in_weight_value = itmTarget.getProperty("in_weight_value", "0");

                winner.in_weight_value = GetDcmVal(in_weight_value);
                winner.win_count++;
                winner.win_points += GetIntVal(in_points);
                winner.win_times += SumTime(entity, in_win_local_time);
                winner.times.Add(in_win_local_time);
            }

            //(1)比較選手勝場數，勝場數多者，名次在前。 

            //(2)若勝場數相同時，則比較選手積分(黃金得分積分與正規時間內得分相同)，
            //    ㄧ勝換算積分 10 分，半勝換算積分 1 分，被判犯規輸等同對方獲一勝之積分。

            //(3)若積分仍相同時，則比較該兩人比賽勝負關係，勝者名次在前。如勝場數、積分及勝負關係仍無法判定時，則依下列順序判定：
            //    A.比較選手勝場之時間總和，時間最短者，名次在前。
            //    B.比較選手之過磅單，體重較輕者，名次在前。
            //    C.若以上方式仍無法判定名次時，採用淘汰賽制重新抽籤加賽。

            var orderList = players
                .OrderByDescending(x => x.win_count) //勝場數
                .ThenByDescending(x => x.win_points) //積分
                .ThenBy(x => x.win_times)            //時間總和
                .ThenBy(x => x.in_weight_value)      //體重
                .ToList();

            TRobin result = new TRobin
            {
                event_count = evts.Count,
                finished_count = finished_count,
                players = orderList,
            };

            return result;
        }

        private void AppendRobinPlayer(List<TRobinPlayer> players, string in_sign_no)
        {
            var player = players.Find(x => x.in_sign_no == in_sign_no);

            if (player == null)
            {
                player = new TRobinPlayer
                {
                    in_sign_no = in_sign_no,
                    win_count = 0,
                    win_points = 0,
                    win_times = 0,
                    times = new List<string>(),
                };

                players.Add(player);
            }
        }

        private void RecoverTwoFight3Rd(TConfig cfg, TScore entity)
        {
            //復原名次
            string sql_upd1 = "UPDATE IN_MEETING_PTEAM SET"
                + " in_final_rank = NULL"
                + " , in_show_rank = NULL"
                + " WHERE source_id = '" + entity.program_id + "'";

            Item itmUpd1 = cfg.inn.applySQL(sql_upd1);


            //復原沒打的那一場
            string sql_upd3 = "UPDATE IN_MEETING_PEVENT SET"
                + " in_win_status = N''"
                + " WHERE source_id = '" + entity.program_id + "'"
                + " AND in_tree_id = 'M103'";

            Item itmUpd3 = cfg.inn.applySQL(sql_upd3);
        }

        private void SetRobinRanks(TConfig cfg, TScore entity, List<TRobinPlayer> players, bool need_cancel_3rd = false)
        {
            ////復原名次
            //string sql_upd1 = "UPDATE IN_MEETING_PTEAM SET"
            //    + " in_final_rank = NULL"
            //    + " , in_show_rank = NULL"
            //    + " WHERE source_id = '" + entity.program_id + "'";

            //Item itmUpd1 = cfg.inn.applySQL(sql_upd1);

            foreach (var player in players)
            {
                //設定名次
                string sql_upd2 = "UPDATE IN_MEETING_PTEAM SET"
                    + " in_final_rank = '" + player.rank + "'"
                    + " , in_show_rank = '" + player.rank + "'"
                    + " WHERE source_id = '" + entity.program_id + "'"
                    + " AND in_sign_no = '" + player.in_sign_no + "'";

                Item itmUpd2 = cfg.inn.applySQL(sql_upd2);
            }

            if (need_cancel_3rd)
            {
                //取消沒打的那一場
                string sql_upd3 = "UPDATE IN_MEETING_PEVENT SET"
                    + " in_win_status = N'cancel'"
                    + " WHERE source_id = '" + entity.program_id + "'"
                    + " AND in_tree_id = 'M103'";

                Item itmUpd3 = cfg.inn.applySQL(sql_upd3);
            }
        }

        private int SumTime(TScore entity, string value)
        {
            if (entity.fight_time <= 0 || value == "") return 0;

            int fight_seconds = entity.fight_time * 60; //比賽總秒數
            int sands = 0;

            if (value.Contains("GS"))
            {
                string tm = value.Trim().Replace("GS", "").Trim();//GS3:10
                string[] arr = tm.Split(':');
                if (arr != null && arr.Length == 2)
                {
                    sands = GetIntVal(arr[0]) * 60 + GetIntVal(arr[1]);
                }
            }
            else
            {
                string[] arr = value.Trim().Split(':');
                if (arr != null && arr.Length == 2)
                {
                    sands = GetIntVal(arr[0]) * 60 + GetIntVal(arr[1]);
                    sands *= -1;
                }
            }

            int total_seconds = fight_seconds + sands;

            if (total_seconds < 0) total_seconds = 0;

            return total_seconds;
        }

        private List<TEvent> GetRobinEvents(TConfig cfg, TScore entity)
        {
            string sql = @"
                SELECT 
	                t1.in_tree_id
	                , t1.in_win_sign_no
	                , t1.in_win_local_time
	                , t2.in_sign_foot
	                , t2.in_sign_no
	                , t2.in_points
	                , t2.in_correct_count
					, t3.in_weight_value
                FROM 
	                IN_MEETING_PEVENT t1 WITH(NOLOCK) 
                INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
				LEFT OUTER JOIN
					VU_MEETING_PTEAM t3
					ON t3.source_id = t1.source_id
					AND ISNULL(t3.in_sign_no, '0') <> '0'
					AND t3.in_sign_no = t2.in_sign_no
	                AND ISNULL(t3.in_type, '') IN ('', 'p')
                WHERE 
	                t1.source_id = '{#program_id}' 
	                AND t1.in_tree_name = 'main'
	                AND ISNULL(t1.in_type, '') IN ('', 'p')
                ORDER BY
	                t1.in_tree_no
	                , t2.in_sign_foot
            ";

            sql = sql.Replace("{#program_id}", entity.program_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();

            var evts = new List<TEvent>();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_tree_id = item.getProperty("in_tree_id", "");
                string in_sign_foot = item.getProperty("in_sign_foot", "");

                var evt = evts.Find(x => x.in_tree_id == in_tree_id);
                if (evt == null)
                {
                    evt = new TEvent
                    {
                        in_tree_id = in_tree_id,
                        value = item,
                    };
                    evts.Add(evt);
                }

                if (in_sign_foot == "1")
                {
                    evt.n1 = new TDetail { value = item };
                }
                else if (in_sign_foot == "2")
                {
                    evt.n2 = new TDetail { value = item };
                }
            }

            return evts;
        }

        private class TRobin
        {
            public int event_count { get; set; }
            public int finished_count { get; set; }
            public List<TRobinPlayer> players { get; set; }
        }

        private class TRobinPlayer
        {
            public string in_sign_no { get; set; }
            public decimal in_weight_value { get; set; }
            public int rank { get; set; }

            public int win_count { get; set; }
            public int win_points { get; set; }
            public int win_times { get; set; }


            public List<string> times { get; set; }
        }

        #endregion 循環賽結算成績

        /// <summary>
        /// 自動生成加賽場次
        /// </summary>
        private void AutoGenerateTiebreaker(TConfig cfg, TScore entity)
        {
            Item itmPost = cfg.inn.newItem();
            itmPost.setType("In_Meeting_Program");
            itmPost.setProperty("meeting_id", entity.meeting_id);
            itmPost.setProperty("program_id", entity.parent_program);
            itmPost.setProperty("mode", "tiebreaker");

            Item itmResult = itmPost.apply("in_meeting_program_group");

            if (itmResult.isError())
            {
                throw new Exception("自動生成加賽場次發生錯誤");
            }
        }

        /// <summary>
        /// 更新循環賽名次
        /// </summary>
        private void UpdateRobinRank(TConfig cfg, TScore entity)
        {
            //場次與選手資料
            var itmEvents = GetEventPlayers(cfg, entity.program_id);

            var map = MapTeam(itmEvents);

            var order_map = map.OrderByDescending(x => x.Value.total_wins).ThenByDescending(x => x.Value.total_points);

            var first = order_map.First().Value;
            var last_wins = first.total_wins;
            var last_points = first.total_points;

            var no = 1;
            var rank = 1;
            foreach (var kv in order_map)
            {
                var team = kv.Value;
                if (team.total_wins == last_wins && team.total_points == last_points)
                {
                    //同名
                    team.rank = rank;
                }
                else
                {
                    last_wins = team.total_wins;
                    last_points = team.total_points;
                    team.rank = no;
                    rank = no;
                }
                no++;
            }

            int group_rank = entity.team_count - 2;

            string sql = "UPDATE IN_MEETING_PTEAM SET"
                + " in_final_rank = {#in_final_rank}"
                + " , in_show_rank = {#in_final_rank}"
                + " WHERE source_id = '{#program_id}'"
                + " AND in_sign_no = '{#in_sign_no}'";

            foreach (var kv in order_map)
            {
                var team = kv.Value;

                string sql_temp = "";

                if (team.rank > group_rank)
                {
                    sql_temp = sql.Replace("{#program_id}", entity.program_id)
                        .Replace("{#in_sign_no}", team.in_sign_no)
                        .Replace("{#in_final_rank}", "NULL");
                }
                else
                {
                    sql_temp = sql.Replace("{#program_id}", entity.program_id)
                        .Replace("{#in_sign_no}", team.in_sign_no)
                        .Replace("{#in_final_rank}", "'" + team.rank.ToString() + "'");
                }

                //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

                Item itmSQL = cfg.inn.applySQL(sql_temp);

                if (itmSQL.isError())
                {
                    throw new Exception("自動計算名次發生錯誤");
                }
                //}
            }
        }

        #endregion 循環賽

        #region 挑戰賽

        /// <summary>
        /// 將123名掛至挑戰賽
        /// </summary>
        private void LinkChallengeAEvent(TConfig cfg, string program_id)
        {
            string sql = "SELECT * FROM IN_MEETING_PTEAM WITH(NOLOCK)"
                + " WHERE source_id = '" + program_id + "' AND in_final_rank IN (1, 2, 3)";
            Item items = cfg.inn.applySQL(sql);
            int count = items.getItemCount();
            if (count <= 0) return;

            //查找復活賽勝出者
            sql = "SELECT TOP 1 in_win_sign_no FROM IN_MEETING_PEVENT WITH(NOLOCK)"
                + " WHERE source_id = '" + program_id + "' AND in_tree_name = N'repechage' AND in_round_code = '2'";
            Item itmRcpChampion = cfg.inn.applySQL(sql);
            if (itmRcpChampion.isError()) return;

            string rcp_champion_sign_no = itmRcpChampion.getProperty("in_win_sign_no", "");

            Item itmEventA1 = GetOneEvent(cfg, program_id, "CA01");
            Item itmEventA2 = GetOneEvent(cfg, program_id, "CA02");

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);

                TDetailEdit detail = new TDetailEdit
                {
                    event_id = "",
                    sign_no = item.getProperty("in_sign_no", ""),
                    rank = item.getProperty("in_final_rank", ""),
                };

                switch (detail.rank)
                {
                    case "1":
                        detail.event_id = itmEventA2.getProperty("id", "");
                        detail.sign_foot = "1";
                        detail.target_foot = "2";
                        break;

                    case "2":
                        detail.event_id = itmEventA1.getProperty("id", "");
                        detail.sign_foot = "1";
                        detail.target_foot = "2";
                        break;

                    case "3":
                        if (detail.sign_no == rcp_champion_sign_no)
                        {
                            detail.event_id = itmEventA1.getProperty("id", "");
                            detail.sign_foot = "2";
                            detail.target_foot = "1";
                        }
                        break;

                }

                if (detail.event_id != "")
                {
                    LinkDetail(cfg, detail);

                    ////複製對戰名單至子場次 (復活賽先不改)
                    //LinkSubEvent(cfg, itmEvent, itmLoser, next_event_id, next_sign_foot);
                }
            }
        }

        /// <summary>
        /// 如果挑戰賽的勝方不為 main 的勝方
        /// </summary>
        private void LinkChallengeBEvent(TConfig cfg, string program_id)
        {
            string sql = "";

            //查找當前冠亞軍
            sql = "SELECT * FROM IN_MEETING_PTEAM WITH(NOLOCK) WHERE source_id = '" + program_id + "' AND in_final_rank IN (1, 2)";
            Item items = cfg.inn.applySQL(sql);
            int count = items.getItemCount();
            if (count <= 0) return;

            TDetailEdit champion_detail = null;
            TDetailEdit second_detail = null;

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);

                TDetailEdit detail = new TDetailEdit
                {
                    sign_no = item.getProperty("in_sign_no", ""),
                    rank = item.getProperty("in_final_rank", ""),
                };
                if (detail.rank == "1")
                {
                    champion_detail = detail;
                }
                else if (detail.rank == "2")
                {
                    second_detail = detail;
                }
            }

            //查無冠亞軍
            if (champion_detail == null || second_detail == null) return;

            //查找主線冠軍
            sql = "SELECT TOP 1 in_win_sign_no FROM IN_MEETING_PEVENT WITH(NOLOCK)"
                + " WHERE source_id = '" + program_id + "' AND in_tree_name = N'main' AND in_round_code = '2'";
            Item itmMainChampion = cfg.inn.applySQL(sql);
            if (itmMainChampion.isError()) return;

            Item itmEventB = GetOneEvent(cfg, program_id, "CB01");
            string event_id = itmEventB.getProperty("id", "");

            string main_champion_sign_no = itmMainChampion.getProperty("in_win_sign_no", "");
            if (champion_detail.sign_no == main_champion_sign_no)
            {
                //維持第一名勝出，不打最終決賽
                cfg.inn.applySQL("UPDATE IN_MEETING_PEVENT SET in_win_status = N'cancel' WHERE id = '" + event_id + "'");
                cfg.inn.applySQL("UPDATE IN_MEETING_PROGRAM SET in_challenge = N'no_more' WHERE id = '" + program_id + "'");
                return;
            }

            cfg.inn.applySQL("UPDATE IN_MEETING_PEVENT SET in_win_status = N'' WHERE id = '" + event_id + "'");
            cfg.inn.applySQL("UPDATE IN_MEETING_PROGRAM SET in_challenge = N'one_more' WHERE id = '" + program_id + "'");

            second_detail.event_id = event_id;
            second_detail.sign_foot = "1";
            second_detail.target_foot = "2";
            LinkDetail(cfg, second_detail);

            champion_detail.event_id = event_id;
            champion_detail.sign_foot = "2";
            champion_detail.target_foot = "1";
            LinkDetail(cfg, champion_detail);
        }

        /// <summary>
        /// [作廢]修復第四名的名次
        /// </summary>
        private void RecoverRank4(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string program_id)
        {
            string sql = @"
        SELECT
	        t2.in_sign_no
        FROM
	        IN_MEETING_PEVENT t1 WITH(NOLOCK)
        INNER JOIN
	        IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK)
	        ON t2.source_id = t1.id 
        WHERE
	        t1.source_id = '{#program_id}'
	        AND t1.in_tree_name = N'repechage'
	        AND t1.in_tree_rank = N'rank34'
	        AND ISNULL(t2.in_status, '') = '0'
        ";

            sql = sql.Replace("{#program_id}", program_id);

            Item item = inn.applySQL(sql);

            int count = item.getItemCount();

            if (count != 1)
            {
                CCO.Utilities.WriteDebug(strMethodName, "第四名資料異常 _# sql: " + sql);
                return;
            }

            string in_sign_no = item.getProperty("in_sign_no", "");

            sql = "UPDATE IN_MEETING_PTEAM SET in_final_rank = '{#in_final_rank}' WHERE source_id = '{#program_id}' AND in_sign_no = '{#in_sign_no}' ";

            sql = sql.Replace("{#program_id}", program_id)
                .Replace("{#in_sign_no}", in_sign_no)
                .Replace("{#in_final_rank}", "3");

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            inn.applySQL(sql);

        }
        #endregion 挑戰賽

        /// <summary>
        /// 自動勝出
        /// </summary>
        private void AutoUpgrade(TConfig cfg, Item itmReturn)
        {
            string program_id = itmReturn.getProperty("program_id", "");

            List<string> err = new List<string>();
            if (program_id == "") err.Add("組別 id 不得為空白");

            if (err.Count > 0)
            {
                itmReturn.setProperty("error_message", string.Join(" <br> ", err));
                return;
            }

            Item itmProgram = GetProgram(cfg, program_id);
            if (itmProgram.isError())
            {
                itmReturn.setProperty("error_message", "取得賽事組別發生錯誤");
                return;
            }

            //輪空，自動晉級
            Item items = GetByPassEventsMain(cfg, program_id);

            RunAutoUpgrade(cfg, items, itmReturn);
        }

        /// <summary>
        /// 自動勝出 (DQ)
        /// </summary>
        private void AutoUpgradeDQ(TConfig cfg, Item itmReturn)
        {
            string program_id = itmReturn.getProperty("program_id", "");

            List<string> err = new List<string>();
            if (program_id == "") err.Add("組別 id 不得為空白");

            if (err.Count > 0)
            {
                itmReturn.setProperty("error_message", string.Join(" <br> ", err));
                return;
            }

            Item itmProgram = GetProgram(cfg, program_id);
            if (itmProgram.isError())
            {
                itmReturn.setProperty("error_message", "取得賽事組別發生錯誤");
                return;
            }

            //DQ，自動晉級
            Item items = GetDQEventsMain(cfg, program_id);
            RunAutoUpgrade(cfg, items, itmReturn);
        }

        private void RunAutoUpgrade(TConfig cfg, Item items, Item itmReturn)
        {
            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string event_id = item.getProperty("event_id", "");

                //刷新成績(遞迴)
                ScoreRecursive(cfg, event_id);
            }
        }

        /// <summary>
        /// 檢錄(報到)
        /// </summary>
        private void RollCall(TConfig cfg, Item itmReturn)
        {
            string sql = "";

            string meeting_id = itmReturn.getProperty("meeting_id", "");
            string in_l1 = itmReturn.getProperty("in_l1", "");
            string in_l2 = itmReturn.getProperty("in_l2", "");
            string in_l3 = itmReturn.getProperty("in_l3", "");
            string in_sno = itmReturn.getProperty("in_sno", "");
            string in_sign_no = itmReturn.getProperty("in_sign_no", "");

            //檢錄類型 rollcall: 報到, weight: 過磅, spot: 抽磅
            string w_type = itmReturn.getProperty("w_type", "");
            //檢錄結果 1: 通過, 0: 不通過
            string w_check = itmReturn.getProperty("w_check", "");
            //過磅狀態 on: 過磅合格、dq: DQ、off: 未到、leave: 請假
            string w_status = itmReturn.getProperty("w_status", "");
            //過磅體重
            string w_value = itmReturn.getProperty("w_value", "");
            //檢錄狀態
            string c_status = w_check == "1" ? "通過" : "DQ";

            string sno_condition = "AND t1.in_sno = N'" + in_sno + "'";
            if (in_sign_no != "")
            {
                sno_condition = "AND t1.in_sign_no = N'" + in_sign_no + "'";
            }

            //參賽組別隊伍(暫不考慮團體賽)
            sql = @"
                SELECT 
                    t1.*
                FROM 
                    IN_MEETING_PTEAM t1 WITH(NOLOCK) 
                INNER JOIN
                    IN_MEETING_PROGRAM t2 WITH(NOLOCK)
                    ON t2.id = t1.source_id
                WHERE 
                    t2.in_meeting = '{#meeting_id}' 
                    AND t2.in_l1 = N'{#in_l1}'
                    AND t2.in_l2 = N'{#in_l2}'
                    AND t2.in_l3 = N'{#in_l3}'
                    {#sno_condition}
            ";

            sql = sql.Replace("{#meeting_id}", meeting_id)
                .Replace("{#in_l1}", in_l1)
                .Replace("{#in_l2}", in_l2)
                .Replace("{#in_l3}", in_l3)
                .Replace("{#sno_condition}", sno_condition);

            //CCO.Utilities.WriteDebug(strMethodName, sql);

            Item itmTeams = cfg.inn.applySQL(sql);

            int count = itmTeams.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item itmTeam = itmTeams.getItemByIndex(i);

                //alan 調整
                string program_id = itmTeam.getProperty("source_id", "");
                string team_id = itmTeam.getProperty("id", "");
                string in_weight_result = itmTeam.getProperty("in_weight_result", "");

                string new_weight_message = "";
                string old_weight_message = itmTeam.getProperty("in_weight_message", "");

                if (w_type == "weight")
                {
                    if (w_check == "0")
                    {
                        new_weight_message = "(DQ)";
                    }
                    else if (w_check == "1")
                    {
                        new_weight_message = "";
                    }
                }

                string sql_update = "";

                if (w_type == "")
                {
                    sql_update = "UPDATE IN_MEETING_PTEAM SET in_check_result = N'" + w_check + "', in_check_status = N'" + c_status + "' WHERE id = '" + team_id + "'";
                }
                else if (w_type == "rollcall")
                {
                    if (in_weight_result == "0")
                    {
                        //過磅不通過，不更改檢核結果
                        sql_update = "UPDATE IN_MEETING_PTEAM SET"
                            + " in_rollcall_result = N'" + w_check + "'"
                            + " WHERE id = '" + team_id + "'";
                    }
                    else
                    {
                        sql_update = "UPDATE IN_MEETING_PTEAM SET"
                            + " in_rollcall_result = N'" + w_check + "'"
                            + ", in_check_result = N'" + w_check + "'"
                            + ", in_check_status = N'" + c_status + "'"
                            + " WHERE id = '" + team_id + "'";
                    }
                }
                else if (w_type == "weight")
                {
                    if (w_value == "")
                    {
                        //無輸入體重
                        sql_update = "UPDATE IN_MEETING_PTEAM SET"
                            + " in_weight_result = N'" + w_check + "'"
                            + ", in_check_result = N'" + w_check + "'"
                            + ", in_check_status = N'" + c_status + "'"
                            + " WHERE id = '" + team_id + "'";
                    }
                    else
                    {
                        sql_update = "UPDATE IN_MEETING_PTEAM SET"
                            + " in_weight_result = N'" + w_check + "'"
                            + ", in_weight_value = N'" + w_value + "'"
                            + ", in_check_result = N'" + w_check + "'"
                            + ", in_check_status = N'" + c_status + "'"
                            + ", in_weight_message = N'" + new_weight_message + "'"
                            + " WHERE id = '" + team_id + "'";
                    }
                }
                else if (w_type == "spot")
                {
                    if (w_check == "1") w_check = "";

                    sql_update = "UPDATE IN_MEETING_PTEAM SET"
                        + "  in_check_result = N'" + w_check + "'"
                        + ", in_check_status = N'spot'"
                        + " WHERE id = '" + team_id + "'";
                }

                if (sql_update != "")
                {
                    //CCO.Utilities.WriteDebug(strMethodName, sql_update);

                    Item itmUpdate = cfg.inn.applySQL(sql_update);

                    if (itmUpdate.isError())
                    {
                        throw new Exception("對戰表更新失敗");
                    }

                    Item itmDetail = GetEventDetail(cfg, itmTeam);

                    if (itmDetail.isError() || itmDetail.getResult() == "")
                    {
                        continue;
                    }

                    string event_id = itmDetail.getProperty("event_id", "");

                    //刷新成績(遞迴)
                    ScoreRecursive(cfg, event_id);

                    itmReturn.setProperty("program_id", program_id);
                }
            }
        }

        /// <summary>
        /// 刷新成績(遞迴)
        /// </summary>
        private void ScoreRecursive(TConfig cfg, string event_id)
        {
            if (event_id == "")
            {
                return;
            }

            Item itmDetails = GetDetails(cfg, event_id);

            TEvent evt = MapEventFromTeam(itmDetails);

            if (evt == null || evt.is_error)
            {
                throw new Exception("對戰表更新失敗");
            }

            Item itmEvent = evt.value;
            Item itmWinner = evt.winner;
            Item itmLoser = evt.loser;

            string next_event_id = itmEvent.getProperty("in_next_win", "");

            if (evt.need_commit)
            {
                if (itmWinner == null || itmLoser == null)
                {
                    return;
                }

                //更新場次勝敗
                WinEvent(cfg, evt, event_id, evt.new_win_status, itmWinner, itmLoser);
                //將籤號更新至下個場次 (W)
                LinkWNextEvent(cfg, itmEvent, itmWinner);
                //將籤號更新至下個場次 (L)
                LinkLNextEvent(cfg, itmEvent, itmLoser);
                //更新名次
                UpdateRank(cfg, itmEvent, itmWinner, itmLoser);
                //更新復活表
                UpdateRepechage(cfg, itmEvent, itmWinner, itmLoser);
                //繼續往下走
                ScoreRecursive(cfg, next_event_id);

            }
            else if (evt.need_rollback)
            {
                //回滾勝敗
                RollbackEvent(cfg, event_id);
                //回滾下個場次
                RollbackNextEvent(cfg, itmEvent);
                //回滾名次
                RollbackRanks(cfg, itmEvent);
                //繼續往下走
                ScoreRecursive(cfg, next_event_id);
            }
            else if (evt.need_cancel)
            {
                //取消場次
                CancelEvent(cfg, event_id);
                //取消下個場次的對手
                CancelNextEvent(cfg, itmEvent);
                //繼續往下走
                ScoreRecursive(cfg, next_event_id);
            }
            else if (evt.need_next)
            {
                //繼續往下走
                ScoreRecursive(cfg, next_event_id);
            }
        }

        /// <summary>
        /// 更新場次勝敗
        /// </summary>
        private void WinEvent(TConfig cfg, TEvent evt, string event_id, string w_status, Item itmWinner, Item itmLoser)
        {
            string sql = "";
            Item itmSQL = null;

            string login_name = cfg.itmLogin.getProperty("in_name");
            string login_sno = cfg.itmLogin.getProperty("in_sno");

            string w_detail_id = itmWinner.getProperty("id", "");
            string l_detail_id = itmLoser.getProperty("id", "");

            string w_sign_no = itmWinner.getProperty("in_sign_no", "");

            string old_win_time = itmWinner.getProperty("in_win_time", "");
            string new_win_time = old_win_time == ""
                ? DateTime.Now.AddHours(-8).ToString("yyyy-MM-ddTHH:mm:ss")
                : Convert.ToDateTime(old_win_time).ToString("yyyy-MM-ddTHH:mm:ss");

            //更新場次勝敗狀態
            sql = "UPDATE IN_MEETING_PEVENT SET in_win_status = N'" + w_status + "'"
                + ", in_win_sign_no = '" + w_sign_no + "'"
                + ", in_win_time = '" + new_win_time + "'"
                + ", in_win_creator = N'" + login_name + "'"
                + ", in_win_creator_sno = N'" + login_sno + "'"
                + " WHERE id = '" + event_id + "' ";
            itmSQL = cfg.inn.applySQL(sql);

            //更新為敗
            sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET in_status = '0' WHERE id = '" + l_detail_id + "' ";
            itmSQL = cfg.inn.applySQL(sql);

            //更新為勝
            sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET in_status = '1' WHERE id = '" + w_detail_id + "' ";
            itmSQL = cfg.inn.applySQL(sql);

            //將未決的子場註記主場決出時間
            sql = "UPDATE IN_MEETING_PEVENT SET in_win_time = '" + new_win_time + "' WHERE in_parent = '" + event_id + "' AND in_win_time IS NULL";
            itmSQL = cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// 回滾勝敗
        /// </summary>
        private void RollbackEvent(TConfig cfg, string event_id)
        {
            string sql = "";
            Item itmSQL = null;

            //清空場次勝敗狀態
            sql = "UPDATE IN_MEETING_PEVENT SET in_win_status = N''"
                + ", in_win_sign_no = ''"
                + ", in_win_time = NULL"
                + ", in_win_creator = N''"
                + ", in_win_creator_sno = N''"
                + " WHERE id = '" + event_id + "' ";
            itmSQL = cfg.inn.applySQL(sql);

            //清空勝敗
            sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET in_status = '', in_sign_status = N'', in_target_status = N'' WHERE source_id = '" + event_id + "' ";
            itmSQL = cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// 取消場次
        /// </summary>
        private void CancelEvent(TConfig cfg, string event_id)
        {
            string sql = "";
            Item itmSQL = null;

            //清空場次勝敗狀態
            sql = "UPDATE IN_MEETING_PEVENT SET in_win_status = N'cancel', in_win_sign_no = '' WHERE id = '" + event_id + "' ";
            itmSQL = cfg.inn.applySQL(sql);

            //更新為敗
            sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET in_status = '0' WHERE source_id = '" + event_id + "' ";
            itmSQL = cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// 將籤號更新至下個場次 (W 路線)
        /// </summary>
        private void LinkWNextEvent(TConfig cfg, Item itmEvent, Item itmWinner)
        {
            //下個場次 id
            string next_event_id = itmEvent.getProperty("in_next_win", "");
            //下個場次腳位
            string next_sign_foot = itmEvent.getProperty("in_next_foot_win", "");
            //下個場次腳位 (對手那一筆)
            string next_target_foot = next_sign_foot == "1" ? "2" : "1";

            if (next_event_id == "") return;

            TDetailEdit detail = new TDetailEdit
            {
                sign_no = itmWinner.getProperty("in_sign_no", ""),
                event_id = next_event_id,
                sign_foot = next_sign_foot,
                target_foot = next_target_foot,
            };

            //將籤號更新至下個場次
            LinkDetail(cfg, detail);
        }

        //取得子場次
        private Item GetSubEventDetails(TConfig cfg, string eid, string foot)
        {
            string sql = @"
                SELECT 
	                t1.in_tree_id
	                , t2.*
                FROM 
	                IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                WHERE 
	                t1.in_parent = '{#eid}'
	                AND t1.in_sub_id <> 7
	                AND t2.in_sign_foot = '{#foot}'
                ORDER BY 
	                t1.in_sub_id
            ";

            sql = sql.Replace("{#eid}", eid)
                .Replace("{#foot}", foot);

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// 將籤號更新至下個場次 (L 路線)
        /// </summary>
        private void LinkLNextEvent(TConfig cfg, Item itmEvent, Item itmLoser)
        {
            //下個場次 id
            string next_event_id = itmEvent.getProperty("in_next_lose", "");
            //下個場次腳位
            string next_sign_foot = itmEvent.getProperty("in_next_foot_lose", "");
            //下個場次腳位 (對手那一筆)
            string next_target_foot = next_sign_foot == "1" ? "2" : "1";

            if (next_event_id == "") return;

            TDetailEdit detail = new TDetailEdit
            {
                sign_no = itmLoser.getProperty("in_sign_no", ""),
                event_id = next_event_id,
                sign_foot = next_sign_foot,
                target_foot = next_target_foot,
            };

            //將籤號更新至下個場次
            LinkDetail(cfg, detail);
        }

        /// <summary>
        /// 將籤號更新至下個場次
        /// </summary>
        private void LinkDetail(TConfig cfg, TDetailEdit detail)
        {
            string sql = "";

            Item itmSQL = null;

            sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET"
                + " in_sign_no = '" + detail.sign_no + "'"
                + ", in_sign_status = ''"
                + " WHERE source_id = '" + detail.event_id + "'"
                + " AND in_sign_foot = '" + detail.sign_foot + "'";

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            itmSQL = cfg.inn.applySQL(sql);


            sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET"
                + " in_target_no = '" + detail.sign_no + "'"
                + ", in_target_status = ''"
                + " WHERE source_id = '" + detail.event_id + "'"
                + " AND in_sign_foot = '" + detail.target_foot + "'";

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            itmSQL = cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// 回滾下個場次
        /// </summary>
        private void RollbackNextEvent(TConfig cfg, Item itmEvent)
        {
            string sql = "";
            Item itmSQL = null;

            //下個場次 id
            string next_event_id = itmEvent.getProperty("in_next_win", "");

            //下個場次腳位
            string next_sign_foot = itmEvent.getProperty("in_next_foot_win", "");

            //下個場次腳位 (對手那一筆)
            string next_target_foot = next_sign_foot == "1" ? "2" : "1";

            if (next_event_id == "")
            {
                return;
            }

            sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET in_sign_no = '', in_sign_status = '' WHERE source_id = '" + next_event_id + "' AND in_sign_foot = '" + next_sign_foot + "'";
            itmSQL = cfg.inn.applySQL(sql);

            sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET in_target_no = '', in_target_status = '' WHERE source_id = '" + next_event_id + "' AND in_sign_foot = '" + next_target_foot + "'";
            itmSQL = cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// 取消下個場次的對手
        /// </summary>
        private void CancelNextEvent(TConfig cfg, Item itmEvent)
        {
            string sql = "";
            Item itmSQL = null;

            //下個場次 id
            string next_event_id = itmEvent.getProperty("in_next_win", "");

            //下個場次腳位
            string next_sign_foot = itmEvent.getProperty("in_next_foot_win", "");

            //下個場次腳位 (對手那一筆)
            string next_target_foot = next_sign_foot == "1" ? "2" : "1";

            if (next_event_id == "")
            {
                return;
            }

            sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET in_sign_no = '', in_sign_status = N'cancel' WHERE source_id = '" + next_event_id + "' AND in_sign_foot = '" + next_sign_foot + "'";
            itmSQL = cfg.inn.applySQL(sql);

            sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET in_target_no = '', in_target_status = N'cancel' WHERE source_id = '" + next_event_id + "' AND in_sign_foot = '" + next_target_foot + "'";
            itmSQL = cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// 更新名次
        /// </summary>
        private void UpdateRank(TConfig cfg, Item itmEvent, Item itmWinner, Item itmLoser)
        {
            string in_tree_rank = itmEvent.getProperty("in_tree_rank", "");
            string in_tree_rank_ns = itmEvent.getProperty("in_tree_rank_ns", "");
            string in_tree_rank_nss = itmEvent.getProperty("in_tree_rank_nss", "");

            if (in_tree_rank == "rank3")
            {
                //同時有2名第三名
                UpdateRank3s(cfg, itmEvent, itmWinner, itmLoser);
            }
            else
            {
                TRank rank = GetRankInfo(in_tree_rank, in_tree_rank_ns, in_tree_rank_nss);

                if (rank.need_update)
                {
                    itmWinner.setProperty("in_final_rank", rank.w_rank);
                    itmLoser.setProperty("in_final_rank", rank.l_rank);

                    itmWinner.setProperty("in_show_rank", rank.show_w_rank);
                    itmLoser.setProperty("in_show_rank", rank.show_l_rank);

                    UpdateRanks(cfg, itmEvent, itmWinner, itmLoser);
                }
            }
        }

        /// <summary>
        /// 執行更新名次
        /// </summary>
        private void UpdateRank3s(TConfig cfg, Item itmEvent, Item itmWinner, Item itmLoser)
        {
            string sql = "";
            Item itmSQL = null;

            string program_id = itmEvent.getProperty("program_id", "");

            string winner_sign_no = itmWinner.getProperty("in_sign_no", "");
            string winner_rank = itmWinner.getProperty("in_final_rank", "");
            string show_winner_rank = itmWinner.getProperty("in_show_rank", "");

            string loser_sign_no = itmLoser.getProperty("in_sign_no");
            string loser_rank = itmLoser.getProperty("in_final_rank");
            string show_loser_rank = itmLoser.getProperty("in_show_rank");


            sql = "UPDATE IN_MEETING_PTEAM SET in_final_rank = {#rank}, in_show_rank = {#rank} WHERE source_id = '{#pid}' AND in_sign_no = '{#sign_no}'";

            //勝者
            string sql_win = sql.Replace("{#pid}", program_id)
                .Replace("{#sign_no}", winner_sign_no)
                .Replace("{#rank}", "3");

            itmSQL = cfg.inn.applySQL(sql_win);

            //敗者
            string sql_los = sql.Replace("{#pid}", program_id)
                .Replace("{#sign_no}", loser_sign_no)
                .Replace("{#rank}", "5");

            itmSQL = cfg.inn.applySQL(sql_los);
        }

        /// <summary>
        /// 執行更新名次
        /// </summary>
        private void UpdateRanks(TConfig cfg, Item itmEvent, Item itmWinner, Item itmLoser)
        {
            Item itmSQL = null;

            string program_id = itmEvent.getProperty("program_id", "");

            string winner_sign_no = itmWinner.getProperty("in_sign_no", "");
            string winner_rank = itmWinner.getProperty("in_final_rank", "");
            string show_winner_rank = itmWinner.getProperty("in_show_rank", "");

            string loser_sign_no = itmLoser.getProperty("in_sign_no");
            string loser_rank = itmLoser.getProperty("in_final_rank");
            string show_loser_rank = itmLoser.getProperty("in_show_rank");

            //清空名次
            string sql1 = "UPDATE IN_MEETING_PTEAM SET in_final_rank = NULL, in_show_rank = NULL"
                + " WHERE source_id = '" + program_id + "' AND in_final_rank IN (" + winner_rank + ", " + loser_rank + ")";
            itmSQL = cfg.inn.applySQL(sql1);

            string sql2 = "UPDATE IN_MEETING_PTEAM SET in_final_rank = '" + winner_rank + "', in_show_rank = '" + show_winner_rank + "'"
                + " WHERE source_id = '" + program_id + "' AND in_sign_no = '" + winner_sign_no + "'";
            itmSQL = cfg.inn.applySQL(sql2);

            string sql3 = "UPDATE IN_MEETING_PTEAM SET in_final_rank = '" + loser_rank + "', in_show_rank = '" + show_loser_rank + "'"
                + " WHERE source_id = '" + program_id + "' AND in_sign_no = '" + loser_sign_no + "'";
            itmSQL = cfg.inn.applySQL(sql3);
        }

        /// <summary>
        /// 回滾名次
        /// </summary>
        private void RollbackRanks(TConfig cfg, Item itmEvent)
        {
            string program_id = itmEvent.getProperty("program_id", "");
            string in_tree_rank = itmEvent.getProperty("in_tree_rank", "");
            string in_tree_rank_ns = itmEvent.getProperty("in_tree_rank_ns", "");
            string in_tree_rank_nss = itmEvent.getProperty("in_tree_rank_nss", "");

            TRank rank = GetRankInfo(in_tree_rank, in_tree_rank_ns, in_tree_rank_nss);

            if (rank.need_update)
            {
                string sql = "UPDATE IN_MEETING_PTEAM SET in_final_rank = NULL, in_show_rank = NULL"
                    + " WHERE source_id = '" + program_id + "'"
                    + " AND in_final_rank IN(" + rank.w_rank + ", " + rank.l_rank + ")";
                //清空名次
                Item itmSQL = cfg.inn.applySQL(sql);
            }
        }

        /// <summary>
        /// 更新復活表
        /// </summary>
        private void UpdateRepechage(TConfig cfg, Item itmEvent, Item itmWinner, Item itmLoser)
        {
            //組別 id
            string program_id = itmEvent.getProperty("program_id", "");
            //樹圖名稱
            string in_tree_name = itmEvent.getProperty("in_tree_name", "");
            //輪次代碼
            string in_round_code = itmEvent.getProperty("in_round_code", "");

            //主線準決賽
            if (in_tree_name == "main" && (in_round_code == "4" || in_round_code == "8"))
            {
                Item itmProgram = GetProgram(cfg, program_id);
                string in_battle_type = itmProgram.getProperty("in_battle_type", "");
                string mt_battle_repechage = itmProgram.getProperty("mt_battle_repechage", "");

                //非四柱復活賽、挑戰賽，不處理
                bool need_rpc = in_battle_type == "JudoTopFour" || in_battle_type == "Challenge";
                if (!need_rpc) return;

                //對戰人數少於 4 人，不處理
                int team_count = GetIntVal(itmProgram.getProperty("in_team_count", ""));
                if (team_count <= 4) return;

                if (mt_battle_repechage == "quarterfinals")
                {
                    //前八強
                    if (in_round_code == "8")
                    {
                        SetRpcPlayer1(cfg, itmEvent, itmWinner, team_count, in_round_code);
                    }
                    else
                    {
                        SetRpcPlayer2(cfg, itmEvent, itmWinner, team_count, in_round_code);
                        SetRpcPlayer2(cfg, itmEvent, itmLoser, team_count, in_round_code);
                    }
                }
                else
                {
                    if (in_round_code == "8")
                    {
                        UpdateJudoTopFour2(cfg, itmEvent, itmWinner, team_count, in_round_code);
                    }
                    else
                    {
                        UpdateJudoTopFour(cfg, itmEvent, itmWinner, team_count, in_round_code);
                        UpdateJudoTopFour(cfg, itmEvent, itmLoser, team_count, in_round_code);
                    }
                }

            }
        }

        #region 前八強

        /// <summary>
        /// 四強賽制(柔道模式)
        /// </summary>
        private void SetRpcPlayer1(TConfig cfg, Item itmEvent, Item itmDetail, int team_count, string in_round_code)
        {
            //組別 id
            string program_id = itmEvent.getProperty("program_id", "");
            //組別 id
            string in_tree_id = itmEvent.getProperty("in_tree_id", "");
            //選手籤號
            string related_sign_no = itmDetail.getProperty("in_sign_no", "");
            //選手籤腳
            string related_sign_foot = itmDetail.getProperty("in_sign_foot", "");
            //選手柱
            string related_column_code = GetColumnCode2(in_tree_id, related_sign_foot);

            if (related_sign_no == "" || related_sign_no == "0" || related_column_code == "")
            {
                return;
            }

            //對手清單
            Item itmFighters = GetFightersQuarterfinals(cfg, program_id, related_sign_no);

            int count = itmFighters.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item itmFighter = itmFighters.getItemByIndex(i);

                TLink link = GetRpcTreeId8(itmEvent, itmDetail, itmFighter, related_column_code, i);

                //更新對戰明細
                UpdateJudoTopFourDetail(cfg
                    , program_id
                    , team_count
                    , link
                    , itmFighter);
            }
        }


        private void SetRpcPlayer2(TConfig cfg, Item itmEvent, Item itmDetail, int team_count, string in_round_code)
        {
            //組別 id
            string program_id = itmEvent.getProperty("program_id", "");
            //組別 id
            string in_tree_id = itmEvent.getProperty("in_tree_id", "");
            //選手籤號
            string related_sign_no = itmDetail.getProperty("in_sign_no", "");
            //選手籤腳
            string related_sign_foot = itmDetail.getProperty("in_sign_foot", "");
            //選手柱
            string related_column_code = GetColumnCode(in_tree_id, related_sign_foot);

            if (related_sign_no == "" || related_sign_no == "0")
            {
                return;
            }

            //對手清單
            Item itmFighters = GetFightersQuarterfinals(cfg, program_id, related_sign_no);

            int count = itmFighters.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item itmFighter = itmFighters.getItemByIndex(i);

                TLink link = GetRpcTreeId8(itmEvent, itmDetail, itmFighter, related_column_code, i);

                //更新對戰明細
                UpdateJudoTopFourDetail(cfg
                    , program_id
                    , team_count
                    , link
                    , itmFighter);
            }
        }

        #endregion 前八強

        /// <summary>
        /// 四強賽制(柔道模式)
        /// </summary>
        private void UpdateJudoTopFour2(TConfig cfg, Item itmEvent, Item itmDetail, int team_count, string in_round_code)
        {
            //組別 id
            string program_id = itmEvent.getProperty("program_id", "");
            //組別 id
            string in_tree_id = itmEvent.getProperty("in_tree_id", "");
            //選手籤號
            string related_sign_no = itmDetail.getProperty("in_sign_no", "");
            //選手籤腳
            string related_sign_foot = itmDetail.getProperty("in_sign_foot", "");
            //選手柱
            string related_column_code = GetColumnCode2(in_tree_id, related_sign_foot);

            if (related_sign_no == "" || related_sign_no == "0" || related_column_code == "")
            {
                return;
            }

            //對手清單
            Item itmFighters = GetFightersBeforeSemiFinal(cfg, program_id, related_sign_no);

            int count = itmFighters.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item itmFighter = itmFighters.getItemByIndex(i);

                TLink link = null;

                if (team_count <= 8)
                {
                    link = GetRpcTreeId8(itmEvent, itmDetail, itmFighter, related_column_code, i);
                }
                else if (team_count <= 16)
                {
                    link = GetRpcTreeId16(itmEvent, itmDetail, itmFighter, related_column_code, i);
                }
                else if (team_count <= 32)
                {
                    link = GetRpcTreeId32(itmEvent, itmDetail, itmFighter, related_column_code, i);
                }
                else if (team_count <= 64)
                {
                    link = GetRpcTreeId64(itmEvent, itmDetail, itmFighter, related_column_code, i);
                }
                else if (team_count <= 128)
                {
                    link = GetRpcTreeId128(itmEvent, itmDetail, itmFighter, related_column_code, i);
                }

                if (link == null)
                {
                    continue;
                }

                //更新對戰明細
                UpdateJudoTopFourDetail(cfg
                    , program_id
                    , team_count
                    , link
                    , itmFighter);
            }
        }

        private void UpdateJudoTopFour(TConfig cfg, Item itmEvent, Item itmDetail, int team_count, string in_round_code)
        {
            //組別 id
            string program_id = itmEvent.getProperty("program_id", "");
            //組別 id
            string in_tree_id = itmEvent.getProperty("in_tree_id", "");
            //選手籤號
            string related_sign_no = itmDetail.getProperty("in_sign_no", "");
            //選手籤腳
            string related_sign_foot = itmDetail.getProperty("in_sign_foot", "");
            //選手柱
            string related_column_code = GetColumnCode(in_tree_id, related_sign_foot);

            if (related_sign_no == "" || related_sign_no == "0")
            {
                return;
            }

            //對手清單
            Item itmFighters = GetFightersBeforeSemiFinal(cfg, program_id, related_sign_no);

            int count = itmFighters.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item itmFighter = itmFighters.getItemByIndex(i);

                TLink link = null;

                if (team_count <= 8)
                {
                    link = GetRpcTreeId8(itmEvent, itmDetail, itmFighter, related_column_code, i);
                }
                else if (team_count <= 16)
                {
                    link = GetRpcTreeId16(itmEvent, itmDetail, itmFighter, related_column_code, i);
                }
                else if (team_count <= 32)
                {
                    link = GetRpcTreeId32(itmEvent, itmDetail, itmFighter, related_column_code, i);
                }
                else if (team_count <= 64)
                {
                    link = GetRpcTreeId64(itmEvent, itmDetail, itmFighter, related_column_code, i);
                }
                else if (team_count <= 128)
                {
                    link = GetRpcTreeId128(itmEvent, itmDetail, itmFighter, related_column_code, i);
                }

                if (link == null)
                {
                    continue;
                }

                //更新對戰明細
                UpdateJudoTopFourDetail(cfg
                    , program_id
                    , team_count
                    , link
                    , itmFighter);
            }
        }

        /// <summary>
        /// 更新對戰明細
        /// </summary>
        private void UpdateJudoTopFourDetail(TConfig cfg, string program_id, int team_count, TLink link, Item itmFighter)
        {
            string sql = "";
            Item itmSQL = null;

            //team id
            string team_id = itmFighter.getProperty("team_id", "");
            //籤號
            string sign_no = itmFighter.getProperty("in_sign_no", "");
            //籤號狀態
            string sign_status = itmFighter.getProperty("in_sign_status", "");
            if (sign_status != "cancel")
            {
                sign_status = "";
            }
            //是否輪空
            string sign_bypass = team_id == "" ? "1" : "0";

            string next_tree_id = link.id;
            string next_sign_foot = link.foot;
            string next_target_foot = next_sign_foot == "1" ? "2" : "1";

            Item itmRpcEvent = GetOneEvent(cfg, program_id, next_tree_id);
            string in_type = itmRpcEvent.getProperty("in_type", "");
            string in_cancel = itmRpcEvent.getProperty("in_cancel", "");

            if (in_cancel == "1")
            {
                //強制取消
                return;
            }

            string next_event_id = itmRpcEvent.getProperty("id", "");

            sql = @"
                UPDATE
                    IN_MEETING_PEVENT_DETAIL 
                SET
                    in_sign_no = '{#sign_no}'
                    , in_sign_bypass = '{#sign_bypass}'
                    , in_sign_status = '{#sign_status}'
                WHERE
                    source_id = '{#event_id}' 
                    AND in_sign_foot = '{#sign_foot}'
            ";

            sql = sql.Replace("{#event_id}", next_event_id)
                .Replace("{#sign_foot}", next_sign_foot)
                .Replace("{#sign_no}", sign_no)
                .Replace("{#sign_bypass}", sign_bypass)
                .Replace("{#sign_status}", sign_status);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            itmSQL = cfg.inn.applySQL(sql);

            sql = @"
                UPDATE
                    IN_MEETING_PEVENT_DETAIL 
                SET
                    in_target_no = '{#sign_no}'
                    , in_target_bypass = '{#sign_bypass}'
                    , in_target_status = '{#sign_status}'
                WHERE
                    source_id = '{#event_id}' 
                    AND in_sign_foot = '{#sign_foot}'
            ";

            sql = sql.Replace("{#event_id}", next_event_id)
                .Replace("{#sign_foot}", next_target_foot)
                .Replace("{#sign_no}", sign_no)
                .Replace("{#sign_bypass}", sign_bypass)
                .Replace("{#sign_status}", sign_status);

            //CCO.Utilities.WriteDebug(strMethodName, sql);

            itmSQL = cfg.inn.applySQL(sql);

            //if (in_type == "p")
            //{
            //    //複製對戰名單至子場次
            //    string my_eid = itmFighter.getProperty("event_id", "");
            //    string my_foot = itmFighter.getProperty("in_sign_foot", "");
            //    LinkSubEvent(cfg, my_eid, my_foot, next_event_id, next_sign_foot);
            //}
        }

        private string GetColumnCode(string in_tree_id, string in_sign_foot)
        {
            if (in_tree_id.EndsWith("1"))
            {
                if (in_sign_foot == "1")
                {
                    return "1";
                }
                else
                {
                    return "2";
                }
            }
            else
            {
                if (in_sign_foot == "1")
                {
                    return "3";
                }
                else
                {
                    return "4";
                }
            }
        }

        private string GetColumnCode2(string in_tree_id, string in_sign_foot)
        {
            var code = in_tree_id[in_tree_id.Length - 1].ToString();
            switch (code)
            {
                case "1": return "1";
                case "2": return "2";
                case "3": return "3";
                case "4": return "4";
                default: return "";
            }
        }
        #region GetRpcTreeId

        private TLink GetRpcTreeId8(Item itmEvent, Item itmDetail, Item itmFighter, string related_column_code, int idx)
        {
            List<TLink> links = new List<TLink>();
            switch (related_column_code)
            {
                case "1":
                    links.Add(new TLink { id = "R101", foot = "1" });
                    break;

                case "2":
                    links.Add(new TLink { id = "R101", foot = "2" });
                    break;

                case "3":
                    links.Add(new TLink { id = "R102", foot = "1" });
                    break;

                case "4":
                    links.Add(new TLink { id = "R102", foot = "2" });
                    break;

                default:
                    break;
            }

            if (links.Count > idx)
            {
                return links[idx];
            }
            else
            {
                return new TLink { id = "", foot = "" };
            }
        }

        private TLink GetRpcTreeId16(Item itmEvent, Item itmDetail, Item itmFighters, string related_column_code, int idx)
        {
            List<TLink> links = new List<TLink>();

            switch (related_column_code)
            {
                case "1":
                    links.Add(new TLink { id = "R101", foot = "1" });
                    links.Add(new TLink { id = "R101", foot = "2" });
                    break;

                case "2":
                    links.Add(new TLink { id = "R102", foot = "1" });
                    links.Add(new TLink { id = "R102", foot = "2" });
                    break;

                case "3":
                    links.Add(new TLink { id = "R103", foot = "1" });
                    links.Add(new TLink { id = "R103", foot = "2" });
                    break;

                case "4":
                    links.Add(new TLink { id = "R104", foot = "1" });
                    links.Add(new TLink { id = "R104", foot = "2" });
                    break;

                default:
                    break;
            }

            return links.Count > idx ? links[idx] : new TLink { id = "", foot = "" };
        }

        private TLink GetRpcTreeId32(Item itmEvent, Item itmDetail, Item itmFighters, string related_column_code, int idx)
        {
            List<TLink> links = new List<TLink>();

            switch (related_column_code)
            {
                case "1":
                    links.Add(new TLink { id = "R101", foot = "1" });
                    links.Add(new TLink { id = "R101", foot = "2" });
                    links.Add(new TLink { id = "R201", foot = "2" });
                    break;

                case "2":
                    links.Add(new TLink { id = "R102", foot = "1" });
                    links.Add(new TLink { id = "R102", foot = "2" });
                    links.Add(new TLink { id = "R202", foot = "2" });
                    break;

                case "3":
                    links.Add(new TLink { id = "R103", foot = "1" });
                    links.Add(new TLink { id = "R103", foot = "2" });
                    links.Add(new TLink { id = "R203", foot = "2" });
                    break;

                case "4":
                    links.Add(new TLink { id = "R104", foot = "1" });
                    links.Add(new TLink { id = "R104", foot = "2" });
                    links.Add(new TLink { id = "R204", foot = "2" });
                    break;

                default:
                    break;
            }

            return links.Count > idx ? links[idx] : new TLink { id = "", foot = "" };
        }

        private TLink GetRpcTreeId64(Item itmEvent, Item itmDetail, Item itmFighters, string related_column_code, int idx)
        {
            List<TLink> links = new List<TLink>();

            switch (related_column_code)
            {
                case "1":
                    links.Add(new TLink { id = "R101", foot = "1" });
                    links.Add(new TLink { id = "R101", foot = "2" });
                    links.Add(new TLink { id = "R201", foot = "2" });
                    links.Add(new TLink { id = "R301", foot = "2" });
                    break;

                case "2":
                    links.Add(new TLink { id = "R102", foot = "1" });
                    links.Add(new TLink { id = "R102", foot = "2" });
                    links.Add(new TLink { id = "R202", foot = "2" });
                    links.Add(new TLink { id = "R302", foot = "2" });
                    break;

                case "3":
                    links.Add(new TLink { id = "R103", foot = "1" });
                    links.Add(new TLink { id = "R103", foot = "2" });
                    links.Add(new TLink { id = "R203", foot = "2" });
                    links.Add(new TLink { id = "R303", foot = "2" });
                    break;

                case "4":
                    links.Add(new TLink { id = "R104", foot = "1" });
                    links.Add(new TLink { id = "R104", foot = "2" });
                    links.Add(new TLink { id = "R204", foot = "2" });
                    links.Add(new TLink { id = "R304", foot = "2" });
                    break;

                default:
                    break;
            }

            return links.Count > idx ? links[idx] : new TLink { id = "", foot = "" };
        }

        private TLink GetRpcTreeId128(Item itmEvent, Item itmDetail, Item itmFighters, string related_column_code, int idx)
        {
            List<TLink> links = new List<TLink>();

            switch (related_column_code)
            {
                case "1":
                    links.Add(new TLink { id = "R101", foot = "1" });
                    links.Add(new TLink { id = "R101", foot = "2" });
                    links.Add(new TLink { id = "R201", foot = "2" });
                    links.Add(new TLink { id = "R301", foot = "2" });
                    links.Add(new TLink { id = "R401", foot = "2" });
                    break;

                case "2":
                    links.Add(new TLink { id = "R102", foot = "1" });
                    links.Add(new TLink { id = "R102", foot = "2" });
                    links.Add(new TLink { id = "R202", foot = "2" });
                    links.Add(new TLink { id = "R302", foot = "2" });
                    links.Add(new TLink { id = "R402", foot = "2" });
                    break;

                case "3":
                    links.Add(new TLink { id = "R103", foot = "1" });
                    links.Add(new TLink { id = "R103", foot = "2" });
                    links.Add(new TLink { id = "R203", foot = "2" });
                    links.Add(new TLink { id = "R303", foot = "2" });
                    links.Add(new TLink { id = "R403", foot = "2" });
                    break;

                case "4":
                    links.Add(new TLink { id = "R104", foot = "1" });
                    links.Add(new TLink { id = "R104", foot = "2" });
                    links.Add(new TLink { id = "R204", foot = "2" });
                    links.Add(new TLink { id = "R304", foot = "2" });
                    links.Add(new TLink { id = "R404", foot = "2" });
                    break;

                default:
                    break;
            }

            return links.Count > idx ? links[idx] : new TLink { id = "", foot = "" };
        }

        #endregion GetRpcTreeId

        /// <summary>
        /// 轉換資料
        /// </summary>
        private TEvent MapEventFromTeam(Item items)
        {
            Dictionary<string, TEvent> map = new Dictionary<string, TEvent>();

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                TEvent evt = AddAndGetEvent(map, item);
                TDetail detail = GetDetailEntity(evt, item);

                if (detail.in_sign_foot == "1")
                {
                    evt.n1 = detail;
                }
                else
                {
                    evt.n2 = detail;
                }
            }

            foreach (var pair in map)
            {
                TEvent evt = pair.Value;
                TDetail n1 = evt.n1;
                TDetail n2 = evt.n2;

                if (evt.n1 == null || evt.n2 == null)
                {
                    evt.is_error = true;
                    continue;
                }

                BindStatus(evt);

                switch (evt.new_win_status)
                {
                    case "cancel":
                        evt.need_cancel = true;
                        break;

                    case "rollcall": //檢錄勝出
                    case "bypass":   //輪空勝出
                    case "fight":    //對戰勝出
                    case "nofight":  //不戰而勝
                    case "force":    //強制勝出
                        evt.need_commit = true;
                        break;
                    // //有勝出者，非該場次已勝出者資料 or 該場次無勝出者資料
                    // if (evt.new_win_sign_no != evt.in_win_sign_no || evt.in_win_status == "")
                    // {
                    //     evt.need_commit = true;
                    // }
                    // else
                    // {
                    //     evt.need_next = true;
                    // }
                    //break;

                    default:
                        //無勝出者，該場次卻已有勝出者資料
                        if (evt.in_win_status != "")
                        {
                            evt.need_rollback = true;
                        }
                        break;
                }
            }

            return map.Count > 0 ? map.First().Value : null;
        }

        /// <summary>
        /// 繫結狀態處理
        /// </summary>
        private void BindStatus(TEvent evt)
        {
            TDetail n1 = evt.n1;
            TDetail n2 = evt.n2;

            evt.new_win_status = "";
            evt.all_checked = n1.has_checked && n2.has_checked;
            evt.all_player = n1.is_player && n2.is_player;
            evt.all_cancel = n1.is_cancel && n2.is_cancel;
            if (!evt.all_cancel)
            {
                evt.all_cancel = n1.is_bypass && n2.is_bypass;
            }

            bool n1_win = false;
            bool n2_win = false;

            if (evt.all_cancel)
            {
                //雙方都取消
                evt.new_win_status = "cancel";
            }
            else if (n1.is_bypass && n2.is_cancel)
            {
                //一方輪空，一方取消
                evt.new_win_status = "cancel";
            }
            else if (n2.is_bypass && n1.is_cancel)
            {
                //一方輪空，一方取消
                evt.new_win_status = "cancel";
            }
            else if (n1.is_allowed && n2.is_cancel)
            {
                //籤腳 1 勝出
                evt.new_win_status = "nofight";
                n1_win = true;
            }
            else if (n2.is_allowed && n1.is_cancel)
            {
                //籤腳 2 勝出
                evt.new_win_status = "nofight";
                n2_win = true;
            }
            else if (n1.has_signed)
            {
                //籤腳 1 已簽出勝負
                evt.new_win_status = "fight";
                if (n1.is_win)
                {
                    n1_win = true;
                }
                else if (n1.is_lose)
                {
                    n2_win = true;
                }
            }
            else if (n2.has_signed)
            {
                //籤腳 2 已簽出勝負
                evt.new_win_status = "fight";
                if (n2.is_win)
                {
                    n2_win = true;
                }
                else if (n2.is_lose)
                {
                    n1_win = true;
                }
            }
            else if (evt.all_checked)
            {
                //雙方都已檢錄
                if (n1.is_allowed && !n2.is_allowed)
                {
                    //籤腳 1 勝出
                    evt.new_win_status = "rollcall";
                    n1_win = true;
                }
                else if (!n1.is_allowed && n2.is_allowed)
                {
                    //籤腳 2 勝出
                    evt.new_win_status = "rollcall";
                    n2_win = true;
                }
                else if (!n1.is_allowed && !n2.is_allowed)
                {
                    //雙方檢錄都不通過
                    evt.new_win_status = "cancel";
                }
                else
                {
                    //雙方檢錄都通過
                }
            }
            else if (n1.has_checked && n2.is_bypass)
            {
                if (n1.is_allowed)
                {
                    //籤腳 1 輪空勝出
                    evt.new_win_status = "bypass";
                    n1_win = true;
                }
                else
                {
                    //一方檢核不通過，一方輪空
                    evt.new_win_status = "cancel";
                }
            }
            else if (n2.has_checked && n1.is_bypass)
            {
                if (n2.is_allowed)
                {
                    //籤腳 2 勝出
                    evt.new_win_status = "bypass";
                    n2_win = true;
                }
                else
                {
                    //一方檢核不通過，一方輪空
                    evt.new_win_status = "cancel";
                }
            }
            else
            {
                //雙方都未檢錄
                if (n1.has_assigned && n2.is_bypass)
                {
                    //籤腳 1 輪空勝出
                    evt.new_win_status = "bypass";
                    n1_win = true;
                }
                else if (n2.has_assigned && n1.is_bypass)
                {
                    //籤腳 2 輪空勝出
                    evt.new_win_status = "bypass";
                    n2_win = true;
                }
                else if (n1.has_assigned && n2.is_cancel)
                {
                    //籤腳 1 不戰而勝
                    evt.new_win_status = "nofight";
                    n1_win = true;
                }
                else if (n2.has_assigned && n1.is_cancel)
                {
                    //籤腳 2 不戰而勝
                    evt.new_win_status = "nofight";
                    n2_win = true;
                }
            }

            //籤腳 1 勝出
            if (n1_win)
            {
                evt.new_win_sign_no = n1.in_sign_no;
                evt.winner = n1.value;
                evt.loser = n2.value;
            }
            //籤腳 2 勝出
            else if (n2_win)
            {
                evt.new_win_sign_no = n2.in_sign_no;
                evt.winner = n2.value;
                evt.loser = n1.value;
            }
        }


        /// <summary>
        /// 取得並附加場次資料
        /// </summary>
        private TEvent AddAndGetEvent(Dictionary<string, TEvent> dictionary, Item item)
        {
            string event_id = item.getProperty("event_id", "");
            TEvent entity = null;
            if (dictionary.ContainsKey(event_id))
            {
                entity = dictionary[event_id];
            }
            else
            {
                entity = new TEvent
                {
                    in_tree_name = item.getProperty("in_tree_name", ""),
                    in_tree_id = item.getProperty("in_tree_id", ""),
                    in_tree_no = item.getProperty("in_tree_no", ""),
                    in_round = item.getProperty("in_round", ""),
                    in_bypass_foot = item.getProperty("in_bypass_foot", ""),
                    in_win_status = item.getProperty("in_win_status", ""),
                    in_win_sign_no = item.getProperty("in_win_sign_no", ""),
                    value = item,
                };
                dictionary.Add(event_id, entity);
            }
            return entity;
        }

        /// <summary>
        /// 取得明細資料
        /// </summary>
        private TDetail GetDetailEntity(TEvent evt, Item item)
        {
            TDetail entity = new TDetail
            {
                in_sign_foot = item.getProperty("in_sign_foot", ""),
                in_sign_no = item.getProperty("in_sign_no", ""),
                in_sign_bypass = item.getProperty("in_sign_bypass", ""),
                in_sign_status = item.getProperty("in_sign_status", ""),
                in_sign_action = item.getProperty("in_sign_action", ""),

                in_target_status = item.getProperty("in_target_status", ""),

                in_status = item.getProperty("in_status", ""),

                in_check_result = item.getProperty("in_check_result", ""),
                in_weight_message = item.getProperty("in_weight_message", ""),

                value = item,
            };

            entity.has_assigned = entity.in_sign_no != "";

            entity.has_checked = entity.in_check_result != "";
            entity.is_allowed = entity.in_check_result == "1";


            entity.is_bypass = evt.in_bypass_foot == entity.in_sign_foot;
            if (entity.in_sign_bypass == "1")
            {
                entity.is_bypass = true;
            }

            entity.is_player = !entity.is_bypass;
            entity.is_cancel = entity.in_sign_status == "cancel";

            if (entity.in_weight_message != "")
            {
                //DQ、請假、未到 視為我方取消
                entity.is_cancel = true;
            }

            entity.has_signed = entity.in_sign_action != "";
            entity.is_win = entity.in_sign_action == "勝出";
            entity.is_lose = entity.in_sign_action == "失格";

            return entity;
        }

        /// <summary>
        /// 取得名次資訊
        /// </summary>
        private TRank GetRankInfo(string in_tree_rank, string in_tree_rank_ns, string in_tree_rank_nss)
        {
            bool need_update_rank = false;
            string[] ranks = null;
            string[] show_ranks_ = null;

            if (in_tree_rank.Contains("rank"))
            {
                if (in_tree_rank_ns == "")
                {
                    throw new Exception("決定名次的場次未設定名次資料");
                }

                need_update_rank = true;
                ranks = GetRankArray(in_tree_rank_ns, "0,0");
                show_ranks_ = GetRankArray(in_tree_rank_nss, "0,0");
            }
            else
            {
                ranks = new string[] { "", "" };
                show_ranks_ = new string[] { "", "" };
            }

            TRank result = new TRank
            {
                w_rank = ranks[0],
                l_rank = ranks[1],
                show_w_rank = show_ranks_[0],
                show_l_rank = show_ranks_[1],
                need_update = need_update_rank,
            };

            return result;
        }


        /// <summary>
        /// 取得組別資訊
        /// </summary>
        private Item GetProgram(TConfig cfg, string program_id)
        {
            string sql = @"
                SELECT 
                    t1.*
                    , t2.in_battle_repechage AS 'mt_battle_repechage'
                FROM 
                    IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
                    IN_MEETING t2 WITH(NOLOCK)
                    ON t2.id = t1.in_meeting
                WHERE 
                    t1.id = '{#program_id}'
            ";

            sql = sql.Replace("{#program_id}", program_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// 取得賽事組別場次明細
        /// </summary>
        private Item GetEventDetail(TConfig cfg, Item itmTeam)
        {
            string sql = @"
                SELECT
                    TOP 1
                    t2.id AS 'event_id'
                    , t1.*
                FROM
                    IN_MEETING_PEVENT_DETAIL t1 WITH(NOLOCK)
                INNER JOIN
                    IN_MEETING_PEVENT t2 WITH(NOLOCK)
                    ON t2.id = t1.source_id
                WHERE
                    t2.source_id = '{#program_id}'
                    AND t2.in_tree_name = N'main'
                    AND t2.in_round = 1
                    AND t1.in_sign_no = '{#in_sign_no}'
            ";

            string program_id = itmTeam.getProperty("source_id", "");
            string in_sign_no = itmTeam.getProperty("in_sign_no", "");

            sql = sql.Replace("{#program_id}", program_id)
                .Replace("{#in_sign_no}", in_sign_no);

            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// 取得場次
        /// </summary>
        private Item GetOneEvent(TConfig cfg, string program_id, string in_tree_id)
        {
            string sql = @"
                SELECT
                    TOP 1 t1.*
                FROM
                    IN_MEETING_PEVENT t1 WITH(NOLOCK)
                WHERE
                    t1.source_id = '{#program_id}'
                    AND t1.in_tree_id = N'{#in_tree_id}'
            ";

            sql = sql.Replace("{#program_id}", program_id)
                .Replace("{#in_tree_id}", in_tree_id);

            return cfg.inn.applySQL(sql);
        }

        //取得賽事組別場次明細
        private Item GetDetails(TConfig cfg, string event_id)
        {
            string sql = @"
                SELECT
                    t1.source_id AS 'program_id'
                    , t1.id      AS 'event_id'
                    , t1.in_tree_id
                    , t1.in_tree_no
                    , t1.in_tree_name
                    , t1.in_tree_rank
                    , t1.in_tree_rank_ns
                    , t1.in_tree_rank_nss
                    , t1.in_round
                    , t1.in_round_code
                    , t1.in_bypass_foot
                    , t1.in_win_time
                    , t1.in_win_status
                    , t1.in_win_sign_no
                    , t1.in_next_win
                    , t1.in_next_foot_win
                    , t1.in_next_lose
                    , t1.in_next_foot_lose
                    , t1.in_type
                    , t2.*
                    , t3.id AS 'team_id'
                    , t3.in_check_result
                    , t3.in_check_status
                    , t3.in_weight_message
                FROM 
                    IN_MEETING_PEVENT t1 WITH(NOLOCK) 
                INNER JOIN 
                    IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK) 
                    ON t2.source_id = t1.id 
                LEFT OUTER JOIN
                    IN_MEETING_PTEAM t3 WITH(NOLOCK)
                    ON t3.source_id = t1.source_id
                    AND t3.in_sign_no = t2.in_sign_no
                WHERE 
                    t1.id = '{#event_id}' 
                ORDER BY
                    t2.in_sign_foot
            ";

            sql = sql.Replace("{#event_id}", event_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// 取得自動晉級場次(主線第一回合)
        /// </summary>
        private Item GetByPassEventsMain(TConfig cfg, string program_id)
        {
            string sql = @"
                SELECT
                    t1.id AS 'event_id'
                    , t1.in_round_code
                    , t1.in_bypass_foot
                FROM
                    IN_MEETING_PEVENT t1 WITH(NOLOCK)
        
                WHERE
                    t1.source_id = '{#program_id}'
                    AND t1.in_tree_name = 'main'
                    AND t1.in_round = '1'
                    AND ISNULL(t1.in_bypass_foot, '') <> ''
                ORDER BY
                    t1.in_round_id
                ";

            sql = sql.Replace("{#program_id}", program_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        //未輪空但 DQ
        private Item GetDQEventsMain(TConfig cfg, string program_id)
        {
            string sql = @"
                SELECT 
                    t1.id AS 'event_id'
                    , t1.in_round_code
                    , t1.in_tree_id
                FROM
                	IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
                	IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK)
                	ON t2.source_id = t1.id
                INNER JOIN
                	VU_MEETING_PTEAM t3 WITH(NOLOCK)
                	ON t3.source_id = t1.source_id
                	AND t3.in_sign_no = t2.in_sign_no
                WHERE
                	t1.source_id = '{#program_id}'
                	AND t1.in_tree_name = N'main'
                	AND ISNULL(t1.in_win_status, '') = ''
                	AND ISNULL(t3.in_check_result, '') = '0'
                	AND ISNULL(t3.in_weight_message, '') <> ''
                ORDER BY
                	t1.in_tree_id
                	, t2.in_sign_foot
            ";

            sql = sql.Replace("{#program_id}", program_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// 取得自動晉級場次(復活表第一回合)
        /// </summary>
        private Item GetByPassEventsRpc(TConfig cfg, string program_id, string in_round)
        {
            string sql = @"
                SELECT
                    t1.id AS 'event_id'
                    , t1.in_round_code
                FROM
                    IN_MEETING_PEVENT t1 WITH(NOLOCK)
                WHERE
                    t1.source_id = '{#program_id}'
                    AND t1.in_tree_name = 'repechage'
                    AND t1.in_round = '{#in_round}'
                ORDER BY
                    t1.in_round_id
                ";

            sql = sql.Replace("{#program_id}", program_id)
                .Replace("{#in_round}", in_round);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// 取得準決賽之前的對手清單
        /// </summary>
        private Item GetFightersBeforeSemiFinal(TConfig cfg, string program_id, string in_sign_no)
        {
            string sql = @"
                SELECT
                    t1.id                   AS 'event_id'
                    , t1.in_tree_key
                    , t1.in_round
                    , t1.in_round_code
                    , t1.in_round_id
                    , t1.in_win_status
                    , t1.in_type
                    , t2.id                 AS 'detail_id'
                    , t2.in_sign_foot
                    , t2.in_sign_no
                    , t2.in_sign_status
                    , t3.id                 AS 'team_id'
                    , t3.in_current_org
                    , t3.in_short_org
                    , t3.in_name
                    , t3.in_sno
                    , t3.in_section_no
                FROM
                    IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
                    IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK)
                    ON t2.source_id = t1.id
                LEFT OUTER JOIN
                    IN_MEETING_PTEAM t3 WITH(NOLOCK)
                    ON t3.source_id = t1.source_id
                    AND t3.in_sign_no = t2.in_sign_no
                WHERE
                    t1.source_id = '{#program_id}'
                    AND t1.in_tree_name = N'main'
                    AND t1.in_round_code > 4
                    AND t2.in_target_no = '{#in_target_no}'
                ORDER BY
                    t1.in_round
            ";

            sql = sql.Replace("{#program_id}", program_id)
                .Replace("{#in_target_no}", in_sign_no);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// 取得八強的對手清單
        /// </summary>
        private Item GetFightersQuarterfinals(TConfig cfg, string program_id, string in_sign_no)
        {
            string sql = @"
                SELECT
                    t1.id                   AS 'event_id'
                    , t1.in_tree_key
                    , t1.in_round
                    , t1.in_round_code
                    , t1.in_round_id
                    , t1.in_win_status
                    , t1.in_type
                    , t2.id                 AS 'detail_id'
                    , t2.in_sign_foot
                    , t2.in_sign_no
                    , t2.in_sign_status
                    , t3.id                 AS 'team_id'
                    , t3.in_current_org
                    , t3.in_short_org
                    , t3.in_name
                    , t3.in_sno
                    , t3.in_section_no
                FROM
                    IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
                    IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK)
                    ON t2.source_id = t1.id
                LEFT OUTER JOIN
                    IN_MEETING_PTEAM t3 WITH(NOLOCK)
                    ON t3.source_id = t1.source_id
                    AND t3.in_sign_no = t2.in_sign_no
                WHERE
                    t1.source_id = '{#program_id}'
                    AND t1.in_tree_name = N'main'
                    AND t1.in_round_code = 8
                    AND t2.in_target_no = '{#in_target_no}'
                ORDER BY
                    t1.in_round
            ";

            sql = sql.Replace("{#program_id}", program_id)
                .Replace("{#in_target_no}", in_sign_no);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// 取得 Detail
        /// </summary>
        private Item GetDetail(TConfig cfg, string detail_id)
        {
            string sql = @"
                SELECT
                    t1.*
                    , t2.id             AS 'event_id'
                    , t2.in_tree_name
                    , t2.in_tree_id
                    , t2.in_tree_no
                    , t2.in_round_code
                    , t2.in_round_id
                    , t2.in_type
                    , t3.in_meeting     AS 'meeting_id'
                    , t3.id             AS 'program_id'
                    , t3.in_battle_type AS 'program_battle_type'
                    , t3.in_rank_type   AS 'program_rank_type'
                    , t3.in_team_count  AS 'program_team_count'
                    , t3.in_fight_time  AS 'program_fight_time'
                    , t4.id             AS 'parent_program'
                    , t4.in_battle_type AS 'parent_battle_type'
                    , t4.in_group_rank  AS 'parent_group_rank'
                    , t4.in_tiebreaker  AS 'parent_tiebreakere'
                FROM
                    IN_MEETING_PEVENT_DETAIL t1 WITH(NOLOCK)
                INNER JOIN
                    IN_MEETING_PEVENT t2 WITH(NOLOCK)
                    ON t2.id = t1.source_id
                INNER JOIN
                    IN_MEETING_PROGRAM t3 WITH(NOLOCK)
                    ON t3.id = t2.source_id
                LEFT OUTER JOIN
                    IN_MEETING_PROGRAM t4 WITH(NOLOCK)
                    ON t4.id = t3.in_program
                WHERE
                    t1.id = '{#detail_id}'
            ";

            sql = sql.Replace("{#detail_id}", detail_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// 取得未進行的場次數量
        /// </summary>
        private Item GetEventCount(TConfig cfg, string program_id, bool is_parent = false)
        {
            string program_condition = is_parent ? "t2.in_program = '{#program_id}'" : "t2.id = '{#program_id}'";
            program_condition = program_condition.Replace("{#program_id}", program_id);

            string sql = @"
                SELECT
                    count(t1.id) AS 'event_count'
                FROM 
                    IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
                    IN_MEETING_PROGRAM t2 WITH(NOLOCK)
                    ON t2.id = t1.source_id
                WHERE
                    {#program_condition}
                    AND ISNULL(t1.in_win_status, '') = ''
            ";

            sql = sql.Replace("{#program_condition}", program_condition);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        //取得賽事組別場次
        private Item GetEventPlayers(TConfig cfg, string program_id)
        {
            string sql = @"
                SELECT
                    t1.id AS 'event_id'
                    , t1.in_tree_name
                    , t1.in_tree_id
                    , t1.in_tree_no
                    , t1.in_round
                    , t1.in_round_id
                    , t1.in_round_code
                    , t1.in_surface_id
                    , t1.in_surface_name
                    , t1.in_site_code
                    , t1.in_site_no
                    , t1.in_site_id
                    , t1.in_site_allocate
                    , t1.in_next_win
                    , t1.in_next_foot_win
                    , t1.in_next_lose
                    , t1.in_next_foot_lose
                    , t1.in_detail_ns
                    , t1.in_win_sign_no
                    , t2.id AS 'detail_id'
                    , t2.in_sign_foot
                    , t2.in_sign_no
                    , t2.in_sign_bypass
                    , t2.in_target_no
                    , t2.in_status
                    , t2.in_points
                    , t2.in_points_type
                    , t3.id AS 'team_id'
                    , t3.in_current_org
                    , t3.in_short_org
                    , t3.map_short_org
                    , t3.in_name
                    , t3.in_names
                    , t3.in_sno
                    , t3.in_section_no
                    , t3.in_org_teams
                    , t3.in_seeds
                    , t3.in_check_result
                FROM 
                    IN_MEETING_PEVENT t1 WITH(NOLOCK)
                LEFT OUTER JOIN
                    IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK)
                    ON t2.source_id = t1.id
                LEFT OUTER JOIN
                    VU_MEETING_PTEAM t3 WITH(NOLOCK)
                    ON t3.source_id = t1.source_id
                    AND ISNULL(t3.in_sign_no, '') <> ''
                    AND t3.in_sign_no = t2.in_sign_no
                WHERE
                    t1.source_id = '{#program_id}'
                ORDER BY
                    t1.in_tree_name
                    , t1.in_round
                    , t1.in_round_id
                    , t2.in_sign_foot
            ";

            sql = sql.Replace("{#program_id}", program_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        private class TStatistics
        {
            /// <summary>
            /// 籤號
            /// </summary>
            public string in_sign_no { get; set; }

            /// <summary>
            /// 總積分
            /// </summary>
            public int total_points { get; set; }

            /// <summary>
            /// 總勝場
            /// </summary>
            public int total_wins { get; set; }

            /// <summary>
            /// 排名
            /// </summary>
            public int rank { get; set; }
        }


        /// <summary>
        /// 統計隊伍積分與勝場
        /// </summary>
        private Dictionary<string, TStatistics> MapTeam(Item itmEvents)
        {
            Dictionary<string, TStatistics> map = new Dictionary<string, TStatistics>();
            int count = itmEvents.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item itmEvent = itmEvents.getItemByIndex(i);
                string in_sign_no = itmEvent.getProperty("in_sign_no", "");
                string in_status = itmEvent.getProperty("in_status", "");
                string in_points = itmEvent.getProperty("in_points", "");
                int points = GetIntVal(in_points);

                TStatistics entity = null;
                if (map.ContainsKey(in_sign_no))
                {
                    entity = map[in_sign_no];
                }
                else
                {
                    entity = new TStatistics
                    {
                        in_sign_no = in_sign_no,
                        total_points = 0,
                        total_wins = 0,
                    };
                    map.Add(in_sign_no, entity);
                }

                entity.total_points += points;

                if (in_status == "1")
                {
                    entity.total_wins++;
                }

            }
            return map;
        }

        #region 資料結構

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public Innosoft.InnovatorHelper InnH { get; set; }

            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }

            public string meeting_id { get; set; }
            public string mode { get; set; }

            public Item itmLogin { get; set; }
        }

        /// <summary>
        /// 對戰場次資料模型
        /// </summary>
        private class TEvent
        {
            /// <summary>
            /// 樹圖名稱
            /// </summary>
            public string in_tree_name { get; set; }

            /// <summary>
            /// 樹圖序號
            /// </summary>
            public string in_tree_id { get; set; }

            /// <summary>
            /// 樹圖序號
            /// </summary>
            public string in_tree_no { get; set; }

            /// <summary>
            /// 回合
            /// </summary>
            public string in_round { get; set; }

            /// <summary>
            /// 輪空籤腳
            /// </summary>
            public string in_bypass_foot { get; set; }

            /// <summary>
            /// 勝出狀態 (bypass: 輪空勝出、rollcall: 檢錄勝出、fight: 對戰勝出、nofight: 不戰而勝、force: 強制勝出、cancel: 取消)
            /// </summary>
            public string in_win_status { get; set; }

            /// <summary>
            /// 勝出籤號
            /// </summary>
            public string in_win_sign_no { get; set; }

            /// <summary>
            /// 籤腳 1 資料
            /// </summary>
            public TDetail n1 { get; set; }

            /// <summary>
            /// 籤腳 2 資料
            /// </summary>
            public TDetail n2 { get; set; }

            /// <summary>
            /// 雙方都是真人
            /// </summary>
            public bool all_player { get; set; }

            /// <summary>
            /// 雙方都已檢錄
            /// </summary>
            public bool all_checked { get; set; }

            /// <summary>
            /// 雙方取消
            /// </summary>
            public bool all_cancel { get; set; }

            /// <summary>
            /// 當前勝出狀態 (bypass: 輪空勝出、rollcall: 檢錄勝出、fight: 對戰勝出、nofight: 不戰而勝、force: 強制勝出、cancel: 取消)
            /// </summary>
            public string new_win_status { get; set; }

            /// <summary>
            /// 當前勝出籤號
            /// </summary>
            public string new_win_sign_no { get; set; }

            /// <summary>
            /// 需提交成績
            /// </summary>
            public bool need_cancel { get; set; }

            /// <summary>
            /// 需返還
            /// </summary>
            public bool need_rollback { get; set; }

            /// <summary>
            /// 需提交成績
            /// </summary>
            public bool need_commit { get; set; }

            /// <summary>
            /// 需進行下一場
            /// </summary>
            public bool need_next { get; set; }

            /// <summary>
            /// 異常
            /// </summary>
            public bool is_error { get; set; }

            /// <summary>
            /// event 資料
            /// </summary>
            public Item value { get; set; }

            /// <summary>
            /// 勝者
            /// </summary>
            public Item winner { get; set; }

            /// <summary>
            /// 敗者
            /// </summary>
            public Item loser { get; set; }
        }

        /// <summary>
        /// 對戰場次明細資料模型
        /// </summary>
        private class TDetail
        {
            /// <summary>
            /// 我方籤腳
            /// </summary>
            public string in_sign_foot { get; set; }

            /// <summary>
            /// 我方籤號
            /// </summary>
            public string in_sign_no { get; set; }

            /// <summary>
            /// 我方輪空
            /// </summary>
            public string in_sign_bypass { get; set; }

            /// <summary>
            /// 我方狀態
            /// </summary>
            public string in_sign_status { get; set; }

            /// <summary>
            /// 我方行為
            /// </summary>
            public string in_sign_action { get; set; }

            /// <summary>
            /// 檢錄結果
            /// </summary>
            public string in_check_result { get; set; }

            /// <summary>
            /// 過磅訊息
            /// </summary>
            public string in_weight_message { get; set; }

            /// <summary>
            /// 對手狀態
            /// </summary>
            public string in_target_status { get; set; }

            /// <summary>
            /// 勝敗狀態
            /// </summary>
            public string in_status { get; set; }

            /// <summary>
            /// 原始資料
            /// </summary>
            public Item value { get; set; }

            /// <summary>
            /// 是否已配置籤號
            /// </summary>
            public bool has_assigned { get; set; }

            /// <summary>
            /// 是否已檢錄
            /// </summary>
            public bool has_checked { get; set; }

            /// <summary>
            /// 檢錄通過
            /// </summary>
            public bool is_allowed { get; set; }

            /// <summary>
            /// 這是輪空的籤
            /// </summary>
            public bool is_bypass { get; set; }

            /// <summary>
            /// 真人選手
            /// </summary>
            public bool is_player { get; set; }

            /// <summary>
            /// 對手已取消
            /// </summary>
            public bool is_cancel { get; set; }

            /// <summary>
            /// 已簽出勝負
            /// </summary>
            public bool has_signed { get; set; }

            /// <summary>
            /// 我方勝出
            /// </summary>
            public bool is_win { get; set; }

            /// <summary>
            /// 我方敗出
            /// </summary>
            public bool is_lose { get; set; }
        }

        private class TDetailEdit
        {
            /// <summary>
            /// 籤號
            /// </summary>
            public string sign_no { get; set; }

            /// <summary>
            /// 當前名次
            /// </summary>
            public string rank { get; set; }

            /// <summary>
            /// 場次編號
            /// </summary>
            public string event_id { get; set; }

            /// <summary>
            /// 腳位
            /// </summary>
            public string sign_foot { get; set; }

            /// <summary>
            /// 對位
            /// </summary>
            public string target_foot { get; set; }

        }

        private class TRank
        {
            public string w_rank { get; set; }

            public string l_rank { get; set; }

            public string show_w_rank { get; set; }

            public string show_l_rank { get; set; }

            public bool need_update { get; set; }
        }

        private class TLink
        {
            public string id { get; set; }
            public string foot { get; set; }
        }

        /// <summary>
        /// 登錄成績資料模型
        /// </summary>
        private class TScore
        {
            //外部輸入欄位

            /// <summary>
            /// 賽事 id
            /// </summary>
            public string meeting_id { get; set; }

            /// <summary>
            /// 組別 id
            /// </summary>
            public string program_id { get; set; }

            /// <summary>
            /// 場次 id
            /// </summary>
            public string event_id { get; set; }

            /// <summary>
            /// 我方 id
            /// </summary>
            public string detail_id { get; set; }

            /// <summary>
            /// 積分類型(外部輸入)
            /// </summary>
            public string points_type { get; set; }

            /// <summary>
            /// 積分內文(外部輸入)
            /// </summary>
            public string points_text { get; set; }

            /// <summary>
            /// 對方 id
            /// </summary>
            public string target_detail_id { get; set; }

            /// <summary>
            /// 檢錄sno
            /// </summary>
            public string equipment_sno { get; set; }

            /// <summary>
            /// 隊伍 or 姓名
            /// </summary>
            public string team_name { get; set; }

            //存取資料

            /// <summary>
            /// 資料來源
            /// </summary>
            public Item Value { get; set; }

            /// <summary>
            /// 資料來源(對手)
            /// </summary>
            public Item TargetValue { get; set; }


            // 主組別

            /// <summary>
            /// 主組別 id
            /// </summary>
            public string parent_program { get; set; }

            /// <summary>
            /// 主組別 - 賽制類型
            /// </summary>
            public string parent_battle_type { get; set; }

            /// <summary>
            /// 主組別 - 取名次類型
            /// </summary>
            public string parent_group_rank { get; set; }

            /// <summary>
            /// 主組別 - 加賽類型
            /// </summary>
            public string parent_tiebreakere { get; set; }


            //該組別

            /// <summary>
            /// 賽制類型
            /// </summary>
            public string pg_battle_type { get; set; }

            /// <summary>
            /// 組別人數/隊數
            /// </summary>
            public string pg_team_count { get; set; }

            /// <summary>
            /// 比賽時間
            /// </summary>
            public string pg_fight_time { get; set; }

            /// <summary>
            /// 名次類型
            /// </summary>
            public string pg_rank_type { get; set; }

            /// <summary>
            /// 組別人數/隊數
            /// </summary>
            public int team_count { get; set; }

            /// <summary>
            /// 比賽時間
            /// </summary>
            public int fight_time { get; set; }

            /// <summary>
            /// 賽制資料
            /// </summary>
            public TBattleMap BattleMap { get; set; }

            /// <summary>
            /// 樹圖名稱
            /// </summary>
            public string in_tree_name { get; set; }

            /// <summary>
            /// 樹圖 id
            /// </summary>
            public string in_tree_id { get; set; }

            /// <summary>
            /// 回合編號
            /// </summary>
            public string in_round_code { get; set; }

            /// <summary>
            /// 籤號
            /// </summary>
            public string in_sign_no { get; set; }

            /// <summary>
            /// 回合編號
            /// </summary>
            public int round_code { get; set; }

            // 更新欄位

            /// <summary>
            /// 我方指導次數
            /// </summary>
            public int old_correct_count { get; set; }

            /// <summary>
            /// 我方指導次數
            /// </summary>
            public int new_correct_count { get; set; }

            /// <summary>
            /// 我方積分(數值)
            /// </summary>
            public int old_points { get; set; }

            /// <summary>
            /// 我方積分
            /// </summary>
            public string in_points { get; set; }

            /// <summary>
            /// 積分類型代碼
            /// </summary>
            public string in_points_type { get; set; }

            /// <summary>
            /// 積分類型
            /// </summary>
            public string in_points_text { get; set; }

            /// <summary>
            /// 勝敗狀態(1: 勝出、0: 敗)
            /// </summary>
            public string status { get; set; }

            /// <summary>
            /// 備註
            /// </summary>
            public string in_note { get; set; }

            //判斷欄位

            /// <summary>
            /// 狀態
            /// </summary>
            public StatusEnum status_enum { get; set; }

            /// <summary>
            /// 是否為勝部
            /// </summary>
            public bool is_main { get; set; }

            /// <summary>
            /// 是否為敗部
            /// </summary>
            public bool is_rpc { get; set; }

            /// <summary>
            /// 是否八強賽(四柱復活賽連結)
            /// </summary>
            public bool is_main_eight { get; set; }

            /// <summary>
            /// 是否準決賽(四柱復活賽連結)
            /// </summary>
            public bool is_main_semi { get; set; }

            /// <summary>
            /// 是否決賽(挑戰賽連結)
            /// </summary>
            public bool is_main_final { get; set; }

            /// <summary>
            /// 是否四柱復活賽決賽(挑戰賽連結)
            /// </summary>
            public bool is_rpc_final { get; set; }

            /// <summary>
            /// 是否挑戰賽 A 前賽
            /// </summary>
            public bool is_ca01 { get; set; }

            /// <summary>
            /// 是否挑戰賽 A 決賽
            /// </summary>
            public bool is_ca02 { get; set; }

            /// <summary>
            /// 是否挑戰賽 B 決賽
            /// </summary>
            public bool is_cb01 { get; set; }

            /// <summary>
            /// 指導滿三次，對方勝出
            /// </summary>
            public string correct_msg { get; set; }
        }

        /// <summary>
        /// 取得賽制類型
        /// </summary>
        private TBattleMap GetBattleMap(string value, int tcount)
        {
            switch (value)
            {
                case "Format": return new TBattleMap { type = BattleEnum.Format };

                case "TopTwo": return new TBattleMap { type = BattleEnum.TopTwo };
                case "TopFour": return new TBattleMap { type = BattleEnum.TopFour };

                case "JudoTopFour": return new TBattleMap { type = BattleEnum.JudoTopFour, isRepechage = true };
                case "Challenge": return new TBattleMap { type = BattleEnum.Challenge, isChallenge = true };

                case "SingleRoundRobin": return new TBattleMap { type = BattleEnum.SingleRoundRobin, isRobin = true, isTwoFight = tcount == 2 };
                case "DoubleRoundRobin": return new TBattleMap { type = BattleEnum.DoubleRoundRobin, isRobin = true, isTwoFight = tcount == 2 };
                case "GroupSRoundRobin": return new TBattleMap { type = BattleEnum.GroupSRoundRobin, isRobin = true, isTwoFight = tcount == 2 };
                case "GroupDRoundRobin": return new TBattleMap { type = BattleEnum.GroupDRoundRobin, isRobin = true, isTwoFight = tcount == 2 };

                case "GroupTopTwo": return new TBattleMap { type = BattleEnum.GroupTopTwo };
                case "Olympics": return new TBattleMap { type = BattleEnum.Olympics };
                default: return new TBattleMap { type = BattleEnum.None };
            }
        }

        private class TBattleMap
        {
            /// <summary>
            /// 賽制類型
            /// </summary>
            public BattleEnum type { get; set; }

            /// <summary>
            /// 是否為四柱復活賽
            /// </summary>
            public bool isRepechage { get; set; }

            /// <summary>
            /// 是否為循環賽
            /// </summary>
            public bool isRobin { get; set; }

            /// <summary>
            /// 是否為挑戰賽
            /// </summary>
            public bool isChallenge { get; set; }

            /// <summary>
            /// 是否八強賽(四柱復活挑戰賽-八強進復活)
            /// </summary>
            public bool isEight { get; set; }

            /// <summary>
            ///三戰兩勝
            /// </summary>
            public bool isTwoFight { get; set; }
        }

        /// <summary>
        /// 賽制類型
        /// </summary>
        private enum BattleEnum
        {
            /// <summary>
            /// 未設定
            /// </summary>
            None = 0,
            /// <summary>
            /// 格式
            /// </summary>
            Format = 100,
            /// <summary>
            /// 單淘
            /// </summary>
            TopTwo = 1100,
            /// <summary>
            /// 雙淘
            /// </summary>
            TopFour = 1200,
            /// <summary>
            /// 四柱復活賽
            /// </summary>
            JudoTopFour = 2100,
            /// <summary>
            /// 挑戰賽(四柱復活挑戰賽)
            /// </summary>
            Challenge = 2200,
            /// <summary>
            /// 單循環
            /// </summary>
            SingleRoundRobin = 3100,
            /// <summary>
            /// 雙循環
            /// </summary>
            DoubleRoundRobin = 3200,
            /// <summary>
            /// 分組單循環
            /// </summary>
            GroupSRoundRobin = 4100,
            /// <summary>
            /// 分組雙循環
            /// </summary>
            GroupDRoundRobin = 4200,
            /// <summary>
            /// 分組單淘
            /// </summary>
            GroupTopTwo = 4300,
            /// <summary>
            /// 奧運賽制
            /// </summary>
            Olympics = 900,
        }

        private enum StatusEnum
        {
            /// <summary>
            /// 未設定
            /// </summary>
            None = 0,
            /// <summary>
            /// 歸零
            /// </summary>
            Initial = 1,
            /// <summary>
            /// 失格
            /// </summary>
            DQ = 2,
            /// <summary>
            /// 指導滿次數而落敗
            /// </summary>
            Correct = 3,
            /// <summary>
            /// 勝出
            /// </summary>
            Win = 100,
        }

        #endregion 資料結構

        /// <summary>
        /// 取得名次陣列
        /// </summary>
        private string[] GetRankArray(string value, string ranks)
        {
            return string.IsNullOrEmpty(value)
                ? ranks.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                : value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
        }

        private decimal GetDcmVal(string value)
        {
            if (value == "") return 0;

            decimal result = 0;
            decimal.TryParse(value, out result);
            return result;
        }

        private int GetIntVal(string value, int def = 0)
        {
            int result = def;
            Int32.TryParse(value, out result);
            return result;
        }
    }
}