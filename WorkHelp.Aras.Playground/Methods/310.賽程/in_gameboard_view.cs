﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class in_gameboard_view : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 競賽看板
                日期: 2020/12/30: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]in_gameboard_view";

            Item itmR = this;

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                inn = inn,

                meeting_id = itmR.getProperty("meeting_id", ""),
                in_date = itmR.getProperty("in_date", ""),
                site_ids = itmR.getProperty("site_ids", ""),
                scene = itmR.getProperty("scene", ""),
                delay = itmR.getProperty("delay", ""),

                in_date_key = "",
                in_date_type = "",
                TopCount = 4 * 2,
            };

            cfg.itmMeeting = GetMeeting(cfg);
            cfg.Days = GetDays(cfg);
            cfg.Sites = GetSites(cfg);

            itmR.setProperty("in_title", cfg.itmMeeting.getProperty("in_title", ""));

            if (cfg.Days != null && cfg.Days.Count > 0)
            {
                //預設日期選項
                if (cfg.in_date == "")
                {
                    var search = cfg.Days.Find(x => x.is_selected);
                    cfg.in_date = search != null ? search.value : cfg.Days[0].value;
                }

                string[] arr = cfg.in_date.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
                if (arr != null)
                {
                    cfg.in_date_key = arr[0];
                    cfg.in_date_type = arr.Length > 1 ? arr[1] : "";
                }

                //附加日期選單
                AppendDayMenu(cfg, itmR);

                //附加場地選單 (modal 內需要)
                AppendSiteMenu(cfg, itmR);

                switch (cfg.scene)
                {
                    case "next"://準備區
                        itmR.setProperty("inn_date_title", "<span class='gameboard_title'>準備區</span> - 比賽日期：" + cfg.in_date_key);
                        ShowNextEvents(cfg, itmR);
                        break;

                    case "rtm"://最新成績
                        itmR.setProperty("inn_date_title", "<span class='gameboard_title'>最新成績</span> - 比賽日期：" + cfg.in_date_key);
                        ShowRTMEvents(cfg, itmR);
                        break;

                    case "all"://全部場次
                        itmR.setProperty("inn_date_title", "<span class='gameboard_title'>全部場次</span> - 比賽日期：" + cfg.in_date_key);
                        ShowALLEvents(cfg, itmR);
                        break;

                    default:
                        break;
                }
            }


            return itmR;
        }

        #region 準備區

        //準備區
        private void ShowNextEvents(TConfig cfg, Item itmR)
        {
            foreach (var site in cfg.Sites)
            {
                Item itmSiteEvents = GetNextSiteEvents2(cfg, site);
                MapEvents4(cfg, itmSiteEvents);
            }

            FixSiteNodes(cfg);

            AppendNextSiteSlide(cfg, itmR);
        }

        private void AppendNextSiteSlide(TConfig cfg, Item itmReturn)
        {
            StringBuilder builder = new StringBuilder();

            for (int i = 0; i < cfg.Sites.Count; i++)
            {
                TSite site = cfg.Sites[i];
                AppendNextSlideContainer(cfg, builder, site);
            }

            itmReturn.setProperty("inn_slides", builder.ToString());
        }

        private void AppendNextSlideContainer(TConfig cfg, StringBuilder builder, TSite site)
        {
            builder.Append("<div class=' swiper-slide'>");
            builder.Append("  <div class='icat-bp'>");


            builder.Append("    <div class='box box-widget widget-user-2' style='padding-top: 3px; padding-bottom: 3px;'>");
            //場地標題
            builder.Append("      <div class='widget-user-header' style='padding-top: 3px; padding-bottom: 3px; padding-left: 15px;'>");
            builder.Append("        <h3 class='match site_title' >" + site.Name + "</h3>");
            builder.Append("      </div>");
            //比賽組
            builder.Append("      <div class='box-footer no-padding competition'>");
            builder.Append("        <h4 class='game-group'><span class='visible-sm visible-xs pull-right gaming'>下一場</span></h4>");

            var top_evt = site.TopOne;
            var top_evt_f1 = top_evt.Foot1;

            builder.Append("<div class='row text-center'>");
            builder.Append(top_evt_f1.ProgramName);
            builder.Append("</div>");
            builder.Append("<div class='row text-center'>");
            builder.Append(GetLink(top_evt_f1.ProgramId, top_evt_f1.TreeId, top_evt_f1.EventName, top_evt_f1.TreeNo));
            builder.Append("</div>");

            builder.Append("        <div class='col-xs-12 gameboard-site text-center' style='justify-content: center;'>");
            AppendPlayerBoxRTM(builder, cfg, site, top_evt, is_top: true);
            builder.Append("        </div>");
            builder.Append("      </div>");
            builder.Append("    </div>");

            //預備區
            builder.Append("    <div class='box box-widget widget-user-2 border-0'>");
            builder.Append("      <div class='box-footer no-padding reserved'>");
            builder.Append("        <h4 class='game-group'><span class='visible-sm visible-xs pull-right perp'>預備區</span></h4>");
            builder.Append("        <div style='overflow-y: scroll; height: 450px;'>");

            int no = 1;
            foreach (var evt in site.Nodes)
            {
                if (no > 1)
                {
                    builder.Append("<hr  style='border: 0; height: 1px; background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0));'>");
                }
                AppendPlayerBoxRTM(builder, cfg, site, evt);
                no++;
            }

            builder.Append("        </div>");
            builder.Append("      </div>");
            builder.Append("    </div>");


            builder.Append("  </div>");
            builder.Append("</div>");
        }
        #endregion 準備區

        #region 最新成績

        private void ShowRTMEvents(TConfig cfg, Item itmR)
        {
            //各場地最新比出成績的場次
            Item itmLastFightEvents = GetLastFightEvents(cfg);
            string site_event_keys = GetEventIds(cfg, itmLastFightEvents);
            if (site_event_keys != "")
            {
                Item itmLastEvents = GetTargetEvents(cfg, site_event_keys);
                MapNewestEvents(cfg, itmLastEvents);
            }
            foreach (var site in cfg.Sites)
            {
                Item itmSiteEvents = GetRTMSiteEvents(cfg, site);
                MapEvents1(cfg, itmSiteEvents);
            }

            FixSiteNodes(cfg);

            AppendNextSiteSlide(cfg, itmR);
        }

        #endregion 最新成績

        //全部場次
        private void ShowALLEvents(TConfig cfg, Item itmR)
        {
            Item itmEvents = GetEvents(cfg);

            MapEvents2(cfg, itmEvents);

            AppendSiteSlide(cfg, itmR);
        }

        //附加場地選單
        private void AppendSiteMenu(TConfig cfg, Item itmReturn)
        {
            Item itmEmpty = cfg.inn.newItem();
            itmEmpty.setType("inn_site");
            itmEmpty.setProperty("value", "");
            itmEmpty.setProperty("text", "設定場地");
            itmEmpty.setProperty("selected", "selected");
            itmReturn.addRelationship(itmEmpty);

            for (int i = 0; i < cfg.Sites.Count; i++)
            {
                var site = cfg.Sites[i];

                Item item = cfg.inn.newItem();
                item.setType("inn_site");
                item.setProperty("value", site.Id);
                item.setProperty("text", site.Name);
                item.setProperty("selected", "");
                itmReturn.addRelationship(item);
            }
        }

        //附加日期選單
        private void AppendDayMenu(TConfig cfg, Item itmReturn)
        {
            for (int i = 0; i < cfg.Days.Count; i++)
            {
                var day = cfg.Days[i];
                var selected = day.value == cfg.in_date ? "selected" : "";

                var itmDay = cfg.inn.newItem();
                itmDay.setType("inn_date");
                itmDay.setProperty("value", day.value);
                itmDay.setProperty("label", day.label);
                itmDay.setProperty("selected", selected);
                itmReturn.addRelationship(itmDay);
            }
        }

        #region 即時場次
        private void AppendNWSiteSlide(TConfig cfg, Item itmReturn)
        {
            StringBuilder builder = new StringBuilder();

            for (int i = 0; i < cfg.Sites.Count; i++)
            {
                TSite site = cfg.Sites[i];
                AppendNWSlideContainer(cfg, builder, site);
            }

            itmReturn.setProperty("inn_slides", builder.ToString());
        }

        private void AppendNWSlideContainer(TConfig cfg, StringBuilder builder, TSite site)
        {
            builder.Append("<div class=' swiper-slide'>");
            builder.Append("  <div class='icat-bp'>");


            builder.Append("    <div class='box box-widget widget-user-2' style='padding-top: 3px; padding-bottom: 3px;'>");
            //場地標題
            builder.Append("      <div class='widget-user-header' style='padding-top: 3px;padding-bottom: 3px;padding-left: 15px;'>");
            builder.Append("        <h3 class='match site_title'>" + site.Name + "</h3>");
            builder.Append("      </div>");
            //比賽組
            builder.Append("      <div class='box-footer no-padding competition'>");
            builder.Append("        <h4 class='game-group'><span class='visible-sm visible-xs pull-right gaming'>最新成績</span></h4>");

            var nevt_f1 = site.Newest.Foot1;
            builder.Append("<div class='row text-center'>");
            builder.Append(nevt_f1.ProgramName);
            builder.Append("</div>");
            builder.Append("<div class='row text-center'>");
            builder.Append(GetLink(nevt_f1.ProgramId, nevt_f1.TreeId, nevt_f1.EventName, nevt_f1.TreeNo));
            builder.Append("</div>");

            builder.Append("        <div class='col-xs-12 gameboard-site text-center' style='justify-content: center;'>");
            AppendPlayerBoxRTM(builder, cfg, site, site.Newest);
            builder.Append("        </div>");
            builder.Append("      </div>");
            builder.Append("    </div>");

            //預備區
            builder.Append("    <div class='box box-widget widget-user-2 border-0'>");
            builder.Append("      <div class='box-footer no-padding reserved'>");
            builder.Append("        <h4 class='game-group'><span class='visible-sm visible-xs pull-right perp'>預備區</span></h4>");
            builder.Append("        <div style='overflow-y: scroll; height: 450px;'>");

            int no = 1;
            foreach (var evt in site.Nodes)
            {
                if (no > 1)
                {
                    builder.Append("<hr  style='border: 0; height: 1px; background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0));'>");
                }
                AppendPlayerBoxRTM(builder, cfg, site, evt);
                no++;
            }

            builder.Append("        </div>");
            builder.Append("      </div>");
            builder.Append("    </div>");


            builder.Append("  </div>");
            builder.Append("</div>");
        }

        private void AppendPlayerBoxRTM(StringBuilder body, TConfig cfg, TSite site, TEvent evt, bool is_top = false)
        {
            if (is_top)
            {
                body.Append("    <div class='waitMatchup' data-aos='fade-right' data-aos-duration='1000' data-aos-delay='800' data-aos-offset='-100' data-aos-once='true'>");

                AppendMatch(body, evt, evt.Foot1);

                body.Append("  <div class='part-vs'>");
                body.Append("    <lable>VS</label>");
                body.Append("  </div>");

                AppendMatch(body, evt, evt.Foot2);

                body.Append("    </div>");
            }
            else
            {
                // body.Append("<div class='row text-center'>");
                // body.Append(evt.Foot1.ProgramName);
                // body.Append("</div>");
                // body.Append("<div class='row text-center'>");
                // body.Append(GetLink(evt.Foot1.ProgramId, evt.Foot1.TreeId, evt.Foot1.EventName, evt.Foot1.TreeNo));
                // body.Append("</div>");


                // body.Append("    <div class='waitMatchup' data-aos='fade-right' data-aos-duration='1000' data-aos-delay='800' data-aos-offset='-100' data-aos-once='true'>");

                // AppendMatch(body, evt, evt.Foot1);

                // body.Append("  <div class='part-vs'>");
                // body.Append("    <lable>VS</label>");
                // body.Append("  </div>");

                // AppendMatch(body, evt, evt.Foot2);

                // body.Append("    </div>");


                body.Append("<div class='row text-center'>");
                body.Append(evt.Foot1.ProgramName);
                body.Append("</div>");


                body.Append("    <div class='waitMatchup' data-aos='fade-right' data-aos-duration='1000' data-aos-delay='800' data-aos-offset='-100' data-aos-once='true'>");

                AppendMatch(body, evt, evt.Foot1);

                body.Append("<div class='part-vs'>");
                body.Append(GetLink(evt.Foot1.ProgramId, evt.Foot1.TreeId, evt.Foot1.EventName, evt.Foot1.TreeNo));
                body.Append("</div>");

                AppendMatch(body, evt, evt.Foot2);

                body.Append("    </div>");
            }
        }

        #endregion 即時場次

        #region 全部場次
        private void AppendSiteSlide(TConfig cfg, Item itmReturn)
        {
            StringBuilder builder = new StringBuilder();

            for (int i = 0; i < cfg.Sites.Count; i++)
            {
                TSite site = cfg.Sites[i];
                AppendSlideContainer(builder, site);
            }

            itmReturn.setProperty("inn_slides", builder.ToString());
        }

        private void AppendSlideContainer(StringBuilder builder, TSite site)
        {
            StringBuilder body = new StringBuilder();
            for (int i = 0; i < site.Nodes.Count; i++)
            {
                var evt = site.Nodes[i];
                AppendMatchBox(body, evt, evt.Foot1, evt.Foot2);
            }

            builder.Append("<div class=' swiper-slide'>");
            builder.Append("  <div class='icat-bp'>");
            builder.Append("    <div class='box box-widget widget-user-2 border-0'>");
            builder.Append("      <div class='box-footer no-padding reserved'>");
            //builder.Append("        <h4 class='game-group'>第 " + site.Key + " 場地<span class='visible-sm visible-xs pull-right perp'>預備區</span></h4>");
            builder.Append("        <h4 class='game-group'>" + site.Name + "</h4>");
            builder.Append("        <div style='overflow-y: scroll; height: 450px;'>");
            builder.Append(body);
            builder.Append("        </div>");
            builder.Append("      </div>");
            builder.Append("    </div>");
            builder.Append("  </div>");
            builder.Append("</div>");
        }

        private void AppendMatchBox(StringBuilder body, TEvent evt, TDetail foot1, TDetail foot2)
        {
            string bg_css = evt.WinSignNo != ""
                ? "style='background-color: rgba(255,169,0,0.9);'"
                : "";

            body.Append("<div class='row text-center' " + bg_css + ">");
            body.Append(foot1.ProgramName);
            body.Append("</div>");
            body.Append("<div class='row text-center'" + bg_css + ">");
            body.Append(GetLink(foot1.ProgramId, foot1.TreeId, foot1.EventName, foot1.TreeNo));
            //body.Append("<span class='badge btn-primary'>" + foot1.EventName + "</span>");
            body.Append("</div>");

            body.Append("<div class='waitMatchup' data-aos='fade-right' data-aos-duration='1000' data-aos-delay='800' data-aos-offset='-100' data-aos-once='true' " + bg_css + ">");

            AppendMatch(body, evt, foot2);

            body.Append("  <div class='part-vs'>");
            body.Append("    <lable>VS</label>");
            body.Append("  </div>");

            AppendMatch(body, evt, foot1);

            body.Append("</div>");

            body.Append("<hr style=' border: 0; height: 1px; background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0));'>");
        }

        private void AppendMatch(StringBuilder body, TEvent evt, TDetail foot)
        {
            body.Append("  <div class='player-vs player-vs-container'>");

            if (evt.NeedPhoto)
            {
                body.Append("    <div class='vs-portrait vs-portrait-wait in_input ' name='in_photo' data-file_maxwidth='220' data-file_maxheight='220' data-file_quality='0.5' style='margin: 10%;'>");
                body.Append("      <img class='in_thumbnail' src='../Images/taekwondo/default_photo.png' data-id='" + foot.PhotoId + "'>");
                body.Append("    </div>");
            }

            body.Append("    <div class='player-vs-infor'>");
            body.Append("      <p class='player-vs-group' style='color: #2C4198; margin-bottom: 0;'>" + foot.OrgName + "</p>");

            string check_css = "";
            if (foot.CheckResult == "1")
            {
                check_css = "bg-green";
            }
            else if (foot.CheckResult == "0")
            {
                check_css = "bg-red";
            }
            else if (foot.CheckResult == "")
            {
                //check_css = "label-default";
            }
            body.Append("      <h4 class='player-vs-name " + check_css + "'>" + foot.PlayerName + "</h4>");

            if (foot.Status != "")
            {
                body.Append("      <div class='label label-primary' style='margin-bottom: 10%;'>" + foot.Points + "</div>");
            }
            body.Append("    </div>");

            body.Append("  </div>");
        }

        #endregion 全部場次

        //修補物件
        private void FixSiteNodes(TConfig cfg)
        {
            foreach (var site in cfg.Sites)
            {
                if (site.Newest == null)
                {
                    site.Newest = new TEvent
                    {
                        Key = "",
                        WinSignNo = "",
                        Value = cfg.inn.newItem()
                    };
                }

                if (site.Newest.Foot1 == null)
                {
                    site.Newest.Foot1 = new TDetail();
                    SetDetailVal(site.Newest.Foot1, cfg.inn.newItem());
                }
                if (site.Newest.Foot2 == null)
                {
                    site.Newest.Foot2 = new TDetail();
                    SetDetailVal(site.Newest.Foot2, cfg.inn.newItem());
                }
            }
        }

        private string GetLink(string pid, string rid, string sid, string tid)
        {
            return "<a class='badge btn-primary' onclick='Event_Click(this)' data-pid='" + pid + "' data-rid='" + rid + "'>" + tid + "</a>";
        }

        private void MapEvents1(TConfig cfg, Item items)
        {
            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_sign_foot = item.getProperty("in_sign_foot", "");

                TSite site = GetAndAddSite(cfg.Sites, item);
                TEvent evt = null;

                if (i == 0)
                {
                    evt = GetEvent(site.Nodes, item);
                    evt.NeedPhoto = true;
                    site.TopOne = evt;
                }
                else if (i == 1)
                {
                    evt = site.TopOne;
                }
                else
                {
                    evt = GetAndAddEvent(site.Nodes, item);
                    evt.NeedPhoto = false;
                }

                if (in_sign_foot == "1")
                {
                    SetDetailVal(evt.Foot1, item);
                }
                else
                {
                    SetDetailVal(evt.Foot2, item);
                }
            }
        }

        private void MapEvents2(TConfig cfg, Item items)
        {
            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_sign_foot = item.getProperty("in_sign_foot", "");

                var site = GetAndAddSite(cfg.Sites, item);
                var evt = GetAndAddEvent(site.Nodes, item);
                evt.NeedPhoto = true;

                if (in_sign_foot == "1")
                {
                    SetDetailVal(evt.Foot1, item);
                }
                else
                {
                    SetDetailVal(evt.Foot2, item);
                }

            }
        }

        private void MapEvents3(TConfig cfg, Item items)
        {
            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_sign_foot = item.getProperty("in_sign_foot", "");

                TSite site = GetAndAddSite(cfg.Sites, item);
                TEvent evt = null;

                if (i == 0)
                {
                    evt = GetEvent(site.Nodes, item);
                    evt.NeedPhoto = true;
                    site.TopOne = evt;
                }
                else if (i == 1)
                {
                    evt = site.TopOne;
                }
                else
                {
                    evt = GetAndAddEvent(site.Nodes, item);
                    evt.NeedPhoto = false;
                }

                if (in_sign_foot == "1")
                {
                    SetDetailVal(evt.Foot1, item);
                }
                else
                {
                    SetDetailVal(evt.Foot2, item);
                }
            }
        }

        private void MapEvents4(TConfig cfg, Item items)
        {
            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_sign_foot = item.getProperty("in_sign_foot", "");

                TSite site = GetAndAddSite(cfg.Sites, item);
                TEvent evt = null;

                if (i == 0)
                {
                    evt = GetEvent(site.Nodes, item);
                    evt.NeedPhoto = false;
                    site.TopOne = evt;
                }
                else if (i == 1)
                {
                    evt = site.TopOne;
                }
                else
                {
                    evt = GetAndAddEvent(site.Nodes, item);
                    evt.NeedPhoto = false;
                }

                if (in_sign_foot == "1")
                {
                    SetDetailVal(evt.Foot1, item);
                }
                else
                {
                    SetDetailVal(evt.Foot2, item);
                }
            }
        }

        private void MapNewestEvents(TConfig cfg, Item items)
        {
            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string event_id = item.getProperty("event_id", "");
                string in_tree_no = item.getProperty("in_tree_no", "");
                string in_sign_foot = item.getProperty("in_sign_foot", "");
                string in_win_sign_no = item.getProperty("in_win_sign_no", "");

                var site = GetAndAddSite(cfg.Sites, item);

                if (site.Newest == null)
                {
                    site.MaxTreeNo = in_tree_no;

                    site.Newest = new TEvent
                    {
                        Key = event_id,
                        WinSignNo = in_win_sign_no,
                        Value = item,
                        Foot1 = new TDetail(),
                        Foot2 = new TDetail(),
                        IsFighted = true,
                    };
                }

                if (in_sign_foot == "1")
                {
                    SetDetailVal(site.Newest.Foot1, item);
                }
                else
                {
                    SetDetailVal(site.Newest.Foot2, item);
                }
            }
        }

        private TSite GetAndAddSite(List<TSite> entities, Item item)
        {
            string site_id = item.getProperty("site_id", "");
            string site_code = item.getProperty("site_code", "");

            //用code找比較快
            var entity = entities.Find(x => x.Code == site_code);

            if (entity == null)
            {
                entity = new TSite
                {
                    Id = site_id,
                    Code = site_code,
                    Name = "未設定場地" + site_code,
                    MaxTreeNo = "0",
                    Nodes = new List<TEvent>(),
                };
                entities.Add(entity);
            }
            return entity;
        }

        private TEvent GetEvent(List<TEvent> entities, Item item)
        {
            string key = item.getProperty("event_id", "");
            string in_win_sign_no = item.getProperty("in_win_sign_no", "");

            var entity = new TEvent
            {
                Key = key,
                WinSignNo = in_win_sign_no,
                Value = item,
                Foot1 = new TDetail(),
                Foot2 = new TDetail(),
            };
            return entity;
        }

        private TEvent GetAndAddEvent(List<TEvent> entities, Item item)
        {
            string key = item.getProperty("event_id", "");
            string in_win_sign_no = item.getProperty("in_win_sign_no", "");

            var entity = entities.Find(x => x.Key == key);

            if (entity == null)
            {
                entity = new TEvent
                {
                    Key = key,
                    WinSignNo = in_win_sign_no,
                    Value = item,
                    Foot1 = new TDetail(),
                    Foot2 = new TDetail(),
                };

                entities.Add(entity);
            }

            return entity;
        }

        private void SetDetailVal(TDetail entity, Item item)
        {
            entity.Value = item;
            entity.ProgramId = item.getProperty("program_id", " ");
            entity.ProgramName = item.getProperty("program_display", " ");
            entity.TreeId = item.getProperty("in_tree_id", " ");
            entity.TreeNo = item.getProperty("in_tree_no", " ");
            entity.EventName = item.getProperty("in_site_id", " ");
            entity.RoundCode = item.getProperty("in_round_code", " ");
            entity.OrgName = item.getProperty("map_short_org", " ");
            entity.PlayerName = item.getProperty("in_name", " ");
            entity.SignNo = item.getProperty("in_sign_no", " ");
            entity.SectionNo = item.getProperty("in_section_no", " ");

            entity.Points = item.getProperty("in_points", "0");
            entity.Corrects = item.getProperty("in_correct_count", " ");
            entity.Status = item.getProperty("in_status", "");
            entity.CheckResult = item.getProperty("in_check_result", "");

            entity.PhotoId = item.getProperty("in_photo", " ");
        }

        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public Innovator inn { get; set; }

            public string meeting_id { get; set; }
            public string in_date { get; set; }
            public string site_ids { get; set; }
            public string scene { get; set; }
            public string delay { get; set; }

            public string in_date_key { get; set; }

            /// <summary>
            /// 有值表示為挑戰賽
            /// </summary>
            public string in_date_type { get; set; }

            public Item itmMeeting { get; set; }
            public List<TDay> Days { get; set; }
            public List<TSite> Sites { get; set; }

            public int TopCount { get; set; }
        }

        private class TSite
        {
            public string Id { get; set; }
            public string Code { get; set; }
            public string Name { get; set; }
            public Item Value { get; set; }

            /// <summary>
            /// 最新一場決出勝負的場次
            /// </summary>
            public TEvent Newest { get; set; }

            /// <summary>
            /// 呈現在第一位的場次
            /// </summary>
            public TEvent TopOne { get; set; }


            public string MaxTreeNo { get; set; }
            public List<TEvent> Nodes { get; set; }
        }

        private class TEvent
        {
            public string Key { get; set; }
            public string WinSignNo { get; set; }
            public Item Value { get; set; }
            public TDetail Foot1 { get; set; }
            public TDetail Foot2 { get; set; }
            public bool IsFighted { get; set; }
            public bool NeedPhoto { get; set; }
        }

        private class TDetail
        {
            public Item Value { get; set; }
            public string ProgramId { get; set; }
            public string ProgramName { get; set; }
            public string TreeId { get; set; }
            public string TreeNo { get; set; }
            public string EventName { get; set; }
            public string RoundCode { get; set; }
            public string OrgName { get; set; }
            public string PlayerName { get; set; }
            public string SignNo { get; set; }
            public string SectionNo { get; set; }
            public string Status { get; set; }
            public string Points { get; set; }
            public string Corrects { get; set; }
            public string Score { get; set; }
            public string CheckResult { get; set; }
            public string PhotoId { get; set; }
        }

        private class TDay
        {
            public string label { get; set; }
            public string value { get; set; }
            public bool is_challenge { get; set; }
            public bool is_selected { get; set; }
            public Item Value { get; set; }
        }

        private Item GetMeeting(TConfig cfg)
        {
            string sql = "SELECT in_title FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'";
            return cfg.inn.applySQL(sql);
        }

        private List<TDay> GetDays(TConfig cfg)
        {
            string sql = @"
                SELECT
	                DISTINCT t1.in_date_key AS 'in_date_key'
                FROM
	                IN_MEETING_PEVENT t1 WITH(NOLOCK)
				WHERE 
					t1.in_meeting = '{#meeting_id}'
					AND ISNULL(t1.in_date_key, '') <> ''
				ORDER BY
					t1.in_date_key
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            Item items = cfg.inn.applySQL(sql);
            int count = items.getItemCount();

            List<TDay> list = new List<TDay>();
            string today = DateTime.Now.ToString("yyyy-MM-dd");

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_date_key = item.getProperty("in_date_key", "");

                TDay day = new TDay
                {
                    label = in_date_key,
                    value = in_date_key,
                    is_selected = in_date_key == today,
                    is_challenge = false,
                    Value = item,
                };

                list.Add(day);

                //TDay day_challenge = new TDay
                //{
                //    label = in_date_key + "(挑戰賽)",
                //    value = in_date_key + "_challenge",
                //    is_selected = false,
                //    is_challenge = true,
                //    Value = item,
                //};

                //list.Add(day_challenge);

            }
            return list;
        }

        private List<TSite> GetSites(TConfig cfg)
        {
            List<TSite> list = new List<TSite>();

            string sql = "SELECT * FROM IN_MEETING_SITE WITH(NOLOCK) WHERE in_meeting = '" + cfg.meeting_id + "' ORDER BY in_code";
            string[] site_id_arr = cfg.site_ids.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            bool need_filter = site_id_arr != null && site_id_arr.Length > 0;

            Item items = cfg.inn.applySQL(sql);
            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string id = item.getProperty("id", "");
                string in_code = item.getProperty("in_code", "");
                string in_name = item.getProperty("in_name", "");

                if (need_filter)
                {
                    if (!site_id_arr.Contains(id))
                    {
                        continue;
                    }
                }

                TSite site = new TSite
                {
                    Id = id,
                    Code = in_code,
                    Name = in_name,
                    MaxTreeNo = "0",
                    Value = item,
                    Nodes = new List<TEvent>(),
                };

                list.Add(site);
            }

            return list;
        }

        private Item GetEvents(TConfig cfg)
        {
            string tree_filter = cfg.in_date_type != ""
                ? "AND t2.in_tree_name NOT IN('main', 'repechage', 'rank34', 'rank56', 'rank78')"
                : "AND t2.in_tree_name NOT IN('rank56', 'rank78', 'challenge-a', 'challenge-b')";

            string sql = @"                
                SELECT
	                t1.id					AS 'program_id'
	                , t1.in_name			AS 'program_name'
	                , t1.in_name2			AS 'program_name2'
	                , t1.in_name3			AS 'program_name3'
	                , t1.in_display			AS 'program_display'
	                , t2.id         AS 'event_id'
	                , t2.in_date_key
	                , t2.in_tree_name
	                , t2.in_tree_id
	                , t2.in_tree_no
	                , t2.in_round_code
	                , t2.in_win_sign_no
	                , t3.in_sign_foot
	                , t3.in_sign_no
	                , t3.in_points
	                , t3.in_correct_count
	                , t3.in_status
					, t4.id         AS 'site_id'
					, t4.in_code    AS 'site_code'
					, t4.in_name    AS 'site_name'
	                , t11.in_name
	                , t11.in_section_no
	                , t11.map_short_org
	                , t11.map_org_name
	                , t11.in_current_org
	                , t11.in_check_result
					, t12.in_photo
                FROM
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PEVENT t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t3 WITH(NOLOCK)
	                ON t3.source_id = t2.id
				INNER JOIN IN_MEETING_SITE t4 WITH(NOLOCK)
					ON t4.in_meeting = t1.in_meeting
					AND t4.id = t2.in_site
                LEFT OUTER JOIN
	                VU_MEETING_PTEAM t11 WITH(NOLOCK)
	                ON t11.source_id = t1.id
	                AND t11.in_sign_no = t3.in_sign_no
				LEFT OUTER JOIN
					IN_RESUME t12 WITH(NOLOCK)
					ON t12.in_sno = t11.in_sno
                WHERE
	                t1.in_meeting = '{#meeting_id}'
                    AND t2.in_date_key = '{#in_date_key}'
	                {#tree_filter}
	                AND ISNULL(t2.in_bypass_foot, '') = ''
	                AND ISNULL(t2.in_tree_no, '') NOT IN ('', '0')
	                AND ISNULL(t2.in_win_status, '') NOT IN ('cancel')
                ORDER BY
	                t2.in_date_key
	                , t4.in_code
	                , t2.in_tree_no
	                , t3.in_sign_foot
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_date_key}", cfg.in_date_key)
                .Replace("{#tree_filter}", tree_filter);

            return cfg.inn.applySQL(sql);
        }

        private Item GetLastFightEvents(TConfig cfg)
        {
            string sql = @"
                SELECT 
	                t2.id
	                , t2.in_code         AS 'in_site_code'
	                , MAX(t1.in_tree_no) AS 'in_tree_no'
                FROM 
	                IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
                    IN_MEETING_SITE t2 WITH(NOLOCK)
                    ON t2.id = t1.in_site
                WHERE
	                t1.in_meeting = '{#meeting_id}'
	                AND t1.in_date_key = '{#in_date_key}' 
	                AND ISNULL(t1.in_tree_no, 0) <> 0
	                AND ISNULL(t1.in_win_status, '') NOT IN ('bypass', 'cancel')
	                AND t1.in_win_time IS NOT NULL 
                GROUP BY 
	                t2.id
	                , t2.in_code
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_date_key}", cfg.in_date_key);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        private Item GetTargetEvents(TConfig cfg, string site_event_keys)
        {
            string sql = @"                
                SELECT
	                t1.id					AS 'program_id'
	                , t1.in_name			AS 'program_name'
	                , t1.in_name2			AS 'program_name2'
	                , t1.in_name3			AS 'program_name3'
	                , t1.in_display			AS 'program_display'
	                , t2.id         AS 'event_id'
	                , t2.in_date_key
	                , t2.in_tree_name
	                , t2.in_tree_id
	                , t2.in_tree_no
	                , t2.in_round_code
	                , t2.in_win_sign_no
	                , t3.in_sign_foot
	                , t3.in_sign_no
	                , t3.in_points
	                , t3.in_correct_count
	                , t3.in_status
					, t4.id         AS 'site_id'
					, t4.in_code    AS 'site_code'
					, t4.in_name    AS 'site_name'
	                , t11.in_name
                    , t11.in_section_no
	                , t11.map_short_org
	                , t11.map_org_name
	                , t11.in_current_org
	                , t11.in_check_result
					, t12.in_photo
                FROM
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PEVENT t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t3 WITH(NOLOCK)
	                ON t3.source_id = t2.id
				INNER JOIN IN_MEETING_SITE t4 WITH(NOLOCK)
					ON t4.in_meeting = t1.in_meeting
					AND t4.id = t2.in_site
                LEFT OUTER JOIN
	                VU_MEETING_PTEAM t11 WITH(NOLOCK)
	                ON t11.source_id = t1.id
	                AND t11.in_sign_no = t3.in_sign_no
				LEFT OUTER JOIN
					IN_RESUME t12 WITH(NOLOCK)
					ON t12.in_sno = t11.in_sno
                WHERE
	                t1.in_meeting = '{#meeting_id}'
                    AND ISNULL(t2.in_date_key, '') + '_' + ISNULL(t2.in_site_code, '') + '_' + CAST(t2.in_tree_no AS VARCHAR) IN ({#site_event_keys})
                ORDER BY
	                t2.in_date_key
	                , t4.in_code
	                , t2.in_tree_no
	                , t3.in_sign_foot
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#site_event_keys}", site_event_keys);
            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        private Item GetNextSiteEvents(TConfig cfg, TSite site)
        {
            //string tree_filter = cfg.in_date_type != ""
            //    ? "AND t2.in_tree_name NOT IN('main', 'repechage', 'rank34', 'rank56', 'rank78')"
            //    : "AND t2.in_tree_name NOT IN('rank56', 'rank78', 'challenge-a', 'challenge-b')";

            string sql = @"                
                SELECT TOP {#TopCount}
	                t1.id					AS 'program_id'
	                , t1.in_name			AS 'program_name'
	                , t1.in_name2			AS 'program_name2'
	                , t1.in_name3			AS 'program_name3'
	                , t1.in_display			AS 'program_display'
	                , t2.id         AS 'event_id'
	                , t2.in_date_key
	                , t2.in_tree_name
	                , t2.in_tree_id
	                , t2.in_tree_no
	                , t2.in_round_code
	                , t2.in_win_sign_no
	                , t3.in_sign_foot
	                , t3.in_sign_no
	                , t3.in_points
	                , t3.in_correct_count
	                , t3.in_status
					, t4.id         AS 'site_id'
					, t4.in_code    AS 'site_code'
					, t4.in_name    AS 'site_name'
	                , t11.in_name
	                , t11.in_section_no
	                , t11.map_short_org
	                , t11.map_org_name
	                , t11.in_current_org
	                , t11.in_check_result
					, t12.in_photo
                FROM
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PEVENT t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t3 WITH(NOLOCK)
	                ON t3.source_id = t2.id
				INNER JOIN IN_MEETING_SITE t4 WITH(NOLOCK)
					ON t4.in_meeting = t1.in_meeting
					AND t4.id = t2.in_site
                LEFT OUTER JOIN
	                VU_MEETING_PTEAM t11 WITH(NOLOCK)
	                ON t11.source_id = t1.id
	                AND t11.in_sign_no = t3.in_sign_no
				LEFT OUTER JOIN
					IN_RESUME t12 WITH(NOLOCK)
					ON t12.in_sno = t11.in_sno
                WHERE
	                t1.in_meeting = '{#meeting_id}'
                    AND t2.in_date_key = '{#in_date_key}'
	                AND ISNULL(t2.in_tree_no, 0) <> 0
	                AND ISNULL(t2.in_win_status, '') NOT IN ('bypass', 'cancel')
                    AND t2.in_site = '{#in_site}'
                    AND t2.in_tree_no > {#max_tree_no}
                ORDER BY
	                t4.in_code
                    , t2.in_tree_no
                    , t3.in_sign_foot
            ";

            sql = sql
                .Replace("{#TopCount}", cfg.TopCount.ToString())
                .Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_date_key}", cfg.in_date_key)
                .Replace("{#in_site}", site.Id)
                .Replace("{#max_tree_no}", site.MaxTreeNo.ToString());

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        private Item GetNextSiteEvents2(TConfig cfg, TSite site)
        {
            //string tree_filter = cfg.in_date_type != ""
            //    ? "AND t2.in_tree_name NOT IN('main', 'repechage', 'rank34', 'rank56', 'rank78')"
            //    : "AND t2.in_tree_name NOT IN('rank56', 'rank78', 'challenge-a', 'challenge-b')";

            string sql = @"                
                SELECT TOP {#TopCount}
	                t1.id					AS 'program_id'
	                , t1.in_name			AS 'program_name'
	                , t1.in_name2			AS 'program_display'
	                , t1.in_name3			AS 'program_name3'
	                , t1.in_display			AS 'program_display2'
	                , t2.id         AS 'event_id'
	                , t2.in_date_key
	                , t2.in_tree_name
	                , t2.in_tree_id
	                , t2.in_tree_no
	                , t2.in_round_code
	                , t2.in_win_sign_no
	                , t3.in_sign_foot
	                , t3.in_sign_no
	                , t3.in_points
	                , t3.in_correct_count
	                , t3.in_status
					, t4.id         AS 'site_id'
					, t4.in_code    AS 'site_code'
					, t4.in_name    AS 'site_name'
	                , t11.in_name
	                , t11.in_section_no
	                , t11.map_short_org
	                , t11.map_org_name
	                , t11.in_current_org
	                , t11.in_check_result
					, t12.in_photo
                FROM
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PEVENT t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t3 WITH(NOLOCK)
	                ON t3.source_id = t2.id
				INNER JOIN IN_MEETING_SITE t4 WITH(NOLOCK)
					ON t4.in_meeting = t1.in_meeting
					AND t4.id = t2.in_site
                LEFT OUTER JOIN
	                VU_MEETING_PTEAM t11 WITH(NOLOCK)
	                ON t11.source_id = t1.id
	                AND t11.in_sign_no = t3.in_sign_no
				LEFT OUTER JOIN
					IN_RESUME t12 WITH(NOLOCK)
					ON t12.in_sno = t11.in_sno
                WHERE
	                t1.in_meeting = '{#meeting_id}'
                    AND t2.in_date_key = '{#in_date_key}'
                    AND t2.in_site = '{#in_site}'
	                AND ISNULL(t2.in_tree_no, 0) <> 0
	                AND ISNULL(t2.in_win_status, '') NOT IN ('bypass', 'cancel')
                    AND t2.in_win_time  IS NULL
                ORDER BY
	                t4.in_code
                    , t2.in_tree_no
                    , t3.in_sign_foot
            ";

            sql = sql
                .Replace("{#TopCount}", cfg.TopCount.ToString())
                .Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_date_key}", cfg.in_date_key)
                .Replace("{#in_site}", site.Id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        private Item GetRTMSiteEvents(TConfig cfg, TSite site)
        {
            string sql = @"                
                SELECT TOP {#TopCount}
	                t1.id					AS 'program_id'
	                , t1.in_name			AS 'program_name'
	                , t1.in_name2			AS 'program_name2'
	                , t1.in_name3			AS 'program_name3'
	                , t1.in_display			AS 'program_display'
	                , t2.id         AS 'event_id'
	                , t2.in_date_key
	                , t2.in_tree_name
	                , t2.in_tree_id
	                , t2.in_tree_no
	                , t2.in_round_code
	                , t2.in_win_sign_no
	                , t3.in_sign_foot
	                , t3.in_sign_no
	                , t3.in_points
	                , t3.in_correct_count
	                , t3.in_status
					, t4.id         AS 'site_id'
					, t4.in_code    AS 'site_code'
					, t4.in_name    AS 'site_name'
	                , t11.in_name
	                , t11.in_section_no
	                , t11.map_short_org
	                , t11.map_org_name
	                , t11.in_current_org
	                , t11.in_check_result
					, t12.in_photo
                FROM
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PEVENT t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t3 WITH(NOLOCK)
	                ON t3.source_id = t2.id
				INNER JOIN IN_MEETING_SITE t4 WITH(NOLOCK)
					ON t4.in_meeting = t1.in_meeting
					AND t4.id = t2.in_site
                LEFT OUTER JOIN
	                VU_MEETING_PTEAM t11 WITH(NOLOCK)
	                ON t11.source_id = t1.id
	                AND t11.in_sign_no = t3.in_sign_no
				LEFT OUTER JOIN
					IN_RESUME t12 WITH(NOLOCK)
					ON t12.in_sno = t11.in_sno
                WHERE
	                t1.in_meeting = '{#meeting_id}'
                    AND t2.in_date_key = '{#in_date_key}'
	                AND ISNULL(t2.in_tree_no, 0) <> 0
	                AND ISNULL(t2.in_win_status, '') NOT IN ('bypass', 'cancel')
                    AND t2.in_site = '{#in_site}'
                    AND t2.in_win_time IS NOT NULL
                ORDER BY
	                t2.in_win_time DESC
            ";

            sql = sql
                .Replace("{#TopCount}", cfg.TopCount.ToString())
                .Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_date_key}", cfg.in_date_key)
                .Replace("{#in_site}", site.Id)
                .Replace("{#max_tree_no}", site.MaxTreeNo.ToString());

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        private string GetEventIds(TConfig cfg, Item items)
        {
            List<string> list = new List<string>();
            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_date_key = cfg.in_date_key;
                string in_site_code = item.getProperty("in_site_code", "");
                string in_tree_no = item.getProperty("in_tree_no", "");
                string key = "'" + in_date_key + "_" + in_site_code + "_" + in_tree_no + "'";
                list.Add(key);
            }

            return string.Join(", ", list);
        }
    }
}