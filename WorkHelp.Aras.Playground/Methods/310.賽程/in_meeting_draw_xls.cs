﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class in_meeting_draw_xls : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 新籤表 (含亂數抽籤結果)
                前提: 需處理
                    1. 設定賽制，產生組別與場次資料
                    2. 產生隊伍資料
                輸入: meeting_id
                輸出: 
                    1. 報名總表
                    2. 參賽各組統計
                    3. 競賽項目
                        個人組籤表
                        格式組籤表
                    4. 團體組報名名單
                人員: lina
                日期: 
                    - 2021-12-24: 改為 Spire.Xls (lina)
                    - 2020-10-15: 改版 (lina)
                    - 2020-07-27: 增加對帳表 (lina)
                    - 2020-07-23: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]in_meeting_draw_xls";

            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "input dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                InnH = _InnH,

                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                CharSet = GetCharSet(),
            };

            if (cfg.meeting_id == "")
            {
                throw new Exception("賽事 id 不得為空白");
            }

            cfg.itmMeeting = cfg.inn.applySQL("SELECT in_title FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            cfg.mt_title = cfg.itmMeeting.getProperty("in_title", "");

            var exp = ExportInfo(cfg, "weight_path");

            //取得與會者資料
            Item itmMUsers = GetMeetingUsers(cfg);
            //取得賽事統計摘要
            GameSummary game_summary = MapToGameSummary(cfg, itmMUsers);

            //轉換與會資料
            Dictionary<string, List<Item>> mapMUsers = MapToDicList1(cfg, itmMUsers, "in_team_key");
            //取得隊伍資料
            Item itmMTeams = GetMeetingPTeams(cfg);
            //取得項目統計摘要
            ProgramSummary program_summary = MapProgramSummary(cfg, itmMTeams, mapMUsers);


            //試算表
            Spire.Xls.Workbook workbook = new Spire.Xls.Workbook();

            //報名總表
            AppendSummarySheet(cfg, workbook, game_summary);

            //參賽各組統計
            AppendStatisticsSheet(cfg, workbook, program_summary);

            //分組籤表與報名名單
            foreach (var pairs in program_summary.Reports)
            {
                var report = pairs.Value;
                if (report.IsMultiPlayers)
                {
                    //團體類型籤表
                    AppendTeamSheet(cfg, workbook, report);
                    //團體類型報名名單
                    AppendTeamMemberSheet(cfg, workbook, report);
                }
                else if (report.InL1 == "格式組")
                {
                    //格式組類型籤表
                    AppendFormatSheet(cfg, workbook, report);
                }
                else
                {
                    //個人類型籤表
                    AppendSingleSheet(cfg, workbook, report);
                }
            }

            workbook.Worksheets[0].Remove();
            workbook.Worksheets[0].Remove();
            workbook.Worksheets[0].Remove();

            string ext_name = ".xlsx";
            string xls_name = cfg.mt_title + "_籤表_" + DateTime.Now.ToString("MMdd_HHmmss");
            string xls_file = exp.export_Path + "\\" + xls_name + ext_name;
            string xls_url = xls_name + ext_name;

            workbook.SaveToFile(xls_file, Spire.Xls.ExcelVersion.Version2013);

            itmR.setProperty("xls_name", xls_url);

            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

            return itmR;
        }

        //報名總表
        private void AppendSummarySheet(TConfig cfg, Spire.Xls.Workbook workbook, GameSummary game_summary)
        {
            Spire.Xls.Worksheet sheet = workbook.CreateEmptySheet();
            sheet.Name = "報名總表";

            List<TField> fields = new List<TField>();
            fields.Add(new TField { ci = 2, title = "序號", property = "no", css = TCss.Center, width = 5, is_merge = true });
            fields.Add(new TField { ci = 3, title = "所屬單位", property = "map_org_name", css = TCss.None, width = 18, is_merge = true });
            fields.Add(new TField { ci = 4, title = "", property = "", css = TCss.None, width = 10 });
            fields.Add(new TField { ci = 5, title = "隊職員人數", property = "inn_manager_count", css = TCss.Number, width = 10 });
            fields.Add(new TField { ci = 6, title = "選手人數", property = "inn_player_count", css = TCss.Number, width = 10 });
            fields.Add(new TField { ci = 7, title = "項目總計", property = "inn_item_count", css = TCss.Number, width = 10 });

            int ci = 8;
            foreach (KeyValuePair<string, string> item_kv in game_summary.ItemMap)
            {
                fields.Add(new TField { ci = ci, title = item_kv.Key, property = item_kv.Key, css = TCss.Number, width = 10 });
                ci++;
            }

            fields.Add(new TField { ci = ci, title = "報名費用", property = "inn_subtotal", css = TCss.Money, width = 10 });
            fields.Add(new TField { ci = ci + 1, title = "協助報名者", property = "inn_creator_name", css = TCss.None, width = 10 });
            fields.Add(new TField { ci = ci + 2, title = "連絡電話", property = "inn_creator_tel", css = TCss.Center, width = 12 });

            MapCharSet(cfg, fields);

            SetHead(sheet, "B2", "報名總費用");
            SetMoney(sheet, "D2", game_summary.TotalAmount);

            var sum_mr = sheet.Range["B2:C2"];
            sum_mr.Merge();
            sum_mr.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;

            int mnRow = 4;
            int wsRow = 4;

            //標題列
            SetHeadRow(sheet, wsRow, fields);
            wsRow++;

            //凍結窗格
            sheet.FreezePanes(wsRow, fields.Last().ci + 1);

            //資料容器
            Item item = cfg.inn.newItem();
            int no = 0;

            foreach (KeyValuePair<string, OrgSummary> org_kv in game_summary.OrgMap.OrderBy(x => x.Key))
            {
                no++;
                OrgSummary org = org_kv.Value;

                foreach (KeyValuePair<string, CreatorSummary> creator_kv in org.CreatorMap)
                {
                    CreatorSummary creator = creator_kv.Value;

                    item.setProperty("no", no.ToString());
                    item.setProperty("map_org_name", org.Name);

                    item.setProperty("inn_manager_count", creator.ManagerMap.Count.ToString());
                    item.setProperty("inn_player_count", creator.PlayerMap.Count.ToString());
                    item.setProperty("inn_item_count", creator.PlayItemMap.Count.ToString());

                    foreach (KeyValuePair<string, string> item_kv in game_summary.ItemMap)
                    {
                        if (creator.SubItemMap.ContainsKey(item_kv.Key))
                        {
                            item.setProperty(item_kv.Key, creator.SubItemMap[item_kv.Key].Count.ToString());
                        }
                        else
                        {
                            item.setProperty(item_kv.Key, "0");
                        }
                    }

                    item.setProperty("inn_subtotal", creator.Subtotal.ToString());

                    item.setProperty("inn_creator_name", creator.Name);
                    item.setProperty("inn_creator_tel", creator.TelNumber);

                    SetItemCell(cfg, sheet, wsRow, item, fields);

                    wsRow++;
                }

                if (org.CreatorMap.Count == 1)
                {
                    continue;
                }

                int row_s = wsRow - org.CreatorMap.Count;
                int row_e = wsRow - 1;

                //合併處理
                foreach (var field in fields)
                {
                    if (!field.is_merge) continue;

                    string cr1 = field.cs + row_s;
                    string cr2 = field.cs + row_e;
                    if (cr1 == cr2) continue;

                    var mr = sheet.Range[cr1 + ":" + cr2];
                    mr.Merge();
                    mr.Style.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;
                }
            }

            //設定格線
            var sum_pos = "B2:D2";
            SetRangeBorder(sheet, sum_pos);

            //設定格線
            SetRangeBorder(sheet, fields, mnRow, wsRow);

            //設定欄寬
            SetColumnWidth(sheet, fields);
        }

        //參賽各組統計
        private void AppendStatisticsSheet(TConfig cfg, Spire.Xls.Workbook workbook, ProgramSummary map)
        {
            Spire.Xls.Worksheet sheet = workbook.CreateEmptySheet();
            sheet.Name = "參賽各組統計";

            List<TField> fields = new List<TField>();
            fields.Add(new TField { title = "項目", property = "in_l1", css = TCss.None, width = 10 });
            fields.Add(new TField { title = "序號", property = "no", css = TCss.Center, width = 5 });
            fields.Add(new TField { title = "組名", property = "program_name3", css = TCss.None, width = 30 });
            fields.Add(new TField { title = "隊伍數量", property = "in_team_count", css = TCss.Number, width = 10 });
            fields.Add(new TField { title = "合計人數", property = "in_team_players", css = TCss.Number, width = 10 });

            MapCharSetAndIndex(cfg, fields);

            int mnRow = 1;
            int wsRow = 1;

            //標題列
            SetHeadRow(sheet, wsRow, fields);
            wsRow++;

            //凍結窗格
            sheet.FreezePanes(wsRow, fields.Last().ci + 1);

            //資料容器
            Item item = cfg.inn.newItem();

            foreach (var pairs1 in map.Statistics)
            {
                var key = pairs1.Key;
                var value = pairs1.Value;

                var no = 1;
                foreach (var pairs2 in value.Map)
                {
                    var list = pairs2.Value;
                    var source = list.First();

                    item.setProperty("in_l1", key);
                    item.setProperty("no", no.ToString());
                    item.setProperty("program_name3", source.getProperty("program_name3", ""));
                    item.setProperty("in_team_count", source.getProperty("in_team_count", "0"));
                    item.setProperty("in_team_players", GetTeamPlayers(list));

                    SetItemCell(cfg, sheet, wsRow, item, fields);

                    no++;
                    wsRow++;
                }
            }

            //設定格線
            SetRangeBorder(sheet, fields, mnRow, wsRow);

            //設定欄寬
            SetColumnWidth(sheet, fields);
        }

        //格式組籤表
        private void AppendFormatSheet(TConfig cfg, Spire.Xls.Workbook workbook, TReport report)
        {
            Spire.Xls.Worksheet sheet = workbook.CreateEmptySheet();
            sheet.Name = report.Key + "籤表";

            List<TField> fields = new List<TField>();
            fields.Add(new TField { title = "序號", property = "no", css = TCss.Center, width = 5 });
            fields.Add(new TField { title = "姓名", property = "in_name", css = TCss.None, width = 22 });
            fields.Add(new TField { title = "身分證字號", property = "in_sno", css = TCss.Center, width = 11 });
            fields.Add(new TField { title = "性別", property = "in_gender", css = TCss.Center, width = 5 });
            fields.Add(new TField { title = "出生年月日", property = "in_birth", css = TCss.Day, width = 12 });
            fields.Add(new TField { title = "組名", property = "program_name3", css = TCss.None, width = 30 });
            fields.Add(new TField { title = "目前單位", property = "map_org_name", css = TCss.None, width = 18 });
            fields.Add(new TField { title = "被施術者", property = "in_exe_a1", css = TCss.None, width = 22 });
            fields.Add(new TField { title = "身分證字號", property = "in_exe_a3", css = TCss.Center, width = 11 });
            fields.Add(new TField { title = "性別", property = "in_exe_a5", css = TCss.Center, width = 5 });
            fields.Add(new TField { title = "出生年月日", property = "in_exe_a2", css = TCss.Day, width = 12 });
            fields.Add(new TField { title = "目前單位", property = "in_exe_a4", css = TCss.None, width = 18 });
            fields.Add(new TField { title = "隊別", property = "in_team", css = TCss.Center, width = 5 });
            fields.Add(new TField { title = "籤號", property = "in_sign_no", css = TCss.Number, width = 5 });
            fields.Add(new TField { title = "種子籤", property = "in_seeds", css = TCss.Number, width = 5 });

            MapCharSetAndIndex(cfg, fields);

            int mnRow = 1;
            int wsRow = 1;

            //資料容器
            Item item = cfg.inn.newItem();

            foreach (var pkv in report.Pages)
            {
                var teams = pkv.Value.Teams;

                //標題列
                SetHeadRow(sheet, wsRow, fields);
                wsRow++;

                int no = 1;

                //foreach (var team in teams.OrderBy(x => x.SignNo))
                foreach (var tkv in teams)
                {
                    var team = tkv.Value;
                    var source = team.Value;

                    item.setProperty("no", no.ToString());
                    item.setProperty("in_sign_no", source.getProperty("in_sign_no", ""));
                    item.setProperty("in_name", source.getProperty("in_name", ""));
                    item.setProperty("in_sno", source.getProperty("in_sno", ""));
                    item.setProperty("in_gender", source.getProperty("in_gender", ""));
                    item.setProperty("in_birth", source.getProperty("in_birth", ""));
                    item.setProperty("program_name3", source.getProperty("program_name3", ""));
                    item.setProperty("map_org_name", source.getProperty("map_org_name", ""));
                    item.setProperty("in_team", source.getProperty("in_team", ""));
                    item.setProperty("in_seeds", source.getProperty("in_seeds", ""));

                    item.setProperty("in_exe_a1", source.getProperty("in_exe_a1", ""));
                    item.setProperty("in_exe_a2", source.getProperty("in_exe_a2", ""));
                    item.setProperty("in_exe_a3", source.getProperty("in_exe_a3", ""));
                    item.setProperty("in_exe_a5", GetGenderBySno(source.getProperty("in_exe_a3", "")));
                    item.setProperty("in_exe_a4", source.getProperty("in_exe_a4", ""));

                    SetItemCell(cfg, sheet, wsRow, item, fields);

                    no++;
                    wsRow++;
                }
            }

            //設定格線
            SetRangeBorder(sheet, fields, mnRow, wsRow);

            //設定欄寬
            SetColumnWidth(sheet, fields);
        }

        //個人類型籤表
        private void AppendSingleSheet(TConfig cfg, Spire.Xls.Workbook workbook, TReport report)
        {
            Spire.Xls.Worksheet sheet = workbook.CreateEmptySheet();
            sheet.Name = report.Key + "籤表";

            List<TField> fields = new List<TField>();
            fields.Add(new TField { title = "序號", property = "no", css = TCss.Center, width = 5 });
            fields.Add(new TField { title = "姓名", property = "in_name", css = TCss.None, width = 22 });
            fields.Add(new TField { title = "身分證字號", property = "in_sno", css = TCss.Center, width = 11 });
            fields.Add(new TField { title = "性別", property = "in_gender", css = TCss.Center, width = 5 });
            fields.Add(new TField { title = "出生年月日", property = "in_birth", css = TCss.Day, width = 12 });
            fields.Add(new TField { title = "組名", property = "program_name3", css = TCss.None, width = 30 });
            fields.Add(new TField { title = "目前單位", property = "map_org_name", css = TCss.None, width = 18 });
            fields.Add(new TField { title = "隊別", property = "in_team", css = TCss.Center, width = 5 });
            fields.Add(new TField { title = "籤號", property = "in_sign_no", css = TCss.Number, width = 5 });
            fields.Add(new TField { title = "種子籤", property = "in_seeds", css = TCss.Number, width = 5 });

            MapCharSetAndIndex(cfg, fields);

            int mnRow = 1;
            int wsRow = 1;

            //資料容器
            Item item = cfg.inn.newItem();

            foreach (var pkv in report.Pages)
            {
                var teams = pkv.Value.Teams;

                //標題列
                SetHeadRow(sheet, wsRow, fields);
                wsRow++;

                int no = 1;
                foreach (var tkv in teams)
                {
                    var team = tkv.Value;
                    var source = team.Value;

                    item.setProperty("no", no.ToString());
                    item.setProperty("in_sign_no", source.getProperty("in_sign_no", ""));
                    item.setProperty("in_section_no", source.getProperty("in_section_no", ""));
                    item.setProperty("in_name", source.getProperty("in_name", ""));
                    item.setProperty("in_sno", source.getProperty("in_sno", ""));
                    item.setProperty("in_gender", source.getProperty("in_gender", ""));
                    item.setProperty("in_birth", source.getProperty("in_birth", ""));
                    item.setProperty("program_name3", source.getProperty("program_name3", ""));
                    item.setProperty("map_org_name", source.getProperty("map_org_name", ""));
                    item.setProperty("in_team", source.getProperty("in_team", ""));
                    item.setProperty("in_seeds", source.getProperty("in_seeds", ""));

                    SetItemCell(cfg, sheet, wsRow, item, fields);

                    no++;
                    wsRow++;
                }
            }

            //設定格線
            SetRangeBorder(sheet, fields, mnRow, wsRow);

            //設定欄寬
            SetColumnWidth(sheet, fields);
        }

        //團體類型籤表
        private void AppendTeamSheet(TConfig cfg, Spire.Xls.Workbook workbook, TReport report)
        {
            Spire.Xls.Worksheet sheet = workbook.CreateEmptySheet();
            sheet.Name = report.Key + "籤表";

            List<TField> fields = new List<TField>();
            fields.Add(new TField { ci = 1, title = "序號", property = "no", css = TCss.Center, width = 5 });
            fields.Add(new TField { ci = 2, title = "目前單位", property = "map_org_name", css = TCss.None, width = 18 });
            fields.Add(new TField { ci = 3, title = "隊別", property = "in_team", css = TCss.Center, width = 5 });
            fields.Add(new TField { ci = 4, title = "組名", property = "program_name3", css = TCss.None, width = 30 });
            fields.Add(new TField { ci = 5, title = "籤號", property = "in_sign_no", css = TCss.Number, width = 5 });

            MapCharSet(cfg, fields);

            int mnRow = 1;
            int wsRow = 1;

            //資料容器
            Item item = cfg.inn.newItem();

            foreach (var pkv in report.Pages)
            {
                var teams = pkv.Value.Teams;

                //標題列
                SetHeadRow(sheet, wsRow, fields);
                wsRow++;

                int no = 1;
                foreach (var tkv in teams)
                {
                    var team = tkv.Value;
                    var source = team.Value;
                    item.setProperty("no", no.ToString());
                    item.setProperty("map_org_name", source.getProperty("map_org_name", ""));
                    item.setProperty("in_name", source.getProperty("in_name", ""));
                    item.setProperty("program_name3", source.getProperty("program_name3", ""));
                    item.setProperty("in_sign_no", source.getProperty("in_sign_no", ""));
                    item.setProperty("in_section_no", source.getProperty("in_section_no", ""));
                    item.setProperty("in_team", source.getProperty("in_team", ""));

                    SetItemCell(cfg, sheet, wsRow, item, fields);

                    no++;
                    wsRow++;
                }
            }

            //設定格線
            SetRangeBorder(sheet, fields, mnRow, wsRow);

            //設定欄寬
            SetColumnWidth(sheet, fields);
        }

        //團體類型報名名單
        private void AppendTeamMemberSheet(TConfig cfg, Spire.Xls.Workbook workbook, TReport report)
        {
            Spire.Xls.Worksheet sheet = workbook.CreateEmptySheet();
            sheet.Name = report.Key + "報名名單";

            List<TField> fields = new List<TField>();
            fields.Add(new TField { ci = 1, title = "序號", property = "no", css = TCss.Center, width = 5, is_merge = true });
            fields.Add(new TField { ci = 2, title = "目前單位", property = "map_org_name", css = TCss.None, width = 18, is_merge = true });
            fields.Add(new TField { ci = 3, title = "隊別", property = "inn_team", css = TCss.Center, width = 5, is_merge = true });
            fields.Add(new TField { ci = 4, title = "組名", property = "program_name3", css = TCss.None, width = 30, is_merge = true });
            fields.Add(new TField { ci = 5, title = "籤號", property = "in_sign_no", css = TCss.Number, width = 5, is_merge = true });
            fields.Add(new TField { ci = 6, title = "隊員姓名", property = "in_name", css = TCss.None, width = 22 });
            fields.Add(new TField { ci = 7, title = "身分證字號", property = "in_sno", css = TCss.Center, width = 11 });
            fields.Add(new TField { ci = 8, title = "性別", property = "in_gender", css = TCss.Center, width = 5 });
            fields.Add(new TField { ci = 9, title = "出生年月日", property = "in_birth", css = TCss.Day, width = 12 });

            MapCharSet(cfg, fields);

            int mnRow = 1;
            int wsRow = 1;

            //資料容器
            Item item = cfg.inn.newItem();

            foreach (var pkv in report.Pages)
            {
                var teams = pkv.Value.Teams;

                //標題列
                SetHeadRow(sheet, wsRow, fields);
                wsRow++;

                int no = 1;
                foreach (var tkv in teams)
                {
                    var team = tkv.Value;
                    var source = team.Value;

                    foreach (var player in team.Players)
                    {
                        player.setProperty("no", no.ToString());
                        player.setProperty("inn_team", source.getProperty("in_name", ""));
                        player.setProperty("in_sign_no", source.getProperty("in_sign_no", ""));
                        player.setProperty("program_name3", source.getProperty("program_name3", ""));
                        SetItemCell(cfg, sheet, wsRow, player, fields);
                        wsRow++;
                    }

                    int row_s = wsRow - team.Players.Count;
                    int row_e = wsRow - 1;

                    //合併處理
                    foreach (var field in fields)
                    {
                        if (!field.is_merge) continue;

                        string cr1 = field.cs + row_s;
                        string cr2 = field.cs + row_e;
                        if (cr1 == cr2) continue;

                        var mr = sheet.Range[cr1 + ":" + cr2];
                        mr.Merge();
                        mr.Style.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;
                    }

                    no++;
                }
            }

            //設定格線
            SetRangeBorder(sheet, fields, mnRow, wsRow);

            //設定欄寬
            SetColumnWidth(sheet, fields);
        }



        /// <summary>
        /// 取得與會者資料
        /// </summary>
        private Item GetMeetingUsers(TConfig cfg)
        {
            string sql = @"
                SELECT
                    t1.in_l1
                    , t1.in_l2
                    , t1.in_l3
                    , t1.in_index
                    , ISNULL(t1.in_l1, '') + '-' + ISNULL(t1.in_l2, '') + '-' + ISNULL(t1.in_l3, '') + '-' + ISNULL(t1.in_index, '') + '-' + ISNULL(t1.in_creator_sno, '') AS 'in_team_key'
                    , t1.in_current_org
                    , t1.in_short_org
                    , t1.in_team
                    , t1.in_name
                    , t1.in_sno
                    , t1.in_gender
                    , t1.in_birth
                    , t1.in_creator
                    , t1.in_creator_sno
                    , t2.in_tel AS 'in_creator_tel'
                    , t1.in_expense
                    , t1.in_paynumber
                    , t1.in_exe_a1
                    , t1.in_exe_a2
                    , t1.in_exe_a3
                    , t1.in_exe_a4
                    , t3.pay_bool
                    , t3.in_pay_amount_exp
                    , t3.in_pay_amount_real
                    , t4.in_stuff_b1 + ISNULL(t4.in_short_org, t1.in_short_org) AS 'map_org_name'
                FROM 
                    IN_MEETING_USER t1 WITH(NOLOCK)
                LEFT OUTER JOIN
                    IN_RESUME t2 WITH(NOLOCK)
                    ON t2.in_sno = t1.in_creator_sno
                LEFT OUTER JOIN
                    IN_MEETING_PAY t3 WITH(NOLOCK)
                    ON t3.in_meeting = t1.source_id 
                    AND t3.item_number = t1.in_paynumber
                LEFT OUTER JOIN
                    IN_ORG_MAP t4 WITH(NOLOCK)
                    ON t4.in_stuff_b1 = t1.in_stuff_b1 
                WHERE 
                    t1.source_id = '{#meeting_id}'
                ORDER BY 
                    t1.in_stuff_b1
                    , t1.in_team
                    , t1.in_sno
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item items = cfg.inn.applySQL(sql);

            if (items.isError() || items.getItemCount() <= 0)
            {
                throw new Exception("查無與會者資料");
            }

            return items;
        }

        /// <summary>
        /// 取得賽事隊伍資料
        /// </summary>
        private Item GetMeetingPTeams(TConfig cfg)
        {
            string sql = @"
                SELECT
                    t1.id AS 'program_id'
                    , t1.in_name  AS 'program_name'
                    , t1.in_name2 AS 'program_name2'
                    , t1.in_name3 AS 'program_name3'
                    , t1.in_display AS 'program_display'
                    , t1.in_l1
                    , t1.in_l2
                    , t1.in_l3
                    , t1.in_team_count
                    , t2.in_group
                    , t2.in_current_org
                    , t2.in_short_org
                    , t2.in_name
                    , t2.in_sno
                    , t2.in_names
                    , t2.in_judo_no AS 'in_sign_no'
                    , t2.in_section_no
                    , t2.in_team
                    , t2.in_team_players
                    , t2.in_seeds
                    , t2.map_org_name
                    , t2.in_team_key
                    , t2.in_index
                    , t2.in_creator_sno
                FROM
                    IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
                    VU_MEETING_PTEAM t2 WITH(NOLOCK)
                    ON t2.SOURCE_ID = t1.id
                WHERE
                    t1.in_meeting = '{#meeting_id}'
                ORDER BY
                    t1.in_sort_order
                    , t2.in_stuff_b1
                    , t2.in_team
                    , t2.in_sno
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item items = cfg.inn.applySQL(sql);

            if (items.isError() || items.getItemCount() <= 0)
            {
                throw new Exception("查無賽事隊伍資料");
            }

            return items;
        }

        #region 單位統計

        /// <summary>
        /// 轉換為報名總表
        /// </summary>
        private GameSummary MapToGameSummary(TConfig cfg, Item items)
        {
            Dictionary<string, OrgSummary> orgMap = new Dictionary<string, OrgSummary>();
            Dictionary<string, string> itemMap = new Dictionary<string, string>();

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i); ;
                string map_org_name = item.getProperty("map_org_name", "").Trim().ToLower();

                string in_l1 = item.getProperty("in_l1", "");
                string in_l2 = item.getProperty("in_l2", "");
                string in_l3 = item.getProperty("in_l3", "");
                string in_index = item.getProperty("in_index", "");

                string in_sno = item.getProperty("in_sno", "");
                string in_name = item.getProperty("in_name", "");

                string in_creator_name = item.getProperty("in_creator", "").Trim();
                string in_creator_sno = item.getProperty("in_creator_sno", "").Trim().ToLower();
                string in_creator_tel = item.getProperty("in_creator_tel", "").Trim().ToLower();

                string in_expense = item.getProperty("in_expense", "");

                string in_team_key = in_l1 + "-" + in_l2 + "-" + in_l3 + "-" + in_index;

                //是否為選手
                bool is_player = in_l1 != "隊職員";

                //單位摘要
                OrgSummary org = GetAndAddOrgSummary(orgMap, map_org_name);

                //協助報名者
                CreatorSummary creator = GetCreatorSummary(org.CreatorMap, in_creator_sno, in_creator_name, in_creator_tel);

                SetSummary(org, is_player, in_l1, in_name, in_sno, in_team_key, in_expense);

                SetSummary(creator, is_player, in_l1, in_name, in_sno, in_team_key, in_expense);
            }

            //統計報名費用(單位與協助報名者)
            SetSummarySubTotal(orgMap);

            GameSummary result = new GameSummary
            {
                OrgMap = orgMap,
                ItemMap = itemMap,
                TotalAmount = orgMap.Sum(x => x.Value.Subtotal)
            };

            return result;
        }

        /// <summary>
        /// 設定競賽項目
        /// </summary>
        private void SetItemMap(Dictionary<string, string> itemMap, bool is_player, string in_l1)
        {
            if (is_player)
            {
                if (!itemMap.ContainsKey(in_l1))
                {
                    itemMap.Add(in_l1, in_l1);
                }
            }
        }

        /// <summary>
        /// 設定摘要
        /// </summary>
        private void SetSummary(TSummary entity, bool is_player, string in_l1, string in_name, string in_sno, string in_team_key, string in_expense)
        {
            if (!is_player)
            {
                //隊職員人數
                if (!entity.ManagerMap.ContainsKey(in_name))
                {
                    entity.ManagerMap.Add(in_name, in_name);
                }
            }
            else
            {
                //選手人數
                if (!entity.PlayerMap.ContainsKey(in_sno))
                {
                    entity.PlayerMap.Add(in_sno, in_sno);
                }

                //加入競賽項目、組別
                AddSubItemMap(entity, in_l1, in_team_key);
            }

            //項目總計
            if (!entity.PlayItemMap.ContainsKey(in_team_key))
            {
                entity.PlayItemMap.Add(in_team_key, in_expense);
            }
        }

        /// <summary>
        /// 加入競賽項目、組別
        /// </summary>
        private void AddSubItemMap(TSummary entity, string in_l1, string in_team_index)
        {
            Dictionary<string, string> map = null;
            if (entity.SubItemMap.ContainsKey(in_l1))
            {
                map = entity.SubItemMap[in_l1];
            }
            else
            {
                map = new Dictionary<string, string>();
                entity.SubItemMap.Add(in_l1, map);
            }

            if (!map.ContainsKey(in_team_index))
            {
                map.Add(in_team_index, in_team_index);
            }
        }

        /// <summary>
        /// 統計報名費用
        /// </summary>
        private void SetSummarySubTotal(Dictionary<string, OrgSummary> orgMap)
        {
            foreach (KeyValuePair<string, OrgSummary> org_kv in orgMap)
            {
                OrgSummary org = org_kv.Value;
                org.Subtotal = GetSubTotal(org.PlayItemMap);

                foreach (KeyValuePair<string, CreatorSummary> creator_kv in org.CreatorMap)
                {
                    CreatorSummary creator = creator_kv.Value;
                    creator.Subtotal = GetSubTotal(creator.PlayItemMap);
                }
            }
        }

        /// <summary>
        /// 統計報名費用
        /// </summary>
        private int GetSubTotal(Dictionary<string, string> dictionary)
        {
            int subtotal = 0;
            foreach (KeyValuePair<string, string> kv in dictionary)
            {
                string in_team_index = kv.Key;
                string in_expense = kv.Value;
                if (in_team_index == "" || in_expense == "")
                {
                    continue;
                }

                int expense = 0;
                if (Int32.TryParse(in_expense, out expense))
                {
                    subtotal += expense;
                }
            }
            return subtotal;
        }

        /// <summary>
        /// 取得單位摘要
        /// </summary>
        private OrgSummary GetAndAddOrgSummary(Dictionary<string, OrgSummary> dictionary, string name)
        {
            if (dictionary.ContainsKey(name))
            {
                return dictionary[name];
            }
            else
            {
                OrgSummary entity = new OrgSummary
                {
                    Name = name,
                    ManagerMap = new Dictionary<string, string>(),
                    PlayerMap = new Dictionary<string, string>(),
                    PlayItemMap = new Dictionary<string, string>(),
                    SubItemMap = new Dictionary<string, Dictionary<string, string>>(),
                    Subtotal = 0,
                    CreatorMap = new Dictionary<string, CreatorSummary>()
                };

                dictionary.Add(name, entity);

                return entity;
            }
        }

        /// <summary>
        /// 取得協助報名者摘要
        /// </summary>
        private CreatorSummary GetCreatorSummary(Dictionary<string, CreatorSummary> dictionary, string key, string name, string tel)
        {
            if (dictionary.ContainsKey(key))
            {
                return dictionary[key];
            }
            else
            {
                CreatorSummary entity = new CreatorSummary
                {
                    SID = key,
                    Name = name,
                    TelNumber = tel,
                    ManagerMap = new Dictionary<string, string>(),
                    PlayerMap = new Dictionary<string, string>(),
                    PlayItemMap = new Dictionary<string, string>(),
                    SubItemMap = new Dictionary<string, Dictionary<string, string>>(),
                    Subtotal = 0,
                };

                dictionary.Add(key, entity);

                return entity;
            }
        }/// <summary>
         /// 轉為一階字典清單
         /// </summary>
        private Dictionary<string, List<Item>> MapToDicList1(TConfig cfg, Item items, string property1)
        {
            Dictionary<string, List<Item>> dicitonary = new Dictionary<string, List<Item>>();

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i); ;
                string key1 = item.getProperty(property1, "");

                List<Item> lv1 = null;
                if (dicitonary.ContainsKey(key1))
                {
                    lv1 = dicitonary[key1];
                }
                else
                {
                    lv1 = new List<Item>();
                    dicitonary.Add(key1, lv1);
                }

                lv1.Add(item);
            }

            return dicitonary;
        }

        /// <summary>
        /// 轉為三階資料字典
        /// </summary>
        private Dictionary<string, Dictionary<string, Dictionary<string, List<Item>>>> MapToDictionary3(TConfig cfg, Item items, string[] properties)
        {
            Dictionary<string, Dictionary<string, Dictionary<string, List<Item>>>> dicitonary = new Dictionary<string, Dictionary<string, Dictionary<string, List<Item>>>>();

            string property1 = properties[0];
            string property2 = properties[1];
            string property3 = properties[2];

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i); ;
                string key1 = item.getProperty(property1, "");
                string key2 = item.getProperty(property2, "");
                string key3 = item.getProperty(property3, "");

                Dictionary<string, Dictionary<string, List<Item>>> lv1 = null;
                if (dicitonary.ContainsKey(key1))
                {
                    lv1 = dicitonary[key1];
                }
                else
                {
                    lv1 = new Dictionary<string, Dictionary<string, List<Item>>>();
                    dicitonary.Add(key1, lv1);
                }

                Dictionary<string, List<Item>> lv2 = null;
                if (lv1.ContainsKey(key2))
                {
                    lv2 = lv1[key2];
                }
                else
                {
                    lv2 = new Dictionary<string, List<Item>>();
                    lv1.Add(key2, lv2);
                }


                List<Item> lv3 = null;
                if (lv2.ContainsKey(key3))
                {
                    lv3 = lv2[key3];
                }
                else
                {
                    lv3 = new List<Item>();
                    lv2.Add(key3, lv3);
                }

                lv3.Add(item);
            }

            return dicitonary;
        }

        /// <summary>
        /// 報名總表
        /// </summary>
        private class GameSummary
        {
            /// <summary>
            /// 單位摘要
            /// </summary>
            public Dictionary<string, OrgSummary> OrgMap { get; set; }

            /// <summary>
            /// 競賽項目
            /// </summary>
            public Dictionary<string, string> ItemMap { get; set; }

            /// <summary>
            /// 報名總費用
            /// </summary>
            public int TotalAmount { get; set; }
        }

        /// <summary>
        /// 項目資料型別
        /// </summary>
        private class ItemSummary
        {
            public string Value { get; set; }

            public int SortOrder { get; set; }
        }

        /// <summary>
        /// 單位摘要
        /// </summary>
        private class OrgSummary : TSummary
        {
            /// <summary>
            /// 所屬單位
            /// </summary>
            public string Name { get; set; }

            /// <summary>
            /// 協助報名者列表 (目前用姓名，之後可用身分證號當 key)
            /// </summary>
            public Dictionary<string, CreatorSummary> CreatorMap { get; set; }

            /// <summary>
            /// 已繳費用
            /// </summary>
            public int PaidAmount { get; set; }

            /// <summary>
            /// 對帳狀態
            /// </summary>
            public string PaidStatus { get; set; }

            /// <summary>
            /// 繳費清單
            /// </summary>
            public List<Item> PayList { get; set; }
        }

        /// <summary>
        /// 協助報名者摘要
        /// </summary>
        private class CreatorSummary : TSummary
        {
            /// <summary>
            /// 協助報名者姓名
            /// </summary>
            public string Name { get; set; }

            /// <summary>
            /// 身分證號
            /// </summary>
            public string SID { get; set; }

            /// <summary>
            /// 聯絡手機
            /// </summary>
            public string TelNumber { get; set; }
        }

        /// <summary>
        /// 統計摘要模板
        /// </summary>
        private class TSummary
        {
            /// <summary>
            /// 隊職員人數
            /// </summary>
            public Dictionary<string, string> ManagerMap { get; set; }

            /// <summary>
            /// 選手人數
            /// </summary>
            public Dictionary<string, string> PlayerMap { get; set; }

            /// <summary>
            /// 項目總計
            /// </summary>
            public Dictionary<string, string> PlayItemMap { get; set; }

            /// <summary>
            /// 競賽項目各項統計
            /// </summary>
            public Dictionary<string, Dictionary<string, string>> SubItemMap { get; set; }

            /// <summary>
            /// 報名費用
            /// </summary>
            public int Subtotal { get; set; }
        }

        #endregion 單位統計

        #region 項目統計

        /// <summary>
        /// 轉換為項目總表
        /// </summary>
        private ProgramSummary MapProgramSummary(TConfig cfg, Item items, Dictionary<string, List<Item>> musers)
        {
            ProgramSummary result = new ProgramSummary
            {
                Statistics = new Dictionary<string, TMap>(),
                Reports = new Dictionary<string, TReport>(),
            };

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                AppendStatistics(result.Statistics, item);
                AppendReports(result.Reports, item, musers);
            }


            return result;
        }

        /// <summary>
        /// 附加競賽項目統計
        /// </summary>
        private void AppendStatistics(Dictionary<string, TMap> statistics, Item item)
        {
            string key1 = item.getProperty("in_l1", "");
            string key2 = item.getProperty("program_name", "");

            TMap map1 = null;
            if (statistics.ContainsKey(key1))
            {
                map1 = statistics[key1];
            }
            else
            {
                map1 = new TMap
                {
                    Key = key1,
                    Map = new Dictionary<string, List<Item>>()
                };
                statistics.Add(key1, map1);
            }

            List<Item> sub = null;
            if (map1.Map.ContainsKey(key2))
            {
                sub = map1.Map[key2];
            }
            else
            {
                sub = new List<Item>();
                map1.Map.Add(key2, sub);
            }
            sub.Add(item);
        }

        /// <summary>
        /// 附加競賽項目報表
        /// </summary>
        private void AppendReports(Dictionary<string, TReport> reports, Item item, Dictionary<string, List<Item>> musers)
        {
            string in_l1 = item.getProperty("in_l1", "");
            string in_l2 = item.getProperty("in_l2", "");
            string in_l3 = item.getProperty("in_l3", "");
            string in_index = item.getProperty("in_index", "");
            string in_creator_sno = item.getProperty("in_creator_sno", "");

            string rkey = in_l1;
            string pkey = item.getProperty("program_name", "");
            string tkey = string.Join("-", new List<string>
            {
                in_l1,
                in_l2,
                in_l3,
                in_index,
                in_creator_sno,
            });

            TReport report = null;
            if (reports.ContainsKey(rkey))
            {
                report = reports[rkey];
            }
            else
            {
                report = new TReport
                {
                    Key = rkey,
                    InL1 = item.getProperty("in_l1", ""),
                    IsMultiPlayers = IsMultiPlayers(rkey),
                    Pages = new Dictionary<string, TPage>(),
                };
                reports.Add(rkey, report);
            }

            TPage page = null;
            if (report.Pages.ContainsKey(pkey))
            {
                page = report.Pages[pkey];
            }
            else
            {
                page = new TPage
                {
                    Key = pkey,
                    Teams = new Dictionary<string, TTeam>(),
                };
                report.Pages.Add(pkey, page);
            }

            TTeam team = null;
            if (page.Teams.ContainsKey(tkey))
            {
                team = page.Teams[tkey];
            }
            else
            {
                team = new TTeam
                {
                    Value = item,
                    Players = GetPlayers(musers, tkey),
                };

                if (team.Players.Count == 1)
                {
                    Item itmPlayer = team.Players[0];
                    team.SignNo = GetIntVal(item.getProperty("in_sign_no", ""));
                    team.Value.setProperty("in_gender", itmPlayer.getProperty("in_gender", ""));
                    team.Value.setProperty("in_birth", itmPlayer.getProperty("in_birth", ""));

                    team.Value.setProperty("in_exe_a1", itmPlayer.getProperty("in_exe_a1", ""));
                    team.Value.setProperty("in_exe_a2", itmPlayer.getProperty("in_exe_a2", ""));
                    team.Value.setProperty("in_exe_a3", itmPlayer.getProperty("in_exe_a3", ""));
                    team.Value.setProperty("in_exe_a4", itmPlayer.getProperty("in_exe_a4", ""));
                }

                page.Teams.Add(tkey, team);
            }
        }

        /// <summary>
        /// 查找隊員清單
        /// </summary>
        private List<Item> GetPlayers(Dictionary<string, List<Item>> map, string key)
        {
            if (map.ContainsKey(key))
            {
                return map[key];
            }
            else
            {
                return new List<Item>();
            }

        }

        /// <summary>
        /// 競賽項目摘要
        /// </summary>
        private class ProgramSummary
        {
            /// <summary>
            /// 統計
            /// </summary>
            public Dictionary<string, TMap> Statistics { get; set; }

            /// <summary>
            /// 報表
            /// </summary>
            public Dictionary<string, TReport> Reports { get; set; }
        }

        /// <summary>
        /// 項
        /// </summary>
        private class TMap
        {
            /// <summary>
            /// 鍵
            /// </summary>
            public string Key { get; set; }

            /// <summary>
            /// 子項
            /// </summary>
            public Dictionary<string, List<Item>> Map { get; set; }
        }

        /// <summary>
        /// 報表
        /// </summary>
        private class TReport
        {
            /// <summary>
            /// 鍵
            /// </summary>
            public string Key { get; set; }

            /// <summary>
            /// 競賽項目
            /// </summary>
            public string InL1 { get; set; }

            /// <summary>
            /// 是否為多人對打
            /// </summary>
            public bool IsMultiPlayers { get; set; }

            /// <summary>
            /// 分頁
            /// </summary>
            public Dictionary<string, TPage> Pages { get; set; }
        }

        private class TPage
        {
            public string Key { get; set; }
            public Dictionary<string, TTeam> Teams { get; set; }
        }

        /// <summary>
        /// 競賽隊伍
        /// </summary>
        private class TTeam
        {
            public string Key { get; set; }

            public int SignNo { get; set; }

            public Item Value { get; set; }

            public List<Item> Players { get; set; }
        }

        #endregion 項目統計

        /// <summary>
        /// 選手合計
        /// </summary>
        private string GetTeamPlayers(List<Item> list)
        {
            int sum = 0;
            for (int i = 0; i < list.Count; i++)
            {
                Item item = list[i];
                string in_team_players = item.getProperty("in_team_players", "0");
                sum += GetIntVal(in_team_players);
            }
            return sum.ToString();
        }

        /// <summary>
        /// 是否為多人對打
        /// </summary>
        private bool IsMultiPlayers(string value)
        {
            if (value.Contains("團體")) return true;
            if (value.Contains("混雙")) return true;
            if (value.Contains("雙人")) return true;
            if (value.Contains("三人")) return true;
            if (value.Contains("五人")) return true;
            if (value.Contains("雙打")) return true;
            if (value.Contains("混合雙打")) return true;

            return false;
        }

        /// <summary>
        /// 用身分證號推算性別
        /// </summary>
        private string GetGenderBySno(string value)
        {
            if (value == "" || value.Length < 2)
            {
                return "";
            }
            if (value[1] == '1') return "男";
            if (value[1] == '2') return "女";
            return "";
        }

        private TExport ExportInfo(TConfig cfg, string in_name)
        {
            Item itmXls = cfg.inn.applyMethod("In_Meeting_Excel_Path", "<in_name>" + in_name + "</in_name>");
            string Export_Path = itmXls.getProperty("export_path", "");
            string Template_Path = itmXls.getProperty("template_path", "");

            TExport result = new TExport
            {
                template_Path = Template_Path,
                export_Path = Export_Path,
            };

            return result;
        }

        //設定資料列
        private void SetItemCell(TConfig cfg, Spire.Xls.Worksheet sheet, int wsRow, Item item, List<TField> fields)
        {
            foreach (var field in fields)
            {
                var cr = field.cs + wsRow;
                var va = "";

                if (field.getValue != null)
                {
                    va = field.getValue(cfg, field, item);
                }
                else if (field.property != "")
                {
                    va = item.getProperty(field.property, "");
                }

                SetCell(sheet, cr, va, field.css);
            }
        }

        //設定資料格
        private void SetCell(Spire.Xls.Worksheet sheet, string cr, string value, TCss css)
        {
            var range = sheet.Range[cr];

            switch (css)
            {
                case TCss.None:
                    range.Text = value;
                    break;
                case TCss.Center:
                    range.Text = value;
                    range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                    break;
                case TCss.Text:
                    range.Text = "'" + value;
                    range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                    break;
                case TCss.Number:
                    range.NumberValue = GetIntVal(value);
                    range.NumberFormat = "0";
                    range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Right;
                    break;
                case TCss.Money:
                    range.NumberValue = GetIntVal(value);
                    range.NumberFormat = "$ #,##0";
                    break;
                case TCss.Day:
                    range.Text = GetDateTimeValue(value, "yyyy/MM/dd");
                    range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                    break;
            }
        }

        private void SetMoney(Spire.Xls.Worksheet sheet, string cr, double value)
        {
            var range = sheet.Range[cr];
            range.NumberValue = value;
            range.NumberFormat = "$ #,##0";
        }

        //設定標題列
        private void SetHead(Spire.Xls.Worksheet sheet, string cr, string value)
        {
            var range = sheet.Range[cr];
            range.Text = value;
            range.Style.Font.IsBold = true;
            range.Style.Font.Color = System.Drawing.Color.White;
            range.Style.Color = System.Drawing.Color.FromArgb(41, 92, 144);//#295C90
            range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
        }

        //設定格線
        private void SetRangeBorder(Spire.Xls.Worksheet sheet, string pos)
        {
            var range = sheet.Range[pos];
            range.BorderInside(Spire.Xls.LineStyleType.Thin, System.Drawing.Color.Black);
            range.BorderAround(Spire.Xls.LineStyleType.Thin, System.Drawing.Color.Black);
        }

        //設定格線
        private void SetRangeBorder(Spire.Xls.Worksheet sheet, List<TField> fields, int mnRow, int wsRow)
        {
            var tbl_ps = fields.First().cs + mnRow;
            var tbl_pe = fields.Last().cs + (wsRow - 1);
            SetRangeBorder(sheet, tbl_ps + ":" + tbl_pe);
        }

        private void MapCharSet(TConfig cfg, List<TField> fields)
        {
            foreach (var field in fields)
            {
                field.cs = cfg.CharSet[field.ci];
            }
        }

        private void MapCharSetAndIndex(TConfig cfg, List<TField> fields)
        {
            for (int i = 0; i < fields.Count; i++)
            {
                var field = fields[i];
                field.ci = i + 1;
                field.cs = cfg.CharSet[field.ci];
            }
        }

        private void SetHeadRow(Spire.Xls.Worksheet sheet, int wsRow, List<TField> fields)
        {
            foreach (var field in fields)
            {
                string cr = field.cs + wsRow;
                SetHead(sheet, cr, field.title);
            }
        }

        private void AutoFit(Spire.Xls.Worksheet sheet, List<TField> fields)
        {
            foreach (var field in fields)
            {
                sheet.Columns[field.ci - 1].AutoFitColumns();
            }
        }

        private void SetColumnWidth(Spire.Xls.Worksheet sheet, List<TField> fields)
        {
            foreach (var field in fields)
            {
                sheet.Columns[field.ci - 1].ColumnWidth = field.width;
            }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public Innosoft.InnovatorHelper InnH { get; set; }

            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }

            public string meeting_id { get; set; }

            public Item itmMeeting { get; set; }
            public string mt_title { get; set; }

            public string[] CharSet { get; set; }
        }

        private class TExport
        {
            public string template_Path { get; set; }
            public string export_Path { get; set; }
        }

        private class TField
        {
            public string title { get; set; }
            public string property { get; set; }
            public TCss css { get; set; }
            public int ci { get; set; }
            public string cs { get; set; }
            public int width { get; set; }
            public Func<TConfig, TField, Item, string> getValue { get; set; }

            public bool is_merge { get; set; }
        }

        private enum TCss
        {
            None = 0,
            Center = 10,
            Text = 100,
            Number = 200,
            Money = 300,
            Day = 400,
        }

        private string[] GetCharSet()
        {
            return new string[] { "", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
        }

        private int GetIntVal(string value, int def = 0)
        {
            if (value == "") return 0;

            int result = def;
            Int32.TryParse(value, out result);
            return result;
        }

        //轉換日期
        private string GetDateTimeValue(string value, string format, bool add8Hours = true)
        {
            if (value == "") return "";
            DateTime dt = Convert.ToDateTime(value);
            if (add8Hours)
            {
                dt = dt.AddHours(8);
            }
            return dt.ToString(format);
        }
    }
}