﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class challenge_in_meeting_allocation : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
            目的: 場地分配
            輸入: meeting_id
            日期: 
                2020-11-30: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]in_meeting_allocation";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "input dom: " + itmR.dom.InnerXml);

            string meeting_id = itmR.getProperty("meeting_id", "");
            string mode = itmR.getProperty("mode", "");

            if (meeting_id == "")
            {
                itmR.setProperty("error_message", "賽事 id 不得為空白");
                return itmR;
            }

            //檢查頁面權限
            Item itmCheckIdentity = inn.applyMethod("In_CheckIdentity", "<method>" + strMethodName + "</method><code>" + "ALL" + "</code>");
            bool isMeetingAdmin = itmCheckIdentity.getProperty("isMeetingAdmin", "") == "1";
            if (!isMeetingAdmin)
            {
                itmR.setProperty("error_message", "您無權限瀏覽此頁面");
                return itmR;
            }

            if (mode == "")
            {
                Query(CCO, strMethodName, inn, itmR, isEdit: false);
            }
            else if (mode == "edit")
            {
                Query(CCO, strMethodName, inn, itmR, isEdit: true);
            }
            else if (mode == "save")
            {
                Save(CCO, strMethodName, inn, itmR);
            }
            else if (mode == "allocate")
            {
                Allocate(CCO, strMethodName, inn, itmR);
            }
            else if (mode == "remove")
            {
                Remove(CCO, strMethodName, inn, itmR);
            }
            else if (mode == "removeDate")
            {
                RemoveDate(CCO, strMethodName, inn, itmR);
            }
            else if (mode == "export")
            {
                Export(CCO, strMethodName, inn, itmR);
            }
            else if (mode == "fix_judo_site")
            {
                FixJudoSite(CCO, strMethodName, inn, itmR);
            }
            return itmR;
        }

        //修正場地
        private void FixJudoSite(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            string meeting_id = itmReturn.getProperty("meeting_id", "");
            string in_date_source = itmReturn.getProperty("in_date", "");
            string in_date_key = GetDateTimeVal(in_date_source, "yyyy-MM-dd");

            UpdateDateAndSite(CCO, strMethodName, inn, meeting_id, in_date_key);

            Item itmSites = GetSites(CCO, strMethodName, inn, meeting_id);

            int site_count = itmSites.getItemCount();

            for (int i = 0; i < site_count; i++)
            {
                Item itmSite = itmSites.getItemByIndex(i);
                UpdateMRTreeNo(CCO, strMethodName, inn, meeting_id, in_date_key, itmSite);
                UpdateCTreeNo(CCO, strMethodName, inn, meeting_id, in_date_key, itmSite);
            }
        }

        //批次更新場地與比賽日期
        private void UpdateDateAndSite(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string meeting_id, string in_date_key)
        {
            string sql = @"
                 UPDATE t1 SET
             	    t1.in_date_key = t3.in_date_key
             	    , t1.in_site = t4.id
             	    , t1.in_site_code = t4.in_code
             	    , t1.in_tree_state = 0
                 FROM
             	    IN_MEETING_PEVENT t1
                 INNER JOIN
                    IN_MEETING_PROGRAM t2
                    ON t2.id = t1.source_id
                 INNER JOIN
             	    IN_MEETING_ALLOCATION t3
             	    ON t3.in_program = t2.id
                 INNER JOIN
             	    IN_MEETING_SITE t4
             	    ON t4.id = t1.in_site
                 WHERE
                    t2.in_meeting = '{#meeting_id}'
             	    AND t3.in_date_key = '{#in_date_key}'
            ";

            sql = sql.Replace("{#meeting_id}", meeting_id)
                .Replace("{#in_date_key}", in_date_key);

            Item itmSQL = inn.applySQL(sql);

        }

        //更新場地序號
        private void UpdateMRTreeNo(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string meeting_id, string in_date_key, Item itmSite)
        {
            TConfig cfg = new TConfig
            {
                CCO = CCO,
                strMethodName = strMethodName,
                inn = inn,
                meeting_id = meeting_id,
                site_id = itmSite.getProperty("id", ""),
                in_date_key = in_date_key,
            };

            List<Item> list = new List<Item>();

            //勝部 8 強以前
            AppendList(list, GetSiteEvents(cfg, EventFilterEnum.main_pre_semi));
            //勝部 4 強
            AppendList(list, GetSiteEvents(cfg, EventFilterEnum.main_semi));

            //敗部 64 強
            AppendList(list, GetSiteEvents(cfg, EventFilterEnum.rpc_64));
            //敗部 32 強
            AppendList(list, GetSiteEvents(cfg, EventFilterEnum.rpc_32));
            //敗部 16 強
            AppendList(list, GetSiteEvents(cfg, EventFilterEnum.rpc_16));
            //敗部 8 強
            AppendList(list, GetSiteEvents(cfg, EventFilterEnum.rpc_8));
            //敗部 4 強
            AppendList(list, GetSiteEvents(cfg, EventFilterEnum.rpc_4));

            //勝部決賽
            AppendList(list, GetSiteEvents(cfg, EventFilterEnum.main_final));

            //敗部決賽
            AppendList(list, GetSiteEvents(cfg, EventFilterEnum.rpc_final));

            for (int i = 0; i < list.Count; i++)
            {
                int no = i + 1;

                Item item = list[i];
                string event_id = item.getProperty("event_id", "");
                string in_site_code = item.getProperty("site_code", "");
                string in_site_no = no.ToString();
                string in_site_id = in_site_code + in_site_no.PadLeft(3, '3');

                string sql = "UPDATE IN_MEETING_PEVENT SET"
                    + " in_site_allocate = '1'"
                    + " , in_site_no = '" + in_site_no + "'"
                    + " , in_site_id = '" + in_site_id + "'"
                    + " , in_tree_no = '" + in_site_no + "'"
                    + " , in_tree_state = '1'"
                    + " WHERE id = '" + event_id + "'";

                Item itmSQL = inn.applySQL(sql);

                if (itmSQL.isError()) throw new Exception("error");
            }
        }

        //更新場地序號(挑戰賽)
        private void UpdateCTreeNo(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string meeting_id, string in_date_key, Item itmSite)
        {
            TConfig cfg = new TConfig
            {
                CCO = CCO,
                strMethodName = strMethodName,
                inn = inn,
                meeting_id = meeting_id,
                site_id = itmSite.getProperty("id", ""),
                in_date_key = in_date_key,
            };

            List<Item> list = new List<Item>();

            //挑戰賽 R1
            AppendList(list, GetSiteEvents(cfg, EventFilterEnum.clg_a1));
            //挑戰賽 R2
            AppendList(list, GetSiteEvents(cfg, EventFilterEnum.clg_a2));
            //挑戰賽 R3
            AppendList(list, GetSiteEvents(cfg, EventFilterEnum.clg_b1));

            for (int i = 0; i < list.Count; i++)
            {
                int no = i + 1;

                Item item = list[i];
                string event_id = item.getProperty("event_id", "");
                string in_site_code = item.getProperty("site_code", "");
                string in_site_no = no.ToString();
                string in_site_id = in_site_code + in_site_no.PadLeft(3, '3');

                string sql = "UPDATE IN_MEETING_PEVENT SET"
                    + " in_site_allocate = '1'"
                    + " , in_site_no = '" + in_site_no + "'"
                    + " , in_site_id = '" + in_site_id + "'"
                    + " , in_tree_no = '" + in_site_no + "'"
                    + " , in_tree_state = '1'"
                    + " WHERE id = '" + event_id + "'";

                Item itmSQL = inn.applySQL(sql);

                if (itmSQL.isError()) throw new Exception("error");
            }
        }

        private void AppendList(List<Item> list, Item items)
        {
            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                list.Add(item);
            }
        }

        private Item GetSites(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string meeting_id)
        {
            string sql = "SELECT * FROM IN_MEETING_SITE WITH(NOLOCK) WHERE in_meeting = '" + meeting_id + "' ORDER BY in_code";
            return inn.applySQL(sql);
        }

        private enum EventFilterEnum
        {
            main_pre_semi = 10,
            main_semi = 20,
            main_final = 30,
            rpc_64 = 110,
            rpc_32 = 120,
            rpc_16 = 130,
            rpc_8 = 140,
            rpc_4 = 150,
            rpc_final = 160,
            clg_a1 = 310,
            clg_a2 = 320,
            clg_b1 = 330,
        }

        private Item GetSiteEvents(TConfig cfg, EventFilterEnum filter)
        {
            string in_battle_type = "";
            string in_tree_name = "";
            string round_code_filter = "";

            switch (filter)
            {
                case EventFilterEnum.main_pre_semi:
                    in_battle_type = "Challenge";
                    in_tree_name = "main";
                    round_code_filter = "AND t4.in_round_code > 4 AND ISNULL(t4.in_tree_no, '') <> ''";
                    break;

                case EventFilterEnum.main_semi:
                    in_battle_type = "Challenge";
                    in_tree_name = "main";
                    round_code_filter = "AND t4.in_round_code = 4 AND ISNULL(t4.in_tree_no, '') <> ''";
                    break;

                case EventFilterEnum.main_final:
                    in_battle_type = "Challenge";
                    in_tree_name = "main";
                    round_code_filter = "AND t4.in_round_code = 2";
                    break;

                case EventFilterEnum.rpc_64:
                    in_battle_type = "Challenge";
                    in_tree_name = "repechage";
                    round_code_filter = "AND t4.in_round_code = 64 AND ISNULL(t4.in_tree_no, '') <> ''";
                    break;

                case EventFilterEnum.rpc_32:
                    in_battle_type = "Challenge";
                    in_tree_name = "repechage";
                    round_code_filter = "AND t4.in_round_code = 32 AND ISNULL(t4.in_tree_no, '') <> ''";
                    break;

                case EventFilterEnum.rpc_16:
                    in_battle_type = "Challenge";
                    in_tree_name = "repechage";
                    round_code_filter = "AND t4.in_round_code = 16 AND ISNULL(t4.in_tree_no, '') <> ''";
                    break;

                case EventFilterEnum.rpc_8:
                    in_battle_type = "Challenge";
                    in_tree_name = "repechage";
                    round_code_filter = "AND t4.in_round_code = 8 AND ISNULL(t4.in_tree_no, '') <> ''";
                    break;

                case EventFilterEnum.rpc_4:
                    in_battle_type = "Challenge";
                    in_tree_name = "repechage";
                    round_code_filter = "AND t4.in_round_code = 4 AND ISNULL(t4.in_tree_no, '') <> ''";
                    break;

                case EventFilterEnum.rpc_final:
                    in_battle_type = "Challenge";
                    in_tree_name = "repechage";
                    round_code_filter = "AND t4.in_round_code = 2";
                    break;

                case EventFilterEnum.clg_a1:
                    in_battle_type = "Challenge";
                    in_tree_name = "challenge-a";
                    round_code_filter = "AND t4.in_tree_id = 'CA01'";
                    break;

                case EventFilterEnum.clg_a2:
                    in_battle_type = "Challenge";
                    in_tree_name = "challenge-a";
                    round_code_filter = "AND t4.in_tree_id = 'CA02'";
                    break;

                case EventFilterEnum.clg_b1:
                    in_battle_type = "Challenge";
                    in_tree_name = "challenge-b";
                    break;
            }

            string sql = @"
                SELECT
	                t2.id				AS 'site_id'
	                , t2.in_code		AS 'site_code'
	                , t2.in_name		AS 'site_name'
	                , t1.in_date_key
                    , t3.id             AS 'program_id'
	                , t3.in_name3       AS 'program_name'
	                , t3.in_battle_type
	                , t4.id             AS 'event_id'
	                , t4.in_round_code
	                , t4.in_tree_id
                FROM 
	                IN_MEETING_ALLOCATION t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_SITE t2 WITH(NOLOCK)
	                ON t2.id = t1.in_site
                INNER JOIN
	                IN_MEETING_PROGRAM t3 WITH(NOLOCK)
	                ON t3.id = t1.in_program
                INNER JOIN
	                IN_MEETING_PEVENT t4 WITH(NOLOCK)
	                ON t4.source_id = t3.id
                WHERE
	                t1.in_meeting = '{#meeting_id}'
	                AND t1.in_site = '{#site_id}'
	                AND t1.in_date_key = '{#in_date_key}'
	                AND t3.in_battle_type = '{#in_battle_type}'
	                AND t4.in_tree_name = N'{#in_tree_name}'
	                {#round_code_filter}
                ORDER BY
	                t2.in_code
	                , t1.created_on
	                , t4.in_round_code DESC
	                , t4.in_tree_sno
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#site_id}", cfg.site_id)
                .Replace("{#in_date_key}", cfg.in_date_key)
                .Replace("{#in_battle_type}", in_battle_type)
                .Replace("{#in_tree_name}", in_tree_name)
                .Replace("{#round_code_filter}", round_code_filter);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        #region 匯出
        //匯出
        private void Export(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            string meeting_id = itmReturn.getProperty("meeting_id", "");

            //取得賽事資訊
            Item itmMeeting = GetMeeting(CCO, strMethodName, inn, meeting_id);
            if (itmMeeting.isError())
            {
                itmReturn.setProperty("error_message", "取得賽事資料發生錯誤");
                return;
            }

            //取得場地資訊
            Item itmSites = GetMeetingSites(CCO, strMethodName, inn, meeting_id);
            if (itmSites.isError())
            {
                itmReturn.setProperty("error_message", "取得場地資訊發生錯誤");
                return;
            }

            //取得場地分配資訊
            Item itmAllocations = GetMeetingAllocations(CCO, strMethodName, inn, meeting_id);
            if (itmAllocations.isError())
            {
                itmReturn.setProperty("error_message", "取得場地分配資訊發生錯誤");
                return;
            }

            //轉換場地分配
            var map = MapAllocation(itmAllocations, itmSites);

            //設定匯出資訊
            string main_name = itmMeeting.getProperty("in_title", "");
            string sub_name = "場次分配表";
            TExport export = GetExportInfo(CCO, strMethodName, inn, main_name, sub_name);

            TConfig cfg = new TConfig
            {
                FontSize = 12,
                FontName = "標楷體",
                RowStart = 3,
                ColStart = 1,
                Title = itmMeeting.getProperty("in_title", ""),
            };

            ClosedXML.Excel.XLWorkbook workbook = new ClosedXML.Excel.XLWorkbook();

            foreach (var kv in map.Allocations)
            {
                var allocation = kv.Value;
                AppendAllocationSheet(CCO, strMethodName, inn, workbook, cfg, map, allocation);
            }

            workbook.SaveAs(export.File);

            itmReturn.setProperty("xls_name", export.Url);
        }

        //場次分配表
        private void AppendAllocationSheet(Aras.Server.Core.CallContext CCO
            , string strMethodName
            , Innovator inn
            , ClosedXML.Excel.XLWorkbook workbook
            , TConfig cfg
            , TMap map
            , TAllocation allocation)
        {
            ClosedXML.Excel.IXLWorksheet sheet = workbook.Worksheets.Add(allocation.DisplayDate);

            //賽事標題
            ClosedXML.Excel.IXLCell cell_title = sheet.Cell(1, cfg.ColStart);
            cell_title.Value = cfg.Title + "場地分配表";
            cell_title.Style.Font.Bold = true;
            cell_title.Style.Font.FontSize = 16;
            cell_title.Style.Font.FontName = cfg.FontName;
            cell_title.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
            cell_title.Style.Alignment.Vertical = ClosedXML.Excel.XLAlignmentVerticalValues.Center;

            sheet.Range(1, cfg.ColStart, 1, cfg.ColStart + map.Cols - 1)
                .Merge()
                .Style.Alignment.Vertical = ClosedXML.Excel.XLAlignmentVerticalValues.Center;

            sheet.Row(1).Height = 50;


            //比賽日期
            ClosedXML.Excel.IXLCell cell_date = sheet.Cell(2, cfg.ColStart);
            cell_date.Value = allocation.ChineseDate;
            cell_date.Style.Font.FontSize = 14;
            cell_date.Style.Font.FontName = cfg.FontName;
            cell_date.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
            cell_date.Style.Alignment.Vertical = ClosedXML.Excel.XLAlignmentVerticalValues.Center;

            sheet.Range(2, cfg.ColStart, 2, cfg.ColStart + map.Cols - 1)
                .Merge()
                .Style.Alignment.Vertical = ClosedXML.Excel.XLAlignmentVerticalValues.Center;

            sheet.Row(2).Height = 30;


            int wsRow = cfg.RowStart;
            int wsCol = cfg.ColStart;

            for (int i = 0; i < map.Rows; i++)
            {
                for (int j = 0; j < map.Cols; j++)
                {
                    string key = i + "_" + j;
                    string value = "";
                    if (map.Sites.ContainsKey(key))
                    {
                        value = map.Sites[key].Name;
                    }
                    SetCell(sheet, wsRow, wsCol + j, cfg, value, format: "head");
                }
                wsRow++;

                for (int j = 0; j < map.Cols; j++)
                {
                    string key = i + "_" + j;
                    if (allocation.SiteGroups.ContainsKey(key))
                    {
                        var list = allocation.SiteGroups[key];
                        string value = string.Join(Environment.NewLine, list.Select(x => x.Label));
                        SetCell(sheet, wsRow, wsCol + j, cfg, value, format: "wrap");
                    }
                }
                wsRow++;
            }

            var width = (int)(84 / map.Cols);
            for (int j = 0; j < map.Cols; j++)
            {
                sheet.Column(wsCol + j).Width = width;
            }
        }

        //設定物件與資料列
        private void SetCell(ClosedXML.Excel.IXLWorksheet sheet, int wsRow, int wsCol, TConfig cfg, string value, string format = "")
        {
            ClosedXML.Excel.IXLCell cell = sheet.Cell(wsRow, wsCol);

            cell.Style.Font.FontSize = cfg.FontSize;
            cell.Style.Font.FontName = cfg.FontName;

            cell.Style.Border.SetRightBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetTopBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetBottomBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetLeftBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);

            switch (format)
            {
                case "head":
                    cell.Value = value;
                    cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    cell.Style.Font.Bold = true;
                    cell.DataType = ClosedXML.Excel.XLDataType.Text;
                    //cell.Style.Fill.BackgroundColor = ClosedXML.Excel.XLColor.FromHtml("#295C90");
                    //cell.Style.Font.FontColor = ClosedXML.Excel.XLColor.White;
                    break;

                case "wrap":
                    cell.Value = value;
                    cell.Style.Alignment.Vertical = ClosedXML.Excel.XLAlignmentVerticalValues.Top;
                    cell.Style.Alignment.WrapText = true;
                    sheet.Columns(wsRow, wsCol).AdjustToContents();
                    break;

                case "center":
                    cell.Value = value;
                    cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    break;

                case "yyyy/MM/dd":
                case "yyyy/MM/dd HH:mm":
                    cell.Value = GetDateTimeVal(value, format);
                    cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    break;

                case "$ #,##0":
                    cell.Value = value;
                    cell.Style.NumberFormat.Format = "$ #,##0";
                    break;

                case "tel":
                    if (value.StartsWith("09"))
                    {
                        cell.Value = value.Replace("-", "");
                        cell.Style.NumberFormat.Format = "0000-000-000";
                        cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    }
                    else
                    {
                        cell.Value = "'" + value;
                        cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    }
                    break;

                case "text":
                    cell.Value = "'" + value;
                    cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    break;

                default:
                    cell.Value = value;
                    break;
            }
        }

        /// <summary>
        /// 匯出資料模型
        /// </summary>
        private class TExport
        {
            /// <summary>
            /// 樣板來源完整路徑
            /// </summary>
            public string Source { get; set; }

            /// <summary>
            /// 檔案名稱
            /// </summary>
            public string File { get; set; }

            /// <summary>
            /// 檔案網址
            /// </summary>
            public string Url { get; set; }
        }

        /// <summary>
        /// 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public Innovator inn { get; set; }

            public string meeting_id { get; set; }
            public string site_id { get; set; }
            public string in_date_key { get; set; }

            /// <summary>
            /// 字型名稱
            /// </summary>
            public string FontName { get; set; }

            /// <summary>
            /// 字型大小
            /// </summary>
            public int FontSize { get; set; }

            /// <summary>
            /// 列起始位置
            /// </summary>
            public int RowStart { get; set; }

            /// <summary>
            /// 欄起始位置
            /// </summary>
            public int ColStart { get; set; }

            /// <summary>
            /// 當前列位置
            /// </summary>
            public int CurrentRow { get; set; }

            /// <summary>
            /// 賽事名稱
            /// </summary>
            public string Title { get; set; }
        }

        /// <summary>
        /// 取得匯出設定資訊
        /// </summary>
        private TExport GetExportInfo(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string main_name, string sub_name)
        {
            Item itmPath = GetXlsPaths(CCO, strMethodName, inn);

            string export_path = itmPath.getProperty("export_path", "").TrimEnd('\\');
            string ext_name = ".xlsx";
            string doc_name = main_name + "_" + sub_name + "_" + DateTime.Now.ToString("yyyyMMdd_HHmmss");

            string doc_file = export_path + "\\" + doc_name + ext_name;
            string doc_url = doc_name + ext_name;

            return new TExport
            {
                Source = itmPath.getProperty("template_path", ""),
                File = doc_file,
                Url = doc_url,
            };
        }

        /// <summary>
        /// 取得樣板與匯出路徑
        /// </summary>
        private Item GetXlsPaths(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string in_name = "")
        {
            Item itmResult = inn.newItem();

            //Excel 匯出路徑 (from 原創.設定參數)
            string sql = @"
        SELECT 
            t2.in_name AS 'variable'
            , t1.in_name
            , t1.in_value 
        FROM 
            In_Variable_Detail t1 WITH(NOLOCK)
        INNER JOIN 
            In_Variable t2 WITH(NOLOCK) 
            ON t2.ID = t1.SOURCE_ID 
        WHERE 
            t2.in_name = N'meeting_excel'  
            AND t1.in_name IN (N'export_path', N'{#in_name}')
        ";

            sql = sql.Replace("{#in_name}", in_name);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item items = inn.applySQL(sql);

            if (items.isError() || items.getItemCount() < 1)
            {
                throw new Exception("未設定樣板與匯出路徑 _# " + in_name);
            }

            for (int i = 0; i < items.getItemCount(); i++)
            {
                Item item = items.getItemByIndex(i);
                string xls_name = item.getProperty("in_name", "");
                string xls_value = item.getProperty("in_value", "");

                if (xls_name == "export_path")
                {
                    itmResult.setProperty("export_path", xls_value);
                }
                else
                {
                    itmResult.setProperty("template_path", xls_value);
                }
            }

            return itmResult;
        }
        #endregion 匯出

        //移除當日場地分配
        private void RemoveDate(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            string sql = "";
            Item itmSQL = null;

            string meeting_id = itmReturn.getProperty("meeting_id", "");
            string in_date_key = itmReturn.getProperty("in_date_key", "");

            sql = "DELETE FROM IN_MEETING_ALLOCATION WHERE in_meeting = '" + meeting_id + "' AND in_date_key = '" + in_date_key + "'";

            itmSQL = inn.applySQL(sql);

            if (itmSQL.isError())
            {
                throw new Exception("移除失敗");
            }
        }

        //移除
        private void Remove(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            string sql = "";
            Item itmSQL = null;

            string meeting_id = itmReturn.getProperty("meeting_id", "");
            string allocation_id = itmReturn.getProperty("allocation_id", "");

            sql = "DELETE FROM IN_MEETING_ALLOCATION WHERE id = '" + allocation_id + "'";

            itmSQL = inn.applySQL(sql);

            if (itmSQL.isError())
            {
                throw new Exception("移除失敗");
            }
        }

        //儲存
        private void Save(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            string sql = "";
            Item itmSQL = null;

            string meeting_id = itmReturn.getProperty("meeting_id", "");
            string in_date_source = itmReturn.getProperty("in_date", "");
            string in_l1 = itmReturn.getProperty("in_l1", "");
            string in_l2 = itmReturn.getProperty("in_l2", "");

            string in_date = GetDateTimeVal(in_date_source, "yyyy-MM-ddTHH:mm:ss", add_hours: 0);
            string in_date_key = GetDateTimeVal(in_date_source, "yyyy-MM-dd");

            sql = @"
        SELECT
	        *
        FROM
	        IN_MEETING_ALLOCATION WITH(NOLOCK)
        WHERE
	        in_meeting = '{#meeting_id}'
	        AND in_date_key = '{#in_date_key}'
	        AND in_l1 = N'{#in_l1}'
	        AND in_l2 = N'{#in_l2}'
        ";

            sql = sql.Replace("{#meeting_id}", meeting_id)
                .Replace("{#in_date_key}", in_date_key)
                .Replace("{#in_l1}", in_l1)
                .Replace("{#in_l2}", in_l2);

            itmSQL = inn.applySQL(sql);
            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            if (itmSQL.getItemCount() > 0)
            {
                throw new Exception("該組別已加入");
            }

            Item itmAllocation = inn.newItem("In_Meeting_Allocation");
            itmAllocation.setProperty("in_meeting", meeting_id);
            itmAllocation.setProperty("in_type", "category");
            itmAllocation.setProperty("in_date_key", in_date_key);
            itmAllocation.setProperty("in_date", in_date);
            itmAllocation.setProperty("in_l1", in_l1);
            itmAllocation.setProperty("in_l2", in_l2);
            itmAllocation = itmAllocation.apply("add");
        }

        //儲存
        private void Allocate(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            string sql = "";
            Item itmSQL = null;

            string meeting_id = itmReturn.getProperty("meeting_id", "");
            string in_date_source = itmReturn.getProperty("in_date_key", "");
            string in_site = itmReturn.getProperty("in_site", "");
            string in_program = itmReturn.getProperty("in_program", "");

            string in_date = GetDateTimeVal(in_date_source, "yyyy-MM-ddTHH:mm:ss", add_hours: 0);
            string in_date_key = in_date_source;

            sql = @"
        SELECT
	        *
        FROM
	        IN_MEETING_ALLOCATION WITH(NOLOCK)
        WHERE
	        in_meeting = '{#meeting_id}'
	        AND in_date_key = '{#in_date_key}'
	        AND in_site = N'{#in_site}'
	        AND in_program = N'{#in_program}'
        ";

            sql = sql.Replace("{#meeting_id}", meeting_id)
                .Replace("{#in_date_key}", in_date_key)
                .Replace("{#in_site}", in_site)
                .Replace("{#in_program}", in_program);

            itmSQL = inn.applySQL(sql);
            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            if (itmSQL.getItemCount() > 0)
            {
                throw new Exception("該組別已加入");
            }

            Item itmAllocation = inn.newItem("In_Meeting_Allocation");
            itmAllocation.setProperty("in_meeting", meeting_id);
            itmAllocation.setProperty("in_type", "fight");
            itmAllocation.setProperty("in_date_key", in_date_key);
            itmAllocation.setProperty("in_date", in_date);
            itmAllocation.setProperty("in_site", in_site);
            itmAllocation.setProperty("in_program", in_program);
            itmAllocation = itmAllocation.apply("add");

            if (itmAllocation.isError())
            {
                throw new Exception("加入失敗");
            }
            else
            {
                Item itmSite = inn.applySQL("SELECT * FROM IN_MEETING_SITE WITH(NOLOCK) WHERE id = '" + in_site + "'");
                string in_site_code = itmSite.getProperty("in_code");

                string allocation_id = itmAllocation.getProperty("id", "");
                string program_id = itmAllocation.getProperty("in_program", "");
                sql = "UPDATE IN_MEETING_PROGRAM SET in_site_code = '" + in_site_code + "', in_allocation = '" + allocation_id + "' WHERE id = '" + program_id + "'";
                inn.applySQL(sql);

                Item itmProgram = inn.applySQL("SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + program_id + "'");

                itmReturn.setProperty("allocation_id", allocation_id);
                itmReturn.setProperty("program_id", program_id);
                itmReturn.setProperty("program_name2", itmProgram.getProperty("in_name2", ""));
                itmReturn.setProperty("program_team_count", itmProgram.getProperty("in_team_count", ""));
            }
        }

        //查詢
        private void Query(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn, bool isEdit = false)
        {
            string meeting_id = itmReturn.getProperty("meeting_id", "");

            Item itmMeeting = GetMeeting(CCO, strMethodName, inn, meeting_id);
            if (itmMeeting.isError())
            {
                itmReturn.setProperty("error_message", "取得賽事資料發生錯誤");
                return;
            }

            //附加賽事資訊
            AppendMeeting(itmMeeting, itmReturn);

            if (isEdit)
            {
                //附加賽事選單
                Item itmJson = inn.newItem("In_Meeting");
                itmJson.setProperty("meeting_id", meeting_id);
                itmJson.setProperty("need_id", "1");
                itmJson = itmJson.apply("in_meeting_program_options");
                itmReturn.setProperty("in_level_json", itmJson.getProperty("json", ""));
                itmReturn.setProperty("in_level_count", itmJson.getProperty("total", ""));
            }

            //取得場地資訊
            Item itmSites = GetMeetingSites(CCO, strMethodName, inn, meeting_id);
            if (itmSites.isError())
            {
                itmReturn.setProperty("error_message", "取得場地資訊發生錯誤");
                return;
            }

            //取得場地分配資訊
            Item itmAllocations = GetMeetingAllocations(CCO, strMethodName, inn, meeting_id);
            if (itmAllocations.isError())
            {
                itmReturn.setProperty("error_message", "取得場地分配資訊發生錯誤");
                return;
            }

            var map = MapAllocation(itmAllocations, itmSites, isEdit);

            //附加場地分配資訊
            AppendAllocations(CCO, strMethodName, inn, map, itmReturn);
        }

        //附加場地分配資訊
        private void AppendAllocations(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, TMap map, Item itmReturn)
        {
            StringBuilder tabs = new StringBuilder();
            StringBuilder contents = new StringBuilder();

            //頁籤
            int no = 1;
            tabs.Append("<ul class='nav nav-tabs'>");
            foreach (var kv in map.Allocations)
            {
                var active = no == 1 ? "class='active'" : "";
                var allocation = kv.Value;

                tabs.Append("<li " + active + "><a data-toggle='tab' href='#" + allocation.Key + "'>" + allocation.DisplayDate + "</a></li>");

                no++;
            }
            tabs.Append("</ul>");

            no = 1;
            contents.Append("<div class='tab-content'>");
            foreach (var kv in map.Allocations)
            {
                var active = no == 1 ? "in active" : "";
                var allocation = kv.Value;

                contents.Append("<div id='" + allocation.Key + "' class='tab-pane fade " + active + "'>");
                AppendAllocationTable(contents, map, allocation);
                contents.Append("</div>");

                no++;
            }
            contents.Append("</div>");

            StringBuilder builder = new StringBuilder();
            builder.Append("<div class='col-lg-12'>");
            builder.Append(tabs);
            builder.Append(contents);
            builder.Append("</div>");
            itmReturn.setProperty("inn_tables", builder.ToString());
        }

        private void AppendAllocationTable(StringBuilder builder, TMap map, TAllocation allocation)
        {
            StringBuilder body = new StringBuilder();

            string remove_icon = " <i class='fa fa-remove' style='color: red;' data-type='category' data-aid='{#allocation_id}' onclick='Remove_Allocation(this)' ></i>";

            if (!map.IsEdit)
            {
                remove_icon = "";
            }

            for (int i = 0; i < map.Rows; i++)
            {
                //標題
                body.Append("  <tr>");
                for (int j = 0; j < map.Cols; j++)
                {
                    string key = i + "_" + j;
                    if (map.Sites.ContainsKey(key))
                    {
                        TSite site = map.Sites[key];
                        body.Append("    <td class='text-center btn-primary site-name'><b>" + site.Name + "</b></td>");
                    }
                    else
                    {
                        body.Append("    <td class='text-center'></td>");
                    }
                }
                body.Append("  </tr>");

                //當日對打量級
                body.Append("  <tr>");
                for (int j = 0; j < map.Cols; j++)
                {
                    string key = i + "_" + j;

                    if (map.Sites.ContainsKey(key))
                    {
                        TSite site = map.Sites[key];
                        body.Append("<td class='text-center'>");
                        body.Append(GetGroupSelectCtrl(map, site, allocation));
                        body.Append(GetGroupSpanCtrl(map, site, allocation));
                        body.Append("</td>");
                    }
                    else
                    {
                        body.Append("<td class='text-center'></td>");
                    }
                }
                body.Append("  </tr>");
            }


            builder.AppendLine("<div class='box-body row container-row' >");
            builder.AppendLine("    <div class='box box-fix'>");

            builder.AppendLine("      <div class='box-header with-border text-center'>");
            builder.AppendLine("        <h3 class='box-title' style='margin-bottom: 10px'>");
            builder.AppendLine("          <b>" + allocation.ChineseDate + GetRemoveDateCtrl(map, allocation) + "</b>");
            builder.AppendLine("        </h3>");

            if (map.IsEdit)
            {
                //當日對打組別
                for (int i = 0; i < allocation.Categories.Count; i++)
                {
                    TCategory category = allocation.Categories[i];
                    builder.Append("<p class='in_category' data-inl1='" + category.InL1 + "' data-inl2='" + category.InL2 + "' style='display:none'>"
                        + category.Label + remove_icon.Replace("{#allocation_id}", category.Id)
                        + "</p>");
                }
            }
            builder.AppendLine("      </div>");

            builder.AppendLine("      <div class='box-body'>");
            builder.AppendLine("        <table class='table' data-toggle='table' style='background-color: #fff;'>");
            builder.Append("              <tbody>");
            builder.Append(body);
            builder.Append("              </tbody>");
            builder.AppendLine("        </table class='table'>");
            builder.AppendLine("      </div>");
            builder.AppendLine("    </div>");
            builder.AppendLine("</div>");
        }

        private string GetRemoveDateCtrl(TMap map, TAllocation allocation)
        {
            if (map.IsEdit)
            {
                return " <i class='fa fa-remove' style='color: red' data-did='" + allocation.Key + "' onclick='Remove_Date(this)'></i>";
            }
            else
            {
                return "";
            }
        }

        private string GetGroupSpanCtrl(TMap map, TSite site, TAllocation allocation)
        {
            List<TGroup> list = null;
            if (allocation.SiteGroups.ContainsKey(site.Key))
            {
                list = allocation.SiteGroups[site.Key];
            }

            if (list == null || list.Count == 0)
            {
                return "";
            }

            StringBuilder builder = new StringBuilder();
            foreach (var group in list)
            {
                builder.Append("<p>");
                builder.Append(group.LabelCnt);

                if (map.IsEdit)
                {
                    builder.Append(" <i class='fa fa-remove' style='color: red' data-aid='" + group.Id + "' data-pid='" + group.ProgramId + "' onclick='Remove_Allocation(this)'></i>");
                }

                builder.Append("</p>");
            }
            return builder.ToString();
        }

        private string GetGroupSelectCtrl(TMap map, TSite site, TAllocation allocation)
        {
            if (map.IsEdit)
            {
                StringBuilder builder = new StringBuilder();
                builder.Append("<select class='form-control ctrl_program' data-sid='" + site.Id + "' data-did='" + allocation.Key + "' onchange='Program_Change(this)'>");
                builder.Append("<option value=''>請選擇</option>");
                builder.Append("</select>");
                return builder.ToString();
            }
            else
            {
                return "";
            }
        }

        /// <summary>
        /// 轉換場地分配資訊
        /// </summary>
        private TMap MapAllocation(Item itmAllocations, Item itmSites, bool isEdit = false)
        {
            Item itmFirstSite = itmSites.getItemByIndex(0);

            TMap map = new TMap
            {
                IsEdit = isEdit,
                Rows = GetIntVal(itmFirstSite.getProperty("in_rows", "")),
                Cols = GetIntVal(itmFirstSite.getProperty("in_cols", "")),
                Sites = new Dictionary<string, TSite>(),
                Allocations = new Dictionary<string, TAllocation>(),
            };

            int site_count = itmSites.getItemCount();

            for (int i = 0; i < site_count; i++)
            {
                Item itmSite = itmSites.getItemByIndex(i);
                string id = itmSite.getProperty("id", "");
                string in_code = itmSite.getProperty("in_code", "");
                string in_name = itmSite.getProperty("in_name", "");
                string in_row_index = itmSite.getProperty("in_row_index", "");
                string in_col_index = itmSite.getProperty("in_col_index", "");
                string key = in_row_index + "_" + in_col_index;

                if (!map.Sites.ContainsKey(key))
                {
                    map.Sites.Add(key, new TSite
                    {
                        Key = key,
                        Id = id,
                        Code = in_code,
                        Name = in_name,
                    });
                }
            }

            int allocate_count = itmAllocations.getItemCount();

            for (int i = 0; i < allocate_count; i++)
            {
                Item itmAllocation = itmAllocations.getItemByIndex(i);
                string id = itmAllocation.getProperty("id", "");

                string in_type = itmAllocation.getProperty("in_type", "");
                string in_date_key = itmAllocation.getProperty("in_date_key", "");
                string in_date = GetDateTimeVal(itmAllocation.getProperty("in_date", ""), "yyy-MM-dd", 8);
                string chinese_date = GetDateTimeVal(itmAllocation.getProperty("in_date", ""), "chinese", 8);

                //分類用
                string in_l1 = itmAllocation.getProperty("in_l1", "");
                string in_l2 = itmAllocation.getProperty("in_l2", "");

                //場地分配
                string sid = itmAllocation.getProperty("sid", "");
                string in_row_index = itmAllocation.getProperty("in_row_index", "");
                string in_col_index = itmAllocation.getProperty("in_col_index", "");
                string skey = in_row_index + "_" + in_col_index;

                //組別分配
                string pid = itmAllocation.getProperty("pid", "");
                string pl1 = itmAllocation.getProperty("pl1", "");
                string pl2 = itmAllocation.getProperty("pl2", "");
                string pl3 = itmAllocation.getProperty("pl3", "");
                string pname = itmAllocation.getProperty("pname", "");
                string pname2 = itmAllocation.getProperty("pname2", "");
                string pname3 = itmAllocation.getProperty("pname3", "");
                string pcnt = itmAllocation.getProperty("pcnt", "");

                TAllocation entity = null;
                if (map.Allocations.ContainsKey(in_date_key))
                {
                    entity = map.Allocations[in_date_key];
                }
                else
                {
                    entity = new TAllocation
                    {
                        Key = in_date_key,
                        DisplayDate = in_date,
                        ChineseDate = chinese_date,
                        Categories = new List<TCategory>(),
                        SiteGroups = new Dictionary<string, List<TGroup>>(),
                    };
                    map.Allocations.Add(in_date_key, entity);
                }

                if (in_type == "category")
                {
                    entity.Categories.Add(new TCategory
                    {
                        Id = id,
                        InL1 = in_l1,
                        InL2 = in_l2,
                        Label = in_l1 + "-" + in_l2,
                    }); ;
                }
                else
                {
                    List<TGroup> groups = null;
                    if (entity.SiteGroups.ContainsKey(skey))
                    {
                        groups = entity.SiteGroups[skey];
                    }
                    else
                    {
                        groups = new List<TGroup>();
                        entity.SiteGroups.Add(skey, groups);
                    }
                    groups.Add(new TGroup
                    {
                        Id = id,
                        SiteId = sid,
                        ProgramId = pid,
                        DateKey = in_date_key,
                        Label = pname2,
                        LabelCnt = pname2 + " (" + pcnt + ")",
                    });
                }
            }

            return map;
        }

        /// <summary>
        /// 場地分配資料模型
        /// </summary>
        private class TMap
        {
            /// <summary>
            /// 是否為編輯狀態
            /// </summary>
            public bool IsEdit { get; set; }

            /// <summary>
            /// 列數
            /// </summary>
            public int Rows { get; set; }

            /// <summary>
            /// 欄數
            /// </summary>
            public int Cols { get; set; }

            /// <summary>
            /// 場地資料
            /// </summary>
            public Dictionary<string, TSite> Sites { get; set; }

            /// <summary>
            /// 日期資料
            /// </summary>
            public Dictionary<string, TAllocation> Allocations { get; set; }
        }

        private class TSite
        {
            /// <summary>
            /// 鍵
            /// </summary>
            public string Key { get; set; }

            /// <summary>
            /// 系統編號
            /// </summary>
            public string Id { get; set; }

            /// <summary>
            /// 場地代號
            /// </summary>
            public string Code { get; set; }

            /// <summary>
            /// 場地名稱
            /// </summary>
            public string Name { get; set; }

        }

        private class TAllocation
        {
            /// <summary>
            /// 鍵
            /// </summary>
            public string Key { get; set; }

            /// <summary>
            /// 日期呈現 ex: 2020-11-16
            /// </summary>
            public string DisplayDate { get; set; }

            /// <summary>
            /// 日期呈現 ex: 109年11月20日(星期五)
            /// </summary>
            public string ChineseDate { get; set; }

            /// <summary>
            /// 當日比賽組別
            /// </summary>
            public List<TCategory> Categories { get; set; }

            /// <summary>
            /// 當日比賽場地組別
            /// </summary>
            public Dictionary<string, List<TGroup>> SiteGroups { get; set; }
        }

        private class TCategory
        {
            public string Id { get; set; }
            public string InL1 { get; set; }
            public string InL2 { get; set; }
            public string Label { get; set; }
        }

        private class TGroup
        {
            /// <summary>
            /// Allocation Id
            /// </summary>
            public string Id { get; set; }

            /// <summary>
            /// Site Id
            /// </summary>
            public string SiteId { get; set; }

            /// <summary>
            /// Program Id
            /// </summary>
            public string ProgramId { get; set; }

            /// <summary>
            /// Date Key
            /// </summary>
            public string DateKey { get; set; }

            /// <summary>
            /// 組別-量級
            /// </summary>
            public string Label { get; set; }

            /// <summary>
            /// 組別-量級
            /// </summary>
            public string LabelCnt { get; set; }
        }

        //附加賽事資訊
        private void AppendMeeting(Item itmMeeting, Item itmReturn)
        {
            itmReturn.setProperty("in_title", itmMeeting.getProperty("in_title", ""));
            itmReturn.setProperty("in_battle_type", itmMeeting.getProperty("in_battle_type", ""));
            itmReturn.setProperty("in_surface_code", itmMeeting.getProperty("in_surface_code", ""));
        }

        //取得賽事資訊
        private Item GetMeeting(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string meeting_id)
        {
            string aml = @"
        <AML>
            <Item type='In_Meeting' action='get' id='{#meeting_id}' select='in_title,in_battle_type,in_surface_code'>
            </Item>
        </AML>
        ".Replace("{#meeting_id}", meeting_id);

            return inn.applyAML(aml);
        }

        //取得分配場地資訊
        private Item GetMeetingAllocations(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string meeting_id)
        {
            string sql = @"
        SELECT
            t1.*
			, t2.id AS 'sid'
			, t2.in_row_index
			, t2.in_col_index
			, t3.id    AS 'pid'
			, t3.in_l1 AS 'pl1'
			, t3.in_l2 AS 'pl2'
			, t3.in_l3 AS 'pl3'
			, t3.in_name AS 'pname'
			, t3.in_name2 AS 'pname2'
			, t3.in_name3 AS 'pname3'
			, t3.in_team_count AS 'pcnt'
        FROM
            IN_MEETING_ALLOCATION t1 WITH(NOLOCK)
		LEFT OUTER JOIN
			IN_MEETING_SITE t2 WITH(NOLOCK)
			ON t2.in_meeting = t1.in_meeting
			AND t2.id = t1.in_site
		LEFT OUTER JOIN
			IN_MEETING_PROGRAM t3 WITH(NOLOCK)
			ON t3.in_meeting = t1.in_meeting
			AND t3.id = t1.in_program
        WHERE
            t1.in_meeting = '{#meeting_id}'
        ORDER BY
            t1.in_date
            , t1.created_on
        ".Replace("{#meeting_id}", meeting_id);

            return inn.applySQL(sql);
        }

        //取得賽事場地資訊
        private Item GetMeetingSites(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string meeting_id)
        {
            string sql = @"
        SELECT
            *
        FROM
            IN_MEETING_SITE
        WHERE
            in_meeting = '{#meeting_id}'
        ".Replace("{#meeting_id}", meeting_id);

            return inn.applySQL(sql);
        }

        private string GetJsonValue(Newtonsoft.Json.Linq.JObject obj, string key)
        {
            Newtonsoft.Json.Linq.JToken value;
            if (obj.TryGetValue(key, out value))
            {
                return value.ToString();
            }
            return "";
        }

        private int GetIntVal(string value, int def = 0)
        {
            int result = def;
            Int32.TryParse(value, out result);
            return result;
        }

        private string GetDateTimeVal(string value, string format, int add_hours = 0)
        {
            DateTime result = DateTime.MinValue;
            DateTime.TryParse(value, out result);
            if (result != DateTime.MinValue)
            {
                if (format == "chinese")
                {
                    return ChineseDateTime(result.AddHours(add_hours));
                }
                else
                {
                    return result.AddHours(add_hours).ToString(format);
                }
            }
            else
            {
                return "";
            }
        }

        /// <summary>
        /// 轉換為民國年月日(星期)
        /// </summary>
        private string ChineseDateTime(DateTime value)
        {
            var y = value.Year - 1911;
            var m = value.Month.ToString().PadLeft(2, '0');
            var d = value.Day.ToString().PadLeft(2, '0');
            var w = "";

            switch (value.DayOfWeek)
            {
                case DayOfWeek.Monday:
                    w = "星期一";
                    break;
                case DayOfWeek.Tuesday:
                    w = "星期二";
                    break;
                case DayOfWeek.Wednesday:
                    w = "星期三";
                    break;
                case DayOfWeek.Thursday:
                    w = "星期四";
                    break;
                case DayOfWeek.Friday:
                    w = "星期五";
                    break;
                case DayOfWeek.Saturday:
                    w = "星期六";
                    break;
                case DayOfWeek.Sunday:
                    w = "星期日";
                    break;
                default:
                    break;
            }

            return y + "年" + m + "月" + d + "日" + "（" + w + "）";
        }
    }
}