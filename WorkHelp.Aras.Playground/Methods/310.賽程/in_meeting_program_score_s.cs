﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class in_meeting_program_score_s : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 成績紀錄查詢
                日期: 
                    2021-03-11: 對戰查詢與匯出 (lina)
                    2020-11-09: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]in_meeting_program_score_s";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, this.dom.InnerXml);

            string meeting_id = itmR.getProperty("meeting_id", "");
            string in_date = itmR.getProperty("in_date", "");
            string site_id = itmR.getProperty("site_id", "");
            string site_code = itmR.getProperty("site_code", "");
            string mode = this.getProperty("mode", "");
            string program_id = this.getProperty("program_id", "");
            string scene = itmR.getProperty("scene", "");

            if (meeting_id == "")
            {
                itmR.setProperty("error_message", "賽事 id 不得為空白");
                return itmR;
            }

            Item itmDays = GetDays(CCO, strMethodName, inn, itmR);

            if (itmDays.isError() || itmDays.getResult() == "")
            {
                itmR.setProperty("error_message", "查無比賽日期");
                return itmR;
            }

            if (in_date == "")
            {
                //itmR.setProperty("in_date", DateTime.Now.ToString("yyyy-MM-dd"));
                if (scene == "next")
                {

                }
                else
                {
                    itmR.setProperty("in_date", itmDays.getItemByIndex(0).getProperty("in_date_key", DateTime.Now.ToString("yyyy-MM-dd")));
                }
            }

            bool need_append = false;
            bool is_next = false;
            AppendMeeting(CCO, strMethodName, inn, itmR);

            if (scene == "")
            {
                need_append = true;
                itmR.setProperty("page_title", "即時成績");
                itmR.setProperty("hide_export_btn", "item_show_0");
                RealTimeTable(CCO, strMethodName, inn, itmR);
            }
            else if (scene == "list")
            {
                need_append = true;
                itmR.setProperty("page_title", "對戰查詢");
                itmR.setProperty("hide_export_btn", "");
                Table2(CCO, strMethodName, inn, itmR);
            }
            else if (scene == "next")
            {
                //檢錄
                is_next = true;
                need_append = false;
                itmR.setProperty("page_title", "比賽進度表");
                itmR.setProperty("hide_export_btn", "");

                if (in_date == "")
                {
                    string in_fight_day = GetTargetFightDay(CCO, strMethodName, inn, itmR);
                    if (in_fight_day != "")
                    {
                        itmR.setProperty("in_date", in_fight_day);
                    }
                }

                if (site_id == "" && site_code != "")
                {
                    string in_fight_site = GetTargetFightSite(CCO, strMethodName, inn, itmR);
                    if (in_fight_site != "")
                    {
                        itmR.setProperty("site_id", in_fight_site);
                    }

                }

                Table3(CCO, strMethodName, inn, itmR);
            }
            else if (scene == "export")
            {
                need_append = false;
                Export(CCO, strMethodName, inn, itmR);
            }

            if (need_append)
            {
                if (mode == "pEvent")
                {
                    Item itmProgram = GetProgram(CCO, strMethodName, inn, program_id);

                    //附加場次選單
                    AppendEventMenu(CCO, strMethodName, inn, itmProgram, itmR);
                }

                //附加日期選單
                AppendDayMenu(CCO, strMethodName, inn, itmDays, itmR);
                //附加場地選單
                AppendSiteMenu(CCO, strMethodName, inn, itmR);
                //附加三階選單
                AppendProgramMenu(CCO, strMethodName, inn, itmR);
            }

            if (is_next)
            {
                //附加日期選單
                AppendDayMenu(CCO, strMethodName, inn, itmDays, itmR);
                //附加場地選單
                AppendSiteMenu(CCO, strMethodName, inn, itmR);
            }

            return itmR;
        }

        private Item GetDays(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            string meeting_id = itmReturn.getProperty("meeting_id", "");

            Item itmDays = inn.applySQL("SELECT DISTINCT in_date_key FROM IN_MEETING_ALLOCATION WHERE in_meeting = '" + meeting_id + "' ORDER BY in_date_key");

            return itmDays;
        }

        //附加日期選單
        private void AppendDayMenu(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmDays, Item itmReturn)
        {
            int count = itmDays.getItemCount();

            for (int i = 0; i < count; i++)
            {
                var itmDay = itmDays.getItemByIndex(i);
                string in_date_key = itmDay.getProperty("in_date_key", "");

                itmDay.setType("inn_date");
                itmDay.setProperty("value", in_date_key);
                itmDay.setProperty("label", in_date_key);
                itmReturn.addRelationship(itmDay);
            }
        }

        private void AppendProgramMenu(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            string meeting_id = itmReturn.getProperty("meeting_id", "");
            string program_id = itmReturn.getProperty("program_id", "");

            //附加選單資訊
            Item itmJson = inn.newItem("In_Meeting");
            itmJson.setProperty("meeting_id", meeting_id);
            itmJson.setProperty("need_id", "1");
            itmJson = itmJson.apply("in_meeting_program_options");
            itmReturn.setProperty("in_level_json", itmJson.getProperty("json", ""));
            itmReturn.setProperty("in_level_count", itmJson.getProperty("total", ""));

            //附加組別資訊
            if (program_id != "")
            {
                Item itmProgram = inn.applySQL("SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + program_id + "'");
                if (!itmProgram.isError() && itmProgram.getResult() != "")
                {
                    itmReturn.setProperty("in_l1", itmProgram.getProperty("in_l1", ""));
                    itmReturn.setProperty("in_l2", itmProgram.getProperty("in_l2", ""));
                    itmReturn.setProperty("in_l3", itmProgram.getProperty("in_l3", ""));
                }
            }
        }

        private void AppendSiteMenu(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            string meeting_id = itmReturn.getProperty("meeting_id", "");

            //附加場地選單
            Item itmEmptySite = inn.newItem();
            itmEmptySite.setType("inn_site");
            itmEmptySite.setProperty("value", "");
            itmEmptySite.setProperty("text", "請選擇");
            itmReturn.addRelationship(itmEmptySite);

            Item itmSites = inn.applySQL("SELECT * FROM IN_MEETING_SITE WITH(NOLOCK) WHERE in_meeting = '" + meeting_id + "' ORDER BY in_code");

            int site_count = itmSites.getItemCount();

            for (int i = 0; i < site_count; i++)
            {
                Item itmSite = itmSites.getItemByIndex(i);
                itmSite.setType("inn_site");
                itmSite.setProperty("value", itmSite.getProperty("id", ""));
                itmSite.setProperty("text", itmSite.getProperty("in_name", ""));
                itmReturn.addRelationship(itmSite);
            }
        }

        private void AppendMeeting(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            string meeting_id = itmReturn.getProperty("meeting_id", "");
            string sql = "SELECT in_title FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + meeting_id + "'";
            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);
            Item itmMeeting = inn.applySQL(sql);
            itmReturn.setProperty("in_title", itmMeeting.getProperty("in_title", ""));
        }

        private void RealTimeTable(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            string sql = "";

            string meeting_id = itmReturn.getProperty("meeting_id", "");
            string program_id = itmReturn.getProperty("program_id", "");
            string site_id = itmReturn.getProperty("site_id", "");

            string in_date = itmReturn.getProperty("in_date", "").Replace("/", "-");
            string in_date_key = GetDateTimeVal(in_date, "yyyy-MM-dd");

            string program_filter = program_id != ""
                ? "AND t1.id = '" + program_id + "'"
                : "";

            string site_filter = site_id != ""
                ? "AND t2.in_site = '" + site_id + "'"
                : "";

            sql = @"
                SELECT 
                    t1.id           AS 'program_id'
                    , t1.in_name    AS 'program_name'
                    , t1.in_name2   AS 'program_name2'
                    , t1.in_display AS 'program_display'
                    , t1.in_team_count AS 'program_team_count'
                    , t2.id         AS 'event_id'
                    , t2.in_tree_name
                    , t2.in_tree_id
                    , t2.in_tree_no
                    , t2.in_tree_alias
                    , t2.in_tree_rank
                    , t2.in_round
                    , t2.in_round_code
                    , t2.in_win_status
                    , t2.in_win_time
                    , t2.in_win_creator
                    , t2.in_note
                    , t2.in_sub_sect
                    , t3.in_sign_foot
                    , t3.in_sign_no
                    , t3.in_target_no
                    , t3.in_status
                    , t3.in_points
                    , t3.in_points_type
                    , t3.in_correct_count
                    , ISNULL(t11.map_short_org, t3.in_player_org) AS 'in_sign_org'
                    , ISNULL(t11.in_name, t3.in_player_name)      AS 'in_sign_name'
                    , ISNULL(t11.in_sno, t3.in_player_sno)        AS 'in_sign_sno'
                    , t11.in_section_no     AS 'in_sign_sect_no'
        			, t13.in_name           AS 'site_name'
                FROM 
                    IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
                    IN_MEETING_PEVENT t2 WITH(NOLOCK)
                    ON t2.source_id = t1.id
                INNER JOIN
                    IN_MEETING_PEVENT_DETAIL t3 WITH(NOLOCK)
                    ON t3.source_id = t2.id
                LEFT OUTER JOIN
                    VU_MEETING_PTEAM t11 WITH(NOLOCK)
                    ON t11.source_id = t2.source_id
                    AND t11.in_sign_no = t3.in_sign_no
					AND ISNULL(t11.in_sign_no, '') <> ''
        		LEFT OUTER JOIN
        			IN_MEETING_SITE t13 WITH(NOLOCK)
        			ON t13.id = t2.in_site
                WHERE
                    t1.in_meeting = '{#meeting_id}'
                    AND ISNULL(t2.in_tree_no, 0) > 0
                    AND ISNULL(t2.in_win_status, '') NOT IN ('', 'bypass')
                    AND t2.in_date_key = '{#in_date_key}'
                    {#program_filter}
                    {#site_filter}
                ORDER BY
                    t2.in_win_time DESC
            ";

            sql = sql.Replace("{#meeting_id}", meeting_id)
                .Replace("{#in_date_key}", in_date_key)
                .Replace("{#program_filter}", program_filter)
                .Replace("{#site_filter}", site_filter);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item items = inn.applySQL(sql);

            List<Item> list = EventScoreList(CCO, strMethodName, inn, items, meeting_id);

            AppendRealTimeTable(list, true, itmReturn);
        }

        private string ProgramLink(Item item, TField field)
        {
            string meeting_id = item.getProperty("meeting_id", "");
            string program_id = item.getProperty("program_id", "");
            string program_display = item.getProperty("program_display", "");
            string open = item.getProperty("open", "");
            string href = "";

            if (open == "1")
            {
                href = "b.aspx?page=PublicCompetition.html"
                    + "&method=in_meeting_program_preview"
                    + "&meeting_id=" + meeting_id
                    + "&program_id=" + program_id;
            }
            else
            {
                href = "c.aspx?page=In_Competition_Preview.html"
                    + "&method=in_meeting_program_preview"
                    + "&meeting_id=" + meeting_id
                    + "&program_id=" + program_id;
            }

            return "<a href='" + href + "' target='_blank' >" + program_display + "</a>";
        }

        private string EventLink(Item item, TField field)
        {
            string program_id = item.getProperty("program_id", "");
            string tree_id = item.getProperty("tree_id", "");
            string program_display = item.getProperty("program_display", "");
            string in_note = item.getProperty("in_note", "");
            string foot1_status = item.getProperty("foot1_status", "");
            string foot2_status = item.getProperty("foot2_status", "");
            string foot1_points_type = item.getProperty("foot1_points_type", "");
            string foot2_points_type = item.getProperty("foot2_points_type", "");

            string text = "";
            string css = "";

            switch (in_note)
            {
                case "91":
                    css = "style='color: red'";
                    if (foot1_points_type == "DQ")
                    {
                        text = "DQ vs " + foot2_status;
                    }
                    else
                    {
                        text = foot1_status + " vs DQ";
                    }
                    break;

                case "92":
                    css = "style='color: red'";
                    text = "DQ vs DQ";
                    break;

                case "31":
                    css = "style='color: red'";
                    text = foot1_status + " vs " + foot2_status;
                    break;

                default:
                    text = foot1_status + " vs " + foot2_status;
                    break;
            }

            return "<a data-pid='" + program_id + "' data-rid='" + tree_id + "' data-pname='" + program_display + "' onclick='Event_Click(this)' " + css + ">" + text + "</a>";
        }

        private void AppendRealTimeTable(List<Item> list, bool need_search, Item itmReturn)
        {
            List<TField> fields = new List<TField>();
            fields.Add(new TField { property = "site_name", title = "場地", css = "text-center", hdcss = "text-center" });
            fields.Add(new TField { property = "tree_no", title = "場編", css = "text-center", hdcss = "text-center" });
            fields.Add(new TField { property = "program_display", title = "量級", css = "text-left", hdcss = "text-center", getValue = ProgramLink });
            fields.Add(new TField { property = "foot1_sign_no", title = "白方<br>籤號", css = "white_group ctrl_search text-center", hdcss = "text-center white_group" });
            fields.Add(new TField { property = "foot1_org", title = "白方<br>代表單位", css = "white_group ctrl_search text-right", hdcss = "text-center white_group" });
            fields.Add(new TField { property = "foot1_name", title = "白方<br>姓名", css = "white_group ctrl_search text-right", hdcss = "text-center white_group" });
            fields.Add(new TField { property = "", title = "vs", css = "text-center", hdcss = "text-center", getValue = EventLink });
            fields.Add(new TField { property = "foot2_name", title = "藍方<br>姓名", css = "blue_group ctrl_search", hdcss = "text-center blue_group" });
            fields.Add(new TField { property = "foot2_org", title = "藍方<br>代表單位", css = "blue_group ctrl_search", hdcss = "text-center blue_group" });
            fields.Add(new TField { property = "foot2_sign_no", title = "藍方<br>籤號", css = "blue_group ctrl_search text-center", hdcss = "text-center blue_group" });

            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            head.AppendLine("<thead>");
            foreach (var field in fields)
            {
                head.AppendLine("<th class='" + field.hdcss + "' data-field='" + field.property + "' data-sortable='false'>" + field.title + "</th>");
            }
            head.AppendLine("</thead>");

            string open = itmReturn.getProperty("open", "");

            int count = list.Count;

            body.AppendLine("<tbody>");
            for (int i = 0; i < count; i++)
            {
                Item item = list[i];
                item.setProperty("open", open);

                body.AppendLine("<tr>");
                foreach (var field in fields)
                {
                    string value = "";
                    if (field.getValue != null)
                    {
                        value = field.getValue(item, field);
                    }
                    else if (field.property != "")
                    {
                        value = item.getProperty(field.property, "");
                    }

                    body.AppendLine("<td class='" + field.css + "'>" + value + "</td>");
                }
                body.AppendLine("</tr>");
            }
            body.AppendLine("</tbody>");


            string table_name = "tb_In_Score";

            StringBuilder builder = new StringBuilder();
            builder.AppendLine(GetTableAttribute(table_name, need_search));
            builder.Append(head);
            builder.Append(body);
            builder.AppendLine("</table>");
            builder.AppendLine("<script>");
            builder.AppendLine("$('#" + table_name + "').bootstrapTable();");
            builder.AppendLine("if ($(window).width() <= 768) { $('#" + table_name + "').bootstrapTable('toggleView'); }");
            builder.AppendLine("</script>");

            itmReturn.setProperty("inn_table", builder.ToString());
        }

        private string GetTableAttribute(string table_name, bool need_search)
        {
            //table table-hover table-bordered table-rwd rwd rwdtable
            if (need_search)
            {
                return "<table id='" + table_name + "' class='table-bordered rwdtable' "
                    + " data-toggle='table' "
                    + " data-search-align='left' "
                    + " data-search='true' "
                    + " data-pagination='true' "
                    + " data-page-size='25' "
                    + " data-click-to-select='true' "
                    + " data-show-toggle='false' "
                    + " data-card-view='false'"
                    + ">";
            }
            else
            {
                return "<table id='" + table_name + "' class='table-bordered rwdtable' "
                    + " data-toggle='table' "
                    + " data-search-align='left' "
                    + " data-search='false' "
                    + " data-click-to-select='true' "
                    + " data-show-toggle='false' "
                    + " data-card-view='false'"
                    + ">";
            }
        }

        #region 匯出
        //匯出
        private void Export(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            var items = GetEvents2(CCO, strMethodName, inn, itmReturn);

            var site_map = MapSites(items);

            var workbook = new ClosedXML.Excel.XLWorkbook();

            foreach (var kv in site_map)
            {
                AppendSiteSheet(CCO, strMethodName, inn, workbook, kv.Value);
            }

            string in_title = itmReturn.getProperty("in_title", "");
            Item itmPath = GetExcelPath(CCO, strMethodName, inn, "export_path");

            string export_path = itmPath.getProperty("export_path", "").TrimEnd('\\');
            string ext_name = ".xlsx";
            string xls_name = in_title + "_對戰查詢_" + DateTime.Now.ToString("MMdd_HHmmss");
            string xls_file = export_path + "\\" + xls_name + ext_name;
            string xls_url = xls_name + ext_name;
            workbook.SaveAs(xls_file);

            itmReturn.setProperty("xls_name", xls_url);
        }

        //場地對戰查詢
        private void AppendSiteSheet(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, ClosedXML.Excel.XLWorkbook workbook, TSite site)
        {
            ClosedXML.Excel.IXLWorksheet sheet = workbook.Worksheets.Add(site.SiteName);

            List<TField> fields = new List<TField>();
            fields.Add(new TField { title = "場地", property = "site_name2", format = "center", width = 4 });
            fields.Add(new TField { title = "編號", property = "in_tree_no", format = "center", width = 4 });
            fields.Add(new TField { title = "量級", getValue = GetProgramDisplay2, width = 19 });
            fields.Add(new TField { title = "部", getValue = GetTreeName, format = "center", width = 4 });
            fields.Add(new TField { title = "輪次", property = "in_round_code", width = 4 });
            fields.Add(new TField { title = "籤號", property = "foot2_sign_no", format = "center", width = 4 });
            fields.Add(new TField { title = "白方單位", property = "foot2_org", width = 13 });
            fields.Add(new TField { title = "白方姓名", property = "foot2_name", width = 8 });
            fields.Add(new TField { title = "vs", property = "", defv = "vs", format = "center", width = 3 });
            fields.Add(new TField { title = "藍方姓名", property = "foot1_name", width = 8 });
            fields.Add(new TField { title = "藍方單位", property = "foot1_org", width = 13 });
            fields.Add(new TField { title = "籤號", property = "foot1_sign_no", format = "center", width = 4 });
            // fields.Add(new TField { title = "場次別名", getValue = GetTreeDisplay, format = "center" });
            // fields.Add(new TField { title = "M/R", getValue = GetEventAlias, format = "center" });
            // fields.Add(new TField { title = "Round", property = "in_round", format = "center" });

            int wsRow = 1;
            int wsCol = 1;

            //凍結標題列
            sheet.SheetView.FreezeRows(1);

            for (int i = 0; i < fields.Count; i++)
            {
                var field = fields[i];
                ClosedXML.Excel.IXLCell cell = sheet.Cell(wsRow, wsCol + i);
                SetHeadCell(cell, field.title);
            }
            wsRow++;

            foreach (var kv in site.Events)
            {
                var evt = kv.Value;
                if (evt.Foot1 == null) evt.Foot1 = inn.newItem();
                if (evt.Foot2 == null) evt.Foot2 = inn.newItem();

                SetItem(evt.Value, evt.Foot1, evt.Foot2, site_change: true);

                SetItemCell(sheet, wsRow, wsCol, evt.Value, fields);

                wsRow++;
            }

            for (int i = 0; i < fields.Count; i++)
            {
                var field = fields[i];
                if (field.width > 0)
                {
                    sheet.Column(wsCol + i).Width = field.width;
                }
                else
                {
                    sheet.Column(wsCol + i).AdjustToContents();
                }
            }

            //重複標題列
            sheet.PageSetup.SetRowsToRepeatAtTop("$1:$1");
        }

        //設定物件與資料列
        private void SetItemCell(ClosedXML.Excel.IXLWorksheet sheet, int wsRow, int wsCol, Item item, List<TField> fields)
        {
            for (int i = 0; i < fields.Count; i++)
            {
                var field = fields[i];
                var cell = sheet.Cell(wsRow, wsCol + i);
                var value = "";

                if (field.getValue != null)
                {
                    value = field.getValue(item, field);
                }
                else if (field.property != "")
                {
                    value = item.getProperty(field.property, "");
                }
                else if (field.defv != "")
                {
                    value = field.defv;
                }

                SetBodyCell(cell, value, field.format);
            }
        }

        //設定資料列
        private void SetBodyCell(ClosedXML.Excel.IXLCell cell, string value, string format)
        {
            switch (format)
            {
                case "center":
                    cell.Value = value;
                    cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    break;

                case "yyyy/MM/dd":
                case "yyyy/MM/dd HH:mm":
                    cell.Value = GetDateTimeValue(value, format);
                    cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    break;

                case "$ #,##0":
                    cell.Value = value;
                    cell.Style.NumberFormat.Format = "$ #,##0";
                    break;

                case "text":
                    cell.Value = "'" + value;
                    cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    break;

                default:
                    cell.Value = value;
                    break;
            }
            cell.Style.Border.SetRightBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetTopBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetBottomBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetLeftBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
        }
        ClosedXML.Excel.XLColor head_bg_color = ClosedXML.Excel.XLColor.FromHtml("#295C90");

        //設定標題列
        private void SetHeadCell(ClosedXML.Excel.IXLCell cell, string title)
        {
            cell.Value = title;

            cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
            cell.Style.Font.Bold = true;
            cell.DataType = ClosedXML.Excel.XLDataType.Text;
            cell.Style.Fill.BackgroundColor = head_bg_color;
            cell.Style.Font.FontColor = ClosedXML.Excel.XLColor.White;

            cell.Style.Border.SetRightBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetTopBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetBottomBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetLeftBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
        }

        //轉換日期
        private string GetDateTimeValue(string value, string format = "yyyy-MM-dd", int hours = 0)
        {
            if (value == null || value == "") return "";

            string day = value.Replace("/", "-");

            DateTime dt = DateTime.Now;
            if (DateTime.TryParse(day, out dt))
            {
                return dt.AddHours(hours).ToString(format);
            }
            else
            {
                return value;
            }
        }

        private Item GetExcelPath(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string in_name)
        {
            Item itmResult = inn.newItem();

            //Excel 匯出路徑 (from 原創.設定參數)
            string sql = @"
                SELECT 
                    TOP 1 t2.in_name AS 'variable', t1.in_name, t1.in_value 
                FROM 
                    In_Variable_Detail t1 WITH(NOLOCK)
                INNER JOIN 
                    In_Variable t2 WITH(NOLOCK) ON t2.ID = t1.SOURCE_ID 
                WHERE 
                    t2.in_name = N'meeting_excel'  
                    AND t1.in_name = N'{#in_name}'
            ";

            sql = sql.Replace("{#in_name}", in_name);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item item = inn.applySQL(sql);

            if (item.isError())
            {
                throw new Exception("未設定匯出路徑 _# " + in_name);
            }

            itmResult.setProperty("export_path", item.getProperty("in_value", ""));

            return itmResult;
        }
        #endregion 匯出

        //對戰查詢
        private void Table3(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            string meeting_id = itmReturn.getProperty("meeting_id", "");

            List<TNode2> items = MapEvents3(CCO, strMethodName, inn, itmReturn);

            List<Item> list = EventScoreList2(CCO, strMethodName, inn, items, meeting_id);

            AppendRealTimeTable(list, false, itmReturn);
            // for (int i = 0; i < list.Count; i++)
            // {
            //     Item item = list[i];
            //     itmReturn.addRelationship(item);
            // }
        }

        //對戰查詢
        private void Table2(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            string meeting_id = itmReturn.getProperty("meeting_id", "");

            Item items = GetEvents2(CCO, strMethodName, inn, itmReturn);

            List<Item> list = EventScoreList(CCO, strMethodName, inn, items, meeting_id);

            AppendRealTimeTable(list, true, itmReturn);
            // for (int i = 0; i < list.Count; i++)
            // {
            //     Item item = list[i];
            //     itmReturn.addRelationship(item);
            // }
        }

        //對戰查詢
        private Item GetEvents2(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            string sql = "";

            string meeting_id = itmReturn.getProperty("meeting_id", "");
            string program_id = itmReturn.getProperty("program_id", "");
            string site_id = itmReturn.getProperty("site_id", "");

            string in_date = itmReturn.getProperty("in_date", "").Replace("/", "-");
            string in_date_key = GetDateTimeVal(in_date, "yyyy-MM-dd");

            string program_filter = program_id != ""
                ? "AND t1.id = '" + program_id + "'"
                : "";

            string site_filter = site_id != ""
                ? "AND t2.in_site = '" + site_id + "'"
                : "";

            sql = @"
                SELECT 
                    t1.id               AS 'program_id'
                    , t1.in_name        AS 'program_name'
                    , t1.in_display     AS 'program_display'
                    , t1.in_battle_type AS 'program_battle'
                    , t1.in_name2       AS 'program_name2'
                    , t2.id             AS 'event_id'
                    , t2.in_tree_name
                    , t2.in_tree_id
                    , t2.in_tree_no
                    , t2.in_tree_alias
                    , t2.in_tree_rank
                    , t2.in_round
                    , t2.in_round_code
                    , t2.in_win_status
                    , t2.in_win_time
                    , t2.in_win_creator
                    , t2.in_note
                    , t3.in_sign_foot
                    , t3.in_sign_no
                    , t3.in_target_no
                    , t3.in_status
                    , t3.in_points
                    , t3.in_points_type
                    , t11.in_name           AS 'in_sign_name'
                    , t11.map_short_org     AS 'in_sign_org'
                    , t11.in_sno            AS 'in_sign_sno'
                    , t11.in_section_no     AS 'in_sign_sect_no'
			        , t13.in_code           AS 'site_code'
			        , t13.in_name           AS 'site_name'
                FROM 
                    IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
                    IN_MEETING_PEVENT t2 WITH(NOLOCK)
                    ON t2.source_id = t1.id
                INNER JOIN
                    IN_MEETING_PEVENT_DETAIL t3 WITH(NOLOCK)
                    ON t3.source_id = t2.id
                LEFT OUTER JOIN
                    VU_MEETING_PTEAM t11 WITH(NOLOCK)
                    ON t11.source_id = t2.source_id
                    AND t11.in_sign_no = t3.in_sign_no
					AND ISNULL(t11.in_sign_no, '') <> ''
		        LEFT OUTER JOIN
			        IN_MEETING_SITE t13 WITH(NOLOCK)
			        ON t13.id = t2.in_site
                WHERE
                    t1.in_meeting = '{#meeting_id}'
                    AND ISNULL(t2.in_tree_no, 0) > 0
                    AND ISNULL(t2.in_win_status, '') NOT IN ('bypass')
                    AND t2.in_date_key = '{#in_date_key}'
                    {#program_filter}
                    {#site_filter}
                ORDER BY
                    t13.in_code
                    , t2.in_tree_no
             ";

            sql = sql.Replace("{#meeting_id}", meeting_id)
                .Replace("{#in_date_key}", in_date_key)
                .Replace("{#program_filter}", program_filter)
                .Replace("{#site_filter}", site_filter);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return inn.applySQL(sql);
        }

        private class TNode2
        {
            public string Id { get; set; }
            public bool IsParent { get; set; }
            public string ParentId { get; set; }
            public List<TNode2> Children { get; set; }
            public Item Value { get; set; }
        }

        //檢錄查詢
        private List<TNode2> MapEvents3(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            List<TNode2> nodes = new List<TNode2>();

            Item items = GetEvents3(CCO, strMethodName, inn, itmReturn);
            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string id = item.getProperty("event_id", "");
                string in_type = item.getProperty("in_type", "");
                string in_parent = item.getProperty("in_parent", "");

                var node = default(TNode2);
                if (in_type == "" || in_type == "p")
                {
                    node = new TNode2
                    {
                        Id = id,
                        IsParent = in_type == "p",
                        Children = new List<TNode2>(),
                        Value = item,
                    };
                    nodes.Add(node);
                }
                else if (in_type == "s")
                {
                    node = new TNode2
                    {
                        Id = id,
                        IsParent = false,
                        Value = item,
                    };
                    var parent = nodes.Find(x => x.Id == in_parent);
                    if (parent != null)
                    {
                        parent.Children.Add(node);
                    }
                }
            }
            return nodes;
        }

        private string GetTargetFightDay(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            string result = "";

            string meeting_id = itmReturn.getProperty("meeting_id", "");
            string today = DateTime.Now.ToString("yyyy-MM-dd");

            string sql = "SELECT DISTINCT in_date_key FROM IN_MEETING_ALLOCATION WITH(NOLOCK) WHERE in_meeting = '" + meeting_id + "'";
            Item items = inn.applySQL(sql);
            int count = items.getItemCount();
            if (count <= 0) return result;

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_date_key = item.getProperty("in_date_key", "");
                if (today == in_date_key)
                {
                    break;
                }
            }

            if (result == "")
            {
                result = items.getItemByIndex(0).getProperty("in_date_key", "");
            }

            return result;
        }

        private string GetTargetFightSite(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            string meeting_id = itmReturn.getProperty("meeting_id", "");
            string site_code = itmReturn.getProperty("site_code", "");

            string sql = "SELECT id FROM IN_MEETING_SITE WITH(NOLOCK) WHERE in_meeting = '" + meeting_id + "' AND in_code = '" + site_code + "'";
            Item itmSite = inn.applySQL(sql);
            if (itmSite.isError() || itmSite.getResult() == "")
            {
                return "";
            }
            else
            {
                return itmSite.getProperty("id", "");
            }
        }

        //檢錄查詢
        private Item GetEvents3(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            string sql = "";

            string meeting_id = itmReturn.getProperty("meeting_id", "");
            string program_id = itmReturn.getProperty("program_id", "");
            string site_id = itmReturn.getProperty("site_id", "");
            string site_code = itmReturn.getProperty("site_code", "");

            string in_date = itmReturn.getProperty("in_date", "").Replace("/", "-");
            string in_date_key = GetDateTimeVal(in_date, "yyyy-MM-dd");

            string program_filter = program_id != ""
                ? "AND t1.id = '" + program_id + "'"
                : "";

            string site_filter = site_id != ""
                ? "AND t2.in_site = '" + site_id + "'"
                : "";

            sql = @"
                SELECT 
                    t1.id               AS 'program_id'
                    , t1.in_name        AS 'program_name'
                    , t1.in_display     AS 'program_display'
                    , t1.in_battle_type AS 'program_battle'
                    , t1.in_name2       AS 'program_name2'
                    , t2.id             AS 'event_id'
                    , t2.in_tree_name
                    , t2.in_tree_id
                    , t2.in_tree_no
                    , t2.in_tree_alias
                    , t2.in_tree_rank
                    , t2.in_round
                    , t2.in_round_code
                    , t2.in_win_status
                    , t2.in_win_time
                    , t2.in_win_creator
                    , t2.in_note
                    , t2.in_type
                    , t2.in_parent
                    , t3.in_sign_foot
                    , t3.in_sign_no
                    , t3.in_target_no
                    , t3.in_status
                    , t3.in_points
                    , t3.in_points_type
                    , ISNULL(t11.map_short_org, in_player_org) AS 'in_sign_org'
                    , ISNULL(t11.in_team, in_player_team) AS 'in_sign_team'
                    , ISNULL(t11.in_name, in_player_name) AS 'in_sign_name'
                    , ISNULL(t11.in_sno, in_player_sno) AS 'in_sign_sno'
                    , t11.in_section_no     AS 'in_sign_sect_no'
                    , t11.in_weight_message
			        , t13.in_code           AS 'site_code'
			        , t13.in_name           AS 'site_name'
                FROM 
                    IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
                    IN_MEETING_PEVENT t2 WITH(NOLOCK)
                    ON t2.source_id = t1.id
                INNER JOIN
                    IN_MEETING_PEVENT_DETAIL t3 WITH(NOLOCK)
                    ON t3.source_id = t2.id
                LEFT OUTER JOIN
                    VU_MEETING_PTEAM t11 WITH(NOLOCK)
                    ON t11.source_id = t2.source_id
                    AND t11.in_sign_no = t3.in_sign_no
					AND ISNULL(t11.in_sign_no, '') <> ''
		        LEFT OUTER JOIN
			        IN_MEETING_SITE t13 WITH(NOLOCK)
			        ON t13.id = t2.in_site
                WHERE
                    t1.in_meeting = '{#meeting_id}'
                    AND ISNULL(t2.in_tree_no, 0) > 0
                    AND ISNULL(t2.in_win_status, '') NOT IN ('bypass', 'cancel', 'nofight')
                    AND t2.in_date_key = '{#in_date_key}'
                    AND t2.in_win_time IS NULL
                    {#program_filter}
                    {#site_filter}
                ORDER BY
                    t13.in_code
                    , t2.in_tree_no
             ";

            sql = sql.Replace("{#meeting_id}", meeting_id)
                .Replace("{#in_date_key}", in_date_key)
                .Replace("{#program_filter}", program_filter)
                .Replace("{#site_filter}", site_filter);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return inn.applySQL(sql);
        }

        private Dictionary<string, TSite> MapSites(Item items)
        {
            int count = items.getItemCount();

            Dictionary<string, TSite> site_map = new Dictionary<string, TSite>();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string site_code = item.getProperty("site_code", "");
                string site_name = item.getProperty("site_name", "");
                string event_id = item.getProperty("event_id", "");
                string in_sign_foot = item.getProperty("in_sign_foot", "");

                TSite site = null;
                if (site_map.ContainsKey(site_code))
                {
                    site = site_map[site_code];
                }
                else
                {
                    site = new TSite
                    {
                        SiteName = site_name,
                        Events = new Dictionary<string, TEvent>(),
                    };
                    site_map.Add(site_code, site);
                }

                TEvent evt = null;
                if (site.Events.ContainsKey(event_id))
                {
                    evt = site.Events[event_id];
                }
                else
                {
                    evt = new TEvent
                    {
                        Id = event_id,
                        Value = item,
                    };
                    site.Events.Add(event_id, evt);
                }

                if (in_sign_foot == "1")
                {
                    evt.Foot1 = item;
                }
                else
                {
                    evt.Foot2 = item;
                }
            }

            return site_map;
        }

        private List<Item> EventScoreList(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item items, string meeting_id)
        {
            List<Item> list = new List<Item>();

            int count = items.getItemCount();

            Dictionary<string, TEvent> map = new Dictionary<string, TEvent>();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string event_id = item.getProperty("event_id", "");
                string in_sign_foot = item.getProperty("in_sign_foot", "");

                TEvent evt = null;
                if (map.ContainsKey(event_id))
                {
                    evt = map[event_id];
                }
                else
                {
                    evt = new TEvent
                    {
                        Id = event_id,
                        Value = item,
                    };
                    map.Add(event_id, evt);
                }

                if (in_sign_foot == "1")
                {
                    evt.Foot1 = item;
                }
                else
                {
                    evt.Foot2 = item;
                }
            }

            foreach (var kv in map)
            {
                TEvent evt = kv.Value;
                if (evt.Foot1 == null) evt.Foot1 = inn.newItem();
                if (evt.Foot2 == null) evt.Foot2 = inn.newItem();

                Item source = evt.Value;
                string program_id = source.getProperty("program_id", "");
                string in_tree_id = source.getProperty("in_tree_id", "");
                string in_tree_no = source.getProperty("in_tree_no", "");
                string in_tree_name = source.getProperty("in_tree_name", "");
                string site_name = source.getProperty("site_name", "");
                string in_round = source.getProperty("in_round", "");
                string in_round_code = source.getProperty("in_round_code", "");
                string in_note = source.getProperty("in_note", "");
                string in_correct_count = source.getProperty("in_correct_count", "");


                Item item = inn.newItem();
                item.setType("inn_score");
                item.setProperty("meeting_id", meeting_id);
                item.setProperty("program_id", program_id);
                item.setProperty("program_display", GetProgramDisplay(source, null));
                item.setProperty("tree_id", in_tree_id);
                item.setProperty("tree_no", in_tree_no);
                item.setProperty("site_name", site_name);
                item.setProperty("tree_display", GetTreeDisplay(source, null));
                item.setProperty("event_alias", GetEventAlias(source, null));
                item.setProperty("in_round", in_round);
                item.setProperty("in_round_code", in_round_code);
                item.setProperty("in_tree_name", in_tree_name);
                item.setProperty("in_note", in_note);
                item.setProperty("in_correct_count", in_correct_count);

                SetItem(item, evt.Foot1, evt.Foot2);

                list.Add(item);
            }
            return list;
        }

        private List<Item> EventScoreList2(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, List<TNode2> nodes, string meeting_id)
        {
            List<Item> list = new List<Item>();

            int count = nodes.Count;

            Dictionary<string, TEvent> map = new Dictionary<string, TEvent>();

            for (int i = 0; i < count; i++)
            {
                TNode2 node = nodes[i];
                if (node.IsParent)
                {
                    foreach (var child in node.Children)
                    {
                        AppendDetail(map, child.Value);
                    }
                }
                else
                {
                    AppendDetail(map, node.Value);
                }
            }

            foreach (var kv in map)
            {
                TEvent evt = kv.Value;
                if (evt.Foot1 == null) evt.Foot1 = inn.newItem();
                if (evt.Foot2 == null) evt.Foot2 = inn.newItem();

                Item source = evt.Value;
                string program_id = source.getProperty("program_id", "");
                string in_tree_id = source.getProperty("in_tree_id", "");
                string in_tree_no = source.getProperty("in_tree_no", "");
                string in_tree_name = source.getProperty("in_tree_name", "");
                string site_name = source.getProperty("site_name", "");
                string in_round = source.getProperty("in_round", "");
                string in_round_code = source.getProperty("in_round_code", "");
                string in_note = source.getProperty("in_note", "");
                string in_correct_count = source.getProperty("in_correct_count", "");


                Item item = inn.newItem();
                item.setType("inn_score");
                item.setProperty("meeting_id", meeting_id);
                item.setProperty("program_id", program_id);
                item.setProperty("program_display", GetProgramDisplay(source, null));
                item.setProperty("tree_id", in_tree_id);
                item.setProperty("tree_no", in_tree_no);
                item.setProperty("site_name", site_name);
                item.setProperty("tree_display", GetTreeDisplay(source, null));
                item.setProperty("event_alias", GetEventAlias(source, null));
                item.setProperty("in_round", in_round);
                item.setProperty("in_round_code", in_round_code);
                item.setProperty("in_tree_name", in_tree_name);
                item.setProperty("in_note", in_note);
                item.setProperty("in_correct_count", in_correct_count);

                SetItem(item, evt.Foot1, evt.Foot2);

                list.Add(item);
            }
            return list;
        }

        private void AppendDetail(Dictionary<string, TEvent> map, Item item)
        {
            string event_id = item.getProperty("event_id", "");
            string in_sign_foot = item.getProperty("in_sign_foot", "");

            TEvent evt = null;
            if (map.ContainsKey(event_id))
            {
                evt = map[event_id];
            }
            else
            {
                evt = new TEvent
                {
                    Id = event_id,
                    Value = item,
                };
                map.Add(event_id, evt);
            }

            if (in_sign_foot == "1")
            {
                evt.Foot1 = item;
            }
            else
            {
                evt.Foot2 = item;
            }
        }

        private void SetItem(Item item, Item foot1, Item foot2, bool site_change = false)
        {
            string icon = "<i class='fa fa-check' style='color: red'></i> ";

            if (site_change)
            {
                string site_name2 = item.getProperty("site_name", "")
                    .Replace("第", "")
                    .Replace("場地", "");

                item.setProperty("site_name2", site_name2);
            }

            //籤腳 1
            string f1_org = foot1.getProperty("in_sign_org", "");
            string f1_team = foot1.getProperty("in_sign_team", "");
            string f1_name = foot1.getProperty("in_sign_name", "");
            string f1_status = foot1.getProperty("in_status", "");
            string f1_weight = foot1.getProperty("in_weight_message", "");

            if (f1_weight != "") f1_name = "(DQ)" + f1_name;
            else if (f1_status == "1") f1_name = icon + f1_name;

            f1_org += f1_team;


            //籤腳 2
            string f2_org = foot2.getProperty("in_sign_org", "");
            string f2_team = foot2.getProperty("in_sign_team", "");
            string f2_name = foot2.getProperty("in_sign_name", "");
            string f2_status = foot2.getProperty("in_status", "");
            string f2_weight = foot2.getProperty("in_weight_message", "");

            if (f2_weight != "") f2_name = "(DQ)" + f2_name;
            else if (f2_status == "1") f2_name = icon + f2_name;

            f2_org += f2_team;

            item.setProperty("foot1_sign_no", foot1.getProperty("in_sign_sect_no", ""));
            item.setProperty("foot1_name", f1_name);
            item.setProperty("foot1_org", f1_org);
            item.setProperty("foot1_sno", foot1.getProperty("in_sign_sno", ""));
            item.setProperty("foot1_status", GetStatusDisplay(foot1));
            item.setProperty("foot1_points_type", foot1.getProperty("in_points_type", ""));

            item.setProperty("foot2_sign_no", foot2.getProperty("in_sign_sect_no", ""));
            item.setProperty("foot2_name", f2_name);
            item.setProperty("foot2_org", f2_org);
            item.setProperty("foot2_sno", foot2.getProperty("in_sign_sno", ""));
            item.setProperty("foot2_status", GetStatusDisplay(foot2));
            item.setProperty("foot2_points_type", foot2.getProperty("in_points_type", ""));
            item.setProperty("foot2_sign_status", foot2.getProperty("in_sign_status", ""));
        }

        private class TSite
        {
            public string SiteName { get; set; }
            public Dictionary<string, TEvent> Events { get; set; }
        }

        private class TEvent
        {
            public string Id { get; set; }
            public Item Value { get; set; }
            public Item Foot1 { get; set; }
            public Item Foot2 { get; set; }
        }

        private class TField
        {
            public string title { get; set; }
            public string property { get; set; }
            public string format { get; set; }
            public string hdcss { get; set; }
            public string css { get; set; }
            public string defv { get; set; }
            public int width { get; set; }
            public Func<Item, TField, string> getValue { get; set; }
        }

        //取得組別別名
        private string GetProgramDisplay(Item item, TField field)
        {
            string program_battle = item.getProperty("program_battle", "");
            string program_name = item.getProperty("program_name2", "");
            string program_team_count = item.getProperty("program_team_count", "");
            string in_sub_sect = item.getProperty("in_sub_sect", "");

            if (in_sub_sect != "")
            {
                return program_battle == "SingleRoundRobin"
                    ? program_name + "(循環賽)" + "<br>" + in_sub_sect
                    : program_name + "<br>" + in_sub_sect;

            }
            else
            {
                return program_battle == "SingleRoundRobin"
                    ? program_name + "(循環賽)"
                    : program_name;
            }
        }

        private string GetTreeName(Item item, TField field)
        {
            string in_tree_name = item.getProperty("in_tree_name", "");
            if (in_tree_name == "main")
            {
                return "勝部";
            }
            else if (in_tree_name == "repechage")
            {
                return "敗部";
            }
            else
            {
                return "";
            }
        }

        //取得組別別名
        private string GetProgramDisplay2(Item item, TField field)
        {
            string program_battle = item.getProperty("program_battle", "");
            string program_name = item.getProperty("program_name2", "");

            return program_battle == "SingleRoundRobin"
                ? program_name + "(循環賽)"
                : program_name;
        }

        //取得樹圖別名
        private string GetTreeDisplay(Item item, TField field)
        {
            string in_tree_name = item.getProperty("in_tree_name", "");

            switch (in_tree_name)
            {
                case "main": return "M";

                case "repechage": return "R";

                case "rank34": return "RK34";

                case "rank56": return "RK56";

                case "rank78": return "RK78";

                default: return "";
            }
        }

        //取得場次別名
        private string GetEventAlias(Item item, TField field)
        {
            string in_tree_alias = item.getProperty("in_tree_alias", "");
            string in_tree_rank = item.getProperty("in_tree_rank", "");

            switch (in_tree_rank)
            {
                case "rank34": return "三四名";

                case "rank56": return "五六名";

                case "rank78": return "七八名";

                default: return in_tree_alias;
            }
        }

        //取得勝敗呈現
        private string GetStatusDisplay(Item item)
        {
            string in_status = item.getProperty("in_status", "");
            string in_points = item.getProperty("in_points", "0");
            string in_correct_count = item.getProperty("in_correct_count", "0");

            switch (in_status)
            {
                case "1": return "<span class='team_score_b'>" + in_points + "</span>";
                case "0":
                    if (in_correct_count == "3")
                    {
                        return "<span class='team_score_c'>" + in_points + " (S3)</span>";
                    }
                    else
                    {
                        return "<span class='team_score_c'>" + in_points + "</span>";
                    }
                default: return "<span class='team_score_a'>&nbsp;</span>";
            }
        }

        //取得場次勝敗狀態
        private string GetEventStatus(string value)
        {
            switch (value)
            {
                case "cancel": return "本場次取消";

                case "nofight": return "一方取消";

                case "bypass": return "輪空";

                default: return "";
            }
        }


        private int GetIntVal(string value, int def = 0)
        {
            int result = def;
            Int32.TryParse(value, out result);
            return result;
        }

        private string GetDateTimeVal(string value, string format)
        {
            DateTime result = DateTime.MinValue;
            DateTime.TryParse(value, out result);
            if (result != DateTime.MinValue)
            {
                return result.ToString(format);
            }
            else
            {
                return "";
            }
        }
        //附加場次選單
        private void AppendEventMenu(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmProgram, Item itmReturn)
        {


            string program_id = itmProgram.getProperty("id", "");

            Dictionary<string, List<Item>> map = new Dictionary<string, List<Item>>();
            AddItems(map, GetEventsMenuItems(CCO, strMethodName, inn, program_id, query_childre: false));
            AddItems(map, GetEventsMenuItems(CCO, strMethodName, inn, program_id, query_childre: true));

            List<TNode> nodes = new List<TNode>();

            foreach (var kv in map)
            {
                var key = kv.Key;
                var list = kv.Value;

                TNode n1 = new TNode
                {
                    Val = key,
                    Nodes = new List<TNode>(),
                };

                int count = list.Count;
                for (int i = 0; i < count; i++)
                {
                    Item itemLast = i == 0 ? inn.newItem() : list[i - 1];
                    Item item = list[i];
                    Item itemNext = i == (count - 1) ? inn.newItem() : list[i + 1];

                    TNode evt = new TNode
                    {
                        Lbl = item.getProperty("in_tree_name", ""),
                        Ext = item.getProperty("in_tree_no", ""),

                        Val = item.getProperty("in_tree_id", ""),
                        Last = itemLast.getProperty("in_tree_id", ""),
                        Next = itemNext.getProperty("in_tree_id", ""),
                    };
                    n1.Nodes.Add(evt);
                }

                nodes.Add(n1);
            }

            itmReturn.setProperty("in_event_json", Newtonsoft.Json.JsonConvert.SerializeObject(nodes));
        }

        private class TNode
        {
            public string Val { get; set; }
            public string Lbl { get; set; }
            public string Ext { get; set; }
            public string Last { get; set; }
            public string Next { get; set; }
            public List<TNode> Nodes { get; set; }

        }
        private void AddItems(Dictionary<string, List<Item>> map, Item items)
        {
            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string source_id = item.getProperty("source_id", "");

                List<Item> list = null;
                if (map.ContainsKey(source_id))
                {
                    list = map[source_id];
                }
                else
                {
                    list = new List<Item>();
                    map.Add(source_id, list);
                }
                list.Add(item);
            }

        }

        private Item GetEventsMenuItems(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string program_id, bool query_childre = false)
        {
            string program_condition = query_childre
                ? "source_id IN (SELECT id FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE in_program = '" + program_id + "')"
                : "source_id = '" + program_id + "'";

            string sql = @"
        SELECT
	        source_id
	        , in_tree_name
	        , in_tree_id
	        , in_tree_no
        FROM 
	        IN_MEETING_PEVENT WITH(NOLOCK)
        WHERE 
	        {#program_condition}
	        AND ISNULL(in_tree_no, '') NOT IN ('', '0')
	        AND ISNULL(in_tree_rank, '') NOT IN (N'rank78')
        ORDER BY
	        in_tree_sort
	        , in_tree_no
        ";

            sql = sql.Replace("{#program_condition}", program_condition);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return inn.applySQL(sql);

        }

        //取得資訊
        private Item GetProgram(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string program_id)
        {
            string sql = @"SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '{#program_id}'";
            sql = sql.Replace("{#program_id}", program_id);
            return inn.applySQL(sql);
        }
    }
}