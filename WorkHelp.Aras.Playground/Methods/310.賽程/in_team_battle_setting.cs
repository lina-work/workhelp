﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;
using System.Net;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class in_team_battle_setting : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 團體戰-組別設定
                日期: 
                    - 2021-12-17 創建 (Lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]in_team_battle_setting";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, this.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                InnH = _InnH,

                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                strIdentityId = inn.getUserAliases(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                scene = itmR.getProperty("scene", ""),
            };

            switch (cfg.scene)
            {
                case "update":
                    Update(cfg, itmR);
                    break;

                case "create":
                    Create(cfg, itmR);
                    break;

                default:
                    Page(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void Create(TConfig cfg, Item itmReturn)
        {
            string fight_mode = itmReturn.getProperty("fight_mode", "");

            string sql = "SELECT id, in_name2 FROM IN_MEETING_PROGRAM WITH(NOLOCK)"
                + " WHERE in_meeting = '" + cfg.meeting_id + "'"
                + " AND in_l1 = N'團體組'"
                + " ORDER BY in_sort_order";

            Item itmPrograms = cfg.inn.applySQL(sql);


            switch (fight_mode)
            {
                case "weight_5":
                    CreateSects_Weight5(cfg, itmPrograms, itmReturn);
                    break;

                case "mix_6":
                    CreateSects_Mix6(cfg, itmPrograms, itmReturn);
                    break;
            }
        }

        private void CreateSects_Mix6(TConfig cfg, Item itmPrograms, Item itmReturn)
        {
            int count = itmPrograms.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item itmProgram = itmPrograms.getItemByIndex(i);
                string in_name2 = itmProgram.getProperty("in_name2", "");

                switch (in_name2)
                {
                    case "混合組團體賽":
                        MergeSects(cfg, itmProgram, GetSects_Mix6);
                        break;

                    default:
                        break;
                }
            }
        }

        //混合組團體賽
        private List<TSect> GetSects_Mix6()
        {
            var sects = new List<TSect>();
            sects.Add(new TSect { T1 = "1", T2 = "女子-57kg", T3 = "W", T4 = "-57kg", T5 = "-48,-52,-57", T6 = "包括個人組-48或-52或-57公斤級" });
            sects.Add(new TSect { T1 = "2", T2 = "男子-73kg", T3 = "M", T4 = "-73kg", T5 = "-60,-66,-73", T6 = "包括個人組-60或-66或-73公斤級" });
            sects.Add(new TSect { T1 = "3", T2 = "女子-70kg", T3 = "W", T4 = "-70kg", T5 = "-57,-63,-70", T6 = "包括個人組-57或-63或-70公斤級" });
            sects.Add(new TSect { T1 = "4", T2 = "男子-90kg", T3 = "M", T4 = "-90kg", T5 = "-73,-81,-90", T6 = "包括個人組-73或-81或-90公斤級" });
            sects.Add(new TSect { T1 = "5", T2 = "女子+70kg", T3 = "W", T4 = "+70kg", T5 = "-70,-78,+78", T6 = "包括個人組-70或-78或+78公斤級" });
            sects.Add(new TSect { T1 = "6", T2 = "男子+90kg", T3 = "M", T4 = "+90kg", T5 = "-90,-100,+100", T6 = "包括個人組-90或-100或+100公斤級" });
            sects.Add(new TSect { T1 = "7", T2 = "代表戰", T3 = "", T4 = "", T5 = "", T6 = "代表戰" });
            return sects;
        }

        private void CreateSects_Weight5(TConfig cfg, Item itmPrograms, Item itmReturn)
        {
            int count = itmPrograms.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item itmProgram = itmPrograms.getItemByIndex(i);
                string in_name2 = itmProgram.getProperty("in_name2", "");

                switch (in_name2)
                {
                    case "特別組團體賽":
                        MergeSects(cfg, itmProgram, GetSects_Weight5_C_M);
                        break;

                    case "社會男子甲組團體賽":
                        MergeSects(cfg, itmProgram, GetSects_Weight5_C_M);
                        break;

                    case "社會男子乙組團體賽":
                        MergeSects(cfg, itmProgram, GetSects_Weight5_C_M);
                        break;

                    case "大專男子甲組團體賽":
                        MergeSects(cfg, itmProgram, GetSects_Weight5_C_M);
                        break;

                    case "大專男子乙組團體賽":
                        MergeSects(cfg, itmProgram, GetSects_Weight5_C_M);
                        break;

                    case "社會女子甲組團體賽":
                        MergeSects(cfg, itmProgram, GetSects_Weight5_C_W);
                        break;

                    case "社會女子乙組團體賽":
                        MergeSects(cfg, itmProgram, GetSects_Weight5_C_W);
                        break;

                    case "大專女子甲組團體賽":
                        MergeSects(cfg, itmProgram, GetSects_Weight5_C_W);
                        break;

                    case "大專女子乙組團體賽":
                        MergeSects(cfg, itmProgram, GetSects_Weight5_C_W);
                        break;


                    case "高中男子組團體賽":
                        MergeSects(cfg, itmProgram, GetSects_Weight5_H_M);
                        break;

                    case "高中女子組團體賽":
                        MergeSects(cfg, itmProgram, GetSects_Weight5_H_W);
                        break;

                    case "國中男子組團體賽":
                        MergeSects(cfg, itmProgram, GetSects_Weight5_J_M);
                        break;

                    case "國中女子組團體賽":
                        MergeSects(cfg, itmProgram, GetSects_Weight5_J_W);
                        break;

                    case "國小男子組團體賽":
                        MergeSects(cfg, itmProgram, GetSects_Weight5_E_M);
                        break;

                    case "國小女子組團體賽":
                        MergeSects(cfg, itmProgram, GetSects_Weight5_E_W);
                        break;

                    default:
                        break;
                }
            }
        }

        private void MergeSects(TConfig cfg, Item itmProgram, Func<List<TSect>> getSects)
        {
            var in_program = itmProgram.getProperty("id", "");

            var sects = getSects();

            for (int i = 0; i < sects.Count; i++)
            {
                var sect = sects[i];

                Item itmPSect = cfg.inn.newItem("IN_MEETING_PSECT");

                itmPSect.setAttribute("where", "in_program = '" + in_program + "'"
                    + " AND in_sub_id = '" + sect.T1 + "'");

                itmPSect.setProperty("in_meeting", cfg.meeting_id);
                itmPSect.setProperty("in_program", in_program);
                itmPSect.setProperty("in_sub_id", sect.T1);

                itmPSect.setProperty("in_name", sect.T2);
                itmPSect.setProperty("in_gender", sect.T3);
                itmPSect.setProperty("in_weight", sect.T4);
                itmPSect.setProperty("in_ranges", sect.T5);
                itmPSect.setProperty("in_note", sect.T6);

                Item itmResult = itmPSect.apply("merge");

                if (itmResult.isError())
                {
                    throw new Exception("新增團體賽組別失敗");
                }
            }
        }

        //大專男子組團體賽
        private List<TSect> GetSects_Weight5_C_M()
        {
            var sects = new List<TSect>();
            sects.Add(new TSect { T1 = "1", T2 = "先鋒", T3 = "M", T4 = "-90kg", T5 = "-60,-66,-73,-81,-90", T6 = "先鋒限第 1-5 級" });
            sects.Add(new TSect { T1 = "2", T2 = "次鋒", T3 = "M", T4 = "-90kg", T5 = "-60,-66,-73,-81,-90", T6 = "次鋒限第 1-5 級" });
            sects.Add(new TSect { T1 = "3", T2 = "中堅", T3 = "M", T4 = "-90kg", T5 = "-60,-66,-73,-81,-90", T6 = "中堅限第 1-5 級" });
            sects.Add(new TSect { T1 = "4", T2 = "副將", T3 = "M", T4 = "+100kg", T5 = "-90,-100,+100", T6 = "副將限第 5-7 級" });
            sects.Add(new TSect { T1 = "5", T2 = "主將", T3 = "M", T4 = "+100kg", T5 = "-90,-100,+100", T6 = "主將限第 5-7 級" });
            return sects;
        }

        //大專女子組團體賽
        private List<TSect> GetSects_Weight5_C_W()
        {
            var sects = new List<TSect>();
            sects.Add(new TSect { T1 = "1", T2 = "先鋒", T3 = "W", T4 = "-70kg", T5 = "-48,-52,-57,-63,-70", T6 = "先鋒限第 1-5 級" });
            sects.Add(new TSect { T1 = "2", T2 = "次鋒", T3 = "W", T4 = "-70kg", T5 = "-48,-52,-57,-63,-70", T6 = "次鋒限第 1-5 級" });
            sects.Add(new TSect { T1 = "3", T2 = "中堅", T3 = "W", T4 = "-70kg", T5 = "-48,-52,-57,-63,-70", T6 = "中堅限第 1-5 級" });
            sects.Add(new TSect { T1 = "4", T2 = "副將", T3 = "W", T4 = "+78kg", T5 = "-70,-78,+78", T6 = "副將限第 5-7 級" });
            sects.Add(new TSect { T1 = "5", T2 = "主將", T3 = "W", T4 = "+78kg", T5 = "-70,-78,+78", T6 = "主將限第 5-7 級" });
            return sects;
        }

        //高中男子組團體賽
        private List<TSect> GetSects_Weight5_H_M()
        {
            var sects = new List<TSect>();
            sects.Add(new TSect { T1 = "1", T2 = "先鋒", T3 = "M", T4 = "-90kg", T5 = "-55,-60,-66,-73,-81,-90", T6 = "先鋒限第 1-6 級" });
            sects.Add(new TSect { T1 = "2", T2 = "次鋒", T3 = "M", T4 = "-90kg", T5 = "-55,-60,-66,-73,-81,-90", T6 = "次鋒限第 1-6 級" });
            sects.Add(new TSect { T1 = "3", T2 = "中堅", T3 = "M", T4 = "-90kg", T5 = "-55,-60,-66,-73,-81,-90", T6 = "中堅限第 1-6 級" });
            sects.Add(new TSect { T1 = "4", T2 = "副將", T3 = "M", T4 = "+100kg", T5 = "-90,-100,+100", T6 = "副將限第 6-8 級" });
            sects.Add(new TSect { T1 = "5", T2 = "主將", T3 = "M", T4 = "+100kg", T5 = "-90,-100,+100", T6 = "主將限第 6-8 級" });
            return sects;
        }

        //高中女子組團體賽
        private List<TSect> GetSects_Weight5_H_W()
        {
            var sects = new List<TSect>();
            sects.Add(new TSect { T1 = "1", T2 = "先鋒", T3 = "W", T4 = "-70kg", T5 = "-44,-48,-52,-57,-63,-70", T6 = "先鋒限第 1-6 級" });
            sects.Add(new TSect { T1 = "2", T2 = "次鋒", T3 = "W", T4 = "-70kg", T5 = "-44,-48,-52,-57,-63,-70", T6 = "次鋒限第 1-6 級" });
            sects.Add(new TSect { T1 = "3", T2 = "中堅", T3 = "W", T4 = "-70kg", T5 = "-44,-48,-52,-57,-63,-70", T6 = "中堅限第 1-6 級" });
            sects.Add(new TSect { T1 = "4", T2 = "副將", T3 = "W", T4 = "+78kg", T5 = "-70,-78,+78", T6 = "副將限第 6-8 級" });
            sects.Add(new TSect { T1 = "5", T2 = "主將", T3 = "W", T4 = "+78kg", T5 = "-70,-78,+78", T6 = "主將限第 6-8 級" });
            return sects;
        }

        //國中男子組團體賽
        private List<TSect> GetSects_Weight5_J_M()
        {
            var sects = new List<TSect>();
            sects.Add(new TSect { T1 = "1", T2 = "先鋒", T3 = "M", T4 = "-46kg", T5 = "-38,-42,-46", T6 = "先鋒限 1-3 級" });
            sects.Add(new TSect { T1 = "2", T2 = "次鋒", T3 = "M", T4 = "-55kg", T5 = "-46,-50,-55", T6 = "次鋒限 3-5 級" });
            sects.Add(new TSect { T1 = "3", T2 = "中堅", T3 = "M", T4 = "-66kg", T5 = "-55,-60,-66", T6 = "中堅限 5-7 級" });
            sects.Add(new TSect { T1 = "4", T2 = "副將", T3 = "M", T4 = "-73kg", T5 = "-60,-66,-73", T6 = "副將限 6-8 級" });
            sects.Add(new TSect { T1 = "5", T2 = "主將", T3 = "M", T4 = "+81kg", T5 = "-73,-81,+81", T6 = "主將限 8-10 級" });
            return sects;
        }

        //國中女子組團體賽
        private List<TSect> GetSects_Weight5_J_W()
        {
            var sects = new List<TSect>();
            sects.Add(new TSect { T1 = "1", T2 = "先鋒", T3 = "W", T4 = "-44kg", T5 = "-36,-40,-44", T6 = "先鋒限 1-3 級" });
            sects.Add(new TSect { T1 = "2", T2 = "次鋒", T3 = "W", T4 = "-52kg", T5 = "-44,-48,-52", T6 = "次鋒限 3-5 級" });
            sects.Add(new TSect { T1 = "3", T2 = "中堅", T3 = "W", T4 = "-63kg", T5 = "-52,-57,-63", T6 = "中堅限 5-7 級" });
            sects.Add(new TSect { T1 = "4", T2 = "副將", T3 = "W", T4 = "-70kg", T5 = "-57,-63,-70", T6 = "副將限 6-8 級" });
            sects.Add(new TSect { T1 = "5", T2 = "主將", T3 = "W", T4 = "+70kg", T5 = "-70,+70", T6 = "主將限 8-10 級" });
            return sects;
        }

        //國小男子組團體賽
        private List<TSect> GetSects_Weight5_E_M()
        {
            var sects = new List<TSect>();
            sects.Add(new TSect { T1 = "1", T2 = "先鋒", T3 = "M", T4 = "-33kg", T5 = "-30,-33", T6 = "先鋒限第 1、2 級" });
            sects.Add(new TSect { T1 = "2", T2 = "次鋒", T3 = "M", T4 = "-41kg", T5 = "-37,-41", T6 = "次鋒限第 3、4 級" });
            sects.Add(new TSect { T1 = "3", T2 = "中堅", T3 = "M", T4 = "-45kg", T5 = "-41,-45", T6 = "中堅限第 4、5 級" });
            sects.Add(new TSect { T1 = "4", T2 = "副將", T3 = "M", T4 = "-50kg", T5 = "-45,-50", T6 = "副將限第 5、6 級" });
            sects.Add(new TSect { T1 = "5", T2 = "主將", T3 = "M", T4 = "-55kg", T5 = "-50,-55", T6 = "主將限第 6、7 級" });
            return sects;
        }

        //國小女子組團體賽
        private List<TSect> GetSects_Weight5_E_W()
        {
            var sects = new List<TSect>();
            sects.Add(new TSect { T1 = "1", T2 = "先鋒", T3 = "W", T4 = "-33kg", T5 = "-30,-33", T6 = "先鋒限第 1、2 級" });
            sects.Add(new TSect { T1 = "2", T2 = "次鋒", T3 = "W", T4 = "-41kg", T5 = "-37,-41", T6 = "次鋒限第 3、4 級" });
            sects.Add(new TSect { T1 = "3", T2 = "中堅", T3 = "W", T4 = "-45kg", T5 = "-41,-45", T6 = "中堅限第 4、5 級" });
            sects.Add(new TSect { T1 = "4", T2 = "副將", T3 = "W", T4 = "-50kg", T5 = "-45,-50", T6 = "副將限第 5、6 級" });
            sects.Add(new TSect { T1 = "5", T2 = "主將", T3 = "W", T4 = "-55kg", T5 = "-50,-55", T6 = "主將限第 6、7 級" });
            return sects;
        }

        private class TSect
        {
            /// <summary>
            /// in_sub_id
            /// </summary>
            public string T1 { get; set; }
            /// <summary>
            /// in_name
            /// </summary>
            public string T2 { get; set; }
            /// <summary>
            /// in_gender
            /// </summary>
            public string T3 { get; set; }
            /// <summary>
            /// in_weight
            /// </summary>
            public string T4 { get; set; }
            /// <summary>
            /// in_ranges
            /// </summary>
            public string T5 { get; set; }
            /// <summary>
            /// in_note
            /// </summary>
            public string T6 { get; set; }
        }


        private void Update(TConfig cfg, Item itmReturn)
        {
            string id = itmReturn.getProperty("sect_id", "");
            string property = itmReturn.getProperty("property", "");
            string value = itmReturn.getProperty("value", "");
            RunUpdate(cfg, id, property, value);
        }

        private void RunUpdate(TConfig cfg, string id, string in_property, string value)
        {
            string sql = "UPDATE IN_MEETING_PSECT SET " + in_property + " = N'" + value + "' WHERE id = '" + id + "'";

            Item itmSQL = cfg.inn.applySQL(sql);

            if (itmSQL.isError())
            {
                throw new Exception("更新失敗");
            }
        }

        private void Page(TConfig cfg, Item itmReturn)
        {
            Item items = GetMSects(cfg);

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                item.setType("inn_sect");
                itmReturn.addRelationship(item);
            }
        }

        private Item GetMSects(TConfig cfg)
        {
            string sql = @"
                SELECT 
	                t2.id            AS 'pg_id'
	                , t2.in_name2    AS 'pg_name'
	                , t1.*
                FROM 
	                IN_MEETING_PSECT t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PROGRAM t2 WITH(NOLOCK)
	                ON t2.id = t1.in_program
                WHERE
	                t1.in_meeting = '{#meeting_id}'
                ORDER BY
	                t2.in_sort_order
	                , t1.in_sub_id
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            return cfg.inn.applySQL(sql);
        }


        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public Innosoft.InnovatorHelper InnH { get; set; }

            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strUserName { get; set; }
            public string strIdentityId { get; set; }

            public string meeting_id { get; set; }
            public string scene { get; set; }
        }
    }
}