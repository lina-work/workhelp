﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class in_meeting_program_score_q : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
            目的: 快速登錄成績
            日期: 
                2020-09-30: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]in_meeting_program_score_q";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, this.dom.InnerXml);

            string meeting_id = itmR.getProperty("meeting_id", "");
            string program_id = itmR.getProperty("program_id", "");
            string tree_id = itmR.getProperty("id", "");
            string mode = itmR.getProperty("mode", "");

            if (meeting_id == "")
            {
                itmR.setProperty("error_message", "賽事 id 不得為空白");
                return itmR;
            }

            // //檢查頁面權限
            // Item itmPermit = inn.applyMethod("In_CheckIdentity", "<method>" + strMethodName + "</method><code>" + "ALL" + "</code>");
            // bool isMeetingAdmin = itmPermit.getProperty("isMeetingAdmin", "") == "1";
            // if (!isMeetingAdmin)
            // {
            //     itmR.setProperty("error_message", "您無權限瀏覽此頁面");
            //     return itmR;
            // }

            if (mode == "")
            {
                if (program_id == "" && tree_id == "")
                {
                    AppendMeeting(CCO, strMethodName, inn, itmR);
                    AppendLevelOptions(CCO, strMethodName, inn, itmR);
                    itmR.setProperty("hide_preview", "item_show_0");
                }
                else if (program_id != "" && tree_id == "")
                {
                    Item itmFirstEvent = GetFirstEvent(CCO, strMethodName, inn, itmR);
                    if (itmFirstEvent.isError())
                    {
                        AppendProgram(CCO, strMethodName, inn, itmR);
                    }
                    else
                    {
                        string first_tree_id = itmFirstEvent.getProperty("in_tree_id", "");
                        AppendPEvent(CCO, strMethodName, inn, first_tree_id, itmR);
                    }
                    AppendLevelOptions(CCO, strMethodName, inn, itmR);
                    AppendEventOptions(CCO, strMethodName, inn, itmR);
                }
                else if (program_id != "" && tree_id != "")
                {
                    AppendPEvent(CCO, strMethodName, inn, tree_id, itmR);
                    AppendLevelOptions(CCO, strMethodName, inn, itmR);
                    AppendEventOptions(CCO, strMethodName, inn, itmR);
                }
            }
            else if (mode == "modal")
            {
                Modal(CCO, strMethodName, inn, itmR);
            }

            return itmR;
        }

        private class TSub
        {
            public string name { get; set; }
            public string org { get; set; }
        }

        private void AppendMeeting(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            string meeting_id = itmReturn.getProperty("meeting_id", "");

            string sql = "SELECT in_title FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + meeting_id + "'";
            sql = sql.Replace("{#meeting_id}", meeting_id);
            Item itmMeeting = inn.applySQL(sql);

            itmReturn.setProperty("in_title", itmMeeting.getProperty("in_title", ""));
        }

        private void AppendProgram(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            string sql = "";

            string meeting_id = itmReturn.getProperty("meeting_id", "");
            string program_id = itmReturn.getProperty("program_id", "");

            List<string> err = new List<string>();
            if (program_id == "") err.Add("組別 id 不得為空白");

            if (err.Count > 0)
            {
                itmReturn.setProperty("error_message", string.Join(" <br> ", err));
                return;
            }

            sql = @"
                SELECT
                    TOP 1
                    t3.in_title
                    , t2.in_l1
                    , t2.in_l2
                    , t2.in_l3
                    , t2.in_name AS 'program_name'
                    , t2.in_display AS 'program_display'
                FROM
                    IN_MEETING_PROGRAM t2 WITH(NOLOCK)
                INNER JOIN
                    IN_MEETING t3 WITH(NOLOCK)
                    ON t3.id = t2.in_meeting
                WHERE
                    t2.id = '{#program_id}'
            ";

            sql = sql.Replace("{#program_id}", program_id);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item itmProgram = inn.applySQL(sql);

            itmReturn.setProperty("in_title", itmProgram.getProperty("in_title", ""));
            itmReturn.setProperty("in_l1", itmProgram.getProperty("in_l1", ""));
            itmReturn.setProperty("in_l2", itmProgram.getProperty("in_l2", ""));
            itmReturn.setProperty("in_l3", itmProgram.getProperty("in_l3", ""));
            itmReturn.setProperty("program_display", itmProgram.getProperty("program_display", ""));
        }

        private Item GetFirstEvent(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            string sql = "";

            string meeting_id = itmReturn.getProperty("meeting_id", "");
            string program_id = itmReturn.getProperty("program_id", "");

            sql = @"
                SELECT
                    TOP 1 *
                FROM
                    IN_MEETING_PEVENT t1 WITH(NOLOCK)
                WHERE
                    source_id = '{#program_id}'
                    AND in_tree_name = 'main'
                    AND ISNULL(in_tree_no, '') NOT IN ('', '0')
                ORDER BY
                    in_tree_no
            ";

            sql = sql.Replace("{#program_id}", program_id);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return inn.applySQL(sql);

        }

        private void AppendPEvent(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string tree_id, Item itmReturn)
        {
            string sql = "";

            string meeting_id = itmReturn.getProperty("meeting_id", "");
            string program_id = itmReturn.getProperty("program_id", "");

            List<string> err = new List<string>();
            if (program_id == "") err.Add("組別 id 不得為空白");
            if (tree_id == "") err.Add("場次 id 不得為空白");

            if (err.Count > 0)
            {
                itmReturn.setProperty("error_message", string.Join(" <br> ", err));
                return;
            }

            sql = @"
                SELECT
                    TOP 1
                    t3.in_title
                    , t2.in_l1
                    , t2.in_l2
                    , t2.in_l3
                    , t2.in_name AS 'program_name'
                    , t2.in_display AS 'program_display'
                    , t1.*
                FROM
                    IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
                    IN_MEETING_PROGRAM t2 WITH(NOLOCK)
                    ON t2.id = t1.source_id
                INNER JOIN
                    IN_MEETING t3 WITH(NOLOCK)
                    ON t3.id = t2.in_meeting
                WHERE
                    t1.source_id = '{#program_id}'
                    AND t1.in_tree_id = '{#in_tree_id}'
            ";

            sql = sql.Replace("{#program_id}", program_id)
                .Replace("{#in_tree_id}", tree_id);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item itmPEvent = inn.applySQL(sql);

            itmReturn.setProperty("in_title", itmPEvent.getProperty("in_title", ""));
            itmReturn.setProperty("in_l1", itmPEvent.getProperty("in_l1", ""));
            itmReturn.setProperty("in_l2", itmPEvent.getProperty("in_l2", ""));
            itmReturn.setProperty("in_l3", itmPEvent.getProperty("in_l3", ""));
            itmReturn.setProperty("program_display", itmPEvent.getProperty("program_display", ""));
            itmReturn.setProperty("in_tree_name", itmPEvent.getProperty("in_tree_name", ""));
            itmReturn.setProperty("in_round", itmPEvent.getProperty("in_round", ""));
            itmReturn.setProperty("in_tree_id", itmPEvent.getProperty("in_tree_id", ""));
        }

        private void Modal(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            string sql = "";

            string meeting_id = itmReturn.getProperty("meeting_id", "");
            string program_id = itmReturn.getProperty("program_id", "");
            string tree_id = itmReturn.getProperty("id", "");

            List<string> err = new List<string>();
            if (program_id == "") err.Add("組別 id 不得為空白");
            if (tree_id == "") err.Add("場次 id 不得為空白");

            if (err.Count > 0)
            {
                itmReturn.setProperty("error_message", string.Join(" <br> ", err));
                return;
            }

            //主成績框
            MainModal(CCO, strMethodName, inn, program_id, tree_id, itmReturn);

            //子場次選單
            SubEventMenu(CCO, strMethodName, inn, program_id, tree_id, itmReturn);
        }

        //子場次選單
        private void SubEventMenu(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string program_id, string tree_id, Item itmReturn)
        {
            string sql = "";

            string p_tree_id = tree_id.Substring(0, 4);

            sql = @"
                SELECT
	                id
	                , in_sub_id
                FROM
	                IN_MEETING_PEVENT WITH(NOLOCK)
                WHERE
	                source_id = '{#program_id}'
	                AND in_tree_id LIKE '{#p_tree_id}%'
	                AND in_type = 's'
                ORDER BY
	                in_sub_id
            ";

            sql = sql.Replace("{#program_id}", program_id)
                .Replace("{#p_tree_id}", p_tree_id);

            Item items = inn.applySQL(sql);
            
            if (items.isError() || items.getResult() == "")
            {
                itmReturn.setProperty("sub_json", "[]");
                return;
            }

            List<TNode> nodes = new List<TNode>();

            int count = items.getItemCount();
            for(int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string id = item.getProperty("id", "");
                string name = "第" + item.getProperty("in_sub_id", "") + "場";

                var node = new TNode
                { 
                    Lbl = name,
                    Val = id,
                };

                nodes.Add(node);
            }

            itmReturn.setProperty("sub_json", Newtonsoft.Json.JsonConvert.SerializeObject(nodes));
        }
        
        //主成績框
        private void MainModal(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string program_id, string tree_id, Item itmReturn)
        {
            string sql = "";

            sql = @"
                SELECT
                    t1.id               AS 'event_id'
                    , t1.in_type        AS 'event_type'
                    , t1.in_tree_name
                    , t1.in_tree_id
                    , t1.in_tree_no
                    , t1.in_tree_alias
                    , t1.in_round
                    , t1.in_round_code
                    , t1.in_site_code
                    , t1.in_site_no
                    , t1.in_site_id
                    , t1.in_site_allocate
                    , t1.in_site
                    , t1.in_date_key
                    , t1.in_win_status
                    , t2.id AS 'detail_id'
                    , t2.in_sign_no
                    , t2.in_target_no
                    , t2.in_status
                    , t2.in_points
                    , t2.in_points_type
                    , t2.in_points_text
                    , t2.in_correct_count
                    , ISNULL(t3.id, '')                          AS 'team_id'
                    , ISNULL(t3.map_short_org, t2.in_player_org) AS 'player_org'
                    , ISNULL(t3.in_team, t2.in_player_team)      AS 'player_team'
                    , ISNULL(t3.in_name, t2.in_player_name)      AS 'player_name'
                    , ISNULL(t3.in_sno, t2.in_player_sno)        AS 'player_sno'
                    , ISNULL(t3.in_team_players, '1')            AS 'player_count'
                    , t3.in_check_result
                    , t3.in_check_status
                    , t5.in_display     AS 'program_display'
                    , t5.in_name2       AS 'program_name2'
                    , t5.in_name3       AS 'program_name3'
                    , t5.in_battle_type AS 'program_battle_type'
                    , t11.in_photo  AS 'player_photo'
                    , t12.in_photo  AS 'org_photo'
                    , t13.in_name   AS 'site_name'
                FROM
                    IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
                    IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK)
                    ON t2.source_id = t1.id
                LEFT OUTER JOIN
                    VU_MEETING_PTEAM t3 WITH(NOLOCK)
                    ON t3.source_id = t1.source_id
                    AND t3.in_sign_no = t2.in_sign_no
                INNER JOIN
                    IN_MEETING_PROGRAM t5 WITH(NOLOCK)
                    ON t5.id = t1.source_id
                LEFT OUTER JOIN
                    IN_RESUME t11 WITH(NOLOCK)
                    ON t11.in_sno = t3.in_sno
                LEFT OUTER JOIN
                    IN_ORG t12 WITH(NOLOCK)
                    ON t12.in_group = t3.in_group
                    AND t12.in_current_org = t3.in_current_org
                LEFT OUTER JOIN
                    IN_MEETING_SITE t13 WITH(NOLOCK)
                    ON t13.id = t1.in_site
                WHERE
                    t1.source_id = '{#program_id}'
                    AND t1.in_tree_id = '{#in_tree_id}'
                ORDER BY
                    t1.in_round_id
                    , t2.in_sign_foot
            ";

            sql = sql.Replace("{#program_id}", program_id)
                .Replace("{#in_tree_id}", tree_id);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item items = inn.applySQL(sql);

            int count = items.getItemCount();

            if (count != 2)
            {
                return;
            }

            Item itmFirst = items.getItemByIndex(0);

            string hide_points = ""; //積分
            string hide_scores = ""; //成績

            string program_battle_type = itmFirst.getProperty("program_battle_type", "");
            switch (program_battle_type)
            {
                case "SingleRoundRobin":
                case "DoubleRoundRobin":
                case "GroupSRoundRobin":
                case "GroupDRoundRobin":
                    hide_scores = "item_show_0";
                    break;

                default:
                    hide_points = "item_show_0";
                    break;
            }

            string event_status = GetEventStatus(itmFirst.getProperty("in_win_status", ""));

            itmReturn.setProperty("program_id", program_id);
            itmReturn.setProperty("program_display", itmFirst.getProperty("program_display", ""));
            itmReturn.setProperty("program_name2", itmFirst.getProperty("program_name2", ""));
            itmReturn.setProperty("program_name3", itmFirst.getProperty("program_name3", ""));
            itmReturn.setProperty("hide_scores", hide_scores);
            itmReturn.setProperty("hide_points", hide_points);

            itmReturn.setProperty("event_id", itmFirst.getProperty("event_id", ""));
            itmReturn.setProperty("event_status", event_status);

            // itmReturn.setProperty("in_tree_name", itmFirst.getProperty("in_tree_name", ""));
            // itmReturn.setProperty("in_tree_alias", itmFirst.getProperty("in_tree_alias", ""));
            itmReturn.setProperty("in_tree_no", itmFirst.getProperty("in_tree_no", ""));
            itmReturn.setProperty("in_tree_id", itmFirst.getProperty("in_tree_id", ""));
            itmReturn.setProperty("in_site", itmFirst.getProperty("in_site", ""));
            itmReturn.setProperty("in_date_key", itmFirst.getProperty("in_date_key", ""));

            itmReturn.setProperty("in_round", itmFirst.getProperty("in_round", ""));

            SetModalItem(itmReturn, items.getItemByIndex(0), "f1");
            SetModalItem(itmReturn, items.getItemByIndex(1), "f2");

            //上一場
            SetLastEvent(CCO, strMethodName, inn, itmFirst, itmReturn);

            //下一場
            SetNextEvent(CCO, strMethodName, inn, itmFirst, itmReturn);
        }

        private void SetLastEvent(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmEvent, Item itmReturn)
        {
            string meeting_id = itmReturn.getProperty("meeting_id", "");
            string in_site = itmEvent.getProperty("in_site", "");
            string in_date_key = itmEvent.getProperty("in_date_key", "");
            string in_tree_no = itmEvent.getProperty("in_tree_no", "");
            string site_name = itmEvent.getProperty("site_name", "");

            if (in_tree_no == "" || in_tree_no == "0")
            {
                itmReturn.setProperty("last_event_title", "無上一場資料");
                itmReturn.setProperty("last_event_message", "");
                return;
            }

            if (in_tree_no == "1")
            {
                itmReturn.setProperty("last_event_title", "已無上一場資料");
                itmReturn.setProperty("last_event_message", "");
                return;
            }

            string sql = @"
                SELECT
                	MAX(in_tree_no) AS 'target_tree_no'
                FROM 
                	IN_MEETING_PEVENT WITH(NOLOCK)
                WHERE
                	ISNULL(in_site, '') = '{#in_site}'
                	AND ISNULL(in_date_key, '') = '{#in_date_key}'
                	AND ISNULL(in_tree_no, 0) <> 0
                	AND in_tree_no < {#in_tree_no}
                	--AND in_tree_name NOT IN ('rank56', 'rank78')
            ";

            sql = sql.Replace("{#in_site}", in_site)
                .Replace("{#in_date_key}", in_date_key)
                .Replace("{#in_tree_no}", in_tree_no);

            Item itmTreeNo = inn.applySQL(sql);
            string target_tree_no = itmTreeNo.getProperty("target_tree_no", "");

            if (target_tree_no == "")
            {
                itmReturn.setProperty("last_event_title", "已無上一場資料");
                itmReturn.setProperty("last_event_message", "");
                return;
            }

            Item itmTargetEvent = GetTargetEvent(CCO, strMethodName, inn, itmEvent, meeting_id, target_tree_no);
            int event_count = itmTargetEvent.getItemCount();

            if (event_count <= 0)
            {
                itmReturn.setProperty("last_event_title", "已無上一場資料");
                itmReturn.setProperty("last_event_message", "");
            }
            else if (event_count > 1)
            {
                string event_title = "[" + site_name + "]場次編號(" + target_tree_no + ")有重複資料";
                string event_message = "";
                for (int i = 0; i < event_count; i++)
                {
                    Item item = itmTargetEvent.getItemByIndex(i);
                    string _program_id = item.getProperty("program_id", "");
                    string _program_name3 = item.getProperty("program_name3", "");
                    string _in_tree_id = item.getProperty("in_tree_id", "");
                    event_message += "<a data-pid='" + _program_id + "' data-rid='" + _in_tree_id + "' onclick='MEvent_click(this)'>" + _program_name3 + "</a><br>";
                }

                itmReturn.setProperty("last_event_title", event_title);
                itmReturn.setProperty("last_event_message", event_message);
            }
            else
            {
                itmReturn.setProperty("last_event_pid", itmTargetEvent.getProperty("program_id", ""));
                itmReturn.setProperty("last_event_pname", itmTargetEvent.getProperty("program_name3", ""));
                itmReturn.setProperty("last_event_rid", itmTargetEvent.getProperty("in_tree_id", ""));
                itmReturn.setProperty("last_event_title", "");
                itmReturn.setProperty("last_event_message", "");
            }
        }

        private void SetNextEvent(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmEvent, Item itmReturn)
        {
            string meeting_id = itmReturn.getProperty("meeting_id", "");
            string in_site = itmEvent.getProperty("in_site", "");
            string in_date_key = itmEvent.getProperty("in_date_key", "");
            string in_tree_no = itmEvent.getProperty("in_tree_no", "");
            string site_name = itmEvent.getProperty("site_name", "");

            if (in_tree_no == "" || in_tree_no == "0")
            {
                itmReturn.setProperty("last_event_title", "無下一場資料");
                itmReturn.setProperty("last_event_message", "");
                return;
            }

            string sql = @"
                SELECT
                	MIN(in_tree_no) AS 'target_tree_no'
                FROM 
                	IN_MEETING_PEVENT WITH(NOLOCK)
                WHERE
                	ISNULL(in_site, '') = '{#in_site}'
                	AND ISNULL(in_date_key, '') = '{#in_date_key}'
                	AND ISNULL(in_tree_no, 0) <> 0
                	AND in_tree_no > {#in_tree_no}
                	--AND in_tree_name NOT IN ('rank56', 'rank78')
            ";

            sql = sql.Replace("{#in_site}", in_site)
                .Replace("{#in_date_key}", in_date_key)
                .Replace("{#in_tree_no}", in_tree_no);

            Item itmTreeNo = inn.applySQL(sql);
            string target_tree_no = itmTreeNo.getProperty("target_tree_no", "");

            if (target_tree_no == "")
            {
                itmReturn.setProperty("next_event_title", "已無下一場資料");
                itmReturn.setProperty("next_event_message", "");
                return;
            }

            Item itmTargetEvent = GetTargetEvent(CCO, strMethodName, inn, itmEvent, meeting_id, target_tree_no);
            int event_count = itmTargetEvent.getItemCount();

            if (event_count <= 0)
            {
                itmReturn.setProperty("next_event_title", "已無下一場資料");
                itmReturn.setProperty("next_event_message", "");
            }
            else if (event_count > 1)
            {
                string event_title = "[" + site_name + "]場次編號(" + target_tree_no + ")有重複資料";
                string event_message = "";

                for (int i = 0; i < event_count; i++)
                {
                    Item item = itmTargetEvent.getItemByIndex(i);
                    string _program_id = item.getProperty("program_id", "");
                    string _program_name3 = item.getProperty("program_name3", "");
                    string _in_tree_id = item.getProperty("in_tree_id", "");
                    event_message += "<a data-pid='" + _program_id + "' data-rid='" + _in_tree_id + "' onclick='MEvent_click(this)'>" + _program_name3 + "</a><br>";
                }

                itmReturn.setProperty("next_event_title", event_title);
                itmReturn.setProperty("next_event_message", event_message);
            }
            else
            {
                itmReturn.setProperty("next_event_pid", itmTargetEvent.getProperty("program_id", ""));
                itmReturn.setProperty("next_event_pname", itmTargetEvent.getProperty("program_name3", ""));
                itmReturn.setProperty("next_event_rid", itmTargetEvent.getProperty("in_tree_id", ""));
                itmReturn.setProperty("next_event_title", "");
                itmReturn.setProperty("next_event_message", "");
            }
        }

        //可能多筆
        private Item GetTargetEvent(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmEvent, string meeting_id, string in_tree_no)
        {
            string program_id = itmEvent.getProperty("program_id", "");
            string in_site = itmEvent.getProperty("in_site", "");
            string in_date_key = itmEvent.getProperty("in_date_key", "");

            string sql = @"
                SELECT 
                	t1.id			AS 'program_id'
                	, t1.in_name3	AS 'program_name3'
                	, t2.id			AS 'event_id'
                	, t2.in_tree_id
                	, t2.in_tree_no
                FROM
                	IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN 
                	IN_MEETING_PEVENT t2 WITH(NOLOCK)
                	ON t2.source_id = t1.id
                WHERE
                	t1.in_meeting = '{#meeting_id}'
                	AND ISNULL(t2.in_site, '') = '{#in_site}'
                	AND ISNULL(t2.in_date_key, '') = '{#in_date_key}'
                	AND t2.in_tree_no = {#in_tree_no}
                	--AND t2.in_tree_name NOT IN ('rank56', 'rank78')
                ORDER BY
                	t1.in_sort_order
            ";

            sql = sql.Replace("{#meeting_id}", meeting_id)
                .Replace("{#in_site}", in_site)
                .Replace("{#in_date_key}", in_date_key)
                .Replace("{#in_tree_no}", in_tree_no);

            return inn.applySQL(sql);
        }

        private void SetModalItem(Item itmReturn, Item source, string pfx)
        {
            string event_type = source.getProperty("event_type", "");

            if (event_type == "" || event_type == "p")
            {
                SetMainModalItem(itmReturn, source, pfx);
            }
            else
            {
                SetSubModalItem(itmReturn, source, pfx);
            }
        }

        //主場次
        private void SetMainModalItem(Item itmReturn, Item source, string pfx)
        {
            string detail_id = source.getProperty("detail_id", "");
            string in_sign_bypass = source.getProperty("in_sign_bypass", "");

            string player_count = source.getProperty("player_count", "");
            string player_photo = source.getProperty("player_photo", "");
            string org_photo = source.getProperty("org_photo", "");

            string player_org = source.getProperty("player_org", "　");
            string player_team = source.getProperty("player_team", "　");
            string player_name = source.getProperty("player_name", "　");
            string player_sno = source.getProperty("player_sno", "　");

            string in_sign_no = source.getProperty("in_sign_no", "　");
            string in_section_no = source.getProperty("in_section_no", "　");
            string in_check_result = source.getProperty("in_check_result", "");
            string in_status = source.getProperty("in_status", "");

            string in_points = source.getProperty("in_points", "");
            string in_points_type = source.getProperty("in_points_type", "");
            string in_points_text = source.getProperty("in_points_text", "");

            string in_correct_count = source.getProperty("in_correct_count", "0");

            bool is_signle = player_count == "1";
            string show_photo = is_signle ? player_photo : org_photo;
            string in_display = GetStatusDisplay(in_status);

            string in_show_name = player_name;

            if (!is_signle)
            {
                if (player_team == "")
                {
                    in_show_name = player_org;
                }
                else
                {
                    in_show_name = player_org + " " + player_team + "隊";
                }
            }

            if (player_name == "")
            {
                player_name = "&nbsp;";
            }

            string in_check = "";
            if (in_check_result == "1")
            {
                in_check = "btn-success";
            }
            else if (in_check_result == "0")
            {
                in_check = "btn-danger";
            }

            itmReturn.setProperty(pfx + "_detail_id", detail_id);
            itmReturn.setProperty(pfx + "_photo", show_photo);
            itmReturn.setProperty(pfx + "_name", player_name);
            itmReturn.setProperty(pfx + "_sno", player_sno);
            itmReturn.setProperty(pfx + "_short_org", player_org);
            itmReturn.setProperty(pfx + "_show_name", in_show_name);

            //itmReturn.setProperty(pfx + "_sign_no", in_sign_no);
            itmReturn.setProperty(pfx + "_sign_no", in_section_no);
            itmReturn.setProperty(pfx + "_check", in_check);
            itmReturn.setProperty(pfx + "_status", in_display);

            itmReturn.setProperty(pfx + "_correct_count", in_correct_count);

            if (in_points == "" || in_points == "0")
            {
                itmReturn.setProperty(pfx + "_points_1", "");
                itmReturn.setProperty(pfx + "_points_2", "");
                itmReturn.setProperty(pfx + "_points_3", "0");
            }
            else if (in_points == "1")
            {
                itmReturn.setProperty(pfx + "_points_1", "");
                itmReturn.setProperty(pfx + "_points_2", "");
                itmReturn.setProperty(pfx + "_points_3", "1");
            }
            else if (in_points == "10")
            {
                itmReturn.setProperty(pfx + "_points_1", "1");
                itmReturn.setProperty(pfx + "_points_2", "0");
                itmReturn.setProperty(pfx + "_points_3", "");
            }
            else if (in_points == "11")
            {
                itmReturn.setProperty(pfx + "_points_1", "1");
                itmReturn.setProperty(pfx + "_points_2", "1");
                itmReturn.setProperty(pfx + "_points_3", "");
            }
            itmReturn.setProperty(pfx + "_points_text", in_points_text);

            if (in_sign_no == "" || in_sign_no == "0" || in_sign_bypass == "1")
            {
                itmReturn.setProperty(pfx + "_hide", "item_show_0");
            }
        }

        //子場次
        private void SetSubModalItem(Item itmReturn, Item source, string pfx)
        {
            string detail_id = source.getProperty("detail_id", "");
            string in_sign_bypass = source.getProperty("in_sign_bypass", "");

            string player_count = source.getProperty("player_count", "");
            string player_photo = source.getProperty("player_photo", "");
            string org_photo = source.getProperty("org_photo", "");

            string player_org = source.getProperty("player_org", "　");
            string player_team = source.getProperty("player_team", "　");
            string player_name = source.getProperty("player_name", "　");
            string player_sno = source.getProperty("player_sno", "　");

            string in_sign_no = source.getProperty("in_sign_no", "　");
            string in_section_no = source.getProperty("in_section_no", "　");
            string in_check_result = source.getProperty("in_check_result", "");
            string in_status = source.getProperty("in_status", "");

            string in_points = source.getProperty("in_points", "");
            string in_points_type = source.getProperty("in_points_type", "");
            string in_points_text = source.getProperty("in_points_text", "");

            string in_correct_count = source.getProperty("in_correct_count", "0");

            bool is_signle = player_count == "1";
            string show_photo = is_signle ? player_photo : org_photo;
            string in_display = GetStatusDisplay(in_status);

            string in_show_name = player_name;

            if (!is_signle)
            {
                if (player_team == "")
                {
                    in_show_name = player_org;
                }
                else
                {
                    in_show_name = player_org + " " + player_team + "隊";
                }
            }

            if (player_name == "")
            {
                player_name = "&nbsp;";
            }

            string in_check = "";
            if (in_check_result == "1")
            {
                in_check = "btn-success";
            }
            else if (in_check_result == "0")
            {
                in_check = "btn-danger";
            }

            itmReturn.setProperty(pfx + "_detail_id", detail_id);
            itmReturn.setProperty(pfx + "_photo", show_photo);
            itmReturn.setProperty(pfx + "_name", player_name);
            itmReturn.setProperty(pfx + "_sno", player_sno);
            itmReturn.setProperty(pfx + "_short_org", player_org);
            itmReturn.setProperty(pfx + "_show_name", in_show_name);

            //itmReturn.setProperty(pfx + "_sign_no", in_sign_no);
            //itmReturn.setProperty(pfx + "_sign_no", in_section_no);
            itmReturn.setProperty(pfx + "_check", in_check);
            itmReturn.setProperty(pfx + "_status", in_display);

            itmReturn.setProperty(pfx + "_correct_count", in_correct_count);

            if (in_points == "" || in_points == "0")
            {
                itmReturn.setProperty(pfx + "_points_1", "");
                itmReturn.setProperty(pfx + "_points_2", "");
                itmReturn.setProperty(pfx + "_points_3", "0");
            }
            else if (in_points == "1")
            {
                itmReturn.setProperty(pfx + "_points_1", "");
                itmReturn.setProperty(pfx + "_points_2", "");
                itmReturn.setProperty(pfx + "_points_3", "1");
            }
            else if (in_points == "10")
            {
                itmReturn.setProperty(pfx + "_points_1", "1");
                itmReturn.setProperty(pfx + "_points_2", "0");
                itmReturn.setProperty(pfx + "_points_3", "");
            }
            else if (in_points == "11")
            {
                itmReturn.setProperty(pfx + "_points_1", "1");
                itmReturn.setProperty(pfx + "_points_2", "1");
                itmReturn.setProperty(pfx + "_points_3", "");
            }
            itmReturn.setProperty(pfx + "_points_text", in_points_text);

            //if (in_sign_no == "" || in_sign_no == "0" || in_sign_bypass == "1")
            //{
            //    itmReturn.setProperty(pfx + "_hide", "item_show_0");
            //}
        }

        //取得勝敗呈現
        private string GetStatusDisplay(string value)
        {
            switch (value)
            {
                case "1": return "<span class='team_score_b'>1</span>";
                case "0": return "<span class='team_score_c'>0</span>";
                default: return "<span class='team_score_a'>0</span>";
            }
        }

        //取得場次勝敗狀態
        private string GetEventStatus(string value)
        {
            switch (value)
            {
                case "cancel": return "(本場次取消)";

                case "nofight": return "(一方取消)";

                case "bypass": return "(輪空)";

                default: return "";
            }
        }
        private void AppendLevelOptions(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            string meeting_id = itmReturn.getProperty("meeting_id", "");

            Item itmJson = inn.newItem("In_Meeting");
            itmJson.setProperty("meeting_id", meeting_id);
            itmJson.setProperty("need_id", "1");
            itmJson = itmJson.apply("in_meeting_program_options");
            itmReturn.setProperty("in_level_json", itmJson.getProperty("json", ""));
            itmReturn.setProperty("in_level_count", itmJson.getProperty("total", ""));
        }

        private void AppendEventOptions(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            string program_id = itmReturn.getProperty("program_id", "");

            string sql = @"
                SELECT 
                    DISTINCT 
                    in_tree_sort
                    , in_tree_name
                    , in_round
                    , in_tree_id
                    , in_tree_no
                    , in_tree_rank
                    , in_tree_alias
                    , in_win_status
                FROM
                    in_meeting_pevent WITH(NOLOCK)
                WHERE
                    source_id = '{#program_id}'
                    AND ISNULL(in_tree_no, '') <> '' 
                ORDER BY
                    in_tree_sort
                    , in_round
                    , in_tree_no
             ";

            sql = sql.Replace("{#program_id}", program_id);

            //CCO.Utilities.WriteDebug(strMethodName, sql);

            Item items = inn.applySQL(sql);

            int count = items.getItemCount();

            List<TNode> nodes = new List<TNode>();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);

                TNode l1 = AddAndGetNode(nodes, item, "in_tree_name");

                if (string.IsNullOrEmpty(l1.Lbl))
                {
                    switch (l1.Val)
                    {
                        case "main":
                            l1.Lbl = "對戰表";
                            l1.Sort = GetIntVal(item.getProperty("in_tree_sort", ""));
                            break;

                        case "repechage":
                            l1.Lbl = "復活表";
                            l1.Sort = GetIntVal(item.getProperty("in_tree_sort", ""));
                            break;

                        case "challenge-a":
                            l1.Lbl = "挑戰賽";
                            l1.Sort = GetIntVal(item.getProperty("in_tree_sort", ""));
                            break;

                        case "challenge-b":
                            l1.Lbl = "挑戰賽-最終賽";
                            l1.Sort = GetIntVal(item.getProperty("in_tree_sort", ""));
                            break;

                        case "rank56":
                            l1.Lbl = "五六名對戰";
                            l1.Sort = GetIntVal(item.getProperty("in_tree_sort", ""));
                            break;

                        case "rank78":
                            l1.Lbl = "七八名對戰";
                            l1.Sort = GetIntVal(item.getProperty("in_tree_sort", ""));
                            break;

                        default:
                            l1.Lbl = l1.Val;
                            l1.Sort = 99999;
                            break;
                    }
                }

                TNode l2 = AddAndGetNode(l1.Nodes, item, "in_round");

                if (string.IsNullOrEmpty(l2.Lbl))
                {
                    l2.Lbl = "R" + l2.Val;
                }

                TNode l3 = AddAndGetNode(l2.Nodes, item, "in_tree_id");
                string in_tree_no = item.getProperty("in_tree_no", "");
                string in_win_status = item.getProperty("in_win_status", "");

                l3.Lbl = "第 " + in_tree_no + " 場";
                if (in_win_status != "")
                {
                    l3.Lbl += "(已登錄)";
                }
            }

            itmReturn.setProperty("in_event_json", Newtonsoft.Json.JsonConvert.SerializeObject(nodes.OrderBy(x => x.Sort)));
        }

        private TNode AddAndGetNode(List<TNode> nodes, Item item, string key)
        {
            string value = item.getProperty(key, "");

            TNode search = nodes.Find(x => x.Val == value);

            if (search == null)
            {
                search = new TNode
                {
                    Val = value,
                    Nodes = new List<TNode>()
                };

                nodes.Add(search);
            }

            return search;
        }

        private class TNode
        {
            public string Val { get; set; }

            public string Lbl { get; set; }

            public int Sort { get; set; }

            public List<TNode> Nodes { get; set; }
        }

        private int GetIntVal(string value, int def = 0)
        {
            int result = def;
            Int32.TryParse(value, out result);
            return result;
        }
    }
}