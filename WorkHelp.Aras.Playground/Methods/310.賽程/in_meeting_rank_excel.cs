﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;
using System.Net;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class in_meeting_rank_excel : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 匯出名次Excel表
                位置: In_Competition_Bulletin_N1.html
                日期: 
                    - 2021-10-09 修改 (Lina)
                    - 2020-11-17 創建 (Alan)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]in_meeting_rank_excel";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, this.dom.InnerXml);

            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                InnH = _InnH,

                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                in_date = itmR.getProperty("in_date", ""),
                program_id = itmR.getProperty("program_id", ""),
                scene = itmR.getProperty("scene", ""),
                CharSet = GetCharSet(),
            };

            cfg.itmMeeting = cfg.inn.applySQL("SELECT in_title FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            cfg.mt_title = cfg.itmMeeting.getProperty("in_title", "");

            switch (cfg.scene)
            {
                case "sum_page":
                    cfg.IsSumReport = true;
                    SumFunc(cfg, itmR, SumPage);
                    break;

                case "sum_xlsx":
                    cfg.IsSumReport = true;
                    SumFunc(cfg, itmR, SumXlsx);
                    break;

                case "rank_xlsx":
                    cfg.IsSumReport = false;
                    RankXlsx(cfg, itmR);
                    break;
            }

            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

            return itmR;
        }

        #region 獎牌統計

        private void SumFunc(TConfig cfg, Item itmReturn, Action<TConfig, List<TMedal>, List<TGroup>, Item> action)
        {
            var medals = new List<TMedal>();
            medals.Add(new TMedal { rank = 1, label = "冠軍", ranks = new int[] { 1 }, prop = "golden_count" });
            medals.Add(new TMedal { rank = 2, label = "亞軍", ranks = new int[] { 2 }, prop = "silver_count" });
            medals.Add(new TMedal { rank = 3, label = "季軍", ranks = new int[] { 3, 4 }, prop = "copper_count" });
            medals.Add(new TMedal { rank = 5, label = "第五名", ranks = new int[] { 5, 6 }, prop = "fifth_count" });
            medals.Add(new TMedal { rank = 7, label = "第七名", ranks = new int[] { 7, 8 }, prop = "seventh_count" });

            var items = GetScores(cfg);
            var groups = MapGroup(cfg, items);
            action(cfg, medals, groups, itmReturn);
        }

        private void SumXlsx(TConfig cfg, List<TMedal> medals, List<TGroup> groups, Item itmReturn)
        {
            var exp = ExportInfo(cfg, "");

            //試算表
            Spire.Xls.Workbook workbook = new Spire.Xls.Workbook();

            foreach (var gp in groups)
            {
                var orgs = MapOrgList(cfg, medals, gp);
                SumXlsxSheet(cfg, workbook, medals, gp, orgs);
            }

            workbook.Worksheets[0].Remove();
            workbook.Worksheets[0].Remove();
            workbook.Worksheets[0].Remove();

            string ext_name = ".xlsx";
            string xls_name = cfg.mt_title + "_獎牌統計_" + DateTime.Now.ToString("MMdd_HHmmss");
            string xls_file = exp.export_Path + "\\" + xls_name + ext_name;
            string xls_url = xls_name + ext_name;

            workbook.SaveToFile(xls_file, Spire.Xls.ExcelVersion.Version2013);

            itmReturn.setProperty("xls_name", xls_url);
        }

        private void SumXlsxSheet(TConfig cfg, Spire.Xls.Workbook workbook, List<TMedal> medals, TGroup gp, List<TOrg> orgs)
        {
            Spire.Xls.Worksheet sheet = workbook.CreateEmptySheet();
            sheet.Name = gp.sheetName;

            List<TField> fields = new List<TField>();
            fields.Add(new TField { ci = 1, title = "序號", property = "no", css = TCss.Center, width = 8 });
            fields.Add(new TField { ci = 2, title = "單位", property = "map_short_org", css = TCss.None, width = 18 });

            int ci = 3;
            foreach (var medal in medals)
            {
                fields.Add(new TField { ci = ci, title = medal.label, property = medal.prop, css = TCss.Number, width = 10 });
                ci++;
            }

            fields.Add(new TField { ci = ci, title = "總計", property = "total", css = TCss.Number, width = 5 });

            MapCharSet(cfg, fields);

            var mt_mr = sheet.Range["A1:H1"];
            mt_mr.Merge();
            mt_mr.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
            mt_mr.Text = cfg.mt_title;
            mt_mr.Style.Font.Size = 16;
            mt_mr.RowHeight = 20;

            var rpt_mr = sheet.Range["A2:H2"];
            rpt_mr.Merge();
            rpt_mr.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
            rpt_mr.Text = "獎牌統計";
            rpt_mr.Style.Font.Size = 16;
            rpt_mr.RowHeight = 20;

            int mnRow = 3;
            int wsRow = 3;

            //標題列
            SetHeadRow(sheet, wsRow, fields);
            wsRow++;

            //凍結窗格
            sheet.FreezePanes(wsRow, fields.Last().ci + 1);

            //資料容器
            Item item = cfg.inn.newItem();
            int no = 0;

            foreach (var org in orgs)
            {
                no++;

                item.setProperty("no", no.ToString());
                item.setProperty("map_short_org", org.map_short_org);
                item.setProperty("golden_count", org.golden_count.ToString());
                item.setProperty("silver_count", org.silver_count.ToString());
                item.setProperty("copper_count", org.copper_count.ToString());
                item.setProperty("fifth_count", org.fifth_count.ToString());
                item.setProperty("seventh_count", org.seventh_count.ToString());
                item.setProperty("total", org.total.ToString());

                SetItemCell(cfg, sheet, wsRow, item, fields);

                wsRow++;
            }

            //設定格線
            SetRangeBorder(sheet, fields, mnRow, wsRow);

            //設定欄寬
            SetColumnWidth(sheet, fields);
        }

        private void SumPage(TConfig cfg, List<TMedal> medals, List<TGroup> groups, Item itmReturn)
        {
            StringBuilder builder = new StringBuilder();

            AppendTabHead(cfg, medals, groups, builder);
            AppendTabBody(cfg, medals, groups, builder);

            itmReturn.setProperty("inn_tab", builder.ToString());
            itmReturn.setProperty("in_title", cfg.mt_title);
        }

        private void AppendTabHead(TConfig cfg, List<TMedal> medals, List<TGroup> groups, StringBuilder builder)
        {
            builder.Append("<ul class='nav nav-pills' style='justify-content: start;' id='pills-tab' role='tablist'>");
            foreach (var gp in groups)
            {
                string active = gp.Id == "1" ? " active" : "";
                builder.Append("<li class='nav-item" + active + "'>");
                builder.Append("  <a class='nav-link' data-toggle='pill' id='" + gp.Tab_Id + "'");
                builder.Append("     role='tab' aria-controls='pills-home' href='#" + gp.Div_Id + "' onclick='StoreTab_Click(this)'>");
                builder.Append("  " + gp.sheetName);
                builder.Append("  </a>");
                builder.Append("</li>");
            }
            builder.Append("</ul>");
        }

        private void AppendTabBody(TConfig cfg, List<TMedal> medals, List<TGroup> groups, StringBuilder builder)
        {
            builder.Append("<div class='tab-content page-tab-content' id='pills-tabContent'>");

            foreach (var gp in groups)
            {
                var orgs = MapOrgList(cfg, medals, gp);

                string active = gp.Id == "1" ? " active in" : "";

                builder.Append("<div id='" + gp.Div_Id + "' class='tab-pane " + active + "' role='tabpanel'>");
                builder.Append("  <div class='box-body row container-row'>");
                builder.Append("    <div class='box box-fix'>");
                builder.Append("      <div class='box-body'>");

                AppendTable(cfg, medals, gp, orgs, builder);


                builder.Append("      </div>");
                builder.Append("    </div>");
                builder.Append("  </div>");
                builder.Append("</div>");
            }
            builder.Append("</div>");
        }

        private void AppendTable(TConfig cfg, List<TMedal> medals, TGroup gp, List<TOrg> orgs, StringBuilder builder)
        {
            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            head.Append("<thead>");
            head.Append("<tr>");
            head.Append("<th class='text-center'>&nbsp;</th>");
            head.Append("<th class='text-center'>單位</th>");
            for (int i = 0; i < medals.Count; i++)
            {
                var medal = medals[i];
                head.Append("<th class='text-center'>" + medal.label + "</th>");
            }
            head.Append("<th class='text-center'>總計</th>");
            head.Append("</tr>");
            head.Append("</thead>");


            body.Append("<tbody>");

            for (int i = 0; i < orgs.Count; i++)
            {
                var org = orgs[i];

                var tr_id = gp.Table_Id + "_tr_" + (i + 1);

                body.Append("<tr>");

                var no_wait = false;
                if (no_wait)
                {
                    body.Append("<td class='text-center'> &nbsp; </td>");
                }
                else
                {
                    body.Append("<td class='text-center'> <span class='event-btn' onclick='ToggleWaitRow_Click(this)' data-id='" + tr_id + "' data-state='hide'> &nbsp; + &nbsp; </span> </td>");
                }

                body.Append("<td class='text-center'>" + org.map_short_org + "</td>");
                for (int j = 0; j < org.medals.Count; j++)
                {
                    var org_medal = org.medals[j];
                    body.Append("<td class='text-center'>" + org_medal.count + "</td>");
                }
                body.Append("<td class='text-center'>" + org.total + "</td>");
                body.Append("</tr>");


                if (!no_wait)
                {
                    //body.Append("<tr id='" + tr_id + "' style='display: none'>");
                    body.Append("<tr id='" + tr_id + "' >");
                    body.Append("<td class='text-center' style='vertical-align: middle'> &nbsp; </td>");
                    body.Append("<td class='text-center' colspan='7'>");
                    body.Append(AppendRanks(cfg, gp, org));
                    body.Append("</td>");


                    body.Append("</tr>");
                }
            }
            body.Append("</tbody>");

            builder.Append("<table id='" + gp.Table_Id + "' class='table table-hover table-bordered table-rwd rwd rwdtable match-table'>");
            builder.Append(head);
            builder.Append(body);
            builder.Append("</table>");
        }

        private StringBuilder AppendRanks(TConfig cfg, TGroup gp, TOrg org)
        {
            StringBuilder builder = new StringBuilder();
            
            builder.Append("<table class='table table-bordered table-rwd rwd'>");
            builder.Append("  <tbody>");

            var teams = org.Teams.OrderBy(x => x.rank).ToList();

            for (int i = 0; i < teams.Count; i++)
            {
                var team = teams[i];

                builder.Append("<tr>");
                builder.Append("  <td>"+ team.pg_name + "</td>");
                builder.Append("  <td>"+ team.player_name + "</td>");
                builder.Append("  <td>名次："+ team.rank + "</td>");
                builder.Append("</tr>");
            }

            builder.Append("  </tbody>");
            builder.Append("</table>");
            
            return builder;
        }

        private List<TOrg> MapOrgList(TConfig cfg, List<TMedal> medals, TGroup gp)
        {
            List<TOrg> list = new List<TOrg>();

            var items = gp.Values;

            int count = items.Count;

            for (int i = 0; i < count; i++)
            {
                Item item = items[i];
                string in_stuff_b1 = item.getProperty("in_stuff_b1", "");
                string map_short_org = item.getProperty("map_short_org", "");
                string in_final_rank = item.getProperty("in_final_rank", "0");
                int final_rank = GetIntVal(in_final_rank);

                var org = list.Find(x => x.map_short_org == map_short_org);

                if (org == null)
                {
                    org = new TOrg
                    {
                        in_stuff_b1 = in_stuff_b1,
                        map_short_org = map_short_org,
                        medals = new List<TMedal>(),
                        Teams = new List<TTeam>(),
                    };

                    for (int j = 0; j < medals.Count; j++)
                    {
                        var medal = medals[j];
                        org.medals.Add(new TMedal
                        {
                            rank = medal.rank,
                            ranks = medal.ranks,
                            label = medal.label,
                            count = 0,
                            items = new List<Item>(),
                        });
                    }
                    list.Add(org);
                }

                var rank_medal = org.medals.Find(x => x.ranks.Contains(final_rank));
                if (rank_medal == null) throw new Exception("異常");

                rank_medal.count++;
                rank_medal.items.Add(item);

                org.Teams.Add(MapTeam(final_rank, item));
            }

            for (int i = 0; i < list.Count; i++)
            {
                var org = list[i];

                for (int j = 0; j < org.medals.Count; j++)
                {
                    var medal = org.medals[j];

                    switch (medal.rank)
                    {
                        case 1: org.golden_count = medal.count; break;
                        case 2: org.silver_count = medal.count; break;
                        case 3: org.copper_count = medal.count; break;
                        case 5: org.fifth_count = medal.count; break;
                        case 7: org.seventh_count = medal.count; break;
                    }

                    org.total += medal.count;
                }
            }

            return list.OrderByDescending(x => x.golden_count)
                .ThenByDescending(x => x.silver_count)
                .ThenByDescending(x => x.copper_count)
                .ToList();
        }

        private TTeam MapTeam(int rank, Item item)
        {
            string pg_name = item.getProperty("program_name2", "");
            string in_team = item.getProperty("in_team", "");
            string in_names = item.getProperty("in_names", "");
            string in_show_rank = item.getProperty("in_show_rank", "");

            if (in_team != "") in_names = "(" + in_team + "隊)" + in_names;

            TTeam result = new TTeam 
            {
                rank = rank,
                pg_name = pg_name,
                player_name = in_names,
            };

            return result;
        }

        private Item GetScores(TConfig cfg)
        {
            string sql = @"
                SELECT
	                t2.in_stuff_b1
	                , t2.map_short_org
	                , t2.in_team
	                , t2.in_names
	                , t2.in_final_rank 
	                , t2.in_show_rank 
	                , t1.id               AS 'program_id'
	                , t1.in_name2         AS 'program_name2'
	                , t1.in_short_name    AS 'program_short_name'
                FROM 
	                IN_MEETING_PROGRAM t1
                INNER JOIN
	                VU_MEETING_PTEAM t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                WHERE 
	                t1.in_meeting = '{#meeting_id}'
	                AND ISNULL(t2.in_final_rank, 0) > 0
                ORDER BY
	                t1.in_sort_order
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        private class TMedal
        {
            public int rank { get; set; }
            public int[] ranks { get; set; }
            public string label { get; set; }
            public string prop { get; set; }
            public int count { get; set; }
            public List<Item> items { get; set; }
        }

        private class TOrg
        {
            public string in_stuff_b1 { get; set; }
            public string map_short_org { get; set; }
            public List<TMedal> medals { get; set; }

            public int golden_count { get; set; }
            public int silver_count { get; set; }
            public int copper_count { get; set; }
            public int fifth_count { get; set; }
            public int seventh_count { get; set; }

            public int total { get; set; }

            public List<TTeam> Teams { get; set; }
        }

        private class TTeam
        {
            public int rank { get; set; }
            public string pg_name { get; set; }
            public string player_name { get; set; }
        }

        #endregion 獎牌統計

        #region 名次清單

        private void RankXlsx(TConfig cfg, Item itmReturn)
        {
            if (cfg.program_id == "")
            {
                ExportRankTable(cfg, itmReturn);
            }
            else
            {
                ExportRankTable(cfg, itmReturn);
            }
        }

        private void ExportRankTable(TConfig cfg, Item itmReturn)
        {
            Item items = GetRankList(cfg);

            if (items.getResult() == "")
            {
                throw new Exception("該組尚無資料再請確認!");
            }

            var exp = ExportInfo(cfg, "rank_path");

            var groups = MapGroup(cfg, items);
            //試算表
            Spire.Xls.Workbook workbook = new Spire.Xls.Workbook();

            foreach (var gp in groups)
            {
                AppendRankTable(cfg, workbook, gp);
            }

            workbook.Worksheets[0].Remove();
            workbook.Worksheets[0].Remove();
            workbook.Worksheets[0].Remove();

            string ext_name = ".xlsx";
            string xls_name = cfg.mt_title + "_名次總表_" + DateTime.Now.ToString("MMdd_HHmmss");
            string xls_file = exp.export_Path + "\\" + xls_name + ext_name;
            string xls_url = xls_name + ext_name;

            if (cfg.in_date != "")
            {
                xls_name = cfg.itmMeeting.getProperty("in_title", "") + "_" + cfg.in_date + "_名次總表_" + DateTime.Now.ToString("MMdd_HHmmss");
            }

            workbook.SaveToFile(xls_file, Spire.Xls.ExcelVersion.Version2013);

            itmReturn.setProperty("xls_name", xls_name + ".xlsx");
        }

        private void AppendRankTable(TConfig cfg, Spire.Xls.Workbook workbook, TGroup gp)
        {
            Spire.Xls.Worksheet sheet = workbook.CreateEmptySheet();
            sheet.Name = gp.sheetName;

            //頁面配置-邊界
            sheet.PageSetup.TopMargin = 0.5;
            // sheet.PageSetup.BottomMargin = 0.5;
            // sheet.PageSetup.LeftMargin = 0.5;
            // sheet.PageSetup.RightMargin = 0.5;

            //列印標題
            sheet.PageSetup.PrintTitleRows = "$1:$2";

            var mt_mr = sheet.Range["A1:G1"];
            mt_mr.Merge();
            mt_mr.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
            mt_mr.Text = cfg.mt_title;
            mt_mr.Style.Font.Size = 16;
            mt_mr.Style.Font.IsBold = true;
            mt_mr.RowHeight = 20;

            var rpt_mr = sheet.Range["A2:G2"];
            rpt_mr.Merge();
            rpt_mr.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
            rpt_mr.Text = "名次清單";
            rpt_mr.Style.Font.Size = 16;
            rpt_mr.RowHeight = 20;

            //男子
            List<TField> fields_m = new List<TField>();
            fields_m.Add(new TField { ci = 1, cs = "A", title = "名次", getValue = RankName, css = TCss.Center, width = 6 });
            fields_m.Add(new TField { ci = 2, cs = "B", title = "單位", getValue = OrgName, css = TCss.Center, width = 15 });
            fields_m.Add(new TField { ci = 3, cs = "C", title = "姓名/隊別", getValue = PlayerName, css = TCss.Center, width = 15 });

            var programs_m = gp.programs.FindAll(x => x.name.Contains("男"));
            AppendHalfSheet(cfg, sheet, fields_m, programs_m);


            //女子
            List<TField> fields_w = new List<TField>();
            fields_w.Add(new TField { ci = 5, cs = "E", title = "名次", getValue = RankName, css = TCss.Center, width = 6 });
            fields_w.Add(new TField { ci = 6, cs = "F", title = "單位", getValue = OrgName, css = TCss.Center, width = 15 });
            fields_w.Add(new TField { ci = 7, cs = "G", title = "姓名/隊別", getValue = PlayerName, css = TCss.Center, width = 15 });

            var programs_w = gp.programs.FindAll(x => x.name.Contains("女"));
            AppendHalfSheet(cfg, sheet, fields_w, programs_w);
        }

        private void AppendHalfSheet(TConfig cfg, Spire.Xls.Worksheet sheet, List<TField> fields, List<TProgram> programs)
        {
            int mnRow = 4;
            int wsRow = 4;

            //資料容器
            Item item = cfg.inn.newItem();

            var fs = fields.First();
            var fe = fields.Last();

            foreach (var program in programs)
            {
                var teams = program.Values;

                var pg_mr = sheet.Range[fs.cs + wsRow + ":" + fe.cs + wsRow];
                pg_mr.Merge();
                pg_mr.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                pg_mr.Text = program.name;
                pg_mr.Style.Font.Size = 12;
                pg_mr.RowHeight = 16;
                wsRow++;

                //標題列
                SetHeadRow(sheet, wsRow, fields, need_color: false);
                wsRow++;

                //foreach (var team in teams)
                //{
                //    SetItemCell(cfg, sheet, wsRow, team, fields);
                //    wsRow++;
                //}

                //維持八列
                var max_row = 8;
                var max_idx = teams.Count - 1;
                for (int i = 0; i < 8; i++)
                {
                    if (i <= max_idx)
                    {
                        var team = teams[i];
                        SetItemCell(cfg, sheet, wsRow, team, fields);
                    }
                    wsRow++;
                }

                var rs = wsRow - max_row - 2;
                var re = wsRow - 1;
                SetRangeBorder(sheet, fs.cs + rs + ":" + fe.cs + re);

                wsRow++;
            }

            ////設定格線
            //SetRangeBorder(sheet, fields, mnRow, wsRow);

            //設定欄寬
            SetColumnWidth(sheet, fields);
        }

        private string RankName(TConfig cfg, TField field, Item item)
        {
            //string[] rank_arr = new string[] { "", "一", "二", "三", "四", "五", "五", "七", "七", "" };
            return item.getProperty("in_show_rank", "");
        }

        private string OrgName(TConfig cfg, TField field, Item item)
        {
            return item.getProperty("map_short_org", "");
        }

        private string PlayerName(TConfig cfg, TField field, Item item)
        {
            return item.getProperty("in_name", "");
        }

        private List<TGroup> MapGroup(TConfig cfg, Item items)
        {
            List<TGroup> result = new List<TGroup>();

            List<TProgram> programs = MapProgram(items);


            foreach (var program in programs)
            {
                program.sheetName = GetSheetName(cfg, program.short_name);

                var gp = result.Find(x => x.sheetName == program.sheetName);

                if (gp == null)
                {
                    gp = new TGroup
                    {
                        sheetName = program.sheetName,
                        programs = new List<TProgram>(),
                        Values = new List<Item>(),

                        Id = (result.Count + 1).ToString(),
                    };

                    gp.Tab_Id = "tab_a_" + gp.Id;
                    gp.Div_Id = "tab_div_" + gp.Id;
                    gp.Table_Id = "tab_div_tb_" + gp.Id;

                    result.Add(gp);
                }

                gp.programs.Add(program);
                gp.Values.AddRange(program.Values);
            }
            return result;
        }

        private List<TProgram> MapProgram(Item items)
        {
            List<TProgram> list = new List<TProgram>();

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string id = item.getProperty("program_id", "");
                string program_name2 = item.getProperty("program_name2", "");
                string program_short_name = item.getProperty("program_short_name", "");

                var entity = list.Find(x => x.id == id);
                if (entity == null)
                {
                    entity = new TProgram
                    {
                        id = id,
                        name = program_name2,
                        short_name = program_short_name,
                        Values = new List<Item>(),
                    };
                    list.Add(entity);
                }

                entity.Values.Add(item);
            }

            return list;
        }

        private string GetSheetName(TConfig cfg, string value)
        {
            var v2 = value.Replace("男", "").Replace("女", "");

            if (cfg.IsSumReport)
            {
                if (v2.StartsWith("團")) return v2;
                if (v2.StartsWith("格")) return v2;
            }
            else
            {
                if (v2.StartsWith("團")) return "團體賽";
                if (v2.StartsWith("格")) return "格式";
            }

            if (v2.Length == 3)
            {
                if (v2.StartsWith("小"))
                {
                    return "國小";
                }
                else
                {
                    return v2.Substring(0, 2);
                }
            }

            if (v2.Length == 2)
            {
                var v3 = v2.Substring(0, 1);

                switch (v3)
                {
                    case "一": return "一般";
                    case "公": return "公開";

                    case "大": return "大專";
                    case "高": return "高中";
                    case "國": return "國中";
                    case "小": return "國小";

                    case "少": return "青少年";
                    case "青": return "青年";
                    case "成": return "成年";
                    case "社": return "社會";
                    default: return "名次總表";
                }
            }

            return "名次總表";
        }

        private Item GetRankList(TConfig cfg)
        {
            string condition = "";

            if (cfg.program_id != "")
            {
                condition = "AND t1.source_id = '" + cfg.program_id + "'";
            }
            else if (cfg.in_date != "")
            {
                condition = "AND t3.in_date_key = '" + cfg.in_date + "'";
            }

            string sql = @"
                SELECT
                    t1.in_current_org
                    , t1.in_name
                    , t1.in_sno
                    , t1.in_final_rank
                    , t1.in_show_rank
                    , t1.in_sign_no
                    , t1.in_section_no
                    , t1.map_short_org
                    , t1.map_org_name
                    , t2.id             AS 'program_id'
                    , t2.in_name        AS 'program_name'
                    , t2.in_name2       AS 'program_name2'
                    , t2.in_name3       AS 'program_name3'
                    , t2.in_display     AS 'program_display'
                    , t2.in_short_name  AS 'program_short_name'
                    , t2.in_sort_order  AS 'program_sort'
                FROM 
                    VU_MEETING_PTEAM t1 WITH(NOLOCK)
                INNER JOIN
                    IN_MEETING_PROGRAM t2 WITH(NOLOCK)
                    ON t2.id = t1.source_id
                INNER JOIN
                    IN_MEETING_ALLOCATION t3 WITH(NOLOCK)
                    ON t3.in_program = t2.id
                WHERE
                    t1.in_meeting = '{#meeting_id}'
                    {#condition}
                    AND ISNULL(t1.in_final_rank, '0') NOT IN ('', '0')
                ORDER BY
                    t2.in_sort_order
                    , t1.in_final_rank
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                      .Replace("{#condition}", condition);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        private string GetRankDisplay(string in_final_rank, string[] rank_arr)
        {
            string rank_display = "";

            int numRank = GetIntVal(in_final_rank);

            if (numRank > rank_arr.Length)
            {
                rank_display = "";
            }
            else
            {
                if (rank_arr[numRank] != "")
                {
                    rank_display = "第 " + rank_arr[numRank] + " 名"; ;
                }
            }
            return rank_display;
        }

        private class TGroup
        {
            public string sheetName { get; set; }
            public List<TProgram> programs { get; set; }
            public List<Item> Values { get; set; }

            public string Id { get; set; }
            public string Tab_Id { get; set; }
            public string Div_Id { get; set; }
            public string Table_Id { get; set; }
        }

        private class TProgram
        {
            public string id { get; set; }
            public string name { get; set; }
            public string short_name { get; set; }
            public List<Item> Values { get; set; }
            public string sheetName { get; set; }
        }

        #endregion 名次清單

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public Innosoft.InnovatorHelper InnH { get; set; }

            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }

            public string meeting_id { get; set; }
            public string in_date { get; set; }
            public string program_id { get; set; }
            public string scene { get; set; }

            public Item itmMeeting { get; set; }
            public string mt_title { get; set; }

            public string[] CharSet { get; set; }
            public bool IsSumReport { get; set; }
        }

        #region Xlsx

        private class TExport
        {
            public string template_Path { get; set; }
            public string export_Path { get; set; }
        }

        private class TField
        {
            public string title { get; set; }
            public string property { get; set; }
            public TCss css { get; set; }
            public int ci { get; set; }
            public string cs { get; set; }
            public int width { get; set; }
            public Func<TConfig, TField, Item, string> getValue { get; set; }

            public bool is_merge { get; set; }
        }

        private enum TCss
        {
            None = 0,
            Center = 10,
            Text = 100,
            Number = 200,
            Money = 300,
            Day = 400,
        }

        private string[] GetCharSet()
        {
            return new string[] { "", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
        }

        private TExport ExportInfo(TConfig cfg, string in_name)
        {
            Item itmXls = cfg.inn.applyMethod("In_Meeting_Excel_Path", "<in_name>" + in_name + "</in_name>");
            string Export_Path = itmXls.getProperty("export_path", "");
            string Template_Path = itmXls.getProperty("template_path", "");

            TExport result = new TExport
            {
                template_Path = Template_Path,
                export_Path = Export_Path,
            };

            return result;
        }

        //設定資料列
        private void SetItemCell(TConfig cfg, Spire.Xls.Worksheet sheet, int wsRow, Item item, List<TField> fields)
        {
            foreach (var field in fields)
            {
                var cr = field.cs + wsRow;
                var va = "";

                if (field.getValue != null)
                {
                    va = field.getValue(cfg, field, item);
                }
                else if (field.property != "")
                {
                    va = item.getProperty(field.property, "");
                }

                SetCell(sheet, cr, va, field.css);
            }
        }

        //設定資料格
        private void SetCell(Spire.Xls.Worksheet sheet, string cr, string value, TCss css)
        {
            var range = sheet.Range[cr];

            switch (css)
            {
                case TCss.None:
                    range.Text = value;
                    break;
                case TCss.Center:
                    range.Text = value;
                    range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                    break;
                case TCss.Text:
                    range.Text = "'" + value;
                    range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                    break;
                case TCss.Number:
                    range.NumberValue = GetIntVal(value);
                    range.NumberFormat = "0";
                    range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Right;
                    break;
                case TCss.Money:
                    range.NumberValue = GetIntVal(value);
                    range.NumberFormat = "$ #,##0";
                    break;
                case TCss.Day:
                    range.Text = GetDateTimeValue(value, "yyyy/MM/dd");
                    range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                    break;
            }
        }

        private void SetMoney(Spire.Xls.Worksheet sheet, string cr, double value)
        {
            var range = sheet.Range[cr];
            range.NumberValue = value;
            range.NumberFormat = "$ #,##0";
        }

        //設定標題列
        private void SetHead(Spire.Xls.Worksheet sheet, string cr, string value, bool need_color = true)
        {
            var range = sheet.Range[cr];
            range.Text = value;
            range.Style.Font.IsBold = true;

            if (need_color)
            {
                range.Style.Font.Color = System.Drawing.Color.White;
                range.Style.Color = System.Drawing.Color.FromArgb(41, 92, 144);//#295C90
            }

            range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
        }

        //設定格線
        private void SetRangeBorder(Spire.Xls.Worksheet sheet, string pos)
        {
            var range = sheet.Range[pos];
            range.BorderInside(Spire.Xls.LineStyleType.Thin, System.Drawing.Color.Black);
            range.BorderAround(Spire.Xls.LineStyleType.Thin, System.Drawing.Color.Black);
        }

        //設定格線
        private void SetRangeBorder(Spire.Xls.Worksheet sheet, List<TField> fields, int mnRow, int wsRow)
        {
            var tbl_ps = fields.First().cs + mnRow;
            var tbl_pe = fields.Last().cs + (wsRow - 1);
            SetRangeBorder(sheet, tbl_ps + ":" + tbl_pe);
        }

        private void MapCharSet(TConfig cfg, List<TField> fields)
        {
            foreach (var field in fields)
            {
                field.cs = cfg.CharSet[field.ci];
            }
        }

        private void MapCharSetAndIndex(TConfig cfg, List<TField> fields)
        {
            for (int i = 0; i < fields.Count; i++)
            {
                var field = fields[i];
                field.ci = i + 1;
                field.cs = cfg.CharSet[field.ci];
            }
        }

        private void SetHeadRow(Spire.Xls.Worksheet sheet, int wsRow, List<TField> fields, bool need_color = true)
        {
            foreach (var field in fields)
            {
                string cr = field.cs + wsRow;
                SetHead(sheet, cr, field.title, need_color);
            }
        }

        private void AutoFit(Spire.Xls.Worksheet sheet, List<TField> fields)
        {
            foreach (var field in fields)
            {
                sheet.Columns[field.ci - 1].AutoFitColumns();
            }
        }

        private void SetColumnWidth(Spire.Xls.Worksheet sheet, List<TField> fields)
        {
            foreach (var field in fields)
            {
                sheet.Columns[field.ci - 1].ColumnWidth = field.width;
            }
        }

        #endregion Xlsx

        private int GetIntVal(string value, int def = 0)
        {
            int result = def;
            Int32.TryParse(value, out result);
            return result;
        }

        //轉換日期
        private string GetDateTimeValue(string value, string format, bool add8Hours = true)
        {
            if (value == "") return "";
            DateTime dt = Convert.ToDateTime(value);
            if (add8Hours)
            {
                dt = dt.AddHours(8);
            }
            return dt.ToString(format);
        }
    }
}