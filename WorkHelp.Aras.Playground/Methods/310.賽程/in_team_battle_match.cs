﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;
using System.Net;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class in_team_battle_match : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]in_team_battle_match";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, this.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                InnH = _InnH,

                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                program_id = itmR.getProperty("program_id", ""),
                team_id = itmR.getProperty("team_id", ""),
                scene = itmR.getProperty("scene", ""),
            };

            cfg.meeting_id = "4420520723C44A70B5F312C38B238128";
            cfg.in_fight_day = "2021-11-13";

            string sql = "SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK)"
                + " WHERE in_meeting = '{#meeting_id}'"
                + " AND in_l1 = N'團體組'"
                + " AND in_fight_day = N'{#in_fight_day}'"
                + " ORDER BY in_sort_order";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_fight_day}", cfg.in_fight_day);

            Item itmPrograms = cfg.inn.applySQL(sql);

            int program_count = itmPrograms.getItemCount();

            for (int i = 0; i < program_count; i++)
            {
                Item itmProgram = itmPrograms.getItemByIndex(i);
                RunTeam(cfg, itmProgram);
            }
            return this;
        }

        private void RunTeam(TConfig cfg, Item itmProgram)
        {
            string sql = "";

            string program_id = itmProgram.getProperty("id", "");

            //主隊伍
            sql = "SELECT * FROM VU_MEETING_PTEAM WHERE source_id = '{#program_id}' AND ISNULL(in_type, '') IN ('', 'p') ORDER BY in_stuff_b1, in_team, in_sno";
            sql = sql.Replace("{#program_id}", program_id);
            Item itmParents = cfg.inn.applySQL(sql);
            int team_count = itmParents.getItemCount();

            //團體棒次量級
            sql = "SELECT * FROM IN_MEETING_PSECT WITH(NOLOCK) WHERE in_program = '{#program_id}' ORDER BY in_sub_id";
            sql = sql.Replace("{#program_id}", program_id);
            Item itmSects = cfg.inn.applySQL(sql);

            for (int i = 0; i < team_count; i++)
            {
                Item itmParent = itmParents.getItemByIndex(i);
                MergeTeamSect(cfg, itmProgram, itmSects, itmParents);
            }
        }

        private void MergeTeamSect(TConfig cfg, Item itmProgram, Item itmSects, Item itmParent)
        {
            string pg_name = itmProgram.getProperty("in_name2", "");
            string og_name = itmParent.getProperty("in_short_org", "");

            var players = MapPlayers(cfg, itmProgram, itmParent);
            int sect_count = itmSects.getItemCount();
            List<TSect> sects = new List<TSect>();

            for (int i = 0; i < sect_count; i++)
            {
                Item itmSect = itmSects.getItemByIndex(i);
                TSect sect = new TSect
                {
                    in_name = itmSect.getProperty("in_name", ""),
                    in_ranges = itmSect.getProperty("in_ranges", ""),
                    in_sub_id = itmSect.getProperty("in_sub_id", ""),
                    Value = itmSect,
                };

                sect.Players = players.FindAll(x => sect.in_ranges.Contains(x.weight));
                if (sect.Players == null)
                {
                    sect.Players = new List<TPlayer>();
                }

                sects.Add(sect);
            }


            foreach (var sect in sects)
            {
                var sorted_list = sect.Players.OrderBy(x => x.code).ToList();
                for(int i = 0; i < sorted_list.Count; i++)
                {
                    var player = sorted_list[i];
                    MergeSubTeam(cfg, itmParent, sect, player, i + 1);
                }
            }
        }

        private void MergeSubTeam(TConfig cfg, Item itmParent, TSect sect, TPlayer player, int serial)
        {
            string in_parent = itmParent.getProperty("id", "");
            string og_name = itmParent.getProperty("in_short_org", "");

            Item itmMUser = cfg.inn.applySQL("SELECT * FROM IN_MEETING_USER WITH(NOLOCK) WHERE id = '" + player.muid + "'");
            if (itmMUser.isError() || itmMUser.getResult() == "")
            {
                throw new Exception("muid 錯誤" + itmMUser.dom.OuterXml);
            }

            Item itmPTeam = cfg.inn.newItem("In_Meeting_Pteam");

            itmPTeam.setAttribute("where", "in_parent = '" + in_parent + "'"
                + " AND in_sub_id = '" + sect.in_sub_id + "'"
                + " AND in_muser = '" + player.muid + "'");

            itmPTeam.setProperty("in_type", "s");
            itmPTeam.setProperty("in_parent", in_parent);
            itmPTeam.setProperty("in_sub_sect", sect.in_name);
            itmPTeam.setProperty("in_sub_id", sect.in_sub_id);
            itmPTeam.setProperty("in_sub_serial", serial.ToString());
            itmPTeam.setProperty("in_muser", player.muid);


            itmPTeam.setProperty("in_meeting", cfg.meeting_id);
            itmPTeam.setProperty("source_id", itmParent.getProperty("source_id", ""));
            itmPTeam.setProperty("in_team_index", "");
            itmPTeam.setProperty("in_team_key", itmParent.getProperty("in_team_key", ""));

            itmPTeam.setProperty("in_index", itmParent.getProperty("in_index", ""));
            itmPTeam.setProperty("in_group", itmParent.getProperty("in_group", ""));
            itmPTeam.setProperty("in_current_org", itmParent.getProperty("in_current_org", ""));
            itmPTeam.setProperty("in_short_org", itmParent.getProperty("in_short_org", ""));
            itmPTeam.setProperty("in_stuff_b1", itmParent.getProperty("in_stuff_b1", ""));
            itmPTeam.setProperty("in_team", itmParent.getProperty("in_team", ""));
            itmPTeam.setProperty("in_team_players", "");
            itmPTeam.setProperty("in_creator_sno", itmParent.getProperty("in_creator_sno", ""));

            itmPTeam.setProperty("in_name", itmMUser.getProperty("in_name", ""));
            itmPTeam.setProperty("in_sno", itmMUser.getProperty("in_sno", ""));
            itmPTeam.setProperty("in_names", "");
            itmPTeam.setProperty("in_sub_weight", player.weight);

            Item itmChild = itmPTeam.apply("merge");

            if (itmChild.isError())
            {
                throw new Exception("更新發生錯誤");
            }
        }

        private void LogSect(TConfig cfg, Item itmProgram, Item itmSects, Item itmTeam)
        {
            string pg_name = itmProgram.getProperty("in_name2", "");
            string og_name = itmTeam.getProperty("in_short_org", "");

            var players = MapPlayers(cfg, itmProgram, itmTeam);
            int sect_count = itmSects.getItemCount();
            List<TSect> sects = new List<TSect>();

            for (int i = 0; i < sect_count; i++)
            {
                Item itmSect = itmSects.getItemByIndex(i);
                TSect sect = new TSect
                {
                    in_ranges = itmSect.getProperty("in_ranges", ""),
                    Value = itmSect,
                };

                sect.Players = players.FindAll(x => sect.in_ranges.Contains(x.weight));
                if (sect.Players == null)
                {
                    sect.Players = new List<TPlayer>();
                }

                sects.Add(sect);
            }

            StringBuilder builder = new StringBuilder();

            builder.AppendLine(" # " + pg_name + " #");
            builder.AppendLine(" ## " + og_name + " ##");

            foreach (var sect in sects)
            {
                builder.AppendLine(" - " + sect.Value.getProperty("in_name", ""));
                var sorted_list = sect.Players.OrderBy(x => x.code);
                foreach (var player in sorted_list)
                {
                    builder.AppendLine("     - " + player.name + "(" + player.weight + ")");
                }
                builder.AppendLine();
            }
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, builder.ToString());
        }

        private List<TPlayer> MapPlayers(TConfig cfg, Item itmProgram, Item itmParent)
        {
            string pg_name = itmProgram.getProperty("in_name2", "");
            string og_name = itmParent.getProperty("in_short_org", "");
            string in_parent = itmParent.getProperty("id", "");

            string sql = @"
                SELECT
	                DISTINCT in_name, in_sub_weight, in_muser
                FROM
	                VU_MEETING_PTEAM 
                WHERE 
	                in_parent = '{#in_parent}'
	                AND ISNULL(in_type, '') = 's'
	                AND ISNULL(in_name, '') <> ''
	                AND ISNULL(in_sub_weight, '') <> ''
            ";

            sql = sql.Replace("{#in_parent}", in_parent);

            Item itmPlayers = cfg.inn.applySQL(sql);

            List<TPlayer> list = new List<TPlayer>();

            int count = itmPlayers.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item itmPlayer = itmPlayers.getItemByIndex(i);
                TPlayer obj = new TPlayer
                {
                    name = itmPlayer.getProperty("in_name", ""),
                    weight = itmPlayer.getProperty("in_sub_weight", ""),
                    muid = itmPlayer.getProperty("in_muser", ""),
                    Value = itmPlayer,
                };

                string w = obj.weight.Replace("-", "").Replace("+", "");
                obj.code = GetIntVal(w);
                if (obj.weight.Contains("+")) obj.code += 100;

                var find = list.Find(x => x.name == obj.name);
                if (find != null)
                {
                    string msg = pg_name + ": " + og_name + ": " + obj.name + ": 設多量級 _# " + obj.weight;
                    //throw new Exception(msg);
                    cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, msg);
                }

                list.Add(obj);
            }
            return list;
        }

        private int GetIntVal(string value, int def = 0)
        {
            int result = def;
            Int32.TryParse(value, out result);
            return result;
        }

        private class TSect
        {
            public string in_name { get; set; }
            public string in_ranges { get; set; }
            public string in_sub_id { get; set; }
            public Item Value { get; set; }
            public List<TPlayer> Players { get; set; }
        }

        private class TPlayer
        {
            public string name { get; set; }
            public string sno { get; set; }
            public string weight { get; set; }
            public int code { get; set; }
            public string muid { get; set; }
            public Item Value { get; set; }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public Innosoft.InnovatorHelper InnH { get; set; }

            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }

            public string meeting_id { get; set; }
            public string in_fight_day { get; set; }

            public string program_id { get; set; }
            public string team_id { get; set; }
            public string scene { get; set; }

            public Item itmMeeting { get; set; }
            public Item itmProgram { get; set; }
        }
    }
}