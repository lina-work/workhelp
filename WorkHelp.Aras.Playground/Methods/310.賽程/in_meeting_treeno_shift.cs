﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class in_meeting_treeno_shift : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
            目的: 調整賽程順序
            日期: 
                2021-04-2-: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]in_meeting_treeno_shift";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, this.dom.InnerXml);

            string meeting_id = itmR.getProperty("meeting_id", "");
            string scene = itmR.getProperty("scene", "");

            if (meeting_id == "")
            {
                itmR.setProperty("error_message", "賽事 id 不得為空白");
                return itmR;
            }

            if (scene == "page")
            {
                Page(CCO, strMethodName, inn, itmR);
            }
            else if (scene == "check")
            {
                CheckTreeNo(CCO, strMethodName, inn, itmR);
            }
            else if (scene == "win_time")
            {
                ChangeWinTime(CCO, strMethodName, inn, itmR);
            }

            return itmR;
        }

        private void ChangeWinTime(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            string sql = "";
            string now = DateTime.Now.AddHours(-8).ToString("yyyy-MM-ddTHH:mm:ss");

            string meeting_id = itmReturn.getProperty("meeting_id", "");
            string event_id = itmReturn.getProperty("event_id", "");
            string exe_mode = itmReturn.getProperty("exe_mode", "");

            if (exe_mode == "clear")
            {
                sql = "UPDATE IN_MEETING_PEVENT SET in_win_time = NULL WHERE id = '" + event_id + "'";
            }
            else
            {
                sql = "UPDATE IN_MEETING_PEVENT SET in_win_time = '" + now + "' WHERE id = '" + event_id + "'";
            }

            CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item itmSQL = inn.applySQL(sql);

            if (itmSQL.isError())
            {
                throw new Exception("執行失敗");
            }
        }

        private void CheckTreeNo(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            string meeting_id = itmReturn.getProperty("meeting_id", "");
            string in_date = itmReturn.getProperty("in_date", "");
            string site_id = itmReturn.getProperty("site_id", "");

            string site_condition = site_id != ""
                ? "AND t1.in_site = '" + site_id + "'"
                : "";

            string sql = @"
                SELECT 
                	t2.in_code, t2.in_name, t1.in_site, t1.in_tree_no 
                FROM 
                	IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
                	IN_MEETING_SITE t2 WITH(NOLOCK) ON t2.id = t1.in_site
                WHERE 
                	t1.in_meeting = '{#meeting_id}' 
                	AND t1.in_date_key = '{#in_date}'
                	{#site_condition}
                	AND ISNULL(t1.in_tree_no, 0) > 0
                GROUP BY 
                	t2.in_code
                	, t2.in_name
                	, t1.in_site
                	, t1.in_tree_no
                HAVING 
                    COUNT(*) > 1
                ORDER BY 
                	t2.in_code
                	, t2.in_name
                	, t1.in_site
                	, t1.in_tree_no
            ";

            sql = sql.Replace("{#meeting_id}", meeting_id)
                .Replace("{#in_date}", in_date)
                .Replace("{#site_condition}", site_condition);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Dictionary<string, List<string>> map = new Dictionary<string, List<string>>();
            List<string> err = new List<string>();
            Item items = inn.applySQL(sql);
            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string site_name = item.getProperty("in_name", "");
                string in_tree_no = item.getProperty("in_tree_no", "");
                if (map.ContainsKey(site_name))
                {
                    map[site_name].Add(in_tree_no);
                }
                else
                {
                    List<string> list = new List<string>();
                    list.Add(in_tree_no);
                    map.Add(site_name, list);
                }
            }

            foreach (var kv in map)
            {
                err.Add(kv.Key + ": " + string.Join("、", kv.Value));
            }

            if (err.Count > 0)
            {
                throw new Exception("場次編號重複：<br>" + string.Join("<br>", err));
            }
        }

        private void Page(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            string meeting_id = itmReturn.getProperty("meeting_id", "");
            string site_id = itmReturn.getProperty("site_id", "");
            string in_date = itmReturn.getProperty("in_date", "");

            Item itmDays = GetDays(CCO, strMethodName, inn, meeting_id);

            if (itmDays.isError() || itmDays.getResult() == "")
            {
                itmReturn.setProperty("error_message", "查無比賽日期");
                return;
            }

            if (in_date == "")
            {
                itmReturn.setProperty("in_date", itmDays.getItemByIndex(0).getProperty("in_date_key", DateTime.Now.ToString("yyyy-MM-dd")));
            }

            //賽事資訊
            AppendMeeting(CCO, strMethodName, inn, itmReturn);
            //附加日期選單
            AppendDayMenu(CCO, strMethodName, inn, itmDays, itmReturn);
            //附加場地選單
            AppendSiteMenu(CCO, strMethodName, inn, itmReturn);

            if (site_id != "" && in_date != "")
            {
                Table(CCO, strMethodName, inn, itmReturn);
            }
        }

        private Item GetDays(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string meeting_id)
        {
            string sql = "SELECT DISTINCT in_date_key FROM IN_MEETING_ALLOCATION WITH(NOLOCK) WHERE in_meeting = '" + meeting_id + "' ORDER BY in_date_key";
            Item itmDays = inn.applySQL(sql);
            return itmDays;
        }

        //附加日期選單
        private void AppendDayMenu(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmDays, Item itmReturn)
        {
            int count = itmDays.getItemCount();

            for (int i = 0; i < count; i++)
            {
                var itmDay = itmDays.getItemByIndex(i);
                string in_date_key = itmDay.getProperty("in_date_key", "");

                itmDay.setType("inn_date");
                itmDay.setProperty("value", in_date_key);
                itmDay.setProperty("label", in_date_key);
                itmReturn.addRelationship(itmDay);
            }
        }

        private void AppendSiteMenu(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            string meeting_id = itmReturn.getProperty("meeting_id", "");

            //附加場地選單
            Item itmEmptySite = inn.newItem();
            itmEmptySite.setType("inn_site");
            itmEmptySite.setProperty("value", "");
            itmEmptySite.setProperty("text", "請選擇");
            itmReturn.addRelationship(itmEmptySite);

            Item itmSites = inn.applySQL("SELECT * FROM IN_MEETING_SITE WITH(NOLOCK) WHERE in_meeting = '" + meeting_id + "' ORDER BY in_code");

            int site_count = itmSites.getItemCount();

            for (int i = 0; i < site_count; i++)
            {
                Item itmSite = itmSites.getItemByIndex(i);
                itmSite.setType("inn_site");
                itmSite.setProperty("value", itmSite.getProperty("id", ""));
                itmSite.setProperty("text", itmSite.getProperty("in_name", ""));
                itmReturn.addRelationship(itmSite);
            }
        }

        private void AppendMeeting(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            string meeting_id = itmReturn.getProperty("meeting_id", "");
            string sql = "SELECT in_title FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + meeting_id + "'";
            Item itmMeeting = inn.applySQL(sql);

            itmReturn.setProperty("in_title", itmMeeting.getProperty("in_title", ""));
        }


        private string GetTableAttribute(string table_name)
        {
            //table table-hover table-bordered table-rwd rwd rwdtable

            return "<table id='" + table_name + "' class='table-bordered rwdtable' "
                + " data-toggle='table' "
                + " data-search-align='left' "
                + " data-search='true' "
                + " data-pagination='false' "
                + " data-click-to-select='true' "
                + " data-show-toggle='false' "
                + " data-card-view='false'"
                + ">";
        }


        //調整場次順序
        private void Table(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            string meeting_id = itmReturn.getProperty("meeting_id", "");

            Item items = GetSiteEvents(CCO, strMethodName, inn, itmReturn);

            List<TEvent> list = MapEvents(items);

            StringBuilder contents = GetTreeNoTableContents(inn, list);

            itmReturn.setProperty("inn_table", contents.ToString());
        }

        private StringBuilder GetTreeNoTableContents(Innovator inn, List<TEvent> list)
        {
            List<TField> fields = new List<TField>();
            fields.Add(new TField { property = "in_date_key", title = "比賽日期", css = "text-center", hdcss = "text-center" });
            fields.Add(new TField { property = "site_name", title = "場地", css = "text-center", hdcss = "text-center", getValue = SiteName });
            fields.Add(new TField { property = "tree_no", title = "場編", css = "text-center", hdcss = "text-center", getValue = TreeNoInput });
            fields.Add(new TField { property = "program_display", title = "量級", css = "text-left", hdcss = "text-center", getValue = ProgramLink });
            fields.Add(new TField { property = "in_round_code", title = "輪次", css = "text-left", hdcss = "text-center", getValue = EventDesc });

            fields.Add(new TField { property = "foot1_sign_no", title = "白方<br>籤號", css = "white_group ctrl_search text-center", hdcss = "text-center white_group" });
            fields.Add(new TField { property = "foot1_org", title = "白方<br>代表單位", css = "white_group ctrl_search text-right", hdcss = "text-center white_group" });
            fields.Add(new TField { property = "foot1_name", title = "白方<br>姓名", css = "white_group ctrl_search text-right", hdcss = "text-center white_group" });
            fields.Add(new TField { property = "", title = "vs", css = "text-center", hdcss = "text-center", getValue = EventLink });
            fields.Add(new TField { property = "foot2_name", title = "藍方<br>姓名", css = "blue_group ctrl_search", hdcss = "text-center blue_group" });
            fields.Add(new TField { property = "foot2_org", title = "藍方<br>代表單位", css = "blue_group ctrl_search", hdcss = "text-center blue_group" });
            fields.Add(new TField { property = "foot2_sign_no", title = "藍方<br>籤號", css = "blue_group ctrl_search text-center", hdcss = "text-center blue_group" });
            //fields.Add(new TField { property = "event_alias", title = "備註", css = "text-center", hdcss = "text-center", getValue = EventLink1 });
            // fields.Add(new TField { property = "inn_btns", title = "功能", css = "text-center", hdcss = "text-center", getValue = Buttons });

            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            head.AppendLine("<thead>");
            foreach (var field in fields)
            {
                head.AppendLine("<th class='" + field.hdcss + "' data-field='" + field.property + "' data-sortable='false'>" + field.title + "</th>");
            }
            head.AppendLine("</thead>");

            int count = list.Count;

            body.AppendLine("<tbody>");
            for (int i = 0; i < count; i++)
            {
                var evt = list[i];
                AppendRow(inn, evt, fields, body);
                if (evt.Children.Count > 0)
                {
                    foreach (var child in evt.Children)
                    {
                        AppendRow(inn, child, fields, body);
                    }
                }
            }
            body.AppendLine("</tbody>");


            string table_name = "tb_In_Score";

            StringBuilder builder = new StringBuilder();
            builder.AppendLine(GetTableAttribute(table_name));
            builder.Append(head);
            builder.Append(body);
            builder.AppendLine("</table>");
            builder.AppendLine("<script>");
            builder.AppendLine("$('#" + table_name + "').bootstrapTable();");
            builder.AppendLine("if ($(window).width() <= 768) { $('#" + table_name + "').bootstrapTable('toggleView'); }");
            builder.AppendLine("</script>");

            return builder;

        }

        private void AppendRow(Innovator inn, TEvent evt, List<TField> fields, StringBuilder body)
        {
            Item item = inn.newItem();

            item.setProperty("in_date_key", evt.Foot1.getProperty("in_date_key", ""));
            item.setProperty("foot1_sign_no", evt.Foot1.getProperty("in_sign_sect_no", ""));
            item.setProperty("foot1_org", evt.Foot1.getProperty("in_sign_org", "")); ;
            item.setProperty("foot1_name", evt.Foot1.getProperty("in_sign_name", ""));

            item.setProperty("foot2_sign_no", evt.Foot2.getProperty("in_sign_sect_no", ""));
            item.setProperty("foot2_org", evt.Foot2.getProperty("in_sign_org", "")); ;
            item.setProperty("foot2_name", evt.Foot2.getProperty("in_sign_name", ""));

            body.AppendLine("<tr>");
            foreach (var field in fields)
            {
                string value = "";
                if (field.getValue != null)
                {
                    value = field.getValue(evt, field);
                }
                else if (field.property != "")
                {
                    value = item.getProperty(field.property, "");
                }

                body.AppendLine("<td class='" + field.css + "'>" + value + "</td>");
            }
            body.AppendLine("</tr>");
        }

        private string SiteName(TEvent evt, TField field)
        {
            string site_code = evt.Value.getProperty("site_code", "");

            switch (site_code)
            {
                case "1": return "一";
                case "2": return "二";
                case "3": return "三";
                case "4": return "四";
                case "5": return "五";
                case "6": return "六";
                case "7": return "七";
                case "8": return "八";
                case "9": return "九";
                case "10": return "十";

                case "11": return "十一";
                case "12": return "十二";
                case "13": return "十三";
                case "14": return "十四";
                case "15": return "十五";
                case "16": return "十六";
                case "17": return "十七";
                case "18": return "十八";
                case "19": return "十九";
                case "20": return "二十";

                case "21": return "二十一";
                case "22": return "二十二";
                case "23": return "二十三";
                case "24": return "二十四";
                case "25": return "二十五";
                case "26": return "二十六";
                case "27": return "二十七";
                case "28": return "二十八";
                case "29": return "二十九";
                case "30": return "三十";

                default: return "";
            }
        }

        private string TreeNoInput(TEvent evt, TField field)
        {
            Item item = evt.Value;

            string program_id = item.getProperty("program_id", "");
            string tree_id = item.getProperty("tree_id", "");
            string tree_no = item.getProperty("tree_no", "");
            string in_site = item.getProperty("in_site", "");
            string in_date_key = item.getProperty("in_date_key", "");

            return "<input type='text' value='" + tree_no + "'"
            + " class='event-input'"
            + " style='text-align: right; width:60px'"
            + " data-pid='" + program_id + "'"
            + " data-sid='" + in_site + "'"
            + " data-did='" + in_date_key + "'"
            + " data-rid='" + tree_id + "'"
            + " onkeyup='TreeNo_KeyUp(this)' />";
        }

        private string ProgramLink(TEvent evt, TField field)
        {
            Item item = evt.Value;

            string meeting_id = item.getProperty("meeting_id", "");
            string program_id = item.getProperty("program_id", "");
            string program_display = item.getProperty("program_display", "");

            if (evt.is_robin)
            {
                program_display += "(循環戰)";
            }

            string href = "c.aspx?page=In_Competition_Preview.html"
                + "&method=in_meeting_program_preview"
                + "&meeting_id=" + meeting_id
                + "&program_id=" + program_id
                + "&mode=draw"
                + "&eno=edit";

            return "<a href='" + href + "' target='_blank' >" + program_display + "</a>";
        }

        private string EventDesc(TEvent evt, TField field)
        {
            string result = "";

            if (evt.pg_team_count == "2")
            {
                result = "三戰兩勝";
            }
            else if (evt.is_robin)
            {
                result = evt.pg_team_count + "人戰#" + evt.tree_sno;
            }
            else
            {
                switch (evt.tree_name)
                {
                    case "main":
                        if (evt.round_code == 2)
                        {
                            result = "勝部-決賽";
                        }
                        else if (evt.round_code == 4)
                        {
                            result = "勝部-準決賽";
                        }
                        else
                        {
                            result = "勝部-" + evt.round_code + "強";
                        }
                        break;

                    case "repechage":
                        if (evt.round_code == 2)
                        {
                            result = "敗部-三四名";
                        }
                        else if (evt.round_code == 4)
                        {
                            if (evt.round_id == 1)
                            {
                                result = "敗部-銅牌戰-W";
                            }
                            else
                            {
                                result = "敗部-銅牌戰-E";
                            }
                        }
                        else
                        {
                            result = "敗部-R" + evt.round;
                        }
                        break;

                    case "challenge-a":
                        if (evt.round_code == 4)
                        {
                            result = "挑戰賽 3 vs 2";
                        }
                        if (evt.round_code == 2)
                        {
                            result = "挑戰賽 2 vs 1";
                        }
                        break;

                    case "challenge-b":
                        result = "挑戰成功加賽";
                        break;

                    case "rank34":
                        result = "三四名";
                        break;

                    case "rank56":
                        result = "五六名";
                        break;

                    case "rank78":
                        result = "七八名";
                        break;

                    case "sub":
                        break;

                    default:
                        break;
                }
            }

            return result;
        }

        private string EventLink(TEvent evt, TField field)
        {
            string foot1_status = StatusDisplay(evt.Foot1);
            string foot2_status = StatusDisplay(evt.Foot2);

            string result = foot1_status + " vs " + foot2_status;

            string in_win_local_time = evt.Value.getProperty("in_win_local_time", "");
            if (in_win_local_time != "")
            {
                result += "<br>" + in_win_local_time;
            }

            return result;
        }

        //取得勝敗呈現
        private string StatusDisplay(Item item)
        {
            string in_status = item.getProperty("in_status", "");
            string in_points = item.getProperty("in_points", "0");
            string correct = item.getProperty("in_correct_count", "0");

            switch (in_status)
            {
                case "1": return "<span class='team_score_b'>" + in_points + "</span>";
                case "0":
                    if (correct == "3")
                    {
                        return "<span class='team_score_c'>S3</span>";
                    }
                    else
                    {
                        return "<span class='team_score_c'>" + in_points + "</span>";
                    }
                default: return "<span class='team_score_a'>&nbsp;</span>";
            }
        }

        private List<TEvent> MapEvents(Item items)
        {
            List<TEvent> evts = new List<TEvent>();

            int count = items.getItemCount();

            for (int i = 0; i < count; i = i + 2)
            {
                Item f1 = items.getItemByIndex(i);
                Item f2 = items.getItemByIndex(i + 1);

                string id = f1.getProperty("event_id", "");
                string in_type = f1.getProperty("in_type", "");
                string in_parent = f1.getProperty("in_parent", "");

                var evt = new TEvent
                {
                    Id = id,
                    Value = f1,
                    Foot1 = f1,
                    Foot2 = f2,

                    pg_team_count = f1.getProperty("program_team_count", ""),
                    pg_battle_type = f1.getProperty("program_battle", ""),
                    tree_name = f1.getProperty("tree_name", ""),
                    tree_sno = f1.getProperty("tree_sno", ""),
                    round = GetIntVal(f1.getProperty("in_round", "0")),
                    round_id = GetIntVal(f1.getProperty("in_round_id", "0")),
                    round_code = GetIntVal(f1.getProperty("in_round_code", "0")),

                    Children = new List<TEvent>(),
                };
                evt.is_robin = evt.pg_battle_type.Contains("Robin");

                if (in_type == "s")
                {
                    //子場次
                    var parent = evts.Find(x => x.Id == in_parent);
                    evt.Children.Add(evt);
                }
                else
                {
                    evts.Add(evt);
                }
            }
            return evts;
        }

        private Item GetSiteEvents(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            string sql = "";

            string meeting_id = itmReturn.getProperty("meeting_id", "");
            string program_id = itmReturn.getProperty("program_id", "");
            string site_id = itmReturn.getProperty("site_id", "");

            string in_date_key = itmReturn.getProperty("in_date", "").Replace("/", "-");

            string program_filter = program_id != ""
                ? "AND t1.id = '" + program_id + "'"
                : "";

            string site_filter = site_id != ""
                ? "AND t2.in_site = '" + site_id + "'"
                : "";

            sql = @"
                SELECT 
                    t1.in_meeting       AS 'meeting_id'
                    , t1.id             AS 'program_id'
                    , t1.in_name2       AS 'program_display'
                    , t1.in_short_name  AS 'program_short_name'
                    , t1.in_battle_type AS 'program_battle'
                    , t1.in_team_count  AS 'program_team_count'
                    , t2.id             AS 'event_id'
                    , t2.in_tree_name   AS 'tree_name'
                    , t2.in_tree_id     AS 'tree_id'
                    , t2.in_tree_no     AS 'tree_no'
                    , t2.in_tree_sno    AS 'tree_sno'
                    , t2.in_tree_alias
                    , t2.in_tree_rank
                    , t2.in_site
                    , t2.in_date_key
                    , t2.in_round
                    , t2.in_round_code
                    , t2.in_round_id
                    , t2.in_win_status
                    , t2.in_win_time
                    , t2.in_win_creator
                    , t2.in_win_local
                    , t2.in_win_local_time
                    , t2.in_type
                    , t2.in_parent
                    , t3.in_sign_foot
                    , t3.in_sign_no
                    , t3.in_status
                    , t3.in_points
                    , t3.in_correct_count
                    , t11.id AS 'player_id'
                    , ISNULL(t11.map_short_org, t3.in_player_org)   AS 'in_sign_org'
                    , ISNULL(t11.in_team, t3.in_player_team)        AS 'in_sign_team'
                    , ISNULL(t11.in_name, t3.in_player_name)        AS 'in_sign_name'
                    , ISNULL(t11.in_sno, t3.in_player_sno)          AS 'in_sign_sno'
                    , t11.in_judo_no                                AS 'in_sign_sect_no'
			        , t13.in_code                                   AS 'site_code'
                FROM 
                    IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
                    IN_MEETING_PEVENT t2 WITH(NOLOCK)
                    ON t2.source_id = t1.id
                INNER JOIN
                    IN_MEETING_PEVENT_DETAIL t3 WITH(NOLOCK)
                    ON t3.source_id = t2.id
                LEFT OUTER JOIN
                    VU_MEETING_PTEAM t11 WITH(NOLOCK)
                    ON t11.source_id = t2.source_id
                    AND t11.in_sign_no = t3.in_sign_no
					AND ISNULL(t11.in_sign_no, '') <> ''
		        LEFT OUTER JOIN
			        IN_MEETING_SITE t13 WITH(NOLOCK)
			        ON t13.id = t2.in_site
                WHERE
                    t1.in_meeting = '{#meeting_id}'
                    AND t2.in_date_key = '{#in_date_key}'
                    AND ISNULL(t2.in_tree_no, 0) > 0
                    AND ISNULL(t2.in_win_status, '') NOT IN ('bypass', 'cancel', 'nofight')
                    {#program_filter}
                    {#site_filter}
                ORDER BY
                    t13.in_code
                    , t2.in_tree_no
                    , t1.in_sort_order
                    , t3.in_sign_foot
             ";

            sql = sql.Replace("{#meeting_id}", meeting_id)
                .Replace("{#in_date_key}", in_date_key)
                .Replace("{#program_filter}", program_filter)
                .Replace("{#site_filter}", site_filter);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return inn.applySQL(sql);
        }

        private class TEvent
        {
            public string Id { get; set; }
            public Item Value { get; set; }
            public Item Foot1 { get; set; }
            public Item Foot2 { get; set; }
            public List<TEvent> Children { get; set; }

            public string pg_team_count { get; set; }
            public string pg_battle_type { get; set; }

            public string tree_name { get; set; }
            public string tree_sno { get; set; }
            public int round { get; set; }
            public int round_id { get; set; }
            public int round_code { get; set; }

            public bool is_robin { get; set; }
        }

        private class TField
        {
            public string title { get; set; }
            public string property { get; set; }
            public string format { get; set; }
            public string hdcss { get; set; }
            public string css { get; set; }
            public string defv { get; set; }
            public Func<TEvent, TField, string> getValue { get; set; }
        }

        private int GetIntVal(string value, int def = 0)
        {
            int result = def;
            Int32.TryParse(value, out result);
            return result;
        }

        private string GetDateTimeVal(string value, string format)
        {
            DateTime result = DateTime.MinValue;
            DateTime.TryParse(value, out result);
            if (result != DateTime.MinValue)
            {
                return result.ToString(format);
            }
            else
            {
                return "";
            }
        }

        //轉換日期
        private string GetDateTimeValue(string value, string format = "yyyy-MM-dd", int hours = 0)
        {
            if (value == null || value == "") return "";

            string day = value.Replace("/", "-");

            DateTime dt = DateTime.Now;
            if (DateTime.TryParse(day, out dt))
            {
                return dt.AddHours(hours).ToString(format);
            }
            else
            {
                return value;
            }
        }
    }
}