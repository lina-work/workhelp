﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class in_meeting_draw_update : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
            目的: 更新籤號相關數值
            輸入: 
                - meeting_id
                - in_team_index
                - exe_type
                    - in_seeds 種子籤
                    - number 跆拳道籤號
                    - section 量級序號
                    - in_seeds 種子籤
            日期: 
                2020-07-28: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]in_meeting_draw_update";

            Item itmR = this;
            string program_id = itmR.getProperty("program_id", "").Trim();
            string team_id = itmR.getProperty("team_id", "").Trim();
            string exe_type = itmR.getProperty("exe_type", "").Trim();

            if (program_id == "" || team_id == "")
            {
                throw new Exception("參數錯誤");
            }

            switch (exe_type)
            {
                case "seed":
                    //更新種子籤
                    UpdateSeed(CCO, strMethodName, inn, itmR);
                    break;

                case "number":
                    //更新籤號
                    UpdateTkdNo(CCO, strMethodName, inn, itmR);
                    break;

                case "section":
                    //更新量級
                    UpdateSectionNo(CCO, strMethodName, inn, itmR);
                    break;

                case "judo_no":
                    //用[柔道籤號]更新[跆拳道籤號]，回傳新跆拳道籤號
                    UpdateJudoNo(CCO, strMethodName, inn, itmR);
                    break;

                default:
                    break;
            }

            return itmR;
        }

        #region 跆拳道籤號

        //用[跆拳道籤號]更新[柔道籤號]，回傳新柔道籤號
        private void UpdateTkdNo(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            string sql = "";

            string program_id = itmReturn.getProperty("program_id", "").Trim();
            string team_id = itmReturn.getProperty("team_id", "").Trim();
            string in_sign_no = itmReturn.getProperty("in_sign_no", "").Trim();

            sql = "SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + program_id + "'";
            Item itmProgram = inn.applySQL(sql);
            string in_battle_type = itmProgram.getProperty("in_battle_type", "");
            string in_team_count = itmProgram.getProperty("in_team_count", "");
            string in_round_code = itmProgram.getProperty("in_round_code", "");

            Item itmData = inn.newItem();
            itmData.setType("In_Meeting_PTeam");
            itmData.setProperty("in_battle_type", in_battle_type);
            itmData.setProperty("in_team_count", in_team_count);
            itmData.setProperty("in_round_code", in_round_code);
            itmData.setProperty("is_tkd_to_judo", "1");
            itmData.setProperty("value", in_sign_no);
            Item itmResult = itmData.apply("In_Judo_SignNo");

            string in_judo_no = itmResult.getProperty("map_no", "");

            sql = @"
                UPDATE 
                    IN_MEETING_PTEAM 
                SET 
                    in_judo_no = '{#in_judo_no}'
                    , in_sign_no = '{#in_sign_no}'
                    , in_section_no = '{#in_judo_no}'
                WHERE
                    source_id = '{#program_id}'
                    AND id = '{#team_id}'
            ";

            sql = sql.Replace("{#program_id}", program_id)
                .Replace("{#team_id}", team_id)
                .Replace("{#in_judo_no}", in_judo_no)
                .Replace("{#in_sign_no}", in_sign_no);

            Item itmSQL = inn.applySQL(sql);

            if (itmSQL.isError())
            {
                CCO.Utilities.WriteDebug(strMethodName, "TKD籤號更新失敗: " + sql);
                throw new Exception("TKD籤號更新失敗");
            }

            itmReturn.setProperty("new_judo_no", in_judo_no);
        }

        #endregion 跆拳道籤號

        #region 柔道籤號

        //用[柔道籤號]更新[跆拳道籤號]，回傳新跆拳道籤號
        private void UpdateJudoNo(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            string sql = "";

            string program_id = itmReturn.getProperty("program_id", "").Trim();
            string team_id = itmReturn.getProperty("team_id", "").Trim();
            string in_judo_no = itmReturn.getProperty("in_judo_no", "").Trim();

            sql = "SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + program_id + "'";
            Item itmProgram = inn.applySQL(sql);
            string in_battle_type = itmProgram.getProperty("in_battle_type", "");
            string in_team_count = itmProgram.getProperty("in_team_count", "");
            string in_round_code = itmProgram.getProperty("in_round_code", "");

            Item itmData = inn.newItem();
            itmData.setType("In_Meeting_PTeam");
            itmData.setProperty("in_battle_type", in_battle_type);
            itmData.setProperty("in_team_count", in_team_count);
            itmData.setProperty("in_round_code", in_round_code);
            itmData.setProperty("is_tkd_to_judo", "0");
            itmData.setProperty("value", in_judo_no);
            Item itmResult = itmData.apply("In_Judo_SignNo");

            string in_sign_no = itmResult.getProperty("map_no", "");

            sql = @"
                UPDATE 
                    IN_MEETING_PTEAM 
                SET 
                    in_judo_no = '{#in_judo_no}'
                    , in_sign_no = '{#in_sign_no}'
                    , in_section_no = '{#in_judo_no}'
                WHERE
                    source_id = '{#program_id}'
                    AND id = '{#team_id}'
            ";

            sql = sql.Replace("{#program_id}", program_id)
                .Replace("{#team_id}", team_id)
                .Replace("{#in_judo_no}", in_judo_no)
                .Replace("{#in_sign_no}", in_sign_no);

            Item itmSQL = inn.applySQL(sql);

            if (itmSQL.isError())
            {
                CCO.Utilities.WriteDebug(strMethodName, "柔道籤號更新失敗: " + sql);
                throw new Exception("柔道籤號更新失敗");
            }

            itmReturn.setProperty("new_sign_no", in_sign_no);
        }

        #endregion 柔道籤號

        //更新種子籤
        private void UpdateSeed(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            string program_id = itmReturn.getProperty("program_id", "").Trim();
            string team_id = itmReturn.getProperty("team_id", "").Trim();
            string in_seeds = itmReturn.getProperty("in_seeds", "").Trim();

            string in_points = ""; // 積分，由種子籤倒扣

            int seed_lottery = 0;
            int base_lottery = 10000;
            if (in_seeds == "")
            {
                // 無值
            }
            else if (Int32.TryParse(in_seeds, out seed_lottery))
            {
                in_points = (base_lottery - seed_lottery).ToString();
            }
            else
            {
                throw new Exception("種子籤必須為整數");
            }

            string sql = @"
                UPDATE 
                    IN_MEETING_PTEAM 
                SET 
                    in_seeds = '{#in_seeds}'
                    , in_points = '{#in_points}'
                WHERE
                    source_id = '{#program_id}'
                    AND id = '{#team_id}'
            ";

            sql = sql.Replace("{#program_id}", program_id)
                .Replace("{#team_id}", team_id)
                .Replace("{#in_seeds}", in_seeds)
                .Replace("{#in_points}", in_points);

            Item itmSQL = inn.applySQL(sql);

            if (itmSQL.isError())
            {
                CCO.Utilities.WriteDebug(strMethodName, "種子籤更新失敗: " + sql);
                throw new Exception("種子籤更新失敗");
            }
        }

        //更新量級序號
        private void UpdateSectionNo(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            string program_id = itmReturn.getProperty("program_id", "").Trim();
            string team_id = itmReturn.getProperty("team_id", "").Trim();
            string in_section_no = itmReturn.getProperty("in_section_no", "").Trim();

            string sql = @"
                UPDATE 
                    IN_MEETING_PTEAM 
                SET 
                    in_section_no = '{#in_section_no}'
                WHERE
                    source_id = '{#program_id}'
                    AND id = '{#team_id}'
            ";

            sql = sql.Replace("{#program_id}", program_id)
                .Replace("{#team_id}", team_id)
                .Replace("{#in_section_no}", in_section_no);

            Item itmSQL = inn.applySQL(sql);

            if (itmSQL.isError())
            {
                CCO.Utilities.WriteDebug(strMethodName, "量級序號更新失敗: " + sql);
                throw new Exception("量級序號更新失敗");
            }

            Item itmProgram = inn.applySQL("SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + program_id + "'");

            if (!itmProgram.isError() && itmProgram.getItemCount() == 1)
            {
                FixMUserSignNo(CCO, strMethodName, inn, itmProgram);
            }
        }

        private void FixMUserSignNo(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmProgram)
        {
            string sql = "";
            Item itmSQL = null;

            string meeting_id = itmProgram.getProperty("in_meeting", "");
            string program_id = itmProgram.getProperty("id", "");

            sql = @"
                UPDATE t1 SET
	                t1.in_sign_no = t2.in_sign_no
	                , t1.in_section_no = t2.in_section_no
                FROM
	                IN_MEETING_USER t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PTEAM t2 WITH(NOLOCK)
	                ON t2.in_meeting = t1.source_id
	                AND t2.in_team_key = ISNULL(t1.in_l1, '') 
		                + '-' + ISNULL(t1.in_l2, '') 
		                + '-' + ISNULL(t1.in_l3, '') 
		                + '-' + ISNULL(t1.in_index, '')
		                + '-' + ISNULL(t1.in_creator_sno, '')
                WHERE
	                t1.source_id = '{#meeting_id}'
	                AND t2.source_id = '{#program_id}'
	                AND ISNULL(t2.in_sign_no, '') <> ''
            ";

            sql = sql.Replace("{#meeting_id}", meeting_id)
                .Replace("{#program_id}", program_id);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            itmSQL = inn.applySQL(sql);

            if (itmSQL.isError())
            {
                CCO.Utilities.WriteDebug(strMethodName, "更新與會者籤號與量級序號發生錯誤 _# sql: " + sql);
            }
        }
    }
}