﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class in_meeting_program_bulletin_print : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
            目的: 
                名次列印
            輸出: 
                docx
            重點: 
                - 需 Xceed.Document.NET.dll
                - 需 Xceed.Words.NET.dll
                - 請於 method-config.xml 引用 DLL
            日期: 
                2020-11-18: 創建 (lina)
            */

            System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]in_meeting_program_bulletin_print";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, this.dom.InnerXml);

            string meeting_id = itmR.getProperty("meeting_id", "");
            string mode = itmR.getProperty("mode", "");

            if (meeting_id == "")
            {
                itmR.setProperty("error_message", "賽事 id 不得為空白");
                return itmR;
            }

            //取得賽事資料
            Item itmMeeting = GetMeeting(CCO, strMethodName, inn, itmR);
            itmR.setProperty("in_title", itmMeeting.getProperty("in_title", ""));

            //取得名次資料
            Item itmRanks = GetRanks(CCO, strMethodName, inn, itmMeeting);

            //轉換名次資料
            Dictionary<string, TTable> map = MapRankList(CCO, strMethodName, inn, itmMeeting, itmRanks);
            ClearRanks(map);

            if (map.Count > 0)
            {
                map.Last().Value.IsEndTable = true;

                if (mode == "")
                {
                    AppendTables(CCO, strMethodName, inn, map, itmR);
                }
                else if (mode == "word")
                {
                    ExportWord(CCO, strMethodName, inn, itmMeeting, map, itmR);
                }
                else if (mode == "pdf")
                {
                    ExportPDF(CCO, strMethodName, inn, itmMeeting, map, itmR);
                }
            }

            return itmR;
        }

        private void ClearRanks(Dictionary<string, TTable> map)
        {
            foreach (var kv in map)
            {
                var table = kv.Value;
                foreach (var kv2 in table.Rows)
                {
                    var row = kv2.Value;
                    switch (row.max_rank)
                    {
                        case 1:
                            row.Rank2.Text = "";
                            row.Rank3.Text = "";
                            row.Rank4.Text = "";
                            row.Rank5.Text = "";
                            row.Rank6.Text = "";
                            row.Rank7.Text = "";
                            row.Rank8.Text = "";
                            break;

                        case 2:
                            row.Rank3.Text = "";
                            row.Rank4.Text = "";
                            row.Rank5.Text = "";
                            row.Rank6.Text = "";
                            row.Rank7.Text = "";
                            row.Rank8.Text = "";
                            break;

                        case 3:
                            row.Rank4.Text = "";
                            row.Rank5.Text = "";
                            row.Rank6.Text = "";
                            row.Rank7.Text = "";
                            row.Rank8.Text = "";
                            break;

                        case 4:
                            row.Rank5.Text = "";
                            row.Rank6.Text = "";
                            row.Rank7.Text = "";
                            row.Rank8.Text = "";
                            break;

                        case 5:
                            row.Rank6.Text = "";
                            row.Rank7.Text = "";
                            row.Rank8.Text = "";
                            break;

                        case 6:
                            row.Rank7.Text = "";
                            row.Rank8.Text = "";
                            break;

                        default:
                            row.Rank7.Text = "";
                            row.Rank8.Text = "";
                            break;
                    }
                }
            }
        }

        #region 匯出 Word

        /// <summary>
        /// 匯出 Word
        /// </summary>
        private void ExportWord(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmMeeting, Dictionary<string, TTable> map, Item itmReturn)
        {
            TExport export = GetExportInfo(CCO, strMethodName, inn, itmMeeting);

            TConfig cfg = new TConfig
            {
                FontName = "標楷體",
                FontSize = 11,
                RowCount = 1 + 20,
                BodyRowCount = 20,
                RowStart = 1,
                ColStart = 0,
                CurrentRow = 1,
                Fields = new List<TField>(),
                MeetingTitle = itmMeeting.getProperty("in_title", ""),
            };

            cfg.Fields.Add(new TField { Title = "第二階", Property = "left_title", Format = "" });
            cfg.Fields.Add(new TField { Title = "第三階", Property = "row_title", Format = "" });
            cfg.Fields.Add(new TField { Title = "冠軍", Property = "rank1", Format = "" });
            cfg.Fields.Add(new TField { Title = "亞軍", Property = "rank2", Format = "" });
            cfg.Fields.Add(new TField { Title = "季軍", Property = "rank3", Format = "" });
            cfg.Fields.Add(new TField { Title = "季軍", Property = "rank4", Format = "" });
            cfg.Fields.Add(new TField { Title = "第五名", Property = "rank5", Format = "" });
            cfg.Fields.Add(new TField { Title = "第五名", Property = "rank6", Format = "" });

            //載入模板
            using (var doc = Xceed.Words.NET.DocX.Load(export.Source))
            {
                foreach (var kv in map)
                {
                    var table = kv.Value;

                    if (table.Rows.Count == 0)
                    {
                        continue;
                    }

                    if (table.Rows.Count > cfg.BodyRowCount)
                    {
                        throw new Exception("量級數量超過預設值，請調整 Word 樣板，並修改參數");
                    }

                    AppendWordTable(inn, doc, cfg, table);
                }

                doc.Tables[0].Remove();

                //doc.RemoveParagraphAt(0);
                doc.RemoveParagraphAt(0);

                doc.SaveAs(export.File);

            }

            itmReturn.setProperty("xls_name", export.Url);
        }

        private void AppendWordTable(Innovator inn, Xceed.Words.NET.DocX doc, TConfig cfg, TTable table)
        {
            cfg.CurrentRow = cfg.RowStart;

            //插入標題段落
            var p = doc.InsertParagraph();

            p.Append(cfg.MeetingTitle)
               .Font(new Xceed.Document.NET.Font(cfg.FontName))
               .FontSize(25)
               .Bold()
               .Spacing(10);
            //.Color(System.Drawing.Color.Red)
            p.Alignment = Xceed.Document.NET.Alignment.center;

            p = doc.InsertParagraph();
            p.Append(table.Title)
               .Font(new Xceed.Document.NET.Font(cfg.FontName))
               .FontSize(12);
            p.Alignment = Xceed.Document.NET.Alignment.left;

            //取得模板表格
            var template_table = doc.Tables[0];

            var docTable = doc.InsertTable(template_table);

            //資料容器
            Item itmContainer = inn.newItem();

            foreach (var rkv in table.Rows)
            {
                var row = rkv.Value;
                itmContainer.setProperty("left_title", table.LeftTitle_Word);
                itmContainer.setProperty("row_title", row.Title_Word);
                itmContainer.setProperty("rank1", row.Rank1.Text);
                itmContainer.setProperty("rank2", row.Rank2.Text);
                itmContainer.setProperty("rank3", row.Rank3.Text);
                itmContainer.setProperty("rank4", row.Rank4.Text);
                itmContainer.setProperty("rank5", row.Rank5.Text);
                itmContainer.setProperty("rank6", row.Rank6.Text);

                SetCellValue(docTable, cfg, itmContainer);

                cfg.CurrentRow++;
            }

            for (int i = cfg.RowCount - 1; i >= cfg.CurrentRow; i--)
            {
                docTable.RemoveRow(i);
            }

            if (!table.IsEndTable)
            {
                var pend = doc.InsertParagraph();
                pend.InsertPageBreakAfterSelf();
            }
        }

        //設定資料表格
        private void SetCellValue(Xceed.Document.NET.Table table, TConfig cfg, Item item)
        {
            int wsRow = cfg.CurrentRow;
            int wsCol = cfg.ColStart;
            int fdCount = cfg.Fields.Count;

            for (int i = 0; i < fdCount; i++)
            {
                var f = cfg.Fields[i];
                var x = wsCol + i;
                var v = GetCellValue(f, item);

                table.Rows[wsRow].Cells[x].Paragraphs.First().Append(v).Font(cfg.FontName).FontSize(cfg.FontSize);
            }
        }

        //設定資料列
        private string GetCellValue(TField field, Item item)
        {
            if (field.Property == "")
            {
                return "";
            }

            string value = item.getProperty(field.Property);

            return value;

            //switch (field.Format)
            //{
            //    case "center":
            //        break;

            //    case "yyyy/MM/dd":
            //    case "yyyy/MM/dd HH:mm":
            //        break;

            //    case "$ #,##0":
            //        cell.Value = value;
            //        cell.Style.NumberFormat.Format = "$ #,##0";
            //        break;

            //    case "tel":
            //        if (value.StartsWith("09"))
            //        {
            //            cell.Value = value.Replace("-", "");
            //            cell.Style.NumberFormat.Format = "0000-000-000";
            //            cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
            //        }
            //        else
            //        {
            //            cell.Value = "'" + value;
            //            cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
            //        }
            //        break;

            //    case "text":
            //        cell.Value = "'" + value;
            //        cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
            //        break;

            //    default:
            //        break;
            //}
        }

        /// <summary>
        /// 取得匯出設定資訊
        /// </summary>
        private TExport GetExportInfo(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmMeeting)
        {
            Item itmPath = GetXlsPaths(CCO, strMethodName, inn, "rank_overall_path");

            //比賽名稱
            string in_title = itmMeeting.getProperty("in_title", "");

            string export_path = itmPath.getProperty("export_path", "").TrimEnd('\\');
            string ext_name = ".docx";
            string doc_name = in_title + "_成績總表_" + DateTime.Now.ToString("yyyyMMdd_HHmmss");

            string doc_file = export_path + "\\" + doc_name + ext_name;
            string doc_url = doc_name + ext_name;

            return new TExport
            {
                Source = itmPath.getProperty("template_path", ""),
                File = doc_file,
                Url = doc_url,
            };
        }

        #endregion 匯出 Word

        #region 匯出 PDF

        /// <summary>
        /// 匯出 PDF
        /// </summary>
        private void ExportPDF(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmMeeting, Dictionary<string, TTable> map, Item itmReturn)
        {
            TConfig cfg = new TConfig
            {
                WsRow = 4,
                WsCol = 1,
                Heads = new string[] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" },
                SheetCount = 1,
                Sheets = new List<string>(),
                FontName = "標楷體",
                FontSize = 11,
                Fields = new List<TField>(),
                MeetingTitle = itmMeeting.getProperty("in_title", ""),
            };

            TExport export = GetPDFExportInfo(CCO, strMethodName, inn, itmMeeting);

            Spire.Xls.Workbook workbook = new Spire.Xls.Workbook();
            workbook.LoadFromFile(export.Source);
            Spire.Xls.Worksheet sheetTemplate = workbook.Worksheets[0];

            foreach (var kv in map)
            {
                var table = kv.Value;

                if (table.Rows.Count == 0)
                {
                    continue;
                }

                AppendSheets(cfg, CCO, strMethodName, inn, workbook, sheetTemplate, table);
            }

            //移除樣板 sheet
            sheetTemplate.Remove();
            //輸出 pdf
            workbook.SaveToFile(export.File.Replace("xlsx", "pdf"), Spire.Xls.FileFormat.PDF);

            itmReturn.setProperty("xls_name", export.Url.Replace("xlsx", "pdf"));
        }

        private void AppendSheets(TConfig cfg
            , Aras.Server.Core.CallContext CCO
            , string strMethodName
            , Innovator inn
            , Spire.Xls.Workbook workbook
            , Spire.Xls.Worksheet sheetTemplate
            , TTable table)
        {

            Spire.Xls.Worksheet sheet = workbook.CreateEmptySheet();
            sheet.CopyFrom(sheetTemplate);
            sheet.Name = table.Title;

            sheet.Range["A1"].Text = table.Head;
            sheet.Range["A1"].Style.Font.FontName = cfg.FontName;

            sheet.Range["A2"].Text = table.Title;
            sheet.Range["A2"].Style.Font.FontName = cfg.FontName;

            int wsRow = cfg.WsRow;
            int wsCol = cfg.WsCol;

            List<TField> fields = new List<TField>();
            //fields.Add(new TField { Title = "第二階", Property = "left_title", Format = "" });
            fields.Add(new TField { Title = "第三階", Property = "row_title", Format = "" });
            fields.Add(new TField { Title = "冠軍", Property = "rank1", Format = "" });
            fields.Add(new TField { Title = "亞軍", Property = "rank2", Format = "" });
            fields.Add(new TField { Title = "季軍", Property = "rank3", Format = "" });
            fields.Add(new TField { Title = "季軍", Property = "rank4", Format = "" });
            fields.Add(new TField { Title = "第五名", Property = "rank5", Format = "" });
            fields.Add(new TField { Title = "第五名", Property = "rank6", Format = "" });

            int row_start = wsRow;
            int row_end = wsRow + table.Rows.Count;

            //資料容器
            Item itmContainer = inn.newItem();

            foreach (var rkv in table.Rows)
            {
                var row = rkv.Value;
                //itmContainer.setProperty("left_title", table.LeftTitle_Word);
                itmContainer.setProperty("row_title", row.Title_Word);
                itmContainer.setProperty("rank1", row.Rank1.Text);
                itmContainer.setProperty("rank2", row.Rank2.Text);
                itmContainer.setProperty("rank3", row.Rank3.Text);
                itmContainer.setProperty("rank4", row.Rank4.Text);
                itmContainer.setProperty("rank5", row.Rank5.Text);
                itmContainer.setProperty("rank6", row.Rank6.Text);

                SetItemCell(sheet, cfg, wsRow, wsCol, itmContainer, fields);

                wsRow++;
            }

            string pos_1 = "A" + row_start;
            string pos_2 = "A" + (row_end - 1);
            
                          sheet.SetRowHeight(4, 50);
            Spire.Xls.CellRange range = sheet.Range[pos_1 + ":" + pos_2];
            range.Merge();
            range.Text = table.LeftTitle_Word;
 
            range.Style.Borders[Spire.Xls.BordersLineType.EdgeTop].LineStyle = Spire.Xls.LineStyleType.Thin;
            range.Style.Borders[Spire.Xls.BordersLineType.EdgeRight].LineStyle = Spire.Xls.LineStyleType.Thin;
            range.Style.Borders[Spire.Xls.BordersLineType.EdgeBottom].LineStyle = Spire.Xls.LineStyleType.Thin;
            range.Style.Borders[Spire.Xls.BordersLineType.EdgeLeft].LineStyle = Spire.Xls.LineStyleType.Thin;

        }

        //設定物件與資料列
        private void SetItemCell(Spire.Xls.Worksheet sheet, TConfig cfg, int wsRow, int wsCol, Item item, List<TField> fields)
        {
            for (int i = 0; i < fields.Count; i++)
            {
                var field = fields[i];
                var value = "";
                var position = cfg.Heads[wsCol + i] + wsRow;

                if (field.getValue != null)
                {
                    value = field.getValue(cfg, item, field);
                }
                else if (field.Property != "")
                {
                    value = item.getProperty(field.Property, "");
                }

                Spire.Xls.CellRange range = sheet.Range[position];
                SetBodyCell(range, cfg, value, field.Property);
            }
        }

        //設定資料列
        private void SetBodyCell(Spire.Xls.CellRange range, TConfig cfg, string value = "", string format = "")
        {
            switch (format)
            {
                case "center":
                    range.Text = value;
                    range.Style.Font.FontName = cfg.FontName;
                    break;

                case "yyyy/MM/dd":
                case "yyyy/MM/dd HH:mm":
                    range.Text = GetDateTimeVal(value, format);
                    range.Style.Font.FontName = cfg.FontName;
                    break;

                case "$ #,##0":
                    range.Text = value;
                    range.Style.Font.FontName = cfg.FontName;
                    break;

                case "text":
                    range.Text = value;
                    range.Style.Font.FontName = cfg.FontName;
                    break;

                default:
                    range.Text = value;
                    range.Style.Font.FontName = cfg.FontName;
                    break;
            }

            range.Style.Borders[Spire.Xls.BordersLineType.EdgeTop].LineStyle = Spire.Xls.LineStyleType.Thin;
            range.Style.Borders[Spire.Xls.BordersLineType.EdgeRight].LineStyle = Spire.Xls.LineStyleType.Thin;
            range.Style.Borders[Spire.Xls.BordersLineType.EdgeBottom].LineStyle = Spire.Xls.LineStyleType.Thin;
            range.Style.Borders[Spire.Xls.BordersLineType.EdgeLeft].LineStyle = Spire.Xls.LineStyleType.Thin;
        }

        /// <summary>
        /// 取得匯出設定資訊
        /// </summary>
        private TExport GetPDFExportInfo(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmMeeting)
        {
            Item itmPath = GetXlsPaths(CCO, strMethodName, inn, "rank_pdf_path");

            //比賽名稱
            string in_title = itmMeeting.getProperty("in_title", "");

            string export_path = itmPath.getProperty("export_path", "").TrimEnd('\\');
            string ext_name = ".xlsx";
            string doc_name = in_title + "_成績總表_" + DateTime.Now.ToString("yyyyMMdd_HHmmss");

            string doc_file = export_path + "\\" + doc_name + ext_name;
            string doc_url = doc_name + ext_name;

            return new TExport
            {
                Source = itmPath.getProperty("template_path", ""),
                File = doc_file,
                Url = doc_url,
            };
        }

        #endregion 匯出 PDF

        //附加表格
        private void AppendTables(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Dictionary<string, TTable> map, Item itmReturn)
        {
            StringBuilder builder = new StringBuilder();

            foreach (var kv in map)
            {
                var table = kv.Value;
                if (table.Rows.Count > 0)
                {
                    AppendTable(builder, kv.Value);
                }
            }

            itmReturn.setProperty("inn_tables", builder.ToString());
        }

        //附加分組名單
        private void AppendTable(StringBuilder builder, TTable table)
        {
            builder.Append("<div class=\"showsheet_" + table.Name + " page-block\">");
            builder.Append("  <h2 class=\"text-center\">" + table.Head + "</h2>");
            builder.Append("  <h4 class=\"sheet-title\">" + table.Title + "<span> </span> </h4>");

            builder.Append("  <table id=\"" + table.Name + "\" data-toggle='table'  class=\"table table-hover table-bordered table-rwd rwd\">");

            AppendRankTable(builder, table);

            builder.Append("  </table>");

            //builder.Append("  <script>");
            //builder.Append("      $('#" + table.Name + "').bootstrapTable();");
            //builder.Append("      if($(window).width() <= 768) { $('#" + table.Name + "').bootstrapTable('toggleView');}");
            //builder.Append("  </script>");

            // builder.Append("  <div style='height: 25px;'> </div>");//2020.10.06 補了間隔 讓其可以蓋章
            // builder.Append("  <div class=\"pull-left signature\">" + "裁判簽章" + "</div>");

            builder.Append("</div>");
        }

        //附加名次
        private void AppendRankTable(StringBuilder builder, TTable table)
        {
            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            head.Append("<thead>");
            head.Append("  <tr>");
            head.Append("    <th class='text-center mailbox-subject search-field' colspan='2'>組別／級別／名次</th>");
            head.Append("    <th class='text-center mailbox-subject search-field'>冠軍</th>");
            head.Append("    <th class='text-center mailbox-subject search-field'>亞軍</th>");
            head.Append("    <th class='text-center mailbox-subject search-field'>季軍</th>");
            head.Append("    <th class='text-center mailbox-subject search-field'>季軍</th>");
            head.Append("    <th class='text-center mailbox-subject search-field'>第五名</th>");
            head.Append("    <th class='text-center mailbox-subject search-field'>第五名</th>");
            head.Append("  </tr>");
            head.Append("</thead>");

            body.Append("<tbody>");
            foreach (var kv in table.Rows)
            {
                var row = kv.Value;

                body.Append("  <tr>");
                if (row.No == 1)
                {
                    body.Append("    <td rowspan='" + table.Rows.Count + "' class='text-center' style='width: 10px'>" + table.LeftTitle_Html + "</td>");
                }
                body.Append("    <td class='text-center' style='width: 65px'>" + row.Title_Html + "</td>");
                body.Append("    <td class='text-center' style='width: 120px'>" + row.Rank1.Text + "</td>");
                body.Append("    <td class='text-center' style='width: 120px'>" + row.Rank2.Text + "</td>");
                body.Append("    <td class='text-center' style='width: 120px'>" + row.Rank3.Text + "</td>");
                body.Append("    <td class='text-center' style='width: 120px'>" + row.Rank4.Text + "</td>");
                body.Append("    <td class='text-center' style='width: 120px'>" + row.Rank5.Text + "</td>");
                body.Append("    <td class='text-center' style='width: 120px'>" + row.Rank6.Text + "</td>");
                body.Append("  </tr>");

            }
            body.Append("</tbody>");

            builder.Append(head);
            builder.Append(body);
        }

        /// <summary>
        /// 取得賽事資訊
        /// </summary>
        private Item GetMeeting(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            string sql = "";

            string meeting_id = itmReturn.getProperty("meeting_id", "");

            sql = @"
        SELECT
            t1.id
            , t1.in_title
        FROM
            IN_MEETING t1 WITH(NOLOCK)
        WHERE
            t1.id = '{#meeting_id}'
        ";

            sql = sql.Replace("{#meeting_id}", meeting_id);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return inn.applySQL(sql);
        }

        /// <summary>
        /// 取得樣板與匯出路徑
        /// </summary>
        private Item GetXlsPaths(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string in_name)
        {
            Item itmResult = inn.newItem();

            //Excel 匯出路徑 (from 原創.設定參數)
            string sql = @"
                SELECT 
                    t2.in_name AS 'variable'
                    , t1.in_name
                    , t1.in_value 
                FROM 
                    In_Variable_Detail t1 WITH(NOLOCK)
                INNER JOIN 
                    In_Variable t2 WITH(NOLOCK) 
                    ON t2.ID = t1.SOURCE_ID 
                WHERE 
                    t2.in_name = N'meeting_excel'  
                    AND t1.in_name IN (N'export_path', N'{#in_name}')
            ";

            sql = sql.Replace("{#in_name}", in_name);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item items = inn.applySQL(sql);

            if (items.isError() || items.getItemCount() < 1)
            {
                throw new Exception("未設定樣板與匯出路徑 _# " + in_name);
            }

            for (int i = 0; i < items.getItemCount(); i++)
            {
                Item item = items.getItemByIndex(i);
                string xls_name = item.getProperty("in_name", "");
                string xls_value = item.getProperty("in_value", "");

                if (xls_name == "export_path")
                {
                    itmResult.setProperty("export_path", xls_value);
                }
                else
                {
                    itmResult.setProperty("template_path", xls_value);
                }
            }

            return itmResult;
        }

        /// <summary>
        /// 取得名次資訊
        /// </summary>
        private Item GetRanks(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmMeeting)
        {
            string meeting_id = itmMeeting.getProperty("id", "");

            string sql = @"
                SELECT
                    t2.id AS 'program_id'     
                    , ISNULL(t2.in_short_name, t2.in_display) AS 'in_short_name'
                    , t2.in_l1
                    , t2.in_l2
                    , t2.in_l3
                    , t2.in_team_count
                    , t1.in_current_org
                    , t1.in_short_org
                    , t1.map_short_org
                    , t1.map_org_name
                    , t1.in_team
                    , t1.in_name
                    , t1.in_sign_no
                    , t1.in_section_no
                    , t1.in_sno
                    , t1.in_final_rank
                    , t1.in_show_rank
                FROM
                    VU_MEETING_PTEAM t1 WITH(NOLOCK)
                INNER JOIN
                    IN_MEETING_PROGRAM t2 WITH(NOLOCK)
                    ON t2.id = t1.source_id
                WHERE
                    t2.in_meeting = '{#meeting_id}'
                    AND ISNULL(t2.in_program, '') = ''
                    AND ISNULL(t1.in_final_rank, 0) > 0
                ORDER BY
                    t2.in_sort_order
                    , t1.in_stuff_b1
            ";

            sql = sql.Replace("{#meeting_id}", meeting_id);

            return inn.applySQL(sql);
        }

        /// <summary>
        /// 取得名次資訊
        /// </summary>
        private Dictionary<string, TTable> MapRankList(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmMeeting, Item items)
        {
            string meeting_title = itmMeeting.getProperty("in_title", "");

            int count = items.getItemCount();

            Dictionary<string, TTable> map = new Dictionary<string, TTable>();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);

                TTable table = AddAndGetTable(map, meeting_title, item);

                AddRow(inn, table, item);
            }

            return map;
        }

        /// <summary>
        /// 附加並回傳 Table
        /// </summary>
        private TTable AddAndGetTable(Dictionary<string, TTable> map, string meeting_title, Item item)
        {
            string in_l1 = item.getProperty("in_l1", "");
            string in_l2 = item.getProperty("in_l2", "");
            string title = ClearLv2(in_l2);

            var key = "";
            var type = TableEnum.None;

            switch (in_l1)
            {
                case "團體組":
                    key = in_l1;
                    type = TableEnum.Multiple;
                    break;

                case "個人組":
                    key = in_l2;
                    type = TableEnum.Single;
                    break;

                case "格式組":
                    key = in_l2;
                    type = TableEnum.Format;
                    break;

                default:
                    key = in_l2;
                    type = TableEnum.Single;
                    break;
            }

            TTable table = null;

            if (map.ContainsKey(key))
            {
                table = map[key];
            }
            else
            {
                table = new TTable
                {
                    Key = key,
                    Id = (map.Count + 1).ToString(),
                    Name = "rank_" + (map.Count + 1).ToString(),
                    Head = meeting_title,
                    Rows = new Dictionary<string, TRow>(),
                    RowSpan = 1,
                    TableType = type
                };

                switch (table.TableType)
                {
                    case TableEnum.Multiple:
                        table.Title = in_l1;
                        table.Title2 = in_l1;
                        table.LeftTitle = in_l1;
                        table.LeftTitle_Html = string.Join("<br>", in_l1.ToCharArray());
                        table.LeftTitle_Word = string.Join("\n", in_l1.ToCharArray());
                        break;

                    case TableEnum.Single:
                    case TableEnum.Format:
                        table.Title = ClearTitle(in_l1, title);
                        table.Title2 = in_l1;
                        table.LeftTitle = title;
                        table.LeftTitle_Html = string.Join("<br>", title.ToCharArray());
                        table.LeftTitle_Word = string.Join("\n", title.ToCharArray());
                        break;

                    default:
                        break;
                }

                map.Add(key, table);
            }
            return table;
        }

        private string ClearLv2(string in_l2)
        {
            if (in_l2 == "") return "";

            return in_l2.Replace("少~", "")
                .Replace("青~", "")
                .Replace("成~", "");

            //in_l2 == "" ? "" : in_l2.Split('-').Last();
        }

        private string ClearTitle(string in_l1, string clear_l2)
        {
            if (in_l1.EndsWith("組") && clear_l2.EndsWith("組"))
            {
                return in_l1.Trim('組') + "-" + clear_l2;
            }
            else
            {
                return in_l1 + "-" + clear_l2;

            }
        }

        /// <summary>
        /// 附加 Row
        /// </summary>
        private void AddRow(Innovator inn, TTable table, Item item)
        {
            string in_l1 = item.getProperty("in_l1", "");
            string in_l2 = item.getProperty("in_l2", "");
            string in_l3 = item.getProperty("in_l3", "");
            string in_short_name = item.getProperty("in_short_name", "");
            string in_team_count = item.getProperty("in_team_count", "");
            string in_final_rank = item.getProperty("in_final_rank", "");

            var key = "";

            switch (table.TableType)
            {
                case TableEnum.Multiple:
                    key = in_l2;
                    break;

                case TableEnum.Single:
                case TableEnum.Format:
                    key = in_l3;
                    break;

                default:
                    break;
            }

            TRow row = null;
            if (table.Rows.ContainsKey(key))
            {
                row = table.Rows[key];
            }
            else
            {
                row = new TRow
                {
                    Key = key,
                    Title = in_l3,
                    No = table.Rows.Count + 1,
                    Rank1 = new TRank(),
                    Rank2 = new TRank(),
                    Rank3 = new TRank(),
                    Rank4 = new TRank(),
                    Rank5 = new TRank(),
                    Rank6 = new TRank(),
                    Rank7 = new TRank(),
                    Rank8 = new TRank(),
                    in_team_count = in_team_count,
                };

                Item itmMaxRank = inn.applyMethod("in_meeting_program_rank", "<in_team_count>" + in_team_count + "</in_team_count>");
                row.max_rank = GetIntVal(itmMaxRank.getProperty("max_rank", "0"));

                switch (table.TableType)
                {
                    case TableEnum.Multiple:
                        var c = in_l3.Contains("男") ? '男' : '女';
                        var arr = in_l3.Split(c);

                        //row.Title = in_l3 + "(" + row.in_team_count + ")";
                        row.Title = in_l3;
                        row.Title_Html = string.Join("<br>" + c, arr);
                        row.Title_Word = string.Join("\n" + c, arr);
                        break;

                    case TableEnum.Single:
                    case TableEnum.Format:
                        row.Title = in_l3.Split('：').First();
                        // row.Title_Html = row.Title + "(" + row.in_team_count + ")";
                        // row.Title_Word = row.Title + "(" + row.in_team_count + ")";
                        row.Title_Html = row.Title;
                        row.Title_Word = row.Title;
                        break;

                    default:
                        break;
                }

                table.Rows.Add(key, row);
            }

            switch (in_final_rank)
            {
                case "1":
                    SetRank(table, row.Rank1, item);
                    break;

                case "2":
                    SetRank(table, row.Rank2, item);
                    break;

                case "3":
                    if (!row.Rank3.HasSetted)
                    {
                        SetRank(table, row.Rank3, item);
                    }
                    else
                    {
                        SetRank(table, row.Rank4, item);
                    }
                    break;

                case "4":
                    SetRank(table, row.Rank4, item);
                    break;

                case "5":
                    if (!row.Rank5.HasSetted)
                    {
                        SetRank(table, row.Rank5, item);
                    }
                    else
                    {
                        SetRank(table, row.Rank6, item);
                    }
                    break;

                case "6":
                    SetRank(table, row.Rank6, item);
                    break;

                case "7":
                    if (!row.Rank7.HasSetted)
                    {
                        SetRank(table, row.Rank7, item);
                    }
                    else
                    {
                        SetRank(table, row.Rank8, item);
                    }
                    break;

                case "8":
                    SetRank(table, row.Rank8, item);
                    break;


                default:
                    break;
            }
        }

        //設定名次資料
        private void SetRank(TTable table, TRank rank, Item item)
        {
            rank.HasSetted = true;

            rank.Value = item;

            switch (table.TableType)
            {
                case TableEnum.Multiple:
                    rank.Text = GetNameInfo2(item);
                    break;

                case TableEnum.Single:
                case TableEnum.Format:
                    rank.Text = GetNameInfo1(item);
                    break;

                default:
                    break;
            }
        }

        //個人組、格式組
        private string GetNameInfo1(Item item)
        {
            if (item == null)
            {
                return "&nbsp;";
            }

            string in_name = item.getProperty("in_name", "");
            string in_current_org = item.getProperty("in_current_org", "");
            string map_short_org = item.getProperty("map_short_org", "");

            if (in_name.Contains("("))
            {
                return in_name;
            }
            else
            {
                return in_name + "(" + map_short_org + ")";
            }
        }

        //團體組
        private string GetNameInfo2(Item item)
        {
            if (item == null)
            {
                return "&nbsp;";
            }

            string in_current_org = item.getProperty("in_current_org", "");
            string map_short_org = item.getProperty("map_short_org", "");
            string in_team = item.getProperty("in_team", "");
            string in_name = item.getProperty("in_name", "");

            if (in_team == "")
            {
                return map_short_org;
            }
            else
            {
                return map_short_org + "(" + in_team + ")";
            }
        }

        /// <summary>
        /// 表格類型
        /// </summary>
        private enum TableEnum
        {
            /// <summary>
            /// 未設定
            /// </summary>
            None = 0,
            /// <summary>
            /// 團體組
            /// </summary>
            Multiple = 1,
            /// <summary>
            /// 個人組
            /// </summary>
            Single = 2,
            /// <summary>
            /// 格式組
            /// </summary>
            Format = 3,
        }

        /// <summary>
        /// 表格資料模型
        /// </summary>
        private class TTable
        {
            /// <summary>
            /// 鍵
            /// </summary>
            public string Key { get; set; }

            /// <summary>
            /// Table 流水號
            /// </summary>
            public string Id { get; set; }

            /// <summary>
            /// Table 名稱
            /// </summary>
            public string Name { get; set; }

            /// <summary>
            /// Table 標題
            /// </summary>
            public string Title { get; set; }

            /// <summary>
            /// Table 標題
            /// </summary>
            public string Title2 { get; set; }

            /// <summary>
            /// Table 左側標題
            /// </summary>
            public string LeftTitle { get; set; }

            /// <summary>
            /// Table 左側標題
            /// </summary>
            public string LeftTitle_Html { get; set; }

            /// <summary>
            /// Table 左側標題
            /// </summary>
            public string LeftTitle_Word { get; set; }


            /// <summary>
            /// 賽事名稱
            /// </summary>
            public string Head { get; set; }

            /// <summary>
            /// 參賽人數
            /// </summary>
            public string TeamCount { get; set; }

            /// <summary>
            /// 列資料集
            /// </summary>
            public Dictionary<string, TRow> Rows { get; set; }

            /// <summary>
            /// 跨列
            /// </summary>
            public int RowSpan { get; set; }

            /// <summary>
            /// 表格類型
            /// </summary>
            public TableEnum TableType { get; set; }

            /// <summary>
            /// 是否為結束表格
            /// </summary>
            public bool IsEndTable { get; set; }
        }

        /// <summary>
        /// 列模式
        /// </summary>
        private class TRow
        {
            /// <summary>
            /// 鍵
            /// </summary>
            public string Key { get; set; }

            /// <summary>
            /// 列序
            /// </summary>
            public int No { get; set; }

            /// <summary>
            /// 項目名稱
            /// </summary>
            public string Title { get; set; }

            /// <summary>
            /// 項目名稱
            /// </summary>
            public string Title_Html { get; set; }

            /// <summary>
            /// 項目名稱
            /// </summary>
            public string Title_Word { get; set; }

            /// <summary>
            /// 冠軍
            /// </summary>
            public TRank Rank1 { get; set; }

            /// <summary>
            /// 亞軍
            /// </summary>
            public TRank Rank2 { get; set; }

            /// <summary>
            /// 季軍
            /// </summary>
            public TRank Rank3 { get; set; }

            /// <summary>
            /// 第 4 名 or 季軍
            /// </summary>
            public TRank Rank4 { get; set; }

            /// <summary>
            /// 第 5 名
            /// </summary>
            public TRank Rank5 { get; set; }

            /// <summary>
            /// 第 6 名
            /// </summary>
            public TRank Rank6 { get; set; }

            /// <summary>
            /// 第 7 名
            /// </summary>
            public TRank Rank7 { get; set; }

            /// <summary>
            /// 第 8 名
            /// </summary>
            public TRank Rank8 { get; set; }

            public string in_team_count { get; set; }
            public int max_rank { get; set; }
        }

        /// <summary>
        /// 名次資料模型
        /// </summary>
        private class TRank
        {
            public string Text { get; set; }

            public Item Value { get; set; }

            public bool HasSetted { get; set; }
        }

        /// <summary>
        /// 匯出資料模型
        /// </summary>
        private class TExport
        {
            /// <summary>
            /// 樣板來源完整路徑
            /// </summary>
            public string Source { get; set; }

            /// <summary>
            /// 檔案名稱
            /// </summary>
            public string File { get; set; }

            /// <summary>
            /// 檔案網址
            /// </summary>
            public string Url { get; set; }
        }

        /// <summary>
        /// 組態
        /// </summary>
        private class TConfig
        {
            /// <summary>
            /// 字型名稱
            /// </summary>
            public string FontName { get; set; }

            /// <summary>
            /// 字型大小
            /// </summary>
            public int FontSize { get; set; }

            /// <summary>
            /// 樣板列數( Head 加 Body)
            /// </summary>
            public int RowCount { get; set; }

            /// <summary>
            /// 樣板列數( Body)
            /// </summary>
            public int BodyRowCount { get; set; }

            /// <summary>
            /// 列起始位置
            /// </summary>
            public int RowStart { get; set; }

            /// <summary>
            /// 欄起始位置
            /// </summary>
            public int ColStart { get; set; }

            /// <summary>
            /// 當前列位置
            /// </summary>
            public int CurrentRow { get; set; }

            /// <summary>
            /// 欄位清單
            /// </summary>
            public List<TField> Fields { get; set; }

            /// <summary>
            /// 賽事名稱
            /// </summary>
            public string MeetingTitle { get; set; }

            public int SheetCount { get; set; }

            public List<string> Sheets { get; set; }

            public int WsRow { get; set; }

            public int WsCol { get; set; }

            public string[] Heads { get; set; }

        }

        /// <summary>
        /// 欄位
        /// </summary>
        private class TField
        {
            /// <summary>
            /// 標題
            /// </summary>
            public string Title { get; set; }
            /// <summary>
            /// 屬性
            /// </summary>
            public string Property { get; set; }
            /// <summary>
            /// 格式
            /// </summary>
            public string Format { get; set; }

            /// <summary>
            /// 欄寬
            /// </summary>
            public int Width { get; set; }

            ///// <summary>
            ///// 是否特殊著色
            ///// </summary>
            //public bool color { get; set; }

            ///// <summary>
            ///// 著色判斷函式
            ///// </summary>
            //public Func<Item, bool> color_func { get; set; }

            public Func<TConfig, Item, TField, string> getValue { get; set; }
        }

        /// <summary>
        /// 轉換為整數
        /// </summary>
        private int GetIntVal(string value, int def = 0)
        {
            int result = def;
            Int32.TryParse(value, out result);
            return result;
        }

        /// <summary>
        /// 轉換為日期時間
        /// </summary>
        private string GetDateTimeVal(string value, string format)
        {
            DateTime result = DateTime.MinValue;
            DateTime.TryParse(value, out result);
            if (result != DateTime.MinValue)
            {
                return result.ToString(format);
            }
            else
            {
                return "";
            }

        }
    }
}