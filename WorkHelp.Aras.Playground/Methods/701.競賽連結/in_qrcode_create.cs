﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;
using System.Net;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class in_qrcode_create : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 競賽連結中心-建立連結
                輸入: meeting_id
                日期: 
                    2021-12-09: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]in_qrcode_create";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, this.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                InnH = _InnH,

                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),

                meeting_id = itmR.getProperty("meeting_id", ""),
            };

            if (cfg.meeting_id == "")
            {
                throw new Exception("賽事 id 不可為空值");
            }

            //賽事
            Item itmMeeting = cfg.inn.applySQL("SELECT * FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            if (itmMeeting.isError() || itmMeeting.getResult() == "")
            {
                throw new Exception("查無 賽事資料");
            }
            cfg.mt_title = itmMeeting.getProperty("in_title", "");

            //iplm 站台網址
            cfg.iplm_url = GetVariable(cfg, "app_url");

            //QrCode 路徑
            cfg.iplm_qrcode_folder = GetVariable(cfg, "meeting_qrcode").TrimEnd('\\');
            cfg.iplm_qrcode_url = cfg.iplm_url + "/"
                + cfg.iplm_qrcode_folder.ToLower().Replace(@"c:\site\judo\", "").Replace("\\", "/");

            //Api
            cfg.api_url = GetVariable(cfg, "api_url");

            //MVC 網址
            cfg.mvc_url = "https://app3.innosoft.com.tw/judo_tool";

            //競賽連結中心
            CreatIndexPage(cfg);

            //競管平台
            CreatAdminPage(cfg);

            return itmR;
        }

        #region 競管平台

        private void CreatAdminPage(TConfig cfg)
        {
            Item itmDays = cfg.inn.applySQL("SELECT DISTINCT in_date_key FROM IN_MEETING_ALLOCATION WITH(NOLOCK) WHERE in_meeting = '" + cfg.meeting_id + "' ORDER BY in_date_key");

            //首頁
            var lnkIndex = new TLink { val = "admin", lbl = "首頁", sort = "1", page = "AdminIndex.html" };
            Item itmIndex = GetMShortUrl(cfg, "", lnkIndex);
            //itmIndex = IndexPage(cfg, lnkIndex);

            //功能頁
            var lnkFunc = new TLink { val = "admin-func", lbl = cfg.mt_title, sort = "1", page = "AdminFunc.html" };
            Item itmFunc = GetMShortUrl(cfg, cfg.meeting_id, lnkFunc);
            if (itmFunc.isError() || itmFunc.getResult() == "")
            {
                lnkFunc.sort = GetFuncSort(cfg, lnkFunc);
                itmFunc = CommonPage(cfg, itmIndex, lnkFunc);
            }
            else
            {
                lnkFunc.sort = itmFunc.getProperty("in_sort_order", "");
                itmFunc = CommonPage(cfg, itmIndex, lnkFunc);
            }

            //過磅
            var lnkWeight = new TLink { val = "admin-weight", lbl = "過磅", sort = "1", page = "AdminWeight.html" };
            Item itmWeight = GetMShortUrl(cfg, cfg.meeting_id, lnkWeight);
            itmWeight = CommonPage(cfg, itmFunc, lnkWeight);

            //過磅-比賽日期
            AddFightDayFuncs(cfg, itmWeight, lnkWeight, itmDays, true);

            //抽籤
            var lnkDraw = new TLink { val = "admin-draw", lbl = "抽籤", sort = "2" };
            Item itmDraw = GetMShortUrl(cfg, cfg.meeting_id, lnkDraw);
            itmDraw = CommonFunc(cfg, itmFunc, lnkDraw, DrawUrl);

            //對戰表
            var lnkBattleTree = new TLink { val = "admin-tree", lbl = "對戰表", sort = "3" };
            Item itmBattleTree = GetMShortUrl(cfg, cfg.meeting_id, lnkBattleTree);
            itmBattleTree = CommonFunc(cfg, itmFunc, lnkBattleTree, TreeUrl);

            //場次編號
            var lnkTreeNo = new TLink { val = "admin-treeno", lbl = "場次編號", sort = "4", page = "AdminSubFunc.html" };
            Item itmTreeNo = GetMShortUrl(cfg, cfg.meeting_id, lnkTreeNo);
            itmTreeNo = CommonPage(cfg, itmFunc, lnkTreeNo);

            //場次編號編輯
            AddTreeNoFuncs(cfg, itmTreeNo);

            //抽磅
            var lnkSpot = new TLink { val = "admin-spotcheck", lbl = "抽磅", sort = "5", page = "AdminWeight.html" };
            Item itmSpot = GetMShortUrl(cfg, cfg.meeting_id, lnkSpot);
            itmSpot = CommonPage(cfg, itmFunc, lnkSpot);

            //抽磅-比賽日期
            AddFightDayFuncs(cfg, itmSpot, lnkSpot, itmDays, false);

            //戰情中心
            var lnkCentral = new TLink { val = "admin-central", lbl = "戰情中心", sort = "6" };
            Item itmCentral = GetMShortUrl(cfg, cfg.meeting_id, lnkCentral);
            itmCentral = CommonFunc(cfg, itmFunc, lnkCentral, RtmCentralUrl);

            //即時成績列表
            var lnkRtmScore = new TLink { val = "admin-rtmscore", lbl = "即時成績列表", sort = "7" };
            Item itmRtmScore = GetMShortUrl(cfg, cfg.meeting_id, lnkRtmScore);
            itmRtmScore = CommonFunc(cfg, itmFunc, lnkRtmScore, RealTimeScoreUrl);

            //場次統計與調場
            var lnkSummary = new TLink { val = "admin-summary", lbl = "場次統計與調場", sort = "8" };
            Item itmSummary = GetMShortUrl(cfg, cfg.meeting_id, lnkSummary);
            itmSummary = CommonFunc(cfg, itmFunc, lnkSummary, SummaryUrl);

            //檢錄(比賽進度表)
            var lnkCheckIn = new TLink { val = "admin-checkin", lbl = "檢錄(比賽進度表)", sort = "9" };
            Item itmCheckIn = GetMShortUrl(cfg, cfg.meeting_id, lnkCheckIn);
            itmCheckIn = CommonFunc(cfg, itmFunc, lnkCheckIn, CheckInUrl);

            //獎牌戰
            var lnkMedal = new TLink { val = "admin-medal", lbl = "獎牌戰(挑戰賽)", sort = "10" };
            Item itmMedal = GetMShortUrl(cfg, cfg.meeting_id, lnkMedal);
            itmMedal = CommonFunc(cfg, itmFunc, lnkMedal, MedalUrl);

            //名次公告
            var lnkRank = new TLink { val = "admin-rank", lbl = "名次公告", sort = "11" };
            Item itmRank = GetMShortUrl(cfg, cfg.meeting_id, lnkRank);
            itmRank = CommonFunc(cfg, itmFunc, lnkRank, RankUrl);


            //賽程管理
            var lnkRule = new TLink { val = "admin-rule", lbl = "賽程管理", sort = "12", page = "AdminSubFunc.html" };
            Item itmRule = GetMShortUrl(cfg, cfg.meeting_id, lnkRule);
            itmRule = CommonPage(cfg, itmFunc, lnkRule);

            //賽程管理-細項功能
            AddRuleFuncs(cfg, itmRule);

            //團體戰
            var lnkTeamBattle = new TLink { val = "admin-team", lbl = "團體戰", sort = "13", page = "AdminSubFunc.html" };
            Item itmTeamBattle = GetMShortUrl(cfg, cfg.meeting_id, lnkTeamBattle);
            itmTeamBattle = CommonPage(cfg, itmFunc, lnkTeamBattle);

            //團體戰-細項功能
            AddTeamFuncs(cfg, itmTeamBattle);

            //選手資訊
            var lnkRtmPlayer = new TLink { val = "admin-player", lbl = "選手資訊", sort = "14" };
            Item itmRtmPlayer = GetMShortUrl(cfg, cfg.meeting_id, lnkRtmPlayer);
            itmRtmPlayer = CommonFunc(cfg, itmFunc, lnkRtmPlayer, RtmPlayerUrl);
        }

        //賽程管理-細項功能
        private void AddRuleFuncs(TConfig cfg, Item itmParent)
        {
            //賽程管理-賽事後台
            var lnkManager = new TLink { val = "admin-rule-manager", lbl = "賽事後台", sort = "2" };
            Item itmManager = GetMShortUrl(cfg, cfg.meeting_id, lnkManager);
            itmManager = CommonFunc(cfg, itmParent, lnkManager, ManagerSetting);

            //賽程管理-賽制設定
            var lnkSetting = new TLink { val = "admin-rule-setting", lbl = "賽制設定", sort = "2" };
            Item itmSetting = GetMShortUrl(cfg, cfg.meeting_id, lnkSetting);
            itmSetting = CommonFunc(cfg, itmParent, lnkSetting, RuleSetting);

            //賽程管理-過磅體重設定
            var lnkWeight = new TLink { val = "admin-rule-weight", lbl = "過磅體重設定", sort = "3" };
            Item itmWeight = GetMShortUrl(cfg, cfg.meeting_id, lnkWeight);
            itmWeight = CommonFunc(cfg, itmParent, lnkWeight, RuleWeight);

            //賽程管理-場地設定
            var lnkSite = new TLink { val = "admin-rule-site", lbl = "場地設定", sort = "4" };
            Item itmSite = GetMShortUrl(cfg, cfg.meeting_id, lnkSite);
            itmSite = CommonFunc(cfg, itmParent, lnkSite, RuleSite);

            ////賽程管理-團體賽設定
            //var lnkTeams = new TLink { val = "admin-rule-team", lbl = "團體賽設定", sort = "5" };
            //Item itmTeams = GetMShortUrl(cfg, cfg.meeting_id, lnkTeams);
            //itmTeams = CommonFunc(cfg, itmParent, lnkTeams, RuleTeams);

            //賽程管理-參賽者管理
            var lnkPlayer = new TLink { val = "admin-rule-player", lbl = "參賽者管理", sort = "6" };
            Item itmPlayer = GetMShortUrl(cfg, cfg.meeting_id, lnkPlayer);
            itmPlayer = CommonFunc(cfg, itmParent, lnkPlayer, RulePlayer);

            //賽程管理-上傳賽程資料
            var lnkUpload = new TLink { val = "admin-rule-upload", lbl = "上傳賽程資料", sort = "7" };
            Item itmUpload = GetMShortUrl(cfg, cfg.meeting_id, lnkUpload);
            itmUpload = CommonFunc(cfg, itmParent, lnkUpload, RuleUpload);
        }

        //賽程管理-賽制設定
        private string RuleSetting(TConfig cfg)
        {
            string in_api_srcurl = cfg.iplm_url + "/pages/c.aspx"
                + "?page=In_MeetingProgram.html"
                + "&method=in_meeting_program"
                + "&meeting_id=" + cfg.meeting_id
                + "&scene="
                + "";

            return in_api_srcurl;
        }

        //賽程管理-賽事後台
        private string ManagerSetting(TConfig cfg)
        {
            string in_api_srcurl = cfg.iplm_url + "/pages/c.aspx"
                + "?page=In_MeetingView.html"
                + "&method=In_Get_SingleItemInfoGeneral"
                + "&itemtype=In_Meeting"
                + "&itemid=" + cfg.meeting_id
                + "&mode=latest"
                + "";

            return in_api_srcurl;
        }

        //賽程管理-過磅體重設定
        private string RuleWeight(TConfig cfg)
        {
            string in_api_srcurl = cfg.iplm_url + "/pages/c.aspx"
                + "?page=In_MeetingWeight.html"
                + "&method=in_meeting_weight"
                + "&meeting_id=" + cfg.meeting_id
                + "";

            return in_api_srcurl;
        }

        //賽程管理-場地設定
        private string RuleSite(TConfig cfg)
        {
            string in_api_srcurl = cfg.iplm_url + "/pages/c.aspx"
                + "?page=In_MeetingSite_Allocate.html"
                + "&method=In_meeting_allocation"
                + "&meeting_id=" + cfg.meeting_id
                + "&mode=edit"
                + "&link=weight"
                + "";

            return in_api_srcurl;
        }

        //賽程管理-團體賽設定
        private string RuleTeams(TConfig cfg)
        {
            string in_api_srcurl = cfg.iplm_url + "/pages/c.aspx"
                + "?page=in_team_battle_setting.html"
                + "&method=in_team_battle_setting"
                + "&meeting_id=" + cfg.meeting_id
                + "";

            return in_api_srcurl;
        }

        //賽程管理-參賽者管理
        private string RulePlayer(TConfig cfg)
        {
            string in_api_srcurl = cfg.iplm_url + "/pages/c.aspx"
                + "?page=In_MeetingProgram_Player.html"
                + "&method=in_meeting_pteam"
                + "&meeting_id=" + cfg.meeting_id
                + "";

            return in_api_srcurl;
        }

        //賽程管理-上傳賽程資料
        private string RuleUpload(TConfig cfg)
        {
            string in_api_srcurl = cfg.iplm_url + "/pages/c.aspx"
                + "?page=In_MeetingSite_Upload.html"
                + "&method=In_meeting_allocation"
                + "&meeting_id=" + cfg.meeting_id
                + "&scene=upload"
                + "";

            return in_api_srcurl;
        }


        //團體戰-細項功能
        private void AddTeamFuncs(TConfig cfg, Item itmParent)
        {
            //團體戰-註冊
            var lnkRegister = new TLink { val = "admin-team-register", lbl = "註冊", sort = "1" };
            Item itmRegister = GetMShortUrl(cfg, cfg.meeting_id, lnkRegister);
            itmRegister = CommonFunc(cfg, itmParent, lnkRegister, TeamBattleRegisterUrl);

            //團體戰-過磅
            var lnkWeight = new TLink { val = "admin-team-weight", lbl = "過磅", sort = "2" };
            Item itmWeight = GetMShortUrl(cfg, cfg.meeting_id, lnkWeight);
            itmWeight = CommonFunc(cfg, itmParent, lnkWeight, TeamBattleWeightUrl);

            //團體戰-出賽
            var lnkFight = new TLink { val = "admin-team-fight", lbl = "出賽", sort = "3" };
            Item itmFight = GetMShortUrl(cfg, cfg.meeting_id, lnkFight);
            itmFight = CommonFunc(cfg, itmParent, lnkFight, TeamBattleFightUrl);

            ////團體戰-成績總表
            //var lnkScore = new TLink { val = "admin-team-score", lbl = "成績總表", sort = "4" };
            //Item itmScore = GetMShortUrl(cfg, cfg.meeting_id, lnkScore);
            //itmScore = CommonFunc(cfg, itmParent, lnkScore, TeamBattleScoreUrl);

            //賽程管理-團體賽設定
            var lnkTeams = new TLink { val = "admin-rule-team", lbl = "量級設定", sort = "5" };
            Item itmTeams = GetMShortUrl(cfg, cfg.meeting_id, lnkTeams);
            itmTeams = CommonFunc(cfg, itmParent, lnkTeams, RuleTeams);
        }

        //團體戰-註冊
        private string TeamBattleRegisterUrl(TConfig cfg)
        {
            string in_api_srcurl = cfg.iplm_url + "/pages/c.aspx"
                + "?page=in_team_battle_register.html"
                + "&method=in_team_battle_register"
                + "&meeting_id=" + cfg.meeting_id
                + "&scene="
                + "";

            return in_api_srcurl;
        }

        //團體戰-過磅
        private string TeamBattleWeightUrl(TConfig cfg)
        {
            string in_api_srcurl = cfg.iplm_url + "/pages/c.aspx"
                + "?page=in_team_battle_weight.html"
                + "&method=in_team_battle_register"
                + "&meeting_id=" + cfg.meeting_id
                + "&scene=weight"
                + "";

            return in_api_srcurl;
        }

        //團體戰-出賽
        private string TeamBattleFightUrl(TConfig cfg)
        {
            string in_api_srcurl = cfg.iplm_url + "/pages/c.aspx"
                + "?page=in_team_battle_fight.html"
                + "&method=in_team_battle_fight"
                + "&meeting_id=" + cfg.meeting_id
                + "&scene="
                + "";

            return in_api_srcurl;
        }

        //團體戰-成績總表
        private string TeamBattleScoreUrl(TConfig cfg)
        {
            string in_api_srcurl = cfg.iplm_url + "/pages/c.aspx"
                + "?page=In_Team_Battle_Score.html"
                + "&method=in_team_battle_fight"
                + "&meeting_id=" + cfg.meeting_id
                + "&scene=score"
                + "";

            return in_api_srcurl;
        }

        //名次公告
        private string RankUrl(TConfig cfg)
        {
            string in_api_srcurl = cfg.iplm_url + "/pages/c.aspx"
                + "?page=In_Competition_Bulletin_N1.html"
                + "&method=in_meeting_program_bulletin_print"
                + "&meeting_id=" + cfg.meeting_id
                + "";

            return in_api_srcurl;
        }

        //獎牌戰
        private string MedalUrl(TConfig cfg)
        {
            string in_api_srcurl = cfg.iplm_url + "/pages/c.aspx"
                + "?page=In_Meeting_Medal.html"
                + "&method=in_meeting_medal"
                + "&meeting_id=" + cfg.meeting_id
                + "";

            return in_api_srcurl;
        }

        //檢錄(比賽進度表)
        private string CheckInUrl(TConfig cfg)
        {
            string in_api_srcurl = cfg.iplm_url + "/pages/b.aspx"
                + "?page=PublicNextSearch.html"
                + "&method=in_meeting_program_score_s"
                + "&meeting_id=" + cfg.meeting_id
                + "&scene=next"
                + "&open=1"
                + "&auto=1"
                + "";

            return in_api_srcurl;
        }

        //場次統計與調場
        private string SummaryUrl(TConfig cfg)
        {
            string in_api_srcurl = cfg.iplm_url + "/pages/c.aspx"
                + "?page=In_Competition_Summary.html"
                + "&method=in_meeting_pevent_sum"
                + "&meeting_id=" + cfg.meeting_id
                + "";

            return in_api_srcurl;
        }

        //即時成績列表
        private string RealTimeScoreUrl(TConfig cfg)
        {
            string in_api_srcurl = cfg.iplm_url + "/pages/c.aspx"
                + "?page=In_Competition_ScoreSearch.html"
                + "&method=in_meeting_program_score_s"
                + "&meeting_id=" + cfg.meeting_id
                + "&scene="
                + "";

            return in_api_srcurl;
        }

        //調整場次順序(列表編輯)
        private string TreeNoEdit1Url(TConfig cfg)
        {
            string in_api_srcurl = cfg.iplm_url + "/pages/c.aspx"
                + "?page=In_MeetingSite_Shift.html"
                + "&method=in_meeting_treeno_shift"
                + "&meeting_id=" + cfg.meeting_id
                + "&scene=page"
                + "";

            return in_api_srcurl;
        }

        //調整場次順序(樹狀編輯)
        private string TreeNoEdit2Url(TConfig cfg)
        {
            string in_api_srcurl = cfg.iplm_url + "/pages/c.aspx"
                + "?page=in_competition_preview.html"
                + "&method=in_meeting_program_preview"
                + "&meeting_id=" + cfg.meeting_id
                + "&eno=edit"
                + "";

            return in_api_srcurl;
        }

        //對戰表(含單位數量)
        private string TreeUrl(TConfig cfg)
        {
            string in_api_srcurl = cfg.iplm_url + "/pages/c.aspx"
                + "?page=In_Competition_Preview.html"
                + "&method=in_meeting_program_preview"
                + "&meeting_id=" + cfg.meeting_id
                + "&mode=draw"
                + "";

            return in_api_srcurl;
        }

        //抽籤
        private string DrawUrl(TConfig cfg)
        {
            string in_api_srcurl = cfg.iplm_url + "/pages/c.aspx"
                + "?page=In_MeetingDraw.html"
                + "&method=in_meeting_draw"
                + "&meeting_id=" + cfg.meeting_id
                + "";

            return in_api_srcurl;
        }

        private void AddFightDayFuncs(TConfig cfg, Item itmParent, TLink lnkParent, Item items, bool is_weight)
        {
            if (items.isError() || items.getResult() == "")
            {
                return;
            }

            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string code = (i + 1).ToString();
                string value = lnkParent.val + "-site-" + code.PadLeft(2, '0');
                string label = "比賽日期 " + item.getProperty("in_date_key", "");
                string key = item.getProperty("in_date_key", "");

                var link = new TLink { val = value, lbl = label, sort = code, key = key };

                WeightDayFunc(cfg, itmParent, link, is_weight);
            }
        }

        //場次編號編輯
        private void AddTreeNoFuncs(TConfig cfg, Item itmTreeNo)
        {
            //場次編號
            var lnkEdit1 = new TLink { val = "admin-treeno-edit-01", lbl = "列表編輯", sort = "1" };
            Item itmEdit1 = GetMShortUrl(cfg, cfg.meeting_id, lnkEdit1);
            itmEdit1 = CommonFunc(cfg, itmTreeNo, lnkEdit1, TreeNoEdit1Url);

            //場次編號
            var lnkEdit2 = new TLink { val = "admin-treeno-edit-02", lbl = "樹狀編輯", sort = "2" };
            Item itmEdit2 = GetMShortUrl(cfg, cfg.meeting_id, lnkEdit2);
            itmEdit2 = CommonFunc(cfg, itmTreeNo, lnkEdit2, TreeNoEdit2Url);
        }

        //過磅與抽磅-比賽日期
        private Item WeightDayFunc(TConfig cfg, Item itmParent, TLink link, bool is_weight)
        {
            string parent_id = itmParent.getProperty("id", "");

            string in_api_srcurl = "";

            if (is_weight)
            {
                in_api_srcurl = cfg.iplm_url + "/pages/c.aspx"
                + "?page=Inspection_check_group.html"
                + "&method=In_Meeting_GetCheckByGroup"
                + "&meeting_id=" + cfg.meeting_id
                + "&in_date=" + link.key
                + "";
            }
            else
            {
                in_api_srcurl = cfg.iplm_url + "/pages/c.aspx"
                + "?page=Inspection_spotcheck_group.html"
                + "&method=in_meeting_program_spotcheck"
                + "&meeting_id=" + cfg.meeting_id
                + "&in_date=" + link.key
                + "";
            }

            //Aras
            var api_entity = GetUrlEntity(cfg, in_api_srcurl);

            //中繼
            var mid_entity = GetUrlEntity(cfg, in_api_srcurl.Replace("in_qrcode_query1", "in_qrcode_query2"));

            //靜態
            string in_mvc_srcurl = GetMvcUrl(cfg, mid_entity.ShortUrl);
            var mvc_entity = GetUrlEntity(cfg, in_mvc_srcurl);

            Item item = cfg.inn.newItem("IN_MEETING_SHORTURL", "merge");
            item.setAttribute("where", "in_meeting = '" + cfg.meeting_id + "' AND in_value = '" + link.val + "'");
            item.setProperty("in_meeting", cfg.meeting_id);
            item.setProperty("in_parent", parent_id);

            SetProperties(item, link.val, link.lbl, link.sort, api_entity, mid_entity, mvc_entity);

            item = item.apply();

            if (item.isError() || item.getResult() == "")
            {
                throw new Exception("競管平台-" + link.lbl + ": 建立失敗");
            }

            return item;
        }

        #endregion 競管平台

        private void CreatIndexPage(TConfig cfg)
        {
            //首頁
            var lnkIndex = new TLink { val = "index", lbl = "首頁", sort = "1", page = "PublicIndex.html" };
            Item itmIndex = GetMShortUrl(cfg, "", lnkIndex);
            itmIndex = IndexPage(cfg, lnkIndex);
            //if (itmIndex.isError() || itmIndex.getResult() == "")
            //{
            //    itmIndex = IndexPage(cfg, lnkIndex);
            //}

            //功能頁
            var lnkFunc = new TLink { val = "function", lbl = cfg.mt_title, sort = "1", page = "PublicFunc.html" };
            Item itmFunc = GetMShortUrl(cfg, cfg.meeting_id, lnkFunc);
            if (itmFunc.isError() || itmFunc.getResult() == "")
            {
                lnkFunc.sort = GetFuncSort(cfg, lnkFunc);
                itmFunc = CommonPage(cfg, itmIndex, lnkFunc);
            }
            else
            {
                lnkFunc.sort = itmFunc.getProperty("in_sort_order", "");
                itmFunc = CommonPage(cfg, itmIndex, lnkFunc);
            }

            //檢錄頁
            var lnkCheckIn = new TLink { val = "checkin", lbl = "檢錄(比賽進度表)", sort = "1", page = "PublicWaiting.html" };
            Item itmCheckIn = GetMShortUrl(cfg, cfg.meeting_id, lnkCheckIn);
            itmCheckIn = CommonPage(cfg, itmFunc, lnkCheckIn);
            // if (itmCheckIn.isError() || itmCheckIn.getResult() == "")
            // {
            //     itmCheckIn = CommonPage(cfg, itmFunc, lnkCheckIn);
            // }

            //檢錄頁-各場地
            Item itmSites = cfg.inn.applySQL("SELECT * FROM IN_MEETING_SITE WITH(NOLOCK) WHERE in_meeting = '" + cfg.meeting_id + "' ORDER BY in_code");
            if (!itmSites.isError() && itmSites.getResult() != "")
            {
                int site_count = itmSites.getItemCount();
                for (int i = 0; i < site_count; i++)
                {
                    Item itmSite = itmSites.getItemByIndex(i);
                    string site_code = itmSite.getProperty("in_code", "0");
                    string site_val = lnkCheckIn.val + "-site-" + site_code.PadLeft(2, '0');
                    string site_lbl = itmSite.getProperty("in_name", "");

                    var lnkSite = new TLink { val = site_val, lbl = site_lbl, sort = site_code };

                    CheckinSiteFunc(cfg, itmCheckIn, itmSite, lnkSite);
                }

                cfg.site_count = site_count;
            }
            else
            {
                cfg.site_count = 0;
            }

            //最新成績(戰情中心)-列表版
            var lnkRtmScore = new TLink { val = "rtm-score-list", lbl = "最新成績(戰情中心)", sort = "2" };
            Item itmRtmScore = GetMShortUrl(cfg, cfg.meeting_id, lnkRtmScore);
            itmRtmScore = CommonFunc(cfg, itmFunc, lnkRtmScore, RtmScoreUrl, in_is_closed: "1");


            //最新成績(戰情中心)-手機版
            var lnkRtmScore2 = new TLink { val = "rtm-score", lbl = "最新成績(戰情中心)", sort = "2" };
            Item itmRtmScore2 = GetMShortUrl(cfg, cfg.meeting_id, lnkRtmScore2);
            itmRtmScore = CommonFunc(cfg, itmFunc, lnkRtmScore2, RtmCentralUrl, in_is_closed: "0");

            //對戰表
            var lnkRtmTree = new TLink { val = "rtm-tree", lbl = "對戰表", sort = "3" };
            Item itmRtmTree = GetMShortUrl(cfg, cfg.meeting_id, lnkRtmTree);
            itmRtmTree = CommonFunc(cfg, itmFunc, lnkRtmTree, RtmTreeUrl);

            //選手資訊
            var lnkRtmPlayer = new TLink { val = "rtm-player", lbl = "選手資訊", sort = "4" };
            Item itmRtmPlayer = GetMShortUrl(cfg, cfg.meeting_id, lnkRtmPlayer);
            itmRtmPlayer = CommonFunc(cfg, itmFunc, lnkRtmPlayer, RtmPlayerUrl);

            //選手場地資訊
            var lnkRtmSite = new TLink { val = "rtm-site", lbl = "選手場地資訊", sort = "5" };
            Item itmRtmSite = GetMShortUrl(cfg, cfg.meeting_id, lnkRtmSite);
            itmRtmPlayer = CommonFunc(cfg, itmFunc, lnkRtmSite, RtmPlayerSiteUrl);

            //場地分配表
            var lnkAllocate = new TLink { val = "allocate-site", lbl = "場地分配表", sort = "6" };
            Item itmAllocate = GetMShortUrl(cfg, cfg.meeting_id, lnkAllocate);
            itmAllocate = CommonFunc(cfg, itmFunc, lnkAllocate, AllocateSiteUrl);
        }

        private string GetVariable(TConfig cfg, string in_name)
        {
            Item item = cfg.inn.applySQL("SELECT TOP 1 in_value FROM IN_VARIABLE WITH(NOLOCK) WHERE in_name = '" + in_name + "'");
            if (item.isError() || item.getResult() == "")
            {
                throw new Exception("查無 " + in_name + " 站台網址");
            }
            return item.getProperty("in_value", "").TrimEnd('/');
        }

        private Item GetMShortUrl(TConfig cfg, string meeting_id, TLink link)
        {
            string sql = "SELECT TOP 1 * FROM IN_MEETING_SHORTURL WITH(NOLOCK)"
                + " WHERE ISNULL(in_meeting, '') = '" + meeting_id + "'"
                + " AND in_value = '" + link.val + "'";

            return cfg.inn.applySQL(sql);
        }

        //首頁
        private Item IndexPage(TConfig cfg, TLink link)
        {
            string in_api_srcurl = cfg.iplm_url + "/pages/b.aspx"
                + "?page=" + link.page
                + "&method=in_qrcode_query1"
                + "&meeting_id="
                + "&func_id=" + link.val;

            //Aras
            var api_entity = GetUrlEntity(cfg, in_api_srcurl);

            //中繼
            var mid_entity = GetUrlEntity(cfg, in_api_srcurl.Replace("in_qrcode_query1", "in_qrcode_query2"));

            //靜態
            string in_mvc_srcurl = GetMvcUrl(cfg, mid_entity.ShortUrl);
            var mvc_entity = GetUrlEntity(cfg, in_mvc_srcurl);

            Item item = cfg.inn.newItem("IN_MEETING_SHORTURL", "merge");
            item.setAttribute("where", "in_value = '" + link.val + "'");

            SetProperties(item, link.val, link.lbl, link.sort, api_entity, mid_entity, mvc_entity);

            item = item.apply();

            if (item.isError() || item.getResult() == "")
            {
                throw new Exception("競賽連結中心-首頁: 建立失敗");
            }

            return item;
        }

        //功能頁
        private Item CommonPage(TConfig cfg, Item itmParent, TLink link)
        {
            string parent_id = itmParent.getProperty("id", "");

            string in_api_srcurl = cfg.iplm_url + "/pages/b.aspx"
                + "?page=" + link.page
                + "&method=in_qrcode_query1"
                + "&meeting_id=" + cfg.meeting_id
                + "&func_id=" + link.val;

            //Aras
            var api_entity = GetUrlEntity(cfg, in_api_srcurl);

            //中繼
            var mid_entity = GetUrlEntity(cfg, in_api_srcurl.Replace("in_qrcode_query1", "in_qrcode_query2"));

            //靜態
            string in_mvc_srcurl = GetMvcUrl(cfg, mid_entity.ShortUrl);
            var mvc_entity = GetUrlEntity(cfg, in_mvc_srcurl);

            Item item = cfg.inn.newItem("IN_MEETING_SHORTURL", "merge");
            item.setAttribute("where", "in_meeting = '" + cfg.meeting_id + "' AND in_value = '" + link.val + "'");
            item.setProperty("in_meeting", cfg.meeting_id);
            item.setProperty("in_parent", parent_id);

            SetProperties(item, link.val, link.lbl, link.sort, api_entity, mid_entity, mvc_entity);

            item = item.apply();

            if (item.isError() || item.getResult() == "")
            {
                throw new Exception("競賽連結中心-" + link.lbl + ": 建立失敗");
            }

            return item;
        }

        //檢錄頁-場地
        private Item CheckinSiteFunc(TConfig cfg, Item itmParent, Item itmSite, TLink link)
        {
            string parent_id = itmParent.getProperty("id", "");

            string in_api_srcurl = cfg.iplm_url + "/pages/b.aspx"
                + "?page=PublicNextSearch.html"
                + "&method=in_meeting_program_score_s"
                + "&meeting_id=" + cfg.meeting_id
                + "&site_code=" + link.sort
                + "&scene=next"
                + "&open=1"
                + "&auto=1";

            //Aras
            var api_entity = GetUrlEntity(cfg, in_api_srcurl);

            //中繼
            var mid_entity = GetUrlEntity(cfg, in_api_srcurl.Replace("in_qrcode_query1", "in_qrcode_query2"));

            //靜態
            string in_mvc_srcurl = GetMvcUrl(cfg, mid_entity.ShortUrl);
            var mvc_entity = GetUrlEntity(cfg, in_mvc_srcurl);

            Item item = cfg.inn.newItem("IN_MEETING_SHORTURL", "merge");
            item.setAttribute("where", "in_meeting = '" + cfg.meeting_id + "' AND in_value = '" + link.val + "'");
            item.setProperty("in_meeting", cfg.meeting_id);
            item.setProperty("in_parent", parent_id);

            SetProperties(item, link.val, link.lbl, link.sort, api_entity, mid_entity, mvc_entity);

            item = item.apply();

            if (item.isError() || item.getResult() == "")
            {
                throw new Exception("競賽連結中心-" + link.lbl + ": 建立失敗");
            }

            return item;
        }

        //通用 Func
        private Item CommonFunc(TConfig cfg, Item itmParent, TLink link, Func<TConfig, string> getUrl, string in_is_closed = "0")
        {
            string parent_id = itmParent.getProperty("id", "");

            string in_api_srcurl = getUrl(cfg);

            //Aras
            var api_entity = GetUrlEntity(cfg, in_api_srcurl);

            //中繼
            var mid_entity = GetUrlEntity(cfg, in_api_srcurl.Replace("in_qrcode_query1", "in_qrcode_query2"));

            //靜態
            string in_mvc_srcurl = GetMvcUrl(cfg, mid_entity.ShortUrl);
            var mvc_entity = GetUrlEntity(cfg, in_mvc_srcurl);

            Item item = cfg.inn.newItem("IN_MEETING_SHORTURL", "merge");
            item.setAttribute("where", "in_meeting = '" + cfg.meeting_id + "' AND in_value = '" + link.val + "'");
            item.setProperty("in_meeting", cfg.meeting_id);
            item.setProperty("in_parent", parent_id);

            SetProperties(item, link.val, link.lbl, link.sort, api_entity, mid_entity, mvc_entity);
            item.setProperty("in_is_closed", in_is_closed);

            item = item.apply();

            if (item.isError() || item.getResult() == "")
            {
                throw new Exception("競賽連結中心-" + link.lbl + ": 建立失敗");
            }

            return item;
        }

        //最新成績(戰情中心)
        private string RtmScoreUrl(TConfig cfg)
        {
            string in_api_srcurl = cfg.iplm_url + "/pages/b.aspx"
                + "?page=PublicScoreSearch.html"
                + "&method=in_meeting_program_score_s"
                + "&meeting_id=" + cfg.meeting_id
                + "&open=1"
                + "";

            return in_api_srcurl;

            //string in_api_srcurl = cfg.iplm_url + "/pages/b.aspx"
            //    + "?page=in_gameboard_view.html"
            //    + "&method=in_gameboard_view"
            //    + "&meeting_id=" + cfg.meeting_id
            //    + "&scene=rtm2"
            //    + "&delay=1500"
            //    + "&count=" + cfg.site_count
            //    + "";

            //return in_api_srcurl;
        }

        //最新成績(戰情中心)
        private string RtmCentralUrl(TConfig cfg)
        {
            string in_api_srcurl = cfg.iplm_url + "/pages/b.aspx"
                + "?page=PublicCentral.html"
                + "&method=in_meeting_pevent_central"
                + "&meeting_id=" + cfg.meeting_id
                + "";

            return in_api_srcurl;
        }

        //對戰表
        private string RtmTreeUrl(TConfig cfg)
        {
            string in_api_srcurl = cfg.iplm_url + "/pages/b.aspx"
                + "?page=PublicCompetition.html"
                + "&method=in_meeting_program_preview"
                + "&meeting_id=" + cfg.meeting_id
                + "";

            return in_api_srcurl;
        }

        //選手資訊
        private string RtmPlayerUrl(TConfig cfg)
        {
            string in_api_srcurl = cfg.iplm_url + "/pages/b.aspx"
                + "?page=PublicPlayer.html"
                + "&method=in_player_info"
                + "&meeting_id=" + cfg.meeting_id
                + "";

            return in_api_srcurl;
        }

        //選手場地資訊
        private string RtmPlayerSiteUrl(TConfig cfg)
        {
            string in_api_srcurl = cfg.iplm_url + "/pages/b.aspx"
                + "?page=PublicPlayerSite.html"
                + "&method=in_player_site"
                + "&meeting_id=" + cfg.meeting_id
                + "";

            return in_api_srcurl;
        }

        //場地分配表
        private string AllocateSiteUrl(TConfig cfg)
        {
            string in_api_srcurl = cfg.iplm_url + "/pages/b.aspx"
                + "?page=In_MeetingSite_Show.html"
                + "&method=In_meeting_allocation"
                + "&meeting_id=" + cfg.meeting_id
                + "&menu=" + "no"
                + "";

            return in_api_srcurl;
        }

        private void SetProperties(Item item, string in_value, string in_label, string in_sort, TUrl api_entity, TUrl mid_entity, TUrl mvc_entity)
        {
            item.setProperty("in_value", in_value);
            item.setProperty("in_label", in_label);
            item.setProperty("in_sort_order", in_sort);

            item.setProperty("in_api_srcurl", api_entity.SrcUrl);
            item.setProperty("in_api_shorturl", api_entity.ShortUrl);
            item.setProperty("in_api_token", api_entity.Token);
            item.setProperty("in_api_qrcode", api_entity.QrCodeUrl);

            item.setProperty("in_mid_srcurl", mid_entity.SrcUrl);
            item.setProperty("in_mid_shorturl", mid_entity.ShortUrl);
            item.setProperty("in_mid_token", mid_entity.Token);
            item.setProperty("in_mid_qrcode", mid_entity.QrCodeUrl);

            item.setProperty("in_mvc_srcurl", mvc_entity.SrcUrl);
            item.setProperty("in_mvc_shorturl", mvc_entity.ShortUrl);
            item.setProperty("in_mvc_token", mvc_entity.Token);
            item.setProperty("in_mvc_qrcode", mvc_entity.QrCodeUrl);
        }

        private TUrl GetUrlEntity(TConfig cfg, string src_url)
        {
            TUrl result = new TUrl
            {
                SrcUrl = src_url,
                ShortUrl = "",
                Token = "",
                QrCodeFolder = "",
                QrCodeUrl = "",
            };

            result.ShortUrl = GetApiShortUrl(cfg, result.SrcUrl);

            if (result.ShortUrl == "")
            {
                throw new Exception("建立短網址發生錯誤 _# " + result.SrcUrl);
            }
            else
            {
                result.Token = result.ShortUrl.Split('/').Last();

                string[] qrcodes = GenerateQrCode(cfg, result.Token, result.ShortUrl);
                result.QrCodeFolder = qrcodes[0];
                result.QrCodeUrl = qrcodes[1];
            }

            return result;
        }

        private TUrl GetMVCUrlEntity(TConfig cfg, string src_url)
        {
            TUrl result = new TUrl
            {
                SrcUrl = src_url,
                ShortUrl = "",
                Token = "",
                QrCodeFolder = "",
                QrCodeUrl = "",
            };

            result.ShortUrl = GetApiShortUrl(cfg, result.SrcUrl);

            if (result.ShortUrl == "")
            {
                throw new Exception("建立短網址發生錯誤 _# " + result.SrcUrl);
            }
            else
            {
                result.Token = result.ShortUrl.Split('/').Last();

                string[] qrcodes = GenerateQrCode(cfg, result.Token, result.ShortUrl);
                result.QrCodeFolder = qrcodes[0];
                result.QrCodeUrl = qrcodes[1];
            }

            return result;
        }


        private string GetPlmMtUrl(TConfig cfg, string page, string func_id)
        {
            return cfg.iplm_url + "/pages/b.aspx"
                + "?page=" + page
                + "&method=in_qrcode_query1"
                + "&meeting_id=" + cfg.meeting_id
                + "&func_id=" + func_id
                + "";
        }

        private string GetMvcUrl(TConfig cfg, string in_api_shorturl)
        {
            return cfg.mvc_url + "/Index/Static_HTML_Read"
                + "?url=" + in_api_shorturl;
        }

        //生成短網址
        private string GetApiShortUrl(TConfig cfg, string srcUrl)
        {
            string result = "";

            try
            {
                string tgturl = System.Web.HttpUtility.UrlEncode(srcUrl);
                string fullurl = cfg.api_url + "/create?url=" + tgturl;
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, fullurl);

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(fullurl);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                StreamReader tReader = new StreamReader(response.GetResponseStream());

                string rst = tReader.ReadToEnd();
                if (!string.IsNullOrWhiteSpace(rst))
                {
                    result = rst.Replace("\"", "");
                }
            }
            catch (Exception ex)
            {
                result = "";
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, ex.Message);
            }

            return result;
        }

        //生成 QrCode
        private string[] GenerateQrCode(TConfig cfg, string name, string contents)
        {
            try
            {
                string folder = cfg.iplm_qrcode_folder;
                if (!System.IO.Directory.Exists(folder))
                {
                    System.IO.Directory.CreateDirectory(folder);
                }

                string file = name + ".jpg";
                string img_file = System.IO.Path.Combine(folder, file);
                string img_url = cfg.iplm_qrcode_url + "/" + file;

                if (!System.IO.File.Exists(img_file))
                {
                    var writer = new ZXing.BarcodeWriter  //dll裡面可以看到屬性
                    {
                        Format = ZXing.BarcodeFormat.QR_CODE,
                        Options = new ZXing.QrCode.QrCodeEncodingOptions //設定大小
                        {
                            Height = 100,
                            Width = 100,
                            Margin = 0,
                            //CharacterSet = "UTF-8",
                            //ErrorCorrection = ZXing.QrCode.Internal.ErrorCorrectionLevel.M
                        },
                    };

                    var img = writer.Write(contents);
                    var myBitmap = new System.Drawing.Bitmap(img);

                    myBitmap.Save(img_file, System.Drawing.Imaging.ImageFormat.Jpeg);
                }

                return new string[] { img_file, img_url };
            }
            catch (Exception ex)
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, ex.Message);
                return new string[] { "", "" };
            }
        }

        private string GetFuncSort(TConfig cfg, TLink link)
        {
            string sql = "SELECT count(*) AS 'cnt' FROM IN_MEETING_SHORTURL WITH(NOLOCK) WHERE in_value = '" + link.val + "'";
            Item itmData = cfg.inn.applySQL(sql);
            if (itmData.isError() || itmData.getResult() == "")
            {
                return "1";
            }

            string cnt = itmData.getProperty("cnt", "0");
            if (cnt == "" || cnt == "0")
            {
                return "1";
            }

            int old_sort = GetIntValue(cnt);
            if (old_sort == 0)
            {
                return "1";
            }
            else
            {
                return (old_sort + 1).ToString();
            }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public Innosoft.InnovatorHelper InnH { get; set; }

            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }

            public string meeting_id { get; set; }
            public string mt_title { get; set; }

            public string iplm_url { get; set; }

            public string iplm_qrcode_folder { get; set; }
            public string iplm_qrcode_url { get; set; }

            public string api_url { get; set; }
            public string mvc_url { get; set; }

            public int site_count { get; set; }
        }

        private class TLink
        {
            public string val { get; set; }
            public string lbl { get; set; }
            public string sort { get; set; }
            public string page { get; set; }

            public string key { get; set; }
        }

        private class TUrl
        {
            public string SrcUrl { get; set; }
            public string ShortUrl { get; set; }
            public string Token { get; set; }
            public string QrCodeFolder { get; set; }
            public string QrCodeUrl { get; set; }
        }

        private int GetIntValue(string value)
        {
            int result = 0;
            if (Int32.TryParse(value, out result))
            {
                return result;
            }
            else
            {
                return 0;
            }
        }
    }
}