﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;
using System.Net;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class in_qrcode_query : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 競賽連結中心-建立連結
                輸入: meeting_id
                日期: 
                    2021-12-09: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]in_qrcode_create";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, this.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                InnH = _InnH,

                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),

                meeting_id = itmR.getProperty("meeting_id", ""),
            };

            cfg.meeting_id = "4420520723C44A70B5F312C38B238128";

            if (cfg.meeting_id == "")
            {
                throw new Exception("賽事 id 不可為空值");
            }

            //賽事
            Item itmMeeting = cfg.inn.applySQL("SELECT * FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            if (itmMeeting.isError() || itmMeeting.getResult() == "")
            {
                throw new Exception("查無 賽事資料");
            }
            cfg.mt_title = itmMeeting.getProperty("in_title", "");

            //iplm 站台網址
            Item itmPlm = cfg.inn.applySQL("SELECT TOP 1 in_value FROM IN_VARIABLE WITH(NOLOCK) WHERE in_name = 'app_url'");
            if (itmPlm.isError() || itmPlm.getResult() == "")
            {
                throw new Exception("查無 iplm 站台網址");
            }
            cfg.iplm_url = itmPlm.getProperty("in_value", "").TrimEnd('/');

            //QrCode 路徑
            Item itmQrCode = cfg.inn.applySQL("SELECT TOP 1 in_value FROM IN_VARIABLE WITH(NOLOCK) WHERE in_name = 'meeting_qrcode'");
            if (itmQrCode.isError() || itmQrCode.getResult() == "")
            {
                throw new Exception("查無 QrCode 存放路徑");
            }
            cfg.iplm_qrcode_folder = itmQrCode.getProperty("in_value", "").TrimEnd('\\');
            cfg.iplm_qrcode_url = cfg.iplm_url + "/"
                + cfg.iplm_qrcode_folder.ToLower().Replace(@"c:\site\judo\", "").Replace("\\", "/");

            //Api 與 MVC 網址
            cfg.api_url = "https://act.innosoft.com.tw/api";
            cfg.mvc_url = "https://app3.innosoft.com.tw/judo_tool";


            //首頁
            var lnkIndex = new TLink { val = "index", lbl = "首頁", sort = "1", page = "PublicIndex.html" };
            Item itmIndex = GetMShortUrl(cfg, "", lnkIndex);
            if (itmIndex.isError() || itmIndex.getResult() == "")
            {
                itmIndex = IndexPage(cfg, lnkIndex);
            }

            //功能頁
            var lnkFunc = new TLink { val = "function", lbl = cfg.mt_title, sort = "1", page = "PublicFunc.html" };
            Item itmFunc = GetMShortUrl(cfg, cfg.meeting_id, lnkFunc);
            if (itmFunc.isError() || itmFunc.getResult() == "")
            {
                itmFunc = CommonPage(cfg, itmIndex, lnkFunc);
            }

            //檢錄頁
            var lnkCheckIn = new TLink { val = "checkin", lbl = "檢錄頁", sort = "1", page = "PublicWaiting.html" };
            Item itmCheckIn = GetMShortUrl(cfg, cfg.meeting_id, lnkCheckIn);
            if (itmCheckIn.isError() || itmCheckIn.getResult() == "")
            {
                itmCheckIn = CommonPage(cfg, itmFunc, lnkCheckIn);
            }

            //檢錄頁-各場地
            Item itmSites = cfg.inn.applySQL("SELECT * FROM IN_MEETING_SITE WITH(NOLOCK) WHERE in_meeting = '" + cfg.meeting_id + "' ORDER BY in_code");
            if (!itmSites.isError() && itmSites.getResult() != "")
            {
                int site_count = itmSites.getItemCount();
                for (int i = 0; i < site_count; i++)
                {
                    Item itmSite = itmSites.getItemByIndex(i);
                    string site_code = itmSite.getProperty("in_code", "0");
                    string site_val = lnkCheckIn.val + "-site-" + site_code.PadLeft(2, '0');
                    string site_lbl = itmSite.getProperty("in_name", "");

                    var lnkSite = new TLink { val = site_val, lbl = site_lbl, sort = site_code};

                    CheckinSiteFunc(cfg, itmCheckIn, itmSite, lnkSite);
                }

                cfg.site_count = site_count;
            }
            else
            {
                cfg.site_count = 0;
            }

            //最新成績(戰情中心)
            var lnkRtmScore = new TLink { val = "rtm-score", lbl = "最新成績(戰情中心)", sort = "2"};
            Item itmRtmScore = GetMShortUrl(cfg, cfg.meeting_id, lnkRtmScore);
            if (itmRtmScore.isError() || itmRtmScore.getResult() == "")
            {
                itmRtmScore = CommonFunc(cfg, itmFunc, lnkRtmScore, RtmScoreUrl);
            }

            //對戰表
            var lnkRtmTree = new TLink { val = "rtm-tree", lbl = "對戰表", sort = "3" };
            Item itmRtmTree = GetMShortUrl(cfg, cfg.meeting_id, lnkRtmTree);
            if (itmRtmTree.isError() || itmRtmTree.getResult() == "")
            {
                itmRtmTree = CommonFunc(cfg, itmFunc, lnkRtmTree, RtmTreeUrl);
            }

            //選手資訊
            var lnkRtmPlayer = new TLink { val = "rtm-player", lbl = "選手資訊", sort = "4" };
            Item itmRtmPlayer = GetMShortUrl(cfg, cfg.meeting_id, lnkRtmPlayer);
            if (itmRtmPlayer.isError() || itmRtmPlayer.getResult() == "")
            {
                itmRtmPlayer = CommonFunc(cfg, itmFunc, lnkRtmPlayer, RtmPlayerUrl);
            }

            //選手場地資訊
            var lnkRtmSite = new TLink { val = "rtm-site", lbl = "選手場地資訊", sort = "5" };
            Item itmRtmSite = GetMShortUrl(cfg, cfg.meeting_id, lnkRtmSite);
            if (itmRtmSite.isError() || itmRtmSite.getResult() == "")
            {
                itmRtmPlayer = CommonFunc(cfg, itmFunc, lnkRtmSite, RtmPlayerSiteUrl);
            }

            //場地分配表
            var lnkAllocate = new TLink { val = "allocate-site", lbl = "場地分配表", sort = "6" };
            Item itmAllocate = GetMShortUrl(cfg, cfg.meeting_id, lnkAllocate);
            if (itmAllocate.isError() || itmAllocate.getResult() == "")
            {
                itmAllocate = CommonFunc(cfg, itmFunc, lnkRtmSite, AllocateSiteUrl);
            }

            return itmR;
        }

        private Item GetMShortUrl(TConfig cfg, string meeting_id, TLink link)
        {
            return cfg.inn.applySQL("SELECT TOP 1 * FROM IN_MEETING_SHORTURL WITH(NOLOCK)"
                + " WHERE ISNULL(in_meeting, '') = '" + meeting_id + "'"
                + " AND in_value = '" + link.val + "'");
        }

        //首頁
        private Item IndexPage(TConfig cfg, TLink link)
        {
            string in_api_srcurl = cfg.iplm_url + "/pages/b.aspx"
                + "?page=" + link.page
                + "&method=in_qrcode_query"
                + "&meeting_id="
                + "&func_id=" + link.val;

            var api_entity = GetUrlEntity(cfg, in_api_srcurl);

            string in_mvc_srcurl = GetMvcUrl(cfg, api_entity.ShortUrl);
            var mvc_entity = GetUrlEntity(cfg, in_mvc_srcurl);

            Item item = cfg.inn.newItem("IN_MEETING_SHORTURL", "merge");
            item.setAttribute("where", "in_value = '" + link.val + "'");

            SetProperties(item, link.val, link.lbl, link.sort, api_entity, mvc_entity);

            item = item.apply();

            if (item.isError() || item.getResult() == "")
            {
                throw new Exception("競賽連結中心-首頁: 建立失敗");
            }

            return item;
        }

        //功能頁
        private Item CommonPage(TConfig cfg, Item itmParent, TLink link)
        {
            string parent_id = itmParent.getProperty("id", "");

            string in_api_srcurl = cfg.iplm_url + "/pages/b.aspx"
                + "?page=" + link.page
                + "&method=in_qrcode_query"
                + "&meeting_id=" + cfg.meeting_id
                + "&func_id=" + link.val;

            var api_entity = GetUrlEntity(cfg, in_api_srcurl);

            string in_mvc_srcurl = GetMvcUrl(cfg, api_entity.ShortUrl);
            var mvc_entity = GetUrlEntity(cfg, in_mvc_srcurl);

            Item item = cfg.inn.newItem("IN_MEETING_SHORTURL", "merge");
            item.setAttribute("where", "in_meeting = '" + cfg.meeting_id + "' AND in_value = '" + link.val + "'");
            item.setProperty("in_meeting", cfg.meeting_id);
            item.setProperty("in_parent", parent_id);

            SetProperties(item, link.val, link.lbl, link.sort, api_entity, mvc_entity);

            item = item.apply();

            if (item.isError() || item.getResult() == "")
            {
                throw new Exception("競賽連結中心-" + link.lbl + ": 建立失敗");
            }

            return item;
        }

        //檢錄頁-場地
        private Item CheckinSiteFunc(TConfig cfg, Item itmParent, Item itmSite, TLink link)
        {
            string parent_id = itmParent.getProperty("id", "");

            string in_api_srcurl = cfg.iplm_url + "/pages/b.aspx"
                + "?page=PublicNextSearch.html"
                + "&method=in_meeting_program_score_s"
                + "&meeting_id=" + cfg.meeting_id
                + "&site_code=" + link.sort
                + "&scene=next"
                + "&open=1"
                + "&auto=1";

            var api_entity = GetUrlEntity(cfg, in_api_srcurl);

            string in_mvc_srcurl = GetMvcUrl(cfg, api_entity.ShortUrl);
            var mvc_entity = GetUrlEntity(cfg, in_mvc_srcurl);

            Item item = cfg.inn.newItem("IN_MEETING_SHORTURL", "merge");
            item.setAttribute("where", "in_meeting = '" + cfg.meeting_id + "' AND in_value = '" + link.val + "'");
            item.setProperty("in_meeting", cfg.meeting_id);
            item.setProperty("in_parent", parent_id);

            SetProperties(item, link.val, link.lbl, link.sort, api_entity, mvc_entity);

            item = item.apply();

            if (item.isError() || item.getResult() == "")
            {
                throw new Exception("競賽連結中心-" + link.lbl + ": 建立失敗");
            }

            return item;
        }

        //通用 Func
        private Item CommonFunc(TConfig cfg, Item itmParent, TLink link, Func<TConfig, string> getUrl)
        {
            string parent_id = itmParent.getProperty("id", "");

            string in_api_srcurl = getUrl(cfg);

            var api_entity = GetUrlEntity(cfg, in_api_srcurl);

            string in_mvc_srcurl = GetMvcUrl(cfg, api_entity.ShortUrl);
            var mvc_entity = GetUrlEntity(cfg, in_mvc_srcurl);

            Item item = cfg.inn.newItem("IN_MEETING_SHORTURL", "merge");
            item.setAttribute("where", "in_meeting = '" + cfg.meeting_id + "' AND in_value = '" + link.val + "'");
            item.setProperty("in_meeting", cfg.meeting_id);
            item.setProperty("in_parent", parent_id);

            SetProperties(item, link.val, link.lbl, link.sort, api_entity, mvc_entity);

            item = item.apply();

            if (item.isError() || item.getResult() == "")
            {
                throw new Exception("競賽連結中心-" + link.lbl + ": 建立失敗");
            }

            return item;
        }

        //最新成績(戰情中心)
        private string RtmScoreUrl(TConfig cfg)
        {
            string in_api_srcurl = cfg.iplm_url + "/pages/b.aspx"
                + "?page=PublicScoreSearch.html"
                + "&method=in_meeting_program_score_s"
                + "&meeting_id=" + cfg.meeting_id
                + "&&open=1"
                + "";

            return in_api_srcurl;

            //string in_api_srcurl = cfg.iplm_url + "/pages/b.aspx"
            //    + "?page=in_gameboard_view.html"
            //    + "&method=in_gameboard_view"
            //    + "&meeting_id=" + cfg.meeting_id
            //    + "&scene=rtm2"
            //    + "&delay=1500"
            //    + "&count=" + cfg.site_count
            //    + "";

            //return in_api_srcurl;
        }

        //對戰表
        private string RtmTreeUrl(TConfig cfg)
        {
            string in_api_srcurl = cfg.iplm_url + "/pages/b.aspx"
                + "?page=PublicCompetition.html"
                + "&method=in_meeting_program_preview"
                + "&meeting_id=" + cfg.meeting_id
                + "";

            return in_api_srcurl;
        }

        //選手資訊
        private string RtmPlayerUrl(TConfig cfg)
        {
            string in_api_srcurl = cfg.iplm_url + "/pages/b.aspx"
                + "?page=PublicPlayer.html"
                + "&method=in_player_info"
                + "&meeting_id=" + cfg.meeting_id
                + "";

            return in_api_srcurl;
        }

        //選手場地資訊
        private string RtmPlayerSiteUrl(TConfig cfg)
        {
            string in_api_srcurl = cfg.iplm_url + "/pages/b.aspx"
                + "?page=PublicPlayerSite.html"
                + "&method=in_player_site"
                + "&meeting_id=" + cfg.meeting_id
                + "";

            return in_api_srcurl;
        }

        //場地分配表
        private string AllocateSiteUrl(TConfig cfg)
        {
            string in_api_srcurl = cfg.iplm_url + "/pages/b.aspx"
                + "?page=In_MeetingSite_Show.html"
                + "&method=In_meeting_allocation"
                + "&meeting_id=" + cfg.meeting_id
                + "&menu=" + "no"
                + "";

            return in_api_srcurl;
        }

        private void SetProperties(Item item, string in_value, string in_label, string in_sort, TUrl api_entity, TUrl mvc_entity)
        {
            item.setProperty("in_value", in_value);
            item.setProperty("in_label", in_label);
            item.setProperty("in_sort_order", in_sort);
            item.setProperty("in_is_closed", "0");

            item.setProperty("in_api_srcurl", api_entity.SrcUrl);
            item.setProperty("in_api_shorturl", api_entity.ShortUrl);
            item.setProperty("in_api_token", api_entity.Token);
            item.setProperty("in_api_qrcode", api_entity.QrCodeUrl);

            item.setProperty("in_mvc_srcurl", mvc_entity.SrcUrl);
            item.setProperty("in_mvc_shorturl", mvc_entity.ShortUrl);
            item.setProperty("in_mvc_token", mvc_entity.Token);
            item.setProperty("in_mvc_qrcode", mvc_entity.QrCodeUrl);
        }

        private TUrl GetUrlEntity(TConfig cfg, string src_url)
        {
            TUrl result = new TUrl
            {
                SrcUrl = src_url,
                ShortUrl = "",
                Token = "",
                QrCodeFolder = "",
                QrCodeUrl = "",
            };

            result.ShortUrl = GetApiShortUrl(cfg, result.SrcUrl);

            if (result.ShortUrl == "")
            {
                throw new Exception("建立短網址發生錯誤 _# " + result.SrcUrl);
            }
            else
            {
                result.Token = result.ShortUrl.Split('/').Last();

                string[] qrcodes = GenerateQrCode(cfg, result.Token, result.ShortUrl);
                result.QrCodeFolder = qrcodes[0];
                result.QrCodeUrl = qrcodes[1];
            }

            return result;
        }

        private TUrl GetMVCUrlEntity(TConfig cfg, string src_url)
        {
            TUrl result = new TUrl
            {
                SrcUrl = src_url,
                ShortUrl = "",
                Token = "",
                QrCodeFolder = "",
                QrCodeUrl = "",
            };

            result.ShortUrl = GetApiShortUrl(cfg, result.SrcUrl);

            if (result.ShortUrl == "")
            {
                throw new Exception("建立短網址發生錯誤 _# " + result.SrcUrl);
            }
            else
            {
                result.Token = result.ShortUrl.Split('/').Last();

                string[] qrcodes = GenerateQrCode(cfg, result.Token, result.ShortUrl);
                result.QrCodeFolder = qrcodes[0];
                result.QrCodeUrl = qrcodes[1];
            }

            return result;
        }


        private string GetPlmMtUrl(TConfig cfg, string page, string func_id)
        {
            return cfg.iplm_url + "/pages/b.aspx"
                + "?page=" + page
                + "&method=in_qrcode_query"
                + "&meeting_id=" + cfg.meeting_id
                + "&func_id=" + func_id
                + "";
        }

        private string GetMvcUrl(TConfig cfg, string in_api_shorturl)
        {
            return cfg.mvc_url + "/Index/Static_HTML_Read"
                + "?url=" + in_api_shorturl;
        }

        //生成短網址
        private string GetApiShortUrl(TConfig cfg, string srcUrl)
        {
            string result = "";

            try
            {
                string tgturl = System.Web.HttpUtility.UrlEncode(srcUrl);
                string fullurl = cfg.api_url + "/create?url=" + tgturl;
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, fullurl);

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(fullurl);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                StreamReader tReader = new StreamReader(response.GetResponseStream());

                string rst = tReader.ReadToEnd();
                if (!string.IsNullOrWhiteSpace(rst))
                {
                    result = rst.Replace("\"", "");
                }
            }
            catch (Exception ex)
            {
                result = "";
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, ex.Message);
            }

            return result;
        }

        //生成 QrCode
        private string[] GenerateQrCode(TConfig cfg, string name, string contents)
        {
            try
            {
                string folder = cfg.iplm_qrcode_folder;
                if (!System.IO.Directory.Exists(folder))
                {
                    System.IO.Directory.CreateDirectory(folder);
                }

                string file = name + ".jpg";
                string img_file = System.IO.Path.Combine(folder, file);
                string img_url = cfg.iplm_qrcode_url + "/" + file;

                if (!System.IO.File.Exists(img_file))
                {
                    var writer = new ZXing.BarcodeWriter  //dll裡面可以看到屬性
                    {
                        Format = ZXing.BarcodeFormat.QR_CODE,
                        Options = new ZXing.QrCode.QrCodeEncodingOptions //設定大小
                        {
                            Height = 100,
                            Width = 100,
                            Margin = 0,
                            //CharacterSet = "UTF-8",
                            //ErrorCorrection = ZXing.QrCode.Internal.ErrorCorrectionLevel.M
                        },
                    };

                    var img = writer.Write(contents);
                    var myBitmap = new System.Drawing.Bitmap(img);

                    myBitmap.Save(img_file, System.Drawing.Imaging.ImageFormat.Jpeg);
                }

                return new string[] { img_file, img_url };
            }
            catch (Exception ex)
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, ex.Message);
                return new string[] { "", "" };
            }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public Innosoft.InnovatorHelper InnH { get; set; }

            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }

            public string meeting_id { get; set; }
            public string mt_title { get; set; }

            public string iplm_url { get; set; }

            public string iplm_qrcode_folder { get; set; }
            public string iplm_qrcode_url { get; set; }

            public string api_url { get; set; }
            public string mvc_url { get; set; }

            public int site_count { get; set; }
        }

        private class TLink
        {
            public string val { get; set; }
            public string lbl { get; set; }
            public string sort { get; set; }
            public string page { get; set; }
        }

        private class TUrl
        {
            public string SrcUrl { get; set; }
            public string ShortUrl { get; set; }
            public string Token { get; set; }
            public string QrCodeFolder { get; set; }
            public string QrCodeUrl { get; set; }
        }
    }
}