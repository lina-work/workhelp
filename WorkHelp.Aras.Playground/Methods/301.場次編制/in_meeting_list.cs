﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class in_meeting_list : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 
                    下拉選單設定
                日期: 
                    2022-01-04: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]in_meeting_list";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                InnH = _InnH,

                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),

                name = itmR.getProperty("name", ""),
                scene = itmR.getProperty("scene", ""),
            };

            switch (cfg.scene)
            {
                case "update": //更新
                    Update(cfg, itmR);
                    break;

                default: //頁面
                    Page(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void Update(TConfig cfg, Item itmReturn)
        {
            string id = itmReturn.getProperty("id", "");
            string property = itmReturn.getProperty("property", "");
            string value = itmReturn.getProperty("value", "");

            string sql = "";
            if (property.ToUpper() == "SORT_ORDER")
            {
                sql = "UPDATE [VALUE] SET [" + property + "] = '" + value + "' WHERE id = '" + id + "'";
            }
            else
            {
                sql = "UPDATE [VALUE] SET [" + property + "] = N'" + value + "' WHERE id = '" + id + "'";
            }

            Item itmUpd = cfg.inn.applySQL(sql);

            if (itmUpd.isError())
            {
                throw new Exception("更新失敗");
            }
        }

        private void Page(TConfig cfg, Item itmReturn)
        {
            string sql = @"
                SELECT 
	                t1.description
	                , t2.value
	                , t2.label_zt
	                , t2.sort_order
                FROM 
	                [LIST] t1 WITH(NOLOCK)
                INNER JOIN
	                [VALUE] t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                WHERE
	                t1.name = '{#name}'
                ORDER BY
	                t2.sort_order
            ";

            sql = sql.Replace("{#name}", cfg.name);

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();

            string page_title = cfg.name;

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                item.setType("inn_value");
                itmReturn.addRelationship(item);

                if (i == 0)
                {
                    page_title = item.getProperty("description", "");
                }
            }

            itmReturn.setProperty("page_title", page_title);
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public Innosoft.InnovatorHelper InnH { get; set; }

            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }

            public string name { get; set; }
            public string scene { get; set; }
        }

        private int GetIntVal(string value)
        {
            if (value == "" || value == "0") return 0;

            int result = 0;
            int.TryParse(value, out result);
            return result;
        }
    }
}