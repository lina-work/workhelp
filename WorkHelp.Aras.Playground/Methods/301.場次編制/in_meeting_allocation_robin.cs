﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class in_meeting_allocation_robin : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 場次編制(循環賽)
                輸入: meeting_id
                日期: 
                    2022-01-03: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]in_meeting_allocation_robin";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "input dom: " + itmR.dom.InnerXml);

            string meeting_id = itmR.getProperty("meeting_id", "");
            string in_date = itmR.getProperty("in_date", "");
            string in_sort = itmR.getProperty("in_sort", "");

            if (meeting_id == "") throw new Exception("賽事 id 不得為空白");
            if (in_date == "") throw new Exception("比賽日期 不得為空白");
            if (in_sort == "") throw new Exception("排列方式 不得為空白");

            //檢查頁面權限
            Item itmPermit = inn.applyMethod("In_CheckIdentity", "<method>" + strMethodName + "</method><code>" + "ALL" + "</code>");
            bool isMeetingAdmin = itmPermit.getProperty("isMeetingAdmin", "") == "1";
            bool isMeetingLeader = itmPermit.getProperty("isMeetingLeader", "") == "1";

            bool can_view = isMeetingAdmin || isMeetingLeader;
            if (!can_view)
            {
                itmR.setProperty("error_message", "您無權限瀏覽此頁面");
                return itmR;
            }

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                strMethodName = strMethodName,
                inn = inn,
                meeting_id = meeting_id,
                in_date = in_date,
                in_sort = in_sort,
            };

            //取得賽事資料
            cfg.itmMeeting = inn.applySQL("SELECT * FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");

            //取得場次排序設定
            cfg.SortMap = MapEventSort(cfg);

            //場次編制
            RefreshTreeNo(cfg);

            return itmR;
        }

        //編制
        private void RefreshTreeNo(TConfig cfg)
        {
            string sql_update = "";

            //紀錄 Meeting 排序方式
            sql_update = "UPDATE IN_MEETING SET in_site_mode = '" + cfg.in_sort + "' WHERE id = '" + cfg.meeting_id + "'";
            cfg.inn.applySQL(sql_update);

            //更新組別 - 有效參賽者人數
            FixProgramRankCount(cfg);

            //更新組別-場地場次序
            FixProgramMatNo(cfg);

            //重設場次編號
            Item itmData = cfg.inn.newItem("In_Meeting_Program");
            itmData.setProperty("meeting_id", cfg.meeting_id);
            itmData.setProperty("program_id", "");
            itmData.setProperty("in_date", cfg.in_date);
            itmData.apply("in_meeting_pevent_reset");

            //紀錄初始 TreeNo
            sql_update = "UPDATE IN_MEETING_PEVENT SET in_tree_sno = in_tree_no"
            + " WHERE source_id IN (SELECT id FROM IN_MEETING_PROGRAM WHERE in_meeting = '" + cfg.meeting_id + "')"
            + " AND ISNULL(in_tree_sno, '') = ''";
            cfg.inn.applySQL(sql_update);

            var itmEvents = GetEvents(cfg);

            var sites = MapSites(cfg, itmEvents);
            foreach (var site in sites)
            {
                ResetTreeNo(cfg, site);
            }
        }

        private void ResetTreeNo(TConfig cfg, TSite site)
        {
            int tree_no = 1;
            var sorted_grps = site.GroupList.OrderBy(x => x.Weight).ToList();

            foreach (var grp in sorted_grps)
            {
                if (grp.Weight == 0) continue;

                foreach (var program in site.ProgramList)
                {
                    var pg_grp = program.GroupList.Find(x => x.Weight == grp.Weight);
                    if (pg_grp == null || pg_grp.EventList == null || pg_grp.EventList.Count == 0)
                    {
                        continue;
                    }

                    var evts = pg_grp.EventList.OrderBy(x => x.tree_sno).ToList();
                    for (int i = 0; i < evts.Count; i++)
                    {
                        var evt = evts[i];
                        evt.new_tree_no = tree_no;
                        tree_no++;
                    }
                }
            }

            foreach (var evt in site.EventList)
            {
                var new_tree_no = evt.new_tree_no <= 0
                    ? "NULL"
                    : "'" + evt.new_tree_no + "'";

                string sql = "UPDATE IN_MEETING_PEVENT SET"
                    + " in_site_allocate = '1'"
                    + " , in_tree_no = " + new_tree_no
                    + " , in_tree_state = '1'"
                    + " WHERE id = '" + evt.id + "'";

                Item itmSQL = cfg.inn.applySQL(sql);

                if (itmSQL.isError()) throw new Exception("error");
            }
        }

        #region Program

        //更新組別 - 有效參賽者人數
        private void FixProgramRankCount(TConfig cfg)
        {
            string sql = @"
                UPDATE t1 SET
	                t1.in_rank_count = t2.in_rank_count
                FROM
	                IN_MEETING_PROGRAM t1
                INNER JOIN
                (
	                SELECT
		                source_id
		                , count(id) AS 'in_rank_count' 
	                FROM
		                IN_MEETING_PTEAM WITH(NOLOCK)
	                WHERe
		                in_meeting = '{#meeting_id}'
		                AND ISNULL(in_weight_message, '') = ''
		                AND ISNULL(in_not_draw, 0) = 0
	                GROUP BY
		                source_id
                ) t2 ON t2.source_id = t1.id
                WHERE
	                t1.in_meeting = '{#meeting_id}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item itmSQL = cfg.inn.applySQL(sql);
        }

        //更新組別-場地場次序
        private void FixProgramMatNo(TConfig cfg)
        {
            string sql = @"
                UPDATE t1 SET
                	t1.in_site_mat = 'MAT ' + CAST(in_code AS varchar) + '-' + CAST(rn AS VARCHAR)
                	, t1.in_site_mat2 = 'MAT ' + CAST(in_code AS varchar) + '-' + RIGHT(REPLICATE('0', 2) + CAST(rn as VARCHAR), 2)
                FROM
                	IN_MEETING_PROGRAM t1
                INNER JOIN
                (
                	SELECT
                		t11.in_program
                		, t12.in_code
                		, ROW_NUMBER() OVER (PARTITION BY t12.in_code ORDER BY t11.created_on) AS 'rn'
                	FROM
                		IN_MEETING_ALLOCATION t11 WITH(NOLOCK)
                	INNER JOIN
                		IN_MEETING_SITE t12 WITH(NOLOCK)
                		ON t12.id = t11.in_site
                	WHERE
                		t11.in_meeting = '{#meeting_id}'
                		AND t11.in_date_key = '{#in_date_key}'
                ) t2 ON t2.in_program = t1.id
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_date_key}", cfg.in_date);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item itmUpd = cfg.inn.applySQL(sql);



            sql = @"UPDATE t1 SET 
            	t1.in_fight_day = t2.in_date_key
            	, t1.in_site = t2.in_site
            FROM
            	IN_MEETING_PROGRAM t1 WITH(NOLOCK)
            INNER JOIN
            	IN_MEETING_ALLOCATION t2 WITH(NOLOCK)
            	ON t2.in_program = t1.id
            WHERE
            	t2.in_meeting = '{#meeting_id}'
            	AND t2.in_date_key = '{#in_date_key}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_date_key}", cfg.in_date);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            itmUpd = cfg.inn.applySQL(sql);

        }

        #endregion Program

        private Item GetEvents(TConfig cfg)
        {
            string sql = @"
                SELECT 
	                t3.in_code			  AS 'site_code'
	                , t1.in_site_mat      AS 'pg_site_mat'
	                , t1.in_name          AS 'pg_name'
	                , t1.in_battle_type   AS 'pg_battle_type'
	                , t1.in_team_count    AS 'pg_team_count'
                    , t1.in_rank_count    AS 'pg_rank_count'
	                , t1.in_round_code    AS 'pg_round_code'
	                , t1.in_round_count   AS 'pg_round_count'
	                , t2.id               AS 'event_id'
	                , t2.in_tree_name
	                , t2.in_tree_id
	                , t2.in_tree_no
	                , t2.in_tree_sno
	                , t2.in_bypass_status
	                , t2.in_round
	                , t2.in_round_code
                FROM
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PEVENT t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                INNER JOIN
	                IN_MEETING_SITE t3 WITH(NOLOCK)
	                ON t3.id = t1.in_site
                WHERE
	                t1.in_meeting = '{#meeting_id}'
	                AND t1.in_fight_day = '{#in_fight_day}'
                ORDER BY
	                t1.in_site_mat2
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_fight_day}", cfg.in_date);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public Innovator inn { get; set; }

            public string meeting_id { get; set; }
            public string in_date { get; set; }
            public string in_sort { get; set; }

            public Item itmMeeting { get; set; }
            public string mt_title { get; set; }

            public Dictionary<EventEnum, int> SortMap { get; set; }
        }

        private class TSite
        {
            public int site_code { get; set; }

            public List<TProgram> ProgramList { get; set; }

            public List<TGroup> GroupList { get; set; }

            public List<TEvent> EventList { get; set; }
        }

        private class TProgram
        {
            public string in_site_mat { get; set; }
            public string in_name { get; set; }
            public string in_battle_type { get; set; }

            public int team_count { get; set; }
            public int rank_count { get; set; }
            public int round_code { get; set; }
            public int round_count { get; set; }

            public TBattleMap BattleMap { get; set; }

            public List<TGroup> GroupList { get; set; }
        }

        private class TGroup
        {
            /// <summary>
            /// 權重
            /// </summary>
            public int Weight { get; set; }

            /// <summary>
            /// 場次集
            /// </summary>
            public List<TEvent> EventList { get; set; }
        }

        private class TEvent
        {
            public string id { get; set; }
            public string in_tree_name { get; set; }
            public string in_tree_id { get; set; }
            public string in_tree_no { get; set; }
            public string in_bypass_status { get; set; }

            public int round { get; set; }
            public int round_code { get; set; }
            public int tree_sno { get; set; }
            public int new_tree_no { get; set; }

            public TreeEnum TreeType { get; set; }

            public TreeMap TreeMap { get; set; }

            public int Weight { get; set; }
            public bool has_assigned { get; set; }
        }

        private class TreeMap
        {
            public int Weight { get; set; }
            public EventEnum EventType { get; set; }
        }

        /// <summary>
        /// 取得賽制類型
        /// </summary>
        private TreeEnum GetTreeType(string value)
        {
            switch (value)
            {
                case "main": return TreeEnum.Main;
                case "rank34": return TreeEnum.rank34;
                case "rank56": return TreeEnum.rank56;
                case "rank78": return TreeEnum.rank78;
                case "repechage": return TreeEnum.Repechage;
                case "challenge-a": return TreeEnum.ChallengeA;
                case "challenge-b": return TreeEnum.ChallengeB;
                case "sub": return TreeEnum.Sub;
                case "ka01": return TreeEnum.KA;
                case "kb01": return TreeEnum.KB;
                default: return TreeEnum.None;
            }
        }

        /// <summary>
        /// 取得賽制類型
        /// </summary>
        private TBattleMap GetBattleMap(string value, int tcount)
        {
            bool isTwoFight = tcount == 2;

            switch (value)
            {
                case "Format": return new TBattleMap { type = BattleEnum.Format };

                case "TopTwo": return new TBattleMap { type = BattleEnum.TopTwo, isTopTwo = true };
                case "TopFour": return new TBattleMap { type = BattleEnum.TopFour };

                case "JudoTopFour": return new TBattleMap { type = BattleEnum.JudoTopFour, isRepechage = true };
                case "Challenge": return new TBattleMap { type = BattleEnum.Challenge, isChallenge = true };

                case "SingleRoundRobin": return new TBattleMap { type = BattleEnum.SingleRoundRobin, isRobin = true, isTwoFight = isTwoFight };
                case "DoubleRoundRobin": return new TBattleMap { type = BattleEnum.DoubleRoundRobin, isRobin = true, isTwoFight = isTwoFight };
                case "GroupSRoundRobin": return new TBattleMap { type = BattleEnum.GroupSRoundRobin, isRobin = true, isTwoFight = isTwoFight };
                case "GroupDRoundRobin": return new TBattleMap { type = BattleEnum.GroupDRoundRobin, isRobin = true, isTwoFight = isTwoFight };

                case "GroupTopTwo": return new TBattleMap { type = BattleEnum.GroupTopTwo };
                case "Olympics": return new TBattleMap { type = BattleEnum.Olympics };
                default: return new TBattleMap { type = BattleEnum.None };
            }
        }

        private class TBattleMap
        {
            /// <summary>
            /// 賽制類型
            /// </summary>
            public BattleEnum type { get; set; }

            /// <summary>
            /// 是否為四柱復活賽
            /// </summary>
            public bool isRepechage { get; set; }

            /// <summary>
            /// 是否為循環賽
            /// </summary>
            public bool isRobin { get; set; }

            /// <summary>
            /// 是否為單淘
            /// </summary>
            public bool isTopTwo { get; set; }

            /// <summary>
            /// 是否為挑戰賽
            /// </summary>
            public bool isChallenge { get; set; }

            /// <summary>
            /// 是否八強賽(四柱復活挑戰賽-八強進復活)
            /// </summary>
            public bool isEight { get; set; }

            /// <summary>
            ///三戰兩勝
            /// </summary>
            public bool isTwoFight { get; set; }
        }

        /// <summary>
        /// 賽制類型
        /// </summary>
        private enum BattleEnum
        {
            None = 0,
            Format = 100,
            TopTwo = 1100,
            TopFour = 1200,
            JudoTopFour = 2100,
            Challenge = 2200,
            SingleRoundRobin = 3100,
            DoubleRoundRobin = 3200,
            GroupSRoundRobin = 4100,
            GroupDRoundRobin = 4200,
            GroupTopTwo = 4300,
            Olympics = 900,
        }

        /// <summary>
        /// 樹圖類型
        /// </summary>
        private enum TreeEnum
        {
            None = 0,
            Main = 100,
            rank34 = 300,
            rank56 = 500,
            rank78 = 700,
            Repechage = 1100,
            ChallengeA = 2100,
            ChallengeB = 2200,
            Sub = 5000,
            KA = 7100,
            KB = 7200,
        }

        private List<TSite> MapSites(TConfig cfg, Item items)
        {
            List<TSite> result = new List<TSite>();

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                int site_code = GetIntVal(item.getProperty("site_code", "0"));

                string in_site_mat = item.getProperty("pg_site_mat", "");

                var site = result.Find(x => x.site_code == site_code);
                if (site == null)
                {
                    site = new TSite
                    {
                        site_code = site_code,
                        ProgramList = new List<TProgram>(),
                        GroupList = new List<TGroup>(),
                        EventList = new List<TEvent>(),
                    };

                    result.Add(site);
                }

                var program = site.ProgramList.Find(x => x.in_site_mat == in_site_mat);
                if (program == null)
                {
                    program = new TProgram
                    {
                        in_site_mat = in_site_mat,
                        in_name = item.getProperty("pg_name", ""),
                        in_battle_type = item.getProperty("pg_battle_type", ""),
                        team_count = GetIntVal(item.getProperty("pg_team_count", "0")),
                        rank_count = GetIntVal(item.getProperty("pg_rank_count", "0")),
                        round_code = GetIntVal(item.getProperty("pg_round_code", "0")),
                        round_count = GetIntVal(item.getProperty("pg_round_count", "0")),
                        GroupList = new List<TGroup>(),
                    };

                    program.BattleMap = GetBattleMap(program.in_battle_type, program.team_count);

                    site.ProgramList.Add(program);
                }

                var evt = MapEvent(cfg, program, item);

                var group = program.GroupList.Find(x => x.Weight == evt.Weight);
                if (group == null)
                {
                    group = new TGroup
                    {
                        Weight = evt.Weight,
                        EventList = new List<TEvent>(),
                    };
                    program.GroupList.Add(group);
                }

                group.EventList.Add(evt);

                var sgrp = site.GroupList.Find(x => x.Weight == evt.Weight);
                if (sgrp == null)
                {
                    sgrp = new TGroup { Weight = evt.Weight };
                    site.GroupList.Add(sgrp);
                }

                site.EventList.Add(evt);

            }

            return result;
        }

        private TEvent MapEvent(TConfig cfg, TProgram program, Item item)
        {
            TEvent evt = new TEvent
            {
                id = item.getProperty("event_id", ""),
                in_tree_name = item.getProperty("in_tree_name", ""),
                in_tree_id = item.getProperty("in_tree_id", ""),
                in_tree_no = item.getProperty("in_tree_no", ""),
                in_bypass_status = item.getProperty("in_bypass_status", ""),

                round = GetIntVal(item.getProperty("in_round", "0")),
                round_code = GetIntVal(item.getProperty("in_round_code", "0")),
                tree_sno = GetIntVal(item.getProperty("in_tree_sno", "0")),
            };

            evt.TreeType = GetTreeType(evt.in_tree_name);

            evt.TreeMap = GetTreeMap(cfg, program, evt);
            evt.Weight = evt.TreeMap.Weight;

            return evt;
        }

        private TreeMap GetTreeMap(TConfig cfg, TProgram program, TEvent evt)
        {
            EventEnum result = EventEnum.NONE;

            var is_robin = program.BattleMap.isRobin;
            var is_topTwo = program.BattleMap.isTopTwo;
            var is_repechage = program.BattleMap.isRepechage;
            var is_challenge = program.BattleMap.isChallenge;

            if (is_robin)
            {
                result = RobinWeight(program, evt);
            }
            else
            {
                switch (evt.TreeType)
                {
                    case TreeEnum.Main:
                        result = MainWeight(program, evt);
                        break;

                    case TreeEnum.Repechage:
                        if (is_repechage || is_challenge)
                        {
                            result = RpcWeight(program, evt);
                        }
                        break;

                    case TreeEnum.ChallengeA:
                    case TreeEnum.ChallengeB:
                        if (is_repechage || is_challenge)
                        {
                            result = ChallengeWeight(program, evt);
                        }
                        break;

                    case TreeEnum.KA:
                    case TreeEnum.KB:
                        result = KingWeight(program, evt);
                        break;

                    case TreeEnum.rank34:
                        if (is_challenge)
                        {
                            result = EventEnum.RNK_34;
                        }
                        break;

                    case TreeEnum.rank56:
                        if (is_topTwo)
                        {

                        }
                        else if (program.rank_count == 7)
                        {
                            result = EventEnum.RNK_56;
                        }
                        else
                        {
                            result = 0;
                        }

                        break;

                    case TreeEnum.rank78:
                        if (is_topTwo)
                        {

                        }
                        else if (program.rank_count == 9)
                        {
                            result = EventEnum.RNK_78;
                        }
                        else
                        {
                            result = 0;
                        }
                        break;

                    default:
                        result = 0;
                        break;
                }
            }

            if (cfg.SortMap != null && result != EventEnum.NONE && cfg.SortMap.ContainsKey(result))
            {
                return new TreeMap { EventType = result, Weight = cfg.SortMap[result] };
            }
            else
            {
                return new TreeMap { EventType = EventEnum.NONE, Weight = 0 };
            }
        }

        #region Main
        private EventEnum MainWeight(TProgram program, TEvent evt)
        {
            if (evt.tree_sno <= 0) return EventEnum.NONE;

            if (evt.round_code == 128) return EventEnum.MAIN_128;
            if (evt.round_code == 64) return EventEnum.MAIN_064;
            if (evt.round_code == 32) return EventEnum.MAIN_032;
            if (evt.round_code == 16) return EventEnum.MAIN_016;

            //八強
            if (evt.round_code == 8) return EventEnum.MAIN_008;

            if (evt.round_code == 4)
            {
                if (program.team_count == 4)
                {
                    //只有四人 比循環賽第1場 早
                    return EventEnum.MAIN_4_004;
                }
                else
                {
                    //四強 比循環賽第1場 慢
                    return EventEnum.MAIN_N_004;
                }
            }

            //決賽
            if (evt.round_code == 2) return EventEnum.MAIN_002;

            return EventEnum.NONE;
        }

        private EventEnum RpcWeight(TProgram program, TEvent evt)
        {
            if (evt.tree_sno <= 0) return 0;

            if (evt.round_code == 128) return EventEnum.RPC_128;
            if (evt.round_code == 64) return EventEnum.RPC_064;
            if (evt.round_code == 32) return EventEnum.RPC_032;
            if (evt.round_code == 16) return EventEnum.RPC_016;

            //八強 比循環賽第2場 慢
            if (evt.round_code == 8) return EventEnum.RPC_008;

            //銅牌 比循環賽第34場 慢
            if (evt.round_code == 4) return EventEnum.RPC_004;

            //敗部 34 名
            if (evt.round_code == 2)
            {
                if (program.team_count == 4)
                {
                    return EventEnum.RPC_4_002;
                }
                else
                {
                    return EventEnum.RPC_002;
                }
            }
            return EventEnum.NONE;
        }

        private EventEnum ChallengeWeight(TProgram program, TEvent evt)
        {
            if (evt.in_tree_id == "CA01") return EventEnum.CA01;
            if (evt.in_tree_id == "CA02") return EventEnum.CA02;
            if (evt.in_tree_id == "CB01") return EventEnum.CB01;
            return 0;
        }

        private EventEnum KingWeight(TProgram program, TEvent evt)
        {
            if (evt.in_tree_id == "KA01") return EventEnum.KA01;
            if (evt.in_tree_id == "KB01") return EventEnum.KB01;
            return 0;
        }

        #endregion Main

        #region RobinWeight
        private EventEnum RobinWeight(TProgram program, TEvent evt)
        {
            if (evt.TreeType == TreeEnum.KA)
            {
                return EventEnum.KA01;
            }
            else if (evt.TreeType == TreeEnum.KB)
            {
                return EventEnum.KB01;
            }
            else
            {
                switch (program.team_count)
                {
                    case 2: return RobinWeight_2(evt.tree_sno);
                    case 3: return RobinWeight_3(evt.tree_sno);
                    case 4: return RobinWeight_4(evt.tree_sno);
                    case 5: return RobinWeight_5(evt.tree_sno);
                    default: return 0;
                }
            }
        }

        private EventEnum RobinWeight_2(int tree_sno)
        {
            switch (tree_sno)
            {
                case 1: return EventEnum.RBN_3_001;
                case 2: return EventEnum.RBN_3_002;
                case 3: return EventEnum.RBN_3_003;
                default: return 0;
            }
        }

        private EventEnum RobinWeight_3(int tree_sno)
        {
            switch (tree_sno)
            {
                case 1: return EventEnum.RBN_3_001;
                case 2: return EventEnum.RBN_3_002;
                case 3: return EventEnum.RBN_3_003;
                default: return 0;
            }
        }

        private EventEnum RobinWeight_4(int tree_sno)
        {
            switch (tree_sno)
            {
                case 1: return EventEnum.RBN_5_001;
                case 2: return EventEnum.RBN_5_001;

                case 3: return EventEnum.RBN_5_002;
                case 4: return EventEnum.RBN_5_002;

                case 5: return EventEnum.RBN_5_003;
                case 6: return EventEnum.RBN_5_003;

                default: return 0;
            }
        }

        private EventEnum RobinWeight_5(int tree_sno)
        {
            switch (tree_sno)
            {
                case 1: return EventEnum.RBN_5_001;
                case 2: return EventEnum.RBN_5_001;

                case 3: return EventEnum.RBN_5_002;
                case 4: return EventEnum.RBN_5_002;

                case 5: return EventEnum.RBN_5_003;
                case 6: return EventEnum.RBN_5_003;

                case 7: return EventEnum.RBN_5_004;
                case 8: return EventEnum.RBN_5_004;

                case 9: return EventEnum.RBN_5_005;
                case 10: return EventEnum.RBN_5_005;
                default: return EventEnum.NONE;
            }
        }

        #endregion RobinWeight

        private Dictionary<EventEnum, int> MapEventSort(TConfig cfg)
        {
            Dictionary<EventEnum, int> map = new Dictionary<EventEnum, int>();

            string sql = @"
                SELECT 
	                t2.value
	                , t2.label_zt
	                , t2.sort_order
                FROM 
	                [LIST] t1 WITH(NOLOCK)
                INNER JOIN
	                [VALUE] t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                WHERE
	                t1.name = 'In_Meeting_EventSort_Robin'
                ORDER BY
	                t2.sort_order
            ";

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string value = item.getProperty("value", "");
                int sort_order = GetIntVal(item.getProperty("sort_order", "0"));

                EventEnum e = EventEnum.NONE;
                switch (value)
                {
                    case "NONE": e = EventEnum.NONE; break;
                    case "MAIN_128": e = EventEnum.MAIN_128; break;
                    case "MAIN_064": e = EventEnum.MAIN_064; break;
                    case "MAIN_032": e = EventEnum.MAIN_032; break;
                    case "MAIN_016": e = EventEnum.MAIN_016; break;
                    case "MAIN_008": e = EventEnum.MAIN_008; break;
                    case "MAIN_4_004": e = EventEnum.MAIN_4_004; break;
                    case "RBN_3_001": e = EventEnum.RBN_3_001; break;
                    case "RBN_5_001": e = EventEnum.RBN_5_001; break;
                    case "MAIN_N_004": e = EventEnum.MAIN_N_004; break;
                    case "RBN_5_002": e = EventEnum.RBN_5_002; break;
                    case "RPC_128": e = EventEnum.RPC_128; break;
                    case "RPC_064": e = EventEnum.RPC_064; break;
                    case "RPC_032": e = EventEnum.RPC_032; break;
                    case "RPC_016": e = EventEnum.RPC_016; break;
                    case "RBN_3_002": e = EventEnum.RBN_3_002; break;
                    case "RBN_5_003": e = EventEnum.RBN_5_003; break;
                    case "RPC_008": e = EventEnum.RPC_008; break;
                    case "RBN_5_004": e = EventEnum.RBN_5_004; break;
                    case "RPC_004": e = EventEnum.RPC_004; break;
                    case "RBN_3_003": e = EventEnum.RBN_3_003; break;
                    case "RBN_5_005": e = EventEnum.RBN_5_005; break;
                    case "RNK_78": e = EventEnum.RNK_78; break;
                    case "RPC_002": e = EventEnum.RPC_002; break;
                    case "RPC_4_002": e = EventEnum.RPC_4_002; break;
                    case "RNK_34": e = EventEnum.RNK_34; break;
                    case "MAIN_002": e = EventEnum.MAIN_002; break;
                    case "RNK_56": e = EventEnum.RNK_56; break;
                    case "CA01": e = EventEnum.CA01; break;
                    case "CA02": e = EventEnum.CA02; break;
                    case "CB01": e = EventEnum.CB01; break;
                    case "KA01": e = EventEnum.KA01; break;
                    case "KB01": e = EventEnum.KB01; break;
                    default: break;
                }

                map.Add(e, sort_order);
            }

            return map;
        }

        private enum EventEnum
        {
            NONE = 0,

            MAIN_128 = 100,
            MAIN_064 = 200,
            MAIN_032 = 300,
            MAIN_016 = 400,
            MAIN_008 = 500,
            MAIN_4_004 = 600,
            MAIN_N_004 = 700,
            MAIN_002 = 800,

            RBN_3_001 = 1100,
            RBN_3_002 = 1200,
            RBN_3_003 = 1300,

            RBN_5_001 = 2100,
            RBN_5_002 = 2200,
            RBN_5_003 = 2300,
            RBN_5_004 = 2400,
            RBN_5_005 = 2500,

            RPC_128 = 3100,
            RPC_064 = 3200,
            RPC_032 = 3300,
            RPC_016 = 3400,
            RPC_008 = 3500,
            RPC_004 = 3600,
            RPC_002 = 3700,
            RPC_4_002 = 3800,

            RNK_34 = 4100,
            RNK_56 = 4200,
            RNK_78 = 4300,

            CA01 = 5100,
            CA02 = 5200,
            CB01 = 5300,

            KA01 = 6100,
            KB01 = 6200,
        }

        private int GetIntVal(string value, int def = 0)
        {
            int result = def;
            Int32.TryParse(value, out result);
            return result;
        }
    }
}