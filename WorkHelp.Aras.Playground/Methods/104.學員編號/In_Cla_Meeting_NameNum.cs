﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;
using System.Net;

namespace WorkHelp.Aras.Playground.Methods.NameNum
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class In_Cla_Meeting_NameNum : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 學員編號 Page
                日期: 2021-10-04 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]In_Cla_Meeting_NameNum";

            string meeting_id = this.getProperty("meeting_id", "");

            Item itmR = inn.newItem("Inn_Result");

            Item itmMeeting = inn.getItemById("In_Cla_Meeting", meeting_id);

            itmR = itmMeeting;
            itmR.setType("Inn_Result");
            itmR.setProperty("inn_item_number", itmMeeting.getProperty("item_number", ""));
            itmR.setProperty("inn_title", itmMeeting.getProperty("in_title", ""));

            string sql = @"
                SELECT 
	                t1.id AS 'muid'
	                , t1.in_name
	                , t1.in_current_org
	                , t1.in_sno_display
	                , t1.in_birth
	                , t1.in_degree
	                , t1.in_degree_id
	                , t1.in_name_num
	                , t1.in_note_state
	                , t1.in_creator
	                , t1.in_creator_sno
                FROM 
	                IN_CLA_MEETING_USER t1 WITH(NOLOCK)
                LEFT OUTER JOIN 
	                IN_CLA_MEETING_RESUME t2 WITH(NOLOCK)
	                ON t2.in_user = t1.id
                WHERE
	                t1.source_id = '{#meeting_id}'
                ORDER BY
                    t1.in_name_num
	                , t1.IN_DEGREE DESC
	                , t1.in_birth
            ";

            sql = sql.Replace("{#meeting_id}", meeting_id);

            Item itmMUsers = inn.applySQL(sql);
            int count = itmMUsers.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item itmMUser = itmMUsers.getItemByIndex(i);
                string in_birth = itmMUser.getProperty("in_birth", "");
                string in_degree = itmMUser.getProperty("in_degree", "");
                string in_note_state = itmMUser.getProperty("in_note_state", "");

                int degree_code = GetInt(in_degree);

                itmMUser.setType("inn_muser");
                itmMUser.setProperty("inn_birth", GetBirth(in_birth));
                itmMUser.setProperty("inn_degree", GetDegreeLabel(degree_code));
                itmMUser.setProperty("inn_note", GetNoteLabel(in_note_state));
                itmR.addRelationship(itmMUser);
            }

            return itmR;

        }

        //生日(外顯)
        private string GetBirth(string value)
        {
            if (value.Contains("1900") || value.Contains("1899"))
            {
                return "";
            }

            return GetDateTimeValue(value, "yyyy年MM月dd日", 8);
        }

        private string GetDegreeLabel(int val)
        {
            switch (val)
            {
                case 1000: return "壹段";
                case 2000: return "貳段";
                case 3000: return "參段";
                case 4000: return "肆段";
                case 5000: return "伍段";
                case 6000: return "陸段";
                case 7000: return "柒段";
                case 8000: return "捌段";
                case 9000: return "玖段";
                default: return "";
            }
        }

        private string GetNoteLabel(string val)
        {
            switch (val)
            {
                case "official": return "正取";
                case "waiting": return "備取";
                default: return val;
            }
        }

        private string GetDateTimeValue(string value, string format, int hours = 0)
        {
            if (value == "") return "";

            DateTime dt = DateTime.Now;
            if (DateTime.TryParse(value, out dt))
            {
                return dt.AddHours(hours).ToString(format);
            }
            else
            {
                return value;
            }
        }

        private int GetInt(string value, int defV = 0)
        {
            int result = defV;
            if (int.TryParse(value, out result))
            {
                return result;
            }
            return defV;
        }
    }
}