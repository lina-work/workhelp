﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class bd_Import_MUser_FromXls : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
            目的: 與會者匯入
            日期: 
                - 2021/03/09: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]Import_MUser_FromXls";

            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            Item itmR = this;
            string meeting_id = "4E1DCD684EF44D4E8706CBCE48987DB4";

            var itmAResume = GetAdminResume(CCO, strMethodName, inn);

            //var musers = GetMUsersFromXLS(CCO, strMethodName, inn);

            //會員轉檔
            //TransMUsers(CCO, strMethodName, inn, itmAResume, musers);

            //建立繳費單
            //TransMPays(CCO, strMethodName, inn, itmAResume, meeting_id);

            //轉講師
            //TransTeachers(CCO, strMethodName, inn, itmAResume, meeting_id);

            //檢查名稱
            CheckNames(CCO, strMethodName, inn);

            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

            return itmR;
        }

        //檢查名稱
        private void CheckNames(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn)
        {
            string sql = "SELECT DISTINCT in_name FROM A_MUser_20210309 WITH(NOLOCK)";

            Item items = inn.applySQL(sql);

            int count = items.getItemCount();

            int no = 1;
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_name = item.getProperty("in_name", "");
                string sql2 = "SELECT * FROM IN_RESUME WITH(NOLOCK) WHERE in_name = N'" + in_name + "'";

                Item itmResumes = inn.applySQL(sql2);

                int resume_count = itmResumes.getItemCount();

                if (resume_count > 1)
                {
                    CCO.Utilities.WriteDebug(strMethodName + "_check", no + ". 姓名重複: " + in_name + " 筆數：" + resume_count);
                    no++;
                }
            }
        }

        //轉講師
        private void TransTeachers(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmAResume, string meeting_id)
        {
            string sql = @"
                SELECT 
	                t1.*
                FROM
	                IN_RESUME t1 WITH(NOLOCK)
                INNER JOIN
                (
	                SELECT DISTINCT in_creator_sno FROM IN_MEETING_USER WITH(NOLOCK) WHERE source_id = '{#meeting_id}'
                ) t2 ON t2.in_creator_sno = t1.in_sno
                WHERE
	                ISNULL(t1.in_is_teacher, '0') = '0'
            ";

            sql = sql.Replace("{#meeting_id}", meeting_id);

            Item itmResumes = inn.applySQL(sql);

            int count = itmResumes.getItemCount();

            for(int i = 0; i < count; i++)
            {
                Item itmResume = itmResumes.getItemByIndex(i);
                string resume_id = itmResume.getProperty("id", "");
                string in_user_id = itmResume.getProperty("in_user_id", "");

                Item itmIdentity = inn.newItem("Identity", "get");
                itmIdentity.setProperty("in_user", in_user_id);
                itmIdentity = itmIdentity.apply();

                string identity_id = itmIdentity.getID();

                //此為申請館主之用,要把報名者加入 7AC8B33DA0F246DDA70BB244AB231E29 ACT_GymOwner
                Item itmMember = inn.newItem("Member", "merge");
                itmMember.setAttribute("where", "source_id='7AC8B33DA0F246DDA70BB244AB231E29' and related_id='" + identity_id + "'");
                itmMember.setProperty("source_id", "7AC8B33DA0F246DDA70BB244AB231E29");
                itmMember.setRelatedItem(itmIdentity);
                itmMember = itmMember.apply();

                //館主的 app start page 應該是開放賽事一覽
                string sql1 = @"UPDATE [User] SET in_app_page='pages/c.aspx?page=In_MeetingSearch_light.html&method=In_Dashboard_MeetingList_light' WHERE id = '" + in_user_id + "'";
                inn.applySQL(sql1);

                //更新講師履歷資料
                string sql2 = "UPDATE IN_RESUME SET in_is_teacher = '1' WHERE id = '" + resume_id + "'";
                inn.applySQL(sql2);
            }
        }

        //建立繳費單
        private void TransMPays(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmAResume, string meeting_id)
        {
            var map = GetMUserGroups(CCO, strMethodName, inn, itmAResume, meeting_id);
            var count = map.Count;
            var no = 0;

            //為了配合 In_Payment_List_Add AML 查詢，一定要把 繳費單號  更新成空白
            //否則繳費單號一定產生失敗!!!!!!!!

            string sql_update = @"UPDATE IN_MEETING_USER SET in_paynumber = N'' WHERE source_id = '" + meeting_id + "' AND in_paynumber IS NULL";
            inn.applySQL(sql_update);

            foreach (var kv in map)
            {
                var pay = kv.Value;
                string current_orgs = string.Join(",", pay.current_orgs);
                string invoice_up = string.Join(",", pay.invoice_up);
                string uniform_numbers = string.Join(",", pay.uniform_numbers);
                CCO.Utilities.WriteDebug(strMethodName, "建立繳費單: " + no + ". " + pay.in_group + "(" + pay.in_creator_sno + ")");

                Item itmResume = inn.applySQL("SELECT * FROM IN_RESUME WITH(NOLOCK) WHERE in_sno = '" + pay.in_creator_sno + "'");
                string resume_id = itmResume.getProperty("id", "");

                Item itmMClose = inn.newItem();
                itmMClose.setType("In_Meeting_GymList");
                itmMClose.setProperty("meeting_id", pay.meeting_id);
                itmMClose.setProperty("in_group", pay.in_group);
                Item itmMCloseResult = itmMClose.apply("In_Close_GymReg");

                Item itmMPay = inn.newItem();
                itmMPay.setType("In_Meeting_Pay");
                itmMPay.setProperty("meeting_id", pay.meeting_id);
                itmMPay.setProperty("in_group", pay.in_group);
                itmMPay.setProperty("in_creator_sno", pay.in_creator_sno);
                itmMPay.setProperty("resume_id", resume_id);
                itmMPay.setProperty("current_orgs", "," + current_orgs);
                itmMPay.setProperty("invoice_up", "," + invoice_up);
                itmMPay.setProperty("uniform_numbers", "," + uniform_numbers);
                Item itmMPayResult = itmMPay.apply("In_Payment_List_Add2");
            }
        }

        private Dictionary<string, TPay> GetMUserGroups(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmAResume, string meeting_id)
        {
            Dictionary<string, TPay> map = new Dictionary<string, TPay>();

            string sql = "SELECT DISTINCT in_group, in_creator_sno, in_current_org FROM IN_MEETING_USER WITH(NOLOCK) WHERE source_id = '" + meeting_id + "'";
            Item items = inn.applySQL(sql);
            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_group = item.getProperty("in_group", "");
                string in_creator_sno = item.getProperty("in_creator_sno", "");
                string in_current_org = item.getProperty("in_current_org", "");
                string key = in_group + "-" + in_creator_sno;

                TPay pay = null;
                if (map.ContainsKey(key))
                {
                    pay = map[key];
                }
                else
                {
                    pay = new TPay
                    {
                        meeting_id = meeting_id,
                        in_group = in_group,
                        in_creator_sno = in_creator_sno,
                        current_orgs = new List<string>(),
                        invoice_up = new List<string>(),
                        uniform_numbers = new List<string>(),
                    };
                    map.Add(key, pay);
                }

                pay.current_orgs.Add(in_current_org);
                pay.invoice_up.Add("");
                pay.uniform_numbers.Add("");
            }

            return map;
        }

        private class TPay
        {
            public string meeting_id { get; set; }
            public string in_group { get; set; }
            public string in_creator_sno { get; set; }
            public List<string> current_orgs { get; set; }
            public List<string> invoice_up { get; set; }
            public List<string> uniform_numbers { get; set; }

        }


        //轉檔
        private void TransMUsers(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmAResume, List<TMUser> entities)
        {
            CCO.Utilities.WriteDebug(strMethodName, "資料轉換: 共 " + entities.Count + " 筆");

            foreach (var entity in entities)
            {
                Item itmEntity = entity.Value;
                string no = itmEntity.getProperty("no", "");
                string in_mail = entity.in_mail;
                string in_name = itmEntity.getProperty("in_name", "");
                string in_sno = itmEntity.getProperty("in_sno", "").ToUpper();
                string in_l1 = itmEntity.getProperty("in_l1", "");
                string in_l2 = itmEntity.getProperty("in_l2", "");
                CCO.Utilities.WriteDebug(strMethodName, "資料轉換: " + no + ". " + in_name + "(" + in_sno + ") : " + in_l1 + ", " + in_l2);

                string sql1 = "SELECT TOP 1 * FROM IN_MEETING_USER WITH(NOLOCK) WHERE source_id = '" + entity.meeting_id + "'"
                + " AND in_mail = N'" + in_mail + "'";

                Item itmMUser = inn.applySQL(sql1);

                if (itmMUser.isError())
                {
                    CCO.Utilities.WriteDebug(strMethodName, " - 取得與會者發生錯誤");
                    continue;
                }

                if (itmMUser.getResult() != "")
                {
                    string sql2 = "UPDATE A_MUser_20210309 SET memo = N'exists' WHERE no = '" + entity.no + "'";
                    inn.applySQL(sql2);
                    continue;
                }

                string memo = "";
                try
                {
                    NewRegister(CCO, strMethodName, inn, itmAResume, itmEntity, entity.meeting_id);
                    memo = "ok";
                }
                catch (Exception ex)
                {
                    memo = ex.Message;
                }

                itmMUser = inn.applySQL(sql1);
                if (!itmMUser.isError() && itmMUser.getResult() != "")
                {
                    string muid = itmMUser.getProperty("id", "");
                    UpdateMUser(CCO, strMethodName, inn, entity, muid);
                }
                else
                {
                    memo += ", 未建立與會者";
                }

                string sql3 = "UPDATE A_MUser_20210309 SET memo = N'" + memo.Replace("'", "\"") + "' WHERE no = '" + entity.no + "'";
                inn.applySQL(sql3);
            }
        }

        /// <summary>
        /// 新建與會者 (註冊模式)
        /// </summary>
        private void NewRegister(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmParent, Item itmEntity, string meeting_id)
        {
            string in_mail = itmEntity.getProperty("in_sno", "") + "-" + itmEntity.getProperty("in_l2", "");

            string survey_type = "1";
            string method = "register_meeting";

            List<Item> lstSurveys = GetMeetingSurveyList(CCO, strMethodName, inn, meeting_id);
            string parameters = GetParamValues(lstSurveys, itmEntity);
            string isUserId = itmParent.getProperty("in_user_id", "");
            string isIndId = itmParent.getProperty("owned_by_id", "");

            string aml = ""
                + "<surveytype>" + survey_type + "</surveytype>"
                + "<meeting_id>" + meeting_id + "</meeting_id>"
                + "<agent_id>" + "</agent_id>"
                + "<email>" + in_mail + "</email>"
                + "<isUserId>" + isUserId + "</isUserId>"
                + "<isIndId>" + isIndId + "</isIndId>"
                + "<method>" + method + "</method>"
                + "<parameter>" + parameters + "</parameter>"
                + "";

            //CCO.Utilities.WriteDebug(strMethodName, "aml: " + aml);

            Item itmMethod = inn.applyMethod("In_meeting_register", aml);

            if (itmMethod.isError())
            {
                throw new Exception("創建成員帳號發生錯誤");
            }
        }

        /// <summary>
        /// 更新與會者
        /// </summary>
        private void UpdateMUser(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, TMUser entity, string muid)
        {
            string sql = @"
            UPDATE
                IN_MEETING_USER
            SET
                in_note = '{#in_note}'
                , in_creator = N'{#in_creator}'
                , in_creator_sno = N'{#in_creator_sno}'
            WHERE
                id = '{#muid}'
            ";

            StringBuilder builder = new StringBuilder(sql);
            builder.Replace("{#muid}", muid);
            builder.Replace("{#in_note}", entity.no);
            builder.Replace("{#in_creator}", entity.Value.getProperty("in_name", ""));
            builder.Replace("{#in_creator_sno}", entity.Value.getProperty("in_sno", ""));

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + builder.ToString());

            Item itmSQL = inn.applySQL(builder.ToString());

            if (itmSQL.isError())
            {
                CCO.Utilities.WriteDebug(strMethodName, "更新與會者發生錯誤 _# sql: " + sql);
            }
        }

        /// <summary>
        /// 個人會員轉檔清單
        /// </summary>
        private List<TMUser> GetMUsersFromXLS(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn)
        {
            List<TMUser> entities = new List<TMUser>();
            try
            {
                string sql = "SELECT * FROM A_MUser_20210309 WHERE ISNULL(memo, '') = '' ORDER BY no";
                Item items = inn.applySQL(sql);
                int count = items.getItemCount();

                DateTime now = System.DateTime.Now;
                string in_reg_date = now.ToString("yyyy-MM-ddTHH:mm:ss");

                for (int i = 0; i < count; i++)
                {
                    Item item = items.getItemByIndex(i);
                    item.setProperty("in_org", "0");
                    item.setProperty("in_is_teacher", "0");
                    item.setProperty("in_gameunit", item.getProperty("in_l2", ""));
                    item.setProperty("in_reg_date", in_reg_date);

                    TMUser entity = new TMUser
                    {
                        meeting_id = item.getProperty("meeting_id", ""),
                        no = item.getProperty("no", ""),
                        in_sno = item.getProperty("in_sno", ""),
                        in_mail = item.getProperty("in_sno", "") + "-" + item.getProperty("in_l2", ""),
                        Value = item,
                    };

                    entities.Add(entity);
                }
            }
            catch (Exception ex)
            {
                CCO.Utilities.WriteDebug(strMethodName, "取得 XLS 資料 _# error: " + ex.Message);
            }

            return entities;
        }


        #region 資料結構

        private class TMUser
        {
            public string no { get; set; }
            public string meeting_id { get; set; }
            public string in_sno { get; set; }
            public string in_mail { get; set; }
            public Item Value { get; set; }
        }

        #endregion 資料結構

        #region 資料存取

        /// <summary>
        /// 取得協會 Resume
        /// </summary>
        private Item GetAdminResume(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn)
        {
            string sql = "SELECT TOP 1 * FROM IN_RESUME WITH(NOLOCK) WHERE in_sno = N'lwu001'";

            return inn.applySQL(sql);
        }

        /// <summary>
        /// 取得問項清單
        /// </summary>
        private List<Item> GetMeetingSurveyList(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string meeting_id)
        {
            string sql = @"
            SELECT
                t3.id,
                t3.in_property
            FROM
                IN_MEETING t1 WITH(NOLOCK)
            INNER JOIN
                IN_MEETING_SURVEYS t2 WITH(NOLOCK) ON t2.source_id = t1.id
            INNER JOIN
                IN_SURVEY t3 WITH(NOLOCK) ON t3.id = t2.related_id
            WHERE
                t1.id = '{#meeting_id}'
                AND ISNULL(in_property, '') <> ''
            ORDER BY
                t2.sort_order
            ";
            sql = sql.Replace("{#meeting_id}", meeting_id);

            Item items = inn.applySQL(sql);

            if (items.isError() || items.getItemCount() <= 0)
            {
                throw new Exception("創建成員帳號發生錯誤(問項)");
            }

            List<Item> list = new List<Item>();

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                list.Add(items.getItemByIndex(i));
            }

            return list;
        }

        #endregion 資料存取

        /// <summary>
        /// 組成 Url 參數
        /// </summary>
        /// <param name="list">問項清單</param>
        /// <param name="itmEntity">與會者資料</param>
        private string GetParamValues(List<Item> list, Item itmEntity)
        {
            string result = "";
            int count = list.Count;
            for (int i = 0; i < count; i++)
            {
                Item item = list[i];
                string id = item.getProperty("id", "");
                string property = item.getProperty("in_property", "");

                string value = itmEntity.getProperty(property, "");

                if (value == "")
                {
                    continue;
                }

                if (property == "in_birth")
                {
                    value = GetDateTimeValue(value, "yyyy-MM-dd");
                }

                if (result != "")
                {
                    result += "&amp;";
                }

                result += id + "=" + value;
            }
            return result;
        }

        private int GetIntValue(string value)
        {
            int result = 0;
            if (Int32.TryParse(value, out result))
            {
                return result;
            }
            else
            {
                return 0;
            }
        }

        private string GetDateTimeValue(string value, string format = "yyyy-MM-dd", int hours = 0)
        {
            if (value == null || value == "") return "";

            string day = value.Replace("/", "-");

            DateTime dt = DateTime.Now;
            if (DateTime.TryParse(day, out dt))
            {
                return dt.AddHours(hours).ToString(format);
            }
            else
            {
                return value;
            }
        }

        private string GetBirth(string value)
        {
            if (value == null || value == "") return "";

            string day = value.Replace("/", "-");

            DateTime dt = DateTime.Now;
            if (DateTime.TryParse(day, out dt))
            {
                return dt.AddHours(-8).ToString("yyyy/MM/dd HH:mm:ss");
            }
            else
            {
                return value;
            }
        }
    }
}