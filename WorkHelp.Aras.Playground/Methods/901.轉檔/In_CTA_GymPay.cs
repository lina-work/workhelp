﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class In_CTA_GymPay : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
            目的: 中華跆協資料轉檔-E道館社團資料(會費)
            日期: 
                - 2021/03/29: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break(); 

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]In_CTA_GymPay";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "input dom: " + itmR.dom.InnerXml);

            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            Item itmPermit = inn.applyMethod("In_CheckIdentity", "<method>" + strMethodName + "</method><code>" + "ALL" + "</code>");
            bool isMeetingAdmin = itmPermit.getProperty("isMeetingAdmin", "") == "1";

            if (!isMeetingAdmin) throw new Exception("無權限");

            string aml = "";
            string sql = "";

            sql = "SELECT * FROM In_CTA_GymPay WITH(NOLOCK) WHERE in_year > 1911 ORDER BY no";
            Item items = inn.applySQL(sql);
            int count = items.getItemCount();
            CCO.Utilities.WriteDebug(strMethodName, "總筆數: " + count);

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string no = item.getProperty("no", "");
                string resume_id = item.getProperty("resume_id", "");
                string in_name = item.getProperty("in_name", "");
                string in_sno = item.getProperty("in_sno", "");
                CCO.Utilities.WriteDebug(strMethodName, "    " + no + ". " + in_name + "(" + in_sno + ")");

                string in_date = item.getProperty("in_date", "").Trim();
                string utc_date = GetDateTimeByFormat(in_date, "yyyy-MM-dd HH:mm:ss", -8);

                Item itmReferee = inn.newItem("In_Resume_Pay", "merge");
                itmReferee.setAttribute("where", "source_id = '" + resume_id + "' AND in_old_no = " + no);
                itmReferee.setProperty("source_id", resume_id);
                itmReferee.setProperty("in_old_no", no);
                itmReferee.setProperty("in_year", item.getProperty("in_year", "").Trim());
                itmReferee.setProperty("in_amount", item.getProperty("in_amount", "").Trim());
                itmReferee.setProperty("in_date", utc_date);
                itmReferee.setProperty("in_note", item.getProperty("in_note", "").Trim());
                itmReferee = itmReferee.apply();

                if (itmReferee.isError())
                {
                    CCO.Utilities.WriteDebug(strMethodName, "      - 發生錯誤");
                }
            }

            CCO.Utilities.WriteDebug(strMethodName, "轉檔結束");

            return itmR;
        }

        private string GetDateTimeByFormat(string value, string format, int hours = 0)
        {
            if (value == "") return "";

            DateTime dt = DateTime.Now;
            if (DateTime.TryParse(value.Replace("/", "-"), out dt))
            {
                return dt.AddHours(hours).ToString(format);
            }
            else
            {
                return value;
            }
        }
    }
}