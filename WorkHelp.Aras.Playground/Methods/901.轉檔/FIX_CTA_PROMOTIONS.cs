﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class FIX_CTA_PROMOTIONS : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]" + "FIX_CTA_PROMOTIONS_" + System.DateTime.Now.ToString("yyyyMMdd");
            //string strMethodName = "[" + strDatabaseName + "]" + "FIX_CTA_PROMOTIONS_" + System.DateTime.Now.ToString("yyyyMMdd_HHmmss");

            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            string mode = this.getProperty("mode", "");


            if (mode == "enable_gym_members")
            {
                EnableGymMembers(CCO, strMethodName, inn);
            }
            else if (mode == "enable_one_member")
            {
                string in_sno = this.getProperty("in_sno", "");
                if (in_sno != "")
                {
                    EnableOneMember(CCO, strMethodName, inn, in_sno);
                }
            }

            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

            return this;
        }

        private void EnableOneMember(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string in_sno)
        {
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            Item item = inn.applySQL("SELECT * FROM IN_RESUME WITH(NOLOCK) WHERE in_sno = '" + in_sno + "'");
            string resume_id = item.getProperty("id", "");
            string in_name = item.getProperty("in_name", "");

            EnableUser(CCO, strMethodName, inn, _InnH, item);
        }

        private void EnableGymMembers(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn)
        {
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string sql = "";

            sql = @"
                SELECT 
	                 t1.*
                FROM 
	                IN_RESUME t1 WITH(NOLOCK)
                INNER JOIN
	                IN_RESUME t2 WITH(NOLOCK)
	                ON t2.in_manager_name = t1.in_manager_name
	                AND t2.in_name = t1.in_current_org
                WHERE 
	                t1.in_org = 0 
	                AND t1.IN_IS_TEACHER = 0
	                AND t1.in_temp_memo <> 'error'
	                AND ISNULL(t1.in_current_org, '') <> ''
                ORDER BY
	                t1.in_sno
            ";

            Item items = inn.applySQL(sql);
            int count = items.getItemCount();
            CCO.Utilities.WriteDebug(strMethodName, "過檔開始 _# 總筆數: " + count);

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);

                string resume_id = item.getProperty("id", "");
                string in_name = item.getProperty("in_name", "");
                string in_sno = item.getProperty("in_sno", "");
                CCO.Utilities.WriteDebug(strMethodName, (i + 1) + ". " + in_name + "(" + in_sno + ")");

                EnableUser(CCO, strMethodName, inn, _InnH, item);
            }

            CCO.Utilities.WriteDebug(strMethodName, "過檔結束.");

        }

        private void LogError(Innovator inn, string resume_id)
        {
            inn.applySQL("UPDATE IN_RESUME SET in_temp_memo = 'error' WHERE id = '" + resume_id + "'");
        }

        private void EnableUser(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Innosoft.InnovatorHelper _InnH, Item item)
        {
            string resume_id = item.getProperty("id", "");
            string in_sno = item.getProperty("in_sno", "");
            string in_temp_memo = item.getProperty("in_temp_memo", "");
            if (in_temp_memo == "error") return;

            string NewPwd = in_sno.Length > 4 ? in_sno.Substring(in_sno.Length - 4, 4) : "1234";
            string NewPwd_md5 = System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile(NewPwd, "md5");

            string SequenceVal = item.getProperty("in_sno", item.getProperty("login_name", ""));

            //建立使用者
            Item itmUser = inn.newItem("User", "merge");
            itmUser.setAttribute("where", "login_name='" + item.getProperty("login_name", "") + "'");
            itmUser.setProperty("last_name", item.getProperty("in_name", ""));//姓
            itmUser.setProperty("first_name", SequenceVal);//名
            itmUser.setProperty("user_no", SequenceVal);//員工編號
            itmUser.setProperty("login_name", item.getProperty("login_name"));//登入帳號
            itmUser.setProperty("cell", item.getProperty("in_tel", ""));//行動電話
            itmUser.setProperty("email", item.getProperty("in_email", ""));//電子郵件
            itmUser.setProperty("in_company", item.getProperty("in_company", ""));//公司別
            itmUser.setProperty("logon_enabled", "1");//可登入
            itmUser.setProperty("in_rank", inn.getItemByKeyedName("in_rank", "SYS-000000").getID());//職級
            itmUser.setProperty("in_app_page", "pages/c.aspx?page=UserDashboard.html&method=in_user_dashboard");
            itmUser.setProperty("password", NewPwd_md5);

            bool merge_user = false;
            try
            {
                itmUser = itmUser.apply();
                if (!itmUser.isError() && itmUser.getResult() != "")
                {
                    merge_user = true;
                }
            }
            catch
            {
                merge_user = false;
            }

            if (!merge_user)
            { 
                LogError(inn, resume_id);
                return;
            }
              
            string in_user_id = itmUser.getID();

            Item itmIdentity = inn.newItem("Identity", "get");
            itmIdentity.setProperty("in_user", in_user_id);
            itmIdentity = itmIdentity.apply();
            string identity_id = itmIdentity.getID();

            //MeetingUser
            Item itmMember;
            itmMember = inn.newItem("Member", "merge");
            itmMember.setAttribute("where", "source_id='95277C725C954FEE959DCE2073DA1F9C' and related_id='" + identity_id + "'");
            itmMember.setProperty("source_id", "95277C725C954FEE959DCE2073DA1F9C");
            itmMember.setRelatedItem(itmIdentity);
            itmMember = itmMember.apply();

            //All Employees
            itmMember = inn.newItem("Member", "merge");
            itmMember.setAttribute("where", "source_id='B32BD81D1AD04207BF1E61E39A4E0E13' and related_id='" + identity_id + "'");
            itmMember.setProperty("source_id", "B32BD81D1AD04207BF1E61E39A4E0E13");
            itmMember.setRelatedItem(itmIdentity);
            itmMember = itmMember.apply();

            //此為申請館主之用,要把報名者加入 7AC8B33DA0F246DDA70BB244AB231E29 ACT_GymOwner
            itmMember = inn.newItem("Member", "merge");
            itmMember.setAttribute("where", "source_id='7AC8B33DA0F246DDA70BB244AB231E29' and related_id='" + identity_id + "'");
            itmMember.setProperty("source_id", "7AC8B33DA0F246DDA70BB244AB231E29");
            itmMember.setRelatedItem(itmIdentity);
            itmMember = itmMember.apply();




            Item itmResume = inn.newItem("In_Resume", "get");
            itmResume.setProperty("id", resume_id);
            itmResume = itmResume.apply();

            string role_remark = "會員";

            //self
            Item itmResumeRole = inn.newItem("In_Resume_Resume", "merge");
            itmResumeRole.setAttribute("where", "source_id='" + resume_id + "' and related_id='" + resume_id + "'");
            itmResumeRole.setProperty("source_id", resume_id);
            itmResumeRole.setProperty("in_resume_role", "sys_9999");
            itmResumeRole.setProperty("in_resume_remark", role_remark);
            itmResumeRole.setRelatedItem(itmResume);
            itmResumeRole = itmResumeRole.apply();


            //建立與會者
            string meeting_id = "249FDB244E534EB0AA66C8E9C470E930";

            Item itmMUser = inn.newItem("In_Meeting_User", "merge");
            itmMUser.setAttribute("where", "source_id='" + meeting_id + "' AND in_mail='" + item.getProperty("login_name", "") + "'");
            itmMUser.setProperty("source_id", meeting_id);
            itmMUser.setProperty("in_name", item.getProperty("in_name", ""));
            itmMUser.setProperty("in_sno", item.getProperty("login_name", ""));
            itmMUser.setProperty("in_sno_display", GetSidDisplay(item.getProperty("login_name", "")));
            itmMUser.setProperty("in_mail", item.getProperty("login_name", ""));
            itmMUser.setProperty("in_email", item.getProperty("in_email", ""));
            itmMUser.setProperty("in_gender", item.getProperty("in_gender", ""));
            itmMUser.setProperty("in_birth", item.getProperty("in_birth", ""));
            itmMUser.setProperty("in_note_state", "official");
            itmMUser.setProperty("in_confirm_mail", DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss"));
            itmMUser.setProperty("in_regdate", DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss"));
            itmMUser.setProperty("in_tel", item.getProperty("in_tel", ""));
            itmMUser.setProperty("in_creator", "lwu001");
            itmMUser.setProperty("in_creator_sno", "lwu001");
            itmMUser.setProperty("in_current_org", item.getProperty("in_name", ""));
            itmMUser.setProperty("in_group", item.getProperty("in_name", ""));
            itmMUser.setProperty("in_role", "player");
            itmMUser.setProperty("in_index", "00001");
            itmMUser.setProperty("in_add", item.getProperty("in_resident_add", ""));

            bool merge_muser = false;

            try
            {
                itmMUser = itmMUser.apply();
                if (!itmMUser.isError() && itmMUser.getResult() != "")
                {
                    merge_muser = true;
                }
            }
            catch
            {
                merge_muser = false;
            }

            if (!merge_muser)
            {
                LogError(inn, resume_id);
                return;
            }
            else
            {
                string sql_update = "UPDATE IN_RESUME SET"
                        + " password = '" + NewPwd_md5 + "'"
                        + ", in_password_plain = '" + NewPwd + "'"
                        + ", in_user_id = '" + in_user_id + "'"
                        + ", owned_by_id = '" + identity_id + "'"
                        + ", in_is_teacher = '1'"
                        + ", in_temp_memo = 'enabled'"
                        + " WHERE id = '" + resume_id + "'";

                inn.applySQL(sql_update);
            }
        }

        //身分證字號(外顯)
        private string GetSidDisplay(string in_sno)
        {
            if (in_sno.Length == 10)
            {
                //10碼(身分證處理)
                string in_sno_1 = in_sno.Substring(0, 2);
                string in_sno_2 = in_sno.Substring(6, 4);
                return in_sno_1 + "****" + in_sno_2;//A1****6789
            }
            else
            {
                return "";
            }
        }

    }
}