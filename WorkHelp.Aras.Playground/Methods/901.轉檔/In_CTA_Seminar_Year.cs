﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;

namespace WorkHelp.Aras.Playground.ArasModules
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class In_CTA_Seminar_Year : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 中華跆協資料轉檔-講習年度記錄
                    - z教練裁判資料(講習)
                日期: 
                    - 2021/03/19: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break(); 

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]In_CTA_Seminar_Year";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "input dom: " + itmR.dom.InnerXml);

            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            Item itmPermit = inn.applyMethod("In_CheckIdentity", "<method>" + strMethodName + "</method><code>" + "ALL" + "</code>");
            bool isMeetingAdmin = itmPermit.getProperty("isMeetingAdmin", "") == "1";

            if (!isMeetingAdmin) throw new Exception("無權限");

            string aml = "";
            string sql = "";
            sql = "SELECT * FROM IN_CTA_REFEREE_YEAR WITH(NOLOCK) WHERE ISNULL(resume_id, '') <> '' ORDER BY in_referee_no, in_acs_no";
            Item itmLogs = inn.applySQL(sql);
            int count = itmLogs.getItemCount();
            CCO.Utilities.WriteDebug(strMethodName, "轉檔開始-總筆數: " + count);

            Dictionary<string, string> map = new Dictionary<string, string>
            {
                { "ref_1", "referee" },
                { "ref_2", "referee" },
                { "ref_3", "referee" },
                { "ref_4", "referee" },
                { "ref_5", "referee" },
                { "ref_6", "referee" },
                { "ref_7", "referee" },
                { "ref_8", "referee" },
                { "ref_9", "referee" },
                { "ref_10", "referee" },
                { "ref_11", "referee" },
                { "ref_12", "referee" },
                { "ref_13", "referee" },
                { "ref_14", "referee" },
                { "ref_15", "referee" },
                { "ref_16", "referee" },
                { "ref_17", "referee" },
                { "ref_18", "referee" },
                { "ref_19", "referee" },
                { "ref_20", "referee" },
                { "coa_1", "instructor" },
                { "coa_2", "instructor" },
                { "coa_3", "instructor" },
                { "coa_4", "instructor" },
                { "coa_5", "instructor" },
                { "coa_6", "instructor" },
                { "coa_7", "instructor" },
                { "coa_8", "instructor" },
                { "coa_9", "instructor" },
                { "coa_10", "instructor" },
                { "coa_11", "instructor" },
                { "coa_12", "instructor" },
                { "coa_13", "instructor" },
                { "coa_14", "instructor" },
                { "coa_15", "instructor" },
                { "coa_16", "instructor" },
                { "coa_17", "instructor" },
                { "coa_18", "instructor" },
                { "coa_19", "instructor" },
                { "coa_20", "instructor" },
                { "rp_1", "poomsae" },
                { "rp_2", "poomsae" },
                { "rp_3", "poomsae" },
                { "rp_4", "poomsae" },
                { "rp_5", "poomsae" },
                { "rp_6", "poomsae" },
                { "rp_7", "poomsae" },
                { "rp_8", "poomsae" },
                { "rp_9", "poomsae" },
                { "rp_10", "poomsae" },
                { "rp_11", "poomsae" },
                { "rp_12", "poomsae" },
                { "rp_13", "poomsae" },
                { "rp_14", "poomsae" },
                { "rp_15", "poomsae" },
                { "rp_16", "poomsae" },
                { "rp_17", "poomsae" },
                { "rp_18", "poomsae" },
                { "rp_19", "poomsae" },
                { "rp_20", "poomsae" },
            };

            for (int i = 0; i < count; i++)
            {
                Item itmLog = itmLogs.getItemByIndex(i);
                string in_referee_no = itmLog.getProperty("in_referee_no", "");
                string resume_id = itmLog.getProperty("resume_id", "");
                string in_name = itmLog.getProperty("in_name", "");
                string in_sno = itmLog.getProperty("in_sno", "");
                CCO.Utilities.WriteDebug(strMethodName, "    " + i + ". " + in_name + "(" + in_sno + ") _# old no = " + in_referee_no);

                TResume entity = new TResume
                {
                    in_referee_no = in_referee_no,
                    resume_id = resume_id,
                    in_name = in_name,
                    in_sno = in_sno,
                    Value = itmLog,
                };

                foreach (var kv in map)
                {
                    string in_property = kv.Key;
                    string in_type = kv.Value;
                    AddResumeYear(CCO, strMethodName, inn, entity, in_property, in_type);
                }
            }

            CCO.Utilities.WriteDebug(strMethodName, "轉檔結束");

            //把身分換回來
            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

            return itmR;
        }

        private void AddResumeYear(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, TResume entity, string in_property, string in_type)
        {
            string in_tw_year = entity.Value.getProperty(in_property, "");
            if (in_tw_year == "") return;

            int tw_year = GetIntValue(in_tw_year);
            if (tw_year <= 0) return;

            int west_year = tw_year + 1911;

            bool need_update = false;

            string sql_qry = "SELECT * FROM In_Resume_Years WITH(NOLOCK)"
                + " WHERE source_id = '" + entity.resume_id + "'"
                + " AND in_old_property = '" + in_property + "'"
                + " AND in_type = '" + in_type + "'"
                + " AND in_year = '" + west_year.ToString() + "'";

            Item itmOld = inn.applySQL(sql_qry);

            if (itmOld.isError() || itmOld.getResult() == "")
            {
                need_update = true;
            }

            if (!need_update)
            {
                return;
            }

            Item itmYear = inn.newItem("In_Resume_Years", "merge");

            itmYear.setAttribute("where", "source_id = '" + entity.resume_id + "'"
                + " AND in_old_property = '" + in_property + "'"
                + " AND in_type = '" + in_type + "'"
                + " AND in_year = '" + west_year.ToString() + "'"
            );

            itmYear.setProperty("source_id", entity.resume_id);
            itmYear.setProperty("in_type", in_type);
            itmYear.setProperty("in_year", west_year.ToString());
            itmYear.setProperty("in_old_no", entity.in_referee_no);
            itmYear.setProperty("in_old_property", in_property);
            //itmYear.setProperty("in_note", "");

            itmYear = itmYear.apply();

            if (itmYear.isError())
            {
                CCO.Utilities.WriteDebug(strMethodName, "      - 發生錯誤");
            }
        }

        private class TResume
        {
            public string resume_id { get; set; }
            public string in_referee_no { get; set; }
            public string in_name { get; set; }
            public string in_sno { get; set; }
            public Item Value { get; set; }
        }

        private int GetIntValue(string value)
        {
            int result = 0;
            Int32.TryParse(value, out result);
            return result;
        }
    }
}