﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class In_CTA_GymMember : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
            目的: 中華跆協資料轉檔-團體會員
            日期: 
                - 2021/03/04: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]In_CTA_GymMember";

            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            Item itmR = this;

            //協會 Resume
            Item itmAResume = GetAdminResume(CCO, strMethodName, inn);

            List<TMember> entities = GetMembersFromXLS(CCO, strMethodName, inn);
            //會員轉檔
            TransMembers(CCO, strMethodName, inn, itmAResume, entities);
            //更新最後繳費年度
            UpdateMaxPayYear(CCO, strMethodName, inn, entities);

            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

            return itmR;
        }

        //更新最後繳費年度
        private void UpdateMaxPayYear(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, List<TMember> entities)
        {
            var searches = entities.Where(x => x.NeedUpdate);

            foreach(var search in searches)
            {
                string sql = @"
                    UPDATE t1 SET
	                    t1.in_member_last_year = t2.max_year
                    FROM
	                    IN_RESUME t1
                    INNER JOIN
                    (
	                    SELECT source_id, Max(In_year) AS 'max_year' FROM IN_RESUME_PAY WITH(NOLOCK) GROUP BY source_id
                    ) t2
                        ON t2.source_id = t1.id
                    WHERE
                        t1.id = '{#resume}'
                ";

                sql = sql.Replace("{#resume}", search.id);

                inn.applySQL(sql);
            }
        }

        //會員轉檔
        private void TransMembers(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmAResume, List<TMember> entities)
        {
            StringBuilder logs = new StringBuilder();

            int no = 1;

            foreach (var entity in entities)
            {
                Item itmEntity = entity.Value;
                string in_name = itmEntity.getProperty("in_name", "");
                string in_principal = itmEntity.getProperty("in_principal", "").ToUpper();
                string in_add = itmEntity.getProperty("in_add", "").ToUpper();

                logs.AppendLine(" - " + no + ". " + in_name + ": " + in_add);

                //手動指定系統帳號
                if (entity.in_sno != "")
                {
                    string sql0 = "SELECT * FROM IN_RESUME WITH(NOLOCK) WHERE in_sno = N'" + entity.in_sno + "'";
                    Item itmTarget = inn.applySQL(sql0);
                    SyncResume(CCO, strMethodName, inn, itmAResume, entity, itmTarget, logs);
                    logs.AppendLine();
                    no++;
                    continue;
                }

                string sql1 = "SELECT * FROM IN_RESUME WITH(NOLOCK) WHERE in_name = N'" + in_name + "'";
                Item itmResumes = inn.applySQL(sql1);
                int resume_count = itmResumes.getItemCount();

                if (itmResumes.isError())
                {
                    logs.AppendLine("    - 存取發生錯誤");
                }
                else if (resume_count > 1)
                {
                    List<TResume> objs = new List<TResume>();
                    int ok_count = 0;
                    bool has_find = false;

                    for (int i = 0; i < resume_count; i++)
                    {
                        Item itmTmp = itmResumes.getItemByIndex(i);
                        string old_add = itmTmp.getProperty("in_add", "");
                        string old_member_status = itmTmp.getProperty("in_member_status", "");

                        TResume obj = new TResume 
                        { 
                            IsOk = old_member_status == "合格會員",
                            InAdd = old_add,
                            Value = itmTmp,
                        };

                        if (obj.IsOk && obj.InAdd == in_add)
                        {
                            has_find = true;
                            SyncResume(CCO, strMethodName, inn, itmAResume, entity, itmTmp, logs);
                        }
                        else
                        {
                            if (obj.IsOk) ok_count++;
                            objs.Add(obj);
                        }
                    }

                    if (!has_find)
                    { 
                        if (ok_count == 1)
                        {
                            var target = objs.Find(x => x.IsOk);
                            SyncResume(CCO, strMethodName, inn, itmAResume, entity, target.Value, logs);
                        }
                        else
                        {
                            foreach (var obj in objs)
                            {
                                AppendLog(logs, obj.Value);
                            }
                        }
                    }
                }
                else if (itmResumes.getResult() == "")
                {
                    string sql2 = "SELECT * FROM IN_RESUME WITH(NOLOCK) WHERE in_name Like N'%" + in_name + "%'";
                    Item itmLikes = inn.applySQL(sql2);
                    int like_count = itmLikes.getItemCount();
                    if (like_count > 0)
                    {
                        logs.AppendLine("    - 查無該團體，相似名稱如下");
                        for (int i = 0; i < like_count; i++)
                        {
                            Item itmTmp = itmLikes.getItemByIndex(i);
                            AppendLog(logs, itmTmp);
                        }
                    }
                    else
                    {
                        logs.AppendLine("    - 查無該團體");
                    }
                }
                else
                {
                    SyncResume(CCO, strMethodName, inn, itmAResume, entity, itmResumes.getItemByIndex(0), logs);
                }
                logs.AppendLine();
                no++;
            }

            CCO.Utilities.WriteDebug(strMethodName, "資料轉換報告: \r\n" + logs.ToString());
        }

        private void SyncResume(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmAResume, TMember entity, Item itmResume, StringBuilder logs)
        {
            //單位註冊帳號
            string meeting_id = "5F73936711E04DC799CB02587F4FF7E0";

            Item itmParent = itmAResume;

            //CCO.Utilities.WriteDebug(strMethodName, "IN_RESUME 存在");

            //更新 Resume 資料
            UpdateOldResume(CCO, strMethodName, inn, entity.Value, meeting_id);

            AddResumePay(CCO, strMethodName, inn, entity, itmResume);

            logs.AppendLine("    - 同步成功 ("+ itmResume .getProperty("in_sno", "") + "): "+ itmResume.getProperty("in_add", "") + " ("+ itmResume.getProperty("in_member_status", "") + ")");

            entity.id = itmResume.getProperty("id", "");
            entity.NeedUpdate = true;
        }

        private void AppendLog(StringBuilder logs, Item itmTmp)
        {
            logs.AppendLine("    - [ ] " + itmTmp.getProperty("in_name", "")
                + "(" + itmTmp.getProperty("in_sno", "") + ")"
                + ": " + itmTmp.getProperty("in_add", "")
                + " (" + itmTmp.getProperty("in_member_status", "") + ")"
                );

        }

        /// <summary>
        /// 建立成員角色
        /// </summary>
        private void BindRole(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmParent, Item itmEntity, Item itmUserResume)
        {
            string group_resume_id = itmParent.getProperty("id", "");

            string in_resume_role = itmEntity.getProperty("in_resume_role", "");
            string in_resume_remark = itmEntity.getProperty("in_resume_remark", "");

            string user_resume_id = itmUserResume.getProperty("id", "");

            if (group_resume_id == "" || user_resume_id == "")
            {
                return;
            }

            bool is_exists = ExistsNewRole(CCO, strMethodName, inn, group_resume_id, user_resume_id, in_resume_role);

            if (is_exists)
            {
                //throw new Exception("成員已擔任該角色");
                return;
            }

            Item itmUserRole = inn.newItem("In_Resume_Resume", "add");
            itmUserRole.setProperty("source_id", group_resume_id);
            itmUserRole.setProperty("related_id", user_resume_id);
            itmUserRole.setProperty("in_resume_role", in_resume_role);
            itmUserRole.setProperty("in_resume_remark", in_resume_remark);
            itmUserRole = itmUserRole.apply();
            if (itmUserRole.isError())
            {
                throw new Exception("建立成員關聯發生錯誤");
            }
        }

        /// <summary>
        /// 更新 Resume 資料
        /// </summary>
        private void UpdateOldResume(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmEntity, string meeting_id)
        {
            string sql = @"
            UPDATE
                IN_RESUME
            SET
                in_principal = N'{#in_principal}'
                , in_add = N'{#in_add}'
            WHERE
                in_sno = N'{#in_sno}'
            ";

            StringBuilder builder = new StringBuilder(sql);
            builder.Replace("{#in_sno}", itmEntity.getProperty("in_sno", ""));
            builder.Replace("{#in_principal}", itmEntity.getProperty("in_principal", ""));
            builder.Replace("{#in_add}", itmEntity.getProperty("in_add", ""));

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + builder.ToString());

            Item itmSQL = inn.applySQL(builder.ToString());

            if (itmSQL.isError())
            {
                CCO.Utilities.WriteDebug(strMethodName, "更新成員履歷發生錯誤 _# sql: " + sql);
            }
        }


        /// <summary>
        /// 成員角色是否已存在
        /// </summary>
        private bool ExistsNewRole(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string group_resume_id, string user_resume_id, string new_resume_role)
        {
            Item itmUserRole = inn.newItem("In_Resume_Resume", "get");
            itmUserRole.setProperty("source_id", group_resume_id);
            itmUserRole.setProperty("related_id", user_resume_id);
            itmUserRole.setProperty("in_resume_role", new_resume_role);
            itmUserRole = itmUserRole.apply();
            return itmUserRole.getItemCount() == 1;
        }

        /// <summary>
        /// 會費繳納過檔
        /// </summary>
        private void AddResumePay(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, TMember entity, Item itmResume)
        {
            string resume_id = itmResume.getProperty("id", "");

            ////刪除會費繳納
            //string aml = @"<AML><Item type='IN_RESUME_PAY' action='delete' where=""source_id='{#source_id}'""/></AML>";
            //aml = aml.Replace("{#source_id}", resume_id);
            //inn.applyAML(aml);

            for (int i = 0; i < entity.PayList.Count; i++)
            {
                var pay = entity.PayList[i];

                Item itmRPay = inn.newItem("In_Resume_Pay");

                itmRPay.setAttribute("where", "source_id = '" + resume_id + "' AND in_year = '" + pay.WestYear + "'");

                itmRPay.setProperty("source_id", resume_id);
                itmRPay.setProperty("in_year", pay.WestYear);
                itmRPay.setProperty("in_date", pay.FullDay);
                itmRPay.setProperty("in_amount", pay.Amount);
                itmRPay.setProperty("in_note", pay.Note.Trim());

                itmRPay = itmRPay.apply("merge");

                if (itmRPay.isError())
                {
                    CCO.Utilities.WriteDebug(strMethodName, "[個人會員費用過檔]過檔失敗 _# dom: " + itmRPay.dom.InnerXml);
                }
            }
        }

        /// <summary>
        /// 新建與會者 (註冊模式)
        /// </summary>
        private void NewRegister(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmParent, Item itmEntity, string meeting_id)
        {
            string in_sno = itmEntity.getProperty("in_sno", "");

            string survey_type = "1";
            string method = "register_meeting";

            List<Item> lstSurveys = GetMeetingSurveyList(CCO, strMethodName, inn, meeting_id);
            string parameters = GetParamValues(lstSurveys, itmEntity);
            string isUserId = itmParent.getProperty("in_user_id", "");
            string isIndId = itmParent.getProperty("owned_by_id", "");

            string aml = ""
                + "<surveytype>" + survey_type + "</surveytype>"
                + "<meeting_id>" + meeting_id + "</meeting_id>"
                + "<agent_id>" + "</agent_id>"
                + "<email>" + in_sno + "</email>"
                + "<isUserId>" + isUserId + "</isUserId>"
                + "<isIndId>" + isIndId + "</isIndId>"
                + "<method>" + method + "</method>"
                + "<parameter>" + parameters + "</parameter>"
                + "";

            //CCO.Utilities.WriteDebug(strMethodName, "aml: " + aml);

            Item itmMethod = inn.applyMethod("In_meeting_register", aml);

            if (itmMethod.isError())
            {
                throw new Exception("創建成員帳號發生錯誤");
            }
        }

        /// <summary>
        /// 取得會員資料
        /// </summary>
        private List<TMember> GetMembersFromXLS(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn)
        {
            var entities = new List<TMember>();
            try
            {
                //手動將 個人及一般團體會員(e化用).xlsx 匯入至 PLMCTA
                string sql = "SELECT * FROM CTA_GYM_MEMBER_20210308";
                //sql = "SELECT * FROM CTA_MEMBER_20210303 WHERE T18 IN ('A220294627')";

                Item items = inn.applySQL(sql);

                int count = items.getItemCount();

                for (int i = 0; i < count; i++)
                {
                    Item item = items.getItemByIndex(i);
                    string in_name = item.getProperty("in_name", "");
                    string in_sno = item.getProperty("in_sno", "");
                    string in_years = item.getProperty("in_years", "");
                    string in_pay_day = item.getProperty("in_pay_day", "");
                    string in_expense = item.getProperty("in_expense", "");
                    string west_pay_day = TwDayToWest(CCO, strMethodName, inn, in_pay_day);

                    Item itmEntity = inn.newItem();
                    itmEntity.setProperty("in_name", in_name);
                    itmEntity.setProperty("in_principal", item.getProperty("in_principal", ""));
                    itmEntity.setProperty("in_add", item.getProperty("in_add", ""));
                    itmEntity.setProperty("in_pay_day", in_pay_day);
                    itmEntity.setProperty("in_expense", in_expense);
                    itmEntity.setProperty("in_years", in_years);

                    itmEntity.setProperty("in_member_type", "vip_gym");
                    itmEntity.setProperty("in_member_role", "sys_9999");

                    itmEntity.setProperty("in_resume_role", "org_vip_gym");
                    itmEntity.setProperty("in_resume_remark", "團體會員");

                    var entity = new TMember
                    {
                        in_name = in_name,
                        in_sno = in_sno,
                        Value = itmEntity,
                        PayList = new List<TPay>(),
                    };

                    if (in_years.Contains("-"))
                    {
                        var arr1 = in_years.Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);
                        var s = GetIntValue(arr1[0].Trim());
                        var e = GetIntValue(arr1[1].Trim());
                        var c = e - s + 1;
                        var amount = GetIntValue(in_expense) / c;

                        for (int tw_year = s; tw_year <= e; tw_year++)
                        {
                            int west_year = tw_year < 1911 ? tw_year + 1911 : tw_year;
                            entity.PayList.Add(new TPay
                            {
                                WestYear = west_year.ToString(),
                                Amount = amount.ToString(),
                                FullDay = west_pay_day,
                                Note = in_years + ": " + in_expense,
                            });
                        }
                    }
                    else if (in_years.Contains("、"))
                    {
                        var arr2 = in_years.Split(new char[] { '、' }, StringSplitOptions.RemoveEmptyEntries);

                        var c = arr2.Length;
                        var amount = GetIntValue(in_expense) / c;

                        foreach (var val in arr2)
                        {
                            int tw_year = GetIntValue(val.Trim());
                            int west_year = tw_year < 1911 ? tw_year + 1911 : tw_year;

                            entity.PayList.Add(new TPay
                            {
                                WestYear = west_year.ToString(),
                                Amount = amount.ToString(),
                                FullDay = west_pay_day,
                                Note = in_years + ": " + in_expense,
                            });
                        }
                    }
                    else
                    {
                        int tw_year = GetIntValue(in_years);
                        int west_year = tw_year < 1911 ? tw_year + 1911 : tw_year;

                        entity.PayList.Add(new TPay
                        {
                            WestYear = west_year.ToString(),
                            Amount = in_expense,
                            FullDay = west_pay_day,
                            Note = ""
                        });
                    }

                    entities.Add(entity);
                }
            }
            catch (Exception ex)
            {
                CCO.Utilities.WriteDebug(strMethodName, "取得 XLS 資料 _# error: " + ex.Message);
            }

            return entities;
        }

        #region 資料結構
        private class TMember
        {
            public string id { get; set; }
            public string in_name { get; set; }
            public string in_sno { get; set; }
            public Item Value { get; set; }
            public List<TPay> PayList { get; set; }
            public bool NeedUpdate { get; set; }
        }

        private class TPay
        {
            public string WestYear { get; set; }
            public string Amount { get; set; }
            public string FullDay { get; set; }
            public string Note { get; set; }

        }
        private class TResume
        {
            public bool IsOk { get; set; }
            public string InAdd { get; set; }
            public Item Value { get; set; }
        }
        #endregion 資料結構

        #region 資料存取

        /// <summary>
        /// 取得協會 Resume
        /// </summary>
        private Item GetAdminResume(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn)
        {
            string sql = "SELECT TOP 1 * FROM IN_RESUME WITH(NOLOCK) WHERE in_sno = N'M001'";

            return inn.applySQL(sql);
        }

        /// <summary>
        /// 取得 ALL Resume
        /// </summary>
        private Item GetAllResume(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn)
        {
            Dictionary<string, Item> map = new Dictionary<string, Item>();
            string sql = "SELECT * FROM IN_RESUME WITH(NOLOCK)";
            return inn.applySQL(sql);
        }

        /// <summary>
        /// 取得問項清單
        /// </summary>
        private List<Item> GetMeetingSurveyList(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string meeting_id)
        {
            string sql = @"
            SELECT
                t3.id,
                t3.in_property
            FROM
                IN_MEETING t1 WITH(NOLOCK)
            INNER JOIN
                IN_MEETING_SURVEYS t2 WITH(NOLOCK) ON t2.source_id = t1.id
            INNER JOIN
                IN_SURVEY t3 WITH(NOLOCK) ON t3.id = t2.related_id
            WHERE
                t1.id = '{#meeting_id}'
                AND ISNULL(in_property, '') <> ''
            ORDER BY
                t2.sort_order
            ";
            sql = sql.Replace("{#meeting_id}", meeting_id);

            Item items = inn.applySQL(sql);

            if (items.isError() || items.getItemCount() <= 0)
            {
                throw new Exception("創建成員帳號發生錯誤(問項)");
            }

            List<Item> list = new List<Item>();

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                list.Add(items.getItemByIndex(i));
            }

            return list;
        }

        private Dictionary<string, Item> MapAllResume(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn)
        {
            Item itmAllResumes = GetAllResume(CCO, strMethodName, inn);
            return GetMap(itmAllResumes, "in_sno");
        }

        /// <summary>
        /// 轉換字典
        /// </summary>
        private Dictionary<string, Item> GetMap(Item items, string property)
        {
            Dictionary<string, Item> map = new Dictionary<string, Item>();

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string key = item.getProperty(property, "").ToUpper();

                if (map.ContainsKey(key))
                {
                    throw new Exception("鍵值重複: " + key);
                }
                else
                {
                    map.Add(key, item);
                }
            }

            return map;
        }
        #endregion 資料存取

        /// <summary>
        /// 組成 Url 參數
        /// </summary>
        /// <param name="list">問項清單</param>
        /// <param name="itmEntity">與會者資料</param>
        private string GetParamValues(List<Item> list, Item itmEntity)
        {
            string result = "";
            int count = list.Count;
            for (int i = 0; i < count; i++)
            {
                Item item = list[i];
                string id = item.getProperty("id", "");
                string property = item.getProperty("in_property", "");

                string value = itmEntity.getProperty(property, "");

                if (value == "")
                {
                    continue;
                }

                if (property == "in_birth")
                {
                    value = GetDateTimeValue(value, "yyyy-MM-dd");
                }

                if (result != "")
                {
                    result += "&amp;";
                }

                result += id + "=" + value;
            }
            return result;
        }


        private string TwDayToWest(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string value)
        {
            //'110/02/18

            if (value == "") return "";
            if (!value.Contains("/"))
            {
                CCO.Utilities.WriteDebug(strMethodName, "date formmat error: " + value);
                return "";
            }

            string tw_day = value.Replace("'", "").Replace("民國", "").Trim();
            string[] str_arr = tw_day.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
            if (str_arr == null || str_arr.Length != 3)
            {
                CCO.Utilities.WriteDebug(strMethodName, "date formmat error: " + value);
                return "";
            }

            int[] arr = str_arr.Select(n => Convert.ToInt32(n)).ToArray();

            int y = arr[0] < 1911 ? arr[0] + 1911 : arr[0];
            int m = arr[1];
            int d = arr[2];

            if (str_arr[2].Length == 4)
            {
                // 4/15/2020
                y = arr[2];
                m = arr[0];
                d = arr[1];
            }

            return new DateTime(y, m, d).ToString("yyyy-MM-ddTHH:mm:ss");
        }

        private int GetIntValue(string value)
        {
            int result = 0;
            if (Int32.TryParse(value, out result))
            {
                return result;
            }
            else
            {
                return 0;
            }
        }

        private string GetDateTimeValue(string value, string format = "yyyy-MM-dd", int hours = 0)
        {
            if (value == null || value == "") return "";

            string day = value.Replace("/", "-");

            DateTime dt = DateTime.Now;
            if (DateTime.TryParse(day, out dt))
            {
                return dt.AddHours(hours).ToString(format);
            }
            else
            {
                return value;
            }
        }
    }
}