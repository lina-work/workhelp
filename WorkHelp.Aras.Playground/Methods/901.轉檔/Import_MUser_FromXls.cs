﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class Import_MUser_FromXls : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
            目的: 與會者匯入
            步驟:
                - 整理 xls 匯入至 sql server: A_MUser_yyyyMMdd
                - sql scripts update 補齊欄位 (resume_id, in_gender, in_birth, in_current_org, in_group)
                - 檢查 in_sno 是否重複
                - 檢查會費 Meeting 問項 in_l1、in_l2
            日期: 
                - 2021/12/10: 調整 (lina)
                - 2021/03/09: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]Import_MUser_FromXls";

            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, this.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                InnH = _InnH,

                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),

                meeting_id = "8EB38171B3164AE2B1AA6C4B7717C477",
                muser_table = "A_MUser_20211210",
            };


            var itmAResume = GetAdminResume(cfg);

            var musers = GetMUsersFromXLS_2022(cfg);

            //會員轉檔
            TransMUsers_2022(cfg, itmAResume, musers);

            //建立繳費單
            TransMPays(cfg, itmAResume);

            //轉講師
            //TransTeachers(CCO, strMethodName, inn, itmAResume, meeting_id);

            //檢查名稱
            //CheckNames(cfg);

            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

            return itmR;
        }

        //檢查名稱
        private void CheckNames(TConfig cfg)
        {
            string sql = "SELECT DISTINCT in_name FROM " + cfg.muser_table + " WITH(NOLOCK)";

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();

            int no = 1;
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_name = item.getProperty("in_name", "");
                string sql2 = "SELECT * FROM IN_RESUME WITH(NOLOCK) WHERE in_name = N'" + in_name + "'";

                Item itmResumes = cfg.inn.applySQL(sql2);

                int resume_count = itmResumes.getItemCount();

                if (resume_count > 1)
                {
                    cfg.CCO.Utilities.WriteDebug(cfg.strMethodName + "_check", no + ". 姓名重複: " + in_name + " 筆數：" + resume_count);
                    no++;
                }
            }
        }

        //轉講師
        private void TransTeachers(TConfig cfg, Item itmAResume)
        {
            string sql = @"
                SELECT 
	                t1.*
                FROM
	                IN_RESUME t1 WITH(NOLOCK)
                INNER JOIN
                (
	                SELECT DISTINCT in_creator_sno FROM IN_MEETING_USER WITH(NOLOCK) WHERE source_id = '{#meeting_id}'
                ) t2 ON t2.in_creator_sno = t1.in_sno
                WHERE
	                ISNULL(t1.in_is_teacher, '0') = '0'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            Item itmResumes = cfg.inn.applySQL(sql);

            int count = itmResumes.getItemCount();

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "轉講師: 共 " + count + " 筆");

            for (int i = 0; i < count; i++)
            {
                Item itmResume = itmResumes.getItemByIndex(i);
                string no = (i + 1).ToString();
                string resume_id = itmResume.getProperty("id", "");
                string in_user_id = itmResume.getProperty("in_user_id", "");
                string in_name = itmResume.getProperty("in_name", "");
                string in_sno = itmResume.getProperty("in_sno", "");

                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "轉講師: " + no + ". " + in_name + "(" + in_sno + ")");

                Item itmIdentity = cfg.inn.newItem("Identity", "get");
                itmIdentity.setProperty("in_user", in_user_id);
                itmIdentity = itmIdentity.apply();

                string identity_id = itmIdentity.getID();

                //此為申請館主之用,要把報名者加入 7AC8B33DA0F246DDA70BB244AB231E29 ACT_GymOwner
                Item itmMember = cfg.inn.newItem("Member", "merge");
                itmMember.setAttribute("where", "source_id='7AC8B33DA0F246DDA70BB244AB231E29' and related_id='" + identity_id + "'");
                itmMember.setProperty("source_id", "7AC8B33DA0F246DDA70BB244AB231E29");
                itmMember.setRelatedItem(itmIdentity);
                itmMember = itmMember.apply();

                //館主的 app start page 應該是開放賽事一覽
                string sql1 = @"UPDATE [User] SET in_app_page='pages/c.aspx?page=In_MeetingSearch_light.html&method=In_Dashboard_MeetingList_light' WHERE id = '" + in_user_id + "'";
                cfg.inn.applySQL(sql1);

                //更新講師履歷資料
                string sql2 = "UPDATE IN_RESUME SET in_is_teacher = '1' WHERE id = '" + resume_id + "'";
                cfg.inn.applySQL(sql2);
            }
        }

        //建立繳費單
        private void TransMPays(TConfig cfg, Item itmAResume)
        {
            var map = GetMUserGroups(cfg);
            var count = map.Count;

            //為了配合 In_Payment_List_Add AML 查詢，一定要把 繳費單號  更新成空白
            //否則繳費單號一定產生失敗!!!!!!!!

            string sql_update = @"UPDATE IN_MEETING_USER SET in_paynumber = N'' WHERE source_id = '" + cfg.meeting_id + "' AND in_paynumber IS NULL";
            cfg.inn.applySQL(sql_update);

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "建立繳費單: 共 " + count + " 筆");

            int no = 0;
            foreach (var kv in map)
            {
                no++;

                var pay = kv.Value;
                string current_orgs = string.Join(",", pay.current_orgs);
                string invoice_up = string.Join(",", pay.invoice_up);
                string uniform_numbers = string.Join(",", pay.uniform_numbers);
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "建立繳費單: " + no + ". " + pay.in_group + "(" + pay.in_creator_sno + ")");

                Item itmResume = cfg.inn.applySQL("SELECT * FROM IN_RESUME WITH(NOLOCK) WHERE in_sno = '" + pay.in_creator_sno + "'");
                string resume_id = itmResume.getProperty("id", "");

                Item itmMClose = cfg.inn.newItem();
                itmMClose.setType("In_Meeting_GymList");
                itmMClose.setProperty("meeting_id", pay.meeting_id);
                itmMClose.setProperty("in_group", pay.in_group);
                Item itmMCloseResult = itmMClose.apply("In_Close_GymReg");

                Item itmMPay = cfg.inn.newItem();
                itmMPay.setType("In_Meeting_Pay");
                itmMPay.setProperty("meeting_id", pay.meeting_id);
                itmMPay.setProperty("in_group", pay.in_group);
                itmMPay.setProperty("in_creator_sno", pay.in_creator_sno);
                itmMPay.setProperty("resume_id", resume_id);
                itmMPay.setProperty("current_orgs", "," + current_orgs);
                itmMPay.setProperty("invoice_up", "," + invoice_up);
                itmMPay.setProperty("uniform_numbers", "," + uniform_numbers);
                Item itmMPayResult = itmMPay.apply("In_Payment_List_Add2");


                string in_paynumber = itmMPayResult.getProperty("numbers", "");

                string sql_upd2 = "UPDATE " + cfg.muser_table + " SET"
                    + " in_paynumber = '" + in_paynumber + "'"
                    + " , memo = N'繳費單已建立'"
                    + " WHERE in_sno = '" + pay.in_creator_sno + "'";

                cfg.inn.applySQL(sql_upd2);
            }
        }

        private Dictionary<string, TPay> GetMUserGroups(TConfig cfg)
        {
            Dictionary<string, TPay> map = new Dictionary<string, TPay>();

            string sql = "SELECT DISTINCT in_group, in_creator_sno, in_current_org FROM IN_MEETING_USER WITH(NOLOCK)"
                + " WHERE source_id = '" + cfg.meeting_id + "'"
                + " AND ISNULL(in_paynumber, '') = ''"
                + " AND in_creator_sno IN (SELECT DISTINCT in_sno FROM A_MUser_20211210)"
                + "";

            Item items = cfg.inn.applySQL(sql);
            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_group = item.getProperty("in_group", "");
                string in_creator_sno = item.getProperty("in_creator_sno", "");
                string in_current_org = item.getProperty("in_current_org", "");
                string key = in_group + "-" + in_creator_sno;

                TPay pay = null;
                if (map.ContainsKey(key))
                {
                    pay = map[key];
                }
                else
                {
                    pay = new TPay
                    {
                        meeting_id = cfg.meeting_id,
                        in_group = in_group,
                        in_creator_sno = in_creator_sno,
                        current_orgs = new List<string>(),
                        invoice_up = new List<string>(),
                        uniform_numbers = new List<string>(),
                    };
                    map.Add(key, pay);
                }

                pay.current_orgs.Add(in_current_org);
                pay.invoice_up.Add("");
                pay.uniform_numbers.Add("");
            }

            return map;
        }

        //轉檔
        private void TransMUsers_2022(TConfig cfg, Item itmAResume, List<TMUser> entities)
        {
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "資料轉換: 共 " + entities.Count + " 筆");

            foreach (var entity in entities)
            {
                foreach (var in_l2 in entity.NoPaidTWYears)
                {
                    TransMUsers_2022(cfg, itmAResume, entity, in_l2);
                }
            }
        }

        private void TransMUsers_2022(TConfig cfg, Item itmAResume, TMUser entity, string in_l2)
        {
            //E100547101-110年個人會員/500
            string no = entity.no;
            string in_sno = entity.in_sno;
            string in_name = entity.in_name;
            string in_l1 = entity.in_l1;
            string in_mail = in_sno + "-" + in_l2;

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "資料轉換: " + no + ". " + in_name + "(" + in_sno + ") : " + in_l1 + ", " + in_l2);

            string sql1 = "SELECT TOP 1 * FROM IN_MEETING_USER WITH(NOLOCK)"
                + " WHERE source_id = '" + cfg.meeting_id + "'"
                + " AND in_mail = N'" + in_mail + "'";

            Item itmMUser = cfg.inn.applySQL(sql1);

            if (itmMUser.isError())
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, " - 取得與會者發生錯誤");
                return;
            }

            if (itmMUser.getResult() != "")
            {
                string sql2 = "UPDATE A_MUser_20211210 SET memo = N'exists' WHERE no = '" + entity.no + "'";
                cfg.inn.applySQL(sql2);
                return;
            }

            string memo = "";
            try
            {
                NewRegister(cfg, itmAResume, entity, in_l2, in_mail);
                memo = "ok";
            }
            catch (Exception ex)
            {
                memo = ex.Message;
            }

            itmMUser = cfg.inn.applySQL(sql1);
            if (!itmMUser.isError() && itmMUser.getResult() != "")
            {
                string muid = itmMUser.getProperty("id", "");
                UpdateMUser(cfg, entity, muid);
            }
            else
            {
                memo += ", 未建立與會者";
            }

            string sql3 = "UPDATE A_MUser_20210311 SET memo = N'" + memo.Replace("'", "\"") + "' WHERE no = '" + entity.no + "'";

            cfg.inn.applySQL(sql3);
        }

        /// <summary>
        /// 新建與會者 (註冊模式)
        /// </summary>
        private void NewRegister(TConfig cfg, Item itmParent, TMUser entity, string in_l2, string in_mail)
        {
            DateTime now = System.DateTime.Now;
            string in_reg_date = now.ToString("yyyy-MM-ddTHH:mm:ss");

            string survey_type = "1";
            string method = "register_meeting";

            Item itmEntity = entity.Value;
            itmEntity.setProperty("in_org", "0");
            itmEntity.setProperty("in_is_teacher", "0");
            itmEntity.setProperty("in_section_name", entity.in_section_name);
            itmEntity.setProperty("in_gameunit", in_l2);
            itmEntity.setProperty("in_l1", entity.in_l1);
            itmEntity.setProperty("in_l2", in_l2);
            itmEntity.setProperty("in_mail", in_mail);
            itmEntity.setProperty("in_reg_date", in_reg_date);

            List<Item> lstSurveys = GetMeetingSurveyList(cfg);
            string parameters = GetParamValues(lstSurveys, itmEntity);
            string isUserId = itmParent.getProperty("in_user_id", "");
            string isIndId = itmParent.getProperty("owned_by_id", "");

            string aml = ""
                + "<meeting_id>" + cfg.meeting_id + "</meeting_id>"
                + "<surveytype>" + survey_type + "</surveytype>"
                + "<agent_id>" + "</agent_id>"
                + "<email>" + in_mail + "</email>"
                + "<isUserId>" + isUserId + "</isUserId>"
                + "<isIndId>" + isIndId + "</isIndId>"
                + "<method>" + method + "</method>"
                + "<parameter>" + parameters + "</parameter>"
                + "";

            //CCO.Utilities.WriteDebug(strMethodName, "aml: " + aml);

            Item itmMethod = cfg.inn.applyMethod("In_meeting_register", aml);

            if (itmMethod.isError())
            {
                throw new Exception("創建成員帳號發生錯誤");
            }
        }

        /// <summary>
        /// 更新與會者
        /// </summary>
        private void UpdateMUser(TConfig cfg, TMUser entity, string muid)
        {
            string sql = @"
                UPDATE
                    IN_MEETING_USER
                SET
                    in_note = '{#in_note}'
                    , in_creator = N'{#in_creator}'
                    , in_creator_sno = N'{#in_creator_sno}'
                WHERE
                    id = '{#muid}'
            ";

            StringBuilder builder = new StringBuilder(sql);
            builder.Replace("{#muid}", muid);
            builder.Replace("{#in_note}", entity.no);
            builder.Replace("{#in_creator}", entity.in_name);
            builder.Replace("{#in_creator_sno}", entity.in_sno);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + builder.ToString());

            Item itmSQL = cfg.inn.applySQL(builder.ToString());

            if (itmSQL.isError())
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "更新與會者發生錯誤 _# sql: " + sql);
            }
        }

        /// <summary>
        /// 個人會員轉檔清單
        /// </summary>
        private List<TMUser> GetMUsersFromXLS_2022(TConfig cfg)
        {
            List<TMUser> entities = new List<TMUser>();
            try
            {
                string sql = "SELECT * FROM " + cfg.muser_table + " WITH(NOLOCK)"
                    + " WHERE resume_id IS NOT NULL"
                    + " AND ISNULL(memo, '') = ''"
                    + " ORDER BY [no]";

                Item items = cfg.inn.applySQL(sql);

                int count = items.getItemCount();

                for (int i = 0; i < count; i++)
                {
                    Item item = items.getItemByIndex(i);
                    string in_name = item.getProperty("in_name", "").Trim();
                    string in_sno = item.getProperty("in_sno", "").Trim().ToUpper();
                    string y107 = item.getProperty("y107", "").Trim();
                    string y108 = item.getProperty("y108", "").Trim();
                    string y109 = item.getProperty("y109", "").Trim();
                    string y110 = item.getProperty("y110", "").Trim();
                    string y111 = item.getProperty("y111", "").Trim();

                    var entity = entities.Find(x => x.in_sno == in_sno);
                    if (entity == null)
                    {
                        entity = new TMUser
                        {
                            no = item.getProperty("no", ""),
                            in_name = in_name,
                            in_sno = in_sno,
                            in_section_name = "個人會員-111年個人會員",
                            in_l1 = "個人會員",
                            Value = item,
                            NoPaidTWYears = new List<string>(),
                        };
                        entities.Add(entity);
                    }
                    else
                    {
                        throw new Exception(in_sno + ": 重複");
                    }

                    if (y107 == "") entity.NoPaidTWYears.Add("107年個人會員/500");
                    if (y108 == "") entity.NoPaidTWYears.Add("108年個人會員/500");
                    if (y109 == "") entity.NoPaidTWYears.Add("109年個人會員/500");
                    if (y110 == "") entity.NoPaidTWYears.Add("110年個人會員/500");
                    if (y111 == "") entity.NoPaidTWYears.Add("111年個人會員/500");
                }
            }
            catch (Exception ex)
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "取得 XLS 資料 _# error: " + ex.Message);
            }

            return entities;
        }

        ///// <summary>
        ///// 個人會員轉檔清單
        ///// </summary>
        //private List<TMUser> GetMUsersFromXLS_2021(TConfig cfg)
        //{
        //    List<TMUser> entities = new List<TMUser>();
        //    try
        //    {
        //        string sql = "SELECT * FROM " + cfg.muser_table + " WITH(NOLOCK)"
        //            + " WHERE resume_id IS NOT NULL"
        //            + " ORDER BY [no])";

        //        Item items = cfg.inn.applySQL(sql);
        //        int count = items.getItemCount();

        //        DateTime now = System.DateTime.Now;
        //        string in_reg_date = now.ToString("yyyy-MM-ddTHH:mm:ss");

        //        for (int i = 0; i < count; i++)
        //        {
        //            Item item = items.getItemByIndex(i);
        //            item.setProperty("in_org", "0");
        //            item.setProperty("in_is_teacher", "0");
        //            item.setProperty("in_gameunit", item.getProperty("in_l2", ""));
        //            item.setProperty("in_reg_date", in_reg_date);

        //            TMUser entity = new TMUser
        //            {
        //                no = item.getProperty("no", ""),
        //                in_sno = item.getProperty("in_sno", ""),
        //                in_mail = item.getProperty("in_sno", "") + "-" + item.getProperty("in_l2", ""),
        //                Value = item,
        //            };

        //            entities.Add(entity);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "取得 XLS 資料 _# error: " + ex.Message);
        //    }

        //    return entities;
        //}

        #region 資料存取

        /// <summary>
        /// 取得協會 Resume
        /// </summary>
        private Item GetAdminResume(TConfig cfg)
        {
            string sql = "SELECT TOP 1 * FROM IN_RESUME WITH(NOLOCK) WHERE in_sno = N'lwu001'";

            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// 取得問項清單
        /// </summary>
        private List<Item> GetMeetingSurveyList(TConfig cfg)
        {
            string sql = @"
                SELECT
                    t3.id,
                    t3.in_property
                FROM
                    IN_MEETING t1 WITH(NOLOCK)
                INNER JOIN
                    IN_MEETING_SURVEYS t2 WITH(NOLOCK) ON t2.source_id = t1.id
                INNER JOIN
                    IN_SURVEY t3 WITH(NOLOCK) ON t3.id = t2.related_id
                WHERE
                    t1.id = '{#meeting_id}'
                    AND ISNULL(in_property, '') <> ''
                ORDER BY
                    t2.sort_order
            ";
            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            Item items = cfg.inn.applySQL(sql);

            if (items.isError() || items.getItemCount() <= 0)
            {
                throw new Exception("創建成員帳號發生錯誤(問項)");
            }

            List<Item> list = new List<Item>();

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                list.Add(items.getItemByIndex(i));
            }

            return list;
        }

        #endregion 資料存取

        /// <summary>
        /// 組成 Url 參數
        /// </summary>
        /// <param name="list">問項清單</param>
        /// <param name="itmEntity">與會者資料</param>
        private string GetParamValues(List<Item> list, Item itmEntity)
        {
            string result = "";
            int count = list.Count;
            for (int i = 0; i < count; i++)
            {
                Item item = list[i];
                string id = item.getProperty("id", "");
                string property = item.getProperty("in_property", "");

                string value = itmEntity.getProperty(property, "");

                if (value == "")
                {
                    continue;
                }

                if (property == "in_birth")
                {
                    value = GetDateTimeValue(value, "yyyy-MM-dd", 8);
                }

                if (result != "")
                {
                    result += "&amp;";
                }

                result += id + "=" + value;
            }
            return result;
        }

        #region 資料結構

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public Innosoft.InnovatorHelper InnH { get; set; }

            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }

            public string meeting_id { get; set; }
            public string muser_table { get; set; }
        }

        private class TMUser
        {
            public string no { get; set; }
            public string in_name { get; set; }
            public string in_sno { get; set; }
            public string in_mail { get; set; }
            public string in_section_name { get; set; }
            public string in_l1 { get; set; }
            public Item Value { get; set; }
            public List<string> NoPaidTWYears { get; set; }
        }

        private class TPay
        {
            public string meeting_id { get; set; }
            public string in_group { get; set; }
            public string in_creator_sno { get; set; }
            public List<string> current_orgs { get; set; }
            public List<string> invoice_up { get; set; }
            public List<string> uniform_numbers { get; set; }

        }

        #endregion 資料結構

        private int GetIntValue(string value)
        {
            int result = 0;
            if (Int32.TryParse(value, out result))
            {
                return result;
            }
            else
            {
                return 0;
            }
        }

        private string GetDateTimeValue(string value, string format = "yyyy-MM-dd", int hours = 0)
        {
            if (value == null || value == "") return "";

            string day = value.Replace("/", "-");

            DateTime dt = DateTime.Now;
            if (DateTime.TryParse(day, out dt))
            {
                return dt.AddHours(hours).ToString(format);
            }
            else
            {
                return value;
            }
        }

        private string GetBirth(string value)
        {
            if (value == null || value == "") return "";

            string day = value.Replace("/", "-");

            DateTime dt = DateTime.Now;
            if (DateTime.TryParse(day, out dt))
            {
                return dt.AddHours(-8).ToString("yyyy/MM/dd HH:mm:ss");
            }
            else
            {
                return value;
            }
        }
    }
}