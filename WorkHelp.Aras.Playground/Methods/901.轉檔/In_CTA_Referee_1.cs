﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class In_CTA_Referee_1 : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
目的: 中華跆協資料轉檔
    - RP品勢裁判講習記錄
日期: 
    - 2021/11/23: 創建 (lina)
*/

            System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]In_CTA_Referee_1";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "input dom: " + itmR.dom.InnerXml);

            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            Item itmPermit = inn.applyMethod("In_CheckIdentity", "<method>" + strMethodName + "</method><code>" + "ALL" + "</code>");
            bool isMeetingAdmin = itmPermit.getProperty("isMeetingAdmin", "") == "1";

            if (!isMeetingAdmin) throw new Exception("無權限");

            string sql = "";

            //RP品勢裁判講習記錄: 1957
            sql = "SELECT * FROM IN_CTA_REFEREE_RP WITH(NOLOCK) WHERE ISNULL(resume_id, '') <> '' AND ISNULL(in_tw_year, 0) > 0 ORDER BY in_old_no";

            Item itmLogs = inn.applySQL(sql);

            int count = itmLogs.getItemCount();
            CCO.Utilities.WriteDebug(strMethodName, "轉檔開始-總筆數: " + count);

            for (int i = 0; i < count; i++)
            {
                Item itmLog = itmLogs.getItemByIndex(i);
                var is_ok = AddPoomsaeReferee(CCO, strMethodName, inn, itmLog, i);
                if (is_ok)
                {
                    AddPoomsaeYear(CCO, strMethodName, inn, itmLog);
                }
            }

            CCO.Utilities.WriteDebug(strMethodName, "轉檔結束");

            //把身分換回來
            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

            return itmR;
        }

        //新增品勢裁判講習記錄
        private bool AddPoomsaeReferee(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmLog, int rowIdx)
        {
            //Resume 品勢裁判講習記錄
            string item_type = "In_Resume_Referee_1";

            string in_old_no = itmLog.getProperty("in_old_no", "").ToUpper();
            string resume_id = itmLog.getProperty("resume_id", "");
            string in_name = itmLog.getProperty("in_name", "");
            string in_sno = itmLog.getProperty("in_sno", "");
            CCO.Utilities.WriteDebug(strMethodName, "    " + rowIdx + ". " + in_name + "(" + in_sno + ") _# old no = " + in_old_no);

            Item itmReferee = inn.newItem(item_type, "merge");
            itmReferee.setAttribute("where", "source_id = '" + resume_id + "' AND in_old_no = " + in_old_no);
            itmReferee.setProperty("source_id", resume_id);
            itmReferee.setProperty("in_old_no", in_old_no);
            itmReferee.setProperty("in_year", itmLog.getProperty("in_west_year", "").Trim());
            itmReferee.setProperty("in_level", itmLog.getProperty("in_level", "").Trim());
            itmReferee.setProperty("in_echelon", itmLog.getProperty("in_echelon", "").Trim());
            itmReferee.setProperty("in_evaluation", itmLog.getProperty("in_evaluation", "").Trim());
            itmReferee.setProperty("in_know_score", itmLog.getProperty("in_know_score", "").Trim());
            itmReferee.setProperty("in_tech_score", itmLog.getProperty("in_tech_score", "").Trim());
            itmReferee.setProperty("in_note", itmLog.getProperty("in_note", "").Trim());

            string in_certificate = itmLog.getProperty("in_certificate", "").ToUpper();
            if (in_certificate == "1" || in_certificate == "TRUE")
            {
                itmReferee.setProperty("in_certificate", "1");
            }
            else
            {
                itmReferee.setProperty("in_certificate", "0");
            }
            itmReferee = itmReferee.apply();

            if (itmReferee.isError())
            {
                CCO.Utilities.WriteDebug(strMethodName, "      - 發生錯誤");
                return false;
            }
            else
            {
                return true;
            }
        }

        //新增品勢年度
        private bool AddPoomsaeYear(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmLog)
        {
            string resume_id = itmLog.getProperty("resume_id", "");
            string in_type = "poomsae";
            string in_west_year = itmLog.getProperty("in_west_year", "");

            Item itmYear = inn.newItem("In_Resume_Years", "merge");
            itmYear.setAttribute("where", "source_id = '" + resume_id + "' AND in_type = '" + in_type + "' AND in_year = '" + in_west_year + "'");
            itmYear.setProperty("source_id", resume_id);
            itmYear.setProperty("in_type", in_type);
            itmYear.setProperty("in_year", in_west_year);

            itmYear = itmYear.apply();

            if (itmYear.isError())
            {
                CCO.Utilities.WriteDebug(strMethodName, "      - 發生錯誤");
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}