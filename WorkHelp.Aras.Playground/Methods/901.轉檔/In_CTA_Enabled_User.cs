﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class In_CTA_Enabled_User : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]In_CTA_Enabled_User";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            string sql = @"
                SELECT
	                t1.in_name
	                , t1.login_name
                FROM 
	                IN_RESUME t1 WITH(NOLOCK) 
                INNER JOIN 
	                [USER] t2 WITH(NOLOCK)
	                ON t2.login_name = t1.login_name
                WHERE
	                t2.logon_enabled = 0
            ";

            Item items = inn.applySQL(sql);

            int count = items.getItemCount();

            CCO.Utilities.WriteDebug(strMethodName, "未啟用帳號 _# 總筆數: " + count.ToString());

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_name = item.getProperty("in_name", "");
                string login_name = item.getProperty("login_name", "");
                CCO.Utilities.WriteDebug(strMethodName, "未啟用帳號 _# 啟用帳號: " + (i + 1).ToString() + ". " + in_name + "(" + login_name + ")");

                Item itmIdentity = inn.newItem("Identity", "get");
                itmIdentity.setProperty("in_number", login_name);
                itmIdentity = itmIdentity.apply();

                //此為申請館主之用,要把報名者加入 7AC8B33DA0F246DDA70BB244AB231E29 ACT_GymOwner
                Item itmMember = inn.newItem("Member", "merge");
                itmMember.setAttribute("where", "source_id='7AC8B33DA0F246DDA70BB244AB231E29' and related_id='" + itmIdentity.getID() + "'");
                itmMember.setProperty("source_id", "7AC8B33DA0F246DDA70BB244AB231E29");
                itmMember.setRelatedItem(itmIdentity);
                itmMember = itmMember.apply();

                sql = "UPDATE [USER] SET logon_enabled = '1' WHERE login_name = '" + login_name + "'";
                inn.applySQL(sql);

                sql = "UPDATE IN_RESUME SET in_is_teacher = '1' WHERE login_name = '" + login_name + "'";
                inn.applySQL(sql);
            }

            return itmR;
        }
    }
}