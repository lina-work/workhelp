﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class In_CTA_Promotion : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
            目的: 中華跆協-晉段資料中繼檔
            日期: 
                - 2022/01/19: 晉段紀錄重整 (lina)
                - 2021/01/12: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]In_Cta_Promotion";

            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            Item itmR = this;


            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),

                scene = itmR.getProperty("scene", ""),
                rawCount = 20000,
                ACCESS_DATABASE_PATH = @"C:\app\PLMCTA\中華跆協系統資料庫.accdb",
            };

            cfg.scene = "ntk_b_all";

            cfg.ACCESS_CONNECTION_STRING = GetAccessConnString(cfg.ACCESS_DATABASE_PATH);

            switch (cfg.scene)
            {
                case "ntk_b_one":
                    TransNTK_B_ONE(cfg);
                    break;

                case "ntk_b_all":
                    TransNTK_B_ALL(cfg);
                    break;

                case "ntk":
                    TransNTK(cfg);
                    break;
            }

            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

            return itmR;
        }

        //同步晉段紀錄
        private void TransNTK_B_ALL(TConfig cfg)
        {
            string sql = @"
                SELECT
	                t2.id AS 'resume_id'
	                , t1.in_sno
	                , t1.in_name
	                , t1.in_acs_no
                FROM 
	                IN_CTA_PROMOTION t1 WITH(NOLOCK)
                INNER JOIN
	                IN_RESUME t2 WITH(NOLOCK)
	                ON t2.in_sno = t1.in_sno
                LEFT JOIN
	                IN_ERR_SNO t3 WITH(NOLOCK)
	                ON t3.in_sno = t1.in_sno
                WHERE 
	                t3.in_sno IS NULL
            ";

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "[晉段紀錄]資料轉換: 共 " + count + " 筆");

            Dictionary<int, List<Item>> map = NTK_B_List_ALL(cfg);

            if (map.Count == 0)
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "查無紀錄");
                return;
            }
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "[晉段紀錄] NTK-B: 共 " + map.Count + " 筆 (有 in_acs_no)");

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);

                string no = (i + 1).ToString();
                string in_sno = item.getProperty("in_sno", "");
                string in_name = item.getProperty("in_name", "");
                string in_acs_no = item.getProperty("in_acs_no", "");

                string page = ((i / 3000) + 1).ToString().PadLeft(5, '0');

                string log_name = cfg.strMethodName + "_" + page + "_";

                cfg.CCO.Utilities.WriteDebug(log_name
                    , "[晉段紀錄]資料轉換開始: " + no + ". " + in_name + "(" + in_sno + ")" + "(" + in_acs_no + ")");

                if (in_acs_no == "" || in_acs_no == "0")
                {
                    cfg.CCO.Utilities.WriteDebug(log_name, "            - 無晉段紀錄 _# in_acs_no 為空");
                    continue;
                }

                int acs_no = GetIntValue(in_acs_no);
                if (acs_no <= 0)
                {
                    cfg.CCO.Utilities.WriteDebug(log_name, "            - 無晉段紀錄 _# in_acs_no 轉型失敗");
                    continue;
                }

                if (!map.ContainsKey(acs_no))
                {
                    cfg.CCO.Utilities.WriteDebug(log_name, "            - 無晉段紀錄 _# ACCESS 無資料");
                    continue;
                }

                List<Item> itmPromotions = map[acs_no];
                MergeNTK_B_ALL(cfg, item, itmPromotions);
                cfg.CCO.Utilities.WriteDebug(log_name, "            - 已整合晉段紀錄");
            }

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "[晉段紀錄]資料轉換 _# 結束 ");
        }

        private void MergeNTK_B_ALL(TConfig cfg, Item item, List<Item> itmPromotions)
        {
            string resume_id = item.getProperty("resume_id", "");
            string in_acs_no = item.getProperty("in_acs_no", "");

            string sql_delete = "DELETE FROM IN_RESUME_PROMOTION WHERE source_id = '" + resume_id + "'";
            cfg.inn.applySQL(sql_delete);

            for (int i = 0; i < itmPromotions.Count; i++)
            {
                Item itmPromotion = itmPromotions[i];
                itmPromotion.setProperty("source_id", resume_id);
                itmPromotion = itmPromotion.apply("add");
            }
        }

        //同步晉段紀錄
        private void TransNTK_B_ONE(TConfig cfg)
        {
            // string sql = @"
            //     SELECT
            //     DISTINCT t2.id AS 'resume_id'
            //     , t3.in_sno
            //     , t3.in_name
            //     , t3.in_acs_no
            //     FROM
            //     IN_CLA_MEETING_USER t1 WITH(NOLOCK)
            //     INNER JOIN
            //     IN_RESUME t2 WITH(NOLOCK)
            //     ON t2.in_sno = t1.in_sno
            //     INNER JOIN
            //     IN_CTA_PROMOTION t3 WITH(NOLOCK)
            //     ON t3.in_sno = t1.in_sno
            //     WHERE
            //     t1.source_id = '6A498778DE2A4258AAD31871FCAF00B0'
            // ";

            string sql = "SELECT t1.id AS 'resume_id', t2.in_sno, t2.in_name, t2.in_acs_no FROM IN_RESUME t1 WITH(NOLOCK)"
                + " INNER JOIN IN_CTA_PROMOTION t2 WITH(NOLOCK) ON t2.in_sno = t1.in_sno"
                + " WHERE t2.in_sno = 'A127313669'";

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "[晉段紀錄]資料轉換: 共 " + count + " 筆");

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);

                string no = (i + 1).ToString();
                string in_sno = item.getProperty("in_sno", "");
                string in_name = item.getProperty("in_name", "");
                string in_acs_no = item.getProperty("in_acs_no", "");

                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName
                    , "[晉段紀錄]資料轉換開始: " + no + ". " + in_name + "(" + in_sno + ")" + "(" + in_acs_no + ")");

                MergeNTK_B(cfg, item);
            }

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "[晉段紀錄]資料轉換 _# 結束 ");
        }

        private void MergeNTK_B(TConfig cfg, Item item)
        {
            string resume_id = item.getProperty("resume_id", "");
            string in_acs_no = item.getProperty("in_acs_no", "");

            string sql_delete = "DELETE FROM IN_RESUME_PROMOTION WHERE source_id = '" + resume_id + "'";
            cfg.inn.applySQL(sql_delete);

            //晉段記錄 (無法使用 merge，資料欄位無法組成唯一性)
            List<Item> itmPromotions = NTK_B_List_ONE(cfg, in_acs_no);

            if (itmPromotions.Count == 0) return;

            for (int i = 0; i < itmPromotions.Count; i++)
            {
                Item itmPromotion = itmPromotions[i];
                itmPromotion.setProperty("source_id", resume_id);
                itmPromotion = itmPromotion.apply("add");
            }
        }

        //同步晉段資料
        private void TransNTK(TConfig cfg)
        {
            List<Item> itmEntities = NTK_List(cfg);

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "[晉段]資料轉換: 共 " + itmEntities.Count + " 筆");

            int no = 1;

            foreach (var itmEntity in itmEntities)
            {
                string in_acs_no = itmEntity.getProperty("in_acs_no", "");
                string in_name = itmEntity.getProperty("in_name", "");
                string in_sno = itmEntity.getProperty("in_sno", "");

                //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName
                //    , "[晉段]資料轉換開始: " + no + ". " + in_name + "(" + in_sno + ")" + "(" + in_acs_no + ")");

                itmEntity.setAttribute("where", "in_acs_no='" + itmEntity.getProperty("in_acs_no", "") + "'");
                Item itmSQL = itmEntity.apply("merge");

                if (itmSQL.isError())
                {
                    cfg.CCO.Utilities.WriteDebug(cfg.strMethodName
                        , "[晉段]資料轉換失敗: " + no + ". " + in_name + "(" + in_sno + ")" + "(" + in_acs_no + ")");
                    throw new Exception("資料轉換失敗");
                }

                no++;
            }

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "[晉段]資料轉換 _# 結束 ");
        }

        /// <summary>
        /// 晉段資料轉檔清單
        /// </summary>
        private List<Item> NTK_List(TConfig cfg)
        {
            List<Item> itmEntities = new List<Item>();
            try
            {
                int min_no = 1;
                int max_no = cfg.rawCount + min_no - 1;

                Item itmCTA = cfg.inn.applySQL("SELECT MAX(in_acs_no) AS 'in_acs_no' FROM IN_CTA_PROMOTION WITH(NOLOCK)");
                if (!itmCTA.isError() && itmCTA.getResult() != "")
                {
                    int in_acs_no = GetIntValue(itmCTA.getProperty("in_acs_no", "0"));
                    if (in_acs_no > 0)
                    {
                        min_no = in_acs_no + 1;
                        max_no = min_no + cfg.rawCount - 1;
                    }
                }

                string sql = @"SELECT * FROM [NTK] WHERE [會員編號] >= {#min_no} AND [會員編號] <= {#max_no} ORDER BY [會員編號]"
                    .Replace("{#min_no}", min_no.ToString())
                    .Replace("{#max_no}", max_no.ToString());


                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "取得 Access 資料: " + sql);

                var dt = GetDataTable(sql, cfg.ACCESS_CONNECTION_STRING);
                if (dt == null || dt.Rows.Count == 0)
                {
                    cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "取得 Access 資料: 無資料");
                    return itmEntities;
                }
                else
                {
                    cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "取得 Access 資料: rawCount: " + dt.Rows.Count.ToString());
                }

                string[] date_cols = new string[]
                {
                    "in_west_birth",
                    "in_tw_birth",
                    "in_gl_degree_date",
                    "in_l1_date",
                    "in_l2_date",
                    "in_l3_date",
                    "in_l4_date",
                    "in_l5_date",
                    "in_l6_date",
                    "in_l7_date",
                };

                DateTime dtMin = new DateTime(1911, 1, 1);
                DateTime dtMax = new DateTime(2021, 12, 31);

                foreach (System.Data.DataRow dr in dt.Rows)
                {
                    Item itmEntity = cfg.inn.newItem("In_Cta_Promotion");
                    itmEntity.setProperty("in_acs_no", GetColVal(dr, "會員編號"));
                    itmEntity.setProperty("in_ntk_no", GetColVal(dr, "編號"));
                    itmEntity.setProperty("in_name", GetColVal(dr, "中文姓名"));
                    itmEntity.setProperty("in_en_name", GetColVal(dr, "英文姓名"));
                    itmEntity.setProperty("in_west_birth", GetDateTimeValue(GetColVal(dr, "出生日期(西)"), "yyyy-MM-ddTHH:mm:ss"));
                    itmEntity.setProperty("in_tw_birth", GetDateTimeValue(GetColVal(dr, "出生日期(民)"), "yyyy-MM-ddTHH:mm:ss"));
                    itmEntity.setProperty("in_sno", GetColVal(dr, "身分證字號"));
                    itmEntity.setProperty("in_gender", GetColVal(dr, "性別"));
                    itmEntity.setProperty("in_country", GetColVal(dr, "國籍"));
                    itmEntity.setProperty("in_job", GetColVal(dr, "職業"));
                    itmEntity.setProperty("in_tel_1", GetColVal(dr, "家用電話"));
                    itmEntity.setProperty("in_tel", GetColVal(dr, "行動電話"));
                    itmEntity.setProperty("in_resident_add_code", GetColVal(dr, "郵遞區號"));
                    itmEntity.setProperty("in_resident_add", GetColVal(dr, "戶籍地址"));
                    itmEntity.setProperty("in_email", GetColVal(dr, "EMAIL"));
                    itmEntity.setProperty("in_education", GetColVal(dr, "學歷"));
                    itmEntity.setProperty("in_area", GetColVal(dr, "出生地"));
                    itmEntity.setProperty("in_guardian", GetColVal(dr, "監護人"));
                    itmEntity.setProperty("in_stuff_c1", GetColVal(dr, "所屬教練"));
                    itmEntity.setProperty("in_current_org", GetColVal(dr, "訓練地點"));
                    itmEntity.setProperty("in_stuff_c2", GetColVal(dr, "現任教練"));
                    itmEntity.setProperty("in_committee", GetColVal(dr, "所屬委員會"));
                    itmEntity.setProperty("in_degree", GetIntValue(GetColVal(dr, "段位")).ToString());
                    itmEntity.setProperty("in_degree_id", GetColVal(dr, "國內證號"));
                    itmEntity.setProperty("in_gl_degree_id", GetColVal(dr, "國際證號"));
                    itmEntity.setProperty("in_gl_degree_date", GetDateTimeValue(GetColVal(dr, "國際日期"), "yyyy-MM-ddTHH:mm:ss"));
                    itmEntity.setProperty("in_l1_date", GetDateTimeValue(GetColVal(dr, "L1_DATE"), "yyyy-MM-ddTHH:mm:ss"));
                    itmEntity.setProperty("in_l1_no", GetColVal(dr, "L1_NO"));
                    itmEntity.setProperty("in_l2_date", GetDateTimeValue(GetColVal(dr, "L2_DATE"), "yyyy-MM-ddTHH:mm:ss"));
                    itmEntity.setProperty("in_l2_no", GetColVal(dr, "L2_NO"));
                    itmEntity.setProperty("in_l3_date", GetDateTimeValue(GetColVal(dr, "L3_DATE"), "yyyy-MM-ddTHH:mm:ss"));
                    itmEntity.setProperty("in_l3_no", GetColVal(dr, "L3_NO"));
                    itmEntity.setProperty("in_l4_date", GetDateTimeValue(GetColVal(dr, "L4_DATE"), "yyyy-MM-ddTHH:mm:ss"));
                    itmEntity.setProperty("in_l4_no", GetColVal(dr, "L4_NO"));
                    itmEntity.setProperty("in_l5_date", GetDateTimeValue(GetColVal(dr, "L5_DATE"), "yyyy-MM-ddTHH:mm:ss"));
                    itmEntity.setProperty("in_l5_no", GetColVal(dr, "L5_NO"));
                    itmEntity.setProperty("in_l6_date", GetDateTimeValue(GetColVal(dr, "L6_DATE"), "yyyy-MM-ddTHH:mm:ss"));
                    itmEntity.setProperty("in_l6_no", GetColVal(dr, "L6_NO"));
                    itmEntity.setProperty("in_l7_date", GetDateTimeValue(GetColVal(dr, "L7_DATE"), "yyyy-MM-ddTHH:mm:ss"));
                    itmEntity.setProperty("in_l7_no", GetColVal(dr, "L7_NO"));
                    itmEntity.setProperty("in_other", GetColVal(dr, "other"));
                    itmEntity.setProperty("in_backup", GetColVal(dr, "備用"));

                    string in_sync_memo = "";
                    foreach (string date_col in date_cols)
                    {
                        string value = itmEntity.getProperty(date_col, "");
                        if (value == "")
                        {
                            continue;
                        }

                        DateTime dt_val = GetDateTime(value);

                        if (dt_val < dtMin || dt_val > dtMax)
                        {
                            if (in_sync_memo != "") in_sync_memo += ", ";
                            in_sync_memo += date_col + ":值異常";
                            itmEntity.setProperty(date_col, "");
                        }
                    }

                    string in_acs_no = itmEntity.getProperty("in_acs_no", "");
                    switch (in_acs_no)
                    {
                        case "36534":
                            if (in_sync_memo != "") in_sync_memo += ", ";
                            in_sync_memo += "in_ntk_no" + ":值異常";
                            itmEntity.setProperty("in_ntk_no", "");
                            break;

                        default:
                            break;
                    }

                    itmEntity.setProperty("in_sync_memo", in_sync_memo);

                    itmEntities.Add(itmEntity);
                }
            }
            catch (Exception ex)
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "取得 Access 資料 _# error: " + ex.Message);
            }
            return itmEntities;
        }

        /// <summary>
        /// 取得晉段記錄
        /// </summary>
        private Dictionary<int, List<Item>> NTK_B_List_ALL(TConfig cfg)
        {
            var map = new Dictionary<int, List<Item>>();
            try
            {
                string sql = @"SELECT * FROM [NTK-B]";

                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "取得 Access 資料: " + sql);

                var dt = GetDataTable(sql, cfg.ACCESS_CONNECTION_STRING);

                if (dt == null || dt.Rows.Count == 0)
                {
                    cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "取得 Access 資料: 無資料");
                    return map;
                }
                else
                {
                    cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "取得 Access 資料: rawCount: " + dt.Rows.Count.ToString());
                }

                string[] date_cols = new string[]
                {
                    "in_date"
                };

                DateTime dtMin = new DateTime(1911, 1, 1);
                DateTime dtMax = new DateTime(2021, 12, 31);

                foreach (System.Data.DataRow dr in dt.Rows)
                {
                    Item itmEntity = cfg.inn.newItem("In_Resume_Promotion");

                    string b_acs_no = GetColVal(dr, "會員編號");
                    if (b_acs_no == "" || b_acs_no == "0") continue;

                    int acs_no = GetIntValue(b_acs_no);
                    if (acs_no <= 0) continue;

                    itmEntity.setProperty("in_acs_no", b_acs_no);
                    itmEntity.setProperty("in_echelon", GetColVal(dr, "梯次"));
                    itmEntity.setProperty("in_manager_area", GetColVal(dr, "區別"));
                    itmEntity.setProperty("in_date", GetDateTimeValue(GetColVal(dr, "晉升日期"), "yyyy-MM-ddTHH:mm:ss"));
                    itmEntity.setProperty("in_apply_degree", GetColVal(dr, "申請段別") + "000");
                    itmEntity.setProperty("in_is_global", GetBoolValue(GetColVal(dr, "晉段審核")));
                    itmEntity.setProperty("in_note", GetColVal(dr, "備註"));
                    itmEntity.setProperty("in_sno", GetColVal(dr, "身份字號"));

                    string in_sync_memo = "";
                    foreach (string date_col in date_cols)
                    {
                        string value = itmEntity.getProperty(date_col, "");
                        if (value == "")
                        {
                            continue;
                        }

                        DateTime dt_val = GetDateTime(value);

                        if (dt_val < dtMin || dt_val > dtMax)
                        {
                            if (in_sync_memo != "") in_sync_memo += ", ";
                            in_sync_memo += date_col + ":值異常";
                            itmEntity.setProperty(date_col, "");
                        }
                    }

                    itmEntity.setProperty("in_sync_memo", in_sync_memo);

                    if (map.ContainsKey(acs_no))
                    {
                        map[acs_no].Add(itmEntity);
                    }
                    else
                    {
                        map.Add(acs_no, new List<Item> { itmEntity });
                    }
                }
            }
            catch (Exception ex)
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "取得 Access 資料 _# error: " + ex.Message);
            }

            return map;
        }

        /// <summary>
        /// 取得晉段記錄
        /// </summary>
        private List<Item> NTK_B_List_ONE(TConfig cfg, string in_acs_no)
        {
            List<Item> itmEntities = new List<Item>();
            try
            {
                string sql = @"SELECT * FROM [NTK-B] WHERE [會員編號] = " + in_acs_no;

                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "取得 Access 資料: " + sql);

                var dt = GetDataTable(sql, cfg.ACCESS_CONNECTION_STRING);

                if (dt == null || dt.Rows.Count == 0)
                {
                    cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "取得 Access 資料: 無資料");
                    return itmEntities;
                }
                else
                {
                    cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "取得 Access 資料: rawCount: " + dt.Rows.Count.ToString());
                }

                string[] date_cols = new string[]
                {
                    "in_date"
                };

                DateTime dtMin = new DateTime(1911, 1, 1);
                DateTime dtMax = new DateTime(2021, 12, 31);

                foreach (System.Data.DataRow dr in dt.Rows)
                {
                    Item itmEntity = cfg.inn.newItem("In_Resume_Promotion");
                    itmEntity.setProperty("in_acs_no", GetColVal(dr, "會員編號"));
                    itmEntity.setProperty("in_echelon", GetColVal(dr, "梯次"));
                    itmEntity.setProperty("in_manager_area", GetColVal(dr, "區別"));
                    itmEntity.setProperty("in_date", GetDateTimeValue(GetColVal(dr, "晉升日期"), "yyyy-MM-ddTHH:mm:ss"));
                    itmEntity.setProperty("in_apply_degree", GetColVal(dr, "申請段別") + "000");
                    itmEntity.setProperty("in_is_global", GetBoolValue(GetColVal(dr, "晉段審核")));
                    itmEntity.setProperty("in_note", GetColVal(dr, "備註"));
                    itmEntity.setProperty("in_sno", GetColVal(dr, "身份字號"));

                    string in_sync_memo = "";
                    foreach (string date_col in date_cols)
                    {
                        string value = itmEntity.getProperty(date_col, "");
                        if (value == "")
                        {
                            continue;
                        }

                        DateTime dt_val = GetDateTime(value);

                        if (dt_val < dtMin || dt_val > dtMax)
                        {
                            if (in_sync_memo != "") in_sync_memo += ", ";
                            in_sync_memo += date_col + ":值異常";
                            itmEntity.setProperty(date_col, "");
                        }
                    }

                    itmEntity.setProperty("in_sync_memo", in_sync_memo);

                    itmEntities.Add(itmEntity);
                }
            }
            catch (Exception ex)
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "取得 Access 資料 _# error: " + ex.Message);
            }
            return itmEntities;
        }

        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }

            public string scene { get; set; }
            public int rawCount { get; set; }

            /// <summary>
            /// Access 資料庫所在路徑
            /// </summary>
            public string ACCESS_DATABASE_PATH { get; set; }

            /// <summary>
            /// Access 資料庫連線字串
            /// </summary>
            public string ACCESS_CONNECTION_STRING { get; set; }
        }

        private void Examine(TConfig cfg)
        {
            //檢查格式A~Z(1) 0~9(9)(台灣身分證)
            var regex = new System.Text.RegularExpressions.Regex("^[A-Z]{1}[0-9]{9}$");
            //檢查格式A~Z(1)A~Z(2) 0~9(8)(居留證)
            var pat = new System.Text.RegularExpressions.Regex("^[A-Z]{1}[A-Z]{1}[0-9]{8}$");
            //檢查格式A~Z(1)A~Z(2) 0~9(6)(外國護照)
            var pas = new System.Text.RegularExpressions.Regex("^[A-Z]{1}[A-Z]{1}[0-9]{6}$");

            string sql = "SELECT * FROM IN_CTA_PROMOTION WITH(NOLOCK) WHERE in_check_flag = 0 ORDER BY in_acs_no";

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();

            int no = 1;

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "[全部檢查]總筆數: " + count);

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string id = item.getProperty("id", "");
                string in_acs_no = item.getProperty("in_acs_no", "");
                string in_name = item.getProperty("in_name", "");
                string in_sno = item.getProperty("in_sno", "");
                string in_gender = item.getProperty("in_gender", "");

                string flag = "0";
                string errs = "";

                int sno_len = in_sno.Length;
                switch (sno_len)
                {
                    case 8: //外國護照
                        if (!pas.IsMatch(in_sno))
                        {
                            flag = "8";
                            errs = "護照格式錯誤";
                        }
                        break;

                    case 10:
                        if (regex.IsMatch(in_sno)) //身分證
                        {
                            if (IsValidSid(in_sno))
                            {
                                char gender_code = in_sno[1];
                                if (Char.IsLetter(gender_code))
                                {
                                    //身分證號第二個字元為字母
                                }
                                else if (gender_code == '1' && (in_gender == "F" || in_gender == "女"))
                                {
                                    flag = "10";
                                    errs = "性別錯誤";
                                }
                                else if (gender_code == '2' && (in_gender == "M" || in_gender == "男"))
                                {
                                    flag = "10";
                                    errs = "性別錯誤";
                                }
                            }
                            else
                            {
                                flag = "10";
                                errs = "身分證格式錯誤";
                            }
                        }
                        else if (pat.IsMatch(in_sno)) //居留證
                        {
                            errs = CheckResident(in_sno);
                            if (errs != "")
                            {
                                flag = "10";
                            }
                        }
                        else
                        {
                            flag = "10";
                            errs = "證號格式錯誤";
                        }
                        break;

                    case 11://港澳居留，不檢查
                        flag = "0";
                        errs = "港澳居留，不檢查";
                        break;

                    case 12://港澳居留，不檢查
                        flag = "0";
                        errs = "港澳居留，不檢查";
                        break;

                    case 15: //中國身分證
                        var new_id = Per15To18(in_sno);
                        errs = CheckIDCards(new_id);
                        if (errs != "")
                        {
                            flag = "15";
                        }
                        break;

                    case 18://中國身分證
                        errs = CheckIDCards(in_sno);
                        if (errs != "")
                        {
                            flag = "18";
                        }
                        break;

                    default:
                        flag = "4";
                        errs = "證號碼數錯誤";
                        break;
                }


                if (flag == "0")
                {
                    cfg.CCO.Utilities.WriteDebug(cfg.strMethodName
                        , "[全部檢查]檢查　通過: " + no + ". " + in_name + " (" + in_sno + ")" + " (" + in_acs_no + ")");
                }
                else
                {
                    cfg.CCO.Utilities.WriteDebug(cfg.strMethodName
                        , "[全部檢查]檢查不通過: " + no + ". " + in_name + " (" + in_sno + ")" + " (" + in_acs_no + "): " + errs);
                }

                sql = "UPDATE IN_CTA_PROMOTION SET in_check_flag = '" + flag + "', in_check_memo = N'" + errs + "'"
                    + " WHERE id = '" + id + "'";

                Item itmSQL = cfg.inn.applySQL(sql);

                if (itmSQL.isError())
                {
                    cfg.CCO.Utilities.WriteDebug(cfg.strMethodName
                        , "[全部檢查]更新失敗: " + in_name + " (" + in_sno + ")" + " (" + in_acs_no + ")");
                }

                no++;
            }

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "[全部檢查]結束");

        }

        private void AppendMembers(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            string gym_resume_id = itmReturn.getProperty("resume_id", "");

            Item itmGym = inn.newItem("In_Resume", "get");
            itmGym.setProperty("id", gym_resume_id);
            itmGym = itmGym.apply();

            if (itmGym.isError() || itmGym.getResult() == "")
            {
                throw new Exception("查無 Resume 資料");
            }

            Item itmMembers = GetMembers(CCO, strMethodName, inn, itmGym);

            if (itmMembers.isError())
            {
                return;
            }

            int count = itmMembers.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item itmMember = itmMembers.getItemByIndex(i);
                string resume_id = itmMember.getProperty("resume_id", "");

                Item item = inn.newItem();
                item.setType("inn_memeber");
                item.setProperty("id", itmMember.getProperty("id", ""));
                item.setProperty("no", (i + 1).ToString());
                item.setProperty("in_name", itmMember.getProperty("in_name", ""));
                item.setProperty("in_en_name", itmMember.getProperty("in_en_name", ""));
                item.setProperty("in_sno", GetSidDisplay(itmMember.getProperty("in_sno", "")));
                item.setProperty("in_gender", GetGender(itmMember.getProperty("in_gender", "")));
                item.setProperty("in_current_org", itmMember.getProperty("in_current_org", ""));
                item.setProperty("in_committee", itmMember.getProperty("in_committee", ""));
                item.setProperty("in_check_memo", itmMember.getProperty("in_check_memo", ""));

                if (resume_id == "")
                {
                    item.setProperty("resume_status", "未過檔");
                }
                else
                {
                    item.setProperty("resume_status", "<a target='_blank' href='c.aspx?page=SingleMemberView.html&method=In_Group_Resume_View&id=" + resume_id + "'>已過檔</a>");
                }

                itmReturn.addRelationship(item);
            }

            itmReturn.setProperty("member_count", count <= 0 ? "0" : count.ToString());
        }

        private void AppendMember(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            string in_sno = itmReturn.getProperty("in_sno", "");

            string sql = @"
                SELECT
	                t1.*
	                , t2.id AS 'resume_id'
                FROM
	                IN_CTA_PROMOTION t1 WITH(NOLOCK)
                LEFT OUTER JOIN
	                IN_RESUME t2 WITH(NOLOCK)
	                ON t2.in_sno = t1.in_sno
                WHERE
	                t1.in_sno = N'{#in_sno}'
            ";

            sql = sql.Replace("{#in_sno}", in_sno);

            Item itmMembers = inn.applySQL(sql);

            if (itmMembers.isError())
            {
                return;
            }

            int count = itmMembers.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item itmMember = itmMembers.getItemByIndex(i);
                string resume_id = itmMember.getProperty("resume_id", "");

                Item item = inn.newItem();
                item.setType("inn_memeber");
                item.setProperty("id", itmMember.getProperty("id", ""));
                item.setProperty("no", (i + 1).ToString());
                item.setProperty("in_name", itmMember.getProperty("in_name", ""));
                item.setProperty("in_en_name", itmMember.getProperty("in_en_name", ""));
                item.setProperty("in_sno", GetSidDisplay(itmMember.getProperty("in_sno", "")));
                item.setProperty("in_gender", GetGender(itmMember.getProperty("in_gender", "")));
                item.setProperty("in_current_org", itmMember.getProperty("in_current_org", ""));
                item.setProperty("in_committee", itmMember.getProperty("in_committee", ""));
                item.setProperty("in_check_memo", itmMember.getProperty("in_check_memo", ""));

                if (resume_id == "")
                {
                    item.setProperty("resume_status", "未過檔");
                }
                else
                {
                    item.setProperty("resume_status", "<a target='_blank' href='c.aspx?page=SingleMemberView.html&method=In_Group_Resume_View&id=" + resume_id + "'>已過檔</a>");
                }

                itmReturn.addRelationship(item);
            }

            itmReturn.setProperty("member_count", count <= 0 ? "0" : count.ToString());
        }

        private Item GetMembers(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmGym)
        {
            string resume_id = itmGym.getProperty("id", "");
            string resume_name = itmGym.getProperty("in_name", "");
            string in_committee = itmGym.getProperty("in_manager_name", "");

            string sql = @"
                SELECT
	                t1.*
	                , t2.id AS 'resume_id'
                FROM
	                IN_CTA_PROMOTION t1 WITH(NOLOCK)
                LEFT OUTER JOIN
	                IN_RESUME t2 WITH(NOLOCK)
	                ON t2.in_sno = t1.in_sno
                WHERE
	                t1.in_committee = N'{#in_committee}'
	                AND t1.in_current_org = N'{#resume_name}'
	            ORDER BY
	                t1.in_sno
            ";

            sql = sql.Replace("{#in_committee}", in_committee)
                .Replace("{#resume_name}", resume_name);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return inn.applySQL(sql);
        }

        private void AppendCommitteeGyms(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            Item itmGyms = GetCommitteeGyms(CCO, strMethodName, inn, itmReturn);

            Dictionary<string, string> map = GetResumeRoleCount(CCO, strMethodName, inn);

            int count = itmGyms.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item itmGym = itmGyms.getItemByIndex(i);
                string resume_id = itmGym.getProperty("id", "");

                Item item = inn.newItem();
                item.setType("inn_gym");
                item.setProperty("id", resume_id);
                item.setProperty("in_name", itmGym.getProperty("in_name", ""));
                item.setProperty("in_sno", itmGym.getProperty("in_sno", ""));
                item.setProperty("in_password_plain", itmGym.getProperty("in_password_plain", ""));
                item.setProperty("in_manager_name", itmGym.getProperty("in_manager_name", ""));
                item.setProperty("in_member_status", itmGym.getProperty("in_member_status", ""));
                item.setProperty("member_cnt", itmGym.getProperty("member_cnt", ""));
                item.setProperty("resume_cnt", GetResumeRoleCount(map, resume_id));

                itmReturn.addRelationship(item);
            }

            itmReturn.setProperty("gym_count", count <= 0 ? "0" : count.ToString());
        }

        private Dictionary<string, string> GetResumeRoleCount(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn)
        {
            string sql = "SELECT source_id, count(*) AS 'resume_cnt' FROM IN_RESUME_RESUME WITH(NOLOCK) WHERE in_resume_role NOT IN ('sys_9999') GROUP BY source_id";
            Item items = inn.applySQL(sql);
            int count = items.getItemCount();
            Dictionary<string, string> map = new Dictionary<string, string>();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string resume_id = item.getProperty("source_id", "");
                string resume_cnt = item.getProperty("resume_cnt", "");
                if (!map.ContainsKey(resume_id))
                {
                    map.Add(resume_id, resume_cnt);
                }
            }
            return map;
        }

        private string GetResumeRoleCount(Dictionary<string, string> map, string resume_id)
        {
            if (!map.ContainsKey(resume_id))
            {
                return "0";
            }
            else
            {
                return map[resume_id];
            }
        }

        private Item GetCommitteeGyms(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            string resume_id = itmReturn.getProperty("resume_id", "");

            string sql = @"
                SELECT 
	                t1.*
	                , ISNULL(t2.cnt, '0') AS 'member_cnt'
                FROM
	                IN_RESUME t1 WITH(NOLOCK)
                LEFT OUTER JOIN
                (
	                SELECT
	                    in_committee
		                , in_current_org
		                , count(*) AS 'cnt'
	                FROM
		                IN_CTA_PROMOTION WITH(NOLOCK)
	                GROUP BY
	                    in_committee
		                , in_current_org
                ) t2
	                ON t2.in_committee = t1.in_manager_name
	                AND t2.in_current_org = t1.in_name
                WHERE
	                t1.in_member_type = 'vip_gym'
                    AND t1.in_manager_org = '{#in_manager_org}'
                ORDER BY
                    t1.in_sno
            ";

            sql = sql.Replace("{#in_manager_org}", resume_id);

            return inn.applySQL(sql);

        }

        //下拉選單
        private void AppendMenu(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string list_name, string type_name, Item itmReturn)
        {
            Item items = GetValues(CCO, strMethodName, inn, list_name: list_name);
            AppendMenu(CCO, strMethodName, inn, items, type_name, itmReturn);
        }

        //下拉選單
        private void AppendMenu(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item items, string type_name, Item itmReturn)
        {
            if (items.isError() || items.getItemCount() <= 0)
            {
                return;
            }

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);

                item.setType(type_name);
                item.setProperty("inn_label", item.getProperty("label", ""));
                item.setProperty("inn_value", item.getProperty("value", ""));
                itmReturn.addRelationship(item);
            }
        }

        //跆委會(專項與地區)
        private Item GetCommittees(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn)
        {
            string sql = @"
                SELECT
                    id AS 'value'
                    , in_name AS 'label'
                FROM
                    IN_RESUME WITH (NOLOCK) 
                WHERE
                    in_member_type = 'area_cmt'
                    AND in_member_role = 'sys_9999'
                ORDER BY 
                    login_name
            ";

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return inn.applySQL(sql);
        }

        //取得下拉選單
        private Item GetValues(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string list_name)
        {
            string sql = @"
                SELECT
                    t2.value
			        , t2.label_zt AS 'label'
                FROM
                    [LIST] t1 WITH(NOLOCK)
		        INNER JOIN
			        [VALUE] t2 WITH(NOLOCK)
			        ON t2.source_id = t1.id
                WHERE
			        t1.name = N'{#list_name}'
		        ORDER BY
			        t2.sort_order
            ";

            sql = sql.Replace("{#list_name}", list_name);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return inn.applySQL(sql);
        }

        private void SyncResumes(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            string resume_id = itmReturn.getProperty("resume_id", "");
            string promotion_id = itmReturn.getProperty("promotion_id", "");

            if (resume_id == "" && promotion_id == "")
            {
                CCO.Utilities.WriteDebug(strMethodName, "SyncResumes: 參數錯誤");
                throw new Exception("參數錯誤");
            }

            bool has_cmt = false;
            string in_current_org = "";
            string in_manager_org = "";
            string in_manager_name = "";

            string committee_id = "";
            string committee_area = "";
            string committee_name = "";

            Item itmGym = null;
            Item itmCommittee = null;

            if (resume_id != "")
            {
                itmGym = inn.newItem("In_Resume", "get");
                itmGym.setProperty("id", resume_id);
                itmGym = itmGym.apply();

                if (itmGym.isError() || itmGym.getResult() == "")
                {
                    throw new Exception("查無 Resume 資料");
                }

                in_current_org = itmGym.getProperty("in_name", "");
                in_manager_org = itmGym.getProperty("in_manager_org", "");
                in_manager_name = itmGym.getProperty("in_manager_name", "");

                itmCommittee = inn.newItem("In_Resume", "get");
                itmCommittee.setProperty("id", in_manager_org);
                itmCommittee = itmCommittee.apply();

                if (itmCommittee.isError() || itmCommittee.getResult() == "")
                {
                    throw new Exception("查無 縣市委員會 Resume 資料");
                }

                committee_id = itmCommittee.getProperty("id", "");
                committee_area = itmCommittee.getProperty("in_manager_area", "");
                committee_name = itmCommittee.getProperty("in_name", "");

                has_cmt = true;
            }

            Item itmEntities = inn.newItem("IN_CTA_PROMOTION", "get");

            if (has_cmt)
            {
                itmEntities.setProperty("in_committee", in_manager_name);
                itmEntities.setProperty("in_current_org", in_current_org);
            }
            else
            {
                itmEntities.setProperty("id", promotion_id);
            }

            itmEntities = itmEntities.apply();


            if (itmEntities.isError() || itmEntities.getItemCount() <= 0)
            {
                CCO.Utilities.WriteDebug(strMethodName, "SyncResumes: 查無晉段資料");
                throw new Exception("查無晉段資料");
            }

            int count = itmEntities.getItemCount();

            List<string> errs = new List<string>();

            for (int i = 0; i < count; i++)
            {
                Item itmEntity = itmEntities.getItemByIndex(i);
                string no = (i + 1).ToString();
                string in_acs_no = itmEntity.getProperty("in_acs_no", "");
                string in_name = itmEntity.getProperty("in_name", "");
                string in_sno = itmEntity.getProperty("in_sno", "");
                string in_degree = itmEntity.getProperty("in_degree", "");
                string in_west_birth = itmEntity.getProperty("in_west_birth", "");
                string in_tw_birth = itmEntity.getProperty("in_tw_birth", "");
                string in_committee = itmEntity.getProperty("in_committee", "");
                string in_check_flag = itmEntity.getProperty("in_check_flag", "");

                string _current_org = itmEntity.getProperty("in_current_org", "");

                if (!has_cmt)
                {
                    itmCommittee = inn.newItem("In_Resume", "get");
                    itmCommittee.setProperty("in_name", in_committee);
                    itmCommittee.setProperty("in_member_type", "area_cmt");
                    itmCommittee = itmCommittee.apply();

                    if (!itmCommittee.isError() && itmCommittee.getResult() != "")
                    {
                        committee_id = itmCommittee.getProperty("id", "");
                        committee_area = itmCommittee.getProperty("in_manager_area", "");
                        committee_name = itmCommittee.getProperty("in_name", "");

                        string sql_gym = "SELECT TOP 1 * FROM IN_RESUME WITH(NOLOCK)"
                            + " WHERE IN_NAME = N'" + _current_org + "'"
                            + " AND in_manager_org = '" + committee_id + "'"
                            + " ORDER BY IN_MEMBER_LAST_YEAR DESC";

                        //CCO.Utilities.WriteDebug(strMethodName, "sql_gym: " + sql_gym);

                        itmGym = inn.applySQL(sql_gym);
                    }
                }

                if (itmGym == null || itmGym.isError() || itmGym.getResult() == "")
                {
                    itmGym = inn.newItem();
                }

                // if (in_check_flag != "0")
                // {
                //     continue;
                // }

                CCO.Utilities.WriteDebug(strMethodName, "    - " + no + ". " + in_name + "(" + in_sno + ")" + " (" + in_acs_no + ")");

                if (in_degree != "")
                {
                    itmEntity.setProperty("in_degree", in_degree + "000");
                }

                if (in_west_birth == "")
                {
                    itmEntity.setProperty("in_birth", in_tw_birth);
                }
                else
                {
                    itmEntity.setProperty("in_birth", in_west_birth);
                }

                if (committee_id != "")
                {
                    itmEntity.setProperty("in_manager_org", committee_id);
                    itmEntity.setProperty("in_manager_area", committee_area);
                    itmEntity.setProperty("in_manager_name", committee_name);
                }

                itmEntity.setProperty("login_name", itmEntity.getProperty("in_sno", ""));
                itmEntity.setProperty("in_stuff_b1", "N" + itmEntity.getProperty("in_acs_no", "").PadLeft(7, '0'));

                string in_gender = "";
                string src_gender = itmEntity.getProperty("in_gender", "").ToUpper();
                if (src_gender == "F")
                {
                    in_gender = "女";
                }
                else if (src_gender == "M")
                {
                    in_gender = "男";
                }
                else
                {
                    in_gender = src_gender;
                }
                itmEntity.setProperty("in_gender", in_gender);


                itmEntity.setProperty("in_group", in_current_org);
                itmEntity.setProperty("in_short_org", in_current_org);
                itmEntity.setProperty("in_stuff_a1", in_current_org);
                itmEntity.setProperty("in_org", "0");

                itmEntity.setProperty("in_note", itmEntity.getProperty("in_other", ""));

                itmEntity.setProperty("in_member_type", "reg");
                itmEntity.setProperty("in_member_role", "reg_110");

                itmEntity.setProperty("in_resume_role", "reg_110");
                itmEntity.setProperty("in_resume_remark", "晉段資料");

                string err = CheckResume(CCO, strMethodName, inn, itmEntity);

                if (err != "")
                {
                    errs.Add(err);
                    CCO.Utilities.WriteDebug(strMethodName, "        - 資料錯誤");
                }
                else
                {
                    CCO.Utilities.WriteDebug(strMethodName, "        - 資料正確，開始轉換...");
                    SyncResume(CCO, strMethodName, inn, itmGym, itmEntity);
                }
            }

            CCO.Utilities.WriteDebug(strMethodName, "[道館-晉段]資料轉換結束");

            if (errs.Count > 0)
            {
                itmReturn.setProperty("err_msg", string.Join("<br>", errs));
            }
        }

        //檢查資料
        private string CheckResume(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmEntity)
        {
            string id = itmEntity.getProperty("id", "");
            string in_name = itmEntity.getProperty("in_name", "");
            string old_check_memo = itmEntity.getProperty("in_check_memo", "");
            string new_check_memo = "";

            string in_sno = itmEntity.getProperty("in_sno", "");
            string in_gender = itmEntity.getProperty("in_gender", "");

            if (in_sno == "")
            {
                new_check_memo = "身分證號為空白";
            }
            else if (in_sno.Length == 10)
            {
                char gender_code = in_sno.ToUpper()[1];
                if (Char.IsLetter(gender_code))
                {
                    //身分證號第二個字元為字母
                }
                else if (gender_code == '1' && (in_gender == "F" || in_gender == "女"))
                {
                    new_check_memo = "性別錯誤";
                }
                else if (gender_code == '2' && (in_gender == "M" || in_gender == "男"))
                {
                    new_check_memo = "性別錯誤";
                }
                else if (!IsValidSid(in_sno))
                {
                    new_check_memo = "身分證號格式錯誤";
                }
            }

            if (new_check_memo != "")
            {
                string sql = "UPDATE IN_CTA_PROMOTION SET in_check_memo = N'" + new_check_memo + "' WHERE id = '" + id + "'";

                inn.applySQL(sql);

                return "[" + in_name + "]無法過檔: " + new_check_memo;
            }
            else
            {
                return "";
            }
        }

        //同步晉段資料
        private void SyncResume(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmGym, Item itmEntity)
        {
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
            string in_sno = itmEntity.getProperty("in_sno", "");
            string NewPwd = in_sno.Length > 4 ? in_sno.Substring(in_sno.Length - 4, 4) : "1234";
            string NewPwd_md5 = System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile(NewPwd, "md5");

            string in_acs_no = itmEntity.getProperty("in_acs_no", "");
            string login_name = itmEntity.getProperty("in_sno", "");
            string SequenceVal = itmEntity.getProperty("in_sno", itmEntity.getProperty("login_name", ""));

            //建立與會者
            string meeting_id = "249FDB244E534EB0AA66C8E9C470E930";
            Item itmMUser = inn.newItem("In_Meeting_User", "merge");
            itmMUser.setAttribute("where", "source_id='" + meeting_id + "' AND in_mail='" + itmEntity.getProperty("login_name", "") + "'");
            itmMUser.setProperty("source_id", meeting_id);
            itmMUser.setProperty("in_name", itmEntity.getProperty("in_name", ""));
            itmMUser.setProperty("in_sno", itmEntity.getProperty("login_name", ""));
            itmMUser.setProperty("in_sno_display", GetSidDisplay(itmEntity.getProperty("login_name", "")));
            itmMUser.setProperty("in_mail", itmEntity.getProperty("login_name", ""));
            itmMUser.setProperty("in_email", itmEntity.getProperty("in_email", ""));
            itmMUser.setProperty("in_gender", itmEntity.getProperty("in_gender", ""));
            itmMUser.setProperty("in_birth", itmEntity.getProperty("in_birth", ""));
            itmMUser.setProperty("in_note_state", "official");
            itmMUser.setProperty("in_confirm_mail", DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss"));
            itmMUser.setProperty("in_regdate", DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss"));
            itmMUser.setProperty("in_tel", itmEntity.getProperty("in_tel", ""));
            itmMUser.setProperty("in_creator", "lwu001");
            itmMUser.setProperty("in_creator_sno", "lwu001");
            itmMUser.setProperty("in_current_org", itmEntity.getProperty("in_name", ""));
            itmMUser.setProperty("in_group", itmEntity.getProperty("in_name", ""));
            itmMUser.setProperty("in_role", "player");
            itmMUser.setProperty("in_index", "00001");
            itmMUser.setProperty("in_add", itmEntity.getProperty("in_resident_add", ""));
            itmMUser = itmMUser.apply();

            //建立使用者
            Item itmUser = inn.newItem("User", "merge");
            itmUser.setAttribute("where", "login_name='" + itmEntity.getProperty("login_name", "") + "'");
            itmUser.setProperty("last_name", itmEntity.getProperty("in_name", ""));//姓
            itmUser.setProperty("first_name", SequenceVal);//名
            itmUser.setProperty("user_no", SequenceVal);//員工編號
            itmUser.setProperty("login_name", itmEntity.getProperty("login_name"));//登入帳號
            itmUser.setProperty("cell", itmEntity.getProperty("in_tel", ""));//行動電話
            itmUser.setProperty("email", itmEntity.getProperty("in_email", ""));//電子郵件
            itmUser.setProperty("in_company", itmEntity.getProperty("in_company", ""));//公司別
            itmUser.setProperty("logon_enabled", "0");//可登入
            itmUser.setProperty("in_rank", inn.getItemByKeyedName("in_rank", "SYS-000000").getID());//職級
            itmUser.setProperty("in_app_page", "pages/c.aspx?page=UserDashboard.html&method=in_user_dashboard");
            itmUser.setProperty("password", NewPwd_md5);
            itmUser = itmUser.apply();

            Item itmIdentity = inn.newItem("Identity", "get"); //_InnH.GetIdentityByUserId(itmUser.getID());
            itmIdentity.setProperty("in_user", itmUser.getID());
            itmIdentity = itmIdentity.apply();
            string identity_id = itmIdentity.getID();

            Item itmResume = inn.newItem("In_Resume");
            itmResume.setAttribute("where", "[In_Resume].login_name = '" + login_name + "'");

            itmResume.setProperty("in_is_student", "1");
            itmResume.setProperty("in_is_temporary", "0");
            itmResume.setProperty("password", NewPwd_md5);
            itmResume.setProperty("in_password_plain", NewPwd);
            itmResume.setProperty("in_user_id", itmUser.getID());//對應使用者
            itmResume.setProperty("owned_by_id", identity_id);//對應使用者

            itmResume.setProperty("in_stuff_b1", itmEntity.getProperty("in_stuff_b1", ""));
            itmResume.setProperty("in_name", itmEntity.getProperty("in_name", ""));
            itmResume.setProperty("in_en_name", itmEntity.getProperty("in_en_name", ""));
            itmResume.setProperty("in_birth", itmEntity.getProperty("in_birth", ""));
            itmResume.setProperty("in_gender", itmEntity.getProperty("in_gender", ""));
            itmResume.setProperty("in_sno", itmEntity.getProperty("in_sno", ""));
            itmResume.setProperty("login_name", itmEntity.getProperty("login_name", ""));
            itmResume.setProperty("in_country", itmEntity.getProperty("in_country", ""));
            itmResume.setProperty("in_job", itmEntity.getProperty("in_job", ""));
            itmResume.setProperty("in_tel_1", itmEntity.getProperty("in_tel_1", ""));
            itmResume.setProperty("in_tel", itmEntity.getProperty("in_tel", ""));
            itmResume.setProperty("in_resident_add_code", itmEntity.getProperty("in_resident_add_code", ""));
            itmResume.setProperty("in_resident_add", itmEntity.getProperty("in_resident_add", ""));
            itmResume.setProperty("in_email", itmEntity.getProperty("in_email", ""));
            itmResume.setProperty("in_education", itmEntity.getProperty("in_education", ""));
            itmResume.setProperty("in_area", itmEntity.getProperty("in_area", ""));
            itmResume.setProperty("in_guardian", itmEntity.getProperty("in_guardian", ""));
            itmResume.setProperty("in_stuff_c1", itmEntity.getProperty("in_stuff_c1", ""));
            itmResume.setProperty("in_stuff_c2", itmEntity.getProperty("in_stuff_c2", ""));
            itmResume.setProperty("in_current_org", itmEntity.getProperty("in_current_org", ""));
            itmResume.setProperty("in_group", itmEntity.getProperty("in_group", ""));
            itmResume.setProperty("in_short_org", itmEntity.getProperty("in_short_org", ""));
            itmResume.setProperty("in_stuff_a1", itmEntity.getProperty("in_stuff_a1", ""));
            itmResume.setProperty("in_org", itmEntity.getProperty("in_org", ""));

            itmResume.setProperty("in_manager_name", itmEntity.getProperty("in_manager_name", ""));
            itmResume.setProperty("in_manager_org", itmEntity.getProperty("in_manager_org", ""));
            itmResume.setProperty("in_manager_area", itmEntity.getProperty("in_manager_area", ""));

            itmResume.setProperty("in_degree", itmEntity.getProperty("in_degree", ""));
            itmResume.setProperty("in_degree_id", itmEntity.getProperty("in_degree_id", ""));
            itmResume.setProperty("in_gl_degree_id", itmEntity.getProperty("in_gl_degree_id", ""));
            itmResume.setProperty("in_gl_degree_date", itmEntity.getProperty("in_gl_degree_date", ""));

            itmResume.setProperty("in_note", itmEntity.getProperty("in_note", ""));


            itmResume.setProperty("in_member_type", itmEntity.getProperty("in_member_type", ""));
            itmResume.setProperty("in_member_role", itmEntity.getProperty("in_member_role", ""));

            itmResume.setProperty("in_resume_role", itmEntity.getProperty("in_resume_role", ""));
            itmResume.setProperty("in_resume_remark", itmEntity.getProperty("in_resume_remark", ""));
            itmResume.setProperty("in_meeting", meeting_id);

            itmResume = itmResume.apply("merge");

            if (itmResume.isError())
            {
                CCO.Utilities.WriteDebug(strMethodName, itmResume.dom.InnerXml);
                throw new Exception("Resume 同步失敗");
            }

            //MeetingUser
            Item itmMember;
            itmMember = inn.newItem("Member", "merge");
            itmMember.setAttribute("where", "source_id='95277C725C954FEE959DCE2073DA1F9C' and related_id='" + identity_id + "'");
            itmMember.setProperty("source_id", "95277C725C954FEE959DCE2073DA1F9C");
            itmMember.setRelatedItem(itmIdentity);
            itmMember = itmMember.apply();

            //All Employees
            itmMember = inn.newItem("Member", "merge");
            itmMember.setAttribute("where", "source_id='B32BD81D1AD04207BF1E61E39A4E0E13' and related_id='" + identity_id + "'");
            itmMember.setProperty("source_id", "B32BD81D1AD04207BF1E61E39A4E0E13");
            itmMember.setRelatedItem(itmIdentity);
            itmMember = itmMember.apply();

            string resume_id = itmResume.getID();

            string role_remark = "會員";

            //self
            Item itmResumeRole = inn.newItem("In_Resume_Resume", "merge");
            itmResumeRole.setAttribute("where", "source_id='" + resume_id + "' and related_id='" + resume_id + "'");
            itmResumeRole.setProperty("source_id", resume_id);
            itmResumeRole.setProperty("in_resume_role", "sys_9999");
            itmResumeRole.setProperty("in_resume_remark", role_remark);
            itmResumeRole.setRelatedItem(itmResume);
            itmResumeRole = itmResumeRole.apply();

            ////gym
            //Item itmRParent = inn.newItem("In_Resume", "get");
            //itmRParent.setProperty("in_name", itmEntity.getProperty("in_current_org", ""));
            //itmRParent.setProperty("in_manager_name", itmEntity.getProperty("in_committee", ""));
            //itmRParent = itmRParent.apply();

            //if (!itmRParent.isError() && itmRParent.getResult() != "")
            //{
            //    string p_resume_id = itmRParent.getID();

            //    Item itmResumeRole2 = inn.newItem("In_Resume_Resume", "merge");
            //    itmResumeRole2.setAttribute("where", "source_id='" + p_resume_id + "' and related_id='" + resume_id + "'");
            //    itmResumeRole2.setProperty("source_id", p_resume_id);
            //    itmResumeRole2.setProperty("in_resume_role", "reg_110");
            //    itmResumeRole2.setProperty("in_resume_remark", "晉段");
            //    itmResumeRole2.setRelatedItem(itmResume);
            //    itmResumeRole2 = itmResumeRole2.apply();
            //}

            //建立道館與人員的 relation
            string p_resume_id = itmGym.getProperty("id", "");
            if (p_resume_id != "")
            {
                Item itmResumeRole2 = inn.newItem("In_Resume_Resume", "merge");
                itmResumeRole2.setAttribute("where", "source_id='" + p_resume_id + "' and related_id='" + resume_id + "'");
                itmResumeRole2.setProperty("source_id", p_resume_id);
                itmResumeRole2.setProperty("in_resume_role", "reg_110");
                itmResumeRole2.setProperty("in_resume_remark", "晉段");
                itmResumeRole2.setRelatedItem(itmResume);
                itmResumeRole2 = itmResumeRole2.apply();
            }

            string sql = "DELETE FROM IN_RESUME_PROMOTION WHERE source_id = '" + resume_id + "'";
            inn.applySQL(sql);

            ////晉段記錄 (無法使用 merge，資料欄位無法組成唯一性)
            //List<Item> itmPromotions = cfg.GetNTKBList(CCO, strMethodName, inn, in_acs_no);
            //if (itmPromotions.Count > 0)
            //{
            //    for (int i = 0; i < itmPromotions.Count; i++)
            //    {
            //        Item itmPromotion = itmPromotions[i];
            //        itmPromotion.setProperty("source_id", resume_id);
            //        itmPromotion = itmPromotion.apply("add");
            //    }
            //}
        }


        /// <summary>
        /// 取得連線字串
        /// </summary>
        /// <returns></returns>
        private string GetAccessConnString(string dbName)
        {
            string connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + dbName + ";Jet OLEDB:Database Password=ct505;";
            return connectionString;
        }

        /// <summary>
        /// 存取 Oledb
        /// </summary>
        /// <param name="selectCommandText">執行指令</param>
        /// <param name="connectionString">連線字串</param>
        /// <returns></returns>
        private System.Data.DataTable GetDataTable(string selectCommandText, string connectionString)
        {
            try
            {
                System.Data.DataTable Result = null;
                using (System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection(connectionString))
                {
                    using (System.Data.OleDb.OleDbDataAdapter da = new System.Data.OleDb.OleDbDataAdapter(selectCommandText, conn))
                    {
                        try
                        {
                            Result = new System.Data.DataTable();
                            da.Fill(Result);
                        }
                        catch (System.Exception ex)
                        {
                            Result = null;
                        }
                    }
                }
                return Result;
            }
            catch (System.Exception ex)
            {
                return null;
            }
        }

        private string GetColVal(System.Data.DataRow dr, string col)
        {
            if (dr[col] == System.DBNull.Value)
            {
                return "";
            }
            else
            {
                return dr[col].ToString();
            }
        }

        #region 檢查證號

        //檢查身分證號
        private bool IsValidSid(string value)
        {
            if (value == null) return false;

            var result = false;
            var timesArr = new int[] { 1, 9, 8, 7, 6, 5, 4, 3, 2, 1 };
            var idSno = value.Trim();

            if (string.IsNullOrWhiteSpace(idSno)) return result;

            if (idSno.Length == 0) return result;
            if (idSno.Length != 10) return result;

            var a = new[] { 10, 11, 12, 13, 14, 15, 16, 17, 34, 18, 19, 20, 21, 22, 35, 23, 24, 25, 26, 27, 28, 29, 32, 30, 31, 33 };
            var b = new int[11];

            b[1] = a[(idSno[0]) - 65] % 10;

            var c = b[0] = a[(idSno[0]) - 65] / 10;

            for (var i = 1; i <= 9; i++)
            {
                b[i + 1] = idSno[i] - 48;
                c += b[i] * (10 - i);
            }

            if (((c % 10) + b[10]) % 10 == 0)
            {
                result = true;
            }

            return result;
        }

        private string CheckResident(string Id)
        {
            string sex = "";
            string nationality = "";
            char[] strArr = Id.ToCharArray(); // 字串轉成char陣列

            int verifyNum = 0;
            int[] pidResidentFirstInt = { 1, 10, 9, 8, 7, 6, 5, 4, 9, 3, 2, 2, 11, 10, 8, 9, 8, 7, 6, 5, 4, 3, 11, 3, 12, 10 };
            char[] pidCharArray = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };
            // 第一碼
            verifyNum += pidResidentFirstInt[Array.BinarySearch(pidCharArray, strArr[0])];
            // 原居留證第二碼英文字應轉換為10~33，並僅取個位數*6，這裡直接取[(個位數*6) mod 10]
            int[] pidResidentSecondInt = { 0, 8, 6, 4, 2, 0, 8, 6, 2, 4, 2, 0, 8, 6, 0, 4, 2, 0, 8, 6, 4, 2, 6, 0, 8, 4 };
            // 第二碼
            verifyNum += pidResidentSecondInt[Array.BinarySearch(pidCharArray, strArr[1])];
            // 第三~八碼
            for (int i = 2, j = 7; i < 9; i++, j--)
            {
                verifyNum += Convert.ToInt32(strArr[i].ToString(), 10) * j;
            }
            // 檢查碼
            verifyNum = (10 - (verifyNum % 10)) % 10;
            bool ok = verifyNum == Convert.ToInt32(strArr[9].ToString(), 10);
            if (ok)
            {
                // 判斷性別 & 國籍
                sex = "男";
                if (strArr[1] == 'B' || strArr[1] == 'D') sex = "女";
                nationality = "外籍人士";
                if (strArr[1] == 'A' || strArr[1] == 'B') nationality += "(臺灣地區無戶籍國民、大陸地區人民、港澳居民)";
                {
                    return "";
                }
            }
            else
            {
                return "居留證格式錯誤";
            }
        }

        //大陸身分證驗證(18碼)
        private string CheckIDCards(string Id)
        {
            int intLen = Id.Length;
            long n = 0;

            if (intLen == 18)
            {
                if (long.TryParse(Id.Remove(17), out n) == false || n < Math.Pow(10, 16) || long.TryParse(Id.Replace('x', '0').Replace('X', '0'), out n) == false)
                {
                    return "數字格式錯誤";
                }
                string address = "11x22x35x44x53x12x23x36x45x54x13x31x37x46x61x14x32x41x50x62x15x33x42x51x63x21x34x43x52x64x65x71x81x82x91";
                if (address.IndexOf(Id.Remove(2)) == -1)
                {
                    return "省份驗證錯誤";
                }
                string birth = Id.Substring(6, 8).Insert(6, "-").Insert(4, "-");
                DateTime time = new DateTime();
                if (DateTime.TryParse(birth, out time) == false)
                {
                    return "生日驗證錯誤";
                }
                string[] arrVarifyCode = ("1,0,x,9,8,7,6,5,4,3,2").Split(',');
                string[] Wi = ("7,9,10,5,8,4,2,1,6,3,7,9,10,5,8,4,2").Split(',');
                char[] Ai = Id.Remove(17).ToCharArray();
                int sum = 0;
                for (int i = 0; i < 17; i++)
                {
                    sum += int.Parse(Wi[i]) * int.Parse(Ai[i].ToString());
                }
                int y = -1;
                Math.DivRem(sum, 11, out y);
                if (arrVarifyCode[y] != Id.Substring(17, 1).ToLower())
                {
                    return "校驗碼錯誤";
                }
                return "";
            }
            else if (intLen == 15)
            {
                //舊身分證為15碼 為了驗證故轉成18碼
                return CheckIDCards(Per15To18(Id));
            }
            else
            {
                return "身分證碼數錯誤";
            }
        }

        //15碼轉18碼(舊身分證為15碼 驗證須轉18碼)
        private string Per15To18(string perIDSrc)
        {
            int iS = 0;
            //加權因子常數
            int[] iW = new int[] { 7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2 };
            //校驗碼常數
            string LastCode = "10X98765432";
            //新身分證號
            string perIDNew;
            perIDNew = perIDSrc.Substring(0, 6);
            //填在第6位及第7位填上'1','9'兩個數字
            perIDNew += "19";
            perIDNew += perIDSrc.Substring(6, 9);

            //進行加權求和
            for (int i = 0; i < 17; i++)
            {
                iS += int.Parse(perIDNew.Substring(i, 1)) * iW[i];
            }

            //取模運算,得到模值
            int iY = iS % 11;
            //從LastCode中取得已模為索引號的值,加到身分證的最後一位,即為新身分證號
            perIDNew += LastCode.Substring(iY, 1);
            return perIDNew;

        }

        #endregion 檢查證號

        //身分證字號(外顯)
        private string GetSidDisplay(string in_sno)
        {
            if (in_sno.Length == 10)
            {
                //10碼(身分證處理)
                string in_sno_1 = in_sno.Substring(0, 2);
                string in_sno_2 = in_sno.Substring(6, 4);
                return in_sno_1 + "****" + in_sno_2;//A1****6789
            }
            else
            {
                return "";
            }
        }

        private string GetGender(string value)
        {
            if (value == "F")
            {
                return "女";
            }
            else if (value == "M")
            {
                return "男";
            }
            else
            {
                return value;
            }
        }

        private int GetIntValue(string value)
        {
            int result = 0;
            if (Int32.TryParse(value, out result))
            {
                return result;
            }
            else
            {
                return 0;
            }
        }

        private string GetBoolValue(string value)
        {
            bool result = false;
            Boolean.TryParse(value, out result);
            return result ? "1" : "0";
        }

        private DateTime GetDateTime(string value)
        {
            DateTime dt = DateTime.MinValue;
            DateTime.TryParse(value, out dt);
            return dt;
        }

        private string GetDateTimeValue(string value, string format = "yyyy-MM-dd", int hours = 0)
        {
            if (value == "") return "";

            string day = value.Replace("/", "-");

            DateTime dt = DateTime.Now;
            if (DateTime.TryParse(day, out dt))
            {
                return dt.AddHours(hours).ToString(format);
            }
            else
            {
                return value;
            }
        }
    }
}