﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class In_CTA_Referee : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
            目的: 中華跆協資料轉檔
                - R裁判講習記錄
                - T教練講習記錄
            日期: 
                - 2021/03/17: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break(); 

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]In_CTA_Referee";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "input dom: " + itmR.dom.InnerXml);

            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            Item itmPermit = inn.applyMethod("In_CheckIdentity", "<method>" + strMethodName + "</method><code>" + "ALL" + "</code>");
            bool isMeetingAdmin = itmPermit.getProperty("isMeetingAdmin", "") == "1";

            if (!isMeetingAdmin) throw new Exception("無權限");

            string sql = "";

            //R裁判講習記錄
            sql = @"
                SELECT 
                    * 
                FROM 
                    IN_CTA_REFEREE_LOG WITH(NOLOCK)
                WHERE 
                    ISNULL(resume_id, '') <> ''
                    AND ISNULL(in_tw_year, '') NOT IN ('', '0', 'T')
                ORDER BY 
                    in_old_no, in_acs_no
            ";

            Item itmLogs = inn.applySQL(sql);

            int count = itmLogs.getItemCount();
            CCO.Utilities.WriteDebug(strMethodName, "轉檔開始-總筆數: " + count);

            for (int i = 0; i < count; i++)
            {
                Item itmLog = itmLogs.getItemByIndex(i);
                string in_old_no = itmLog.getProperty("in_old_no", "");
                string resume_id = itmLog.getProperty("resume_id", "");
                string in_name = itmLog.getProperty("in_name", "");
                string in_sno = itmLog.getProperty("in_sno", "");
                string in_west_year = itmLog.getProperty("in_west_year", "").Trim();
                string in_level = itmLog.getProperty("in_level", "").Trim();
                string in_echelon = itmLog.getProperty("in_echelon", "").Trim();
                CCO.Utilities.WriteDebug(strMethodName, "    " + i + ". " + in_name + "(" + in_sno + ") _# old no = " + in_old_no);

                bool need_update = false;

                string sql_qry = "SELECT * FROM In_Resume_Referee WITH(NOLOCK)"
                    + " WHERE source_id = '" + resume_id + "'"
                    + " AND in_year = '" + in_west_year + "'"
                    + " AND in_level = '" + in_level + "'"
                    + " AND in_echelon = '" + in_echelon + "'";

                Item itmOld = inn.applySQL(sql_qry);
                if (itmOld.isError() || itmOld.getResult() == "")
                {
                    need_update = true;
                }

                if (!need_update)
                {
                    continue;
                }

                CCO.Utilities.WriteDebug(strMethodName, "        - merge");

                Item itmReferee = inn.newItem("In_Resume_Referee", "merge");

                itmReferee.setAttribute("where", "source_id = '" + resume_id + "'"
                    + " AND in_year = " + in_west_year
                    + " AND in_level = " + in_level
                    + " AND in_echelon = " + in_echelon
                    );

                itmReferee.setProperty("source_id", resume_id);
                itmReferee.setProperty("in_old_no", in_old_no);
                itmReferee.setProperty("in_year", in_west_year);
                itmReferee.setProperty("in_level", in_level);
                itmReferee.setProperty("in_echelon", in_echelon);
                itmReferee.setProperty("in_evaluation", itmLog.getProperty("in_evaluation", "").Trim());
                itmReferee.setProperty("in_know_score", itmLog.getProperty("in_know_score", "").Trim());
                itmReferee.setProperty("in_tech_score", itmLog.getProperty("in_tech_score", "").Trim());
                itmReferee.setProperty("in_note", itmLog.getProperty("in_note", "").Trim());

                string in_certificate = itmLog.getProperty("in_certificate", "").ToUpper();
                if (in_certificate == "1" || in_certificate == "TRUE")
                {
                    itmReferee.setProperty("in_certificate", "1");
                }
                else
                {
                    itmReferee.setProperty("in_certificate", "0");
                }

                itmReferee = itmReferee.apply();

                if (itmReferee.isError())
                {
                    CCO.Utilities.WriteDebug(strMethodName, "      - 發生錯誤");
                }
            }

            CCO.Utilities.WriteDebug(strMethodName, "轉檔結束");

            //把身分換回來
            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

            return itmR;
        }
    }
}