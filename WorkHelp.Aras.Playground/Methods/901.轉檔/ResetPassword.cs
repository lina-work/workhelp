﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class ResetPassword : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
            目的: 中華跆協資料轉檔-重設密碼
            日期: 
                - 2020/12/21: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]ResetPassword";

            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            Item itmR = this;

            //講師履歷
            string sql = "SELECT * FROM IN_RESUME WITH(NOLOCK) WHERE in_sno = 'M001'";

            Item items = inn.applySQL(sql);

            int count = items.getItemCount();

            CCO.Utilities.WriteDebug(strMethodName, "[重設密碼]資料轉換開始: 共 " + count + " 筆");

            string NewPwd = "";

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string resume_id = item.getProperty("id", "");
                string in_name = item.getProperty("in_name", "");
                string in_sno = item.getProperty("in_sno", "");
                string in_member_type = item.getProperty("in_member_type", "");
                string in_user_id = item.getProperty("in_user_id", "");

                CCO.Utilities.WriteDebug(strMethodName, "[重設密碼]資料轉換資料: " + in_name + " (" + in_sno + ")");

                //新密碼
                NewPwd = "1234";
                string NewPwd_md5 = System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile(NewPwd, "md5");

                string sql_update1 = "UPDATE [User] SET password = N'" + NewPwd_md5 + "' WHERE id = '" + in_user_id + "'";
                inn.applySQL(sql_update1);

                string sql_update2 = "UPDATE [In_Resume] SET password = N'" + NewPwd_md5 + "', in_password_plain = N'" + NewPwd + "' WHERE id = '" + resume_id + "'";
                inn.applySQL(sql_update2);
            }

            CCO.Utilities.WriteDebug(strMethodName, "[重設密碼]資料轉換結束");


            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

            return itmR;
        }
    }
}