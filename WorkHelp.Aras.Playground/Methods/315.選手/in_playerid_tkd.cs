﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;
using System.Net;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class in_playerid_tkd : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的:創建選手證(cta_test)
                日期: 
                    - 2021-11-25 調整 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]in_playerid";

            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            string aml = "";
            string sql = "";
            string strError = "";
            string strUserId = inn.getUserID();
            //string strIdentityId = inn.getUserAliases();

            _InnH.AddLog(strMethodName, "MethodSteps");
            Item itmR = this;

            string meeting_id = this.getProperty("meeting_id", "");
            string id_type = this.getProperty("id_type", "");

            bool open = false;
            Item Resume = this;
            string SQL_OnlyMyGym = "";

            //判斷現在的登入者是否為此課程的共同講師 
            aml = "<AML>" +
                    "<Item type='In_Meeting_Resumelist' action='get'>" +
                    "<source_id>" + meeting_id + "</source_id>" +
                    "</Item></AML>";
            Item Meeting_Resumelists = inn.applyAML(aml);

            for (int i = 0; i < Meeting_Resumelists.getItemCount(); i++)
            {
                Item Meeting_Resumelist = Meeting_Resumelists.getItemByIndex(i);
                //判定登入者ID是否存在於該課程共同講師裡面
                if (strUserId == Meeting_Resumelist.getProperty("in_user_id", ""))//此次登入者是共同講師
                {
                    open = true;
                }
                else
                {
                    open = false;
                }
            }

            Item itmPermit = inn.applyMethod("In_CheckIdentity", "<method>" + strMethodName + "</method><code>ALL</code>");
            bool isMeetingAdmin = itmPermit.getProperty("isMeetingAdmin", "") == "1";


            aml = "<AML>" +
            "<Item type='In_Resume' action='get'>" +
            "<in_user_id>" + strUserId + "</in_user_id>" +
            "</Item></AML>";
            Resume = inn.applyAML(aml);



            if (!isMeetingAdmin)
            {

                if (!open)//不是共同講師走這
                {
                    if (CCO.Permissions.IdentityListHasId(Aras.Server.Security.Permissions.Current.IdentitiesList, "7AC8B33DA0F246DDA70BB244AB231E29"))
                    {

                        SQL_OnlyMyGym = " and [in_meeting_user].in_current_org=N'" + Resume.getProperty("in_current_org", "") + "'";
                    }
                }
            }

            if (meeting_id == "")
            { 
                throw new Exception("課程id不得為空白");
            }

            bool IsTemplate = false;


            aml = "<AML>" +
                "<Item type='In_Meeting' action='get' id='" + meeting_id + "'>" +
                "<Relationships>" +

                "<Item type='In_Meeting_Surveys' action='get' select='related_id(id,in_questions,in_request,in_selectoption,in_question_type,in_property,in_expense,in_is_pkey)'>" +
                "<in_surveytype>1</in_surveytype>" +
                "<in_property condition='in'>'in_l1','in_l2','in_l3'</in_property>" +
                "</Item>" +
                "</Relationships>" +
            "</Item></AML>";

            //1.取得課程主檔
            Item Meeting = inn.applyAML(aml);


            //賽事的資訊
            string in_title = Meeting.getProperty("in_title", "");//賽事名稱
            string in_address = Meeting.getProperty("in_address", "");//比賽地點
            string in_date_s = Meeting.getProperty("in_date_s", "");//賽事開始日
            string in_date_e = Meeting.getProperty("in_date_e", "");//賽事結束日
                                                                    //轉民國年

            DateTime in_date_s_dt = DateTime.Parse(in_date_s);
            DateTime in_date_e_dt = DateTime.Parse(in_date_e);
            var time_date_s = string.Format("{0}年{1}月{2}", new System.Globalization.TaiwanCalendar().GetYear(in_date_s_dt), in_date_s_dt.Month, in_date_s_dt.Day);
            var time_date_e = string.Format("{2}日", new System.Globalization.TaiwanCalendar().GetYear(in_date_e_dt), in_date_e_dt.Month, in_date_e_dt.Day);



            //開始整理EXCEL Template
            aml = "<AML>" +
                "<Item type='In_Variable' action='get'>" +
                "<in_name>meeting_excel</in_name>" +
                "<Relationships>" +
                "<Item type='In_Variable_Detail' action='get'/>" +
                "</Relationships>" +
                "</Item></AML>";
            Item Vairable = inn.applyAML(aml);

            string Template_Path = "";
            string Export_Path = "";

            //判斷此賽事的類型是是什麼
            string in_template_path = "";
            switch (Meeting.getProperty("in_meeting_type", ""))
            {
                case "game": //比賽
                    in_template_path = "template_4_path";
                    break;
                case "seminar": //講習
                    in_template_path = "template_5_path";
                    break;
            }


            for (int i = 0; i < Vairable.getRelationships("In_Variable_Detail").getItemCount(); i++)
            {
                Item In_Variable_Detail = Vairable.getRelationships("In_Variable_Detail").getItemByIndex(i);
                if (In_Variable_Detail.getProperty("in_name", "") == in_template_path)
                    Template_Path = In_Variable_Detail.getProperty("in_value", "");

                if (In_Variable_Detail.getProperty("in_name", "") == "export_path")
                    Export_Path = In_Variable_Detail.getProperty("in_value", "");
                if (!Export_Path.EndsWith(@"\"))
                    Export_Path = Export_Path + @"\";
            }

            //Word
            Xceed.Words.NET.DocX doc;//word
            doc = Xceed.Words.NET.DocX.Load(Template_Path);//載入模板
            var docTables = doc.Tables[0];//取得模板第一個表格
                                          //var d = doc.Pictures[0];



            int range = Convert.ToInt32(this.getProperty("range", "1"));
            string in_l1_condition = id_type == "staff"
                ? "AND in_l1 = N'隊職員'"
                : "AND in_l1 <> N'隊職員'";

            sql = @"SELECT * FROM 
            (
                SELECT rowno = ROW_NUMBER() OVER (ORDER BY in_current_org, in_l1), * FROM in_meeting_user WITH (NOLOCK)
                WHERE source_id = '{#meeting_id}' 
                {#in_l1_condition}
                AND in_note_state ='official' {#SQL_OnlyMyGym}
            ) AS LIST
            WHERE rowno > {#page_s} and rowno <= {#page_e}
            ";

            sql = sql.Replace("{#meeting_id}", meeting_id)
                    .Replace("{#in_l1_condition}", in_l1_condition)
                    .Replace("{#SQL_OnlyMyGym}", SQL_OnlyMyGym)
                    .Replace("{#page_s}", ((range - 1) * 300).ToString())
                    .Replace("{#page_e}", (range * 300).ToString());

            Item Orgs = inn.applySQL(sql);

            Dictionary<string, Item> DicOrgs = new Dictionary<string, Item>();
            string filename = "";
            string id_1 = "";
            string id_2 = "";
            string id_3 = "";
            string url = @"C:\Aras\Vault\PLMCTA\";
            for (int i = 0; i < Orgs.getItemCount(); i++)
            {
                Item Org = Orgs.getItemByIndex(i);


                string in_current_org = Org.getProperty("in_current_org", "");
                string in_name = Org.getProperty("in_name", "");
                string in_l3 = Org.getProperty("in_l3", "");
                string in_l1 = Org.getProperty("in_l1", "");
                string OrgName = in_current_org + "_" + in_name + "_" + in_l1 + "_" + in_l3;

                Item orgnode = inn.newItem();
                orgnode.setProperty("sheet_index", "0");
                orgnode.setProperty("last_row", "3");
                //in_meeting_user
                aml = "<AML>" +
                "<Item type='File' action='get'>" +
                "<id>" + Org.getProperty("in_photo1", "") + "</id>" +
                "</Item></AML>";
                Item Files = inn.applyAML(aml);

                if (Files.getItemCount() > 0)
                {
                    filename = Files.getProperty("filename", "");//取得檔名
                }
                orgnode.setProperty("in_photo", Org.getProperty("in_photo1", ""));
                orgnode.setProperty("filename", filename);
                orgnode.setProperty("in_name", Org.getProperty("in_name", ""));
                orgnode.setProperty("in_current_org", Org.getProperty("in_current_org", ""));
                orgnode.setProperty("in_l1", Org.getProperty("in_l1", ""));
                orgnode.setProperty("in_l2", Org.getProperty("in_l2", ""));
                orgnode.setProperty("in_l3", Org.getProperty("in_l3", ""));
                //in_meeting
                orgnode.setProperty("in_title", in_title);
                orgnode.setProperty("in_address", in_address);
                orgnode.setProperty("in_date_se", time_date_s + "~" + time_date_e);
                orgnode.setProperty("in_sno", Org.getProperty("in_sno", ""));//身分證字號


                if (DicOrgs.ContainsKey(OrgName))
                {
                    CCO.Utilities.WriteDebug(strMethodName, "選手證 key 重複: " + orgnode.dom.InnerXml);
                }
                else
                {
                    DicOrgs.Add(OrgName, orgnode);
                }
            }


            foreach (KeyValuePair<string, Item> DicOrg in DicOrgs)
            {
                Item Org = DicOrg.Value;
                doc.InsertParagraph();
                if (!Org.getProperty("in_l1", "").Contains("隊職員"))
                {

                    docTables = doc.InsertTable(doc.Tables[0]);
                    //賽事名稱
                    docTables.Rows[0].Cells[0].Paragraphs.First().Append(Org.getProperty("in_title", "").Replace("中華民國跆拳道協會", "")).Bold().Font("標楷體").FontSize(15);

                    //姓名
                    docTables.Rows[2].Cells[2].Paragraphs.First().Append(Org.getProperty("in_name", "")).Bold().Font("標楷體").FontSize(11);

                    switch (Meeting.getProperty("in_meeting_type", ""))
                    {
                        case "game": //比賽

                            //選手證
                            docTables.Rows[1].Cells[0].Paragraphs.First().Append("選手證").Bold().Font("標楷體").FontSize(36);
                            //單位
                            docTables.Rows[3].Cells[2].Paragraphs.First().Append(Org.getProperty("in_current_org", "")).Bold().Font("標楷體").FontSize(11);
                            //組別
                            docTables.Rows[4].Cells[2].Paragraphs.First().Append(Org.getProperty("in_l1", "") + "-" + Org.getProperty("in_l2", "")).Bold().Font("標楷體").FontSize(11);
                            //量級
                            docTables.Rows[5].Cells[2].Paragraphs.First().Append(Org.getProperty("in_l3", "")).Bold().Font("標楷體").FontSize(11);
                            //地點
                            docTables.Rows[6].Cells[2].Paragraphs.First().Append(Org.getProperty("in_address", "")).Bold().Font("標楷體").FontSize(11);
                            //日期
                            docTables.Rows[7].Cells[2].Paragraphs.First().Append(Org.getProperty("in_date_se", "")).Bold().Font("標楷體").FontSize(11);

                            break;
                        case "seminar": //講習

                            //學員證
                            docTables.Rows[1].Cells[0].Paragraphs.First().Append("學員證").Bold().Font("標楷體").FontSize(48);
                            //地點
                            docTables.Rows[3].Cells[2].Paragraphs.First().Append(Org.getProperty("in_address", "")).Bold().Font("標楷體").FontSize(11);
                            //日期
                            docTables.Rows[4].Cells[2].Paragraphs.First().Append(Org.getProperty("in_date_se", "")).Bold().Font("標楷體").FontSize(11);
                            //身分證
                            //docTables.Rows[5].Cells[1].Paragraphs.First().Append(Org.getProperty("in_sno","")).Bold().Font("標楷體").FontSize(11);

                            //身分證 QR Code
                            //路徑切出來
                            /*
                            id_1= Org.getProperty("in_sno","").Substring(0,1);//切割出第一個字母
                            Xceed.Document.NET.Image img = doc.AddImage(url + "in_resume" + @"\" + id_1 + @"\" + Org.getProperty("in_sno","") + ".jpg");//取得路徑圖片
                            Xceed.Document.NET.Picture p = img.CreatePicture(60,60);
                            docTables.Rows[5].Cells[1].Paragraphs.First().AppendPicture(p);
                            */
                            break;
                    }

                    //有大頭照才切
                    if (Org.getProperty("in_photo", "") != "")
                    {
                        //路徑切出來
                        //CCO.Utilities.WriteDebug("in_playerid", "in_photo_path: " + Org.getProperty("in_photo",""));
                        id_1 = Org.getProperty("in_photo", "").Substring(0, 1);
                        id_2 = Org.getProperty("in_photo", "").Substring(1, 2);
                        id_3 = Org.getProperty("in_photo", "").Substring(3, 29);

                        var pic = url + id_1 + @"\" + id_2 + @"\" + id_3 + @"\" + Org.getProperty("filename", "");

                        if (System.IO.File.Exists(pic))
                        {
                            Xceed.Document.NET.Image img = doc.AddImage(pic);//取得路徑圖片
                            Xceed.Document.NET.Picture p = img.CreatePicture(170, 120);
                            docTables.Rows[2].Cells[0].Paragraphs.First().AppendPicture(p);
                        }
                    }
                }
                else
                {
                    //doc.InsertParagraph();
                    docTables = doc.InsertTable(doc.Tables[0]);
                    //賽事名稱
                    docTables.Rows[0].Cells[0].Paragraphs.First().Append(Org.getProperty("in_title", "").Replace("中華民國跆拳道協會", "")).Bold().Font("標楷體").FontSize(15);
                    //姓名
                    docTables.Rows[2].Cells[2].Paragraphs.First().Append(Org.getProperty("in_name", "")).Bold().Font("標楷體").FontSize(11);

                    switch (Meeting.getProperty("in_meeting_type", ""))
                    {
                        case "game": //比賽

                            //領隊&教練&管理證
                            docTables.Rows[1].Cells[0].Paragraphs.First().Append(Org.getProperty("in_l3", "") + "證").Bold().Font("標楷體").FontSize(36);
                            //單位
                            docTables.Rows[3].Cells[2].Paragraphs.First().Append(Org.getProperty("in_current_org", "")).Bold().Font("標楷體").FontSize(11);
                            //組別
                            docTables.Rows[4].Cells[2].Paragraphs.First().Append(Org.getProperty("in_l1", "")).Bold().Font("標楷體").FontSize(11);
                            //量級
                            docTables.Rows[5].Cells[2].Paragraphs.First().Append(Org.getProperty("in_l3", "")).Bold().Font("標楷體").FontSize(11);
                            //地點
                            docTables.Rows[6].Cells[2].Paragraphs.First().Append(Org.getProperty("in_address", "")).Bold().Font("標楷體").FontSize(11);
                            //日期
                            docTables.Rows[7].Cells[2].Paragraphs.First().Append(Org.getProperty("in_date_se", "")).Bold().Font("標楷體").FontSize(11);

                            break;
                        case "seminar": //講習

                            //學員證
                            docTables.Rows[1].Cells[0].Paragraphs.First().Append("學員證").Bold().Font("標楷體").FontSize(48);
                            //地點
                            docTables.Rows[3].Cells[2].Paragraphs.First().Append(Org.getProperty("in_address", "")).Bold().Font("標楷體").FontSize(11);
                            //日期
                            docTables.Rows[4].Cells[2].Paragraphs.First().Append(Org.getProperty("in_date_se", "")).Bold().Font("標楷體").FontSize(11);
                            //身分證
                            //docTables.Rows[5].Cells[1].Paragraphs.First().Append(Org.getProperty("in_sno","")).Bold().Font("標楷體").FontSize(11);

                            //身分證 QR Code
                            //路徑切出來
                            /*
                            id_1= Org.getProperty("in_sno","").Substring(0,1);//切割出第一個字母
                            Xceed.Document.NET.Image img = doc.AddImage(url + "in_resume" + @"\" + id_1 + @"\" + Org.getProperty("in_sno","") + ".jpg");//取得路徑圖片
                            Xceed.Document.NET.Picture p = img.CreatePicture(60,60);
                            docTables.Rows[5].Cells[1].Paragraphs.First().AppendPicture(p);
                            */
                            break;
                    }

                    //有大頭照才切
                    if (Org.getProperty("in_photo", "") != "")
                    {
                        //路徑切出來
                        id_1 = Org.getProperty("in_photo", "").Substring(0, 1);
                        id_2 = Org.getProperty("in_photo", "").Substring(1, 2);
                        id_3 = Org.getProperty("in_photo", "").Substring(3, 29);

                        var pic = url + id_1 + @"\" + id_2 + @"\" + id_3 + @"\" + Org.getProperty("filename", "");

                        if (System.IO.File.Exists(pic))
                        {
                            Xceed.Document.NET.Image img = doc.AddImage(pic);//取得路徑圖片
                            Xceed.Document.NET.Picture p = img.CreatePicture(170, 120);
                            docTables.Rows[2].Cells[0].Paragraphs.First().AppendPicture(p);
                        }
                    }
                }
            }


            // foreach (KeyValuePair<string, Item> DicOrg in DicOrgs)
            // {
            //     Item Org = DicOrg.Value;
            //     if(Org.getProperty("in_l1","").Contains("隊職員"))
            //     {
            //         //doc.InsertParagraph();
            //         docTables = doc.InsertTable(doc.Tables[0]);
            //         //賽事名稱
            //         docTables.Rows[0].Cells[0].Paragraphs.First().Append(Org.getProperty("in_title","").Replace("中華民國跆拳道協會","")).Bold().Font("標楷體").FontSize(15);
            //         //姓名
            //         docTables.Rows[2].Cells[2].Paragraphs.First().Append(Org.getProperty("in_name","")).Bold().Font("標楷體").FontSize(11);

            //         switch(Meeting.getProperty("in_meeting_type",""))
            //         {
            //             case "game": //比賽

            //                 //領隊&教練&管理證
            //                 docTables.Rows[1].Cells[0].Paragraphs.First().Append(Org.getProperty("in_l3","") + "證").Bold().Font("標楷體").FontSize(48);
            //                 //單位
            //                 docTables.Rows[3].Cells[2].Paragraphs.First().Append(Org.getProperty("in_current_org","")).Bold().Font("標楷體").FontSize(11);
            //                 //組別
            //                 docTables.Rows[4].Cells[2].Paragraphs.First().Append(Org.getProperty("in_l1","")).Bold().Font("標楷體").FontSize(11);
            //                 //量級
            //                 docTables.Rows[5].Cells[2].Paragraphs.First().Append(Org.getProperty("in_l3","")).Bold().Font("標楷體").FontSize(11);
            //                 //地點
            //                 docTables.Rows[6].Cells[2].Paragraphs.First().Append(Org.getProperty("in_address","")).Bold().Font("標楷體").FontSize(11);
            //                 //日期
            //                 docTables.Rows[7].Cells[2].Paragraphs.First().Append(Org.getProperty("in_date_se","")).Bold().Font("標楷體").FontSize(11);

            //                 break;
            //             case "seminar": //講習

            //                 //學員證
            //                 docTables.Rows[1].Cells[0].Paragraphs.First().Append("學員證").Bold().Font("標楷體").FontSize(48);
            //                 //地點
            //                 docTables.Rows[3].Cells[2].Paragraphs.First().Append(Org.getProperty("in_address","")).Bold().Font("標楷體").FontSize(11);
            //                 //日期
            //                 docTables.Rows[4].Cells[2].Paragraphs.First().Append(Org.getProperty("in_date_se","")).Bold().Font("標楷體").FontSize(11);
            //                 //身分證
            //                 //docTables.Rows[5].Cells[1].Paragraphs.First().Append(Org.getProperty("in_sno","")).Bold().Font("標楷體").FontSize(11);

            //                 //身分證 QR Code
            //                 //路徑切出來
            //                 /*
            //                 id_1= Org.getProperty("in_sno","").Substring(0,1);//切割出第一個字母
            //                 Xceed.Document.NET.Image img = doc.AddImage(url + "in_resume" + @"\" + id_1 + @"\" + Org.getProperty("in_sno","") + ".jpg");//取得路徑圖片
            //                 Xceed.Document.NET.Picture p = img.CreatePicture(60,60);
            //                 docTables.Rows[5].Cells[1].Paragraphs.First().AppendPicture(p);
            //                 */
            //                 break;
            //         }

            //         //有大頭照才切
            //         if(Org.getProperty("in_photo","") != "")
            //         {
            //             //路徑切出來
            //             id_1= Org.getProperty("in_photo","").Substring(0,1);
            //             id_2= Org.getProperty("in_photo","").Substring(1,2);
            //             id_3= Org.getProperty("in_photo","").Substring(3,29);
            //             Xceed.Document.NET.Image img = doc.AddImage(url + id_1 + @"\" + id_2 + @"\" + id_3 + @"\" + Org.getProperty("filename",""));//取得路徑圖片
            //             Xceed.Document.NET.Picture p = img.CreatePicture(170,120);
            //             docTables.Rows[2].Cells[0].Paragraphs.First().AppendPicture(p);
            //         }
            //     }
            // }

            doc.Tables[0].Remove();
            doc.RemoveParagraphAt(0);
            doc.RemoveParagraphAt(0);

            string xlsName = Meeting.getProperty("in_title", "");
            xlsName = xlsName.Replace("+", "")
                            .Replace("-", "")
                            .Replace(".", "")
                            .Replace(" ", "");
            doc.SaveAs(Export_Path + xlsName + "_選手證" + "_區間" + range.ToString() + ".docx");
            itmR.setProperty("xls_name", xlsName + "_選手證" + "_區間" + range.ToString() + ".docx");

            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

            return itmR;
        }
    }
}