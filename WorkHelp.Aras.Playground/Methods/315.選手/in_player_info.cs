﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;
using System.Net;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class in_player_info : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 競賽連結中心-選手資訊
                輸入: 
                    - meeting_id
                    - in_sno
                    - scene
                日期: 
                    - 2021-12-09: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]in_player_info";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, this.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                InnH = _InnH,

                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                in_sno = itmR.getProperty("in_sno", ""),
                scene = itmR.getProperty("scene", ""),
            };

            //iplm 站台網址
            Item itmPlm = cfg.inn.applySQL("SELECT TOP 1 in_value FROM IN_VARIABLE WITH(NOLOCK) WHERE in_name = 'app_url'");
            if (itmPlm.isError() || itmPlm.getResult() == "")
            {
                throw new Exception("查無 iplm 站台網址");
            }
            cfg.iplm_url = itmPlm.getProperty("in_value", "").TrimEnd('/');

            //賽事
            Item itmMeeting = cfg.inn.applySQL("SELECT * FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            if (itmMeeting.isError() || itmMeeting.getResult() == "")
            {
                throw new Exception("查無 賽事資料");
            }

            itmR.setProperty("in_title", itmMeeting.getProperty("in_title", ""));

            if (cfg.scene == "modal")
            {
                Modal(cfg, itmR);
            }

            return itmR;
        }

        private void Modal(TConfig cfg, Item itmReturn)
        {
            Item itmMUsers = GetMUsers(cfg);

            if (itmMUsers.isError() || itmMUsers.getResult() == "")
            {
                itmReturn.setProperty("player_name", "查無選手資料");
                return;
                //throw new Exception("查無 選手資料");
            }

            //選手基本資訊
            Item itmFirst = itmMUsers.getItemByIndex(0);
            itmReturn.setProperty("player_name", itmFirst.getProperty("in_name", ""));
            itmReturn.setProperty("player_org", itmFirst.getProperty("in_short_org", ""));

            string user_photo = cfg.iplm_url + "/images/taekwondo/default_photo.png";
            string in_photo1 = itmFirst.getProperty("in_photo1", "");
            if (in_photo1 != "")
            {
                user_photo = cfg.iplm_url + "/DownloadMeetingFile.aspx?fileid=" + in_photo1;
            }
            itmReturn.setProperty("player_photo", user_photo);

            //選手參賽組別
            AppendMUsers(cfg, itmMUsers, itmReturn);

            //對戰情形
            Item itmEvents = GetMPEvents(cfg);
            AppendMPEvents(cfg, itmEvents, itmReturn);
        }

        private void AppendMPEvents(TConfig cfg, Item itmEvents, Item itmReturn)
        {
            if (itmEvents.isError() || itmEvents.getResult() == "")
            {
                return;
            }

            int count = itmEvents.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item itmEvent = itmEvents.getItemByIndex(i);
                Item itmLastEvent = i == 0 ? cfg.inn.newItem() : itmEvents.getItemByIndex(i - 1);

                string in_win_time = itmEvent.getProperty("in_win_time", "");
                string fighter_display = "";

                if (in_win_time == "")
                {
                    //查找對手場次
                    fighter_display = FighterEventInfo(cfg, itmEvent, itmLastEvent);
                }
                else
                {
                    fighter_display = itmEvent.getProperty("fighter_name", "");
                }

                itmEvent.setType("inn_event");
                itmEvent.setProperty("fighter_display", fighter_display);
                itmEvent.setProperty("score_display", ScoreInfo(cfg, itmEvent));
                itmReturn.addRelationship(itmEvent);
            }
        }

        private string FighterEventInfo(TConfig cfg, Item itmEvent, Item itmLastEvent)
        {
            string result = "";

            string sql = @"
                SELECT 
	                TOP 1 t1.id
	                , t1.in_tree_id
	                , t1.in_tree_no 
                FROM 
	                IN_MEETING_PEVENT t1 WITH(NOLOCK)
                WHERE 
	                t1.in_next_win = '{#next_event_id}'
	                AND t1.id <> '{#filter_id}'
            ";

            sql = sql.Replace("{#next_event_id}", itmEvent.getProperty("event_id", ""))
                .Replace("{#filter_id}", itmLastEvent.getProperty("event_id", ""));

            Item item = cfg.inn.applySQL(sql);

            if (!item.isError() && item.getResult() != "")
            {
                result = "等待場次: " + item.getProperty("in_tree_no", "");
            }

            return result;
        }

        private string ScoreInfo(TConfig cfg, Item item)
        {
            string player_status = item.getProperty("player_status", "");
            string player_points = item.getProperty("player_points", "0");
            string player_correct = item.getProperty("player_correct", "0");

            string fighter_points = item.getProperty("fighter_points", "0");
            string fighter_correct = item.getProperty("fighter_correct", "0");

            switch (player_status)
            {
                case "1":
                    if (fighter_correct == "3")
                    {
                        //對手指導犯滿
                        return "(win) " + player_points + ":" + "S3";
                    }
                    else
                    {
                        return "(win) " + player_points + ":" + fighter_points;
                    }
                case "0":
                    if (player_correct == "3")
                    {
                        return "S3" + ":" + fighter_points + " (win)";
                    }
                    else
                    {
                        return player_points + ":" + fighter_points + " (win)";
                    }
                default:
                    return "--";
            }
        }

        //選手參賽組別
        private void AppendMUsers(TConfig cfg, Item itmMUsers, Item itmReturn)
        {
            int count = itmMUsers.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item itmMUser = itmMUsers.getItemByIndex(i);
                string in_weight = itmMUser.getProperty("in_weight", "");
                if (in_weight == "")
                {
                    in_weight = "--";
                }

                itmMUser.setType("inn_muser");
                itmMUser.setProperty("in_weight", in_weight);
                itmReturn.addRelationship(itmMUser);
            }
        }

        private Item GetMPEvents(TConfig cfg)
        {
            string sql = @"

                SELECT
	                t1.in_name2			AS 'pg_name'
	                , t1.in_short_name	AS 'pg_short_name'
	                , t1.in_fight_day	AS 'pg_fight_day'
	                , t1.in_site_mat	AS 'pg_site_mat'
	                , t3.in_name		AS 'site_name'
	                , t3.in_code		AS 'site_code'
	                , t2.id				AS 'event_id'
	                , t2.in_tree_id
	                , t2.in_tree_no
	                , t2.in_time_start
	                , t2.in_time_end
	                , t2.in_win_status
	                , t2.in_win_time
	                , t2.in_win_sign_no
					, t11.in_sign_foot								AS 'player_foot'
					, t11.in_sign_no  								AS 'player_tkd_no'
					, t21.in_section_no								AS 'player_judo_no'
					, t11.in_status									AS 'player_status'
					, t11.in_points  								AS 'player_points'
					, t11.in_points_type 							AS 'player_points_type'
					, t11.in_correct_count							AS 'player_correct'
	                , ISNULL(t21.map_short_org, t11.in_player_org)	AS 'player_org'
	                , ISNULL(t21.in_team, t11.in_player_team)		AS 'player_team'
	                , ISNULL(t21.in_name, t11.in_player_name)		AS 'player_name'
	                , ISNULL(t21.in_sno, t11.in_player_sno)			AS 'player_sno'
					, t21.in_check_result							AS 'player_check'
					, t21.in_weight_message							AS 'player_weight'
					, t12.in_sign_foot								AS 'fighter_foot'
					, t12.in_sign_no								AS 'fighter_tkd_no'
					, t22.in_section_no								AS 'fighter_judo_no'
					, t12.in_status									AS 'fighter_status'
					, t12.in_points  								AS 'fighter_points'
					, t12.in_points_type 							AS 'fighter_points_type'
					, t12.in_correct_count							AS 'fighter_correct'
	                , ISNULL(t22.map_short_org, t12.in_player_org)	AS 'fighter_org'
	                , ISNULL(t22.in_team, t12.in_player_team)		AS 'fighter_team'
	                , ISNULL(t22.in_name, t12.in_player_name)		AS 'fighter_name'
	                , ISNULL(t22.in_sno, t12.in_player_sno)			AS 'fighter_sno'
					, t22.in_check_result							AS 'fighter_check'
					, t22.in_weight_message							AS 'fighter_weight'
                FROM
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PEVENT t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                INNER JOIN
	                IN_MEETING_SITE t3 WITH(NOLOCK)
	                ON t3.id = t2.in_site
                INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t11 WITH(NOLOCK)
	                ON t11.source_id = t2.id
                INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t12 WITH(NOLOCK)
	                ON t12.source_id = t11.source_id
	                AND t12.in_sign_foot = t11.in_target_foot
				LEFT OUTER JOIN
					VU_MEETING_PTEAM t21 WITH(NOLOCK)
					ON t21.source_id = t2.source_id
					AND t21.in_sign_no = t11.in_sign_no
				LEFT OUTER JOIN
					VU_MEETING_PTEAM t22 WITH(NOLOCK)
					ON t22.source_id = t2.source_id
					AND t22.in_sign_no = t12.in_sign_no
                WHERE
	                t1.in_meeting = '{#meeting_id}'
					AND ISNULL(t2.in_win_status, '') NOT IN ('bypass', 'cancel', 'nofight')
	                AND ISNULL(t2.in_tree_no, 0) > 0
	                AND ISNULL(t21.in_sno, t11.in_player_sno) = '{#in_sno}'
                ORDER BY
	                t1.in_fight_day DESC
	                , t2.in_time_start DESC
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_sno}", cfg.in_sno);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        private Item GetMUsers(TConfig cfg)
        {
            string sql = @"
                SELECT 
                	t1.in_name
                	, t1.in_sno
                	, t1.in_photo1
                	, t1.in_current_org
                	, t1.in_group
                	, t1.in_short_org
                	, t1.in_stuff_b1 
                	, t1.in_team
                	, t1.in_l1
                	, t1.in_l2
                	, t1.in_l3
                	, t1.in_index
                	, t1.in_creator_sno
                	, t1.in_section_no
                	, t1.in_weight
                	, t2.in_name AS 'pg_name'
                	, t2.in_name2 AS 'pg_name2'
                	, t2.in_name3 AS 'pg_name3'
                	, t2.in_fight_day
                	, t2.in_weight_day
                	, t2.in_site_code
                	, t2.in_site_mat
                	, t3.in_name AS 'site_name'
                FROM 
                	IN_MEETING_USER t1 WITH(NOLOCK)
                INNER JOIN
                	IN_MEETING_PROGRAM t2 WITH(NOLOCK)
                	ON t2.in_meeting = t1.source_id
                	AND t2.in_name = (ISNULL(t1.in_l1, '') + '-' + ISNULL(t1.in_l2, '') + '-' + ISNULL(t1.in_l3, ''))
                INNER JOIN
                	IN_MEETING_SITE t3 WITH(NOLOCK)
                	ON t3.id = t2.in_site
                WHERE 
                	t1.source_id = '{#meeting_id}'
                	AND t1.in_sno = '{#in_sno}'
                ORDER BY
                	t2.in_fight_day
                	, t2.in_site_mat2
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_sno}", cfg.in_sno);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public Innosoft.InnovatorHelper InnH { get; set; }

            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }

            public string meeting_id { get; set; }
            public string in_sno { get; set; }
            public string scene { get; set; }

            public string iplm_url { get; set; }
        }
    }
}