﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;
using System.Net;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class in_player_list : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;            
            
            /*
                目的: 選手證介面
                輸入: meeting_id
                日期: 
                    2021-11-02: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]in_player_list";

            Item itmR = this;

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                InnH = _InnH,

                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                scene = itmR.getProperty("scene", ""),
                folder_type = itmR.getProperty("folder_type", ""),
                output_type = itmR.getProperty("output_type", ""),
                in_current_org = itmR.getProperty("in_current_org", ""),
                in_sno = itmR.getProperty("in_sno", ""),

                vault_url = @"C:\Aras\Vault\judo\",//讀取大頭照的路徑
                OutPutFiles = new List<string>(),
            };

            switch (cfg.scene)
            {
                case "list":
                    List(cfg, itmR);
                    break;

                case "word":
                    Word(cfg, itmR);
                    break;

                default:
                    Page(cfg, itmR);
                    break;
            }

            return itmR;
        }

        #region Table

        private void List(TConfig cfg, Item itmReturn)
        {
            if (cfg.folder_type == "")
            {
                throw new Exception("請選擇資料夾模式");
            }

            //取得賽事主檔
            cfg.itmMeeting = cfg.inn.applySQL("SELECT in_title FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            //賽事名稱
            cfg.mt_title = cfg.itmMeeting.getProperty("in_title", "");

            var setting = GetSql(cfg);
            var items = cfg.inn.applySQL(setting.sql);
            int muser_count = items.getItemCount();

            if (muser_count <= 0)
            {
                throw new Exception("查無與會者資料");
            }

            List<TField> fields = new List<TField>();
            fields.Add(new TField { title = "單位", property = "map_short_org", css = "col-md-2" });
            fields.Add(new TField { title = "姓名", property = "in_name", css = "col-md-1" });
            fields.Add(new TField { title = "身分證號", property = "in_sno_display", css = "col-md-1" });
            fields.Add(new TField { title = "性別", property = "in_gender", css = "col-md-1" });
            fields.Add(new TField { title = "職務", type = SectType.Staff, css = "col-md-1" });
            fields.Add(new TField { title = "個人", type = SectType.Solo, css = "col-md-2" });
            fields.Add(new TField { title = "團體", type = SectType.Group, css = "col-md-2" });
            fields.Add(new TField { title = "格式", type = SectType.Format, css = "col-md-2" });
            fields.Add(new TField { title = "功能", type = SectType.Export, css = "" });

            string contents = "";

            switch (cfg.folder_type)
            {
                case "C1":
                    contents = ProgramTables(cfg, setting, items, fields);
                    break;

                case "C2":
                    contents = OrgTables(cfg, setting, items, fields);
                    break;

                case "C3":
                    contents = PlayerTables(cfg, setting, items, fields);
                    break;
            }


            //輸出
            itmReturn.setProperty("inn_tables", contents);
        }

        private string ProgramTables(TConfig cfg, TSetting setting, Item items, List<TField> fields)
        {
            StringBuilder builder = new StringBuilder();

            List<TProgram> pgs = MapPrograms(cfg, items);

            for (int i = 0; i < pgs.Count; i++)
            {
                if (builder.Length > 0) builder.Append("<hr>");

                var pg = pgs[i];
                builder.Append("<div class='row'>");
                builder.Append("    <div class='box-header'>");
                builder.Append("        <div class='form-inline'>");
                builder.Append("            " + pg.SectName);
                builder.Append("        </div>");
                builder.Append("    </div>");

                builder.Append("    <div class='box-body box-scroll'>");

                builder.Append("        <table class='table table-bordered' data-toggle='table'>");
                builder.Append("            <thead>");
                builder.Append("                <tr>");

                foreach (var field in fields)
                {
                    builder.Append("                <th class='text-center " + field.css + "'>" + field.title + "</th>");
                }
                builder.Append("                </tr>");
                builder.Append("            </thead>");

                builder.Append("            <tbody>");
                AppendUserRows(cfg, setting, pg.Users, builder, fields);
                builder.Append("            </tbody>");

                builder.Append("        </table>");
                builder.Append("    </div>");


                builder.Append("</div>");
            }

            return builder.ToString();
        }

        private string OrgTables(TConfig cfg, TSetting setting, Item items, List<TField> fields)
        {
            StringBuilder builder = new StringBuilder();

            List<TOrg> orgs = MapOrgs(cfg, items);

            for (int i = 0; i < orgs.Count; i++)
            {
                var org = orgs[i];
                builder.Append("<div class='row'>");
                builder.Append("    <div class='box-header'>");
                builder.Append("        <div class='form-inline'>");
                builder.Append("            " + org.ShortName);
                builder.Append("        </div>");
                builder.Append("    </div>");

                builder.Append("    <div class='box-body box-scroll'>");

                builder.Append("        <table class='table table-bordered' data-toggle='table'>");
                builder.Append("            <thead>");
                builder.Append("                <tr>");

                foreach (var field in fields)
                {
                    builder.Append("                <th class='text-center " + field.css + "'>" + field.title + "</th>");
                }
                builder.Append("                </tr>");

                builder.Append("            </thead>");

                builder.Append("            <tbody>");

                AppendUserRows(cfg, setting, org.Users, builder, fields);

                builder.Append("            </tbody>");
                builder.Append("        </table>");
                builder.Append("    </div>");


                builder.Append("</div>");
            }

            return builder.ToString();
        }

        private string PlayerTables(TConfig cfg, TSetting setting, Item items, List<TField> fields)
        {
            StringBuilder builder = new StringBuilder();

            builder.Append("<div class='row'>");
            builder.Append("    <div class='box-header'>");
            builder.Append("        <div class='form-inline'>");
            builder.Append("            " + setting.certName + "(" + setting.typeName + ")");
            builder.Append("        </div>");
            builder.Append("    </div>");

            builder.Append("    <div class='box-body box-scroll'>");

            builder.Append("        <table class='table table-bordered' data-toggle='table'>");
            builder.Append("            <thead>");
            builder.Append("                <tr>");

            foreach (var field in fields)
            {
                builder.Append("                <th class='text-center " + field.css + "'>" + field.title + "</th>");
            }
            builder.Append("                </tr>");

            builder.Append("            </thead>");

            builder.Append("            <tbody>");

            List<TOrg> orgs = MapOrgs(cfg, items);

            for (int i = 0; i < orgs.Count; i++)
            {
                var org = orgs[i];

                AppendUserRows(cfg, setting, org.Users, builder, fields);
            }

            builder.Append("            </tbody>");
            builder.Append("        </table>");
            builder.Append("    </div>");
            builder.Append("</div>");

            return builder.ToString();
        }

        private void AppendUserRows(TConfig cfg, TSetting setting, List<TMUser> users, StringBuilder builder, List<TField> fields)
        {
            for (int i = 0; i < users.Count; i++)
            {
                var user = users[i];
                if (user.WorkSects.Count > 0)
                {
                    AppendUserRows(cfg, setting, user, builder, fields);
                }
            }

            for (int i = 0; i < users.Count; i++)
            {
                var user = users[i];
                if (user.GameSects.Count > 0)
                {
                    AppendUserRows(cfg, setting, user, builder, fields);
                }
            }
        }

        private void AppendUserRows(TConfig cfg, TSetting setting, TMUser user, StringBuilder builder, List<TField> fields)
        {
            Item itmMUser = user.Value;
            string in_stuff_b1 = itmMUser.getProperty("in_stuff_b1", "");
            string in_short_org = itmMUser.getProperty("in_short_org", "");
            itmMUser.setProperty("map_short_org", in_stuff_b1 + in_short_org);

            builder.Append("<tr>");

            foreach (var field in fields)
            {
                string val = "";
                if (field.property != null)
                {
                    val = user.Value.getProperty(field.property, "");
                }
                else if (field.value != null)
                {
                    val = field.value;
                }
                else
                {
                    switch (field.type)
                    {
                        case SectType.Staff:
                            val = SectNames(cfg, user.WorkSects.OrderBy(x => x.SectSort).ToList());
                            break;

                        case SectType.Solo:
                            val = SectNames(cfg, user.SoloSects);
                            break;

                        case SectType.Group:
                            val = SectNames(cfg, user.GroupSects);
                            break;

                        case SectType.Format:
                            val = SectNames(cfg, user.FormatSects);
                            break;

                        case SectType.Export:
                            val = Buttons(cfg, user);
                            break;
                    }
                }

                builder.Append("<td>" + val + "</td>");
            }
            builder.Append("</tr>");
        }

        private string Buttons(TConfig cfg, TMUser user)
        {
            string result = "";

            if (user.WorkSects.Count > 0)
            {
                result += "<button class='btn btn-sm btn-primary'"
                    + " data-org='" + user.OrgName + "'"
                    + " data-sno='" + user.UserSno + "'"
                    + " data-code='T20'"
                    + " onclick='OneExport_Click(this)'"
                    + " >隊職員證</button>";
            }

            if (user.GameSects.Count > 0)
            {
                result += "<button class='btn btn-sm btn-primary'"
                    + " data-org='" + user.OrgName + "'"
                    + " data-sno='" + user.UserSno + "'"
                    + " data-code='T21'"
                    + " onclick='OneExport_Click(this)'"
                    + " >選手證</button>";
            }

            if (user.FormatSects.Count > 0)
            {
                result += "<button class='btn btn-sm btn-primary'"
                    + " data-org='" + user.OrgName + "'"
                    + " data-sno='" + user.UserSno + "'"
                    + " data-code='T22'"
                    + " onclick='OneExport_Click(this)'"
                    + " >格式組</button>";
            }

            return result;
        }

        private string SectNames(TConfig cfg, List<TUSect> sects)
        {
            return string.Join("<br>", sects.Select(x => x.SectName));
        }

        #endregion Table

        private void Word(TConfig cfg, Item itmReturn)
        {
            if (cfg.folder_type == "")
            {
                throw new Exception("請選擇資料夾模式");
            }

            string aml = "<AML>"
                + "<Item type='In_Meeting' action='get' id='" + cfg.meeting_id + "'>"
                + "<Relationships>"
                + "  <Item type='In_Meeting_Surveys' action='get'"
                + "    select='related_id(id,in_questions,in_request,in_selectoption,in_question_type,in_property,in_expense,in_is_pkey)'>"
                + "  <in_surveytype>1</in_surveytype>"
                + "  <in_property condition='in'>'in_l1','in_l2','in_l3'</in_property>"
                + "  </Item>"
                + "</Relationships>"
                + "</Item></AML>";

            //取得賽事主檔
            cfg.itmMeeting = cfg.inn.applyAML(aml);

            //賽事資訊
            cfg.mt_title = cfg.itmMeeting.getProperty("in_title", "");//賽事名稱
            //string in_address = cfg.itmMeeting.getProperty("in_address", "");//比賽地點
            //string in_date_s = cfg.itmMeeting.getProperty("in_date_s", "");//賽事開始日
            //string in_date_e = cfg.itmMeeting.getProperty("in_date_e", "");//賽事結束日
            //                                                               //轉民國年
            //DateTime in_date_s_dt = DateTime.Parse(in_date_s);
            //DateTime in_date_e_dt = DateTime.Parse(in_date_e);
            //var time_date_s = string.Format("{0}年{1}月{2}", new System.Globalization.TaiwanCalendar().GetYear(in_date_s_dt), in_date_s_dt.Month, in_date_s_dt.Day);
            //var time_date_e = string.Format("{2}日", new System.Globalization.TaiwanCalendar().GetYear(in_date_e_dt), in_date_e_dt.Month, in_date_e_dt.Day);

            var setting = GetSql(cfg);
            var items = cfg.inn.applySQL(setting.sql);
            int muser_count = items.getItemCount();
            if (muser_count <= 0)
            {
                throw new Exception("查無與會者資料");
            }

            var exp = ExportInfo(cfg, "template_4_path");

            //生成檔案在該賽事名稱資料夾下
            var in_title = cfg.mt_title;
            var target_folder = exp.export_Path.Replace("meeting_excel", "meeting_player").Trim('\\');
            var zip_name = in_title + "_" + setting.certName + ".zip";
            var zip_file = target_folder + "\\" + zip_name;
            exp.export_Path = target_folder + "\\" + in_title;

            switch (cfg.folder_type)
            {
                case "C1":
                    ProgramWords(cfg, exp, setting, items);
                    break;

                case "C2":
                    OrgWords(cfg, exp, setting, items);
                    break;

                case "C3":
                    PlayerWords(cfg, exp, setting, items);
                    break;
            }

            if (cfg.OutPutFiles.Count == 1)
            {
                string[] arr = cfg.OutPutFiles[0].Split(new char[] { '\\' }, StringSplitOptions.RemoveEmptyEntries);
                if (arr != null || arr.Length >= 2)
                {
                    var n1 = arr[arr.Length - 2];
                    var n2 = arr[arr.Length - 1];
                    itmReturn.setProperty("xls_name", n1+ "/"+ n2);
                }
                else
                {
                    throw new Exception("Word發生錯誤");
                }
            }
            else
            {
                //打包
                Zip(zip_file, exp.export_Path);

                //輸出
                itmReturn.setProperty("xls_name", zip_name);
            }
        }


        private void ProgramWords(TConfig cfg, TExport exp, TSetting setting, Item items)
        {
            List<TProgram> pgs = MapPrograms(cfg, items);

            //清除檔案
            FixDirAndClearFile(exp.export_Path);

            for (int i = 0; i < pgs.Count; i++)
            {
                var pg = pgs[i];

                //載入模板
                Xceed.Words.NET.DocX doc = Xceed.Words.NET.DocX.Load(exp.template_Path);
                //繪製表格
                SetWordTables(cfg, doc, pg);

                //移除表格樣板與多餘換行
                doc.Tables[0].Remove();
                doc.RemoveParagraphAt(0);
                doc.RemoveParagraphAt(0);

                string xls_name = pg.L1 + "_" + pg.SectName + "_" + setting.certName;

                xls_name = xls_name.Replace("+", "")
                                .Replace("-", "")
                                .Replace(".", "")
                                .Replace(" ", "");

                string ext_name = ".docx";

                string word_file = exp.export_Path + "\\" + xls_name + ext_name;

                doc.SaveAs(word_file);
                
                cfg.OutPutFiles.Add(word_file);
            }
        }

        private void OrgWords(TConfig cfg, TExport exp, TSetting setting, Item items)
        {
            List<TOrg> orgs = MapOrgs(cfg, items);

            //清除檔案
            FixDirAndClearFile(exp.export_Path);

            for (int i = 0; i < orgs.Count; i++)
            {
                var org = orgs[i];

                //載入模板
                Xceed.Words.NET.DocX doc = Xceed.Words.NET.DocX.Load(exp.template_Path);
                //繪製表格
                SetWordTables(cfg, doc, org);

                //移除表格樣板與多餘換行
                doc.Tables[0].Remove();
                doc.RemoveParagraphAt(0);
                doc.RemoveParagraphAt(0);

                string xls_name = org.StuffB1 + org.ShortName + "_" + setting.certName;

                xls_name = xls_name.Replace("+", "")
                                .Replace("-", "")
                                .Replace(".", "")
                                .Replace(" ", "");

                string ext_name = ".docx";

                string word_file = exp.export_Path + "\\" + xls_name + ext_name;

                doc.SaveAs(word_file);

                cfg.OutPutFiles.Add(word_file);
            }
        }

        private void PlayerWords(TConfig cfg, TExport exp, TSetting setting, Item items)
        {
            List<TOrg> orgs = MapOrgs(cfg, items);

            //載入模板
            Xceed.Words.NET.DocX doc = Xceed.Words.NET.DocX.Load(exp.template_Path);

            //清除檔案
            FixDirAndClearFile(exp.export_Path);

            foreach (var org in orgs)
            {
                SetWordTables(cfg, doc, org);
            }

            //移除表格樣板與多餘換行
            doc.Tables[0].Remove();
            doc.RemoveParagraphAt(0);
            doc.RemoveParagraphAt(0);

            string xls_name = cfg.mt_title;

            xls_name = xls_name.Replace("+", "")
                            .Replace("-", "")
                            .Replace(".", "")
                            .Replace(" ", "");

            xls_name = xls_name + "_" + setting.certName + "(" + setting.typeName + ")";

            string ext_name = ".docx";

            string word_file = exp.export_Path + "\\" + xls_name + ext_name;

            doc.SaveAs(word_file);

            cfg.OutPutFiles.Add(word_file);
        }

        private void FixDirAndClearFile(string path)
        {
            if (System.IO.Directory.Exists(path))
            {
                var dir = new System.IO.DirectoryInfo(path);
                dir.Delete(true);
            }

            if (!System.IO.Directory.Exists(path))
            {
                System.IO.Directory.CreateDirectory(path);
            }
        }

        private void Zip(string zip_file, string zip_folder)
        {
            //刪除既有檔案
            if (System.IO.File.Exists(zip_file))
            {
                System.IO.File.Delete(zip_file);
            }

            //壓縮資料庫
            using (var zip = new Ionic.Zip.ZipFile(zip_file, System.Text.Encoding.UTF8))
            {
                zip.AddDirectory(zip_folder);
                zip.Comment = "This zip was created at " + System.DateTime.Now.ToString("G");
                zip.Save();
            }
        }

        private void SetWordTables(TConfig cfg, Xceed.Words.NET.DocX doc, TProgram program)
        {
            for (int i = 0; i < program.Orgs.Count; i++)
            {
                var org = program.Orgs[i];
                SetWordTables(cfg, doc, org);
            }
        }

        private void SetWordTables(TConfig cfg, Xceed.Words.NET.DocX doc, TOrg org)
        {
            for (int j = 0; j < org.Users.Count; j++)
            {
                var user = org.Users[j];

                if (user.WorkSects.Count > 0)
                {
                    doc.InsertParagraph();
                    var table = doc.InsertTable(doc.Tables[0]);

                    StaffWordTable(cfg, doc, table, org, user);
                }
            }

            for (int j = 0; j < org.Users.Count; j++)
            {
                var user = org.Users[j];

                if (user.GameSects.Count > 0)
                {
                    doc.InsertParagraph();
                    var table = doc.InsertTable(doc.Tables[0]);
                    PlayerWordTable(cfg, doc, table, org, user);
                }
            }
        }

        //隊職員證
        private void StaffWordTable(TConfig cfg, Xceed.Words.NET.DocX doc, Xceed.Document.NET.Table table, TOrg org, TMUser user)
        {
            Item itmMUser = user.Value;

            string mt_title = cfg.mt_title;

            List<TUSect> work_sects = user.WorkSects.OrderBy(x => x.SectSort).ToList();
            string cert_name = string.Join("、", work_sects.Select(x => x.L3));

            //賽事名稱
            table.Rows[1].Cells[0].Paragraphs.First().Append(mt_title).Font("標楷體").FontSize(16);
            //隊職員證
            table.Rows[1].Cells[0].Paragraphs.First().Append("\n" + cert_name + "證").Font("標楷體").FontSize(18).Alignment = Xceed.Document.NET.Alignment.center;
            //單位(簡稱)
            table.Rows[2].Cells[1].Paragraphs.First().Append(org.ShortName).Font("標楷體").FontSize(14);

            int name_fontSize = 14;
            if (user.UserName.Length >= 20) name_fontSize = 10;
            else if (user.UserName.Length >= 10) name_fontSize = 12;
            //姓名
            table.Rows[3].Cells[1].Paragraphs.First().Append(user.UserName).Font("標楷體").FontSize(name_fontSize);

            int work_fonSize = 12;
            if (cert_name.Length >= 6) work_fonSize = 11;
            //x量級(個人)
            table.Rows[4].Cells[1].Paragraphs.First().Append("隊職員 " + cert_name).Font("標楷體").FontSize(work_fonSize);

            //設定大頭照
            SetHeadShot(cfg, doc, table, user);
        }

        //選手證
        private void PlayerWordTable(TConfig cfg, Xceed.Words.NET.DocX doc, Xceed.Document.NET.Table table, TOrg org, TMUser user)
        {
            Item itmMUser = user.Value;

            string mt_title = cfg.mt_title;

            //賽事名稱
            table.Rows[1].Cells[0].Paragraphs.First().Append(mt_title).Font("標楷體").FontSize(14);
            //選手證
            table.Rows[1].Cells[0].Paragraphs.First().Append("\n" + "選手證").Font("標楷體").FontSize(16).Alignment = Xceed.Document.NET.Alignment.center;
            //單位(簡稱)
            table.Rows[2].Cells[1].Paragraphs.First().Append(org.ShortName).Font("標楷體").FontSize(14);

            int name_fontSize = 14;
            if (user.UserName.Length >= 20) name_fontSize = 10;
            else if (user.UserName.Length >= 10) name_fontSize = 12;
            //姓名
            table.Rows[3].Cells[1].Paragraphs.First().Append(user.UserName).Font("標楷體").FontSize(name_fontSize);

            //x量級(個人)
            table.Rows[4].Cells[1].Paragraphs.First().Append(SoloSects(cfg, org, user, user.SoloSects)).Font("標楷體").FontSize(12);
            //x教練(團體)
            table.Rows[5].Cells[1].Paragraphs.First().Append(GroupSects(cfg, org, user, user.GroupSects)).Font("標楷體").FontSize(12);

            //設定大頭照
            SetHeadShot(cfg, doc, table, user);
        }

        //設定大頭照
        private void SetHeadShot(TConfig cfg, Xceed.Words.NET.DocX doc, Xceed.Document.NET.Table table, TMUser user)
        {
            try
            {
                string file_id = user.Value.getProperty("in_photo1");
                string filename = user.Value.getProperty("filename");
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "file_id: " + file_id);

                if (file_id != "")
                {
                    string id_1 = file_id.Substring(0, 1);
                    string id_2 = file_id.Substring(1, 2);
                    string id_3 = file_id.Substring(3, 29);

                    string pic = cfg.vault_url + id_1 + @"\" + id_2 + @"\" + id_3 + @"\" + filename;
                    cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "大頭照: " + pic);

                    if (System.IO.File.Exists(pic))
                    {
                        Xceed.Document.NET.Image img = doc.AddImage(pic);//取得路徑圖片
                        Xceed.Document.NET.Picture p = img.CreatePicture(150, 100);
                        table.Rows[6].Cells[0].Paragraphs.First().AppendPicture(p);
                    }
                }
            }
            catch (Exception ex)
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "大頭照出錯 _# dom: " + user.Value.dom);
            }
        }

        private string SoloSects(TConfig cfg, TOrg org, TMUser user, List<TUSect> sects)
        {
            //return "個人組";
            return string.Join("\n", sects.Select(x => x.SectName));
        }

        private string GroupSects(TConfig cfg, TOrg org, TMUser user, List<TUSect> sects)
        {
            //return "團體組";
            return string.Join("\n", sects.Select(x => x.SectName));
        }

        private string FormatSects(TConfig cfg, TOrg org, TMUser user, List<TUSect> sects)
        {
            //return "格式組";
            return string.Join("\n", sects.Select(x => x.SectName));
        }

        private List<TProgram> MapPrograms(TConfig cfg, Item items)
        {
            var objs = GetObjs(cfg, items);
            var programs = GetPrograms(cfg, objs);
            var users = GetMUsers(cfg, objs);

            foreach (var pg in programs)
            {
                var pg_users = users.FindAll(x => x.AllSects.Any(y => y.Key == pg.Key));
                if (pg_users != null && pg_users.Count > 0)
                {
                    pg.Users = pg_users.OrderBy(x => x.OrgName).ToList();
                }
                else
                {
                    pg.Users = new List<TMUser>();
                }
            }

            return programs;
        }

        private List<TOrg> MapOrgs(TConfig cfg, Item items)
        {
            var objs = GetObjs(cfg, items);

            List<TOrg> result = new List<TOrg>();

            for (int i = 0; i < objs.Count; i++)
            {
                var obj = objs[i];

                var org = result.Find(x => x.Key == obj.okey);
                if (org == null)
                {
                    org = NewOrg(obj);
                    result.Add(org);
                }

                var user = org.Users.Find(x => x.Key == obj.ukey);
                if (user == null)
                {
                    user = NewMUser(obj);
                    org.Users.Add(user);
                }

                var sect = NewSect(obj);

                user.AllSects.Add(sect);

                SetMUserSect(user, sect, obj);
            }

            return result;
        }

        private List<TObj> GetObjs(TConfig cfg, Item items)
        {
            List<TObj> list = new List<TObj>();
            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                TObj obj = MapObj(item);
                list.Add(obj);
            }
            return list;
        }

        private List<TProgram> GetPrograms(TConfig cfg, List<TObj> objs)
        {
            List<TProgram> list = new List<TProgram>();

            for (int i = 0; i < objs.Count; i++)
            {
                var obj = objs[i];
                var program = list.Find(x => x.Key == obj.skey);
                if (program == null)
                {
                    program = NewProgram(obj);
                    list.Add(program);
                }
            }

            return list;
        }

        private List<TMUser> GetMUsers(TConfig cfg, List<TObj> objs)
        {
            List<TMUser> list = new List<TMUser>();

            for (int i = 0; i < objs.Count; i++)
            {
                var obj = objs[i];
                var user = list.Find(x => x.Key == obj.ukey);
                if (user == null)
                {
                    user = NewMUser(obj);
                    list.Add(user);
                }

                var sect = NewSect(obj);

                user.AllSects.Add(sect);

                SetMUserSect(user, sect, obj);

            }

            return list;
        }

        private TProgram NewProgram(TObj obj)
        {
            var program = new TProgram
            {
                Key = obj.skey,
                L1 = obj.in_l1,
                L2 = obj.in_l2,
                L3 = obj.in_l3,
                SectName = obj.pg_name,
                Value = obj.Value,
            };

            if (program.L1 == "隊職員")
            {
                program.SectName = obj.in_l3;
                program.SectSort = GetStaffSort(obj.in_l3);
            }

            return program;
        }

        private TOrg NewOrg(TObj obj)
        {
            return new TOrg
            {
                Key = obj.okey,
                OrgName = obj.in_current_org,
                StuffB1 = obj.in_stuff_b1,
                ShortName = obj.in_short_org,
                Users = new List<TMUser>(),
                Value = obj.Value,
            };
        }

        private TMUser NewMUser(TObj obj)
        {
            return new TMUser
            {
                Key = obj.ukey,
                L1 = obj.in_l1,
                OrgName = obj.in_current_org,
                UserName = obj.in_name,
                UserSno = obj.in_sno,
                AllSects = new List<TUSect>(),
                WorkSects = new List<TUSect>(),
                GameSects = new List<TUSect>(),
                SoloSects = new List<TUSect>(),
                GroupSects = new List<TUSect>(),
                FormatSects = new List<TUSect>(),
                Value = obj.Value,
            };
        }

        private TUSect NewSect(TObj obj)
        {
            return new TUSect
            {
                Key = obj.skey,
                L1 = obj.in_l1,
                L2 = obj.in_l2,
                L3 = obj.in_l3,
                SectName = obj.pg_name,
                Value = obj.Value,
            };
        }

        private void SetMUserSect(TMUser user, TUSect sect, TObj obj)
        {
            switch (obj.in_l1)
            {
                case "隊職員":
                    sect.SectName = sect.L3;
                    sect.SectSort = GetStaffSort(sect.L3);
                    user.WorkSects.Add(sect);
                    break;

                case "團體組":
                    user.GameSects.Add(sect);
                    user.GroupSects.Add(sect);
                    break;

                case "格式組":
                    user.GameSects.Add(sect);
                    user.FormatSects.Add(sect);
                    break;

                default:
                    user.GameSects.Add(sect);
                    user.SoloSects.Add(sect);
                    break;
            }
        }

        private TExport ExportInfo(TConfig cfg, string in_name)
        {
            Item itmXls = cfg.inn.applyMethod("In_Meeting_Excel_Path", "<in_name>" + in_name + "</in_name>");

            return new TExport
            {
                template_Path = itmXls.getProperty("template_path", ""),
                export_Path = itmXls.getProperty("export_path", ""),
            };
        }

        private TSetting GetSql(TConfig cfg)
        {
            string cert_name = "";
            string type_name = "";
            string sql = "";

            List<string> ocols = new List<string>
            {
                "t1.in_gender DESC",
                "t1.in_current_org",
                "t1.in_name",
                "t1.in_excel_order",
                "t1.in_excel_order_2",
                "t1.in_excel_order_3",
            };

            if (cfg.folder_type == "C1")
            {
                ocols = new List<string>
            {
                "t11.in_sort_order",
                "t1.in_gender DESC",
                "t1.in_current_org",
                "t1.in_name",
                "t1.in_excel_order",
                "t1.in_excel_order_2",
                "t1.in_excel_order_3",
            };
            }

            List<string> sub = new List<string>();

            switch (cfg.output_type)
            {
                case "T0":
                    //隊職員單獨出一份
                    cert_name = "隊職員證";
                    type_name = "隊職員";
                    sub.Add("AND t1.in_l1 = N'隊職員'");
                    break;

                case "T1":
                    //出社會組之前判斷有沒有報過大專 高中 國中 有的則不出(只報名社會組的才出) 
                    cert_name = "選手證";
                    type_name = "只報社會";
                    sub.Add("AND t1.in_l1 NOT IN (N'隊職員', N'格式組')");
                    sub.Add("AND t1.in_l2 LIKE N'%社會%'");
                    sub.Add("AND t1.in_sno NOT IN (SELECT in_sno FROM IN_MEETING_USER WITH(NOLOCK) WHERE source_id = '{#meeting_id}' AND (in_l2 LIKE N'%大專%' or in_l2 LIKE N'%高中%' or in_l2 LIKE N'%國中%' or in_l2 LIKE N'%國小%'))");
                    break;

                case "T2":
                    //大專可跨社會(出大專有報社會 就一樣把組別拚進去  格式組不包含在這裡)
                    cert_name = "選手證";
                    type_name = "大專(含社會)";
                    sub.Add("AND t1.in_l1 NOT IN (N'隊職員', N'格式組')");
                    sub.Add("AND (t1.in_l2 LIKE N'%社會%' OR t1.in_l2 LIKE N'%大專%')");
                    sub.Add("AND t1.in_sno IN (SELECT in_sno FROM IN_MEETING_USER WITH(NOLOCK) WHERE source_id = '{#meeting_id}' AND in_l2 LIKE N'%大專%')");
                    break;

                case "T3":
                    //高中可跨社會(出高中有報社會 就一樣把組別拚進去  格式組不包含在這裡)
                    cert_name = "選手證";
                    type_name = "高中(含社會)";
                    sub.Add("AND t1.in_l1 NOT IN (N'隊職員', N'格式組')");
                    sub.Add("AND (t1.in_l2 LIKE N'%社會%' OR t1.in_l2 LIKE N'%高中%')");
                    sub.Add("AND t1.in_sno IN (SELECT in_sno FROM IN_MEETING_USER WITH(NOLOCK) WHERE source_id = '{#meeting_id}' AND in_l2 LIKE N'%高中%')");
                    break;

                case "T4":
                    //國中可跨社會(出國中有報社會 就一樣把組別拚進去  格式組不包含在這裡)
                    cert_name = "選手證";
                    type_name = "國中(含社會)";
                    sub.Add("AND t1.in_l1 NOT IN (N'隊職員', N'格式組')");
                    sub.Add("AND (t1.in_l2 LIKE N'%社會%' OR t1.in_l2 LIKE N'%國中%')");
                    sub.Add("AND t1.in_sno IN (SELECT in_sno FROM IN_MEETING_USER WITH(NOLOCK) WHERE source_id = '{#meeting_id}' AND in_l2 LIKE N'%國中%')");
                    break;

                case "T5":
                    //國小不會報社會(格式組不包含在這裡)
                    cert_name = "選手證";
                    type_name = "國小";
                    sub.Add("AND t1.in_l1 NOT IN (N'隊職員', N'格式組')");
                    sub.Add("AND t1.in_l2 LIKE N'%國小%'");
                    break;

                case "T6":
                    // 2.團體組單獨出一份(只有報團體)
                    cert_name = "選手證";
                    type_name = "只報團體";
                    sub.Add("AND t1.in_l1 = N'團體組'");
                    sub.Add("AND t1.in_sno NOT IN(SELECT in_sno FROM IN_MEETING_USER WITH(NOLOCK) WHERE source_id = '{#meeting_id}' AND in_l1 IN (N'個人組', N'格式組'))");
                    break;

                case "T7":
                    // 4.格式組單獨出一份
                    cert_name = "選手證";
                    type_name = "格式";
                    sub.Add("AND t1.in_l1 = N'格式組'");
                    break;

                case "T8":
                    //沒有以上組別(人數1200以下)
                    cert_name = "全部";
                    type_name = "1200人以下";
                    break;

                case "T9":
                    // 4.成年組單獨出一份
                    cert_name = "選手證";
                    type_name = "成年";
                    sub.Add("AND t1.in_l1 = N'成年組'");
                    break;

                case "T10":
                    //選手
                    cert_name = "選手證";
                    type_name = "選手";
                    sub.Add("AND t1.in_l1 <> N'隊職員'");
                    break;

                case "T20":
                    //單一隊職員
                    cert_name = "隊職員證";
                    type_name = "單張";
                    sub.Add("AND t1.in_current_org = N'" + cfg.in_current_org + "'");
                    sub.Add("AND t1.in_sno = N'" + cfg.in_sno + "'");
                    sub.Add("AND t1.in_l1 = N'隊職員'");
                    break;

                case "T21":
                    //單一隊職員
                    cert_name = "選手證";
                    type_name = "單張";
                    sub.Add("AND t1.in_current_org = N'" + cfg.in_current_org + "'");
                    sub.Add("AND t1.in_sno = N'" + cfg.in_sno + "'");
                    sub.Add("AND t1.in_l1 NOT IN (N'隊職員', N'格式組')");
                    break;

                case "T22":
                    //單一隊職員
                    cert_name = "選手證";
                    type_name = "單張";
                    sub.Add("AND t1.in_current_org = N'" + cfg.in_current_org + "'");
                    sub.Add("AND t1.in_sno = N'" + cfg.in_sno + "'");
                    sub.Add("AND t1.in_l1 = N'格式組'");
                    break;
            }

            sql = @"
            SELECT 
                t1.in_name
                , t1.in_sno
                , t1.in_sno_display
                , t1.in_gender
                , t1.in_section_name
                , t1.in_current_org
                , t1.in_stuff_b1
                , t1.in_short_org
                , t1.in_team
                , t1.in_l1
                , t1.in_l2
                , t1.in_l3
                , t1.in_index
                , t1.in_excel_order
                , t1.in_excel_order_2
                , t1.in_excel_order_3
                , t1.in_photo1
                , t1.in_sign_no
                , t1.in_section_no
                , t2.filename
                , t11.in_name2 AS 'pg_name'
                , t11.in_sign_time
            FROM 
                IN_MEETING_USER t1 WITH(NOLOCK) 
            LEFT OUTER JOIN
                [FILE] t2 WITH(NOLOCK)
                ON t2.id = t1.in_photo1
            LEFT OUTER JOIN
                IN_MEETING_PROGRAM t11 WITH(NOLOCK)
                ON t11.in_meeting = t1.source_id
                AND t11.in_l1 = ISNULL(t1.in_l1, '')
                AND t11.in_l2 = ISNULL(t1.in_l2, '')
                AND t11.in_l3 = ISNULL(t1.in_l3, '')
            WHERE 
                t1.source_id = '{#meeting_id}' 
                AND t1.in_note_state = 'official'
                {#sub_condition}
            ORDER BY 
                {#order_cols}
            ";

            string sub_condition = string.Join("\r\n                ", sub)
                .Replace("{#meeting_id}", cfg.meeting_id);

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#sub_condition}", sub_condition)
                .Replace("{#order_cols}", string.Join(", ", ocols));

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return new TSetting { sql = sql, typeName = type_name, certName = cert_name };
        }

        private string SectionName(string name)
        {
            string result = "";
            string[] arr = name.Split(new char[] { '-', '+', '：' });
            for (int i = 0; i < arr.Length - 1; i++)
            {
                if (arr[i].ToLower().Contains("kg"))
                {
                    continue;
                }
                if (result.Contains(arr[i]))
                {
                    continue;
                }
                result += arr[i];
            }
            return result;
        }

        private void Page(TConfig cfg, Item itmReturn)
        {
            Item itmMeeting = cfg.inn.applySQL("SELECT id, in_title FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            if (itmMeeting.isError())
            {
                itmReturn.setProperty("error_message", "取得賽事資料發生錯誤");
                return;
            }
            itmReturn.setProperty("in_title", itmMeeting.getProperty("in_title", ""));

            //附加選單資訊
            Item itmJson = cfg.inn.newItem("In_Meeting");
            itmJson.setProperty("meeting_id", cfg.meeting_id);
            itmJson.setProperty("need_id", "");
            itmJson = itmJson.apply("in_meeting_program_options");
            itmReturn.setProperty("in_level_json", itmJson.getProperty("json", ""));
            itmReturn.setProperty("in_level_count", itmJson.getProperty("total", ""));
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public Innosoft.InnovatorHelper InnH { get; set; }

            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }

            public string meeting_id { get; set; }
            public string scene { get; set; }
            public string folder_type { get; set; }
            public string output_type { get; set; }

            public string vault_url { get; set; }

            public Item itmMeeting { get; set; }
            public Item itmProgram { get; set; }

            public string mt_title { get; set; }


            public string in_current_org { get; set; }
            public string in_sno { get; set; }

            public List<string> OutPutFiles { get; set; }
        }

        private class TSetting
        {
            public string sql { get; set; }
            public string certName { get; set; }
            public string typeName { get; set; }
        }

        private class TExport
        {
            public string template_Path { get; set; }
            public string export_Path { get; set; }
        }

        private TObj MapObj(Item item)
        {
            var result = new TObj
            {
                in_current_org = item.getProperty("in_current_org", ""),
                in_stuff_b1 = item.getProperty("in_stuff_b1", ""),
                in_short_org = item.getProperty("in_short_org", ""),
                in_gender = item.getProperty("in_gender", ""),
                in_name = item.getProperty("in_name", ""),
                in_sno = item.getProperty("in_sno", "").ToUpper(),

                in_l1 = item.getProperty("in_l1", ""),
                in_l2 = item.getProperty("in_l2", ""),
                in_l3 = item.getProperty("in_l3", ""),
                pg_name = item.getProperty("pg_name", ""),

                Value = item,
            };

            result.okey = result.in_current_org + "-" + result.in_gender;
            result.ukey = result.in_current_org + "-" + result.in_name + "-" + result.in_sno;
            result.skey = result.in_l1 + "-" + result.in_l2 + "-" + result.in_l3;

            result.map_short_org = result.in_stuff_b1 + result.in_short_org;

            return result;
        }

        private class TObj
        {
            public string in_current_org { get; set; }
            public string in_stuff_b1 { get; set; }
            public string in_short_org { get; set; }

            public string in_name { get; set; }
            public string in_sno { get; set; }
            public string in_gender { get; set; }

            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }
            public string pg_name { get; set; }

            /// <summary>
            /// Org Key
            /// </summary>
            public string okey { get; set; }

            /// <summary>
            /// MUser Key
            /// </summary>
            public string ukey { get; set; }

            /// <summary>
            /// Section Key
            /// </summary>
            public string skey { get; set; }

            public string map_short_org { get; set; }

            public Item Value { get; set; }
        }

        private class TProgram
        {
            public string Key { get; set; }
            public string L1 { get; set; }
            public string L2 { get; set; }
            public string L3 { get; set; }
            public string SectName { get; set; }
            public int SectSort { get; set; }
            public List<TOrg> Orgs { get; set; }
            public List<TMUser> Users { get; set; }
            public Item Value { get; set; }
        }

        private class TOrg
        {
            public string Key { get; set; }
            public string OrgName { get; set; }
            public string StuffB1 { get; set; }
            public string ShortName { get; set; }
            public List<TMUser> Users { get; set; }
            public Item Value { get; set; }
        }

        private class TMUser
        {
            public string Key { get; set; }
            public string L1 { get; set; }
            public string OrgName { get; set; }
            public string UserName { get; set; }
            public string UserSno { get; set; }
            public Item Value { get; set; }

            public List<TUSect> AllSects { get; set; }
            public List<TUSect> WorkSects { get; set; }
            public List<TUSect> GameSects { get; set; }
            public List<TUSect> SoloSects { get; set; }
            public List<TUSect> GroupSects { get; set; }
            public List<TUSect> FormatSects { get; set; }
        }

        private class TUSect
        {
            public string Key { get; set; }
            public string L1 { get; set; }
            public string L2 { get; set; }
            public string L3 { get; set; }
            public string SectName { get; set; }
            public int SectSort { get; set; }
            public Item Value { get; set; }
        }

        private class TField
        {
            public string title { get; set; }
            public string property { get; set; }
            public string css { get; set; }
            public string value { get; set; }
            public SectType type { get; set; }
        }

        private enum SectType
        {
            None = 0,
            Staff = 100,
            Solo = 200,
            Group = 300,
            Format = 400,
            Export = 500,
        }

        private int GetStaffSort(string value)
        {
            switch (value)
            {
                case "領隊": return 100;
                case "男子領隊": return 101;
                case "女子領隊": return 102;

                case "管理": return 200;
                case "男子管理": return 201;
                case "女子管理": return 202;

                case "教練": return 300;
                case "男子教練": return 301;
                case "女子教練": return 302;
                default: return 0;
            }
        }

        private string ClearName(string value)
        {
            return value.Replace("+", "")
                .Replace("-", "")
                .Replace(".", "")
                .Replace(":", "")
                .Replace("：", "")
                .Replace(" ", "");
        }

        private string text_check(string value)
        {
            //2020.11.03 檢查丟進來的字串 是否包含{-,/} 以便切割
            bool a1 = false;
            bool a2 = false;
            bool a3 = false;

            if (value.Contains('-'))
                a1 = true;

            if (value.Contains('/'))
                a2 = true;

            if (value.Contains('~'))
                a3 = true;


            if (a1 && a2)
            {
                string[] values = value.Split(new char[2] { '-', '/' });
                return values[1];
            }
            else if (a1)
            {
                return value.Split('-')[1];
            }
            else if (a2)
            {
                return value.Split('/')[0];
            }
            else if (a3)
            {
                return value.Split('~')[1];
            }
            else
            {
                return value;
            }
        }

        private int GetIntVal(string value, int def = 0)
        {
            int result = def;
            Int32.TryParse(value, out result);
            return result;
        }

    }    
}