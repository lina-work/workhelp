﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;
using System.Net;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class in_player_site : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
               目的: 團體戰-出賽、紀錄表
               日期: 
                   - 2021-10-13 創建 (Lina)
           */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]in_team_battle_fight";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, this.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                InnH = _InnH,

                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),

                meeting_id = itmR.getProperty("meeting_id", ""),
            };

            var items = GetItems(cfg);
            
            var contents = Table(cfg, items);

            itmR.setProperty("inn_table", contents.ToString());

            return itmR;
        }

        private StringBuilder Table(TConfig cfg, Item items)
        {
            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            head.AppendLine("<thead>");
            head.AppendLine("<tr>");
            head.AppendLine("<th class='text-center'>級組</th>");
            head.AppendLine("<th class='text-center'>比賽日期</th>");
            head.AppendLine("<th class='text-center'>比賽場地</th>");
            head.AppendLine("<th class='text-center'>場次</th>");
            head.AppendLine("<th class='text-center'>預計開賽</th>");
            head.AppendLine("<th class='text-center'>白方單位</th>");
            head.AppendLine("<th class='text-center'>白方選手</th>");
            head.AppendLine("<th class='text-center'>--</th>");
            head.AppendLine("<th class='text-center'>藍方選手</th>");
            head.AppendLine("<th class='text-center'>藍方單位</th>");
            head.AppendLine("</tr>");
            head.AppendLine("</thead>");

            body.AppendLine("<tbody>");
            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                body.AppendLine("<tr>");
                body.AppendLine("  <td class='text-center'>" + item.getProperty("pg_name", "") + "</td>");
                body.AppendLine("  <td class='text-center'>" + item.getProperty("pg_fight_day", "") + "</td>");
                body.AppendLine("  <td class='text-center'>" + item.getProperty("site_name", "") + "</td>");
                body.AppendLine("  <td class='text-center'>" + item.getProperty("in_tree_no", "") + "</td>");
                body.AppendLine("  <td class='text-center'>" + item.getProperty("in_time_start", "") + "</td>");
                body.AppendLine("  <td class='text-center'>" + item.getProperty("white_org", "") + "</td>");
                body.AppendLine("  <td class='text-center'>" + item.getProperty("white_name", "") + "</td>");
                body.AppendLine("  <td class='text-center'> vs </td>");
                body.AppendLine("  <td class='text-center'>" + item.getProperty("blue_name", "") + "</td>");
                body.AppendLine("  <td class='text-center'>" + item.getProperty("blue_org", "") + "</td>");
                body.AppendLine("</tr>");
            }
            body.AppendLine("</tbody>");

            var table_name = "table_player_site";

            var builder = new StringBuilder();
            builder.AppendLine(GetTableAttribute(table_name));
            builder.Append(head);
            builder.Append(body);
            builder.AppendLine("</table>");

            return builder;
        }

        private string GetTableAttribute(string table_name)
        {
            return "<table id='" + table_name + "' class='table table-hover table-bordered table-rwd rwd rwdtable' data-toggle='table' data-click-to-select='true' data-striped='false' data-search='true' data-pagination='true' data-show-pagination-switch='false'>";
        }
        
        private Item GetItems(TConfig cfg)
        {
            string sql = @"
                SELECT
	                t1.in_name2			AS 'pg_name'
	                , t1.in_short_name	AS 'pg_short'
	                , t1.in_fight_day	AS 'pg_fight_day'
	                , t1.in_site_mat	AS 'pg_site_mat'
	                , t4.in_name		AS 'site_name'
	                , t4.in_code		AS 'site_code'
	                , t2.in_tree_id
	                , t2.in_tree_no
	                , t2.in_time_start
	                , t2.in_time_end
	                , t21.id										AS 'white_id'
	                , ISNULL(t21.map_short_org, t3.in_player_org)	AS 'white_org'
	                , ISNULL(t21.in_team, t3.in_player_team)		AS 'white_team'
	                , ISNULL(t21.in_name, t3.in_player_name)		AS 'white_name'
	                , ISNULL(t21.in_sno, t3.in_player_sno)			AS 'white_sno'
	                , t22.id										AS 'blue_id'
	                , ISNULL(t22.map_short_org, t3.in_target_org)	AS 'blue_org'
	                , ISNULL(t22.in_team, t3.in_target_team)		AS 'blue_team'
	                , ISNULL(t22.in_name, t3.in_target_name)		AS 'blue_name'
	                , ISNULL(t22.in_sno, t3.in_target_sno)			AS 'blue_sno'
                FROM
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PEVENT t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t3 WITH(NOLOCK)
	                ON t3.source_id = t2.id
                INNER JOIN
	                IN_MEETING_SITE t4 WITH(NOLOCK)
	                ON t4.id = t2.in_site
                LEFT OUTER JOIN
	                VU_MEETING_PTEAM t21 WITH(NOLOCK)
	                ON t21.source_id = t1.id
	                AND t21.in_sign_no = t3.in_sign_no
                LEFT OUTER JOIN
	                VU_MEETING_PTEAM t22 WITH(NOLOCK)
	                ON t22.source_id = t1.id
	                AND t22.in_sign_no = t3.in_target_no
                WHERE
	                t1.in_meeting = '{#meeting_id}'
	                AND ISNULL(t2.in_tree_no, 0) > 0
	                AND t3.in_sign_foot = '1'
                ORDER BY
	                t1.in_fight_day
	                , t4.in_code
	                , t2.in_tree_no
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public Innosoft.InnovatorHelper InnH { get; set; }

            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }

            public string meeting_id { get; set; }
        }
    }
}