﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;
using System.Net;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class In_File_Upload : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 
                    檔案上傳
                日期: 
                    - 2022-01-21: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]In_File_Upload";

            //CCO.Utilities.WriteDebug(strMethodName, this.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                InnH = _InnH,

                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),

                itemtype = this.getProperty("itemtype", ""),
                property = this.getProperty("property", ""),
                id = this.getProperty("id", ""),
                scene = this.getProperty("scene", ""),
            };

            if (cfg.scene == "modal")
            {
                return Modal(cfg);
            }
            else
            {
                return this;
            }
        }

        //跳窗
        private Item Modal(TConfig cfg)
        {
            string sql = @"
                SELECT 
	                TOP 1 t1.*
                    , t2.id AS      'old_file'
                    , t2.filename   'old_name'
                FROM 
	                {#itemtype} t1 WITH(NOLOCK)
                LEFT OUTER JOIN 
                    [FILE] t2 WITH(NOLOCK)
                    ON t2.id = t1.{#property}
                WHERE
	                t1.id = '{#id}'
            ";

            sql = sql.Replace("{#itemtype}", cfg.itemtype)
                .Replace("{#property}", cfg.property)
                .Replace("{#id}", cfg.id);

            Item item = cfg.inn.applySQL(sql);

            if (item.isError() || item.getResult() == "")
            {
                throw new Exception("查無資料");
            }

            return item;
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public Innosoft.InnovatorHelper InnH { get; set; }

            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }

            public string itemtype { get; set; }
            public string property { get; set; }
            public string id { get; set; }
            public string scene { get; set; }
        }
    }
}