﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;
using System.Net;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class In_Meeting_Competition_Export : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的:匯出對戰表
                日期: 
                    - 2021-12-29 循環賽紀錄表 (lina)
                    - 2021-11-29 DQ 看不到調整 (lina)
                    - 2021-10-05 8強敗部調整 (lina)
                    - 2021-03-09 創建 (Alan)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]In_Meeting_Competition_Export";

            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            string aml = "";
            string sql = "";
            string strError = "";

            Item itmR = this;

            Item itmPermit = inn.applyMethod("In_CheckIdentity", "<method>" + strMethodName + "</method><code>" + "ALL" + "</code>");
            bool isMeetingAdmin = itmPermit.getProperty("isMeetingAdmin", "") == "1";
            bool isMeetingLeader = itmPermit.getProperty("isMeetingLeader", "") == "1";
            bool can_view = isMeetingAdmin || isMeetingLeader;
            if (!can_view)
            {
                itmR.setProperty("error_message", "您無權限瀏覽此頁面，請重新登入");
                return itmR;
            }

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                RequestState = RequestState,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                inn = inn,

                meeting_id = itmR.getProperty("meeting_id", ""),
                program_id = itmR.getProperty("program_id", ""),
                in_date = itmR.getProperty("in_date", ""),
                in_l1 = itmR.getProperty("in_l1", ""),
                in_l2 = itmR.getProperty("in_l2", ""),
                in_l3 = itmR.getProperty("in_l3", ""),
                scene = itmR.getProperty("scene", ""),
                is_empty_xls = itmR.getProperty("is_empty_xls", "") == "1",
            };

            itmR.setProperty("id", cfg.meeting_id);

            sql = @"SELECT in_title, in_battle_repechage FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'";
            cfg.itmMeeting = inn.applySQL(sql);
            if (cfg.itmMeeting.isError() || cfg.itmMeeting.getResult() == "")
            {
                throw new Exception("找無該場賽事資料!");
            }

            cfg.mt_title = cfg.itmMeeting.getProperty("in_title", "");
            cfg.mt_battle_repechage = cfg.itmMeeting.getProperty("in_battle_repechage", "");
            itmR.setProperty("in_title", cfg.mt_title);

            List<TProgram> pg_list = MapProgram(cfg);
            if (pg_list.Count == 0)
            {
                throw new Exception("查無組別資料");
            }

            string target_name = cfg.mt_battle_repechage == "quarterfinals"
                ? "competition_path2"
                : "competition_path";


            Item itmXls = inn.applyMethod("In_Meeting_Excel_Path", "<in_name>" + target_name + "</in_name>");
            string Export_Path = itmXls.getProperty("export_path", "");
            string Template_Path = itmXls.getProperty("template_path", "");

            Spire.Xls.Workbook workbook = new Spire.Xls.Workbook();
            workbook.LoadFromFile(Template_Path);

            for (int i = 0; i < pg_list.Count; i++)
            {
                TProgram pg = pg_list[i];
                ExportXls(cfg, pg, workbook);
            }

            if (workbook.Worksheets.Count == 8)
            {
                throw new Exception("無對戰資料");
            }

            //移除樣板 Sheet
            for (int i = 7; i >= 0; i--)
            {
                workbook.Worksheets.RemoveAt(i);
            }


            string xlsName = "";

            if (pg_list.Count == 1)
            {
                TProgram pg = pg_list[0];

                xlsName = pg.in_fight_day
                    + "_" + pg.in_site_mat
                    + "_" + pg.in_name2
                    + "_" + DateTime.Now.ToString("MMdd_HHmmss");
            }
            else
            {

                xlsName = cfg.mt_title
                    + "_" + cfg.in_date
                    + "_" + "對戰表"
                    + "_" + DateTime.Now.ToString("MMdd_HHmmss");
            }

            string is_batch = itmR.getProperty("is_batch", "");
            string batch_export = itmR.getProperty("batch_export", "");
            if (is_batch == "1")
            {
                Export_Path = batch_export + "\\";
            }

            string xlsFile = Export_Path + xlsName + ".xlsx";
            workbook.SaveToFile(xlsFile);

            string xls_url = xlsName + ".xlsx";
            itmR.setProperty("xls_name", xls_url);

            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

            return itmR;
        }

        private void ExportXls(TConfig cfg, TProgram pg, Spire.Xls.Workbook workbook)
        {
            //取得該組對戰清單     
            Item itmPevents = GetMPEvents(cfg, pg);

            if (itmPevents.isError() || itmPevents.getItemCount() <= 0)
            {
                //throw new Exception("查無可匯出對戰表資料");
                return;
            }

            Spire.Xls.Worksheet sheetTemplate = workbook.Worksheets[pg.sheetName];

            Spire.Xls.Worksheet sheet = workbook.CreateEmptySheet();
            sheet.CopyFrom(sheetTemplate);
            sheet.Name = pg.in_short_name;


            Spire.Xls.CellRange[] ranges = { };

            string sign_time = "抽籤日期：" + pg.signTime;
            string sign_count = "參賽人數：" + pg.in_team_count;

            Spire.Xls.CellRange[] mt_ranges = sheet.FindAllString("#mt_title", true, true);
            foreach (var rg in mt_ranges)
            {
                rg.Text = cfg.mt_title;
            }

            Spire.Xls.CellRange[] pgt_ranges = sheet.FindAllString("#pg_title", true, true);
            for (int i = 0; i < pgt_ranges.Length; i++)
            {
                var rg = pgt_ranges[i];
                rg.Text = GetProgramName(pg.in_name2, pgt_ranges.Length, i);
            }

            Spire.Xls.CellRange[] pgc_ranges = sheet.FindAllString("#pg_count", true, true);
            foreach (var rg in pgc_ranges)
            {
                rg.Text = sign_count;
            }

            Spire.Xls.CellRange[] pgm_ranges = sheet.FindAllString("#pg_time", true, true);
            foreach (var rg in pgm_ranges)
            {
                rg.Text = sign_time;
            }

            // if (p_in_l1 == "團體組")
            // {
            //     in_site_mat = "";
            // }

            Spire.Xls.CellRange[] mat_ranges = sheet.FindAllString("#mat", true, true);
            foreach (var rg in mat_ranges)
            {
                rg.Text = pg.in_site_mat;
            }

            string in_fight = pg.in_fight_day.Replace("-", ".");
            if (pg.in_fight_site != "") in_fight += "-" + pg.in_fight_site;

            Spire.Xls.CellRange[] mat_fdays = sheet.FindAllString("#in_fight_day", true, true);
            foreach (var rg in mat_fdays)
            {
                rg.Text = in_fight;
            }

            switch (pg.in_battle_type)
            {
                //四柱復活賽 挑戰賽
                case "TopTwo":
                case "JudoTopFour":
                case "Challenge":
                    xlsForChallenge(cfg, pg, sheet, itmPevents);
                    break;

                //單循環賽
                case "SingleRoundRobin":
                    xlsForSingleRoundRobin(cfg, pg, sheet, itmPevents);
                    break;
            }
        }

        private string GetProgramName(string name, int cnt, int idx)
        {
            if (cnt == 3)
            {
                if (idx == 0)
                {
                    return name + " A面";
                }
                else if (idx == 1)
                {
                    return name + " B面";
                }
                else
                {
                    return name;
                }
            }
            return name;
        }

        //四柱復活賽 挑戰賽
        private void xlsForChallenge(TConfig cfg, TProgram program, Spire.Xls.Worksheet sheet, Item itmPevents)
        {
            Spire.Xls.CellRange[] ranges = { };

            for (int i = 0; i < itmPevents.getItemCount(); i++)
            {
                Item itmPevent = itmPevents.getItemByIndex(i);
                TPlayer player = MapPlayer(cfg, program, itmPevent);

                ranges = sheet.FindAllString("#" + player.in_sign_foot + player.in_tree_id, true, true);
                foreach (Spire.Xls.CellRange range in ranges)
                {
                    range.Text = player.in_section_no;
                }

                ranges = sheet.FindAllString("#" + player.in_sign_foot + "C" + player.in_tree_id, true, true);
                foreach (Spire.Xls.CellRange range in ranges)
                {
                    if (player.isPlayer)
                    {
                        range.Text = player.map_short_org;
                    }
                    else if (player.isByPass)
                    {
                        range.Text = "-bye-";
                    }
                    else
                    {
                        range.Text = "";
                    }
                }

                ranges = sheet.FindAllString("#" + player.in_sign_foot + "P" + player.in_tree_id, true, true);
                foreach (Spire.Xls.CellRange range in ranges)
                {
                    if (player.isPlayer)
                    {
                        range.Text = player.showName;
                    }
                    else if (player.isByPass)
                    {
                        range.Text = "-bye-";
                    }
                    else
                    {
                        range.Text = "";
                    }

                    if (player.isDQ)
                    {
                        range.Borders[Spire.Xls.BordersLineType.DiagonalDown].LineStyle = Spire.Xls.LineStyleType.Thin;
                        range.Borders[Spire.Xls.BordersLineType.DiagonalUp].LineStyle = Spire.Xls.LineStyleType.Thin;
                    }
                }

                ranges = sheet.FindAllString("#" + player.in_tree_id, true, true);
                foreach (Spire.Xls.CellRange range in ranges)
                {
                    if (cfg.is_empty_xls)
                    {
                        range.Text = "";
                        range.Style.Color = System.Drawing.Color.LightGray;
                    }
                    else
                    {
                        range.Text = player.in_tree_no;
                    }
                }
            }

            //五六名、七八名
            if (program.fighterCount == 7)
            {
                //7取5，所以要打56名
                SetRankEventCell(cfg, program, sheet, "rank56", "#RK56");
            }
            else if (program.fighterCount == 9)
            {
                //9取7，所以要打78名
                SetRankEventCell(cfg, program, sheet, "rank78", "#RK78");
            }

            ranges = sheet.FindAllString("#", true, true);
            foreach (Spire.Xls.CellRange range in ranges)
            {
                range.Text = "";
            }

            Item itmRanks = GetRanks(cfg, program);

            if (itmRanks.getItemCount() > 0)
            {
                string name_col = "AT";
                if (program.roundCode == 128)
                {
                    name_col = "AX";
                }

                for (int i = 0; i < itmRanks.getItemCount(); i++)
                {
                    Item itmRank = itmRanks.getItemByIndex(i);
                    sheet.Range["W" + (7 + i)].Text = itmRank.getProperty("in_final_rank", "");
                    sheet.Range["AC" + (7 + i)].Text = itmRank.getProperty("map_short_org", "");
                    sheet.Range[name_col + (7 + i)].Text = itmRank.getProperty("in_name", "");
                }
            }
        }

        private void SetRankEventCell(TConfig cfg, TProgram program, Spire.Xls.Worksheet sheet, string in_tree_name, string cell_name)
        {
            Item itmRkEvent = GetRankEvent(cfg, program, in_tree_name);

            Spire.Xls.CellRange[] ranges = sheet.FindAllString(cell_name, true, true);

            foreach (Spire.Xls.CellRange range in ranges)
            {
                if (cfg.is_empty_xls)
                {
                    range.Text = "";
                    range.Style.Color = System.Drawing.Color.LightGray;
                }
                else
                {
                    string rk_tree_no = itmRkEvent.getProperty("in_tree_no", "");
                    if (rk_tree_no == "0")
                    {
                        rk_tree_no = "";
                    }
                    range.Text = rk_tree_no;
                }
            }
        }

        //單循環賽
        private void xlsForSingleRoundRobin(TConfig cfg, TProgram program, Spire.Xls.Worksheet sheet, Item itmPevents)
        {
            Spire.Xls.CellRange[] ranges = { };

            List<string> tree_no_ls = new List<string>();

            string last_tree_id = "";
            for (int i = 0; i < itmPevents.getItemCount(); i++)
            {
                Item itmPevent = itmPevents.getItemByIndex(i);
                TPlayer player = MapPlayer(cfg, program, itmPevent);

                if (last_tree_id != player.in_tree_id)
                {
                    last_tree_id = player.in_tree_id;
                    tree_no_ls.Add(player.in_tree_no);
                }

                ranges = sheet.FindAllString("#CM" + player.robinNo, true, true);
                foreach (Spire.Xls.CellRange range in ranges)
                {
                    if (player.isPlayer)
                    {
                        range.Text = player.map_short_org;
                    }
                    else
                    {
                        range.Text = "-bye-";
                    }
                }

                ranges = sheet.FindAllString("#P" + player.robinNo, true, true);
                foreach (Spire.Xls.CellRange range in ranges)
                {
                    if (player.isPlayer)
                    {
                        range.Text = player.showName;
                    }
                    else
                    {
                        range.Text = "-bye-";
                    }

                    if (player.isDQ)
                    {
                        range.Borders[Spire.Xls.BordersLineType.DiagonalDown].LineStyle = Spire.Xls.LineStyleType.Thin;
                        range.Borders[Spire.Xls.BordersLineType.DiagonalUp].LineStyle = Spire.Xls.LineStyleType.Thin;
                    }
                }
            }

            string[] m_n;
            switch (program.teamCount)
            {
                case 2:
                    if (program.isTwoOutOfThree)
                    {
                        //三戰兩勝
                        m_n = new string[] { "", "", "" };
                        if (tree_no_ls.Count > 0) m_n[0] = "[" + tree_no_ls[0] + "]";
                        if (tree_no_ls.Count > 1) m_n[1] = "[" + tree_no_ls[1] + "]";
                        if (tree_no_ls.Count > 2) m_n[2] = "[" + tree_no_ls[2] + "]";
                    }
                    else
                    {
                        m_n = new string[] { "" };
                        m_n[0] = "[" + tree_no_ls[0] + "]";
                    }
                    break;
                case 3:
                    m_n = new string[] { "", "", "" };
                    m_n[0] = "[" + tree_no_ls[0] + "]";
                    m_n[1] = "[" + tree_no_ls[1] + "]";
                    m_n[2] = "[" + tree_no_ls[2] + "]";
                    break;
                case 4:
                    m_n = new string[] { "", "", "", "", "", "" };
                    m_n[0] = "[" + tree_no_ls[0] + "]";
                    m_n[1] = "[" + tree_no_ls[1] + "]";
                    m_n[2] = "[" + tree_no_ls[3] + "]";
                    m_n[3] = "[" + tree_no_ls[2] + "]";
                    m_n[4] = "[" + tree_no_ls[4] + "]";
                    m_n[5] = "[" + tree_no_ls[5] + "]";
                    break;
                case 5:
                    m_n = new string[] { "", "", "", "", "", "", "", "", "", "" };
                    m_n[0] = "[" + tree_no_ls[0] + "]";
                    m_n[1] = "[" + tree_no_ls[1] + "]";
                    m_n[2] = "[" + tree_no_ls[4] + "]";
                    m_n[3] = "[" + tree_no_ls[2] + "]";
                    m_n[4] = "[" + tree_no_ls[5] + "]";
                    m_n[5] = "[" + tree_no_ls[7] + "]";
                    m_n[6] = "[" + tree_no_ls[3] + "]";
                    m_n[7] = "[" + tree_no_ls[6] + "]";
                    m_n[8] = "[" + tree_no_ls[8] + "]";
                    m_n[9] = "[" + tree_no_ls[9] + "]";
                    break;
                default:
                    m_n = new string[] { "[1]", "[6]", "[4]", "[9]", "[7]", "[2]", "[3]", "[10]", "[8]", "[5]" };
                    break;

            }

            for (int i = 0; i < m_n.Length; i++)
            {
                ranges = sheet.FindAllString("#SR" + i.ToString(), true, true);
                foreach (Spire.Xls.CellRange range in ranges)
                {
                    if (cfg.is_empty_xls)
                    {
                        range.Text = "";
                        range.Style.Color = System.Drawing.Color.LightGray;
                    }
                    else
                    {
                        range.Text = m_n[i];
                    }
                }
            }

            //單循環賽資訊
            string sql = @"
            SELECT
                t1.in_win_sign_no
                , t2.in_sign_no
                , sum(t2.in_points) AS in_points
                , count(*) as qty
            FROM
                IN_MEETING_PEVENT t1 WITH(NOLOCK)
            INNER JOIN
                IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK)
                ON t2.source_id = t1.id
		    WHERE
				t2.source_id = '{#program_id}'
                AND t2.in_points is not null
            GROUP BY 
                t1.in_win_sign_no
                , t2.in_sign_no
            ";

            sql = sql.Replace("{#program_id}", program.id);

            Item itmInfos = cfg.inn.applySQL(sql);

            if (itmInfos.getItemCount() > 0)
            {
                for (int i = 0; i < itmInfos.getItemCount(); i++)
                {
                    Item itmInfo = itmInfos.getItemByIndex(i);
                    ranges = sheet.FindAllString("#WQ" + itmInfo.getProperty("in_win_sign_no", ""), true, true);
                    foreach (Spire.Xls.CellRange range in ranges)
                    {
                        range.Text = itmInfo.getProperty("qty", "");
                    }
                    ranges = sheet.FindAllString("#WP" + itmInfo.getProperty("in_win_sign_no", ""), true, true);
                    foreach (Spire.Xls.CellRange range in ranges)
                    {
                        range.Text = itmInfo.getProperty("in_points", "");
                    }
                }
            }

            ranges = sheet.FindAllString("#C", true, true);
            foreach (Spire.Xls.CellRange range in ranges)
            {
                range.Text = "-bye-";
            }

            ranges = sheet.FindAllString("#P", true, true);
            foreach (Spire.Xls.CellRange range in ranges)
            {
                range.Text = "-bye-";
            }

            ranges = sheet.FindAllString("#SR", true, true);
            foreach (Spire.Xls.CellRange range in ranges)
            {
                range.Text = "";
            }


            Item itmRanks = GetRanks(cfg, program);

            if (itmRanks.getItemCount() > 0)
            {
                for (int i = 0; i < itmRanks.getItemCount(); i++)
                {
                    Item itmRank = itmRanks.getItemByIndex(i);
                    //單循環名次
                    ranges = sheet.FindAllString("#WR" + itmRank.getProperty("in_sign_no", ""), true, true);
                    foreach (Spire.Xls.CellRange range in ranges)
                    {
                        range.Text = itmRank.getProperty("in_final_rank", "");
                    }

                    ranges = sheet.FindAllString("#WQ" + itmRank.getProperty("in_sign_no", ""), true, true);
                    foreach (Spire.Xls.CellRange range in ranges)
                    {
                        range.Text = "0";
                    }

                    ranges = sheet.FindAllString("#WP" + itmRank.getProperty("in_sign_no", ""), true, true);
                    foreach (Spire.Xls.CellRange range in ranges)
                    {
                        range.Text = "0";
                    }

                    if (i > 3)
                    {
                        continue;
                    }

                    sheet.Range["V" + (7 + i)].Text = itmRank.getProperty("in_final_rank", "");
                    sheet.Range["AB" + (7 + i)].Text = itmRank.getProperty("map_short_org", "");
                    sheet.Range["AS" + (7 + i)].Text = itmRank.getProperty("in_name", "");
                }
            }

            //循環賽紀錄表
            SetRobinRecord(cfg, sheet, itmPevents);

            ranges = sheet.FindAllString("#W", true, true);
            foreach (Spire.Xls.CellRange range in ranges)
            {
                range.Text = "";
            }
        }

        private void SetRobinRecord(TConfig cfg, Spire.Xls.Worksheet sheet, Item itmPevents)
        {
            Spire.Xls.CellRange[] ranges = { };

            var dic = MapToDic(cfg, itmPevents);
            foreach (var kv in dic)
            {
                ranges = sheet.FindAllString(kv.Key, true, true);
                foreach (Spire.Xls.CellRange range in ranges)
                {
                    range.Text = kv.Value;
                }
            }
        }

        private Item GetRankEvent(TConfig cfg, TProgram program, string in_tree_name)
        {
            string sql = "SELECT * FROM IN_MEETING_PEVENT WITH(NOLOCK)"
                + " WHERE source_id = '" + program.id + "'"
                + " AND in_tree_name = '" + in_tree_name + "'";

            Item item = cfg.inn.applySQL(sql);

            if (item.isError() || item.getResult() == "")
            {
                item = cfg.inn.newItem();
            }

            return item;
        }

        private Item GetMPEvents(TConfig cfg, TProgram program)
        {
            string sql = @"
                SELECT
                    t1.in_tree_id
                    , t1.in_tree_no
                    , t1.in_win_time
                    , t1.in_win_local_time
                    , t2.in_sign_foot
                    , t2.in_sign_no
                    , t2.in_sign_bypass
                    , t2.in_points
                    , t2.in_correct_count
                    , t2.in_status
					, t3.in_name			AS 'site_name'
                    , t11.in_section_no
                    , t11.in_name
                    , t11.map_short_org
                    , t11.in_final_rank
                    , t11.in_sign_time
                    , t11.in_weight_value
                    , t11.in_weight_message
                FROM
                    IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
                    IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK)
                    ON t2.source_id = t1.id
                LEFT OUTER JOIN
                    IN_MEETING_SITE t3 WITH(NOLOCK)
                    ON t3.id = t1.in_site
                LEFT OUTER JOIN
                    VU_MEETING_PTEAM  t11 WITH(NOLOCK)
                    ON t11.source_id = t1.source_id
                    AND t11.in_sign_no = t2.in_sign_no
    	        WHERE
    			    t1.source_id = '{#program_id}'
    			    AND in_tree_name in ('main', 'repechage')
                ORDER BY
                      t1.in_tree_sort
    			    , t1.in_tree_id
                    , t1.in_tree_no
                    , t2.in_sign_foot
            ";

            sql = sql.Replace("{#program_id}", program.id);

            return cfg.inn.applySQL(sql);
        }

        //名次
        private Item GetRanks(TConfig cfg, TProgram program)
        {
            string sql = @"
                SELECT 
                    in_name
                    , map_short_org
                    , in_final_rank 
                    , in_judo_no
                FROM 
                    VU_MEETING_PTEAM WITH(NOLOCK)
                WHERE 
                    source_id = '{#program_id}'
				    AND ISNULL(in_final_rank, 0) > 0
                ORDER BY 
                    in_final_rank";

            sql = sql.Replace("{#program_id}", program.id);

            return cfg.inn.applySQL(sql);
        }

        private List<TProgram> MapProgram(TConfig cfg)
        {
            Dictionary<string, int> fighter_map = FighterCountMap(cfg);

            List<TProgram> result = new List<TProgram>();

            string sql = "";

            if (cfg.program_id != "")
            {
                sql = @"SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + cfg.program_id + "'";
            }
            else
            {
                sql = @"SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE in_meeting = '" + cfg.meeting_id + "' AND in_fight_day = '" + cfg.in_date + "'";
            }

            sql += " ORDER BY in_site_mat2";

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);

                TProgram obj = new TProgram
                {
                    id = item.getProperty("id", ""),
                    in_name2 = item.getProperty("in_name2", ""),
                    in_short_name = item.getProperty("in_short_name", ""),
                    in_team_count = item.getProperty("in_team_count", "0"),
                    in_round_code = item.getProperty("in_round_code", "0"),
                    in_battle_type = item.getProperty("in_battle_type", ""),
                    in_site_mat = item.getProperty("in_site_mat", ""),
                    in_fight_day = item.getProperty("in_fight_day", ""),
                    in_fight_site = item.getProperty("in_fight_site", ""),
                    in_sign_time = item.getProperty("in_sign_time", ""),

                    in_l1 = item.getProperty("in_l1", ""),
                    in_l2 = item.getProperty("in_l2", ""),
                    in_l3 = item.getProperty("in_l3", ""),

                    Value = item,
                };

                obj.isRobin = obj.in_battle_type.Contains("SingleRoundRobin");

                if (obj.isRobin && obj.in_team_count == "2")
                {
                    obj.isTwoOutOfThree = true;
                }

                if (obj.isTwoOutOfThree)
                {
                    obj.sheetName = "TwoOutOfThree";
                }
                else if (obj.isRobin)
                {
                    obj.sheetName = "SingleRoundRobin";
                }
                else
                {
                    obj.sheetName = obj.in_round_code;
                }

                obj.signTime = GetDateTimeValue(obj.in_sign_time, "yyyy-MM-dd", 8);
                obj.teamCount = GetIntVal(obj.in_team_count);
                obj.roundCode = GetIntVal(obj.in_round_code);

                if (fighter_map.ContainsKey(obj.id))
                {
                    obj.fighterCount = fighter_map[obj.id];
                }
                else
                {
                    obj.fighterCount = 0;
                }

                result.Add(obj);
            }

            return result;
        }

        private TPlayer MapPlayer(TConfig cfg, TProgram program, Item itmPevent)
        {
            TPlayer result = new TPlayer
            {
                in_tree_id = itmPevent.getProperty("in_tree_id", ""),
                in_tree_no = itmPevent.getProperty("in_tree_no", ""),
                map_short_org = itmPevent.getProperty("map_short_org", ""),
                in_section_no = itmPevent.getProperty("in_section_no", ""),
                in_name = itmPevent.getProperty("in_name", ""),
                in_weight_message = itmPevent.getProperty("in_weight_message", ""),
                in_sign_foot = itmPevent.getProperty("in_sign_foot", "").PadLeft(2, '0'),
                in_sign_bypass = itmPevent.getProperty("in_sign_bypass", ""),
            };

            result.robinNo = result.in_section_no.PadLeft(2, '0');

            if (result.in_tree_no == "0")
            {
                result.in_tree_no = "";
            }

            result.isPlayer = result.map_short_org != "" || result.in_name != "";
            result.isByPass = result.in_sign_bypass == "1";

            if (result.in_weight_message != "")
            {
                result.isDQ = true;
                result.showName = "(DQ)" + result.in_name;
            }
            else
            {
                result.showName = result.in_name;
            }

            return result;
        }

        private Dictionary<string, int> FighterCountMap(TConfig cfg)
        {
            string condition = "";

            if (cfg.program_id != "")
            {
                condition = @"t2.id = '" + cfg.program_id + "'";
            }
            else
            {
                condition = @"t2.in_meeting = '" + cfg.meeting_id + "' AND t2.in_fight_day = '" + cfg.in_date + "'";
            }

            Dictionary<string, int> result = new Dictionary<string, int>();

            string sql = @"
                SELECT 
	                t1.source_id
	                , count(*) AS 'cnt' 
                FROM 
	                IN_MEETING_PTEAM t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PROGRAM t2 WITH(NOLOCK)
	                ON t2.id = t1.source_id
                WHERE 
	                {#condition}
	                AND ISNULL(t1.in_type, '') IN ('', 'p') 
	                AND ISNULL(t1.in_weight_message, '') = '' 
                GROUP BY 
	                t1.source_id
            ";

            sql = sql.Replace("{#condition}", condition);

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string source_id = item.getProperty("source_id", "");
                string cnt = item.getProperty("cnt", "0");
                result.Add(source_id, GetIntVal(cnt));
            }

            return result;
        }

        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Aras.Server.Core.IContextState RequestState { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public Innovator inn { get; set; }

            public string meeting_id { get; set; }
            public string program_id { get; set; }
            public string in_date { get; set; }
            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }
            public string scene { get; set; }

            public Item itmMeeting { get; set; }

            public string mt_title { get; set; }
            public string mt_battle_repechage { get; set; }
            public bool is_empty_xls { get; set; }
        }

        private class TProgram
        {
            public Item Value { get; set; }
            public string id { get; set; }
            public string in_name2 { get; set; }
            public string in_short_name { get; set; }
            public string in_team_count { get; set; }
            public string in_round_code { get; set; }
            public string in_battle_type { get; set; }
            public string in_site_mat { get; set; }
            public string in_fight_day { get; set; }
            public string in_fight_site { get; set; }
            public string in_sign_time { get; set; }

            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }


            public bool isRobin { get; set; }
            public bool isTwoOutOfThree { get; set; }
            public string sheetName { get; set; }
            public string signTime { get; set; }
            public int teamCount { get; set; }
            public int roundCode { get; set; }

            public int fighterCount { get; set; }
        }

        private class TPlayer
        {
            public string in_tree_id { get; set; }
            public string in_tree_no { get; set; }
            public string in_section_no { get; set; }
            public string map_short_org { get; set; }
            public string in_name { get; set; }
            public string in_sign_foot { get; set; }
            public string in_sign_bypass { get; set; }

            public string in_weight_message { get; set; }

            public bool isPlayer { get; set; }
            public bool isByPass { get; set; }
            public bool isDQ { get; set; }

            public string showName { get; set; }
            public string robinNo { get; set; }
        }

        private Dictionary<string, string> MapToDic(TConfig cfg, Item items)
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();

            List<TEvent> list = MapEvents(items).OrderBy(x => x.TreeNo).ToList();

            int max_idx = list.Count - 1;
            for (int i = 1; i <= 10; i++)
            {
                int idx = i - 1;
                string row = "R" + i.ToString().PadLeft(2, '0');

                TEvent evt = null;

                if (idx > max_idx)
                {
                    evt = new TEvent
                    {
                        Value = cfg.inn.newItem(),
                        Foot1 = cfg.inn.newItem(),
                        Foot2 = cfg.inn.newItem(),
                    };
                }
                else
                {
                    evt = list[idx];
                }

                string site_name = evt.Value.getProperty("site_name", "");
                string in_tree_no = evt.Value.getProperty("in_tree_no", "");
                string in_win_status = evt.Value.getProperty("in_win_status", "");

                site_name = site_name.Replace("第", "").Replace("場地", "");
                if (in_win_status == "cancel") in_tree_no += "(C)";

                for (int j = 1; j <= 10; j++)
                {
                    string col = "C" + j.ToString().PadLeft(2, '0');

                    string pos = "#" + row + col;
                    string val = "";

                    switch (col)
                    {
                        case "C01"://場地
                            val = site_name;
                            break;
                        case "C02"://場次
                            val = in_tree_no;
                            break;
                        case "C03"://白方選手
                            val = evt.Foot1.getProperty("in_name", "");
                            break;
                        case "C04"://白方選手單位
                            val = evt.Foot1.getProperty("map_short_org", "");
                            break;
                        case "C05"://白方選手體重
                            val = evt.Foot1.getProperty("in_weight_value", "");
                            break;
                        case "C06"://藍方選手
                            val = evt.Foot2.getProperty("in_name", "");
                            break;
                        case "C07"://藍方選手單位
                            val = evt.Foot2.getProperty("map_short_org", "");
                            break;
                        case "C08"://藍方選手體重
                            val = evt.Foot2.getProperty("in_weight_value", "");
                            break;
                        case "C09"://得分
                            val = StatusInfo(evt);
                            break;
                        case "C10"://時間
                            val = evt.Value.getProperty("in_win_local_time", "");
                            break;
                    }

                    dic.Add(pos, val);
                }
            }

            return dic;
        }

        private string StatusInfo(TEvent evt)
        {
            string in_win_time = evt.Value.getProperty("in_win_time", "");
            if (in_win_time == "") return "";

            string f1_points = evt.Foot1.getProperty("in_points", "0");
            string f1_correct = evt.Foot1.getProperty("in_correct_count", "0");

            string f2_points = evt.Foot2.getProperty("in_points", "0");
            string f2_correct = evt.Foot2.getProperty("in_correct_count", "0");

            if (f1_correct == "3") return "S3" + ":" + f2_points;
            if (f2_correct == "3") return f1_points + ":" + "S3";

            return f1_points + ":" + f2_points;
        }

        private List<TEvent> MapEvents(Item items)
        {
            List<TEvent> list = new List<TEvent>();

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string id = item.getProperty("in_tree_id", "");
                string in_tree_no = item.getProperty("in_tree_no", "0");
                string in_sign_foot = item.getProperty("in_sign_foot", "");

                var evt = list.Find(x => x.Id == id);

                if (evt == null)
                {
                    evt = new TEvent
                    {
                        Id = id,
                        TreeNo = GetIntVal(in_tree_no),
                        Value = item,
                    };

                    list.Add(evt);
                }

                if (in_sign_foot == "1")
                {
                    evt.Foot1 = item;
                }
                else
                {
                    evt.Foot2 = item;
                }
            }

            return list;
        }

        private class TEvent
        {
            public string Id { get; set; }
            public int TreeNo { get; set; }
            public Item Value { get; set; }
            public Item Foot1 { get; set; }
            public Item Foot2 { get; set; }
        }

        private string GetDateTimeValue(string value, string format = "yyyy-MM-dd", int hours = 0)
        {
            if (value == null || value == "") return "";

            DateTime dt = DateTime.Now;
            if (DateTime.TryParse(value, out dt))
            {
                return dt.AddHours(hours).ToString(format);
            }
            else
            {
                return value;
            }
        }

        private int GetIntVal(string value, int def = 0)
        {
            int result = def;
            Int32.TryParse(value, out result);
            return result;
        }
    }
}