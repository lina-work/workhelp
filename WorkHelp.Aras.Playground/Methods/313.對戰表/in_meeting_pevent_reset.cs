﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class in_meeting_pevent_reset : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
            目的: 重設所有場次
            輸入: 
                - meeting_id
                - program_id
            日期: 
                - 2021-12-01: 循環賽清資料 (lina)
                - 2021-02-04: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]in_meeting_pevent_reset";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "input dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                RequestState = RequestState,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                inn = inn,
                meeting_id = itmR.getProperty("meeting_id", ""),
                program_id = itmR.getProperty("program_id", ""),
                in_date = itmR.getProperty("in_date", ""),
                scene = itmR.getProperty("scene", ""),
                is_run = itmR.getProperty("is_run", ""),
                is_build = itmR.getProperty("is_build", ""),
                Service = new InnSport.Core.Services.Impl.JudoSportService()
            };

            if (cfg.meeting_id == "")
            {
                itmR.setProperty("error_message", "賽事 id 不得為空白");
                return itmR;
            }

            //檢查頁面權限
            Item itmPermit = inn.applyMethod("In_CheckIdentity", "<method>" + strMethodName + "</method><code>" + "ALL" + "</code>");
            bool isMeetingAdmin = itmPermit.getProperty("isMeetingAdmin", "") == "1";
            bool isMeetingLeader = itmPermit.getProperty("isMeetingLeader", "") == "1";
            bool can_view = isMeetingAdmin || isMeetingLeader;
            if (!can_view)
            {
                itmR.setProperty("error_message", "您無權限瀏覽此頁面");
                return itmR;
            }

            //重設所有場次
            ResetAllEvents(cfg, itmR);

            return itmR;
        }

        /// <summary>
        /// 重設所有場次
        /// </summary>
        private void ResetAllEvents(TConfig cfg, Item itmReturn)
        {
            string sql = "";

            if (cfg.in_date != "")
            {
                sql = @"
                    SELECT
                    	t1.*
                    FROM
                    	IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                    INNER JOIN
                    	IN_MEETING_ALLOCATION t2 WITH(NOLOCK)
                    	ON t2.in_program = t1.id
                    WHERE
                    	t1.in_meeting = '{#meeting_id}'
                    	AND t2.in_date_key = '{#in_date_key}'
                    ORDER BY
                    	t2.created_on
                ";

                sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                    .Replace("{#in_date_key}", cfg.in_date);
            }
            else if (cfg.program_id != "")
            {
                sql = "SELECT * FROM In_Meeting_Program WITH(NOLOCK) WHERE id = '" + cfg.program_id + "'";
            }
            else
            {
                sql = "SELECT * FROM In_Meeting_Program WITH(NOLOCK) WHERE in_meeting = '" + cfg.meeting_id + "'";
            }

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item itmPrograms = cfg.inn.applySQL(sql);
            if (itmPrograms.isError() || itmPrograms.getItemCount() <= 0)
            {
                throw new Exception("組別量級錯誤");
            }

            var count = itmPrograms.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item itmProgram = itmPrograms.getItemByIndex(i);
                TProgram entProgram = new TProgram
                {
                    Id = itmProgram.getProperty("id", ""),
                    BattleType = itmProgram.getProperty("in_battle_type", ""),
                    TeamCount = GetInt(itmProgram.getProperty("in_team_count", "0")),
                    RoundCode = GetInt(itmProgram.getProperty("in_round_code", "0")),
                    RoundCount = GetInt(itmProgram.getProperty("in_round_count", "0")),
                };

                entProgram.IsRoundRobin = entProgram.BattleType.Contains("RoundRobin");
                if (entProgram.TeamCount <= 1 || entProgram.IsRoundRobin)
                {
                    //重設場次(非重建)
                    ResetRobinEvents(cfg, entProgram);

                    //重算場次數
                    RecountEvents(cfg, entProgram);

                    continue;
                }

                entProgram.NoList = cfg.Service.TkdNoListByPosition(entProgram.TeamCount);
                entProgram.ByPassNoList = entProgram.NoList.FindAll(x => x.IsByPass).Select(x => x.TkdNo).ToList();

                // if (cfg.is_build != "1")
                // {
                //     //重設場次(非重建)
                //     ResetEvents(cfg, entProgram);
                // }

                //重設場次(非重建)
                ResetEvents(cfg, entProgram);

                //勝部
                var map = GetMPEDTs(cfg, entProgram);

                foreach (var kv in map)
                {
                    var evt = kv.Value;
                    FixEventBypass(cfg, entProgram, evt);
                }

                //更新場次序號(時間)
                UpdateEventTreeNo(cfg, entProgram);

                //更新場次序號(時間) (敗部)
                UpdateRpcEventTreeNo(cfg, entProgram);

                //修正場次序號(時間) (敗部)
                FixRpcEventTreeNo(cfg, entProgram);

                if (entProgram.TeamCount == 4)
                {
                    //復原 3, 4 名場次序號
                    FixRank34TreeNo(cfg, entProgram);

                }
                else
                {
                    //清空敗部 3, 4 名場次序號
                    ClearRpcFinalTreeNo(cfg, entProgram);
                }

                //重算場次數
                RecountEvents(cfg, entProgram);

                // if (cfg.is_run != "1")
                // {
                //     //修補隊伍的量級序號
                //     FixTeamSectionNo(cfg, itmProgram);
                // }

                // //重新註記等待選手場次
                // RemarkWaitPlayers(cfg, itmProgram);

                //自動晉級
                AutoUpgrade(cfg, itmProgram);

                if (cfg.is_build != "1")
                {
                    //修補日期
                    FixDate(cfg, itmProgram);
                }
            }
        }

        //重算場次數
        private void RecountEvents(TConfig cfg, TProgram entProgram)
        {
            string sql = "";
            Item itmSQL = null;

            string program_id = entProgram.Id;

            sql = @"
                UPDATE t1 SET
	                t1.in_event_count = t2.cnt
                FROM
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
                (
	                SELECT
		                source_id
		                , count(*) AS 'cnt'
	                FROM
		                IN_MEETING_PEVENT WITH(NOLOCK)
	                WHERE
		                source_id = '{#program_id}'
		                AND ISNULL(in_tree_no, 0) > 0
		                AND ISNULL(in_win_status, '') NOT IN ('cancel', 'nofight', 'bypass')
	                GROUP BY
		                source_id
                ) t2
                ON t2.source_id = t1.id
            ";

            sql = sql.Replace("{#program_id}", program_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            itmSQL = cfg.inn.applySQL(sql);

            if (itmSQL.isError()) throw new Exception("發生錯誤: 重算場次數");
        }

        //復原 3, 4 名場次序號
        private void FixRank34TreeNo(TConfig cfg, TProgram entProgram)
        {
            string sql = "";
            Item itmSQL = null;

            string program_id = entProgram.Id;

            sql = @"
                UPDATE IN_MEETING_PEVENT SET
                	in_tree_no = 4
                WHERE 
                	source_id = '{#program_id}' 
                	AND in_tree_name IN ('rank34', 'repechage')
            ";

            sql = sql.Replace("{#program_id}", program_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            itmSQL = cfg.inn.applySQL(sql);

            if (itmSQL.isError()) throw new Exception("發生錯誤: 清空敗部 3, 4 名場次序號");

        }

        //清空敗部 3, 4 名場次序號
        private void ClearRpcFinalTreeNo(TConfig cfg, TProgram entProgram)
        {
            string sql = "";
            Item itmSQL = null;

            string program_id = entProgram.Id;

            sql = @"
                UPDATE IN_MEETING_PEVENT SET
                	in_tree_no = NULL
                WHERE 
                	source_id = '{#program_id}' 
                	AND in_tree_name = 'repechage' 
                	AND in_round_code = 2
            ";

            sql = sql.Replace("{#program_id}", program_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            itmSQL = cfg.inn.applySQL(sql);

            if (itmSQL.isError()) throw new Exception("發生錯誤: 清空敗部 3, 4 名場次序號");

        }

        /// <summary>
        /// 重設場次(非重建)
        /// </summary>
        private void ResetRobinEvents(TConfig cfg, TProgram entProgram)
        {
            string sql = "";
            Item itmSQL = null;

            string program_id = entProgram.Id;

            //清除對戰
            sql = @"
                UPDATE t1 SET
                    in_status = NULL
	                , in_sign_bypass = NULL
                    , in_sign_status = NULL
                    , in_sign_action = NULL
	                , in_target_bypass = NULL
                    , in_target_status = NULL
                    , in_points = NULL
                    , in_points_type = NULL
                    , in_points_text = NULL
                    , in_correct_count = NULL
                FROM
                    IN_MEETING_PEVENT_DETAIL t1
                INNER JOIN
                    IN_MEETING_PEVENT t2
                    ON t2.id = t1.source_id
                WHERE
                    t2.source_id = '{#program_id}'
            ";

            sql = sql.Replace("{#program_id}", program_id);
            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);
            itmSQL = cfg.inn.applySQL(sql);
            if (itmSQL.isError()) throw new Exception("發生錯誤: 重設循環賽【場次明細】資料");


            //【場次】資料
            sql = @"
                UPDATE t1 SET
	                in_bypass_foot = NULL
	                , in_bypass_status = NULL
	                , in_win_status = NULL
	                , in_win_sign_no = NULL
	                , in_win_time = NULL
	                , in_win_creator = NULL
	                , in_win_creator_sno = NULL
	                , in_wait_player = NULL
                    , in_win_local = NULL
                FROM
	                IN_MEETING_PEVENT t1
                WHERE
	                t1.source_id = '{#program_id}'
            ";

            sql = sql.Replace("{#program_id}", program_id);
            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);
            itmSQL = cfg.inn.applySQL(sql);
            if (itmSQL.isError()) throw new Exception("發生錯誤: 重設循環賽【場次】資料");
        }

        /// <summary>
        /// 重設場次(非重建)
        /// </summary>
        private void ResetEvents(TConfig cfg, TProgram entProgram)
        {
            string sql = "";
            Item itmSQL = null;

            string program_id = entProgram.Id;

            //勝部第一回合場次【對戰】資料 (不能清籤號)
            sql = @"
                UPDATE t1 SET
                    in_status = NULL
	                , in_sign_bypass = NULL
                    , in_sign_status = NULL
                    , in_sign_action = NULL
	                , in_target_bypass = NULL
                    , in_target_status = NULL
                    , in_points = NULL
                    , in_points_type = NULL
                    , in_points_text = NULL
                    , in_correct_count = NULL
                FROM
                    IN_MEETING_PEVENT_DETAIL t1
                INNER JOIN
                    IN_MEETING_PEVENT t2
                    ON t2.id = t1.source_id
                WHERE
                    t2.source_id = '{#program_id}'
                    AND t2.in_tree_name IN (N'main', N'sub')
                    AND t2.in_round = 1
            ";

            sql = sql.Replace("{#program_id}", program_id);
            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);
            itmSQL = cfg.inn.applySQL(sql);
            if (itmSQL.isError()) throw new Exception("發生錯誤: 重設勝部第一回合場次【對戰】資料");

            //勝部第二回合以上場次【對戰】資料
            sql = @"
                UPDATE t1 SET
                    in_status = NULL
                    , in_sign_no = NULL
	                , in_sign_bypass = NULL
                    , in_sign_status = NULL
                    , in_sign_action = NULL
                    , in_target_no = NULL
	                , in_target_bypass = NULL
                    , in_target_status = NULL
                    , in_points = NULL
                    , in_points_type = NULL
                    , in_points_text = NULL
                    , in_correct_count = NULL
                FROM
                    IN_MEETING_PEVENT_DETAIL t1
                INNER JOIN
                    IN_MEETING_PEVENT t2
                    ON t2.id = t1.source_id
                WHERE
                    t2.source_id = '{#program_id}'
                    AND t2.in_tree_name IN (N'main', N'sub')
                    AND t2.in_round >= 2
            ";

            sql = sql.Replace("{#program_id}", program_id);
            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);
            itmSQL = cfg.inn.applySQL(sql);
            if (itmSQL.isError()) throw new Exception("發生錯誤: 重設勝部第二回合以上場次【對戰】資料");

            //勝部以外的場次【對戰】資料
            sql = @"
                UPDATE t1 SET
                    in_status = NULL
                    , in_sign_no = NULL
	                , in_sign_bypass = NULL
                    , in_sign_status = NULL
                    , in_sign_action = NULL
                    , in_target_no = NULL
	                , in_target_bypass = NULL
                    , in_target_status = NULL
                    , in_points = NULL
                    , in_points_type = NULL
                    , in_points_text = NULL
                    , in_correct_count = NULL
                FROM
                    IN_MEETING_PEVENT_DETAIL t1
                INNER JOIN
                    IN_MEETING_PEVENT t2
                    ON t2.id = t1.source_id
                WHERE
                    t2.source_id = '{#program_id}'
                    AND t2.in_tree_name NOT IN (N'main', N'sub')
            ";

            sql = sql.Replace("{#program_id}", program_id);
            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);
            itmSQL = cfg.inn.applySQL(sql);
            if (itmSQL.isError()) throw new Exception("發生錯誤: 重設勝部以外的場次【對戰】資料");

            //【場次】資料
            sql = @"
                UPDATE t1 SET
	                in_tree_no = NULL
	                , in_tree_sno = NULL
	                , in_bypass_foot = NULL
	                , in_bypass_status = NULL
	                , in_win_status = NULL
	                , in_win_sign_no = NULL
	                , in_win_time = NULL
	                , in_win_creator = NULL
	                , in_win_creator_sno = NULL
	                , in_wait_player = NULL
                    , in_win_local = NULL
                FROM
	                IN_MEETING_PEVENT t1
                WHERE
	                t1.source_id = '{#program_id}'
	                --AND t1.in_tree_name IN ('main', 'repechage', 'sub');
            ";

            sql = sql.Replace("{#program_id}", program_id);
            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);
            itmSQL = cfg.inn.applySQL(sql);
            if (itmSQL.isError()) throw new Exception("發生錯誤: 重設場次資料");

            // //【場次】資料(不可清 in_tree_no)
            // sql = @"
            //     UPDATE t1 SET
            //     in_bypass_foot = NULL
            //     , in_bypass_status = NULL
            //     , in_win_status = NULL
            //     , in_win_sign_no = NULL
            //     , in_win_time = NULL
            //     , in_win_creator = NULL
            //     , in_win_creator_sno = NULL
            //         , in_win_local = NULL
            //     FROM
            //     IN_MEETING_PEVENT t1
            //     WHERE
            //     t1.source_id = '{#program_id}'
            //     AND t1.in_tree_name NOT IN ('main', 'repechage', 'sub');
            // ";

            // sql = sql.Replace("{#program_id}", program_id);
            // //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);
            // itmSQL = cfg.inn.applySQL(sql);
            // if (itmSQL.isError()) throw new Exception("發生錯誤: 重設場次資料");
        }

        /// <summary>
        /// 取得場次對戰與隊伍資料
        /// </summary>
        private Dictionary<string, TEvent> GetMPEDTs(TConfig cfg, TProgram entProgram, string next_win_id = "")
        {
            string sql = "";

            string program_id = entProgram.Id;
            string next_condition = next_win_id == ""
                ? ""
                : "AND t2.in_next_win = '" + next_win_id + "'";

            sql = @"
                SELECT
	                t1.id       AS 'program_id'
	                , t1.in_l1
	                , t1.in_l2
	                , t1.in_l3
	                , t2.id     AS 'event_id'
                    , t2.in_tree_id
                    , t2.in_round
                    , t2.in_bypass_foot
                    , t2.in_bypass_status
	                , t3.id     AS 'detail_id'
	                , t3.in_sign_foot
	                , t3.in_sign_no
                FROM
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PEVENT t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t3 WITH(NOLOCK)
	                ON t3.source_id = t2.id
                WHERE
	                t1.id = '{#program_id}'
	                AND t2.in_tree_name = N'main'
                    {#next_condition}
                ORDER BY
	                t2.in_tree_sort
	                , t2.in_round_code DESC
	                , t2.in_tree_id
	                , t3.in_sign_foot
	                , t3.in_sign_no
            ";

            sql = sql.Replace("{#program_id}", program_id)
                .Replace("{#next_condition}", next_condition);
            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item items = cfg.inn.applySQL(sql);

            if (items.isError() || items.getItemCount() <= 0)
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "查詢資料異常 sql: " + sql);
                throw new Exception("場次對戰資料查詢發生錯誤");
            }

            Dictionary<string, TEvent> map = new Dictionary<string, TEvent>();

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string key = item.getProperty("event_id", "");
                string in_tree_id = item.getProperty("in_tree_id", "");
                string in_bypass_foot = item.getProperty("in_bypass_foot", "");
                string in_sign_foot = item.getProperty("in_sign_foot", "");
                string in_round = item.getProperty("in_round", "");

                TEvent evt = null;
                if (map.ContainsKey(key))
                {
                    evt = map[key];
                }
                else
                {
                    evt = new TEvent
                    {
                        Id = key,
                        TreeId = in_tree_id,
                        Round = in_round,
                        ByPassFoot = in_bypass_foot,
                        Foot1 = new TDetail(),
                        Foot2 = new TDetail(),
                    };
                    map.Add(key, evt);
                }

                if (in_sign_foot == "1")
                {
                    SetTEDetial(entProgram, evt.Foot1, item);
                }
                else
                {
                    SetTEDetial(entProgram, evt.Foot2, item);
                }
            }

            foreach (var kv in map)
            {
                var evt = kv.Value;
                FixTEDetial(evt.Foot1);
            }

            return map;
        }

        /// <summary>
        /// 修正場次輪空狀態
        /// </summary>
        private void FixEventBypass(TConfig cfg, TProgram entProgram, TEvent evt)
        {
            string sql = "";
            Item itmSQL = null;

            if (evt.Foot1.IsError || evt.Foot2.IsError)
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "場次對戰資料異常 event id: " + evt.Id);
                throw new Exception("場次對戰資料異常");
            }

            string in_bypass_foot = "";
            string in_bypass_status = "1"; //整數，0 = 不排, 1 = 無輪空, 2 = 單腳輪空(上場次), 8 兩腳皆輪空(上場次)

            if (evt.Round == "1")
            {
                if (evt.Foot1.IsSignBypass)
                {
                    in_bypass_foot = "1";
                    in_bypass_status = "0";
                }
                else if (evt.Foot2.IsSignBypass)
                {
                    in_bypass_foot = "2";
                    in_bypass_status = "0";
                }
                else
                {
                    in_bypass_status = "1";
                }

                string detail_id_1 = evt.Foot1.Id;
                string in_sign_bypass_1 = evt.Foot1.IsSignBypass ? "1" : "0";
                sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET in_sign_bypass = '" + in_sign_bypass_1 + "' WHERE id = '" + detail_id_1 + "'";
                itmSQL = cfg.inn.applySQL(sql);
                if (itmSQL.isError()) throw new Exception("error");

                string detail_id_2 = evt.Foot2.Id;
                string in_sign_bypass_2 = evt.Foot2.IsSignBypass ? "1" : "0";
                sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET in_sign_bypass = '" + in_sign_bypass_2 + "' WHERE id = '" + detail_id_2 + "'";
                itmSQL = cfg.inn.applySQL(sql);
                if (itmSQL.isError()) throw new Exception("error");

                sql = "UPDATE IN_MEETING_PEVENT SET in_bypass_foot = '" + in_bypass_foot + "', in_bypass_status = '" + in_bypass_status + "' WHERE id = '" + evt.Id + "'";
                itmSQL = cfg.inn.applySQL(sql);
                if (itmSQL.isError()) throw new Exception("error");
            }
            else if (evt.Round == "2")
            {
                // Dictionary<string, TEvent> sub = GetMPEDTs(cfg, entProgram, next_win_id: evt.Id);
                // if (sub == null || sub.Count != 2) throw new Exception("error");

                // var son1 = sub.First().Value;
                // var son2 = sub.Last().Value;

                // if (son1.ByPassFoot != "" && son2.ByPassFoot != "")
                // {
                //     in_bypass_status = "8";
                // }
                // else if (son1.ByPassFoot != "" || son2.ByPassFoot != "")
                // {
                //     in_bypass_status = "4";
                // }
                // else
                // {
                //     in_bypass_status = "1";
                // }

                in_bypass_status = "1";
                sql = "UPDATE IN_MEETING_PEVENT SET in_bypass_status = '" + in_bypass_status + "' WHERE id = '" + evt.Id + "'";
                itmSQL = cfg.inn.applySQL(sql);
                if (itmSQL.isError()) throw new Exception("error");
            }
            else
            {
                sql = "UPDATE IN_MEETING_PEVENT SET in_bypass_status = '1' WHERE id = '" + evt.Id + "'";
                itmSQL = cfg.inn.applySQL(sql);
                if (itmSQL.isError()) throw new Exception("error");
            }
        }

        /// <summary>
        /// 更新場次序號(時序)
        /// </summary>
        private void UpdateEventTreeNo(TConfig cfg, TProgram entProgram)
        {
            string program_id = entProgram.Id;

            string sql = @"
                UPDATE t1 SET
                    t1.in_tree_no = t2.rno
                    , t1.in_tree_sno = t2.rno
                FROM 
                    IN_MEETING_PEVENT t1
                INNER JOIN
                (
                    SELECT 
                        ROW_NUMBER() OVER (ORDER BY in_round, in_bypass_status DESC, in_round_id) AS rno
                        , id
                        , in_round
                        , in_bypass_status
                        , in_round_id
                    FROM
                        IN_MEETING_PEVENT WITH(NOLOCK)
                    WHERE
                        source_id = '{#program_id}'
                        AND ISNULL(in_bypass_foot, '') = ''
                        AND in_tree_name = N'main'
                ) t2
                    ON t2.id = t1.id
                WHERE
                    t1.source_id = '{#program_id}'
                    AND t1.in_tree_name = N'main'
            ";

            sql = sql.Replace("{#program_id}", program_id);

            cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// 更新場次序號(時序)
        /// </summary>
        private void UpdateRpcEventTreeNo(TConfig cfg, TProgram entProgram)
        {
            string program_id = entProgram.Id;

            string sql = @"
                UPDATE t1 SET
                    t1.in_tree_no = t2.rno
                    , t1.in_tree_sno = t2.rno
                FROM 
                    IN_MEETING_PEVENT t1
                INNER JOIN
                (
                    SELECT 
                        ROW_NUMBER() OVER (ORDER BY in_round, in_round_id) AS rno
                        , id
                        , in_round
                        , in_round_id
                    FROM
                        IN_MEETING_PEVENT WITH(NOLOCK)
                    WHERE
                        source_id = '{#program_id}'
                        AND in_tree_name = N'repechage'
                ) t2
                    ON t2.id = t1.id
                WHERE
                    t1.source_id = '{#program_id}'
                    AND t1.in_tree_name = N'repechage'
            ";

            sql = sql.Replace("{#program_id}", program_id);

            cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// 修正場次序號(時序)
        /// </summary>
        private void FixRpcEventTreeNo(TConfig cfg, TProgram entProgram)
        {
            var rpc = GetRpc(cfg, entProgram);
            int w_cnt = rpc.C1.Players.Count + rpc.C2.Players.Count;
            int e_cnt = rpc.C3.Players.Count + rpc.C4.Players.Count;

            switch (rpc.real_player_count)
            {
                case 1:
                case 2:
                case 3:
                case 4:
                case 8:
                case 16:
                case 32:
                case 64:
                case 128:
                    break;

                case 5:
                    CancelRpcEvent(cfg, rpc, "R101");
                    CancelRpcEvent(cfg, rpc, "R102");
                    if (w_cnt < 3) ClearRpcTreeNo(cfg, rpc, "R201");
                    if (e_cnt < 3) ClearRpcTreeNo(cfg, rpc, "R202");
                    break;

                case 6:
                    ClearRpcTreeNo(cfg, rpc, "R101");
                    ClearRpcTreeNo(cfg, rpc, "R102");
                    break;

                case 7:
                    if (w_cnt < 4) ClearRpcTreeNo(cfg, rpc, "R101");
                    if (e_cnt < 4) ClearRpcTreeNo(cfg, rpc, "R102");
                    break;

                default:
                    int code = rpc.col_code / 2;
                    if (rpc.C1.Players.Count <= code) ClearRpcTreeNo(cfg, rpc, "R101");
                    if (rpc.C2.Players.Count <= code) ClearRpcTreeNo(cfg, rpc, "R102");
                    if (rpc.C3.Players.Count <= code) ClearRpcTreeNo(cfg, rpc, "R103");
                    if (rpc.C4.Players.Count <= code) ClearRpcTreeNo(cfg, rpc, "R104");
                    break;
            }
        }

        /// <summary>
        /// 清除場次序號
        /// </summary>
        private void ClearRpcTreeNo(TConfig cfg, TRpc rpc, string in_tree_id)
        {
            string sql = "UPDATE IN_MEETING_PEVENT SET in_tree_no = NULL, in_tree_sno = NULL WHERE source_id = '" + rpc.program_id + "' AND in_tree_id = '" + in_tree_id + "'";
            cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// 取消場次
        /// </summary>
        private void CancelRpcEvent(TConfig cfg, TRpc rpc, string in_tree_id)
        {
            string sql = "";

            sql = "UPDATE IN_MEETING_PEVENT SET in_tree_no = NULL, in_tree_sno = NULL, in_win_status = N'cancel' WHERE source_id = '" + rpc.program_id + "' AND in_tree_id = '" + in_tree_id + "'";
            cfg.inn.applySQL(sql);

            sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET in_sign_status = N'cancel' WHERE source_id IN "
                + "("
                + "     SELECT id FROM IN_MEETING_PEVENT WHERE source_id = '" + rpc.program_id + "' AND in_tree_id = '" + in_tree_id + "'"
                + ")";
            cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// 取得敗部資料
        /// </summary>
        private TRpc GetRpc(TConfig cfg, TProgram entProgram)
        {
            string program_id = entProgram.Id;
            int team_count = entProgram.TeamCount;
            int round_code = entProgram.RoundCode;
            int round_count = entProgram.RoundCount;

            int col_code = round_code / 4;
            TRpc rpc = new TRpc
            {
                program_id = program_id,
                tree_name = "repechage",
                real_player_count = team_count,
                plan_player_count = round_code,
                plan_round_count = round_count,
                col_code = col_code,
                C1 = new TColumn { Suffix = "1", Players = new List<InnSport.Core.Models.Output.TNoStatus> { } },
                C2 = new TColumn { Suffix = "2", Players = new List<InnSport.Core.Models.Output.TNoStatus> { } },
                C3 = new TColumn { Suffix = "3", Players = new List<InnSport.Core.Models.Output.TNoStatus> { } },
                C4 = new TColumn { Suffix = "4", Players = new List<InnSport.Core.Models.Output.TNoStatus> { } },
            };

            var list = entProgram.NoList;
            for (int i = 0; i < list.Count; i++)
            {
                var item = list[i];

                if (item.IsByPass)
                {
                    continue;
                }

                int id = i / col_code;

                switch (id)
                {
                    case 0:
                        rpc.C1.Players.Add(item);
                        break;
                    case 1:
                        rpc.C2.Players.Add(item);
                        break;
                    case 2:
                        rpc.C3.Players.Add(item);
                        break;
                    case 3:
                        rpc.C4.Players.Add(item);
                        break;
                }
            }

            return rpc;
        }

        ///// <summary>
        ///// 取得敗部資料
        ///// </summary>
        //private TRpc GetRpcOLD(TConfig cfg, Item itmProgram)
        //{
        //    string program_id = itmProgram.getProperty("id", "");
        //    string in_team_count = itmProgram.getProperty("in_team_count", "0");
        //    string in_round_code = itmProgram.getProperty("in_round_code", "0");
        //    string in_round_count = itmProgram.getProperty("in_round_count", "0");

        //    int team_count = GetInt(in_team_count);
        //    int round_code = GetInt(in_round_code);
        //    int round_count = GetInt(in_round_count);

        //    int col_code = round_code / 4;
        //    TRpc rpc = new TRpc
        //    {
        //        program_id = program_id,
        //        tree_name = "repechage",
        //        real_player_count = team_count,
        //        plan_player_count = round_code,
        //        plan_round_count = round_count,
        //        col_code = col_code,
        //        C1 = new TColumn { Suffix = "1", Players = new List<Item> { } },
        //        C2 = new TColumn { Suffix = "2", Players = new List<Item> { } },
        //        C3 = new TColumn { Suffix = "3", Players = new List<Item> { } },
        //        C4 = new TColumn { Suffix = "4", Players = new List<Item> { } },
        //    };

        //    string sql = @"
        //        SELECT
        //         t1.in_round
        //            , t1.in_tree_id
        //         , t2.in_sign_foot
        //         , t2.in_sign_no
        //         , t3.id AS 'team_id'
        //         , t3.in_section_no
        //        FROM
        //         IN_MEETING_PEVENT t1
        //        INNER JOIN
        //         IN_MEETING_PEVENT_DETAIL t2
        //         ON t2.source_id = t1.id
        //        LEFT OUTER JOIN
        //         VU_MEETING_PTEAM t3
        //         ON t3.source_id = t1.source_id
        //         AND t3.in_sign_no = t2.in_sign_no
        //        WHERE
        //         t1.source_id = '{#program_id}'
        //         AND t1.in_tree_name = 'main'
        //         AND t1.in_round = 1
        //        ORDER BY
        //         t1.in_tree_id
        //         , t2.in_sign_foot
        //    ";

        //    sql = sql.Replace("{#program_id}", program_id);

        //    Item items = cfg.inn.applySQL(sql);

        //    int count = items.getItemCount();

        //    for (int i = 0; i < count; i++)
        //    {
        //        Item item = items.getItemByIndex(i);
        //        string team_id = item.getProperty("team_id", "");
        //        if (team_id == "")
        //        {
        //            continue;
        //        }

        //        int id = i / col_code;

        //        switch (id)
        //        {
        //            case 0:
        //                rpc.C1.Players.Add(item);
        //                break;
        //            case 1:
        //                rpc.C2.Players.Add(item);
        //                break;
        //            case 2:
        //                rpc.C3.Players.Add(item);
        //                break;
        //            case 3:
        //                rpc.C4.Players.Add(item);
        //                break;
        //        }
        //    }

        //    return rpc;
        //}

        /// <summary>
        /// 修補隊伍的量級序號
        /// </summary>
        private void FixTeamSectionNo(TConfig cfg, Item itmProgram)
        {
            Item item = cfg.inn.newItem();
            item.setType("In_Meeting_Program");
            item.setProperty("meeting_id", itmProgram.getProperty("in_meeting", ""));
            item.setProperty("program_id", itmProgram.getProperty("id", ""));
            item.apply("in_meeting_program_team_fix2");
        }

        /// <summary>
        /// 重新註記等待選手場次
        /// </summary>
        private void RemarkWaitPlayers(TConfig cfg, Item itmProgram)
        {
            string sql = @"
                UPDATE t1 SET 
                	t1.in_wait_player = 1
                FROM
                	IN_MEETING_PEVENT t1
                INNER JOIN
                	IN_MEETING_PEVENT_DETAIL t2
                	ON t2.source_id = t1.id
                WHERE
                	t1.source_id = '{#program_id}'
                	AND t1.in_tree_no > 0
                	AND t1.in_tree_name NOT IN ('rank56', 'rank78')
                	AND t2.in_sign_no IS NULL
            ";

            sql = sql.Replace("{#program_id}", itmProgram.getProperty("id", ""));

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item itmSQL = cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// 自動晉級
        /// </summary>
        private void AutoUpgrade(TConfig cfg, Item itmProgram)
        {
            Item item = cfg.inn.newItem();
            item.setType("In_Meeting_Program");
            item.setProperty("meeting_id", itmProgram.getProperty("in_meeting", ""));
            item.setProperty("program_id", itmProgram.getProperty("id", ""));
            item.setProperty("mode", "upgrade");
            item.apply("in_meeting_program_score");
        }

        //修正日期
        private void FixDate(TConfig cfg, Item itmProgram)
        {
            string sql = "";
            Item itmSQL = null;

            string program_id = itmProgram.getProperty("id", "");

            sql = @"
                 UPDATE t1 SET
             	    t1.in_date_key = t3.in_date_key
             	    , t1.in_site = t4.id
             	    , t1.in_site_code = t4.in_code
             	    , t1.in_tree_state = 0
                 FROM
             	    IN_MEETING_PEVENT t1
                 INNER JOIN
                    IN_MEETING_PROGRAM t2
                    ON t2.id = t1.source_id
                 INNER JOIN
             	    IN_MEETING_ALLOCATION t3
             	    ON t3.in_program = t2.id
                 INNER JOIN
             	    IN_MEETING_SITE t4
             	    ON t4.id = t3.in_site
                 WHERE
                    t2.id = '{#program_id}'
            ";

            sql = sql.Replace("{#program_id}", program_id);

            itmSQL = cfg.inn.applySQL(sql);
        }

        private void FixTEDetial(TDetail foot)
        {
            if (foot.Value == null)
            {
                foot.Id = "";
                foot.IsSignBypass = true;
                foot.IsError = true;
            }
        }

        private void SetTEDetial(TProgram entProgram, TDetail foot, Item item)
        {
            //跆拳道籤號
            string in_sign_no = item.getProperty("in_sign_no", "0");
            int sign_no = GetInt(in_sign_no);
            bool is_bypass = entProgram.ByPassNoList.Contains(sign_no);

            foot.Id = item.getProperty("detail_id", "");
            foot.SignFoot = item.getProperty("in_sign_foot", "");
            foot.SignNo = in_sign_no;
            foot.Value = item;

            if (is_bypass)
            {
                foot.IsSignBypass = true;
            }

            if (foot.SignNo == "")
            {
                foot.UnSigned = true;
            }

            foot.IsError = false;
        }

        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Aras.Server.Core.IContextState RequestState { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public Innovator inn { get; set; }

            public string meeting_id { get; set; }
            public string program_id { get; set; }
            public string in_date { get; set; }
            public string scene { get; set; }
            public string is_run { get; set; }
            public string is_build { get; set; }

            public InnSport.Core.Services.Impl.JudoSportService Service { get; set; }
        }

        private class TProgram
        {
            public string Id { get; set; }
            public string BattleType { get; set; }
            public int TeamCount { get; set; }
            public int RoundCode { get; set; }
            public int RoundCount { get; set; }
            public bool IsRoundRobin { get; set; }
            public Item Value { get; set; }

            public List<InnSport.Core.Models.Output.TNoStatus> NoList { get; set; }
            public List<int> ByPassNoList { get; set; }
        }

        private class TEvent
        {
            public string Id { get; set; }
            public string TreeId { get; set; }
            public string Round { get; set; }
            public string ByPassFoot { get; set; }
            public Item Value { get; set; }
            public TDetail Foot1 { get; set; }
            public TDetail Foot2 { get; set; }
        }

        private class TDetail
        {
            public string Id { get; set; }
            public string SignFoot { get; set; }
            public string SignNo { get; set; }
            public Item Value { get; set; }
            public bool IsError { get; set; }
            public bool IsSignBypass { get; set; }
            public bool UnSigned { get; set; }
        }

        private class TRpc
        {
            public string program_id { get; set; }
            public string tree_name { get; set; }
            public int real_player_count { get; set; }
            public int plan_player_count { get; set; }
            public int plan_round_count { get; set; }
            public int col_code { get; set; }
            public TColumn C1 { get; set; }
            public TColumn C2 { get; set; }
            public TColumn C3 { get; set; }
            public TColumn C4 { get; set; }
        }

        private class TColumn
        {
            public string Suffix { get; set; }
            //public List<Item> Players { get; set; }
            public List<InnSport.Core.Models.Output.TNoStatus> Players { get; set; }
        }

        private int GetInt(string value, int def = 0)
        {
            int result = def;
            Int32.TryParse(value, out result);
            return result;
        }
    }
}