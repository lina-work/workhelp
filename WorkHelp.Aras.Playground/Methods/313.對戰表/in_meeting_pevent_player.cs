﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;
using System.Net;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class in_meeting_pevent_player : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 藍白方對調
                日期: 
                    - 2021-11-29 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]in_meeting_pevent_player";

            Item itmR = this;

            Item itmPermit = inn.applyMethod("In_CheckIdentity", "<method>" + strMethodName + "</method><code>" + "ALL" + "</code>");
            bool isMeetingAdmin = itmPermit.getProperty("isMeetingAdmin", "") == "1";
            bool isMeetingLeader = itmPermit.getProperty("isMeetingLeader", "") == "1";
            bool can_view = isMeetingAdmin || isMeetingLeader;

            if (!can_view)
            {
                throw new Exception("您無權限瀏覽此頁面，請重新登入");
            }

            string sql = "";
            
            string meeting_id = itmR.getProperty("meeting_id", "");
            string program_id = itmR.getProperty("program_id", "");
            string event_id = itmR.getProperty("event_id", "");
            string scene = itmR.getProperty("scene", "");

            sql = "SELECT in_win_time FROM IN_MEETING_PEVENT WITH(NOLOCK) WHERE id = '" + event_id + "'";
            Item itmEvent = inn.applySQL(sql);
            if (itmEvent.isError() || itmEvent.getResult() == "" )
            {
                throw new Exception("場次資料錯誤");
            }
            string in_win_time = itmEvent.getProperty("in_win_time", "");
            if (itmEvent.isError() || itmEvent.getResult() == "")
            {
                throw new Exception("已決出勝負，不可異動藍白方");
            }

            sql = "SELECT id, in_sign_foot, in_sign_no FROM IN_MEETING_PEVENT_DETAIL WITH(NOLOCK) WHERE source_id = '" + event_id + "' ORDER BY in_sign_foot";
            Item itmDetails = inn.applySQL(sql);
            if (itmDetails.isError() || itmDetails.getResult() == "" || itmDetails.getItemCount() != 2)
            {
                throw new Exception("場次明細錯誤");
            }

            string f1_id = "";
            string f1_sign_no = "";

            string f2_id = "";
            string f2_sign_no = "";

            int count = itmDetails.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item itmDetail = itmDetails.getItemByIndex(i);
                string id = itmDetail.getProperty("id", "");
                string in_sign_foot = itmDetail.getProperty("in_sign_foot", "");
                string in_sign_no = itmDetail.getProperty("in_sign_no", "");

                if (in_sign_foot == "1")
                {
                    f1_id = id;
                    f1_sign_no = in_sign_no;
                }
                else
                {
                    f2_id = id;
                    f2_sign_no = in_sign_no;
                }
            }

            string sql_upd1 = "UPDATE IN_MEETING_PEVENT_DETAIL SET in_sign_no = '" + f2_sign_no + "', in_target_no = '" + f1_sign_no+ "' WHERE id = '" + f1_id + "'";
            string sql_upd2 = "UPDATE IN_MEETING_PEVENT_DETAIL SET in_sign_no = '" + f1_sign_no + "', in_target_no = '" + f2_sign_no + "' WHERE id = '" + f2_id + "'";

            CCO.Utilities.WriteDebug(strMethodName, "sq1: " + sql_upd1);
            CCO.Utilities.WriteDebug(strMethodName, "sq1: " + sql_upd2);

            Item itmUpd1 = inn.applySQL(sql_upd1);
            Item itmUpd2 = inn.applySQL(sql_upd2);

            if (itmUpd1.isError() || itmUpd2.isError())
            {
                throw new Exception("藍白方對調發生錯誤");
            }

            return itmR;
        }
    }
}