﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;
using System.Net;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class in_meeting_day_options : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 
                    取得日期場地選單
                輸入: 
                    meeting_id
                日期: 
                    - 2020-12-22: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]in_meeting_day_options";

            string meeting_id = this.getProperty("meeting_id", "").Trim();
            string scene = this.getProperty("scene", "").Trim();

            if (meeting_id == "")
            {
                throw new Exception("賽事 id 不得為空白");
            }

            if (scene == "clear")
            {
                return ClearCache(CCO, strMethodName, inn, meeting_id);
            }
            else
            {
                return JsonData(CCO, strMethodName, inn, meeting_id);
            }
        }

        //清除暫存資訊(三階選單)
        private Item ClearCache(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string meeting_id)
        {
            var _caching = System.Runtime.Caching.MemoryCache.Default;

            var key = meeting_id + "_DAY_OPTIONS_KEY";

            _caching.Remove(key);

            return inn.newItem();
        }

        private Item JsonData(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string meeting_id)
        {
            string aml = "";
            string sql = "";

            var _caching = System.Runtime.Caching.MemoryCache.Default;
            var _policy = new System.Runtime.Caching.CacheItemPolicy
            {
                SlidingExpiration = TimeSpan.FromMinutes(10)
            };

            var key = meeting_id + "_DAY_OPTIONS_KEY";

            string str = _caching.Contains(key) ? _caching.Get(key) as string : "";

            if (string.IsNullOrEmpty(str))
            {
                sql = @"
                    SELECT 
                        t1.id
                        , t1.in_fight_day
                        , t1.in_name2
                        , t1.in_team_count
                        , t1.in_event_count
                        , t1.in_site_mat
                        , t2.in_code AS 'site_code'
                        , t2.in_name AS 'site_name'
                    FROM 
                        IN_MEETING_PROGRAM t1 WITH(NOLOCK) 
                    INNER JOIN
                        IN_MEETING_SITE t2 WITH(NOLOCK)
                        ON t2.id = t1.in_site
                    WHERE
                        t1.in_meeting = '{#meeting_id}'
                        AND ISNULL(t1.in_program, '') = ''
                    ORDER BY 
                        t1.in_site_mat2
                ";

                sql = sql.Replace("{#meeting_id}", meeting_id);

                Item items = inn.applySQL(sql);

                int count = items.getItemCount();

                List<TNode> nodes = new List<TNode>();

                bool need_id = true;

                for (int i = 0; i < count; i++)
                {
                    Item item = items.getItemByIndex(i);
                    TNode l1 = AddAndGetNode(nodes, item, "in_fight_day", need_id);
                    TNode l2 = AddAndGetNode(l1.Nodes, item, "site_name", need_id);
                    TNode l3 = AddAndGetNode(l2.Nodes, item, "in_site_mat", need_id);

                    string in_team_count = item.getProperty("in_team_count", "0");
                    int team_count = GetIntVal(in_team_count);

                    string in_event_count = item.getProperty("in_event_count", "0");
                    int event_count = GetIntVal(in_event_count);
                }

                str = Newtonsoft.Json.JsonConvert.SerializeObject(nodes);

                _caching.Set(key, str, _policy);
            }

            Item itmResult = inn.newItem();
            itmResult.setProperty("json", str);
            return itmResult;
        }

        private TNode AddAndGetNode(List<TNode> nodes, Item item, string lv, bool need_id)
        {
            string value = item.getProperty(lv, "");

            TNode search = nodes.Find(x => x.Val == value);

            if (search == null)
            {
                search = new TNode
                {
                    Lv = lv,
                    Val = value,
                    Lbl = value,
                    Cnt = 0,
                    Nodes = new List<TNode>()
                };

                if (need_id)
                {
                    search.Id = item.getProperty("id", "");
                }

                nodes.Add(search);
            }

            return search;
        }

        private int GetIntVal(string value, int def = 0)
        {
            int result = def;
            Int32.TryParse(value, out result);
            return result;
        }

        private class TNode
        {
            public string Id { get; set; }

            public string Lv { get; set; }

            public string Val { get; set; }

            public string Lbl { get; set; }

            public int Cnt { get; set; }

            public int ECnt { get; set; }

            public List<TNode> Nodes { get; set; }
        }
    }
}