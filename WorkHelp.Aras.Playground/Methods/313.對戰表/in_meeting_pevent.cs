﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;
using System.Net;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class in_meeting_pevent : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 
                    場次管理
                日期: 
                    2021-10-08: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]in_meeting_pevent";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                InnH = _InnH,

                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),

                scene = itmR.getProperty("scene", ""),
                meeting_id = itmR.getProperty("meeting_id", ""),
                program_id = itmR.getProperty("program_id", ""),
                event_id = itmR.getProperty("event_id", ""),
            };


            switch (cfg.scene)
            {
                case "list"://清單
                    Page(cfg, itmR);
                    break;

                case "clone_modal"://複製
                    Modal(cfg, itmR);
                    itmR.setProperty("from_event_id", cfg.event_id);
                    itmR.setProperty("event_id", "");
                    itmR.setProperty("btn_title", "新增");
                    itmR.setProperty("btn_action", "new");
                    break;

                case "edit_modal"://修改
                    Modal(cfg, itmR);
                    itmR.setProperty("btn_title", "儲存");
                    itmR.setProperty("btn_action", "edit");
                    break;

                case "new"://新增
                    NewPEvent(cfg, itmR);
                    break;

                case "edit"://編輯
                    EditPEvent(cfg, itmR);
                    break;

                case "remove"://刪除
                    RemovePEvent(cfg, itmR);
                    break;

                case "modal"://跳窗
                    Modal(cfg, itmR);
                    break;

                case "king"://建立盟主戰
                    NewKingPEvent(cfg, itmR);
                    break;

                default: //頁面
                    Page(cfg, itmR);
                    break;
            }

            return itmR;
        }

        //頁面
        private void Page(TConfig cfg, Item itmReturn)
        {
            Item itmMeeting = cfg.inn.applySQL("SELECT in_title FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            itmReturn.setProperty("in_title", itmMeeting.getProperty("in_title", " "));

            //三階選單
            Item itmJson = cfg.inn.newItem("In_Meeting");
            itmJson.setProperty("meeting_id", cfg.meeting_id);
            itmJson.setProperty("need_id", "1");
            itmJson = itmJson.apply("in_meeting_program_options");
            itmReturn.setProperty("in_level_json", itmJson.getProperty("json", ""));
            itmReturn.setProperty("in_level_count", itmJson.getProperty("total", ""));

            if (cfg.program_id == "")
            {
                return;
            }

            string sql = "SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + cfg.program_id + "'";
            Item itmProgram = cfg.inn.applySQL(sql);
            itmReturn.setProperty("in_l1", itmProgram.getProperty("in_l1", ""));
            itmReturn.setProperty("in_l2", itmProgram.getProperty("in_l2", ""));
            itmReturn.setProperty("in_l3", itmProgram.getProperty("in_l3", ""));

            List<TEvent> evts = GetEventMap(cfg);

            for (int i = 0; i < evts.Count; i++)
            {
                Item itmEvent = evts[i].Value;
                itmEvent.setType("inn_event");
                itmEvent.setProperty("no", (i + 1).ToString());
                itmReturn.addRelationship(itmEvent);
            }
        }

        private List<TEvent> GetEventMap(TConfig cfg)
        {
            string condition = cfg.event_id == ""
                ? "t1.source_id = '" + cfg.program_id + "'"
                : "t1.id = '" + cfg.event_id + "'";

            string sql = @"
                SELECT 
                    t1.*
                    , t2.in_sign_foot
                    , t2.in_sign_no         AS 'detail_sign_no'
                    , t11.map_short_org     AS 'player_org'
                    , t11.in_name           AS 'player_name'
                    , t21.in_name           AS 'site_name'
                FROM 
                    IN_MEETING_PEVENT t1 WITH(NOLOCK)
                LEFT OUTER JOIN
                    IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK)
                    ON t2.source_id = t1.id
                LEFT OUTER JOIN
                    VU_MEETING_PTEAM t11 WITH(NOLOCK)
                    ON t11.source_id = t1.source_id
                    AND t11.in_sign_no = t2.in_sign_no
                LEFT OUTER JOIN
                    IN_MEETING_SITE t21 WITH(NOLOCK)
                    ON t21.id = t1.in_site
                WHERE 
                    {#condition}
                ORDER BY
                    t1.in_tree_sort
                    , t1.in_tree_id
            ";

            sql = sql.Replace("{#condition}", condition);
            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item itmEventDetails = cfg.inn.applySQL(sql);

            List<TEvent> evts = MapEvent(cfg, itmEventDetails);

            return evts;
        }

        private List<TEvent> MapEvent(TConfig cfg, Item items)
        {
            List<TEvent> list = new List<TEvent>();
            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string id = item.getProperty("id", "");
                string in_sign_foot = item.getProperty("in_sign_foot", "");

                TEvent evt = list.Find(x => x.Id == id);
                if (evt == null)
                {
                    evt = new TEvent
                    {
                        Id = id,
                        Value = item,
                    };

                    list.Add(evt);
                }

                if (in_sign_foot == "1")
                {
                    evt.Foot1 = item;
                }
                else if (in_sign_foot == "2")
                {
                    evt.Foot2 = item;
                }
            }

            foreach (var evt in list)
            {
                if (evt.Foot1 == null) evt.Foot1 = cfg.inn.newItem();
                if (evt.Foot2 == null) evt.Foot2 = cfg.inn.newItem();

                // evt.Value.setProperty("WhiteD", evt.Foot1.getProperty("player_org", ""));
                // evt.Value.setProperty("WhiteN", evt.Foot1.getProperty("player_name", ""));
                // evt.Value.setProperty("white_sign_no", evt.Foot1.getProperty("in_sign_no", ""));

                // evt.Value.setProperty("BlueD", evt.Foot2.getProperty("player_org", ""));
                // evt.Value.setProperty("BlueN", evt.Foot2.getProperty("player_name", ""));
                // evt.Value.setProperty("blue_sign_no", evt.Foot2.getProperty("in_sign_no", ""));
            }

            return list;
        }

        //跳窗(新增)
        private void Modal(TConfig cfg, Item itmReturn)
        {
            string sql = "SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + cfg.program_id + "'";
            Item itmProgram = cfg.inn.applySQL(sql);
            itmReturn.setProperty("in_l1", itmProgram.getProperty("in_l1", ""));
            itmReturn.setProperty("in_l2", itmProgram.getProperty("in_l2", ""));
            itmReturn.setProperty("in_l3", itmProgram.getProperty("in_l3", ""));

            List<TEvent> evts = GetEventMap(cfg);
            if (evts == null || evts.Count <= 0) return;

            TEvent evt = evts[0];
            Item itmEvent = evt.Value;
            itmReturn.setProperty("site_name", itmEvent.getProperty("site_name", ""));
            itmReturn.setProperty("in_date_key", itmEvent.getProperty("in_date_key", ""));

            itmReturn.setProperty("in_site_code", itmEvent.getProperty("in_site_code", ""));

            itmReturn.setProperty("in_tree_name", itmEvent.getProperty("in_tree_name", ""));
            itmReturn.setProperty("in_tree_sort", itmEvent.getProperty("in_tree_sort", ""));
            itmReturn.setProperty("in_tree_id", itmEvent.getProperty("in_tree_id", ""));
            itmReturn.setProperty("in_tree_no", itmEvent.getProperty("in_tree_no", ""));
            itmReturn.setProperty("in_tree_alias", itmEvent.getProperty("in_tree_alias", ""));

            itmReturn.setProperty("in_round", itmEvent.getProperty("in_round", ""));
            itmReturn.setProperty("in_round_code", itmEvent.getProperty("in_round_code", ""));
            itmReturn.setProperty("in_round_id", itmEvent.getProperty("in_round_id", ""));

            itmReturn.setProperty("white_sign_no", evt.Foot1.getProperty("detail_sign_no", ""));
            itmReturn.setProperty("blue_sign_no", evt.Foot2.getProperty("detail_sign_no", ""));
        }

        //新增
        private void NewPEvent(TConfig cfg, Item itmReturn)
        {
            Item itmNewEvent = ApplyNewPEvent(cfg, itmReturn);
            AddDetail(cfg, itmNewEvent, "1", itmReturn);
            AddDetail(cfg, itmNewEvent, "2", itmReturn);
        }

        //新增
        private Item ApplyNewPEvent(TConfig cfg, Item itmReturn)
        {
            Item item = cfg.inn.newItem("In_Meeting_PEvent", "add");
            item.setProperty("in_meeting", cfg.meeting_id);
            item.setProperty("source_id", cfg.program_id);

            item.setProperty("in_tree_id", itmReturn.getProperty("in_tree_id", ""));
            item.setProperty("in_tree_no", itmReturn.getProperty("in_tree_no", ""));
            item.setProperty("in_tree_alias", itmReturn.getProperty("in_tree_alias", ""));
            item.setProperty("in_tree_sno", itmReturn.getProperty("in_tree_no", ""));
            item.setProperty("in_tree_state", "0");
            item.setProperty("in_cancel", "0");
            item.setProperty("in_wait_player", "0");

            item.setProperty("in_round", itmReturn.getProperty("in_round", ""));
            item.setProperty("in_round_id", itmReturn.getProperty("in_round_id", ""));
            item.setProperty("in_round_code", itmReturn.getProperty("in_round_code", ""));

            item.setProperty("in_sign_code", "");
            item.setProperty("in_sign_no", "");

            item.setProperty("in_surface_id", "0");
            item.setProperty("in_surface_name", "");

            string from_event_id = itmReturn.getProperty("from_event_id", "");
            string white_sign_no = itmReturn.getProperty("white_sign_no", "");
            string blue_sign_no = itmReturn.getProperty("blue_sign_no", "");

            if (from_event_id != "")
            {
                Item itmFromEvent = cfg.inn.getItemById("In_Meeting_PEvent", from_event_id);
                if (itmFromEvent.isError())
                {
                    throw new Exception("取得場次資料來源發生錯誤");
                }
                item.setProperty("in_tree_name", itmFromEvent.getProperty("in_tree_name", "")); ;
                item.setProperty("in_tree_sort", itmFromEvent.getProperty("in_tree_sort", "")); ;
                item.setProperty("in_date_key", itmFromEvent.getProperty("in_date_key", "")); ;
                item.setProperty("in_site", itmFromEvent.getProperty("in_site", "")); ;
                item.setProperty("in_detail_ns", white_sign_no + "," + blue_sign_no); ;
            }

            item = item.apply();

            if (item.isError())
            {
                throw new Exception("建立場次發生失敗");
            }

            return item;
        }

        #region 盟主戰
        //新增
        private void NewKingPEvent(TConfig cfg, Item itmReturn)
        {
            Item itmProgram = cfg.inn.applySQL("SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + cfg.program_id + "'");
            string in_fight_day = itmProgram.getProperty("in_fight_day", "");
            string in_site = itmProgram.getProperty("in_site", "");

            Item itmNewData1 = cfg.inn.newItem();
            itmNewData1.setProperty("in_tree_id", "KA01");
            itmNewData1.setProperty("in_tree_no", "7001");
            itmNewData1.setProperty("in_tree_alias", "盟主戰");
            itmNewData1.setProperty("in_round", "1");
            itmNewData1.setProperty("in_round_id", "1");
            itmNewData1.setProperty("in_round_code", "2");
            itmNewData1.setProperty("in_tree_name", "ka01");
            itmNewData1.setProperty("in_tree_sort", "500");
            itmNewData1.setProperty("in_date_key", in_fight_day);
            itmNewData1.setProperty("in_site", in_site);
            
            Item itmNewEvent1 = ApplyNewKingPEvent(cfg, itmNewData1);
            AddKingDetail(cfg, itmNewEvent1, "1");
            AddKingDetail(cfg, itmNewEvent1, "2");

            Item itmNewData2 = cfg.inn.newItem();
            itmNewData2.setProperty("in_tree_id", "KB01");
            itmNewData2.setProperty("in_tree_no", "7002");
            itmNewData2.setProperty("in_tree_alias", "盟主戰加賽");
            itmNewData2.setProperty("in_round", "1");
            itmNewData2.setProperty("in_round_id", "1");
            itmNewData2.setProperty("in_round_code", "2");
            itmNewData2.setProperty("in_tree_name", "kb01");
            itmNewData2.setProperty("in_tree_sort", "600");
            itmNewData2.setProperty("in_date_key", in_fight_day);
            itmNewData2.setProperty("in_site", in_site);

            Item itmNewEvent2 = ApplyNewKingPEvent(cfg, itmNewData2);
            AddKingDetail(cfg, itmNewEvent2, "1");
            AddKingDetail(cfg, itmNewEvent2, "2");
        }

        //新增
        private Item ApplyNewKingPEvent(TConfig cfg, Item itmNewData)
        {
            string in_tree_id = itmNewData.getProperty("in_tree_id", "");

            Item item = cfg.inn.newItem("In_Meeting_PEvent", "merge");
            item.setAttribute("where", "source_id = '" + cfg.program_id + "' AND in_tree_id = '" + in_tree_id + "'");

            item.setProperty("in_meeting", cfg.meeting_id);
            item.setProperty("source_id", cfg.program_id);

            item.setProperty("in_tree_id", in_tree_id);
            item.setProperty("in_tree_no", itmNewData.getProperty("in_tree_no", ""));
            item.setProperty("in_tree_alias", itmNewData.getProperty("in_tree_alias", ""));
            item.setProperty("in_tree_sno", itmNewData.getProperty("in_tree_no", ""));
            item.setProperty("in_tree_state", "0");
            item.setProperty("in_cancel", "0");
            item.setProperty("in_wait_player", "0");

            item.setProperty("in_round", itmNewData.getProperty("in_round", ""));
            item.setProperty("in_round_id", itmNewData.getProperty("in_round_id", ""));
            item.setProperty("in_round_code", itmNewData.getProperty("in_round_code", ""));

            item.setProperty("in_sign_code", "");
            item.setProperty("in_sign_no", "");

            item.setProperty("in_surface_id", "0");
            item.setProperty("in_surface_name", "");

            item.setProperty("in_tree_name", itmNewData.getProperty("in_tree_name", "")); ;
            item.setProperty("in_tree_sort", itmNewData.getProperty("in_tree_sort", "")); ;
            item.setProperty("in_date_key", itmNewData.getProperty("in_date_key", "")); ;
            item.setProperty("in_site", itmNewData.getProperty("in_site", "")); ;
            item.setProperty("in_detail_ns", ","); ;

            item = item.apply();

            if (item.isError())
            {
                throw new Exception("建立場次發生失敗");
            }

            return item;
        }

        private void AddKingDetail(TConfig cfg, Item itmNewEvent, string in_sign_foot)
        {
            string event_id = itmNewEvent.getID();

            Item itmDetail = cfg.inn.newItem("In_Meeting_PEvent_Detail", "merge");
            itmDetail.setAttribute("where", "source_id = '" + event_id + "' AND in_sign_foot = '" + in_sign_foot + "'");
            itmDetail.setProperty("source_id", event_id);

            if (in_sign_foot == "1")
            {
                itmDetail.setProperty("in_sign_foot", "1");
                itmDetail.setProperty("in_sign_no", "");
                itmDetail.setProperty("in_target_no", "");
                itmDetail.setProperty("in_target_foot", "2");
            }
            else
            {
                itmDetail.setProperty("in_sign_foot", "2");
                itmDetail.setProperty("in_sign_no", "");
                itmDetail.setProperty("in_target_no", "");
                itmDetail.setProperty("in_target_foot", "1");
            }

            itmDetail = itmDetail.apply();

            if (itmDetail.isError())
            {
                throw new Exception("建立場次明細發生失敗: " + in_sign_foot);
            }
        }
        #endregion 盟主戰

        private void AddDetail(TConfig cfg, Item itmNewEvent, string in_sign_foot, Item itmReturn)
        {
            Item itmDetail = cfg.inn.newItem("In_Meeting_PEvent_Detail", "add");
            itmDetail.setProperty("source_id", itmNewEvent.getID());

            if (in_sign_foot == "1")
            {
                itmDetail.setProperty("in_sign_foot", "1");
                itmDetail.setProperty("in_sign_no", itmReturn.getProperty("white_sign_no", ""));
                itmDetail.setProperty("in_target_no", itmReturn.getProperty("blue_sign_no", ""));
                itmDetail.setProperty("in_target_foot", "2");
            }
            else
            {
                itmDetail.setProperty("in_sign_foot", "2");
                itmDetail.setProperty("in_sign_no", itmReturn.getProperty("blue_sign_no", ""));
                itmDetail.setProperty("in_target_no", itmReturn.getProperty("white_sign_no", ""));
                itmDetail.setProperty("in_target_foot", "1");
            }

            itmDetail = itmDetail.apply();

            if (itmDetail.isError())
            {
                throw new Exception("建立場次明細發生失敗: " + in_sign_foot);
            }
        }

        //修改
        private void EditPEvent(TConfig cfg, Item itmReturn)
        {
            Item item = cfg.inn.newItem("In_Meeting_PEvent");
            item.setAttribute("where", "[In_Meeting_PEvent].id='" + cfg.event_id + "'");
            item.setProperty("id", cfg.event_id);
            item.setProperty("in_tree_id", itmReturn.getProperty("in_tree_id", ""));
            item.setProperty("in_tree_no", itmReturn.getProperty("in_tree_no", ""));
            item.setProperty("in_tree_alias", itmReturn.getProperty("in_tree_alias", ""));
            item.setProperty("in_tree_sno", itmReturn.getProperty("in_tree_no", ""));
            item.setProperty("in_tree_state", "0");
            item.setProperty("in_cancel", "0");
            item.setProperty("in_wait_player", "1");

            item.setProperty("in_round", itmReturn.getProperty("in_round", ""));
            item.setProperty("in_round_id", itmReturn.getProperty("in_round_id", ""));
            item.setProperty("in_round_code", itmReturn.getProperty("in_round_code", ""));

            item = item.apply("merge");

            if (item.isError())
            {
                throw new Exception("儲存發生錯誤");
            }

            string white_sign_no = itmReturn.getProperty("white_sign_no", "");
            string blue_sign_no = itmReturn.getProperty("blue_sign_no", "");

            Item itmFoot1 = cfg.inn.newItem("In_Meeting_PEvent_Detail");
            itmFoot1.setAttribute("where", "source_id='" + cfg.event_id + "' AND in_sign_foot='1'");
            itmFoot1.setProperty("source_id", cfg.event_id);
            itmFoot1.setProperty("in_sign_foot", "1");
            itmFoot1.setProperty("in_sign_no", white_sign_no);
            itmFoot1.setProperty("in_target_no", blue_sign_no);
            itmFoot1.setProperty("in_target_foot", "2");
            itmFoot1 = itmFoot1.apply("merge");

            Item itmFoot2 = cfg.inn.newItem("In_Meeting_PEvent_Detail");
            itmFoot2.setAttribute("where", "source_id='" + cfg.event_id + "' AND in_sign_foot='2'");
            itmFoot2.setProperty("source_id", cfg.event_id);
            itmFoot2.setProperty("in_sign_foot", "2");
            itmFoot2.setProperty("in_sign_no", blue_sign_no);
            itmFoot2.setProperty("in_target_no", white_sign_no);
            itmFoot1.setProperty("in_target_foot", "1");
            itmFoot2 = itmFoot2.apply("merge");
        }

        //刪除
        private void RemovePEvent(TConfig cfg, Item itmReturn)
        {
            //Item itmProgram = cfg.inn.applySQL("SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + program_id + "'");
            //string in_l1 = itmProgram.getProperty("in_l1", "");
            //string in_l2 = itmProgram.getProperty("in_l2", "");
            //string in_l3 = itmProgram.getProperty("in_l3", "");

            //Item itmPEvent = cfg.inn.applySQL("SELECT * FROM IN_MEETING_PEVENT WITH(NOLOCK) WHERE id = '" + event_id + "'");
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public Innosoft.InnovatorHelper InnH { get; set; }

            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }

            public string meeting_id { get; set; }
            public string program_id { get; set; }
            public string event_id { get; set; }
            public string scene { get; set; }

            public Item itmMeeting { get; set; }
            public string in_title { get; set; }
            public string mt_battle_type { get; set; }
            public int mt_robin_player { get; set; }
        }

        private class TEvent
        {
            public string Id { get; set; }
            public Item Value { get; set; }
            public Item Foot1 { get; set; }
            public Item Foot2 { get; set; }
        }

        private string GetDateTimeValue(string value, string format, int hours = 0)
        {
            if (value == "") return "";
            DateTime result = DateTime.MinValue;
            DateTime.TryParse(value, out result);
            return result.AddHours(hours).ToString(format);
        }

        private int GetIntVal(string value)
        {
            if (value == "" || value == "0") return 0;

            int result = 0;
            int.TryParse(value, out result);
            return result;
        }
    }
}