﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;
using System.Net;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class in_meeting_pevent_central : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 
                    競賽連結-戰情中心
                日期: 
                    - 2021-12-15: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]in_meeting_pevent_central";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                InnH = _InnH,

                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                in_date = itmR.getProperty("in_date", ""),
            };

            Page(cfg, itmR);

            return itmR;
        }

        private void Page(TConfig cfg, Item itmReturn)
        {
            string sql = "SELECT in_title, in_battle_type FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'";
            cfg.itmMeeting = cfg.inn.applySQL(sql);
            cfg.in_title = cfg.itmMeeting.getProperty("in_title", "");
            cfg.in_battle_type = cfg.itmMeeting.getProperty("in_battle_type", "");
            itmReturn.setProperty("in_title", cfg.in_title);

            //日期選單
            List<TDay> days = MapDateMenu(cfg);
            AppendItems(cfg, days, "inn_date", "in_date_key", "in_date_key", itmReturn);

            if (cfg.in_date == "")
            {
                string today = DateTime.Now.ToString("yyyy-MM-dd");
                if (days.Exists(x => x.day == today))
                {
                    cfg.in_date = today;
                    itmReturn.setProperty("in_date", cfg.in_date);
                }
                else
                {
                    cfg.in_date = days[0].day;
                    itmReturn.setProperty("in_date", cfg.in_date);
                }
            }

            if (cfg.in_date != "")
            {
                Table(cfg, itmReturn);
            }
        }

        private void Table(TConfig cfg, Item itmReturn)
        {
            //場地選單
            List<Item> itmSites = GetSiteMenu(cfg);

            Item itmEmptySite = cfg.inn.newItem();
            itmEmptySite.setType("inn_site");
            itmEmptySite.setProperty("in_name", "請選擇");
            itmReturn.addRelationship(itmEmptySite);

            for (int i = 0; i < itmSites.Count; i++)
            {
                Item itmSite = itmSites[i];
                itmSite.setType("inn_site");
                itmReturn.addRelationship(itmSite);
            }

            Dictionary<int, List<Item>> map = GetAllRows(cfg);

            //頁籤列表
            List<TSiteBox> sites = new List<TSiteBox>();

            for (int i = 0; i < itmSites.Count; i++)
            {
                Item itmSite = itmSites[i];
                string no = (i + 1).ToString();
                string id = "site_tab_" + no;
                string active = i == 0 ? "active" : "";

                var site = new TSiteBox
                {
                    id = id,
                    active = active,
                    title = itmSite.getProperty("in_name", ""),
                    in_site = itmSite.getProperty("id", ""),
                    in_code = GetInt32(itmSite.getProperty("in_code", "0")),
                    EvtList = new List<TEvt>(),
                    WaitingList = new List<TEvt>(),
                    FightedList = new List<TEvt>(),
                };

                if (map.ContainsKey(site.in_code))
                {
                    AppendEvents(cfg, site, map[site.in_code]);
                    sites.Add(site);
                }
            }


            SetBoxColMd(cfg, sites.Count, itmReturn);

            string box_col_md = itmReturn.getProperty("box_col_md", "");

            StringBuilder builder = new StringBuilder();

            for (int i = 0; i < sites.Count; i++)
            {
                TSiteBox site = sites[i];
                AppendSiteContents(cfg, site, box_col_md, builder);
            }

            itmReturn.setProperty("inn_tabs", builder.ToString());
        }

        private void SetBoxColMd(TConfig cfg, int count, Item itmReturn)
        {
            int cssCode = count % 12;
            string col_md = "col-md-3";

            string style = "";
            string style1 = ".flex-container { display: flex; justify-content: left; align-items: flex-start; flex-wrap: wrap; }";
            string style2 = ".flex-container { display: flex; justify-content: center; align-items: center; flex-wrap: wrap; }";

            switch (cssCode)
            {
                case 0:
                    col_md = "col-md-1";
                    style = style2;
                    break;
                case 1:
                    col_md = "col-md-4";
                    style = style2;
                    break;
                case 2:
                    col_md = "col-md-4";
                    style = style2;
                    break;
                case 3:
                    col_md = "col-md-4";
                    style = style1;
                    break;
                case 4:
                    col_md = "col-md-3";
                    style = style1;
                    break;
                case 5:
                    col_md = "col-md-3";
                    style = style1;
                    break;
                case 6:
                    col_md = "col-md-3";
                    style = style1;
                    break;
                case 7:
                    col_md = "col-md-4";
                    style = style1;
                    break;
                case 8:
                    col_md = "col-md-4";
                    style = style1;
                    break;
                case 9:
                    col_md = "col-md-3";
                    break;
                case 10:
                    col_md = "col-md-4";
                    style = style1;
                    break;
                case 11:
                    col_md = "col-md-4";
                    style = style1;
                    break;
                default:
                    break;
            }

            itmReturn.setProperty("box_col_md", col_md);
            itmReturn.setProperty("flex_container", style);
        }

        private void AppendSiteContents(TConfig cfg, TSiteBox site, string col_md, StringBuilder builder)
        {
            builder.Append("<div class='" + col_md + "'>");
            builder.Append("  <div class='box box-default' style='padding: 5px 5px;'>");

            builder.Append("    <div class='box-header with-border'>");
            builder.Append("      <i class='fa fa-bullhorn'></i>");
            builder.Append("      <h3 class='box-title'>" + site.title + "</h3>");
            builder.Append("    </div>");


            builder.Append("    <div class='box-body'>");

            //最新成績: 2筆
            AppendFightedEvents(cfg, site, 2, builder);

            //等待中: 3筆
            AppendWaitingEvents(cfg, site, 2, builder);


            builder.Append("    </div>");

            builder.Append("  </div>");
            builder.Append("</div>");
        }

        //最新成績
        private void AppendFightedEvents(TConfig cfg, TSiteBox site, int cnt, StringBuilder builder)
        {
            var fighteds = site.FightedList;
            if (fighteds == null || fighteds.Count == 0)
            {
                return;
            }

            var idx_s = fighteds.Count - cnt;
            var idx_e = fighteds.Count;
            var max_idx = fighteds.Count - 1;
            for (int i = idx_s; i < idx_e; i++)
            {
                int idx = i;
                if (idx < 0 || idx > max_idx)
                {
                    continue;
                }

                var evt = fighteds[idx];
                AppendEventBox(cfg, site, evt, builder, "rtm-green-box", "evt-green-btn");
            }
        }

        //等待中
        private void AppendWaitingEvents(TConfig cfg, TSiteBox site, int cnt, StringBuilder builder)
        {
            var waitins = site.WaitingList;
            if (waitins == null || waitins.Count == 0)
            {
                return;
            }

            //場上對打
            var first = site.WaitingList.First();
            AppendEventBox(cfg, site, first, builder, "rtm-red-box", "evt-red-btn");

            var idx_s = 1;
            var idx_e = 1 + cnt;
            var max_idx = waitins.Count - 1;
            for (int i = idx_s; i < idx_e; i++)
            {
                int idx = i;
                if (idx < 0 || idx > max_idx)
                {
                    continue;
                }

                var evt = waitins[idx];
                AppendEventBox(cfg, site, evt, builder, "rtm-blue-box", "evt-blue-btn");
            }
        }

        private void AppendEventBox(TConfig cfg, TSiteBox site, TEvt evt, StringBuilder builder, string box_css, string evt_css)
        {
            string pg_lv = "h4";
            string pg_name = evt.program_name;
            if (pg_name.Length > 10)
            {
                pg_lv = "h5";
            }
            builder.Append("<div class='callout " + box_css + "'>");
            builder.Append("  <" + pg_lv + " class='form-inline'>");
            builder.Append("    " + pg_name);
            builder.Append("    <small class='label pull-right bg-blue'>" + evt.in_tree_no + "</small>");
            builder.Append("  </" + pg_lv + ">");

            builder.Append("  <div class='row'>");
            builder.Append("    <div class='col-md-5 rtm-player'>");
            builder.Append("      <p class='rtm-par'>" + NameInfo(cfg, evt, evt.Foot1) + "</p>");
            builder.Append("      <p class='rtm-par'>" + OrgInfo(cfg, evt, evt.Foot1) + "</p>");
            builder.Append("    </div>");
            builder.Append("    <div class='col-md-2 rtm-score'>");
            builder.Append("      <p class='" + evt_css + "'>" + ScoreInfo(cfg, evt) + "</p>");
            builder.Append("    </div>");
            builder.Append("    <div class='col-md-5 rtm-player'>");
            builder.Append("      <p class='rtm-par'>" + NameInfo(cfg, evt, evt.Foot2) + "</p>");
            builder.Append("      <p class='rtm-par'>" + OrgInfo(cfg, evt, evt.Foot2) + "</p>");
            builder.Append("    </div>");
            builder.Append("  </div>");

            builder.Append("</div>");
        }

        private string NameInfo(TConfig cfg, TEvt evt, Item itmFoot)
        {
            string in_status = itmFoot.getProperty("in_status", "");
            string in_name = itmFoot.getProperty("in_name", "");
            if (in_status == "1")
            {
                return "<i class='fa fa-check red-bag'></i> " + in_name.Replace("(", "<br>(");
            }
            else
            {
                return in_name.Replace("(", "<br>(");
            }
        }

        private string OrgInfo(TConfig cfg, TEvt evt, Item itmFoot)
        {
            return itmFoot.getProperty("map_short_org", "");
        }

        private string ScoreInfo(TConfig cfg, TEvt evt)
        {
            if (!evt.has_fighted)
            {
                return "vs";
            }

            string f1_status = evt.Foot1.getProperty("in_status", "");
            string f1_points = evt.Foot1.getProperty("in_points", "0");
            string f1_ptype = evt.Foot1.getProperty("in_points_type", "");
            string f1_correct = evt.Foot1.getProperty("in_correct_count", "");

            string f2_status = evt.Foot2.getProperty("in_status", "");
            string f2_points = evt.Foot2.getProperty("in_points", "0");
            string f2_ptype = evt.Foot2.getProperty("in_points_type", "");
            string f2_correct = evt.Foot2.getProperty("in_correct_count", "");

            bool f1_out = f1_correct == "3";
            bool f2_out = f2_correct == "3";

            if (f1_out && f2_out)
            {
                return "S3" + ":" + "S3";
            }
            else if (f1_out)
            {
                return "S3" + ":" + f2_points;
            }
            else if (f2_out)
            {
                return f1_points + ":" + "S3";
            }


            return f1_points + ":" + f2_points;
        }

        private void AppendEvents(TConfig cfg, TSiteBox site, List<Item> items)
        {
            var evts = site.EvtList;
            var waitings = site.WaitingList;
            var fighteds = site.FightedList;

            int count = items.Count;

            for (int i = 0; i < count; i = i + 2)
            {
                Item item = items[i];
                Item f2 = items[i + 1];

                TEvt evt = new TEvt
                {
                    program_id = item.getProperty("program_id", ""),
                    program_name = item.getProperty("pg_name2", ""),
                    program_short_name = item.getProperty("pg_short_name", ""),
                    program_team_count = item.getProperty("in_team_count", "0"),
                    program_site_mat = item.getProperty("in_site_mat", ""),
                    program_site_mat2 = item.getProperty("in_site_mat2", ""),
                    program_fight_time = item.getProperty("in_fight_time", "0"),

                    event_id = item.getProperty("event_id", ""),
                    in_tree_id = item.getProperty("in_tree_id", ""),
                    in_tree_no = item.getProperty("in_tree_no", ""),
                    in_win_time = item.getProperty("in_win_time", ""),
                    in_type = item.getProperty("in_type", ""),
                    in_parent = item.getProperty("in_parent", ""),
                    Foot1 = item,
                    Foot2 = items[i + 1],
                    Children = new List<TEvt>(),
                };

                evt.has_fighted = evt.in_win_time != "";

                if (evt.in_type == "s")
                {
                    TEvt parent = evts.Find(x => x.event_id == evt.in_parent);
                    if (parent != null)
                    {
                        parent.Children.Add(evt);
                    }
                }
                else
                {
                    evts.Add(evt);
                }
            }

            //場次打上時間與分箱
            foreach (var evt in evts)
            {
                if (evt.Children.Count > 0)
                {
                    foreach (var child in evt.Children)
                    {
                        if(child.has_fighted)
                        {
                            fighteds.Add(child);
                        }
                        else
                        {
                            waitings.Add(child);
                        }
                    }
                }
                else
                {
                    if (evt.has_fighted)
                    {
                        fighteds.Add(evt);
                    }
                    else
                    {
                        waitings.Add(evt);
                    }
                }
            }
        }

        //日期選單
        private List<TDay> MapDateMenu(TConfig cfg)
        {
            List<TDay> list = new List<TDay>();
            Item items = GetDateMenu(cfg);
            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_date_key = item.getProperty("in_date_key", "");
                list.Add(new TDay { day = in_date_key, Value = item });
            }
            return list;
        }

        private Dictionary<int, List<Item>> GetAllRows(TConfig cfg)
        {
            Dictionary<int, List<Item>> map = new Dictionary<int, List<Item>>();

            string sql = @"
                SELECT
	                t1.id		        AS 'program_id'
	                , t1.in_name2       AS 'pg_name2'
	                , t1.in_short_name  AS 'pg_short_name'
	                , t1.in_team_count
	                , t1.in_site_mat
	                , t1.in_site_mat2
	                , t1.in_fight_time
	                , t2.id				AS 'event_id'
	                , t2.in_tree_id
	                , t2.in_tree_no
	                , t2.in_win_time
	                , t2.in_type
	                , t2.in_parent
	                , t3.id				AS 'detail_id'
	                , t3.in_sign_foot
	                , t3.in_sign_no
	                , t3.in_status
	                , t3.in_points
	                , t3.in_points_type
	                , t3.in_correct_count
	                , t4.id				AS 'team_id'
	                , ISNULL(t4.map_short_org, t3.in_player_org) AS 'map_short_org'
	                , ISNULL(t4.in_team, t3.in_player_team)      AS 'in_team'
	                , ISNULL(t4.in_name, t3.in_player_name)      AS 'in_name'
	                , ISNULL(t4.in_sno, t3.in_player_sno)        AS 'in_sno'
	                , t4.in_weight_message
                    , t11.in_code AS 'site_code' 
                FROM 
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PEVENT t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t3 WITH(NOLOCK)
	                ON t3.source_id = t2.id
                LEFT OUTER JOIN
                    VU_MEETING_PTEAM t4 WITH(NOLOCK)
                    ON t4.source_id = t1.id
                    AND t4.in_sign_no = t3.in_sign_no
                INNER JOIN
                	IN_MEETING_SITE t11 WITH(NOLOCK)
                	ON t11.id = t2.in_site
                WHERE 
	                t2.in_meeting = '{#meeting_id}'
	                AND t2.in_date_key = '{#in_date_key}'
	                AND ISNULL(t2.in_tree_no, 0) > 0
					AND ISNULL(t2.in_win_status, '') NOT IN ('bypass', 'cancel', 'nofight')
                ORDER BY
                    t11.in_code
	                , t2.in_tree_no
                    , t3.in_sign_foot
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_date_key}", cfg.in_date);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                int site_code = GetInt32(item.getProperty("site_code", "0"));

                List<Item> list = null;
                if (map.ContainsKey(site_code))
                {
                    list = map[site_code];
                }
                else
                {
                    list = new List<Item>();
                    map.Add(site_code, list);
                }

                list.Add(item);
            }

            return map;
        }

        //日期選單
        private Item GetDateMenu(TConfig cfg)
        {
            string sql = @"
                SELECT 
	                DISTINCT t1.in_date_key
                FROM 
	                IN_MEETING_PEVENT t1 WITH(NOLOCK) 
                WHERE 
	                t1.in_meeting = '{#meeting_id}'
	                AND ISNULL(t1.in_date_key, '') <> ''
                ORDER BY 
	                t1.in_date_key
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        //場地選單
        private List<Item> GetSiteMenu(TConfig cfg)
        {
            string sql = @"
                SELECT 
	                DISTINCT t2.id
                    , t2.in_code
                    , t2.in_name
                FROM 
	                IN_MEETING_PEVENT t1 WITH(NOLOCK) 
                INNER JOIN
                    IN_MEETING_SITE t2 WITH(NOLOCK)
                    ON t2.id = t1.in_site
                WHERE 
	                t1.in_meeting = '{#meeting_id}'
	                AND ISNULL(t1.in_site, '') <> ''
                ORDER BY 
	                t2.in_code
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return MapList(cfg.inn.applySQL(sql));
        }

        private List<Item> MapList(Item items)
        {
            int count = items.getItemCount();
            List<Item> list = new List<Item>();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                list.Add(item);
            }
            return list;
        }

        private void AddItem(TConfig cfg, List<Item> list, string type, string value, string label)
        {
            Item item = cfg.inn.newItem();
            item.setType(type);
            item.setProperty("value", value);
            item.setProperty("label", label);
            list.Add(item);
        }

        private void AppendItems(
            TConfig cfg
            , List<TDay> list
            , string type_name
            , string val_property
            , string lbl_property
            , Item itmReturn)
        {

            Item itmEmpty = cfg.inn.newItem();
            itmEmpty.setType(type_name);
            itmEmpty.setProperty("label", "請選擇");
            itmEmpty.setProperty("value", "");
            itmReturn.addRelationship(itmEmpty);

            int count = list.Count;
            for (int i = 0; i < count; i++)
            {
                Item item = list[i].Value;
                item.setType(type_name);
                item.setProperty("value", item.getProperty(val_property, ""));
                item.setProperty("label", item.getProperty(lbl_property, ""));
                itmReturn.addRelationship(item);
            }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public Innosoft.InnovatorHelper InnH { get; set; }

            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }

            public string meeting_id { get; set; }
            public string in_date { get; set; }

            public Item itmMeeting { get; set; }
            public string in_title { get; set; }
            public string in_battle_type { get; set; }
        }

        private class TDay
        {
            public string day { get; set; }
            public Item Value { get; set; }
        }

        private class TSiteBox
        {
            public string id { get; set; }
            public string active { get; set; }
            public string title { get; set; }
            public string groups { get; set; }

            public string li_id { get; set; }
            public string a_id { get; set; }
            public string content_id { get; set; }
            public string folder_id { get; set; }
            public string toggle_id { get; set; }
            public string table_id { get; set; }
            public string in_site { get; set; }
            public int in_code { get; set; }
            public bool is_error { get; set; }

            public List<TEvt> EvtList { get; set; }
            public List<TEvt> WaitingList { get; set; }
            public List<TEvt> FightedList { get; set; }
        }

        private class TEvt
        {
            public string program_id { get; set; }
            public string program_name { get; set; }
            public string program_short_name { get; set; }
            public string program_team_count { get; set; }
            public string program_site_mat { get; set; }
            public string program_site_mat2 { get; set; }
            public string program_fight_time { get; set; }

            public string event_id { get; set; }
            public string in_tree_id { get; set; }
            public string in_tree_no { get; set; }
            public string in_win_time { get; set; }
            public string in_type { get; set; }
            public string in_parent { get; set; }

            public bool has_fighted { get; set; }
            public List<TEvt> Children { get; set; }

            public Item Foot1 { get; set; }
            public Item Foot2 { get; set; }

        }

        //轉換日期
        private DateTime GetDateTime(string value)
        {
            if (value == "") return DateTime.MinValue;
            DateTime dt = DateTime.MinValue;
            DateTime.TryParse(value, out dt);
            return dt;
        }

        //轉換日期
        private string GetDateTimeValue(string value, string format)
        {
            if (value == "") return "";
            DateTime dt = DateTime.MinValue;
            DateTime.TryParse(value, out dt);
            return dt.ToString(format);
        }

        private int GetInt32(string value)
        {
            if (value == "" || value == "0") return 0;

            int result = 0;
            int.TryParse(value, out result);
            return result;
        }
    }
}