﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;
using System.Net;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class in_meeting_pevent_sub : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 建立子場次
                日期: 
                    - 2021-10-11 創建 (Lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]in_meeting_pevent_sub";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, this.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                InnH = _InnH,

                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                program_id = itmR.getProperty("program_id", ""),
                event_id = itmR.getProperty("event_id", ""),
            };

            string sql = "";

            sql = "SELECT in_l1, in_sub_event FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + cfg.program_id + "'";
            Item itmProgram = cfg.inn.applySQL(sql);

            if (itmProgram.isError())
            {
                throw new Exception("賽程組別發生錯誤");
            }

            string in_l1 = itmProgram.getProperty("in_l1", "");
            if (in_l1 != "團體組")
            {
                return itmR;
            }

            int sub_event = GetIntVal(itmProgram.getProperty("in_sub_event", "0"));
            if (sub_event <= 0)
            {
                throw new Exception("賽程組別 子場次數量 發生錯誤");
            }

            var lstSect = GetMeetingSection(cfg);

            if (sub_event > lstSect.Count)
            {
                throw new Exception("量級數量異常");
            }

            if (cfg.event_id != "")
            {
                sql = "SELECT * FROM IN_MEETING_PEVENT WITH(NOLOCK) WHERE id = '" + cfg.event_id + "'"
                    + " ORDER BY in_tree_sort, in_tree_id";
            }
            else
            {
                sql = "SELECT * FROM IN_MEETING_PEVENT WITH(NOLOCK) WHERE source_id = '" + cfg.program_id + "'"
                    + " ORDER BY in_tree_sort, in_tree_id";
            }

            Item itmEvents = cfg.inn.applySQL(sql);

            int count = itmEvents.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item itmEvent = itmEvents.getItemByIndex(i);

                TEvent evt = GetEvent(cfg, itmProgram, itmEvent, sub_event, lstSect);

                //建立子場次
                GenerateSub(cfg, evt);

                //建立代表戰
                GenerateEndEvent(cfg, evt);

                //更新主場次資料
                string sql_upd = "UPDATE IN_MEETING_PEVENT SET in_type = 'p' WHERE id = '" + itmEvent.getProperty("id", "") + "'";
                cfg.inn.applySQL(sql_upd);
            }

            return itmR;
        }

        //建立子場次
        private void GenerateSub(TConfig cfg, TEvent evt)
        {
            for (int i = 0; i < evt.sub_event; i++)
            {
                int no = i + 1;
                string sno = no.ToString();

                string s_tree_id = evt.in_tree_id + "-" + sno.PadLeft(2, '0');
                int s_tree_no = evt.base_no + no;

                Item item = cfg.inn.newItem("In_Meeting_PEvent", "add");
                item.setProperty("in_meeting", cfg.meeting_id);
                item.setProperty("source_id", cfg.program_id);
                item.setProperty("in_parent", evt.id);

                item.setProperty("in_tree_name", "sub");
                item.setProperty("in_tree_sort", "9000");
                item.setProperty("in_tree_id", s_tree_id);
                item.setProperty("in_tree_no", s_tree_no.ToString());
                item.setProperty("in_tree_rank", "");
                item.setProperty("in_tree_rank_ns", "");
                item.setProperty("in_tree_rank_nss", "");
                item.setProperty("in_tree_alias", "第" + sno + "場");

                item.setProperty("in_round", evt.in_round);
                item.setProperty("in_round_id", sno.ToString());
                item.setProperty("in_round_code", evt.sub_event.ToString());

                item.setProperty("in_sign_code", "");
                item.setProperty("in_sign_no", "");

                item.setProperty("in_surface_id", "0");
                item.setProperty("in_surface_name", "");

                item.setProperty("in_type", "s");
                item.setProperty("in_sub_id", sno);
                item.setProperty("in_sub_sect", evt.lstSect[i].getProperty("in_name", ""));

                item = item.apply();

                if (item.isError())
                {
                    throw new Exception("建立單場賽事組別場次發生失敗");
                }

                AppendDetail(cfg, evt, item, no, "1");
                AppendDetail(cfg, evt, item, no, "2");
            }
        }

        //建立代表戰
        private void GenerateEndEvent(TConfig cfg, TEvent evt)
        {
            if (evt.sub_event % 2 > 0) return;

            int no = evt.sub_event + 1;
            string sno = no.ToString();

            string s_tree_id = evt.in_tree_id + "-" + sno.PadLeft(2, '0');
            int s_tree_no = evt.base_no + no;

            Item item = cfg.inn.newItem("In_Meeting_PEvent", "add");
            item.setProperty("in_meeting", cfg.meeting_id);
            item.setProperty("source_id", cfg.program_id);
            item.setProperty("in_parent", evt.id);

            item.setProperty("in_tree_name", "sub");
            item.setProperty("in_tree_sort", "9000");
            item.setProperty("in_tree_id", s_tree_id);
            item.setProperty("in_tree_no", s_tree_no.ToString());
            item.setProperty("in_tree_rank", "");
            item.setProperty("in_tree_rank_ns", "");
            item.setProperty("in_tree_rank_nss", "");
            item.setProperty("in_tree_alias", "代表戰");

            item.setProperty("in_round", evt.in_round);
            item.setProperty("in_round_id", sno.ToString());
            item.setProperty("in_round_code", evt.sub_event.ToString());

            item.setProperty("in_sign_code", "");
            item.setProperty("in_sign_no", "");

            item.setProperty("in_surface_id", "0");
            item.setProperty("in_surface_name", "");

            item.setProperty("in_type", "s");
            item.setProperty("in_sub_id", sno);
            item.setProperty("in_sub_sect", "代表戰");

            item = item.apply();

            if (item.isError())
            {
                throw new Exception("建立單場賽事組別場次發生失敗");
            }

            AppendDetail(cfg, evt, item, no, "1");
            AppendDetail(cfg, evt, item, no, "2");

        }

        private void AppendDetail(TConfig cfg, TEvent evt, Item itmSub, int sub_no, string foot)
        {
            string name = "第" + sub_no + "位";

            Item itmDetail = cfg.inn.newItem("In_Meeting_PEvent_Detail", "add");

            itmDetail.setProperty("source_id", itmSub.getProperty("id", ""));
            itmDetail.setProperty("in_sign_foot", foot);
            itmDetail.setProperty("in_player_name", name);

            int sign_no = 0;

            if (evt.is_main_round_1)
            {
                sign_no = foot == "1"
                    ? evt.left_base_sign + sub_no
                    : evt.right_base_sign + sub_no;
            }
            else
            {
                sign_no = foot == "1"
                    ? (10 + sub_no) * -1
                    : (20 + sub_no) * -1;
            }

            if (sign_no != 0)
            {
                itmDetail.setProperty("in_sign_no", sign_no.ToString());
            }

            itmDetail = itmDetail.apply();
        }

        private TEvent GetEvent(TConfig cfg, Item itmProgram, Item itmEvent, int sub_event, List<Item> lstSect)
        {
            var evt = new TEvent
            {
                id = itmEvent.getProperty("id", ""),
                in_tree_name = itmEvent.getProperty("in_tree_name", ""),
                in_tree_id = itmEvent.getProperty("in_tree_id", ""),
                in_tree_no = itmEvent.getProperty("in_tree_no", ""),
                in_round = itmEvent.getProperty("in_round", ""),
                in_round_code = itmEvent.getProperty("in_round_code", ""),
                left_base_sign = 0,
                right_base_sign = 0,
                is_main_round_1 = false,
            };

            evt.sub_event = sub_event;
            evt.tree_no = GetIntVal(evt.in_tree_no);
            evt.base_no = evt.tree_no * 100;

            evt.itmProgram = itmProgram;
            evt.Value = itmEvent;
            evt.lstSect = lstSect;

            if (evt.in_tree_name == "main" && evt.in_round == "1")
            {
                evt.is_main_round_1 = true;
                string sql = "SELECT * FROM IN_MEETING_PEVENT_DETAIL WITH(NOLOCK) WHERE source_id = '" + evt.id + "'";
                Item itmDetails = cfg.inn.applySQL(sql);
                int count = itmDetails.getItemCount();
                for (int i = 0; i < count; i++)
                {
                    Item itmDetail = itmDetails.getItemByIndex(i);
                    string in_sign_foot = itmDetail.getProperty("in_sign_foot", "");
                    string in_sign_no = itmDetail.getProperty("in_sign_no", "0");
                    int sign_no = GetIntVal(in_sign_no);
                    if (sign_no <= 0)
                    {
                        continue;
                    }

                    if (in_sign_foot == "1")
                    {
                        evt.left_base_sign = sign_no * 100;
                    }
                    else if (in_sign_foot == "2")
                    {
                        evt.right_base_sign = sign_no * 100;
                    }
                }
            }

            return evt;
        }

        //取得團體賽量級清單
        private List<Item> GetMeetingSection(TConfig cfg)
        {
            List<Item> result = new List<Item>();

            string sql = "SELECT * FROM In_Meeting_PSect WITH(NOLOCK) WHERE in_meeting = '" + cfg.meeting_id + "'"
                + " ORDER BY in_sub_id";

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);
            Item items = cfg.inn.applySQL(sql);
            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                result.Add(items.getItemByIndex(i));
            }
            return result;
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public Innosoft.InnovatorHelper InnH { get; set; }

            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }

            public string meeting_id { get; set; }
            public string program_id { get; set; }
            public string event_id { get; set; }
        }

        private class TEvent
        {
            public string id { get; set; }
            public string in_tree_name { get; set; }
            public string in_tree_id { get; set; }
            public string in_tree_no { get; set; }
            public string in_round { get; set; }
            public string in_round_code { get; set; }

            public int sub_event { get; set; }
            public int tree_no { get; set; }
            public int base_no { get; set; }

            public bool is_main_round_1 { get; set; }
            public int left_base_sign { get; set; }
            public int right_base_sign { get; set; }

            public Item Value { get; set; }
            public Item itmProgram { get; set; }

            public List<Item> lstSect { get; set; }
        }

        private int GetIntVal(string value, int def = 0)
        {
            int result = def;
            Int32.TryParse(value, out result);
            return result;
        }
    }
}