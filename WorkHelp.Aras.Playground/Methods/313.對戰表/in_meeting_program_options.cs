﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;
using System.Net;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class in_meeting_program_options : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 
                    取得三階選單 json
                輸入: 
                    meeting_id
                日期: 
                    - 2021-12-01: 加入 Cache (lina)
                    - 2020-10-30: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]in_meeting_program_options";

            string meeting_id = this.getProperty("meeting_id", "").Trim();
            string scene = this.getProperty("scene", "").Trim();

            if (meeting_id == "")
            {
                throw new Exception("賽事 id 不得為空白");
            }

            if (scene == "clear")
            {
                return ClearCache(CCO, strMethodName, inn, meeting_id);
            }
            else
            {
                return JsonData(CCO, strMethodName, inn, meeting_id);
            }
        }

        //清除暫存資訊(三階選單)
        private Item ClearCache(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string meeting_id)
        {
            var _caching = System.Runtime.Caching.MemoryCache.Default;

            var key1 = meeting_id + "_LV_OPTIONS_TOTAL";
            var key2 = meeting_id + "_LV_OPTIONS_KEY";

            _caching.Remove(key1);
            _caching.Remove(key2);

            return inn.newItem();
        }

        private Item JsonData(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string meeting_id)
        {
            string aml = "";
            string sql = "";

            var _caching = System.Runtime.Caching.MemoryCache.Default;
            var _policy = new System.Runtime.Caching.CacheItemPolicy
            {
                SlidingExpiration = TimeSpan.FromMinutes(10)
            };

            var key1 = meeting_id + "_LV_OPTIONS_TOTAL";
            var key2 = meeting_id + "_LV_OPTIONS_KEY";

            string str1 = _caching.Contains(key1) ? _caching.Get(key1) as string : "";
            string str2 = _caching.Contains(key2) ? _caching.Get(key2) as string : "";

            if (string.IsNullOrEmpty(str1) || string.IsNullOrEmpty(str2))
            {
                sql = @"
                SELECT 
                    *
                FROM 
                    IN_MEETING_PROGRAM WITH(NOLOCK) 
                WHERE
                    in_meeting = '{#meeting_id}'
                    AND ISNULL(in_program, '') = ''
                ORDER BY 
                    in_sort_order
            ";

                sql = sql.Replace("{#meeting_id}", meeting_id);

                Item items = inn.applySQL(sql);

                int count = items.getItemCount();

                List<TNode> nodes = new List<TNode>();

                bool need_id = this.getProperty("need_id", "").Trim() == "1";

                int ptotal = 0;
                int etotal = 0;

                for (int i = 0; i < count; i++)
                {
                    Item item = items.getItemByIndex(i);
                    TNode l1 = AddAndGetNode(nodes, item, "in_l1", need_id);
                    TNode l2 = AddAndGetNode(l1.Nodes, item, "in_l2", need_id);
                    TNode l3 = AddAndGetNode(l2.Nodes, item, "in_l3", need_id);

                    string in_team_count = item.getProperty("in_team_count", "0");
                    int team_count = GetIntVal(in_team_count);

                    string in_event_count = item.getProperty("in_event_count", "0");
                    int event_count = GetIntVal(in_event_count);


                    l3.Cnt = team_count;
                    l2.Cnt = l2.Cnt + team_count;
                    l1.Cnt = l1.Cnt + team_count;
                    ptotal = ptotal + team_count;

                    l3.ECnt = event_count;
                    l2.ECnt = l2.ECnt + event_count;
                    l1.ECnt = l1.ECnt + event_count;
                    etotal = etotal + event_count;

                }

                str1 = ptotal.ToString();
                str2 = Newtonsoft.Json.JsonConvert.SerializeObject(nodes);

                _caching.Set(key1, str1, _policy);
                _caching.Set(key2, str2, _policy);
            }

            Item itmResult = inn.newItem();
            itmResult.setProperty("total", str1);
            itmResult.setProperty("json", str2);
            return itmResult;
        }

        private TNode AddAndGetNode(List<TNode> nodes, Item item, string lv, bool need_id)
        {
            string value = item.getProperty(lv, "");

            TNode search = nodes.Find(x => x.Val == value);

            if (search == null)
            {
                search = new TNode
                {
                    Lv = lv,
                    Val = value,
                    Lbl = value,
                    Cnt = 0,
                    Nodes = new List<TNode>()
                };

                if (need_id)
                {
                    search.Id = item.getProperty("id", "");
                }

                nodes.Add(search);
            }

            return search;
        }

        private int GetIntVal(string value, int def = 0)
        {
            int result = def;
            Int32.TryParse(value, out result);
            return result;
        }

        private class TNode
        {
            public string Id { get; set; }

            public string Lv { get; set; }

            public string Val { get; set; }

            public string Lbl { get; set; }

            public int Cnt { get; set; }

            public int ECnt { get; set; }

            public List<TNode> Nodes { get; set; }
        }
    }
}