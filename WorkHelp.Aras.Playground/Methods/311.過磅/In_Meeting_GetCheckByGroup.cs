﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;
using System.Net;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class In_Meeting_GetCheckByGroup : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 抓取競賽群組尚未過磅資訊
                日期: 
                    - 2021-11-26 備註加入上次過磅紀錄 (lina)
                    - 2020-10-29 創建 (Alan)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]In_Meeting_GetCheckByGroup";

            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            Item itmR = this;

            Item itmPermit = inn.applyMethod("In_CheckIdentity", "<method>" + strMethodName + "</method><code>" + "ALL" + "</code>");
            bool isMeetingAdmin = itmPermit.getProperty("isMeetingAdmin", "") == "1";
            bool isMeetingLeader = itmPermit.getProperty("isMeetingLeader", "") == "1";
            bool can_view = isMeetingAdmin || isMeetingLeader;
            if (!can_view)
            {
                itmR.setProperty("error_message", "您無權限瀏覽此頁面，請重新登入");
                return itmR;
            }

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                InnH = _InnH,

                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                program_id = itmR.getProperty("program_id", ""),
                in_date = itmR.getProperty("in_date", ""),
                in_l1 = itmR.getProperty("in_l1", ""),
                in_l2 = itmR.getProperty("in_l2", ""),
                in_l3 = itmR.getProperty("in_l3", ""),
                mode = itmR.getProperty("mode", ""),
                scene = itmR.getProperty("scene", ""),
            };

            if (cfg.meeting_id == "")
            {
                return itmR;
            }
            else
            {
                itmR.setProperty("id", cfg.meeting_id);
            }

            cfg.itmMeeting = cfg.inn.applySQL("SELECT in_title FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");

            if (cfg.itmMeeting.isError() || cfg.itmMeeting.getResult() == "")
            {
                throw new Exception("找無該場賽事資料!");
            }

            cfg.mt_title = cfg.itmMeeting.getProperty("in_title", "");
            itmR.setProperty("in_title", cfg.mt_title);

            if (cfg.scene == "export_weight")
            {
                //下載稱量體重表(單一 OR 當日)
                XlsFunc(cfg, itmR);
            }
            else
            {
                PageFunc(cfg, itmR);
            }


            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

            return itmR;
        }

        #region 當日過磅單

        private void XlsFunc(TConfig cfg, Item itmReturn)
        {
            Item itmPrograms = GetDayPrograms(cfg);
            if (itmPrograms.isError() || itmPrograms.getItemCount() <= 0)
            {
                throw new Exception("組別資料錯誤");
            }

            var programs = MapProgram(cfg, itmPrograms);

            var exp = ExportInfo(cfg, "weight_path");
            exp.export_Path = exp.export_Path.Replace("meeting_excel", "meeting_weight");

            if (!System.IO.Directory.Exists(exp.export_Path))
            {
                System.IO.Directory.CreateDirectory(exp.export_Path);
            }

            //總表
            CreateALLWeight(cfg, programs, exp, itmReturn);
        }

        private TExport ExportInfo(TConfig cfg, string in_name)
        {
            Item itmXls = cfg.inn.applyMethod("In_Meeting_Excel_Path", "<in_name>weight_path</in_name>");
            string Export_Path = itmXls.getProperty("export_path", "");
            string Template_Path = itmXls.getProperty("template_path", "");

            TExport result = new TExport
            {
                template_Path = Template_Path,
                export_Path = Export_Path,
            };

            return result;
        }

        private List<TProgram> MapProgram(TConfig cfg, Item itmPrograms)
        {
            List<TProgram> list = new List<TProgram>();

            int count = itmPrograms.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item itmProgram = itmPrograms.getItemByIndex(i);
                string no = (i + 1).ToString().PadLeft(2, '0');
                string in_name = itmProgram.getProperty("in_name", "");
                string in_name2 = itmProgram.getProperty("in_name2", "");
                string in_short_name = itmProgram.getProperty("in_short_name", "");

                TProgram entity = new TProgram
                {
                    no = no,
                    name = in_name,
                    name2 = in_name2,
                    sheet_name = in_short_name,
                    in_l1 = itmProgram.getProperty("in_l1", ""),
                    in_l2 = itmProgram.getProperty("in_l2", ""),
                    in_l3 = itmProgram.getProperty("in_l3", ""),
                };

                list.Add(entity);
            }

            return list;
        }

        private void CreateALLWeight(TConfig cfg, List<TProgram> programs, TExport exp, Item itmReturn)
        {
            Spire.Xls.Workbook workbook = new Spire.Xls.Workbook();
            workbook.LoadFromFile(exp.template_Path);

            Spire.Xls.Worksheet sheetTemplate = workbook.Worksheets[0];

            List<TField> fields = new List<TField>();
            fields.Add(new TField { RS = "A", RE = "A", property = "cno", fontName = "Times New Roman", fontSize = 12, fontBold = true, is_num = true });
            fields.Add(new TField { RS = "B", RE = "B", property = "in_name", fontName = "新細明體", fontSize = 12 });
            fields.Add(new TField { RS = "C", RE = "C", property = "in_short_org", fontName = "新細明體", fontSize = 12 });
            fields.Add(new TField { RS = "D", RE = "D", property = "in_weight", fontName = "Times New Roman", fontSize = 12, is_num = true });
            fields.Add(new TField { RS = "E", RE = "F", property = "space4", fontName = "Times New Roman", fontSize = 12 });
            fields.Add(new TField { RS = "G", RE = "G", property = "in_weight_note", fontName = "Times New Roman", fontSize = 10 });

            for (int i = 0; i < programs.Count; i++)
            {
                var program = programs[i];
                if (program.sheet_name == "")
                {
                    cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "n1-n3 is null: " + program.name);
                    continue;
                }

                CreateWeightXls(cfg, program, workbook, sheetTemplate, fields);
            }

            //移除樣板 sheet
            sheetTemplate.Remove();

            string ext_name = ".xlsx";

            string xls_name = "稱量體重表"
                    + "_" + cfg.mt_title
                    + "_" + DateTime.Now.ToString("_MMdd_HHmmss");

            if (cfg.in_l3 != "" && programs.Count == 1)
            {
                xls_name = "稱量體重表"
                    + "_" + programs[0].name2
                    + "_" + DateTime.Now.ToString("_MMdd_HHmmss");
            }
            else if (cfg.in_date != "")
            {
                xls_name = "稱量體重表"
                    + "_" + cfg.in_date.Replace("-", "") + "(比賽日期)"
                    + "_" + DateTime.Now.ToString("_MMdd_HHmmss");

            }

            string xls_file = exp.export_Path + "\\" + xls_name + ext_name;
            string xls_url = xls_name + ext_name;

            workbook.SaveToFile(xls_file, Spire.Xls.ExcelVersion.Version2010);

            itmReturn.setProperty("xls_name", xls_url);
        }

        private void CreateWeightXls(TConfig cfg, TProgram program, Spire.Xls.Workbook workbook, Spire.Xls.Worksheet sheetTemplate, List<TField> fields)
        {
            Spire.Xls.Worksheet sheet = workbook.CreateEmptySheet();
            sheet.CopyFrom(sheetTemplate);
            sheet.Name = program.sheet_name;

            sheet.Range["A1"].Text = cfg.mt_title;

            Item itmMUsers = GetMUsers(cfg, program.in_l1, program.in_l2, program.in_l3);
            if (itmMUsers.isError() || itmMUsers.getItemCount() < 0)
            {
                return;
            }

            Dictionary<string, TPlayer> map = GetWeightRecords(cfg, program.in_l1, program.in_l2, program.in_l3);

            string in_section_name = itmMUsers.getItemByIndex(0).getProperty("in_section_name");
            sheet.Range["C5"].Text = in_section_name;

            int wsRow = 8;
            int count = itmMUsers.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item itmMUser = itmMUsers.getItemByIndex(i);

                //取得上次過磅體重
                itmMUser.setProperty("in_weight_note", GetWeightNote2(cfg, itmMUser, map, Environment.NewLine));

                string in_name = itmMUser.getProperty("in_name", "");
                bool long_name = in_name.Contains("(");
                int length = in_name.Length;

                int height = long_name ? 36 : 21;
                if (length >= 12) height = 45;

                itmMUser.setProperty("cno", (i + 1).ToString());

                int ctRow = wsRow + i;

                sheet.Range["E" + ctRow + ":F" + ctRow].Merge();
                sheet.Rows[ctRow - 1].RowHeight = height;

                for (int j = 0; j < fields.Count; j++)
                {
                    var field = fields[j];
                    string value = "";

                    if (long_name)
                    {
                        value = itmMUser.getProperty(field.property, "").Replace("(", Environment.NewLine + "(");
                    }
                    else
                    {
                        value = itmMUser.getProperty(field.property, "");
                    }

                    var pos = field.RS + ctRow;
                    var range = sheet.Range[pos];

                    if (value == "")
                    {

                    }
                    else if (field.is_num)
                    {
                        range.NumberValue = GetDblVal(value);
                        range.NumberFormat = "#,##0.00";
                    }
                    else
                    {
                        range.Text = value;
                    }

                    range.Text = value;
                    range.Style.Font.FontName = field.fontName;
                    range.Style.Font.Size = field.fontSize;
                    range.Style.Font.IsBold = field.fontBold;
                    range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                }
            }

            var minRow = wsRow;
            var maxRow = wsRow + count - 1;

            var fs = fields.First();
            var fe = fields.Last();

            var tbl_pos = fs.RS + minRow + ":" + fe.RE + maxRow;
            SetRangeBorder(sheet, tbl_pos);

            // for (int i = 0; i < count; i++)
            // {
            //     int ctRow = wsRow + i;
            //     SetRangeBorder(sheet, fs.RS + ctRow + ":" + fe.RE + ctRow);
            // }
        }

        private void SetRangeBorder(Spire.Xls.Worksheet sheet, string pos)
        {
            var range = sheet.Range[pos];
            range.BorderInside(Spire.Xls.LineStyleType.Thin, System.Drawing.Color.Black);
            range.BorderAround(Spire.Xls.LineStyleType.Thin, System.Drawing.Color.Black);

            //lina 2021.11.29: 有 bug，合併儲存格加上格式會有錯亂格式
            // rg.Style.Borders[Spire.Xls.BordersLineType.EdgeTop].LineStyle = Spire.Xls.LineStyleType.Thin;
            // rg.Style.Borders[Spire.Xls.BordersLineType.EdgeRight].LineStyle = Spire.Xls.LineStyleType.Thin;
            // rg.Style.Borders[Spire.Xls.BordersLineType.EdgeBottom].LineStyle = Spire.Xls.LineStyleType.Thin;
            // rg.Style.Borders[Spire.Xls.BordersLineType.EdgeLeft].LineStyle = Spire.Xls.LineStyleType.Thin;

        }

        private Item GetDayPrograms(TConfig cfg)
        {
            string sql = "SELECT t1.* FROM IN_MEETING_PROGRAM t1 WITH(NOLOCK)"
            + " WHERE t1.in_meeting = '" + cfg.meeting_id + "'";

            if (cfg.in_date != "") sql += " AND t1.in_fight_day = '" + cfg.in_date + "'";

            if (cfg.in_l1 != "") sql += " AND t1.in_l1 = N'" + cfg.in_l1 + "'";
            if (cfg.in_l2 != "") sql += " AND t1.in_l2 = N'" + cfg.in_l2 + "'";
            if (cfg.in_l3 != "") sql += " AND t1.in_l3 = N'" + cfg.in_l3 + "'";
            sql += " ORDER BY t1.in_sort_order";

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        #endregion 當日過磅單

        private void PageFunc(TConfig cfg, Item itmReturn)
        {
            if (cfg.program_id != "" && cfg.in_l1 == "")
            {
                cfg.itmProgram = cfg.inn.applySQL("SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + cfg.program_id + "'");
                if (!cfg.itmProgram.isError() && cfg.itmProgram.getResult() != "")
                {
                    cfg.in_l1 = cfg.itmProgram.getProperty("in_l1", "");
                    cfg.in_l2 = cfg.itmProgram.getProperty("in_l2", "");
                    cfg.in_l3 = cfg.itmProgram.getProperty("in_l3", "");
                    itmReturn.setProperty("l1", cfg.in_l1);
                    itmReturn.setProperty("l2", cfg.in_l2);
                    itmReturn.setProperty("l3", cfg.in_l3);
                }
            }
            else
            {
                cfg.itmProgram = GetProgram(cfg);
                cfg.program_id = cfg.itmProgram.getProperty("id", "");
            }

            cfg.pg_sign_time = cfg.itmProgram.getProperty("in_sign_time", "");

            cfg.has_sign_no = cfg.pg_sign_time != "";
            cfg.has_fight_day = cfg.in_date != "";


            if (cfg.mode == "list")
            {
                ListPage(cfg, itmReturn);
            }
            else if (cfg.mode == "related")
            {
                //分組統計
                Item itmSurveyLevel = GetSurveyLevels(cfg);
                AppendGroupingStatistics(cfg, itmSurveyLevel, itmReturn);

                //附加選單資訊
                Item itmJson = cfg.inn.newItem("In_Meeting");
                itmJson.setProperty("meeting_id", cfg.meeting_id);
                itmJson.setProperty("need_id", "1");
                itmJson = itmJson.apply("in_meeting_program_options");
                itmReturn.setProperty("in_level_json", itmJson.getProperty("json", ""));

            }
            else
            {
                //附加比賽日期選單
                AppendFightMenu(cfg, itmReturn);

                //分組統計
                Item itmSurveyLevel = GetSurveyLevels(cfg);
                AppendGroupingStatistics(cfg, itmSurveyLevel, itmReturn);

                //附加選單資訊
                Item itmJson = cfg.inn.newItem("In_Meeting");
                itmJson.setProperty("meeting_id", cfg.meeting_id);
                itmJson.setProperty("need_id", "1");
                itmJson = itmJson.apply("in_meeting_program_options");
                itmReturn.setProperty("in_level_json", itmJson.getProperty("json", ""));
            }
        }

        #region 附加比賽日期選單

        //附加比賽日期選單
        private void AppendFightMenu(TConfig cfg, Item itmReturn)
        {
            Item itmDays = GetFightDays(cfg);
            List<TOpt> day_opts = MapOptList(cfg, itmDays, IsDayMatch, "請選擇比賽日期");
            string day_ctrl = ToSelectCtrl(cfg, day_opts, "date_menu", "FightDay_Change(this)");
            itmReturn.setProperty("inn_fight_days", day_ctrl);

            if (cfg.has_fight_day)
            {
                Item itmPrograms = GetFightPrograms(cfg);
                List<TOpt> program_opts = MapOptList(cfg, itmPrograms, IsProgramMatch, "請選擇組別");
                string program_ctrl = ToSelectCtrl(cfg, program_opts, "program_menu", "FightProgram_Change(this)");
                itmReturn.setProperty("inn_fight_programs", program_ctrl);
            }
        }

        private Item GetFightPrograms(TConfig cfg)
        {
            string sql = @"
                SELECT 
	                t1.id				AS 'value'
	                , t1.in_name2 +' (' + CAST(t1.in_team_count AS VARCHAR) + ')' AS 'text'
	                , t1.in_l1 + '@' + t1.in_l2 + '@' + t1.in_l3 AS 'ext'
	                , t1.in_l2 AS 'group'
                FROM 
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
				INNER JOIN
					IN_MEETING_ALLOCATION t2 WITH(NOLOCK)
					ON t2.in_program = t1.id
				INNER JOIN
					IN_MEETING_SITE t3 WITH(NOLOCK)
					ON t3.id = t2.in_site
                WHERE 
	                t1.in_meeting = '{#meeting_id}' 
	                AND t1.in_fight_day = '{#in_fight_day}'
                ORDER BY 
	                t1.in_sort_order
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_fight_day}", cfg.in_date);

            return cfg.inn.applySQL(sql);
        }

        private Item GetFightDays(TConfig cfg)
        {
            string sql = @"
                SELECT 
	                DISTINCT in_fight_day AS 'value'
	                , in_fight_day AS 'text'
                FROM 
	                IN_MEETING_PROGRAM WITH(NOLOCK) 
                WHERE 
	                in_meeting = '{#meeting_id}' 
	                AND ISNULL(in_fight_day, '') <> '' 
                ORDER BY 
	                in_fight_day
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            return cfg.inn.applySQL(sql);
        }

        private string ToSelectCtrl(TConfig cfg, List<TOpt> list, string id, string onchange)
        {
            StringBuilder options = new StringBuilder();

            foreach (var opt in list)
            {
                if (opt.Children != null)
                {
                    options.Append("<optgroup label='" + opt.group + "'>");
                    foreach (var child in opt.Children)
                    {
                        options.Append("<option data-ext='" + child.ext + "'"
                            + " value='" + child.value + "' " + child.selected + ">" + child.text + "</option>");
                    }
                    options.Append("</optgroup>");
                }
                else
                {
                    options.Append("<option data-ext='" + opt.ext + "'"
                        + " value='" + opt.value + "' " + opt.selected + ">" + opt.text + "</option>");
                }
            }

            StringBuilder builder = new StringBuilder();
            builder.Append("<select id='" + id + "' class='form-control' onchange='" + onchange + "'>");
            builder.Append(options);
            builder.Append("</select>");
            return builder.ToString();
        }

        private bool IsDayMatch(TConfig cfg, TOpt opt)
        {
            if (cfg.has_fight_day)
            {
                return opt.value == cfg.in_date;
            }
            else
            {
                return opt.value == DateTime.Now.ToString("yyyy-MM-dd");
            }
        }

        private bool IsProgramMatch(TConfig cfg, TOpt opt)
        {
            return opt.value == cfg.program_id;
        }

        private List<TOpt> MapOptList(TConfig cfg, Item items, Func<TConfig, TOpt, bool> is_match, string def_opt_text)
        {
            List<TOpt> result = new List<TOpt>();

            result.Add(new TOpt
            {
                text = def_opt_text,
                value = "",
                selected = "",
            });

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string gp = item.getProperty("group", "");

                if (gp.Contains("團-"))
                {
                    gp = "團體組";
                }
                else
                {
                    gp = gp.Replace("個-", "");
                }

                if (gp != "")
                {
                    TOpt group = result.Find(x => x.group == gp);
                    if (group == null)
                    {
                        group = new TOpt
                        {
                            group = gp,
                            Children = new List<TOpt>(),
                        };
                        result.Add(group);
                    }

                    TOpt opt = new TOpt
                    {
                        value = item.getProperty("value", ""),
                        text = item.getProperty("text", ""),
                        ext = item.getProperty("ext", ""),
                    };

                    opt.selected = is_match(cfg, opt) ? "selected" : "";

                    group.Children.Add(opt);
                }
                else
                {
                    TOpt opt = new TOpt
                    {
                        value = item.getProperty("value", ""),
                        text = item.getProperty("text", ""),
                        ext = item.getProperty("ext", ""),
                    };

                    opt.selected = is_match(cfg, opt) ? "selected" : "";

                    result.Add(opt);
                }
            }

            return result;
        }

        private class TOpt
        {
            public string group { get; set; }
            public string text { get; set; }
            public string value { get; set; }
            public string ext { get; set; }
            public string selected { get; set; }
            public List<TOpt> Children { get; set; }
        }

        #endregion 附加比賽日期選單

        private void ListPage(TConfig cfg, Item itmReturn)
        {
            if (cfg.program_id == "") return;

            if (cfg.has_sign_no)
            {
                //更新與會者籤號
                FixMUserSignNo(cfg);
            }

            Item itmMUsers = GetMUsers(cfg, cfg.in_l1, cfg.in_l2, cfg.in_l3);

            if (itmMUsers.isError() || itmMUsers.getResult() == "")
            {
                return;
            }

            Dictionary<string, TPlayer> map = GetWeightRecords(cfg, cfg.in_l1, cfg.in_l2, cfg.in_l3);

            int count = itmMUsers.getItemCount();
            int group_y = 0;
            int group_n = 0;
            int group_null = 0;

            for (int i = 0; i < count; i++)
            {
                string no = (i + 1).ToString();

                Item itmMUser = itmMUsers.getItemByIndex(i);
                itmMUser.setType("inn_groupList");

                string userID = itmMUser.getProperty("id", "");
                string in_weight = itmMUser.getProperty("in_weight", "");
                string in_weight_result = itmMUser.getProperty("in_weight_result", "");
                string weight_class = itmMUser.getProperty("in_extend_value2", "");
                string rollcall = itmMUser.getProperty("rollcall", "");
                string in_sno = itmMUser.getProperty("in_sno", "");

                itmMUser.setProperty("in_weight", "<input data-value='" + in_weight + "' id='" + in_sno + "' style='width:100%' class='eventKD' type='number' value='" + in_weight + "' > ");

                itmMUser.setProperty("in_weight_status", GetWeightStatus(itmMUser));

                //未設定體重限制
                if (weight_class == "")
                {
                    //group_y++;
                    if (in_weight != "")
                    {
                        itmMUser.setProperty("in_weight_result", "<font id='" + in_sno + "_" + "result" + "'>-</font>");
                    }
                    else
                    {
                        itmMUser.setProperty("in_weight_result", "<font id='" + in_sno + "_" + "result" + "' color='darkgoldenrod'><b>未過磅</b></font>");
                        group_null++;
                    }

                    if (in_weight == "" && in_weight_result == "0")
                    {
                        itmMUser.setProperty("in_weight_result", "<font id='" + in_sno + "_" + "result" + "' color='red'><b>DQ(未過磅)</b></font>");
                    }

                }
                else
                {
                    if (in_weight != "" || in_weight_result != "")
                    {
                        if (in_weight_result == "1")
                        {
                            itmMUser.setProperty("in_weight_result", "<font id='" + in_sno + "_" + "result" + "' color='green'><b>過磅合格</b></font>");
                            group_y++;
                        }
                        else
                        {
                            if (in_weight == "")
                            {
                                in_weight = "未過磅";
                            }
                            else
                            {
                                in_weight = "公斤不符";
                            }
                            itmMUser.setProperty("in_weight_result", "<font id='" + in_sno + "_" + "result" + "' color='red'><b>DQ(" + in_weight + ")</b></font>");
                            group_n++;
                        }
                    }
                    else
                    {
                        itmMUser.setProperty("in_weight_result", "<font id='" + in_sno + "_" + "result" + "' color='darkgoldenrod'><b>未過磅</b></font>");
                        group_null++;
                    }
                }

                if (rollcall == "1")
                {
                    itmMUser.setProperty("in_rollcall_result", "<font color='green'><b>已檢錄</b></font>");
                }
                else
                {
                    itmMUser.setProperty("in_rollcall_result", "<font color='darkgoldenrod'><b>未檢錄</b></font>");
                }

                string in_check = @"<button class='btn btn-sm btn-info' onclick='go_rollcall(""{#in_sno}"")'>檢錄</button>&emsp;&emsp;<button class='btn btn-sm btn-primary' onclick='go_check(""{#iD}"",""{#rollcall}"")'>過磅</button>";
                in_check = in_check.Replace("{#iD}", userID).Replace("{#rollcall}", rollcall)
                            .Replace("{#in_sno}", in_sno);
                itmMUser.setProperty("in_check", in_check);

                //排列序號，抽籤後會變為柔道籤號
                string in_section_no = itmMUser.getProperty("in_section_no", "");

                if (cfg.has_sign_no)
                {
                    itmMUser.setProperty("no", (i + 1).ToString());
                    itmMUser.setProperty("in_section_no", in_section_no);
                }
                else
                {
                    itmMUser.setProperty("no", (i + 1).ToString());
                    itmMUser.setProperty("in_section_no", "-");
                }

                //取得上次過磅體重
                itmMUser.setProperty("in_weight_note", GetWeightNote(cfg, itmMUser, map, "<br>"));

                itmReturn.addRelationship(itmMUser);
            }

            itmReturn.setProperty("group_c", count.ToString());
            itmReturn.setProperty("group_y", group_y.ToString());
            itmReturn.setProperty("group_n", group_n.ToString());
            itmReturn.setProperty("group_null", group_null.ToString());
        }

        private string GetWeightNote(TConfig cfg, Item itmMUser, Dictionary<string, TPlayer> map, string line_char)
        {
            string in_sno = itmMUser.getProperty("in_sno", "").ToUpper();
            if (!map.ContainsKey(in_sno)) return "";

            TPlayer player = map[in_sno];
            if (player.WeightRecords == null || player.WeightRecords.Count == 0) return "";

            return string.Join(line_char, player.WeightRecords.Select(x => x.show_weight_time
                + " " + x.pg_short_name
                + " " + x.in_weight
                ));
        }

        private string GetWeightNote2(TConfig cfg, Item itmMUser, Dictionary<string, TPlayer> map, string line_char)
        {
            string result = "";
            string in_weight_status = itmMUser.getProperty("in_weight_status", "");

            if (in_weight_status == "leave")
            {
                result = "請假 ";
            }

            result += GetWeightNote(cfg, itmMUser, map, line_char);

            return result;
        }
        /// <summary>
        /// 更新與會者籤號
        /// </summary>
        private void FixMUserSignNo(TConfig cfg)
        {
            string sql = @"
                UPDATE t1 SET
        	        t1.in_sign_no = t2.in_sign_no
        	        , t1.in_section_no = t2.in_section_no
                FROM
        	        IN_MEETING_USER t1 WITH(NOLOCK)
                INNER JOIN
        	        IN_MEETING_PTEAM t2 WITH(NOLOCK)
        	        ON t2.in_meeting = t1.source_id
        	        AND t2.in_team_key = ISNULL(t1.in_l1, '') 
        		        + '-' + ISNULL(t1.in_l2, '') 
        		        + '-' + ISNULL(t1.in_l3, '') 
        		        + '-' + ISNULL(t1.in_index, '')
        		        + '-' + ISNULL(t1.in_creator_sno, '')
                WHERE
        	        t1.source_id = '{#meeting_id}'
        	        AND t2.source_id = '{#program_id}'
        	        AND ISNULL(t2.in_sign_no, '') <> ''
        	        AND ISNULL(t2.in_type, '') IN ('', 'p')
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#program_id}", cfg.program_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item itmSQL = cfg.inn.applySQL(sql);

            if (itmSQL.isError())
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "更新與會者籤號與量級序號發生錯誤 _# sql: " + sql);
            }
        }

        //分組統計
        private void AppendGroupingStatistics(TConfig cfg, Item itmSurveyLevel, Item itmReturn)
        {
            string sql = "";

            string meeting_id = itmSurveyLevel.getProperty("meeting_id", "");
            string group_condition = itmSurveyLevel.getProperty("group_condition", "");

            string distinct_cols = itmSurveyLevel.getProperty("distinct_cols", "");

            //計算各分組的人數( in_l1)

            sql = @"SELECT DISTINCT in_l1 FROM IN_MEETING_USER WITH(NOLOCK) 
            WHERE source_id = '{#meeting_id}' AND in_note_state = 'official' {#group_condition} 
            AND in_l1 ! = N'隊職員'";

            sql = sql.Replace("{#meeting_id}", meeting_id)
                .Replace("{#group_condition}", group_condition);


            Item l1_list = cfg.inn.applySQL(sql);

            Item l1_all = cfg.inn.newItem("inn_l1_group");
            l1_all.setProperty("inn_l1_name", "請選擇");
            l1_all.setProperty("inn_count", "");
            itmReturn.addRelationship(l1_all);

            //暫將JOIN IN_MEETING_PAY註解    
            // sql = @"SELECT DISTINCT {#distinct_cols} FROM in_meeting_user AS T1 WITH(NOLOCK) 
            //         JOIN IN_MEETING_PAY AS T2 WITH(NOLOCK) 
            //         ON T1.SOURCE_ID = T2.IN_MEETING AND T1.IN_PAYNUMBER = T2.ITEM_NUMBER
            //         WHERE T1.source_id = '{#meeting_id}' AND T1.in_note_state = 'official' {#group_condition} AND T1.in_l1 = N'{#in_l1}'";

            sql = @"SELECT DISTINCT {#distinct_cols} FROM IN_MEETING_USER AS T1 WITH(NOLOCK) 
            WHERE T1.source_id = '{#meeting_id}' AND T1.in_note_state = 'official' {#group_condition} AND T1.in_l1 = N'{#in_l1}'";


            int count = l1_list.getItemCount();
            int total = 0;

            for (int i = 0; i < count; i++)
            {
                Item item = l1_list.getItemByIndex(i);
                string in_l1 = item.getProperty("in_l1", "");

                string temp_sql = sql.Replace("{#meeting_id}", meeting_id)
                    .Replace("{#group_condition}", group_condition)
                    .Replace("{#in_l1}", in_l1)
                    .Replace("{#distinct_cols}", distinct_cols);

                //CCO.Utilities.WriteDebug(strMethodName, "sql: " + temp_sql);

                Item in_l1_result = cfg.inn.applySQL(temp_sql);
                int in_l1_count = in_l1_result.getItemCount();

                Item l1_option = cfg.inn.newItem("inn_l1_group");
                l1_option.setProperty("inn_l1_name", in_l1);
                l1_option.setProperty("inn_count", in_l1_count.ToString());
                itmReturn.addRelationship(l1_option);

                //累加各類組數
                total = total + in_l1_count;
            }

            l1_all.setProperty("inn_count", total.ToString());

            string l1_hide = itmSurveyLevel.getProperty("l1_hide", "");
            string l2_hide = itmSurveyLevel.getProperty("l2_hide", "");
            string l3_hide = itmSurveyLevel.getProperty("l3_hide", "");

            itmReturn.setProperty("l1_hide", l1_hide);
            itmReturn.setProperty("l2_hide", l2_hide);
            itmReturn.setProperty("l3_hide", l3_hide);

            if (l2_hide == "")
            {
                string l2_values = GetSurveyOptionContents(cfg, "in_l2", group_condition);
                itmReturn.setProperty("l2_values", l2_values);
            }

            if (l3_hide == "")
            {
                string l3_values = GetSurveyOptionContents(cfg, "in_l3", group_condition);
                itmReturn.setProperty("l3_values", l3_values);
            }
        }

        private Item GetSurveyLevels(TConfig cfg)
        {
            Item itmResult = cfg.inn.newItem();

            string sql = @"
                SELECT 
                    t1.*
                FROM 
                    IN_SURVEY t1 WITH(NOLOCK)
                INNER JOIN 
                    IN_MEETING_SURVEYS t2 WITH(NOLOCK) ON t2.related_id = t1.id 
                WHERE 
                    t2.source_id = N'{#meeting_id}' 
                    AND t1.in_client_remove = N'0'
                    AND t1.in_property IN (N'in_l1', N'in_l2', N'in_l3')
                ORDER BY 
                    t2.sort_order
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            //CCO.Utilities.WriteDebug(strMethodName, "sql_levels: " + sql);

            Item items = cfg.inn.applySQL(sql);

            if (items.isError() || items.getItemCount() <= 0)
            {
                return itmResult;
            }

            int count = items.getItemCount();

            string level_fields = "";
            string number_key_level = "";
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_property = item.getProperty("in_property", "").Trim();
                string in_is_nokey = item.getProperty("in_is_nokey", "").Trim();

                if (level_fields != "")
                {
                    level_fields += ",";
                }
                level_fields += in_property;

                if (in_is_nokey == "1")
                {
                    if (number_key_level != "")
                    {
                        number_key_level += ",";
                    }
                    number_key_level += in_property;
                }
            }

            if (level_fields == "")
            {
                level_fields = "in_l1";
            }

            if (number_key_level == "")
            {
                number_key_level = "in_l1";
            }

            string default_cols = "";
            string user_order_by = "";
            string distinct_cols = "";
            string l1_hide = "";
            string l2_hide = "";
            string l3_hide = "";

            if (level_fields.Contains("in_l3"))
            {
                default_cols = "in_l1, in_l2, in_l3";
            }
            else if (level_fields.Contains("in_l2"))
            {
                default_cols = "in_l1, in_l2";
                l3_hide = "item_show_0";

            }
            else if (level_fields.Contains("in_l1"))
            {
                default_cols = "in_l1";
                l2_hide = "item_show_0";
                l3_hide = "item_show_0";
            }
            else
            {
                l1_hide = "item_show_0";
                l2_hide = "item_show_0";
                l3_hide = "item_show_0";
            }

            //定序處理
            if (number_key_level.Contains("in_l3"))
            {
                distinct_cols = "in_l1, in_l2, in_l3, in_index";
                user_order_by = "in_group, in_short_org, in_l1, in_l2, in_l3, in_index, in_name";
            }
            else if (number_key_level.Contains("in_l2"))
            {
                distinct_cols = "in_l1, in_l2, in_index";
                user_order_by = "in_group, in_short_org, in_l1, in_l2, in_index, in_name";
            }
            else if (number_key_level.Contains("in_l1"))
            {
                distinct_cols = "in_l1, in_index";
                user_order_by = "in_group, in_short_org, in_l1, in_index, in_name";
            }

            itmResult.setProperty("meeting_id", cfg.meeting_id);
            //定序欄位
            itmResult.setProperty("number_key_level", number_key_level);
            //預設欄位組合
            itmResult.setProperty("default_cols", default_cols);
            //分組統計欄位組合
            itmResult.setProperty("distinct_cols", distinct_cols);
            //定序欄位組合
            itmResult.setProperty("user_order_by", user_order_by);
            //第一階選單是否隱藏
            itmResult.setProperty("l1_hide", l1_hide);
            //第二階選單是否隱藏
            itmResult.setProperty("l2_hide", l2_hide);
            //第三階選單是否隱藏
            itmResult.setProperty("l3_hide", l3_hide);

            return itmResult;
        }

        private string GetSurveyOptionContents(TConfig cfg, string in_property, string group_condition)
        {
            string sql = "";

            if (in_property == "in_l2")
            {
                sql = @"
            SELECT t3.in_l1 AS 'in_filter', t3.in_l2 AS 'in_value', count(t3.team_index) AS 'in_count' FROM
            (
                SELECT 
                    DISTINCT T1.in_l1, T1.in_l2, T1.in_l3, (T1.in_l1 + '-' + ISNULL(T1.in_l2, '') + '-' + ISNULL(T1.in_l3, '') + '-' + T1.in_index) AS 'team_index' , T4.IN_L2_SORT
                FROM 
                    IN_MEETING_USER AS T1 WITH(NOLOCK)
                LEFT JOIN IN_MEETING_PROGRAM AS T4 WITH(NOLOCK) 
            	ON T1.SOURCE_ID = T4.IN_MEETING and T1.in_l1 = T4.in_l1 and T1.in_l2 = T4.in_l2 and T1.in_l3 = T4.in_l3
                WHERE 
                    T1.source_id = '{#meeting_id}'
                    {#group_condition}
            ) t3
                GROUP BY t3.in_l1, t3.in_l2, t3.IN_L2_SORT
                ORDER BY t3.in_l2_sort";
            }
            else if (in_property == "in_l3")
            {
                sql = @"
            SELECT t3.in_l1 AS 'in_l1', t3.in_l2 AS 'in_filter', ISNULL(t3.in_l3, '') AS 'in_value', count(t3.team_index) AS 'in_count' FROM
            (
                SELECT 
                    DISTINCT T1.in_l1, T1.in_l2, T1.in_l3, (T1.in_l1 + '-' + ISNULL(T1.in_l2, '') + '-' + ISNULL(T1.in_l3, '') + '-' + T1.in_index) AS 'team_index' , T4.IN_L3_SORT
                FROM 
                    IN_MEETING_USER AS T1 WITH(NOLOCK)
                LEFT JOIN IN_MEETING_PROGRAM AS T4 WITH(NOLOCK) 
            	ON T1.source_id = T4.in_meeting and T1.in_l1 = T4.in_l1 and T1.in_l2 = T4.in_l2 and T1.in_l3 = T4.in_l3
                WHERE 
                    T1.source_id = '{#meeting_id}'
                    {#group_condition}
            ) t3
                GROUP BY t3.in_l1, t3.in_l2, t3.in_l3 , t3.in_l3_sort
                ORDER BY t3.in_l3_sort";

            }

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#group_condition}", group_condition);

            Item items = cfg.inn.applySQL(sql);

            if (items.isError() || items.getItemCount() <= 0)
            {
                return "";
            }

            return ConvertSurveyOptionContents(items);

        }

        private string GetWeightStatus(Item item)
        {
            string muid = item.getProperty("id", "");
            string in_sno = item.getProperty("in_sno", "");
            string in_weight_status = item.getProperty("in_weight_status", "");

            var menu = GetStatusMenu();

            switch (in_weight_status)
            {
                case "on":
                    menu.On.IsSelected = true;
                    break;

                case "leave":
                    menu.Leave.IsSelected = true;
                    break;

                case "off":
                    menu.Off.IsSelected = true;
                    break;

                case "dq":
                    menu.DQ.IsSelected = true;
                    break;

                case "king":
                    menu.King.IsSelected = true;
                    break;

                default:
                    break;
            }

            StringBuilder builder = new StringBuilder();

            builder.Append("<select id='w_status_" + in_sno + "' class='form-control weight-status' data-id='" + muid + "' onchange='WeightStatus_Change(this)'>");
            builder.Append("<option value=''>--</option>");
            builder.Append(GetStatusOption(menu.Leave));
            builder.Append(GetStatusOption(menu.Off));
            builder.Append(GetStatusOption(menu.DQ));
            builder.Append(GetStatusOption(menu.On));
            builder.Append(GetStatusOption(menu.King));
            builder.Append("</select>");

            return builder.ToString();
        }

        string GetStatusOption(TStatus status)
        {
            if (status.IsSelected)
            {
                return "<option value='" + status.Value + "' " + status.Css + " selected >" + status.Label+"</option>";
            }
            else
            {
                return "<option value='" + status.Value + "' " + status.Css + " >" + status.Label+"</option>";
            }
        }

        private string GetWeightStatus_OLD(Item item)
        {
            string muid = item.getProperty("id", "");
            string in_sno = item.getProperty("in_sno", "");
            string in_weight_status = item.getProperty("in_weight_status", "");


            string options = "";
            switch (in_weight_status)
            {
                case "on":
                    options = "<option value=''>--</option>"
                        + "<option value='leave' style='background-color: yellow'>請假</option>"
                        + "<option value='off'   style='background-color: orange'>DNS</option>"
                        + "<option value='dq'    style='background-color: #F8D7DA'>DQ</option>"
                        + "<option value='on'    style='background-color: white'  selected>過磅合格</option>";
                    break;

                case "leave":
                    options = "<option value=''>--</option>"
                        + "<option value='leave' style='background-color: yellow' selected>請假</option>"
                        + "<option value='off'   style='background-color: orange'>DNS</option>"
                        + "<option value='dq'    style='background-color: #F8D7DA'>DQ</option>"
                        + "<option value='on'    style='background-color: white'  >過磅合格</option>";
                    break;

                case "off":
                    options = "<option value=''>--</option>"
                        + "<option value='leave' style='background-color: yellow'>請假</option>"
                        + "<option value='off'   style='background-color: orange' selected>DNS</option>"
                        + "<option value='dq'    style='background-color: #F8D7DA'>DQ</option>"
                        + "<option value='on'    style='background-color: white'  >過磅合格</option>";
                    break;

                case "dq":
                    options = "<option value=''>--</option>"
                        + "<option value='leave' style='background-color: yellow'>請假</option>"
                        + "<option value='off'   style='background-color: orange'>DNS</option>"
                        + "<option value='dq'    style='background-color: #F8D7DA' selected>DQ</option>"
                        + "<option value='on'    style='background-color: white'  >過磅合格</option>";
                    break;

                default:
                    options = "<option value=''>--</option>"
                        + "<option value='leave' style='background-color: yellow' >請假</option>"
                        + "<option value='off'   style='background-color: orange' >DNS</option>"
                        + "<option value='dq'    style='background-color: #F8D7DA'>DQ</option>"
                        + "<option value='on'    style='background-color: white'  >過磅合格</option>";
                    break;
            }

            StringBuilder builder = new StringBuilder();
            builder.Append("<select id='w_status_" + in_sno + "' class='form-control weight-status' data-id='" + muid + "' onchange='WeightStatus_Change(this)'>");
            builder.Append(options);
            builder.Append("</select>");

            return builder.ToString();
        }

        private Item GetProgram(TConfig cfg)
        {
            if (cfg.in_l3 == "")
            {
                return cfg.inn.newItem();
            }

            string sql = "SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK)"
                + " WHERE in_meeting = '" + cfg.meeting_id + "'"
                + " AND in_l1 = N'" + cfg.in_l1 + "'"
                + " AND in_l2 = N'" + cfg.in_l2 + "'"
                + " AND in_l3 = N'" + cfg.in_l3 + "'"
                + "";

            Item itmProgram = cfg.inn.applySQL(sql);

            if (itmProgram.isError() || itmProgram.getResult() == "")
            {
                return cfg.inn.newItem();
            }
            else
            {
                return itmProgram;
            }
        }

        private Item GetMUsers(TConfig cfg, string in_l1, string in_l2, string in_l3)
        {
            string sql = @"
                SELECT 
	                t1.id
	                , t1.in_name
	                , t1.in_sno
	                , t1.in_short_org
	                , t1.in_team
	                , t1.in_sign_no
	                , t1.in_weight
	                , t1.in_weight_result
	                , t1.in_weight_status
	                , IIF(t1.in_rollcall_time > 0, '1', '0') AS 'rollcall'
	                , t1.in_section_no
	                , t1.in_show_no
	                , t1.in_section_name
	                , t4.in_extend_value2
                FROM 
	                IN_MEETING_USER t1 WITH(NOLOCK)
                JOIN 
	                IN_MEETING_SURVEYS t3 WITH(NOLOCK)
	                ON t3.source_id = t1.source_id
                JOIN 
	                IN_SURVEY_OPTION t4 WITH(NOLOCK)
	                ON t4.source_id = t3.related_id 
	                AND t4.in_grand_filter + t4.in_filter + t4.in_value = t1.in_l1 + t1.in_l2 + t1.in_l3
                WHERE 
	                t1.source_id = '{#meeting_id}' 
	                AND t1.in_l1 = N'{#in_l1}' 
	                AND t1.in_l2 = N'{#in_l2}' 
	                AND t1.in_l3 = N'{#in_l3}' 
                ORDER BY 
	                t1.in_stuff_b1
	                , t1.in_team
	                , t1.in_sno
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                    .Replace("{#in_l1}", in_l1)
                    .Replace("{#in_l2}", in_l2)
                    .Replace("{#in_l3}", in_l3);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        private Dictionary<string, TPlayer> GetWeightRecords(TConfig cfg, string in_l1, string in_l2, string in_l3)
        {
            string pg_key = string.Join("-", new List<string> { in_l1, in_l2, in_l3 });

            Dictionary<string, TPlayer> map = new Dictionary<string, TPlayer>();

            string sql = @"
		        SELECT
			        t1.id
			        , t1.in_current_org
			        , t1.in_stuff_b1
			        , t1.in_short_org
			        , t1.in_team
			        , t1.in_l1
			        , t1.in_l2
			        , t1.in_l3
			        , t1.in_sno
			        , t1.in_name
			        , t1.in_weight_time
			        , t1.in_weight
			        , t3.in_short_name AS 'pg_short_name'
		        FROM
			        IN_MEETING_USER t1
		        INNER JOIN
		        (
			        SELECT 
				        source_id
				        , in_sno
			        FROM 
				        IN_MEETING_USER WITH(NOLOCK)
			        WHERE 
				        source_id = '{#meeting_id}' 
				        AND in_l1 = N'{#in_l1}' 
				        AND in_l2 = N'{#in_l2}' 
				        AND in_l3 = N'{#in_l3}' 
		        ) t2 
		            ON t2.source_id = t1.source_id
		            AND t2.in_sno = t1.in_sno
		        LEFT OUTER JOIN
		            IN_MEETING_PROGRAM t3 WITH(NOLOCK)
		            ON t3.in_meeting = t1.source_id
		            AND t3.in_name = ISNULL(t1.in_l1, '') + '-' + ISNULL(t1.in_l2, '') + '-' + ISNULL(t1.in_l3, '')
		        WHERE
			        t1.source_id = '{#meeting_id}' 
			        AND ISNULL(t1.in_l1, '') + '-' + ISNULL(t1.in_l2, '') + '-' + ISNULL(t1.in_l3, '') <> N'{#pg_key}'
			        AND ISNULL(t1.in_weight_time, '') <> ''
			    ORDER BY
			        t1.in_stuff_b1
			        , t1.in_team
			        , t1.in_sno
			        , t1.in_weight_time
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_l1}", in_l1)
                .Replace("{#in_l2}", in_l2)
                .Replace("{#in_l3}", in_l3)
                .Replace("{#pg_key}", pg_key);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_sno = item.getProperty("in_sno", "").ToUpper();
                TPlayer player = null;
                if (map.ContainsKey(in_sno))
                {
                    player = map[in_sno];
                }
                else
                {
                    player = new TPlayer
                    {
                        in_sno = in_sno,
                        in_name = item.getProperty("in_name", ""),
                        WeightRecords = new List<TWeight>(),
                        Value = item,
                    };
                    map.Add(in_sno, player);
                }

                TWeight weight = new TWeight
                {
                    pg_short_name = item.getProperty("pg_short_name", ""),
                    in_weight = item.getProperty("in_weight", ""),
                    in_weight_time = item.getProperty("in_weight_time", ""),
                };

                weight.weight_time = GetDateTime(weight.in_weight_time);
                weight.show_weight_time = weight.weight_time.AddHours(8).ToString("MM/dd");

                player.WeightRecords.Add(weight);
            }

            return map;
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public Innosoft.InnovatorHelper InnH { get; set; }

            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }

            public string meeting_id { get; set; }
            public string program_id { get; set; }
            public string in_date { get; set; }
            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }
            public string mode { get; set; }
            public string scene { get; set; }

            public Item itmMeeting { get; set; }
            public Item itmProgram { get; set; }

            public string mt_title { get; set; }
            public string pg_sign_time { get; set; }
            public bool has_sign_no { get; set; }
            public bool has_fight_day { get; set; }
        }

        private class TExport
        {
            public string template_Path { get; set; }
            public string export_Path { get; set; }
        }

        private class TProgram
        {
            public string no { get; set; }
            public string name { get; set; }
            public string name2 { get; set; }
            public string sheet_name { get; set; }
            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }
        }

        private class TPlayer
        {
            public string in_sno { get; set; }
            public string in_name { get; set; }
            public List<TWeight> WeightRecords { get; set; }
            public Item Value { get; set; }
        }

        private class TWeight
        {
            public string pg_short_name { get; set; }
            public string in_weight_time { get; set; }
            public string in_weight { get; set; }
            public DateTime weight_time { get; set; }
            public string show_weight_time { get; set; }
            public Item Value { get; set; }
        }

        private class TField
        {
            public string title { get; set; }
            public string property { get; set; }
            public string fontName { get; set; }
            public double fontSize { get; set; }
            public bool fontBold { get; set; }
            public bool IsSectionNo { get; set; }
            public string RS { get; set; }
            public string RE { get; set; }
            public bool is_num { get; set; }
        }

        private TStatusMenu GetStatusMenu()
        {
            var result = new TStatusMenu
            {
                Leave = new TStatus { Label = "請假", Value = "leave", Message = "(請假)", Css = "style='background-color: yellow'", NeedRemove = true },
                Off = new TStatus { Label = "DNS", Value = "off", Message = "(DNS)", Css = "style='background-color: orange'", NeedRemove = false, Desc = "未到(Did Not Start)" },
                DQ = new TStatus { Label = "DQ", Value = "dq", Message = "(DQ)", Css = "style='background-color: #F8D7DA'", NeedRemove = false, Desc = "" },
                On = new TStatus { Label = "過磅合格", Value = "on", Message = "", Css = "style='background-color: white'", NeedRemove = false },
                King = new TStatus { Label = "盟主", Value = "king", Message = "(king)", Css = "style='background-color: white'", NeedRemove = true },
            };
            return result;
        }

        private class TStatusMenu
        {
            /// <summary>
            /// 過磅合格
            /// </summary>
            public TStatus On { get; set; }
            /// <summary>
            /// 未到
            /// </summary>
            public TStatus Off { get; set; }
            /// <summary>
            /// 請假
            /// </summary>
            public TStatus Leave { get; set; }
            /// <summary>
            /// 超磅
            /// </summary>
            public TStatus DQ { get; set; }
            /// <summary>
            /// 盟主
            /// </summary>
            public TStatus King { get; set; }
        }

        private class TStatus
        { 
            public string Label { get; set; }
            public string Value { get; set; }
            public string Css { get; set; }
            public string Desc { get; set; }
            public bool IsSelected { get; set; }
            public bool NeedRemove { get; set; }
            public string Message { get; set; }
        }


        /// <summary>
        /// 將問項明細轉換為字典內容字串
        /// </summary>
        private string ConvertSurveyOptionContents(Item items)
        {
            Dictionary<string, List<string>> dictionary = new Dictionary<string, List<string>>();

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_filter = item.getProperty("in_filter", "");
                string in_value = item.getProperty("in_value", "");
                string in_count = item.getProperty("in_count", "");

                List<string> list = null;
                if (dictionary.ContainsKey(in_filter))
                {
                    list = dictionary[in_filter];
                }
                else
                {
                    list = new List<string>();
                    dictionary.Add(in_filter, list);
                }

                if (!list.Contains(in_value))
                {
                    list.Add(in_value + "_#" + in_count);
                }
            }

            List<string> list2 = new List<string>();
            foreach (KeyValuePair<string, List<string>> kv in dictionary)
            {
                string key = kv.Key;
                string values = string.Join("@", kv.Value.Where(x => !string.IsNullOrEmpty(x)));
                string row = key + ":" + values;
                list2.Add(row);
            }

            return string.Join("@@", list2.Where(x => !string.IsNullOrEmpty(x)));
        }

        private DateTime GetDateTime(string value)
        {
            if (value == "") return DateTime.MinValue;
            DateTime result = DateTime.MinValue;
            DateTime.TryParse(value, out result);
            return result;
        }

        private int GetIntVal(string value)
        {
            if (value == "" || value == "0") return 0;

            int result = 0;
            int.TryParse(value, out result);
            return result;
        }

        private double GetDblVal(string value)
        {
            if (value == "" || value == "0") return 0;

            double result = 0;
            double.TryParse(value, out result);
            return result;
        }
    }
}