﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;
using System.Net;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class In_Meeting_Weight_Xls : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的:匯出對戰表
                日期: 
                    - 2021-11-29 DQ 看不到調整 (lina)
                    - 2021-10-05 8強敗部調整 (lina)
                    - 2021-03-09 創建 (Alan)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]In_Meeting_Competition_Export";

            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            string aml = "";
            string sql = "";
            string strError = "";

            Item itmR = this;

            Item itmPermit = inn.applyMethod("In_CheckIdentity", "<method>" + strMethodName + "</method><code>" + "ALL" + "</code>");
            bool isMeetingAdmin = itmPermit.getProperty("isMeetingAdmin", "") == "1";
            bool isMeetingLeader = itmPermit.getProperty("isMeetingLeader", "") == "1";
            bool can_view = isMeetingAdmin || isMeetingLeader;
            if (!can_view)
            {
                itmR.setProperty("error_message", "您無權限瀏覽此頁面，請重新登入");
                return itmR;
            }

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                RequestState = RequestState,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                inn = inn,

                meeting_id = itmR.getProperty("meeting_id", ""),
                program_id = itmR.getProperty("program_id", ""),
                mode = itmR.getProperty("mode", ""),
                in_date = itmR.getProperty("in_date", ""),
                in_l1 = itmR.getProperty("in_l1", ""),
                in_l2 = itmR.getProperty("in_l2", ""),
                in_l3 = itmR.getProperty("in_l3", ""),
                SportService = new InnSport.Core.Services.Impl.JudoSportService(),
            };

            itmR.setProperty("id", cfg.meeting_id);

            sql = @"SELECT in_title, in_battle_repechage FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'";
            cfg.itmMeeting = inn.applySQL(sql);
            if (cfg.itmMeeting.isError() || cfg.itmMeeting.getResult() == "")
            {
                throw new Exception("找無該場賽事資料!");
            }

            cfg.mt_title = cfg.itmMeeting.getProperty("in_title", "");
            cfg.mt_battle_repechage = cfg.itmMeeting.getProperty("in_battle_repechage", "");
            itmR.setProperty("in_title", cfg.mt_title);

            List<TProgram> pg_list = MapProgram(cfg);
            if (pg_list.Count == 0)
            {
                throw new Exception("查無組別資料");
            }

            string target_name = cfg.mt_battle_repechage == "quarterfinals"
                ? "competition_path2"
                : "competition_path";

            Item itmXls = inn.applyMethod("In_Meeting_Excel_Path", "<in_name>" + target_name + "</in_name>");
            string Export_Path = itmXls.getProperty("export_path", "");
            string Template_Path = itmXls.getProperty("template_path", "");

            Spire.Xls.Workbook workbook = new Spire.Xls.Workbook();
            workbook.LoadFromFile(Template_Path);

            for (int i = 0; i < pg_list.Count; i++)
            {
                TProgram pg = pg_list[i];
                ExportXls(cfg, pg, workbook);
            }

            //移除樣板 Sheet
            for (int i = 7; i >= 0; i--)
            {
                workbook.Worksheets.RemoveAt(i);
            }


            string xlsName = "";

            if (pg_list.Count == 1)
            {
                TProgram pg = pg_list[0];

                xlsName = pg.in_fight_day
                    + "_" + pg.in_site_mat
                    + "_" + pg.in_name2
                    + "_" + DateTime.Now.ToString("MMdd_HHmmss");
            }
            else
            {

                xlsName = cfg.mt_title
                    + "_" + cfg.in_date
                    + "_" + "對戰表"
                    + "_" + DateTime.Now.ToString("MMdd_HHmmss");
            }

            string is_batch = itmR.getProperty("is_batch", "");
            string batch_export = itmR.getProperty("batch_export", "");
            if (is_batch == "1")
            {
                Export_Path = batch_export + "\\";
            }

            string xlsFile = Export_Path + xlsName + ".xlsx";
            workbook.SaveToFile(xlsFile);

            string xls_url = xlsName + ".xlsx";
            itmR.setProperty("xls_name", xls_url);

            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

            return itmR;
        }

        private void ExportXls(TConfig cfg, TProgram pg, Spire.Xls.Workbook workbook)
        {
            Spire.Xls.Worksheet sheetTemplate = workbook.Worksheets[pg.sheetName];

            Spire.Xls.Worksheet sheet = workbook.CreateEmptySheet();
            sheet.CopyFrom(sheetTemplate);
            sheet.Name = pg.in_short_name;

            //取得該組對戰清單     
            Item itmPevents = GetMPEvents(cfg, pg);

            if (itmPevents.isError() || itmPevents.getItemCount() <= 0)
            {
                //throw new Exception("查無可匯出對戰表資料");
                return;
            }

            Spire.Xls.CellRange[] ranges = { };

            string sign_time = "抽籤日期：" + pg.signTime;
            string sign_count = "參賽人數：" + pg.in_team_count;

            Spire.Xls.CellRange[] mt_ranges = sheet.FindAllString("#mt_title", true, true);
            foreach (var rg in mt_ranges)
            {
                rg.Text = cfg.mt_title + "(" + pg.in_l1 + ")";
            }

            Spire.Xls.CellRange[] pgt_ranges = sheet.FindAllString("#pg_title", true, true);
            for (int i = 0; i < pgt_ranges.Length; i++)
            {
                var rg = pgt_ranges[i];
                rg.Text = GetProgramName(pg.in_name2, pgt_ranges.Length, i);
            }

            Spire.Xls.CellRange[] pgc_ranges = sheet.FindAllString("#pg_count", true, true);
            foreach (var rg in pgc_ranges)
            {
                rg.Text = sign_count;
            }

            Spire.Xls.CellRange[] pgm_ranges = sheet.FindAllString("#pg_time", true, true);
            foreach (var rg in pgm_ranges)
            {
                rg.Text = sign_time;
            }

            // if (p_in_l1 == "團體組")
            // {
            //     in_site_mat = "";
            // }

            Spire.Xls.CellRange[] mat_ranges = sheet.FindAllString("#mat", true, true);
            foreach (var rg in mat_ranges)
            {
                rg.Text = pg.in_site_mat;
            }

            Spire.Xls.CellRange[] mat_fdays = sheet.FindAllString("#in_fight_day", true, true);
            foreach (var rg in mat_fdays)
            {
                rg.Text = "比賽日期: " + pg.in_fight_day;
            }

            switch (pg.in_battle_type)
            {
                //四柱復活賽 挑戰賽
                case "TopTwo":
                case "JudoTopFour":
                case "Challenge":
                    xlsForChallenge(cfg, pg, sheet, itmPevents);
                    break;

                //單循環賽
                case "SingleRoundRobin":
                    xlsForSingleRoundRobin(cfg, pg, sheet, itmPevents);
                    break;
            }
        }

        private string GetProgramName(string name, int cnt, int idx)
        {
            if (cnt == 3)
            {
                if (idx == 0)
                {
                    return name + " A面";
                }
                else if (idx == 1)
                {
                    return name + " B面";
                }
                else
                {
                    return name;
                }
            }
            return name;
        }


        private class TPlayer
        {
            public string in_tree_id { get; set; }
            public string in_tree_no { get; set; }
            public string in_section_no { get; set; }
            public string map_short_org { get; set; }
            public string in_name { get; set; }
            public string in_sign_foot { get; set; }
            public string in_sign_bypass { get; set; }

            public string in_weight_message { get; set; }

            public bool isPlayer { get; set; }
            public bool isByPass { get; set; }

            public string n1 { get; set; }
            public string n2 { get; set; }
            public string n3 { get; set; }
            public string showNo { get; set; }
            public string showName { get; set; }
            public int sectionNo { get; set; }
        }

        private TPlayer MapPlayer(TConfig cfg, TProgram program, Item itmPevent, bool need_posi = false)
        {
            TPlayer result = new TPlayer
            {
                in_tree_id = itmPevent.getProperty("in_tree_id", ""),
                in_tree_no = itmPevent.getProperty("in_tree_no", ""),
                map_short_org = itmPevent.getProperty("map_short_org", ""),
                in_section_no = itmPevent.getProperty("in_section_no", ""),
                in_name = itmPevent.getProperty("in_name", ""),
                in_weight_message = itmPevent.getProperty("in_weight_message", ""),
                in_sign_foot = itmPevent.getProperty("in_sign_foot", "").PadLeft(2, '0'),
                in_sign_bypass = itmPevent.getProperty("in_sign_bypass", ""),
            };

            result.showNo = result.in_section_no.PadLeft(2, '0');

            if (result.in_tree_no == "0")
            {
                result.in_tree_no = "";
            }

            result.isPlayer = result.map_short_org != "" || result.in_name != "";
            result.isByPass = result.in_sign_bypass == "1";

            if (result.in_name.Contains("("))
            {
                //姓名(實屬單位)												
                string[] arr = result.in_name.Split(new char[] { '(' }, StringSplitOptions.RemoveEmptyEntries);
                result.n1 = arr[0];
                result.n2 = "(" + arr[1];
            }
            else
            {
                result.n1 = result.in_name;
                result.n2 = "";
            }

            if (result.in_weight_message != "")
            {
                result.n3 = "(DQ)";
            }
            else
            {
                result.n3 = "";
            }

            if (need_posi)
            {
                result.sectionNo = GetIntVal(result.in_section_no);

                var posi = FindPosi(cfg, program, result);

                switch (posi)
                {
                    case CellPosi.NotFind:
                        result.showName = result.in_name;
                        break;
                    case CellPosi.Left:
                        result.showName = result.n2 + result.n1 + result.n3;
                        break;
                    case CellPosi.Right:
                        result.showName = result.n3 + result.n1 + result.n2;
                        break;
                }
            }
            else
            {
                result.showName = result.in_name;
            }

            return result;
        }

        //計算選手在左側 OR 右側
        private CellPosi FindPosi(TConfig cfg, TProgram program, TPlayer player)
        {
            if (player.sectionNo <= 0)
            {
                return CellPosi.NotFind;
            }

            int idx = program.JudoNumList.FindIndex(x => x == player.sectionNo);
            if (idx < 0)
            {
                return CellPosi.NotFind;
            }

            //計算選手在左側 OR 右側
            if (program.roundCode == 128)
            {
                int a_left_idx_s = 0;
                int a_left_idx_e = 31;

                int b_left_idx_s = 64;
                int b_left_idx_e = 95;

                if (idx >= a_left_idx_s && idx <= a_left_idx_e)
                {
                    return CellPosi.Left;
                }
                else if (idx >= b_left_idx_s && idx <= b_left_idx_e)
                {
                    return CellPosi.Left;
                }
                else
                {
                    return CellPosi.Right;
                }
            }
            else
            {
                int left_idx_s = 0;
                int left_idx_e = (program.roundCode / 2) - 1;

                if (idx >= left_idx_s && idx <= left_idx_e)
                {
                    return CellPosi.Left;
                }
                else
                {
                    return CellPosi.Right;
                }
            }
        }

        private enum CellPosi
        {
            NotFind = 0,
            Left = 1,
            Right = 2,
        }

        //四柱復活賽 挑戰賽
        private int xlsForChallenge(TConfig cfg, TProgram program, Spire.Xls.Worksheet sheet, Item itmPevents)
        {
            Spire.Xls.CellRange[] ranges = { };

            int play_count = 0;
            for (int i = 0; i < itmPevents.getItemCount(); i++)
            {
                Item itmPevent = itmPevents.getItemByIndex(i);
                TPlayer player = MapPlayer(cfg, program, itmPevent, need_posi: true);

                ranges = sheet.FindAllString("#" + player.in_sign_foot + player.in_tree_id, true, true);
                foreach (Spire.Xls.CellRange range in ranges)
                {
                    range.Text = player.in_section_no;
                }

                ranges = sheet.FindAllString("#" + player.in_sign_foot + "C" + player.in_tree_id, true, true);
                foreach (Spire.Xls.CellRange range in ranges)
                {
                    if (player.isPlayer)
                    {
                        range.Text = player.map_short_org;
                    }
                    else if (player.isByPass)
                    {
                        range.Text = "-bye-";
                    }
                    else
                    {
                        range.Text = "";
                    }
                }

                ranges = sheet.FindAllString("#" + player.in_sign_foot + "P" + player.in_tree_id, true, true);
                foreach (Spire.Xls.CellRange range in ranges)
                {
                    if (player.isPlayer)
                    {
                        range.Text = player.showName;
                    }
                    else if (player.isByPass)
                    {
                        range.Text = "-bye-";
                    }
                    else
                    {
                        range.Text = "";
                    }
                }

                ranges = sheet.FindAllString("#" + player.in_tree_id, true, true);
                foreach (Spire.Xls.CellRange range in ranges)
                {
                    range.Text = player.in_tree_no;
                }
            }

            ranges = sheet.FindAllString("#", true, true);
            foreach (Spire.Xls.CellRange range in ranges)
            {
                range.Text = "";
            }

            Item itmRanks = GetRanks(cfg, program);

            if (itmRanks.getItemCount() > 0)
            {
                string name_col = "AT";
                if (program.roundCode == 128)
                {
                    name_col = "AX";
                }

                for (int i = 0; i < itmRanks.getItemCount(); i++)
                {
                    Item itmRank = itmRanks.getItemByIndex(i);
                    sheet.Range["W" + (7 + i)].Text = itmRank.getProperty("in_final_rank", "");
                    sheet.Range["AC" + (7 + i)].Text = itmRank.getProperty("map_short_org", "");
                    sheet.Range[name_col + (7 + i)].Text = itmRank.getProperty("in_name", "");
                }
            }

            return play_count;
        }

        //單循環賽
        private int xlsForSingleRoundRobin(TConfig cfg, TProgram pg, Spire.Xls.Worksheet sheet, Item itmPevents)
        {
            Spire.Xls.CellRange[] ranges = { };

            int play_count = 0;
            string in_tree_id = "";
            string in_tree_no = "";

            for (int i = 0; i < itmPevents.getItemCount(); i++)
            {
                Item itmPevent = itmPevents.getItemByIndex(i);
                if (in_tree_id != itmPevent.getProperty("in_tree_id", ""))
                {
                    in_tree_no += itmPevent.getProperty("in_tree_no", "") + ",";
                }

                in_tree_id = itmPevent.getProperty("in_tree_id", "");
                string in_section_no = itmPevent.getProperty("in_section_no", "").PadLeft(2, '0');

                string in_name = itmPevent.getProperty("in_name", "");
                string in_weight_message = itmPevent.getProperty("in_weight_message", "");
                in_name = in_name + in_weight_message;

                string map_short_org = itmPevent.getProperty("map_short_org", "");
                string in_sign_foot = itmPevent.getProperty("in_sign_foot", "").PadLeft(2, '0');

                ranges = sheet.FindAllString("#CM" + in_section_no, true, true);
                foreach (Spire.Xls.CellRange range in ranges)
                {
                    if (map_short_org != "" || in_name != "")
                    {
                        range.Text = map_short_org;
                        if (in_tree_id.Contains("M"))
                        {
                            play_count++;
                        }
                    }
                    else
                    {
                        range.Text = "-bye3-";
                    }
                }

                ranges = sheet.FindAllString("#P" + in_section_no, true, true);
                foreach (Spire.Xls.CellRange range in ranges)
                {
                    if (map_short_org != "" || in_name != "")
                    {
                        range.Text = in_name;
                    }
                    else
                    {
                        range.Text = "-bye4-";
                    }

                }

            }

            string[] noArr = in_tree_no.Split(',');
            play_count = play_count / 2;
            string[] m_n;
            switch (play_count)
            {
                case 2:
                    if (pg.isTwoOutOfThree)
                    {
                        //三戰兩勝
                        m_n = new string[] { "", "", "" };
                        m_n[0] = "[" + noArr[0] + "]";
                        m_n[1] = "[" + noArr[1] + "]";
                        m_n[2] = "[" + noArr[2] + "]";
                    }
                    else
                    {
                        m_n = new string[] { "" };
                        m_n[0] = "[" + noArr[0] + "]";
                    }
                    break;
                case 3:
                    m_n = new string[] { "", "", "" };
                    m_n[0] = "[" + noArr[0] + "]";
                    m_n[1] = "[" + noArr[1] + "]";
                    m_n[2] = "[" + noArr[2] + "]";
                    break;
                case 4:
                    m_n = new string[] { "", "", "", "", "", "" };
                    m_n[0] = "[" + noArr[0] + "]";
                    m_n[1] = "[" + noArr[1] + "]";
                    m_n[2] = "[" + noArr[3] + "]";
                    m_n[3] = "[" + noArr[2] + "]";
                    m_n[4] = "[" + noArr[4] + "]";
                    m_n[5] = "[" + noArr[5] + "]";
                    break;
                case 5:
                    m_n = new string[] { "", "", "", "", "", "", "", "", "", "" };
                    m_n[0] = "[" + noArr[0] + "]";
                    m_n[1] = "[" + noArr[1] + "]";
                    m_n[2] = "[" + noArr[4] + "]";
                    m_n[3] = "[" + noArr[2] + "]";
                    m_n[4] = "[" + noArr[5] + "]";
                    m_n[5] = "[" + noArr[7] + "]";
                    m_n[6] = "[" + noArr[3] + "]";
                    m_n[7] = "[" + noArr[6] + "]";
                    m_n[8] = "[" + noArr[8] + "]";
                    m_n[9] = "[" + noArr[9] + "]";
                    break;
                default:
                    m_n = new string[] { "[1]", "[6]", "[4]", "[9]", "[7]", "[2]", "[3]", "[10]", "[8]", "[5]" };
                    break;

            }

            for (int i = 0; i < m_n.Length; i++)
            {
                ranges = sheet.FindAllString("#SR" + i.ToString(), true, true);
                foreach (Spire.Xls.CellRange range in ranges)
                {
                    range.Text = m_n[i];
                }
            }

            //單循環賽資訊
            string sql = @"
            SELECT
                t1.in_win_sign_no
                , t2.in_sign_no
                , sum(t2.in_points) AS in_points
                , count(*) as qty
            FROM
                IN_MEETING_PEVENT t1 WITH(NOLOCK)
            INNER JOIN
                IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK)
                ON t2.source_id = t1.id
		    WHERE
				t2.source_id = '{#program_id}'
                AND t2.in_points is not null
            GROUP BY 
                t1.in_win_sign_no
                , t2.in_sign_no
            ";

            sql = sql.Replace("{#program_id}", pg.id);

            Item itmInfos = cfg.inn.applySQL(sql);

            if (itmInfos.getItemCount() > 0)
            {
                for (int i = 0; i < itmInfos.getItemCount(); i++)
                {
                    Item itmInfo = itmInfos.getItemByIndex(i);
                    ranges = sheet.FindAllString("#WQ" + itmInfo.getProperty("in_win_sign_no", ""), true, true);
                    foreach (Spire.Xls.CellRange range in ranges)
                    {
                        range.Text = itmInfo.getProperty("qty", "");
                    }
                    ranges = sheet.FindAllString("#WP" + itmInfo.getProperty("in_win_sign_no", ""), true, true);
                    foreach (Spire.Xls.CellRange range in ranges)
                    {
                        range.Text = itmInfo.getProperty("in_points", "");
                    }
                }
            }

            ranges = sheet.FindAllString("#C", true, true);
            foreach (Spire.Xls.CellRange range in ranges)
            {
                range.Text = "-bye-";
            }

            ranges = sheet.FindAllString("#P", true, true);
            foreach (Spire.Xls.CellRange range in ranges)
            {
                range.Text = "-bye-";
            }

            ranges = sheet.FindAllString("#SR", true, true);
            foreach (Spire.Xls.CellRange range in ranges)
            {
                range.Text = "";
            }


            Item itmRanks = GetRanks(cfg, pg);

            if (itmRanks.getItemCount() > 0)
            {
                for (int i = 0; i < itmRanks.getItemCount(); i++)
                {
                    Item itmRank = itmRanks.getItemByIndex(i);
                    //單循環名次
                    ranges = sheet.FindAllString("#WR" + itmRank.getProperty("in_sign_no", ""), true, true);
                    foreach (Spire.Xls.CellRange range in ranges)
                    {
                        range.Text = itmRank.getProperty("in_final_rank", "");
                    }

                    ranges = sheet.FindAllString("#WQ" + itmRank.getProperty("in_sign_no", ""), true, true);
                    foreach (Spire.Xls.CellRange range in ranges)
                    {
                        range.Text = "0";
                    }

                    ranges = sheet.FindAllString("#WP" + itmRank.getProperty("in_sign_no", ""), true, true);
                    foreach (Spire.Xls.CellRange range in ranges)
                    {
                        range.Text = "0";
                    }

                    if (i > 3)
                    {
                        continue;
                    }

                    sheet.Range["V" + (7 + i)].Text = itmRank.getProperty("in_final_rank", "");
                    sheet.Range["AB" + (7 + i)].Text = itmRank.getProperty("map_short_org", "");
                    sheet.Range["AS" + (7 + i)].Text = itmRank.getProperty("in_name", "");
                }
            }

            ranges = sheet.FindAllString("#W", true, true);
            foreach (Spire.Xls.CellRange range in ranges)
            {
                range.Text = "";
            }

            return play_count;
        }

        private Item GetMPEvents(TConfig cfg, TProgram program)
        {
            string sql = @"
                SELECT
                    t1.in_tree_id
                    , t1.in_tree_no
                    , t2.in_sign_foot
                    , t2.in_sign_no
                    , t2.in_sign_bypass
                    , t11.in_section_no
                    , t11.in_name
                    , t11.map_short_org
                    , t11.in_final_rank
                    , t11.in_sign_time
                    , t11.in_weight_message
                FROM
                    IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
                    IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK)
                    ON t2.source_id = t1.id
                LEFT OUTER JOIN
                    VU_MEETING_PTEAM  t11 WITH(NOLOCK)
                    ON t11.source_id = t1.source_id
                    AND t11.in_sign_no = t2.in_sign_no
    	        WHERE
    			    t1.source_id = '{#program_id}'
    			    AND in_tree_name in ('main', 'repechage')
                ORDER BY
                      t1.in_tree_sort 
    			    , t1.in_tree_id
                    , t1.in_tree_no
                    , t2.in_sign_foot
            ";

            sql = sql.Replace("{#program_id}", program.id);

            return cfg.inn.applySQL(sql);
        }

        //名次
        private Item GetRanks(TConfig cfg, TProgram program)
        {
            string sql = @"
                SELECT 
                    in_name
                    , map_short_org
                    , in_final_rank 
                    , in_judo_no
                FROM 
                    VU_MEETING_PTEAM WITH(NOLOCK)
                WHERE 
                    source_id = '{#program_id}'
				    AND ISNULL(in_final_rank, 0) > 0
                ORDER BY 
                    in_final_rank";

            sql = sql.Replace("{#program_id}", program.id);

            return cfg.inn.applySQL(sql);
        }

        private List<TProgram> MapProgram(TConfig cfg)
        {
            List<TProgram> result = new List<TProgram>();

            string sql = "";

            if (cfg.program_id != "")
            {
                sql = @"SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + cfg.program_id + "'";
            }
            else
            {
                sql = @"SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE in_meeting = '" + cfg.meeting_id + "' AND in_fight_day = '" + cfg.in_date + "'";
            }

            sql += " ORDER BY in_site_mat";

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);

                TProgram obj = new TProgram
                {
                    id = item.getProperty("id", ""),
                    in_name2 = item.getProperty("in_name2", ""),
                    in_short_name = item.getProperty("in_short_name", ""),
                    in_team_count = item.getProperty("in_team_count", "0"),
                    in_round_code = item.getProperty("in_round_code", "0"),
                    in_battle_type = item.getProperty("in_battle_type", ""),
                    in_site_mat = item.getProperty("in_site_mat", ""),
                    in_fight_day = item.getProperty("in_fight_day", ""),
                    in_sign_time = item.getProperty("in_sign_time", ""),

                    in_l1 = item.getProperty("in_l1", ""),
                    in_l2 = item.getProperty("in_l2", ""),
                    in_l3 = item.getProperty("in_l3", ""),

                    Value = item,
                };

                obj.isRobin = obj.in_battle_type.Contains("SingleRoundRobin");

                if (obj.isRobin && obj.in_team_count == "2")
                {
                    obj.isTwoOutOfThree = true;
                }

                if (obj.isTwoOutOfThree)
                {
                    obj.sheetName = "TwoOutOfThree";
                }
                else if (obj.isRobin)
                {
                    obj.sheetName = "SingleRoundRobin";
                }
                else
                {
                    obj.sheetName = obj.in_round_code;
                }

                obj.signTime = GetDateTimeValue(obj.in_sign_time, "yyyy-MM-dd", 8);
                obj.teamCount = GetIntVal(obj.in_team_count);
                obj.roundCode = GetIntVal(obj.in_round_code);

                obj.JudoNumList = cfg.SportService.NoArray(obj.teamCount).ToList();

                result.Add(obj);
            }

            return result;
        }

        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Aras.Server.Core.IContextState RequestState { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public Innovator inn { get; set; }

            public string meeting_id { get; set; }
            public string program_id { get; set; }
            public string in_date { get; set; }
            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }
            public string mode { get; set; }

            public Item itmMeeting { get; set; }

            public string mt_title { get; set; }
            public string mt_battle_repechage { get; set; }

            public InnSport.Core.Services.ISportService SportService { get; set; }
        }

        private class TProgram
        {
            public Item Value { get; set; }
            public string id { get; set; }
            public string in_name2 { get; set; }
            public string in_short_name { get; set; }
            public string in_team_count { get; set; }
            public string in_round_code { get; set; }
            public string in_battle_type { get; set; }
            public string in_site_mat { get; set; }
            public string in_fight_day { get; set; }
            public string in_sign_time { get; set; }

            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }


            public bool isRobin { get; set; }
            public bool isTwoOutOfThree { get; set; }
            public string sheetName { get; set; }
            public string signTime { get; set; }
            public int teamCount { get; set; }
            public int roundCode { get; set; }
            public List<int> JudoNumList { get; set; }
        }

        private string GetDateTimeValue(string value, string format = "yyyy-MM-dd", int hours = 0)
        {
            if (value == null || value == "") return "";

            DateTime dt = DateTime.Now;
            if (DateTime.TryParse(value, out dt))
            {
                return dt.AddHours(hours).ToString(format);
            }
            else
            {
                return value;
            }
        }

        private int GetIntVal(string value, int def = 0)
        {
            int result = def;
            Int32.TryParse(value, out result);
            return result;
        }
    }
}