﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class In_Meeting_Weight_Status : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
            目的: 過磅狀態變更
            輸入: 
                - muid: In_Meeting_User id
                - in_weight_status
                    - leave: 請假
                    - off: 未到
                    - dq: DQ
                    - on: 過磅合格
            日期: 
                2021-03-31: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]In_Meeting_Weight_Status";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, this.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                InnH = _InnH,

                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                muid = itmR.getProperty("muid", ""),
                in_weight_status = itmR.getProperty("in_weight_status", ""),
                scene = itmR.getProperty("scene", ""),
            };

            switch (cfg.scene)
            {
                case "setting":
                    SettingPage(cfg, itmR);
                    break;

                case "update":
                    Update(cfg, itmR);
                    break;

                default:
                    ChangeMUserWStatus(cfg);
                    break;
            }

            return itmR;
        }

        private void Update(TConfig cfg, Item itmReturn)
        {
            string in_status = itmReturn.getProperty("id", "");
            string property = itmReturn.getProperty("property", "");
            string value = itmReturn.getProperty("value", "");
            RunMerge(cfg, in_status, property, value);
        }

        private void RunMerge(TConfig cfg, string in_status, string property, string value)
        {
            Item item = cfg.inn.newItem("In_Meeting_PWeight", "merge");
            item.setAttribute("where", "in_meeting = '" + cfg.meeting_id + "' AND in_status = '" + in_status + "'");
            item.setProperty("in_meeting", cfg.meeting_id);
            item.setProperty("in_status", in_status);
            item.setProperty(property, value);

            Item itmResult = item.apply();

            if (itmResult.isError())
            {
                throw new Exception("狀態更新失敗");
            }
        }

        private void SettingPage(TConfig cfg, Item itmReturn)
        {
            var itmMeeting = cfg.inn.applySQL("SELECT in_title FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            itmReturn.setProperty("in_title", itmMeeting.getProperty("in_title", ""));

            var items = GetWStatus(cfg);

            var map = MapWStatus(cfg, items);

            if (map.Count == 0)
            {
                map = DefaultWStatusMap(cfg);

                foreach (var kv in map)
                {
                    var obj = kv.Value;
                    RunMerge(cfg, obj.value, "in_is_remove", obj.in_is_remove);
                }
            }

            foreach (var kv in map)
            {
                var obj = kv.Value;

                Item item = cfg.inn.newItem();
                item.setType("inn_item");
                item.setProperty("label", obj.label);
                item.setProperty("value", obj.value);
                item.setProperty("inn_remove", obj.in_is_remove);

                itmReturn.addRelationship(item);
            }
        }

        //變更與會者過磅狀態
        private void ChangeMUserWStatus(TConfig cfg)
        {
            if (cfg.muid == "")
            {
                throw new Exception("與會者 id 不可為空值");
            }

            Item itmMUser = cfg.inn.applySQL("SELECT * FROM IN_MEETING_USER WITH(NOLOCK) WHERE id = '" + cfg.muid + "'");
            if (itmMUser.isError())
            {
                throw new Exception("與會者錯誤");
            }

            string[] dq_status_arr = new string[] { "leave", "off", "dq" };
            string in_weight_result = "";
            string in_check_result = "";
            string in_weight_message = "";

            switch (cfg.in_weight_status)
            {
                case "dq":
                    in_weight_message = "(DQ)";
                    break;

                case "off":
                    in_weight_message = "(DNS)";
                    break;

                case "leave":
                    in_weight_message = "(請假)";
                    break;

                case "king":
                    break;

                case "":
                    break;

                default:
                    break;
            }

            if (dq_status_arr.Contains(cfg.in_weight_status))
            {
                in_weight_result = "0";
                in_check_result = "0";
            }
            else
            {
                in_weight_result = "1";
                in_check_result = "";
            }

            string sql = "UPDATE IN_MEETING_USER SET"
                + " in_weight_result = N'" + in_weight_result + "'"
                + ", in_weight_status = N'" + cfg.in_weight_status + "'"
                + " WHERE id = '" + cfg.muid + "'";

            Item itmSQL = cfg.inn.applySQL(sql);

            if (itmSQL.isError())
            {
                throw new Exception("更新與會者 發生錯誤");
            }

            string in_l1 = itmMUser.getProperty("in_l1", "");

            if (in_l1 == "團體組")
            {
                return;
            }

            sql = @"
                UPDATE t1 SET
	                t1.in_check_result = N'{#in_check_result}'
	                , t1.in_weight_message = N'{#in_weight_message}'
                FROM
	                IN_MEETING_PTEAM t1 
                INNER JOIN 
	                IN_MEETING_USER t2 
	                ON t2.source_id = t1.in_meeting
	                AND t1.in_team_key = ISNULL(t2.in_l1, '')
	                 + '-' + ISNULL(t2.in_l2, '')
	                 + '-' + ISNULL(t2.in_l3, '')
	                 + '-' + ISNULL(t2.in_index, '')
	                 + '-' + ISNULL(t2.in_creator_sno, '')
                WHERE
	                t2.id = '{#muid}'
            ";

            sql = sql.Replace("{#muid}", cfg.muid)
                .Replace("{#in_check_result}", in_check_result)
                .Replace("{#in_weight_message}", in_weight_message);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            itmSQL = cfg.inn.applySQL(sql);

            if (itmSQL.isError())
            {
                throw new Exception("更新組別隊伍 發生錯誤");
            }


            Item itmData = cfg.inn.newItem();
            itmData.setType("In_Meeting_Program");
            itmData.setProperty("meeting_id", itmMUser.getProperty("source_id", ""));
            itmData.setProperty("l1_value", itmMUser.getProperty("in_l1", ""));
            itmData.setProperty("l2_value", itmMUser.getProperty("in_l2", ""));
            itmData.setProperty("l3_value", itmMUser.getProperty("in_l3", ""));
            itmData.setProperty("muid", itmMUser.getProperty("id", ""));
            itmData.setProperty("no_straw", "");
            itmData.setProperty("scene", "");
            itmData.apply("in_meeting_straw_off");
        }

        private Dictionary<string, TWStatus> DefaultWStatusMap(TConfig cfg)
        {
            var result = new Dictionary<string, TWStatus>();
            result.Add("off", new TWStatus { value = "off", label = "未到", in_is_remove = "0", is_remove = false });
            result.Add("leave", new TWStatus { value = "leave", label = "請假", in_is_remove = "1", is_remove = true });
            result.Add("dq", new TWStatus { value = "dq", label = "DQ", in_is_remove = "0", is_remove = false });
            return result;
        }

        private Dictionary<string, TWStatus> MapWStatus(TConfig cfg, Item items)
        {
            var result = new Dictionary<string, TWStatus>();

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);

                TWStatus obj = new TWStatus
                {
                    id = item.getProperty("id", ""),
                    value = item.getProperty("value", ""),
                    label = item.getProperty("label", ""),
                    in_is_remove = item.getProperty("in_is_remove", ""),
                };

                obj.is_remove = obj.in_is_remove == "1";

                result.Add(obj.value, obj);
            }

            return result;
        }

        private Item GetWStatus(TConfig cfg)
        {
            string sql = @"
                SELECT 
	                t1.id
                    , t1.in_status  AS 'value'
                    , t1.in_is_remove
	                , t2.label_zt   AS 'label'
                FROM 
	                IN_MEETING_PWEIGHT t1 WITH(NOLOCK)
                INNER JOIN
                (
	                SELECT 
		                t12.value, t12.label_zt, t12.sort_order 
	                FROM 
		                LIST t11 WITH(NOLOCK) 
	                INNER JOIN 
		                [VALUE] t12 WITH(NOLOCK) 
		                ON t12.source_id = t11.id 
	                WHERE 
		                t11.name = 'In_Weight_Status'
						AND t12.value <> 'on'
                ) t2 ON t2.value = t1.in_status
                WHERE
	                t1.in_meeting = '{#meeting_id}'
                ORDER BY
                    t2.sort_order
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public Innosoft.InnovatorHelper InnH { get; set; }

            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }

            public string meeting_id { get; set; }
            public string muid { get; set; }
            public string in_weight_status { get; set; }
            public string scene { get; set; }
        }

        private class TWStatus
        {
            public string id { get; set; }
            public string value { get; set; }
            public string label { get; set; }
            public string in_is_remove { get; set; }
            public bool is_remove { get; set; }
        }
    }
}