﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;
using System.Net;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class In_Meeting_Weight_Xls : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 過磅單下載
                日期: 
                    - 2021-10-10 改為批次 (Lina)
                    - 2020-10-29 創建 (Alan)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]In_Meeting_Weight_Xls";

            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, this.dom.InnerXml);

            Item itmPermit = inn.applyMethod("In_CheckIdentity", "<method>" + strMethodName + "</method><code>" + "ALL" + "</code>");
            bool isMeetingAdmin = itmPermit.getProperty("isMeetingAdmin", "") == "1";
            bool isMeetingLeader = itmPermit.getProperty("isMeetingLeader", "") == "1";
            bool can_view = isMeetingAdmin || isMeetingLeader;
            if (!can_view)
            {
                itmR.setProperty("error_message", "您無權限瀏覽此頁面，請重新登入");
                return itmR;
            }

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                InnH = _InnH,

                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                in_l1 = itmR.getProperty("in_l1", ""),
                in_l2 = itmR.getProperty("in_l2", ""),
                in_l3 = itmR.getProperty("in_l3", ""),
                mode = itmR.getProperty("mode", ""),
            };

            cfg.itmMeeting = inn.applySQL("SELECT in_title FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            if (cfg.itmMeeting.isError())
            {
                throw new Exception("賽事資料錯誤");
            }

            Item itmPrograms = GetPrograms(cfg);
            if (itmPrograms.isError() || itmPrograms.getItemCount() <= 0)
            {
                throw new Exception("組別資料錯誤");
            }

            var programs = MapProgram(cfg, itmPrograms);

            var exp = ExportInfo(cfg, "weight_path");

            //生成檔案在該賽事名稱資料夾下
            var in_title = cfg.itmMeeting.getProperty("in_title", "");
            var target_folder = exp.export_Path.Replace("meeting_excel", "meeting_weight").Trim('\\');
            var zip_name = in_title + "_稱量體重表.zip";
            var zip_file = target_folder + "\\" + zip_name;
            exp.export_Path = target_folder + "\\" + in_title;

            //清除檔案
            FixDirAndClearFile(exp.export_Path);

            //總表
            CreateALLWeight(cfg, programs, exp);

            // //產生組別過磅單
            // for (int i = 0; i < programs.Count; i++)
            // {
            //     var program = programs[i];
            //     CreateWeightXls(cfg, program, exp);
            // }

            //打包
            Zip(zip_file, exp.export_Path);

            //輸出
            itmR.setProperty("xls_name", zip_name);

            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

            return itmR;
        }

        private List<TProgram> MapProgram(TConfig cfg, Item itmPrograms)
        {
            List<TProgram> list = new List<TProgram>();
            int count = itmPrograms.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item itmProgram = itmPrograms.getItemByIndex(i);
                string no = (i + 1).ToString().PadLeft(2, '0');
                string in_name = itmProgram.getProperty("in_name", "");
                string in_n1 = itmProgram.getProperty("in_n1", "");
                string in_n2 = itmProgram.getProperty("in_n2", "");
                string in_n3 = itmProgram.getProperty("in_n3", "");
                string sheet_name = in_n1 + in_n2 + in_n3;

                itmProgram.setProperty("no", no);
                TProgram entity = new TProgram
                {
                    no = no,
                    name = in_name,
                    sheet_name = sheet_name,
                    Value = itmProgram,
                };

                entity.List = GetMUsers(cfg, itmProgram);

                list.Add(entity);
            }
            return list;
        }

        private void Zip(string zip_file, string zip_folder)
        {
            //刪除既有檔案
            if (System.IO.File.Exists(zip_file))
            {
                System.IO.File.Delete(zip_file);
            }

            //壓縮資料庫
            using (var zip = new Ionic.Zip.ZipFile(zip_file, System.Text.Encoding.UTF8))
            {
                zip.AddDirectory(zip_folder);
                zip.Comment = "This zip was created at " + System.DateTime.Now.ToString("G");
                zip.Save();
            }
        }

        private void FixDirAndClearFile(string path)
        {
            if (System.IO.Directory.Exists(path))
            {
                var dir = new System.IO.DirectoryInfo(path);
                dir.Delete(true);
            }

            if (!System.IO.Directory.Exists(path))
            {
                System.IO.Directory.CreateDirectory(path);
            }
        }

        private void CreateALLWeight(TConfig cfg, List<TProgram> programs, TExport exp)
        {
            Spire.Xls.Workbook workbook = new Spire.Xls.Workbook();
            workbook.LoadFromFile(exp.template_Path);

            int count = programs.Count;

            Spire.Xls.Worksheet sheetTemplate = workbook.Worksheets[0];

            for (int i = 0; i < 2; i++)
            {
                var program = programs[i];
                if (program.sheet_name == "")
                {
                    cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "n1-n3 is null: " + program.name);
                    continue;
                }

                CreateWeightXls(cfg, program, exp, workbook, sheetTemplate);
            }

            //移除樣板 sheet
            sheetTemplate.Remove();

            string ext_name = ".xlsx";

            string xls_name = "00.稱量體重表_總表";

            string xls_file = exp.export_Path + "\\" + xls_name + ext_name;

            workbook.SaveToFile(xls_file, Spire.Xls.ExcelVersion.Version2010);
        }

        private void CreateWeightXls(TConfig cfg, TProgram program, TExport exp, Spire.Xls.Workbook workbook, Spire.Xls.Worksheet sheetTemplate)
        {
            Spire.Xls.Worksheet sheet = workbook.CreateEmptySheet();
            sheet.CopyFrom(sheetTemplate);
            sheet.Name = program.sheet_name;

            sheet.Range["A1"].Text = cfg.itmMeeting.getProperty("in_title");

            Item items = program.List;

            string in_section_name = items.getItemByIndex(0).getProperty("in_section_name");
            sheet.Range["C5"].Text = in_section_name;

            int wsRow = 8;
            int wsCol = 0;
            string[] fields = { "cno", "in_name", "in_short_org", "in_weight", "space2", "space3", "space4" };
            int count = items.getItemCount();

            var Heads = new string[] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                item.setProperty("cno", (i + 1).ToString());

                sheet.Range["E" + (wsRow + i) + ":F" + (wsRow + i)].Merge();
                
                for (int j = 0; j < fields.Length; j++)
                {
                    string value = "";
                    string in_sign_no = item.getProperty("in_sign_no", "");
                    if ((in_sign_no == "" || in_sign_no == "0") && fields[j] == "in_section_no")
                    {
                        value = item.getProperty(fields[j], "");
                    }
                    else
                    {
                        if (fields[j] == "in_section_no")
                        {
                            value = "";
                        }
                        else
                        {
                            value = item.getProperty(fields[j], "");
                        }
                    }

                    var cidx = wsCol + j;
                    var pos = Heads[cidx] + (wsRow + i);

                    var range = sheet.Range[pos];
                    range.Text = value;
                    range.Style.Borders[Spire.Xls.BordersLineType.EdgeTop].LineStyle = Spire.Xls.LineStyleType.Thin;
                    range.Style.Borders[Spire.Xls.BordersLineType.EdgeRight].LineStyle = Spire.Xls.LineStyleType.Thin;
                    range.Style.Borders[Spire.Xls.BordersLineType.EdgeBottom].LineStyle = Spire.Xls.LineStyleType.Thin;
                    range.Style.Borders[Spire.Xls.BordersLineType.EdgeLeft].LineStyle = Spire.Xls.LineStyleType.Thin;
                }

                sheet.Rows[(wsRow + i - 1)].RowHeight = 21;
            }
        }

        /*
         * 
        private void CreateWeightXls(TConfig cfg, TProgram program, TExport exp)
        {
            ClosedXML.Excel.XLWorkbook workbook = new ClosedXML.Excel.XLWorkbook(exp.template_Path);
            ClosedXML.Excel.IXLWorksheet sheet = workbook.Worksheet(1);

            CreateWeightXls(cfg, program, exp, sheet);

            Item itmProgram = program.Value;

            string ext_name = ".xlsx";

            string xls_name = itmProgram.getProperty("no") + "."
                + "稱量體重表"
                + "_" + itmProgram.getProperty("in_name2")
                + "_" + itmProgram.getProperty("in_date_key").Replace("-", "");

            string xls_file = exp.export_Path + "\\" + xls_name + ext_name;

            workbook.SaveAs(xls_file);
        }
        */
        private Item GetPrograms(TConfig cfg)
        {
            string sql = "SELECT t1.*, t2.in_date_key FROM IN_MEETING_PROGRAM t1 WITH(NOLOCK)"
            + " LEFT OUTER JOIN IN_MEETING_ALLOCATION t2 WITH(NOLOCK) ON t2.in_program = t1.id"
            + " WHERE t1.in_meeting = '" + cfg.meeting_id + "'";

            if (cfg.in_l1 != "") sql += " AND t1.in_l1 = N'" + cfg.in_l1 + "'";
            if (cfg.in_l2 != "") sql += " AND t1.in_l2 = N'" + cfg.in_l2 + "'";
            if (cfg.in_l3 != "") sql += " AND t1.in_l3 = N'" + cfg.in_l2 + "'";
            sql += " ORDER BY t1.in_sort_order";

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        private Item GetMUsers(TConfig cfg, Item itmProgram)
        {
            string sql = @"
                SELECT
                     t1.in_section_no
                    , t1.in_name
                    , t1.in_section_name
                    , t1.in_short_org
                    , t1.in_title
                    , t1.in_weight
                    , t1.in_sign_no
                FROM 
                    IN_MEETING_USER AS t1 WITH(NOLOCK)
                LEFT OUTER JOIN
                    IN_RESUME AS t2 WITH(NOLOCK)
                    ON t2.in_sno = t1.in_sno
                WHERE 
                    t1.source_id = '{#meeting_id}' 
                    AND t1.in_l1 = N'{#in_l1}' 
                    AND t1.in_l2 = N'{#in_l2}' 
                    AND t1.in_l3 = N'{#in_l3}'
                    AND ISNULL(t1.in_weight_status, '') NOT IN ('leave')
                ORDER BY 
                    t1.in_stuff_b1
                    , t1.in_team
                    , t1.in_sno  
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_l1}", itmProgram.getProperty("in_l1", ""))
                .Replace("{#in_l2}", itmProgram.getProperty("in_l2", ""))
                .Replace("{#in_l3}", itmProgram.getProperty("in_l3", ""));

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }


        private TExport ExportInfo(TConfig cfg, string in_name)
        {
            TExport result = new TExport
            {
                template_Path = "",
                export_Path = "",
            };

            string aml = "<AML>" +
                "<Item type='In_Variable' action='get'>" +
                "<in_name>meeting_excel</in_name>" +
                "<Relationships>" +
                "<Item type='In_Variable_Detail' action='get'/>" +
                "</Relationships>" +
                "</Item></AML>";

            Item Vairable = cfg.inn.applyAML(aml);

            for (int i = 0; i < Vairable.getRelationships("In_Variable_Detail").getItemCount(); i++)
            {
                Item In_Variable_Detail = Vairable.getRelationships("In_Variable_Detail").getItemByIndex(i);
                if (In_Variable_Detail.getProperty("in_name", "") == in_name)
                    result.template_Path = In_Variable_Detail.getProperty("in_value", "");

                if (In_Variable_Detail.getProperty("in_name", "") == "export_path")
                    result.export_Path = In_Variable_Detail.getProperty("in_value", "");
                if (!result.export_Path.EndsWith(@"\"))
                    result.export_Path = result.export_Path + @"\";
            }

            return result;
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public Innosoft.InnovatorHelper InnH { get; set; }

            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }

            public string mode { get; set; }
            public string meeting_id { get; set; }
            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }

            public Item itmMeeting { get; set; }
        }

        private class TExport
        {
            public string template_Path { get; set; }
            public string export_Path { get; set; }
        }

        private class TProgram
        {
            public string no { get; set; }
            public string name { get; set; }
            public string sheet_name { get; set; }
            public Item Value { get; set; }
            public Item List { get; set; }
        }
    }
}