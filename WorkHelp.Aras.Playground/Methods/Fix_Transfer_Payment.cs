﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;
using System.Net;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class Fix_Transfer_Payment : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 轉檔檢查
                輸入: 
                    - meeting_id
                    - scene
                日期:
                    - 2022-01-20 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]" + "Fix_Transfer_Payment";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),

                log_file = @"C:\App\PLMCTA\error\[PLMCTA]In_Payment_List_Edi.log",
                txn_path = @"C:\App\PLMCTA\error",
            };

            //建立 IN_ERR_TXN
            AppendErrTxc(cfg);

            //分析 IN_ERR_TXN
            AnalysisErrTxc(cfg);

            return itmR;
        }

        private void AppendErrTxc(TConfig cfg)
        {
            var contents = System.IO.File.ReadAllText(cfg.log_file, System.Text.Encoding.UTF8);
            var dir = new System.IO.DirectoryInfo(cfg.txn_path);
            var files = dir.GetFiles();

            List<TTXN> list = new List<TTXN>();

            for (int i = 0; i < files.Length; i++)
            {
                var file = files[i];
                var name = file.Name;
                if (name.EndsWith(".x"))
                {
                    var pos1 = contents.IndexOf(name);
                    var pos2 = contents.IndexOf("銷帳編號:", pos1);

                    var code = contents.Substring(pos2 + "銷帳編號:".Length, 16);

                    var obj = new TTXN
                    {
                        in_txn = "name",
                        in_code_2 = code,
                        in_note = "",
                        in_is_refile = false,
                    };

                    list.Add(obj);
                }
            }

            //清空所有資料
            cfg.inn.applySQL("DELETE FROM IN_ERR_TXN");

            foreach (var obj in list)
            {
                Item item = cfg.inn.newItem("IN_ERR_TXN", "add");
                item.setProperty("in_txn", obj.in_txn);
                item.setProperty("in_code_2", obj.in_code_2);
                Item itmResult = item.apply();
                if (itmResult.isError())
                {
                    throw new Exception("新增 IN_ERR_TXN 發生錯誤");
                }
            }
        }

        //分析 IN_ERR_TXN
        private void AnalysisErrTxc(TConfig cfg)
        {
            string sql = "SELECT * FROM IN_ERR_TXN WITH(NOLOCK)";
            Item items = cfg.inn.applySQL(sql);
            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);

                var obj = new TTXN
                {
                    id = item.getProperty("id", ""),
                    in_txn = item.getProperty("in_txn", ""),
                    in_code_2 = item.getProperty("in_code_2", ""),
                    in_note = "",
                    in_is_refile = false,
                };

                AnalysisErrTxc(cfg, obj);

                string refile = obj.in_is_refile ? "1" : "0";

                string sql_upd = "UPDATE IN_ERR_TXN SET"
                    + " in_is_refile = '"+ refile + "'"
                    + ", in_note = N'" + obj.in_note + "'"
                    + " WHERE id = '" + obj.id + "'";

                Item itmResult = cfg.inn.applySQL(sql_upd);
                if (itmResult.isError())
                {
                    throw new Exception("更新 IN_ERR_TXN 發生錯誤");
                }

            }
        }
        //分析 IN_ERR_TXN
        private void AnalysisErrTxc(TConfig cfg, TTXN obj)
        {
            Item itmPay = cfg.inn.newItem("In_Meeting_Pay", "get");
            itmPay.setProperty("in_code_2", obj.in_code_2);
            itmPay = itmPay.apply();

            if (itmPay.isError() || itmPay.getResult() == "")
            {
                obj.in_note = "查無繳費單";
                return;
            }

            string in_paynumber = itmPay.getProperty("item_number", "");
            string in_pay_date_real = itmPay.getProperty("in_pay_date_real", "");
            string in_meeting = itmPay.getProperty("in_meeting", "");
            string in_cla_meeting = itmPay.getProperty("in_cla_meeting", "");

            string mt_name = "IN_MEETING";
            string mtur_name = "IN_MEETING_USER";
            string mt_id = in_meeting;
            if (in_cla_meeting != "")
            {
                mt_name = "IN_CLA_MEETING";
                mtur_name = "IN_CLA_MEETING_USER";
                mt_id = in_cla_meeting;
            }

            if (in_pay_date_real == "")
            {
                obj.in_note = "in_pay_date_real 未更新: " + in_paynumber;
                obj.in_is_refile = true;
                return;
            }

            bool is_muser_error = false;

            string sql = "SELECT * FROM " + mtur_name + " WITH(NOLOCK) WHERE in_paynumber = '" + in_paynumber + "'";
            Item itmMUsers = cfg.inn.applySQL(sql);
            int count = itmMUsers.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item itmMUser = itmMUsers.getItemByIndex(i);
                string in_paytime = itmMUser.getProperty("in_paytime", "");
                if (in_paytime == "")
                {
                    is_muser_error = true;
                    break;
                }
            }

            if (is_muser_error)
            {
                obj.in_note = "與會者未更新: " + in_paynumber;
                obj.in_is_refile = true;
                return;
            }

            sql = "SELECT in_meeting_type FROM " + mt_name + " WITH(NOLOCK) WHERE id = '" + mt_id + "'";
            Item itmMeeting = cfg.inn.applySQL(sql);
            if (itmMeeting.isError())
            {
                obj.in_note = "Meeting 資料異常: " + in_paynumber;
                return;
            }

            string in_meeting_type = itmMeeting.getProperty("in_meeting_type", "");
            if (in_meeting_type != "payment")
            {
                return;
            }

            sql = "SELECT * FROM In_Resume_Pay WITH(NOLOCK) WHERE in_paynumber = '" + in_paynumber + "'";
            Item itmRPay = cfg.inn.applySQL(sql);
            if (itmRPay.isError())
            {
                obj.in_note = "In_Resume_Pay 資料異常: " + in_paynumber;
            }
            else if (itmRPay.getResult() == "")
            {
                obj.in_note = "需手動補寫會費繳納紀錄: " + in_paynumber;
            }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }

            public string log_file { get; set; }
            public string txn_path { get; set; }
        }

        private class TTXN
        {
            public string id { get; set; }
            public string in_txn { get; set; }
            public string in_code_2 { get; set; }
            public string in_note { get; set; }
            public bool in_is_refile { get; set; }

            public string pay_id { get; set; }
            public string in_paynumber { get; set; }
            public string in_paytime { get; set; }
        }

    }
}