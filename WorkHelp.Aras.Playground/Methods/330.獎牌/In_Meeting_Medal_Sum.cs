﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class In_Meeting_Medal_Sum : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 
                    獎牌戰統計
                輸出: 
                    XLSX
                重點: 
                    - 需 System.Draw.dll (建議一併引用 System.Drawing.Common.dll)
                    - 需 Spire.Xls 系統 dll
                    - Spire.Xls 需 license
                    - 請於 method-config.xml 引用 DLL
                日期: 
                    2021-12-30: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]In_Meeting_Medal_Sum";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                InnH = _InnH,

                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                scene = itmR.getProperty("scene", ""),
                CharSet = GetCharSet(),
            };

            cfg.itmMeeting = cfg.inn.applySQL("SELECT in_title FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            cfg.mt_title = cfg.itmMeeting.getProperty("in_title", "");

            var medals = new List<TMedal>();
            medals.Add(new TMedal { rank = 1, label = "冠軍", ranks = new int[] { 1 }, prop = "golden_count" });
            medals.Add(new TMedal { rank = 2, label = "亞軍", ranks = new int[] { 2 }, prop = "silver_count" });
            medals.Add(new TMedal { rank = 3, label = "季軍", ranks = new int[] { 3, 4 }, prop = "copper_count" });
            medals.Add(new TMedal { rank = 5, label = "第五名", ranks = new int[] { 5, 6 }, prop = "fifth_count" });
            medals.Add(new TMedal { rank = 7, label = "第七名", ranks = new int[] { 7, 8 }, prop = "seventh_count" });

            var items = GetScores(cfg);
            var orgs = MapOrgList(cfg, items, medals);

            switch (cfg.scene)
            {
                case "xlsx":
                    Xlsx(cfg, medals, orgs, itmR);
                    break;

                default:
                    Page(cfg, medals, orgs, itmR);
                    break;
            }

            return itmR;
        }

        private void Xlsx(TConfig cfg, List<TMedal> medals, List<TOrg> orgs, Item itmReturn)
        {
            var exp = ExportInfo(cfg, "");

            //試算表
            Spire.Xls.Workbook workbook = new Spire.Xls.Workbook();

            AppendRankSheet(cfg, workbook, medals, orgs);

            workbook.Worksheets[0].Remove();
            workbook.Worksheets[0].Remove();
            workbook.Worksheets[0].Remove();

            string ext_name = ".xlsx";
            string xls_name = cfg.mt_title + "_獎牌統計_" + DateTime.Now.ToString("MMdd_HHmmss");
            string xls_file = exp.export_Path + "\\" + xls_name + ext_name;
            string xls_url = xls_name + ext_name;

            workbook.SaveToFile(xls_file, Spire.Xls.ExcelVersion.Version2013);

            itmReturn.setProperty("xls_name", xls_url);
        }


        //報名總表
        private void AppendRankSheet(TConfig cfg, Spire.Xls.Workbook workbook, List<TMedal> medals, List<TOrg> orgs)
        {
            Spire.Xls.Worksheet sheet = workbook.CreateEmptySheet();
            sheet.Name = "獎牌統計";

            List<TField> fields = new List<TField>();
            fields.Add(new TField { ci = 1, title = "序號", property = "no", css = TCss.Center, width = 8 });
            fields.Add(new TField { ci = 2, title = "單位", property = "map_short_org", css = TCss.None, width = 18 });

            int ci = 3;
            foreach (var medal in medals)
            {
                fields.Add(new TField { ci = ci, title = medal.label, property = medal.prop, css = TCss.Number, width = 10 });
                ci++;
            }

            fields.Add(new TField { ci = ci, title = "總計", property = "total", css = TCss.Number, width = 5 });

            MapCharSet(cfg, fields);

            var mt_mr = sheet.Range["A1:H1"];
            mt_mr.Merge();
            mt_mr.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
            mt_mr.Text = cfg.mt_title;
            mt_mr.Style.Font.Size = 16;
            mt_mr.RowHeight = 20;

            var rpt_mr = sheet.Range["A2:H2"];
            rpt_mr.Merge();
            rpt_mr.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
            rpt_mr.Text = "獎牌統計";
            rpt_mr.Style.Font.Size = 16;
            rpt_mr.RowHeight = 20;

            int mnRow = 3;
            int wsRow = 3;

            //標題列
            SetHeadRow(sheet, wsRow, fields);
            wsRow++;

            //凍結窗格
            sheet.FreezePanes(wsRow, fields.Last().ci + 1);

            //資料容器
            Item item = cfg.inn.newItem();
            int no = 0;

            foreach (var org in orgs)
            {
                no++;

                item.setProperty("no", no.ToString());
                item.setProperty("map_short_org", org.map_short_org);
                item.setProperty("golden_count", org.golden_count.ToString());
                item.setProperty("silver_count", org.silver_count.ToString());
                item.setProperty("copper_count", org.copper_count.ToString());
                item.setProperty("fifth_count", org.fifth_count.ToString());
                item.setProperty("seventh_count", org.seventh_count.ToString());
                item.setProperty("total", org.total.ToString());

                SetItemCell(cfg, sheet, wsRow, item, fields);

                wsRow++;
            }

            //設定格線
            SetRangeBorder(sheet, fields, mnRow, wsRow);

            //設定欄寬
            SetColumnWidth(sheet, fields);
        }

        private void Page(TConfig cfg, List<TMedal> medals, List<TOrg> orgs, Item itmReturn)
        {
            var contents = TableContents(cfg, medals, orgs);
            itmReturn.setProperty("inn_table", contents);
            itmReturn.setProperty("in_title", cfg.mt_title);
        }

        private string TableContents(TConfig cfg, List<TMedal> medals, List<TOrg> orgs)
        {
            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            head.AppendLine("<thead>");
            head.AppendLine("<tr>");
            head.AppendLine("<th class='text-center'>單位</th>");
            for (int i = 0; i < medals.Count; i++)
            {
                var medal = medals[i];
                head.AppendLine("<th class='text-center'>" + medal.label + "</th>");
            }
            head.AppendLine("<th class='text-center'>總計</th>");
            head.AppendLine("</tr>");
            head.AppendLine("</thead>");


            body.AppendLine("<tbody>");

            for (int i = 0; i < orgs.Count; i++)
            {
                var org = orgs[i];

                body.AppendLine("<tr>");
                body.AppendLine("<td class='text-center'>" + org.map_short_org + "</td>");
                for (int j = 0; j < org.medals.Count; j++)
                {
                    var org_medal = org.medals[j];
                    body.AppendLine("<td class='text-center'>" + org_medal.count + "</td>");
                }
                body.AppendLine("<td class='text-center'>" + org.total + "</td>");
                body.AppendLine("</tr>");
            }
            body.AppendLine("</tbody>");

            var table_name = "sum_table";

            StringBuilder builder = new StringBuilder();
            builder.AppendLine("<table id='" + table_name + "' class='table table-hover table-bordered table-rwd rwd rwdtable match-table'>");
            builder.Append(head);
            builder.Append(body);
            builder.AppendLine("</table>");

            // builder.AppendLine("<script>");
            // builder.AppendLine("$('#" + table_name + "').bootstrapTable({});");

            // builder.AppendLine("if($(window).width() <= 768) {");
            // builder.AppendLine("    $('#" + table_name + "').bootstrapTable('toggleView');");
            // builder.AppendLine("}");

            // builder.AppendLine("</script>");

            return builder.ToString();
        }

        private List<TOrg> MapOrgList(TConfig cfg, Item items, List<TMedal> medals)
        {
            List<TOrg> list = new List<TOrg>();

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_stuff_b1 = item.getProperty("in_stuff_b1", "");
                string map_short_org = item.getProperty("map_short_org", "");
                string in_final_rank = item.getProperty("in_final_rank", "0");
                int final_rank = GetIntVal(in_final_rank);

                var org = list.Find(x => x.map_short_org == map_short_org);

                if (org == null)
                {
                    org = new TOrg
                    {
                        in_stuff_b1 = in_stuff_b1,
                        map_short_org = map_short_org,
                        medals = new List<TMedal>(),
                    };

                    for (int j = 0; j < medals.Count; j++)
                    {
                        var medal = medals[j];
                        org.medals.Add(new TMedal
                        {
                            rank = medal.rank,
                            ranks = medal.ranks,
                            label = medal.label,
                            count = 0,
                            items = new List<Item>(),
                        });
                    }
                    list.Add(org);
                }

                var rank_medal = org.medals.Find(x => x.ranks.Contains(final_rank));
                if (rank_medal == null) throw new Exception("異常");

                rank_medal.count++;
                rank_medal.items.Add(item);

            }

            for (int i = 0; i < list.Count; i++)
            {
                var org = list[i];

                for (int j = 0; j < org.medals.Count; j++)
                {
                    var medal = org.medals[j];

                    switch (medal.rank)
                    {
                        case 1: org.golden_count = medal.count; break;
                        case 2: org.silver_count = medal.count; break;
                        case 3: org.copper_count = medal.count; break;
                        case 5: org.fifth_count = medal.count; break;
                        case 7: org.seventh_count = medal.count; break;
                    }

                    org.total += medal.count;
                }
            }

            return list.OrderByDescending(x => x.golden_count)
                .ThenByDescending(x => x.silver_count)
                .ThenByDescending(x => x.copper_count)
                .ToList();
        }

        private Item GetScores(TConfig cfg)
        {
            string sql = @"
                SELECT
	                t2.in_stuff_b1
	                , t2.map_short_org
	                , t1.in_name2 AS 'pg_name'
	                , t2.in_team
	                , t2.in_names
	                , t2.in_final_rank 
	                , t2.in_show_rank 
                FROM 
	                IN_MEETING_PROGRAM t1
                INNER JOIN
	                VU_MEETING_PTEAM t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                WHERE 
	                t1.in_meeting = '{#meeting_id}'
	                AND ISNULL(t2.in_final_rank, 0) > 0
                ORDER BY
	                t2.in_stuff_b1
	                , t1.in_sort_order
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }


        private class TMedal
        {
            public int rank { get; set; }
            public int[] ranks { get; set; }
            public string label { get; set; }
            public string prop { get; set; }
            public int count { get; set; }
            public List<Item> items { get; set; }
        }

        private class TOrg
        {
            public string in_stuff_b1 { get; set; }
            public string map_short_org { get; set; }
            public List<TMedal> medals { get; set; }

            public int golden_count { get; set; }
            public int silver_count { get; set; }
            public int copper_count { get; set; }
            public int fifth_count { get; set; }
            public int seventh_count { get; set; }

            public int total { get; set; }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public Innosoft.InnovatorHelper InnH { get; set; }

            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }

            public string meeting_id { get; set; }
            public string scene { get; set; }

            public Item itmMeeting { get; set; }
            public string mt_title { get; set; }
            public string mt_battle_type { get; set; }
            public string[] CharSet { get; set; }
        }

        #region Xlsx

        private class TExport
        {
            public string template_Path { get; set; }
            public string export_Path { get; set; }
        }

        private class TField
        {
            public string title { get; set; }
            public string property { get; set; }
            public TCss css { get; set; }
            public int ci { get; set; }
            public string cs { get; set; }
            public int width { get; set; }
            public Func<TConfig, TField, Item, string> getValue { get; set; }

            public bool is_merge { get; set; }
        }

        private enum TCss
        {
            None = 0,
            Center = 10,
            Text = 100,
            Number = 200,
            Money = 300,
            Day = 400,
        }

        private string[] GetCharSet()
        {
            return new string[] { "", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
        }

        private TExport ExportInfo(TConfig cfg, string in_name)
        {
            Item itmXls = cfg.inn.applyMethod("In_Meeting_Excel_Path", "<in_name>" + in_name + "</in_name>");
            string Export_Path = itmXls.getProperty("export_path", "");
            string Template_Path = itmXls.getProperty("template_path", "");

            TExport result = new TExport
            {
                template_Path = Template_Path,
                export_Path = Export_Path,
            };

            return result;
        }

        //設定資料列
        private void SetItemCell(TConfig cfg, Spire.Xls.Worksheet sheet, int wsRow, Item item, List<TField> fields)
        {
            foreach (var field in fields)
            {
                var cr = field.cs + wsRow;
                var va = "";

                if (field.getValue != null)
                {
                    va = field.getValue(cfg, field, item);
                }
                else if (field.property != "")
                {
                    va = item.getProperty(field.property, "");
                }

                SetCell(sheet, cr, va, field.css);
            }
        }

        //設定資料格
        private void SetCell(Spire.Xls.Worksheet sheet, string cr, string value, TCss css)
        {
            var range = sheet.Range[cr];

            switch (css)
            {
                case TCss.None:
                    range.Text = value;
                    break;
                case TCss.Center:
                    range.Text = value;
                    range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                    break;
                case TCss.Text:
                    range.Text = "'" + value;
                    range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                    break;
                case TCss.Number:
                    range.NumberValue = GetIntVal(value);
                    range.NumberFormat = "0";
                    range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Right;
                    break;
                case TCss.Money:
                    range.NumberValue = GetIntVal(value);
                    range.NumberFormat = "$ #,##0";
                    break;
                case TCss.Day:
                    range.Text = GetDateTimeValue(value, "yyyy/MM/dd");
                    range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                    break;
            }
        }

        private void SetMoney(Spire.Xls.Worksheet sheet, string cr, double value)
        {
            var range = sheet.Range[cr];
            range.NumberValue = value;
            range.NumberFormat = "$ #,##0";
        }

        //設定標題列
        private void SetHead(Spire.Xls.Worksheet sheet, string cr, string value)
        {
            var range = sheet.Range[cr];
            range.Text = value;
            range.Style.Font.IsBold = true;
            range.Style.Font.Color = System.Drawing.Color.White;
            range.Style.Color = System.Drawing.Color.FromArgb(41, 92, 144);//#295C90
            range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
        }

        //設定格線
        private void SetRangeBorder(Spire.Xls.Worksheet sheet, string pos)
        {
            var range = sheet.Range[pos];
            range.BorderInside(Spire.Xls.LineStyleType.Thin, System.Drawing.Color.Black);
            range.BorderAround(Spire.Xls.LineStyleType.Thin, System.Drawing.Color.Black);
        }

        //設定格線
        private void SetRangeBorder(Spire.Xls.Worksheet sheet, List<TField> fields, int mnRow, int wsRow)
        {
            var tbl_ps = fields.First().cs + mnRow;
            var tbl_pe = fields.Last().cs + (wsRow - 1);
            SetRangeBorder(sheet, tbl_ps + ":" + tbl_pe);
        }

        private void MapCharSet(TConfig cfg, List<TField> fields)
        {
            foreach (var field in fields)
            {
                field.cs = cfg.CharSet[field.ci];
            }
        }

        private void MapCharSetAndIndex(TConfig cfg, List<TField> fields)
        {
            for (int i = 0; i < fields.Count; i++)
            {
                var field = fields[i];
                field.ci = i + 1;
                field.cs = cfg.CharSet[field.ci];
            }
        }

        private void SetHeadRow(Spire.Xls.Worksheet sheet, int wsRow, List<TField> fields)
        {
            foreach (var field in fields)
            {
                string cr = field.cs + wsRow;
                SetHead(sheet, cr, field.title);
            }
        }

        private void AutoFit(Spire.Xls.Worksheet sheet, List<TField> fields)
        {
            foreach (var field in fields)
            {
                sheet.Columns[field.ci - 1].AutoFitColumns();
            }
        }

        private void SetColumnWidth(Spire.Xls.Worksheet sheet, List<TField> fields)
        {
            foreach (var field in fields)
            {
                sheet.Columns[field.ci - 1].ColumnWidth = field.width;
            }
        }

        #endregion Xlsx

        private int GetIntVal(string value, int def = 0)
        {
            if (value == "") return 0;

            int result = def;
            Int32.TryParse(value, out result);
            return result;
        }

        //轉換日期
        private string GetDateTimeValue(string value, string format, bool add8Hours = true)
        {
            if (value == "") return "";
            DateTime dt = Convert.ToDateTime(value);
            if (add8Hours)
            {
                dt = dt.AddHours(8);
            }
            return dt.ToString(format);
        }
    }
}