﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class TEST_SPIRE_TRANSFER : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]in_meeting_draw_preview";

            Item itmR = this;

            Spire.Xls.Workbook workbook = new Spire.Xls.Workbook();
            workbook.LoadFromFile(@"C:\site\CTA_git\pages\PdfIO\CTA List.xlsx");
            workbook.SaveToFile(@"C:\site\CTA_git\pages\PdfIO\團體會員帳號清冊.pdf", Spire.Xls.FileFormat.PDF);

            var sheet = workbook.Worksheets[0];

            //sheet.AutoFitColumn(1);
            //sheet.AllocatedRange.AutoFitColumns();
            //sheet.AllocatedRange.AutoFitRows();
            sheet.Range["A7:F7"].Style.Color = System.Drawing.Color.Yellow;

            sheet.Range["B1"].Text = "Font setting";
            sheet.Range["B1"].Style.Font.IsBold = true;

            sheet.Range["B3"].Text = "Arial";
            sheet.Range["B3"].Style.Font.FontName = "Arial";

            sheet.Range["B4"].Text = "Large size";
            sheet.Range["B4"].Style.Font.Size = 20;

            sheet.Range["B5"].Text = "Bold";
            sheet.Range["B5"].Style.Font.IsBold = true;

            sheet.Range["B6"].Text = "Italic";
            sheet.Range["B6"].Style.Font.IsItalic = true;

            sheet.Range["B7"].Text = "Superscript";
            sheet.Range["B7"].Style.Font.IsSuperscript = true;

            sheet.Range["B8"].Text = "Colored";
            sheet.Range["B8"].Style.Font.Color = System.Drawing.Color.FromArgb(255, 125, 125);

            sheet.Range["B9"].Text = "Underline";
            sheet.Range["B9"].Style.Font.Underline = Spire.Xls.FontUnderlineType.Single;

            sheet.Range["B1"].Text = "NUMBER FORMATTING";
            sheet.Range["B1"].Style.Font.IsBold = true;

            sheet.Range["B3"].Text = "0";
            sheet.Range["C3"].NumberValue = 1234.5678;
            sheet.Range["C3"].NumberFormat = "0";

            sheet.Range["B4"].Text = "0.00";
            sheet.Range["C4"].NumberValue = 1234.5678;
            sheet.Range["C4"].NumberFormat = "0.00";

            sheet.Range["B5"].Text = "#,##0.00";
            sheet.Range["C5"].NumberValue = 1234.5678;
            sheet.Range["C5"].NumberFormat = "#,##0.00";

            sheet.Range["B6"].Text = "$#,##0.00";
            sheet.Range["C6"].NumberValue = 1234.5678;
            sheet.Range["C6"].NumberFormat = "$#,##0.00";

            sheet.Range["B7"].Text = "0;[Red]-0";
            sheet.Range["C7"].NumberValue = -1234.5678;
            sheet.Range["C7"].NumberFormat = "0;[Red]-0";

            sheet.Range["B8"].Text = "0.00;[Red]-0.00";
            sheet.Range["C8"].NumberValue = -1234.5678;
            sheet.Range["C8"].NumberFormat = "0.00;[Red]-0.00";

            sheet.Range["B9"].Text = "#,##0;[Red]-#,##0";
            sheet.Range["C9"].NumberValue = -1234.5678;
            sheet.Range["C9"].NumberFormat = "#,##0;[Red]-#,##0";

            sheet.Range["B10"].Text = "#,##0.00;[Red]-#,##0.000";
            sheet.Range["C10"].NumberValue = -1234.5678;
            sheet.Range["C10"].NumberFormat = "#,##0.00;[Red]-#,##0.00";

            sheet.Range["B11"].Text = "0.00E+00";
            sheet.Range["C11"].NumberValue = 1234.5678;
            sheet.Range["C11"].NumberFormat = "0.00E+00";

            sheet.Range["B12"].Text = "0.00%";
            sheet.Range["C12"].NumberValue = 1234.5678;
            sheet.Range["C12"].NumberFormat = "0.00%";

            sheet.Range["B3:B12"].Style.KnownColor = Spire.Xls.ExcelColors.Gray25Percent;


            sheet.AutoFitColumn(2);

            return itmR;
        }
    }
}