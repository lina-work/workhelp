﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using WorkHelp.Aras.Playground.ArasModules;
using System.Net;

namespace WorkHelp.Aras.Playground.Methods
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class In_Association_Role : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 協會權限判定
                指令: Item itmRoleResult = inn.applyMethod("In_Association_Role", "<in_sno></in_sno><idt_names>ACT_ASC_Degree</idt_names>");
                輸入: idt_names ex: ACT_ASC_Degree,ACT_ASC_Training
                輸出: isMeetingAdmin = itmRoleResult.getProperty("inn_result", "") == "1";
                日期: 
                    - 2022-01.19: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();

            Item itmR = inn.newItem();
            itmR.setProperty("inn_result", "0");

            string in_sno = this.getProperty("in_sno", "");
            string idt_names = this.getProperty("idt_names", "");

            if (in_sno == "" || idt_names == "")
            {
                return itmR;
            }

            string ids = idt_names;
            if (idt_names.Contains(","))
            {
                var list = idt_names.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();
                ids = string.Join(", ", list.Select(x => "N'" + x.Trim() + "'"));
            }

            string sql = @"
                SELECT
                    TOP 1 t3.id
                    , t3.name
                    , t1.in_number
                FROM
                    [IDENTITY] t1 WITH(NOLOCK)
                INNER JOIN
                    [MEMBER] t2 WITH(NOLOCK)
                    ON t2.related_id = t1.id
                INNER JOIN
                    [IDENTITY] t3 WITH(NOLOCK)
                    ON t3.id = t2.source_id
                WHERE
                    t1.in_number = '{#login_name}'
                    AND t3.name = '{#ids}'
            ";

            sql = sql.Replace("{#login_name}", in_sno)
                .Replace("{#ids}", ids);

            Item item = inn.applySQL(sql);

            if (item.getResult() != "")
            {
                itmR.setProperty("inn_result", "1");
            }

            return itmR;
        }
    }
}