﻿
namespace WorkHelp.Aras.Playground
{
    public class arasOledb
    {

        /// <summary>
        /// XLSX 轉換為 DataTable
        /// </summary>
        /// <param name="sFilePath">檔案路徑</param>
        /// <param name="sSheetName">sheet name (不需錢字號)</param>
        /// <returns>DataTable</returns>
        public static System.Data.DataTable XLSToDataTable2007(string sFilePath, string sSheetName)
        {
            try
            {
                string sConnectionString = @"Provider='Microsoft.ACE.OLEDB.12.0';Data Source='" + sFilePath + "';";
                sConnectionString += "Extended Properties='Excel 12.0 Xml;HDR=YES;IMEX=1;Persist Security Info=False'";
                string sSelectText = "SELECT * FROM [" + sSheetName + "$]";
                return BaseOleDBGetFile(sConnectionString, sSelectText);
            }
            catch (System.Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// 存取 Oledb
        /// </summary>
        /// <param name="sConnectionString">連線字串</param>
        /// <param name="sSelectText">執行指令</param>
        /// <returns></returns>
        private static System.Data.DataTable BaseOleDBGetFile(string sConnectionString, string sSelectText)
        {
            try
            {
                System.Data.DataTable Result = null;
                using (System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection(sConnectionString))
                {
                    using (System.Data.OleDb.OleDbDataAdapter da = new System.Data.OleDb.OleDbDataAdapter(sSelectText, conn))
                    {
                        try
                        {
                            Result = new System.Data.DataTable();
                            da.Fill(Result);
                        }
                        catch (System.Exception ex)
                        {
                            Result = null;
                        }
                    }
                }
                return Result;
            }
            catch (System.Exception ex)
            {
                return null;
            }
        }
    }
}
