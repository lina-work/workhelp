﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;

namespace WorkHelp.Aras.Playground.ArasModules
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class Startup : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
目的: 中華跆協資料轉檔-講習年度記錄
    - z教練裁判資料(講習)
日期: 
    - 2021/03/19: 創建 (lina)
*/

            //System.Diagnostics.Debugger.Break(); 

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]In_CTA_Seminar_Year";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "input dom: " + itmR.dom.InnerXml);

            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            Item itmPermit = inn.applyMethod("In_CheckIdentity", "<method>" + strMethodName + "</method><code>" + "ALL" + "</code>");
            bool isMeetingAdmin = itmPermit.getProperty("isMeetingAdmin", "") == "1";

            if (!isMeetingAdmin) throw new Exception("無權限");

            string aml = "";
            string sql = "";

            //sql = "SELECT * FROM IN_CTA_REFEREE_LOG WITH(NOLOCK) WHERE ISNULL(resume_id, '') <> '' AND in_tw_year IS NOT NULL ORDER BY no";
            sql = "SELECT * FROM IN_CTA_INSTRUCTOR_LOG WITH(NOLOCK) WHERE ISNULL(resume_id, '') <> '' AND in_tw_year IS NOT NULL ORDER BY no";
            Item itmLogs = inn.applySQL(sql);
            int count = itmLogs.getItemCount();
            CCO.Utilities.WriteDebug(strMethodName, "[R裁判講習記錄]轉檔開始-總筆數: " + count);

            for (int i = 0; i < count; i++)
            {
                Item itmLog = itmLogs.getItemByIndex(i);
                string no = itmLog.getProperty("no", "");
                string resume_id = itmLog.getProperty("resume_id", "");
                string in_name = itmLog.getProperty("in_name", "");
                string in_sno = itmLog.getProperty("in_sno", "");
                //CCO.Utilities.WriteDebug(strMethodName, "    " + no + ". " + in_name + "(" + in_sno + ")");

                //Item itmReferee = inn.newItem("In_Resume_Referee", "merge");
                Item itmReferee = inn.newItem("In_Resume_Instructor", "merge");
                itmReferee.setAttribute("where", "source_id = '" + resume_id + "' AND in_old_no = " + no);
                itmReferee.setProperty("source_id", resume_id);
                itmReferee.setProperty("in_old_no", no);
                itmReferee.setProperty("in_year", itmLog.getProperty("in_west_year", "").Trim());
                itmReferee.setProperty("in_level", itmLog.getProperty("in_level", "").Trim());
                itmReferee.setProperty("in_echelon", itmLog.getProperty("in_echelon", "").Trim());
                itmReferee.setProperty("in_know_score", itmLog.getProperty("in_know_score", "").Trim());
                itmReferee.setProperty("in_tech_score", itmLog.getProperty("in_tech_score", "").Trim());
                itmReferee.setProperty("in_note", itmLog.getProperty("in_note", "").Trim());

                string in_certificate = itmLog.getProperty("in_certificate", "").ToUpper();
                if (in_certificate == "1" || in_certificate == "TRUE")
                {
                    itmReferee.setProperty("in_certificate", "1");
                }
                else
                {
                    itmReferee.setProperty("in_certificate", "0");
                }
                itmReferee = itmReferee.apply();

                if (itmReferee.isError())
                {
                    CCO.Utilities.WriteDebug(strMethodName, "      - 發生錯誤");
                }
            }

            CCO.Utilities.WriteDebug(strMethodName, "[R裁判講習記錄]轉檔結束");

            return itmR;
        }
    }
}