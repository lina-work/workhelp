﻿using DocumentFormat.OpenXml.Drawing;
using DocumentFormat.OpenXml.Wordprocessing;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Calendar.v3;
using Google.Apis.Calendar.v3.Data;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Routing;

namespace WorkHelp.Aras.Playground
{
    /// <summary>
    /// 程序
    /// </summary>
    class Program
    {
 
        static void Main(string[] args)
        {
            string val = "038文化大學";
            string txt = val.Substring(0, 3);
            Console.WriteLine(txt);

            //for (int i = 0; i <= 128; i++)
            //{
            //    int[] arr = GetRoundAndSum(i, 2, 2, 1);
            //    int max_round = arr[0];
            //    int max_code = arr[1];
            //    Console.WriteLine("對戰人數: " + i.ToString().PadLeft(4, ' ') + ", 最大回合數: " + max_round + "(" + max_code + ")");
            //}
            Console.ReadLine();
        }

        /// <summary>
        /// 取得最大輪次代碼
        /// </summary>
        private static int[] GetRoundAndSum(int value, int code, int sum, int round)
        {
            if (value == 0)
            {
                return new int[] { 0, 0 };
            }
            else if (value > sum)
            {
                return GetRoundAndSum(value, code, code * sum, round + 1);
            }
            else
            {
                return new int[] { round, sum };
            }
        }


        /// <summary>
        /// 取得籤號陣列
        /// </summary>
        private static int[] GetNoArray(int code)
        {
            List<TNode> nodes = new List<TNode>();

            nodes.Add(new TNode { No = 1 , IsEven = false}) ;

            for (int i = 2; i <= code; i++)
            {
                TNode node = new TNode
                {
                    No = i,
                    IsEven = (i % 2) == 0
                };
                AppendTreeNode(nodes, node);
            }

            return nodes.Select(x => x.No).ToArray();
        }

        /// <summary>
        /// 附加節點
        /// </summary>
        private static void AppendTreeNode(List<TNode> tree, TNode node)
        {
            //當前節點的落點
            int code = GetMaxRingCode(node.No, 2, 2);
            int sum = code + 1;

            //尋找對手
            int target_no = sum - node.No;
            int target_pos = -1;

            for (int i = 0; i < tree.Count; i++)
            {
                TNode x = tree[i];
                if (x.No == target_no)
                {
                    target_pos = i;
                }
            }

            //找不到對手節點
            if (target_pos == -1)
            {
                return;
            }
            else
            {
                if (node.IsEven)
                {
                    tree.Insert(target_pos + 1, node);
                }
                else
                {
                    tree.Insert(target_pos, node);
                }
            }
        }

        private class TNode
        {
            /// <summary>
            /// 籤號
            /// </summary>
            public int No { get; set; }
            /// <summary>
            /// 是否為偶數
            /// </summary>
            public bool IsEven { get; set; }
        }

        /// <summary>
        /// 取得場次別名
        /// </summary>
        private static string GetBattleAlias(int value)
        {
            if (value == 2)
            {
                return "Final";
            }
            else if (value == 4)
            {
                return "Semi-Final";
            }
            else if (value == 8)
            {
                return "Top Eight";
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// 取得組面名稱
        /// </summary>
        private static string GetSurfaceName(int value, string type = "")
        {
            string[] arr;

            switch(type)
            {
                default:
                    arr = new string[] { "", "West", "East" };
                    break;
            }

            if (value > -1 && value < (arr.Length))
            {
                return arr[value];
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// 取得最大輪次代碼
        /// </summary>
        private static int GetMaxRingCode(int value, int code, int sum)
        {
            if (value > sum)
            {
                return GetMaxRingCode(value, code, code * sum);
            }
            else
            {
                return sum;
            }
        }
    }

}
