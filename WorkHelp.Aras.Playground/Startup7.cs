﻿using DocumentFormat.OpenXml.Drawing;
using DocumentFormat.OpenXml.Presentation;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml.Wordprocessing;
using Google.Apis.Auth;
using Google.Apis.Auth.OAuth2.Requests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using System.Xml.XPath;
using WorkHelp.Extension;

namespace WorkHelp.Aras.Playground.ArasModules
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class Startup7 : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
            目的: 中華跆協資料轉檔-個人會員
            日期: 
                - 2021/03/04: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
            
            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]In_CTA_VipMember";

            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            Item itmR = this;

            //協會 Resume
            Item itmAResume = GetAdminResume(CCO, strMethodName, inn);
            CCO.Utilities.WriteDebug(strMethodName, "取得協會資料");

            //同步個人會員 XLS
            TransVipMember(CCO, strMethodName, inn, itmAResume);

            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

            return itmR;
        }

        private void UpdateMaxPayYear(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn)
        {
            string sql = @"
                UPDATE t1 SET
	                t1.in_member_last_year = t2.max_year
                FROM
	                IN_RESUME t1
                INNER JOIN
                (
	                SELECT source_id, Max(In_year) AS 'max_year' FROM IN_RESUME_PAY WITH(NOLOCK) GROUP BY source_id
                ) t2
                ON t2.source_id = t1.id
            ";

            inn.applySQL(sql);
        }

        //同步個人會員 XLS
        private void TransVipMember(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmAResume)
        {
            var entities = GetVipMemberListFromXLS(CCO, strMethodName, inn);

            //個人註冊帳號
            string meeting_id = "249FDB244E534EB0AA66C8E9C470E930";

            CCO.Utilities.WriteDebug(strMethodName, "[個人會員]資料轉換: 共 " + entities.Count + " 筆");

            int no = 1;
            foreach (var entity in entities)
            {
                Item itmEntity = entity.Value;
                string in_name = itmEntity.getProperty("in_name", "");
                string in_sno = itmEntity.getProperty("in_sno", "").ToUpper();
                string in_stuff_b1 = itmEntity.getProperty("in_stuff_b1", "").ToUpper();
                CCO.Utilities.WriteDebug(strMethodName, "[個人會員]資料轉換: " + no + ". " + in_name + "(" + in_sno + ")" + "(" + in_stuff_b1 + ")");

                Item itmResume = inn.applySQL("SELECT TOP 1 * FROM IN_RESUME WITH(NOLOCK) WHERE login_name = '" + in_sno + "'");
                if (itmResume.isError())
                {
                    CCO.Utilities.WriteDebug(strMethodName, "SELECT IN_RESUME 發生錯誤");
                    no++;
                    continue;
                }

                Item itmParent = itmAResume;

                itmEntity.setProperty("in_manager_org", itmParent.getProperty("id", ""));
                itmEntity.setProperty("in_manager_name", itmParent.getProperty("in_name", ""));

                if (itmResume.getResult() == "")
                {
                    CCO.Utilities.WriteDebug(strMethodName, "IN_RESUME 不存在");

                    NewRegister(CCO, strMethodName, inn, itmParent, itmEntity, meeting_id);

                    itmResume = inn.applySQL("SELECT * FROM IN_RESUME WITH(NOLOCK) WHERE in_sno = N'" + in_sno + "'");

                    if (itmResume.isError() || itmResume.getResult() == "" || itmResume.getItemCount() != 1)
                    {
                        throw new Exception("創建帳號後，取得成員履歷資料發生錯誤");
                    }

                    BindRole(CCO, strMethodName, inn, itmParent, itmEntity, itmResume);

                    //更新 Resume 資料
                    UpdateNewResume(CCO, strMethodName, inn, itmEntity, meeting_id);

                    AddResumePay(CCO, strMethodName, inn, entity, itmResume);
                }
                else
                {
                    CCO.Utilities.WriteDebug(strMethodName, "IN_RESUME 存在");

                    BindRole(CCO, strMethodName, inn, itmParent, itmEntity, itmResume);

                    //更新 Resume 資料
                    UpdateNewResume(CCO, strMethodName, inn, itmEntity, meeting_id);

                    AddResumePay(CCO, strMethodName, inn, entity, itmResume);
                }

                no++;
            }
        }

        /// <summary>
        /// 建立成員角色
        /// </summary>
        private void BindRole(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmParent, Item itmEntity, Item itmUserResume)
        {
            string group_resume_id = itmParent.getProperty("id", "");

            string in_resume_role = itmEntity.getProperty("in_resume_role", "");
            string in_resume_remark = itmEntity.getProperty("in_resume_remark", "");

            string user_resume_id = itmUserResume.getProperty("id", "");

            if (group_resume_id == "" || user_resume_id == "")
            {
                return;
            }

            bool is_exists = ExistsNewRole(CCO, strMethodName, inn, group_resume_id, user_resume_id, in_resume_role);

            if (is_exists)
            {
                //throw new Exception("成員已擔任該角色");
                return;
            }

            Item itmUserRole = inn.newItem("In_Resume_Resume", "add");
            itmUserRole.setProperty("source_id", group_resume_id);
            itmUserRole.setProperty("related_id", user_resume_id);
            itmUserRole.setProperty("in_resume_role", in_resume_role);
            itmUserRole.setProperty("in_resume_remark", in_resume_remark);
            itmUserRole = itmUserRole.apply();
            if (itmUserRole.isError())
            {
                throw new Exception("建立成員關聯發生錯誤");
            }
        }

        /// <summary>
        /// 更新 Resume 資料
        /// </summary>
        private void UpdateNewResume(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmEntity, string meeting_id)
        {
            string sql = @"
            UPDATE
                IN_MEETING_USER
            SET
                in_creator = N'{#in_creator}'
                , in_creator_sno = N'{#in_creator_sno}'
            WHERE
                source_id = '{#meeting_id}'
                AND in_sno = '{#in_sno}'
            ";

            StringBuilder builder = new StringBuilder(sql);
            builder.Replace("{#meeting_id}", meeting_id);
            builder.Replace("{#in_sno}", itmEntity.getProperty("in_sno", ""));
            builder.Replace("{#in_creator}", "lwu001");
            builder.Replace("{#in_creator_sno}", "lwu001");

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + builder.ToString());

            Item itmSQL = inn.applySQL(builder.ToString());

            if (itmSQL.isError())
            {
                CCO.Utilities.WriteDebug(strMethodName, "更新協助報名者發生錯誤 _# sql: " + sql);
            }

            sql = @"
            UPDATE
                IN_RESUME
            SET
                in_org = N'{#in_org}'
                , in_group = N'{#in_group}'
                , in_current_org = N'{#in_current_org}'
                , in_short_org = N'{#in_short_org}'
                , in_stuff_a1 = N'{#in_stuff_a1}'
                , in_stuff_b1 = N'{#in_stuff_b1}'
                , in_is_teacher = N'{#in_is_teacher}'

                , in_head_coach = N'{#in_head_coach}'
                , in_principal = N'{#in_principal}'
                , in_assistant_coaches = N'{#in_assistant_coaches}'
                , in_member_unit = N'{#in_member_unit}'

                , in_gender = N'{#in_gender}'
                , in_birth = N'{#in_birth}'
                , in_area = N'{#in_area}'

                , in_add = N'{#in_add}'
                , in_add_code = N'{#in_add_code}'
                , in_resident_add = N'{#in_resident_add}'
                , in_resident_add_code = N'{#in_resident_add_code}'
                , in_url = N'{#in_url}'
                , in_email_exam = N'{#in_email_exam}'
                , in_tel_1 = N'{#in_tel_1}'
                , in_tel_2 = N'{#in_tel_2}'
                , in_fax = N'{#in_fax}'

                , in_member_type = N'{#in_member_type}'
                , in_member_role = N'{#in_member_role}'
                , in_member_status = N'{#in_member_status}'

                , in_manager_name = N'{#in_manager_name}'
                , in_manager_org = N'{#in_manager_org}'
                , in_manager_area = N'{#in_manager_area}'

                , in_reg_date = N'{#in_reg_date}'
                , in_change_date = N'{#in_change_date}'

                , in_education = N'{#in_education}'
                , in_work_history = N'{#in_work_history}'
                , in_degree = N'{#in_degree}'
                , in_source = N'{#in_source}'
                , in_note = N'{#in_note}'
            WHERE
                in_sno = N'{#in_sno}'
            ";

            builder = new StringBuilder(sql);
            builder.Replace("{#in_sno}", itmEntity.getProperty("in_sno", ""));

            builder.Replace("{#in_org}", itmEntity.getProperty("in_org", ""));
            builder.Replace("{#in_group}", itmEntity.getProperty("in_group", ""));
            builder.Replace("{#in_current_org}", itmEntity.getProperty("in_current_org", ""));
            builder.Replace("{#in_short_org}", itmEntity.getProperty("in_short_org", ""));
            builder.Replace("{#in_stuff_a1}", itmEntity.getProperty("in_stuff_a1", ""));
            builder.Replace("{#in_stuff_b1}", itmEntity.getProperty("in_stuff_b1", ""));
            builder.Replace("{#in_is_teacher}", itmEntity.getProperty("in_is_teacher", ""));

            //跆委會 & 道館社團
            builder.Replace("{#in_head_coach}", itmEntity.getProperty("in_head_coach", ""));
            builder.Replace("{#in_principal}", itmEntity.getProperty("in_principal", ""));

            //道館社團
            builder.Replace("{#in_assistant_coaches}", itmEntity.getProperty("in_assistant_coaches", ""));
            builder.Replace("{#in_member_unit}", itmEntity.getProperty("in_member_unit", ""));

            //個人會員
            builder.Replace("{#in_gender}", itmEntity.getProperty("in_gender", ""));
            builder.Replace("{#in_birth}", itmEntity.getProperty("in_birth", ""));
            builder.Replace("{#in_area}", itmEntity.getProperty("in_area", ""));

            builder.Replace("{#in_add}", itmEntity.getProperty("in_add", ""));
            builder.Replace("{#in_add_code}", itmEntity.getProperty("in_add_code", ""));
            builder.Replace("{#in_resident_add}", itmEntity.getProperty("in_resident_add", ""));
            builder.Replace("{#in_resident_add_code}", itmEntity.getProperty("in_resident_add_code", ""));
            builder.Replace("{#in_url}", itmEntity.getProperty("in_url", ""));
            builder.Replace("{#in_email_exam}", itmEntity.getProperty("in_email_exam", ""));
            builder.Replace("{#in_tel_1}", itmEntity.getProperty("in_tel_1", ""));
            builder.Replace("{#in_tel_2}", itmEntity.getProperty("in_tel_2", ""));
            builder.Replace("{#in_fax}", itmEntity.getProperty("in_fax", ""));

            builder.Replace("{#in_member_type}", itmEntity.getProperty("in_member_type", ""));
            builder.Replace("{#in_member_role}", itmEntity.getProperty("in_member_role", ""));
            builder.Replace("{#in_member_status}", itmEntity.getProperty("in_member_status", ""));

            builder.Replace("{#in_manager_name}", itmEntity.getProperty("in_manager_name", ""));
            builder.Replace("{#in_manager_org}", itmEntity.getProperty("in_manager_org", ""));
            builder.Replace("{#in_manager_area}", itmEntity.getProperty("in_manager_area", ""));

            builder.Replace("{#in_reg_date}", itmEntity.getProperty("in_reg_date", ""));
            builder.Replace("{#in_change_date}", itmEntity.getProperty("in_change_date", ""));

            builder.Replace("{#in_education}", itmEntity.getProperty("in_education", ""));
            builder.Replace("{#in_work_history}", itmEntity.getProperty("in_work_history", ""));
            builder.Replace("{#in_degree}", itmEntity.getProperty("in_degree", ""));
            builder.Replace("{#in_source}", itmEntity.getProperty("in_source", ""));
            builder.Replace("{#in_note}", itmEntity.getProperty("in_note", ""));

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + builder.ToString());

            itmSQL = inn.applySQL(builder.ToString());

            if (itmSQL.isError())
            {
                CCO.Utilities.WriteDebug(strMethodName, "更新成員履歷發生錯誤 _# sql: " + sql);
            }
        }

        /// <summary>
        /// 成員角色是否已存在
        /// </summary>
        private bool ExistsNewRole(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string group_resume_id, string user_resume_id, string new_resume_role)
        {
            Item itmUserRole = inn.newItem("In_Resume_Resume", "get");
            itmUserRole.setProperty("source_id", group_resume_id);
            itmUserRole.setProperty("related_id", user_resume_id);
            itmUserRole.setProperty("in_resume_role", new_resume_role);
            itmUserRole = itmUserRole.apply();
            return itmUserRole.getItemCount() == 1;
        }
        /// <summary>
        /// 會費繳納過檔
        /// </summary>
        private void AddResumePay(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, TVipMember entity, Item itmResume)
        {
            //Item itmResume = null;
            //if (itmResumeSource != null)
            //{
            //    itmResume = itmResumeSource;
            //}
            //else
            //{
            //    inn.newItem("In_Resume", "get");
            //    itmResume.setProperty("login_name", entity.login_name);
            //    itmResume = itmResume.apply();
            //}

            //if (itmResume.isError() || itmResume.getItemCount() != 1)
            //{
            //    throw new Exception("個人會員費用過檔: 發生錯誤");
            //}

            string resume_id = itmResume.getProperty("id", "");

            //刪除會費繳納
            string aml = @"<AML><Item type='IN_RESUME_PAY' action='delete' where=""source_id='{#source_id}'""/></AML>";
            aml = aml.Replace("{#source_id}", resume_id);
            inn.applyAML(aml);

            for (int i = 0; i < entity.PayList.Count; i++)
            {
                Item item = entity.PayList[i];

                var west = GetWestDay(CCO, strMethodName, inn, item);

                string inn_date = GetDateTimeValue(west.FullDay, "yyyy-MM-ddTHH:mm:ss");

                Item itmRPay = inn.newItem("In_Resume_Pay");

                itmRPay.setAttribute("where", "[In_Resume_Pay].source_id = '" + resume_id + "' AND [In_Resume_Pay].in_year = '" + west.Year + "'");

                itmRPay.setProperty("source_id", resume_id);
                itmRPay.setProperty("in_year", west.Year);
                itmRPay.setProperty("in_date", inn_date);
                itmRPay.setProperty("in_amount", "");
                itmRPay.setProperty("in_note", west.Memo.Trim());

                itmRPay = itmRPay.apply("merge");

                if (itmRPay.isError())
                {
                    CCO.Utilities.WriteDebug(strMethodName, "[個人會員費用過檔]過檔失敗 _# dom: " + itmRPay.dom.InnerXml);
                }
            }
        }

        /// <summary>
        /// 新建與會者 (註冊模式)
        /// </summary>
        private void NewRegister(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmParent, Item itmEntity, string meeting_id)
        {
            string in_sno = itmEntity.getProperty("in_sno", "");

            string survey_type = "1";
            string method = "register_meeting";

            List<Item> lstSurveys = GetMeetingSurveyList(CCO, strMethodName, inn, meeting_id);
            string parameters = GetParamValues(lstSurveys, itmEntity);
            string isUserId = itmParent.getProperty("in_user_id", "");
            string isIndId = itmParent.getProperty("owned_by_id", "");

            string aml = ""
                + "<surveytype>" + survey_type + "</surveytype>"
                + "<meeting_id>" + meeting_id + "</meeting_id>"
                + "<agent_id>" + "</agent_id>"
                + "<email>" + in_sno + "</email>"
                + "<isUserId>" + isUserId + "</isUserId>"
                + "<isIndId>" + isIndId + "</isIndId>"
                + "<method>" + method + "</method>"
                + "<parameter>" + parameters + "</parameter>"
                + "";

            //CCO.Utilities.WriteDebug(strMethodName, "aml: " + aml);

            Item itmMethod = inn.applyMethod("In_meeting_register", aml);

            if (itmMethod.isError())
            {
                throw new Exception("創建成員帳號發生錯誤");
            }
        }


        /// <summary>
        /// 個人會員轉檔清單
        /// </summary>
        private List<TVipMember> GetVipMemberListFromXLS(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn)
        {
            List<TVipMember> entities = new List<TVipMember>();
            try
            {
                //手動將 個人及一般團體會員(e化用).xlsx 匯入至 PLMCTA
                string sql = "SELECT * FROM CTA_MEMBER_20210303";
                //sql = "SELECT * FROM CTA_MEMBER_20210303 WHERE T18 IN ('A220294627')";

                string[] err_sid = new string[]
                {
                    "",
                };

                Item items = inn.applySQL(sql);

                int count = items.getItemCount();

                for (int i = 0; i < count; i++)
                {
                    Item item = items.getItemByIndex(i);

                    string in_stuff_b1 = "V" + item.getProperty("t1", "").PadLeft(5, '0');

                    string src_reg_date = item.getProperty("t2", "");
                    string in_reg_date = "";
                    TWest west = GetWestDay(CCO, strMethodName, inn, src_reg_date);
                    if (!west.Ignore && !west.IsError)
                    {
                        in_reg_date = GetDateTimeValue(west.FullDay, "yyyy-MM-ddTHH:mm:ss");
                    }

                    string in_name = item.getProperty("t16", "");
                    string in_gender = item.getProperty("t17", "");
                    string login_name = item.getProperty("t18", "");
                    string in_birth = item.getProperty("t19", "");


                    if (err_sid.Contains(login_name))
                    {
                        continue;
                    }


                    string in_sno = login_name;
                    string in_org = "0";
                    string in_group = in_name;
                    string in_current_org = in_name;
                    string in_short_org = in_name;
                    string in_stuff_a1 = in_name;

                    //沒有生日資料 無法新增 (因此暫時設定為 1900/01/01)
                    if (in_birth == "")
                    {
                        in_birth = "1899-12-31 16:00:00";
                    }

                    string in_area = "";
                    string in_email = "";


                    string in_member_status = item.getProperty("t13", "");
                    string in_degree = item.getProperty("t15", "").PadRight(4, '0');
                    string in_education = item.getProperty("t20", "");
                    string in_work_history = item.getProperty("t21", "");

                    string in_resident_add_code = item.getProperty("t22", "");
                    string in_resident_add = item.getProperty("t23", "");

                    string in_add_code = item.getProperty("t24", "");
                    string in_add = item.getProperty("t25", "");

                    string in_tel_1 = item.getProperty("t26", "");
                    string in_tel = item.getProperty("t27", "");
                    string in_note = item.getProperty("t28", "");

                    Item itmEntity = inn.newItem();
                    itmEntity.setProperty("in_name", in_name);
                    itmEntity.setProperty("in_sno", in_sno);
                    itmEntity.setProperty("login_name", login_name);
                    itmEntity.setProperty("in_org", in_org);
                    itmEntity.setProperty("in_is_teacher", "1");

                    itmEntity.setProperty("in_group", in_group);
                    itmEntity.setProperty("in_current_org", in_current_org);
                    itmEntity.setProperty("in_short_org", in_short_org);
                    itmEntity.setProperty("in_stuff_a1", in_stuff_a1);
                    itmEntity.setProperty("in_stuff_b1", in_stuff_b1);

                    itmEntity.setProperty("in_gender", in_gender);
                    itmEntity.setProperty("in_birth", in_birth);

                    itmEntity.setProperty("in_add", in_add);
                    itmEntity.setProperty("in_add_code", in_add_code);

                    itmEntity.setProperty("in_area", in_area);
                    itmEntity.setProperty("in_resident_add", in_resident_add);
                    itmEntity.setProperty("in_resident_add_code", in_resident_add_code);

                    itmEntity.setProperty("in_email", in_email);

                    itmEntity.setProperty("in_tel", in_tel);
                    itmEntity.setProperty("in_tel_1", in_tel_1);

                    itmEntity.setProperty("in_reg_date", in_reg_date);

                    itmEntity.setProperty("in_education", in_education);
                    itmEntity.setProperty("in_work_history", in_work_history);
                    itmEntity.setProperty("in_degree", in_degree);
                    itmEntity.setProperty("in_source", "xls");
                    itmEntity.setProperty("in_note", in_note);

                    itmEntity.setProperty("in_member_type", "vip_mbr");
                    itmEntity.setProperty("in_member_role", "sys_9999");
                    itmEntity.setProperty("in_member_status", in_member_status);

                    itmEntity.setProperty("in_resume_role", "org_vip_mbr");
                    itmEntity.setProperty("in_resume_remark", "個人會員");


                    TVipMember entity = new TVipMember
                    {
                        login_name = login_name,
                        Value = itmEntity,
                        PayList = new List<Item>()
                    };

                    //2018
                    AddPay(inn, entity.PayList, item, "2018", "107年度", "t3", "t4");
                    //2019
                    AddPay(inn, entity.PayList, item, "2019", "108年度", "t5", "t6");
                    //2020
                    AddPay(inn, entity.PayList, item, "2020", "109年度", "t7", "t8");
                    //2021
                    AddPay(inn, entity.PayList, item, "2021", "110年度", "t9", "t10");
                    //2022
                    AddPay(inn, entity.PayList, item, "2022", "111年度", "t11", "t12");

                    entities.Add(entity);
                }
            }
            catch (Exception ex)
            {
                CCO.Utilities.WriteDebug(strMethodName, "取得 XLS 資料 _# error: " + ex.Message);
            }

            return entities;
        }

        private void AddPay(Innovator inn, List<Item> list, Item item, string in_year, string tw_year, string p1, string p2)
        {
            //  單據流水號 or 備註
            string v1 = item.getProperty(p1, "").Trim();
            //  繳費情形
            string v2 = item.getProperty(p2, "").Trim();

            if (v1 == "" && v2 == "")
            {
                return;
            }

            if (v1 != "")
            {
                v1 = v1.PadLeft(6, '0');
            }

            Item itmPay = inn.newItem();
            itmPay.setProperty("in_year", in_year);
            itmPay.setProperty("in_note", v1);
            itmPay.setProperty("in_date", v2);
            itmPay.setProperty("tw_year", tw_year);

            list.Add(itmPay);
        }

        #region 資料結構
        private class TVipMember
        {
            public string login_name { get; set; }
            public Item Value { get; set; }
            public List<Item> PayList { get; set; }
        }

        private class TWest
        {
            public bool Ignore { get; set; }
            public bool IsError { get; set; }

            public string FullDay { get; set; }
            public string Year { get; set; }
            public string Memo { get; set; }
        }
        #endregion 資料結構

        #region 資料存取

        /// <summary>
        /// 取得協會 Resume
        /// </summary>
        private Item GetAdminResume(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn)
        {
            string sql = "SELECT TOP 1 * FROM IN_RESUME WITH(NOLOCK) WHERE in_sno = N'M001'";

            return inn.applySQL(sql);
        }

        /// <summary>
        /// 取得 ALL Resume
        /// </summary>
        private Item GetAllResume(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn)
        {
            Dictionary<string, Item> map = new Dictionary<string, Item>();
            string sql = "SELECT * FROM IN_RESUME WITH(NOLOCK)";
            return inn.applySQL(sql);
        }

        /// <summary>
        /// 取得問項清單
        /// </summary>
        private List<Item> GetMeetingSurveyList(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string meeting_id)
        {
            string sql = @"
            SELECT
                t3.id,
                t3.in_property
            FROM
                IN_MEETING t1 WITH(NOLOCK)
            INNER JOIN
                IN_MEETING_SURVEYS t2 WITH(NOLOCK) ON t2.source_id = t1.id
            INNER JOIN
                IN_SURVEY t3 WITH(NOLOCK) ON t3.id = t2.related_id
            WHERE
                t1.id = '{#meeting_id}'
                AND ISNULL(in_property, '') <> ''
            ORDER BY
                t2.sort_order
            ";
            sql = sql.Replace("{#meeting_id}", meeting_id);

            Item items = inn.applySQL(sql);

            if (items.isError() || items.getItemCount() <= 0)
            {
                throw new Exception("創建成員帳號發生錯誤(問項)");
            }

            List<Item> list = new List<Item>();

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                list.Add(items.getItemByIndex(i));
            }

            return list;
        }

        private Dictionary<string, Item> MapAllResume(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn)
        {
            Item itmAllResumes = GetAllResume(CCO, strMethodName, inn);
            return GetMap(itmAllResumes, "in_sno");
        }

        /// <summary>
        /// 轉換字典
        /// </summary>
        private Dictionary<string, Item> GetMap(Item items, string property)
        {
            Dictionary<string, Item> map = new Dictionary<string, Item>();

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string key = item.getProperty(property, "").ToUpper();

                if (map.ContainsKey(key))
                {
                    throw new Exception("鍵值重複: " + key);
                }
                else
                {
                    map.Add(key, item);
                }
            }

            return map;
        }
        #endregion 資料存取

        /// <summary>
        /// 組成 Url 參數
        /// </summary>
        /// <param name="list">問項清單</param>
        /// <param name="itmEntity">與會者資料</param>
        private string GetParamValues(List<Item> list, Item itmEntity)
        {
            string result = "";
            int count = list.Count;
            for (int i = 0; i < count; i++)
            {
                Item item = list[i];
                string id = item.getProperty("id", "");
                string property = item.getProperty("in_property", "");

                string value = itmEntity.getProperty(property, "");

                if (value == "")
                {
                    continue;
                }

                if (property == "in_birth")
                {
                    value = GetDateTimeValue(value, "yyyy-MM-dd");
                }

                if (result != "")
                {
                    result += "&amp;";
                }

                result += id + "=" + value;
            }
            return result;
        }

        private TWest GetWestDay(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string value)
        {
            if (value == "")
            {
                return new TWest { Ignore = true };
            }

            var str_arr = value.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
            if (str_arr == null)
            {
                return new TWest { IsError = true };
            }

            if (str_arr.Length != 3)
            {
                return new TWest { IsError = true };
            }

            int[] arr = str_arr.Select(n => Convert.ToInt32(n)).ToArray();

            int y = arr[0] < 1911 ? arr[0] + 1911 : arr[0];
            int m = arr[1];
            int d = arr[2];

            return new TWest
            {
                Year = y.ToString(),
                FullDay = y + "-" + m.ToString().PadLeft(2, '0') + "-" + d.ToString().PadLeft(2, '0'),
            };
        }

        private TWest GetWestDay(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item item)
        {
            string memo = item.getProperty("memo", "");
            string in_year = item.getProperty("in_year", "");
            string in_note = item.getProperty("in_note", "");
            string in_date = item.getProperty("in_date", "");

            var result = new TWest
            {
                Year = in_year,
                Memo = in_note,
            };

            if (in_date == "")
            {
                return result;
            }

            if (!in_date.Contains("/"))
            {
                result.Memo += " " + in_date;
                return result;
            }

            var str_arr = in_date.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
            if (str_arr == null)
            {
                result.Memo += " " + in_date;
                return result;
            }

            if (str_arr.Length != 3)
            {
                CCO.Utilities.WriteDebug(strMethodName, in_date);
                result.Memo += " " + in_date;
                return result;
            }

            int[] arr = str_arr.Select(n => Convert.ToInt32(n)).ToArray();

            int y = arr[0] < 1911 ? arr[0] + 1911 : arr[0];
            int m = arr[1];
            int d = arr[2];

            if (str_arr[2].Length == 4)
            {
                // 4/15/2020
                y = arr[2];
                m = arr[0];
                d = arr[1];
            }

            result.FullDay = y + "-" + m.ToString().PadLeft(2, '0') + "-" + d.ToString().PadLeft(2, '0');

            return result;
        }


        private int GetIntValue(string value)
        {
            int result = 0;
            if (Int32.TryParse(value, out result))
            {
                return result;
            }
            else
            {
                return 0;
            }
        }

        private string GetDateTimeValue(string value, string format = "yyyy-MM-dd", int hours = 0)
        {
            if (value == null || value == "") return "";

            string day = value.Replace("/", "-");

            DateTime dt = DateTime.Now;
            if (DateTime.TryParse(day, out dt))
            {
                return dt.AddHours(hours).ToString(format);
            }
            else
            {
                return value;
            }
        }
    }
}