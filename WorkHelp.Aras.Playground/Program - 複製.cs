﻿using DocumentFormat.OpenXml.EMMA;
using DocumentFormat.OpenXml.Office2010.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using WorkHelp.Aras.Playground.ArasModules;

namespace WorkHelp.Aras.Playground
{
    /// <summary>
    /// 程序
    /// </summary>
    internal class Program
    {
        /// <summary>
        /// 主入口
        /// </summary>
        /// <param name="args"></param>
        private static void Main(string[] args)
        {
            int player_count = 67;
            int node_count = GetNodeCount(player_count); //樹狀基數

            List<TNode> tree = GetBaseTree(node_count, player_count);
            BindBattle(tree);
            AppendOtherNodes(tree, player_count);
            AfterGenerateTree(tree);

            //WriteFirstTree(tree, 1);

            List<TNode> tree2 = GetDeepTree(tree, false);
            AppendBattleAgain(tree2);
            WriteDeepTree(tree2, 2);

            List<TNode> tree3 = GetDeepTree(tree2, true);
            AppendBattleAgain(tree3);
            WriteDeepTree(tree3, 3);

            List<TNode> tree4 = GetDeepTree(tree3, true);
            AppendBattleAgain(tree4);
            WriteDeepTree(tree4, 4);

            List<TNode> tree5 = GetDeepTree(tree4, true);
            AppendBattleAgain(tree5);
            WriteDeepTree(tree5, 5);

            List<TNode> tree6 = GetDeepTree(tree5, true);
            AppendBattleAgain(tree6);
            WriteDeepTree(tree6, 6);

            Console.ReadKey();
        }

        private static void WriteFirstTree(List<TNode> tree, int ring)
        {
            foreach (var node in tree)
            {
                string id = node.Id.ToString();
                string signNo = node.SignNo.ToString().PadLeft(3, ' ');
                string foot = node.SignFoot.ToString();
                string battle = node.BattleNode.SignNo.ToString().PadLeft(3, ' ');
                string surface = GetSurfaceEng(node.SurfaceCode);
                string code = node.RowCode.ToString();

                Console.WriteLine($"Id: {node.Id}，籤號: {signNo}，腳位: {foot}，對戰籤號: {battle}，列代碼: {code}");
            }
        }

        private static void WriteDeepTree(List<TNode> tree, int ring)
        {
            Console.WriteLine("");
            Console.WriteLine(" ================== 第 " + ring + " 輪 ================== ");
            Console.WriteLine("");

            foreach (var node in tree)
            {
                string id = node.Id.ToString();
                string display = node.DisplayName;
                string signNo = node.SignNo.ToString().PadLeft(3, ' ');
                string foot = node.SignFoot.ToString();
                string battle_again = node.NeedBattleAgain.ToString();
                string code = node.RowCode.ToString();

                Console.WriteLine($"Id: {node.Id}，{display}，腳位: {foot}，要再打: {battle_again}，新腳位: {node.NewSignFoot}，場次代碼: {code}");
            }
        }


        /// <summary>
        /// 取得組面
        /// </summary>
        private static string GetSurfaceEng(int code)
        {
            switch (code)
            {
                case 0: return "A組";
                case 1: return "B組";
                case 2: return "C組";
                case 3: return "D組";
                case 4: return "E組";
                default: return "over";
            }
        }

        /// <summary>
        /// 建立基本節點
        /// </summary>
        private static List<TNode> GetBaseTree(int max_no, int player_count)
        {
            List<TNode> result = new List<TNode>();

            for (int no = 1; no <= max_no; no++)
            {
                TNode node = GetNewNode(no);
                node.IsPlayer = node.SignNo <= player_count;
                AppendTreeNode(result, node);
            }
            return result;
        }

        /// <summary>
        /// 連結節點
        /// </summary>
        /// <param name="tree"></param>
        private static void BindBattle(List<TNode> tree)
        {
            int face_count = 32;

            for (int i = 0; i < tree.Count; i++)
            {
                TNode item = tree[i];
                item.SurfaceCode = i / face_count;

                if (item.IsEven)
                {
                    item.BattleNode = tree[i - 1];
                }
                else
                {
                    item.BattleNode = tree[i + 1];
                }
            }
        }

        /// <summary>
        /// 取得可視節點
        /// </summary>
        /// <param name="no">籤號</param>
        /// <returns></returns>
        private static TNode GetNewNode(int no)
        {
            TNode result = new TNode();
            result.IsEven = no % 2 == 0;

            result.SignNo = no;
            result.SignFoot = result.IsEven ? 2 : 1;

            result.IsPlayer = false;
            result.IsFake = false;
            result.RingCode = 1;

            return result;
        }

        /// <summary>
        /// 附加節點
        /// </summary>
        private static void AppendTreeNode(List<TNode> tree, TNode node)
        {
            if (tree.Count == 0)
            {
                tree.Add(node);
            }
            else
            {
                int sum = GetSumCode(node.SignNo);
                int target_no = sum - node.SignNo;
                int target_pos = tree.FindIndex(x => x.SignNo == target_no);
                if (node.IsEven)
                {
                    tree.Insert(target_pos + 1, node);
                }
                else
                {
                    tree.Insert(target_pos, node);
                }
            }
        }

        /// <summary>
        /// 非正常節點
        /// </summary>
        private static void AppendOtherNodes(List<TNode> tree, int max_no)
        {
            int min_no = tree.Count + 1;
            for (int no = min_no; no <= max_no; no++)
            {
                TNode node = GetNewNode(no);
                node.IsPlayer = true;
                ChangeTreeNode(tree, node);
            }
        }

        private static void AfterGenerateTree(List<TNode> tree)
        {
            int index = 0;
            for (int i = 0; i < tree.Count; i++)
            {
                TNode item = tree[i];
                item.Id = item.RingCode * 100 + i + 1;

                if (!item.IsEven)
                {
                    index++;
                }

                item.RowCode = index;
            }
        }

        /// <summary>
        /// 變更節點結構
        /// </summary>
        private static void ChangeTreeNode(List<TNode> tree, TNode other_node)
        {
            int sum = GetSumCode(other_node.SignNo);

            //對戰目標 (Ex 籤號 64)
            int target_no = sum - other_node.SignNo;
            TNode target_node = tree.Find(x => x.SignNo == target_no);

            //處理對戰目標的原對手 (Ex 籤號 1)
            TNode related_node = target_node.BattleNode;
            AppendNewBattleNode(tree, related_node, sum);

            other_node.BattleNode = target_node;
            other_node.SurfaceCode = target_node.SurfaceCode;
            other_node.NeedBattleAgain = true;

            target_node.BattleNode = other_node;
            target_node.NeedBattleAgain = true;

            int pos = tree.FindIndex(x => x.SignNo == target_node.SignNo);

            if (other_node.IsEven)
            {
                tree.Insert(pos + 1, other_node);
            }
            else
            {
                tree.Insert(pos, other_node);
            }
        }

        /// <summary>
        /// 附加假節點
        /// </summary>
        private static void AppendNewBattleNode(List<TNode> tree, TNode node, int sum)
        {
            //給新的對手 對戰目標的原對手 (Ex 給籤號 1 指向新對手 fake)
            TNode fake = GetNewNode(sum - node.SignNo);
            fake.IsPlayer = false;
            fake.IsFake = true;
            fake.BattleNode = node;
            fake.SurfaceCode = node.SurfaceCode;
            fake.NeedBattleAgain = true;

            node.BattleNode = fake;
            node.NeedBattleAgain = true;

            int pos = tree.FindIndex(x => x.SignNo == node.SignNo);

            if (fake.IsEven)
            {
                tree.Insert(pos + 1, fake);
            }
            else
            {
                tree.Insert(pos, fake);
            }
        }


        private static void AppendBattleAgain(List<TNode> tree)
        {
            List<TNode> search_list = new List<TNode>();
            for (int i = 0; i < tree.Count; i++)
            {
                TNode node = tree[i];
                if (!node.NeedBattleAgain || node.SignNo == -1)
                {
                    continue;
                }

                TNode search_node = new TNode();
                search_node.Id = node.Id;
                search_node.SignNo = node.SignNo;
                search_node.SignFoot = node.SignFoot;
                search_node.RingCode = node.RingCode;

                if (search_node.SignFoot == 1)
                {
                    search_node.Battle_Id = tree[i + 1].Id;
                }
                else
                {
                    search_node.Battle_Id = tree[i - 1].Id;
                }

                search_list.Add(search_node);
            }

            for (int i = 0; i < search_list.Count; i++)
            {
                TNode search_node = search_list[i];
                int pos = tree.FindIndex(x => x.Id == search_node.Battle_Id);

                TNode node = new TNode();
                node.DisplayName = "{#場次}勝者";
                node.IsPlayer = false;
                node.IsFake = false;
                node.NeedBattleAgain = false;
                node.RingCode = search_node.RingCode;
                node.SignFoot = -1;
                node.Id = node.RingCode * 100 + (tree.Count + 1);

                if (search_node.SignFoot == 1)
                {
                    tree.Insert(pos + 1, node);
                }
                else
                {
                    tree.Insert(pos, node);
                }
            }

            List<TNode> real_list = new List<TNode>();
            for (int i = 0; i < tree.Count; i++)
            {
                TNode node = tree[i];
                if (!node.NeedBattleAgain)
                {
                    real_list.Add(node);
                }
            }

            int index = 0;
            for (int i = 0; i < real_list.Count; i++)
            {
                int current_number = i + 1;
                TNode node = real_list[i];
                node.IsEven = current_number % 2 == 0;

                if (!node.IsEven)
                {
                    index++;
                }

                if (node.IsEven)
                {
                    node.NewSignFoot = 2;
                    node.BattleNode = real_list[i - 1];
                }
                else
                {
                    node.NewSignFoot = 1;
                    node.BattleNode = real_list[i + 1];
                }
                node.RowCode = index;
            }
        }

        private static List<TNode> GetDeepTree(List<TNode> source, bool filterAgain)
        {
            List<TNode> result = new List<TNode>();
            for (int i = 0; i < source.Count; i++)
            {
                TNode n1 = source[i];
                TNode n2 = n1.BattleNode;
                if (n1.HasChecked)
                {
                    continue;
                }
                if (filterAgain && n1.NeedBattleAgain)
                {
                    continue;
                }

                n1.HasChecked = true;
                n2.HasChecked = true;

                AppendDeepNode(result, n1, n2);
            }

            return result;
        }

        private static void AppendDeepNode(List<TNode> tree, TNode n1, TNode n2)
        {
            bool needBattleAgain = false;
            if (n1.NeedBattleAgain || n2.NeedBattleAgain)
            {
                needBattleAgain = true;
            }

            bool isPlayer = false;
            bool isBattle = false;
            int signNo = -1;
            int signFoot = -1;
            int ringCode = n1.RingCode;
            string displayName = "";

            if (n1.IsPlayer && n2.IsPlayer)
            {
                displayName = "場次勝者";
                isBattle = true;
            }
            else if (n1.IsBattle || n2.IsBattle)
            {
                displayName = "場次勝者";
                isBattle = true;
            }
            else if (n1.IsPlayer)
            {
                signNo = n1.SignNo;
                signFoot = n1.SignFoot;
                displayName = "籤號：" + signNo.ToString();
                isPlayer = true;
                isBattle = false;
            }
            else if (n2.IsPlayer)
            {
                signNo = n2.SignNo;
                signFoot = n2.SignFoot;
                displayName = "籤號：" + signNo.ToString();
                isPlayer = true;
                isBattle = false;
            }
            else
            {
                displayName = "場次勝者";
                isBattle = true;
            }

            TNode node = new TNode();
            node.DisplayName = displayName;
            node.SignNo = signNo;
            node.SignFoot = signFoot;

            node.IsPlayer = isPlayer;
            node.IsFake = false;
            node.IsBattle = isBattle;

            node.NeedBattleAgain = needBattleAgain;
            node.RingCode = ringCode + 1;

            node.Id = node.RingCode * 100 + (tree.Count + 1);

            tree.Add(node);
        }

        /// <summary>
        /// 取得基數
        /// </summary>
        private static int GetNodeCount(int count)
        {
            if (count <= 2) return 2;
            if (count <= 4) return 4;
            if (count <= 8) return 8;
            if (count <= 16) return 16;
            if (count <= 32) return 32;
            if (count <= 40) return 32; // 擠一下
            if (count <= 64) return 64;
            if (count <= 72) return 64; // 擠一下
            if (count <= 128) return 128;
            return 256;
        }

        /// <summary>
        /// 取得對戰籤號加總
        /// </summary>
        private static int GetSumCode(int index)
        {
            if (index <= 2) return 3;
            if (index <= 4) return 5;
            if (index <= 8) return 9;
            if (index <= 16) return 17;
            if (index <= 32) return 33;
            if (index <= 64) return 65;
            if (index <= 128) return 129;
            if (index <= 256) return 257;
            if (index <= 512) return 513;
            return 1025;
        }

        /// <summary>
        /// 自訂節點類型
        /// </summary>
        private class TNode
        {
            /// <summary>
            /// 編號
            /// </summary>
            public int Id {get; set; }

            /// <summary>
            /// 籤號
            /// </summary>
            public int SignNo { get; set; }

            /// <summary>
            /// 籤腳
            /// </summary>
            public int SignFoot { get; set; }

            /// <summary>
            /// 新籤腳
            /// </summary>
            public int NewSignFoot { get; set; }

            /// <summary>
            /// 對戰目標
            /// </summary>
            public TNode BattleNode { get; set; }

            /// <summary>
            /// 是否為選手
            /// </summary>
            public bool IsPlayer { get; set; }

            /// <summary>
            /// 是否為假目標
            /// </summary>
            public bool IsFake { get; set; }

            /// <summary>
            /// 是否為偶數
            /// </summary>
            public bool IsEven { get; set; }

            /// <summary>
            /// 組面代碼
            /// </summary>
            public int SurfaceCode { get; set; }

            /// <summary>
            /// 資料列代碼
            /// </summary>
            public int RowCode { get; set; }

            /// <summary>
            /// 輪次代碼
            /// </summary>
            public int RingCode { get; set; }

            /// <summary>
            /// 名稱
            /// </summary>
            public string DisplayName { get; set; }

            /// <summary>
            /// 是否已檢查
            /// </summary>
            public bool HasChecked { get; set; }

            /// <summary>
            /// 是否需要繼續往下打
            /// </summary>
            public bool IsNeedBattle { get; set; }

            /// <summary>
            /// 需要多打一場
            /// </summary>
            public bool NeedBattleAgain { get; set; }

            /// <summary>
            /// 對戰代號
            /// </summary>
            public int Battle_Id { get; set; }

            /// <summary>
            /// 對戰勝者
            /// </summary>
            public bool IsBattle { get; set; }
        }


    }
}