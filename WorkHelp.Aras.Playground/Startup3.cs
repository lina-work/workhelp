﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace WorkHelp.Aras.Playground.ArasModules
{
    /// <summary>
    /// 啟動
    /// </summary>
    public class Startup3 : Item
    {
        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = new Server.Core.CallContext();
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
            目的: 繳費單查詢
            日期: 2021-02-19 創建 (Alan)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]in_finance_dashboard_payment";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "input dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                mode = itmR.getProperty("mode", ""),
                member_type = itmR.getProperty("type", ""),
                title = itmR.getProperty("in_title", ""),
                title_name = itmR.getProperty("in_title_name", ""),
                pay_bool = itmR.getProperty("in_pay_bool", ""),
                filter = itmR.getProperty("in_filter", ""),
                date_type = itmR.getProperty("in_date_type", ""),
                date_s = itmR.getProperty("in_date_s", ""),
                date_e = itmR.getProperty("in_date_e", "")
            };

            //取得登入者資訊
            string sql = "SELECT * FROM IN_RESUME WITH(NOLOCK) WHERE in_user_id = N'" + inn.getUserID() + "'";
            Item itmLoginResume = inn.applySQL(sql);
            if (IsError(itmLoginResume))
            {
                throw new Exception("登入者履歷異常");
            }

            //權限檢核
            Item itmPermit = inn.applyMethod("In_CheckIdentity", "<method>" + strMethodName + "</method><code>ALL</code>");
            bool isMeetingAdmin = itmPermit.getProperty("isMeetingAdmin", "") == "1";

            Item itmResumes = null;
            Item items = null;
            Item items2 = null;

            string view_resume_sno = itmLoginResume.getProperty("in_sno", "");

            if (isMeetingAdmin)
            {
                itmResumes = GetPaymentList(cfg, "", 0);
                items = GetTitleGroup(cfg, "", 0);
                items2 = GetPayBoolGroup(cfg, "", 0);
            }
            else
            {
                itmResumes = GetPaymentList(cfg, view_resume_sno, 2);
                items = GetTitleGroup(cfg, view_resume_sno, 2);
                items2 = GetPayBoolGroup(cfg, view_resume_sno, 2);
            }


            if (cfg.mode == "xls")
            {
                Export(cfg, itmResumes, itmR);
            }
            else
            {
                if (itmResumes == null || !IsError(itmResumes, isSingle: false))
                {
                    Query(cfg, itmResumes, itmPermit, itmR);
                }

                //下拉清單 項目名稱
                AppendMenu(cfg, items, "inn_title", itmR);
                AppendMenu(cfg, items2, "inn_pay_bool", itmR);
            }

            return itmR;
        }

        //取得成員
        private Item GetPaymentList(TConfig cfg, string view_filter, int type)
        {
            string condition = "WHERE 1 = 1";

            if (type == 1)
            {
                condition += " AND t4.in_manager_org = N'" + view_filter + "'";
            }
            else if (type == 2)
            {
                condition += " AND t4.in_sno = N'" + view_filter + "'";
            }

            if (cfg.title != "All" && cfg.title != "")
            {
                condition += " AND (t2.id = N'" + cfg.title + "' OR t3.id = N'" + cfg.title + "')";
            }

            if (cfg.pay_bool != "All" && cfg.title != "")
            {
                condition += " AND IIF(in_collection_agency IS NOT NULL AND pay_bool = '未繳費','已繳費.', pay_bool) = N'" + cfg.pay_bool + "'";
            }

            if (cfg.filter != "")
            {
                condition += "AND (t1.item_number LIKE N'%" + cfg.filter + "%' OR t1.in_pay_amount_exp LIKE N'%" + cfg.filter + "%' OR t1.in_pay_date_exp LIKE N'%" + cfg.filter + "%' OR in_pay_date_exp1 like N'%" + cfg.filter + "%' OR in_pay_date_real like N'%" + cfg.filter + "%' OR in_creator like N'%" + cfg.filter + "%' OR in_credit_date like N'%" + cfg.filter + "%' OR IIF(in_collection_agency is not null and pay_bool='未繳費','已繳費.',pay_bool) LIKE N'%" + cfg.filter + "%' OR t2.in_title LIKE N'%" + cfg.filter + "%' OR t4.in_current_org LIKE N'%" + cfg.filter + "%' OR t4.in_tel LIKE N'%" + cfg.filter + "%')";
            }

            if (cfg.date_s != "" || cfg.date_e != "")
            {
                if (cfg.date_s != "" && cfg.date_e != "")
                {
                    condition += " AND CONVERT(varchar(100), DATEADD(hour, 8," + cfg.date_type + "), 23) BETWEEN '" + cfg.date_s + "' AND '" + cfg.date_e + "'";
                }
                else if (cfg.date_s != "")
                {
                    condition += " AND CONVERT(varchar(100), DATEADD(hour, 8," + cfg.date_type + "), 23) >= '" + cfg.date_s + "'";
                }
                else
                {
                    condition += " AND CONVERT(varchar(100), DATEADD(hour, 8," + cfg.date_type + "), 23) <= '" + cfg.date_e + "'";
                }
            }

            string order_by = cfg.mode == "xls"
                ? "in_title, item_number"
                : "item_number DESC";

            string sql = @"
                SELECT * FROM 
                (
                    SELECT 
		                t1.item_number
		                , t1.in_pay_amount_exp
		                , t1.in_pay_date_exp
		                , t1.in_pay_date_exp1
		                , t1.in_pay_date_real
		                , t1.in_creator
		                , t1.in_credit_date
		                , IIF(t1.in_collection_agency IS NOT NULL AND t1.pay_bool = '未繳費','已繳費.', t1.pay_bool) AS 'pay_bool'
		                , 'game' AS 'meeting_type'
		                , t1.in_meeting AS 'meeting_id'
		                , t2.in_title as in_title
		                , t4.in_current_org
		                , t4.in_sno
		                , t4.in_tel
		                , '' AS in_manager_org
                    FROM 
		                IN_MEETING_PAY AS t1 WITH (NOLOCK)
                    LEFT JOIN 
		                IN_MEETING AS t2 WITH (NOLOCK)
		                ON t2.id = t1.in_meeting
                    LEFT JOIN 
		                IN_RESUME AS t4 WITH (NOLOCK)
		                ON t4.in_sno = t1.in_creator_sno
                    {#condition}
                ) AS LIST
                ORDER BY 
	                {#order_by}
            ";

            sql = sql.Replace("{#condition}", condition)
                .Replace("{#order_by}", order_by);

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        //取得項目名稱 Group
        private Item GetTitleGroup(TConfig cfg, string view_filter, int type)
        {
            string condition = "";

            if (type == 1)
            {
                condition = " WHERE t4.in_manager_org = N'" + view_filter + "'";
            }
            else if (type == 2)
            {
                condition = " WHERE t4.in_sno = N'" + view_filter + "'";
            }

            string sql = @"
                SELECT * FROM 
                (
                    SELECT 
					    '全部'			AS 'label' 
					    , 'All'			AS 'value'
                    UNION ALL
                    SELECT 
					    in_title		AS 'label'
					    , meeting_id	AS 'value' 
				    FROM 
                    (
                        SELECT
						    t2.in_title		AS 'in_title'
						    , t1.in_meeting AS 'meeting_id'
                        FROM 
						    IN_MEETING_PAY AS t1 WITH (NOLOCK)
                        LEFT JOIN 
						    in_meeting AS t2 WITH (NOLOCK)
						    ON t2.id = t1.in_meeting
                        LEFT JOIN 
						    IN_RESUME AS T4 WITH (NOLOCK)
						    ON t4.in_sno = t1.in_creator_sno
                        {#condition}
                    ) AS LIST
                    GROUP BY 
					    in_title
					    , meeting_id
                ) AS GList
                ORDER BY (
                    CASE 
					    WHEN label = '全部' THEN -1
					    WHEN NOT(label='全部') THEN 0
                    END
                ), label
            ";

            sql = sql.Replace("{#condition}", condition);

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);

        }

        //取得繳費狀態 Group
        private Item GetPayBoolGroup(TConfig cfg, string view_filter, int type)
        {
            string condition = "";

            if (type == 1)
            {
                condition = " WHERE t4.in_manager_org = N'" + view_filter + "'";
            }
            else if (type == 2)
            {
                condition = " WHERE t4.in_sno = N'" + view_filter + "'";
            }

            string sql = @"
            SELECT * FROM 
            (
                SELECT '全部' AS label , 'All' AS value
                UNION ALL
                SELECT pay_bool AS label, pay_bool AS value FROM 
                (
                    SELECT 
                        IIF(in_collection_agency is not null and pay_bool='未繳費','已繳費.',pay_bool) AS pay_bool
                    FROM 
                        IN_MEETING_PAY AS T1 WITH (NOLOCK)
                    LEFT JOIN 
                        IN_MEETING AS T2 WITH (NOLOCK)
                        ON T1.in_meeting = T2.id
                    LEFT JOIN 
                        IN_RESUME AS T4 WITH (NOLOCK)
                        ON t1.IN_CREATOR_SNO = T4.in_sno
                    {#condition}
                ) AS LIST
                GROUP BY pay_bool
            ) AS GList
            ORDER BY (
                CASE 
                    WHEN label='全部' THEN -1
                    WHEN NOT(label='全部') THEN 0
                END
            ), label
            ";

            sql = sql.Replace("{#condition}", condition);

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);

        }

        //繳費單清單
        private void Query(TConfig cfg, Item items, Item itmCheckIdentity, Item itmReturn)
        {
            bool isMeetingAdmin = itmCheckIdentity.getProperty("isMeetingAdmin", "") == "1";
            bool isCommittee = itmCheckIdentity.getProperty("isCommittee", "") == "1";

            List<TField> fields = new List<TField>();
            // fields.Add(new TField { property = "meeting_type", title = "類別", css = "text-center"});
            fields.Add(new TField { property = "item_number", title = "繳費單號", css = "text-center" });
            fields.Add(new TField { property = "in_title", title = "項目名稱", css = "text-left" });
            fields.Add(new TField { property = "in_creator", title = "協助報名者", css = "text-center" });
            fields.Add(new TField { property = "in_tel", title = "聯絡方式", format = "tel" });
            fields.Add(new TField { property = "pay_bool", title = "繳費狀態", css = "text-center" });
            fields.Add(new TField { property = "in_pay_amount_exp", title = "金額", css = "text-right" });
            fields.Add(new TField { property = "in_pay_date_exp", title = "應繳日期", css = "text-center" });
            // fields.Add(new TField { property = "in_pay_date_exp1", title = "最後<BR>收費日期", css = "text-center" });
            fields.Add(new TField { property = "in_pay_date_real", title = "實繳日期", css = "text-center" });
            fields.Add(new TField { property = "in_credit_date", title = "代收機構<BR>入帳日", css = "text-center" });
            fields.Add(new TField { property = "in_member_type", title = "類別", css = "text-center", hide = true });
            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            head.AppendLine("<thead>");
            foreach (var field in fields)
            {
                if (field.hide)
                {
                    head.AppendLine("<th class='text-center' data-field='" + field.property + "' data-sortable='true' data-visible='false'>" + field.title + "</th>");
                }
                else
                {
                    head.AppendLine("<th class='text-center' data-field='" + field.property + "' data-sortable='true'>" + field.title + "</th>");
                }
            }
            head.AppendLine("</thead>");

            int count = items.getItemCount(); ;

            body.AppendLine("<tbody>");
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                item.setProperty("pay_bool", GetPayBoolLink(item));
                string value = "";
                string phone = "";
                value = item.getProperty("in_pay_date_exp", "");
                item.setProperty("in_pay_date_exp", GetDateTimeValue(value, "yyyy/MM/dd", 8));
                value = item.getProperty("in_pay_date_exp1", "");
                item.setProperty("in_pay_date_exp1", GetDateTimeValue(value, "yyyy/MM/dd", 8));
                value = item.getProperty("in_pay_date_real", "");
                item.setProperty("in_pay_date_real", GetDateTimeValue(value, "yyyy/MM/dd", 8));
                value = item.getProperty("in_credit_date", "");
                item.setProperty("in_credit_date", GetDateTimeValue(value, "yyyy/MM/dd", 8));
                value = item.getProperty("in_pay_amount_exp", "");
                item.setProperty("in_pay_amount_exp", Money(value));
                value = item.getProperty("in_creator", "");
                // phone ="聯絡方式：" +  item.getProperty("in_tel","");
                // item.setProperty("in_creator", "<span onclick='alertPhone(\"" + phone + "\")'>"+ value +"<i class='fa fa-phone-square'></i></span>" );
                body.AppendLine("<tr>");
                foreach (var field in fields)
                {
                    body.AppendLine("<td class='" + field.css + "' data-field='" + field.property + "' >" + field.GetStr(item) + "</td>");
                }
                body.AppendLine("</tr>");
            }
            body.AppendLine("</tbody>");


            string table_name = "resume_table";

            StringBuilder builder = new StringBuilder();
            builder.AppendLine(GetTableAttribute(table_name));
            builder.Append(head);
            builder.Append(body);
            builder.AppendLine("</table>");
            builder.AppendLine("<script>");
            builder.AppendLine("$('#" + table_name + "').bootstrapTable();");
            builder.AppendLine("if ($(window).width() <= 768) { $('#" + table_name + "').bootstrapTable('toggleView'); }");
            builder.AppendLine("</script>");

            itmReturn.setProperty("inn_table", builder.ToString());
        }

        private string GetTableAttribute(string table_name)
        {
            return "<table id='" + table_name + "' class='table table-hover table-bordered table-rwd rwd rwdtable' "
                + " data-toggle='table' "
                + " data-click-to-select='true' "
                + " data-striped='false' "
                 + " data-pagination='true' "
                 + " data-show-pagination-switch='false'"
                 + " data-page-size='25'"
                + ">";
        }


        //取得檢視對象 Resume
        private Item GetTargetResume(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmLoginResume, Item itmReturn)
        {
            itmLoginResume.setType("In_Resume");
            itmLoginResume.setProperty("target_resume_id", itmReturn.getProperty("id", ""));
            itmLoginResume.setProperty("login_resume_id", itmLoginResume.getID());
            itmLoginResume.setProperty("login_member_type", itmLoginResume.getProperty("in_member_type", ""));
            return itmLoginResume.apply("In_Get_Target_Resume");
        }

        #region 匯出
        private void Export(TConfig cfg, Item itmResumes, Item itmReturn)
        {
            string xls_title = "";

            if (cfg.pay_bool == "All")
            {
                cfg.pay_bool = "繳費清單";
            }
            else
            {
                cfg.pay_bool += "清單";
            }

            if (cfg.title_name == "全部")
            {
                xls_title = cfg.pay_bool;
            }
            else
            {
                xls_title = cfg.title_name + "_" + cfg.pay_bool;
            }

            ClosedXML.Excel.XLWorkbook workbook = new ClosedXML.Excel.XLWorkbook();
            string title_name = "";
            if (itmResumes == null || !IsError(itmResumes, isSingle: false))
            {

                if (cfg.title_name == "全部")
                {
                    for (int i = 0; i < itmResumes.getItemCount(); i++)
                    {
                        Item itmResume = itmResumes.getItemByIndex(i);
                        if (title_name != CheckStr(itmResume.getProperty("in_title", "")))
                        {
                            title_name = CheckStr(itmResume.getProperty("in_title", ""));
                            AppendSheet(cfg, workbook, title_name, itmResumes, 1);

                        }
                    }
                }
                else
                {
                    cfg.pay_bool = CheckStr(cfg.pay_bool);
                    AppendSheet(cfg, workbook, cfg.pay_bool, itmResumes, 0);
                }

            }


            Item itmPath = GetExcelPath(cfg, "export_path");

            string export_path = itmPath.getProperty("export_path", "").TrimEnd('\\');
            string ext_name = ".xlsx";
            string xls_name = xls_title + "_" + DateTime.Now.ToString("MMdd_HHmmss");
            string xls_file = export_path + "\\" + xls_name + ext_name;
            string xls_url = xls_name + ext_name;

            workbook.SaveAs(xls_file);

            itmReturn.setProperty("xls_name", xls_url);
        }

        private Item GetExcelPath(TConfig cfg, string in_name)
        {
            Item itmResult = cfg.inn.newItem();

            //Excel 匯出路徑 (from 原創.設定參數)
            string sql = @"
            SELECT 
                TOP 1 t2.in_name AS 'variable', t1.in_name, t1.in_value 
            FROM 
                In_Variable_Detail t1 WITH(NOLOCK)
            INNER JOIN 
                In_Variable t2 WITH(NOLOCK) ON t2.ID = t1.SOURCE_ID 
            WHERE 
                t2.in_name = N'meeting_excel'  
                AND t1.in_name = N'{#in_name}'
            ";

            sql = sql.Replace("{#in_name}", in_name);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item item = cfg.inn.applySQL(sql);

            if (item.isError())
            {
                throw new Exception("未設定匯出路徑 _# " + in_name);
            }

            itmResult.setProperty("export_path", item.getProperty("in_value", ""));

            return itmResult;
        }
        //附加道館社團 Sheet
        private void AppendSheet(TConfig cfg, ClosedXML.Excel.XLWorkbook workbook, string sheet_name, Item itmResumes, int type)
        {
            ClosedXML.Excel.IXLWorksheet sheet = workbook.Worksheets.Add(sheet_name);

            List<TField> fields = new List<TField>();
            fields.Add(new TField { property = "item_number", title = "繳費單號", format = "center" });
            fields.Add(new TField { property = "in_title", title = "項目名稱", format = "left" });
            fields.Add(new TField { property = "in_creator", title = "協助報名者", format = "center" });
            fields.Add(new TField { property = "in_tel", title = "聯絡方式", format = "tel" });
            fields.Add(new TField { property = "pay_bool", title = "繳費狀態", format = "center" });
            fields.Add(new TField { property = "in_pay_amount_exp", title = "金額", format = "$ #,##0" });
            fields.Add(new TField { property = "in_pay_date_exp", title = "應繳日期", format = "yyyy/MM/dd" });
            // fields.Add(new TField { property = "in_pay_date_exp1", title = "最後收費日期", format = "yyyy/MM/dd"  });
            fields.Add(new TField { property = "in_pay_date_real", title = "實繳日期", format = "yyyy/MM/dd" });
            fields.Add(new TField { property = "in_credit_date", title = "代收機構入帳日", format = "yyyy/MM/dd" });
            int wsRow = 1;
            int wsCol = 1;
            int field_count = fields.Count;

            for (int i = 0; i < field_count; i++)
            {
                TField field = fields[i];
                ClosedXML.Excel.IXLCell cell = sheet.Cell(wsRow, wsCol + i);
                SetHeadCell(cell, field.title);
            }
            wsRow++;

            int count = itmResumes.getItemCount();


            for (int i = 0; i < count; i++)
            {
                Item item = itmResumes.getItemByIndex(i);
                if (type == 1 && CheckStr(item.getProperty("in_title")) == sheet_name)
                {
                    SetItemCell(sheet, wsRow, wsCol, item, fields);
                    wsRow++;
                }
                else if (type == 0)
                {
                    SetItemCell(sheet, wsRow, wsCol, item, fields);
                    wsRow++;
                }

            }

            //自動調整欄寬
            sheet.Columns().AdjustToContents();
        }
        ClosedXML.Excel.XLColor head_bg_color = ClosedXML.Excel.XLColor.FromHtml("#295C90");
        //設定標題列
        private void SetHeadCell(ClosedXML.Excel.IXLCell cell, string title)
        {
            cell.Value = title;

            cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
            cell.Style.Font.Bold = true;
            cell.DataType = ClosedXML.Excel.XLDataType.Text;
            cell.Style.Fill.BackgroundColor = head_bg_color;
            cell.Style.Font.FontColor = ClosedXML.Excel.XLColor.White;

            cell.Style.Border.SetRightBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetTopBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetBottomBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetLeftBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
        }

        //設定資料列
        private void SetBodyCell(ClosedXML.Excel.IXLCell cell, string value, string format)
        {
            switch (format)
            {
                case "center":
                    cell.Value = value;
                    cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    break;

                case "yyyy/MM/dd":
                case "yyyy/MM/dd HH:mm":
                    cell.Value = GetDateTimeValue(value, format, 8);
                    cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    break;

                case "$ #,##0":
                    cell.Value = value;
                    cell.Style.NumberFormat.Format = "$ #,##0";
                    break;

                case "tel":
                    if (value.StartsWith("09"))
                    {
                        cell.Value = value.Replace("-", "");
                        cell.Style.NumberFormat.Format = "0000-000-000";
                        cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    }
                    else
                    {
                        cell.Value = "'" + value;
                        cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    }
                    break;

                case "text":
                    cell.Value = "'" + value;
                    cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    break;

                default:
                    cell.Value = value;
                    break;
            }
            cell.Style.Border.SetRightBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetTopBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetBottomBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetLeftBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
        }

        //設定物件與資料列
        private void SetItemCell(ClosedXML.Excel.IXLWorksheet sheet, int wsRow, int wsCol, Item item, List<TField> fields)
        {
            int count = fields.Count;
            for (int i = 0; i < count; i++)
            {
                TField field = fields[i];

                ClosedXML.Excel.IXLCell cell = sheet.Cell(wsRow, wsCol + i);

                if (field.property == "")
                {
                    SetBodyCell(cell, "", "");
                }
                else
                {
                    string value = item.getProperty(field.property, "");
                    SetBodyCell(cell, value, field.format);
                }
            }
        }
        #endregion 匯出

        private class TField
        {
            public string property { get; set; }
            public string title { get; set; }
            public string css { get; set; }
            public string format { get; set; }
            public bool hide { get; set; }

            public string GetStr(Item item)
            {
                return item.getProperty(this.property, "");
            }
        }

        private string GetPayBoolLink(Item itmResume)
        {
            string meeting_type = itmResume.getProperty("meeting_type", "");
            string meeting_id = itmResume.getProperty("meeting_id", "");
            string item_number = itmResume.getProperty("item_number", "");
            string pay_bool = itmResume.getProperty("pay_bool", "");

            if (pay_bool.Contains("已繳費"))
            {
                pay_bool = "<span style='color:green;'><i class='fa fa-list-alt'></i>" + pay_bool + "</span>";
            }
            else
            {
                pay_bool = "<span style='color:red;'><i class='fa fa-list-alt'></i>" + pay_bool + "</span>";
            }

            if (meeting_type == "payment" || meeting_type == "game")
            {
                //onclick=\"ajaxindicatorstart('載入中...');\"
                return "<a target='_blank' href='../pages/c.aspx?page=detail_list_n1.html&method=In_Payment_DetailList&meeting_id=" + meeting_id + "&item_number=" + item_number + "' >" + pay_bool + "</a>";
            }
            else
            {
                //onclick=\"ajaxindicatorstart('載入中...');\"
                return "<a target='_blank' href='../pages/c.aspx?page=Cla_detail_list_n1.html&method=In_Cla_Payment_DetailList&meeting_id=" + meeting_id + "&item_number=" + item_number + "' >" + pay_bool + "</a>";
            }
        }

        private string GetSidDisplay(Item itmResume)
        {
            return GetSidDisplay(itmResume.getProperty("in_sno", ""));
        }

        //生日(外顯)
        private string GetBirthDisplay(Item itmResume)
        {
            string value = itmResume.getProperty("in_birth", "");

            if (value.Contains("1900") || value.Contains("1899"))
            {
                return "";
            }

            return GetDateTimeValue(value, "yyyy年MM月dd日", 8);
        }

        //身分證字號(外顯)
        private string GetSidDisplay(string in_sno)
        {
            if (in_sno.Length == 10)
            {
                //10碼(身分證處理)
                string in_sno_1 = in_sno.Substring(0, 2);
                string in_sno_2 = in_sno.Substring(6, 4);
                return in_sno_1 + "****" + in_sno_2;//A1****6789
            }
            else
            {
                return "";
            }
        }

        private string GetDateTimeValue(string value, string format, int hours = 0)
        {
            if (value == "") return "";

            DateTime dt = DateTime.Now;
            if (DateTime.TryParse(value, out dt))
            {
                return dt.AddHours(hours).ToString(format);
            }
            else
            {
                return value;
            }
        }

        private bool IsError(Item item, bool isSingle = true)
        {
            if (item.isError() || item.getResult() == "")
            {
                return true;
            }
            else if (isSingle && item.getItemCount() != 1)
            {
                return true;
            }
            else if (item.getItemCount() <= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        //下拉選單
        private void AppendMenu(TConfig cfg, Item items, string type_name, Item itmReturn)
        {
            if (items.isError() || items.getItemCount() <= 0)
            {
                return;
            }

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);

                item.setType(type_name);
                item.setProperty("inn_label", item.getProperty("label", ""));
                item.setProperty("inn_value", item.getProperty("value", ""));
                itmReturn.addRelationship(item);
            }
        }

        //檢查是否有特殊字元
        private string CheckStr(string fileName)
        {
            string[] Special_text = new string[] { "?", "=", ".", "*", "[", "@", "#", "$", "%", "^", "&", ".", "+", "-", "]", " ", "/", @"\" }; //特殊字元

            if (!string.IsNullOrWhiteSpace(fileName))
            {
                foreach (string _text in Special_text)
                {
                    if (fileName.Contains(_text))
                    {
                        fileName = fileName.Replace(_text, "");
                    }
                }
            }

            return fileName;
        }

        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string mode { get; set; }
            public string member_type { get; set; }
            public string title { get; set; }
            public string title_name { get; set; }
            public string pay_bool { get; set; }
            public string filter { get; set; }
            public string date_type { get; set; }
            public string date_s { get; set; }
            public string date_e { get; set; }
        }

        private string Money(string value)
        {
            if (value == "" || value == "0") return "0";
            return Convert.ToInt32(value).ToString("N0");
        }
    }
}