﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkHelp.Aras.Playground.CCO
{
    /// <summary>
    /// 通用函式庫
    /// </summary>
    public class Utilities
    {
        /// <summary>
        /// 偵測日誌
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="message">訊息</param>
        public static void WriteDebug(string value, string message)
        {
            Console.WriteLine($"=================== {value} :S =================== ");
            Console.WriteLine($"{message}");
            Console.WriteLine($"=================== {value} :E =================== ");
        }
    }
}