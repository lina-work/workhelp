﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkHelp.Playground.MyLinkQueue;

namespace WorkHelp.Playground.MyGraph
{
    public class Node<T>
    {
        private T data;

        public Node(T v)
        {
            data = v;
        }

        public T Data
        {
            get { return data; }
            set { data = value; }
        }
    }

    public interface IGraph<T>
    {
        //獲取頂點數
        int GetNumOfVertex();

        //獲取邊或弧的數目
        int GetNumOfEdge();

        //在兩個頂點之間添加權為 v 的邊或弧
        void SetEdge(Node<T> v1, Node<T> v2, int v);

        //刪除兩個頂點之間的邊或弧
        void DelEdge(Node<T> v1, Node<T> v2);

        //判斷兩個頂點之間是否有邊或弧
        bool IsEdge(Node<T> v1, Node<T> v2);
    }

    public class GraphAdjMatrix<T>
    {
        private Node<T>[] nodes;
        private int numEdges;
        private int[,] matrix;

        public GraphAdjMatrix(int n)
        {
            nodes = new Node<T>[n];
            matrix = new int[n, n];
            numEdges = 0;
        }

        public Node<T> GetNode(int index)
        {
            return nodes[index];
        }

        public void SetNode(int index, Node<T> v)
        {
            nodes[index] = v;
        }

        public int NumEdges
        { 
            get { return numEdges; }
            set { numEdges = value; }
        }

        public int GetMatrix(int index1, int index2)
        {
            return matrix[index1, index2];
        }

        public void SetMatrix(int index1, int index2)
        {
            matrix[index1, index2] = 1;
        }

        public int GetNumOfVertex()
        {
            return nodes.Length;
        }

        public int GetNumOfEdge()
        {
            return numEdges;
        }

        public bool IsNode(Node<T> v)
        {
            foreach (Node<T> nd in nodes)
            {
                if (v.Equals(nd))
                {
                    return true;
                }
            }

            return false;
        }
        
        public int GetIndex(Node<T> v)
        {
            int i = -1;

            for (i = 0; i < nodes.Length; ++i)
            {
                if (nodes[i].Equals(v))
                {
                    return i;
                }
            }

            return i;
        }

        public void SetEdge(Node<T> v1, Node<T> v2, int v)
        {
            if (!IsNode(v1) || !IsNode(v2))
            {
                return;
            }

            if (v != 1)
            {
                return;
            }

            matrix[GetIndex(v1), GetIndex(v2)] = v;
            matrix[GetIndex(v2), GetIndex(v1)] = v;
            ++NumEdges;
        }

        public void DelEdge(Node<T> v1, Node<T> v2)
        {
            if (!IsNode(v1) || !IsNode(v2))
            {
                return;
            }

            if (matrix[GetIndex(v1), GetIndex(v2)] == 1)
            {
                matrix[GetIndex(v1), GetIndex(v2)] = 0;
                matrix[GetIndex(v2), GetIndex(v1)] = 0;
                --numEdges;
            }
        }

        public bool IsEdge(Node<T> v1, Node<T> v2)
        {
            if (!IsNode(v1) || !IsNode(v2))
            {
                return false;
            }

            if (matrix[GetIndex(v1), GetIndex(v2)] == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    public class adjListNode<T>
    {
        private int adjvex;
        private adjListNode<T> next;

        public int Adjvex
        {
            get { return adjvex; }
            set { adjvex = value; }
        }

        public adjListNode<T> Next
        {
            get { return next; }
            set { next = value; }
        }

        public adjListNode(int vex)
        {
            adjvex = vex;
            next = null;
        }
    }

    public class VexNode<T>
    {
        private Node<T> data;
        private adjListNode<T> firstAdj;

        public Node<T> Data
        {
            get { return data; }
            set { data = value; }
        }

        public adjListNode<T> FirstAdj
        {
            get { return firstAdj; }
            set { firstAdj = value; }
        }

        public VexNode()
        {
            data = null;
            firstAdj = null;
        }

        public VexNode(Node<T> nd)
        {
            data = nd;
            firstAdj = null;
        }

        public VexNode(Node<T> nd, adjListNode<T> alNode)
        {
            data = nd;
            firstAdj = alNode;
        }
    }

    public class GraphAdjList<T> : IGraph<T>
    {
        private VexNode<T>[] adjList;

        public VexNode<T> this[int index]
        {
            get { return adjList[index]; }
            set { adjList[index] = value; }
        }

        public int GetNumOfVertex()
        {
            return adjList.Length;
        }

        public int GetNumOfEdge()
        {
            int i = 0;

            foreach (VexNode<T> nd in adjList)
            {
                adjListNode<T> p = nd.FirstAdj;
                
                while (p != null)
                {
                    ++i;
                    p = p.Next;
                }
            }

            return i / 2;
        }

        public bool IsNode(Node<T> v)
        {
            foreach (VexNode<T> nd in adjList)
            {
                if (v.Equals(nd.Data))
                {
                    return true;
                }
            }

            return false;
        }

        public bool IsEdge(Node<T> v1, Node<T> v2)
        {
            if (!IsNode(v1) || !IsNode(v2))
            {
                return false;
            }

            adjListNode<T> p = adjList[GetIndex(v1)].FirstAdj;

            while (p != null)
            {
                if (p.Adjvex == GetIndex(v2))
                {
                    return true;
                }

                p = p.Next;
            }

            return false;
        }
        public int GetIndex(Node<T> v)
        {
            int i = -1;

            for (i = 0; i < adjList.Length; ++i)
            {
                if (adjList[i].Data.Equals(v))
                {
                    return i;
                }
            }

            return i;
        }

        public void SetEdge(Node<T> v1, Node<T> v2, int v)
        {
            if (!IsNode(v1) || !IsNode(v2) || !IsEdge(v1, v2))
            {
                return;
            }

            if (v != 1)
            {
                return;
            }

            adjListNode<T> p = new adjListNode<T>(GetIndex(v2));

            if (adjList[GetIndex(v1)].FirstAdj == null)
            {
                adjList[GetIndex(v1)].FirstAdj = p;
            }
            else
            {
                p.Next = adjList[GetIndex(v1)].FirstAdj;
                adjList[GetIndex(v1)].FirstAdj = p;
            }

            p = new adjListNode<T>(GetIndex(v1));

            if (adjList[GetIndex(v2)].FirstAdj == null)
            {
                adjList[GetIndex(v2)].FirstAdj = p;
            }
            else
            {
                p.Next = adjList[GetIndex(v2)].FirstAdj;
                adjList[GetIndex(v2)].FirstAdj = p;
            }
        }

        public void DelEdge(Node<T> v1, Node<T> v2)
        {
            if (!IsNode(v1) || !IsNode(v2))
            {
                return;
            }

            if (IsEdge(v1, v2))
            {
                adjListNode<T> p = adjList[GetIndex(v1)].FirstAdj;
                adjListNode<T> pre = null;

                while (p != null)
                {
                    if (p.Adjvex != GetIndex(v2))
                    {
                        pre = p;
                        p = p.Next;
                    }
                }

                pre.Next = p.Next;

                p = adjList[GetIndex(v2)].FirstAdj;
                pre = null;

                while (p != null)
                {
                    if (p.Adjvex != GetIndex(v1))
                    {
                        pre = p;
                        p = p.Next;
                    }
                }

                pre.Next = p.Next;

            }
        }

        private int[] visited;
        
        public int[] Visited
        {
            get { return visited; }
            set { visited = value; }
        }

        //public GraphAdjList(Node<T>[] nodes)
        //{
        //    adjList = new VexNode<T>[nodes.Length];
        //    for (int i = 0; i < nodes.Length; ++i)
        //    {
        //        adjList[i].Data = nodes[i];
        //        adjList[i].FirstAdj = null;
        //    }
        //}

        public GraphAdjList(Node<T>[] nodes)
        {
            adjList = new VexNode<T>[nodes.Length];
            for (int i = 0; i < nodes.Length; ++i)
            {
                adjList[i].Data = nodes[i];
                adjList[i].FirstAdj = null;
                
            }

            visited = new int[adjList.Length];
            for (int i = 0; i < visited.Length; ++i)
            {
                visited[i] = 0;
            }
        }

        public void DFS()
        {
            for (int i = 0; i < visited.Length; ++i)
            {
                if (visited[i] == 0)
                {
                    DFSAL(i);
                }
            }
        }

        public void DFSAL(int i)
        {
            visited[i] = 1;
            adjListNode<T> p = adjList[i].FirstAdj;

            while (p != null)
            {
                if (visited[p.Adjvex] == 0)
                {
                    DFSAL(p.Adjvex);
                }

                p = p.Next;
            }
        }

        public void BFS()
        {
            for (int i = 0; i < visited.Length; ++i)
            {
                if (visited[i] == 0)
                {
                    BFSAL(i);
                }
            }
        }

        public void BFSAL(int i)
        {
            visited[i] = 1;
            CSeqQueue<int> cq = new CSeqQueue<int>(visited.Length);
            cq.In(i);

            while (!cq.IsEmpty())
            {
                int k = cq.Out();
                adjListNode<T> p = adjList[k].FirstAdj;

                while (p != null)
                {
                    if (visited[p.Adjvex] == 0)
                    {
                        visited[p.Adjvex] = 1;
                        cq.In(p.Adjvex);
                    }

                    p = p.Next;
                }
            }
        }

    }

    public class NetAdjMatrix<T> : IGraph<T>
    {
        private Node<T>[] nodes;
        private int numEdges;
        private int[,] matrix;

        public NetAdjMatrix(int n)
        {
            nodes = new Node<T>[n];
            matrix = new int[n, n];
            numEdges = 0;
        }

        public Node<T> GetNode(int index)
        {
            return nodes[index];
        }

        public void SetNode(int index, Node<T> v)
        {
            nodes[index] = v;
        }

        public int NumEdges
        {
            get { return numEdges; }
            set { numEdges = value; }
        }

        public int GetMatrix(int index1, int index2)
        {
            return matrix[index1, index2];
        }

        public void SetMatrix(int index1, int index2, int v)
        {
            matrix[index1, index2] = v;
        }

        public int GetNumOfVertex()
        {
            return nodes.Length;
        }

        public int GetNumOfEdge()
        {
            return numEdges;
        }

        public bool IsNode(Node<T> v)
        {
            foreach (Node<T> nd in nodes)
            {
                if (v.Equals(nd))
                {
                    return true;
                }
            }

            return false;
        }

        public int GetIndex(Node<T> v)
        {
            int i = -1;

            for (i = 0; i < nodes.Length; ++i)
            {
                if (nodes[i].Equals(v))
                {
                    return i;
                }
            }

            return i;
        }

        public void SetEdge(Node<T> v1, Node<T> v2, int v)
        {
            if (!IsNode(v1) || !IsNode(v2))
            {
                return;
            }

            matrix[GetIndex(v1), GetIndex(v2)] = v;
            matrix[GetIndex(v2), GetIndex(v1)] = v;
            ++numEdges;
        }

        public void DelEdge(Node<T> v1, Node<T> v2)
        {
            if (!IsNode(v1) || !IsNode(v2))
            {
                return;
            }

            if (matrix[GetIndex(v1), GetIndex(v2)] != int.MinValue)
            {
                matrix[GetIndex(v1), GetIndex(v2)] = int.MinValue;
                matrix[GetIndex(v2), GetIndex(v1)] = int.MinValue;
                --numEdges;
            }
        }

        public bool IsEdge(Node<T> v1, Node<T> v2)
        {
            if (!IsNode(v1) || !IsNode(v2))
            {
                return false;
            }

            if (matrix[GetIndex(v1), GetIndex(v2)] != int.MinValue)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// 普里姆算法
        /// </summary>
        public int[] Prim()
        {
            int[] lowcost = new int[nodes.Length];
            int[] closevex = new int[nodes.Length];
            int mincost = int.MaxValue;

            for (int i = 1; i < nodes.Length; ++i)
            {
                lowcost[i] = matrix[0, 1];
                closevex[i] = 0;
            }

            lowcost[0] = 0;
            closevex[0] = 0;

            for (int i = 0; i < nodes.Length; ++i )
            {
                int k = 1;
                int j = 1;

                while (j < nodes.Length)
                {
                    if (lowcost[j] < mincost && lowcost[j] != 0)
                    {
                        k = j;
                    }
                    ++j;
                }

                lowcost[k] = 0;

                for (j = 1; j < nodes.Length; ++j)
                {
                    if (matrix[k, j] < lowcost[j])
                    {
                        lowcost[j] = matrix[k, j];
                        closevex[j] = k;
                    }
                }
            }

            return closevex;
        }
    }

    public class DirecNetAdjMatrix<T> : IGraph<T>
    {
        private Node<T>[] nodes;
        private int numArcs;
        private int[,] matrix;

        public DirecNetAdjMatrix(int n)
        {
            nodes = new Node<T>[n];
            matrix = new int[n, n];
            numArcs = 0;
        }

        public Node<T> GetNode(int index)
        {
            return nodes[index];
        }

        public void SetNode(int index, Node<T> v)
        {
            nodes[index] = v;
        }

        public int NumArcs
        {
            get { return numArcs; }
            set { numArcs = value; }
        }

        public int GetMatrix(int index1, int index2)
        {
            return matrix[index1, index2];
        }

        public void SetMatrix(int index1, int index2, int v)
        {
            matrix[index1, index2] = v;
        }

        public int GetNumOfVertex()
        {
            return nodes.Length;
        }

        public int GetNumOfEdge()
        {
            return numArcs;
        }

        public bool IsNode(Node<T> v)
        {
            foreach (Node<T> nd in nodes)
            {
                if (v.Equals(nd))
                {
                    return true;
                }
            }

            return false;
        }

        public int GetIndex(Node<T> v)
        {
            int i = -1;

            for (i = 0; i < nodes.Length; ++i)
            {
                if (nodes[i].Equals(v))
                {
                    return i;
                }
            }

            return i;
        }

        public void SetEdge(Node<T> v1, Node<T> v2, int v)
        {
            if (!IsNode(v1) || !IsNode(v2))
            {
                return;
            }

            matrix[GetIndex(v1), GetIndex(v2)] = v;
            ++numArcs;
        }

        public void DelEdge(Node<T> v1, Node<T> v2)
        {
            if (!IsNode(v1) || !IsNode(v2))
            {
                return;
            }

            if (matrix[GetIndex(v1), GetIndex(v2)] != int.MinValue)
            {
                matrix[GetIndex(v1), GetIndex(v2)] = int.MinValue;
                --numArcs;
            }
        }

        public bool IsEdge(Node<T> v1, Node<T> v2)
        {
            if (!IsNode(v1) || !IsNode(v2))
            {
                return false;
            }

            if (matrix[GetIndex(v1), GetIndex(v2)] != int.MinValue)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void Dijkstra(ref bool[,] pathMatricArr, ref int[] shortPathArr, Node<T> n)
        {
            int k = 0;
            bool[] final = new bool[nodes.Length];

            for (int i = 0; i < nodes.Length; ++i)
            {
                final[i] = false;
                shortPathArr[i] = matrix[GetIndex(n), i];

                for (int j = 0; j < nodes.Length; ++j)
                {
                    pathMatricArr[i, j] = false;
                }

                if (shortPathArr[i] != 0 && shortPathArr[i] < int.MaxValue)
                {
                    pathMatricArr[i, GetIndex(n)] = true;
                }
            }

            shortPathArr[GetIndex(n)] = 0;
            final[GetIndex(n)] = true;

            for (int i = 0; i < nodes.Length; ++i)
            {
                int min = int.MaxValue;

                for (int j = 0; j < nodes.Length; ++j)
                {
                    if (!final[j])
                    {
                        if (shortPathArr[j] < min)
                        {
                            k = j;
                            min = shortPathArr[j];
                        }
                    }
                }

                final[k] = true;

                for (int j = 0; j < nodes.Length; ++j)
                {
                    if (!final[j] && (min + matrix[k, j] < shortPathArr[j]))
                    {
                        shortPathArr[j] = min + matrix[k, j];

                        for (int w = 0; w < nodes.Length; ++w)
                        {
                            pathMatricArr[j, w] = pathMatricArr[k, w];
                        }
                        pathMatricArr[j, j] = true;
                    }
                }
            }
        }
    }


}
