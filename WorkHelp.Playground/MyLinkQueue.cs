﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkHelp.Playground.MyLinkStack;

namespace WorkHelp.Playground.MyLinkQueue
{
    public class MyQueueTest
    {
        public static string RecStr(string str)
        {
            LinkQueue<char> q = new LinkQueue<char>();
            for (int i = 0; i < str.Length; ++i)
            {
                q.In(str[i]);
            }

            string result = "";
            while(!q.IsEmpty())
            {
                result += q.Out();
            }

            return result;
        }

        //是否為回文
        public static bool IsRecStr(string str)
        {
            TSeqStack<char> s = new TSeqStack<char>(50);
            CSeqQueue<char> q = new CSeqQueue<char>(50);

            for (int i = 0; i < str.Length; ++i)
            {
                s.Push(str[i]);
                q.In(str[i]);
            }

            while(!s.IsEmpty() && !q.IsEmpty())
            {
                char a = s.Pop();
                char b = q.Out();
                if (a != b) 
                {
                    break;
                }
            }

            if (!s.IsEmpty() || !q.IsEmpty())
            {
                return false;
            }
            else
            {
                return true;
            }

        }
    }

    public class LinkQueue<T> : IMyQueue<T>
    {
        private Node<T> front;
        private Node<T> rear;
        private int num;

        public Node<T> Front
        {
            get { return front; }
            set { front = value; }
        }

        public Node<T> Rear
        {
            get { return rear; }
            set { rear = value; }
        }

        public int Num
        {
            get { return num; }
            set { num = value; }
        }

        public LinkQueue()
        {
            front = rear = null;
            num = 0;
        }

        public int GetLenth()
        {
            return num;
        }

        public bool IsEmpty()
        {
            if (front == rear && num == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool IsFull()
        {
            return false;
        }

        public void Clear()
        {
            front = rear = null;
            num = 0;
        }

        public void In(T item)
        {
            Node<T> q = new Node<T>(item);

            if (rear == null)
            {
                front = rear = q;
            }
            else
            {
                rear.Next = q;
                rear = q;
            }
            ++num;
        }

        public T Out()
        {
            if (IsEmpty())
            {
                return default(T);
            }

            Node<T> p = front;
            front = front.Next;

            if (front == null)
            {
                rear = null;
            }
            --num;

            return p.Data;
        }

        public T GetFront()
        {
            if (IsEmpty())
            {
                return default(T);
            }

            return front.Data;
        }

    }

    public class CSeqQueue<T> : IMyQueue<T>
    {
        private int maxsize;
        private T[] data;
        private int front;
        private int rear;

        public int MaxSize
        {
            get { return maxsize; }
            set { maxsize = value; }
        }

        public T this[int index]
        {
            get { return data[index]; }
            set { data[index] = value; }
        }

        public int Front
        {
            get { return front; }
            set { front = value; }
        }

        public int Rear
        {
            get { return rear; }
            set { rear = value; }
        }

        public CSeqQueue(int size)
        {
            data = new T[size];
            maxsize = size;
            front = rear = -1;
        }

        public int GetLenth()
        {
            return (rear - front + maxsize) % maxsize;
        }

        public bool IsEmpty()
        {
            if (front == rear)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void Clear()
        {
            front = rear = -1;
        }

        public bool IsFull()
        {
            if ((rear + 1) % maxsize == front)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void In(T item)
        {
            if (IsFull())
            {
                return;
            }

            data[++rear] = item;
        }

        public T Out()
        {
            T tmp = default(T);
            if (IsEmpty())
            {
                return tmp;
            }

            tmp = data[++front];
            return tmp;
        }

        public T GetFront()
        { 
            if (IsEmpty())
            {
                return default(T);
            }

            return data[front + 1];
        }
    }

    public class Node<T>
    {
        private T data;
        private Node<T> next;

        public Node(T val, Node<T> p)
        {
            data = val;
            next = p;
        }

        public Node(Node<T> p)
        {
            next = p;
        }

        public Node(T val)
        {
            data = val;
            next = null;
        }

        public Node()
        {
            data = default(T);
            next = null;
        }

        public T Data
        {
            get { return data; }
            set { data = value; }
        }

        public Node<T> Next
        {
            get { return next; }
            set { next = value; }
        }

    }

    public interface IMyQueue<T>
    {
        int GetLenth();
        bool IsEmpty();
        bool IsFull();
        void Clear();
        void In(T item);
        T Out();
        T GetFront();
    }
}
