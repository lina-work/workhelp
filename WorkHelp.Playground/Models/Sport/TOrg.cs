﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkHelp.Playground.Models.Sport
{
    /// <summary>
    /// 單位
    /// </summary>
    public class TOrg
    {
        /// <summary>
        /// 單位名稱
        /// </summary>
        public string OrgName { get; set; }

        /// <summary>
        /// 選手列表
        /// </summary>
        public List<TPlayer> Players { get; set; }

        /// <summary>
        /// 會打到自己人
        /// </summary>
        public bool HasSames { get; set; }

        /// <summary>
        /// 已建置最高層級
        /// </summary>
        public bool HasMaxLvBlack { get; set; }

        /// <summary>
        /// 黑名單字典
        /// </summary>
        public Dictionary<int, List<int>> BlackDictionary { get; set; }
    }
}
