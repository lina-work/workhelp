﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkHelp.Playground.Models.Sport
{
    /// <summary>
    /// 梯次
    /// </summary>
    public class TEchelon
    {
        /// <summary>
        /// 所有選手
        /// </summary>
        public List<TPlayer> Players { get; set; }

        /// <summary>
        /// 值
        /// </summary>
        public int Value { get; set; }
    }
}
