﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkHelp.Playground.Models.Sport
{
    /// <summary>
    /// 選手
    /// </summary>
    public class TPlayer
    {
        /// <summary>
        /// 選手編號
        /// </summary>
        public string PlayerNo { get; set; }

        /// <summary>
        /// 選手姓名
        /// </summary>
        public string PlayerName { get; set; }

        /// <summary>
        /// 單位名稱
        /// </summary>
        public string OrgName { get; set; }

        /// <summary>
        /// 組別量級
        /// </summary>
        public string SectName { get; set; }

        /// <summary>
        /// 種子籤
        /// </summary>
        public int SeedNo { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int SeqNo { get; set; }

        /// <summary>
        /// 積分
        /// </summary>
        public int Score { get; set; }

        /// <summary>
        /// 籤號
        /// </summary>
        public int DrawNo { get; set; }

        /// <summary>
        /// 已分配籤號
        /// </summary>
        public bool HasAssigned { get; set; }

        /// <summary>
        /// 梯號
        /// </summary>
        public int EchelonNo { get; set; }
    }
}
