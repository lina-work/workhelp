﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkHelp.Playground.DrawLots;

namespace WorkHelp.Playground.Models.Sport
{
    public class TDraw
    {
        /// <summary>
        /// 抽籤類型
        /// </summary>
        public string DrawType { get; set; }

        /// <summary>
        /// 種子籤
        /// </summary>
        public string Integral { get; set; }

        /// <summary>
        /// 99號籤
        /// </summary>
        public string Draw99 { get; set; }

        /// <summary>
        /// 同隊分面
        /// </summary>
        public bool SameTeamSeparate { get; set; }

        /// <summary>
        /// 是否重抽
        /// </summary>
        public bool ResetDraw { get; set; }

        /// <summary>
        /// 組別量級(單人抽籤、組別抽籤)
        /// </summary>
        public string SectName { get; set; }

        /// <summary>
        /// 選手編號(單人抽籤)
        /// </summary>
        public string PlayerNo { get; set; }

        /// <summary>
        /// Excel 檔案完整路徑
        /// </summary>
        public string XlsPath { get; set; }

        /// <summary>
        /// 選手列表
        /// </summary>
        public List<TPlayer> Players { get; set; }

        /// <summary>
        /// 單位列表
        /// </summary>
        public List<TOrg> Orgs { get; set; }



        /// <summary>
        /// 真實選手數
        /// </summary>
        private int RealPlayerCount { get; set; }

        /// <summary>
        /// 賽制輪數
        /// </summary>
        private int RoundPower { get; set; }
        
        /// <summary>
        /// 賽制選手數
        /// </summary>
        private int RoundPlayerCount { get; set; }

        /// <summary>
        /// 賽制總和
        /// </summary>
        private int RoundMax { get; set; }

        /// <summary>
        /// 所有球
        /// </summary>
        private List<TBall> BallList { get; set; }

        /// <summary>
        /// 參數初始化
        /// </summary>
        private void Initial()
        {
            if (this.Players == null)
            {
                this.Players = new List<TPlayer>();
            }

            int player_count = this.Players.Count;
            int[] nm = DrawUtility.Cardinarity(player_count);

            this.RealPlayerCount = player_count;

            this.RoundPower = nm[0];
            this.RoundPlayerCount = nm[1];
            this.RoundMax = nm[1] + 1;
        }

        /// <summary>
        /// 建立所有球號
        /// </summary>
        private void BuildBalls()
        {
            int p_cnt = this.RealPlayerCount;
            int r_cnt = this.RoundPlayerCount;
            int[] ns = DrawUtility.TkdNumbers(p_cnt);

            List<TBall> balls = new List<TBall>();

            for (int i = 0; i < r_cnt; i++)
            {
                int taekwondo_no = ns[i];

                TBall ball = new TBall
                {
                    BallNo = taekwondo_no,
                    CanCatch = true,
                    IsByPass = taekwondo_no > p_cnt,
                };

                if (ball.IsByPass)
                {
                    ball.CanCatch = false;
                }

                balls.Add(ball);
            }

            this.BallList = balls;
        }

        /// <summary>
        /// 單位整隊
        /// </summary>
        private void BuildOrg()
        {
            List<TOrg> orgs = new List<TOrg>();

            for (int i = 0; i < this.Players.Count; i++)
            {
                TPlayer player = this.Players[i];
                TOrg org = orgs.Find(x => x.OrgName.Equals(player.OrgName, StringComparison.InvariantCultureIgnoreCase));

                if (org == null)
                {
                    org = new TOrg
                    {
                        OrgName = player.OrgName,
                        Players = new List<TPlayer> { player },
                        BlackDictionary = new Dictionary<int, List<int>>(),
                        HasSames = false,
                    };
                    orgs.Add(org);
                }
                else
                {
                    org.Players.Add(player);
                }
            }

            //單位依選手數量排序(由多到少)
            this.Orgs = orgs.OrderByDescending(x => x.Players.Count).ToList();
        }

        /// <summary>
        /// 執行抽籤
        /// </summary>
        public void Execute()
        {
            //初始化
            this.Initial();

            if (this.RealPlayerCount <= 0)
            {
                return;
            }
            else if (this.RealPlayerCount == 1)
            {
                this.Players[0].DrawNo = 1;
            }
            else if (this.RealPlayerCount == 2)
            {
                int[] result = DrawUtility.RandomNoArr(this.RealPlayerCount);

                for (int i = 0; i < this.RealPlayerCount; i++)
                {
                    this.Players[i].DrawNo = result[i];
                }
            }
            else
            {
                if (this.SameTeamSeparate)
                {
                    //建立球號
                    this.BuildBalls();
                    //先分配種子籤
                    if (this.Integral == "Y")
                    {

                    }

                    //單位整隊
                    this.BuildOrg();
                    //執行抽籤(單位)
                    this.Orgs.ForEach(org => OrgStart(this.BallList, org));
                }
                else if (this.Integral == "Y")
                {
                    //先分配種子籤
                }
                else
                {
                    int[] result = DrawUtility.RandomNoArr(this.RealPlayerCount);

                    for (int i = 0; i < this.RealPlayerCount; i++)
                    {
                        this.Players[i].DrawNo = result[i];
                    }
                }
            }
        }

        /// <summary>
        /// 抽籤
        /// </summary>
        private void OrgStart(List<TBall> balls, TOrg org)
        {
            int half = this.RoundPlayerCount / 2;
            int p_cnt = org.Players.Count;

            if (p_cnt == 0)
            {
                return;
            }
            else if (p_cnt == 1)
            {
                //單人出賽
                BindDrawNo(balls, org, org.Players[0]);
            }
            else if (p_cnt == 2)
            {
                //兩人出賽
                OrgHalf(balls, org, 2);
            }
            else if (p_cnt >= half)
            {
                //過半
                OrgHalf(balls, org, half);
            }
            else
            {
                OrgOther(balls, org);
            }
        }

        /// <summary>
        /// 抽籤
        /// </summary>
        private void OrgOther(List<TBall> balls, TOrg org)
        {
            int p_cnt = org.Players.Count;
            int[] nm = DrawUtility.Cardinarity(p_cnt);

            //第一梯次取號
            int code = nm[1] / 2;
            List<TBox> boxes = SpilitBalls(balls, code);

            //挑出可取球的箱子們
            List<TBox> availables = GetAvailableBoxes(boxes);
            int box_count = availables.Count;
            
            //取得隨機箱號陣列
            int[] ns = DrawUtility.RandomNoArr(box_count);
            int end = box_count < code ? box_count : code;

            for (int i = 0; i < end; i++)
            {
                int box_idx = ns[i] - 1;
                TPlayer player = org.Players[i];
                TBox box = availables[box_idx];
                BindDrawNo(box.Balls, org, player, addBlack: true);
            }

            //第二梯次取號

            List<TPlayer> un_assigned_players = org.Players.FindAll(x => !x.HasAssigned);
            int una_count = un_assigned_players == null ? 0 : un_assigned_players.Count;
            int try_count = 0;

            while (una_count > 0 && try_count < 3)
            {
                code = code * 2;

                //剩餘未取號
                boxes = SpilitBalls(balls, code);

                for (int i = 1; i < org.BlackDictionary.Count; i++)
                {
                    List<int> black_no_list = org.BlackDictionary[i];

                    //過濾黑名單
                    availables = GetAvailableBoxes(boxes, black_no_list);
                    box_count = availables.Count;

                    if (box_count > 0)
                    {
                        break;
                    }
                }

                if (box_count > 0)
                {
                    ns = DrawUtility.RandomNoArr(box_count);
                    end = box_count < una_count ? box_count : una_count;

                    for (int i = 0; i < end; i++)
                    {
                        int box_idx = ns[i] - 1;
                        TPlayer player = un_assigned_players[i];
                        TBox box = availables[box_idx];
                        BindDrawNo(box.Balls, org, player);
                    }

                    if (una_count == ns.Length)
                    {
                        una_count = 0;
                    }
                    else
                    {
                        un_assigned_players = org.Players.FindAll(x => !x.HasAssigned);
                        una_count = un_assigned_players == null ? 0 : un_assigned_players.Count;
                    }
                }
                
                try_count++;
            }
        }

        /// <summary>
        /// 抽籤
        /// </summary>
        private void OrgHalf(List<TBall> balls, TOrg org, int code)
        {
            List<TBox> boxes = SpilitBalls(balls, code);
            List<TEchelon> echelons = SpilitPlayers(org.Players, code);

            for(int i = 0; i < echelons.Count; i++)
            {
                TEchelon echelon = echelons[i];
                int player_count = echelon.Players.Count;

                //挑出可取球的箱子們
                List<TBox> availables = GetAvailableBoxes(boxes);
                int box_count = availables.Count;

                if (player_count > box_count)
                {
                    throw new Exception("[異常]選手人數超過箱子數量");
                }

                //取得隨機箱號陣列
                int[] ns = DrawUtility.RandomNoArr(box_count);

                for (int j = 0; j < player_count; j++)
                {
                    int box_idx = ns[j] - 1;

                    TPlayer player = echelon.Players[j];
                    TBox selected_box = availables[box_idx];
                    BindDrawNo(selected_box.Balls, org, player);
                }
            }
        }

        /// <summary>
        /// 球號分箱
        /// </summary>
        private List<TBox> SpilitBalls(List<TBall> balls, int item_count)
        {
            List<TBox> boxes = new List<TBox>();
            //每個箱子裡的球數
            int box_ball_count = balls.Count / item_count;

            for (int i = 0; i < item_count; i++)
            {
                TBox box = new TBox
                {
                    Balls = new List<TBall>(),
                };

                for (int j = 0; j < box_ball_count; j++)
                {
                    int idx = i * box_ball_count + j;
                    TBall ball = balls[idx];
                    box.Balls.Add(ball);
                }

                boxes.Add(box);
            }
            return boxes;
        }

        /// <summary>
        /// 選手分梯
        /// </summary>
        private List<TEchelon> SpilitPlayers(List<TPlayer> players, int item_count)
        {
            List<TEchelon> echelons = new List<TEchelon>();

            //梯次數量
            int echelon_count = players.Count / item_count;
            int echelon_rsd = players.Count % item_count;
            if (echelon_rsd > 0) echelon_count++;

            for (int i = 0; i < echelon_count; i++)
            {
                TEchelon echelon = new TEchelon
                {
                    Players = new List<TPlayer>(),
                    Value = item_count,
                };

                for (int j = 0; j < item_count; j++)
                {
                    int idx = i * item_count + j;
                    if (idx < players.Count)
                    {
                        TPlayer player = players[idx];
                        echelon.Players.Add(player);
                    }
                }

                echelons.Add(echelon);
            }

            return echelons;
        }

        /// <summary>
        /// 挑出可取球的箱子們
        /// </summary>
        private List<TBox> GetAvailableBoxes(List<TBox> source)
        {
            List<TBox> result = new List<TBox>();

            for (int i = 0; i < source.Count; i++)
            {
                TBox box = source[i];
                bool can_catch = false;

                for (int j = 0; j < box.Balls.Count; j++)
                {
                    TBall ball = box.Balls[j];
                    if (ball.CanCatch)
                    {
                        can_catch = true;
                        break;
                    }
                }

                if (can_catch)
                {
                    result.Add(box);
                }
            }

            return result;
        }

        /// <summary>
        /// 挑出可取球的箱子們
        /// </summary>
        private List<TBox> GetAvailableBoxes(List<TBox> source, List<int> blackNos)
        {
            List<TBox> result = new List<TBox>();

            for (int i = 0; i < source.Count; i++)
            {
                TBox box = source[i];
                bool can_catch = false;

                for (int j = 0; j < box.Balls.Count; j++)
                {
                    TBall ball = box.Balls[j];
                    if (ball.CanCatch)
                    {
                        if (!blackNos.Contains(ball.BallNo))
                        {
                            can_catch = true;
                            break;
                        }
                    }
                }

                if (can_catch)
                {
                    result.Add(box);
                }
            }

            return result;
        }

        /// <summary>
        /// 抽籤
        /// </summary>
        private void BindDrawNo(List<TBall> source, TOrg org, TPlayer player, bool addBlack = false)
        {
            List<TBall> balls = new List<TBall>();

            //挑出可抓的球
            source.ForEach(x =>
            {
                if (x.CanCatch)
                {
                    balls.Add(x);
                }
            });


            //隨機取球
            int num = DrawUtility.RandomNo(1, balls.Count);
            int idx = num - 1;

            TBall ball = balls[idx];
            ball.IsCatched = true;
            ball.CanCatch = false;

            player.DrawNo = ball.BallNo;
            player.HasAssigned = true;

            if (addBlack)
            {
                AppendBlackList(org, balls, ball);
            }

            //WorkHelp.Logging.TLog.Watch(message: player.PlayerName + ": " + player.DrawNo);

        }

        private void AppendBlackList(TOrg org, List<TBall> box_balls, TBall selected_ball)
        {
            //ex: 32
            int total = box_balls.Count;
            //ex: 25
            int idx = box_balls.FindIndex(x => x.BallNo == selected_ball.BallNo);
            //ex: 16
            int half = box_balls.Count / 2;
            int min = 0;
            int max = total;

            bool over_half = idx >= (min + half);

            if (over_half)
            {
                min = min + half;
            }
            else
            {
                max = min + half;
            }

            int lv = 1;
            while (max >= min && half > 0)
            {
                List<int> dic = null;
                if (org.BlackDictionary.ContainsKey(lv))
                {
                    dic = org.BlackDictionary[lv];
                }
                else
                {
                    dic = new List<int>();
                    org.BlackDictionary.Add(lv, dic);
                }

                for (int i = min; i < max; i++)
                {
                    int no = box_balls[i].BallNo;
                    dic.Add(no);
                }

                half = half / 2;
                over_half = idx >= (min + half);

                if (over_half)
                {
                    min = min + half;
                }
                else
                {
                    max = min + half;
                }

                lv++;
            }

            if (!org.HasMaxLvBlack)
            {
                //最後不過濾
                org.HasMaxLvBlack = true;
                org.BlackDictionary.Add(lv, new List<int>());
            }
        }

        /// <summary>
        /// 列印結果
        /// </summary>
        /// <returns></returns>
        public string ExecutePrint()
        {
            this.Execute();

            int[] ns = DrawUtility.TkdNumbers(this.RoundPlayerCount);

            StringBuilder builder = new StringBuilder();
            builder.AppendLine("選手數：" + this.Players.Count);

            for (int i = 0; i < ns.Length; i++)
            {
                int no = ns[i];
                TPlayer player = this.Players.Find(x => x.DrawNo == no);
                if (player == null)
                {

                    builder.AppendLine($"{no}\t\t輪空");
                }
                else
                {
                    builder.AppendLine($"{no}\t\t{player.OrgName}\t\t{player.PlayerName}\t\t");
                }
            }

            return builder.ToString();
        }
    }
}
