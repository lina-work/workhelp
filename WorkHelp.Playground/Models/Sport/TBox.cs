﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkHelp.Playground.Models.Sport
{
    public class TBox
    {
        /// <summary>
        /// 箱子內的所有球
        /// </summary>
        public List<TBall> Balls { get; set; }
    }
}
