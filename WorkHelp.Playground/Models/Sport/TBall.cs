﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkHelp.Playground.Models.Sport
{
    public class TBall
    {
        /// <summary>
        /// 球號(籤號)
        /// </summary>
        public int BallNo { get; set; }

        /// <summary>
        /// 可抓球
        /// </summary>
        public bool CanCatch { get; set; }

        /// <summary>
        /// 是否為輪空號
        /// </summary>
        public bool IsByPass { get; set; }

        /// <summary>
        /// 是否已被抓取
        /// </summary>
        public bool IsCatched { get; set; }
    }
}
