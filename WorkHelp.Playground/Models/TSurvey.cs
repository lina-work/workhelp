﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkHelp.Playground.Models
{
    public class TInnoSoft
    { 
        public string baseUrl { get; set; }
    }
    
    public class TSurvey
    {
        public int idx { get; set; }
        public string id { get; set; }
        public string url { get; set; }
        public string html { get; set; }
    }
}
