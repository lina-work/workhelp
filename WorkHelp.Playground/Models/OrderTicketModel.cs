﻿namespace WorkHelp.Playground.Models
{
    /// <summary>
    /// 訂單票券資料模型
    /// </summary>
    public class OrderTicketModel
    {
        /// <summary>
        /// 票區名稱
        /// </summary>
        public string AreaName { get; set; }

        /// <summary>
        /// 座位名稱
        /// </summary>
        public string SeatName { get; set; }

        /// <summary>
        /// 票券類型名稱
        /// </summary>
        public string TicketName { get; set; }

        /// <summary>
        /// 票價
        /// </summary>
        public decimal TicketPrice { get; set; }
    }
}
