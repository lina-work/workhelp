﻿using System;
using System.Collections.Generic;

namespace WorkHelp.Playground.Models
{
    /// <summary>
    /// 訂單主檔資料模型
    /// </summary>
    public class OrderMainModel
    {
        /// <summary>
        /// 訂單編號
        /// </summary>
        public string OrderNo { get; set; }

        /// <summary>
        /// 訂購者代號
        /// </summary>
        public int Userid { get; set; }

        /// <summary>
        /// 訂購時間
        /// </summary>
        public DateTime OrderTime { get; set; }

        /// <summary>
        /// 取票方式
        /// </summary>
        public string GetName { get; set; }

        /// <summary>
        /// 付款方式
        /// </summary>
        public string PayName { get; set; }

        /// <summary>
        /// 總金額
        /// </summary>
        public decimal TotalPrice { get; set; }

        /// <summary>
        /// 節目名稱
        /// </summary>
        public string ShowName { get; set; }

        /// <summary>
        /// 場次名稱
        /// </summary>
        public string EventName { get; set; }

        /// <summary>
        /// 票券清單
        /// </summary>
        public List<OrderTicketModel> Tickets { get; set; }

    }
}
