﻿namespace WorkHelp.Playground.Models
{
    /// <summary>
    /// 退票申請
    /// </summary>
    public class OrderReturnModel
    {
        /// <summary>
        /// 申請原因
        /// </summary>
        public string ApplyReason { get; set; }

        /// <summary>
        /// 訂單資料
        /// </summary>
        public OrderMainModel OrderData { get; set; }
    }
}
