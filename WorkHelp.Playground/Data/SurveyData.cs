﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkHelp.Playground.Models;

namespace WorkHelp.Playground.Data
{
    public class SurveyData
    {
        public static List<TSurvey> GetList_20()
        {
            List<TSurvey> list = new List<TSurvey>();
            list.Add(new TSurvey { idx = 1, id = "0502F7D45077491981519E9ACDEED8E4" });

            var app = "/pages/b.aspx?page=In_Cla_MeetingSurvey.html&method=In_Cla_Get_MeetingSurveyAndAnswer"
                + "&meeting_id=66244AB840984A79B946DF15FB4CF530"
                + "&surveytype=5";

            list.ForEach(x => 
            {
                x.url = app + "&muid=" + x.id;
            });
            
            return list;
        }
    }
}
