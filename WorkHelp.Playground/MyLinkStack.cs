﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkHelp.Playground.MyLinkStack
{
    public class TConversion
    {
        //優先等級
        public static char Precede<T>(T data, char c)
        {
            //判定 optr 棧頂算符與讀入的算法之間的優先及關係的方法
            return c;
        }

        public static int Operate(int a, char theta, int b)
        {
            //進行二元運算的方法
            switch (theta)
            {
                case '+': return a + b;
                case '-': return a - b;
                case '*': return a * b;
                case '/': return a / b;
                default: return 0;
            }
        }

        //表達式求值
        public static int EvaluateExpression()
        {
            TSeqStack<char> optr = new TSeqStack<char>(20);
            TSeqStack<int> optn = new TSeqStack<int>(20);
            optr.Push('#');
            char c = Console.Read().ToString()[0];
            char theta = '0';
            int a = 0;
            int b = 0;

            char[] cs = new char[] 
            { 
                '+',
                '-',
                '*',
                '/',
                '(',
                ')',
            };

            while (c != '#')
            {
                if (!cs.Contains(c))
                {
                    optr.Push(c);
                }
                else
                {
                    switch (Precede(optr.GetTop(), c))
                    {
                        case '<' :
                            optr.Push(c);
                            c = Console.Read().ToString()[0];
                            break;

                        case '=':
                            theta = optr.Pop();
                            a = optn.Pop();
                            b = optn.Pop();
                            optn.Push(Operate(a, theta, b));
                            break;
                    }
                }
            }

            return optn.GetTop();

        }

        //括號匹配
        public static bool MatchBracket(char[] charlist)
        {
            TSeqStack<char> s = new TSeqStack<char>(50);
            int len = charlist.Length;
            for (int i = 0; i < len; ++i)
            {
                if (s.IsEmpty())
                {
                    s.Push(charlist[i]);
                }
                else if (
                    (s.GetTop() == '(' && charlist[i] == ')')
                    || 
                    (s.GetTop() == '[' && charlist[i] == ']'))
                {
                    s.Pop();
                }
                else
                {
                    s.Push(charlist[i]);
                }
            }

            if (s.IsEmpty())
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static void TEST()
        {
            string data = Console.ReadLine();
            while (!data.Contains("q"))
            {
                int n = 0;
                int.TryParse(data, out n);
                Console.WriteLine(Conversion16(n));
                Console.WriteLine("請輸入資料：");
                data = Console.ReadLine();
            }
        }

        //十進制轉八進制
        public static string Conversion8(int n)
        {
            TLinkStack<int> s = new TLinkStack<int>();
            while (n > 0)
            {
                s.Push(n % 8);
                n = n / 8;
            }

            string result = "";
            while (!s.IsEmpty())
            {
                int m = s.Pop();
                result += m.ToString();
            }
            return result;
        }

        //十進制轉十六進制
        public static string Conversion16(int n)
        {
            TLinkStack<int> s = new TLinkStack<int>();
            while (n > 0)
            {
                s.Push(n % 16);
                n = n / 16;
            }

            string result = "";
            while (!s.IsEmpty())
            {
                int m = s.Pop();
                string c = "";
                switch (m)
                {
                    case 10: c = "A"; break;
                    case 11: c = "B"; break;
                    case 12: c = "C"; break;
                    case 13: c = "D"; break;
                    case 14: c = "E"; break;
                    case 15: c = "F"; break;
                    default: c = m.ToString(); break;
                }

                result += c.ToString();
            }
            return result;
        }
    }

    public class TSeqStack<T> : IStack<T>
    {
        private int maxsize;
        private T[] data;
        private int top;

        public T this[int index]
        {
            get { return data[index]; }
            set { data[index] = value; }
        }

        public int MaxSize
        {
            get { return maxsize; }
            set { maxsize = value; }
        }

        public int Top
        {
            get { return top; }
            set { top = value; }
        }

        public TSeqStack(int size)
        {
            data = new T[size];
            maxsize = size;
            top = -1;
        }

        public int GetLenth()
        {
            return top + 1;
        }

        public bool IsEmpty()
        {
            if (top == -1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool IsFull()
        {
            if (top == maxsize - 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void Clear()
        {
            top = -1;
        }

        public void Push(T item)
        {
            if (IsFull())
            {
                return;
            }

            data[++top] = item;
        }

        public T Pop()
        {
            T tmp = default(T);
            if (IsEmpty())
            {
                return tmp;
            }

            tmp = data[top];
            --top;
            return tmp;
        }

        public T GetTop() 
        {
            if (IsEmpty())
            {
                return default(T);
            }

            return data[top];
        }

    }

    public class TLinkStack<T> : IStack<T>
    {
        private Node<T> top;
        private int num;

        public Node<T> Top
        {
            get { return top; }
            set { top = value; }
        }

        public int Num
        {
            get { return num; }
            set { num = value; }
        }

        public TLinkStack()
        {
            top = null;
            num = 0;
        }

        public int GetLenth()
        {
            return num;
        }

        public void Clear()
        {
            top = null;
            num = 0;
        }

        public bool IsEmpty()
        {
            if (top == null && num == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void Push(T item)
        {
            Node<T> q = new Node<T>(item);

            if (top == null)
            {
                top = q;
            }
            else
            {
                q.Next = top;
                top = q;
            }

            ++num;
        }

        public T Pop()
        {
            if (IsEmpty())
            {
                return default(T);
            }

            Node<T> p = top;
            top = top.Next;
            --num;

            return p.Data;
        }

        public T GetTop()
        {
            if (IsEmpty())
            {
                return default(T);
            }

            return top.Data;
        }

    }

    public class Node<T>
    {
        private T data;
        private Node<T> next;

        public Node(T val, Node<T> p)
        {
            data = val;
            next = p;
        }

        public Node(Node<T> p)
        {
            next = p;
        }

        public Node(T val)
        {
            data = val;
            next = null;
        }

        public Node()
        {
            data = default(T);
            next = null;
        }

        public T Data
        {
            get { return data; }
            set { data = value; }
        }

        public Node<T> Next
        {
            get { return next; }
            set { next = value; }
        }

    }

    public interface IStack<T>
    {
        int GetLenth();
        bool IsEmpty();
        void Clear();
        void Push(T item);
        T Pop();
        T GetTop();
    }

    public class TStack<T> : IEnumerable<T>, ICollection, IEnumerable
    {
        public TStack() { }
        public TStack(int capacity) { }
        

        public int Count => throw new NotImplementedException();

        public object SyncRoot => throw new NotImplementedException();

        public bool IsSynchronized => throw new NotImplementedException();

        public void CopyTo(Array array, int index)
        {
            throw new NotImplementedException();
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            throw new NotImplementedException();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }
    }

}
