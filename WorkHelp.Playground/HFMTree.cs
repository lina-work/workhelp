﻿using System;

namespace WorkHelp.Playground.HFMTree
{
    public class HuffmanTree
    {
        private Node[] data;
        private int leafNum;

        public Node this[int index]
        {
            get { return data[index]; }
            set { data[index] = value; }
        }

        public int LeafNum
        {
            get { return leafNum; }
            set { leafNum = value; }
        }

        public HuffmanTree(int n)
        {
            data = new Node[2 * n - 1];
            leafNum = n;
        }


        //是一種用於無失真資料壓縮的熵編碼（權編碼）演算法
        //可用於構造總長度最短的編碼方案
        //尋找最有效的二進制編碼
        //基於有序頻率二元樹編碼的想法
        //《一種構建極小多餘編碼的方法》（A Method for the Construction of Minimum-Redundancy Codes）
        public void Create()
        {
            int max1;
            int max2;
            int tmp1;
            int tmp2;

            for (int i = 0; i < this.leafNum; ++i)
            {
                data[i].Weight = Console.Read();
            }

            for (int i = 0; i < this.leafNum - 1; ++i)
            {
                max1 = max2 = Int32.MaxValue;
                tmp1 = tmp2 = 0;

                for ( int j = 0; j < this.leafNum + i; ++j)
                {
                    if (data[i].Weight < max1 && data[i].Parent == -1)
                    {
                        max2 = max1;
                        tmp2 = tmp1;
                        tmp1 = j;
                        max1 = data[j].Weight;
                    }
                    else if (data[i].Weight < max2 && data[i].Parent == -1)
                    {
                        max2 = data[j].Weight;
                        tmp2 = j;
                    }
                }

                data[tmp1].Parent = this.leafNum + i;
                data[this.leafNum + i].Weight = data[tmp1].Weight + data[tmp2].Weight;

                data[this.leafNum + i].LChild = tmp1;
                data[this.leafNum + i].RChild = tmp2;

            }
        }


    }
    public class Node
    {
        private int weight;
        private int lChild;
        private int rChild;
        private int parent;

        public int Weight
        { 
            get { return weight; }
            set { weight = value; }
        }

        public int LChild
        {
            get { return lChild; }
            set { lChild = value; }
        }

        public int RChild
        {
            get { return rChild; }
            set { rChild = value; }
        }

        public int Parent
        {
            get { return parent; }
            set { parent = value; }
        }

        public Node()
        {
            weight = 0;
            lChild = -1;
            rChild = -1;
            parent = -1;
        }

        public Node(int w, int l, int r, int p)
        {
            weight = w;
            lChild = l;
            rChild = r;
            parent = p;
        }
    }
}
