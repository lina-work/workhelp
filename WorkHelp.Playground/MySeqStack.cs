﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkHelp.Playground.MySeqStack
{
    public interface IStack<T>
    {

        bool IsEmpty();
        void Clear();
        void Push(T item);
        T Pop();
        T GetTop();
    }

    public class SeqStack<T> : IStack<T>
    {
        private int maxsize;
        private T[] data;
        private int top;

        public T this[int index]
        {
            get { return data[index]; }
            set { data[index] = value; }
        }

        public int MaxSize
        {
            get { return maxsize; }
            set { maxsize = value; }
        }

        public int Top
        {
            get { return top; }
            set { top = value; }
        }

        public SeqStack(int size)
        {
            data = new T[size];
            maxsize = size;
            top = -1;
        }

        public int GetLenth()
        {
            return top + 1;
        }

        public void Clear()
        {
            top = -1;
        }

        public bool IsEmpty()
        {
            if (top == -1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool IsFull()
        {
            if (top == maxsize - 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void Push(T item)
        {
            if (IsFull())
            {
                return;
            }

            data[++top] = item;
        }

        public T Pop()
        {
            T tmp = default(T);
            if (IsEmpty())
            {
                return tmp;
            }

            tmp = data[top];
            --top;
            return tmp;
        }

        public T GetTop()
        {
            if (IsEmpty())
            {
                return default(T);
            }

            return data[top];
        }

    }
}
