﻿using System;
using System.Collections.Generic;
using System.IO.Compression;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace WorkHelp.Playground.Extension
{
    /// <summary>
    /// 字串擴充功能
    /// </summary>
    public static class StringExtension
    {
        /// <summary>
        /// 取得請求內容
        /// </summary>
        public static string RequestBodyText()
        {
            try
            {
                var bodyStream = new StreamReader(HttpContext.Current.Request.InputStream);
                bodyStream.BaseStream.Seek(0, SeekOrigin.Begin);
                return bodyStream.ReadToEnd();
            }
            catch (Exception ex)
            {
                return "";
            }
        }


        /// <summary>
        /// 在 null 或空白字串時，以預設文字取代
        /// </summary>
        /// <param name="source">原始字串</param>
        /// <param name="defaultValue">預設值</param>
        /// <returns>回傳字串</returns>
        public static string DefaultIfEmpty(this string source, string defaultValue)
        {
            return string.IsNullOrWhiteSpace(source) ? defaultValue : source;
        }

        /// <summary>
        /// 空白字串轉為 <code>null</code>
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static string EmptyAsNull(this string source)
        {
            return string.IsNullOrWhiteSpace(source) ? null : source;
        }

        /// <summary>
        /// 取字串的最右邊幾個字
        /// </summary>
        /// <param name="source">原始字串</param>
        /// <param name="length">Number of characters to return</param>
        /// <returns></returns>
        public static string Right(this string source, int length)
        {
            if (string.IsNullOrEmpty(source) || length < 1)
            {
                throw new ArgumentNullException(nameof(source));
            }
            else if (source.Length > length)
            {
                return source.Substring(source.Length - length, length);
            }
            else
            {
                return source;
            }
        }

        #region -- [GZip 演算法]通用函式(壓縮與解壓縮) --

        /// <summary>
        /// 將傳入的字串以GZip演算法壓縮後，傳回 Base64 編碼字串
        /// </summary>
        /// <param name="value">要壓縮的字串</param>
        /// <returns>壓縮後的字串(Base64)</returns>
        public static string Compress(this string value)
        {
            string result = string.Empty;

            if (!string.IsNullOrEmpty(value))
            {
                var bytes = Encoding.UTF8.GetBytes(value);

                if (bytes != null && bytes.Length > 0)
                {
                    var inArray = Compress(bytes);
                    result = CompressToBase64(inArray);
                }
            }

            return result;
        }

        /// <summary>
        /// GZip壓縮
        /// </summary>
        /// <param name="rawData">要壓縮的byte[]</param>
        /// <returns></returns>
        private static byte[] Compress(byte[] rawData)
        {
            byte[] result = null;

            using (var memoryStream = new MemoryStream())
            {
                using (var gZipStream = new GZipStream(memoryStream, CompressionMode.Compress, true))
                {
                    gZipStream.Write(rawData, 0, rawData.Length);
                }

                result = memoryStream.ToArray();
            }

            return result;
        }

        /// <summary>
        /// byte[] 轉換為 Base64String
        /// </summary>
        /// <param name="inArray">要轉換之byte[]</param>
        /// <returns></returns>
        private static string CompressToBase64(byte[] inArray)
        {
            if (inArray == null || inArray.Length == 0)
            {
                return string.Empty;
            }
            else
            {
                return Convert.ToBase64String(inArray);
            }
        }

        /// <summary>
        /// 解壓縮(將傳入的二進位字串資料以GZip演算法)
        /// </summary>
        /// <param name="value">傳入經 GZip 壓縮後的二進位字串資料</param>
        /// <returns>傳回原後的未壓縮原始字串資料</returns>
        public static string Decompress(this string value)
        {
            string result = string.Empty;

            if (!string.IsNullOrEmpty(value))
            {
                try
                {
                    var zippedData = Convert.FromBase64String(value);

                    if (zippedData != null && zippedData.Length > 0)
                    {
                        var bytes = Decompress(zippedData);
                        result = DecompressBytes(bytes);
                    }
                }
                catch (Exception ex)
                {
                }
            }
            return result;
        }

        /// <summary>
        /// byte[] 轉換為 string
        /// </summary>
        /// <param name="bytes">要轉換之byte[]</param>
        /// <returns></returns>
        private static string DecompressBytes(byte[] bytes)
        {
            if (bytes == null || bytes.Length == 0)
            {
                return string.Empty;
            }
            else
            {
                return Encoding.UTF8.GetString(bytes);
            }
        }

        /// <summary>
        /// GZip解壓縮
        /// </summary>
        /// <param name="zippedData">要解壓縮的byte[]</param>
        /// <returns></returns>
        private static byte[] Decompress(byte[] zippedData)
        {
            byte[] result = null;

            using (var memoryStream = new MemoryStream(zippedData))
            {
                using (var gzipstream = new GZipStream(memoryStream, CompressionMode.Decompress))
                {
                    using (var resultStream = new MemoryStream())
                    {
                        gzipstream.CopyTo(resultStream);
                        result = resultStream.ToArray();
                    }
                }
            }

            return result;
        }

        #endregion -- [GZip 演算法]通用函式(壓縮與解壓縮) --

        #region -- Convert --

        /// <summary>
        /// 將字串轉換為byteArray
        /// </summary>
        /// <param name="source">欲轉換之字串</param>
        /// <returns></returns>
        public static byte[] ToByteArray(this string source)
        {
            byte[] result = null;

            if (!string.IsNullOrWhiteSpace(source))
            {
                var outputLength = source.Length / 2;
                var output = new byte[outputLength];

                for (var i = 0; i < outputLength; i++)
                {
                    output[i] = Convert.ToByte(source.Substring(i * 2, 2), 16);
                }
                result = output;
            }

            return result;
        }
        #endregion -- Convert --
    }
}
