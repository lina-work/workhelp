﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkHelp.Playground.MySeqStack;

namespace WorkHelp.Playground.MySort
{
    public class MySortUtility
    {
        /// <summary>
        /// 直接插入排序
        /// </summary>
        public void InsertSort(SeqList<int> sqList)
        {
            for (int i = 1; i < sqList.Last; ++i)
            {
                if (sqList[i] < sqList[i - 1])
                {
                    int tmp = sqList[i];
                    int j = 0;

                    for (j = i - 1; j >= 0 && tmp < sqList[j]; --j)
                    {
                        sqList[j + 1] = sqList[j];
                    }
                    sqList[j + 1] = tmp;
                }
            }
        }

        /// <summary>
        /// 冒泡排序
        /// </summary>
        public void BubbleSort(SeqList<int> sqList)
        {
            int tmp;
            for (int i = 0; i < sqList.Last; ++i)
            { 
                for (int j = sqList.Last - 1; j >= i; --j)
                {
                    if (sqList[j + 1] < sqList[j])
                    {
                        tmp = sqList[j + 1];
                        sqList[j + 1] = sqList[j];
                        sqList[j] = tmp;
                    }
                }
            }
        }

        /// <summary>
        /// 簡單選擇排序
        /// </summary>
        public void SimpleSelectSort(SeqList<int> sqList)
        {
            int tmp = 0;
            int t = 0;
            for (int i  = 0; i < sqList.Last; ++i)
            {
                t = i;
                for (int j = i + 1; j <= sqList.Last; ++j)
                {
                    if (sqList[t] > sqList[j])
                    {
                        t = j;
                    }
                }
                tmp = sqList[i];
                sqList[i] = sqList[t];
                sqList[t] = tmp;
            }
        }

        /// <summary>
        /// 快速排序
        /// </summary>
        public void QuickSort(SeqList<int> sqList, int low, int high)
        {
            int i = low;
            int j = high;
            int tmp = sqList[low];
            while(low < high)
            {
                while((low < high) && (sqList[high] >= tmp))
                {
                    --high;
                }
                sqList[low] = sqList[high];
                ++low;
                while((low < high) && (sqList[low] <= tmp))
                {
                    ++low;
                }
                sqList[high] = sqList[low];
                --high;
            }
            sqList[low] = tmp;

            if (i < low - 1)
            {
                QuickSort(sqList, i, low - 1);
            }

            if (low + 1 < j)
            {
                QuickSort(sqList, low + 1, j);
            }
        }

        /// <summary>
        /// 堆排序 - 建堆
        /// </summary>
        public void CreateHeap(SeqList<int> sqList, int low, int high)
        { 
            if ((low < high) && (high <= sqList.Last))
            {
                int j = 0;
                int tmp = 0;
                int k = 0;
                for (int i = high / 2; i >= low; --i)
                {
                    k = i;
                    j = 2 * k + 1;
                    tmp = sqList[i];
                    while (j <= high)
                    {
                        if ((j < high) && (j + 1 <= high) && (sqList[j] < sqList[j + 1]))
                        {
                            ++j;
                        }

                        if (tmp < sqList[j])
                        {
                            sqList[k] = sqList[j];
                            k = j;
                            j = 2 * k + 1;
                        }
                        else
                        {
                            j = high + 1;
                        }
                    }
                    sqList[k] = tmp;
                }
            }
        }

        /// <summary>
        /// 堆排序
        /// </summary>
        public void HeapSort(SeqList<int> sqList)
        {
            int tmp = 0;

            CreateHeap(sqList, 0, sqList.Last);

            for (int i = sqList.Last; i > 0; --i)
            {
                tmp = sqList[0];
                sqList[0] = sqList[i];
                sqList[i] = tmp;

                CreateHeap(sqList, 0, i - 1);
            }
        }

        /// <summary>
        /// 歸併排序
        /// </summary>
        public void Merge(SeqList<int> sqList, int len)
        {
            int m = 0;
            int l1 = 0;
            int h1;
            int l2;
            int h2;
            int i = 0;
            int j = 0;

            SeqList<int> tmp = new SeqList<int>(sqList.GetLength());

            while (l1 + len < sqList.GetLength())
            {
                l2 = l1 + len;
                h1 = l2 - 1;

                h2 = (l2 + len - 1 < sqList.GetLength())
                    ? l2 + len - 1 : sqList.Last;

                j = l2;
                i = l1;

                while ((i <= h1) && (j <= h2))
                {
                    if (sqList[i] <= sqList[j])
                    {
                        tmp[m++] = sqList[i++];
                    }
                    else
                    {
                        tmp[m++] = sqList[j++];
                    }
                }

                while (i <= h1)
                {
                    tmp[m++] = sqList[i++];
                }

                while (j <= h2)
                {
                    tmp[m++] = sqList[j++];
                }

                l1 = h2 + 1;
            }

            i = l1;

            while (i < sqList.GetLength())
            {
                tmp[m++] = sqList[i++];
            }

            for (i = 0; i < sqList.GetLength(); ++i)
            {
                sqList[i] = tmp[i];
            }
        }

        /// <summary>
        /// 歸併排序
        /// </summary>
        public void MergeSort(SeqList<int> sqList)
        {
            int k = 1;

            while (k < sqList.GetLength())
            {
                Merge(sqList, k);
                k *= 2;
            }
        }

    }
}
