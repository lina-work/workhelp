﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkHelp.Extensions;
using WorkHelp.Playground.Models;

namespace WorkHelp.Playground
{
    class Program
    {
        /// <summary>
        /// 存取鎖
        /// </summary>
        private static object _WriteFileLock = new object();

        static void Main(string[] args)
        {
            var in_name = "劉黃元澤(慈濟大學)";
        }
        static void QMDMWQLMQMDMWQLMQMDMWQLM()
        {
            var partners = new List<TPartner>();
            partners.Add(new TPartner { group = "主辦單位", name = "台北市政府" });
            partners.Add(new TPartner { group = "主辦單位", name = "台北市觀傳局" });
            partners.Add(new TPartner { group = "協辦單位", name = "原創網路服務" });
            partners.Add(new TPartner { group = "協辦單位", name = "町巷" });
            partners.Add(new TPartner { group = "贊助單位", name = "舒跑" });

            var groups = partners
                .GroupBy(c => new
                {
                    c.group,
                })
                .Select(gcs => new TPGroup<TPartner>()
                {
                    Label = gcs.Key.group,
                    Children = gcs.ToList()
                }).ToList();

            
            var json = Newtonsoft.Json.JsonConvert.SerializeObject(groups);
            Console.WriteLine(json);
            Console.Read();
        }

        private class TPGroup<T>
        {
            public string Label { get; set; }
            public List<T> Children { get; set; }
        }
        private class TPartner
        {
            public string group { get; set; }
            public string name { get; set; }
        }

        private static string GetCompressStr()
        {
            return "H4sIAAAAAAAEAN3XW0/aUBwA8K+y9EkTTXp6Qe2jwRkeNJm68LDsQSNTEi/Lpg/LssR5Q7mtOgQ2wcu8sVUYDA2IC34Zzmn7LXZaASu0HXU2JOPpXP6NyI//hRfvieF5gqMphqW6iFHvgofgiAGiixgYmV+cm8QboGzcHu/U9ALeyUK8UkyLkQJM8+Llqnh+XiluihdfcZB7Gj/txDGicAUTodvI2vkwPod8HiVzklDCh/0zi2osfhplt+qxyrEaeh2F1yV07q8978KHZDXAVX1X6oVbc1Fbqxejmova2u2dw6t+vHK+GvPOKv8rw5Gkuh9UgvBqTD2mOcDizfPXk+MLnqczU7d3Q8OLsxOeN3jjGhrrJpUXoBzKhXP8HT6lSIrpBqAbUMqHpgkeUIMphqIZBl+53o7MT6jvhSQ+dN0R0PoElGUCFPXBXK6ZACXDldKeeMJrCCRfCbPIQuQ+gbRyKAllHN0SAXh8AsCpH6MNBKwJAaNPQFsmgKEY8u80E0ixEMyciKmAhgAGY7C4DI/PIB9szoXQZym1JCei7csFirYHwmEC4dCHYC1DyMFTuB3UKUf+TbRblKNhDQSKh2G50EzA5yUhg+JL/0YAHkxAcWyvPQS9JgS9+gQ9lgkaqvxdOdqLyb/DKL3+93KEQ6WVY+nHQbuyABP02ULAksYEDlKfoM96OTo+E/n1ZgKxvIayRZhIaQgaGriGAP8B+GmjfQQMsIeAMiEABnMRab0rn/jRt7VmA/lLHu7sy/64Ng0CW9LRYXMlygRQ5EYS0loDoDEgjQyAkQGoG7hbMBgcfUJyjD2zEUubMBiNp9bnUxTN4m+4WDxC+xF4FkGJlUaM9QO4ut0B+V30s8rV2eF81tlqx1YSJeOTrreNEsUQ6ZESpXpsg5DJ9OowmF6B9fG1oQdoW7a8VETJrDZR4j5UzqPLQqUU0BiI4YC4XBIv4u0qVvYZmIyvDoPxFVifX416tpTKicIRWi00ZsT9ebdesMo8nqgqV+X/z8FkenWwBg7MA6qV/k85uLWMTjPo43eNAQ7BVQnXN73f1OjXRuVmD6aTlltHCxKttA7A0Ta1755GiZd/AHENlvjYEAAA"
;
                }
        private static string ResetName(string value)
        {
            if (string.IsNullOrWhiteSpace(value)) return "";
            var words = value.Split(new char[] { '(' }, StringSplitOptions.RemoveEmptyEntries);
            var w1 = words != null && words.Length > 0 ? words[0] : "";
            var w2 = words != null && words.Length > 1 ? words[1].Replace(")", "") : "";
            if (w2 == "") return w1;
            return "(" + w2 + ")" + w1;
        }

        static void ADONEIONIOQNOIQN()
        {
            TSvy svy = new TSvy { title = "標題" };
            TSvyExt ext = (TSvyExt)svy;
            ext.exntend = "XxX";
            Console.WriteLine(ext);
            Console.ReadLine();
        }

        private class TSvy
        {
            public string title { get; set; }
        }

        private class TSvyExt : TSvy
        {
            public string exntend { get; set; }
        }


        static void XOIJOCWJOIWOJEFWJEOJPQoJWPQJP()
        {
            var in_meeting_code  = "IMT-00001194";
            var a = in_meeting_code.Substring(in_meeting_code.Length - 3) + "0001";
            var b = in_meeting_code.Substring(in_meeting_code.Length - 4) + "0001";
            Console.WriteLine($"in_meeting_code.Length - 3: {a}");
            Console.WriteLine($"in_meeting_code.Length - 4: {b}");

            var s = "11940001";
            var c = s.Right(s.Length - 1);
            Console.WriteLine(c);
            Console.ReadLine();
        }

        static string FixSectName(string value)
        {
            var resule = "";
            var arr = value.Split(new char[] { '：' }, StringSplitOptions.RemoveEmptyEntries);
            var w1 = arr.Length > 0 ? arr[0] : "";
            var w2 = arr.Length > 1 ? arr[1] : "";
            
            if (w2.Contains("以上至"))
            { 
                w2 = w2.Replace(".1公斤以上至", "~");
                w2 = w2.Replace(".0公斤以下", "");
                resule = w1.Replace("-", ": ") + "(" + w2 + ")";
            }
            else
            {
                resule = w1.Replace("-", ": ") + "(" + w2 + ")";
            }
            return resule;
        }
        static void JDQIJWONWDOQINOQJPI()
        {
            var path = @"C:\Users\Administrator\source\innosoft repos\in_api\In_api\dat\CTOA\113年臺北市青年盃定向越野錦標賽";
            path.Split(new char[] { '、' }, StringSplitOptions.RemoveEmptyEntries).ToList();

            var clips = new List<TClip>();
            var dir = new System.IO.DirectoryInfo(path);
            var dirs = dir.GetDirectories();
            foreach (var d in dirs)
            {
                var splitFile = System.IO.Path.Combine(d.FullName, $"test.dat");
                lock (_WriteFileLock)
                {
                    clips.Add(new TClip
                    {
                        full = splitFile,
                        lines = System.IO.File.ReadAllLines(splitFile, System.Text.Encoding.UTF8),
                    });
                }
            }

            var length = clips.Sum(x => x.lines.Length);
            var currentIndex = 0;

            var lines = new string[length];
            foreach (var clip in clips)
            {
                clip.lines.CopyTo(lines, currentIndex);
                currentIndex += clip.lines.Length;
            }

            Console.ReadLine();
        }

        private class TClip
        {
            public string full { get; set; }
            public string[] lines { get; set; }
        }

        static void JDIEOIWEOWOEODJWJE()
        {
            ResetWrestlingSectPlayers();
        }

        static void ResetWrestlingSectPlayers()
        {
            try
            {
                var file = @"C:\temp\113全大運-角力-組別選手.txt";
                var lines = File.ReadAllLines(file, System.Text.Encoding.UTF8);
                var rows = new List<TAge>();
                var lastAge = default(TAge);
                var lastGroup = default(TGroup);
                foreach (var line in lines)
                {
                    if (string.IsNullOrWhiteSpace(line)) continue;

                    var values = line.Trim();
                    var words = default(string[]);

                    if (values.Contains("]."))
                    {
                        lastAge = new TAge
                        {
                            weights = new List<TGroup>(),
                        };

                        words = values.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
                        var w1 = words[0].Replace("[", "").Replace("]", "");
                        var w2 = (words[1].Split(new char[] { '(' }, StringSplitOptions.RemoveEmptyEntries))[0];

                        lastAge.code = w1;
                        lastAge.name = w2;
                        rows.Add(lastAge);
                        continue;
                    }

                    var c1 = values[1];
                    var c2 = values[2];
                    if (c1 == '.' || c2 == '.')
                    {
                        words = values.Split(new char[] { '(' }, StringSplitOptions.RemoveEmptyEntries);
                        var names = words[0].Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);

                        lastGroup = new TGroup
                        {
                            code = names[0].Trim(),
                            name = names[1].Trim(),
                            players = new List<TPlayer>()
                        };
                        lastAge.weights.Add(lastGroup);
                        continue;
                    }

                    words = values.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

                    var mx = words.Length - 1;
                    for (var i = 0; i < words.Length; i = i + 2)
                    {
                        if ((i + 1) > mx) continue;

                        var s1 = words[i + 0].Trim();
                        var s2 = words[i + 1].Trim();

                        var arr = s2.Split(new char[] { '(' }, StringSplitOptions.RemoveEmptyEntries);
                        var name = arr[0].Trim();
                        var org = arr.Length > 1 ? arr[1].Trim().Replace(")", "") : string.Empty;

                        lastGroup.players.Add(new TPlayer
                        {
                            code = s1,
                            name = name,
                            org = org,
                        });
                    }
                }


                var data = new StringBuilder();
                for (var i = 0; i < rows.Count; i++)
                {
                    var row = rows[i];
                    for (var j = 0; j < row.weights.Count; j++)
                    {
                        var w = row.weights[j];
                        for (var k = 0; k < w.players.Count; k++)
                        {
                            var p = w.players[k];
                            data.AppendLine($"{row.code}\t{row.name}\t{w.name}\t{p.code}\t{p.org}\t{p.name}");
                        }
                    }
                }

                var newSuffix = DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".txt";
                var output = file.Replace(".txt", newSuffix);

                File.WriteAllText(output, data.ToString(), System.Text.Encoding.UTF8);

                Console.WriteLine("FINISHED.");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            Console.ReadLine();
        }

        static void FNUIWEBNWQONIOQWNEQ()
        {
            int s = 0;
            int e = 128;
            for (int i = s; i <= e; i++)
            {
                var pages = GenerateWightPage(i);
                Console.WriteLine($"當前人數 = {i}，頁數 = {pages.Count}");
                for (int j = 0; j < pages.Count; j++)
                {
                    var page = pages[j];
                    Console.WriteLine($"\t - 頁碼: {j + 1}, MIN = {page.min} \t MAX = {page.max} \t 範本 = {page.sheetName}");
                }
                Console.WriteLine();
            }

            Console.Read();
        }

        private class Item
        {
        }

        private static List<TWeightPage> GenerateWightPage(int count)
        {
            var pages = new List<TWeightPage>();
            var c1 = count;
            var c2 = 26;
            var c3 = 20;
            var n1 = c2 + "N";

            var min = 0;
            var max = c1 - 1;

            while (c1 > 0)
            {
                var sheetName = c1.ToString();
                if (c1 > c2)
                {
                    sheetName = n1;
                    max = min + c2 - 1;
                }
                else if (c1 < c3)
                {
                    sheetName = c3.ToString();
                    max = min + c1 - 1;
                }
                else
                {
                    max = min + c1 - 1;
                }

                pages.Add(new TWeightPage
                {
                    sheetName = sheetName,
                    pagePlayers = new List<Item>(),
                    min = min,
                    max = max,
                });

                c1 = c1 - c2;
                min = min + c2;
                max = min + c2 - 1;
            }
            return pages;
        }

        private class TWeightPage
        {
            public int min { get; set; }
            public int max { get; set; }
            public string sheetName { get; set; }
            public List<Item> pagePlayers { get; set; }
        }

        public class TAge
        {
            public string code { get; set; }
            public string name { get; set; }
            public List<TGroup> weights { get; set; }
        }

        public class TGroup
        {
            public string code { get; set; }
            public string name { get; set; }
            public List<TPlayer> players{ get; set; }
        }

        public class TPlayer
        {
            public string code { get; set; }
            public string name { get; set; }
            public string org { get; set; }
        }

        static void ResetWrestlingOrgPlayers()
        {
            try
            {
                var file = @"C:\temp\全中運-角力-選手編號.txt";
                var lines = File.ReadAllLines(file, System.Text.Encoding.UTF8);
                var rows = new List<TGroup>();
                var last = default(TGroup);
                foreach(var line in lines) 
                {
                    if (string.IsNullOrWhiteSpace(line)) continue;

                    var values = line.Trim();
                    var words = default(string[]);

                    if (values.Contains("]."))
                    {
                        last = new TGroup
                        {
                            players = new List<TPlayer>(),
                        };

                        words = values.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
                        var w1 = words[0].Replace("[", "").Replace("]", "");
                        var w2 = (words[1].Split(new char[] { '(' }, StringSplitOptions.RemoveEmptyEntries))[0];

                        last.code = w1;
                        last.name = w2;
                        rows.Add(last);
                        continue;
                    }

                    var c = values[0];
                    if (!char.IsDigit(c)) continue;

                    words = values.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

                    var mx = words.Length - 1;
                    for (var i = 0; i < words.Length; i = i + 2) 
                    {
                        if ((i + 1) > mx) continue;

                        var s1 = words[i+0].Trim();
                        var s2 = words[i+1].Trim();
                        last.players.Add(new TPlayer
                        {
                            code = s1,
                            name = s2,
                        });
                    }
                }


                var data = new StringBuilder();
                for (var i = 0; i <  rows.Count; i++) 
                {
                    var row = rows[i];
                    for (var j = 0; j < row.players.Count; j++) 
                    {
                        var p = row.players[j];
                        data.AppendLine($"{row.code}\t{row.name}\t{p.code}\t{p.name}");
                    }
                }

                var output = $@"C:\temp\全中運-角力-選手編號_{DateTime.Now.ToString("yyyyMMdd_HHmmss")}.txt";

                File.WriteAllText(output, data.ToString(), System.Text.Encoding.UTF8);

                Console.WriteLine("FINISHED.");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            Console.ReadLine();
        }

        static void MDOIEODIJPQJWPJPOJPOWPJQOIHQGYGTCRTIHNO()
        {
            try
            {
                var file = @"C:\temp\sportopen\data2.json";
                var json = File.ReadAllText(file, System.Text.Encoding.UTF8);
                var list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<TUserSurveyResult>>(json);

                var json2 = Newtonsoft.Json.JsonConvert.SerializeObject(list);

                var baseUrl = "https://localhost:44333";
                var resource = "/Module/MultipleRegister";

                var client = new RestSharp.RestClient(baseUrl);
                //client.Timeout = 30 * 1000; // 30秒為逾期


                var request = new RestSharp.RestRequest(resource
                    , RestSharp.Method.POST
                    , RestSharp.DataFormat.Json);

                request.AddJsonBody(json2);

                var response = client.Post(request);

                var data = response.Content;

                Console.WriteLine(data);
            }
            catch (Exception ex) 
            {
                Console.WriteLine(ex.Message);
            }

            Console.ReadLine();
        }
        public class TUserSurveyResult
        {
            public string meeting_id { get; set; }
            public string muid { get; set; }
            public string primary_value { get; set; }
            public string in_token { get; set; }
            public string in_creator_sno { get; set; }
            public string payment_number { get; set; }
            public List<TSurveyO> questions { get; set; }
        }
        public class TSurveyO
        {
            public string sid { get; set; }
            public string property { get; set; }
            public string answer { get; set; }
        }

        public class TUserSurveyEditView
        {
            public string meeting_id { get; set; }
            public string muid { get; set; }
            public string sid { get; set; }
            public string property { get; set; }
            public string title { get; set; }
            public string answer { get; set; }

        }
        public class TUserListSurveyResult
        {
            public string meeting_id { get; set; }
            public string[] muidList { get; set; }
            public string primary_value { get; set; }
            public string in_token { get; set; }
            public string in_creator_sno { get; set; }
        }
        public class TSurveyOption
        {
            public int no { get; set; }
            public string oid { get; set; }
            public string label { get; set; }
            public string value { get; set; }
            public string selected { get; set; }
            public int count { get; set; }
            public string expense_value { get; set; }
            public string extend_value { get; set; }
            public string extend_value2 { get; set; }
            public string filter { get; set; }
            public string grand_filter { get; set; }


            public string GetGenderValue(string property)
            {
                return GetExtendValue(property);
            }
            public TRangeDate GetBirthDayValue(string property)
            {
                string rangeDatetime = GetExtendValue(property);
                string[] rangeDatetimeArr = rangeDatetime.Split('~');
                TRangeDate rangeDate = new TRangeDate();
                DateTime start = new DateTime();
                DateTime end = new DateTime();
                if (rangeDatetimeArr.Length == 2)
                {
                    if (DateTime.TryParse(rangeDatetimeArr[0], out start))
                    {
                        rangeDate.start = start;
                        rangeDate.condition = "_start";
                    }
                    if (DateTime.TryParse(rangeDatetimeArr[1], out end))
                    {
                        rangeDate.end = end;
                        rangeDate.condition += "_end";
                    }
                }
                return rangeDate;
            }
            public string GetExtendValue(string property)
            {
                string[] extendArr = extend_value.Split('|');
                foreach (string extend in extendArr)
                {
                    string[] extendValue = extend.Split(':');
                    if (extendValue[0].ToLower() == property)
                    {
                        return extendValue[1];
                    }
                }
                return "";
            }
        }

        public class TRangeDate
        {
            public string condition { get; set; }
            public DateTime? start { get; set; }
            public DateTime? end { get; set; }
        }
        public class TSurveyOptionView : TSurvey
        {
            public string oid { get; set; }
            public string label { get; set; }
            public string value { get; set; }
            public string expense_value { get; set; }
            public string extend_value { get; set; }
            public string extend_value2 { get; set; }
            public string filter { get; set; }
            public string grand_filter { get; set; }
        }

        public class TSurveySqlModel
        {
            public string sid { get; set; }
            public string title { get; set; }
            public string ctrl { get; set; }
            public string property { get; set; }

        }

        static void djioejoqiwo()
        {
            var result = GetNamesTW("高男組", "自由式", "第一級：57.0 公斤以下");
            Console.WriteLine(result[0]);
            Console.WriteLine(result[1]);
            Console.ReadLine() ; 
        }

        static string[] GetNamesTW(string in_l1, string in_l2, string in_l3)
        {
            string value = in_l3;
            if (!value.EndsWith("級") && !value.Contains("級："))
            {
                value = value.Replace("級", "級：");
            }

            string in_name2 = "";
            string in_name3 = "";

            string[] arr = value.Split(new char[] { '：' }, StringSplitOptions.RemoveEmptyEntries);
            if (arr != null && arr.Length > 1)
            {
                string[] arr2 = arr[1].Split(new char[] { '至' }, StringSplitOptions.RemoveEmptyEntries);
                string in_weight = arr2.Length == 1 ? arr2[0] : arr2[1];
                in_weight = in_weight.Replace(" ", "");

                in_name2 = in_l1 + "-" + in_l2 + "-" + arr[0];
                in_name3 = in_l1 + "-" + in_l2 + "-" + arr[0] + "-" + in_weight;
            }
            else
            {
                in_name2 = in_l1 + "-" + in_l2 + "-" + in_l3;
                in_name3 = in_name2;
            }

            return new string[] { in_name2, in_name3 };
        }
        static void FMROIWNEIOWNO()
        {
            Console.WriteLine("Hello, world.");

            var report = new TRpt
            {
                site = "B",
                number = "666",
                section = "國男組 希羅式 45.1公斤以上至48.0公斤以下",
                foot1 = new TRptPlayer
                { 
                    org = "縣立宏仁國中",
                    name = "高周遠",
                    no = "1006",
                },
                foot2 = new TRptPlayer
                {
                    org = "縣立介壽國中",
                    name = "周楷晸",
                    no = "1009",
                },
            };

            LoadReport(report);
            
            Console.Read();
        }

        private class TRpt
        {
            public string site { get; set; }
            public string number { get; set; }
            public string section { get; set; }
            public TRptPlayer foot1 { get; set; }
            public TRptPlayer foot2 { get; set; }
        }

        private class TRptPlayer
        {
            public string no { get; set; }
            public string org { get; set; }
            public string name { get; set; }
        }

        static void LoadReport(TRpt report)
        {
            try
            {
                var resource = @"C:\temp\TA.docx";

                //載入模板
                Xceed.Words.NET.DocX doc = Xceed.Words.NET.DocX.Load(resource);

                //場次編號
                var cellNumber = doc.Tables[0].Rows[0].Cells[0].Paragraphs.First();
                cellNumber.Append(report.number);

                string xlsName = "113全中運角力計分表_0420_" + report.site + "_" + report.number;
                
                xlsName = xlsName.Replace("+", "")
                                .Replace("-", "")
                                .Replace(".", "")
                                .Replace(" ", "");

                var export_path = @"C:\temp\"+ xlsName + ".docx";

                doc.SaveAs(export_path);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }
        static void LoadDocPosition()
        {
            try
            {
                var resource = @"C:\temp\TA.docx";
                //載入模板
                Xceed.Words.NET.DocX doc = Xceed.Words.NET.DocX.Load(resource);
                var tableCount = doc.Tables.Count;
                for (var i = 0; i < tableCount; i++)
                {
                    var table = doc.Tables[i];
                    var rows = table.Rows;
                    for (var j = 0; j < rows.Count; j++)
                    {
                        var row = rows[j];
                        var cells = row.Cells;
                        for (var k = 0; k < cells.Count; k++)
                        {
                            var cell = cells[k];
                            foreach(var p in cell.Paragraphs)
                            {
                                string text = p.Text;
                                if (string.IsNullOrWhiteSpace(text)) continue;
                                Console.WriteLine($"Table[{i}] Rows[{j}] Cells[{k}] = {text}");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }

        static void UHDOOQIWJOIQJWOIQJOQ()
        {
            var result = Distance();
            var content = result.Content;
            Console.WriteLine(content);
            Console.Read();
        }

        public static IRestResponse Distance()
        {
            //https://maps.googleapis.com/maps/api/distancematrix/json

            var client = new RestClient();
            client.BaseUrl = new Uri("https://maps.googleapis.com/");
            client.AddDefaultParameter("key", "AIzaSyCInztt40lOPZqi6Isw58udhWr7ZwmAlbs", ParameterType.QueryString);

            var request = new RestRequest("maps/api/distancematrix/json?language=zh-TW");
            request.AddQueryParameter("destinations", "台北市大同區承德路一段48號");
            request.AddQueryParameter("origins", "台北市大同區長安西路39號");
            request.AddQueryParameter("units", "imperial");
            //request.AddQueryParameter("address", "臺北市大同區承德路一段48號");
            
            return client.Get(request);
        }

        public static IRestResponse GoogleAddress()
        {
            //https://maps.googleapis.com/maps/api/geocode/json

            var client = new RestClient();
            client.BaseUrl = new Uri("https://maps.googleapis.com/");
            client.AddDefaultParameter("key", "AIzaSyCInztt40lOPZqi6Isw58udhWr7ZwmAlbs", ParameterType.QueryString);

            var request = new RestRequest("maps/api/geocode/json?language=zh-TW");
            request.AddQueryParameter("latlng", "25.051516261423007,121.51693591783835");
            //request.AddQueryParameter("address", "臺北市大同區承德路一段48號");

            return client.Get(request);
        }

        static void XKCOOI()
        {
            var disks = GetHardDiskFreeSpace();
            foreach(var disk in disks)
            {
                Console.WriteLine(disk.name + "(" + disk.label + ")" + ": " + disk.total_free_space + "GB(" + disk.perct + ")");
            }
            Console.Read();
        }

        private static List<TDisk> GetHardDiskFreeSpace()
        {
            List<TDisk> disks = new List<TDisk>();
            System.IO.DriveInfo[] drives = System.IO.DriveInfo.GetDrives();
            foreach (System.IO.DriveInfo drive in drives)
            {
                var disk = new TDisk 
                {
                    name = drive.Name,
                    label = drive.VolumeLabel,
                    total_free_space = drive.TotalFreeSpace / (1024 * 1024 * 1024),
                    total_size = drive.TotalSize / (1024 * 1024 * 1024),
                };

                if (disk.total_size > 0)
                {
                        disk.perct = ((decimal)disk.total_free_space * 100 / disk.total_size).ToString("##") + "%";
                }

                disks.Add(disk);
            }
            return disks;
        }

        private class TDisk
        {
            public string name { get; set; }
            public string label { get; set; }
            public long total_free_space { get; set; }
            public long total_size { get; set; }
            public string perct { get; set; }
        }

        //static System.Diagnostics.PerformanceCounter cpu = 
        //    new System.Diagnostics.PerformanceCounter("Processor", "% Processor Time", "_Total");
        //static System.Diagnostics.PerformanceCounter memory = 
        //    new System.Diagnostics.PerformanceCounter("Memory", "% Available Bytes in Use");

        static void SSSSS()
        {
            var info = new Microsoft.VisualBasic.Devices.ComputerInfo();
            var total = info.TotalPhysicalMemory;
            total = total / 1024;
            total = total / 1024;

            var avail = info.AvailablePhysicalMemory;
            avail = avail / 1024;
            avail = avail / 1024;
            
            var a = avail * 100000 / total;
            var u = 100000 - a;
            var perc = (decimal)u / 100000 * 100;
            
            Console.WriteLine("Userd Memory: {0:n0}%", perc);
            Console.ReadLine();
            //while (true)
            //{
            //    Console.WriteLine("CPU: {0:n1}%", cpu.NextValue());
            //    Console.WriteLine("Memory: {0:n0}%", memory.NextValue());
            //    System.Threading.Thread.Sleep(3 * 1000);
            //}
        }

        static void XXXXXXXXX()
        {
            string file = @"C:\App\WinStatus\iislog\cmd.log";
            string contents = "start";
            System.IO.File.WriteAllText(file, contents, System.Text.Encoding.UTF8);
            //StringBuilder builder_csv = new StringBuilder();
            //StringBuilder builder_sql = new StringBuilder();

            //var log_path = @"C:\App\PLMCTA\error\[PLMCTA]In_Payment_List_Edi - 複製.log";
            //var contents = System.IO.File.ReadAllText(log_path, System.Text.Encoding.UTF8);

            //var path = @"C:\App\PLMCTA\error";
            //var dir = new System.IO.DirectoryInfo(path);
            //var files = dir.GetFiles();
            //for (int i = 0; i < files.Length; i++)
            //{
            //    var file = files[i];
            //    var name = file.Name;
            //    if (name.EndsWith(".x"))
            //    {
            //        var pos1 = contents.IndexOf(name);
            //        var pos2 = contents.IndexOf("銷帳編號:", pos1);

            //        var code = contents.Substring(pos2 + "銷帳編號:".Length, 16);

            //        builder_csv.AppendLine(file.Name + "\t" + code);
            //        builder_sql.AppendLine("INSERT INTO IN_ERR_TXN(txn, in_code_2) VALUES(N'"+ name + "', N'"+ code + "')");

            //    }
            //}

            //WorkHelp.Logging.TLog.Watch(message: "\r\n" + builder_csv.ToString());
            //WorkHelp.Logging.TLog.Watch(message: "\r\n" + builder_sql.ToString());

        }
        static void XXXXXX()
        {
            StringBuilder builder = new StringBuilder();
            var path = @"C:\App\PLMCTA\error";
            var dir = new System.IO.DirectoryInfo(path);
            var files = dir.GetFiles();
            foreach (var file in files)
            {
                if (file.Name.EndsWith(".x"))
                {
                    builder.AppendLine(file.Name);
                }
            }

            WorkHelp.Logging.TLog.Watch(message: "\r\n" + builder.ToString());

        }

        static void AAAAAAA()
        {
            Console.WriteLine("START:");

            int max = 50;

            for (int i = 1; i <= max; i++)
            {
                string no = i.ToString().PadLeft(2, '0');
                string nm = (i + 1).ToString().PadLeft(2, '0');

                string uid = "2420520723C44A70B5F312C38B238" + no;
                string ds = "";
                string de = "";

                int m = i / 30;
                int d1 = (i % 30) + 1;
                int d2 = d1 + 1;

                string s1 = d1.ToString().PadLeft(2, '0');
                string s2 = d2.ToString().PadLeft(2, '0');
                if (m == 0)
                {
                    ds = $"202112{s1}T100000Z";
                    de = $"202112{s2}T100000Z";

                }
                else
                {
                    ds = $"202201{s1}T100000Z";
                    de = $"202201{s2}T100000Z";
                }

                string file_name = $@"C:\site\justmove\assets\test-{no}.ics";
                StringBuilder contents = new StringBuilder();
                contents.AppendLine("BEGIN:VCALENDAR");
                contents.AppendLine("PRODID:-//Google Inc//Google Calendar 70.9054//EN");
                contents.AppendLine("VERSION:2.0");
                contents.AppendLine("CALSCALE:GREGORIAN");
                contents.AppendLine("METHOD:PUBLISH");
                contents.AppendLine($"X-WR-CALNAME:{no}課行事曆");
                contents.AppendLine("X-WR-TIMEZONE:Asia/Taipei");
                contents.AppendLine($"X-WR-CALDESC:{no}課行事曆");
                contents.AppendLine("BEGIN:VEVENT");
                contents.AppendLine($"UID:{uid}@innosoft.com.tw");
                contents.AppendLine($"DTSTART:{ds}");
                contents.AppendLine($"DTEND:{de}");
                contents.AppendLine("LOCATION:台灣\\, 台北當代藝術館");
                contents.AppendLine("DESCRIPTION;X-ALT-DESC;FMTTYPE=text/html:<a href='https://www.google.com.tw/'> GOOGLE </a>");
                contents.AppendLine($"SUMMARY:{no}課工作摘要");
                contents.AppendLine("PRIORITY:3");
                contents.AppendLine("END:VEVENT");
                contents.AppendLine("END:VCALENDAR");

                WriteFileNoBOM(file_name, contents.ToString());
            }

            Console.WriteLine("END.");
            Console.ReadLine();
        }

        /// <su
        /// mmary>
        /// 新建或覆寫檔案
        /// </summary>
        private static void WriteFileNoBOM(string path, string contents)
        {
            Encoding utf8WithoutBom = new UTF8Encoding(false);
            File.WriteAllText(path, contents, utf8WithoutBom);
        }

        static void AAAA()
        {
            string in_current_org = "羅東國小";
            string in_stuff_b1 = in_current_org.Substring(0, 3);
            bool is_digit = true;

            foreach (var c in in_stuff_b1)
            {
                if (!char.IsDigit(c))
                {
                    is_digit = false;
                    break;
                }
            }

            if (is_digit)
            {
                Console.WriteLine("皆為數字");
            }
            else
            {
                Console.WriteLine("資料有誤");
            }

            Console.Read();

            ////var obj = new AAA { BBB = "deji" };
            //var url = "https%3A%2F%2Fact.innosoft.com.tw%2Fjudo%2Fpages%2Fb.aspx%3Fpage%3DPublicNextSearch.html%26method%3Din_meeting_program_score_s%26meeting_id%3DAFFECDB9D87E42E88C003C868A5483AA%26site_id%3DE750100ACB304600913DD883E9C0EDF8%26in_date%3D2021-11-10%26scene%3Dnext%26open%3D1%26auto%3D0";
            //var eurl = System.Web.HttpUtility.UrlDecode(url);
            //Console.WriteLine(eurl);
            //Console.Read();
            //int sect_count = 6;
            //int sect_start = 2;
            //int round_serial = 1;
            //int[] fight_arr = GetFightArr(sect_count, sect_start, round_serial);
            //List<string> a = fight_arr.TO
            //Console.WriteLine("arr: " + string.Join(",", fight_arr));
            //Console.Read();
        }

        private class AAA
        {
            public string BBB { get; set; } = "cjidjcd";
        }
        private static int[] GetFightArr(int count, int start, int serial)
        {
            int[] result = new int[count];
            int rno = start + (serial - 1);
            int sidx = rno - 1;
            int eidx = sidx + count;

            int no = 1;
            for (int i = sidx; i < eidx; i++)
            {
                var idx = i % count;
                result[idx] = no;
                no++;
            }

            return result;
        }

        void XXX()
        {
            //string id = System.Guid.NewGuid().ToString();

            //WorkHelp.Logging.TLog.Watch(message: id);

            //Console.WriteLine(id);
            //DateTime dtStart = DateTime.Now;

            //int max = 100;
            //for (int i = 1; i <= max; i++)
            //{
            //    SurveyPage(i, dtStart);
            //}

            long totalSize = new long();
            long totalFreeSpace = new long();
            long availableFreeSpace = new long();
            string str_HardDiskName = @"C:\";

            System.IO.DriveInfo[] drives = System.IO.DriveInfo.GetDrives();
            foreach (System.IO.DriveInfo drive in drives)
            {
                if (drive.Name == str_HardDiskName)
                {
                    totalSize = drive.TotalSize / (1024 * 1024 * 1024);
                    totalFreeSpace = drive.TotalFreeSpace / (1024 * 1024 * 1024);
                    availableFreeSpace = drive.AvailableFreeSpace / (1024 * 1024 * 1024);
                }
            }
            Console.WriteLine("totalSize: " + totalSize + " GB");
            Console.WriteLine("totalSize: " + totalFreeSpace + " GB");
            Console.WriteLine("totalSize: " + availableFreeSpace + " GB");

            Console.Read();
        }


        private static async Task SurveyPage(int idx, DateTime dt)
        {
            var client = new RestClient("http://localhost/CTA_test");

            var restRequest = new RestRequest("/pages/b.aspx?page=In_Cla_MeetingSurvey.html&meeting_id=BF055DBC49DD4B66801C5C376C8058B1&surveytype=5&muid=4927A0CC37584420854F9033D5AA50DB&method=In_Cla_Get_MeetingSurveys"
                , Method.GET
                , DataFormat.None);

            //RestRequest restRequest = new RestRequest("/pages/" 
            //    + "e.aspx"
            //    + "?page=Cla_MeetingUserLogin.html"
            //    + "&method=In_Cla_Get_MeetingUserLogin" 
            //    + "&meeting_id=BF055DBC49DD4B66801C5C376C8058B1"
            //    , Method.GET
            //    , DataFormat.None);

            Console.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + ": " + idx + ". " + "開始");

            var result = await client.ExecuteAsync<string>(restRequest);
            var content = result.Content;
            //Console.WriteLine(content);

            Console.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + ": " + idx + ". " + "結束: " + "(" + GetSeconds(dt) + ")");
        }

        private static string GetSeconds(DateTime dt)
        {
            var ts = DateTime.Now - dt;
            return ts.TotalMilliseconds.ToString("###,###");
        }


        //static void Main(string[] args)
        //{
        //    Console.Write("請輸入資料：");
        //    string data = Console.ReadLine();

        //    while (!data.Contains("q"))
        //    {
        //        var stopWatch = WorkHelp.Logging.TLog.GetStopwatch();

        //        int value = 0;
        //        int.TryParse(data, out value);

        //        //int[] nm = Draw.Cardinarity(value);
        //        //Console.WriteLine("　檢查結果：" + nm[0] + ", " + nm[1]);

        //        //int[] ns = Draw.TkdNumbers(value);
        //        //Console.WriteLine("　檢查結果：" + Draw.ArrToStr(ns));

        //        int[] result = Draw.DrawLots(value);
        //        WorkHelp.Logging.TLog.Watch(stopWatch, "演算結束");

        //        string message = Draw.ArrToStr(result);
        //        Console.WriteLine("　檢查結果：" + message);

        //        Console.Write("請輸入資料：");
        //        data = Console.ReadLine();
        //    }

        //    //Console.Write("請輸入資料：");
        //    //string data = Console.ReadLine();

        //    //while (!data.Contains("q"))
        //    //{
        //    //    if (MyQueueTest.IsRecStr(data))
        //    //    {
        //    //        Console.WriteLine("　檢查結果：是回文");
        //    //    }
        //    //    else
        //    //    {
        //    //        Console.WriteLine("　檢查結果：不是回文");
        //    //    }

        //    //    Console.Write("請輸入資料：");
        //    //    data = Console.ReadLine();
        //    //}

        //    //string data = Console.ReadLine();
        //    //while(!data.Contains("q"))
        //    //{
        //    //    int n = 0;
        //    //    int.TryParse(data, out n);
        //    //    Console.WriteLine(TConversion.Conversion16(n));
        //    //    Console.WriteLine("請輸入資料：");
        //    //    data = Console.ReadLine();
        //    //}

        //    //string sid = "~2002/01/01";
        //    //var values = sid.Split('~');
        //    //Console.WriteLine(values[0]);
        //    //Console.WriteLine(values[1]);
        //    //Console.ReadLine();
        //}

        private static string GetNextTempSid(string max_sno)
        {
            var now_no = max_sno.TrimStart('T').TrimStart('0');
            var next_no = Convert.ToInt32(now_no) + 1;
            return "T" + next_no.ToString().PadLeft(5, '0');
        }

        private static string GetCellProperty(string value)
        {
            int posS = value.IndexOf("[$");
            int posE = value.IndexOf("$]");

            if (posS == -1 || posE == -1)
            {
                return "";
            }
            else
            {
                return value.Substring(posS + 2, posE - posS - 2);
            }
        }
    }
}
