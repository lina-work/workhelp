﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkHelp.Playground.MyLinkTree;
using WorkHelp.Playground.MySeqStack;

namespace WorkHelp.Playground.MySearch
{
    public class MySearchUtility
    {
        /// <summary>
        /// 順序查找
        /// </summary>
        public int SeqSearch(SeqList<int> sqList, int data)
        {
            sqList[0] = data;
            int i = 0;

            for (i = sqList.GetLength(); sqList[i] > data; --i) ;
            return i;
        }

        /// <summary>
        /// 有序表的折半查找
        /// </summary>
        public int BinarySearch(SeqList<int> sqList, int key)
        {
            sqList[0] = key;
            int mid = 0;
            int flag = -1;
            int low = 1;
            int high = sqList.GetLength();

            while (low <= high)
            {
                mid = (low + high) / 2;

                if (sqList[0] == sqList[mid])
                {
                    flag = mid;
                    break;
                }
                else if (sqList[0] < sqList[mid])
                {
                    high = mid - 1;
                }
                else
                {
                    low = mid + 1;
                }
            }

            if (flag > 0)
            {
                return flag;
            }
            else
            {
                return -1;
            }
        }

        /// <summary>
        /// 二叉排序樹
        /// </summary>
        public int Search(BiTree<int> bt, int key)
        {
            Node<int> p;

            if (bt.IsEmpty() == true)
            {
                return 1;
            }

            p = bt.Head;
            while (p != null)
            {
                if (p.Data == key)
                {
                    return 0;
                }
                else if (p.Data < key)
                {
                    p = p.LChild;
                }
                else
                {
                    p = p.RChild;
                }
            }

            return 1;
        }

        /// <summary>
        /// 二叉排序樹的插入操作
        /// </summary>
        public int Insert(BiTree<int> bt, int key)
        {
            //這裡的查找過程同時記住當前結點的位置
            //，以便 當查找不成功時 把由要查找的記錄生成的結點的地址 賦給當前結點的左孩子引用域或右孩子引用域

            Node<int> p;
            Node<int> parent = new Node<int>();

            p = bt.Head;
            while (p != null)
            {
                if (p.Data == key)
                {
                    return 1;
                }

                parent = p;
                if (p.Data < key)
                {
                    p = p.RChild;
                }
                else
                {
                    p = p.LChild;
                }
            }

            p = new Node<int>(key);
            if (parent == null)
            {
                bt.Head = p;
            }
            else if (p.Data < parent.Data)
            {
                parent.LChild = p;
            }
            else
            {
                parent.RChild = p;
            }

            return 0;
        }


        /// <summary>
        /// 二叉排序樹的刪除操作
        /// </summary>
        public int Delete(BiTree<int> bt, int key)
        {
            Node<int> p;
            Node<int> parent = new Node<int>();
            Node<int> s = new Node<int>();
            Node<int> q = new Node<int>();

            if (bt.IsEmpty() == true)
            {
                return 1;
            }

            p = bt.Head;
            parent = p;

            while (p != null)
            {
                if (p.Data == key)
                {
                    if (bt.IsLeaf(p))
                    {
                        if (p == bt.Head)
                        {
                            bt.Head = null;
                        }
                        else if (p == parent.LChild)
                        {
                            parent.LChild = null;
                        }
                        else
                        {
                            parent.RChild = null;
                        }
                    }
                    else if (p.RChild == null && p.LChild != null)
                    {
                        if (p == parent.LChild)
                        {
                            parent.LChild = p.LChild;
                        }
                        else
                        {
                            parent.RChild = p.LChild;
                        }
                    }
                    else if (p.LChild == null && p.RChild != null)
                    {
                        if (p == parent.LChild)
                        {
                            parent.LChild = p.RChild;
                        }
                        else
                        {
                            parent.RChild = p.RChild;
                        }
                    }
                    else
                    {
                        q = p;
                        s = p.RChild;
                        while (s.LChild != null)
                        {
                            q = s;
                            s = s.LChild;
                        }
                        p.Data = s.Data;
                        if (q != p)
                        {
                            q.LChild = s.RChild;
                        }
                        else
                        {
                            q.RChild = s.RChild;
                        }
                    }
                    return 0;
                }
                else if (p.Data < key)
                {
                    parent = p;
                    p = p.RChild;
                }
                else
                {
                    parent = p;
                    p = p.LChild;
                }
            }
         
            return -1;
        }

    }
}
