﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkHelp.Playground.DrawLots
{
    public class DrawUtility
    {
        public static int RandomNo(int min, int max)
        {
            return TRNG.Next(min, max);
        }

        public static int[] RandomNoArr(int value)
        {
            List<int> ls = new List<int>();

            int n = 1;
            while (n <= value)
            {
                ls.Add(n++);
            }

            int[] result = new int[value];

            int pos = -1;
            int min = 1;
            int max = value;

            while (max > 1)
            {
                int num = TRNG.Next(min, max);
                int idx = num - 1;
                int val = ls[idx];
                ls.RemoveAt(idx);
                result[++pos] = val;
                max--;
            }

            result[++pos] = ls[0];
            
            return result;
        }

        //public static int[] DrawLots(int value)
        //{
        //    LinkQueue<int> q = new LinkQueue<int>();

        //    int n = 1;
        //    while(n <= value)
        //    {
        //        q.In(n++);
        //    }

        //    int[] result = new int[value];

        //    int pos = -1;
        //    int min = 1;
        //    int max = value + 1;

        //    Random rd = new System.Random();
        //    while(max > 1)
        //    {
        //        int num = rd.Next(min, max);
        //        int val = q.Jenga(num - 1);
        //        result[++pos] = val;
        //        max--;
        //    }

        //    return result;
        //}

        public static string ArrToStr(int[] value)
        {
            string result = string.Join(", ", value).TrimEnd(' ').TrimEnd(',');
            return result;
        }

        public static int[] TkdNumbers(int value)
        {
            LinkQueue<int> q = new LinkQueue<int>();

            int[] nm = Cardinarity(value);
            int m = nm[1];

            int n = 1;
            int c = 2;
            int x = c;
            int sm = x + 1;

            q.In(1);

            while (n < m)
            {
                int l = q.GetLenth();

                while(l > 0)
                { 
                    int a = q.Out();

                    if (a % 2 == 0)
                    {
                        q.In(sm - a);
                        q.In(a);
                    }
                    else
                    {
                        q.In(a);
                        q.In(sm - a);
                    }

                    l--;
                }

                n = n * c;
                x = x * c;
                sm = x + 1;
            }

            return LinkQueueToArr(q);
        }

        public static int[] LinkQueueToArr(LinkQueue<int> q)
        {
            int len = q.GetLenth();
            int[] result = new int[len];
            int idx = 0;
            while (!q.IsEmpty())
            {
                result[idx] = q.Out();
                idx++;
            }
            return result;
        }

        public static int[] Cardinarity(int value)
        {
            int c = 2;
            int n = 1;
            int m = c * n;

            while (m < value)
            {
                n++;
                m = m * c;
            }

            return new int[] { n, m };
        }

        #region Model

        public class Node<T>
        {
            private T data;
            private Node<T> next;

            public Node(T val, Node<T> p)
            {
                data = val;
                next = p;
            }

            public Node(Node<T> p)
            {
                next = p;
            }

            public Node(T val)
            {
                data = val;
                next = null;
            }

            public Node()
            {
                data = default(T);
                next = null;
            }

            public T Data
            {
                get { return data; }
                set { data = value; }
            }

            public Node<T> Next
            {
                get { return next; }
                set { next = value; }
            }
        }

        public class LinkQueue<T> : IMyQueue<T>
        {
            private Node<T> front;
            private Node<T> rear;
            private int num;

            public Node<T> Front
            {
                get { return front; }
                set { front = value; }
            }

            public Node<T> Rear
            {
                get { return rear; }
                set { rear = value; }
            }

            public int Num
            {
                get { return num; }
                set { num = value; }
            }

            public LinkQueue()
            {
                front = rear = null;
                num = 0;
            }

            public int GetLenth()
            {
                return num;
            }

            public bool IsEmpty()
            {
                if (front == rear && num == 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            public bool IsFull()
            {
                return false;
            }

            public void Clear()
            {
                front = rear = null;
                num = 0;
            }

            public void In(T item)
            {
                Node<T> q = new Node<T>(item);

                if (rear == null)
                {
                    front = rear = q;
                }
                else
                {
                    rear.Next = q;
                    rear = q;
                }
                ++num;
            }

            public T Out()
            {
                if (IsEmpty())
                {
                    return default(T);
                }

                Node<T> p = front;
                front = front.Next;

                if (front == null)
                {
                    rear = null;
                }
                --num;

                return p.Data;
            }

            public T Jenga(int index)
            {
                if (index == 0)
                {
                    return Out();
                }
                else
                {
                    if (IsEmpty())
                    {
                        return default(T);
                    }

                    Node<T> now = front;
                    Node<T> last = null;

                    int pos = 0;
                    while(pos < index)
                    {
                        last = now;
                        now = last.Next;
                        pos++;
                    }

                    last.Next = now.Next;
                    --num;

                    return now.Data;
                }
            }

            public T GetFront()
            {
                if (IsEmpty())
                {
                    return default(T);
                }

                return front.Data;
            }
        }

        public interface IMyQueue<T>
        {
            int GetLenth();
            bool IsEmpty();
            bool IsFull();
            void Clear();
            void In(T item);
            T Out();
            T GetFront();
        }
        #endregion
    }
}
