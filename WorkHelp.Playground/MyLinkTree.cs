﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkHelp.Playground.MyLinkQueue;

namespace WorkHelp.Playground.MyLinkTree
{
    public class BiTree<T>
    {
        private Node<T> head;

        public Node<T> Head
        {
            get { return head; }
            set { head = value; }
        }

        public BiTree()
        {
            head = null;
        }

        public BiTree(T val)
        {
            Node<T> p = new Node<T>(val);
            head = p;
        }

        public BiTree(T val, Node<T> lp, Node<T> rp)
        {
            Node<T> p = new Node<T>(val, lp, rp);
            head = p;
        }

        public bool IsEmpty()
        {
            if (head == null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public Node<T> Root()
        {
            return head;
        }

        public Node<T> GetLChild(Node<T> p)
        {
            return p.LChild;
        }

        public Node<T> GetRChild(Node<T> p)
        {
            return p.RChild;
        }

        public void InsertL(T val, Node<T> p)
        {
            Node<T> tmp = new Node<T>(val);
            tmp.LChild = p.LChild;
            p.LChild = tmp;
        }

        public void InsertR(T val, Node<T> p)
        {
            Node<T> tmp = new Node<T>(val);
            tmp.RChild = p.RChild;
            p.RChild = tmp;
        }

        public Node<T> DeleteL(Node<T> p)
        {
            if (p == null || p.LChild == null)
            {
                return null;
            }

            Node<T> tmp = p.LChild;
            p.LChild = null;

            return tmp;
        }

        public Node<T> DeleteR(Node<T> p)
        {
            if (p == null || p.RChild == null)
            {
                return null;
            }

            Node<T> tmp = p.RChild;
            p.RChild = null;

            return tmp;
        }

        public bool IsLeaf(Node<T> p)
        {
            if (p != null && p.LChild == null && p.RChild == null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void PreOrder(Node<T> root)
        {
            if (root == null)
            {
                return;
            }

            Console.WriteLine("{0}", root.Data);

            PreOrder(root.LChild);

            PreOrder(root.RChild);
        }

        public void InOrder(Node<T> root)
        {
            if (root == null)
            {
                return;
            }

            PreOrder(root.LChild);

            Console.WriteLine("{0}", root.Data);

            PreOrder(root.RChild);
        }

        public void PostOrder(Node<T> root)
        {
            if (root == null)
            {
                return;
            }

            PreOrder(root.LChild);

            PreOrder(root.RChild);

            Console.WriteLine("{0}", root.Data);
        }

        public void LevelOrder(Node<T> root)
        {
            if (root == null)
            {
                return;
            }

            //設置一個隊列保存層序遍歷的結點
            CSeqQueue<Node<T>> sq = new CSeqQueue<Node<T>>(50);

            //根結點入隊
            sq.In(root);

            while(!sq.IsEmpty())
            {
                Node<T> tmp = sq.Out();
                
                Console.WriteLine("{0}", tmp);

                if (tmp.LChild != null)
                {
                    sq.In(tmp.LChild);
                }
                
                if (tmp.RChild != null)
                {
                    sq.In(tmp.RChild);
                }
            }
        }

        public Node<T> Search(Node<T> root, T value)
        {
            Node<T> p = root;
            
            if (p == null)
            {
                return null;
            }

            if (p.Data.Equals(value))
            {
                return p;
            }

            if (p.LChild != null)
            {
                return Search(p.LChild, value);
            }
            else if (p.RChild != null)
            {
                return Search(p.RChild, value);
            }
            else
            {
                return null;
            }
        }

        //統計出二叉樹中葉子結點的數目
        public int CountLeafNode(Node<T> root)
        {
            if (root == null)
            {
                return 0;
            }
            else if (root.LChild == null && root.RChild == null)
            {
                return 1;
            }
            else
            {
                return CountLeafNode(root.LChild) + CountLeafNode(root.RChild);
            }
        }

        //求二叉樹深度
        public int GetHeight(Node<T> root)
        {
            int lh;
            int rh;

            if (root == null)
            {
                return 0;
            }
            else if (root.LChild == null && root.RChild == null)
            {
                return 1;
            }
            else
            {
                lh = GetHeight(root.LChild);
                rh = GetHeight(root.RChild);
                return (lh > rh ? lh : rh) + 1;
            }
        }
    }

    public class Node<T>
    {
        private T data;
        private Node<T> lChild;
        private Node<T> rChild;

        public Node(T val,Node<T> lp, Node<T> rp)
        {
            data = val;
            lChild = lp;
            rChild = rp;
        }

        public Node(Node<T> lp, Node<T> rp)
        {
            data = default(T);
            lChild = lp;
            rChild = rp;
        }

        public Node(T val)
        {
            data = val;
            lChild = null;
            rChild = null;
        }

        public Node()
        {
            data = default(T);
            lChild = null;
            rChild = null;
        }

        public T Data
        {
            get { return data; }
            set { data = value; }
        }

        public Node<T> LChild
        {
            get { return lChild; }
            set { lChild = value; }
        }

        public Node<T> RChild
        {
            get { return rChild; }
            set { rChild = value; }
        }

    }
}
