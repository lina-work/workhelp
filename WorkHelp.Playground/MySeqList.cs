﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkHelp.Playground.MySeqStack
{
    public interface IListDS<T>
    {

    }

    public class SeqList<T> : IListDS<T>
    {
        private int maxsize;
        private T[] data;
        private int last;

        public T this[int index]
        {
            get { return data[index]; }
            set { data[index] = value; }
        }

        public int Last
        {
            get { return last; }
            set { last = value; }
        }

        public int MaxSize
        {
            get { return maxsize; }
            set { maxsize = value; }
        }

        public SeqList(int size)
        {
            data = new T[size];
            maxsize = size;
            last = -1;
        }

        public int GetLength()
        {
            return last + 1;
        }

        public void Clear()
        {
            last = -1;
        }

        public bool IsEmpty()
        {
            if (last == -1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool IsFull()
        {
            if (last == maxsize - 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void Append(T item)
        {
            if (IsFull())
            {
                return;
            }

            data[++last] = item;
        }

        public void Insert(T item, int i)
        {
            if (IsFull())
            {
                return;
            }

            if (i < 1 || i > last + 2)
            {
                return;
            }

            if (i == last + 2 )
            {
                data[last + 1] = item;
            }
            else
            {
                for (int j = last; j >= i - 1; --j)
                {
                    data[j + 1] = data[j];
                }
                data[i - 1] = item;
            }
            ++last;
        }

        public T Delete(int i)
        {
            T tmp = default(T);

            if (IsEmpty())
            {
                return default(T);
            }

            if (i < 1 || i < last + 1)
            {
                return tmp;
            }

            if (i == last + 1)
            {
                tmp = data[last--];
            }
            else
            {
                tmp = data[i - 1];
                for (int j = i; j <= last; ++j)
                {
                    data[j] = data[j + 1];
                }
            }

            --last;
            return tmp;
        }

        public T GetElem(int i)
        {
            if (IsEmpty() || (i < 1) || (i > last + 1 ))
            {
                return default(T);
            }

            return data[i - 1];
        }

        public int Locate(T value)
        {
            if (IsEmpty())
            {
                return -1;
            }

            int i = 0;
            for (i = 0; i <= last; ++i)
            {
                if (value.Equals(data[i]))
                {
                    break;
                }
            }

            if (i > last)
            {
                return -1;
            }

            return i;
        }
    }
}
