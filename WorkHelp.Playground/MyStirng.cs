﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkHelp.Playground
{
    public class MyString
    {
        private char[] data;

        public char this[int index]
        {
            get { return data[index]; }
            set { data[index] = value; }
        }

        public MyString(char[] arr)
        {
            data = new char[arr.Length];
            for (int i = 0; i < arr.Length; ++i)
            {
                data[i] = arr[i];
            }
        }

        public MyString(int len)
        {
            data = new char[len];
        }

        public int GetLenth()
        {
            return data.Length;
        }

        public int Compare(MyString str)
        {
            int len = this.GetLenth() <= str.GetLenth()
                ? this.GetLenth()
                : str.GetLenth();

            int i = 0;

            for (i = 0; i < len; ++i)
            {
                if (this[i] != str[i])
                {
                    break;
                }
            }

            if (i <= len)
            {
                if (this[i] < str[i])
                {
                    return -1;
                }
                else if (this[i] > str[i])
                {
                    return 1;
                }
            }
            else if (this.GetLenth() == str.GetLenth())
            {
                return 0;
            }
            else if (this.GetLenth() < str.GetLenth())
            {
                return -1;
            }

            return 1;
        }

        public MyString SubString(int index, int len)
        {
            if (index < 0
                || index > this.GetLenth() - 1
                || len < 0
                || len > this.GetLenth() - index
                )
            {
                return null;
            }

            MyString str = new MyString(len);
            for (int i = 0; i < len; ++i)
            {
                str[i] = this[i + index - 1];
            }

            return str;
        }

        public MyString Concat(MyString val)
        {
            MyString str = new MyString(this.GetLenth() + val.GetLenth());

            for (int i = 0; i < this.GetLenth(); ++i)
            {
                str.data[i] = this[i];
            }
            for (int j = 0; j < val.GetLenth(); ++j)
            {
                str.data[this.GetLenth() + j] = val[j];
            }

            return str;
        }

        public MyString Insert(int index, MyString val)
        {
            int len = val.GetLenth();
            int len2 = this.GetLenth();
            MyString str = new MyString( len2);

            if (index < 0 || index > len - 1)
            {
                return null;
            }

            for (int i = 0; i < index; ++i)
            {
                str[i] = this[i];
            }

            for (int i = index; i < index + len; ++i)
            {
                str[i] = val[i - index];
            }

            for (int i = index + len; i < len2; ++i)
            {
                str[i] = this[i - len];
            }

            return str;
        }

        public MyString Delete(int index, int len)
        {
            if (index < 0
                || index > this.GetLenth() - 1
                || len < 0
                || len > this.GetLenth() - index
                )
            {
                return null;
            }

            MyString str = new MyString(this.GetLenth() - len);

            for (int i = 0; i < index; ++i)
            {
                str[i] = this[i];
            }

            for (int i = index + len; i < this.GetLenth(); ++i)
            {
                str[i] = this[i];
            }

            return str;
        }

        public int Index(MyString val)
        {
            if (this.GetLenth() < val.GetLenth())
            {
                return -1;
            }

            int i = 0;
            int len = this.GetLenth() - val.GetLenth();
            while (i < len)
            {
                if (this.Compare(val) == 0)
                {
                    break;
                }

            }

            if (i <= len)
            {
                return i;
            }
            return -1;
        }
    }
}
