﻿using RestSharp;
using Spire.Pdf.Exporting.XPS.Schema;
using Spire.Xls;
using Spire.Xls.Core.Spreadsheet;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkHelp.Playground2
{
    public class TUser
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
    }
    class Program
    {
        static void Main(string[] args)
        {
            FRWFPWEMFWMOWEPFEOFPMPQD1();
        }

        static void FRWFPWEMFWMOWEPFEOFPMPQD1()
        {
            var path = @"C:\site\wrestling_new\pages\ExcelIO\wrestling_draw_sheets.xlsx";
            var xls_file = @"C:\temp\對戰表_" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".pdf";

            //試算表
            var book = new Spire.Xls.Workbook();
            book.LoadFromFile(path);

            ////複製
            //Spire.Xls.Worksheet sheet = book.CreateEmptySheet();
            //sheet.CopyFrom(sheetTemplate);
            //sheet.Name = entity.key;


            book.SaveToFile(xls_file, Spire.Xls.FileFormat.PDF);
            //book.SaveToFile(xls_file, Spire.Xls.ExcelVersion.Version2013);
        }

        static void NQWDNOQIWPDQPQO()
        {
            var total = 29;
            var lanes = 8;

            var quotient = total / lanes;//商數
            var remainder = total % lanes;//餘數
            var divisible = remainder == 0;//整除
            var groupCount = divisible ? quotient : quotient + 1;

            for (var i = 0; i < groupCount; i++)
            {
                var groupNo = groupCount - i;
                Console.WriteLine($"第 {groupNo} 組: ");

                for (var j = 0; j < lanes; j++)
                {
                    var no = i * lanes + j + 1;
                    var serial = j + 1;
                    var lane = GetLaneNumber(serial);
                    if (no <= total)
                    {
                        Console.WriteLine($"\t {no}. existed #{serial}: 水道 = {lane}");
                    }
                    else
                    {
                        Console.WriteLine($"\t {no}. bypass #{serial}: 水道 = {lane}");
                    }

                }
            }

            Console.ReadLine();
        }

        static void DEWJIOFHOIWEHOWEOIW()
        {
            var mn = 33;
            var mx = 33;
            var lanes = 8;
            var topGroups = 3;
            var minLimit = 3;
            for (var i = mn; i <= mx; i++)
            {
                var nsList = MapSwimmingGroups(i, lanes, topGroups, minLimit);

                var no = 1;

                Console.WriteLine($"總人數: {i}，分成: {nsList.Count} 組");
                for (int j = nsList.Count - 1; j >= 0; j--)
                {
                    var rows = nsList[j];
                    var text = "";
                    foreach (var x in rows)
                    {
                        text += x + " \t ";
                    }

                    Console.WriteLine($" - 第 {no} 組: {rows.Count} 人");
                    Console.WriteLine($"     - {text}");
                    Console.WriteLine("");

                    no++;
                }
                Console.WriteLine("");
            }
            Console.ReadLine();
        }

        private static List<List<int>> MapSwimmingGroups(int total, int lanes, int topGroups, int minLimit)
        {
            var rows = new List<int>();
            for (var i = 1; i <= total; i++) rows.Add(i);

            var result = new List<List<int>>();
            if (total <= lanes)
            {
                result.Add(rows);
                return result;
            }

            var groupA = new List<List<int>>();
            var groupB = new List<List<int>>();
            var groupE = new List<int>();

            //已排序: 快到慢
            var quotient = total / lanes;//商數
            var remainder = total % lanes;//餘數
            var divisible = remainder == 0;//整除
            var groupCount = divisible ? quotient : quotient + 1;

            var a1 = 0;
            var a2 = topGroups;
            if (a2 > groupCount) a2 = groupCount;

            for (var i = a1; i < a2; i++)
            {
                groupA.Add(new List<int>());
            }

            //1. 處理最後一組
            if (groupCount > topGroups)
            {
                var seek = remainder;
                if (divisible) seek = lanes;
                else if (seek < minLimit) seek = minLimit;
                for (var i = 0; i < seek; i++)
                {
                    //從最後抓
                    var idx = rows.Count - 1;
                    var item = rows[idx];
                    groupE.Insert(0, item);
                    rows.RemoveAt(idx);
                }
            }

            var topRows = lanes * topGroups;// 8 個水道 * 3 組

            if (rows.Count > topRows)
            {
                var rowsA = new List<int>();
                var rowsB = new List<int>();

                rowsA.AddRange(rows.Take(24));
                rowsB.AddRange(rows.Skip(24));

                var code = groupA.Count;
                for (var i = 0; i < rowsA.Count; i++)
                {
                    var row = rowsA[i];
                    var idx = i % code;
                    groupA[idx].Add(row);
                }

                for (var i = 0; i < rowsB.Count; i++)
                {
                    var row = rowsB[i];
                    var idx = i / lanes;
                    var cnt = idx + 1;
                    if (groupB.Count < cnt)
                    {
                        groupB.Add(new List<int>());
                    }
                    groupB[idx].Add(row);
                }
            }
            else
            {
                var code = groupA.Count;
                for (var i = 0; i < rows.Count; i++)
                {
                    var row = rows[i];
                    var idx = i % code;
                    groupA[idx].Add(row);
                }
            }

            foreach (var ns in groupA)
            {
                if (ns.Count > 0) result.Add(ns);
            }

            foreach (var ns in groupB)
            {
                if (ns.Count > 0) result.Add(ns);
            }

            if (groupE.Count > 0) result.Add(groupE);

            return result;
        }

        static void JWDIOWJIEOJWODEFJWOEIJ()
        {
            var players = GetSwimmingRows();
            var rows = players.OrderBy(x => x.score).ToList();
            var pkg = MapSwimmingGroups(rows.Count, 8, rows);
            var groups = new List<TSwimmingGroup>();

            if (pkg.Top3Groups != null && pkg.Top3Groups.Count > 0)
            {
                groups.AddRange(pkg.Top3Groups);
            }
            if (pkg.OtherGroups != null && pkg.OtherGroups.Count > 0)
            {
                groups.AddRange(pkg.OtherGroups);
            }
            if (pkg.EndGroup != null)
            {
                groups.Add(pkg.EndGroup);
            }
            var mx = groups.Count;
            for (var i = 0; i < mx; i++)
            {
                groups[i].no = mx - i;
            }

            Console.WriteLine($"總人數：{rows.Count}");

            var sorted = groups.OrderBy(x => x.no);
            foreach (var group in sorted)
            {
                if (group.rows == null || group.rows.Count == 0)
                {
                    Console.WriteLine($" - 第 {group.no} 組: 異常");
                }
                else
                {
                    Console.WriteLine($" - 第 {group.no} 組: {group.rows.Count}");
                    var text = "";
                    foreach (var x in group.rows)
                    {
                        text += x.score + " | ";
                    }
                    Console.WriteLine($"     - {text}");
                }
                Console.WriteLine("");
            }
            Console.ReadLine();
        }

        private static TSwimmingPackage MapSwimmingGroups(int total, int lanes, List<TSwimmingRow> resource)
        {
            var rows = new List<TSwimmingRow>();
            foreach (var x in resource) rows.Add(x);

            var result = new TSwimmingPackage
            { 
                Top3Groups = new List<TSwimmingGroup>(),
                OtherGroups = new List<TSwimmingGroup>(),
                EndGroup = null,
            };

            //已排序: 快到慢
            var quotient = total / lanes;//商數
            var remainder = total % lanes;//餘數
            var groupCount = remainder > 0 ? quotient + 1 : quotient;
            var minLimit = 3;

            if (total <= 8)
            {
                result.EndGroup = new TSwimmingGroup { rows = rows };
                return result;
            }

            var groupA = result.Top3Groups;
            var groupB = result.OtherGroups;
         
            int a1 = 0;
            int a2 = 3;
            if (a2 > groupCount) a2 = groupCount;

            int b1 = a2 + 1;
            int b2 = groupCount - 4;

            for (int i = a1; i < a2; i++)
            {
                groupA.Add(new TSwimmingGroup { rows = new List<TSwimmingRow>() });
            }

            if (groupCount > 3)
            {
                result.EndGroup = new TSwimmingGroup { rows = new List<TSwimmingRow>() };
            }

            if (result.EndGroup != null)
            {
                var end = result.EndGroup;
                int seek = remainder;
                if (seek < minLimit) seek = minLimit;
                for (int i = 0; i < seek; i++)
                {
                    //從最後抓
                    var idx = rows.Count - 1;
                    var item = rows[idx];
                    end.rows.Add(item);
                    rows.RemoveAt(idx);
                }
                end.rows = end.rows.OrderBy(x => x.score).ToList();
            }

            if (rows.Count > 24)
            {
                var rowsA = new List<TSwimmingRow>();
                var rowsB = new List<TSwimmingRow>();

                rowsA.AddRange(rows.Take(24));
                rowsB.AddRange(rows.Skip(24));

                var code = groupA.Count;
                for (int i = 0; i < rowsA.Count; i++)
                {
                    var row = rowsA[i];
                    var idx = i % code;
                    groupA[idx].rows.Add(row);
                }

                for (int i = b1; i < b2; i++)
                {
                    groupB.Add(new TSwimmingGroup { rows = new List<TSwimmingRow>() });
                }

                for (int i = 0; i < rowsB.Count; i++)
                {
                    var row = rowsB[i];
                    var idx = i / lanes;
                    groupB[idx].rows.Add(row);
                }
            }
            else
            {
                var code = groupA.Count;
                for (int i = 0; i < rows.Count; i++)
                {
                    var row = rows[i];
                    var idx = i % code;
                    groupA[idx].rows.Add(row);
                }
            }
            return result;
        }

        private class TSwimmingPackage
        {
            public List<TSwimmingGroup> Top3Groups { get; set; }
            public List<TSwimmingGroup> OtherGroups { get; set; }
            public TSwimmingGroup EndGroup { get; set; }
        }

        private class TSwimmingGroup
        {
            public int no { get; set; }
            public List<TSwimmingRow> rows { get; set; }
        }

        private static List<TSwimmingRow> GetSwimmingRows()
        {
            var rows = new List<TSwimmingRow>();
            AddSwimmingRows(rows, "張愷岳", "00:59.23");
            AddSwimmingRows(rows, "陳彥甯", "00:59.59");
            AddSwimmingRows(rows, "陳宗言", "01:01.21");
            AddSwimmingRows(rows, "黃育騰", "01:03.11");
            AddSwimmingRows(rows, "李睿濬", "01:03.48");
            AddSwimmingRows(rows, "陳永承", "01:03.50");
            AddSwimmingRows(rows, "田樂翔", "01:03.57");
            AddSwimmingRows(rows, "凃博仁", "01:03.86");
            AddSwimmingRows(rows, "洪啟文", "01:04.21");
            AddSwimmingRows(rows, "呂是雋", "01:04.35");
            AddSwimmingRows(rows, "莊侑晟", "01:04.49");
            AddSwimmingRows(rows, "黃暘恩", "01:04.67");
            AddSwimmingRows(rows, "吳兆騏", "01:04.95");
            AddSwimmingRows(rows, "曾安睿", "01:05.03");
            AddSwimmingRows(rows, "吳定謙", "01:05.43");
            AddSwimmingRows(rows, "letessier leon", "01:05.65");
            AddSwimmingRows(rows, "侯詠森", "01:06.29");
            AddSwimmingRows(rows, "黃宥鈞", "01:06.59");
            AddSwimmingRows(rows, "游凱宇", "01:07.28");
            AddSwimmingRows(rows, "張祐瑋", "01:07.62");
            AddSwimmingRows(rows, "許峻維", "01:07.66");
            AddSwimmingRows(rows, "林祐新", "01:08.05");
            AddSwimmingRows(rows, "黃適然", "01:08.10");
            AddSwimmingRows(rows, "陳威仁", "01:08.12");
            AddSwimmingRows(rows, "馮冠睿", "01:08.35");
            return rows;
        }

        private static void AddSwimmingRows(List<TSwimmingRow> rows, string name, string score)
        {
            rows.Add(new TSwimmingRow { name = name, score = score });
        }

        private class TSwimmingRow
        {
            public string name { get; set; }
            public string score { get; set; }
        }

        static void WROINFOWJEJWO()
        {
            var mn = 0;
            var mx = 128;
            var lanes = 8;
            for (var i = mn; i <= mx; i++)
            {
                var list = SplitNumArray(i, lanes);
                if (list.Count > 0)
                {
                    Console.WriteLine($"SUM = {i}: {string.Join(", ", list)}");
                }
            }
            Console.ReadLine();
        }

        private static int GetLaneNumber(int serial)
        {
            switch(serial)
            {
                case 1: return 4;
                case 2: return 5;
                case 3: return 3;
                case 4: return 6;
                case 5: return 2;
                case 6: return 7;
                case 7: return 1;
                case 8: return 8;
                default: return 0;
            }
        }

        private static List<int> SplitNumArray(int total, int lanes)
        {
            var list = new List<int>();
            if (total <= 0) return list;

            var quotient = total / lanes;//商數
            var remainder = total % lanes;//餘數

            if (quotient == 0)
            {
                list.Add(total);
                return list;
            }

            for (int i = 0; i < quotient; i++)
            {
                list.Add(lanes);
            }

            if (remainder == 0) return list;

            list.Add(lanes);

            //EX: 33/8 = 4 + 1 = 5

            var idxP = list.Count - 2;
            var idxE = list.Count - 1;//尾數

            var half = lanes / 2;
            if (remainder < half)
            {
                var sub = remainder + lanes;
                var q2 = sub / 2;
                var r2 = sub % 2;
                if (r2 == 0)
                {
                    list[idxP] = q2;
                    list[idxE] = q2;
                }
                else
                {
                    list[idxP] = q2 + 1;
                    list[idxE] = q2;
                }
            }
            else
            {
                list[idxE] = remainder;
            }
            return list;
        }

        static void QWMDOQKMLQK()
        {
            var value = "63720";
            var len = 6;
            var pos = value.Length - len;
            var data = value.Length <= 6 ? value : value.Substring(pos, len);

            Console.WriteLine(data);
            Console.ReadLine();

            //ReadOnlySpan<char> readOnlySpan = "This is a sample data for testing purposes.".AsSpan();

            //int index = readOnlySpan.IndexOf(' ');

            //var data = ((index < 0)
            //    ? readOnlySpan 
            //    : readOnlySpan.Slice(0, index)).ToArray();

            //Console.WriteLine("GET");

            //Console.WriteLine(data);

            //Console.ReadLine();
        }
        //public static IEnumerable<ReadOnlyMemory<char>> ExtractStrings(ReadOnlyMemory<char> c)
        //{
        //    int index = 0, length = c.Length;
        //    for (int i = 0; i < length; i++)
        //    {
        //        if (char.IsWhiteSpace(c.Span[i]) || i == length)
        //        {
        //            yield return c[index..i];
        //            index = i + 1;
        //        }
        //    }
        //}

        static void nIEUOH()
        {
            var list = new List<TUser>();
            list.Add(new TUser { UserName = "in_birth" });
            list.Add(new TUser { UserName = "in_name" });
            list.Add(new TUser { UserName = "in_sno" });

            var cols = list.Select(x => "[" + x.UserName + "]").ToList();
            Console.WriteLine(string.Join(", ", cols));
            Console.ReadLine();
        }

        static void XEDDEDOW()
        {
            var r1 = new TUser
            {
                UserName = "A123456789",
                Password = "1234",
                ConfirmPassword = "1234",
            };

            var r3 = new TUser
            {
                UserName = "N224669809",
                Password = "1234",
                ConfirmPassword = "1234",
            };

            var r2 = new TUser
            {
                UserName = "A164083004",
                Password = "123456",
                ConfirmPassword = "123456",
            };

            NewUser(r1);
            NewUser(r2);
            NewUser(r3);
        }

        static  void NewUser(TUser model)
        {
            try
            {
                var options = new RestClientOptions("https://localhost:44367")
                {
                   
                };

                var client = new RestClient(options);
                var request = new RestSharp.RestRequest("Account/RegisterImport").AddJsonBody(model);
                var response = client.PostAsync(request);
                var rsp_data = response.Result.Content;

                //var client = new RestSharp.RestClient("https://localhost:44399/");
                ////client.Timeout = 30 * 1000; // 30秒為逾期

                //var request = new RestSharp.RestRequest("/Account/RegisterImport"
                //    , RestSharp.Method.Post);

                ////string json = Newtonsoft.Json.JsonConvert.SerializeObject(new TUser
                ////{
                ////    UserName = "A168934468",
                ////    Password = "1234",
                ////    ConfirmPassword = "1234",
                ////});

                ////request.add

                //var response = client.ExecuteAsPost(request, "POST");
                //var rsp_data = response.Content;
                //Console.WriteLine(rsp_data);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        static void ASADJIOE()
        {
            try
            {
                var path = @"C:\temp";
                var dir = new System.IO.DirectoryInfo(path);
                var files = dir.GetFiles();
                foreach (var file in files)
                {
                    var ext = file.Extension.ToLower();
                    if (!ext.Contains("png")) continue;

                    var oldImagePath = file.FullName;
                    var webpFilePath = file.FullName.Replace(".png", ".webp");

                    using (var webPFileStream = new FileStream(webpFilePath, FileMode.Create))
                    {
                        using (var imageFactory = new ImageProcessor.ImageFactory(preserveExifData: false))
                        {
                            imageFactory.Load(oldImagePath)
                                        .Format(new ImageProcessor.Plugins.WebP.Imaging.Formats.WebPFormat())
                                        .Quality(50)
                                        .Save(webPFileStream);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        static void ADEWIOJF()
        {
            var now = DateTime.Now.ToString("yyyyMMdd_HHmmss");
            var filePath = @"C:\Users\Administrator\source\lina repos\workhelp\WorkHelp.Playground2\Temp\Judo_English_Match_List.xlsx";
            var newPath = $@"C:\Users\Administrator\source\lina repos\workhelp\WorkHelp.Playground2\Output\{now}.xlsx";

            var book = new Workbook();
            book.LoadFromFile(filePath);

            var sheetTemplate = book.Worksheets[0];
            var shaps = sheetTemplate.OvalShapes;

            var sheet = book.CreateEmptySheet();
            sheet.CopyFrom(sheetTemplate);
            sheet.Name = "Sheet1";

            var os = sheet.RectangleShapes[0];
            
            var o1 = sheet.OvalShapes[0];
            o1.Text = "A";


            //shape.HAlignment = Spire.Xls.CommentHAlignType.Center;
            //shape.VAlignment = Spire.Xls.CommentVAlignType.Center;
            //((Spire.Xls.Core.Spreadsheet.Shapes.XlsRectangleShape)shape).fn
            //((XlsOvalShape)shape).Fill.ForeColor = Color.Green;

            book.SaveToFile(newPath, Spire.Xls.ExcelVersion.Version2013);

        }

        static void SDJIEO()
        {
            var oldFolder = @"C:\site\judo\Images\flagsXlsx\old";
            var newFolder = @"C:\site\judo\Images\flagsXlsx\new";
            
            var dir = new System.IO.DirectoryInfo(oldFolder);
            var files = dir.GetFiles();
            foreach(var file in files)
            {
                //var img = resizeImage(36, 20, file.FullName);
                //var img = CreateThumbnail(36, 20, file.FullName);
                var img = resizeImageFromFile(file.FullName, 20, 36, true, true);
                var newFile = newFolder + @"\" + file.Name;
                img.Save(newFile);
            }

            Console.ReadLine();
        }

        public static Bitmap CreateThumbnail(int maxWidth, int maxHeight, string path)
        {

            var image = System.Drawing.Image.FromFile(path);
            var ratioX = (double)maxWidth / image.Width;
            var ratioY = (double)maxHeight / image.Height;
            var ratio = Math.Min(ratioX, ratioY);
            var newWidth = (int)(image.Width * ratio);
            var newHeight = (int)(image.Height * ratio);
            var newImage = new Bitmap(newWidth, newHeight);
            Graphics thumbGraph = Graphics.FromImage(newImage);

            thumbGraph.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
            thumbGraph.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            //thumbGraph.InterpolationMode = InterpolationMode.HighQualityBicubic;

            thumbGraph.DrawImage(image, 0, 0, newWidth, newHeight);
            image.Dispose();

            //string fileRelativePath = "newsizeimages/" + maxWidth + Path.GetFileName(path);
            //newImage.Save(Server.MapPath(fileRelativePath), newImage.RawFormat);
            return newImage;
        }

        public static Image resizeImageFromFile(String OriginalFileLocation, int heigth, int width, Boolean keepAspectRatio, Boolean getCenter)
        {
            int newheigth = heigth;
            System.Drawing.Image FullsizeImage = System.Drawing.Image.FromFile(OriginalFileLocation);

            // Prevent using images internal thumbnail
            FullsizeImage.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone);
            FullsizeImage.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone);

            if (keepAspectRatio || getCenter)
            {
                int bmpY = 0;
                double resize = (double)FullsizeImage.Width / (double)width;//get the resize vector
                if (getCenter)
                {
                    bmpY = (int)((FullsizeImage.Height - (heigth * resize)) / 2);// gives the Y value of the part that will be cut off, to show only the part in the center
                    Rectangle section = new Rectangle(new Point(0, bmpY), new Size(FullsizeImage.Width, (int)(heigth * resize)));// create the section to cut of the original image
                                                                                                                                 //System.Console.WriteLine("the section that will be cut off: " + section.Size.ToString() + " the Y value is minimized by: " + bmpY);
                    Bitmap orImg = new Bitmap((Bitmap)FullsizeImage);//for the correct effect convert image to bitmap.
                    FullsizeImage.Dispose();//clear the original image
                    using (Bitmap tempImg = new Bitmap(section.Width, section.Height))
                    {
                        Graphics cutImg = Graphics.FromImage(tempImg);//              set the file to save the new image to.
                        cutImg.DrawImage(orImg, 0, 0, section, GraphicsUnit.Pixel);// cut the image and save it to tempImg
                        FullsizeImage = tempImg;//save the tempImg as FullsizeImage for resizing later
                        orImg.Dispose();
                        cutImg.Dispose();
                        return FullsizeImage.GetThumbnailImage(width, heigth, null, IntPtr.Zero);
                    }
                }
                else newheigth = (int)(FullsizeImage.Height / resize);//  set the new heigth of the current image
            }//return the image resized to the given heigth and width
            return FullsizeImage.GetThumbnailImage(width, newheigth, null, IntPtr.Zero);
        }

        static Image resizeImage1(int newWidth, int newHeight, string stPhotoPath)
        {
            Image imgPhoto = Image.FromFile(stPhotoPath);

            int sourceWidth = imgPhoto.Width;
            int sourceHeight = imgPhoto.Height;

            //Consider vertical pics
            if (sourceWidth < sourceHeight)
            {
                int buff = newWidth;

                newWidth = newHeight;
                newHeight = buff;
            }

            int sourceX = 0, sourceY = 0, destX = 0, destY = 0;
            float nPercent = 0, nPercentW = 0, nPercentH = 0;

            nPercentW = ((float)newWidth / (float)sourceWidth);
            nPercentH = ((float)newHeight / (float)sourceHeight);
            if (nPercentH < nPercentW)
            {
                nPercent = nPercentH;
                destX = System.Convert.ToInt32((newWidth -
                          (sourceWidth * nPercent)) / 2);
            }
            else
            {
                nPercent = nPercentW;
                destY = System.Convert.ToInt16((newHeight -
                          (sourceHeight * nPercent)) / 2);
            }

            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);


            Bitmap bmPhoto = new Bitmap(newWidth, newHeight,
                          PixelFormat.Format24bppRgb);

            bmPhoto.SetResolution(imgPhoto.HorizontalResolution,
                         imgPhoto.VerticalResolution);

            Graphics grPhoto = Graphics.FromImage(bmPhoto);
            grPhoto.Clear(Color.White);
            grPhoto.InterpolationMode =
                System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;

            grPhoto.DrawImage(imgPhoto,
                new Rectangle(destX, destY, destWidth, destHeight),
                new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight),
                GraphicsUnit.Pixel);

            grPhoto.Dispose();
            imgPhoto.Dispose();

            return bmPhoto;
        }

    }
}
