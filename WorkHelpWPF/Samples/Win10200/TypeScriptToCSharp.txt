﻿/** API 結果 */
export interface APIResult<T> {
  /** 結果代碼 */
  resultCode?: number;
  /** 訊息 */
  message: string;
  /** 是否成功 */
  success?: boolean;
  /** 資料 */
  value?: T;
}
