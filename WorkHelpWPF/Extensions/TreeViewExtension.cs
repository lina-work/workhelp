﻿using System.Windows.Controls;

namespace WorkHelp.Wpf.Extensions
{
    /// <summary>
    /// TreeView 擴充
    /// </summary>
    public static class TreeViewExtension
    {
        /// <summary>
        /// 清除選取
        /// </summary>
        /// <param name="input"></param>
        public static void ClearSelection(this TreeView input)
        {
            // this should be some container that you put in
            // possibly the actual treeviewitem, not sure on that though
            object selected = input.SelectedItem;

            if (selected == null)
            {
                return;
            }

            // in my case this works perfectly
            TreeViewItem tvi = input.ItemContainerGenerator.ContainerFromItem(selected) as TreeViewItem;
            if (tvi != null)
            {
                tvi.IsSelected = false;
            }
        }
    }
}