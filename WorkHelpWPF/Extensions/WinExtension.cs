﻿using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace WorkHelp.Wpf.Extensions
{
    /// <summary>
    /// 專案擴充函式
    /// </summary>
    public static class WinExtension
    {
        /// <summary>
        /// 日誌
        /// </summary>
        public static Logger Logger => LogManager.GetCurrentClassLogger();

        #region UserControl

        /// <summary>取得親系[UserControl]</summary>
        public static T BtnUsrCtrl<T>(object sender) where T : System.Windows.Controls.UserControl
        {
            return GetUsrCtrl<T>((System.Windows.Controls.Button)sender);
        }

        /// <summary>取得親系[UserControl]</summary>
        public static T CheckBoxUsrCtrl<T>(object sender) where T : System.Windows.Controls.UserControl
        {
            return GetUsrCtrl<T>((System.Windows.Controls.CheckBox)sender);
        }

        /// <summary>取得親系[UserControl]</summary>
        public static T GetUsrCtrl<T>(DependencyObject child) where T : System.Windows.Controls.UserControl
        {
            var usrCtrl = FindUserControl(child);
            if (usrCtrl != null) return (T)usrCtrl;
            return null;
        }

        /// <summary>取得親系[UserControl]</summary>
        public static System.Windows.Controls.UserControl FindUserControl(DependencyObject child)
        {
            DependencyObject parent = VisualTreeHelper.GetParent(child);
            if (parent == null) return null;

            if (parent is System.Windows.Controls.UserControl ucItem) return ucItem;

            return FindUserControl(parent);
        }

        /// <summary>取得親系[Window]</summary>
        public static System.Windows.Window FindWindow(DependencyObject child)
        {
            DependencyObject parent = VisualTreeHelper.GetParent(child);
            if (parent == null) return null;

            if (parent is System.Windows.Window ucItem) return ucItem;

            return FindWindow(parent);
        }

        #endregion UserControl

        #region SetValue

        #region Label

        /// <summary>
        /// Label 給值
        /// </summary>
        /// <param name="ui">Label</param>
        /// <param name="val">值</param>
        /// <param name="def">預設值</param>
        public static void SetContent(this Label ui, string val, string def = "")
        {
            if (string.IsNullOrWhiteSpace(val))
            {
                ui.Content = def;
            }
            else
            {
                ui.Content = val.Trim();
            }
        }

        /// <summary>
        /// Label 給值
        /// </summary>
        /// <param name="ui">Label</param>
        /// <param name="val">值</param>
        /// <param name="def">預設值</param>
        /// <param name="format">格式</param>
        public static void SetContent(this Label ui, DateTime val, string def = "", string format = "yyyy-MM-dd HH:mm:ss")
        {
            if (val == DateTime.MinValue)
            {
                ui.Content = def;
            }
            else
            {
                ui.Content = val.ToString(format);
            }
        }

        /// <summary>
        /// Label 給值
        /// </summary>
        /// <param name="ui">Label</param>
        /// <param name="val">值</param>
        /// <param name="format">格式</param>
        public static void SetContent(this Label ui, DateTimeOffset val, string format = "yyyy-MM-dd HH:mm:ss")
        {
            ui.Content = val.LocalDateTime.ToString(format);
        }

        /// <summary>
        /// Label 給值
        /// </summary>
        /// <param name="ui">Label</param>
        /// <param name="val">值</param>
        /// <param name="def">預設值</param>
        /// <param name="format">格式</param>
        public static void SetContentDtmfstNull(this Label ui, DateTimeOffset? val, string def = "", string format = "yyyy-MM-dd HH:mm:ss")
        {
            if (!val.HasValue)
            {
                ui.Content = def;
            }
            else
            {
                ui.Content = val.Value.LocalDateTime.ToString(format);
            }
        }

        /// <summary>
        /// Label 給值
        /// </summary>
        /// <param name="ui">Label</param>
        /// <param name="val">值</param>
        /// <param name="def">預設值</param>
        public static void SetContent(this Label ui, int val, string def = "")
        {
            if (val == int.MinValue)
            {
                ui.Content = def;
            }
            else
            {
                ui.Content = val.ToString();
            }
        }

        /// <summary>
        /// Label 給值
        /// </summary>
        /// <param name="ui">Label</param>
        /// <param name="val">值</param>
        /// <param name="def">預設值</param>
        public static void SetContent(this Label ui, int? val, string def = "")
        {
            if (val.HasValue)
            {
                SetContent(ui, val.Value, def);
            }
            else
            {
                ui.Content = def;
            }
        }

        /// <summary>
        /// Label 給值
        /// </summary>
        /// <param name="ui">Label</param>
        /// <param name="val">值</param>
        /// <param name="def">預設值</param>
        /// <param name="format">格式</param>
        public static void SetContent(this Label ui, decimal val, string def = "", string format = "###,###")
        {
            if (val == decimal.MinValue)
            {
                ui.Content = def;
            }
            else
            {
                ui.Content = val.ToString(format);
            }
        }

        /// <summary>
        /// Label 給值
        /// </summary>
        /// <param name="ui">Label</param>
        /// <param name="val">值</param>
        /// <param name="def">預設值</param>
        /// <param name="format">格式</param>
        public static void SetContentDcmNull(this Label ui, decimal? val, string def = "", string format = "###,###")
        {
            if (val.HasValue)
            {
                SetContent(ui, val.Value, def, format);
            }
            else
            {
                ui.Content = def;
            }
        }

        #endregion Label

        #region ComboBox

        /// <summary>
        /// ComboBox 繫結資料
        /// </summary>
        /// <param name="ui">控件</param>
        /// <param name="list">字典資料</param>
        public static void BindMenu(this ComboBox ui, Dictionary<int, string> list)
        {
            ui.ItemsSource = list;
            ui.SelectedIndex = -1;
            ui.SelectedValuePath = "Key";
            ui.DisplayMemberPath = "Value";
        }

        /// <summary>
        /// ComboBox 繫結資料
        /// </summary>
        public static void BindMenu(this ComboBox ui, List<string> list)
        {
            ui.Items.Clear();

            foreach (var value in list)
            {
                ui.Items.Add(value);
            }

            if (ui.Items.Count > 0)
            {
                ui.Text = ui.Items[0].ToString();
                ui.SelectedIndex = 0;
            }
        }

        /// <summary>
        /// ComboBox 給值
        /// </summary>
        /// <param name="ui">Label</param>
        /// <param name="val">值</param>
        public static void SetVal(this ComboBox ui, string val)
        {
            if (!string.IsNullOrWhiteSpace(val))
            {
                ui.Items.Add(val);
                ui.Text = val;
            }
        }

        /// <summary>
        /// ComboBox 給值
        /// </summary>
        /// <param name="ui">Label</param>
        /// <param name="val">值</param>
        public static void SetText(this ComboBox ui, string val)
        {
            if (!string.IsNullOrWhiteSpace(val))
            {
                ui.Text = val;
            }
        }

        /// <summary>
        /// ComboBox 給值
        /// </summary>
        /// <param name="ui">Label</param>
        /// <param name="val">值</param>
        public static void Selected(this ComboBox ui, int val)
        {
            if (val != int.MinValue)
            {
                ui.SelectedValue = val.ToString();
            }
        }
        #endregion ComboBox

        #region TextBox

        /// <summary>
        /// Text 給值
        /// </summary>
        /// <param name="ui">Label</param>
        /// <param name="val">值</param>
        /// <param name="def">預設值</param>
        public static void SetText(this TextBox ui, string val, string def = "")
        {
            if (string.IsNullOrWhiteSpace(val))
            {
                ui.Text = def;
            }
            else
            {
                ui.Text = val.Trim();
            }
        }

        /// <summary>
        /// Text 給值
        /// </summary>
        /// <param name="ui">Label</param>
        /// <param name="val">值</param>
        /// <param name="def">預設值</param>
        /// <param name="format">格式</param>
        public static void SetText(this TextBox ui, DateTime val, string def = "", string format = "yyyy-MM-dd HH:mm:ss")
        {
            if (val == DateTime.MinValue)
            {
                ui.Text = def;
            }
            else
            {
                ui.Text = val.ToString(format);
            }
        }

        /// <summary>
        /// Text 給值
        /// </summary>
        /// <param name="ui">Label</param>
        /// <param name="val">值</param>
        /// <param name="def">預設值</param>
        /// <param name="format">格式</param>
        public static void SetText(this TextBox ui, DateTimeOffset val, string def = "", string format = "yyyy-MM-dd HH:mm:ss")
        {
            if (val.LocalDateTime.Year > 2016)
            {
                SetText(ui, val.LocalDateTime, def, format);
            }
        }

        /// <summary>
        /// Text 給值
        /// </summary>
        /// <param name="ui">Label</param>
        /// <param name="val">值</param>
        /// <param name="def">預設值</param>
        /// <param name="format">格式</param>
        public static void SetTextDtmfstNull(this TextBox ui, DateTimeOffset? val, string def = "", string format = "yyyy-MM-dd HH:mm:ss")
        {
            if (!val.HasValue)
            {
                ui.Text = def;
            }
            else
            {
                if (val.Value.LocalDateTime.Year > 2016)
                {
                    SetText(ui, val.Value.LocalDateTime, def, format);
                }
            }
        }

        /// <summary>
        /// Text 給值
        /// </summary>
        /// <param name="ui">Label</param>
        /// <param name="val">值</param>
        /// <param name="def">預設值</param>
        public static void SetText(this TextBox ui, int val, string def = "")
        {
            if (val == int.MinValue)
            {
                ui.Text = def;
            }
            else
            {
                ui.Text = val.ToString();
            }
        }

        /// <summary>
        /// Text 給值
        /// </summary>
        /// <param name="ui">Label</param>
        /// <param name="val">值</param>
        /// <param name="def">預設值</param>
        public static void SetText(this TextBox ui, int? val, string def = "")
        {
            if (val.HasValue)
            {
                SetText(ui, val.Value, def);
            }
            else
            {
                ui.Text = def;
            }
        }

        /// <summary>
        /// Text 給值
        /// </summary>
        /// <param name="ui">Label</param>
        /// <param name="val">值</param>
        /// <param name="def">預設值</param>
        /// <param name="format">格式</param>
        public static void SetText(this TextBox ui, decimal val, string def = "", string format = "###,###")
        {
            if (val == decimal.MinValue)
            {
                ui.Text = def;
            }
            else
            {
                ui.Text = val.ToString(format);
            }
        }

        /// <summary>
        /// Text 給值
        /// </summary>
        /// <param name="ui">Label</param>
        /// <param name="val">值</param>
        /// <param name="def">預設值</param>
        /// <param name="format">格式</param>
        public static void SetTextDcmNull(this TextBox ui, decimal? val, string def = "", string format = "###,###")
        {
            if (val.HasValue)
            {
                SetText(ui, val.Value, def, format);
            }
            else
            {
                ui.Text = def;
            }
        }

        /// <summary>Label 給值</summary>
        public static void SetText(this TextBox ui, double val, string def = "", string format = "###,###")
        {
            if (val == double.MinValue)
            {
                ui.Text = def;
            }
            else
            {
                ui.Text = val.ToString(format);
            }
        }

        /// <summary>Label 給值</summary>
        public static void SetTextDblNull(this TextBox ui, double? val, string def = "", string format = "###,###")
        {
            if (val.HasValue)
            {
                SetText(ui, val.Value, def, format);
            }
            else
            {
                ui.Text = def;
            }
        }

        #endregion TextBox

        #endregion SetValue

        #region GetValue

        /// <summary>Label 取值</summary>
        public static string StringValue(this Label ui)
        {
            return (string)ui.Content;
        }

        /// <summary>
        /// 轉換為 int
        /// </summary>
        public static int IntValue(this Label ui, int def = 0)
        {
            if (int.TryParse((string)ui.Content, out int result))
            {
                return result;
            }
            else
            {
                return def;
            }
        }

        /// <summary>
        /// 轉換為 int?
        /// </summary>
        public static int? IntNullValue(this Label ui)
        {
            if (int.TryParse((string)ui.Content, out int result))
            {
                return result;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 轉換為 string (Trim)
        /// </summary>
        public static string StringValue(this TextBox ui)
        {
            return ui.Text.Trim();
        }

        /// <summary>
        /// 轉換為 string (Trim)
        /// </summary>
        public static string StringNullValue(this TextBox ui)
        {
            string result = ui.Text.Trim();
            return !string.IsNullOrWhiteSpace(result) ? result : null;
        }

        /// <summary>
        /// 轉換為 int
        /// </summary>
        public static int IntValue(this TextBox ui, int def = 0)
        {
            if (int.TryParse(ui.Text.Trim(), out int result))
            {
                return result;
            }
            else
            {
                return def;
            }
        }

        /// <summary>
        /// 轉換為 int?
        /// </summary>
        public static int? IntNullValue(this TextBox ui)
        {
            if (int.TryParse(ui.Text.Trim(), out int result))
            {
                return result;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 轉換為 double
        /// </summary>
        public static double DoubleValue(this TextBox ui, double def = 0)
        {
            if (double.TryParse(ui.Text.Trim(), out double result))
            {
                return result;
            }
            else
            {
                return def;
            }
        }

        /// <summary>
        /// 轉換為 double?
        /// </summary>
        public static double? DoubleNullValue(this TextBox ui)
        {
            if (double.TryParse(ui.Text.Trim(), out double result))
            {
                return result;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 轉換為 DateTime?
        /// </summary>
        public static DateTime? NullValue(this DatePicker ui)
        {
            return ui.SelectedDate;
        }

        /// <summary>
        /// 轉換為 DateTime
        /// </summary>
        public static DateTime Value(this DatePicker ui)
        {
            return ui.SelectedDate ?? DateTime.MinValue;
        }

        /// <summary>
        /// 轉換為 DateTime?
        /// </summary>
        public static DateTimeOffset? DateTimeOffsetNullValue(this DatePicker ui)
        {
            if (ui.SelectedDate.HasValue)
            {
                return new DateTimeOffset(ui.SelectedDate.Value);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// ComboBox 取值
        /// </summary>
        public static string StringValue(this ComboBox ui)
        {
            return ui.Text.Trim();
        }

        /// <summary>
        /// ComboBox 取值
        /// </summary>
        public static int SelectedIntValue(this ComboBox ui, int def = 0)
        {
            string s = ui.SelectedValue?.ToString();
            if (!string.IsNullOrWhiteSpace(s) && int.TryParse(s, out int result))
            {
                return result;
            }
            else
            {
                return def;
            }
        }

        /// <summary>
        /// ComboBox 取值
        /// </summary>
        public static int? SelectedIntNull(this ComboBox ui)
        {
            string s = ui.SelectedValue?.ToString();
            if (!string.IsNullOrWhiteSpace(s) && int.TryParse(s, out int result))
            {
                return result;
            }
            else
            {
                return null;
            }
        }

        /// <summary>CheckBox 取值</summary>
        public static bool BoolValue(this CheckBox ui)
        {
            return ui.IsChecked.HasValue && ui.IsChecked.Value;
        }

        #endregion GetValue

        #region 其他

        /// <summary>json 檔案轉為型別</summary>
        /// <typeparam name="TOut">泛型類別</typeparam>
        public static TOut Get<TOut>(this string file)
        {
            string path = AppDomain.CurrentDomain.BaseDirectory.TrimEnd('\\') + "\\" + file.TrimStart('\\');

            TOut result = default(TOut);

            string contents = File.ReadAllText(path, System.Text.Encoding.UTF8);

            if (!string.IsNullOrWhiteSpace(contents))
            {
                result = Newtonsoft.Json.JsonConvert.DeserializeObject<TOut>(contents);
            }

            return result;
        }

        /// <summary>instance 轉為 json 字串存入檔案</summary>
        /// <typeparam name="TIn">泛型類別</typeparam>
        public static void Save<TIn>(this string file, TIn model)
        {
            string contents = Newtonsoft.Json.JsonConvert.SerializeObject(model);

            string path = AppDomain.CurrentDomain.BaseDirectory.TrimEnd('\\') + "\\" + file.TrimStart('\\');

            File.WriteAllText(path, contents, System.Text.Encoding.UTF8);
        }

        /// <summary>檢查 IEnumerable 型別是否為 null</summary>
        /// <typeparam name="T">泛型類別</typeparam>
        public static bool IsNullOrEmpty<T>(this IEnumerable<T> enumerable)
        {
            return enumerable == null || !enumerable.Any();
        }

        /// <summary>換行字元轉換</summary>
        public static string Rn2Br(this string val)
        {
            if (string.IsNullOrWhiteSpace(val))
            {
                return string.Empty;
            }
            else
            {
                return val.Replace("\r\n", "<br />");
            }
        }

        /// <summary>換行字元轉換</summary>
        public static string Br2Rn(this string val)
        {
            if (string.IsNullOrWhiteSpace(val))
            {
                return string.Empty;
            }
            else
            {
                return val.Replace("<br />", "\r\n");
            }
        }

        #endregion 其他

        /// <summary>
        /// Finds a Child of a given item in the visual tree.
        /// </summary>
        /// <param name="parent">A direct parent of the queried item.</param>
        /// <typeparam name="T">The type of the queried item.</typeparam>
        /// <param name="childName">x:Name or Name of child. </param>
        /// <returns>The first parent item that matches the submitted type parameter.
        /// If not matching item can be found,
        /// a null parent is being returned.</returns>
        public static T FindChild<T>(DependencyObject parent, string childName)
           where T : DependencyObject
        {
            // Confirm parent and childName are valid.
            if (parent == null) return null;

            T foundChild = null;

            int childrenCount = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < childrenCount; i++)
            {
                var child = VisualTreeHelper.GetChild(parent, i);
                // If the child is not of the request child type child
                T childType = child as T;
                if (childType == null)
                {
                    // recursively drill down the tree
                    foundChild = FindChild<T>(child, childName);

                    // If the child is found, break so we do not overwrite the found child.
                    if (foundChild != null) break;
                }
                else if (!string.IsNullOrEmpty(childName))
                {
                    var frameworkElement = child as FrameworkElement;
                    // If the child's name is set for search
                    if (frameworkElement != null && frameworkElement.Name == childName)
                    {
                        // if the child's name is of the request name
                        foundChild = (T)child;
                        break;
                    }
                }
                else
                {
                    // child element found.
                    foundChild = (T)child;
                    break;
                }
            }

            return foundChild;
        }
    }
}