﻿using System;
using System.IO;
using System.Reflection;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using WorkHelp.Wpf.Providers;
using WorkHelp.Wpf.Services;

namespace WorkHelp.Wpf.Extensions
{
    /// <summary>
    /// 瀏覽器擴充
    /// </summary>
    public class BrowserExtension
    {
        /// <summary>
        /// 建構元
        /// </summary>
        protected BrowserExtension() { }

        /// <summary>
        /// 設置無聲
        /// </summary>
        public static void SetSilent(WebBrowser browser, bool silent = true)
        {
            if (browser == null)
                throw new ArgumentNullException("browser");

            // get an IWebBrowser2 from the document
            IOleServiceProvider sp = browser.Document as IOleServiceProvider;
            if (sp != null)
            {
                Guid IID_IWebBrowserApp = new Guid("0002DF05-0000-0000-C000-000000000046");
                Guid IID_IWebBrowser2 = new Guid("D30C1661-CDAF-11d0-8A3E-00C04FC9E26E");

                object webBrowser;
                sp.QueryService(ref IID_IWebBrowserApp, ref IID_IWebBrowser2, out webBrowser);
                if (webBrowser != null)
                {
                    webBrowser.GetType().InvokeMember("Silent", BindingFlags.Instance | BindingFlags.Public | BindingFlags.PutDispProperty, null, webBrowser, new object[] { silent });
                }
            }
        }

        /// <summary>
        /// 視窗瀏覽
        /// </summary>
        public static void NavigateUrl(WebBrowser browser, TextBox txt)
        {
            string url = txt.Text.Trim();
            if (!string.IsNullOrWhiteSpace(url))
            {
                browser.Navigate(url);
            }
        }

        /// <summary>
        /// 樹狀檢視控件瀏覽
        /// </summary>
        public static void NavigateUrl(TreeView tree, TextBox txt)
        {
            string url = txt.Text.Trim();
            if (!string.IsNullOrWhiteSpace(url))
            {
                tree.ItemsSource = Win502Service.GetMenuNodes(url);
            }
        }

        /// <summary>
        /// 設置網址
        /// </summary>
        public static void SetUrlText(TextBox txt, WebBrowser browser)
        {
            if (browser.Source != null)
            {
                txt.Text = browser.Source.ToString();
            }
            else
            {
                txt.Text = string.Empty;
            }
        }

        ///// <summary>
        ///// 視窗瀏覽
        ///// </summary>
        //public static void NavigateUrl(DotNetBrowser.WPF.WPFBrowserView view, TextBox txt)
        //{
        //    string url = txt.Text.Trim();
        //    if (!string.IsNullOrWhiteSpace(url))
        //    {
        //        view.Browser.LoadURL(url);
        //    }
        //}

        ///// <summary>
        ///// 設置網址
        ///// </summary>
        //public static void SetUrlText(TextBox txt, DotNetBrowser.WPF.WPFBrowserView view)
        //{
        //    if (view.Browser != null && !string.IsNullOrWhiteSpace(view.Browser.URL))
        //    {
        //        txt.Text = view.Browser.URL;
        //    }
        //    else
        //    {
        //        txt.Text = string.Empty;
        //    }
        //}

        /// <summary>
        /// 圖片瀏覽
        /// </summary>
        public static void NavigateUrl(Image img, TextBox txt)
        {
            string url = txt.Text.Trim();
            if (!string.IsNullOrWhiteSpace(url) && File.Exists(url))
            {
                FileStream fstream = new FileStream(url, FileMode.Open);
                BitmapImage bitmap = new BitmapImage();
                bitmap.BeginInit();
                bitmap.StreamSource = fstream;
                bitmap.CacheOption = BitmapCacheOption.OnLoad;
                bitmap.EndInit();
                fstream.Close();

                img.BeginInit();
                img.Source = bitmap;
                img.EndInit();
            }
        }
    }
}