﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using WorkHelp.Modules;
using WorkHelp.Wpf.Extensions;
using WorkHelp.Wpf.Models.Main;
using WorkHelp.Wpf.Models.ViewSplit;
using WorkHelp.Wpf.Models.ViewSplit.Extends;
using WorkHelp.Wpf.Models.Windows;
using WorkHelp.Wpf.Services;
using WorkHelp.Wpf.Utility;

namespace WorkHelp.Wpf.Views
{
    /// <summary>
    /// 瀏覽器處理-垂直雙欄+右側欄
    /// </summary>
    public partial class Win50100 : BaseWindow
    {
        //-----------------------------系統原生函式------------------------------//

        /// <summary>
        /// 視窗組態檔名稱
        /// </summary>
        public static string ConfigFile = "Config501";

        /// <summary>
        /// 瀏覽器應用選單-組態檔名稱
        /// </summary>
        public static string BrowserConfigFile = "BrowserConfig";

        /// <summary>
        /// 畫面切割選單
        /// </summary>
        private Dictionary<int, string> ViewPartingMenu { get; set; }

        /// <summary>
        /// 建構元
        /// </summary>
        public Win50100()
        {
            InitializeComponent();
            BindEvents();
        }

        /// <summary>
        /// 繫結事件
        /// </summary>
        public void BindEvents()
        {
            this.Loaded += Window_Loaded;
            this.BtnClose.Click += base.FWindowClose_Click;
            this.KeyDown += base.FWindow_KeyDown;

            this.TreeMenu.SelectedItemChanged += TreeMenu_SelectedItemChanged;

            this.BtnNewNode.Click += BtnNewNode_Click;
            this.BtnClearNode.Click += BtnClearNode_Click;
            this.BtnUpdateNode.Click += BtnUpdateNode_Click;
            this.BtnDeleteNode.Click += BtnDeleteNode_Click;

            this.BtnNewMember.Click += BtnNewMember_Click;
            this.BtnClearMember.Click += BtnClearMember_Click;
            this.BtnUpdateMember.Click += BtnUpdateMember_Click;
            this.BtnDeleteMember.Click += BtnDeleteMember_Click;

            this.BtnReadConfig.Click += BtnReadConfig_Click;
            this.BtnWriteConfig.Click += BtnWriteConfig_Click;
        }

        /// <summary>
        /// 視窗載入
        /// </summary>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            LoadConfig();
            LoadMenu();
        }

        /// <summary>
        /// 選單選取
        /// </summary>
        private void TreeMenu_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            TreeView tree = sender as TreeView;
            object selected = tree.SelectedItem;
            if (selected is MenuNode node)
            {
                LoadNodeData(node);
                ClearMember();

                BtnNewMember.Visibility = Visibility.Visible;
                BtnClearMember.Visibility = Visibility.Visible;

                BtnUpdateMember.Visibility = Visibility.Collapsed;
                BtnDeleteMember.Visibility = Visibility.Collapsed;
            }
            else if (selected is MenuNodeMember member)
            {
                LoadMemberData(member);

                BtnNewMember.Visibility = Visibility.Collapsed;
                BtnClearMember.Visibility = Visibility.Collapsed;

                BtnUpdateMember.Visibility = Visibility.Visible;
                BtnDeleteMember.Visibility = Visibility.Visible;
            }
        }

        #region Node

        /// <summary>
        /// 新增群組
        /// </summary>
        private void BtnNewNode_Click(object sender, RoutedEventArgs e)
        {
            NewNode();
            TxtNodeName.Text = string.Empty;
        }

        /// <summary>
        /// 清除群組名稱
        /// </summary>
        private void BtnClearNode_Click(object sender, RoutedEventArgs e)
        {
            TxtNodeName.Text = string.Empty;
        }

        /// <summary>
        /// 修改群組名稱
        /// </summary>
        private void BtnUpdateNode_Click(object sender, RoutedEventArgs e)
        {
            if (noSelectedNode()) return;
            UpdateNode();
        }

        /// <summary>
        /// 刪除群組名稱
        /// </summary>
        private void BtnDeleteNode_Click(object sender, RoutedEventArgs e)
        {
            if (noSelectedNode()) return;

            MessageBoxResult dialogResult = MessageBox.Show($"是否確認刪除【{TxtSelectedNodeName.Text}】？", "Confirmation", MessageBoxButton.YesNo);
            if (dialogResult != MessageBoxResult.Yes) return;

            DeleteNode();
            ClearSelectedNode();
        }

        #endregion Node

        #region Member

        /// <summary>
        /// 新增成員資料
        /// </summary>
        private void BtnNewMember_Click(object sender, RoutedEventArgs e)
        {
            if (noSelectedNode()) return;
            NewMember();
            ClearMember();
        }

        /// <summary>
        /// 清除成員資料
        /// </summary>
        private void BtnClearMember_Click(object sender, RoutedEventArgs e)
        {
            if (noSelectedNode()) return;
            ClearMember();
        }

        /// <summary>
        /// 修改成員資料
        /// </summary>
        private void BtnUpdateMember_Click(object sender, RoutedEventArgs e)
        {
            if (noSelectedNode() || noSelectedMember()) return;
            UpdateMember();
        }

        /// <summary>
        /// 刪除成員資料
        /// </summary>
        private void BtnDeleteMember_Click(object sender, RoutedEventArgs e)
        {
            if (noSelectedNode() || noSelectedMember()) return;

            MessageBoxResult dialogResult = MessageBox.Show($"是否確認刪除【{TxtMemberName.Text}】？", "Confirmation", MessageBoxButton.YesNo);
            if (dialogResult != MessageBoxResult.Yes) return;

            DeleteMember();
            ClearMember();
        }

        #endregion Member

        #region Config

        /// <summary>
        /// 載入畫面組態
        /// </summary>
        private void BtnReadConfig_Click(object sender, RoutedEventArgs e)
        {
            if (noSelectedNode() || noSelectedMember()) return;

            string configFile = TxtMemberConfigFile.Text.Trim();
            string viewName = TxtMemberName.Text.Trim();

            if (string.IsNullOrWhiteSpace(configFile))
            {
                System.Windows.Forms.MessageBox.Show("請輸入組態檔名稱");
                return;
            }
            if (string.IsNullOrWhiteSpace(viewName))
            {
                System.Windows.Forms.MessageBox.Show("請輸入畫面分割名稱");
                return;
            }

            LoadViewConfig(configFile, viewName);
        }

        /// <summary>
        /// 寫入畫面組態
        /// </summary>
        private void BtnWriteConfig_Click(object sender, RoutedEventArgs e)
        {
            if (noSelectedNode() || noSelectedMember()) return;

            string configFile = TxtMemberConfigFile.Text.Trim();
            string viewName = TxtMemberName.Text.Trim();

            if (string.IsNullOrWhiteSpace(configFile))
            {
                System.Windows.Forms.MessageBox.Show("請輸入組態檔名稱");
                return;
            }
            if (string.IsNullOrWhiteSpace(viewName))
            {
                System.Windows.Forms.MessageBox.Show("請輸入畫面分割名稱");
                return;
            }

            SaveViewConfig(configFile, viewName);
        }

        /// <summary>
        /// 載入畫面組態
        /// </summary>
        private void LoadViewConfig(string configFile, string viewName)
        {
            ViewConfig config = ConfigUtility.Read<ViewConfig>(configFile);
            if (config == null)
            {
                config = new ViewConfig();
            }

            ViewParting parting = config.Items?.Find(x => x.Name.Equals(viewName, StringComparison.CurrentCultureIgnoreCase));
            if (parting == null)
            {
                parting = new ViewParting();
            }

            TxtTopLeftUrl.SetText(parting.TopLeftUrl);
            TxtTopCenterUrl.SetText(parting.TopCenterUrl);
            TxtTopRightUrl.SetText(parting.TopRightUrl);

            TxtMiddleLeftUrl.SetText(parting.MiddleLeftUrl);
            TxtMiddleCenterUrl.SetText(parting.MiddleCenterUrl);
            TxtMiddleRightUrl.SetText(parting.MiddleRightUrl);

            TxtBottomLeftUrl.SetText(parting.BottomLeftUrl);
            TxtBottomCenterUrl.SetText(parting.BottomCenterUrl);
            TxtBottomRightUrl.SetText(parting.BottomRightUrl);

            System.Windows.Forms.MessageBox.Show("畫面組態讀取成功!!");
        }

        /// <summary>
        /// 寫入畫面組態
        /// </summary>
        private void SaveViewConfig(string configFile, string viewName)
        {
            ViewConfig config = ConfigUtility.Read<ViewConfig>(configFile);
            if (config == null)
            {
                config = new ViewConfig();
            }

            if (config.Items == null || config.Items.Count == 0)
            {
                config.Items = new List<ViewParting>();
            }

            ViewParting parting = config.Items?.Find(x => x.Name.Equals(viewName, StringComparison.CurrentCultureIgnoreCase));
            if (parting == null)
            {
                parting = new ViewParting
                {
                    Id = System.Guid.NewGuid().ToString(),
                    Name = viewName
                };

                config.Items.Add(parting);
            }

            parting.TopLeftUrl = TxtTopLeftUrl.Text.Trim();
            parting.TopCenterUrl = TxtTopCenterUrl.Text.Trim();
            parting.TopRightUrl = TxtTopRightUrl.Text.Trim();

            parting.MiddleLeftUrl = TxtMiddleLeftUrl.Text.Trim();
            parting.MiddleCenterUrl = TxtMiddleCenterUrl.Text.Trim();
            parting.MiddleRightUrl = TxtMiddleRightUrl.Text.Trim();

            parting.BottomLeftUrl = TxtBottomLeftUrl.Text.Trim();
            parting.BottomCenterUrl = TxtBottomCenterUrl.Text.Trim();
            parting.BottomRightUrl = TxtBottomRightUrl.Text.Trim();

            ConfigUtility.Write<ViewConfig>(config, configFile);

            System.Windows.Forms.MessageBox.Show("畫面組態儲存成功!!");
        }

        #endregion Config

        //-----------------------------使用者自訂函式-----------------------------//

        /// <summary>
        /// 載入組態檔
        /// </summary>
        private void LoadConfig()
        {
            Config501 config = ReadConfig();
            if (config == null || config.Nodes == null || config.Nodes.Count == 0)
            {
                config = InitialConfig();
                WriteConfig(config);
            }

            TreeMenu.ItemsSource = config.Nodes;
        }

        /// <summary>
        /// 讀取組態檔
        /// </summary>
        private Config501 ReadConfig()
        {
            return ConfigUtility.Read<Config501>(ConfigFile);
        }

        /// <summary>
        /// 組態檔自動初始化
        /// </summary>
        private Config501 InitialConfig()
        {
            Config501 config = new Config501 { Nodes = new List<MenuNode>() };
            config.Nodes.Add(Win501Service.GetNodeCommon(BrowserConfigFile));
            config.Nodes.Add(Win501Service.GetNodeProgram(BrowserConfigFile));
            return config;
        }

        /// <summary>
        /// 寫入組態檔
        /// </summary>
        private void WriteConfig(Config501 config)
        {
            ConfigUtility.Write(config, ConfigFile);
            // 向全域通報要更新瀏覽器應用選單
            EventSystem.Notify(EventTopic.ReloadBrowserMenu);
        }

        #region Menu

        /// <summary>
        /// 載入選單
        /// </summary>
        private void LoadMenu()
        {
            Dictionary<int, string> list = ViewPartingExtend.GetViewPartingList();
            // 暫存畫面分割類型選單
            ViewPartingMenu = list;
            // 可綁定多個下拉式選單
            ListViewParting(CbxViewParting, list);
        }

        /// <summary>
        /// 畫面分割類型選單
        /// </summary>
        /// <param name="ui">控件</param>
        private void ListViewParting(ComboBox ui, Dictionary<int, string> list)
        {
            ui.ItemsSource = list;
            ui.SelectedIndex = -1;
            ui.SelectedValuePath = "Key";
            ui.DisplayMemberPath = "Value";
        }

        #endregion Menu

        #region Node

        /// <summary>
        /// 新增群組
        /// </summary>
        private void NewNode()
        {
            Config501 config = ReadConfig();
            MenuNode node = GetNewNode();

            if (config == null || config.Nodes == null || config.Nodes.Count == 0)
            {
                System.Windows.Forms.MessageBox.Show("[新增群組]組態檔發生錯誤");
                return;
            }

            if (string.IsNullOrWhiteSpace(node.Name))
            {
                System.Windows.Forms.MessageBox.Show("[新增群組]請輸入群組名稱");
                return;
            }

            config.Nodes.Add(node);
            WriteConfig(config);
            System.Windows.Forms.MessageBox.Show("群組新增成功");

            TreeMenu.ItemsSource = config.Nodes;
        }

        /// <summary>
        /// 修改群組
        /// </summary>
        private void UpdateNode()
        {
            Config501 config = ReadConfig();
            MenuNode selected = GetUINode();
            MenuNode searched = config?.Nodes?.Find(x => x.Id == selected.Id);

            if (config == null || config.Nodes == null || config.Nodes.Count == 0)
            {
                System.Windows.Forms.MessageBox.Show("[修改群組]組態檔發生錯誤");
                return;
            }

            if (string.IsNullOrWhiteSpace(selected.Id))
            {
                System.Windows.Forms.MessageBox.Show("[修改群組]請選擇群組");
                return;
            }

            if (string.IsNullOrWhiteSpace(selected.Name))
            {
                System.Windows.Forms.MessageBox.Show("[修改群組]請輸入群組名稱");
                return;
            }

            if (searched == null)
            {
                System.Windows.Forms.MessageBox.Show("[修改群組]查無該群組資料");
                return;
            }

            searched.Name = selected.Name;
            WriteConfig(config);
            System.Windows.Forms.MessageBox.Show("群組修改成功");

            TreeMenu.ItemsSource = config.Nodes;
        }

        /// <summary>
        /// 刪除群組
        /// </summary>
        private void DeleteNode()
        {
            Config501 config = ReadConfig();
            MenuNode selected = GetUINode();
            MenuNode searched = config?.Nodes?.Find(x => x.Id == selected.Id);

            if (config == null || config.Nodes == null || config.Nodes.Count == 0)
            {
                System.Windows.Forms.MessageBox.Show("[刪除群組]組態檔發生錯誤");
                return;
            }

            if (string.IsNullOrWhiteSpace(selected.Id))
            {
                System.Windows.Forms.MessageBox.Show("[刪除群組]請選擇群組");
                return;
            }

            if (searched == null)
            {
                System.Windows.Forms.MessageBox.Show("[刪除群組]查無該群組資料");
                return;
            }

            config.Nodes.Remove(searched);
            WriteConfig(config);
            System.Windows.Forms.MessageBox.Show("群組刪除成功");

            TreeMenu.ItemsSource = config.Nodes;
        }

        /// <summary>
        /// 清除選取的群組資料
        /// </summary>
        private void ClearSelectedNode()
        {
            TxtSelectedNodeId.Text = string.Empty;
            TxtSelectedNodeName.Text = string.Empty;

            TreeMenu.ClearSelection();
        }

        /// <summary>
        /// 取得新增群組
        /// </summary>
        private MenuNode GetNewNode()
        {
            return new MenuNode
            {
                Id = System.Guid.NewGuid().ToString(),
                Name = TxtNodeName.Text.Trim(),
                Members = new List<MenuNodeMember>()
            };
        }

        /// <summary>
        /// 取得介面群組
        /// </summary>
        private MenuNode GetUINode()
        {
            return new MenuNode
            {
                Id = TxtSelectedNodeId.Text.Trim(),
                Name = TxtSelectedNodeName.Text.Trim()
            };
        }

        /// <summary>
        /// 載入群組資料
        /// </summary>
        private void LoadNodeData(MenuNode selected)
        {
            TxtSelectedNodeId.Text = selected.Id.Trim();
            TxtSelectedNodeName.Text = selected.Name.Trim();
        }

        /// <summary>
        /// 沒有選取群組
        /// </summary>
        private bool noSelectedNode()
        {
            return string.IsNullOrWhiteSpace(TxtSelectedNodeId.Text);
        }

        #endregion Node

        #region Member

        /// <summary>
        /// 載入成員資料
        /// </summary>
        private void LoadMemberData(MenuNodeMember selected)
        {
            Config501 config = ReadConfig();
            if (config == null) return;

            MenuNode parent = config.Nodes?.Find(x => x.Id == selected.ParentId);
            if (parent == null) return;

            LoadNodeData(parent);

            TxtMemberId.Text = selected.Id;
            TxtMemberName.Text = selected.Name;
            CbxViewParting.SelectedValue = selected.ModeCode;
            TxtMemberConfigFile.Text = selected.ConfigFile;
        }

        /// <summary>
        /// 新增成員
        /// </summary>
        private void NewMember()
        {
            Config501 config = ReadConfig();
            MenuNode uiNode = GetUINode();
            MenuNode selectedNode = config?.Nodes?.Find(x => x.Id == uiNode.Id);
            MenuNodeMember member = GetUIMember();

            if (config == null || config.Nodes == null || config.Nodes.Count == 0)
            {
                System.Windows.Forms.MessageBox.Show("[新增成員]組態檔發生錯誤");
                return;
            }

            if (selectedNode == null || string.IsNullOrWhiteSpace(selectedNode.Id))
            {
                System.Windows.Forms.MessageBox.Show("[新增成員]請選擇群組");
                return;
            }

            if (string.IsNullOrWhiteSpace(member.Name))
            {
                System.Windows.Forms.MessageBox.Show("[新增成員]請輸入成員名稱");
                return;
            }

            if (member.ModeCode == 0)
            {
                System.Windows.Forms.MessageBox.Show("[新增成員]請選擇畫面分割類型");
                return;
            }

            if (string.IsNullOrWhiteSpace(member.ConfigFile))
            {
                System.Windows.Forms.MessageBox.Show("[新增成員]請輸入組態檔名稱");
                return;
            }

            member.Id = Guid.NewGuid().ToString();
            member.ParentId = selectedNode.Id;
            selectedNode.Members.Add(member);

            WriteConfig(config);
            System.Windows.Forms.MessageBox.Show("成員新增成功");

            TreeMenu.ItemsSource = config.Nodes;
        }

        /// <summary>
        /// 修改成員
        /// </summary>
        private void UpdateMember()
        {
            Config501 config = ReadConfig();

            MenuNode uiNode = GetUINode();
            MenuNode selectedNode = config?.Nodes?.Find(x => x.Id == uiNode.Id);

            MenuNodeMember uiMember = GetUIMember();
            MenuNodeMember selectedMember = selectedNode?.Members?.Find(x => x.Id == uiMember.Id);

            if (config == null || config.Nodes == null || config.Nodes.Count == 0)
            {
                System.Windows.Forms.MessageBox.Show("[修改成員]組態檔發生錯誤");
                return;
            }

            if (selectedNode == null || string.IsNullOrWhiteSpace(selectedNode.Id))
            {
                System.Windows.Forms.MessageBox.Show("[修改成員]請選擇群組");
                return;
            }

            if (selectedMember == null || string.IsNullOrWhiteSpace(selectedMember.Id))
            {
                System.Windows.Forms.MessageBox.Show("[修改成員]請選擇成員");
                return;
            }

            if (string.IsNullOrWhiteSpace(uiMember.Name))
            {
                System.Windows.Forms.MessageBox.Show("[修改成員]請輸入成員名稱");
                return;
            }

            if (uiMember.ModeCode == 0)
            {
                System.Windows.Forms.MessageBox.Show("[修改成員]請選擇畫面分割類型");
                return;
            }

            if (string.IsNullOrWhiteSpace(uiMember.ConfigFile))
            {
                System.Windows.Forms.MessageBox.Show("[修改成員]請輸入組態檔名稱");
                return;
            }

            selectedMember.Name = uiMember.Name;
            selectedMember.ModeType = uiMember.ModeType;
            selectedMember.ModeCode = uiMember.ModeCode;
            selectedMember.ModeName = uiMember.ModeName;
            selectedMember.ConfigFile = uiMember.ConfigFile;

            WriteConfig(config);
            System.Windows.Forms.MessageBox.Show("成員修改成功");

            TreeMenu.ItemsSource = config.Nodes;
        }

        /// <summary>
        /// 刪除成員
        /// </summary>
        private void DeleteMember()
        {
            Config501 config = ReadConfig();

            MenuNode uiNode = GetUINode();
            MenuNode selectedNode = config?.Nodes?.Find(x => x.Id == uiNode.Id);

            MenuNodeMember uiMember = GetUIMember();
            MenuNodeMember selectedMember = selectedNode?.Members?.Find(x => x.Id == uiMember.Id);

            if (config == null || config.Nodes == null || config.Nodes.Count == 0)
            {
                System.Windows.Forms.MessageBox.Show("[刪除成員]組態檔發生錯誤");
                return;
            }

            if (selectedNode == null || string.IsNullOrWhiteSpace(selectedNode.Id))
            {
                System.Windows.Forms.MessageBox.Show("[刪除成員]請選擇群組");
                return;
            }

            if (selectedMember == null || string.IsNullOrWhiteSpace(selectedMember.Id))
            {
                System.Windows.Forms.MessageBox.Show("[刪除成員]請選擇成員");
                return;
            }

            selectedNode.Members.Remove(selectedMember);

            WriteConfig(config);
            System.Windows.Forms.MessageBox.Show("成員刪除成功");

            TreeMenu.ItemsSource = config.Nodes;
        }

        /// <summary>
        /// 清除成員控件資料
        /// </summary>
        private void ClearMember()
        {
            TxtMemberId.Text = string.Empty;
            TxtMemberName.Text = string.Empty;
            CbxViewParting.SelectedIndex = -1;
            TxtMemberConfigFile.Text = BrowserConfigFile;
        }

        /// <summary>
        /// 取得介面群組
        /// </summary>
        private MenuNodeMember GetUIMember()
        {
            int modeCode = CbxViewParting.SelectedIntValue();
            ViewPartingType modeType = (ViewPartingType)modeCode;

            return new MenuNodeMember
            {
                Id = TxtMemberId.Text.Trim(),
                Name = TxtMemberName.Text.Trim(),
                ModeCode = modeCode,
                ModeType = modeType,
                ModeName = modeType.Name(),
                ConfigFile = TxtMemberConfigFile.Text.Trim()
            };
        }

        /// <summary>
        /// 沒有選取成員
        /// </summary>
        private bool noSelectedMember()
        {
            return string.IsNullOrWhiteSpace(TxtMemberId.Text);
        }

        #endregion Member
    }
}