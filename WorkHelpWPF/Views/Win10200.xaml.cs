﻿using System.Linq;
using System.Windows;
using System.Windows.Controls;
using WorkHelp.Utilities;
using WorkHelp.Wpf.Enums;
using WorkHelp.Wpf.Enums.Extends;
using WorkHelp.Wpf.Extensions;
using WorkHelp.Wpf.Models;
using WorkHelp.Wpf.Models.Windows;
using WorkHelp.Wpf.Services;
using WorkHelp.Wpf.Utility;

namespace WorkHelp.Wpf.Views
{
    /// <summary>
    /// 文字置換處理
    /// </summary>
    public partial class Win10200 : BaseWindow
    {
        /// <summary>
        /// 資料夾名稱
        /// </summary>
        private string FolderName { get; set; } = "Win10200";

        /// <summary>
        /// 建構元
        /// </summary>
        public Win10200()
        {
            InitializeComponent();
            BindEvents();
        }

        /// <summary>
        /// 繫結事件
        /// </summary>
        private void BindEvents()
        {
            this.Loaded += Window_Loaded;
            this.KeyDown += base.FWindow_KeyDown;
            this.BtnClose.Click += base.FWindowClose_Click;

            this.BtnSaveSetting.Click += this.BtnSaveSetting_Click;
            this.BtnDatabase.Click += this.BtnDatabase_Click;
            this.BtnTable.Click += this.BtnTable_Click;
            this.BtnExecuteTableFunc.Click += this.BtnExecuteTableFunc_Click;

            this.BtnSample.Click += this.BtnSample_Click;
            this.BtnExecute.Click += this.BtnExecute_Click;
            this.BtnClear.Click += this.BtnClear_Click;
            this.BtnOpenFolder.Click += this.BtnOpenFolder_Click;

            this.CbxFunction.SelectionChanged += this.CbxFunction_SelectionChanged;
        }

        //-----------------------------系統原生函式------------------------------//

        /// <summary>
        /// 視窗載入
        /// </summary>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            var config = ConfigUtility.Read<Config102>();
            if (config != null)
            {
                TxtServer.SetText(config.ServerName);
                TxtUid.SetText(config.UserId);
                TxtPwd.SetText(config.Password);

                CbxDatabase.Items.Add(config.DatabaseName);
                CbxDatabase.SetText(config.DatabaseName);

                CbxTable.Items.Add(config.TableName);
                CbxTable.SetText(config.TableName);

                TxtNameSpace.SetText(config.NameSpace);
                TxtFormName.SetText(config.FormName);
            }
            HiddenCondition();
            LoadMenu();
        }

        /// <summary>
        /// 儲存設定
        /// </summary>
        private void BtnSaveSetting_Click(object sender, RoutedEventArgs e)
        {
            ConfigUtility.Write(GetConfig());
            MessageBox.Show("儲存成功!!");
        }

        /// <summary>
        /// 載入資料庫
        /// </summary>
        private void BtnDatabase_Click(object sender, RoutedEventArgs e)
        {
            var config = GetConfig();
            if (!string.IsNullOrWhiteSpace(config.ServerName) && !string.IsNullOrWhiteSpace(config.UserId) && !string.IsNullOrWhiteSpace(config.Password))
            {
                var connString = ServiceUtility.MasterConnectionString(config);
                var source = Win401Service.GetDBList(connString);
                var list = source.Select(x => x.name).ToList();
                CbxDatabase.BindMenu(list);
            }
        }

        /// <summary>
        /// 載入資料表
        /// </summary>
        private void BtnTable_Click(object sender, RoutedEventArgs e)
        {
            var config = GetConfig();
            if (!string.IsNullOrWhiteSpace(config.ServerName) && !string.IsNullOrWhiteSpace(config.UserId) && !string.IsNullOrWhiteSpace(config.Password) && !string.IsNullOrWhiteSpace(config.DatabaseName))
            {
                var connString = ServiceUtility.GetConnectionString(config);
                var source = Win401Service.GetTableList(connString);
                var list = source.Select(x => x.name).ToList();
                CbxTable.BindMenu(list);
            }
        }

        /// <summary>
        /// 產生 Entity
        /// </summary>
        private void BtnExecuteTableFunc_Click(object sender, RoutedEventArgs e)
        {
            ExecuteTableFunction();
        }

        /// <summary>
        /// 功能選項變更
        /// </summary>
        private void CbxFunction_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //ClearView();
            HiddenCondition();
            if (CbxFunction.SelectedIndex > -1)
            {
                ReplaceFunction type = (ReplaceFunction)CbxFunction.SelectedIntValue();
                LblNotice.Content = ReplaceFunctionExtend.GetMenuNotice(type);
                switch (type)
                {
                    case ReplaceFunction.EqualSignExchange:
                        ChkTrim.Visibility = Visibility.Visible;
                        break;

                    case ReplaceFunction.CSharpClass:
                    case ReplaceFunction.CSharpEnum:
                        ChkTrim.Visibility = Visibility.Visible;
                        break;

                    case ReplaceFunction.MarkdownToHtml:
                        ChkDisabled.Visibility = Visibility.Visible;
                        break;

                    case ReplaceFunction.IonicFormSnippets:
                    case ReplaceFunction.IonicFormSnippetsFromInterface:
                        LblFormName.Visibility = Visibility.Visible;
                        TxtFormName.Visibility = Visibility.Visible;
                        break;

                    case ReplaceFunction.Format:
                        ChkBrassBracket.Visibility = Visibility.Visible;
                        LblPrefix.Visibility = Visibility.Visible;
                        TxtPrefix.Visibility = Visibility.Visible;
                        break;

                    default:
                        break;
                }
            }
            else
            {
                LblNotice.Content = string.Empty;
            }
        }

        /// <summary>
        /// 觀看範例
        /// </summary>
        private void BtnSample_Click(object sender, RoutedEventArgs e)
        {
            ReplaceClick(isLoadSample: true);
        }

        /// <summary>
        /// 執行置換
        /// </summary>
        private void BtnExecute_Click(object sender, RoutedEventArgs e)
        {
            ReplaceClick(isLoadSample: false);
        }

        /// <summary>
        /// 清除
        /// </summary>
        private void BtnClear_Click(object sender, RoutedEventArgs e)
        {
            ClearView();
        }

        /// <summary>
        /// 開啟資料夾
        /// </summary>
        private void BtnOpenFolder_Click(object sender, RoutedEventArgs e)
        {
            FileUtility.Explorer(this.FolderName);
        }

        //-----------------------------使用者自訂函式-----------------------------//

        /// <summary>
        /// 載入功能選單
        /// </summary>
        private void LoadMenu()
        {
            CbxFunction.BindMenu(ReplaceFunctionExtend.GetMenuList());
            CbxTableFunction.BindMenu(TableFunctionExtend.GetMenuList());
        }

        /// <summary>
        /// 清除畫面控件
        /// </summary>
        private void ClearView()
        {
            ChkTrim.IsChecked = true;
            ChkGenerateClass.IsChecked = true;

            TxtSample.Text = string.Empty;
            TxtInput.Text = string.Empty;
            TxtOutput.Text = string.Empty;
            LblNotice.Content = "說明";
        }

        /// <summary>
        /// 隱藏附加條件
        /// </summary>
        private void HiddenCondition()
        {
            ChkTrim.Visibility = Visibility.Collapsed;
            ChkGenerateClass.Visibility = Visibility.Collapsed;
            ChkDisabled.Visibility = Visibility.Collapsed;
            LblFormName.Visibility = Visibility.Collapsed;
            TxtFormName.Visibility = Visibility.Collapsed;

            ChkBrassBracket.Visibility = Visibility.Collapsed;
            LblPrefix.Visibility = Visibility.Collapsed;
            TxtPrefix.Visibility = Visibility.Collapsed;
        }

        /// <summary>
        /// 點擊執行置換
        /// </summary>
        private void ReplaceClick(bool isLoadSample = false)
        {
            if (!isLoadSample && string.IsNullOrWhiteSpace(TxtInput.Text.Trim()))
            {
                // 不載入範例又沒有輸入內容
                System.Windows.Forms.MessageBox.Show("請輸入要置換的內容");
                return;
            }

            Config102 config = GetConfig(isLoadSample: isLoadSample);

            ReplaceFunctionExtend.Config(config);

            if (config.IsExecute)
            {
                ReplaceExecute(config);
            }
        }

        /// <summary>
        /// 取得組態檔
        /// </summary>
        /// <returns></returns>
        private Config102 GetConfig(bool isLoadSample = false)
        {
            return new Config102
            {
                ServerName = TxtServer.StringValue(),
                UserId = TxtUid.StringValue(),
                Password = TxtPwd.StringValue(),
                DatabaseName = CbxDatabase.StringValue(),
                TableName = CbxTable.StringValue(),
                NameSpace = TxtNameSpace.StringValue(),
                TableFunctionType = (TableFunction)CbxTableFunction.SelectedIntValue(),

                ReplaceType = (ReplaceFunction)CbxFunction.SelectedIntValue(),
                IsExecute = true,
                IsLoadSample = isLoadSample,
                IsCheckTrim = ChkTrim.BoolValue(),
                IsGenerateClass = ChkGenerateClass.BoolValue(),
                IsDisabled = ChkDisabled.BoolValue(),
                FormName = TxtFormName.StringValue(),

                IsCheckFormat = ChkBrassBracket.BoolValue(),
                DefaultValue = TxtPrefix.StringValue(),
            };
        }

        /// <summary>
        /// 轉換配置
        /// </summary>
        private void ReplaceExecute(Config102 config)
        {
            if (config.IsLoadSample && string.IsNullOrWhiteSpace(config.SampleFile))
            {
                System.Windows.Forms.MessageBox.Show("未設定範例檔");
                return;
            }

            if (config.Transform == null)
            {
                System.Windows.Forms.MessageBox.Show("未設定轉換函式");
                return;
            }

            if (string.IsNullOrWhiteSpace(config.OutputFile))
            {
                config.OutputFile = config.SampleFile;
            }

            if (config.IsLoadSample)
            {
                TxtSample.Text = ConfigUtility.SampleFileContents("Win10200", config.SampleFile);
                TxtInput.Text = TxtSample.Text;
            }

            string inputContents = TxtInput.Text;
            string outputContents = config.Transform(inputContents, config);

            TxtOutput.Text = outputContents;
            if (string.IsNullOrWhiteSpace(config.OutputFile))
            {
                System.Windows.Forms.MessageBox.Show("未設定輸出檔名稱");
            }
            else
            {
                FileUtility.WriteFile(this.FolderName, config.OutputFile, outputContents);
            }
        }

        #region Database

        /// <summary>
        /// 執行 Table Function
        /// </summary>
        private void ExecuteTableFunction()
        {
            var config = GetConfig();
            var wModel = TransformTo401(config);
            if (!wModel.IsValid)
            {
                System.Windows.Forms.MessageBox.Show("資料有誤，請重新檢查");
                return;
            }

            var isExecute = true;
            var sModel = Win401Service.CreateSchemaModel(wModel);
            switch (config.TableFunctionType)
            {
                case TableFunction.GenerateEntity:
                    GenerateEntity(wModel, sModel);
                    break;

                case TableFunction.GenerateColumns:
                    GenerateColumns(wModel, sModel);
                    break;

                case TableFunction.SelectScript:
                    GenerateSelectScript(wModel, sModel);
                    break;

                default:
                    isExecute = false;
                    System.Windows.Forms.MessageBox.Show("請選擇要執行的 Table Function");
                    break;
            }

            if (isExecute)
            {
                FileUtility.WriteFile(this.FolderName, sModel.FileName, sModel.FileContent);
                TxtSample.Text = sModel.FileContent;
                TxtInput.Text = sModel.FileContent;
            }
        }

        /// <summary>
        /// 產生 Entity
        /// </summary>
        private void GenerateEntity(Win401Model wModel, SchemaModel sModel)
        {
            var eModel = Win401Service.NewEntityModel(wModel);

            sModel.FileName = $"{sModel.MainName}.cs";
            Win401EntityService.GenerateContent(sModel, eModel);
        }

        #region Columns

        /// <summary>
        /// 產生欄位清單
        /// </summary>
        private void GenerateColumns(Win401Model wModel, SchemaModel sModel)
        {
            sModel.FileName = $"{sModel.MainName}_Columns.txt";
            Service102.GenerateColumns(sModel);
        }

        #endregion Columns

        /// <summary>
        /// 產生查詢指令
        /// </summary>
        private void GenerateSelectScript(Win401Model wModel, SchemaModel sModel)
        {
            sModel.FileName = $"{sModel.MainName}_SelectScriipt.txt";
            Service102.GenerateSelectScript(sModel);
        }

        /// <summary>
        /// 產生 Entity
        /// </summary>
        private Win401Model TransformTo401(Config102 config)
        {
            var result = new Win401Model
            {
                ConnectionString = ServiceUtility.GetConnectionString(config),
                TableName = config.TableName,
                FolderName = this.FolderName,
                NameSpaceName = config.NameSpace,
                MainName = config.TableName.Replace("_", string.Empty),
            };

            result.IsEasyName = false;

            if (string.IsNullOrWhiteSpace(result.ConnectionString)
                || string.IsNullOrWhiteSpace(result.TableName)
                || string.IsNullOrWhiteSpace(result.FolderName))
            {
                System.Windows.Forms.MessageBox.Show("資料設定有誤，請重新設定");
                result.IsValid = false;
            }
            else
            {
                result.IsValid = true;
            }
            return result;
        }

        #endregion Database
    }
}