﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using WorkHelp.Wpf.Models.Windows;
using WorkHelp.Wpf.Utility;

namespace WorkHelp.Wpf.Views
{
    /// <summary>
    /// EPub 處理
    /// </summary>
    public partial class Win10400 : BaseWindow
    {
        //-----------------------------系統原生函式------------------------------//
        public Win10400()
        {
            InitializeComponent();
            this.Loaded += this.Window_Loaded;

            this.KeyDown += base.FWindow_KeyDown;
            this.BtnClose.Click += base.FWindowClose_Click;

            BtnSaveSetting.Click += BtnSaveSetting_Click;

            BtnExecute.Click += BtnExecute_Click;
            BtnClear.Click += BtnClear_Click;
        }

        /// <summary>
        /// 視窗載入
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Config104 config = ConfigUtility.Read<Config104>();
            TxtEPubName.Text = config.EpubName;
            TxtEPubFolder.Text = config.EpubPath;
            TxtPhotoFolder.Text = config.PhotoPath;
            TxtExtend.Text = config.ExtendName;
        }

        /// <summary>
        /// 儲存組態
        /// </summary>
        private void BtnSaveSetting_Click(object sender, RoutedEventArgs e)
        {
            ConfigUtility.Write(GetConfig());
        }

        /// <summary>
        /// 清除
        /// </summary>
        private void BtnClear_Click(object sender, RoutedEventArgs e)
        {
            TxtPhotoFolder.Text = string.Empty;
            TxtEPubFolder.Text = string.Empty;
            TxtOutput.Text = string.Empty;
        }

        /// <summary>
        /// 產生 Markdown 內容
        /// </summary>
        private void BtnExecute_Click(object sender, RoutedEventArgs e)
        {
            Execute();
        }

        //-----------------------------使用者自訂函式-----------------------------//

        /// <summary>
        /// 將 UI 輸入控件資料轉為組態物件
        /// </summary>
        /// <returns></returns>
        private Config104 GetConfig()
        {
            return new Config104
            {
                EpubName = TxtEPubName.Text.Trim(),
                EpubPath = TxtEPubFolder.Text.Trim(),
                PhotoPath = TxtPhotoFolder.Text.Trim(),
                ExtendName = TxtExtend.Text.Trim()
            };
        }

        private void Execute()
        {
            string photoFolder = TxtPhotoFolder.Text.Trim();
            string ePubFolder = TxtEPubFolder.Text.Trim();
            string newPhotoFolder = photoFolder + "_NEW";

            if (string.IsNullOrWhiteSpace(photoFolder) || string.IsNullOrWhiteSpace(ePubFolder))
            {
                System.Windows.Forms.MessageBox.Show("請輸入路徑!!");
                return;
            }
            if (!Directory.Exists(photoFolder) || !Directory.Exists(ePubFolder))
            {
                System.Windows.Forms.MessageBox.Show("資料夾不存在!!");
                return;
            }

            if (Directory.Exists(newPhotoFolder))
            {
                System.Windows.Forms.MessageBox.Show("請手動刪除圖片暫存資料夾!!");
                return;
            }

            List<string> names = new List<string>();

            EpubItem root = new EpubItem();
            root.IsValid = true;
            root.Level = 0;
            root.RootPath = ePubFolder;
            root.Title = "ROOT";

            root.IsFile = false;
            root.OriginPath = photoFolder;
            root.NewPath = newPhotoFolder;
            root.Items = new List<EpubItem>();
            root.OriginDirectoryInfo = new DirectoryInfo(photoFolder);

            SetEpubItems(root, names);

            if (root.Items.Count == 0)
            {
                System.Windows.Forms.MessageBox.Show("該資料夾沒有任何檔案!!");
            }
            else
            {
                WriteEpub(ePubFolder, root.Items);
                System.Windows.Forms.MessageBox.Show("Finished!!");
            }
        }

        private void SetEpubItems(EpubItem parent, List<string> names)
        {
            if (!Directory.Exists(parent.NewPath))
            {
                Directory.CreateDirectory(parent.NewPath);
            }

            IOrderedEnumerable<EpubItem> files = GetFiles(parent, names);
            parent.Items.AddRange(files);

            DirectoryInfo[] dirs = parent.OriginDirectoryInfo.GetDirectories();
            if (dirs != null && dirs.Length > 0)
            {
                int i = 1;
                foreach (DirectoryInfo dir in dirs)
                {
                    EpubItem item = GetEpubDirItem(parent, names, dir, i);
                    parent.Items.Add(item);
                    SetEpubItems(item, names);
                    i++;
                }
            }
        }

        private IOrderedEnumerable<EpubItem> GetFiles(EpubItem parent, List<string> names)
        {
            List<EpubItem> result = new List<EpubItem>();
            FileInfo[] files = parent.OriginDirectoryInfo.GetFiles();
            if (files != null && files.Length > 0)
            {
                foreach (FileInfo file in files)
                {
                    EpubItem item = GetEpubFileItem(parent, names, file);
                    parent.Items.Add(item);
                }
            }

            return result.OrderBy(x => x.Name);
        }

        private EpubItem GetEpubFileItem(EpubItem parent, List<string> names, FileInfo file)
        {
            string extend = TxtExtend.Text.Trim().ToUpper().TrimStart('.');

            EpubItem item = new EpubItem();
            (bool isValid, string id) = GetUniqueId(names);

            item.Id = id;
            item.IsValid = isValid;
            item.Level = parent.Level + 1;
            item.RootPath = parent.RootPath;
            item.Name = file.Name;
            item.Title = FixTileLength(GetPhotoTitle(parent.Name, file.Name));
            item.IsFile = true;
            item.NewName = $"{item.Id}.{extend}";
            item.NewFullFile = $"{parent.NewPath}\\{item.NewName}";

            if (!item.IsValid)
            {
                item.Message = "檔案取得唯一名稱失敗";
            }
            else
            {
                file.CopyTo(item.NewFullFile);
                if (!File.Exists(item.NewFullFile))
                {
                    item.Message = "檔案複製失敗";
                }
                else
                {
                    item.EpubUrl = GetEpubUrl(item);
                }
            }
            return item;
        }

        private EpubItem GetEpubDirItem(EpubItem parent, List<string> names, DirectoryInfo dir, int i)
        {
            (bool isValid, string id) = GetUniqueId(names);
            string newfolder = "A" + i.ToString().PadLeft(5, '0');

            EpubItem item = new EpubItem();
            item.Id = id;
            item.IsValid = isValid;
            item.Level = parent.Level + 1;
            item.RootPath = parent.RootPath;
            item.Title = dir.Name;
            item.Name = dir.Name;

            item.IsFile = false;
            item.OriginPath = dir.FullName;
            item.NewPath = $"{parent.NewPath}\\{newfolder}";
            item.Items = new List<EpubItem>();
            item.OriginDirectoryInfo = dir;

            if (!item.IsValid)
            {
                item.Message = "資料夾取得唯一名稱失敗";
            }
            return item;
        }

        private string GetEpubUrl(EpubItem item)
        {
            return item.NewFullFile.Replace(item.RootPath, string.Empty).TrimStart('\\').Replace("\\", "/");
        }

        private (bool isValid, string id) GetUniqueId(List<string> names, int maxTryCount = 3, int currentTryCount = 0)
        {
            if (currentTryCount >= maxTryCount)
            {
                return (false, string.Empty);
            }
            else
            {
                string name = System.Guid.NewGuid().ToString();
                if (names.Count > 0 && Name.Contains(name))
                {
                    return GetUniqueId(names, maxTryCount, currentTryCount + 1);
                }
                else
                {
                    names.Add(name);
                    return (true, name);
                }
            }
        }

        private void WriteEpub(string folder, List<EpubItem> list)
        {
            string name = TxtEPubName.Text.Trim();
            string contents = GetEpubContents(list);
            File.WriteAllText($"{folder}\\{name}", contents, Encoding.UTF8);

            TxtOutput.Text = $"pandoc {name} -o {name.ToLower().Replace("md", "epub")} --toc";
        }

        private string GetEpubContents(List<EpubItem> list)
        {
            StringBuilder builder = new StringBuilder(4096 * 8);
            builder.AppendLine("% 潔客幫 APP");
            builder.AppendLine("% " + DateTime.Now.ToString("yyyy/MM/dd"));
            builder.AppendLine();

            AppendFile(builder, list);

            return builder.ToString();
        }

        public void AppendFile(StringBuilder builder, List<EpubItem> list)
        {
            foreach (EpubItem node in list)
            {
                if (node.IsFile)
                {
                    builder.AppendLine($"{node.Title}");
                    builder.AppendLine($"![]({node.EpubUrl})");
                    builder.AppendLine();
                }
                else
                {
                    if (node.Items != null && node.Items.Count > 0)
                    {
                        builder.AppendLine();
                        builder.AppendLine(GetLevelTitle(node.Level, node.Title));
                        builder.AppendLine();
                        AppendFile(builder, node.Items);
                    }
                }
            }
        }

        public string GetLevelTitle(int level, string title)
        {
            switch (level)
            {
                case 1: return $"# {title} #";
                case 2: return $"## {title} ##";
                case 3: return $"### {title} ###";
                case 4: return $"#### {title} ####";
                case 5: return $"##### {title} #####";
                default: return title;
            }
        }

        public string GetPhotoTitle(string title, string value)
        {
            string extend = "." + TxtExtend.Text.Trim().ToUpper().TrimStart('.');
            return value.ToUpper().Replace(title + "-", string.Empty).Replace(extend, string.Empty);
        }

        public string FixTileLength(string value)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                return "                      ";
            }
            else
            {
                return value.PadRight(20, ' ');
            }
        }
    }
}