﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using WorkHelp.Utilities;
using WorkHelp.Wpf.Models;
using WorkHelp.Wpf.Services;

namespace WorkHelp.Wpf.Views
{
    /// <summary>
    /// 單檔開發
    /// </summary>
    public partial class Win40100 : BaseWindow
    {
        /// <summary>
        /// 組態檔 Section Name
        /// </summary>
        public string IniSection => "Win40100";

        /// <summary>
        /// 建構元
        /// </summary>
        public Win40100()
        {
            InitializeComponent();
            BindEvent();
        }

        /// <summary>
        /// 綁定控件事件
        /// </summary>
        private void BindEvent()
        {
            this.Loaded += this.Window_Loaded;
            this.KeyDown += base.FWindow_KeyDown;
            this.btnClose.Click += base.FWindowClose_Click;

            this.btnSetting.Click += this.btnSetting_Click;
            this.btnFolder.Click += this.btnFolder_Click;

            this.btnDB.Click += this.btnDB_Click;
            this.btnTable.Click += this.btnTable_Click;

            this.btnEntityModel.Click += this.btnEntityModel_Click;
            this.btnProtobuf.Click += this.btnProtobuf_Click;
            this.btnAllScripts.Click += this.btnAllScripts_Click;

            this.cbxTable.SelectionChanged += this.cbxTable_SelectionChanged;
        }

        /// <summary>
        /// 視窗載入
        /// </summary>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            txtServer.Text = IniUtility.ReadStr(IniSection, "ServerName");
            txtUID.Text = IniUtility.ReadStr(IniSection, "UID");
            txtPWD.Text = IniUtility.ReadStr(IniSection, "PWD");

            string DB = IniUtility.ReadStr(IniSection, "Database");
            cbxDB.Items.Clear();
            cbxDB.Items.Add(DB);
            cbxDB.Text = DB;

            string tableName = IniUtility.ReadStr(IniSection, "TableName");
            cbxTable.Items.Clear();
            cbxTable.Items.Add(tableName);
            cbxTable.Text = tableName;

            txtNameSpace.Text = IniUtility.ReadStr(IniSection, "Project");
            txtFolder.Text = IniUtility.ReadStr(IniSection, "Folder");
            txtCtrlName.Text = IniUtility.ReadStr(IniSection, "Controller");
            txtMainName.Text = IniUtility.ReadStr(IniSection, "MainName");
            txtMainRouteName.Text = IniUtility.ReadStr(IniSection, "MainRouteName");
            txtSubRouteName.Text = IniUtility.ReadStr(IniSection, "SubRouteName");

            chkEasyName.IsChecked = IniUtility.ReadBool(IniSection, "EasyName");
        }

        private void btnSetting_Click(object sender, RoutedEventArgs e)
        {
            IniUtility.WriteStr(IniSection, "ServerName", txtServer.Text.Trim());
            IniUtility.WriteStr(IniSection, "UID", txtUID.Text.Trim());
            IniUtility.WriteStr(IniSection, "PWD", txtPWD.Text.Trim());
            IniUtility.WriteStr(IniSection, "Database", cbxDB.Text.Trim());
            IniUtility.WriteStr(IniSection, "TableName", cbxTable.Text.Trim());

            IniUtility.WriteStr(IniSection, "Project", txtNameSpace.Text.Trim());
            IniUtility.WriteStr(IniSection, "Folder", txtFolder.Text.Trim());
            IniUtility.WriteStr(IniSection, "Controller", txtCtrlName.Text.Trim());
            IniUtility.WriteStr(IniSection, "MainName", txtMainName.Text.Trim());
            IniUtility.WriteStr(IniSection, "MainRouteName", txtMainRouteName.Text.Trim());
            IniUtility.WriteStr(IniSection, "SubRouteName", txtSubRouteName.Text.Trim());

            if (chkEasyName.IsChecked.HasValue)
            {
                IniUtility.WriteStr(IniSection, "EasyName", chkEasyName.IsChecked.ToString().ToLower());
            }

            MessageBox.Show("儲存成功!!");
        }

        /// <summary>
        /// 資料表變更
        /// </summary>
        private void cbxTable_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string table = cbxTable.SelectedItem?.ToString();
            if (string.IsNullOrWhiteSpace(table))
            {
                return;
            }

            if (table.Contains("ProgramA"))
            {
                table = table.Replace("ProgramA", "Program");
            }

            table = table.Replace("_", "");

            List<string> list = new List<string>();
            char[] args = table.ToCharArray();
            string piece = string.Empty;
            foreach (char c in args)
            {
                if (char.IsUpper(c))
                {
                    if (!string.IsNullOrWhiteSpace(piece))
                    {
                        list.Add(piece);
                    }
                    piece = c.ToString().ToLower();
                }
                else
                {
                    piece += c.ToString().ToLower();
                }
            }

            if (!string.IsNullOrWhiteSpace(piece))
            {
                list.Add(piece);
            }

            string controller = string.Empty;
            string mainRoute = "api";
            string subRoute = string.Empty;
            int lastIdx = list.Count - 1;
            for (int i = 0; i < list.Count; i++)
            {
                string str = list[i];
                if (i == 0)
                {
                    controller = str.Substring(0, 1).ToUpper() + str.Substring(1, str.Length - 1);
                }

                if (i == lastIdx)
                {
                    subRoute = list[i];
                }
                else
                {
                    mainRoute += "/" + list[i];
                }
            }

            txtMainName.Text = table;
            txtCtrlName.Text = controller;
            txtMainRouteName.Text = mainRoute;
            txtSubRouteName.Text = subRoute;
            txtFolder.Text = $"ApiScripts\\{table}";
        }

        private void btnDB_Click(object sender, RoutedEventArgs e)
        {
            cbxDB.Items.Clear();
            var connString = GetConnectionString(isMaster: false);
            var source = Win401Service.GetDBList(connString);
            var list = source.Select(x => x.name).ToList();
            SetItems(cbxDB, list);
        }

        private void btnTable_Click(object sender, RoutedEventArgs e)
        {
            cbxTable.Items.Clear();
            var connString = GetConnectionString(isMaster: false);
            var source = Win401Service.GetTableList(connString);
            var list = source.Select(x => x.name).ToList();
            SetItems(cbxTable, list);
        }

        private void btnEntityModel_Click(object sender, RoutedEventArgs e)
        {
            txtFolder.Text = "Entities";
            var wModel = GetWindowModel();
            if (wModel.IsValid)
            {
                var sModel = Win401Service.CreateSchemaModel(wModel);
                var eModel = Win401Service.NewEntityModel(wModel);
                GenerateEntity(sModel, eModel);
                System.Windows.Forms.MessageBox.Show("Entity Model Finished!!");
            }
        }

        private void btnProtobuf_Click(object sender, RoutedEventArgs e)
        {
            txtFolder.Text = "Protos";
            var wModel = GetWindowModel();
            if (wModel.IsValid)
            {
                var sModel = Win401Service.CreateSchemaModel(wModel);
                var pModel = Win401Service.NewProtobufModelModel(wModel);
                GenerateProtobuf(sModel, pModel);
                System.Windows.Forms.MessageBox.Show("Protobuf Model Finished!!");
            }
        }

        private void btnAllScripts_Click(object sender, RoutedEventArgs e)
        {
            var wModel = GetWindowModel();
            if (wModel.IsValid)
            {
                wModel.FolderName = $"ApiScripts\\{wModel.MainName}";
                txtFolder.Text = wModel.FolderName;

                var sModel = Win401Service.CreateSchemaModel(wModel);
                var eModel = Win401Service.NewEntityModel(wModel);
                var pModel = Win401Service.NewProtobufModelModel(wModel);

                GenerateEntity(sModel, eModel);
                GenerateProtobuf(sModel, pModel);
                GenerateMapper(sModel, eModel);
                GenerateDbContext(sModel, eModel);
                GenerateRepositoryInterface(sModel, eModel);
                GenerateRepository(sModel, eModel);
                GenerateServiceInterface(sModel, eModel);
                GenerateService(sModel, eModel);
                GenerateTestService(sModel, eModel);
                GenerateControllerService(sModel, eModel);
                GenerateColumnService(sModel, eModel);
                GenerateWpfServiceInterface(sModel, eModel);
                GenerateWpfService(sModel, eModel);
                GenerateWpfTestService(sModel, eModel);

                GenerateFacadeService(sModel, eModel);

                GenerateSqlService(sModel, eModel);
                GenerateSqlRepositoryServiceInterface(sModel, eModel);
                GenerateSqlRepositoryService(sModel, eModel);

                GenerateAras(sModel, eModel);

                System.Windows.Forms.MessageBox.Show("Api All Scripts Finished!!");
            }
        }

        private void btnFolder_Click(object sender, RoutedEventArgs e)
        {
            var folder = txtFolder.Text.Trim();
            if (string.IsNullOrWhiteSpace(folder))
            {
                folder = "Entities";
            }

            FileUtility.Explorer(folder);
        }

        // ======================================================== //
        // ======================================================== //
        // ======================================================== //

        private Win401Model GetWindowModel()
        {
            var result = new Win401Model
            {
                ConnectionString = GetConnectionString(isMaster: false),
                TableName = cbxTable.Text.Trim(),
                FolderName = txtFolder.Text.Trim(),
                MainName = cbxTable.Text.Trim().Replace("_", string.Empty),
                ControllerName = txtCtrlName.Text.Trim(),
                MainRouteName = txtMainRouteName.Text.Trim(),
                SubRouteName = txtSubRouteName.Text.Trim(),
            };

            var mainName = txtMainName.Text.Trim();
            if (!string.IsNullOrWhiteSpace(mainName))
            {
                result.MainName = mainName;
            }

            if (chkEasyName.IsChecked.HasValue)
            {
                result.IsEasyName = chkEasyName.IsChecked.Value;
            }

            if (string.IsNullOrWhiteSpace(result.ConnectionString)
                || string.IsNullOrWhiteSpace(result.TableName)
                || string.IsNullOrWhiteSpace(result.FolderName)
                || string.IsNullOrWhiteSpace(result.ControllerName)
                || string.IsNullOrWhiteSpace(result.MainRouteName)
                || string.IsNullOrWhiteSpace(result.SubRouteName))
            {
                System.Windows.Forms.MessageBox.Show("資料設定有誤，請重新設定");
                result.IsValid = false;
            }
            else
            {
                result.IsValid = true;
            }
            return result;
        }

        private string GetConnectionString(bool isMaster = false)
        {
            string sSvr = txtServer.Text.Trim();
            string sUID = txtUID.Text.Trim();
            string sPWD = txtPWD.Text.Trim();
            string sDB = isMaster ? "master" : cbxDB.Text.Trim();
            return $"SERVER={sSvr};DATABASE={sDB};UID={sUID};PWD={sPWD}";
        }

        private void SetItems(ComboBox ui, List<string> list)
        {
            if (list == null || list.Count == 0)
            {
                MessageBox.Show("no data!!");
                return;
            }

            foreach (var value in list)
            {
                ui.Items.Add(value);
            }

            if (ui.Items.Count > 0) ui.Text = ui.Items[0].ToString();
        }

        private void GenerateEntity(SchemaModel sModel, EntityModel eModel)
        {
            sModel.FileName = $"{sModel.MainName}.cs";
            sModel.NameSpaceName = txtNameSpace.Text.Trim();

            Win401EntityService.GenerateContent(sModel, eModel);
            FileUtility.WriteFile(sModel.FolderName, sModel.FileName, sModel.FileContent);
        }

        private void GenerateProtobuf(SchemaModel sModel, ProtobufModel pModel)
        {
            sModel.FileName = $"{sModel.MainName}Model.proto";
            sModel.NameSpaceName = txtNameSpace.Text.Trim();
            pModel.PackageName = txtNameSpace.Text.Trim();

            Win401Service.ProtobufImportMapper(sModel, pModel);

            Win401ProtobufService.GenerateContent(sModel, pModel);
            FileUtility.WriteFile(sModel.FolderName, sModel.FileName, sModel.FileContent);
        }

        private void GenerateMapper(SchemaModel sModel, EntityModel eModel)
        {
            sModel.FileName = $"{sModel.MainName}Mapper.cs";

            Win401MapperService.GenerateContent(sModel, eModel);

            FileUtility.WriteFile(sModel.FolderName, sModel.FileName, sModel.FileContent);
        }

        private void GenerateDbContext(SchemaModel sModel, EntityModel eModel)
        {
            sModel.FileName = $"{sModel.MainName}DbContext.cs";

            Win401DbContextService.GenerateContent(sModel, eModel);

            FileUtility.WriteFile(sModel.FolderName, sModel.FileName, sModel.FileContent);
        }

        private void GenerateRepositoryInterface(SchemaModel sModel, EntityModel eModel)
        {
            sModel.FileName = $"I{sModel.MainName}Repository.cs";

            Win401RepositoryServiceInterface.GenerateContent(sModel, eModel);

            FileUtility.WriteFile(sModel.FolderName, sModel.FileName, sModel.FileContent);
        }

        private void GenerateRepository(SchemaModel sModel, EntityModel eModel)
        {
            sModel.FileName = $"{sModel.MainName}Repository.cs";

            Win401RepositoryService.GenerateContent(sModel, eModel);

            FileUtility.WriteFile(sModel.FolderName, sModel.FileName, sModel.FileContent);
        }

        private void GenerateServiceInterface(SchemaModel sModel, EntityModel eModel)
        {
            sModel.FileName = $"I{sModel.MainName}Service.cs";

            Win401ServiceServiceInterface.GenerateContent(sModel, eModel);

            FileUtility.WriteFile(sModel.FolderName, sModel.FileName, sModel.FileContent);
        }

        private void GenerateService(SchemaModel sModel, EntityModel eModel)
        {
            sModel.FileName = $"{sModel.MainName}Service.cs";

            Win401ServiceService.GenerateContent(sModel, eModel);

            FileUtility.WriteFile(sModel.FolderName, sModel.FileName, sModel.FileContent);
        }

        private void GenerateTestService(SchemaModel sModel, EntityModel eModel)
        {
            sModel.FileName = $"I{sModel.MainName}ServiceTests.cs";

            Win401TestService.GenerateContent(sModel, eModel);

            FileUtility.WriteFile(sModel.FolderName, sModel.FileName, sModel.FileContent);
        }

        private void GenerateControllerService(SchemaModel sModel, EntityModel eModel)
        {
            sModel.FileName = $"{sModel.MainName}Controller.cs";

            Win401ControllerService.GenerateContent(sModel, eModel);

            FileUtility.WriteFile(sModel.FolderName, sModel.FileName, sModel.FileContent);
        }

        private void GenerateColumnService(SchemaModel sModel, EntityModel eModel)
        {
            sModel.FileName = $"{sModel.MainName}UtilityPieces.cs";

            Win401ColumnService.GenerateContent(sModel, eModel);

            FileUtility.WriteFile(sModel.FolderName, sModel.FileName, sModel.FileContent);
        }

        private void GenerateWpfServiceInterface(SchemaModel sModel, EntityModel eModel)
        {
            sModel.FileName = $"I{sModel.MainName}Service.cs";

            Win401WpfServiceInterface.GenerateContent(sModel, eModel);

            FileUtility.WriteFile(sModel.FolderName + "\\Wpf", sModel.FileName, sModel.FileContent);
        }

        private void GenerateWpfService(SchemaModel sModel, EntityModel eModel)
        {
            sModel.FileName = $"{sModel.MainName}Service.cs";

            Win401WpfService.GenerateContent(sModel, eModel);

            FileUtility.WriteFile(sModel.FolderName + "\\Wpf", sModel.FileName, sModel.FileContent);
        }

        private void GenerateWpfTestService(SchemaModel sModel, EntityModel eModel)
        {
            sModel.FileName = $"I{sModel.MainName}ServiceTests.cs";

            Win401WpfTestService.GenerateContent(sModel, eModel);

            FileUtility.WriteFile(sModel.FolderName + "\\Wpf", sModel.FileName, sModel.FileContent);
        }

        private void GenerateFacadeService(SchemaModel sModel, EntityModel eModel)
        {
            StringBuilder builder = new StringBuilder(256);

            builder.AppendLine("        /// <summary>");
            builder.AppendLine($"        /// {sModel.TableDescription}服務");
            builder.AppendLine("        /// </summary>");
            builder.AppendLine($"        public I{sModel.MainName}Service {sModel.MainName}Service " + "{ get; set; }");

            FileUtility.AppendFile(sModel.FolderName, $"{sModel.ControllerName}Facade.cs", builder.ToString());
        }

        private void GenerateSqlService(SchemaModel sModel, EntityModel eModel)
        {
            sModel.FileName = $"{sModel.MainName}.sql";

            Win401SqlService.GenerateContent(sModel, eModel);

            FileUtility.WriteFile(sModel.FolderName + "\\Sql", sModel.FileName, sModel.FileContent);
        }

        private void GenerateSqlRepositoryServiceInterface(SchemaModel sModel, EntityModel eModel)
        {
            sModel.FileName = $"I{sModel.MainName}Repository.cs";

            Win401SqlRepositoryServiceInterface.GenerateContent(sModel, eModel);

            FileUtility.WriteFile(sModel.FolderName + "\\Sql", sModel.FileName, sModel.FileContent);
        }

        private void GenerateSqlRepositoryService(SchemaModel sModel, EntityModel eModel)
        {
            sModel.FileName = $"{sModel.MainName}Repository.cs";

            Win401SqlRepositoryService.GenerateContent(sModel, eModel);

            FileUtility.WriteFile(sModel.FolderName + "\\Sql", sModel.FileName, sModel.FileContent);
        }

        private void GenerateAras(SchemaModel sModel, EntityModel eModel)
        {
            sModel.FileName = $"{sModel.MainName}.cs";

            Win401ArasService.GenerateContent(sModel, eModel);

            FileUtility.WriteFile(sModel.FolderName + "\\Aras", sModel.FileName, sModel.FileContent);
        }

        
    }
}