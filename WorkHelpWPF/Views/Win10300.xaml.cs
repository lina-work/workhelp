﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Forms;

namespace WorkHelp.Wpf.Views
{
    /// <summary>
    /// YAML 處理
    /// </summary>
    public partial class Win10300 : BaseWindow
    {
        //-----------------------------系統原生函式------------------------------//
        public Win10300()
        {
            InitializeComponent();
            this.BtnClose.Click += base.FWindowClose_Click;
            this.KeyDown += base.FWindow_KeyDown;

            BtnSample.Click += BtnSample_Click;
            BtnOpenFile.Click += BtnOpenFile_Click;
            BtnExecute.Click += BtnExecute_Click;
            BtnClear.Click += BtnClear_Click;

            BtnSelectFolder.Click += BtnSelectFolder_Click;
            BtnFolderTree.Click += BtnFolderTree_Click;
            BtnFolderTreeMD.Click += BtnFolderTreeMD_Click;
            BtnImportList.Click += BtnImportList_Click;

            BtnRename.Click += BtnRename_Click;
            BtnImages.Click += BtnImages_Click;
        }

        /// <summary>
        /// 組態範例
        /// </summary>
        private void BtnSample_Click(object sender, RoutedEventArgs e)
        {
            this.ExecuteSample();
        }

        /// <summary>
        /// 開啟檔案
        /// </summary>
        private void BtnOpenFile_Click(object sender, RoutedEventArgs e)
        {
            this.ExecuteOpenFile();
        }

        /// <summary>
        /// 建置資料夾
        /// </summary>
        private void BtnExecute_Click(object sender, RoutedEventArgs e)
        {
            this.Execute();
        }

        /// <summary>
        /// 清除
        /// </summary>
        private void BtnClear_Click(object sender, RoutedEventArgs e)
        {
            TxtOutput.Text = string.Empty;
        }

        /// <summary>
        /// 選取資料夾
        /// </summary>
        private void BtnSelectFolder_Click(object sender, RoutedEventArgs e)
        {
            this.SelectFolder();
        }

        /// <summary>
        /// 產生檔案樹
        /// </summary>
        private void BtnFolderTree_Click(object sender, RoutedEventArgs e)
        {
            this.FolderTree();
        }

        /// <summary>
        /// 產生檔案樹 (Markdown)
        /// </summary>
        private void BtnFolderTreeMD_Click(object sender, RoutedEventArgs e)
        {
            this.FolderTreeMarkdown();
        }

        /// <summary>
        /// 蒐集 Import
        /// </summary>
        private void BtnImportList_Click(object sender, RoutedEventArgs e)
        {
            this.ImportList();
        }

        /// <summary>
        /// 檔案重新命名
        /// </summary>
        private void BtnRename_Click(object sender, RoutedEventArgs e)
        {
            this.Rename();
        }

        /// <summary>
        /// 圖片大小
        /// </summary>
        private void BtnImages_Click(object sender, RoutedEventArgs e)
        {
            ImageInfo();
        }

        //-----------------------------使用者自訂函式-----------------------------//

        #region 圖片大小
        private void ImageInfo()
        {
            string folder = TxtFolder.Text.Trim();
            if (!System.IO.Directory.Exists(folder))
            {
                System.Windows.Forms.MessageBox.Show("資料夾不存在");
            }

            var builder = new StringBuilder();
            var dir = new System.IO.DirectoryInfo(folder);
            var files = dir.GetFiles();

            builder.AppendLine("width x height");
            foreach (var file in files)
            {
                var img = new Bitmap(file.FullName, true);
                if (img != null)
                { 
                    builder.AppendLine(file.Name + ": " + img.Width + " x " + img.Height);
                }
            }
            TxtOutput.Text = builder.ToString();
        }
        #endregion 圖片大小

        #region 建置資料夾

        /// <summary>
        /// 組態範例
        /// </summary>
        private void ExecuteSample()
        {
            StringBuilder builder = new StringBuilder(1024);

            builder.AppendLine("folders:");
            builder.AppendLine("  - 01.個人");
            builder.AppendLine("  - 02.生活:");
            builder.AppendLine("    - 001.Home");
            builder.AppendLine("    - 002.Novel");
            builder.AppendLine("    - 003.Game");
            builder.AppendLine("    - 004.Tool");
            builder.AppendLine("  - 03.隨身書櫃");
            builder.AppendLine("  - 09.租屋");
            builder.AppendLine("  - 30.書房:");
            builder.AppendLine("    - 001.博物學");
            builder.AppendLine("    - 002.文學");
            builder.AppendLine("    - 003.外語");
            builder.AppendLine("    - 004.哲學");
            builder.AppendLine("    - 005.歷史學");
            builder.AppendLine("    - 011.數學");
            builder.AppendLine("    - 012.生物學");
            builder.AppendLine("    - 013.物理學");
            builder.AppendLine("    - 014.化學");
            builder.AppendLine("    - 015.宇宙科學");
            builder.AppendLine("    - 021.經濟學");
            builder.AppendLine("    - 022.法學");
            builder.AppendLine("    - 023.人類學");
            builder.AppendLine("    - 024.宗教學");
            builder.AppendLine("    - 025.文物學");
            builder.AppendLine("    - 031.建築學");
            builder.AppendLine("    - 101.計算機: ");
            builder.AppendLine("      - 10101.編程");
            builder.AppendLine("      - 10102.作業系統");
            builder.AppendLine("      - 10103.資料結構(演算法)");
            builder.AppendLine("      - 10104.資料庫");
            builder.AppendLine("      - 10105.版本控管");
            builder.AppendLine("      - 10106.通訊協議");
            builder.AppendLine("      - 10121.視覺互動設計");
            builder.AppendLine("      - 10122.遊戲編程");
            builder.AppendLine("      - 10123.可控制編程");
            builder.AppendLine("      - 10131.Cloud");
            builder.AppendLine("      - 10141.系統設計");
            builder.AppendLine("      - 10142.負載平衡設計");
            builder.AppendLine("      - 10143.軟體思維設計");
            builder.AppendLine("      - 10151.軟體測試");
            builder.AppendLine("      - 10152.資訊安全");
            builder.AppendLine("      - 10161.網站設計框架");
            builder.AppendLine("      - 10162.網站與搜尋");
            builder.AppendLine("      - 10171.軟體文件撰寫");
            builder.AppendLine("    - 102.專案管理:");
            builder.AppendLine("      - 10201.軟體管理");
            builder.AppendLine("      - 10202.管理學");
            builder.AppendLine("    - 201.醫學");
            builder.AppendLine("    - 202.社會學");
            builder.AppendLine("    - 203.新聞採訪");
            builder.AppendLine("    - 204.旅遊荒遊");
            builder.AppendLine("    - 301.美學");
            builder.AppendLine("    - 302.音樂");
            builder.AppendLine("    - 303.繪畫");
            builder.AppendLine("    - 304.戲劇");
            builder.AppendLine("    - 305.書法");
            builder.AppendLine("    - 306.漫畫");
            builder.AppendLine("    - 307.寫作");
            builder.AppendLine("    - 308.電影");
            builder.AppendLine("    - 309.攝影");
            builder.AppendLine("    - 310.烹飪飲食");
            builder.AppendLine("    - 601.思維與學習:");
            builder.AppendLine("      - 60101.人際溝通");
            builder.AppendLine("      - 60102.職場");
            builder.AppendLine("    - 701.出版社專區");
            builder.AppendLine("    - 801.教程專區");
            builder.AppendLine("    - 901.未鑑定");
            builder.AppendLine("  - 40.社群:");
            builder.AppendLine("    - 101.工程師");
            builder.AppendLine("  - 50.書案");
            builder.AppendLine("  - 51.工作");
            builder.AppendLine("  - 52.公司");
            builder.AppendLine("  - 99.暫存");

            TxtInput.Text = builder.ToString();
        }

        /// <summary>
        /// 開啟檔案
        /// </summary>
        private void ExecuteOpenFile()
        {
            var fileContent = string.Empty;
            var filePath = string.Empty;
            var dirPath = string.Empty;

            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = "c:\\";
                openFileDialog.RestoreDirectory = false;

                if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    //Get the path of specified file
                    filePath = openFileDialog.FileName;

                    //Read the contents of the file into a stream
                    var fileStream = openFileDialog.OpenFile();

                    using (StreamReader reader = new StreamReader(fileStream))
                    {
                        fileContent = reader.ReadToEnd();
                    }

                    dirPath = System.IO.Path.GetDirectoryName(filePath);
                }
            }

            TxtPath.Text = filePath;
            TxtInput.Text = fileContent;
            TxtOutputPath.Text = dirPath;
        }

        /// <summary>
        /// 建置資料夾
        /// </summary>
        private void Execute()
        {
            string contents = TxtInput.Text;
            string rootPath = TxtOutputPath.Text;

            if (string.IsNullOrWhiteSpace(contents) || string.IsNullOrWhiteSpace(rootPath))
            {
                System.Windows.Forms.MessageBox.Show("請選取 YMAL 檔");
                return;
            }

            string[] lines = TxtInput.Text.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
            if (lines == null || lines.Length <= 1)
            {
                System.Windows.Forms.MessageBox.Show("YMAL 檔格式錯誤");
                return;
            }

            TNode tree = GetTree(lines);
            if (tree == null || tree.children == null || tree.children.Count == 0)
            {
                System.Windows.Forms.MessageBox.Show("YMAL 檔解析失敗");
                return;
            }

            tree.folder = rootPath;
            System.Windows.Forms.MessageBox.Show("檔案解析完成，開始建立資料夾...");

            CreateFolders(tree.children);
            System.Windows.Forms.MessageBox.Show("資料夾建立完成");
        }

        /// <summary>
        /// 建立樹狀結構資料夾 (遞迴)
        /// </summary>
        /// <param name="nodes">節點集</param>
        public void CreateFolders(List<TNode> nodes)
        {
            foreach (TNode node in nodes)
            {
                node.folder = $"{node.parent?.folder}\\{node.name}";
                if (!Directory.Exists(node.folder))
                {
                    Directory.CreateDirectory(node.folder);
                }
                if (node.hasChildren && node.children.Count > 0)
                {
                    CreateFolders(node.children);
                }
            }
        }

        /// <summary>
        /// 取得樹狀結構資料夾
        /// </summary>
        /// <param name="lines">YAML 組態檔</param>
        /// <returns></returns>
        public TNode GetTree(string[] lines)
        {
            TNode root = GetNode(lines[0]);
            TNode lastParentNode = root;
            for (int i = 1; i < lines.Length; i++)
            {
                string line = lines[i];
                TNode currentNode = GetNode(line);

                if (currentNode.parentLevel == lastParentNode.level)
                {
                    currentNode.parent = lastParentNode;
                    lastParentNode.children.Add(currentNode);
                }
                else
                {
                    TNode parent = GetParent(lastParentNode.parent, currentNode);
                    if (parent != null && parent.hasChildren)
                    {
                        currentNode.parent = parent;
                        parent.children.Add(currentNode);

                        lastParentNode = parent;
                    }
                }

                if (currentNode.hasChildren)
                {
                    lastParentNode = currentNode;
                }
            }

            string json = Newtonsoft.Json.JsonConvert.SerializeObject(root, Newtonsoft.Json.Formatting.Indented);
            WorkHelp.Logging.TLog.Watch(message: json);
            TxtOutput.Text = json;

            return root;
        }

        /// <summary>
        /// 取得父節點 (遞迴)
        /// </summary>
        /// <param name="forkNode">岔道節點</param>
        /// <param name="currentNode">當前節點</param>
        /// <returns></returns>
        public TNode GetParent(TNode forkNode, TNode currentNode)
        {
            if (forkNode.level == currentNode.parentLevel)
            {
                return forkNode;
            }
            else
            {
                return GetParent(forkNode.parent, currentNode);
            }
        }

        /// <summary>
        /// 取得節點
        /// </summary>
        /// <param name="comment">YAML 組態</param>
        /// <returns></returns>
        public TNode GetNode(string comment)
        {
            TNode result = new TNode();

            int count = 0;
            foreach (char c in comment)
            {
                if (c == ' ')
                {
                    count++;
                }
                else
                {
                    break;
                }
            }

            result.level = count / 2;
            result.parentLevel = result.level - 1;
            result.name = comment.Trim().TrimEnd(':').TrimStart('-').TrimStart(' ');

            if (comment.Trim().EndsWith(":"))
            {
                result.hasChildren = true;
                result.children = new List<TNode>();
            }

            return result;
        }

        /// <summary>
        /// 節點
        /// </summary>
        public class TNode
        {
            /// <summary>
            /// 階層
            /// </summary>
            public int level { get; set; }

            /// <summary>
            /// 節點名稱
            /// </summary>
            public string name { get; set; }

            /// <summary>
            /// 親節點階層
            /// </summary>
            public int parentLevel { get; set; }

            /// <summary>
            /// 親節點
            /// </summary>
            [JsonIgnore]
            public TNode parent { get; set; }

            /// <summary>
            /// 是否包含子節點
            /// </summary>
            public bool hasChildren { get; set; }

            /// <summary>
            /// 子節點列表
            /// </summary>
            public List<TNode> children { get; set; }

            /// <summary>
            ///  資料夾路徑
            /// </summary>
            public string folder { get; set; }
        }

        #endregion 建置資料夾

        #region 產生檔案樹

        /// <summary>
        /// 選取資料夾
        /// </summary>
        private void SelectFolder()
        {
            var dirPath = string.Empty;

            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (result == System.Windows.Forms.DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    dirPath = fbd.SelectedPath;
                }
            }

            TxtFolder.Text = dirPath;
        }

        /// <summary>
        /// 產生檔案樹
        /// </summary>
        private void FolderTree()
        {
            string folder = TxtFolder.Text.Trim();
            string space = TxtSpace.Text.Trim();

            if (string.IsNullOrWhiteSpace(folder)
                || !Directory.Exists(folder)
                || string.IsNullOrWhiteSpace(space)
                || !int.TryParse(space, out int spaceCount))
            {
                System.Windows.Forms.MessageBox.Show("請選取資料夾");
                return;
            }

            DirectoryInfo rootDir = new DirectoryInfo(folder);
            TDirFile rootNode = GetDirNode();

            // 結構分析
            appendDirFile(rootNode, rootDir);

            Dictionary<string, int> dictionary = new Dictionary<string, int>();
            summaryDirFile(dictionary, rootNode);

            string yamlContents = getYamlFileContents(rootNode, folder, spaceCount, dictionary);
            string pathContents = getPathFileContents(rootNode);

            CreateYamlFile(folder, yamlContents);
            CreatePathFile(folder, pathContents);

            TxtOutput.Text = yamlContents;
            System.Windows.Forms.MessageBox.Show("檔案樹建立完成");
        }

        /// <summary>
        /// 產生檔案類型統計 (遞迴)
        /// </summary>
        private void summaryDirFile(Dictionary<string, int> dictionary, TDirFile parent)
        {
            if (parent.hasChildren)
            {
                foreach (var child in parent.children)
                {
                    summaryDirFile(dictionary, child);
                }
            }
            else
            {
                string key = parent.extendName;
                if (!string.IsNullOrWhiteSpace(key))
                {
                    if (dictionary.ContainsKey(key))
                    {
                        dictionary[key] = dictionary[key] + 1;
                    }
                    else
                    {
                        dictionary.Add(key, 1);
                    }
                }
            }
        }

        /// <summary>
        /// 產生檔案樹 (Markdown)
        /// </summary>
        private void FolderTreeMarkdown()
        {
            string folder = TxtFolder.Text.Trim();
            string space = TxtSpace.Text.Trim();

            if (string.IsNullOrWhiteSpace(folder)
                || !Directory.Exists(folder)
                || string.IsNullOrWhiteSpace(space)
                || !int.TryParse(space, out int spaceCount))
            {
                System.Windows.Forms.MessageBox.Show("請選取資料夾(Markdown)");
                return;
            }

            DirectoryInfo rootDir = new DirectoryInfo(folder);
            TDirFile rootNode = GetDirNode();

            // 結構分析
            appendDirFile(rootNode, rootDir);

            string markdownContents = getMarkDownFileContents(rootNode, folder, spaceCount);

            CreateMarkdownFile(folder, markdownContents);

            TxtOutput.Text = markdownContents;
            System.Windows.Forms.MessageBox.Show("檔案樹建立完成(Markdown)");
        }

        /// <summary>
        /// 取得 YAML 樹狀結構內容
        /// </summary>
        private string getYamlFileContents(TDirFile rootNode, string folder, int spaceCount, Dictionary<string, int> dictionary)
        {
            string spaceOrigin = string.Empty.PadLeft(spaceCount, ' ');

            StringBuilder builder = new StringBuilder(2048);
            builder.AppendLine($"target path: {folder}");
            builder.AppendLine($"create time: {DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")}");
            builder.AppendLine();

            if (dictionary != null && dictionary.Count > 0)
            {
                var orderDic = dictionary.OrderBy(x => x.Key);

                int total = 0;
                builder.AppendLine($"summary: ");
                foreach (var item in orderDic)
                {
                    builder.AppendLine($"{spaceOrigin}- {item.Key}: {item.Value} 筆");
                    total += item.Value;
                }
                builder.AppendLine($"{spaceOrigin}- 總計: {total} 筆");
                builder.AppendLine();
            }

            // 轉換為 YAML 字串格式
            builder.AppendLine("document structure:");
            appendYamlTree(builder, rootNode.children, spaceOrigin);

            return builder.ToString();
        }

        /// <summary>
        /// 加入 YAML 樹狀結構
        /// </summary>
        /// <param name="builder">字串建構物件</param>
        /// <param name="nodes">節點集</param>
        /// <returns></returns>
        private void appendYamlTree(StringBuilder builder, List<TDirFile> nodes, string spaceOrigin)
        {
            if (nodes == null || nodes.Count == 0)
            {
                return;
            }

            string space = getSpace(spaceOrigin, nodes[0].level);
            foreach (TDirFile node in nodes)
            {
                if (node.children != null && node.children.Count > 0)
                {
                    builder.AppendLine($"{space}- {node.name}:");
                    appendYamlTree(builder, node.children, spaceOrigin);
                }
                else
                {
                    builder.AppendLine($"{space}- {node.name}");
                }
            }
        }

        /// <summary>
        /// 取得 Markdown 樹狀結構內容
        /// </summary>
        private string getMarkDownFileContents(TDirFile rootNode, string folder, int spaceCount)
        {
            string spaceOrigin = string.Empty.PadLeft(spaceCount, ' ');

            StringBuilder builder = new StringBuilder(2048);
            builder.AppendLine("% 書房收藏");
            builder.AppendLine("% straying");
            builder.AppendLine();

            builder.AppendLine("### 收藏資訊");
            builder.AppendLine($"* 分析時間: {DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")}");
            builder.AppendLine($"* 檔案清單");
            appendMarkdownTree(builder, rootNode.children, spaceOrigin);

            return builder.ToString();
        }

        /// <summary>
        /// 加入 Markdown 樹狀結構
        /// </summary>
        /// <param name="builder">字串建構物件</param>
        /// <param name="nodes">節點集</param>
        /// <returns></returns>
        private void appendMarkdownTree(StringBuilder builder, List<TDirFile> nodes, string spaceOrigin)
        {
            if (nodes == null || nodes.Count == 0)
            {
                return;
            }

            string space = getSpace(spaceOrigin, nodes[0].level);
            foreach (TDirFile node in nodes)
            {
                if (node.children != null && node.children.Count > 0)
                {
                    builder.AppendLine($"{space}+ {node.name}");
                    appendYamlTree(builder, node.children, spaceOrigin);
                }
                else
                {
                    builder.AppendLine($"{space}- {node.name}");
                }
            }
        }

        /// <summary>
        /// 取得間距
        /// </summary>
        private string getSpace(string space, int level)
        {
            string result = string.Empty;

            for (int i = 0; i < level; i++)
            {
                result += space;
            }
            return result;
        }

        /// <summary>
        /// 取得檔案完整路徑內容
        /// </summary>
        private string getPathFileContents(TDirFile rootNode)
        {
            StringBuilder builder = new StringBuilder(4096);
            appendPathFile(builder, rootNode.children);
            return builder.ToString();
        }

        /// <summary>
        /// 加入檔案完整路徑
        /// </summary>
        private void appendPathFile(StringBuilder builder, List<TDirFile> nodes)
        {
            if (nodes == null || nodes.Count == 0)
            {
                return;
            }

            foreach (TDirFile node in nodes)
            {
                if (node.isFolder)
                {
                    if (node.children != null && node.children.Count > 0)
                    {
                        appendPathFile(builder, node.children);
                    }
                }
                else
                {
                    builder.AppendLine($"{node.path}");
                }
            }
        }

        /// <summary>
        /// 建立 YAML 樹狀結構
        /// </summary>
        /// <param name="folder">資料夾</param>
        ///
        /// <param name="contents">YAML 內容</param>
        private void CreateYamlFile(string folder, string contents)
        {
            string name = $"{DateTime.Now.ToString("yyyyMMdd")}.文件結構.yml";
            string file = Path.Combine(folder, name);
            File.WriteAllText(file, contents, Encoding.UTF8);
        }

        /// <summary>
        /// 建立檔案完整目錄清單
        /// </summary>
        /// <param name="folder">資料夾</param>
        /// <param name="contents">內容</param>
        private void CreatePathFile(string folder, string contents)
        {
            string name = $"{DateTime.Now.ToString("yyyyMMdd")}.文件路徑.txt";
            string file = Path.Combine(folder, name);

            File.WriteAllText(file, contents, Encoding.UTF8);

            this.pathFolder = folder;
            this.pathFile = file;
        }

        /// <summary>
        /// 建立 Markdown 樹狀結構
        /// </summary>
        /// <param name="folder">資料夾</param>
        /// <param name="contents">YAML 內容</param>
        private void CreateMarkdownFile(string folder, string contents)
        {
            string name = $"{DateTime.Now.ToString("yyyyMMdd")}.書房收藏.md";
            string file = Path.Combine(folder, name);
            File.WriteAllText(file, contents, Encoding.UTF8);

            string epubFile = file.Replace(".md", ".epub");
            if (File.Exists(epubFile))
            {
                File.Delete(epubFile);
            }
        }

        /// <summary>
        /// 加入節點 (遞迴)
        /// </summary>
        /// <param name="currentNode">當前節點</param>
        /// <param name="currentDir">當前資料夾路徑</param>
        private void appendDirFile(TDirFile currentNode, DirectoryInfo currentDir)
        {
            int currentLevel = currentNode.level + 1;

            DirectoryInfo[] dirsSource = currentDir.GetDirectories();
            IOrderedEnumerable<DirectoryInfo> dirs = dirsSource.OrderBy(x => x.Name);
            foreach (DirectoryInfo dir in dirs)
            {
                TDirFile node = GetDirNode(dir, currentLevel);

                currentNode.children.Add(node);

                appendDirFile(node, dir);
            }

            FileInfo[] filesSource = currentDir.GetFiles();
            IOrderedEnumerable<FileInfo> files = filesSource.OrderBy(x => x.Name);
            foreach (FileInfo file in files)
            {
                currentNode.children.Add(GetFileNode(file, currentLevel));
            }
        }

        /// <summary>
        /// 取得資料夾節點
        /// </summary>
        /// <param name="dir">資料夾</param>
        /// <param name="level">層級</param>
        /// <returns></returns>
        private TDirFile GetDirNode(DirectoryInfo dir = null, int level = 0)
        {
            return new TDirFile
            {
                name = dir?.Name,
                isFolder = true,
                level = level,
                hasChildren = true,
                children = new List<TDirFile>(),
            };
        }

        /// <summary>
        /// 取得檔案節點
        /// </summary>
        /// <param name="file">檔案</param>
        /// <param name="level">層級</param>
        /// <returns></returns>
        private TDirFile GetFileNode(FileInfo file, int level)
        {
            TDirFile result = new TDirFile
            {
                name = file.Name,
                path = file.FullName,
                isFolder = false,
                level = level,
                hasChildren = false,
            };

            if (result.name.IndexOf('.') > -1)
            {
                string[] args = result.name.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
                if (args != null && args.Length > 0)
                {
                    string extend = args.Last();
                    if (extend.Length <= 5)
                    {
                        result.extendName = extend.ToLower();
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// 資料夾與檔案節點
        /// </summary>
        public class TDirFile
        {
            /// <summary>
            /// 完整路徑
            /// </summary>
            public string path { get; set; }

            /// <summary>
            /// 名稱
            /// </summary>
            public string name { get; set; }

            /// <summary>
            /// 描述
            /// </summary>
            public string descript { get; set; }

            /// <summary>
            /// 層級
            /// </summary>
            public int level { get; set; }

            /// <summary>
            /// 是否為資料夾
            /// </summary>
            public bool isFolder { get; set; }

            /// <summary>
            /// 是否有子節點
            /// </summary>
            public bool hasChildren { get; set; }

            /// <summary>
            /// 副檔名
            /// </summary>
            public string extendName { get; set; }

            /// <summary>
            /// 子節點
            /// </summary>
            public List<TDirFile> children { get; set; }
        }

        #endregion 產生檔案樹

        #region 蒐集 Import

        /// <summary>
        /// 蒐集 Import 使用 - 資料夾
        /// </summary>
        private string pathFolder { get; set; }

        /// <summary>
        /// 蒐集 Import 使用 - 文件路徑檔案
        /// </summary>
        private string pathFile { get; set; }

        /// <summary>
        /// 蒐集 Import
        /// </summary>
        private void ImportList()
        {
            string folder = this.pathFolder;
            string path = this.pathFile;

            if (string.IsNullOrEmpty(folder) || string.IsNullOrEmpty(path) || !File.Exists(path))
            {
                System.Windows.Forms.MessageBox.Show("請先產生文件路徑檔案");
                return;
            }

            string[] files = File.ReadAllLines(path, Encoding.UTF8);
            if (files == null || files.Length == 0)
            {
                System.Windows.Forms.MessageBox.Show("文件路徑檔案無資料");
                return;
            }

            string name = $"{DateTime.Now.ToString("yyyyMMdd")}.ImportList.txt";
            string file = Path.Combine(folder, name);
            string contents = getImportList(files).ToString();

            File.WriteAllText(file, contents, Encoding.UTF8);
            System.Windows.Forms.MessageBox.Show("ImportList 建立完成");
        }

        /// <summary>
        /// Import 清單字串
        /// </summary>
        /// <param name="files">檔案清單</param>
        /// <returns></returns>
        private StringBuilder getImportList(string[] files)
        {
            List<string> imports = new List<string>();
            IEnumerable<string> filters = files.Where(x => x.EndsWith(".ts", StringComparison.CurrentCultureIgnoreCase));

            foreach (string file in filters)
            {
                string[] rows = File.ReadAllLines(file, Encoding.UTF8);
                IEnumerable<string> lines = rows.Where(x => x.StartsWith("import { ", StringComparison.CurrentCultureIgnoreCase));
                imports.AddRange(lines);
            }

            IOrderedEnumerable<string> result = imports.Distinct().OrderBy(x => x);

            StringBuilder builder = new StringBuilder(2048);
            foreach (string import in result)
            {
                builder.AppendLine(import);
            }
            return builder;
        }

        #endregion 蒐集 Import

        /// <summary>
        /// 檔案重新命名
        /// </summary>
        private void Rename()
        {
            string folder = TxtFolder.Text.Trim();
            string prefix = TxtOutputPath.Text.Trim();

            if (string.IsNullOrWhiteSpace(folder)
                || !Directory.Exists(folder))
            {
                System.Windows.Forms.MessageBox.Show("請選取資料夾");
                return;
            }

            DirectoryInfo rootDir = new DirectoryInfo(folder);

            FileInfo[] files = rootDir.GetFiles();
            List<FileInfo> removeList = new List<FileInfo>();

            foreach (FileInfo file in files)
            {
                string name = file.FullName;
                string target = "Screenshot_";
                if (name.Contains(target))
                {
                    string newName = string.Empty;
                    if (string.IsNullOrWhiteSpace(prefix))
                    {
                        newName = file.FullName.Replace(target, string.Empty);
                    }
                    else
                    {
                        newName = file.FullName.Replace(target, prefix + "_");
                    }
                    file.CopyTo(newName);
                    removeList.Add(file);
                }
            }

            foreach (FileInfo file in removeList)
            {
                file.Delete();
            }

            System.Windows.Forms.MessageBox.Show("finished!!");
        }
    }
}