﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using WorkHelp.Utilities;
using WorkHelp.Wpf.Models;
using WorkHelp.Wpf.Services;

namespace WorkHelp.Wpf.Views
{
    /// <summary>
    /// 多檔開發
    /// </summary>
    public partial class Win40200 : BaseWindow
    {
        /// <summary>
        /// 組態檔 Section Name
        /// </summary>
        public string IniSection => "Win40100";

        /// <summary>
        /// 資料表清單
        /// </summary>
        public List<Win401Model> TableList { get; set; } = new List<Win401Model>();

        public Win40200()
        {
            InitializeComponent();
            BindEvent();
        }

        private void BindEvent()
        {
            this.Loaded += this.Window_Loaded;
            this.KeyDown += base.FWindow_KeyDown;
            this.btnClose.Click += base.FWindowClose_Click;

            this.btnSetting.Click += this.btnSetting_Click;
            this.btnFolder.Click += this.btnFolder_Click;

            this.btnDB.Click += this.btnDB_Click;
            this.btnTable.Click += this.btnTable_Click;

            this.btnAllScripts.Click += this.btnAllScripts_Click;
            this.btnDeleteFiles.Click += this.btnDeleteFiles_Click;

            this.btnClearTables.Click += this.btnClearTables_Click;
            this.btnAddTable.Click += this.btnAddTable_Click;

            this.cbxTable.SelectionChanged += this.cbxTable_SelectionChanged;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            txtServer.Text = IniUtility.ReadStr(IniSection, "ServerName");
            txtUID.Text = IniUtility.ReadStr(IniSection, "UID");
            txtPWD.Text = IniUtility.ReadStr(IniSection, "PWD");

            string DB = IniUtility.ReadStr(IniSection, "Database");
            cbxDB.Items.Clear();
            cbxDB.Items.Add(DB);
            cbxDB.Text = DB;

            string tableName = IniUtility.ReadStr(IniSection, "TableName");
            cbxTable.Items.Clear();
            cbxTable.Items.Add(tableName);
            cbxTable.Text = tableName;

            txtNameSpace.Text = IniUtility.ReadStr(IniSection, "Project");
            txtFolder.Text = IniUtility.ReadStr(IniSection, "Folder");
            txtCtrlName.Text = IniUtility.ReadStr(IniSection, "Controller");
            txtMainName.Text = IniUtility.ReadStr(IniSection, "MainName");
            txtMainRouteName.Text = IniUtility.ReadStr(IniSection, "MainRouteName");
            txtSubRouteName.Text = IniUtility.ReadStr(IniSection, "SubRouteName");

            chkEasyName.IsChecked = IniUtility.ReadBool(IniSection, "EasyName");
            chkArasMode.IsChecked = IniUtility.ReadBool(IniSection, "ArasMode");

            txtTableFilter.Text = IniUtility.ReadStr(IniSection, "TableFilter");

            TableList.Clear();
        }

        private void btnSetting_Click(object sender, RoutedEventArgs e)
        {
            IniUtility.WriteStr(IniSection, "ServerName", txtServer.Text.Trim());
            IniUtility.WriteStr(IniSection, "UID", txtUID.Text.Trim());
            IniUtility.WriteStr(IniSection, "PWD", txtPWD.Text.Trim());
            IniUtility.WriteStr(IniSection, "Database", cbxDB.Text.Trim());
            IniUtility.WriteStr(IniSection, "TableName", cbxTable.Text.Trim());

            IniUtility.WriteStr(IniSection, "Project", txtNameSpace.Text.Trim());
            IniUtility.WriteStr(IniSection, "Folder", txtFolder.Text.Trim());
            IniUtility.WriteStr(IniSection, "Controller", txtCtrlName.Text.Trim());
            IniUtility.WriteStr(IniSection, "MainName", txtMainName.Text.Trim());
            IniUtility.WriteStr(IniSection, "MainRouteName", txtMainRouteName.Text.Trim());
            IniUtility.WriteStr(IniSection, "SubRouteName", txtSubRouteName.Text.Trim());

            IniUtility.WriteStr(IniSection, "TableFilter", txtTableFilter.Text.Trim());

            if (chkEasyName.IsChecked.HasValue)
            {
                IniUtility.WriteStr(IniSection, "EasyName", chkEasyName.IsChecked.ToString().ToLower());
            }
            if (chkArasMode.IsChecked.HasValue)
            {
                IniUtility.WriteStr(IniSection, "ArasMode", chkArasMode.IsChecked.ToString().ToLower());
            }

            MessageBox.Show("儲存成功!!");
        }

        private void cbxTable_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string table = cbxTable.SelectedItem?.ToString();
            if (string.IsNullOrWhiteSpace(table))
            {
                //System.Windows.Forms.MessageBox.Show("請選擇資料表");
            }
            else
            {
                if (chkArasMode.IsChecked.HasValue && chkArasMode.IsChecked.Value)
                {
                    SetTableUIAras(table);
                }
                else
                {
                    SetTableUI(table);
                }
            }
        }

        private void btnDB_Click(object sender, RoutedEventArgs e)
        {
            cbxDB.Items.Clear();
            var connString = GetConnectionString(isMaster: false);
            var source = Win401Service.GetDBList(connString);
            var list = source.Select(x => x.name).ToList();
            SetItems(cbxDB, list);
        }

        private void btnTable_Click(object sender, RoutedEventArgs e)
        {
            cbxTable.Items.Clear();
            var connString = GetConnectionString(isMaster: false);
            var source = Win401Service.GetTableList(connString);
            var list = source.Select(x => x.name).ToList();

            var filter = txtTableFilter.Text.Trim().ToLower();
            if (string.IsNullOrWhiteSpace(filter))
            {
                SetItems(cbxTable, list);
            }
            else
            {
                var listFilter = list.Where(x => x.ToLower().Contains(filter)).ToList();
                SetItems(cbxTable, listFilter);
            }
        }

        private void btnDeleteFiles_Click(object sender, RoutedEventArgs e)
        {
            txtFolder.Text = "ApiScripts\\Multi";
            RemoveExistsFoldersAndFiles();
            System.Windows.Forms.MessageBox.Show("檔案與資料夾已全部移除");
        }

        private void btnFolder_Click(object sender, RoutedEventArgs e)
        {
            var folder = txtFolder.Text.Trim();
            if (string.IsNullOrWhiteSpace(folder))
            {
                folder = "Entities";
            }

            FileUtility.Explorer(folder);
        }

        private void btnClearTables_Click(object sender, RoutedEventArgs e)
        {
            txtTables.Text = string.Empty;
            TableList.Clear();
            System.Windows.Forms.MessageBox.Show("資料表已全部移除");
        }

        private void btnAddTable_Click(object sender, RoutedEventArgs e)
        {
            string select_table = cbxTable.Text;
            string tables = txtTables.Text;

            if (tables.Contains(select_table))
            {
                return;
            }

            var item = GetWindowModel();
            if (item.IsValid)
            {
                TableList.Add(item);
                txtTables.Text += select_table + "\r\n";
            }
        }

        private void btnAllScripts_Click(object sender, RoutedEventArgs e)
        {
            txtFolder.Text = "ApiScripts\\Multi";
            if (TableList == null || TableList.Count == 0)
            {
                var wModel = GetWindowModel();

                if (wModel.IsArasMode)
                { 
                    var dictionary = GetArasModeDictionary(wModel.TableName);
                    wModel.MainName = dictionary["MainName"];
                    wModel.ControllerName = dictionary["CtrlName"];
                    wModel.MainRouteName = dictionary["MainRouteName"];
                    wModel.SubRouteName = dictionary["SubRouteName"];
                    wModel.FolderName = dictionary["Folder"];
                }

                GenerateScripts(wModel);
                System.Windows.Forms.MessageBox.Show("Api All Scripts Finished!!");
            }
            else
            {
                TableList.ForEach(wModel =>
                {
                    GenerateScripts(wModel);
                });

                GenerateArasMain();

                System.Windows.Forms.MessageBox.Show("Api All Scripts Finished!!");
            }
        }

        private void GenerateScripts(Win401Model wModel)
        {
            if (wModel.IsValid)
            {
                var sModel = Win401Service.CreateSchemaModel(wModel);
                var eModel = Win401Service.NewEntityModel(wModel);
                var pModel = Win401Service.NewProtobufModelModel(wModel);

                GenerateEntity(sModel, eModel);
                GenerateProtobuf(sModel, pModel);
                GenerateMapper(sModel, eModel);
                GenerateDbContext(sModel, eModel);

                GenerateRepositoryInterface(sModel, eModel);
                GenerateRepository(sModel, eModel);

                GenerateServiceInterface(sModel, eModel);
                GenerateService(sModel, eModel);
                GenerateTestService(sModel, eModel);
                GenerateControllerService(sModel, eModel);
                GenerateColumnService(sModel, eModel);
                GenerateWpfServiceInterface(sModel, eModel);
                GenerateWpfService(sModel, eModel);
                GenerateWpfTestService(sModel, eModel);

                GenerateFacadeService(sModel, eModel);

                GenerateSqlService(sModel, eModel);
                GenerateSqlServiceCreateProcedure(sModel, eModel);
                GenerateSqlRepositoryServiceInterface(sModel, eModel);
                GenerateSqlRepositoryService(sModel, eModel);

                GenerateArasSelect(sModel, eModel);
                GenerateArasColumn(sModel, eModel);
            }
        }

        // ======================================================== //
        // ======================================================== //
        // ======================================================== //

        private Win401Model GetWindowModel()
        {
            var result = new Win401Model
            {
                ConnectionString = GetConnectionString(isMaster: false),
                DatabaseName = cbxDB.Text.Trim(),
                TableName = cbxTable.Text.Trim(),
                FolderName = txtFolder.Text.Trim(),
                MainName = cbxTable.Text.Trim().Replace("_", string.Empty),
                ControllerName = txtCtrlName.Text.Trim(),
                MainRouteName = txtMainRouteName.Text.Trim(),
                SubRouteName = txtSubRouteName.Text.Trim(),
            };

            var mainName = txtMainName.Text.Trim();
            if (!string.IsNullOrWhiteSpace(mainName))
            {
                result.MainName = mainName;
            }

            if (chkEasyName.IsChecked.HasValue)
            {
                result.IsEasyName = chkEasyName.IsChecked.Value;
            }

            if (chkArasMode.IsChecked.HasValue)
            {
                result.IsArasMode = chkArasMode.IsChecked.Value;
            }

            if (string.IsNullOrWhiteSpace(result.ConnectionString)
                || string.IsNullOrWhiteSpace(result.TableName)
                || string.IsNullOrWhiteSpace(result.FolderName)
                || string.IsNullOrWhiteSpace(result.ControllerName)
                || string.IsNullOrWhiteSpace(result.MainRouteName)
                || string.IsNullOrWhiteSpace(result.SubRouteName))
            {
                System.Windows.Forms.MessageBox.Show("資料設定有誤，請重新設定");
                result.IsValid = false;
            }
            else
            {
                result.IsValid = true;
            }
            return result;
        }

        private string GetConnectionString(bool isMaster = false)
        {
            string sSvr = txtServer.Text.Trim();
            string sUID = txtUID.Text.Trim();
            string sPWD = txtPWD.Text.Trim();
            string sDB = isMaster ? "master" : cbxDB.Text.Trim();
            return $"SERVER={sSvr};DATABASE={sDB};UID={sUID};PWD={sPWD}";
        }

        private void SetItems(ComboBox ui, List<string> list)
        {
            if (list == null || list.Count == 0)
            {
                MessageBox.Show("no data!!");
                return;
            }

            foreach (var value in list)
            {
                ui.Items.Add(value);
            }

            if (ui.Items.Count > 0) ui.Text = ui.Items[0].ToString();
        }

        private void SetTableUI(string table)
        {
            if (table.Contains("ProgramA"))
            {
                table = table.Replace("ProgramA", "Program");
            }

            table = table.Replace("_", "");

            List<string> list = new List<string>();
            char[] args = table.ToCharArray();
            string piece = string.Empty;
            foreach (char c in args)
            {
                if (char.IsUpper(c))
                {
                    if (!string.IsNullOrWhiteSpace(piece))
                    {
                        list.Add(piece);
                    }
                    piece = c.ToString().ToLower();
                }
                else
                {
                    piece += c.ToString().ToLower();
                }
            }

            if (!string.IsNullOrWhiteSpace(piece))
            {
                list.Add(piece);
            }

            string controller = string.Empty; ;
            string mainRoute = "api";
            string subRoute = string.Empty;
            int lastIdx = list.Count - 1;
            for (int i = 0; i < list.Count; i++)
            {
                string str = list[i];
                if (i == 0)
                {
                    controller = str.Substring(0, 1).ToUpper() + str.Substring(1, str.Length - 1);
                }

                if (i == lastIdx)
                {
                    subRoute = list[i];
                }
                else
                {
                    mainRoute += "/" + list[i];
                }
            }

            txtMainName.Text = table;
            txtCtrlName.Text = controller;
            txtMainRouteName.Text = mainRoute;
            txtSubRouteName.Text = subRoute;
            txtFolder.Text = $"ApiScripts\\{table}";
        }

        private void SetTableUIAras(string table)
        {
            var dictionary = GetArasModeDictionary(table);
            txtMainName.Text = dictionary["MainName"];
            txtCtrlName.Text = dictionary["CtrlName"];
            txtMainRouteName.Text = dictionary["MainRouteName"];
            txtSubRouteName.Text = dictionary["SubRouteName"];
            txtFolder.Text = dictionary["Folder"];
        }

        private Dictionary<string, string> GetArasModeDictionary(string table)
        {
            string newTableName = string.Empty;
            string newController = string.Empty;
            string newRoute = string.Empty;
            string newSubRoute = string.Empty;

            bool isFirst = true;
            for (int i = 0; i < table.Length; i++)
            {
                char c = table[i];

                if (c == '_')
                {
                    newTableName += c;
                    isFirst = true;
                }
                else
                {
                    if (isFirst)
                    {
                        newTableName += c.ToString().ToUpper();
                        isFirst = false;
                    }
                    else
                    {
                        newTableName += c.ToString().ToLower();
                    }
                }
            }

            string[] tags = newTableName.Split('_');
            if (tags.Length > 1)
            {
                newController = tags[1];
            }
            else
            {
                newController = newTableName.Replace("_", string.Empty);
            }

            newRoute = newController;

            if (tags.Length > 2)
            {
                newSubRoute = tags[2];
            }
            else
            {
                newSubRoute = newTableName.Replace("_", string.Empty);
            }

            Dictionary<string, string> result = new Dictionary<string, string>();
            result.Add("MainName", newTableName);
            result.Add("CtrlName", newController);
            result.Add("MainRouteName", newRoute);
            result.Add("SubRouteName", newSubRoute);
            result.Add("Folder", $"ApiScripts\\{newTableName}");

            return result;
        }

        private void GenerateProtobuf(SchemaModel sModel, ProtobufModel pModel)
        {
            sModel.FileName = $"{sModel.MainName}Model.proto";
            sModel.NameSpaceName = txtNameSpace.Text.Trim();
            pModel.PackageName = txtNameSpace.Text.Trim();

            Win401Service.ProtobufImportMapper(sModel, pModel);

            Win401ProtobufService.GenerateContent(sModel, pModel);
            FileUtility.WriteFile($"ApiScripts\\Multi\\Message\\{sModel.ControllerName}", sModel.FileName, sModel.FileContent);
        }

        private void GenerateEntity(SchemaModel sModel, EntityModel eModel)
        {
            sModel.FileName = $"{sModel.MainName}.cs";
            sModel.NameSpaceName = txtNameSpace.Text.Trim();

            Win401EntityService.GenerateContent(sModel, eModel);
            FileUtility.WriteFile($"ApiScripts\\Multi\\Api\\Entities\\{sModel.ControllerName}", sModel.FileName, sModel.FileContent);
        }

        private void GenerateMapper(SchemaModel sModel, EntityModel eModel)
        {
            sModel.FileName = $"{sModel.MainName}Mapper.cs";

            Win401MapperService.GenerateContent(sModel, eModel);

            FileUtility.WriteFile($"ApiScripts\\Multi\\Api\\Mappers\\{sModel.ControllerName}", sModel.FileName, sModel.FileContent);
        }

        private void GenerateDbContext(SchemaModel sModel, EntityModel eModel)
        {
            sModel.FileName = $"{sModel.MainName}DbContext.cs";

            Win401DbContextService.GenerateContent(sModel, eModel);

            FileUtility.WriteFile($"ApiScripts\\Multi\\Api\\Context", sModel.FileName, sModel.FileContent);
        }

        private void GenerateRepositoryInterface(SchemaModel sModel, EntityModel eModel)
        {
            sModel.FileName = $"I{sModel.MainName}Repository.cs";

            Win401RepositoryServiceInterface.GenerateContent(sModel, eModel);

            FileUtility.WriteFile($"ApiScripts\\Multi\\Api\\Repositories\\{sModel.ControllerName}", sModel.FileName, sModel.FileContent);
        }

        private void GenerateRepository(SchemaModel sModel, EntityModel eModel)
        {
            sModel.FileName = $"{sModel.MainName}Repository.cs";

            Win401RepositoryService.GenerateContent(sModel, eModel);

            FileUtility.WriteFile($"ApiScripts\\Multi\\Api\\Repositories\\{sModel.ControllerName}\\Impl", sModel.FileName, sModel.FileContent);
        }

        private void GenerateServiceInterface(SchemaModel sModel, EntityModel eModel)
        {
            sModel.FileName = $"I{sModel.MainName}Service.cs";

            Win401ServiceServiceInterface.GenerateContent(sModel, eModel);

            FileUtility.WriteFile($"ApiScripts\\Multi\\Api\\Services\\{sModel.ControllerName}", sModel.FileName, sModel.FileContent);
        }

        private void GenerateService(SchemaModel sModel, EntityModel eModel)
        {
            sModel.FileName = $"{sModel.MainName}Service.cs";

            Win401ServiceService.GenerateContent(sModel, eModel);

            FileUtility.WriteFile($"ApiScripts\\Multi\\Api\\Services\\{sModel.ControllerName}\\Impl", sModel.FileName, sModel.FileContent);
        }

        private void GenerateTestService(SchemaModel sModel, EntityModel eModel)
        {
            sModel.FileName = $"I{sModel.MainName}ServiceTests.cs";

            Win401TestService.GenerateContent(sModel, eModel);

            FileUtility.WriteFile($"ApiScripts\\Multi\\Api.Test\\Services\\{sModel.ControllerName}", sModel.FileName, sModel.FileContent);
        }

        private void GenerateControllerService(SchemaModel sModel, EntityModel eModel)
        {
            sModel.FileName = $"{sModel.MainName}Controller.cs";

            Win401ControllerService.GenerateContent(sModel, eModel);

            FileUtility.WriteFile($"ApiScripts\\Multi\\Web\\Controllers\\{sModel.ControllerName}", sModel.FileName, sModel.FileContent);
        }

        private void GenerateWpfServiceInterface(SchemaModel sModel, EntityModel eModel)
        {
            sModel.FileName = $"I{sModel.MainName}Service.cs";

            Win401WpfServiceInterface.GenerateContent(sModel, eModel);

            FileUtility.WriteFile($"ApiScripts\\Multi\\Wpf\\Services\\{sModel.ControllerName}", sModel.FileName, sModel.FileContent);
        }

        private void GenerateWpfService(SchemaModel sModel, EntityModel eModel)
        {
            sModel.FileName = $"{sModel.MainName}Service.cs";

            Win401WpfService.GenerateContent(sModel, eModel);

            FileUtility.WriteFile($"ApiScripts\\Multi\\Wpf\\Services\\{sModel.ControllerName}\\Impl", sModel.FileName, sModel.FileContent);
        }

        private void GenerateColumnService(SchemaModel sModel, EntityModel eModel)
        {
            sModel.FileName = $"{sModel.MainName}UtilityPieces.cs";

            Win401ColumnService.GenerateContent(sModel, eModel);

            FileUtility.WriteFile($"ApiScripts\\Multi\\Pieces", sModel.FileName, sModel.FileContent);
        }

        private void GenerateWpfTestService(SchemaModel sModel, EntityModel eModel)
        {
            sModel.FileName = $"I{sModel.MainName}ServiceTests.cs";

            Win401WpfTestService.GenerateContent(sModel, eModel);

            FileUtility.WriteFile($"ApiScripts\\Multi\\Wpf.Test\\Services\\{sModel.ControllerName}", sModel.FileName, sModel.FileContent);
        }

        private void GenerateFacadeService(SchemaModel sModel, EntityModel eModel)
        {
            StringBuilder builder = new StringBuilder(256);

            builder.AppendLine("        /// <summary>");
            builder.AppendLine($"        /// {sModel.TableDescription}服務");
            builder.AppendLine("        /// </summary>");
            builder.AppendLine($"        public I{sModel.MainName}Service {sModel.MainName}Service " + "{ get; set; }");

            FileUtility.AppendFile("ApiScripts\\Multi", $"{sModel.ControllerName}Facade.cs", builder.ToString());
        }

        private void GenerateSqlService(SchemaModel sModel, EntityModel eModel)
        {
            sModel.FileName = $"{sModel.MainName}.sql";

            Win401SqlService.GenerateContent(sModel, eModel);

            FileUtility.WriteFile($"ApiScripts\\Multi\\Sql", sModel.FileName, sModel.FileContent);
        }

        private void GenerateSqlServiceCreateProcedure(SchemaModel sModel, EntityModel eModel)
        {
            sModel.FileName = $"{sModel.MainName}_CreateProcedure.sql";

            Win401SqlService_CreateProcedure.GenerateContent(sModel, eModel);

            FileUtility.WriteFile($"ApiScripts\\Multi\\Sql", sModel.FileName, sModel.FileContent);
        }

        private void GenerateSqlRepositoryServiceInterface(SchemaModel sModel, EntityModel eModel)
        {
            sModel.FileName = $"I{sModel.MainName}Repository.cs";

            Win401SqlRepositoryServiceInterface.GenerateContent(sModel, eModel);

            FileUtility.WriteFile($"ApiScripts\\Multi\\Sql", sModel.FileName, sModel.FileContent);
        }

        private void GenerateSqlRepositoryService(SchemaModel sModel, EntityModel eModel)
        {
            sModel.FileName = $"{sModel.MainName}Repository.cs";

            Win401SqlRepositoryService.GenerateContent(sModel, eModel);

            FileUtility.WriteFile($"ApiScripts\\Multi\\Sql", sModel.FileName, sModel.FileContent);
        }

        private void GenerateArasMain()
        {
            string filter = txtTableFilter.Text.Trim();

            if (!string.IsNullOrWhiteSpace(filter))
            {
                List<string> list = new List<string>();
                foreach (var item in cbxTable.Items)
                {
                    list.Add(item.ToString());
                }

                string fileName = "ArasMain.sql";
                string fileContent = Win401SqlService_ArasMain.GenerateContent(list);
                FileUtility.WriteFile($"ApiScripts\\Multi\\Aras", fileName, fileContent);
            }
        }
        private void GenerateArasSelect(SchemaModel sModel, EntityModel eModel)
        {
            sModel.FileName = $"SELECT_{sModel.MainName}.sql";

            Win401SqlService_Aras.GenerateContent(sModel, eModel);

            FileUtility.WriteFile($"ApiScripts\\Multi\\Aras", sModel.FileName, sModel.FileContent);
        }
        private void GenerateArasColumn(SchemaModel sModel, EntityModel eModel)
        {
            sModel.FileName = $"COLUMN_{sModel.MainName}.sql";

            Win401SqlService_ArasColumn.GenerateContent(sModel, eModel);

            FileUtility.WriteFile($"ApiScripts\\Multi\\Aras", sModel.FileName, sModel.FileContent);
        }

        #region 刪除檔案

        private void RemoveExistsFoldersAndFiles()
        {
            RemoveExistsFiles();
            RemoveExistsFolders();
        }

        private void RemoveExistsFiles()
        {
            var files = ListFiles();
            if (files == null) return;

            foreach (FileInfo file in files)
            {
                file.Delete();
            }
        }

        private void RemoveExistsFolders()
        {
            var dirs = ListDires();
            if (dirs == null) return;

            var list = dirs.ToList();
            for (int i = list.Count - 1; i >= 0; i--)
            {
                DirectoryInfo dir = list[i];
                dir.Delete();
            }
        }

        /// <summary>
        /// 取得所有專案下的 proto 檔
        /// </summary>
        /// <param name="dir"></param>
        /// <param name="ext"></param>
        /// <returns></returns>
        private IEnumerable<FileInfo> ListFiles(DirectoryInfo dir = null)
        {
            if (dir == null)
            {
                string basePath = AppDomain.CurrentDomain.BaseDirectory;
                string tagertPath = "ApiScripts\\Multi";
                string path = System.IO.Path.Combine(basePath, tagertPath);

                if (Directory.Exists(path))
                {
                    dir = new DirectoryInfo(path);
                }
                else
                {
                    return null;
                }
            }

            List<FileInfo> files = new List<FileInfo>();

            files.AddRange(dir.GetFiles());

            // 逐層往下尋找
            foreach (DirectoryInfo subDir in dir.GetDirectories())
            {
                files.AddRange(ListFiles(subDir));
            }

            return files;
        }

        /// <summary>
        /// 取得所有專案下的 proto 檔
        /// </summary>
        /// <param name="dir"></param>
        /// <param name="ext"></param>
        /// <returns></returns>
        private IEnumerable<DirectoryInfo> ListDires(DirectoryInfo dir = null)
        {
            if (dir == null)
            {
                string basePath = AppDomain.CurrentDomain.BaseDirectory;
                string tagertPath = "ApiScripts\\Multi";
                string path = System.IO.Path.Combine(basePath, tagertPath);

                if (Directory.Exists(path))
                {
                    dir = new DirectoryInfo(path);
                }
                else
                {
                    return null;
                }
            }

            List<DirectoryInfo> dirs = new List<DirectoryInfo>();

            dirs.AddRange(dir.GetDirectories());

            // 逐層往下尋找
            foreach (DirectoryInfo subDir in dir.GetDirectories())
            {
                dirs.AddRange(ListDires(subDir));
            }

            return dirs;
        }

        #endregion 刪除檔案
    }
}