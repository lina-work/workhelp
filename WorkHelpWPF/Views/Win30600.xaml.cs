﻿using DingOK.Library;
using System.Collections.Generic;
using System.Net;
using System.Windows;

namespace WorkHelpWPF
{
    /// <summary></summary>
    public partial class win30600 : BaseWindow
    {
        string IniSectName = "win30600";
        T30600 PIniItem { set; get; }
        //-----------------------------系統原生函式------------------------------//
        public win30600()
        {
            InitializeComponent();
            btnSave.Click += btnSave_Click;
            btnPast.Click += btnPast_Click;
            btnClose.Click += base.winClose_Click;
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            PIniItem = Utility.FromConfig<T30600>(IniSectName);
            Utility.IntoUI<Window, T30600>(this, PIniItem);
        }
        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            Utility.FromUI<T30600, Window>(PIniItem, this);
            Utility.SaveConfig(PIniItem, IniSectName);
            System.Windows.Forms.MessageBox.Show("儲存成功");
        }
        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            Utility.ClearText<Window>(this);
        }
        private void btnPast_Click(object sender, RoutedEventArgs e)
        {
            //this.Past();
            this.Execute();
        }

        //-----------------------------使用者自訂函式-----------------------------//
        private void Past()
        {
            txtUrlTW.Text = Clipboard.GetText();
        }

        private void Execute()
        {
            //Utility.FromUI<T30600, Window>(PIniItem, this);
            //if (!IsValid(PIniItem))
            //{
            //    System.Windows.Forms.MessageBox.Show("資料有誤，請重新檢查！");
            //    return;
            //}
            var file = @"D:\file.txt";
            var args = System.IO.File.ReadAllLines(file);
            foreach (var url in args)
            {
                var item = new T30600();
                item.UrlTW = url;
                item.UrlEN = item.UrlTW.Replace("zh-tw", "en-us");
                SetUrlDataTW(item);
                SetUrlDataEN(item);
                //SetUrlDataUI(item);
                SaveMSDNRule(item);
                ArtTks.Entity.Log.Add(item.RuleId + " finished.");
            }
            System.Windows.Forms.MessageBox.Show("finished.");
        }

        private WebClient _PClient;
        private WebClient PClient
        {
            get
            {
                if (_PClient == null)
                {
                    _PClient = new WebClient();
                    _PClient.Encoding = System.Text.Encoding.UTF8; ;
                }
                return _PClient;
            }

        }

        private void SetUrlDataTW(T30600 item)
        {
            try
            {
                var html = PClient.DownloadString(item.UrlTW);
                if (!string.IsNullOrEmpty(html))
                {
                    //<title> CA1000：不要在泛型類型上宣告靜態成員</title>
                    var posS = html.IndexOf("<title>") + 7;
                    var posE = html.IndexOf("</title>") - 1;
                    var len = posE - posS + 1;
                    item.TitleTW = html.Substring(posS, len).Trim();
                    item.ReasonTW = GetReason(item, html, "<span class=\"LW_CollapsibleArea_Title\">原因</span>");
                    if (!string.IsNullOrEmpty(item.TitleTW))
                    {
                        item.RuleId = item.TitleTW.Substring(0, 6);
                    }
                }
            }
            catch (System.Exception ex)
            {
                ArtTks.Entity.Log.Add(ex);
            }
        }

        private void SetUrlDataEN(T30600 item)
        {
            try
            {
                var html = PClient.DownloadString(item.UrlEN);
                if (!string.IsNullOrEmpty(html))
                {
                    //<title> CA1000：不要在泛型類型上宣告靜態成員</title>
                    var posS = html.IndexOf("<title>") + 7;
                    var posE = html.IndexOf("</title>") - 1;
                    var len = posE - posS + 1;
                    item.TitleEN = html.Substring(posS, len).Trim();
                    item.ReasonEN = GetReason(item, html, "<span class=\"LW_CollapsibleArea_Title\">Cause</span>");

                    //if (!string.IsNullOrEmpty(item.TitleEN))
                    //{
                    //    item.RuleId = item.TitleTW.Substring(0, 6);
                    //}
                }
            }
            catch (System.Exception ex)
            {
                ArtTks.Entity.Log.Add(ex);
            }
        }

        private string GetReason(T30600 item, string html, string pattern)
        {
            var result = string.Empty;
            try
            {
                var pos01 = html.IndexOf(pattern);
                var pos11 = (pos01 > -1) ? html.IndexOf("<p>", pos01) : -1;
                var pos12 = (pos11 > -1) ? html.IndexOf("</p>", pos11) : -1;

                var posS = (pos11 > -1) ? (pos11 + 3) : -1;
                var posE = (pos12 > -1) ? (pos12 - 1) : -1;

                var len = posE - posS + 1;
                if (posS > -1 && len > 0)
                {
                    result = html.Substring(posS, len);
                    result = result.Replace("<code>", "");
                    result = result.Replace("</code>", "");
                }
            }
            catch (System.Exception ex)
            {
                ArtTks.Entity.Log.Add(ex);
            }
            return result;
        }

        private void SetUrlDataUI(T30600 item)
        {
            Utility.IntoUI<Window, T30600>(this, item);
        }

        private void SaveMSDNRule(T30600 item)
        {
            var book = WorkHelpWPF.ConfigControl.DesignFactory.GetExeBook<T30600>();
            book.AddTable("MSDNRule");
            book.AddField(x => x.UrlTW);
            book.AddField(x => x.TitleTW);
            book.AddField(x => x.ReasonTW);
            //book.AddField(x => x.ContentTW);
            book.AddField(x => x.UrlEN);
            book.AddField(x => x.TitleEN);
            book.AddField(x => x.ReasonEN);
            //book.AddField(x => x.ContentEN);
            book.InsKeyField(x => x.RuleId);
            var result = book.InsOrUpd(item);
            if (result.Status != ArtTks.EndStatus.Success)
            {
                System.Windows.Forms.MessageBox.Show("儲存失敗!!");
            }
        }

        #region Base UI Item
        private bool IsValid(T30600 item)
        {
            if (StringUtl.IsNullOrEmptys(item.UrlTW))
                return false;

            return true;
        }

        /// <summary>[MSDN 程式碼分析]資料存取</summary>
        class T30600
        {
            /// <summary>規則代碼</summary>
            public string RuleId { get; set; }

            /// <summary>繁體中文網址</summary>
            public string UrlTW { get; set; }

            /// <summary>繁體中文標題</summary>
            public string TitleTW { get; set; }

            /// <summary>繁體中文原因</summary>
            public string ReasonTW { get; set; }

            /// <summary>繁體中文內容</summary>
            public string ContentTW { get; set; }

            /// <summary>英文網址</summary>
            public string UrlEN { get; set; }

            /// <summary>英文標題</summary>
            public string TitleEN { get; set; }

            /// <summary>英文原因</summary>
            public string ReasonEN { get; set; }

            /// <summary>英文內容</summary>
            public string ContentEN { get; set; }
        }
        #endregion Base Flow
    }

}
