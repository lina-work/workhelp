﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using WorkHelp.Wpf.Enums;
using WorkHelp.Wpf.Extensions;
using WorkHelp.Wpf.Models.Main;
using WorkHelp.Wpf.Models.ViewSplit;
using WorkHelp.Wpf.Models.ViewSplit.Extends;
using WorkHelp.Wpf.Services;
using WorkHelp.Wpf.Utility;

namespace WorkHelp.Wpf.Views
{
    /// <summary>
    /// 瀏覽器處理-四欄分割
    /// </summary>
    public partial class Win50204 : BaseWindow
    {
        //-----------------------------系統原生函式------------------------------//

        /// <summary>
        /// 視窗組態檔名稱
        /// </summary>
        private string ConfigFile { get; set; } = "Win50204";

        /// <summary>
        /// 建構元
        /// </summary>
        public Win50204()
        {
            InitializeComponent();
        }

        
    }
}