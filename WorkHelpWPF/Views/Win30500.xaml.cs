﻿using DingOK.Library;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows;

namespace WorkHelpWPF
{
    /// <summary></summary>
    public partial class win30500 : BaseWindow
    {
        string IniSectName = "BookList";
        T30500 PIniItem { set; get; }
        //-----------------------------系統原生函式------------------------------//
        public win30500()
        {
            InitializeComponent();
            btnSave.Click += btnSave_Click;
            btnExe.Click += btnExe_Click;
            btnClose.Click += base.winClose_Click;
            btnSearch.Click += btnSearch_Click;
            btnFolder.Click += btnFolder_Click;
            btnAll.Click += btnAll_Click;
            btnCombine.Click += btnCombine_Click;
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            PIniItem = Utility.FromConfig<T30500>(IniSectName);
            Utility.IntoUI<Window, T30500>(this, PIniItem);
        }
        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            Utility.FromUI<T30500, Window>(PIniItem, this);
            Utility.SaveConfig(PIniItem, IniSectName);
            System.Windows.Forms.MessageBox.Show("儲存成功");
        }
        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            Utility.ClearText<Window>(this);
        }
        private void btnExe_Click(object sender, RoutedEventArgs e)
        {
            this.Execute();
        }
        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            this.Search();
        }
        private void btnFolder_Click(object sender, RoutedEventArgs e)
        {
            this.FolderStructure();
        }
        private void btnAll_Click(object sender, RoutedEventArgs e)
        {
            this.DirFileAll();
        }
        private void btnCombine_Click(object sender, RoutedEventArgs e)
        {
            this.Combine();
        }
        //-----------------------------使用者自訂函式-----------------------------//
        void Execute()
        {
            Utility.FromUI<T30500, Window>(PIniItem, this);
            if (!IsValid(PIniItem)) return;

            var dinfo = new DirectoryInfo(PIniItem.InputPath);
            var list = new List<string>();

            RecursiveFile(list, dinfo);
            if (PIniItem.AndSort)
            {
                list.Sort();
            }

            var contents = GetContents(list);
            PIniItem.OutputContents = contents;
            txtOutputContents.Text = contents;

            WriteFile(PIniItem);
        }

        void RecursiveFile(List<string> list, DirectoryInfo dinfo)
        {
            var files = dinfo.GetFiles();
            foreach (var file in files)
            {
                if (PIniItem.AndPath)
                {
                    list.Add(file.DirectoryName + '\t' + file.Name);
                }
                else
                {
                    list.Add(file.Name);
                }
            }
            var dirs = dinfo.GetDirectories();
            foreach (var dir in dirs)
            {
                RecursiveFile(list, dir);
            }
        }

        void WriteFile(T30500 item)
        {
            if (string.IsNullOrEmpty(item.OutputContents))
            {
                System.Windows.Forms.MessageBox.Show("無資料。");
            }
            else
            {
                if (!Directory.Exists(item.OutputPath))
                {
                    Directory.CreateDirectory(item.OutputPath);
                }
                if (!Directory.Exists(item.OutputPath))
                {
                    System.Windows.Forms.MessageBox.Show("輸出資料夾不存在。");
                }
                else
                {
                    if (!item.OutputFile.ToUpper().EndsWith(".TXT"))
                    {
                        item.OutputFile += ".txt";
                    }

                    var full = Path.Combine(item.OutputPath, item.OutputFile);
                    File.WriteAllText(full, PIniItem.OutputContents, Encoding.UTF8);
                }
            }
        }

        void Search()
        {
            Utility.FromUI<T30500, Window>(PIniItem, this);
            if (!IsValid(PIniItem))
            {
                return;
            }

            PIniItem.KeyWord = PIniItem.KeyWord.ToUpper();
            if (PIniItem.KeyWord[0] != '.')
            {
                PIniItem.KeyWord = "." + PIniItem.KeyWord;
            }

            var dinfo = new DirectoryInfo(PIniItem.InputPath);
            var list = new List<string>();
            RecursiveSearch(list, dinfo);

            if (PIniItem.AndSort)
            {
                list.Sort();
            }

            var contents = GetContents(list);
            PIniItem.OutputContents = contents;
            txtOutputContents.Text = contents;
        }

        void RecursiveSearch(List<string> list, DirectoryInfo dinfo)
        {
            var files = dinfo.GetFiles();
            foreach (var file in files)
            {
                var ext = file.Extension.ToUpper();
                var bSame = ext == PIniItem.KeyWord;
                var bAdd1 = (PIniItem.NotMatch && !bSame);
                var bAdd2 = (!PIniItem.NotMatch && bSame);
                if (bAdd1 || bAdd2)
                {
                    if (PIniItem.AndPath)
                    {
                        list.Add(file.DirectoryName + '\t' + file.Name);
                    }
                    else
                    {
                        list.Add(file.Name);
                    }
                }
            }
            var dirs = dinfo.GetDirectories();
            foreach (var dir in dirs)
            {
                RecursiveSearch(list, dir);
            }
        }

        void FolderStructure()
        {
            Utility.FromUI<T30500, Window>(PIniItem, this);
            if (!IsValid(PIniItem)) return;

            var dinfo = new DirectoryInfo(PIniItem.InputPath);
            var list = new List<string>();

            RecursiveFolder(list, dinfo, 0);

            var contents = GetContents(list);
            PIniItem.OutputContents = contents;
            txtOutputContents.Text = contents;

            WriteFile(PIniItem);
        }

        void RecursiveFolder(List<string> list, DirectoryInfo dinfo, int lv)
        {
            var dirs = dinfo.GetDirectories();
            foreach (var dir in dirs)
            {
                if ((dir.Attributes & FileAttributes.Hidden) != FileAttributes.Hidden)
                {
                    AddTab(list, dir.Name, lv);
                    RecursiveFolder(list, dir, lv + 1);
                }
            }
        }

        void DirFileAll()
        {
            Utility.FromUI<T30500, Window>(PIniItem, this);
            if (!IsValid(PIniItem)) return;

            var dinfo = new DirectoryInfo(PIniItem.InputPath);
            var list = new List<string>();

            RecursiveDirFileAFolder(list, dinfo, 0);

            var contents = GetContents(list);
            PIniItem.OutputContents = contents;
            txtOutputContents.Text = contents;

            WriteFile(PIniItem);
        }

        void RecursiveDirFileAFolder(List<string> list, DirectoryInfo dinfo, int lv)
        {
            var dirs = dinfo.GetDirectories();
            foreach (var dir in dirs)
            {
                if ((dir.Attributes & FileAttributes.Hidden) != FileAttributes.Hidden)
                {
                    AddTab(list, dir.Name, lv);
                    RecursiveDirFileAFolder(list, dir, lv + 1);
                }
            }

            var files = dinfo.GetFiles();
            foreach (var file in files)
            {
                if ((file.Attributes & FileAttributes.Hidden) != FileAttributes.Hidden)
                {
                    AddTab(list, file.Name, lv);
                }
            }
        }

        void Combine()
        {
            Utility.FromUI<T30500, Window>(PIniItem, this);
            //if (!IsValid(PIniItem)) return;

            var lists = new List<List<TNode>>();
            var paths = PIniItem.InputPath.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (var path in paths)
            {
                var list = new List<TNode>();
                var dinfo = new DirectoryInfo(path);
                RecursiveCombine(list, dinfo, 0);
                lists.Add(list);
            }
            Combine(lists);

            if (lists.Count > 0)
            {
                var strlist = new List<string>();
                Combine(strlist, lists[0]);

                var contents = GetContents(strlist);
                PIniItem.OutputContents = contents;
                txtOutputContents.Text = contents;

                WriteFile(PIniItem);
            }
        }

        void RecursiveCombine(List<TNode> list, DirectoryInfo dinfo, int lv)
        {
            var dirs = dinfo.GetDirectories();
            foreach (var dir in dirs)
            {
                if ((dir.Attributes & FileAttributes.Hidden) != FileAttributes.Hidden)
                {
                    var node = new TNode
                    {
                        Name = dir.Name,
                        Level = lv,
                        IsDir = true,
                        FullName = dir.FullName,
                        Children = new List<TNode>()
                    };
                    list.Add(node);
                    RecursiveCombine(node.Children, dir, lv + 1);
                }
            }

            var files = dinfo.GetFiles();
            foreach (var file in files)
            {
                if ((file.Attributes & FileAttributes.Hidden) != FileAttributes.Hidden)
                {
                    var node = new TNode
                    {
                        Name = file.Name,
                        Level = lv,
                        IsDir = false,
                        FullName = file.FullName,
                    };
                    list.Add(node);
                }
            }
        }

        void Combine(List<List<TNode>> lists)
        {
            if (lists.Count > 1)
            {
                var listFst = lists[0];
                for (var i = 1; i < lists.Count; i++)
                {
                    var list = lists[i];
                    Combine(listFst, list);
                }
            }
        }

        void Combine(List<TNode> listDest, List<TNode> listSource)
        {
            if (listDest != null && listSource != null)
            {
                for (var i = 0; i < listSource.Count; i++)
                {
                    var node = listSource[i];
                    var dest = listDest.Find(x => x.Name == node.Name);
                    if (dest == null)
                    {
                        listDest.Add(node);
                    }
                    else
                    {
                        Combine(dest.Children, node.Children);
                    }
                }
            }
        }

        void Combine(List<string> list, List<TNode> src)
        {
            if (src != null && src.Count > 0)
            {
                for (var i = 0; i < src.Count; i++)
                {
                    var node = src[i];
                    AddTab(list, node.Name, node.Level);
                    Combine(list, node.Children);
                }
            }
        }

        void AddTab(List<string> list, string name, int lv)
        {
            var content = new StringBuilder(64);
            for (int i = 0; i < lv; i++)
            {
                content.Append('\t');
            }
            content.Append(name);
            list.Add(content.ToString());
        }

        string GetContents(List<string> list)
        {
            var result = new StringBuilder(1024);
            if (list != null && list.Count > 0)
            {
                list.ForEach(x =>
                {
                    result.AppendLine(x);
                });
            }
            return result.ToString();
        }

        #region Base UI Item
        private bool IsValid(T30500 item)
        {
            if (StringUtl.IsNullOrEmptys(item.InputPath, item.OutputPath, item.OutputFile))
            {
                System.Windows.Forms.MessageBox.Show("資料有誤，請重新檢查！");
                return false;
            }
            if (!Directory.Exists(item.InputPath))
            {
                System.Windows.Forms.MessageBox.Show("來源資料夾不存在。");
                return false;
            }
            return true;
        }

        class T30500
        {
            public string InputPath { get; set; }
            public string KeyWord { get; set; }
            public string OutputPath { get; set; }
            public string OutputFile { get; set; }
            public string OutputContents { get; set; }
            public bool AndPath { get; set; }
            public bool AndSort { get; set; }
            public bool NotMatch { get; set; }
        }
        #endregion Base Flow

        class TNode
        {
            public string Name { get; set; }
            public int Level { get; set; }
            public bool IsDir { get; set; }
            public string FullName { get; set; }
            public List<TNode> Children { get; set; }
        }

    }

}
