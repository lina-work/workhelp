﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using WorkHelp.Wpf.Enums;
using WorkHelp.Wpf.Extensions;
using WorkHelp.Wpf.Models.Main;
using WorkHelp.Wpf.Models.ViewSplit;
using WorkHelp.Wpf.Models.ViewSplit.Extends;
using WorkHelp.Wpf.Services;
using WorkHelp.Wpf.Utility;

namespace WorkHelp.Wpf.Views
{
    /// <summary>
    /// 瀏覽器處理-垂直雙欄
    /// </summary>
    public partial class Win50102 : BaseWindow
    {
        //-----------------------------系統原生函式------------------------------//

        /// <summary>
        /// 視窗組態檔名稱
        /// </summary>
        private string ConfigFile { get; set; } = "Win50102";

        /// <summary>
        /// 建構元
        /// </summary>
        public Win50102()
        {
            InitializeComponent();
            BindEvents();
        }

        /// <summary>
        /// 繫結事件
        /// </summary>
        public void BindEvents()
        {
            this.Loaded += Window_Loaded;
            this.BtnClose.Click += base.FWindowClose_Click;

            #region Setting Ctrl

            BtnDefaultItem.Click += BtnDefaultItem_Click;
            BtnNewItem.Click += BtnNewItem_Click;
            BtnUpdateItem.Click += BtnUpdateItem_Click;
            BtnDeleteItem.Click += BtnDeleteItem_Click;

            BtnSaveItem.Click += BtnSaveItem_Click;
            BtnSaveCancel.Click += BtnSaveCancel_Click;

            #endregion Setting Ctrl

            #region 畫面事件

            #region 上方

            #region 上左側

            BrowserTopLeft.Navigated += new NavigatedEventHandler(Browser_Navigated);
            BtnNavigateTopLeft.Click += BtnNavigateTopLeft_Click; // 瀏覽
            BtnUrlTopLeft.Click += BtnUrlTopLeft_Click;           // 網址
            BtnSaveTopLeft.Click += BtnSaveSetting_Click;         // 儲存 (通用)

            #endregion 上左側



            #region 上右側

            BrowserTopRight.Navigated += new NavigatedEventHandler(Browser_Navigated);
            BtnNavigateTopRight.Click += BtnNavigateTopRight_Click; // 瀏覽
            BtnUrlTopRight.Click += BtnUrlTopRight_Click;           // 網址
            BtnSaveTopRight.Click += BtnSaveSetting_Click;          // 儲存 (通用)

            #endregion 上右側

            #endregion 上方

            #endregion 畫面事件
        }

        #region Setting Ctrl

        /// <summary>
        /// 視窗載入
        /// </summary>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            // 繫結標籤資料後開始載入
            Win501Service.WindowLoaded(this.Tag, this.SetMenuItem, this.LoadConfigFromImport, this.LoadConfigFromDefault);
            // 繫結選單變更事件
            this.CbxGroup.SelectionChanged += this.CbxGroup_SelectionChanged;
        }

        /// <summary>
        /// 畫面組態選單變更
        /// </summary>
        private void CbxGroup_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Win501Service.ChangeViewConfig(CbxGroup, this.ReadConfig, (selected) =>
            {
                RefreshTexts(selected);
                RefreshNavigate();
                EnabledItemAction();
            });
        }

        /// <summary>
        /// 將當前項目設為預設
        /// </summary>
        private void BtnDefaultItem_Click(object sender, RoutedEventArgs e)
        {
            if (CbxGroup.SelectedIndex <= -1)
            {
                System.Windows.Forms.MessageBox.Show("請選取項目");
            }
            else
            {
                DefaultConfig();
            }
        }

        /// <summary>
        /// 切換為新增模式
        /// </summary>
        private void BtnNewItem_Click(object sender, RoutedEventArgs e)
        {
            // 暫存當前編號
            TxtTempId.Text = TxtId.Text;
            // 清空畫面組態編號
            TxtId.Text = string.Empty;
            // 清空畫面組態名稱
            TxtName.Text = string.Empty;
            // 切換為編輯模式
            RefreshUiMode(UiStatus.Edit);
        }

        /// <summary>
        /// 切換為修改模式
        /// </summary>
        private void BtnUpdateItem_Click(object sender, RoutedEventArgs e)
        {
            // 複製畫面組態名稱
            TxtName.Text = CbxGroup.Text;
            // 切換為檢視模式
            RefreshUiMode(UiStatus.Edit);
        }

        /// <summary>
        /// 取消操作
        /// </summary>
        private void BtnSaveCancel_Click(object sender, RoutedEventArgs e)
        {
            // 還原當前畫面組態編號
            TxtId.Text = TxtTempId.Text;
            // 清空暫存編號
            TxtTempId.Text = string.Empty;
            // 切換為檢視模式
            RefreshUiMode(UiStatus.View);
        }

        /// <summary>
        /// 刪除畫面組態項目
        /// </summary>
        private void BtnDeleteItem_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult dialogResult = MessageBox.Show($"是否確認刪除【{CbxGroup.Text}】？", "Confirmation", MessageBoxButton.YesNo);
            if (dialogResult != MessageBoxResult.Yes) return;

            this.CbxGroup.SelectionChanged -= this.CbxGroup_SelectionChanged;
            DeleteItem();
            this.CbxGroup.SelectionChanged += this.CbxGroup_SelectionChanged;
        }

        /// <summary>
        /// 儲存畫面組態項目
        /// </summary>
        private void BtnSaveItem_Click(object sender, RoutedEventArgs e)
        {
            this.CbxGroup.SelectionChanged -= this.CbxGroup_SelectionChanged;
            SaveConfig();
            this.CbxGroup.SelectionChanged += this.CbxGroup_SelectionChanged;
        }

        /// <summary>
        /// 儲存畫面組態的網址
        /// </summary>
        private void BtnSaveSetting_Click(object sender, RoutedEventArgs e)
        {
            UpdateUrl();
        }

        #endregion Setting Ctrl

        #region 畫面事件

        /// <summary>
        /// 視窗靜音
        /// </summary>
        private void Browser_Navigated(object sender, NavigationEventArgs e)
        {
            BrowserExtension.SetSilent((WebBrowser)sender);
        }

        #region 上方

        #region 上左側

        /// <summary>
        /// 上左側視窗瀏覽
        /// </summary>
        private void BtnNavigateTopLeft_Click(object sender, RoutedEventArgs e)
        {
            BrowserExtension.NavigateUrl(BrowserTopLeft, TxtTopLeftUrl);
        }

        /// <summary>
        /// 上左側視窗網址
        /// </summary>
        private void BtnUrlTopLeft_Click(object sender, RoutedEventArgs e)
        {
            BrowserExtension.SetUrlText(TxtTopLeftUrl, BrowserTopLeft);
        }

        #endregion 上左側

        #region 上右側

        /// <summary>
        /// 上右側視窗瀏覽
        /// </summary>
        private void BtnNavigateTopRight_Click(object sender, RoutedEventArgs e)
        {
            BrowserExtension.NavigateUrl(BrowserTopRight, TxtTopRightUrl);
        }

        /// <summary>
        /// 上右側視窗網址
        /// </summary>
        private void BtnUrlTopRight_Click(object sender, RoutedEventArgs e)
        {
            BrowserExtension.SetUrlText(TxtTopRightUrl, BrowserTopRight);
        }

        #endregion 上右側

        #endregion 上方

        #endregion 畫面事件

        //-----------------------------使用者自訂函式-----------------------------//

        #region 通用 Function

        /// <summary>
        /// 外部修改組態檔
        /// </summary>
        private void SetMenuItem(TMenuItem menu)
        {
            this.ConfigFile = menu.ConfigFile;
        }

        /// <summary>
        /// 讀取組態
        /// </summary>
        private ViewConfig ReadConfig()
        {
            return Win501Service.ReadConfig(this.ConfigFile);
        }

        /// <summary>
        /// 寫入組態
        /// </summary>
        private void WriteConfig(ViewConfig config)
        {
            ConfigUtility.Write<ViewConfig>(config, this.ConfigFile);
        }

        /// <summary>
        /// 載入組態(外部項目)
        /// </summary>
        private void LoadConfigFromImport(TMenuItem menu)
        {
            ViewConfig config = ReadConfig();
            ViewParting selected = config.Items?.Find(x => x.Name.Equals(menu.Value, System.StringComparison.CurrentCultureIgnoreCase));

            if (selected != null)
            {
                LoadConfigParting(config, selected);
            }
            else
            {
                TxtId.Text = System.Guid.NewGuid().ToString();
                TxtName.Text = menu.Value;

                // 從介面取得組態
                selected = GetViewPartingItem();
                // 新增畫面組態
                config.Items.Add(selected);
                // 寫入組態
                WriteConfig(config);
                // 刷新選單控件
                RefreshCbxGroup(config, text: selected.Name);
                // 設定為檢視模式
                RefreshUiMode(UiStatus.View);
            }
        }

        /// <summary>
        /// 載入組態(預設項目)
        /// </summary>
        private void LoadConfigFromDefault()
        {
            ViewConfig config = ReadConfig();
            ViewParting selected = config.Items?.Find(x => x.Id.Equals(config.DefaultId, System.StringComparison.CurrentCultureIgnoreCase));

            if (selected != null)
            {
                LoadConfigParting(config, selected);
            }
            else
            {
                // 刷新選單控件
                RefreshCbxGroup(config, text: "請選擇");
                // 刷新畫面模式
                RefreshUiMode(config);
            }
        }

        /// <summary>
        /// 載入組態(預設項目)
        /// </summary>
        private void LoadConfigParting(ViewConfig config, ViewParting selected)
        {
            // 設置組態編號
            TxtId.Text = selected.Id;
            // 設置畫面組態名稱
            TxtName.Text = selected.Name;
            // 刷新選單控件
            RefreshCbxGroup(config, text: selected.Name);
            // 刷新項目控件內容
            RefreshTexts(selected);
            // 刷新導航
            RefreshNavigate();
            // 設定為檢視模式
            RefreshUiMode(UiStatus.View);
        }

        /// <summary>
        /// 將當前項目設定為預設值
        /// </summary>
        private void DefaultConfig()
        {
            ViewConfig config = ReadConfig();
            ViewParting selected = GetViewPartingItem();

            if (config == null || selected == null || string.IsNullOrWhiteSpace(selected.Id))
            {
                System.Windows.Forms.MessageBox.Show("發生錯誤");
                return;
            }

            // 設定畫面組態編碼
            config.DefaultId = selected.Id;
            // 寫入組態
            WriteConfig(config);
            // 通知操作結果
            System.Windows.Forms.MessageBox.Show("設定成功");
        }

        /// <summary>
        /// 修改組態檔的網址 (全部 Url 更新) (不異動組態選單)
        /// </summary>
        private void UpdateUrl()
        {
            ViewConfig config = ReadConfig();
            ViewParting ui = GetViewPartingItem();
            ViewParting selected = config?.Items?.Find(x => x.Id == ui.Id);

            if (selected == null)
            {
                System.Windows.Forms.MessageBox.Show("儲存網址發生錯誤");
            }
            else
            {
                // 複製資料
                selected.Clone(ui, updateName: false);
                // 寫入組態
                WriteConfig(config);
                // 通知操作結果
                System.Windows.Forms.MessageBox.Show($"儲存網址成功!!");
            }
        }

        /// <summary>
        /// 儲存組態
        /// </summary>
        private void SaveConfig()
        {
            ViewConfig config = ReadConfig();
            ViewParting ui = GetViewPartingItem();

            if (string.IsNullOrWhiteSpace(ui.Name))
            {
                System.Windows.Forms.MessageBox.Show("請輸入名稱");
                return;
            }

            if (string.IsNullOrWhiteSpace(ui.Id))
            {
                NewConfig(config, ui);
            }
            else
            {
                UpdateConfig(config, ui);
            }
        }

        /// <summary>
        /// 執行新增組態
        /// </summary>
        private void NewConfig(ViewConfig config, ViewParting ui)
        {
            // 新增項目
            ViewParting selected = new ViewParting
            {
                // 創建編號
                Id = System.Guid.NewGuid().ToString()
            };

            // 加入畫面組態清單
            config.Items.Add(selected);

            ExecuteSaveConfig(config, selected, ui);
        }

        /// <summary>
        /// 執行修改組態
        /// </summary>
        private void UpdateConfig(ViewConfig config, ViewParting ui)
        {
            // 修改項目
            ViewParting selected = config.Items?.Find(x => x.Id == ui.Id);

            if (selected == null)
            {
                System.Windows.Forms.MessageBox.Show("此群組已不存在");
                return;
            }

            ExecuteSaveConfig(config, selected, ui);
        }

        /// <summary>
        /// 執行儲存組態
        /// </summary>
        private void ExecuteSaveConfig(ViewConfig config, ViewParting selected, ViewParting ui)
        {
            // 複製資料
            selected.Clone(ui, updateName: true);
            // 寫入組態
            WriteConfig(config);
            // 刷新畫面組態選單
            RefreshCbxGroup(config, text: selected.Name);
            // 刷新項目控件內容
            RefreshTexts(selected);
            // 設定為檢視模式
            RefreshUiMode(UiStatus.View);
            // 通知操作結果
            System.Windows.Forms.MessageBox.Show($"儲存成功!!");
        }

        /// <summary>
        /// 刪除項目
        /// </summary>
        private void DeleteItem()
        {
            ViewConfig config = ReadConfig();
            ViewParting ui = GetViewPartingItem();
            ViewParting selected = config?.Items?.Find(x => x.Id == ui.Id);

            if (selected == null)
            {
                System.Windows.Forms.MessageBox.Show("項目不存在");
                return;
            }

            // 移除項目
            config.Items.Remove(selected);

            // 移除預設編號
            if (config.DefaultId == ui.Id)
            {
                config.DefaultId = string.Empty;
            }

            // 寫入組態
            WriteConfig(config);
            // 清除項目控件內容
            ClearGroupItem();
            // 刷新群組控件
            RefreshCbxGroup(config, "請選擇");
            // 刷新畫面模式
            RefreshUiMode(config);
            // 通知操作結果
            System.Windows.Forms.MessageBox.Show($"刪除成功!!");
        }

        #endregion 通用 Function

        #region 通用 UI

        /// <summary>
        /// 刷新群組控件
        /// </summary>
        private void RefreshCbxGroup(ViewConfig config, string text)
        {
            CbxGroup.Items.Clear();
            foreach (ViewParting item in config.Items)
            {
                CbxGroup.Items.Add(item.Name);
            }

            CbxGroup.Text = text;
        }

        /// <summary>
        /// 清除項目控件
        /// </summary>
        private void ClearGroupItem()
        {
            TxtId.Text = string.Empty;
            TxtName.Text = string.Empty;
        }

        /// <summary>
        /// 刷新畫面模式
        /// </summary>
        private void RefreshUiMode(ViewConfig config)
        {
            if (config == null || config.Items == null || config.Items.Count == 0)
            {
                RefreshUiMode(UiStatus.Edit);
            }
            else
            {
                RefreshUiMode(UiStatus.View);
            }
        }

        /// <summary>
        /// 刷新畫面模式
        /// </summary>
        private void RefreshUiMode(UiStatus e)
        {
            switch (e)
            {
                case UiStatus.Edit:
                    CbxGroup.Visibility = Visibility.Collapsed;
                    TxtName.Visibility = Visibility.Visible;

                    BtnDefaultItem.Visibility = Visibility.Collapsed;
                    BtnNewItem.Visibility = Visibility.Collapsed;
                    BtnUpdateItem.Visibility = Visibility.Collapsed;
                    BtnDeleteItem.Visibility = Visibility.Collapsed;

                    BtnSaveItem.Visibility = Visibility.Visible;
                    BtnSaveCancel.Visibility = Visibility.Visible;

                    BtnSaveCancel.IsEnabled = CbxGroup.Items.Count > 0;

                    break;

                case UiStatus.View:
                    CbxGroup.Visibility = Visibility.Visible;
                    TxtName.Visibility = Visibility.Collapsed;

                    BtnDefaultItem.Visibility = Visibility.Visible;
                    BtnNewItem.Visibility = Visibility.Visible;
                    BtnUpdateItem.Visibility = Visibility.Visible;
                    BtnDeleteItem.Visibility = Visibility.Visible;

                    BtnSaveItem.Visibility = Visibility.Collapsed;
                    BtnSaveCancel.Visibility = Visibility.Collapsed;

                    EnabledItemAction();
                    break;

                default:
                    break;
            }
        }

        /// <summary>
        /// 啟用選取項目功能
        /// </summary>
        private void EnabledItemAction()
        {
            bool isEnabled = CbxGroup.SelectedIndex > -1;
            BtnDeleteItem.IsEnabled = isEnabled;
            BtnDefaultItem.IsEnabled = isEnabled;
            BtnUpdateItem.IsEnabled = isEnabled;
        }

        #endregion 通用 UI

        /// <summary>
        /// 刷新項目控件
        /// </summary>
        private void RefreshTexts(ViewParting selected)
        {
            TxtId.Text = selected.Id;
            TxtName.Text = selected.Name;

            TxtTopLeftUrl.Text = selected.TopLeftUrl;
            TxtTopRightUrl.Text = selected.TopRightUrl;
        }

        /// <summary>
        /// 刷新導航
        /// </summary>
        private void RefreshNavigate()
        {
            BrowserExtension.NavigateUrl(BrowserTopLeft, TxtTopLeftUrl);
            BrowserExtension.NavigateUrl(BrowserTopRight, TxtTopRightUrl);
        }

        /// <summary>
        /// 將 UI 輸入控件資料轉為組態物件
        /// </summary>
        /// <returns></returns>
        private ViewParting GetViewPartingItem()
        {
            return new ViewParting
            {
                Id = TxtId.Text.Trim(),
                Name = TxtName.Text.Trim(),

                TopLeftUrl = TxtTopLeftUrl.Text.Trim(),
                TopRightUrl = TxtTopRightUrl.Text.Trim(),
            };
        }
    }
}