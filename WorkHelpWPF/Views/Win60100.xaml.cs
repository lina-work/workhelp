﻿using DocumentFormat.OpenXml.Vml;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using WorkHelp.Wpf.Enums;
using WorkHelp.Wpf.Extensions;
using WorkHelp.Wpf.Models.Main;
using WorkHelp.Wpf.Models.ViewSplit;
using WorkHelp.Wpf.Models.ViewSplit.Extends;
using WorkHelp.Wpf.Services;
using WorkHelp.Wpf.Utility;


namespace WorkHelp.Wpf.Views
{
    /// <summary>
    /// MOD
    /// </summary>
    public partial class Win60100 : BaseWindow
    {
        /// <summary>
        /// 資料夾名稱
        /// </summary>
        private string FolderName { get; set; } = "Win60100";

        /// <summary>
        /// 建構元
        /// </summary>
        public Win60100()
        {
            InitializeComponent();
            BindEvents();
        }

        /// <summary>
        /// 繫結事件
        /// </summary>
        private void BindEvents()
        {
            this.Loaded += Window_Loaded;
            this.KeyDown += base.FWindow_KeyDown;

            BtnReadFile.Click += BtnReadFile_Click;
            BtnLoadTree.Click += BtnLoadTree_Click;

            FileTree.SelectedItemChanged += FileTree_SelectedItemChanged;
            BtnClose.Click += base.FWindowClose_Click;
        }

        //-----------------------------系統原生函式------------------------------//

        /// <summary>
        /// 視窗載入
        /// </summary>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //var config = ConfigUtility.Read<Config412>();
            //if (config != null)
            //{

            //}
        }
        /// <summary>
        /// 選取檔案項目
        /// </summary>
        private void FileTree_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            TreeView tree = sender as TreeView;
            object selected = tree.SelectedItem;
            if (selected is MenuNodeMember member)
            {
                if (member.IsFolder)
                {
                    TxtFolder.SetText(member.FullName);
                    LoadTree(member.FullName);
                }
                else
                {
                    TxtSelectedFile.SetText(member.FullName);
                    LoadFile(member.FullName);
                    SplitContents(member.FullName);
                }
            }
        }

        /// <summary>
        /// 讀取檔案
        /// </summary>
        private void BtnReadFile_Click(object sender, RoutedEventArgs e)
        {
            var dirPath = string.Empty;

            using (var fbd = new System.Windows.Forms.FolderBrowserDialog())
            {
                System.Windows.Forms.DialogResult result = fbd.ShowDialog();

                if (result == System.Windows.Forms.DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    dirPath = fbd.SelectedPath;
                }
            }

            TxtFolder.Text = dirPath;
        }

        /// <summary>
        /// 重載檔案樹
        /// </summary>
        private void BtnLoadTree_Click(object sender, RoutedEventArgs e)
        {
            LoadTree(TxtFolder.Text.Trim());
        }
        
        /// <summary>
        /// 儲存設定
        /// </summary>
        private void BtnSaveSetting_Click(object sender, RoutedEventArgs e)
        {
            //ConfigUtility.Write(GetConfig());
            //MessageBox.Show("儲存成功!!");
        }

        /// <summary>
        /// 開啟資料夾
        /// </summary>
        private void BtnOpenFolder_Click(object sender, RoutedEventArgs e)
        {
            //FileUtility.Explorer(this.FolderName);
        }

        //-----------------------------使用者自訂函式-----------------------------//

        /// <summary>
        /// 載入檔案樹
        /// </summary>
        private void LoadTree(string url)
        {
            if (!string.IsNullOrWhiteSpace(url))
            {
                FileTree.ItemsSource = Win502Service.GetMenuNodes(url, isAppendImage: false);
            }
        }

        /// <summary>
        /// 載入檔案內容
        /// </summary>
        /// <param name="file">檔案完整路徑</param>
        private void LoadFile(string file)
        {
            if (System.IO.File.Exists(file))
            {
                TxtFileContents.Text = System.IO.File.ReadAllText(file);
            }
        }

        /// <summary>
        /// 切割檔案內容
        /// </summary>
        /// <param name="file">檔案完整路徑</param>
        private void SplitContents(string file)
        {
            string[] lines = System.IO.File.ReadAllLines(file);
            if (lines == null || lines.Length == 0)
            {
                System.Windows.Forms.MessageBox.Show("檔案無內容");
                return;
            }

            StringBuilder builder_left = new StringBuilder();
            StringBuilder builder_middle = new StringBuilder();
            StringBuilder builder_right = new StringBuilder();

            foreach (string row in lines)
            {
                if(!row.Contains("<"))
                {
                    builder_left.AppendLine(row);
                }

                int pos1 = row.IndexOf(">");
                int pos2 = row.IndexOf("<", pos1 + 1);
                int pos3 = row.IndexOf(">", pos2 + 1);
                
                string part1 = row.Substring(0, pos1 + 1);
                //string part2 = row.Substring(pos1 + 1, );

                builder_left.AppendLine(part1);
            }

            TxtLeftContents.Text = builder_left.ToString();
        }
    }
}