﻿using DingOK.Library;
using DingOK.Library.Web;
using DingOK.Library.WinAPI;
using DingOK.Library.Extension;
using System;
using System.IO;
using System.Windows;
using System.Net;

namespace WorkHelpWPF
{
    /// <summary></summary>
    public partial class win30300 : BaseWindow
    {
        //-----------------------------系統原生函式------------------------------//
        public win30300()
        {
            InitializeComponent();
            btnDownload.Click += btnDownload_Click;
            btnIni.Click += btnIni_Click;
            btnClear.Click += btnClear_Click;
            btnClose.Click += btnClose_Click;
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            IntoUI(FromConfig);
        }
        private void btnIni_Click(object sender, RoutedEventArgs e)
        {
            IntoConfig(FromUI);
            System.Windows.Forms.MessageBox.Show("儲存成功");
        }
        private void btnDownload_Click(object sender, RoutedEventArgs e)
        {
            this.Download();
        }
        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            txtDnlUrls.Text = "";
        }
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        //-----------------------------使用者自訂函式-----------------------------//

        private void Download()
        {
            var item = FromUI();

            if (!IsValid(item))
            {
                System.Windows.Forms.MessageBox.Show("資料有誤，請重新檢查！");
                return;
            }

            var urlList = GetUrlList(item.DnlUrls);
            if (urlList == null || urlList.Length == 0)
            {
                System.Windows.Forms.MessageBox.Show("下載路徑有誤，請重新檢查！");
                return;
            }

            int summary = urlList.Length;
            int savecnt = 0;
            foreach (string url in urlList)
            {
                if (DnlFile(item, url)) savecnt++;
            }

            if (summary == savecnt)
                System.Windows.Forms.MessageBox.Show(string.Format("下載完成(下載檔案/總檔案數)：{0}/{1}", savecnt, summary));
            else
                System.Windows.Forms.MessageBox.Show(string.Format("下載有誤(下載檔案/總檔案數)：{0}/{1}", savecnt, summary));
        }

        private WebClient client { get; set; }

        private bool DnlFile(TItem item, string url)
        {
            var file = url.GetArgsLast('/');
            if (string.IsNullOrEmpty(file)) return false;

            var fullfile = Path.Combine(item.SavePath, file);

            if (client == null) client = new WebClient();
            client.DownloadFile(url, fullfile);	

            return File.Exists(fullfile);
        }
        private string[] GetUrlList(string val)
        {
            return val.Split(new string[] { "<br />" }, StringSplitOptions.None);
        }
        private bool IsValid(TItem item)
        {
            if (StringUtl.IsNullOrEmptys(item.SavePath, item.DnlUrls))
                return false;

            if (!Directory.Exists(item.SavePath)) Directory.CreateDirectory(item.SavePath);

            return true;
        }

        private string IniSectName = "CDNDNL";
        private TItem FromConfig()
        {
            var result = new TItem();
            var ini = IniFactory.GetIni(result, IniSectName);

            ini.ReadStr(x => x.SavePath);
            ini.ReadStr(x => x.DnlUrls);

            return result;
        }
        private TItem FromUI()
        {
            var result = new TItem
            {
                SavePath = txtSavePath.Text.Trim(),
                DnlUrls = txtDnlUrls.Text.Trim().Replace("\r\n", "<br />"),
            };

            return result;
        }
        private void IntoConfig(Func<TItem> func)
        {
            var item = func();
            var ini = IniFactory.GetIni(item, IniSectName);

            ini.WriteStr(x => x.SavePath);
            ini.WriteStr(x => x.DnlUrls);
        }
        private void IntoUI(Func<TItem> func)
        {
            var item = func();
            txtSavePath.Text = item.SavePath.Trim();
            txtDnlUrls.Text = item.DnlUrls.Trim().Replace("<br />", "\r\n");
        }
        private class TItem
        {
            public string SavePath { get; set; }
            public string DnlUrls { get; set; }
        }
    }
}
