﻿using DingOK.DBTransaction;
using DingOK.Library;
using DingOK.Library.Load;
using DingOK.Library.Web;
using DingOK.Library.WinAPI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Net;
using System.Windows;

namespace WorkHelpWPF
{
    /// <summary></summary>
    public partial class win30100 : BaseWindow
    {
        //-----------------------------系統原生函式------------------------------//
        public win30100()
        {
            InitializeComponent();
            btnDownload.Click += btnDownload_Click;
            btnIni.Click += btnIni_Click;
            btnClear.Click += btnClear_Click;
            btnClose.Click += btnClose_Click;
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            txtDir.Text = IniUtl.ReadStr("BaiDuDNL", "Dir");
            txtFile.Text = IniUtl.ReadStr("BaiDuDNL", "File");
            txtURL.Text = IniUtl.ReadStr("BaiDuDNL", "URL");
        }
        private void btnDownload_Click(object sender, RoutedEventArgs e)
        {
            this.Download();
        }
        private void btnIni_Click(object sender, RoutedEventArgs e)
        {
            IniUtl.WriteStr("BaiDuDNL", "Dir", txtDir.Text.Trim());
            IniUtl.WriteStr("BaiDuDNL", "File", txtFile.Text.Trim());
            IniUtl.WriteStr("BaiDuDNL", "URL", txtURL.Text.Trim());
            System.Windows.Forms.MessageBox.Show("儲存成功");
        }
        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            txtFile.Text = "";
            txtURL.Text = "";
        }
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        //-----------------------------使用者自訂函式-----------------------------//
        private ILoadBook load { get; set; }
        private bool Start { get; set; }

        private void Download()
        {
            var url = txtURL.Text.Trim();
            var dir = txtDir.Text.Trim();
            var file = txtFile.Text.Trim();
            load = NetFactory.CreateLoadBook(url, dir, file);
            load.Download();

            //this.DownloadFile();
            //this.CheckFile();
        }
        #region 下載 Work
        void DownloadFile()
        {
            BackgroundWorker bw = new BackgroundWorker();
            bw.WorkerSupportsCancellation = true;
            bw.WorkerReportsProgress = false;

            /// add the event handler for each progress
            bw.DoWork += new DoWorkEventHandler(DoWork1);
            //bw.ProgressChanged += new ProgressChangedEventHandler(DuringWork1);
            bw.RunWorkerCompleted += new RunWorkerCompletedEventHandler(AfterWork1);

            bw.RunWorkerAsync();
        }
        void DoWork1(object sender, DoWorkEventArgs e)
        {
            if (!Start)
            {
                Start = true;
                load.Download();
            }
        }

        void AfterWork1(object sender, RunWorkerCompletedEventArgs e)
        {
            ///// reflect the result after background work
            //if (e.Cancelled == true)
            //{
            //    System.Windows.Forms.MessageBox.Show(load.ExeMessage);
            //    lblPercent.Content = "Canceled!";
            //}
            //else if (!(e.Error == null))
            //{
            //    System.Windows.Forms.MessageBox.Show(load.ExeMessage);
            //    lblPercent.Content = ("Error: " + e.Error.Message);
            //}
            //else
            //{
            //    lblPercent.Content = "Done!";
            //    System.Windows.Forms.MessageBox.Show("Done at " + DateTime.Now.ToString("HH:mm:ss.fff"));
            //}
        }
        #endregion 下載 Work

        //#region 檢查進度
        //void CheckFile()
        //{
        //    BackgroundWorker bw2 = new BackgroundWorker();
        //    bw2.WorkerSupportsCancellation = true;
        //    bw2.WorkerReportsProgress = true;

        //    /// add the event handler for each progress
        //    bw2.DoWork += new DoWorkEventHandler(DoWork2);
        //    bw2.ProgressChanged += new ProgressChangedEventHandler(DuringWork2);
        //    bw2.RunWorkerCompleted += new RunWorkerCompletedEventHandler(AfterWork2);

        //    bw2.RunWorkerAsync();
        //}
        //void DoWork2(object sender, DoWorkEventArgs e)
        //{
        //    BackgroundWorker w = sender as BackgroundWorker;

        //    System.Threading.Thread.Sleep(20 * 1000);

        //    int end = (int)load.GetPercent();
        //    w.ReportProgress(end);
        //}
        //void DuringWork2(object sender, ProgressChangedEventArgs e)
        //{
        //    /// reflect the change of progress in UI
        //    lblPercent.Content = e.ProgressPercentage.ToString() + "%";
        //}

        //void AfterWork2(object sender, RunWorkerCompletedEventArgs e)
        //{
        //    /// reflect the result after background work
        //    if (e.Cancelled == true)
        //    {
        //        System.Windows.Forms.MessageBox.Show(load.ExeMessage);
        //        lblPercent.Content = "Canceled!";
        //    }
        //    else if (!(e.Error == null))
        //    {
        //        System.Windows.Forms.MessageBox.Show(load.ExeMessage);
        //        lblPercent.Content = ("Error: " + e.Error.Message);
        //    }
        //    else
        //    {
        //        lblPercent.Content = "Done!";
        //        System.Windows.Forms.MessageBox.Show("Done at " + DateTime.Now.ToString("HH:mm:ss.fff"));
        //    }
        //}
        //#endregion 檢查進度
    }
}
