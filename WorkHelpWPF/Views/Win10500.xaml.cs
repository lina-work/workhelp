﻿using System.Speech.Synthesis;
using System.Windows;
using WorkHelp.Utilities;
using WorkHelp.Wpf.Extensions;
using WorkHelp.Wpf.Models.Windows;
using WorkHelp.Wpf.Utility;

namespace WorkHelp.Wpf.Views
{
    /// <summary>
    /// Google Cloud Speech
    /// </summary>
    public partial class Win10500 : BaseWindow
    {
        /// <summary>
        /// 資料夾名稱
        /// </summary>
        private string FolderName { get; set; } = "Win10500";

        /// <summary>
        /// 建構元
        /// </summary>
        public Win10500()
        {
            InitializeComponent();
            BindEvents();
        }

        /// <summary>
        /// 繫結事件
        /// </summary>
        private void BindEvents()
        {
            this.Loaded += Window_Loaded;
            this.KeyDown += base.FWindow_KeyDown;
            this.BtnClose.Click += base.FWindowClose_Click;

            this.BtnExecute.Click += this.BtnExecute_Click;
            this.BtnClear.Click += this.BtnClear_Click;
            this.BtnSaveSetting.Click += this.BtnSaveSetting_Click;
            this.BtnOpenFolder.Click += this.BtnOpenFolder_Click;
        }

        //----------------------------- 系統原生函式 ------------------------------//

        /// <summary>
        /// 視窗載入
        /// </summary>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            LoadConfig();
        }

        /// <summary>
        /// 開啟資料夾
        /// </summary>
        private void BtnOpenFolder_Click(object sender, RoutedEventArgs e)
        {
            FileUtility.Explorer(this.FolderName);
        }

        /// <summary>
        /// 儲存設定
        /// </summary>
        private void BtnSaveSetting_Click(object sender, RoutedEventArgs e)
        {
            ConfigUtility.Write(GetConfig());
            MessageBox.Show("儲存成功!!");
        }

        /// <summary>
        /// 清除
        /// </summary>
        private void BtnClear_Click(object sender, RoutedEventArgs e)
        {
            ClearView();
        }

        /// <summary>
        /// 執行置換
        /// </summary>
        private void BtnExecute_Click(object sender, RoutedEventArgs e)
        {
            Execute();
        }

        //----------------------------- 控件連動函式 -----------------------------//

        /// <summary>
        /// 載入組態
        /// </summary>
        private void LoadConfig()
        {
            var config = ConfigUtility.Read<Config105>();
            if (config != null)
            {
                TxtInterval.SetText(config.Interval);
                TxtFrequency.SetText(config.Frequency);
            }
        }

        /// <summary>
        /// 取得組態
        /// </summary>
        private Config105 GetConfig()
        {
            return new Config105
            {
                Word = TxtInput.StringValue(),
                Interval = TxtInterval.IntValue(),
                Frequency = TxtFrequency.IntValue(),
            };
        }

        /// <summary>
        /// 清除畫面控件
        /// </summary>
        private void ClearView()
        {
            TxtInterval.Text = string.Empty;
            TxtFrequency.Text = string.Empty;
            TxtInput.Text = string.Empty;
            TxtOutput.Text = string.Empty;
        }

        //----------------------------- 用戶自訂函式 -----------------------------//

        /// <summary>
        /// 執行
        /// </summary>
        private void Execute()
        {
            Config105 config = GetConfig();
            if (string.IsNullOrWhiteSpace(config.Word))
            {
                System.Windows.Forms.MessageBox.Show("請輸入單字");
            }
            else if (config.Interval <= 0)
            {
                System.Windows.Forms.MessageBox.Show("請輸入朗讀間隔時間(毫秒)");
            }
            else if (config.Frequency <= 0)
            {
                System.Windows.Forms.MessageBox.Show("請輸入朗讀次數");
            }
            else
            {
                int stopIndex = config.Frequency - 1;
                for (int i = 0; i < config.Frequency; i++)
                {
                    Speech(config.Word);
                    if (i != stopIndex)
                    {
                        System.Threading.Thread.Sleep(config.Interval);
                    }
                }
            }
        }

        /// <summary>
        /// 朗讀
        /// </summary>
        private void Speech(string contents)
        {
            // Google 的辨識度更高，沒有網路時堪用
            SpeechSynthesizer ssyer = new SpeechSynthesizer();
            ssyer.Speak(contents);
        }
    }
}