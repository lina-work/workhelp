﻿using System;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using WorkHelp.Utilities;
using WorkHelp.Wpf.Enums;
using WorkHelp.Wpf.Enums.Extends;
using WorkHelp.Wpf.Extensions;
using WorkHelp.Wpf.Models;
using WorkHelp.Wpf.Models.Windows;
using WorkHelp.Wpf.Services;
using WorkHelp.Wpf.Services.Win411;
using WorkHelp.Wpf.Utility;

namespace WorkHelp.Wpf.Views
{
    /// <summary>
    /// Aras 處理
    /// </summary>
    public partial class Win10700 : BaseWindow
    {
        /// <summary>
        /// 資料夾名稱
        /// </summary>
        private string FolderName { get; set; } = "Win10700";

        /// <summary>
        /// 建構元
        /// </summary>
        public Win10700()
        {
            InitializeComponent();
            BindEvents();
        }

        /// <summary>
        /// 繫結事件
        /// </summary>
        private void BindEvents()
        {
            this.Loaded += Window_Loaded;
            this.KeyDown += base.FWindow_KeyDown;
            this.BtnClose.Click += base.FWindowClose_Click;
            this.BtnExecute.Click += this.BtnExecute_Click;
        }

        //-----------------------------系統原生函式------------------------------//

        /// <summary>
        /// 視窗載入
        /// </summary>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
        }


        /// <summary>
        /// 執行
        /// </summary>
        private void BtnExecute_Click(object sender, RoutedEventArgs e)
        {
            Execute();
        }

        /// <summary>
        /// 清除
        /// </summary>
        private void BtnClear_Click(object sender, RoutedEventArgs e)
        {
            ClearView();
        }

        /// <summary>
        /// 開啟資料夾
        /// </summary>
        private void BtnOpenFolder_Click(object sender, RoutedEventArgs e)
        {
            FileUtility.Explorer(this.FolderName);
        }

        //-----------------------------使用者自訂函式-----------------------------//

        /// <summary>
        /// 執行
        /// </summary>
        private void Execute()
        {
            try
            {
                var path = TxtPath.Text.Trim();
                if (string.IsNullOrWhiteSpace(path) || !System.IO.Directory.Exists(path))
                {
                    System.Windows.Forms.MessageBox.Show("請輸入路徑");
                    return;
                }
                var file = TxtFile.Text.Trim();
                if (string.IsNullOrWhiteSpace(file))
                {
                    System.Windows.Forms.MessageBox.Show("請輸入檔案");
                    return;
                }
                var oldKey = TxtOldKey.Text.Trim();
                if (string.IsNullOrWhiteSpace(oldKey))
                {
                    System.Windows.Forms.MessageBox.Show("請輸入[原始字串]");
                    return;
                }
                var newKey = TxtNewKey.Text.Trim();
                if (string.IsNullOrWhiteSpace(newKey))
                {
                    System.Windows.Forms.MessageBox.Show("請輸入[新字串]");
                    return;
                }

                var log = new StringBuilder();
                var isOneSite = IsOneSite.IsChecked.HasValue && IsOneSite.IsChecked.Value;
                if (isOneSite)
                {
                    var one_folder = new System.IO.DirectoryInfo(path);
                    ExecuteOneFolder(one_folder, file, oldKey, newKey, log);
                }
                else
                {

                    var root = new System.IO.DirectoryInfo(path);
                    var site_folders = root.GetDirectories();
                    foreach (var site_folder in site_folders)
                    {
                        ExecuteOneFolder(site_folder, file, oldKey, newKey, log);
                    }
                }

                var text = log.ToString();
                WorkHelp.Logging.TLog.Watch(message: text);
                TxtOutput.Text = text;

            }
            catch (Exception ex)
            {
                WorkHelp.Logging.TLog.Watch(message: ex.Message);
            }
        }

        private void ExecuteOneFolder(System.IO.DirectoryInfo dir, string file, string oldKey, string newKey, StringBuilder output)
        {
            try
            {
                var path = dir.FullName.TrimEnd('\\') + "\\" + file;
                output.AppendLine($"檔案: {path}");

                if (!System.IO.File.Exists(path))
                {
                    output.AppendLine($"    - 不存在");
                    return;
                }
                else
                {
                    output.AppendLine($"    - 存在");
                }

                lock (_AppSeetingLock)
                {
                    var contents = System.IO.File.ReadAllText(path, System.Text.Encoding.UTF8);
                    if (string.IsNullOrWhiteSpace(contents))
                    {
                        output.AppendLine($"    - 無內容");
                    }
                    else if (!contents.Contains(oldKey))
                    {
                        output.AppendLine($"    - 查無原始字串 {oldKey}");
                    }
                    else
                    {
                        var newContents = contents.Replace(oldKey, newKey);
                        System.IO.File.WriteAllText(path, newContents);
                        output.AppendLine($"    - 取代成功");
                    }
                }
            }
            catch (Exception ex)
            {
                WorkHelp.Logging.TLog.Watch(message: ex.Message);
            }
        }

        /// <summary>
        /// 鎖
        /// </summary>
        private static object _AppSeetingLock = new object();

        /// <summary>
        /// 清除
        /// </summary>
        private void ClearView()
        {

        }
    }
}