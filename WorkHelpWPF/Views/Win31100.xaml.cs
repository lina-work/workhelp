﻿using DingOK.Library.WinAPI;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using WorkHelpWPF.Models;
using WorkHelpWPF.Services;

namespace WorkHelpWPF
{
    /// <summary>
    /// win40100.xaml 的互動邏輯
    /// </summary>
    public partial class win31100 : BaseWindow
    {
        private string ConfigSectionNmae = "Config311";

        public win31100()
        {
            InitializeComponent();
            BindEvent();
        }

        private void BindEvent()
        {
            this.Loaded += this.Window_Loaded;
            this.KeyDown += base.winKeyDownHandler;

            this.btnSetting.Click += this.btnSetting_Click;
            this.btnFolder.Click += this.btnFolder_Click;

            this.btnDB.Click += this.btnDB_Click;
            this.btnTable.Click += this.btnTable_Click;

            this.btnExec.Click += this.btnExec_Click;

            this.cbxTable.SelectionChanged += this.cbxTable_SelectionChanged;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            txtServer.Text = IniUtl.ReadStr(ConfigSectionNmae, "SvrName");
            txtUID.Text = IniUtl.ReadStr(ConfigSectionNmae, "UID");
            txtPWD.Text = IniUtl.ReadStr(ConfigSectionNmae, "PWD");

            string DB = IniUtl.ReadStr(ConfigSectionNmae, "Database");
            cbxDB.Items.Clear();
            cbxDB.Items.Add(DB);
            cbxDB.Text = DB;

            string tableName = IniUtl.ReadStr(ConfigSectionNmae, "TableName");
            cbxTable.Items.Clear();
            cbxTable.Items.Add(tableName);
            cbxTable.Text = tableName;

            txtNameSpace.Text = IniUtl.ReadStr(ConfigSectionNmae, "Project");
            txtFolder.Text = IniUtl.ReadStr(ConfigSectionNmae, "Folder");
            txtCtrlName.Text = IniUtl.ReadStr(ConfigSectionNmae, "Controller");
            txtMainName.Text = IniUtl.ReadStr(ConfigSectionNmae, "MainName");
            txtMainRouteName.Text = IniUtl.ReadStr(ConfigSectionNmae, "MainRouteName");
            txtSubRouteName.Text = IniUtl.ReadStr(ConfigSectionNmae, "SubRouteName");

            chkEasyName.IsChecked = IniUtl.ReadBln(ConfigSectionNmae, "EasyName");
        }

        private void btnSetting_Click(object sender, RoutedEventArgs e)
        {
            IniUtl.IniWriteValue(ConfigSectionNmae, "SvrName", txtServer.Text.Trim());
            IniUtl.IniWriteValue(ConfigSectionNmae, "UID", txtUID.Text.Trim());
            IniUtl.IniWriteValue(ConfigSectionNmae, "PWD", txtPWD.Text.Trim());
            IniUtl.IniWriteValue(ConfigSectionNmae, "Database", cbxDB.Text.Trim());
            IniUtl.IniWriteValue(ConfigSectionNmae, "TableName", cbxTable.Text.Trim());

            IniUtl.IniWriteValue(ConfigSectionNmae, "Project", txtNameSpace.Text.Trim());
            IniUtl.IniWriteValue(ConfigSectionNmae, "Folder", txtFolder.Text.Trim());
            IniUtl.IniWriteValue(ConfigSectionNmae, "Controller", txtCtrlName.Text.Trim());
            IniUtl.IniWriteValue(ConfigSectionNmae, "MainName", txtMainName.Text.Trim());
            IniUtl.IniWriteValue(ConfigSectionNmae, "MainRouteName", txtMainRouteName.Text.Trim());
            IniUtl.IniWriteValue(ConfigSectionNmae, "SubRouteName", txtSubRouteName.Text.Trim());

            if (chkEasyName.IsChecked.HasValue)
            {
                IniUtl.IniWriteValue(ConfigSectionNmae, "EasyName", chkEasyName.IsChecked.ToString().ToLower());
            }

            MessageBox.Show("儲存成功!!");
        }

        private void cbxTable_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string table = cbxTable.SelectedItem?.ToString();
            if (string.IsNullOrWhiteSpace(table))
            {
                return;
            }

            if (table.Contains("ProgramA"))
            {
                table = table.Replace("ProgramA", "Program");
            }

            table = table.Replace("_", "");

            List<string> list = new List<string>();
            char[] args = table.ToCharArray();
            string piece = string.Empty;
            foreach (char c in args)
            {
                if (char.IsUpper(c))
                {
                    if (!string.IsNullOrWhiteSpace(piece))
                    {
                        list.Add(piece);
                    }
                    piece = c.ToString().ToLower();
                }
                else
                {
                    piece += c.ToString().ToLower();
                }
            }

            if (!string.IsNullOrWhiteSpace(piece))
            {
                list.Add(piece);
            }

            string controller = string.Empty; ;
            string mainRoute = "api";
            string subRoute = string.Empty;
            int lastIdx = list.Count - 1;
            for (int i = 0; i < list.Count; i++)
            {
                string str = list[i];
                if (i == 0)
                {
                    controller = str.Substring(0, 1).ToUpper() + str.Substring(1, str.Length - 1);
                }

                if (i == lastIdx)
                {
                    subRoute = list[i];
                }
                else
                {
                    mainRoute += "/" + list[i];
                }
            }

            txtMainName.Text = table;
            txtCtrlName.Text = controller;
            txtMainRouteName.Text = mainRoute;
            txtSubRouteName.Text = subRoute;
            txtFolder.Text = $"ApiScripts\\{table}";
        }

        private void btnDB_Click(object sender, RoutedEventArgs e)
        {
            cbxDB.Items.Clear();
            var connString = GetConnectionString(isMaster: false);
            var source = Win401Service.GetDBList(connString);
            var list = source.Select(x => x.name).ToList();
            SetItems(cbxDB, list);
        }

        private void btnTable_Click(object sender, RoutedEventArgs e)
        {
            cbxTable.Items.Clear();
            var connString = GetConnectionString(isMaster: false);
            var source = Win401Service.GetViewList(connString);
            var list = source.Select(x => x.name).ToList();
            SetItems(cbxTable, list);
        }

        private void btnExec_Click(object sender, RoutedEventArgs e)
        {
            var wModel = GetWindowModel();
            if (wModel.IsValid)
            {
                Service311.LogUserMoreInfo(wModel.ConnectionString);
                System.Windows.Forms.MessageBox.Show("Api All Scripts Finished!!");
            }
        }

        private void btnFolder_Click(object sender, RoutedEventArgs e)
        {
            var folder = "logs";
            var dir = DingOK.Library.AppBasic.getFullSubDir(folder);
            DingOK.Library.FileUtl.openFolderFile(dir);
        }

        // ======================================================== //
        // ======================================================== //
        // ======================================================== //

        private Win401Model GetWindowModel()
        {
            var result = new Win401Model
            {
                ConnectionString = GetConnectionString(isMaster: false),
                TableName = cbxTable.Text.Trim(),
                FolderName = txtFolder.Text.Trim(),
                MainName = cbxTable.Text.Trim().Replace("_", string.Empty),
                ControllerName = txtCtrlName.Text.Trim(),
                MainRouteName = txtMainRouteName.Text.Trim(),
                SubRouteName = txtSubRouteName.Text.Trim(),
            };

            var mainName = txtMainName.Text.Trim();
            if (!string.IsNullOrWhiteSpace(mainName))
            {
                result.MainName = mainName;
            }

            if (chkEasyName.IsChecked.HasValue)
            {
                result.IsEasyName = chkEasyName.IsChecked.Value;
            }

            if (string.IsNullOrWhiteSpace(result.ConnectionString))
            {
                System.Windows.Forms.MessageBox.Show("資料設定有誤，請重新設定");
                result.IsValid = false;
            }
            else
            {
                result.IsValid = true;
            }
            return result;
        }

        private string GetConnectionString(bool isMaster = false)
        {
            string sSvr = txtServer.Text.Trim();
            string sUID = txtUID.Text.Trim();
            string sPWD = txtPWD.Text.Trim();
            string sDB = isMaster ? "master" : cbxDB.Text.Trim();
            return $"SERVER={sSvr};DATABASE={sDB};UID={sUID};PWD={sPWD}";
        }

        private void SetItems(ComboBox ui, List<string> list)
        {
            if (list == null || list.Count == 0)
            {
                MessageBox.Show("no data!!");
                return;
            }

            foreach (var value in list)
            {
                ui.Items.Add(value);
            }

            if (ui.Items.Count > 0) ui.Text = ui.Items[0].ToString();
        }
    }
}