﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using WorkHelp.Modules;
using WorkHelp.Wpf.Factory;
using WorkHelp.Wpf.Models.Main;
using WorkHelp.Wpf.Models.Main.Extends;
using WorkHelp.Wpf.Models.Windows;
using WorkHelp.Wpf.Utility;

namespace WorkHelp.Wpf.Views
{
    /// <summary>
    /// 主控介面
    /// </summary>
    public partial class MainWindow : BaseWindow
    {
        //-----------------------------系統原生函式------------------------------//

        /// <summary>
        /// 日誌
        /// </summary>
        public Logger Logger => LogManager.GetCurrentClassLogger();

        /// <summary>
        /// 建構元
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
            this.Loaded += Window_Loaded;
            this.KeyDown += Window_KeyDown;
        }

        /// <summary>
        /// 視窗載入
        /// </summary>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            EventSystem.Register(EventTopic.ReloadBrowserMenu, this.RefreshBrowserMenu);
            RefreshBrowserMenu();
        }

        /// <summary>
        /// 視窗按鍵處理
        /// </summary>
        protected void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                this.Close();
            }
        }

        /// <summary>
        /// 功能選單-項目點擊
        /// </summary>
        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            TMenuItem item = TMenuItemExtend.GetTagData((MenuItem)sender);
            MenuItem_Click(item);
        }

        /// <summary>
        /// 功能選單-刷新項目
        /// </summary>
        private void MenuItemRefresh_Click(object sender, RoutedEventArgs e)
        {
            RefreshBrowserMenu();
        }

        //-----------------------------使用者自訂函式-----------------------------//
        
        /// <summary>
        /// 功能選單-項目點擊
        /// </summary>
        private void MenuItem_Click(TMenuItem item)
        {
            if (item.Code == 0)
            {
                System.Windows.Forms.MessageBox.Show($"查無此視窗代碼 _# {item.Code}");
                return;
            }

            IEnumerable<Type> types = GetWindowTypes(item.TypeName);

            if (types.Any())
            {
                WindowFactory.Default.ShowMenuWindow(types.Single(), this, tag: item.Json, e: item.ShowMode);
            }
            else
            {
                System.Windows.Forms.MessageBox.Show($"查無此視窗代碼(型別檢查) _# {item.Code}");
            }
        }

        /// <summary>
        /// 用視窗代碼取得視窗型別清單
        /// </summary>
        /// <param name="windowTypeName">視窗型別代碼</param>
        /// <returns>視窗型別清單</returns>
        private IEnumerable<Type> GetWindowTypes(string windowTypeName)
        {
            // 從 Views 取得資料類型
            var typelist = this.GetType().Assembly.GetTypes()
                .Where(t => t.BaseType == typeof(Window) || t.BaseType == typeof(BaseWindow));

            // 取得符合的型別清單
            return from t in typelist where t.IsClass && t.Name == windowTypeName select t;
        }

        /// <summary>
        /// 功能選單-系統關閉
        /// </summary>
        private void SystemClose_Click(object sender, RoutedEventArgs e)
        {
            var dialogResult = MessageBox.Show("是否確認關閉程序?", "Close Confirmation", MessageBoxButton.YesNo);
            if (dialogResult == MessageBoxResult.Yes)
            {
                this.Close();
            }
        }

        #region ViewParting

        /// <summary>
        /// 刷新項目
        /// </summary>
        private void RefreshBrowserMenu()
        {
            AppendBrowserMenu(MnuBrowser, Win50100.ConfigFile);
        }

        /// <summary>
        /// 附加動態畫面分割群組
        /// </summary>
        private void AppendBrowserMenu(MenuItem main, string configFile)
        {
            main.Items.Clear();

            Config501 config = ConfigUtility.Read<Config501>(configFile);

            if (config == null || config.Nodes == null || config.Nodes.Count == 0)
            {
                return;
            }

            config.Nodes.ForEach(x =>
            {
                MenuItem menu = new MenuItem();
                menu.Header = x.Name;
                if (x.Members != null && x.Members.Count > 0)
                {
                    AppendViewPartingItems(menu, x.Members);
                }
                main.Items.Add(menu);
            });
        }

        /// <summary>
        /// 附加動態畫面分割項目
        /// </summary>
        private void AppendViewPartingItems(MenuItem menu, List<MenuNodeMember> members)
        {
            members.ForEach(x =>
            {
                if (x.ModeCode > 0 && !string.IsNullOrWhiteSpace(x.ConfigFile) && !string.IsNullOrWhiteSpace(x.Name))
                {
                    MenuItem sub = new MenuItem
                    {
                        Header = x.Name,
                        Tag = $"{x.ModeCode}|c:{x.ConfigFile}|v:{x.Name}|m:1"
                    };

                    sub.Click += MenuItem_Click;

                    menu.Items.Add(sub);
                }
            });
        }

        #endregion ViewParting
    }
}