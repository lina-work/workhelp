﻿using DingOK.Library;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Windows;

namespace WorkHelpWPF
{
    /// <summary></summary>
    public partial class win30700 : BaseWindow
    {
        string IniSectName = "win30700";
        T30700 PIniItem { set; get; }
        //-----------------------------系統原生函式------------------------------//
        public win30700()
        {
            InitializeComponent();
            btnSave.Click += btnSave_Click;
            btnExe.Click += btnExe_Click;
            btnClose.Click += base.winClose_Click;
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            PIniItem = Utility.FromConfig<T30700>(IniSectName);
            Utility.IntoUI<Window, T30700>(this, PIniItem);
        }
        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            Utility.FromUI<T30700, Window>(PIniItem, this);
            Utility.SaveConfig(PIniItem, IniSectName);
            System.Windows.Forms.MessageBox.Show("儲存成功");
        }
        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            Utility.ClearText<Window>(this);
        }
        private void btnExe_Click(object sender, RoutedEventArgs e)
        {
            this.Execute();
        }

        //-----------------------------使用者自訂函式-----------------------------//
        private void Execute()
        {
            var vInput = txtInput.Text.Trim();
            if (string.IsNullOrWhiteSpace(vInput))
            {
                System.Windows.Forms.MessageBox.Show("Input content error!");
            }
            else
            {
                var enfix = vInput.Substring(vInput.Length - 2, 2);
                if (enfix.ToUpper() != "RX") vInput += "Rx";

                var contents = new StringBuilder(256);

                contents.AppendLine(vInput);
                contents.AppendLine(vInput + ".en-US");
                contents.AppendLine(vInput + ".ja-JP");
                contents.AppendLine(vInput + ".zh-CN");

                contents.AppendLine("");
                contents.AppendLine("");
                contents.AppendLine(vInput + ".resx");
                contents.AppendLine(vInput + ".en-US.resx");
                contents.AppendLine(vInput + ".ja-JP.resx");
                contents.AppendLine(vInput + ".zh-CN.resx");

                txtOutput.Text = contents.ToString();

                var contents2 = new StringBuilder(256);
                contents2.AppendLine("zh-TW");
                contents2.AppendLine("en-US");
                contents2.AppendLine("ja-JP");
                contents2.AppendLine("zh-CN");
                txtOutput2.Text = contents2.ToString();
            }
        }

        #region Base UI Item
        private bool IsValid(T30700 item)
        {
            //if (StringUtl.IsNullOrEmptys(item.UrlTW))
            //    return false;

            return true;
        }

        class T30700
        {

        }
        #endregion Base Flow
    }

}
