﻿using DocumentFormat.OpenXml.Drawing;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using WorkHelp.Utilities;
using WorkHelp.Wpf.Enums;
using WorkHelp.Wpf.Enums.Extends;
using WorkHelp.Wpf.Extensions;
using WorkHelp.Wpf.Models;
using WorkHelp.Wpf.Models.Aras;
using WorkHelp.Wpf.Models.Windows;
using WorkHelp.Wpf.Services;
using WorkHelp.Wpf.Services.Win412;
using WorkHelp.Wpf.Utility;

namespace WorkHelp.Wpf.Views
{
    /// <summary>
    /// Aras DB Group 管理
    /// </summary>
    public partial class Win41300 : BaseWindow
    {
        /// <summary>
        /// 資料夾名稱
        /// </summary>
        private string FolderName { get; set; } = "Win41200";

        /// <summary>
        /// 建構元
        /// </summary>
        public Win41300()
        {
            InitializeComponent();
            BindEvents();
        }

        /// <summary>
        /// 繫結事件
        /// </summary>
        private void BindEvents()
        {
            this.Loaded += Window_Loaded;
            this.KeyDown += base.FWindow_KeyDown;
            this.BtnClose.Click += base.FWindowClose_Click;

            this.BtnSaveSetting.Click += this.BtnSaveSetting_Click;
            this.BtnDatabase.Click += this.BtnDatabase_Click;
            this.BtnTable.Click += this.BtnTable_Click;

            this.BtnExecute.Click += this.BtnExecute_Click;
            //this.BtnClear.Click += this.BtnClear_Click;
            this.BtnOpenFolder.Click += this.BtnOpenFolder_Click;

            this.BtnDocxTest.Click += this.BtnDocxTest_Click;
        }

        //-----------------------------系統原生函式------------------------------//

        /// <summary>
        /// 視窗載入
        /// </summary>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            var config = ConfigUtility.Read<Config412>();
            if (config != null)
            {
                TxtServer.SetText(config.ServerName);
                TxtUid.SetText(config.UserId);
                TxtPwd.SetText(config.Password);

                CbxDatabase.Items.Add(config.DatabaseName);
                CbxDatabase.SetText(config.DatabaseName);

                CbxTable.Items.Add(config.TableName);
                CbxTable.SetText(config.TableName);

                TxtKeyedName.SetText(config.KeyedName);
            }
            LoadMenu();

        }

        /// <summary>
        /// Docx 測試
        /// </summary>
        private void BtnDocxTest_Click(object sender, RoutedEventArgs e)
        {
            Service412.ExportDocx();
        }

        /// <summary>
        /// 儲存設定
        /// </summary>
        private void BtnSaveSetting_Click(object sender, RoutedEventArgs e)
        {
            ConfigUtility.Write(GetConfig());
            MessageBox.Show("儲存成功!!");
        }

        /// <summary>
        /// 載入資料庫
        /// </summary>
        private void BtnDatabase_Click(object sender, RoutedEventArgs e)
        {
            var config = GetConfig();
            if (!string.IsNullOrWhiteSpace(config.ServerName) && !string.IsNullOrWhiteSpace(config.UserId) && !string.IsNullOrWhiteSpace(config.Password))
            {
                var connString = ServiceUtility.MasterConnectionString(config);
                var source = Win401Service.GetDBList(connString);
                var list = source.Select(x => x.name).ToList();
                CbxDatabase.BindMenu(list);
            }
        }

        /// <summary>
        /// 載入資料表
        /// </summary>
        private void BtnTable_Click(object sender, RoutedEventArgs e)
        {
            var config = GetConfig();
            if (!string.IsNullOrWhiteSpace(config.ServerName) && !string.IsNullOrWhiteSpace(config.UserId) && !string.IsNullOrWhiteSpace(config.Password) && !string.IsNullOrWhiteSpace(config.DatabaseName))
            {
                var connString = ServiceUtility.GetConnectionString(config);
                var source = Win401Service.GetTableList(connString);
                var list = source.Select(x => x.name).ToList();
                CbxTable.BindMenu(list);
            }
        }

        /// <summary>
        /// 產生 Entity
        /// </summary>
        private void BtnExecuteTableFunc_Click(object sender, RoutedEventArgs e)
        {
            ExecuteTableFunction();
        }

        /// <summary>
        /// 執行
        /// </summary>
        private void BtnExecute_Click(object sender, RoutedEventArgs e)
        {
            ExecuteClick();
        }

        /// <summary>
        /// 清除
        /// </summary>
        private void BtnClear_Click(object sender, RoutedEventArgs e)
        {
            ClearView();
        }

        /// <summary>
        /// 開啟資料夾
        /// </summary>
        private void BtnOpenFolder_Click(object sender, RoutedEventArgs e)
        {
            FileUtility.Explorer(this.FolderName);
        }

        //-----------------------------使用者自訂函式-----------------------------//

        /// <summary>
        /// 載入功能選單
        /// </summary>
        private void LoadMenu()
        {
            CbxMethod.Items.Add("檢查問項 ID");

            CbxMethod.Text = "檢查問項 ID";
        }

        /// <summary>
        /// 清除畫面控件
        /// </summary>
        private void ClearView()
        {
            TxtInput1.Text = string.Empty;
            TxtInput2.Text = string.Empty;
            TxtOutput.Text = string.Empty;
            LblNotice.Content = "說明";
        }

        /// <summary>
        /// 點擊執行
        /// </summary>
        private void ExecuteClick()
        {
            string method = CbxMethod.Text;
            switch(method)
            {
                case "檢查問項 ID":
                    CheckSurveyID();
                    break;
            }
        }

        private void CheckSurveyID()
        {
            string contents = TxtInput1.Text;
            string jsons = TxtInput2.Text;

            if (string.IsNullOrWhiteSpace(contents) || string.IsNullOrWhiteSpace(jsons))
            {
                System.Windows.Forms.MessageBox.Show("參數錯誤");
                return;
            }

            string[] array = contents.Split(new char[] { '\n' }, StringSplitOptions.None);
            if (array == null || array.Length == 0)
            {
                System.Windows.Forms.MessageBox.Show("參數錯誤");
                return;
            }
            try 
            {
                StringBuilder builder = new StringBuilder();

                var jobj = JsonConvert.DeserializeObject<JObject>(jsons);

                List<string> fields = new List<string>();

                foreach (JToken child in jobj.Children())
                {
                    var property = child as JProperty;

                    if (property != null)
                    {
                        fields.Add(property.Name);
                    }

                }

                foreach (string field_name in fields)
                {
                    string field_value = GetJsonValue(jobj, field_name);

                    bool isExists = false;
                    foreach(string row in array)
                    {
                        string[] cols = row.Split(new char[] { ' ' }, StringSplitOptions.None);
                        if (cols == null || cols.Length == 0)
                        {
                            continue;
                        }

                        string survey_id = cols[0];
                        if (survey_id.Equals(field_name, StringComparison.CurrentCultureIgnoreCase))
                        {
                            isExists = true;
                        }
                    }

                    if (!isExists)
                    {
                        builder.AppendLine(field_name + ": " + field_value + " 不存在");
                    }
                }

                TxtOutput.Text = builder.ToString();

            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);

            }
        }
        
        private static string GetJsonValue(Newtonsoft.Json.Linq.JObject obj, string key)
        {
            if (obj == null || string.IsNullOrWhiteSpace(key))
            {
                return "";
            }

            Newtonsoft.Json.Linq.JToken value;
            if (obj.TryGetValue(key, out value))
            {
                return value.ToString();
            }
            return "";
        }
        /// <summary>
        /// 執行 Function
        /// </summary>
        private void FunctionExecute(Config412 config)
        {
            switch (config.ArasTypeFunction)
            {
                case ArasTypeFunction.ExportProperySummary:
                    config.FolderName = $"ApiScripts\\Multi\\DatabaseGroup\\{config.ServerFolderName}";
                    config.FileName = $"ExportProperty Summary 結果.md";
                    config.FileContent = Service412.Main(config, Service412.ExportProperySummary);
                    break;

                case ArasTypeFunction.ExportMethod:
                    config.FolderName = $"ApiScripts\\Multi\\DatabaseGroup\\{config.ServerFolderName}";
                    config.FileName = $"ExportMethod 結果.md";
                    config.FileContent = Service412.Main(config, Service412.ExportMethod);
                    break;

                case ArasTypeFunction.Scalar:
                    config.FolderName = $"ApiScripts\\Multi\\DatabaseGroup\\{config.ServerFolderName}";
                    config.FileName = $"Scalar 結果.md";
                    config.FileContent = Service412.Main(config, Service412.Scalar);
                    break;
            }


            if (config.IsExcuted)
            {
                FileUtility.WriteFile(config.FolderName, config.FileName, config.FileContent);
                System.Windows.Forms.MessageBox.Show($"執行總秒數: {config.ExecuteTimeSpan.TotalSeconds}");
            }
        }

        /// <summary>
        /// 取得組態檔
        /// </summary>
        /// <returns></returns>
        private Config412 GetConfig()
        {
            var result = new Config412
            {
                ServerName = TxtServer.StringValue(),
                UserId = TxtUid.StringValue(),
                Password = TxtPwd.StringValue(),
                DatabaseName = CbxDatabase.StringValue(),
                TableName = CbxTable.StringValue(),

                ArasType = (ArasType)CbxType.SelectedIntValue(),
                KeyedName = TxtKeyedName.StringValue(),

                FileContent = string.Empty,
            };

            result.ServerFolderName = result.ServerName.Replace("\\", "_");
            result.StorageList = new List<Storage>();

            var array = result.DbGroup.Split(new char[] { ',' }, System.StringSplitOptions.RemoveEmptyEntries);
            if (array != null && array.Length > 0)
            {
                foreach (var db in array)
                {
                    var storage = new Storage
                    {
                        DatabaseName = db,
                        ConnectionString = ServiceUtility.GetArasConnectionString(result, db)
                    };
                    result.StorageList.Add(storage);
                }
            }

            return result;
        }

        #region Database

        /// <summary>
        /// 執行 Table Function
        /// </summary>
        private void ExecuteTableFunction()
        {
            var config = GetConfig();
            var wModel = TransformTo401(config);
            if (!wModel.IsValid)
            {
                System.Windows.Forms.MessageBox.Show("資料有誤，請重新檢查");
                return;
            }

            var isExecute = true;
            var sModel = Win401Service.CreateSchemaModel(wModel);
            switch (config.ArasType)
            {
                case ArasType.Method:
                    //GenerateEntity(wModel, sModel);
                    break;

                default:
                    isExecute = false;
                    System.Windows.Forms.MessageBox.Show("請選擇 Aras Type");
                    break;
            }

            if (isExecute)
            {
                FileUtility.WriteFile(this.FolderName, sModel.FileName, sModel.FileContent);
            }
        }

        /// <summary>
        /// 產生 Entity
        /// </summary>
        private void GenerateEntity(Win401Model wModel, SchemaModel sModel)
        {
            var eModel = Win401Service.NewEntityModel(wModel);

            sModel.FileName = $"{sModel.MainName}.cs";
            Win401EntityService.GenerateContent(sModel, eModel);
        }

        #region Columns

        /// <summary>
        /// 產生欄位清單
        /// </summary>
        private void GenerateColumns(Win401Model wModel, SchemaModel sModel)
        {
            sModel.FileName = $"{sModel.MainName}_Columns.txt";
            Service102.GenerateColumns(sModel);
        }

        #endregion Columns

        /// <summary>
        /// 產生查詢指令
        /// </summary>
        private void GenerateSelectScript(Win401Model wModel, SchemaModel sModel)
        {
            sModel.FileName = $"{sModel.MainName}_SelectScriipt.txt";
            Service102.GenerateSelectScript(sModel);
        }

        /// <summary>
        /// 產生 Entity
        /// </summary>
        private Win401Model TransformTo401(Config412 config)
        {
            var result = new Win401Model
            {
                ConnectionString = ServiceUtility.GetConnectionString(config),
                TableName = config.TableName,
                FolderName = this.FolderName,
                NameSpaceName = "Aras.Innosoft",
                MainName = config.TableName.Replace("_", string.Empty),
            };

            result.IsEasyName = false;

            if (string.IsNullOrWhiteSpace(result.ConnectionString)
                || string.IsNullOrWhiteSpace(result.TableName)
                || string.IsNullOrWhiteSpace(result.FolderName))
            {
                System.Windows.Forms.MessageBox.Show("資料設定有誤，請重新設定");
                result.IsValid = false;
            }
            else
            {
                result.IsValid = true;
            }
            return result;
        }

        #endregion Database
    }
}