﻿using DingOK.Library;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Windows;

namespace WorkHelpWPF
{
    /// <summary></summary>
    public partial class win30800 : BaseWindow
    {
        string IniSectName = "win30800";
        T30800 PIniItem { set; get; }
        //-----------------------------系統原生函式------------------------------//
        public win30800()
        {
            InitializeComponent();
            btnSave.Click += btnSave_Click;
            btnExe.Click += btnExe_Click;
            btnFolder.Click += btnFolder_Click;
            btnClose.Click += base.winClose_Click;
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            PIniItem = Utility.FromConfig<T30800>(IniSectName);
            Utility.IntoUI<Window, T30800>(this, PIniItem);
        }
        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            Utility.FromUI<T30800, Window>(PIniItem, this);
            Utility.SaveConfig(PIniItem, IniSectName);
            System.Windows.Forms.MessageBox.Show("儲存成功");
        }
        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            Utility.ClearText<Window>(this);
        }
        private void btnFolder_Click(object sender, RoutedEventArgs e)
        {
            var dir = txtPath.Text.Trim();
            if (!string.IsNullOrWhiteSpace(dir))
            {
                FileUtl.openFolderFile(dir);
            }
        }
        private void btnExe_Click(object sender, RoutedEventArgs e)
        {
            //this.Past();
            this.Execute();
        }

        //-----------------------------使用者自訂函式-----------------------------//
        private void Execute()
        {
            Utility.FromUI<T30800, Window>(PIniItem, this);
            if (!IsValid(PIniItem)) return;

            PIniItem.CreateTime = DateTime.Now;
            PIniItem.KeyValue = ArtTks.Entity.GetRandomStr().GetValue(PIniItem.KeyLen);

            txtKeyValue.Text = PIniItem.KeyValue;

            if (!Directory.Exists(PIniItem.Path))
            {
                Directory.CreateDirectory(PIniItem.Path);
            }
            if (Directory.Exists(PIniItem.Path))
            {
                var path = Path.Combine(PIniItem.Path, PIniItem.File);
                File.AppendAllText(path, JsonConvert.SerializeObject(PIniItem) + "\r\n");
                System.Windows.Forms.MessageBox.Show("finished.");
            }
            else
            {
                System.Windows.Forms.MessageBox.Show("檔案路徑建立時發生錯誤.");
            }
        }


        #region Base UI Item
        private bool IsValid(T30800 item)
        {
            if (StringUtl.IsNullOrEmptys(item.KeyName, item.KeyDesc, item.Path, item.File) || item.KeyLen <= 0)
            {
                System.Windows.Forms.MessageBox.Show("資料有誤，請重新檢查！");
                return false;
            }
            return true;
        }

        /// <summary>[金鑰]資料存取</summary>
        [JsonObject(MemberSerialization.OptIn)]
        class T30800
        {
            /// <summary>金鑰名稱</summary>
            [JsonProperty]
            public string KeyName { get; set; }

            /// <summary>金鑰說明</summary>
            [JsonProperty]
            public string KeyDesc { get; set; }

            /// <summary>金鑰長度</summary>
            [JsonProperty]
            public int KeyLen { get; set; }

            /// <summary>金鑰密碼</summary>
            [JsonProperty]
            public string KeyValue { get; set; }

            /// <summary>存放資料夾</summary>
            public string Path { get; set; }

            /// <summary>存放檔名</summary>
            public string File { get; set; }

            /// <summary>金鑰產生時間</summary>
            [JsonProperty]
            public DateTime CreateTime { get; set; }
        }
        #endregion Base Flow
    }

}
