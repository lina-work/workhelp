﻿using NLog.Filters;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using WorkHelp.Wpf.Models.Windows;
using WorkHelp.Wpf.Utility;

namespace WorkHelp.Wpf.Views
{
    /// <summary>
    /// Icon 處理
    /// </summary>
    public partial class Win10600 : BaseWindow
    {
        //-----------------------------系統原生函式------------------------------//
        public Win10600()
        {
            InitializeComponent();
            this.Loaded += this.Window_Loaded;

            this.KeyDown += base.FWindow_KeyDown;
            this.BtnClose.Click += base.FWindowClose_Click;

            BtnSaveSetting.Click += BtnSaveSetting_Click;

            BtnExecute.Click += BtnExecute_Click;
            BtnClear.Click += BtnClear_Click;
        }

        /// <summary>
        /// 視窗載入
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Config106 config = ConfigUtility.Read<Config106>();
            TxtOriginName.Text = config.OriginName;
            TxtTargetName.Text = config.TargetName;
            TxtIconPath.Text = config.IconPath;
        }

        /// <summary>
        /// 儲存組態
        /// </summary>
        private void BtnSaveSetting_Click(object sender, RoutedEventArgs e)
        {
            ConfigUtility.Write(GetConfig());
            System.Windows.Forms.MessageBox.Show("Finished!!");
        }

        /// <summary>
        /// 清除
        /// </summary>
        private void BtnClear_Click(object sender, RoutedEventArgs e)
        {
            TxtOriginName.Text = string.Empty;
            TxtTargetName.Text = string.Empty;
            TxtIconPath.Text = string.Empty;
            TxtOutput.Text = string.Empty;
        }

        /// <summary>
        /// 產生 Markdown 內容
        /// </summary>
        private void BtnExecute_Click(object sender, RoutedEventArgs e)
        {
            Execute();
        }

        //-----------------------------使用者自訂函式-----------------------------//

        /// <summary>
        /// 將 UI 輸入控件資料轉為組態物件
        /// </summary>
        /// <returns></returns>
        private Config106 GetConfig()
        {
            return new Config106
            {
                OriginName = TxtOriginName.Text.Trim(),
                TargetName = TxtTargetName.Text.Trim(),
                IconPath = TxtIconPath.Text.Trim()
            };
        }

        /// <summary>
        /// 執行
        /// </summary>
        private void Execute()
        {
            var config = GetConfig();

            if (string.IsNullOrWhiteSpace(config.OriginName) || string.IsNullOrWhiteSpace(config.TargetName) || string.IsNullOrWhiteSpace(config.IconPath))
            {
                System.Windows.Forms.MessageBox.Show("請輸入參數!!");
                return;
            }
            if (!Directory.Exists(config.IconPath))
            {
                System.Windows.Forms.MessageBox.Show("Icon 資料夾不存在!!");
                return;
            }

            config.OldIconPath = config.IconPath.TrimEnd('\\');
            config.NewIconPath = config.IconPath.TrimEnd('\\') + "_new_" + DateTime.Now.ToString("yyyyMMdd_HHmmss");
            config.NewIconPath = config.NewIconPath.Replace(config.OriginName, config.TargetName);

            if (!Directory.Exists(config.NewIconPath))
            {
                Directory.CreateDirectory(config.NewIconPath);
            }

            var baseDir = new DirectoryInfo(config.IconPath);

            ExecuteCopy(baseDir, config);

            System.Windows.Forms.MessageBox.Show("Finished!!");

            TxtOutput.Text = Newtonsoft.Json.JsonConvert.SerializeObject(config);
        }

        private void ExecuteCopy(DirectoryInfo dir, Config106 config)
        {
            var files = dir.GetFiles();

            if (files != null && files.Length > 0)
            {
                foreach (var file in files)
                {
                    string new_name = file.FullName.Replace(config.OldIconPath, config.NewIconPath).Replace(config.OriginName, config.TargetName);

                    if (File.Exists(new_name))
                    {
                        File.Delete(new_name);
                    }

                    file.CopyTo(new_name);
                }
            }

            var subDirs = dir.GetDirectories();
            if (subDirs != null && subDirs.Length > 0)
            {
                foreach (var subDir in subDirs)
                {
                    ExecuteCopy(subDir, config);
                }
            }
        }
    }
}