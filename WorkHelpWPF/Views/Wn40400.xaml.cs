﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using WorkHelpWPF.Extension;

namespace WorkHelpWPF
{
    /// <summary></summary>
    public partial class win40400 : BaseWindow
    {
        //-----------------------------系統原生函式------------------------------//
        public win40400()
        {
            InitializeComponent();
            btnSave.Click += btnSave_Click;
            btnClose.Click += base.winClose_Click;
            btnDetection.Click += btnDetection_Click;
            btnAutoClick.Click += btnAutoClick_Click;
            btnSecondClick.Click += btnSecondClick_Click;
            btnDrag.Click += btnDrag_Click;
            btnAuthorize.Click += btnAuthorize_Click;
            btnDragBack.Click += btnDragBack_Click;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
        }

        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
        }

        private void btnDetection_Click(object sender, RoutedEventArgs e)
        {
            KeepReportMousePos();
        }

        private void btnAutoClick_Click(object sender, RoutedEventArgs e)
        {
            int x = IntVal(txtNowX);
            int y = IntVal(txtNowY);
            int inc = IntVal(txtIncValue);
            int count = IntVal(txtCount);

            if (x == int.MinValue || y == int.MinValue || count == int.MinValue || inc == int.MinValue)
            {
                System.Windows.Forms.MessageBox.Show("Position is error");
            }
            else
            {
                ExecuteAutoClick(x, y, inc, count);
            }
        }

        private void btnAuthorize_Click(object sender, RoutedEventArgs e)
        {
            Clipboard.SetDataObject("17d42ac5-be29-4338-aff8-0944a112ed94");

            // 點擊右上角
            MyMouse.MoveToClick(-322, 165);
            Thread.Sleep(300);

            // 點擊 Available authorizations Value Input
            MyMouse.MoveToClick(-1220, 625);
            Thread.Sleep(300);

            // 呼叫選單
            MyMouse.RightClick();
            Thread.Sleep(300);

            // 執行貼上
            MyMouse.MoveToClick(-1128, 753);
            Thread.Sleep(300);

            // 進行授權
            MyMouse.MoveToClick(-1037, 669);
            Thread.Sleep(300);

            // Close
            MyMouse.MoveToClick(-892, 669);
        }

        private void btnDragBack_Click(object sender, RoutedEventArgs e)
        {
            int x = IntVal(txtScrollbarX);
            int y = IntVal(txtScrollbarY);
            int newX = IntVal(txtScrollbarNewX);
            int newY = IntVal(txtScrollbarNewY);
            int sleepMiniSecond = IntVal(txtSleepMiniSecond);
            MyMouse.DragTo(newX, newY, x, y, sleepMiniSecond);
        }

        private void btnSecondClick_Click(object sender, RoutedEventArgs e)
        {
            int x = IntVal(txtNowX);
            int y = IntVal(txtNowY);
            int inc = IntVal(txtIncValue);
            int count = IntVal(txtCount2);

            if (x == int.MinValue || y == int.MinValue || count == int.MinValue || inc == int.MinValue)
            {
                System.Windows.Forms.MessageBox.Show("Position is error");
            }
            else
            {
                ExecuteAutoClick(x, y, inc, count);
            }
        }

        private void btnDrag_Click(object sender, RoutedEventArgs e)
        {
            int x = IntVal(txtScrollbarX);
            int y = IntVal(txtScrollbarY);
            int newX = IntVal(txtScrollbarNewX);
            int newY = IntVal(txtScrollbarNewY);
            int sleepMiniSecond = IntVal(txtSleepMiniSecond);
            MyMouse.DragTo(x, y, newX, newY, sleepMiniSecond);
        }

        private int IntVal(TextBox ui)
        {
            string val = ui.Text.Trim();
            int result = int.MinValue;

            if (int.TryParse(val, out result))
            {
                return result;
            }
            else
            {
                return int.MinValue;
            }
        }

        //-----------------------------使用者自訂函式-----------------------------//

        private void KeepReportMousePos()
        {
            Task.Factory.StartNew(() =>
            {
                while (true)
                {
                    this.Dispatcher.Invoke(
                        DispatcherPriority.SystemIdle,
                        new Action(() =>
                        {
                            GetCursorPos();
                        }));
                }
            });
        }

        private void GetCursorPos()
        {
            //get the mouse position and show on the TextBlock
            System.Drawing.Point p = System.Windows.Forms.Cursor.Position;
            txtPositionX.Text = p.X + " ";
            txtPositionY.Text = p.Y + " ";
        }

        private void Window_OnMouseWheel(object sender, MouseWheelEventArgs e)
        {
            //invoke mouse position detect when wheel the mouse
            KeepReportMousePos();
        }

        private void ExecuteAutoClick(int x, int y, int inc, int count)
        {
            Task.Factory.StartNew(() =>
            {
                for (int i = 0; i < count; i++)
                {
                    this.Dispatcher.Invoke(
                        DispatcherPriority.SystemIdle,
                        new Action(() =>
                        {
                            int sumInc = i * inc;
                            MyMouse.MoveToClick(x, y + sumInc);
                            Thread.Sleep(200);

                            tbkCount.Text = (i + 1).ToString();
                        }));
                }
            });
        }
    }
}