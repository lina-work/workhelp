﻿using RestSharp;
using System;
using System.Windows;

namespace WorkHelp.Wpf.Views
{
    /// <summary>
    /// HTTP 測試
    /// </summary>
    public partial class Win60200 : BaseWindow
    {
        public Win60200()
        {
            InitializeComponent();
            BindEvents();
        }

        /// <summary>
        /// 繫結事件
        /// </summary>
        private void BindEvents()
        {
            this.Loaded += Window_Loaded;
            this.KeyDown += base.FWindow_KeyDown;
            this.BtnClose.Click += base.FWindowClose_Click;
            this.BtnCreateAccount.Click += BtnCreateAccount_Click;
        }

        //-----------------------------系統原生函式------------------------------//

        /// <summary>
        /// 視窗載入
        /// </summary>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            var server = "http://54.88.219.103";

            TxtServer.Text = server;

            TxtMemo.Text = "{\"client\":\"輸入的帳號\",\"error_code\":\"OK\",\"message\":\"ok\"} 為 PHP 建立帳號成功"
                        + "\r\n\r\n"
                        + "{\"d\": \"1000\"} 為 ASPX 建立帳號成功"
                        + "\r\n\r\n"
                        + $"兩者成功後，即可至 Server 【{server}】登入";
        }

        //-----------------------------使用者自訂函式-----------------------------//

        /// <summary>
        /// 建立帳號
        /// </summary>
        private void BtnCreateAccount_Click(object sender, RoutedEventArgs e)
        {
            CreateAccount();
        }

        /// <summary>
        /// 建立帳號
        /// </summary>
        private void CreateAccount()
        {
            var server = TxtServer.Text.Trim();

            var objPhp = GetNewAccount();

            var objAspx = NewAccountFromPhp(objPhp);

            if (string.IsNullOrWhiteSpace(server) || string.IsNullOrWhiteSpace(objPhp.client) || string.IsNullOrWhiteSpace(objPhp.pwd))
            {
                System.Windows.Forms.MessageBox.Show("請輸入資料!!");
            }
            else
            {
                TxtOutput.Text = string.Empty;
                Create_Account_PHP(server, objPhp);
                Create_Account_Aspx(server, objAspx);
            }
        }

        private void Create_Account_PHP(string server, NewAccountPhp obj)
        {
            try
            {
                var json = Newtonsoft.Json.JsonConvert.SerializeObject(obj);

                var client = new RestClient(server);

                var request = new RestRequest("/phpapi/check_or_create.php", Method.POST);
                request.AddHeader("Content-Type", "application/json");
                request.AddJsonBody(json);

                IRestResponse response = client.Execute(request);
                var content = response.Content;

                TxtOutput.Text = content + "\r\n" + TxtOutput.Text;
                System.Windows.Forms.MessageBox.Show(content);
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);

                TxtException.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff")
                    + ": " + ex.ToString() + "\r\n"
                    + TxtException.Text;
            }
        }

        private void Create_Account_Aspx(string server, NewAccountAspx obj)
        {
            try
            {
                var json = Newtonsoft.Json.JsonConvert.SerializeObject(obj);

                var client = new RestClient(server);

                var request = new RestRequest("/Registered.aspx/UserCreat", Method.POST);
                request.AddHeader("Content-Type", "application/json");
                request.AddJsonBody(json);

                IRestResponse response = client.Execute(request);
                var content = response.Content;

                TxtOutput.Text = content + "\r\n" + TxtOutput.Text;
                System.Windows.Forms.MessageBox.Show(content);
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);

                TxtException.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff")
                    + ": " + ex.ToString() + "\r\n"
                    + TxtException.Text;
            }
        }

        private NewAccountPhp GetNewAccount()
        {
            string account = TxtAccount.Text.Trim();
            string password = TxtPassword.Text.Trim();

            return new NewAccountPhp
            {
                client = account,
                pwd = password,
                vipHandicaps = 12,
                orHandicaps = 1,
                orHallRebate = 0
            };
        }

        private NewAccountAspx NewAccountFromPhp(NewAccountPhp account)
        {
            return new NewAccountAspx
            {
                id = account.client,
                pass = account.pwd,
                gift = "admin",
                _ip = "127.0.0.1"
            };
        }
    }

    public class NewAccountPhp
    {
        public string client { get; set; }
        public string pwd { get; set; }
        public int vipHandicaps { get; set; }
        public int orHandicaps { get; set; }
        public int orHallRebate { get; set; }
    }

    public class NewAccountAspx
    {
        public string id { get; set; }
        public string pass { get; set; }
        public string gift { get; set; }
        public string _ip { get; set; }
    }
}