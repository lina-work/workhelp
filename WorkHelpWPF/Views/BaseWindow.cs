﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using WorkHelp.Wpf.Extensions;

namespace WorkHelp.Wpf.Views
{
    /// <summary>共用資源管理</summary>
    public partial class BaseWindow : Window
    {
        protected void FWindowClose_Click(object sender, RoutedEventArgs e)
        {
            if (this.DialogResult != null) this.DialogResult = false;
            this.Close();
        }

        protected void FWindow_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key != Key.Escape) return;
            if (this.DialogResult != null) this.DialogResult = false;
            this.Close();
        }

        ///// <summary>
        ///// DotNetBrowser 關閉
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="e"></param>
        //protected void FWindow_DotNetBrowser_Closing(object sender, CancelEventArgs e)
        //{
        //    Window window = (Window)sender;
        //    DisposeDotNetBrowser(window, "BrowserTopLeft");
        //    DisposeDotNetBrowser(window, "BrowserTopCenter");
        //    DisposeDotNetBrowser(window, "BrowserTopRight");

        //    DisposeDotNetBrowser(window, "BrowserMiddleLeft");
        //    DisposeDotNetBrowser(window, "BrowserMiddleCenter");
        //    DisposeDotNetBrowser(window, "BrowserMiddleRight");

        //    DisposeDotNetBrowser(window, "BrowserBottomLeft");
        //    DisposeDotNetBrowser(window, "BrowserBottomCenter");
        //    DisposeDotNetBrowser(window, "BrowserBottomRight");
        //}

        ///// <summary>
        ///// 關閉 DotNetBrowser
        ///// </summary>
        //protected void DisposeDotNetBrowser(Window window, string viewName)
        //{
        //    DotNetBrowser.WPF.WPFBrowserView view = WinExtension.FindChild<DotNetBrowser.WPF.WPFBrowserView>(window, viewName);
        //    if (view != null)
        //    {
        //        if (view.Browser != null)
        //        {
        //            view.Browser.Dispose();
        //        }
        //        view.Dispose();
        //    }
        //}
    }
}