﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using WorkHelp.Utilities;
using WorkHelp.Wpf.Enums;
using WorkHelp.Wpf.Enums.Extends;
using WorkHelp.Wpf.Extensions;
using WorkHelp.Wpf.Models;
using WorkHelp.Wpf.Models.Windows;
using WorkHelp.Wpf.Services;
using WorkHelp.Wpf.Services.Win411;
using WorkHelp.Wpf.Utility;

namespace WorkHelp.Wpf.Views
{
    /// <summary>
    /// Aras 處理
    /// </summary>
    public partial class Win41100 : BaseWindow
    {
        /// <summary>
        /// 資料夾名稱
        /// </summary>
        private string FolderName { get; set; } = "Win41100";

        /// <summary>
        /// 建構元
        /// </summary>
        public Win41100()
        {
            InitializeComponent();
            BindEvents();
        }

        /// <summary>
        /// 繫結事件
        /// </summary>
        private void BindEvents()
        {
            this.Loaded += Window_Loaded;
            this.KeyDown += base.FWindow_KeyDown;
            this.BtnClose.Click += base.FWindowClose_Click;

            this.BtnSaveSetting.Click += this.BtnSaveSetting_Click;
            this.BtnDatabase.Click += this.BtnDatabase_Click;
            this.BtnTable.Click += this.BtnTable_Click;
            this.BtnExecuteTableFunc.Click += this.BtnExecuteTableFunc_Click;

            this.BtnSample.Click += this.BtnSample_Click;
            this.BtnExecute.Click += this.BtnExecute_Click;
            this.BtnClear.Click += this.BtnClear_Click;
            this.BtnOpenFolder.Click += this.BtnOpenFolder_Click;

            this.CbxFunction.SelectionChanged += this.CbxFunction_SelectionChanged;
        }

        //-----------------------------系統原生函式------------------------------//

        /// <summary>
        /// 視窗載入
        /// </summary>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            var config = ConfigUtility.Read<Config411>();
            if (config != null)
            {
                TxtServer.SetText(config.ServerName);
                TxtUid.SetText(config.UserId);
                TxtPwd.SetText(config.Password);

                CbxDatabase.Items.Add(config.DatabaseName);
                CbxDatabase.SetText(config.DatabaseName);

                CbxTable.Items.Add(config.TableName);
                CbxTable.SetText(config.TableName);

                TxtTargetName.SetText(config.TargetName);
                TxtFormName.SetText(config.FormName);
            }
            HiddenCondition();
            LoadMenu();
        }

        /// <summary>
        /// 儲存設定
        /// </summary>
        private void BtnSaveSetting_Click(object sender, RoutedEventArgs e)
        {
            ConfigUtility.Write(GetConfig());
            MessageBox.Show("儲存成功!!");
        }

        /// <summary>
        /// 載入資料庫
        /// </summary>
        private void BtnDatabase_Click(object sender, RoutedEventArgs e)
        {
            var config = GetConfig();
            if (!string.IsNullOrWhiteSpace(config.ServerName) && !string.IsNullOrWhiteSpace(config.UserId) && !string.IsNullOrWhiteSpace(config.Password))
            {
                var connString = ServiceUtility.MasterConnectionString(config);
                var source = Win401Service.GetDBList(connString);
                var list = source.Select(x => x.name).ToList();
                CbxDatabase.BindMenu(list);
            }
        }

        /// <summary>
        /// 載入資料表
        /// </summary>
        private void BtnTable_Click(object sender, RoutedEventArgs e)
        {
            var config = GetConfig();
            if (!string.IsNullOrWhiteSpace(config.ServerName) && !string.IsNullOrWhiteSpace(config.UserId) && !string.IsNullOrWhiteSpace(config.Password) && !string.IsNullOrWhiteSpace(config.DatabaseName))
            {
                var connString = ServiceUtility.GetConnectionString(config);
                var source = Win401Service.GetTableList(connString);
                var list = source.Select(x => x.name).ToList();
                CbxTable.BindMenu(list);
            }
        }

        /// <summary>
        /// 產生 Entity
        /// </summary>
        private void BtnExecuteTableFunc_Click(object sender, RoutedEventArgs e)
        {
            ExecuteTableFunction();
        }

        /// <summary>
        /// 功能選項變更
        /// </summary>
        private void CbxFunction_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //ClearView();
            HiddenCondition();
            if (CbxFunction.SelectedIndex > -1)
            {
                ReplaceFunction type = (ReplaceFunction)CbxFunction.SelectedIntValue();
                LblNotice.Content = ReplaceFunctionExtend.GetMenuNotice(type);
                switch (type)
                {
                    case ReplaceFunction.EqualSignExchange:
                        ChkTrim.Visibility = Visibility.Visible;
                        break;

                    case ReplaceFunction.CSharpClass:
                    case ReplaceFunction.CSharpEnum:
                        ChkTrim.Visibility = Visibility.Visible;
                        break;

                    case ReplaceFunction.MarkdownToHtml:
                        ChkDisabled.Visibility = Visibility.Visible;
                        break;

                    case ReplaceFunction.IonicFormSnippets:
                    case ReplaceFunction.IonicFormSnippetsFromInterface:
                        LblFormName.Visibility = Visibility.Visible;
                        TxtFormName.Visibility = Visibility.Visible;
                        break;

                    case ReplaceFunction.Format:
                        ChkBrassBracket.Visibility = Visibility.Visible;
                        LblPrefix.Visibility = Visibility.Visible;
                        TxtPrefix.Visibility = Visibility.Visible;
                        break;

                    default:
                        break;
                }
            }
            else
            {
                LblNotice.Content = string.Empty;
            }
        }

        /// <summary>
        /// 觀看範例
        /// </summary>
        private void BtnSample_Click(object sender, RoutedEventArgs e)
        {
            ReplaceClick(isLoadSample: true);
        }

        /// <summary>
        /// 執行置換
        /// </summary>
        private void BtnExecute_Click(object sender, RoutedEventArgs e)
        {
            ReplaceClick(isLoadSample: false);
        }

        /// <summary>
        /// 清除
        /// </summary>
        private void BtnClear_Click(object sender, RoutedEventArgs e)
        {
            ClearView();
        }

        /// <summary>
        /// 開啟資料夾
        /// </summary>
        private void BtnOpenFolder_Click(object sender, RoutedEventArgs e)
        {
            FileUtility.Explorer(this.FolderName);
        }

        //-----------------------------使用者自訂函式-----------------------------//

        /// <summary>
        /// 載入功能選單
        /// </summary>
        private void LoadMenu()
        {
            CbxTableFunction.BindMenu(ArasTableFunctionExtend.GetMenuList());
        }

        /// <summary>
        /// 清除畫面控件
        /// </summary>
        private void ClearView()
        {
            ChkTrim.IsChecked = true;
            ChkGenerateClass.IsChecked = true;

            //TxtSample.Text = string.Empty;
            //TxtInput.Text = string.Empty;
            TxtOutput.Text = string.Empty;
            LblNotice.Content = "說明";
        }

        /// <summary>
        /// 隱藏附加條件
        /// </summary>
        private void HiddenCondition()
        {
            ChkTrim.Visibility = Visibility.Collapsed;
            ChkGenerateClass.Visibility = Visibility.Collapsed;
            ChkDisabled.Visibility = Visibility.Collapsed;
            LblFormName.Visibility = Visibility.Collapsed;
            TxtFormName.Visibility = Visibility.Collapsed;

            ChkBrassBracket.Visibility = Visibility.Collapsed;
            LblPrefix.Visibility = Visibility.Collapsed;
            TxtPrefix.Visibility = Visibility.Collapsed;
        }

        /// <summary>
        /// 點擊執行置換
        /// </summary>
        private void ReplaceClick(bool isLoadSample = false)
        {
            //if (!isLoadSample && string.IsNullOrWhiteSpace(TxtInput.Text.Trim()))
            //{
            //    // 不載入範例又沒有輸入內容
            //    System.Windows.Forms.MessageBox.Show("請輸入要置換的內容");
            //    return;
            //}

            //var config = GetConfig(isLoadSample: isLoadSample);

            //ReplaceFunctionExtend.Config(config);

            //if (config.IsExecute)
            //{
            //    ReplaceExecute(config);
            //}
        }

        /// <summary>
        /// 取得組態檔
        /// </summary>
        /// <returns></returns>
        private Config411 GetConfig(bool isLoadSample = false)
        {
            return new Config411
            {
                ServerName = TxtServer.StringValue(),
                UserId = TxtUid.StringValue(),
                Password = TxtPwd.StringValue(),
                DatabaseName = CbxDatabase.StringValue(),
                TableName = CbxTable.StringValue(),
                //NameSpace = TxtNameSpace.StringValue(),
                TargetName = TxtTargetName.StringValue(),
                ArasTableFunctionType = (ArasTableFunction)CbxTableFunction.SelectedIntValue(),

                //ReplaceType = (ReplaceFunction)CbxFunction.SelectedIntValue(),
                IsExecute = true,
                IsLoadSample = isLoadSample,
                IsCheckTrim = ChkTrim.BoolValue(),
                IsGenerateClass = ChkGenerateClass.BoolValue(),
                IsDisabled = ChkDisabled.BoolValue(),
                FormName = TxtFormName.StringValue(),

                IsCheckFormat = ChkBrassBracket.BoolValue(),
                DefaultValue = TxtPrefix.StringValue(),
                Command = TxtInput.StringValue(),
            };
        }

        /// <summary>
        /// 轉換配置
        /// </summary>
        private void ReplaceExecute(Config102 config)
        {
            //if (config.IsLoadSample && string.IsNullOrWhiteSpace(config.SampleFile))
            //{
            //    System.Windows.Forms.MessageBox.Show("未設定範例檔");
            //    return;
            //}

            //if (config.Transform == null)
            //{
            //    System.Windows.Forms.MessageBox.Show("未設定轉換函式");
            //    return;
            //}

            //if (string.IsNullOrWhiteSpace(config.OutputFile))
            //{
            //    config.OutputFile = config.SampleFile;
            //}

            //if (config.IsLoadSample)
            //{
            //    TxtSample.Text = ConfigUtility.SampleFileContents("Win41100", config.SampleFile);
            //    TxtInput.Text = TxtSample.Text;
            //}

            //string inputContents = TxtInput.Text;
            //string outputContents = config.Transform(inputContents, config);

            //TxtOutput.Text = outputContents;
            //if (string.IsNullOrWhiteSpace(config.OutputFile))
            //{
            //    System.Windows.Forms.MessageBox.Show("未設定輸出檔名稱");
            //}
            //else
            //{
            //    FileUtility.WriteFile(this.FolderName, config.OutputFile, outputContents);
            //}
        }

        #region Database

        /// <summary>
        /// 執行 Table Function
        /// </summary>
        private void ExecuteTableFunction()
        {
            var config = GetConfig();
            var wModel = TransformTo401(config);

            var isExecute = true;
            var sModel = Win401Service.CreateSchemaModel(wModel);

            DateTime dtS = DateTime.Now;
            switch (config.ArasTableFunctionType)
            {
                case ArasTableFunction.AnalyzeItemType:
                    sModel.FolderName = "ApiScripts\\Multi\\IACT-CLA";
                    sModel.FileName = $"物件類型分析.md";
                    sModel.FileContent = Win411Service.AnalyzeItemType(wModel, sModel);
                    break;

                case ArasTableFunction.IgnoreMethod:
                    sModel.FolderName = "ApiScripts\\Multi\\IACT-CLA";
                    sModel.FileName = $"Ignore_Method.txt";
                    sModel.FileContent = Win411Service.IgnoreMethod(wModel, sModel);
                    break;

                case ArasTableFunction.AnalyzeMethod:
                    sModel.FolderName = "ApiScripts\\Multi\\IACT-CLA";
                    sModel.FileName = $"方法分析.md";
                    sModel.FileContent = Win411Service.AnalyzeMethod(wModel, sModel);
                    break;

                case ArasTableFunction.AnalyzeMethodDetail:
                    if (string.IsNullOrWhiteSpace(config.TargetName))
                    {
                        isExecute = false;
                        System.Windows.Forms.MessageBox.Show("請輸入目標項");
                    }
                    else
                    {
                        sModel.FolderName = "ApiScripts\\Multi\\IACT-CLA\\細項";
                        sModel.FileName = $"方法分析_{config.TargetName}.md";
                        sModel.FileContent = Win411Service.AnalyzeMethodDetail(wModel, sModel);
                    }
                    break;

                case ArasTableFunction.AnalyzeIgnoreMethod:
                    sModel.FolderName = "ApiScripts\\Multi\\IACT-CLA";
                    sModel.FileName = $"方法分析 Scope2.md";
                    sModel.FileContent = Win411Service.AnalyzeIgnoreMethod(wModel, sModel);
                    break;

                case ArasTableFunction.AnalyzeCSharpPage:
                    if (string.IsNullOrWhiteSpace(config.TargetName))
                    {
                        isExecute = false;
                        System.Windows.Forms.MessageBox.Show("請輸入目標項");
                    }
                    else if (!System.IO.File.Exists(config.TargetName))
                    {
                        isExecute = false;
                        System.Windows.Forms.MessageBox.Show("該檔案不存在");
                    }
                    else
                    {
                        var file = new System.IO.FileInfo(config.TargetName);
                        var name = file.Name.Replace(file.Extension, string.Empty);
                        var lines = System.IO.File.ReadAllLines(config.TargetName, System.Text.Encoding.UTF8);

                        sModel.FolderName = "ApiScripts\\Multi\\IACT-CLA\\C#";
                        sModel.FileName = $"{name}.md";
                        sModel.FileContent = Win411Service.AnalyzeCSharpPage(wModel, sModel, name, lines);
                    }
                    break;

                case ArasTableFunction.AnalyzeItemTypeProperty:
                    sModel.FolderName = "ApiScripts\\Multi\\IACT-CLA";
                    sModel.FileName = $"Distinct Property.sql";
                    sModel.FileContent = Win411Service.AnalyzeItemTypeProperty(wModel, sModel);
                    break;

                case ArasTableFunction.CheckItemTypeProperty:
                    sModel.FolderName = "ApiScripts\\Multi\\IACT-CLA";
                    sModel.FileName = $"Check Distinct Property.sql";
                    sModel.FileContent = Win411Service.CheckItemTypeProperty(wModel, sModel);
                    break;

                case ArasTableFunction.UpdatePropertyLabel:
                    sModel.FolderName = "ApiScripts\\Multi\\IACT-CLA";
                    sModel.FileName = $"欄位標籤更新指令.sql";
                    sModel.FileContent = Win411Service.UpdatePropertyLabel(wModel, sModel);
                    break;

                case ArasTableFunction.AnalyzePropertyDataType:
                    if(string.IsNullOrWhiteSpace(wModel.TargetName))
                    {
                        isExecute = false;
                        System.Windows.Forms.MessageBox.Show("請輸入 ItemType 搜尋的關鍵字");
                    }
                    else
                    {
                        sModel.FolderName = "ApiScripts\\Multi\\IACT-CLA";
                        sModel.FileName = $"欄位關聯分析.md";
                        sModel.FileContent = Win411Service.AnalyzePropertyDataType(wModel, sModel);
                    }
                    break;

                case ArasTableFunction.FindPropertyUnOk:
                    sModel.FolderName = "ApiScripts\\Multi\\IACT-CLA";
                    sModel.FileName = $"欄位異常清單.md";
                    sModel.FileContent = Win411Service.FindPropertyUnOk(wModel, sModel);
                    break;

                case ArasTableFunction.DebuggerBreakMethod:
                    sModel.FolderName = "ApiScripts\\Multi\\IACT-CLA";
                    sModel.FileName = $"DebuggerBreakMethod 異常清單.md";
                    sModel.FileContent = Win411Service.DebuggerBreakMethod(wModel, sModel, "System.Diagnostics.Debugger.Break();");
                    break;

                case ArasTableFunction.CCOWriteDebug:
                    sModel.FolderName = "ApiScripts\\Multi\\IACT-CLA";
                    sModel.FileName = $"CCO.Utilities.WriteDebug 異常清單.md";
                    sModel.FileContent = Win411Service.DebuggerBreakMethod(wModel, sModel, "CCO.Utilities.WriteDebug");
                    break;

                case ArasTableFunction.LockedItemList:
                    sModel.FolderName = "ApiScripts\\Multi\\IACT-CLA";
                    sModel.FileName = $"列出被鎖定項目.md";
                    sModel.FileContent = Win411Service.LockedItemList(wModel, sModel);
                    break;

                case ArasTableFunction.ReportProperty:
                    string itemType = wModel.TargetName;
                    if (string.IsNullOrWhiteSpace(wModel.TargetName))
                    {
                        itemType = wModel.TableName;
                    }
                    if (string.IsNullOrWhiteSpace(itemType))
                    {
                        isExecute = false;
                        System.Windows.Forms.MessageBox.Show("請輸入 ItemType 名稱");
                    }
                    else
                    {
                        sModel.FolderName = $"ApiScripts\\Multi\\IACT-CLA\\Properties\\{wModel.DatabaseName.ToUpper()}";
                        sModel.FileName = $"{itemType.ToUpper()}.md";
                        sModel.FileContent = Win411Service.ReportProperty(wModel, sModel, itemType);
                    }
                    break;

                case ArasTableFunction.GeneratePropertyClass:
                    string tableName = wModel.TargetName;
                    if (string.IsNullOrWhiteSpace(wModel.TargetName))
                    {
                        tableName = wModel.TableName;
                    }
                    if (string.IsNullOrWhiteSpace(tableName))
                    {
                        isExecute = false;
                        System.Windows.Forms.MessageBox.Show("請輸入 ItemType 名稱");
                    }
                    else
                    {
                        sModel.FolderName = $"ApiScripts\\Multi\\IACT-CLA\\C# Class\\{wModel.DatabaseName.ToUpper()}";
                        sModel.FileName = $"{tableName.ToUpper()}.cs";
                        sModel.FileContent = Win411Service.GeneratePropertyClass(wModel, sModel, tableName);
                    }
                    break;

                case ArasTableFunction.SQL_Property:
                    sModel.FolderName = "ApiScripts\\Multi\\Property";
                    sModel.FileName = $"[SQL 指令]Property 更新中文描述.sql";
                    sModel.FileContent = Win411Service.SQL_UpdatePropertyLabel(wModel, sModel, config.TargetName);
                    break;

                case ArasTableFunction.METHOD_List:
                    sModel.FolderName = "ApiScripts\\Multi\\Method";
                    sModel.FileName = $"method list.txt";
                    sModel.FileContent = Win411Service.MethodList(wModel, sModel, config.TargetName);
                    isExecute = false;
                    break;

                case ArasTableFunction.METHOD_ID:
                    sModel.FolderName = "ApiScripts\\Multi\\Method";
                    sModel.FileName = $"method id.txt";
                    sModel.FileContent = Win411Service.MethodID(wModel, sModel, config.TargetName);
                    isExecute = false;
                    break;

                case ArasTableFunction.METHOD_Today:
                    sModel.FolderName = $@"G:\我的雲端硬碟\23.工作日誌\{System.DateTime.Now.ToString("yyyyMMdd")}\當日異動";
                    sModel.FileName = $"method today.txt";
                    sModel.FileContent = Win411Service.MethodToday(wModel, sModel, config.TargetName);
                    isExecute = false;
                    break;

                case ArasTableFunction.METHOD_List_By_Sql :
                    string server = config.ServerName.Replace(@"\", "_").Replace(@"-", "_");
                    sModel.FolderName = $@"G:\我的雲端硬碟\23.工作日誌\{System.DateTime.Now.ToString("yyyyMMdd")}\{server}";
                    sModel.FileName = $"method today.txt";
                    sModel.FileContent = Win411Service.MethodListBySql(wModel, sModel, config.Command);
                    isExecute = false;
                    break;

                case ArasTableFunction.Super_User:
                    sModel.FolderName = $@"G:\我的雲端硬碟\23.工作日誌\{System.DateTime.Now.ToString("yyyyMMdd")}\backup";
                    sModel.FileName = $"super user.txt";
                    sModel.FileContent = Win411Service.MethodUseSuperUser(wModel, sModel, config.TargetName);
                    break;

                default:
                    isExecute = false;
                    System.Windows.Forms.MessageBox.Show("請選擇要執行的 Table Function");
                    break;
            }

            if (isExecute)
            {
                TxtOutput.Text = sModel.FileContent;
                FileUtility.WriteFile(sModel.FolderName, sModel.FileName, sModel.FileContent);
            }

            DateTime dtE = DateTime.Now;
            TimeSpan ts = dtE - dtS;
            System.Windows.Forms.MessageBox.Show($"執行總秒數: {ts.TotalSeconds}");
        }

        /// <summary>
        /// 產生 Entity
        /// </summary>
        private Win401Model TransformTo401(Config411 config)
        {
            var result = new Win401Model
            {
                ConnectionString = ServiceUtility.GetConnectionString(config),
                DatabaseName = config.DatabaseName,
                TableName = config.TableName,
                TargetName = config.TargetName,
                FolderName = this.FolderName,
                NameSpaceName = config.NameSpace,
                MainName = config.TableName.Replace("_", string.Empty),
            };

            result.IsEasyName = false;

            //if (string.IsNullOrWhiteSpace(result.ConnectionString)
            //    || string.IsNullOrWhiteSpace(result.TableName)
            //    || string.IsNullOrWhiteSpace(result.FolderName))
            //{
            //    System.Windows.Forms.MessageBox.Show("資料設定有誤，請重新設定");
            //    result.IsValid = false;
            //}
            //else
            //{
            //    result.IsValid = true;
            //}
            return result;
        }

        #endregion Database
    }
}