﻿using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows;

namespace WorkHelp.Wpf.Views
{
    /// <summary></summary>
    public partial class Win30200 : BaseWindow
    {
        //-----------------------------系統原生函式------------------------------//
        public Win30200()
        {
            InitializeComponent();
            this.btnDownload.Click += btnDownload_Click;
            this.btnIni.Click += btnIni_Click;
            this.btnClear.Click += btnClear_Click;
            this.btnClose.Click += base.FWindowClose_Click;
            this.KeyDown += base.FWindow_KeyDown;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
        }

        private void btnIni_Click(object sender, RoutedEventArgs e)
        {
        }

        private void btnDownload_Click(object sender, RoutedEventArgs e)
        {
            this.Download();
        }

        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            txtFile.Text = "";
            txtURL.Text = "";
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        //-----------------------------使用者自訂函式-----------------------------//

        #region 網路資源

        // http://jian-zhoung.blogspot.tw/2012/07/c.html
        private const int LocaleSystemDefault = 0x0800;

        private const int LcmapSimplifiedChinese = 0x02000000;
        private const int LcmapTraditionalChinese = 0x04000000;

        [DllImport("kernel32", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern int LCMapString(int locale, int dwMapFlags, string lpSrcStr, int cchSrc, [Out] string lpDestStr, int cchDest);

        // cht 是 Chinese Traditional 繁体字（正体中文）的缩写，chs 是 Chinese Simplified 简体中文的缩写
        // cht 表示繁体字，chs 表示简体字.
        private string CHTFromCHS(string source)
        {
            var t = new string(' ', source.Length);
            LCMapString(LocaleSystemDefault, LcmapTraditionalChinese, source, source.Length, t, source.Length);
            return t;
        }

        #endregion 網路資源

        private void Download()
        {
            var item = FromUI();

            if (!IsValid(item))
            {
                System.Windows.Forms.MessageBox.Show("資料有誤，請重新檢查！");
                return;
            }

            // cmd.exe
            // C:\Program Files\wkhtmltopdf\bin>wkhtmltopdf.exe http://www.codeceo.com/article/swift-style-guide.html D:\123.pdf

            var script = @item.efull + " " + @item.url + " " + @item.full;

            var p = System.Diagnostics.Process.Start(@item.efull, @item.url + " " + @item.full);
            p.WaitForExit(); //若不加這一行，程式就會馬上執行下一句而抓不到檔案發生例外：System.IO.FileNotFoundException: 找不到檔案 ''。

            if (File.Exists(item.full))
            {
                System.Windows.Forms.MessageBox.Show("下載成功！");
            }
            else
            {
                System.Windows.Forms.MessageBox.Show("下載失敗！");
            }
        }

        //private void Download()
        //{
        //    var item = FromUI();

        //    if (!IsValid(item))
        //    {
        //        System.Windows.Forms.MessageBox.Show("資料有誤，請重新檢查！");
        //        return;
        //    }

        //    // cmd.exe
        //    // C:\Program Files\wkhtmltopdf\bin>wkhtmltopdf.exe http://www.codeceo.com/article/swift-style-guide.html D:\123.pdf

        //    var script = @item.efull + " " + @item.url + " " + @item.full;
        //    LogUtl.writeMsg(script);

        //    var Arguments = Encoding.Default.GetString(Encoding.UTF8.GetBytes(@item.url + " " + @item.full));

        //    var info = new System.Diagnostics.ProcessStartInfo();

        //    info.FileName = @item.efull; // 設定應用程式名稱
        //    info.Arguments = Arguments;  // 設定應用程式參數
        //    info.UseShellExecute = false;

        //    //info.RedirectStandardInput = true;
        //    //info.RedirectStandardOutput = true;
        //    info.RedirectStandardError = true;

        //    //info.StandardOutputEncoding = System.Text.Encoding.UTF8; // 設置標準輸出編碼
        //    info.StandardErrorEncoding = System.Text.Encoding.UTF8;

        //    //info.CreateNoWindow = true; // 不顯示窗口

        //    var process = new System.Diagnostics.Process();
        //    process.StartInfo = info;

        //    var bDownload = false;
        //    try
        //    {
        //        process.Start();
        //        //若不加這一行，程式就會馬上執行下一句而抓不到檔案發生例外：System.IO.FileNotFoundException: 找不到檔案 ''。
        //        process.WaitForExit();
        //        bDownload = true;
        //    }
        //    catch (Exception ex)
        //    {
        //        System.Windows.Forms.MessageBox.Show(ex.Message);
        //    }

        //    if (bDownload && File.Exists(item.full))
        //    {
        //        System.Windows.Forms.MessageBox.Show("下載成功！");
        //    }
        //    else
        //    {
        //        System.Windows.Forms.MessageBox.Show("下載失敗！");
        //    }
        //}

        private bool IsValid(TItem item)
        {
            if (string.IsNullOrWhiteSpace(item.path)
                || string.IsNullOrWhiteSpace(item.file)
                || string.IsNullOrWhiteSpace(item.url)
                || string.IsNullOrWhiteSpace(item.epath)
                || string.IsNullOrWhiteSpace(item.efile))
                return false;

            if (!Directory.Exists(item.path)) Directory.CreateDirectory(item.path);

            if (File.Exists(item.full)) File.Delete(item.full);

            if (!File.Exists(item.efull))
                return false;

            return true;
        }

        private TItem FromUI()
        {
            var result = new TItem
            {
                url = txtURL.Text.Trim(),
                path = txtDir.Text.Trim(),
                file = txtFile.Text.Trim(),

                epath = txtExePath.Text.Trim(),
                efile = txtExeFile.Text.Trim(),
            };

            result.file = CHTFromCHS(result.file);
            result.file = FixPDF(result.file);
            return result;
        }

        private class TItem
        {
            /// <summary>下載路徑 url</summary>
            public string url { get; set; }

            /// <summary>存放路徑</summary>
            public string path { get; set; }

            /// <summary>存放檔案名稱</summary>
            public string file { get; set; }

            /// <summary>轉檔程式路徑</summary>
            public string epath { get; set; }

            /// <summary>轉檔程式檔案名稱</summary>
            public string efile { get; set; }

            private string _full = "";

            /// <summary>完整存放路徑</summary>
            public string full
            {
                get
                {
                    if (_full.Length == 0) _full = Path.Combine(path, file);
                    return _full;
                }
            }

            private string _efull = "";

            /// <summary>轉檔程式完整存放路徑</summary>
            public string efull
            {
                get
                {
                    if (_efull.Length == 0) _efull = Path.Combine(epath, efile);
                    return _efull;
                }
            }
        }

        private string FixPDF(string source)
        {
            var result = clean(source);
            if (!result.ToUpper().EndsWith(".PDF"))
            {
                return result + ".pdf";
            }
            else
            {
                return result;
            }
        }

        private string clean(string s)
        {
            var sb = new StringBuilder(s);

            sb.Replace("-", "_");
            sb.Replace(" ", "");

            sb.Replace("&", "and");

            sb.Replace("\"", "");
            sb.Replace("'", "");
            sb.Replace("“", "");
            sb.Replace("”", "");

            sb.Replace("/", "");
            sb.Replace("\\", "");
            sb.Replace(":", "");
            sb.Replace("@", "");

            //sb.Replace("eacute;", "é");

            return sb.ToString();
        }
    }
}