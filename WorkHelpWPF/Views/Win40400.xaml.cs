﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using WorkHelp.Repositories;
using WorkHelp.Utilities;
using WorkHelp.Wpf.Enums;
using WorkHelp.Wpf.Enums.Extends;
using WorkHelp.Wpf.Extensions;
using WorkHelp.Wpf.Models;
using WorkHelp.Wpf.Models.Windows;
using WorkHelp.Wpf.Services;
using WorkHelp.Wpf.Utility;

namespace WorkHelp.Wpf.Views
{
    /// <summary>
    /// Access
    /// </summary>
    public partial class Win40400 : BaseWindow
    {
        /// <summary>
        /// 資料夾名稱
        /// </summary>
        private string FolderName { get; set; } = "Win40400";

        /// <summary>
        /// 建構元
        /// </summary>
        public Win40400()
        {
            InitializeComponent();
            BindEvents();
        }

        /// <summary>
        /// 繫結事件
        /// </summary>
        private void BindEvents()
        {
            this.Loaded += Window_Loaded;
            this.KeyDown += base.FWindow_KeyDown;
            this.BtnClose.Click += base.FWindowClose_Click;

            this.BtnQuery.Click += BtnQuery_Click;
            this.BtnExecute.Click += BtnExecute_Click;

            this.BtnConn2007.Click += BtnConn2007_Click;
            this.BtnConn2019.Click += BtnConn2019_Click;
            this.BtnConnPwd.Click += BtnConnPwd_Click;
            
        }

        //-----------------------------系統原生函式------------------------------//

        /// <summary>
        /// 視窗載入
        /// </summary>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            TxtConnStr.Text = AccessDbTest.GetConnectionString();
            TxtDbName.Text = AccessDbTest.GetDbName();
            CbxFunction.Items.Add("單一資料表");
            CbxFunction.Items.Add("指令");

            CbxFunction.Text = "單一資料表";
        }

        /// <summary>
        /// 2019
        /// </summary>
        private void BtnConn2019_Click(object sender, RoutedEventArgs e)
        {
            TxtConnStr.Text = AccessDbTest.GetConnectionString();
        }

        /// <summary>
        /// 2007
        /// </summary>
        private void BtnConn2007_Click(object sender, RoutedEventArgs e)
        {
            TxtConnStr.Text = AccessDbTest.GetConnectionString2007();
        }

        /// <summary>
        /// 需密碼
        /// </summary>
        private void BtnConnPwd_Click(object sender, RoutedEventArgs e)
        {
            if (!TxtConnStr.Text.Contains("Password"))
            {
                TxtConnStr.Text = TxtConnStr.Text.Replace("Persist Security Info=False;", "Jet OLEDB:Database Password=1234;");
            }
        }

        /// <summary>
        /// 查詢
        /// </summary>
        private void BtnQuery_Click(object sender, RoutedEventArgs e)
        {
            Query("所有資料表");
        }

        /// <summary>
        /// 執行
        /// </summary>
        private void BtnExecute_Click(object sender, RoutedEventArgs e)
        {
            string funcName = CbxFunction.Text;
            Query(funcName);
        }

        //-----------------------------使用者自訂函式-----------------------------//
        private void Query(string funcName)
        {
            string connSting = TxtConnStr.Text.Replace("{db_name}", TxtDbName.Text);
            string command = TxtTbName.Text.Trim();

            switch (funcName)
            {
                case "所有資料表":
                    AllTablesName(connSting);
                    break;

                case "單一資料表":
                    if (string.IsNullOrEmpty(command))
                    {
                        System.Windows.Forms.MessageBox.Show("請輸入資料表");
                    }
                    else
                    {
                        ExecuteSql(connSting, "SELECT * FROM [" + command + "]");
                    }
                    break;

                case "指令":
                    if (string.IsNullOrEmpty(command))
                    {
                        System.Windows.Forms.MessageBox.Show("請輸入指令");
                    }
                    else
                    {
                        ExecuteSql(connSting, command);
                    }
                    break;
            }
        }

        private void AllTablesName(string connectionString)
        {
            var dt = AccessDbTest.GetTableList(connectionString);

            if (dt == null || dt.Rows.Count == 0)
            {
                System.Windows.Forms.MessageBox.Show("查無資料");
                return;
            }

            List<string> tbs = new List<string>(); 
            foreach (System.Data.DataRow dr in dt.Rows)
            {
                string tb_name = "";
                if (dr["TABLE_NAME"] == System.DBNull.Value)
                {
                    //tb_name = "dbnull";
                }
                else
                {
                    tbs.Add(dr["TABLE_NAME"].ToString());
                }

            }

            TxtSample.Text = string.Join("\r\n", tbs);
            LblTabls.Content = "Table Names：共 " + tbs.Count.ToString() + " 筆";
        }

        private void ExecuteSql(string connectionString, string command)
        {
            var dt = AccessDbTest.GetDataTable(connectionString, command);
            if (dt != null)
            {
                TxtOutput.Text = "共 " + dt.Rows.Count + " 筆";
                TxtInput.Text = Newtonsoft.Json.JsonConvert.SerializeObject(dt, Newtonsoft.Json.Formatting.Indented);
            }
            else
            {
                TxtOutput.Text = "查無資料";
                TxtInput.Text = string.Empty;
            }
        }

    }
}