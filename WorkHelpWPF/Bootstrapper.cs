﻿using Autofac;
using Autofac.Extras.DynamicProxy;
using Prism.Autofac;
using System.Reflection;
using System.Windows;
using WorkHelp.Wpf.Factory;
using WorkHelp.Wpf.Modules;
using WorkHelp.Wpf.Modules.Proxy;
using WorkHelp.Wpf.Views;

namespace WorkHelp.Wpf
{
    /// <summary>
    /// 引導程序
    /// </summary>
    internal class Bootstrapper : AutofacBootstrapper
    {
        /// <summary>
        /// 創建外殼
        /// </summary>
        protected override DependencyObject CreateShell()
        {
            RegisterFactories();
            return Container.Resolve<MainWindow>();
        }

        /// <summary>
        /// 外殼初始化
        /// </summary>
        protected override void InitializeShell()
        {
            base.InitializeShell();
            Application.Current.MainWindow?.Show();
        }

        /// <summary>
        /// 配置容器生成器
        /// </summary>
        /// <param name="builder">容器生成器</param>
        protected override void ConfigureContainerBuilder(ContainerBuilder builder)
        {
            base.ConfigureContainerBuilder(builder);

            // 註冊 aop proxy (Aspect-oriented programming proxy, 切面導向程式設計)
            builder.RegisterType<ExceptionLogProxy>()
                .PropertiesAutowired()
                .SingleInstance();

            // 註冊此專案
            builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly())
                .AsImplementedInterfaces()                 // 作為已實現的接口
                .SingleInstance()                          // 單一實例
                .PropertiesAutowired()                     // 屬性自動連線
                .EnableInterfaceInterceptors()             // 啟用接口攔截器
                .InterceptedBy(typeof(ExceptionLogProxy));

            // 註冊 config service 中的服務
            builder.RegisterAssemblyTypes(Assembly.Load("WorkHelp"))
                .AsImplementedInterfaces()                 // 作為已實現的接口
                .SingleInstance()                          // 單一實例
                .PropertiesAutowired()                     // 屬性自動連線
                .EnableInterfaceInterceptors()             // 啟用接口攔截器
                .InterceptedBy(typeof(ExceptionLogProxy));

            // 註冊 一般例外處理
            builder.RegisterModule(new ViewModule());
        }

        /// <summary>
        /// 注入工廠
        /// </summary>
        private void RegisterFactories()
        {
            WindowFactory.Default = new WindowFactory(container: Container);
        }
    }
}