﻿using Autofac;
using System;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Media;
using WorkHelp.Wpf.Enums;

namespace WorkHelp.Wpf.Factory
{
    /// <summary>
    /// Window 工廠
    /// </summary>
    public class WindowFactory
    {
        /// <summary>
        /// Singleton
        /// </summary>
        public static WindowFactory Default { get; set; }

        /// <summary>
        /// IoC容器
        /// </summary>
        private IContainer Container { get; set; }

        /// <summary>
        /// 建立 Window 的工廠服務
        /// </summary>
        /// <param name="container">IoC容器</param>
        public WindowFactory(IContainer container)
        {
            Container = container;
        }

        /// <summary>
        /// 建立新視窗
        /// </summary>
        /// <typeparam name="T">視窗型別</typeparam>
        /// <param name="owner">視窗擁有者</param>
        /// <param name="useContainer">是否使用 IoC 容器注入型別資料</param>
        /// <returns>回傳新視窗</returns>
        public T NewWindow<T>(Window owner, bool useContainer = true) where T : Window
        {
            T window = (T)Activator.CreateInstance(typeof(T));
            if (useContainer)
            {
                Inject(window);
            }
            return window;
        }

        /// <summary>
        /// 視窗開啟關閉共用函式
        /// </summary>
        /// <param name="type">視窗型別</param>
        /// <param name="owner">視窗擁有者</param>
        /// <param name="tag">任意物件值</param>
        /// <param name="e">開啟模式</param>
        public void ShowMenuWindow(Type type, Window owner, string tag = "", WindowShowMode e = WindowShowMode.ShowDialog)
        {
            Window window = CreateWindow(type: type, useContainer: true);
            ShowDialog(owner, window, tag, e);
        }

        /// <summary>
        /// 視窗開啟關閉共用函式
        /// </summary>
        /// <typeparam name="T">視窗型別</typeparam>
        /// <param name="owner">視窗擁有者</param>
        /// <param name="initialAction">設定視窗初始化資料事件</param>
        public void ShowSubWindow<T>(Window owner, Action<T> initialAction = null) where T : Window
        {
            Window window = CreateWindow(initialAction: initialAction, useContainer: true);
            ShowDialog(owner, window);
        }

        /// <summary>
        /// 建立視窗 (包含 Container 初始化)
        /// </summary>
        /// <param name="type">視窗型別</param>
        /// <param name="useContainer">是否使用 IoC 容器注入型別資料</param>
        /// <returns>回傳視窗</returns>
        private Window CreateWindow(Type type, bool useContainer = true)
        {
            Window window = (Window)Activator.CreateInstance(type);
            if (useContainer)
            {
                Inject(window);
            }
            return window;
        }

        /// <summary>
        /// 建立視窗
        /// </summary>
        /// <typeparam name="T">視窗型別</typeparam>
        /// <param name="initialAction">設定視窗初始化資料事件</param>
        /// <param name="useContainer">是否使用 IoC 容器注入型別資料</param>
        /// <returns></returns>
        private Window CreateWindow<T>(Action<T> initialAction = null, bool useContainer = true) where T : Window
        {
            T window = (T)Activator.CreateInstance(typeof(T));
            if (useContainer)
            {
                Inject(window);
            }
            initialAction?.Invoke(window);
            return window;
        }

        /// <summary>
        /// 注入型別
        /// </summary>
        /// <param name="window">視窗</param>
        public void Inject(Window window)
        {
            foreach (PropertyInfo property in window.GetType()
                .GetProperties(BindingFlags.Public | BindingFlags.Instance)
                .Where(p => p.PropertyType.IsInterface && p.GetValue(window) == null))
            {
                property.SetValue(window, Container.Resolve(property.PropertyType));
            }
        }

        #region Show

        /// <summary>
        /// 開啟視窗
        /// </summary>
        /// <param name="owner">視窗擁有者</param>
        /// <param name="window">要開啟的視窗</param>
        private void ShowDialog(Window owner, Window window, string tag = "", WindowShowMode e = WindowShowMode.ShowDialog)
        {
            // 設定擁有者
            window.Owner = owner;
            // 設定工作列按鈕
            window.ShowInTaskbar = false;
            // 設定視窗大小
            window.WindowState = WindowState.Normal;
            window.Width = SystemParameters.PrimaryScreenWidth - 10;
            window.Height = SystemParameters.PrimaryScreenHeight - 100;
            // 設定出現位置
            window.WindowStartupLocation = WindowStartupLocation.Manual;
            window.Top = 55;
            window.Left = 5;
            // 設定大小模式
            window.ResizeMode = ResizeMode.NoResize;
            // 設定背景顏色
            window.Background = new SolidColorBrush(Colors.LightGray);

            // 任意物件值
            window.Tag = tag;

            if (e == WindowShowMode.ShowDialog)
            {
                // 以 ShowDialog 方式強制呈現
                window.ShowDialog();
            }
            else
            {
                window.Show();
            }
        }

        #endregion Show
    }
}