﻿using Castle.DynamicProxy;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WorkHelp.Wpf.Modules.Proxy
{
    /// <summary>
    /// 記錄錯誤
    /// </summary>
    public class ExceptionLogProxy : IInterceptor
    {
        private static Logger Logger => LogManager.GetCurrentClassLogger();

        /// <summary>
        /// 記錄執行錯誤時的方法
        /// </summary>
        /// <param name="invocation">調用</param>
        public void Intercept(IInvocation invocation)
        {
            try
            {
                // 執行
                invocation.Proceed();

                // 取得回傳值
                var value = invocation.ReturnValue;

                // async task 執行失敗
                if (value is Task task && task.IsFaulted)
                {
                    LogError(invocation, task.Exception);
                }
            }
            catch (Exception e)
            {
                LogError(invocation, e);
            }
        }

        /// <summary>
        /// 記錄錯誤
        /// </summary>
        /// <param name="invocation">調用</param>
        /// <param name="exception">例外</param>
        private void LogError(IInvocation invocation, Exception exception)
        {
            string[] parameterNames = invocation.Method.GetParameters().Select(p => p.Name).ToArray();
            Dictionary<string, object> inputs = new Dictionary<string, object>();
            for (var i = 0; i < parameterNames.Length; i++)
            {
                inputs[parameterNames[i]] = invocation.Arguments[i];
            }

            Logger.Error($"執行 {invocation.TargetType.FullName}.{invocation.Method.Name} 發生錯誤, 輪入參數: {JsonConvert.SerializeObject(inputs)}{Environment.NewLine}{exception}");
        }
    }
}