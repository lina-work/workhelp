﻿using Autofac;
using NLog;
using System;
using System.Threading;
using WorkHelp.Context;

namespace WorkHelp.Wpf.Modules
{
    /// <summary>
    /// 查看模塊
    /// </summary>
    public class ViewModule : Module
    {
        /// <summary>
        /// 記錄
        /// </summary>
        private static Logger Logger => LogManager.GetCurrentClassLogger();

        /// <summary>
        /// 載入
        /// </summary>
        /// <param name="builder"></param>
        protected override void Load(ContainerBuilder builder)
        {
            // 加入 async 例外處理
            SynchronizationContext.SetSynchronizationContext(new AsyncSynchronizationContext());

            // 加入一般例外處理
            AppDomain.CurrentDomain.UnhandledException += (sender, e) =>
            {
                Logger.Error(e.ExceptionObject);
            };
        }
    }
}