﻿using System;
using WorkHelp.Logging;

namespace WorkHelp.Wpf.Utility
{
    /// <summary>
    /// 組態檔通用函式
    /// </summary>
    public class ConfigUtility
    {
        /// <summary>
        /// 儲存組態檔
        /// </summary>
        /// <typeparam name="T">組態檔型別</typeparam>
        /// <returns></returns>
        public static void Write<T>(T value, string fileName = "")
        {
            if (value != null)
            {
                string file = string.IsNullOrWhiteSpace(fileName)
                    ? ConfigFile<T>()
                    : ConfigFile(fileName);

                string contents = Newtonsoft.Json.JsonConvert.SerializeObject(value);

                System.IO.File.WriteAllText(file, contents, System.Text.Encoding.UTF8);
            }
        }

        /// <summary>
        /// 讀取組態檔
        /// </summary>
        /// <typeparam name="T">組態檔型別</typeparam>
        /// <returns></returns>
        public static T Read<T>(string fileName = "") where T : new()
        {
            string file = string.IsNullOrWhiteSpace(fileName)
                ? ConfigFile<T>()
                : ConfigFile(fileName);

            if (System.IO.File.Exists(file))
            {
                string contents = System.IO.File.ReadAllText(file, System.Text.Encoding.UTF8);
                if (!string.IsNullOrWhiteSpace(contents))
                {
                    try
                    {
                        return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(contents);
                    }
                    catch (Exception ex)
                    {
                        TLog.Watch(message: ex.ToString());
                    }
                }
            }
            return new T();
        }

        /// <summary>
        /// 取得檔案路徑
        /// </summary>
        /// <returns></returns>
        private static string ConfigFile<T>()
        {
            Type t = typeof(T);
            return ConfigFile(t.Name);
        }

        /// <summary>
        /// 取得檔案路徑
        /// </summary>
        /// <returns></returns>
        private static string ConfigFile(string name)
        {
            string baseDirectory = AppDomain.CurrentDomain.BaseDirectory;
            string path = System.IO.Path.Combine(baseDirectory, "Configs");
            string file = System.IO.Path.Combine(path, $"{name}.json");

            if (!System.IO.Directory.Exists(path))
            {
                System.IO.Directory.CreateDirectory(path);
            }

            return file;
        }

        /// <summary>
        /// 取得檔案路徑
        /// </summary>
        /// <param name="win">視窗(完整八碼)</param>
        /// <param name="name">包含副檔名</param>
        /// <returns></returns>
        public static string SampleFileContents(string win, string name)
        {
            string file = SampleFile(win, name);
            if (System.IO.File.Exists(file))
            {
                return System.IO.File.ReadAllText(file, System.Text.Encoding.UTF8);
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// 取得檔案路徑
        /// </summary>
        /// <param name="win">視窗(完整八碼)</param>
        /// <param name="name">包含副檔名</param>
        /// <returns></returns>
        private static string SampleFile(string win, string name)
        {
            string baseDirectory = AppDomain.CurrentDomain.BaseDirectory;
            string path = System.IO.Path.Combine(baseDirectory, $"Samples\\{win}");
            string file = System.IO.Path.Combine(path, $"{name}");

            if (!System.IO.Directory.Exists(path))
            {
                System.IO.Directory.CreateDirectory(path);
            }

            return file;
        }
    }
}