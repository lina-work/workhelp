﻿using System;
using System.Collections.Generic;
using WorkHelp.Wpf.Enums;
using WorkHelp.Wpf.Models.Aras;

namespace WorkHelp.Wpf.Models.Windows
{
    /// <summary>
    /// 組態檔
    /// </summary>
    public class Config412
    {
        /// <summary>
        /// 主機名稱
        /// </summary>
        public string ServerName { get; set; }

        /// <summary>
        /// 主機資料夾名稱
        /// </summary>
        public string ServerFolderName { get; set; }

        /// <summary>
        /// 帳號
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// 密碼
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// 資料庫名稱
        /// </summary>
        public string DatabaseName { get; set; }

        /// <summary>
        /// 資料表名稱
        /// </summary>
        public string TableName { get; set; }

        /// <summary>
        /// Aras 主要類別
        /// </summary>
        public ArasType ArasType { get; set; }

        /// <summary>
        /// 鍵名
        /// </summary>
        public string KeyedName { get; set; }

        /// <summary>
        /// 要進行置換的類別
        /// </summary>
        public ArasTypeFunction ArasTypeFunction { get; set; }

        /// <summary>
        /// 資料庫群組
        /// </summary>
        public string DbGroup { get; set; }

        /// <summary>
        /// 儲存體列表
        /// </summary>
        public List<Storage> StorageList { get; set; }

        /// <summary>
        /// 是否執行
        /// </summary>
        public bool IsExcuted { get; set; }

        /// <summary>
        /// 是否執行成功
        /// </summary>
        public bool IsSuccess { get; set; }

        /// <summary>
        /// 資料夾名稱
        /// </summary>
        public string FolderName { get; set; }

        /// <summary>
        /// 檔案名稱
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// 檔案內容
        /// </summary>
        public string FileContent { get; set; }

        /// <summary>
        /// 開始執行時間
        /// </summary>
        public DateTime ExecuteTimeStart { get; set; }

        /// <summary>
        /// 結束執行時間
        /// </summary>
        public DateTime ExecuteTimeEnd { get; set; }

        /// <summary>
        /// 執行時間間隔
        /// </summary>
        public TimeSpan ExecuteTimeSpan { get; set; }

        ///// <summary>
        ///// 目標項
        ///// </summary>
        //public string TargetName { get; set; }

        ///// <summary>
        ///// 是否執行置換
        ///// </summary>
        //public bool IsExecute { get; set; }

        ///// <summary>
        ///// 是否載入範例檔
        ///// </summary>
        //public bool IsLoadSample { get; set; }

        ///// <summary>
        ///// 是否處理 Trim
        ///// </summary>
        //public bool IsCheckTrim { get; set; }

        ///// <summary>
        ///// 是否輸出 Class 模式
        ///// </summary>
        //public bool IsGenerateClass { get; set; }

        ///// <summary>
        ///// 是否將 Markdown 轉換的 Checkbox 設為停用
        ///// </summary>
        //public bool IsDisabled { get; set; }

        ///// <summary>
        ///// 表單名稱
        ///// </summary>
        //public string FormName { get; set; }

        ///// <summary>
        ///// 範例檔名稱
        ///// </summary>
        //public string SampleFile { get; set; }

        ///// <summary>
        ///// 輸出檔名稱
        ///// </summary>
        //public string OutputFile { get; set; }

        ///// <summary>
        ///// 預設資料
        ///// </summary>
        //public string DefaultValue { get; set; }

        ///// <summary>
        ///// 是否檢查格式
        ///// </summary>
        //public bool IsCheckFormat { get; set; }
    }
}