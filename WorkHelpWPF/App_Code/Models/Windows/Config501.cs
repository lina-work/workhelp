﻿using System.Collections.Generic;
using WorkHelp.Wpf.Models.Main;

namespace WorkHelp.Wpf.Models.Windows
{
    /// <summary>
    /// 組態項目
    /// </summary>
    public class Config501
    {
        /// <summary>
        /// 選單項目集
        /// </summary>
        public List<MenuNode> Nodes { get; set; }
    }
}