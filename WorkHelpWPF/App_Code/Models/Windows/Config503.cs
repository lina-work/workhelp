﻿using System.Collections.Generic;
using WorkHelp.Wpf.Models.ViewSplit;

namespace WorkHelp.Wpf.Models.Windows
{
    /// <summary>
    /// 組態項目
    /// </summary>
    public class Config503
    {
        /// <summary>
        /// 預設畫面切割組態編號
        /// </summary>
        public string DefaultId { get; set; }

        /// <summary>
        /// 畫面切割組態集
        /// </summary>
        public List<ViewParting> Items { get; set; }
    }
}