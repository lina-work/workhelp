﻿using System.Collections.Generic;

namespace WorkHelp.Wpf.Models.Windows
{
    /// <summary>
    /// 組態項目
    /// </summary>
    public class Config50101
    {
        /// <summary>
        /// 畫面切割組態集
        /// </summary>
        public List<ViewParting> Items { get; set; }
    }
}