﻿using System.Collections.Generic;
using System.IO;

namespace WorkHelp.Wpf.Models.Windows
{
    /// <summary>
    /// 組態項目
    /// </summary>
    public class Config106
    {
        /// <summary>
        /// Icon 來源名稱
        /// </summary>
        public string OriginName { get; set; }

        /// <summary>
        /// Icon 目標名稱
        /// </summary>
        public string TargetName { get; set; }

        /// <summary>
        /// Icon 檔案路徑
        /// </summary>
        public string IconPath { get; set; }

        /// <summary>
        /// Old Icon 檔案路徑
        /// </summary>
        public string OldIconPath { get; set; }

        /// <summary>
        /// New Icon 檔案路徑
        /// </summary>
        public string NewIconPath { get; set; }
    }

}