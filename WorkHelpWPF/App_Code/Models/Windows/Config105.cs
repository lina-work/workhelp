﻿namespace WorkHelp.Wpf.Models.Windows
{
    /// <summary>
    /// 組態檔
    /// </summary>
    public class Config105
    {
        /// <summary>
        /// 單字
        /// </summary>
        public string Word { get; set; }

        /// <summary>
        /// 間隔(毫秒)
        /// </summary>
        public int Interval { get; set; }

        /// <summary>
        /// 朗讀次數
        /// </summary>
        public int Frequency { get; set; }
    }
}