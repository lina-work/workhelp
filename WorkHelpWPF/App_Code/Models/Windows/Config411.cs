﻿using System;
using WorkHelp.Wpf.Enums;

namespace WorkHelp.Wpf.Models.Windows
{
    /// <summary>
    /// 組態檔
    /// </summary>
    public class Config411
    {
        /// <summary>
        /// 主機名稱
        /// </summary>
        public string ServerName { get; set; }

        /// <summary>
        /// 帳號
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// 密碼
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// 資料庫名稱
        /// </summary>
        public string DatabaseName { get; set; }

        /// <summary>
        /// 資料表名稱
        /// </summary>
        public string TableName { get; set; }

        /// <summary>
        /// 命名空間
        /// </summary>
        public string NameSpace { get; set; }

        /// <summary>
        /// 目標項
        /// </summary>
        public string TargetName { get; set; }

        /// <summary>
        /// 要進行 Table Function 的類別
        /// </summary>
        public ArasTableFunction ArasTableFunctionType { get; set; }

        /// <summary>
        /// 是否執行置換
        /// </summary>
        public bool IsExecute { get; set; }

        /// <summary>
        /// 是否載入範例檔
        /// </summary>
        public bool IsLoadSample { get; set; }

        /// <summary>
        /// 是否處理 Trim
        /// </summary>
        public bool IsCheckTrim { get; set; }

        /// <summary>
        /// 是否輸出 Class 模式
        /// </summary>
        public bool IsGenerateClass { get; set; }

        /// <summary>
        /// 是否將 Markdown 轉換的 Checkbox 設為停用
        /// </summary>
        public bool IsDisabled { get; set; }

        /// <summary>
        /// 表單名稱
        /// </summary>
        public string FormName { get; set; }

        /// <summary>
        /// 範例檔名稱
        /// </summary>
        public string SampleFile { get; set; }

        /// <summary>
        /// 輸出檔名稱
        /// </summary>
        public string OutputFile { get; set; }

        /// <summary>
        /// 預設資料
        /// </summary>
        public string DefaultValue { get; set; }

        /// <summary>
        /// 是否檢查格式
        /// </summary>
        public bool IsCheckFormat { get; set; }

        /// <summary>
        /// 置換函式
        /// </summary>
        public Func<string, Config411, string> Transform { get; set; }

        /// <summary>
        /// 指令
        /// </summary>
        public string Command { get; set; }
    }
}