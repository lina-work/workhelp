﻿using System.Collections.Generic;
using System.IO;

namespace WorkHelp.Wpf.Models.Windows
{
    /// <summary>
    /// 組態項目
    /// </summary>
    public class Config104
    {
        /// <summary>
        /// 電子檔 檔案名稱
        /// </summary>
        public string EpubName { get; set; }

        /// <summary>
        /// 電子檔 檔案路徑
        /// </summary>
        public string EpubPath { get; set; }

        /// <summary>
        /// 電子檔 圖片路徑
        /// </summary>
        public string PhotoPath { get; set; }

        /// <summary>
        /// 電子檔 圖片副檔名
        /// </summary>
        public string ExtendName { get; set; }
    }

    /// <summary>
    /// 電子書項目
    /// </summary>
    public class EpubItem
    {
        /// <summary>
        /// 代碼
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 是否正常
        /// </summary>
        [Newtonsoft.Json.JsonIgnore]
        public bool IsValid { get; set; }

        /// <summary>
        /// 當前層級
        /// </summary>
        public int Level { get; set; }

        /// <summary>
        /// 標題
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 根路徑
        /// </summary>
        [Newtonsoft.Json.JsonIgnore]
        public string RootPath { get; set; }

        /// <summary>
        /// 來源路徑
        /// </summary>
        [Newtonsoft.Json.JsonIgnore]
        public string OriginPath { get; set; }

        /// <summary>
        /// 複製後路徑
        /// </summary>
        [Newtonsoft.Json.JsonIgnore]
        public string NewPath { get; set; }

        /// <summary>
        /// 複製後檔案名稱
        /// </summary>
        [Newtonsoft.Json.JsonIgnore]
        public string NewName { get; set; }

        /// <summary>
        /// 複製後完整路徑
        /// </summary>
        public string NewFullFile { get; set; }

        /// <summary>
        /// EPub 連結路徑，必須為英數字 (EX: path/to/img.jpg)
        /// </summary>
        public string EpubUrl { get; set; }

        /// <summary>
        /// 子項目
        /// </summary>
        public List<EpubItem> Items { get; set; }

        /// <summary>
        /// 是否是檔案
        /// </summary>
        [Newtonsoft.Json.JsonIgnore]
        public bool IsFile { get; set; }

        /// <summary>
        /// 來源資料夾路徑
        /// </summary>
        [Newtonsoft.Json.JsonIgnore]
        public DirectoryInfo OriginDirectoryInfo { get; set; }

        /// <summary>
        /// 訊息
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// 原始檔案名稱
        /// </summary>
        public string Name { get; set; }
    }
}