﻿namespace WorkHelp.Wpf.Models
{
    /// <summary>
    /// 註冊碼資料模型
    /// </summary>
    public class RegModel
    {
        /// <summary>
        /// 真實程式名稱
        /// </summary>
        public string ActualName { get; set; }

        /// <summary>
        /// 呈現程式名稱
        /// </summary>
        public string DisplayName { get; set; }

        /// <summary>
        /// 安裝日期
        /// </summary>
        public string InstallDate { get; set; }

        /// <summary>
        /// 安裝路徑
        /// </summary>
        public string InstallLocation { get; set; }

        /// <summary>
        /// 呈現版本號
        /// </summary>
        public string DisplayVersion { get; set; }

        /// <summary>
        /// 發行者
        /// </summary>
        public string Publisher { get; set; }

        /// <summary>
        /// 呈現 icon 路徑
        /// </summary>
        public string DisplayIcon { get; set; }

        /// <summary>
        /// 解除安裝 script
        /// </summary>
        public string UninstallString { get; set; }

        /// <summary>
        /// 修改路徑
        /// </summary>
        public string ModifyPath { get; set; }

        /// <summary>
        /// 修復路徑
        /// </summary>
        public string RepairPath { get; set; }
    }
}