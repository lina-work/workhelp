﻿namespace WorkHelp.Wpf.Models
{
    public class SqlColumnModel
    {
        public ColumnModel ColumnData { get; set; }
        public string KeyName { get; set; }
        public string KeyDeclare { get; set; }
        public string KeyType { get; set; }
        public int KeyLength { get; set; }
    }
}