﻿using System;

namespace WorkHelp.Wpf.Models
{
    public class DataModel
    {
        public string name { get; set; }
        public string type { get; set; }
        public string coldef { get; set; }
        public string nullable { get; set; }
        public int maxlength { get; set; }
        public string iskey { get; set; }
        public string memo { get; set; }

        public int isIdentity { get; set; }

        public string table_name { get; set; }
        public string table_desc { get; set; }
        
        public bool bKey { get; set; }
        public bool bIdentity { get; set; }
    }
}
