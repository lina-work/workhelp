﻿using System.Collections.Generic;

namespace WorkHelp.Wpf.Models
{
    public class SchemaModel
    {
        public string ConnectionString { get; set; }

        public string DatabaseName { get; set; }

        public string TableName { get; set; }

        public string TableDescription { get; set; }

        public string TableSchema { get; set; } = "dbo";

        public List<ColumnModel> ColumnList { get; set; }

        public List<KeyModel> KeyEasyList { get; set; }

        public List<ColumnModel> KeyList { get; set; }

        public List<ColumnModel> DescriptionList { get; set; }

        public string NameSpaceName { get; set; }

        public string MainName { get; set; }

        public string SubMain { get; set; }

        public string ControllerName { get; set; }

        public string FolderName { get; set; }

        public string FileName { get; set; }

        public string FileContent { get; set; }

        public string MainRouteName { get; set; }

        public string SubRouteName { get; set; }

        public bool IsArasMode { get; set; }

        #region pieces

        public string KeyParamSummary { get; set; }
        public string KeyParamDataType { get; set; }
        public string KeyParamValidate { get; set; }
        public string KeyEntitySingleOrDefault { get; set; }
        public string KeyParamDynamic { get; set; }
        public string KeyParamValue { get; set; }
        public string KeyParamName { get; set; }

        public string KeyEntityValue { get; set; }
        public string KeyEntityWhere { get; set; }
        public string KeyEntityLines_GoogleProto { get; set; }
        public string KeyEntityEquals { get; set; }

        public string KeyParamTestCase { get; set; }

        public string KeyRoute { get; set; }
        public string KeyRouteItem { get; set; }

        public string KeyApiRouteList { get; set; }
        public string KeyApiRouteItem { get; set; }
        public string KeyApiRouteSave { get; set; }
        public string KeyApiRouteRemove { get; set; }

        public string ColumEntityValidate { get; set; }
        public string ColumEntityCopy { get; set; }
        public string ColumEntityLines_GoogleProto { get; set; }
        public string ColumEntityLines_GoogleProto_Update { get; set; }

        #endregion pieces
    }
}