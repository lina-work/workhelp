﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace WorkHelp.Wpf.Models
{
    /// <summary>類別摘要</summary>
    [Serializable]
    public class RDT101Model
    {
        [JsonIgnoreAttribute]
        public int old_rdtoid { get; set; }

        /// <summary>
        /// 
        /// (不允許 NULL) 
        /// </summary>
        public int rdtoid { get; set; }
        /// <summary></summary>
        public int rdt002 { get; set; }
        /// <summary></summary>
        public DateTime rdt001 { get; set; }
        /// <summary></summary>
        public double rdt003 { get; set; }
        /// <summary></summary>
        public int rdt004 { get; set; }
        /// <summary></summary>
        public string rdt005 { get; set; }
    }
}
