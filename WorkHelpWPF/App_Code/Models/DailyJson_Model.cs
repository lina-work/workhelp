﻿namespace WorkHelp.Wpf.Models
{
    /// <summary>[Trello 日誌檔]存取資料模型</summary>
    public class DailyJson_Model
    {
        /// <summary>Trello Json 壓縮字串</summary>
        public string JsonData { get; set; }
    }
}