﻿namespace WorkHelp.Wpf.Models
{
    public class ProtobufModel
    {
        public string MessageName { get; set; }

        public string PackageName { get; set; }

        public bool IsImportTimestamp { get; set; }

        public bool IsImportWrappers { get; set; }

        public bool IsImportDecimal { get; set; }
    }
}