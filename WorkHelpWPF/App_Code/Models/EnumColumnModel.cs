﻿namespace WorkHelp.Wpf.Models
{
    /// <summary>
    /// 列舉模型
    /// </summary>
    public class EnumColumnModel
    {
        /// <summary>
        /// 代碼
        /// </summary>
        public int Code { get; set; }

        /// <summary>
        /// 說明
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 列舉名稱
        /// </summary>
        public string EnumName { get; set; }

        /// <summary>
        /// 所屬列舉型別名稱
        /// </summary>
        public string TypeName { get; set; }
    }
}