﻿using WorkHelp.Wpf.Enums;

namespace WorkHelp.Wpf.Models
{
    public class ColumnModel
    {
        public string TABLE_CATALOG { get; set; }
        public string TABLE_SCHEMA { get; set; }
        public string TABLE_NAME { get; set; }
        public string COLUMN_NAME { get; set; }
        public int ORDINAL_POSITION { get; set; }
        public string COLUMN_DEFAULT { get; set; }
        public string IS_NULLABLE { get; set; }
        public string DATA_TYPE { get; set; }
        public string DATA_TYPE_UPPER { get; set; }
        public int? CHARACTER_MAXIMUM_LENGTH { get; set; }
        public int? CHARACTER_OCTET_LENGTH { get; set; }
        public int? NUMERIC_PRECISION { get; set; }
        public int? NUMERIC_PRECISION_RADIX { get; set; }
        public int? NUMERIC_SCALE { get; set; }
        public int? DATETIME_PRECISION { get; set; }
        public string CHARACTER_SET_CATALOG { get; set; }
        public string CHARACTER_SET_SCHEMA { get; set; }
        public string CHARACTER_SET_NAME { get; set; }
        public string COLLATION_CATALOG { get; set; }
        public string COLLATION_SCHEMA { get; set; }
        public string COLLATION_NAME { get; set; }
        public string DOMAIN_CATALOG { get; set; }
        public string DOMAIN_SCHEMA { get; set; }
        public string DOMAIN_NAME { get; set; }
        public string COLUMN_DESCRIPTION { get; set; }
        public bool IS_KEY { get; set; }
        public bool IS_NULLABLE_BOOL { get; set; }

        public SqlDataTypeEnum DATA_TYPE_SQL { get; set; }
        public CSharpDataTypeEnum DATA_TYPE_CSHARP { get; set; }
        public GoogleProtoDataTypeEnum DATA_TYPE_PROTOBUF { get; set; }

        public string COLUMN_CLASS_DATA_TYPE { get; set; }

        public bool IS_OUTPUT { get; set; } = true;
        public string COLUMN_NAME_ENTITY { get; set; }
        public string COLUMN_NAME_PROTO { get; set; }
        public string COLUMN_NAME_MODEL { get; set; }
        public string COLUMN_NAME_PARAM { get; set; }

        public string DATA_TYPE_CSHARP_STRING { get; set; }
        public string DATA_TYPE_CSHARP_STRING_NULL { get; set; }

        public string COLUMN_NAME_TYPESCRIPT { get; set; }
        public string DATA_TYPE_TYPESCRIPT_STRING { get; set; }
    }
}