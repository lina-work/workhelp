﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkHelp.Wpf.Models.Game
{
    /// <summary>
    /// 賽事資料模型
    /// </summary>
    public class TMeeting
    {
        /// <summary>
        /// 賽事 id
        /// </summary>
        public string id { get; set; }

        /// <summary>
        /// 賽事名稱
        /// </summary>
        public string in_title { get; set; }

        /// <summary>
        /// 賽事開始
        /// </summary>
        public DateTime in_date_s { get; set; }

        /// <summary>
        /// 賽事結束
        /// </summary>
        public DateTime in_date_e { get; set; }

        /// <summary>
        /// 報名開始日
        /// </summary>
        public DateTime in_state_time_start { get; set; }

        /// <summary>
        /// 報名截止日
        /// </summary>
        public DateTime in_state_time_end { get; set; }

        /// <summary>
        /// 專屬展示網址
        /// </summary>
        public string in_url { get; set; }

        /// <summary>
        /// 專屬報名網址
        /// </summary>
        public string in_register_url { get; set; }

        /// <summary>
        /// 正取人數
        /// </summary>
        public string in_taking { get; set; }

        /// <summary>
        /// 實際正取
        /// </summary>
        public string in_real_taking { get; set; }
    }
}
