﻿using System;

namespace WorkHelp.Wpf.Models
{
    /// <summary>
    /// 會員額外個資
    /// </summary>
    public class UserAdditionalModel
    {
        /// <summary>
        /// 國家
        /// </summary>
        public string CountryName { get; set; }

        /// <summary>
        /// 國碼
        /// </summary>
        public string CountryCode { get; set; }

        /// <summary>
        /// 性別
        /// </summary>
        public string Gender { get; set; }

        /// <summary>
        /// 生日
        /// </summary>
        public DateTime? Birthday { get; set; }

        /// <summary>
        /// 縣市
        /// </summary>
        public string CityName { get; set; }

        /// <summary>
        /// 鄉鎮市區
        /// </summary>
        public string District { get; set; }

        /// <summary>
        /// 郵遞區號
        /// </summary>
        public string Zipcode { get; set; }

        /// <summary>
        /// 地址
        /// </summary>
        public string Address { get; set; }
    }
}