﻿namespace WorkHelp.Wpf.Models
{
    public class UserBasic
    {
        public string GuidID { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public string MoreJson { get; set; }
        public string NewMoreJson { get; set; }
    }
}