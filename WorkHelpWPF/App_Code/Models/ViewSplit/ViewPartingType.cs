﻿namespace WorkHelp.Wpf.Models.ViewSplit
{
    /// <summary>
    /// 畫面分割類型
    /// </summary>
    public enum ViewPartingType
    {
        /// <summary>
        /// 垂直雙欄+右側欄
        /// </summary>
        Row1Column2RightSide = 50201,

        /// <summary>
        /// 水平雙欄+右側欄
        /// </summary>
        Row2Column1RightSide = 50211,

        /// <summary>
        /// 垂直雙欄
        /// </summary>
        Row1Column2 = 50202,

        /// <summary>
        /// 垂直三欄
        /// </summary>
        Row1Column3 = 50203,

        /// <summary>
        /// 四欄分割
        /// </summary>
        Row2Column2 = 50204,

        /// <summary>
        /// 五欄分割
        /// </summary>
        Row2Column221 = 50205,

        /// <summary>
        /// 六欄分割 2X3
        /// </summary>
        Row2Column3 = 50206,

        /// <summary>
        /// 八欄分割
        /// </summary>
        Row3Column332 = 50208,

        /// <summary>
        /// 九欄分割
        /// </summary>
        Row3Column3 = 50209,

        /// <summary>
        /// 水平雙欄
        /// </summary>
        Row2Column1 = 50212,

        /// <summary>
        /// 水平三欄
        /// </summary>
        Row3Column1 = 50213,

        /// <summary>
        /// 六欄分割 3X2
        /// </summary>
        Row3Column2 = 50216,

        /// <summary>
        /// 垂直欄+水平雙欄
        /// </summary>
        Row1Column11 = 50223,

        /// <summary>
        /// 複合式開發
        /// </summary>
        Complex1 = 50231,
    }
}