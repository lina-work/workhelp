﻿namespace WorkHelp.Wpf.Models.ViewSplit
{
    /// <summary>
    /// 畫面切割組態
    /// </summary>
    public class ViewParting
    {
        /// <summary>
        /// 畫面組態編號
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 畫面組態名稱
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 上左側 Url
        /// </summary>
        public string TopLeftUrl { get; set; }

        /// <summary>
        /// 上中側 Url
        /// </summary>
        public string TopCenterUrl { get; set; }

        /// <summary>
        /// 上右側 Url
        /// </summary>
        public string TopRightUrl { get; set; }

        /// <summary>
        /// 上右側邊欄 Url
        /// </summary>
        public string TopRightSideUrl { get; set; }

        /// <summary>
        /// 央左側 Url
        /// </summary>
        public string MiddleLeftUrl { get; set; }

        /// <summary>
        /// 央中側 Url
        /// </summary>
        public string MiddleCenterUrl { get; set; }

        /// <summary>
        /// 央右側 Url
        /// </summary>
        public string MiddleRightUrl { get; set; }

        /// <summary>
        /// 下左側 Url
        /// </summary>
        public string BottomLeftUrl { get; set; }

        /// <summary>
        /// 下中側 Url
        /// </summary>
        public string BottomCenterUrl { get; set; }

        /// <summary>
        /// 下右側 Url
        /// </summary>
        public string BottomRightUrl { get; set; }

        /// <summary>
        /// 首欄
        /// </summary>
        public string HeaderUrl { get; set; }

        /// <summary>
        /// 主欄
        /// </summary>
        public string MainUrl { get; set; }

        /// <summary>
        /// 次欄
        /// </summary>
        public string SubUrl { get; set; }

        /// <summary>
        /// 尾欄
        /// </summary>
        public string FooterUrl { get; set; }

        /// <summary>
        /// 左側側邊欄 Url
        /// </summary>
        public string LeftSideUrl { get; set; }

        /// <summary>
        /// 右側側邊欄 Url
        /// </summary>
        public string RightSideUrl { get; set; }
    }
}