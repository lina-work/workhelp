﻿namespace WorkHelp.Wpf.Models.ViewSplit
{
    /// <summary>
    /// 畫面分割類型
    /// </summary>
    public enum ViewPartingTypeOld
    {
        /// <summary>
        /// 垂直雙欄+右側欄
        /// </summary>
        Row1Column2RightSide = 50101,

        /// <summary>
        /// 水平雙欄+右側欄
        /// </summary>
        Row2Column1RightSide = 50111,

        /// <summary>
        /// 垂直雙欄
        /// </summary>
        Row1Column2 = 50102,

        /// <summary>
        /// 垂直三欄
        /// </summary>
        Row1Column3 = 50103,

        /// <summary>
        /// 四欄分割
        /// </summary>
        Row2Column2 = 50104,

        /// <summary>
        /// 六欄分割 2X3
        /// </summary>
        Row2Column3 = 50106,

        /// <summary>
        /// 九欄分割
        /// </summary>
        Row3Column3 = 50109,

        /// <summary>
        /// 水平雙欄
        /// </summary>
        Row2Column1 = 50112,

        /// <summary>
        /// 水平三欄
        /// </summary>
        Row3Column1 = 50113,

        /// <summary>
        /// 六欄分割 3X2
        /// </summary>
        Row3Column2 = 50116,

        /// <summary>
        /// 要錢而且很昂貴
        /// </summary>
        Expensive = 50200,
    }
}