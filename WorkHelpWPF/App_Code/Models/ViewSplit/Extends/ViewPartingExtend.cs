﻿using System.Collections.Generic;

namespace WorkHelp.Wpf.Models.ViewSplit.Extends
{
    /// <summary>
    /// ViewParting 擴充函式
    /// </summary>
    public static class ViewPartingExtend
    {
        /// <summary>
        /// 項目資料複製
        /// </summary>
        public static void Clone(this ViewParting selected, ViewParting ui, bool updateName = true)
        {
            if (updateName)
            {
                selected.Name = ui.Name;
            }

            selected.TopLeftUrl = ui.TopLeftUrl;
            selected.TopCenterUrl = ui.TopCenterUrl;
            selected.TopRightUrl = ui.TopRightUrl;
            selected.TopRightSideUrl = ui.TopRightSideUrl;

            selected.MiddleLeftUrl = ui.MiddleLeftUrl;
            selected.MiddleCenterUrl = ui.MiddleCenterUrl;
            selected.MiddleRightUrl = ui.MiddleRightUrl;

            selected.BottomLeftUrl = ui.BottomLeftUrl;
            selected.BottomCenterUrl = ui.BottomCenterUrl;
            selected.BottomRightUrl = ui.BottomRightUrl;
        }

        /// <summary>
        /// 分割名稱
        /// </summary>
        public static string Name(this ViewPartingType e)
        {
            switch (e)
            {
                case ViewPartingType.Row1Column2: return "垂直雙欄";
                case ViewPartingType.Row1Column3: return "垂直三欄";
                case ViewPartingType.Row2Column2: return "四欄分割";
                case ViewPartingType.Row2Column221: return "五欄分割";
                case ViewPartingType.Row2Column3: return "六欄分割 2X3";
                case ViewPartingType.Row3Column332: return "八欄分割";
                case ViewPartingType.Row3Column3: return "九欄分割";

                case ViewPartingType.Row2Column1: return "水平雙欄";
                case ViewPartingType.Row3Column1: return "水平三欄";
                case ViewPartingType.Row3Column2: return "六欄分割 3X2";

                case ViewPartingType.Row1Column11: return "垂直欄+水平雙欄";

                case ViewPartingType.Row1Column2RightSide: return "垂直雙欄 + 右側欄";
                case ViewPartingType.Row2Column1RightSide: return "水平雙欄 + 右側欄";

                case ViewPartingType.Complex1: return "複合式開發";

                default: return string.Empty;
            }
        }

        /// <summary>
        /// 畫面分割類型選單
        /// </summary>
        /// <param name="ui">控件</param>
        public static Dictionary<int, string> GetViewPartingList()
        {
            Dictionary<int, string> list = new Dictionary<int, string>
            {
                { (int)ViewPartingType.Row1Column2, ViewPartingType.Row1Column2.Name() },
                { (int)ViewPartingType.Row1Column3, ViewPartingType.Row1Column3.Name() },
                { (int)ViewPartingType.Row2Column2, ViewPartingType.Row2Column2.Name() },
                { (int)ViewPartingType.Row2Column221, ViewPartingType.Row2Column221.Name() },
                { (int)ViewPartingType.Row2Column3, ViewPartingType.Row2Column3.Name() },
                { (int)ViewPartingType.Row3Column332, ViewPartingType.Row3Column332.Name() },
                { (int)ViewPartingType.Row3Column3, ViewPartingType.Row3Column3.Name() },

                { (int)ViewPartingType.Row2Column1, ViewPartingType.Row2Column1.Name() },
                { (int)ViewPartingType.Row3Column1, ViewPartingType.Row3Column1.Name() },
                { (int)ViewPartingType.Row3Column2, ViewPartingType.Row3Column2.Name() },

                { (int)ViewPartingType.Row1Column11, ViewPartingType.Row1Column11.Name() },

                { (int)ViewPartingType.Row1Column2RightSide, ViewPartingType.Row1Column2RightSide.Name() },
                { (int)ViewPartingType.Row2Column1RightSide, ViewPartingType.Row2Column1RightSide.Name() },

                { (int)ViewPartingType.Complex1, ViewPartingType.Complex1.Name() },
            };

            return list;
        }
    }
}