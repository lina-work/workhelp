﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkHelp.Wpf.Models.Aras
{
    public class ItemProperty
    {
        public string TableName { get; set; }
        public string FieldName { get; set; }
        public string Statement { get; set; }
    }
}
