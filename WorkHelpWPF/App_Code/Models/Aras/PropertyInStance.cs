﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkHelp.Wpf.Models.Aras
{
    /// <summary>
    /// Property 實例
    /// </summary>
    public class PropertyInstance
    {
        /// <summary>
        /// 名稱
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// 標籤
        /// </summary>
        public string label { get; set; }

        /// <summary>
        /// 標籤
        /// </summary>
        public string label_zt { get; set; }

        /// <summary>
        /// 資料類型
        /// </summary>
        public string data_type { get; set; }

        /// <summary>
        /// 長度
        /// </summary>
        public int? stored_length { get; set; }

        /// <summary>
        /// 預設值
        /// </summary>
        public string default_value { get; set; }

        /// <summary>
        /// 預設值
        /// </summary>
        public string default_value_zt { get; set; }

        /// <summary>
        /// 外部屬性
        /// </summary>
        public string foreign_property { get; set; }

        /// <summary>
        /// 簡易表單
        /// </summary>
        public string in_web_view { get; set; }

        /// <summary>
        /// 行動版新增
        /// </summary>
        public string in_web_add { get; set; }

        /// <summary>
        /// 行動版編輯
        /// </summary>
        public string in_web_edit { get; set; }

        /// <summary>
        /// 搜索中隱藏
        /// </summary>
        public string is_hidden { get; set; }

        /// <summary>
        /// 關係中隱藏
        /// </summary>
        public string is_hidden2 { get; set; }

        /// <summary>
        /// 索引
        /// </summary>
        public string is_indexed { get; set; }

        /// <summary>
        /// 唯一
        /// </summary>
        public string is_keyed { get; set; }

        /// <summary>
        /// 必填
        /// </summary>
        public string is_required { get; set; }

        /// <summary>
        /// 鍵名排序
        /// </summary>
        public string keyed_name_order { get; set; }

        /// <summary>
        /// 序號
        /// </summary>
        public string sort_order { get; set; }

        /// <summary>
        /// 式樣
        /// </summary>
        public string pattern { get; set; }

    }
}
