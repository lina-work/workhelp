﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkHelp.Wpf.Models.Aras
{
   public class ForeignProperty
    {
        public string Table_Id { get; set; }
        public string Table_Name { get; set; }
        public string Table_Label { get; set; }

        public string Form_Id { get; set; }
        public string Form_Name { get; set; }

        public string Body_Id { get; set; }

        public string Field_Id { get; set; }
        public string Field_Name { get; set; }
        public string Field_Keyed_Name { get; set; }
        public string Field_Label { get; set; }
        public string Field_Lable_ZT { get; set; }
        public string Field_Lable_ZC { get; set; }

        public string Field_Property_Id { get; set; }
        public string Field_Property_Name { get; set; }

        public string Data_Type { get; set; }
        public string Data_Source { get; set; }
        public string Foreign_Propery { get; set; }
    }
}
