namespace WorkHelp.Wpf.Models.Aras
{
    /// <summary>
    /// 物件類型
    /// </summary>
    public class ItemType
    {
        /// <summary>
        /// 編號
        /// </summary>
        public string ID { get; set; }
        /// <summary>
        /// 名稱
        /// </summary>
        public string NAME { get; set; }
        /// <summary>
        /// 鍵名(人類可識別)
        /// </summary>
        public string KEYED_NAME { get; set; }
        /// <summary>
        /// 複數標籤
        /// </summary>
        public string LABEL_PLURAL_ZT { get; set; }
        /// <summary>
        /// 單數標籤
        /// </summary>
        public string LABEL_ZT { get; set; }
        /// <summary>
        /// 結構檢視
        /// </summary>
        public string STRUCTURE_VIEW { get; set; }
        /// <summary>
        /// 是否為關聯類別
        /// </summary>
        public string IS_RELATIONSHIP { get; set; }
        /// <summary>
        /// 實體資料
        /// </summary>
        public string INSTANCE_DATA { get; set; }
        /// <summary>
        /// 資源編號
        /// </summary>
        public string SOURCE_ID { get; set; }
        /// <summary>
        /// 資源名稱
        /// </summary>
        public string SOURCE_NAME { get; set; }
        /// <summary>
        /// 關聯編號
        /// </summary>
        public string RELATED_ID { get; set; }
        /// <summary>
        /// 關聯名稱
        /// </summary>
        public string RELATED_NAME { get; set; }

        #region Method
        /// <summary>
        /// 備註
        /// </summary>
        public string COMMENTS { get; set; }
        /// <summary>
        /// 程式碼
        /// </summary>
        public string METHOD_CODE { get; set; }
        /// <summary>
        /// 程式碼類別
        /// </summary>
        public string METHOD_TYPE { get; set; }
        #endregion

        /// <summary>
        /// 名稱是否已完成變更
        /// </summary>
        public string IS_CHANGED { get; set; }

        /// <summary>
        /// 需要進行變更 (不論是否完成變更)
        /// </summary>
        public bool NeedChanged { get; set; }

        /// <summary>
        /// 已完成內容變更
        /// </summary>
        public bool HasFinished { get; set; }

        /// <summary>
        /// 原名稱
        /// </summary>
        public string OLD_NAME { get; set; }
        /// <summary>
        /// 新名稱
        /// </summary>
        public string NEW_NAME { get; set; }

        /// <summary>
        /// 原名稱
        /// </summary>
        public string OLD_TAG1 { get; set; }
        /// <summary>
        /// 新名稱
        /// </summary>
        public string NEW_TAG1 { get; set; }

        /// <summary>
        /// 原名稱
        /// </summary>
        public string OLD_TAG2 { get; set; }
        /// <summary>
        /// 新名稱
        /// </summary>
        public string NEW_TAG2 { get; set; }

        /// <summary>
        /// 原名稱
        /// </summary>
        public string OLD_TAG3 { get; set; }
        /// <summary>
        /// 新名稱
        /// </summary>
        public string NEW_TAG3 { get; set; }

        /// <summary>
        /// 原名稱
        /// </summary>
        public string OLD_TAG4 { get; set; }
        /// <summary>
        /// 新名稱
        /// </summary>
        public string NEW_TAG4 { get; set; }

        /// <summary>
        /// 是否為
        /// </summary>
        public bool IsMethod { get; set; }


        /// <summary>
        /// 屬性
        /// </summary>
        public string FieldName { get; set; }

        /// <summary>
        /// 指令
        /// </summary>
        public string Statement { get; set; }
    }
}