﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkHelp.Wpf.Models.Aras
{
    /// <summary>
    /// Property 實體
    /// </summary>
    public class PropertyEntity
    {
        /// <summary>
        /// 名稱
        /// </summary>
        public string NAME { get; set; }

        /// <summary>
        /// 標籤
        /// </summary>
        public string LABEL { get; set; }

        /// <summary>
        /// 標籤(正體中文)
        /// </summary>
        public string LABEL_ZT { get; set; }

        /// <summary>
        /// 資料類型
        /// </summary>
        public string DATA_TYPE { get; set; }

    }
}
