﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkHelp.Wpf.Models.Aras
{
    public class ItemLock
    {
        /// <summary>
        /// 被鎖定類別
        /// </summary>
        public string C1 { get; set; }
        /// <summary>
        /// 被鎖定名稱
        /// </summary>
        public string C2 { get; set; }
        /// <summary>
        /// 被鎖定說明
        /// </summary>
        public string C3 { get; set; }
    }
}
