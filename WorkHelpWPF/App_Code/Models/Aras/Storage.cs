﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkHelp.Wpf.Models.Aras
{
    /// <summary>
    /// 儲存體
    /// </summary>
    public class Storage
    {
        /// <summary>
        /// 資料庫名稱
        /// </summary>
        public string DatabaseName { get; set; }

        /// <summary>
        /// 資料庫連線
        /// </summary>
        public string ConnectionString { get; set; }

        /// <summary>
        /// 資料夾完整路徑
        /// </summary>
        public string FolderName { get; set; }

        /// <summary>
        /// 檔案名稱
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// 內文
        /// </summary>
        public string FileContent { get; set; }

        /// <summary>
        /// 備註
        /// </summary>
        public string Comment { get; set; }

        /// <summary>
        /// 訊息
        /// </summary>
        public string Message { get; set; }
    }
}
