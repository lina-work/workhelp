﻿namespace WorkHelp.Wpf.Models
{
    public class Win401Model
    {
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
        public string TableName { get; set; }
        public string TargetName { get; set; }
        public string FolderName { get; set; }
        public string MainName { get; set; }
        public string ControllerName { get; set; }
        public string NameSpaceName { get; set; }
        public string MainRouteName { get; set; }
        public string SubRouteName { get; set; }
        public bool IsValid { get; set; }
        public bool IsEasyName { get; set; }

        public bool IsArasMode { get; set; }
    }
}