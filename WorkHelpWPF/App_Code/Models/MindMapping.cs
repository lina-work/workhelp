﻿using System;

namespace WorkHelp.Wpf.Models
{
    public class MindNode
    {
        public int id { get; set; }
        public string label { get; set; }
        public string group { get; set; }
    }
    public class MindEdges
    {
        public int from { get; set; }
        public int to { get; set; }
    }
}
