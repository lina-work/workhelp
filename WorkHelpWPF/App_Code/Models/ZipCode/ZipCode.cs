﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace WorkHelp.Wpf.Models.ZipCode
{
    public class RoadList
    {
        public string RoadName { get; set; }
        public string RoadEngName { get; set; }
    }

    [JsonObject(MemberSerialization.OptIn)]
    public class AreaList
    {
        [JsonProperty]
        public string ZipCode { get; set; }

        [JsonProperty]
        public string AreaName { get; set; }

        [JsonProperty]
        public string AreaEngName { get; set; }

        public List<RoadList> RoadList { get; set; }
    }

    [JsonObject(MemberSerialization.OptIn)]
    public class City
    {
        [JsonProperty]
        public string CityName { get; set; }

        [JsonProperty]
        public string CityEngName { get; set; }

        [JsonProperty]
        public List<AreaList> AreaList { get; set; }

        public int Sequence { get; set; }
    }
}