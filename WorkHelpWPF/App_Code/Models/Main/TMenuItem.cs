﻿using WorkHelp.Wpf.Enums;

namespace WorkHelp.Wpf.Models.Main
{
    /// <summary>
    /// 選單項目
    /// </summary>
    public class TMenuItem
    {
        /// <summary>
        /// 型別代碼
        /// </summary>
        public int Code { get; set; }

        /// <summary>
        /// 型別名稱
        /// </summary>
        public string TypeName { get; set; }

        /// <summary>
        /// 視窗開啟模式
        /// </summary>
        public WindowShowMode ShowMode { get; set; }

        /// <summary>
        /// 視窗值
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// 組態檔名稱
        /// </summary>
        public string ConfigFile { get; set; }

        /// <summary>
        /// Json 字串
        /// </summary>
        public string Json { get; set; }
    }
}