﻿using WorkHelp.Wpf.Models.ViewSplit;

namespace WorkHelp.Wpf.Models.Main
{
    /// <summary>
    /// 自訂節點成員
    /// </summary>
    public class MenuNodeMember
    {
        /// <summary>
        /// 親編號
        /// </summary>
        public string ParentId { get; set; }

        /// <summary>
        /// 編號
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 名稱
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 是否為資料夾
        /// </summary>
        public bool IsFolder { get; set; }

        /// <summary>
        /// 圖檔路徑
        /// </summary>
        public string Photo { get; set; }

        /// <summary>
        /// 畫面模式代碼
        /// </summary>
        public int ModeCode { get; set; }

        /// <summary>
        /// 畫面模式類型
        /// </summary>
        public ViewPartingType ModeType { get; set; }

        /// <summary>
        /// 畫面模式名稱
        /// </summary>
        public string ModeName { get; set; }

        /// <summary>
        /// 組態名稱
        /// </summary>
        public string ConfigFile { get; set; }

        /// <summary>
        /// 全部資料
        /// </summary>
        public string FullName { get; set; }
    }
}