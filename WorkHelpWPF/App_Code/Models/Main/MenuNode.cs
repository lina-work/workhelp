﻿using System.Collections.Generic;

namespace WorkHelp.Wpf.Models.Main
{
    /// <summary>
    /// 自訂節點
    /// </summary>
    public class MenuNode
    {
        /// <summary>
        /// 建構元
        /// </summary>
        public MenuNode()
        {
            this.Members = new List<MenuNodeMember>();
        }

        /// <summary>
        /// 編號
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 名稱
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 成員項目
        /// </summary>
        public List<MenuNodeMember> Members { get; set; }
    }
}