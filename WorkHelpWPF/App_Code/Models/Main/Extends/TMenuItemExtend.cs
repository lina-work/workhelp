﻿using System;
using System.Windows.Controls;
using WorkHelp.Wpf.Enums;

namespace WorkHelp.Wpf.Models.Main.Extends
{
    /// <summary>
    /// TMenuItem 擴充
    /// </summary>
    public class TMenuItemExtend
    {
        /// <summary>
        /// 建構元
        /// </summary>
        protected TMenuItemExtend() { }

        /// <summary>
        /// 取得標籤物件
        /// </summary>
        public static TMenuItem GetItem(string json)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(json))
                {
                    return Newtonsoft.Json.JsonConvert.DeserializeObject<TMenuItem>(json);
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                WorkHelp.Logging.TLog.Watch(message: ex.ToString());
                return null;
            }
        }

        /// <summary>
        /// 取得標籤資料
        /// </summary>
        public static TMenuItem GetTagData(MenuItem item)
        {
            return GetTagData((string)item.Tag);
        }

        /// <summary>
        /// 取得標籤資料
        /// </summary>
        public static TMenuItem GetTagData(string tag)
        {
            // Tag Format: code與其他資料，以 | 分割，前綴意義如下:
            //   c: ConfigName
            //   v: New Window Tag Value
            //   m: New Window ShowMode
            // 範例: 50101|c:Config50100|v:天氣概況|m:2

            // 選單項目
            TMenuItem result = new TMenuItem
            {
                Code = 0,
                ShowMode = WindowShowMode.MultiDialog,
                Value = string.Empty,
                ConfigFile = string.Empty
            };

            // 解析標籤資料
            SetTagData(result, tag);
            // 將標籤物件轉為 Json 字串
            result.Json = Newtonsoft.Json.JsonConvert.SerializeObject(result);

            return result;
        }

        /// <summary>
        /// 解析標籤資料
        /// </summary>
        private static void SetTagData(TMenuItem item, string tag)
        {
            if (string.IsNullOrEmpty(tag))
            {
                return;
            }

            string[] args = tag.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
            if (args == null || args.Length == 0)
            {
                return;
            }

            // 取得視窗型別代碼
            item.Code = GetInt(args[0]);
            // 視窗型別長度不足(需 5 碼)，自動補 0
            item.TypeName = $"Win{item.Code.ToString().PadRight(5, '0')}";

            if (args.Length > 1)
            {
                SetTagOtherData(item, args);
            }
        }

        /// <summary>
        /// 解析標籤其他資料
        /// </summary>
        private static void SetTagOtherData(TMenuItem item, string[] kvList)
        {
            for (int i = 1; i < kvList.Length; i++)
            {
                (bool ok, string key, string value) = GetKV(kvList[i]);
                if (!ok) continue;

                // 範例: 50101|c:Config50100|v:天氣概況|m:2

                switch (key)
                {
                    case "C":
                        item.ConfigFile = value;
                        break;

                    case "V":
                        item.Value = value;
                        break;

                    case "M":
                        item.ShowMode = (WindowShowMode)GetInt(value);
                        break;

                    default:
                        break;
                }
            }
        }

        /// <summary>
        /// 解析鍵值
        /// </summary>
        private static (bool ok, string key, string value) GetKV(string kvLine)
        {
            // 範例: 50101|c:Config50100|v:天氣概況|m:2
            string[] kvArgs = kvLine.Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);

            if (kvArgs == null || kvArgs.Length != 2)
            {
                return (false, string.Empty, string.Empty);
            }

            string key = kvArgs[0].Replace(":", string.Empty).Trim().ToUpper();
            string value = kvArgs[1].Trim();

            return (true, key, value);
        }

        /// <summary>
        /// 轉為整數
        /// </summary>
        private static int GetInt(string value)
        {
            if (int.TryParse(value, out int result))
            {
                return result;
            }
            else
            {
                return 0;
            }
        }
    }
}