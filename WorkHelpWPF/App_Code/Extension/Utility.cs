﻿using DingOK.Library;
using DingOK.Library.WinAPI;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using WorkHelpWPF.Models.ZipCode;

namespace WorkHelpWPF
{
    public static class Utility
    {
        #region Ini

        public static T FromConfig<T>(string vSectName) where T : class, new()
        {
            var result = new T();
            FromConfig(result, vSectName);
            return result;
        }

        public static void FromConfig<T>(T item, string vSectName)
        {
            if (item == null) return;

            var ini = new IniUtl<T>();
            ini.SectName = vSectName;
            ini.Item = item;

            var attribute = typeof(T);
            var plist = attribute.GetProperties();

            foreach (var p in plist)
            {
                switch (Type.GetTypeCode(p.PropertyType))
                {
                    case TypeCode.Boolean:
                        p.SetValue(item, IniUtl.ReadBln(ini.SectName, p.Name), null);
                        break;

                    case TypeCode.String:
                        p.SetValue(item, IniUtl.ReadStr(ini.SectName, p.Name).Trim(), null);
                        break;

                    case TypeCode.Int32:
                        p.SetValue(item, IniUtl.ReadInt(ini.SectName, p.Name), null);
                        break;

                    case TypeCode.DateTime:
                        p.SetValue(item, IniUtl.ReadDtm(ini.SectName, p.Name), null);
                        break;

                    case TypeCode.Double:
                        p.SetValue(item, IniUtl.ReadDbl(ini.SectName, p.Name), null);
                        break;

                    case TypeCode.Decimal:
                        p.SetValue(item, IniUtl.ReadDcm(ini.SectName, p.Name), null);
                        break;
                }
            }
        }

        public static bool SaveConfig<T>(T item, string vSectName = "")
        {
            if (item == null || string.IsNullOrEmpty(vSectName))
            {
                return false;
            }
            else
            {
                var ini = new IniUtl<T>();
                ini.SectName = vSectName;
                ini.Item = item;

                var attribute = typeof(T);
                var plist = attribute.GetProperties();

                foreach (var p in plist)
                {
                    switch (Type.GetTypeCode(p.PropertyType))
                    {
                        case TypeCode.Boolean:
                            var vBln = (bool)p.GetValue(ini.Item, null);
                            IniUtl.WriteStr(ini.SectName, p.Name, vBln.ToString());
                            break;

                        case TypeCode.String:
                            var vStr = TypeUtl.toStr(p.GetValue(ini.Item, null));
                            IniUtl.WriteStr(ini.SectName, p.Name, vStr);
                            break;

                        case TypeCode.Int32:
                            var vInt = TypeUtl.IntToStr((int)p.GetValue(ini.Item, null));
                            IniUtl.WriteStr(ini.SectName, p.Name, vInt);
                            break;

                        case TypeCode.DateTime:
                            var vDtm = TypeUtl.DateTimeToStr(p.GetValue(ini.Item, null));
                            IniUtl.WriteStr(ini.SectName, p.Name, vDtm);
                            break;

                        case TypeCode.Double:
                            var vDbl = TypeUtl.DblToStr((double)p.GetValue(ini.Item, null));
                            IniUtl.WriteStr(ini.SectName, p.Name, vDbl);
                            break;

                        case TypeCode.Decimal:
                            var vDcm = TypeUtl.DcmToStr((decimal)p.GetValue(ini.Item, null));
                            IniUtl.WriteStr(ini.SectName, p.Name, vDcm);
                            break;
                    }
                }

                return true;
            }
        }

        #endregion Ini

        #region UI

        public static TModel FromUI<TModel, TWin>(DependencyObject wnd)
            where TModel : class, new()
            where TWin : DependencyObject
        {
            var result = new TModel();
            FromUI<TModel, TWin>(result, wnd);
            return result;
        }

        public static void IntoUI<TWin, TModel>(DependencyObject wnd, TModel item) where TWin : DependencyObject
        {
            if (item == null) return;

            var textEnums = FindVisualChildren<TextBox>(wnd);
            //if (textEnums == null) return;

            var chkEnums = FindVisualChildren<CheckBox>(wnd);
            //if (chkEnums == null) return;

            var textList = (textEnums != null && textEnums.Count() > 0) ? textEnums.ToList() : new List<TextBox>(); //textEnums.ToList();
            var chkList = (chkEnums != null && chkEnums.Count() > 0) ? chkEnums.ToList() : new List<CheckBox>();

            var attribute = typeof(TModel);
            var plist = attribute.GetProperties();
            foreach (var p in plist)
            {
                if (p.PropertyType == typeof(string))
                {
                    var v = TypeUtl.toStr(p.GetValue(item, null));
                    SetTextVal(textList, "txt" + p.Name, v);
                }
                else if (p.PropertyType == typeof(int))
                {
                    var v = TypeUtl.toStr(p.GetValue(item, null));
                    SetTextVal(textList, "txt" + p.Name, v);
                }
                else if (p.PropertyType == typeof(bool))
                {
                    var v = TypeUtl.toBool(p.GetValue(item, null));
                    SetChkVal(chkList, "chk" + p.Name, v);
                }
            }
        }

        public static void FromUI<TModel, TWin>(TModel item, DependencyObject wnd) where TWin : DependencyObject
        {
            if (item == null) return;

            var textEnums = FindVisualChildren<TextBox>(wnd);
            //if (textEnums == null) return;

            var chkEnums = FindVisualChildren<CheckBox>(wnd);
            //if (chkEnums == null) return;

            var textList = (textEnums != null && textEnums.Count() > 0) ? textEnums.ToList() : new List<TextBox>();
            var chkList = (chkEnums != null && chkEnums.Count() > 0) ? chkEnums.ToList() : new List<CheckBox>();

            var attribute = typeof(TModel);
            var plist = attribute.GetProperties();
            foreach (var p in plist)
            {
                if (p.PropertyType == typeof(string))
                {
                    var v = GetTextVal(textList, "txt" + p.Name);
                    TypeUtl.SetPropetyVal(item, p, v);
                }
                else if (p.PropertyType == typeof(int))
                {
                    var v = GetTextVal(textList, "txt" + p.Name);
                    TypeUtl.SetPropetyVal(item, p, v);
                }
                else if (p.PropertyType == typeof(bool))
                {
                    var v = GetChkVal(chkList, "chk" + p.Name);
                    TypeUtl.SetPrpValFromObj(item, p, v);
                }
            }
        }

        public static string GetTextVal(List<TextBox> vList, string vName)
        {
            var txt = vList.FirstOrDefault(x => x.Name == vName);
            if (txt == null)
                return "";
            else
                return txt.Text.Trim();
        }

        public static void SetTextVal(List<TextBox> vList, string vName, string vVal)
        {
            if (vVal == null) return;
            if (vList == null || vList.Count == 0) return;

            var txt = vList.FirstOrDefault(x => x.Name == vName);
            if (txt != null)
            {
                txt.Text = vVal.Trim();
            }
        }

        public static bool GetChkVal(List<CheckBox> vList, string vName)
        {
            if (vList == null || vList.Count == 0)
            {
                return false;
            }

            var chk = vList.FirstOrDefault(x => x.Name == vName);
            if (chk == null)
            {
                return false;
            }
            else
            {
                var result = chk.IsChecked;
                return (result.HasValue && result.Value);
            }
        }

        public static void SetChkVal(List<CheckBox> vList, string vName, bool vVal)
        {
            if (vList == null || vList.Count == 0) return;

            var txt = vList.FirstOrDefault(x => x.Name == vName);
            if (txt != null)
            {
                txt.IsChecked = vVal;
            }
        }

        public static void ClearText<TWin>(DependencyObject wnd) where TWin : DependencyObject
        {
            var textEnums = FindVisualChildren<TextBox>(wnd);
            if (textEnums == null) return;
            foreach (var txt in textEnums)
            {
                txt.Text = "";
            }
        }

        public static IEnumerable<T> FindVisualChildren<T>(DependencyObject depObj) where T : DependencyObject
        {
            if (depObj != null)
            {
                for (int i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++)
                {
                    DependencyObject child = VisualTreeHelper.GetChild(depObj, i);
                    if (child != null && child is T)
                    {
                        yield return (T)child;
                    }

                    foreach (T childOfChild in FindVisualChildren<T>(child))
                    {
                        yield return childOfChild;
                    }
                }
            }
        }

        //public static string GetTextVal(DependencyObject wnd, string childName)
        //{
        //    var txt = FindChild<TextBox>(wnd, childName);
        //    if (txt == null)
        //        return "";
        //    else
        //        return txt.Text.Trim();
        //}
        //public static T FindChild<T>(DependencyObject parent, string childName) where T : DependencyObject
        //{
        //    // Confirm parent and childName are valid.
        //    if (parent == null) return null;

        //    T foundChild = null;

        //    int childrenCount = VisualTreeHelper.GetChildrenCount(parent);
        //    for (int i = 0; i < childrenCount; i++)
        //    {
        //        var child = VisualTreeHelper.GetChild(parent, i);
        //        // If the child is not of the request child type child
        //        T childType = child as T;
        //        if (childType == null)
        //        {
        //            // recursively drill down the tree
        //            foundChild = FindChild<T>(child, childName);

        //            // If the child is found, break so we do not overwrite the found child.
        //            if (foundChild != null) break;
        //        }
        //        else if (!string.IsNullOrEmpty(childName))
        //        {
        //            var frameworkElement = child as FrameworkElement;
        //            // If the child's name is set for search
        //            if (frameworkElement != null && frameworkElement.Name == childName)
        //            {
        //                // if the child's name is of the request name
        //                foundChild = (T)child;
        //                break;
        //            }
        //        }
        //        else
        //        {
        //            // child element found.
        //            foundChild = (T)child;
        //            break;
        //        }
        //    }

        //    return foundChild;
        //}

        #endregion UI

        public static bool IsNullOrEmpty<T>(this IEnumerable<T> enumerable)
        {
            return enumerable == null || !enumerable.Any();
        }

        public enum MenuTitle
        {
            All,
            Select,
            Line,
            None
        }

        public static void ResetCityArea()
        {
            try
            {
                var path = @"D:\TFSServer\ArtTks\ArtTks.Web\Scripts\Extension\CityAreaRoadList.json";
                var content = System.IO.File.ReadAllText(path);
                var list = JsonConvert.DeserializeObject<List<City>>(content);
                var len = list.Count;
                for (var i = len - 1; i >= 0; i--)
                {
                    var item = list[i];
                    if (item.CityName.Contains("釣魚臺") || item.CityName.Contains("南海島"))
                    {
                        list.RemoveAt(i);
                    }
                    else
                    {
                        switch (item.CityName)
                        {
                            case "基隆市": item.Sequence = 1; break;
                            case "臺北市": item.Sequence = 2; break;
                            case "新北市": item.Sequence = 3; break;
                            case "桃園市": item.Sequence = 4; break;
                            case "新竹市": item.Sequence = 5; break;
                            case "新竹縣": item.Sequence = 6; break;
                            case "苗栗縣": item.Sequence = 7; break;
                            case "臺中市": item.Sequence = 8; break;
                            case "彰化縣": item.Sequence = 9; break;
                            case "南投縣": item.Sequence = 10; break;
                            case "雲林縣": item.Sequence = 11; break;
                            case "嘉義市": item.Sequence = 12; break;
                            case "嘉義縣": item.Sequence = 13; break;
                            case "臺南市": item.Sequence = 14; break;
                            case "高雄市": item.Sequence = 15; break;
                            case "屏東縣": item.Sequence = 16; break;
                            case "臺東縣": item.Sequence = 17; break;
                            case "花蓮縣": item.Sequence = 18; break;
                            case "宜蘭縣": item.Sequence = 19; break;
                            case "澎湖縣": item.Sequence = 20; break;
                            case "金門縣": item.Sequence = 21; break;
                            case "連江縣": item.Sequence = 22; break;
                            default: break;
                        }
                    }
                }

                list.Sort((x, y) => x.Sequence.CompareTo(y.Sequence));

                var simpleJson = JsonConvert.SerializeObject(list);
                var newpath = @"C:\Users\user\Downloads\CityAreaList.json";
                System.IO.File.WriteAllText(newpath, simpleJson, System.Text.Encoding.UTF8);
            }
            catch (Exception ex)
            {
            }
        }
    }
}