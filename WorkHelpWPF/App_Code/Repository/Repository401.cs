﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using WorkHelp.Wpf.Models;

namespace WorkHelp.Wpf.Repository
{
    public class Repository401
    {
        public static List<DataModel> GetDBList(string connectionString)
        {
            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    string statement = @"
                    SELECT
                        [name]
                    FROM
                        sys.sysdatabases WITH(NOLOCK)
                    WHERE
                        [name] not in ('master', 'tempdb', 'model', 'msdb', 'ReportServer', 'ReportServerTempDB')
                    ORDER BY
                        [name];
                ";

                    var result = connection.Query<DataModel>(statement);

                    if (result.Any())
                    {
                        return result.ToList();
                    }
                    else
                    {
                        return new List<DataModel>();
                    }
                }
            }
            catch (Exception ex)
            {
                return new List<DataModel>
                {
                    new DataModel
                    {
                        name = "發生錯誤"
                    }
                };
            }
        }

        public static List<DataModel> GetTableList(string connectionString)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                string statement = @"
                    SELECT
                        TABLE_NAME AS 'name'
                    FROM
                        INFORMATION_SCHEMA.TABLES WITH(NOLOCK)
                    ORDER BY
                        TABLE_TYPE, TABLE_NAME;
                ";

                var result = connection.Query<DataModel>(statement);

                if (result.Any())
                {
                    return result.ToList();
                }
                else
                {
                    return new List<DataModel>();
                }
            }
        }

        public static List<DataModel> GetViewList(string connectionString)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                string statement = @"
                    SELECT
                        TABLE_NAME AS 'name'
                    FROM
                        INFORMATION_SCHEMA.TABLES WITH(NOLOCK)
                    WHERE
                        TABLE_TYPE = N'VIEW'
                    ORDER BY
                        TABLE_TYPE, TABLE_NAME;
                ";

                var result = connection.Query<DataModel>(statement);

                if (result.Any())
                {
                    return result.ToList();
                }
                else
                {
                    return new List<DataModel>();
                }
            }
        }

        public static string GetTableDescription(string connectionString, string tableName)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                string statement = @"
                    SELECT
                        value
                    FROM
                        ::fn_listextendedproperty (NULL, 'schema', 'dbo', 'table', @TABLE_NAME, null, null);
                ";

                return connection.ExecuteScalar<string>(statement, new { TABLE_NAME = tableName });
            }
        }

        public static List<ColumnModel> GetColumnList(string connectionString, string tableName, bool isSorted = false)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                string statement = @"
SELECT
    *
FROM
    INFORMATION_SCHEMA.COLUMNS
WHERE
    TABLE_NAME = @TABLE_NAME
                ";
                if (isSorted)
                {
                    statement += @"
ORDER BY
    COLUMN_NAME
";
                }

                var result = connection.Query<ColumnModel>(statement, new { TABLE_NAME = tableName });

                if (result.Any())
                {
                    return result.ToList();
                }
                else
                {
                    return new List<ColumnModel>();
                }
            }
        }

        public static List<KeyModel> GetKeyList(string connectionString, string tableName)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                string statement = @"
                    SELECT
                        *
                    FROM
                        INFORMATION_SCHEMA.KEY_COLUMN_USAGE
                    WHERE
                        TABLE_NAME = @TABLE_NAME;
                ";

                var result = connection.Query<KeyModel>(statement, new { TABLE_NAME = tableName });

                if (result.Any())
                {
                    return result.ToList();
                }
                else
                {
                    return new List<KeyModel>();
                }
            }
        }

        public static List<ColumnModel> GetDescriptionList(string connectionString, string tableName)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                string statement = @"
                    SELECT
                        a.TABLE_NAME,
                        b.COLUMN_NAME,
                        (
                            SELECT
                                value
                            FROM
                                fn_listextendedproperty (NULL, 'schema', 'dbo', 'table', a.TABLE_NAME, 'column', default)

                            WHERE
                                name = 'MS_Description'
                                and objtype = 'COLUMN'
                                and objname Collate Chinese_Taiwan_Stroke_CI_AS = b.COLUMN_NAME
                        ) as 'COLUMN_DESCRIPTION'
                    FROM
                        INFORMATION_SCHEMA.TABLES  a
                        LEFT JOIN INFORMATION_SCHEMA.COLUMNS b ON (a.TABLE_NAME = b.TABLE_NAME)
                    WHERE
                        TABLE_TYPE = 'BASE TABLE'
                        and a.TABLE_NAME = @TABLE_NAME
                    ORDER BY
                        a.TABLE_NAME, ordinal_position;
                ";

                var result = connection.Query<ColumnModel>(statement, new { TABLE_NAME = tableName });

                if (result.Any())
                {
                    return result.ToList();
                }
                else
                {
                    return new List<ColumnModel>();
                }
            }
        }

        public static List<ColumnModel> GetViewDescriptionList(string connectionString, string tableName)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                string statement = @"
                    SELECT DISTINCT '' AS 'TABLE_NAME', COLUMN_NAME, COLUMN_DESCRIPTION FROM
                    (
                        SELECT
                            b.COLUMN_NAME,
                            (
                                SELECT
                                    value
                                FROM
                                    fn_listextendedproperty (NULL, 'schema', 'dbo', 'table', a.TABLE_NAME, 'column', default)

                                WHERE
                                    name = 'MS_Description'
                                    and objtype = 'COLUMN'
                                    and objname Collate Chinese_Taiwan_Stroke_CI_AS = b.COLUMN_NAME
                            ) as 'COLUMN_DESCRIPTION',
		                    ordinal_position AS 'ORDINAL_POSITION'
                        FROM
                            INFORMATION_SCHEMA.TABLES  a
                            LEFT JOIN INFORMATION_SCHEMA.COLUMNS b ON (a.TABLE_NAME = b.TABLE_NAME)
                        WHERE
                            TABLE_TYPE = 'BASE TABLE'
		                    AND COLUMN_NAME IN (SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = @TABLE_NAME)
                    ) t1
                    WHERE
                        ISNULL(COLUMN_DESCRIPTION, '') <> ''
                    ORDER BY
                        COLUMN_NAME
                ";

                var result = connection.Query<ColumnModel>(statement, new { TABLE_NAME = tableName });

                if (result.Any())
                {
                    return result.ToList();
                }
                else
                {
                    return new List<ColumnModel>();
                }
            }
        }
    }
}