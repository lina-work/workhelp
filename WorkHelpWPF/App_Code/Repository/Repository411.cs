﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using WorkHelp.Wpf.Models;
using WorkHelp.Wpf.Models.Aras;

namespace WorkHelp.Wpf.Repository
{
    public class Repository411
    {
        public static List<ItemType> GetItemTypeList(string connectionString)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                string statement = @"
                    SELECT 
                        t1.ID, 
                        t1.NAME, 
                        t1.KEYED_NAME, 
                        t1.LABEL_PLURAL_ZT, 
                        t1.LABEL_ZT, 
                        t1.STRUCTURE_VIEW, 
                        t1.IS_RELATIONSHIP, 
                        t1.INSTANCE_DATA
                    FROM 
                        [ITEMTYPE] t1
                    WHERE  
                        t1.name LIKE N'In_MEETING%'
                        OR t1.name LIKE N'In_SURVEY%'
                        OR t1.name LIKE N'In_CLA_MEETING%'
                        OR t1.name LIKE N'In_CLA_SURVEY%'
                    ORDER BY 
                        t1.name
                ";

                var result = connection.Query<ItemType>(statement);

                if (result.Any())
                {
                    return result.ToList();
                }
                else
                {
                    return new List<ItemType>();
                }
            }
        }

        public static List<ItemType> GetRelationShipTypeList(string connectionString)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                string statement = @"
                    SELECT 
                        t1.NAME,
                        t1.RELATIONSHIP_ID AS 'ID',
                        t1.LABEL_ZT,
                        t1.SOURCE_ID,
                        t2.NAME AS 'SOURCE_NAME',
                        t1.RELATED_ID,
                        t3.NAME AS 'RELATED_NAME'
                    FROM 
                        [RELATIONSHIPTYPE] t1
                        LEFT OUTER JOIN [ITEMTYPE] t2 ON t2.ID = t1.SOURCE_ID
                        LEFT OUTER JOIN [ITEMTYPE] t3 ON t3.ID = t1.RELATED_ID
                    WHERE  
                        t1.name LIKE N'In_MEETING%'
                        OR t1.name LIKE N'In_SURVEY%'
                        OR t1.name LIKE N'In_CLA_MEETING%'
                        OR t1.name LIKE N'In_CLA_SURVEY%'
                    ORDER BY 
                        t1.name
                ";

                var result = connection.Query<ItemType>(statement);

                if (result.Any())
                {
                    return result.ToList();
                }
                else
                {
                    return new List<ItemType>();
                }
            }
        }

        public static List<ItemType> GetAllMethodTypeList(string connectionString)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                string statement = @"
                    SELECT 
                        [ID], 
                        [NAME], 
                        [KEYED_NAME], 
                        [COMMENTS],
                        [METHOD_CODE], 
                        [METHOD_TYPE]
                    FROM 
                        [innovator].[METHOD] 
                    WHERE 
                        [IS_CURRENT] = '1'
                        AND [Name] LIKE N'In_%'
                    ORDER BY 
                        [METHOD_TYPE], [NAME]
                ";

                var result = connection.Query<ItemType>(statement);

                if (result.Any())
                {
                    return result.ToList();
                }
                else
                {
                    return new List<ItemType>();
                }
            }
        }

        public static List<ItemType> GetMethodTypeList(string connectionString, string targetName = "")
        {
            using (var connection = new SqlConnection(connectionString))
            {
                string statement = $@"
                    SELECT 
                        [ID], 
                        [NAME], 
                        [KEYED_NAME], 
                        [COMMENTS],
                        [METHOD_CODE], 
                        [METHOD_TYPE]
                    FROM 
                        [Innovator].[METHOD] 
                    WHERE 
                        [Name] = N'{targetName}'
                        AND [IS_CURRENT] = '1'
                ";

                var result = connection.Query<ItemType>(statement);

                if (result.Any())
                {
                    return result.ToList();
                }
                else
                {
                    return new List<ItemType>();
                }
            }
        }

        public static List<ItemType> GetClassMethodTypeList(string connectionString)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                string statement = @"
                    select 
                        [ID], 
                        [NAME], 
                        [KEYED_NAME], 
                        [COMMENTS],
                        [METHOD_CODE], 
                        [METHOD_TYPE]
                    FROM 
                        [innovator].[METHOD] 
                    WHERE 
                        [IS_CURRENT] = '1'
                        AND [Name] like 'In_Cla%'
                    ORDER BY 
                        [METHOD_TYPE], [NAME]
                ";

                var result = connection.Query<ItemType>(statement);

                if (result.Any())
                {
                    return result.ToList();
                }
                else
                {
                    return new List<ItemType>();
                }
            }
        }

        public static List<ItemProperty> GetDistinctPropertySQLList(string connectionString)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                string statement = @"
                    SELECT
                        t2.Name AS 'TableName',
                        t1.Name AS 'FieldName',
                        'SELECT DISTINCT [' + REPLACE(t1.NAME, ' ', '_') + '] FROM [innovator].[' + REPLACE(t2.NAME, ' ', '_') + '] WHERE ISNULL([' + t1.NAME + '], '''') <> '''' ORDER BY [' + t1.NAME + '];' AS 'Statement'
                    FROM
                        [innovator].[PROPERTY] t1
                        INNER JOIN [innovator].[ITEMTYPE] t2 ON t2.id = t1.SOURCE_ID
                    WHERE
                        t2.IMPLEMENTATION_TYPE = 'table'
                        AND t1.NAME LIKE '%itemtype%'
                        AND t1.DATA_TYPE <> 'foreign'
                    ORDER BY
                        t2.NAME,
                        t1.NAME
                ";

                var result = connection.Query<ItemProperty>(statement);

                if (result.Any())
                {
                    return result.ToList();
                }
                else
                {
                    return new List<ItemProperty>();
                }
            }
        }

        public static List<string> GetValueList(string statement, string connectionString)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                var result = connection.Query<string>(statement);

                if (result.Any())
                {
                    return result.ToList();
                }
                else
                {
                    return new List<string>();
                }
            }
        }

        public static List<T> GetUpdatePropertySQLList<T>(string connectionString)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                string statement = @"
                    SELECT 
                        t2.NAME AS 'Name',
                        t1.NAME AS 'FieldName',
                        'UPDATE t1 SET t1.LABEL_ZT = N''' + t1.LABEL_ZT +''' FROM [innovator].[PROPERTY] t1 INNER JOIN [innovator].[ITEMTYPE] t2 ON t2.id = t1.SOURCE_ID WHERE t2.NAME = N''' + t2.NAME + ''' AND t1.NAME = N''' + t1.NAME + ''';' AS 'Statement'
                    FROM
                        [innovator].[PROPERTY] t1
                        INNER JOIN [innovator].[ITEMTYPE] t2 ON t2.id = t1.SOURCE_ID
                    WHERE
                        t2.NAME LIKE 'In_Meeting%'
                        AND ISNULL(t1.LABEL_ZT, '') <> ''
                    ORDER BY
                        t2.NAME,
                        t1.NAME
                ";

                var result = connection.Query<T>(statement);

                if (result.Any())
                {
                    return result.ToList();
                }
                else
                {
                    return new List<T>();
                }
            }
        }

        public static List<T> GetUpdatePropertySQLList<T>(string connectionString, string table_name)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                string statement = $@"
                    SELECT 
                        t2.NAME AS 'Name',
                        t1.NAME AS 'FieldName',
                        'UPDATE t1 SET t1.LABEL_ZT = N''' + t1.LABEL_ZT +''' FROM [innovator].[PROPERTY] t1 INNER JOIN [innovator].[ITEMTYPE] t2 ON t2.id = t1.SOURCE_ID WHERE t2.NAME = N''' + t2.NAME + ''' AND t1.NAME = N''' + t1.NAME + ''';' AS 'Statement'
                    FROM
                        [innovator].[PROPERTY] t1
                        INNER JOIN [innovator].[ITEMTYPE] t2 ON t2.id = t1.SOURCE_ID
                    WHERE
                        t2.NAME LIKE '%{table_name}%'
                        AND ISNULL(t1.LABEL_ZT, '') <> ''
                    ORDER BY
                        t2.NAME,
                        t1.NAME
                ";

                var result = connection.Query<T>(statement);

                if (result.Any())
                {
                    return result.ToList();
                }
                else
                {
                    return new List<T>();
                }
            }
        }

        public static List<T> GetAllProperty<T>(string connectionString)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                string statement = @"
                    SELECT 
                        t2.ID                   AS 'Table_Id',
                        t2.NAME                 AS 'Table_Name',
                        t2.LABEL_ZT             AS 'Table_Label',
                        t1.ID                   AS 'Field_Id',
                        t1.NAME                 AS 'Field_Name',
                        t1.LABEL_ZT             AS 'Field_Label',
                        t1.DATA_TYPE            AS 'Data_Type',
                        t1.DATA_SOURCE          AS 'Data_Source',
                        t1.FOREIGN_PROPERTY     As 'Foreign_Propery'
                    FROM
                        [innovator].[PROPERTY] t1
                        INNER JOIN [innovator].[ITEMTYPE] t2 ON t2.id = t1.SOURCE_ID
                    ORDER BY
                        t2.NAME,
                        t1.NAME
                ";

                var result = connection.Query<T>(statement);

                if (result.Any())
                {
                    return result.ToList();
                }
                else
                {
                    return new List<T>();
                }
            }
        }

        public static List<T> GetUnOkProperty<T>(string connectionString)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                string statement = @"
                    SELECT 
                        t2.ID                   AS 'Table_Id',
                        t2.NAME                 AS 'Table_Name',
                        t2.LABEL_ZT             AS 'Table_Label',
                        t1.ID                   AS 'Field_Id',
                        t1.NAME                 AS 'Field_Name',
                        t1.LABEL_ZT             AS 'Field_Label',
                        t1.DATA_TYPE            AS 'Data_Type',
                        t1.DATA_SOURCE          AS 'Data_Source',
                        t1.FOREIGN_PROPERTY     As 'Foreign_Propery'
                    FROM
                        [innovator].[PROPERTY] t1
                        INNER JOIN [innovator].[ITEMTYPE] t2 ON t2.id = t1.SOURCE_ID
                    WHERE 
                        t1.DATA_SOURCE = N'00000000000000000000000000000000'
                    ORDER BY
                        t2.NAME,
                        t1.NAME
                ";

                var result = connection.Query<T>(statement);

                if (result.Any())
                {
                    return result.ToList();
                }
                else
                {
                    return new List<T>();
                }
            }
        }

        public static List<T> MethodList<T>(string keyword, string connectionString)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                string statement = $@"
                    SELECT 
                        * 
                    FROM 
                        [innovator].[METHOD] 
                    WHERE 
                        IS_CURRENT = '1' 
                        AND [NAME] LIKE N'{keyword}%'
                ";

                var result = connection.Query<T>(statement);

                if (result.Any())
                {
                    return result.ToList();
                }
                else
                {
                    return new List<T>();
                }
            }
        }

        public static List<T> MethodID<T>(string id, string keyword, string connectionString)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                string statement = $@"
                    SELECT 
                        * 
                    FROM 
                        [innovator].[METHOD] 
                    WHERE 
                        [ID] = '{id}'
                        AND [NAME] LIKE N'{keyword}%'
                ";

                var result = connection.Query<T>(statement);

                if (result.Any())
                {
                    return result.ToList();
                }
                else
                {
                    return new List<T>();
                }
            }
        }

        public static List<T> MethodToday<T>(string keyword, string connectionString)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                string statement = $@"
                    SELECT 
                        * 
                    FROM 
                        [innovator].[METHOD] 
                    WHERE 
                        IS_CURRENT = '1' 
                        AND MODIFIED_ON >= '{DateTime.Now.AddHours(-8).ToString("yyyy/MM/dd")}'
                ";

                var result = connection.Query<T>(statement);

                if (result.Any())
                {
                    return result.ToList();
                }
                else
                {
                    return new List<T>();
                }
            }
        }

        public static List<T> MethodListBySql<T>(string statement, string connectionString)
        {
            using (var connection = new SqlConnection(connectionString))
            {
               
                var result = connection.Query<T>(statement);

                if (result.Any())
                {
                    return result.ToList();
                }
                else
                {
                    return new List<T>();
                }
            }
        }

        public static List<T> METHOD_LIKE<T>(string keyword, string connectionString)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                string statement = $@"
                    SELECT 
                        * 
                    FROM 
                        [innovator].[METHOD] WITH(NOLOCK)
                    WHERE 
                        IS_CURRENT = '1' 
                        AND [METHOD_CODE] LIKE N'%{keyword}%'
                    ORDER BY
                        [name]
                ";

                var result = connection.Query<T>(statement);

                if (result.Any())
                {
                    return result.ToList();
                }
                else
                {
                    return new List<T>();
                }
            }
        }
        
        public static List<T> GetLockItemList<T>(string connectionString)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                string statement = $@"
SELECT * FROM
(
    SELECT '[ITEMTYPE]'         AS 'C1', [NAME]         AS 'C2', [INSTANCE_DATA]    AS 'C3' FROM [ITEMTYPE]         WHERE ISNULL([LOCKED_BY_ID], '') <> '' AND [IS_CURRENT] = '1'
    UNION ALL
    SELECT '[METHOD]'           AS 'C1', [NAME]         AS 'C2', [COMMENTS]         AS 'C3' FROM [METHOD]           WHERE ISNULL([LOCKED_BY_ID], '') <> '' AND [IS_CURRENT] = '1'
    UNION ALL
    SELECT '[IDENTITY]'         AS 'C1', [NAME]         AS 'C2', ''                 AS 'C3' FROM [IDENTITY]         WHERE ISNULL([LOCKED_BY_ID], '') <> '' AND [IS_CURRENT] = '1'
    UNION ALL
    SELECT '[USER]'             AS 'C1', [LOGIN_NAME]   AS 'C2', ''                 AS 'C3' FROM [USER]             WHERE ISNULL([LOCKED_BY_ID], '') <> '' AND [IS_CURRENT] = '1'
    UNION ALL
    SELECT '[WORKFLOW_MAP]'     AS 'C1', [NAME]         AS 'C2', [DESCRIPTION_ZT]   AS 'C3' FROM [WORKFLOW_MAP]     WHERE ISNULL([LOCKED_BY_ID], '') <> '' AND [IS_CURRENT] = '1'
    UNION ALL
    SELECT '[LIFE_CYCLE_MAP]'   AS 'C1', [NAME]         AS 'C2', [DESCRIPTION]      AS 'C3' FROM [LIFE_CYCLE_MAP]   WHERE ISNULL([LOCKED_BY_ID], '') <> '' AND [IS_CURRENT] = '1'
    UNION ALL
    SELECT '[FORM]'             AS 'C1', [NAME]         AS 'C2', [DESCRIPTION]      AS 'C3' FROM [FORM]             WHERE ISNULL([LOCKED_BY_ID], '') <> '' AND [IS_CURRENT] = '1'
    UNION ALL
    SELECT '[ACTION]'           AS 'C1', [NAME]         AS 'C2', [LABEL_ZT]         AS 'C3' FROM [ACTION]           WHERE ISNULL([LOCKED_BY_ID], '') <> '' AND [IS_CURRENT] = '1'
    UNION ALL
    SELECT '[PERMISSION]'       AS 'C1', [KEYED_NAME]   AS 'C2', ''                 AS 'C3' FROM [PERMISSION]       WHERE ISNULL([LOCKED_BY_ID], '') <> '' AND [IS_CURRENT] = '1'
    UNION ALL
    SELECT '[REPORT]'           AS 'C1', [KEYED_NAME]   AS 'C2', ''                 AS 'C3' FROM [REPORT]           WHERE ISNULL([LOCKED_BY_ID], '') <> '' AND [IS_CURRENT] = '1'
    UNION ALL
    SELECT '[TABLE]'            AS 'C1', 'In_Survey'                       AS 'C2', [KEYED_NAME]       AS 'C3' FROM [IN_SURVEY]                        WHERE ISNULL([LOCKED_BY_ID], '') <> ''
    UNION ALL
    SELECT '[TABLE]'            AS 'C1', 'In_Cla_Survey'                   AS 'C2', [KEYED_NAME]       AS 'C3' FROM [IN_CLA_SURVEY]                    WHERE ISNULL([LOCKED_BY_ID], '') <> ''
    UNION ALL
    SELECT '[TABLE]'            AS 'C1', 'In_Meeting'                      AS 'C2', [KEYED_NAME]       AS 'C3' FROM [IN_MEETING]                       WHERE ISNULL([LOCKED_BY_ID], '') <> ''
    UNION ALL
    SELECT '[TABLE]'            AS 'C1', 'In_Meeting_User'                 AS 'C2', [KEYED_NAME]       AS 'C3' FROM [IN_MEETING_USER]                  WHERE ISNULL([LOCKED_BY_ID], '') <> ''
    UNION ALL
    SELECT '[TABLE]'            AS 'C1', 'In_Meeting_Resume'               AS 'C2', [KEYED_NAME]       AS 'C3' FROM [IN_MEETING_RESUME]                WHERE ISNULL([LOCKED_BY_ID], '') <> ''
    UNION ALL
    SELECT '[TABLE]'            AS 'C1', 'In_Meeting_Surveys'              AS 'C2', [KEYED_NAME]       AS 'C3' FROM [IN_MEETING_SURVEYS]               WHERE ISNULL([LOCKED_BY_ID], '') <> ''
    UNION ALL
    SELECT '[TABLE]'            AS 'C1', 'In_Cla_Meeting'                  AS 'C2', [KEYED_NAME]       AS 'C3' FROM [IN_CLA_MEETING]                   WHERE ISNULL([LOCKED_BY_ID], '') <> ''
    UNION ALL
    SELECT '[TABLE]'            AS 'C1', 'In_Cla_Meeting_User'             AS 'C2', [KEYED_NAME]       AS 'C3' FROM [IN_CLA_MEETING_USER]              WHERE ISNULL([LOCKED_BY_ID], '') <> ''
    UNION ALL
    SELECT '[TABLE]'            AS 'C1', 'In_Cla_Meeting_Resume'           AS 'C2', [KEYED_NAME]       AS 'C3' FROM [IN_CLA_MEETING_RESUME]            WHERE ISNULL([LOCKED_BY_ID], '') <> ''
    UNION ALL
    SELECT '[TABLE]'            AS 'C1', 'In_Cla_Meeting_Surveys'          AS 'C2', [KEYED_NAME]       AS 'C3' FROM [IN_CLA_MEETING_SURVEYS]           WHERE ISNULL([LOCKED_BY_ID], '') <> ''
    UNION ALL
    SELECT '[TABLE]'            AS 'C1', 'In_Cla_Meeting_Resume_Credit'    AS 'C2', [KEYED_NAME]       AS 'C3' FROM [IN_CLA_MEETING_RESUME_CREDIT]     WHERE ISNULL([LOCKED_BY_ID], '') <> ''
) t1
ORDER BY
    t1.C1,
    t1.C2,
    t1.C3
                ";

                var result = connection.Query<T>(statement);

                if (result.Any())
                {
                    return result.ToList();
                }
                else
                {
                    return new List<T>();
                }
            }
        }

        public static List<T> GetPropertyList<T>(string itemType, string connectionString)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                string statement = $@"
                    SELECT
                        t1.*
                    FROM
                        [PROPERTY] t1
                        INNER JOIN [ITEMTYPE] t2 ON t2.ID = t1.SOURCE_ID
                    WHERE
                        t1.IS_CURRENT = '1'
                        AND t2.IS_CURRENT = '1'
                        AND t2.NAME = N'{itemType}'
                    ORDER BY
                        t1.NAME
                ";

                var result = connection.Query<T>(statement);

                if (result.Any())
                {
                    return result.ToList();
                }
                else
                {
                    return new List<T>();
                }
            }
        }
    }
}