﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using WorkHelp.Wpf.Models.Aras;
using WorkHelp.Wpf.Models.Game;

namespace WorkHelp.Wpf.Repository
{
     public class Repository412
     {
        public static List<ItemType> GetAllMethodTypeList(string keyedName, string connectionString)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                string statement = $@"
SELECT 
    [ID], 
    [NAME], 
    [KEYED_NAME], 
    [COMMENTS],
    [METHOD_CODE], 
    [METHOD_TYPE]
FROM 
    [METHOD] WITH(NOLOCK)
WHERE 
    [IS_CURRENT] = '1'
    AND [Name] LIKE N'{keyedName}%'
ORDER BY 
    [METHOD_TYPE], [NAME]
                ";

                var result = connection.Query<ItemType>(statement);
                
                if (result.Any())
                {
                    return result.ToList();
                }
                else
                {
                    return new List<ItemType>();
                }
            }
        }

        public static string Scalar(string statement, string connectionString)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                var result = connection.ExecuteScalarAsync<string>(statement);
                return result.Result;
            }
        }

        public static List<TMeeting> GetMeetingList(string connectionString)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                string statement = $@"
SELECT
    id
    , in_title
    , DATEADD(hour, 8, in_date_s)           AS 'in_date_s'            -- '賽事開始'
    , DATEADD(hour, 8, in_date_e)           AS 'in_date_e'            -- '賽事結束'
    , DATEADD(hour, 8, in_state_time_start) AS 'in_state_time_start'  -- '報名開始日'
    , DATEADD(hour, 8, in_state_time_end)   AS 'in_state_time_end'    -- '報名截止日'
    , in_url                                                          -- '專屬展示網址'
    , in_register_url                                                 -- '專屬報名網址'
    , in_taking                                                       -- '正取人數'
    , in_real_taking                                                  -- '實際正取'
FROM
    IN_MEETING WITH(NOLOCK)
WHERE
    id NOT IN ('5F73936711E04DC799CB02587F4FF7E0', '249FDB244E534EB0AA66C8E9C470E930')
    AND ISNULL(in_is_template, '') <> '1'
    AND DATEADD(hour, 8, in_state_time_end) >= GETDATE()
ORDER BY
    in_state_time_end
                ";

                var result = connection.Query<TMeeting>(statement);

                if (result.Any())
                {
                    return result.ToList();
                }
                else
                {
                    return new List<TMeeting>();
                }
            }
        }


    }
}
