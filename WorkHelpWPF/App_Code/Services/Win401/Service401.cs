﻿using System.Collections.Generic;
using WorkHelp.Wpf.Models;
using WorkHelp.Wpf.Repository;

namespace WorkHelp.Wpf.Services
{
    public partial class Win401Service
    {
        #region Repository

        public static List<DataModel> GetDBList(string connectionString)
        {
            return Repository401.GetDBList(connectionString);
        }

        public static List<DataModel> GetTableList(string connectionString)
        {
            return Repository401.GetTableList(connectionString);
        }

        public static List<DataModel> GetViewList(string connectionString)
        {
            return Repository401.GetViewList(connectionString);
        }

        public static string GetTableDescription(string connectionString, string tableName)
        {
            return Repository401.GetTableDescription(connectionString, tableName);
        }

        public static List<ColumnModel> GetColumnList(string connectionString, string tableName, bool isSorted = false)
        {
            return Repository401.GetColumnList(connectionString, tableName, isSorted);
        }

        public static List<KeyModel> GetKeyList(string connectionString, string tableName)
        {
            return Repository401.GetKeyList(connectionString, tableName);
        }

        public static List<ColumnModel> GetDescriptionList(string connectionString, string tableName)
        {
            return Repository401.GetDescriptionList(connectionString, tableName);
        }

        #endregion Repository

        public static SchemaModel CreateSchemaModel(Win401Model wModel)
        {
            SchemaModel result = NewSchemaModel(wModel);
            SetSchemaModel(result);
            SetPieces(result);

            if (wModel.IsEasyName)
            {
                result.SubMain = string.Empty;
            }

            return result;
        }

        private static SchemaModel NewSchemaModel(Win401Model wModel)
        {
            return new SchemaModel
            {
                ConnectionString = wModel.ConnectionString,
                DatabaseName = wModel.DatabaseName,
                TableName = wModel.TableName,
                FolderName = wModel.FolderName,
                MainName = wModel.MainName,
                SubMain = wModel.MainName,
                ControllerName = wModel.ControllerName,
                NameSpaceName = wModel.NameSpaceName,
                MainRouteName = wModel.MainRouteName,
                SubRouteName = wModel.SubRouteName,
                IsArasMode = wModel.IsArasMode
            };
        }

        private static void SetSchemaModel(SchemaModel model)
        {
            model.TableDescription = GetTableDescription(model.ConnectionString, model.TableName);
            if (string.IsNullOrWhiteSpace(model.TableDescription))
            {
                model.TableDescription = "類別說明";
            }

            model.ColumnList = GetColumnList(model.ConnectionString, model.TableName, isSorted: model.IsArasMode);
            model.KeyEasyList = GetKeyList(model.ConnectionString, model.TableName);
            model.DescriptionList = GetDescriptionList(model.ConnectionString, model.TableName);

            ColumnDataTypeMapper(model);
            ColumnKeyMapper(model.ColumnList, model.KeyEasyList);
            ColumnDescriptionMapper(model.ColumnList, model.DescriptionList);

            CSharpMapper(model);

            // 設定 Key ColumnList
            model.KeyList = model.ColumnList.FindAll(x => x.IS_KEY);
        }

        private static void SetPieces(SchemaModel model)
        {
            model.KeyParamSummary = Win401Utility.KeyParamSummary(model.KeyList);
            model.KeyParamDataType = Win401Utility.KeyParamDataType(model.KeyList);
            model.KeyParamValidate = Win401Utility.KeyParamValidate(model.KeyList);
            model.KeyParamDynamic = Win401Utility.KeyParamDynamic(model.KeyList);
            model.KeyParamValue = Win401Utility.KeyParamValue(model.KeyList);
            model.KeyParamName = Win401Utility.KeyParamName(model.KeyList);

            model.KeyEntitySingleOrDefault = Win401Utility.KeyEntitySingleOrDefault(model.KeyList);
            model.KeyEntityValue = Win401Utility.KeyEntityValue(model.KeyList);
            model.KeyEntityWhere = Win401Utility.KeyEntityWhere(model.KeyList);
            model.KeyEntityLines_GoogleProto = Win401Utility.ColumEntityLines_GoogleProto(model.KeyList);
            model.KeyEntityEquals = Win401Utility.KeyEntityEquals(model.KeyList);

            model.KeyParamTestCase = Win401Utility.KeyParamTestCase(model.KeyList);
            model.KeyRoute = Win401Utility.KeyRoute(model.SubRouteName, model.KeyList);
            model.KeyRouteItem = Win401Utility.KeyRouteItem(model.SubRouteName, model.KeyList);

            var tupleApiRoute = Win401Utility.KeyRouteApi(model.MainRouteName, model.SubRouteName, model.KeyList);
            model.KeyApiRouteList = tupleApiRoute.Item1;
            model.KeyApiRouteItem = tupleApiRoute.Item2;
            model.KeyApiRouteSave = tupleApiRoute.Item3;
            model.KeyApiRouteRemove = tupleApiRoute.Item4;

            model.ColumEntityValidate = Win401Utility.ColumEntityValidate(model.ColumnList);
            model.ColumEntityCopy = Win401Utility.ColumEntityCopy(model.ColumnList);
            model.ColumEntityLines_GoogleProto = Win401Utility.ColumEntityLines_GoogleProto(model.ColumnList);
            model.ColumEntityLines_GoogleProto_Update = Win401Utility.ColumEntityLines_GoogleProto_Update(model.ColumnList);
        }

        public static EntityModel NewEntityModel(Win401Model wModel)
        {
            return new EntityModel();
        }

        public static ProtobufModel NewProtobufModelModel(Win401Model wModel)
        {
            return new ProtobufModel();
        }

        public static void ColumnDataTypeMapper(SchemaModel model)
        {
            model.ColumnList.ForEach(x =>
            {
                AnalysisColumn(x);
            });
        }

        public static void AnalysisColumn(ColumnModel x)
        {
            if (x.IS_NULLABLE.Equals("YES"))
            {
                x.IS_NULLABLE_BOOL = true;
            }

            if (x.COLUMN_NAME.Equals("TS"))
            {
                x.IS_OUTPUT = false;
            }

            ColumnDataTypeMapper(x);
            x.COLUMN_NAME_ENTITY = GetEntityColumnName(x);
            x.COLUMN_NAME_PROTO = GetProtoColumnName(x);
            x.COLUMN_NAME_MODEL = GetModelColumnName(x);
            x.COLUMN_NAME_PARAM = GetParamColumnName(x);
            x.COLUMN_NAME_TYPESCRIPT = GetTypeScriptColumnName(x);

            x.DATA_TYPE_UPPER = x.DATA_TYPE.ToUpper();
        }

        private static void ColumnDataTypeMapper(ColumnModel x)
        {
            switch (x.DATA_TYPE.ToLower())
            {
                case "bit":
                    x.DATA_TYPE_SQL = Enums.SqlDataTypeEnum.BIT;
                    if (x.IS_NULLABLE_BOOL)
                    {
                        x.DATA_TYPE_CSHARP = Enums.CSharpDataTypeEnum.BOOLEAN_NULL;
                        x.DATA_TYPE_PROTOBUF = Enums.GoogleProtoDataTypeEnum.BOOL_NULL;
                    }
                    else
                    {
                        x.DATA_TYPE_CSHARP = Enums.CSharpDataTypeEnum.BOOLEAN;
                        x.DATA_TYPE_PROTOBUF = Enums.GoogleProtoDataTypeEnum.BOOL;
                    }
                    break;

                case "char":
                    x.DATA_TYPE_SQL = Enums.SqlDataTypeEnum.CHAR;
                    x.DATA_TYPE_CSHARP = Enums.CSharpDataTypeEnum.STRING;
                    if (x.IS_NULLABLE_BOOL)
                    {
                        x.DATA_TYPE_PROTOBUF = Enums.GoogleProtoDataTypeEnum.STRING_NULL;
                    }
                    else
                    {
                        x.DATA_TYPE_PROTOBUF = Enums.GoogleProtoDataTypeEnum.STRING;
                    }
                    break;

                case "nchar":
                    x.DATA_TYPE_SQL = Enums.SqlDataTypeEnum.NCHAR;
                    x.DATA_TYPE_CSHARP = Enums.CSharpDataTypeEnum.STRING;
                    if (x.IS_NULLABLE_BOOL)
                    {
                        x.DATA_TYPE_PROTOBUF = Enums.GoogleProtoDataTypeEnum.STRING_NULL;
                    }
                    else
                    {
                        x.DATA_TYPE_PROTOBUF = Enums.GoogleProtoDataTypeEnum.STRING;
                    }
                    break;

                case "varchar":
                    x.DATA_TYPE_SQL = Enums.SqlDataTypeEnum.VARCHAR;
                    x.DATA_TYPE_CSHARP = Enums.CSharpDataTypeEnum.STRING;
                    if (x.IS_NULLABLE_BOOL)
                    {
                        x.DATA_TYPE_PROTOBUF = Enums.GoogleProtoDataTypeEnum.STRING_NULL;
                    }
                    else
                    {
                        x.DATA_TYPE_PROTOBUF = Enums.GoogleProtoDataTypeEnum.STRING;
                    }
                    break;

                case "nvarchar":
                    x.DATA_TYPE_SQL = Enums.SqlDataTypeEnum.NVARCHAR;
                    x.DATA_TYPE_CSHARP = Enums.CSharpDataTypeEnum.STRING;
                    if (x.IS_NULLABLE_BOOL)
                    {
                        x.DATA_TYPE_PROTOBUF = Enums.GoogleProtoDataTypeEnum.STRING_NULL;
                    }
                    else
                    {
                        x.DATA_TYPE_PROTOBUF = Enums.GoogleProtoDataTypeEnum.STRING;
                    }
                    break;

                case "text":
                    x.DATA_TYPE_SQL = Enums.SqlDataTypeEnum.TEXT;
                    x.DATA_TYPE_CSHARP = Enums.CSharpDataTypeEnum.STRING;
                    if (x.IS_NULLABLE_BOOL)
                    {
                        x.DATA_TYPE_PROTOBUF = Enums.GoogleProtoDataTypeEnum.STRING_NULL;
                    }
                    else
                    {
                        x.DATA_TYPE_PROTOBUF = Enums.GoogleProtoDataTypeEnum.STRING;
                    }
                    break;

                case "xml":
                    x.DATA_TYPE_SQL = Enums.SqlDataTypeEnum.XML;
                    x.DATA_TYPE_CSHARP = Enums.CSharpDataTypeEnum.STRING;
                    if (x.IS_NULLABLE_BOOL)
                    {
                        x.DATA_TYPE_PROTOBUF = Enums.GoogleProtoDataTypeEnum.STRING_NULL;
                    }
                    else
                    {
                        x.DATA_TYPE_PROTOBUF = Enums.GoogleProtoDataTypeEnum.STRING;
                    }
                    break;

                case "datetime?":
                case "datetime":
                    x.DATA_TYPE_SQL = Enums.SqlDataTypeEnum.DATETIME;
                    if (x.IS_NULLABLE_BOOL)
                    {
                        x.DATA_TYPE_CSHARP = Enums.CSharpDataTypeEnum.DATETIME_NULL;
                    }
                    else
                    {
                        x.DATA_TYPE_CSHARP = Enums.CSharpDataTypeEnum.DATETIME;
                    }
                    x.DATA_TYPE_PROTOBUF = Enums.GoogleProtoDataTypeEnum.TIMESTAMP;
                    break;

                case "datetime2":
                    x.DATA_TYPE_SQL = Enums.SqlDataTypeEnum.DATETIME2;
                    if (x.IS_NULLABLE_BOOL)
                    {
                        x.DATA_TYPE_CSHARP = Enums.CSharpDataTypeEnum.DATETIME_NULL;
                    }
                    else
                    {
                        x.DATA_TYPE_CSHARP = Enums.CSharpDataTypeEnum.DATETIME;
                    }
                    x.DATA_TYPE_PROTOBUF = Enums.GoogleProtoDataTypeEnum.TIMESTAMP;
                    break;

                case "date":
                    x.DATA_TYPE_SQL = Enums.SqlDataTypeEnum.DATE;
                    if (x.IS_NULLABLE_BOOL)
                    {
                        x.DATA_TYPE_CSHARP = Enums.CSharpDataTypeEnum.DATETIME_NULL;
                    }
                    else
                    {
                        x.DATA_TYPE_CSHARP = Enums.CSharpDataTypeEnum.DATETIME;
                    }
                    x.DATA_TYPE_PROTOBUF = Enums.GoogleProtoDataTypeEnum.TIMESTAMP;
                    break;

                case "smalldatetime":
                    x.DATA_TYPE_SQL = Enums.SqlDataTypeEnum.SMALLDATETIME;
                    if (x.IS_NULLABLE_BOOL)
                    {
                        x.DATA_TYPE_CSHARP = Enums.CSharpDataTypeEnum.DATETIME_NULL;
                    }
                    else
                    {
                        x.DATA_TYPE_CSHARP = Enums.CSharpDataTypeEnum.DATETIME;
                    }
                    x.DATA_TYPE_PROTOBUF = Enums.GoogleProtoDataTypeEnum.TIMESTAMP;
                    break;

                case "datetimeoffset":
                    x.DATA_TYPE_SQL = Enums.SqlDataTypeEnum.DATETIMEOFFSET;
                    if (x.IS_NULLABLE_BOOL)
                    {
                        x.DATA_TYPE_CSHARP = Enums.CSharpDataTypeEnum.DATETIMEOFFSET_NULL;
                    }
                    else
                    {
                        x.DATA_TYPE_CSHARP = Enums.CSharpDataTypeEnum.DATETIMEOFFSET;
                    }
                    x.DATA_TYPE_PROTOBUF = Enums.GoogleProtoDataTypeEnum.TIMESTAMP;
                    break;

                case "int16":
                case "short":
                case "smallint":
                    x.DATA_TYPE_SQL = Enums.SqlDataTypeEnum.SMALLINT;
                    if (x.IS_NULLABLE_BOOL)
                    {
                        x.DATA_TYPE_CSHARP = Enums.CSharpDataTypeEnum.INT16_NULL;
                    }
                    else
                    {
                        x.DATA_TYPE_CSHARP = Enums.CSharpDataTypeEnum.INT16;
                    }
                    x.DATA_TYPE_PROTOBUF = Enums.GoogleProtoDataTypeEnum.INT16;
                    break;

                case "int":
                case "int?":
                case "int32":
                    x.DATA_TYPE_SQL = Enums.SqlDataTypeEnum.INT;
                    if (x.IS_NULLABLE_BOOL)
                    {
                        x.DATA_TYPE_CSHARP = Enums.CSharpDataTypeEnum.INT32_NULL;
                        x.DATA_TYPE_PROTOBUF = Enums.GoogleProtoDataTypeEnum.INT32_NULL;
                    }
                    else
                    {
                        x.DATA_TYPE_CSHARP = Enums.CSharpDataTypeEnum.INT32;
                        x.DATA_TYPE_PROTOBUF = Enums.GoogleProtoDataTypeEnum.INT32;
                    }
                    break;

                case "long":
                case "int64":
                case "bigint":
                    x.DATA_TYPE_SQL = Enums.SqlDataTypeEnum.BIGINT;
                    if (x.IS_NULLABLE_BOOL)
                    {
                        x.DATA_TYPE_CSHARP = Enums.CSharpDataTypeEnum.INT64_NULL;
                    }
                    else
                    {
                        x.DATA_TYPE_CSHARP = Enums.CSharpDataTypeEnum.INT64;
                    }
                    x.DATA_TYPE_PROTOBUF = Enums.GoogleProtoDataTypeEnum.INT64;
                    break;

                case "real":
                    x.DATA_TYPE_SQL = Enums.SqlDataTypeEnum.REAL;
                    if (x.IS_NULLABLE_BOOL)
                    {
                        x.DATA_TYPE_CSHARP = Enums.CSharpDataTypeEnum.DOUBLE_NULL;
                    }
                    else
                    {
                        x.DATA_TYPE_CSHARP = Enums.CSharpDataTypeEnum.DOUBLE;
                    }
                    x.DATA_TYPE_PROTOBUF = Enums.GoogleProtoDataTypeEnum.DOUBLE_NULL;
                    break;

                case "float":
                    x.DATA_TYPE_SQL = Enums.SqlDataTypeEnum.FLOAT;
                    if (x.IS_NULLABLE_BOOL)
                    {
                        x.DATA_TYPE_CSHARP = Enums.CSharpDataTypeEnum.DOUBLE_NULL;
                    }
                    else
                    {
                        x.DATA_TYPE_CSHARP = Enums.CSharpDataTypeEnum.DOUBLE;
                    }
                    x.DATA_TYPE_PROTOBUF = Enums.GoogleProtoDataTypeEnum.DOUBLE_NULL;
                    break;

                case "double":
                case "double?":
                case "numeric":
                    x.DATA_TYPE_SQL = Enums.SqlDataTypeEnum.NUMERIC;
                    if (x.IS_NULLABLE_BOOL)
                    {
                        x.DATA_TYPE_CSHARP = Enums.CSharpDataTypeEnum.DOUBLE_NULL;
                    }
                    else
                    {
                        x.DATA_TYPE_CSHARP = Enums.CSharpDataTypeEnum.DOUBLE;
                    }
                    x.DATA_TYPE_PROTOBUF = Enums.GoogleProtoDataTypeEnum.DOUBLE_NULL;
                    break;

                case "decimal":
                case "decimal?":
                    x.DATA_TYPE_SQL = Enums.SqlDataTypeEnum.DECIMAL;
                    if (x.IS_NULLABLE_BOOL)
                    {
                        x.DATA_TYPE_CSHARP = Enums.CSharpDataTypeEnum.DECIMAL_NULL;
                    }
                    else
                    {
                        x.DATA_TYPE_CSHARP = Enums.CSharpDataTypeEnum.DECIMAL;
                    }
                    x.DATA_TYPE_PROTOBUF = Enums.GoogleProtoDataTypeEnum.DOUBLE_NULL;
                    break;

                case "money":
                    x.DATA_TYPE_SQL = Enums.SqlDataTypeEnum.MONEY;
                    if (x.IS_NULLABLE_BOOL)
                    {
                        x.DATA_TYPE_CSHARP = Enums.CSharpDataTypeEnum.DECIMAL_NULL;
                    }
                    else
                    {
                        x.DATA_TYPE_CSHARP = Enums.CSharpDataTypeEnum.DECIMAL;
                    }
                    x.DATA_TYPE_PROTOBUF = Enums.GoogleProtoDataTypeEnum.DOUBLE_NULL;
                    break;

                case "smallmoney":
                    x.DATA_TYPE_SQL = Enums.SqlDataTypeEnum.SMALLMONEY;
                    if (x.IS_NULLABLE_BOOL)
                    {
                        x.DATA_TYPE_CSHARP = Enums.CSharpDataTypeEnum.DECIMAL_NULL;
                    }
                    else
                    {
                        x.DATA_TYPE_CSHARP = Enums.CSharpDataTypeEnum.DECIMAL;
                    }
                    x.DATA_TYPE_PROTOBUF = Enums.GoogleProtoDataTypeEnum.DOUBLE_NULL;
                    break;

                case "tinyint":
                    x.DATA_TYPE_SQL = Enums.SqlDataTypeEnum.TINYINT;
                    if (x.IS_NULLABLE_BOOL)
                    {
                        x.DATA_TYPE_CSHARP = Enums.CSharpDataTypeEnum.BYTE_NULL;
                    }
                    else
                    {
                        x.DATA_TYPE_CSHARP = Enums.CSharpDataTypeEnum.BYTE;
                    }
                    x.DATA_TYPE_PROTOBUF = Enums.GoogleProtoDataTypeEnum.NONE;
                    break;

                case "timestamp":
                    x.DATA_TYPE_SQL = Enums.SqlDataTypeEnum.TIMESTAMP;
                    x.DATA_TYPE_CSHARP = Enums.CSharpDataTypeEnum.TIMESTAMP;
                    x.DATA_TYPE_PROTOBUF = Enums.GoogleProtoDataTypeEnum.TIMESTAMP;
                    break;

                case "binary":
                    x.DATA_TYPE_SQL = Enums.SqlDataTypeEnum.BINARY;
                    x.DATA_TYPE_CSHARP = Enums.CSharpDataTypeEnum.BYTEARRAY;
                    x.DATA_TYPE_PROTOBUF = Enums.GoogleProtoDataTypeEnum.NONE;
                    break;

                case "varbinary":
                    x.DATA_TYPE_SQL = Enums.SqlDataTypeEnum.VARBINARY;
                    x.DATA_TYPE_CSHARP = Enums.CSharpDataTypeEnum.BYTEARRAY;
                    x.DATA_TYPE_PROTOBUF = Enums.GoogleProtoDataTypeEnum.NONE;
                    break;

                case "nvarbinary":
                    x.DATA_TYPE_SQL = Enums.SqlDataTypeEnum.NVARBINARY;
                    x.DATA_TYPE_CSHARP = Enums.CSharpDataTypeEnum.BYTEARRAY;
                    x.DATA_TYPE_PROTOBUF = Enums.GoogleProtoDataTypeEnum.NONE;
                    break;

                case "image":
                    x.DATA_TYPE_SQL = Enums.SqlDataTypeEnum.IMAGE;
                    x.DATA_TYPE_CSHARP = Enums.CSharpDataTypeEnum.BYTEARRAY;
                    x.DATA_TYPE_PROTOBUF = Enums.GoogleProtoDataTypeEnum.NONE;
                    break;

                case "sql_variant":
                    x.DATA_TYPE_SQL = Enums.SqlDataTypeEnum.SQL_VARIANT;
                    x.DATA_TYPE_CSHARP = Enums.CSharpDataTypeEnum.BYTEARRAY;
                    x.DATA_TYPE_PROTOBUF = Enums.GoogleProtoDataTypeEnum.NONE;
                    break;

                case "geography":
                    x.DATA_TYPE_SQL = Enums.SqlDataTypeEnum.GEOGRAPHY;
                    x.DATA_TYPE_CSHARP = Enums.CSharpDataTypeEnum.BYTEARRAY;
                    x.DATA_TYPE_PROTOBUF = Enums.GoogleProtoDataTypeEnum.NONE;
                    break;

                case "geometry":
                    x.DATA_TYPE_SQL = Enums.SqlDataTypeEnum.GEOMETRY;
                    x.DATA_TYPE_CSHARP = Enums.CSharpDataTypeEnum.BYTEARRAY;
                    x.DATA_TYPE_PROTOBUF = Enums.GoogleProtoDataTypeEnum.NONE;
                    break;

                case "hierarchyid":
                    x.DATA_TYPE_SQL = Enums.SqlDataTypeEnum.HIERARCHYID;
                    x.DATA_TYPE_CSHARP = Enums.CSharpDataTypeEnum.BYTEARRAY;
                    x.DATA_TYPE_PROTOBUF = Enums.GoogleProtoDataTypeEnum.NONE;
                    break;

                case "guid":
                case "uniqueidentifier":
                    x.DATA_TYPE_SQL = Enums.SqlDataTypeEnum.UNIQUEIDENTIFIER;
                    x.DATA_TYPE_CSHARP = Enums.CSharpDataTypeEnum.GUID;
                    x.DATA_TYPE_PROTOBUF = Enums.GoogleProtoDataTypeEnum.NONE;
                    break;
            }
        }

        public static void CSharpMapper(SchemaModel sModel)
        {
            sModel.ColumnList.ForEach(x =>
            {
                CSharpMapperColumn(x);
            });
        }

        public static void CSharpMapperColumn(ColumnModel x)
        {
            switch (x.DATA_TYPE_CSHARP)
            {
                case Enums.CSharpDataTypeEnum.BOOLEAN:
                    x.DATA_TYPE_CSHARP_STRING = "bool";
                    x.DATA_TYPE_CSHARP_STRING_NULL = "bool?";
                    x.DATA_TYPE_TYPESCRIPT_STRING = "boolean";
                    break;

                case Enums.CSharpDataTypeEnum.BOOLEAN_NULL:
                    x.DATA_TYPE_CSHARP_STRING = "bool?";
                    x.DATA_TYPE_CSHARP_STRING_NULL = "bool?";
                    x.DATA_TYPE_TYPESCRIPT_STRING = "boolean";
                    break;

                case Enums.CSharpDataTypeEnum.DATETIME:
                    x.DATA_TYPE_CSHARP_STRING = "DateTime";
                    x.DATA_TYPE_CSHARP_STRING_NULL = "DateTime?";
                    x.DATA_TYPE_TYPESCRIPT_STRING = "string | Moment";
                    break;

                case Enums.CSharpDataTypeEnum.DATETIME_NULL:
                    x.DATA_TYPE_CSHARP_STRING = "DateTime?";
                    x.DATA_TYPE_CSHARP_STRING_NULL = "DateTime?";
                    x.DATA_TYPE_TYPESCRIPT_STRING = "string | Moment";
                    break;

                case Enums.CSharpDataTypeEnum.DATETIMEOFFSET:
                    x.DATA_TYPE_CSHARP_STRING = "DateTimeOffset";
                    x.DATA_TYPE_CSHARP_STRING_NULL = "DateTimeOffset?";
                    x.DATA_TYPE_TYPESCRIPT_STRING = "string | Moment";
                    break;

                case Enums.CSharpDataTypeEnum.DATETIMEOFFSET_NULL:
                    x.DATA_TYPE_CSHARP_STRING = "DateTimeOffset?";
                    x.DATA_TYPE_CSHARP_STRING_NULL = "DateTimeOffset?";
                    x.DATA_TYPE_TYPESCRIPT_STRING = "string | Moment";
                    break;

                case Enums.CSharpDataTypeEnum.INT16:
                    x.DATA_TYPE_CSHARP_STRING = "short";
                    x.DATA_TYPE_CSHARP_STRING_NULL = "short?";
                    x.DATA_TYPE_TYPESCRIPT_STRING = "number";
                    break;

                case Enums.CSharpDataTypeEnum.INT16_NULL:
                    x.DATA_TYPE_CSHARP_STRING = "short?";
                    x.DATA_TYPE_CSHARP_STRING_NULL = "short?";
                    x.DATA_TYPE_TYPESCRIPT_STRING = "number";
                    break;

                case Enums.CSharpDataTypeEnum.INT32:
                    x.DATA_TYPE_CSHARP_STRING = "int";
                    x.DATA_TYPE_CSHARP_STRING_NULL = "int?";
                    x.DATA_TYPE_TYPESCRIPT_STRING = "number";
                    break;

                case Enums.CSharpDataTypeEnum.INT32_NULL:
                    x.DATA_TYPE_CSHARP_STRING = "int?";
                    x.DATA_TYPE_CSHARP_STRING_NULL = "int?";
                    x.DATA_TYPE_TYPESCRIPT_STRING = "number";
                    break;

                case Enums.CSharpDataTypeEnum.INT64:
                    x.DATA_TYPE_CSHARP_STRING = "long";
                    x.DATA_TYPE_CSHARP_STRING_NULL = "long?";
                    x.DATA_TYPE_TYPESCRIPT_STRING = "number";
                    break;

                case Enums.CSharpDataTypeEnum.INT64_NULL:
                    x.DATA_TYPE_CSHARP_STRING = "long?";
                    x.DATA_TYPE_CSHARP_STRING_NULL = "long?";
                    x.DATA_TYPE_TYPESCRIPT_STRING = "number";
                    break;

                case Enums.CSharpDataTypeEnum.STRING:
                    x.DATA_TYPE_CSHARP_STRING = "string";
                    x.DATA_TYPE_CSHARP_STRING_NULL = "string";
                    x.DATA_TYPE_TYPESCRIPT_STRING = "string";
                    break;

                case Enums.CSharpDataTypeEnum.DOUBLE:
                case Enums.CSharpDataTypeEnum.SINGLE:
                    x.DATA_TYPE_CSHARP_STRING = "double";
                    x.DATA_TYPE_CSHARP_STRING_NULL = "double?";
                    x.DATA_TYPE_TYPESCRIPT_STRING = "number";
                    break;

                case Enums.CSharpDataTypeEnum.DOUBLE_NULL:
                    x.DATA_TYPE_CSHARP_STRING = "double?";
                    x.DATA_TYPE_CSHARP_STRING_NULL = "double?";
                    x.DATA_TYPE_TYPESCRIPT_STRING = "number";
                    break;

                case Enums.CSharpDataTypeEnum.DECIMAL:
                    x.DATA_TYPE_CSHARP_STRING = "decimal";
                    x.DATA_TYPE_CSHARP_STRING_NULL = "decimal?";
                    x.DATA_TYPE_TYPESCRIPT_STRING = "number";
                    break;

                case Enums.CSharpDataTypeEnum.DECIMAL_NULL:
                    x.DATA_TYPE_CSHARP_STRING = "decimal?";
                    x.DATA_TYPE_CSHARP_STRING_NULL = "decimal?";
                    x.DATA_TYPE_TYPESCRIPT_STRING = "number";
                    break;

                case Enums.CSharpDataTypeEnum.BYTE:
                    x.DATA_TYPE_CSHARP_STRING = "byte";
                    x.DATA_TYPE_CSHARP_STRING_NULL = "byte?";
                    x.DATA_TYPE_TYPESCRIPT_STRING = "byte[]";
                    break;

                case Enums.CSharpDataTypeEnum.BYTE_NULL:
                    x.DATA_TYPE_CSHARP_STRING = "byte?";
                    x.DATA_TYPE_CSHARP_STRING_NULL = "byte?";
                    x.DATA_TYPE_TYPESCRIPT_STRING = "byte[]";
                    break;

                case Enums.CSharpDataTypeEnum.BYTEARRAY:
                    x.DATA_TYPE_CSHARP_STRING = "byte[]";
                    x.DATA_TYPE_TYPESCRIPT_STRING = "byte[]";
                    break;

                case Enums.CSharpDataTypeEnum.OBJECT:
                    x.DATA_TYPE_CSHARP_STRING = "object";
                    x.DATA_TYPE_TYPESCRIPT_STRING = "any";
                    break;

                case Enums.CSharpDataTypeEnum.SQLGEOGRAPHY:
                    x.DATA_TYPE_CSHARP_STRING = "SQLGEOGRAPHY";
                    x.DATA_TYPE_TYPESCRIPT_STRING = "any";
                    break;

                case Enums.CSharpDataTypeEnum.SQLGEOMETRY:
                    x.DATA_TYPE_CSHARP_STRING = "SQLGEOMETRY";
                    x.DATA_TYPE_TYPESCRIPT_STRING = "any";
                    break;

                case Enums.CSharpDataTypeEnum.SQLHIERARCHYID:
                    x.DATA_TYPE_CSHARP_STRING = "SQLHIERARCHYID";
                    x.DATA_TYPE_TYPESCRIPT_STRING = "any";
                    break;

                case Enums.CSharpDataTypeEnum.TIMESTAMP:
                    x.DATA_TYPE_CSHARP_STRING = "byte[]";
                    x.DATA_TYPE_TYPESCRIPT_STRING = "byte[]";
                    break;

                case Enums.CSharpDataTypeEnum.GUID:
                    x.DATA_TYPE_CSHARP_STRING = "Guid";
                    x.DATA_TYPE_TYPESCRIPT_STRING = "any";
                    break;

                default:
                    break;
            }
        }

        public static void ProtobufImportMapper(SchemaModel sModel, ProtobufModel pModel)
        {
            sModel.ColumnList.ForEach(x =>
            {
                switch (x.DATA_TYPE_PROTOBUF)
                {
                    case Enums.GoogleProtoDataTypeEnum.TIMESTAMP:
                        pModel.IsImportTimestamp = true;
                        break;

                    case Enums.GoogleProtoDataTypeEnum.DECIMAL:
                        pModel.IsImportDecimal = true;
                        break;

                    case Enums.GoogleProtoDataTypeEnum.BOOL_NULL:
                    case Enums.GoogleProtoDataTypeEnum.DECIMAL_NULL:
                    case Enums.GoogleProtoDataTypeEnum.DOUBLE_NULL:
                    case Enums.GoogleProtoDataTypeEnum.INT16_NULL:
                    case Enums.GoogleProtoDataTypeEnum.INT32_NULL:
                    case Enums.GoogleProtoDataTypeEnum.INT64_NULL:
                    case Enums.GoogleProtoDataTypeEnum.STRING_NULL:
                        pModel.IsImportWrappers = true;
                        break;

                    default:
                        break;
                }
            });
        }

        public static void ColumnKeyMapper(List<ColumnModel> columnList, List<KeyModel> keyList)
        {
            columnList.ForEach(x =>
            {
                x.IS_KEY = keyList.Exists(y => y.COLUMN_NAME == x.COLUMN_NAME);
            });
        }

        public static void ColumnDescriptionMapper(List<ColumnModel> columnList, List<ColumnModel> descriptionList)
        {
            columnList.ForEach(x =>
            {
                var search = descriptionList?.Find(y => y.COLUMN_NAME == x.COLUMN_NAME);
                if (search != null)
                {
                    x.COLUMN_DESCRIPTION = search.COLUMN_DESCRIPTION;
                }

                if (string.IsNullOrWhiteSpace(x.COLUMN_DESCRIPTION))
                {
                    x.COLUMN_DESCRIPTION = "欄位說明";
                }
            });
        }

        /// <summary>
        /// 去底線
        /// </summary>
        private static string GetEntityColumnName(ColumnModel column)
        {
            char cPrevious = char.MinValue;

            string new_name = string.Empty;
            string org_name = column.COLUMN_NAME;
            int length = column.COLUMN_NAME.Length;

            for (int i = 0; i < length; i++)
            {
                char cNow = org_name[i];

                if (i == 0)
                {
                    new_name += cNow.ToString().ToUpper();
                }
                else
                {
                    if (cPrevious == '_')
                    {
                        new_name += cNow.ToString().ToUpper();
                    }
                    else if (cNow != '_')
                    {
                        new_name += cNow.ToString();
                    }
                }

                cPrevious = org_name[i];
            }

            return new_name;
        }

        /// <summary>
        /// 小寫加底線
        /// </summary>
        private static string GetProtoColumnName(ColumnModel column)
        {
            char cPrevious = char.MinValue;

            string new_name = string.Empty;
            string org_name = column.COLUMN_NAME;
            int length = column.COLUMN_NAME.Length;

            for (int i = 0; i < length; i++)
            {
                bool issplit = false;
                char cNow = org_name[i];

                if (i > 0 && char.IsUpper(cNow))
                {
                    if (cPrevious == 'I' && cNow == 'D')
                    {
                        issplit = false;
                    }
                    else if (cPrevious != '_')
                    {
                        issplit = true;
                    }
                }

                if (issplit)
                {
                    new_name += "_" + cNow.ToString().ToLower();
                }
                else
                {
                    new_name += cNow.ToString().ToLower();
                }
                cPrevious = org_name[i];
            }

            return new_name;
        }

        /// <summary>
        /// 開頭小寫
        /// </summary>
        private static string GetTypeScriptColumnName(ColumnModel column)
        {
            string name = column.COLUMN_NAME;

            bool isAllUp = true;
            foreach (char c in name)
            {
                if (char.IsLetter(c) && char.IsLower(c))
                {
                    isAllUp = false;
                    break;
                }
            }

            string col = isAllUp
                ? name.ToLower()
                : name[0].ToString().ToLower() + name.Substring(1, name.Length - 1);

            return col;
        }

        /// <summary>
        /// 小寫加底線 (EntityColumnName, ID 需變成 Id), 因為 Proto轉型)
        /// </summary>
        private static string GetModelColumnName(ColumnModel column)
        {
            string new_name = string.Empty;
            string org_name = column.COLUMN_NAME_ENTITY;
            if (!string.IsNullOrWhiteSpace(org_name) && org_name.Length > 1 && org_name.EndsWith("ID"))
            {
                new_name = org_name.Substring(0, org_name.Length - 2) + "Id";
            }
            else
            {
                new_name = org_name;
            }
            return new_name;
        }

        /// <summary>
        /// 開頭小寫去底線
        /// </summary>
        private static string GetParamColumnName(ColumnModel column)
        {
            string colName = column.COLUMN_NAME_ENTITY;

            if (colName.Length > 1)
            {
                return colName[0].ToString().ToLower() + colName.Substring(1, colName.Length - 1);
            }
            else
            {
                return colName.ToLower();
            }
        }
    }
}