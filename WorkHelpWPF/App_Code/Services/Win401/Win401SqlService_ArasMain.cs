﻿using System.Collections.Generic;
using System.Text;
using WorkHelp.Wpf.Models;

namespace WorkHelp.Wpf.Services
{
    public class Win401SqlService_ArasMain
    {
        public static string GenerateContent(List<string> list)
        {
            StringBuilder builder = new StringBuilder(2560);
            for (int i = 0; i < list.Count; i++)
            {
                string value = list[i];
                if (builder.Length > 0)
                {
                    builder.AppendLine("UNION ALL");
                }
                else
                {
                    builder.AppendLine($"-- 共 {list.Count} 張資料表--");
                    builder.AppendLine();
                }
                builder.AppendLine($"SELECT '{i}' AS '編號', '{value}' AS'名稱', COUNT(*) AS '筆數' FROM [Innovator].[{value}]");
            }
            builder.AppendLine("GO");
            return builder.ToString();
        }
    }
}
