﻿using System.Text;
using WorkHelp.Wpf.Models;

namespace WorkHelp.Wpf.Services
{
    public class Win401SqlRepositoryServiceInterface
    {
        public static void GenerateContent(SchemaModel sModel, EntityModel eModel)
        {
            StringBuilder builder = new StringBuilder(2560);
            AddContents(builder, sModel, eModel);
            sModel.FileContent = builder.ToString();
        }

        private static void AddContents(StringBuilder builder, SchemaModel sModel, EntityModel eModel)
        {
            builder.AppendLine($"using Microsoft.EntityFrameworkCore;");
            builder.AppendLine($"using System;");
            builder.AppendLine($"using System.Collections.Generic;");
            builder.AppendLine($"using System.Data;");
            builder.AppendLine($"using System.Threading.Tasks;");
            builder.AppendLine($"using {sModel.NameSpaceName}.Entities.{sModel.ControllerName};");
            builder.AppendLine();

            builder.AppendLine($"namespace {sModel.NameSpaceName}.Repositories.{sModel.ControllerName}");
            builder.AppendLine("{");
            builder.AppendLine($"    /// <summary>");
            builder.AppendLine($"    /// {sModel.TableDescription}存取介面");
            builder.AppendLine($"    /// </summary>");
            builder.AppendLine($"    public interface I{sModel.MainName}Repository<TDbContext> : IRepository<TDbContext> where TDbContext : DbContext");
            builder.AppendLine("    {");
            builder.AppendLine($"        /// <summary>");
            builder.AppendLine($"        /// 取得{sModel.TableDescription}清單");
            builder.AppendLine($"        /// </summary>");
            builder.Append(sModel.KeyParamSummary);
            builder.AppendLine($"        /// <param name=\"connection\">資料庫連線，透過 Proxy 注入</param>");
            builder.AppendLine($"        /// <returns>回傳{sModel.TableDescription}清單</returns>");
            builder.AppendLine($"        Task<IEnumerable<{sModel.MainName}>> List{sModel.SubMain}({sModel.KeyParamDataType}, IDbConnection connection = null);");
            builder.AppendLine();
            builder.AppendLine($"        /// <summary>");
            builder.AppendLine($"        /// 取得{sModel.TableDescription}項目");
            builder.AppendLine($"        /// </summary>");
            builder.Append(sModel.KeyParamSummary);
            builder.AppendLine($"        /// <param name=\"connection\">資料庫連線，透過 Proxy 注入</param>");
            builder.AppendLine($"        /// <returns>回傳{sModel.TableDescription}項目</returns>");
            builder.AppendLine($"        Task<{sModel.MainName}> Item{sModel.SubMain}({sModel.KeyParamDataType}, IDbConnection connection = null);");
            builder.AppendLine();
            builder.AppendLine($"        /// <summary>");
            builder.AppendLine($"        /// 儲存{sModel.TableDescription}");
            builder.AppendLine($"        /// </summary>");
            builder.AppendLine($"        /// <param name=\"list\">要儲存的{sModel.TableDescription}內容</param>");
            builder.AppendLine($"        /// <param name=\"connection\">資料庫連線，透過 Proxy 注入</param>");
            builder.AppendLine($"        Task<bool> Save{sModel.SubMain}({sModel.MainName}[] list, IDbConnection connection = null);");
            builder.AppendLine();
            builder.AppendLine($"        /// <summary>");
            builder.AppendLine($"        /// 刪除{sModel.TableDescription}");
            builder.AppendLine($"        /// </summary>");
            builder.AppendLine($"        /// <param name=\"list\">要刪除的{sModel.TableDescription}內容</param>");
            builder.AppendLine($"        /// <param name=\"connection\">資料庫連線，透過 Proxy 注入</param>");
            builder.AppendLine($"        Task<bool> Remove{sModel.SubMain}({sModel.MainName}[] list, IDbConnection connection = null);");
            builder.AppendLine("    }");
            builder.AppendLine("}");
        }
    }
}