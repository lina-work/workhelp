﻿using System.Text;
using WorkHelp.Wpf.Models;

namespace WorkHelp.Wpf.Services
{
    public class Win401SqlRepositoryService
    {
        public static void GenerateContent(SchemaModel sModel, EntityModel eModel)
        {
            StringBuilder builder = new StringBuilder(2560);
            AddContents(builder, sModel, eModel);
            sModel.FileContent = builder.ToString();
        }

        private static void AddContents(StringBuilder builder, SchemaModel sModel, EntityModel eModel)
        {
            builder.AppendLine($"using Dapper;");
            builder.AppendLine($"using Microsoft.Extensions.Configuration;");
            builder.AppendLine($"using System;");
            builder.AppendLine($"using System.Collections.Generic;");
            builder.AppendLine($"using System.Data;");
            builder.AppendLine($"using System.Linq;");
            builder.AppendLine($"using System.Threading.Tasks;");
            builder.AppendLine($"using {sModel.NameSpaceName}.Context;");
            builder.AppendLine($"using {sModel.NameSpaceName}.Context.Data;");
            builder.AppendLine($"using {sModel.NameSpaceName}.Entities.{sModel.ControllerName};");
            builder.AppendLine($"using {sModel.NameSpaceName}.Errors;");
            builder.AppendLine($"using Ticket.Library.Extension;");
            builder.AppendLine();

            builder.AppendLine($"namespace {sModel.NameSpaceName}.Repositories.{sModel.ControllerName}.Impl");
            builder.AppendLine("{");
            builder.AppendLine($"    /// <summary>");
            builder.AppendLine($"    /// {sModel.TableDescription}存取實作");
            builder.AppendLine($"    /// </summary>");
            builder.AppendLine($"    public class {sModel.MainName}Repository : I{sModel.MainName}Repository<TicketDbContext>");
            builder.AppendLine("    {");
            builder.AppendLine($"        /// <summary>");
            builder.AppendLine($"        /// 設定檔");
            builder.AppendLine($"        /// </summary>");
            builder.AppendLine("        public IConfiguration Configuration { get; set; }");
            builder.AppendLine();
            builder.AppendLine($"        /// <summary>");
            builder.AppendLine($"        /// 設定檔物件");
            builder.AppendLine($"        /// </summary>");
            builder.AppendLine($"        public TicketConfig TicketConfig => Configuration.Bind<TicketConfig>();");
            builder.AppendLine();
            builder.AppendLine($"        /// <summary>");
            builder.AppendLine($"        /// 取得{sModel.TableDescription}清單");
            builder.AppendLine($"        /// </summary>");
            builder.Append(sModel.KeyParamSummary);
            builder.AppendLine($"        /// <param name=\"connection\">資料庫連線，透過 Proxy 注入</param>");
            builder.AppendLine($"        /// <returns>回傳{sModel.TableDescription}</returns>");
            builder.AppendLine($"        public async Task<IEnumerable<{sModel.MainName}>> List{sModel.SubMain}({sModel.KeyParamDataType}, IDbConnection connection = null)");
            builder.AppendLine("        {");
            builder.AppendLine($"            // 參數驗證");
            builder.AppendLine($"            InvalidInputExceptionBuilder.Create()");
            builder.Append(sModel.KeyParamValidate);
            builder.AppendLine($"                .ThrowIfError();");
            builder.AppendLine();
            builder.AppendLine($"            string statement = @\"");
            builder.Append(GetSelectStatement(sModel));
            builder.AppendLine($"\";");
            builder.AppendLine();
            builder.AppendLine($"            return await connection.QueryAsync<{sModel.MainName}>(statement, new");
            builder.AppendLine("            {");
            builder.Append(GetSelectStatementParameters(sModel));
            builder.AppendLine("            });");
            builder.AppendLine("        }");
            builder.AppendLine();
            builder.AppendLine($"        /// <summary>");
            builder.AppendLine($"        /// 取得{sModel.TableDescription}項目");
            builder.AppendLine($"        /// </summary>");
            builder.Append(sModel.KeyParamSummary);
            builder.AppendLine($"        /// <param name=\"connection\">資料庫連線，透過 Proxy 注入</param>");
            builder.AppendLine($"        /// <returns>回傳{sModel.TableDescription}項目</returns>");
            builder.AppendLine($"        public async Task<{sModel.MainName}> Item{sModel.SubMain}({sModel.KeyParamDataType}, IDbConnection connection = null)");
            builder.AppendLine("        {");
            builder.AppendLine($"            // 參數驗證");
            builder.AppendLine($"            InvalidInputExceptionBuilder.Create()");
            builder.Append(sModel.KeyParamValidate);
            builder.AppendLine($"                .ThrowIfError();");
            builder.AppendLine();
            builder.AppendLine($"            string statement = @\"");
            builder.Append(GetSelectStatement(sModel));
            builder.AppendLine($"\";");
            builder.AppendLine();
            builder.AppendLine($"            IEnumerable<{sModel.MainName}> result = await connection.QueryAsync<{sModel.MainName}>(statement, new");
            builder.AppendLine("            {");
            builder.Append(GetSelectStatementParameters(sModel));
            builder.AppendLine("            });");
            builder.AppendLine();
            builder.AppendLine($"            return result.FirstOrDefault();");
            builder.AppendLine("        }");
            builder.AppendLine();
            builder.AppendLine($"        /// <summary>");
            builder.AppendLine($"        /// 儲存{sModel.TableDescription}");
            builder.AppendLine($"        /// </summary>");
            builder.AppendLine($"        /// <param name=\"list\">要儲存的{sModel.TableDescription}內容</param>");
            builder.AppendLine($"        /// <param name=\"connection\">資料庫連線，透過 Proxy 注入</param>");
            builder.AppendLine($"        public async Task<bool> Save{sModel.SubMain}({sModel.MainName}[] list, IDbConnection connection = null)");
            builder.AppendLine("        {");
            builder.AppendLine($"            if (!list.Any()) return false;");
            builder.AppendLine();
            builder.AppendLine($"            // 參數驗證");
            builder.AppendLine($"            InvalidInputExceptionBuilder.Create()");
            builder.AppendLine($"                .ValidateAll(list, (item, builder) =>");
            builder.AppendLine("                {");
            builder.AppendLine($"                    builder");
            builder.Append(sModel.ColumEntityValidate);
            builder.AppendLine("                })");
            builder.AppendLine($"                .ThrowIfError();");
            builder.AppendLine();
            builder.AppendLine($"            string statement = @\"");
            builder.Append(GetSaveStatement(sModel));
            builder.AppendLine($"\";");
            builder.AppendLine($"            foreach ({sModel.MainName} item in list)");
            builder.AppendLine("            {");
            builder.AppendLine($"                int rowsAffected = await connection.ExecuteAsync(statement, new");
            builder.AppendLine("                {");
            builder.Append(GetSaveStatementParameters(sModel));
            builder.AppendLine("                });");
            builder.AppendLine($"                if (rowsAffected <= 0)");
            builder.AppendLine("                {");
            builder.AppendLine($"                    return false;");
            builder.AppendLine("                }");
            builder.AppendLine("            }");
            builder.AppendLine();
            builder.AppendLine($"            return true;");
            builder.AppendLine("        }");
            builder.AppendLine();
            builder.AppendLine($"        /// <summary>");
            builder.AppendLine($"        /// 刪除{sModel.TableDescription}");
            builder.AppendLine($"        /// </summary>");
            builder.AppendLine($"        /// <param name=\"list\">要刪除的{sModel.TableDescription}內容</param>");
            builder.AppendLine($"        /// <param name=\"connection\">資料庫連線，透過 Proxy 注入</param>");
            builder.AppendLine($"        public async Task<bool> Remove{sModel.SubMain}({sModel.MainName}[] list, IDbConnection connection = null)");
            builder.AppendLine("        {");
            builder.AppendLine($"            if (!list.Any()) return false;");
            builder.AppendLine();
            builder.AppendLine($"            // 參數驗證");
            builder.AppendLine($"            InvalidInputExceptionBuilder.Create()");
            builder.AppendLine($"                .ValidateAll(list, (item, builder) =>");
            builder.AppendLine("                {");
            builder.AppendLine($"                    builder");
            builder.Append(sModel.ColumEntityValidate);
            builder.AppendLine("                })");
            builder.AppendLine($"                .ThrowIfError();");
            builder.AppendLine();
            builder.AppendLine($"            string statement = @\"");
            builder.Append(GetDeleteStatement(sModel));
            builder.AppendLine($"\";");
            builder.AppendLine();
            builder.AppendLine($"            foreach ({sModel.MainName} item in list)");
            builder.AppendLine("            {");
            builder.AppendLine($"                int rowsAffected = await connection.ExecuteAsync(statement, new");
            builder.AppendLine("                {");
            builder.Append(GetDeleteStatementParameters(sModel));
            builder.AppendLine("                });");
            builder.AppendLine($"                if (rowsAffected <= 0)");
            builder.AppendLine("                {");
            builder.AppendLine($"                    return false;");
            builder.AppendLine("                }");
            builder.AppendLine("            }");
            builder.AppendLine();
            builder.AppendLine($"            return true;");
            builder.AppendLine("        }");
            builder.AppendLine("    }");
            builder.AppendLine("}");
        }

        private static string GetSelectStatement(SchemaModel sModel)
        {
            StringBuilder builder = new StringBuilder(512);
            StringBuilder builderCol = new StringBuilder(512);
            StringBuilder builderWhere = new StringBuilder(512);

            if (sModel.ColumnList != null && sModel.ColumnList.Count > 0)
            {
                sModel.ColumnList.ForEach(x =>
                {
                    if (x.IS_OUTPUT)
                    {
                        if (builderCol.Length > 0)
                        {
                            builderCol.AppendLine(",");
                        }
                        builderCol.Append($"                    [{x.COLUMN_NAME}]");
                    }
                });
                builderCol.AppendLine();
            }

            if (sModel.KeyList != null && sModel.KeyList.Count > 0)
            {
                string partAnd = "";
                sModel.KeyList.ForEach(x =>
                {
                    if (builderWhere.Length > 0)
                    {
                        partAnd = "AND ";
                    }
                    else
                    {
                        builderWhere.AppendLine("                WHERE");
                    }
                    builderWhere.AppendLine($"                    {partAnd}[{x.COLUMN_NAME}] = @{x.COLUMN_NAME}");
                });
            }

            builder.AppendLine("                SELECT");
            builder.Append(builderCol);
            builder.AppendLine("                FROM");
            builder.AppendLine($"                    [dbo].[{sModel.TableName}]");
            builder.Append(builderWhere);
            return builder.ToString();
        }

        private static string GetSelectStatementParameters(SchemaModel sModel)
        {
            StringBuilder builder = new StringBuilder(512);

            if (sModel.KeyList != null && sModel.KeyList.Count > 0)
            {
                sModel.KeyList.ForEach(x =>
                {
                    if (builder.Length > 0)
                    {
                        builder.AppendLine(",");
                    }

                    builder.Append($"    {x.COLUMN_NAME} = {x.COLUMN_NAME_PARAM}");
                });
                builder.AppendLine();
            }

            return builder.ToString();
        }

        private static string GetSaveStatement(SchemaModel sModel)
        {
            StringBuilder builder = new StringBuilder(512);
            StringBuilder builderCol = new StringBuilder(512);

            if (sModel.ColumnList != null && sModel.ColumnList.Count > 0)
            {
                sModel.ColumnList.ForEach(x =>
                {
                    if (x.IS_OUTPUT)
                    {
                        if (builderCol.Length > 0)
                        {
                            builderCol.AppendLine(",");
                        }
                        builderCol.Append($"                    @{x.COLUMN_NAME}");
                    }
                });
                builderCol.AppendLine();
            }

            builder.AppendLine($"                EXEC [dbo].[usp_Save{sModel.MainName}]");
            builder.Append(builderCol);
            return builder.ToString();
        }

        private static string GetSaveStatementParameters(SchemaModel sModel)
        {
            StringBuilder builder = new StringBuilder(512);

            if (sModel.ColumnList != null && sModel.ColumnList.Count > 0)
            {
                sModel.ColumnList.ForEach(x =>
                {
                    if (x.IS_OUTPUT)
                    {
                        if (builder.Length > 0)
                        {
                            builder.AppendLine(",");
                        }

                        builder.Append($"    {x.COLUMN_NAME} = item.{x.COLUMN_NAME_ENTITY}");
                    }
                });
                builder.AppendLine();
            }

            return builder.ToString();
        }

        private static string GetDeleteStatement(SchemaModel sModel)
        {
            StringBuilder builder = new StringBuilder(512);
            StringBuilder builderWhere = new StringBuilder(512);

            if (sModel.KeyList != null && sModel.KeyList.Count > 0)
            {
                string partAnd = "";
                sModel.KeyList.ForEach(x =>
                {
                    if (builderWhere.Length > 0)
                    {
                        partAnd = "AND ";
                    }
                    else
                    {
                        builderWhere.AppendLine("                WHERE");
                    }
                    builderWhere.AppendLine($"                    {partAnd}[{x.COLUMN_NAME}] = @{x.COLUMN_NAME}");
                });
            }

            builder.AppendLine($"                DELETE FROM");
            builder.AppendLine($"                    [dbo].[{sModel.TableName}]");
            builder.Append(builderWhere);
            return builder.ToString();
        }

        private static string GetDeleteStatementParameters(SchemaModel sModel)
        {
            StringBuilder builder = new StringBuilder(512);

            if (sModel.KeyList != null && sModel.KeyList.Count > 0)
            {
                sModel.KeyList.ForEach(x =>
                {
                    if (builder.Length > 0)
                    {
                        builder.AppendLine(",");
                    }

                    builder.Append($"    {x.COLUMN_NAME} = item.{x.COLUMN_NAME_ENTITY}");
                });
                builder.AppendLine();
            }

            return builder.ToString();
        }
    }
}