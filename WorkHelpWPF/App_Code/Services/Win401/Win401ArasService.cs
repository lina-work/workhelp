﻿using System.Text;
using WorkHelp.Wpf.Models;

namespace WorkHelp.Wpf.Services
{
    public class Win401ArasService
    {
        public static void GenerateContent(SchemaModel sModel, EntityModel eModel)
        {
            StringBuilder builder = new StringBuilder(2560);
            AddContents(builder, sModel, eModel);
            sModel.FileContent = builder.ToString();
        }

        private static void AddContents(StringBuilder builder, SchemaModel sModel, EntityModel eModel)
        {
            builder.AppendLine(GetProperties(sModel));
        }

        private static string GetProperties(SchemaModel sModel)
        {
            StringBuilder builder = new StringBuilder(512);
            StringBuilder builderCol = new StringBuilder(512);
            StringBuilder builderClass = new StringBuilder(512);

            builderClass.AppendLine("private class T" + sModel.TableName);
            builderClass.AppendLine("{");
            if (sModel.ColumnList != null && sModel.ColumnList.Count > 0)
            {
                sModel.ColumnList.ForEach(x =>
                {
                    if (x.IS_OUTPUT)
                    {
                        builderCol.AppendLine($" string col_aaa = item.getProperty(\"{x.COLUMN_NAME}\", \"\");");
                        builderClass.AppendLine($"    public string {x.COLUMN_NAME.ToLower()}" + " { get; set; }" );
                    }
                });
                builderCol.AppendLine();
            }
            builderClass.AppendLine("}");

            builder.Append(builderClass);
            builder.AppendLine();
            builder.Append(builderCol);
            return builder.ToString();
        }

    }
}