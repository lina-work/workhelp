﻿using System.Text;
using WorkHelp.Wpf.Models;

namespace WorkHelp.Wpf.Services
{
    public class Win401WpfServiceInterface
    {
        public static void GenerateContent(SchemaModel sModel, EntityModel eModel)
        {
            StringBuilder builder = new StringBuilder(2560);
            AddContents(builder, sModel, eModel);
            sModel.FileContent = builder.ToString();
        }

        private static void AddContents(StringBuilder builder, SchemaModel sModel, EntityModel eModel)
        {
            builder.AppendLine("using System.Collections.Generic;");
            builder.AppendLine($"using Ticket.Models.{sModel.ControllerName};");
            builder.AppendLine("");
            builder.AppendLine($"namespace {sModel.NameSpaceName}.Services.{sModel.ControllerName}");
            builder.AppendLine("{");
            builder.AppendLine("    /// <summary>");
            builder.AppendLine($"    /// {sModel.TableDescription}服務");
            builder.AppendLine("    /// </summary>");
            builder.AppendLine($"    public interface I{sModel.MainName}Service");
            builder.AppendLine("    {");
            builder.AppendLine("        /// <summary>");
            builder.AppendLine($"        /// 取得{sModel.TableDescription}清單");
            builder.AppendLine("        /// </summary>");
            builder.Append(sModel.KeyParamSummary);
            builder.AppendLine("        /// <param name=\"useCache\">先由快取找值</param>");
            builder.AppendLine($"        /// <returns>回傳{sModel.TableDescription}清單</returns>");
            builder.AppendLine($"        List<{sModel.MainName}Model> List{sModel.SubMain}({sModel.KeyParamDataType}, bool useCache = true);");
            builder.AppendLine("");
            builder.AppendLine("        /// <summary>");
            builder.AppendLine($"        /// 取得{sModel.TableDescription}項目");
            builder.AppendLine("        /// </summary>");
            builder.Append(sModel.KeyParamSummary);
            builder.AppendLine("        /// <param name=\"useCache\">先由快取找值</param>");
            builder.AppendLine($"        /// <returns>回傳{sModel.TableDescription}項目</returns>");
            builder.AppendLine($"        {sModel.MainName}Model Item{sModel.SubMain}({sModel.KeyParamDataType}, bool useCache = true); ");
            builder.AppendLine("");
            builder.AppendLine("        /// <summary>");
            builder.AppendLine($"        /// 儲存{sModel.TableDescription}");
            builder.AppendLine("        /// </summary>");
            builder.AppendLine($"        /// <param name=\"list\">要儲存的{sModel.TableDescription}內容</param>");
            builder.AppendLine($"        bool Save{sModel.SubMain}(params {sModel.MainName}Model[] list);");
            builder.AppendLine("");
            builder.AppendLine("        /// <summary>");
            builder.AppendLine($"        /// 刪除{sModel.TableDescription}");
            builder.AppendLine("        /// </summary>");
            builder.AppendLine($"        /// <param name=\"list\">要刪除的{sModel.TableDescription}內容</param>");
            builder.AppendLine($"        bool Remove{sModel.SubMain}(params {sModel.MainName}Model[] list);");
            builder.AppendLine("");
            builder.AppendLine("    }");
            builder.AppendLine("}");
        }
    }
}