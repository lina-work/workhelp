﻿using System.Collections.Generic;
using System.Text;
using WorkHelp.Wpf.Models;

namespace WorkHelp.Wpf.Services
{
    public class Win401ProtobufService
    {
        public static void GenerateContent(SchemaModel sModel, ProtobufModel pModel)
        {
            StringBuilder builder = new StringBuilder(2560);
            AddContents(builder, sModel, pModel);
            sModel.FileContent = builder.ToString();
        }

        private static void AddContents(StringBuilder builder, SchemaModel sModel, ProtobufModel pModel)
        {
            builder.AppendLine("syntax=\"proto3\";");
            builder.AppendLine();

            bool bImport = false;
            if (pModel.IsImportWrappers)
            {
                builder.AppendLine("import \"google/protobuf/timestamp.proto\";");
                bImport = true;
            }
            if (pModel.IsImportWrappers)
            {
                builder.AppendLine("import \"google/protobuf/wrappers.proto\";");
                bImport = true;
            }
            if (pModel.IsImportDecimal)
            {
                builder.AppendLine("import \"Base/Decimal.proto\";");
                bImport = true;
            }

            if (bImport)
            {
                builder.AppendLine();
            }

            builder.AppendLine($"package Ticket.Models.{sModel.ControllerName};");
            builder.AppendLine();

            builder.AppendLine($"// [{sModel.TableDescription}]");
            builder.AppendLine($"message {sModel.MainName}Model" + " {");

            ColumnList(builder, sModel.ColumnList);

            builder.AppendLine("}");
        }

        private static void ColumnList(StringBuilder builder, List<ColumnModel> list)
        {
            list.ForEach(column =>
            {
                if (column.IS_OUTPUT)
                {
                    SetColumnBefore(builder, column);
                    switch (column.DATA_TYPE_PROTOBUF)
                    {
                        case Enums.GoogleProtoDataTypeEnum.NONE:
                            NoneValue(builder, column);
                            break;

                        case Enums.GoogleProtoDataTypeEnum.BOOL:
                            BoolValue(builder, column);
                            break;

                        case Enums.GoogleProtoDataTypeEnum.BOOL_NULL:
                            BoolNullValue(builder, column);
                            break;

                        case Enums.GoogleProtoDataTypeEnum.STRING:
                            StringValue(builder, column);
                            break;

                        case Enums.GoogleProtoDataTypeEnum.STRING_NULL:
                            StringNullValue(builder, column);
                            break;

                        case Enums.GoogleProtoDataTypeEnum.INT16:
                            Int16Value(builder, column);
                            break;

                        case Enums.GoogleProtoDataTypeEnum.INT16_NULL:
                            Int16NullValue(builder, column);
                            break;

                        case Enums.GoogleProtoDataTypeEnum.INT32:
                            Int32Value(builder, column);
                            break;

                        case Enums.GoogleProtoDataTypeEnum.INT32_NULL:
                            Int32NullValue(builder, column);
                            break;

                        case Enums.GoogleProtoDataTypeEnum.INT64:
                            Int64Value(builder, column);
                            break;

                        case Enums.GoogleProtoDataTypeEnum.INT64_NULL:
                            Int64NullValue(builder, column);
                            break;

                        case Enums.GoogleProtoDataTypeEnum.DOUBLE:
                            DoubleValue(builder, column);
                            break;

                        case Enums.GoogleProtoDataTypeEnum.DOUBLE_NULL:
                            DoubleNullValue(builder, column);
                            break;

                        case Enums.GoogleProtoDataTypeEnum.DECIMAL:
                            DecimalValue(builder, column);
                            break;

                        case Enums.GoogleProtoDataTypeEnum.DECIMAL_NULL:
                            DecimalNullValue(builder, column);
                            break;

                        case Enums.GoogleProtoDataTypeEnum.TIMESTAMP:
                            TimestampValue(builder, column);
                            break;

                        default:
                            break;
                    }
                    SetColumnAfter(builder, column);
                }
            });
        }

        private static void SetColumnBefore(StringBuilder builder, ColumnModel column)
        {
            builder.AppendLine($"	// {column.COLUMN_DESCRIPTION}");
        }

        private static void SetColumnMiddle(StringBuilder builder, ColumnModel column)
        {
        }

        private static void SetColumnAfter(StringBuilder builder, ColumnModel column)
        {
        }

        #region DataType

        private static void NoneValue(StringBuilder builder, ColumnModel column)
        {
            builder.AppendLine($"	{column.DATA_TYPE} {GetColumnName(column)} = {column.ORDINAL_POSITION};");
        }

        private static void BoolValue(StringBuilder builder, ColumnModel column)
        {
            builder.AppendLine($"	bool {GetColumnName(column)} = {column.ORDINAL_POSITION};");
        }

        private static void BoolNullValue(StringBuilder builder, ColumnModel column)
        {
            builder.AppendLine($"	google.protobuf.BoolValue {GetColumnName(column)} = {column.ORDINAL_POSITION};");
        }

        private static void StringValue(StringBuilder builder, ColumnModel column)
        {
            builder.AppendLine($"	string {GetColumnName(column)} = {column.ORDINAL_POSITION};");
        }

        private static void StringNullValue(StringBuilder builder, ColumnModel column)
        {
            builder.AppendLine($"	google.protobuf.StringValue {GetColumnName(column)} = {column.ORDINAL_POSITION};");
        }

        private static void Int16Value(StringBuilder builder, ColumnModel column)
        {
            builder.AppendLine($"	int16 {GetColumnName(column)} = {column.ORDINAL_POSITION};");
        }

        private static void Int16NullValue(StringBuilder builder, ColumnModel column)
        {
            builder.AppendLine($"	google.protobuf.Int16Value {GetColumnName(column)} = {column.ORDINAL_POSITION};");
        }

        private static void Int32Value(StringBuilder builder, ColumnModel column)
        {
            builder.AppendLine($"	int32 {GetColumnName(column)} = {column.ORDINAL_POSITION};");
        }

        private static void Int32NullValue(StringBuilder builder, ColumnModel column)
        {
            builder.AppendLine($"	google.protobuf.Int32Value {GetColumnName(column)} = {column.ORDINAL_POSITION};");
        }

        private static void Int64Value(StringBuilder builder, ColumnModel column)
        {
            builder.AppendLine($"	int64 {GetColumnName(column)} = {column.ORDINAL_POSITION};");
        }

        private static void Int64NullValue(StringBuilder builder, ColumnModel column)
        {
            builder.AppendLine($"	google.protobuf.Int64Value {GetColumnName(column)} = {column.ORDINAL_POSITION};");
        }

        private static void TimestampValue(StringBuilder builder, ColumnModel column)
        {
            builder.AppendLine($"	google.protobuf.Timestamp {GetColumnName(column)} = {column.ORDINAL_POSITION};");
        }

        private static void DoubleValue(StringBuilder builder, ColumnModel column)
        {
            builder.AppendLine($"	google.protobuf.DoubleValue {GetColumnName(column)} = {column.ORDINAL_POSITION};");
        }

        private static void DoubleNullValue(StringBuilder builder, ColumnModel column)
        {
            builder.AppendLine($"	google.protobuf.DoubleValue {GetColumnName(column)} = {column.ORDINAL_POSITION};");
        }

        private static void DecimalValue(StringBuilder builder, ColumnModel column)
        {
            builder.AppendLine($"	Ticket.Base.Decimal {GetColumnName(column)} = {column.ORDINAL_POSITION};");
        }

        private static void DecimalNullValue(StringBuilder builder, ColumnModel column)
        {
            builder.AppendLine($"	google.protobuf.DoubleValue {GetColumnName(column)} = {column.ORDINAL_POSITION};");
        }

        private static void ByteValue(StringBuilder builder, ColumnModel column)
        {
            builder.AppendLine($"	Byte {GetColumnName(column)} = {column.ORDINAL_POSITION};");
        }

        private static void ByteNullValue(StringBuilder builder, ColumnModel column)
        {
            builder.AppendLine($"	ByteNull {GetColumnName(column)} = {column.ORDINAL_POSITION};");
        }

        private static void ByteArrayValue(StringBuilder builder, ColumnModel column)
        {
            builder.AppendLine($"	ByteArray {GetColumnName(column)} = {column.ORDINAL_POSITION};");
        }

        private static void GUIDValue(StringBuilder builder, ColumnModel column)
        {
            builder.AppendLine($"	string {GetColumnName(column)} = {column.ORDINAL_POSITION};");
        }

        #endregion DataType

        private static string GetColumnName(ColumnModel column)
        {
            return column.COLUMN_NAME_PROTO;
        }

        private static string GetColumnDescription(ColumnModel column)
        {
            return column.COLUMN_DESCRIPTION.Replace("\r\n", "； ");
        }
    }
}