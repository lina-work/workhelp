﻿using System;
using System.Collections.Generic;
using System.Text;
using WorkHelp.Wpf.Models;

namespace WorkHelp.Wpf.Services
{
    public class Win401Utility
    {
        #region Key

        public static string KeyParamSummary(List<ColumnModel> keyList)
        {
            /* Format
             *      /// <param name="platform">鍵值，通路平台代碼，預設值<code>null</code></param>
             *      /// <param name="templateID">鍵值，樣板代碼，預設值<code>null</code></param>
             * */

            var builder = new StringBuilder(128);
            if (keyList != null && keyList.Count > 0)
            {
                keyList.ForEach(x =>
                {
                    builder.AppendLine($"        /// <param name=\"{x.COLUMN_NAME_PARAM}\">鍵值，{x.COLUMN_DESCRIPTION}，預設值<code>null</code></param>");
                });
            }
            return builder.ToString();
        }

        public static string KeyParamDataType(List<ColumnModel> keyList)
        {
            /* Format
             *     string platform, int templateID
             * */

            var builder = new StringBuilder(128);
            if (keyList != null && keyList.Count > 0)
            {
                keyList.ForEach(x =>
                {
                    if (builder.Length > 0)
                    {
                        builder.Append(", ");
                    }
                    builder.Append($"{x.DATA_TYPE_CSHARP_STRING_NULL} {x.COLUMN_NAME_PARAM}");
                    //builder.Append($"{x.DATA_TYPE_CSHARP_STRING} {x.COLUMN_NAME_PARAM}");
                });
            }
            return builder.ToString();
        }

        public static string KeyParamValidate(List<ColumnModel> keyList)
        {
            /* Format
             *    .ValidateNotNullOrEmpty(platform, nameof(platform))
             *    .Validate(() => templateID > 0, nameof(templateID))
             * */

            var builder = new StringBuilder(128);
            if (keyList != null && keyList.Count > 0)
            {
                keyList.ForEach(x =>
                {
                    if (!x.IS_NULLABLE_BOOL)
                    {
                        switch (x.DATA_TYPE_CSHARP)
                        {
                            case Enums.CSharpDataTypeEnum.INT16:
                            case Enums.CSharpDataTypeEnum.INT32:
                            case Enums.CSharpDataTypeEnum.INT64:
                                builder.AppendLine($"                .Validate(() => {x.COLUMN_NAME_PARAM} > 0, nameof({x.COLUMN_NAME_PARAM}))");
                                break;

                            default:
                                builder.AppendLine($"                .ValidateNotNullOrEmpty({x.COLUMN_NAME_PARAM}, nameof({x.COLUMN_NAME_PARAM}))");
                                break;
                        }
                    }
                });
            }
            return builder.ToString();
        }

        public static string KeyEntityWhere(List<ColumnModel> keyList)
        {
            /* Format
             *    e.Platform == platform
             *    && e.TemplateID == templateID);
             * */

            var builder = new StringBuilder(128);
            var part = string.Empty;
            if (keyList != null && keyList.Count > 0)
            {
                keyList.ForEach(x =>
                {
                    if (builder.Length > 0)
                    {
                        builder.AppendLine();
                        part = "&& ";
                    }

                    switch (x.DATA_TYPE_CSHARP)
                    {
                        case Enums.CSharpDataTypeEnum.INT16:
                        case Enums.CSharpDataTypeEnum.INT16_NULL:
                        case Enums.CSharpDataTypeEnum.INT32:
                        case Enums.CSharpDataTypeEnum.INT32_NULL:
                        case Enums.CSharpDataTypeEnum.INT64:
                        case Enums.CSharpDataTypeEnum.INT64_NULL:
                        case Enums.CSharpDataTypeEnum.GUID:
                            builder.Append($"                {part}e.{x.COLUMN_NAME_ENTITY} == {x.COLUMN_NAME_PARAM}");
                            break;

                        default:
                            builder.Append($"                {part}string.Equals(e.{x.COLUMN_NAME_ENTITY}, {x.COLUMN_NAME_PARAM}, StringComparison.CurrentCultureIgnoreCase)");
                            break;
                    }
                });
                builder.Append(");");
            }
            return builder.ToString();
        }

        public static string KeyEntitySingleOrDefault(List<ColumnModel> list)
        {
            /* Format
             *      .SingleOrDefault(row => item.Platform == row.Platform && item.TemplateID == row.TemplateID);
             * */

            var builder1 = new StringBuilder(128);
            var builder2 = new StringBuilder(128);
            if (list != null && list.Count > 0)
            {
                list.ForEach(x =>
                {
                    if (builder2.Length > 0)
                    {
                        builder2.Append(" && ");
                    }
                    builder2.Append($"item.{x.COLUMN_NAME_ENTITY} == row.{x.COLUMN_NAME_ENTITY}");
                });
            }

            builder1.Append("                    .SingleOrDefault(row => ");
            builder1.Append(builder2);
            builder1.Append(");");

            return builder1.ToString();
        }

        public static string KeyParamDynamic(List<ColumnModel> keyList)
        {
            /* Format
             *     platform: platform, programID: programID
             * */

            var builder = new StringBuilder(128);
            if (keyList != null && keyList.Count > 0)
            {
                keyList.ForEach(x =>
                {
                    if (builder.Length > 0)
                    {
                        builder.Append(", ");
                    }
                    builder.Append($"{x.COLUMN_NAME_PARAM}: {x.COLUMN_NAME_PARAM}");
                });
            }
            return builder.ToString();
        }

        public static string KeyParamValue(List<ColumnModel> keyList)
        {
            /* Format
             *     platform: "平台代碼", programID: "000001"
             * */

            var builder = new StringBuilder(128);
            if (keyList != null && keyList.Count > 0)
            {
                keyList.ForEach(x =>
                {
                    if (builder.Length > 0)
                    {
                        builder.Append(", ");
                    }

                    switch (x.DATA_TYPE_PROTOBUF)
                    {
                        case Enums.GoogleProtoDataTypeEnum.BOOL:
                        case Enums.GoogleProtoDataTypeEnum.BOOL_NULL:
                            builder.Append($"{x.COLUMN_NAME_PARAM}: false");
                            break;

                        case Enums.GoogleProtoDataTypeEnum.DECIMAL:
                        case Enums.GoogleProtoDataTypeEnum.DECIMAL_NULL:
                            builder.Append($"{x.COLUMN_NAME_PARAM}: 1.2");
                            break;

                        case Enums.GoogleProtoDataTypeEnum.DOUBLE:
                        case Enums.GoogleProtoDataTypeEnum.DOUBLE_NULL:
                            builder.Append($"{x.COLUMN_NAME_PARAM}: 2.3");
                            break;

                        case Enums.GoogleProtoDataTypeEnum.INT16:
                        case Enums.GoogleProtoDataTypeEnum.INT16_NULL:
                            builder.Append($"{x.COLUMN_NAME_PARAM}: 9999999");
                            break;

                        case Enums.GoogleProtoDataTypeEnum.INT32:
                        case Enums.GoogleProtoDataTypeEnum.INT32_NULL:
                            builder.Append($"{x.COLUMN_NAME_PARAM}: 9999999");
                            break;

                        case Enums.GoogleProtoDataTypeEnum.INT64:
                        case Enums.GoogleProtoDataTypeEnum.INT64_NULL:
                            builder.Append($"{x.COLUMN_NAME_PARAM}: 9999999");
                            break;

                        case Enums.GoogleProtoDataTypeEnum.STRING:
                        case Enums.GoogleProtoDataTypeEnum.STRING_NULL:
                            builder.Append($"{x.COLUMN_NAME_PARAM}: \"{x.COLUMN_NAME_ENTITY}\"");
                            break;

                        case Enums.GoogleProtoDataTypeEnum.TIMESTAMP:
                            builder.Append($"{x.COLUMN_NAME_PARAM}: DateTimeOffset.Parse(\"2018-02-13T00:00:00+08:00\").ToTimestamp()");
                            break;

                        case Enums.GoogleProtoDataTypeEnum.NONE:
                        default:
                            builder.Append($"{x.COLUMN_NAME_PARAM}: \"{x.COLUMN_NAME_ENTITY}\"");
                            break;
                    }
                });
            }

            return builder.ToString();
        }

        public static string KeyParamName(List<ColumnModel> keyList)
        {
            /* Format
             *     {platform}#{typeID}
             * */

            var builder = new StringBuilder(128);
            if (keyList != null && keyList.Count > 0)
            {
                keyList.ForEach(x =>
                {
                    if (builder.Length > 0)
                    {
                        builder.Append("#");
                    }

                    builder.Append("{" + x.COLUMN_NAME_PARAM + "}");
                });
            }

            return builder.ToString();
        }

        public static string KeyEntityValue(List<ColumnModel> keyList)
        {
            /* Format
             *     Platform: "平台代碼", ProgramID: "000001"
             * */

            var builder = new StringBuilder(128);
            if (keyList != null && keyList.Count > 0)
            {
                keyList.ForEach(x =>
                {
                    if (builder.Length > 0)
                    {
                        builder.AppendLine(", ");
                    }

                    switch (x.DATA_TYPE_PROTOBUF)
                    {
                        case Enums.GoogleProtoDataTypeEnum.BOOL:
                        case Enums.GoogleProtoDataTypeEnum.BOOL_NULL:
                            builder.Append($"{x.COLUMN_NAME_ENTITY}: false;");
                            break;

                        case Enums.GoogleProtoDataTypeEnum.DECIMAL:
                        case Enums.GoogleProtoDataTypeEnum.DECIMAL_NULL:
                            builder.Append($"{x.COLUMN_NAME_ENTITY}: 1.2;");
                            break;

                        case Enums.GoogleProtoDataTypeEnum.DOUBLE:
                        case Enums.GoogleProtoDataTypeEnum.DOUBLE_NULL:
                            builder.Append($"{x.COLUMN_NAME_ENTITY}: 2.3;");
                            break;

                        case Enums.GoogleProtoDataTypeEnum.INT16:
                        case Enums.GoogleProtoDataTypeEnum.INT16_NULL:
                            builder.Append($"{x.COLUMN_NAME_ENTITY}: 9999999;");
                            break;

                        case Enums.GoogleProtoDataTypeEnum.INT32:
                        case Enums.GoogleProtoDataTypeEnum.INT32_NULL:
                            builder.Append($"{x.COLUMN_NAME_ENTITY}: 9999999;");
                            break;

                        case Enums.GoogleProtoDataTypeEnum.INT64:
                        case Enums.GoogleProtoDataTypeEnum.INT64_NULL:
                            builder.Append($"{x.COLUMN_NAME_ENTITY}: 9999999;");
                            break;

                        case Enums.GoogleProtoDataTypeEnum.STRING:
                        case Enums.GoogleProtoDataTypeEnum.STRING_NULL:
                            builder.Append($"{x.COLUMN_NAME_ENTITY}: \"{x.COLUMN_NAME_ENTITY}\"");
                            break;

                        case Enums.GoogleProtoDataTypeEnum.TIMESTAMP:
                            builder.Append($"{x.COLUMN_NAME_ENTITY}: DateTimeOffset.Parse(\"2018-02-13T00:00:00+08:00\").ToTimestamp();");
                            break;

                        case Enums.GoogleProtoDataTypeEnum.NONE:
                        default:
                            builder.Append($"{x.COLUMN_NAME_ENTITY}: \"{x.COLUMN_NAME_ENTITY}\"");
                            break;
                    }
                });
            }

            return builder.ToString();
        }

        public static string KeyParamTestCase(List<ColumnModel> keyList)
        {
            /* Format
             *     "平台代碼", 1
             * */

            var builder = new StringBuilder(128);
            if (keyList != null && keyList.Count > 0)
            {
                keyList.ForEach(x =>
                {
                    if (builder.Length > 0)
                    {
                        builder.Append(", ");
                    }

                    switch (x.DATA_TYPE_PROTOBUF)
                    {
                        case Enums.GoogleProtoDataTypeEnum.BOOL:
                        case Enums.GoogleProtoDataTypeEnum.BOOL_NULL:
                            builder.Append("false");
                            break;

                        case Enums.GoogleProtoDataTypeEnum.DECIMAL:
                        case Enums.GoogleProtoDataTypeEnum.DECIMAL_NULL:
                            builder.Append("1.2");
                            break;

                        case Enums.GoogleProtoDataTypeEnum.DOUBLE:
                        case Enums.GoogleProtoDataTypeEnum.DOUBLE_NULL:
                            builder.Append("2.3");
                            break;

                        case Enums.GoogleProtoDataTypeEnum.INT16:
                        case Enums.GoogleProtoDataTypeEnum.INT16_NULL:
                            builder.Append("9999999");
                            break;

                        case Enums.GoogleProtoDataTypeEnum.INT32:
                        case Enums.GoogleProtoDataTypeEnum.INT32_NULL:
                            builder.Append("9999999");
                            break;

                        case Enums.GoogleProtoDataTypeEnum.INT64:
                        case Enums.GoogleProtoDataTypeEnum.INT64_NULL:
                            builder.Append("9999999");
                            break;

                        case Enums.GoogleProtoDataTypeEnum.STRING:
                        case Enums.GoogleProtoDataTypeEnum.STRING_NULL:
                            builder.Append($"\"{x.COLUMN_NAME_ENTITY}\"");
                            break;

                        case Enums.GoogleProtoDataTypeEnum.TIMESTAMP:
                            builder.Append("\"2018-02-13T00:00:00+08:00\"");
                            break;

                        case Enums.GoogleProtoDataTypeEnum.NONE:
                        default:
                            builder.Append($"\"{x.COLUMN_NAME_ENTITY}\"");
                            break;
                    }
                });
            }

            return builder.ToString();
        }

        public static string KeyRoute(string subRoute, List<ColumnModel> keyList)
        {
            /* Format
             *     [Route("status/list/{platform}")]
             *     [Route("status/list/{platform}/{statusCode}")]
             * */

            var baseRoute = $"{subRoute}/list";
            var builder = new StringBuilder(128);
            if (keyList != null && keyList.Count > 0)
            {
                keyList.ForEach(x =>
                {
                    baseRoute += "/{" + x.COLUMN_NAME_PARAM + "}";
                    builder.AppendLine("        [Route(\"" + baseRoute + "\")]");
                });
            }

            return builder.ToString();
        }

        public static string KeyRouteItem(string subRoute, List<ColumnModel> keyList)
        {
            /* Format
             *     [Route("status/item/{platform}/{statusCode}")]
             * */

            var baseRoute = $"{subRoute}/item";
            var builder = new StringBuilder(128);
            if (keyList != null && keyList.Count > 0)
            {
                keyList.ForEach(x =>
                {
                    baseRoute += "/{" + x.COLUMN_NAME_PARAM + "}";
                });
                builder.AppendLine("        [Route(\"" + baseRoute + "\")]");
            }

            return builder.ToString();
        }

        public static Tuple<string, string, string, string> KeyRouteApi(string mainRoute, string subRoute, List<ColumnModel> keyList)
        {
            /* Format
             *     /api/program/ticket/template/list/{platform}
             *     /api/program/ticket/template/item/{platform}/{typeID}
             *     /api/program/ticket/template/save
             *     /api/program/ticket/template/remove
             * */

            string routeList = $"/{mainRoute}/{subRoute}/list";
            string routeItem = $"/{mainRoute}/{subRoute}/item";
            string routeSave = $"/{mainRoute}/{subRoute}/save";
            string routeRmve = $"/{mainRoute}/{subRoute}/remove";

            if (keyList != null && keyList.Count > 0)
            {
                keyList.ForEach(x =>
                {
                    routeList += "/{" + x.COLUMN_NAME_PARAM + "}";
                    routeItem += "/{" + x.COLUMN_NAME_PARAM + "}";
                });
            }
            return Tuple.Create(routeList, routeItem, routeSave, routeRmve);
        }

        public static string KeyEntityEquals(List<ColumnModel> keyList)
        {
            /* Format
             *      string.Equals(c.Platform, platform, StringComparison.CurrentCultureIgnoreCase)
             *      && string.Equals(c.TypeID, typeID, StringComparison.CurrentCultureIgnoreCase));
             * */

            var builder = new StringBuilder(256);
            var part = string.Empty;
            if (keyList != null && keyList.Count > 0)
            {
                keyList.ForEach(x =>
                {
                    if (builder.Length > 0)
                    {
                        part = "&& ";
                        builder.AppendLine();
                    }

                    switch (x.DATA_TYPE_CSHARP)
                    {
                        case Enums.CSharpDataTypeEnum.INT16:
                        case Enums.CSharpDataTypeEnum.INT16_NULL:
                        case Enums.CSharpDataTypeEnum.INT32:
                        case Enums.CSharpDataTypeEnum.INT32_NULL:
                        case Enums.CSharpDataTypeEnum.INT64:
                        case Enums.CSharpDataTypeEnum.INT64_NULL:
                            builder.Append($"                {part}e.{x.COLUMN_NAME_ENTITY} == {x.COLUMN_NAME_PARAM}");
                            break;

                        default:
                            builder.Append($"                {part}string.Equals(e.{x.COLUMN_NAME_ENTITY}, {x.COLUMN_NAME_PARAM}, StringComparison.CurrentCultureIgnoreCase)");
                            break;
                    }
                });
                builder.Append(");");
            }

            return builder.ToString();
        }

        //public static string KeyParamTestCaseValue(List<ColumnModel> keyList)
        //{
        //    /* Format
        //     *     platform: "平台代碼", programID: "000001"
        //     * */

        //    var builder = new StringBuilder(128);
        //    if (keyList != null && keyList.Count > 0)
        //    {
        //        keyList.ForEach(x =>
        //        {
        //            if (builder.Length > 0)
        //            {
        //                builder.Append(", ");
        //            }

        //            switch (x.DATA_TYPE_PROTOBUF)
        //            {
        //                case Enums.GoogleProtoDataTypeEnum.BOOL:
        //                case Enums.GoogleProtoDataTypeEnum.BOOL_NULL:
        //                    builder.Append($"                {x.COLUMN_NAME_PARAM}: false");
        //                    break;

        //                case Enums.GoogleProtoDataTypeEnum.DECIMAL:
        //                case Enums.GoogleProtoDataTypeEnum.DECIMAL_NULL:
        //                    builder.Append($"                {x.COLUMN_NAME_PARAM}: 1.2");
        //                    break;

        //                case Enums.GoogleProtoDataTypeEnum.DOUBLE:
        //                case Enums.GoogleProtoDataTypeEnum.DOUBLE_NULL:
        //                    builder.Append($"                {x.COLUMN_NAME_PARAM}: 2.3");
        //                    break;

        //                case Enums.GoogleProtoDataTypeEnum.INT16:
        //                case Enums.GoogleProtoDataTypeEnum.INT16_NULL:
        //                    builder.Append($"                {x.COLUMN_NAME_PARAM}: 16");
        //                    break;

        //                case Enums.GoogleProtoDataTypeEnum.INT32:
        //                case Enums.GoogleProtoDataTypeEnum.INT32_NULL:
        //                    builder.Append($"                {x.COLUMN_NAME_PARAM}: 32");
        //                    break;

        //                case Enums.GoogleProtoDataTypeEnum.INT64:
        //                case Enums.GoogleProtoDataTypeEnum.INT64_NULL:
        //                    builder.Append($"                {x.COLUMN_NAME_PARAM}: 64");
        //                    break;

        //                case Enums.GoogleProtoDataTypeEnum.STRING:
        //                case Enums.GoogleProtoDataTypeEnum.STRING_NULL:
        //                    builder.Append($"                {x.COLUMN_NAME_PARAM}: \"{x.COLUMN_NAME_ENTITY}\"");
        //                    break;

        //                case Enums.GoogleProtoDataTypeEnum.TIMESTAMP:
        //                    builder.Append($"                {x.COLUMN_NAME_PARAM}: DateTimeOffset.Parse(\"2018-02-13T00:00:00+08:00\").ToTimestamp()");
        //                    break;

        //                case Enums.GoogleProtoDataTypeEnum.NONE:
        //                default:
        //                    builder.Append($"                {x.COLUMN_NAME_PARAM}: \"{x.COLUMN_NAME_ENTITY}\"");
        //                    break;
        //            }
        //        });
        //    }

        //    return builder.ToString();
        //}

        #endregion Key

        public static string ColumEntityValidate(List<ColumnModel> columnList)
        {
            /* Format
             *    .ValidateNotNullOrEmpty(item.Platform, "list", "Platform 為必要參數")
             *    .Validate(() => item.TemplateID > 0, "list", "TemplateID 為必要參數");
             * */

            var builder = new StringBuilder(128);
            if (columnList != null && columnList.Count > 0)
            {
                bool bNext = false;
                columnList.ForEach(x =>
                {
                    if (!x.IS_NULLABLE_BOOL)
                    {
                        if (bNext)
                        {
                            builder.AppendLine();
                        }

                        switch (x.DATA_TYPE_CSHARP)
                        {
                            case Enums.CSharpDataTypeEnum.INT16:
                            case Enums.CSharpDataTypeEnum.INT32:
                            case Enums.CSharpDataTypeEnum.INT64:
                                builder.Append($"                        .Validate(() => item.{x.COLUMN_NAME_ENTITY} > 0, \"list\", \"{x.COLUMN_NAME_ENTITY} 為必要參數\")");
                                break;

                            default:
                                builder.Append($"                        .ValidateNotNullOrEmpty(item.{x.COLUMN_NAME_ENTITY}, \"list\", \"{x.COLUMN_NAME_ENTITY} 為必要參數\")");
                                break;
                        }

                        bNext = true;
                    }
                });
                if (bNext)
                {
                    builder.AppendLine(";");
                }
            }
            return builder.ToString();
        }

        public static string ColumEntityCopy(List<ColumnModel> columnList)
        {
            /* Format
             *     old.Platform = item.Platform;
             *     old.TemplateID = item.TemplateID;
             * */

            var builder = new StringBuilder(128);
            if (columnList != null && columnList.Count > 0)
            {
                columnList.ForEach(x =>
                {
                    if (x.IS_OUTPUT)
                    {
                        builder.AppendLine($"                    old.{x.COLUMN_NAME_ENTITY} = item.{x.COLUMN_NAME_ENTITY};");
                    }
                });
            }
            return builder.ToString();
        }

        public static string ColumEntityLines_GoogleProto(List<ColumnModel> list)
        {
            /* Format
             *     Platform = "平台代碼",
             *     ProgramID = "000001",
             * */

            var builder = new StringBuilder(128);
            if (list != null && list.Count > 0)
            {
                list.ForEach(x =>
                {
                    if (x.IS_OUTPUT)
                    {
                        if (builder.Length > 0)
                        {
                            builder.AppendLine(",");
                        }

                        switch (x.DATA_TYPE_PROTOBUF)
                        {
                            case Enums.GoogleProtoDataTypeEnum.BOOL:
                            case Enums.GoogleProtoDataTypeEnum.BOOL_NULL:
                                builder.Append($"                {x.COLUMN_NAME_MODEL} = false");
                                break;

                            case Enums.GoogleProtoDataTypeEnum.DECIMAL:
                            case Enums.GoogleProtoDataTypeEnum.DECIMAL_NULL:
                                builder.Append($"                {x.COLUMN_NAME_MODEL} = 1.2");
                                break;

                            case Enums.GoogleProtoDataTypeEnum.DOUBLE:
                            case Enums.GoogleProtoDataTypeEnum.DOUBLE_NULL:
                                builder.Append($"                {x.COLUMN_NAME_MODEL} = 2.3");
                                break;

                            case Enums.GoogleProtoDataTypeEnum.INT16:
                            case Enums.GoogleProtoDataTypeEnum.INT16_NULL:
                                builder.Append($"                {x.COLUMN_NAME_MODEL} = 9999999");
                                break;

                            case Enums.GoogleProtoDataTypeEnum.INT32:
                            case Enums.GoogleProtoDataTypeEnum.INT32_NULL:
                                builder.Append($"                {x.COLUMN_NAME_MODEL} = 9999999");
                                break;

                            case Enums.GoogleProtoDataTypeEnum.INT64:
                            case Enums.GoogleProtoDataTypeEnum.INT64_NULL:
                                builder.Append($"                {x.COLUMN_NAME_MODEL} = 9999999");
                                break;

                            case Enums.GoogleProtoDataTypeEnum.STRING:
                            case Enums.GoogleProtoDataTypeEnum.STRING_NULL:
                                builder.Append($"                {x.COLUMN_NAME_MODEL} = \"{x.COLUMN_NAME_MODEL}\"");
                                break;

                            case Enums.GoogleProtoDataTypeEnum.TIMESTAMP:
                                builder.Append($"                {x.COLUMN_NAME_MODEL} = DateTimeOffset.Parse(\"2018-02-13T00:00:00+08:00\").ToTimestamp()");
                                break;

                            case Enums.GoogleProtoDataTypeEnum.NONE:
                            default:
                                builder.Append($"                {x.COLUMN_NAME_MODEL} = \"{x.COLUMN_NAME_MODEL}\"");
                                break;
                        }
                    }
                });
            }

            return builder.ToString();
        }

        public static string ColumEntityLines_GoogleProto_Update(List<ColumnModel> list)
        {
            /* Format
             *     Platform = "平台代碼",
             *     ProgramID = "000001",
             * */

            var builder = new StringBuilder(128);
            if (list != null && list.Count > 0)
            {
                list.ForEach(x =>
                {
                    if (x.IS_OUTPUT)
                    {
                        if (builder.Length > 0)
                        {
                            builder.AppendLine(",");
                        }

                        switch (x.DATA_TYPE_PROTOBUF)
                        {
                            case Enums.GoogleProtoDataTypeEnum.BOOL:
                            case Enums.GoogleProtoDataTypeEnum.BOOL_NULL:
                                builder.Append($"                {x.COLUMN_NAME_MODEL} = true");
                                break;

                            case Enums.GoogleProtoDataTypeEnum.DECIMAL:
                            case Enums.GoogleProtoDataTypeEnum.DECIMAL_NULL:
                                builder.Append($"                {x.COLUMN_NAME_MODEL} = 2.1");
                                break;

                            case Enums.GoogleProtoDataTypeEnum.DOUBLE:
                            case Enums.GoogleProtoDataTypeEnum.DOUBLE_NULL:
                                builder.Append($"                {x.COLUMN_NAME_MODEL} = 3.2");
                                break;

                            case Enums.GoogleProtoDataTypeEnum.INT16:
                            case Enums.GoogleProtoDataTypeEnum.INT16_NULL:
                                builder.Append($"                {x.COLUMN_NAME_MODEL} = 9999999");
                                break;

                            case Enums.GoogleProtoDataTypeEnum.INT32:
                            case Enums.GoogleProtoDataTypeEnum.INT32_NULL:
                                builder.Append($"                {x.COLUMN_NAME_MODEL} = 9999999");
                                break;

                            case Enums.GoogleProtoDataTypeEnum.INT64:
                            case Enums.GoogleProtoDataTypeEnum.INT64_NULL:
                                builder.Append($"                {x.COLUMN_NAME_MODEL} = 9999999");
                                break;

                            case Enums.GoogleProtoDataTypeEnum.STRING:
                            case Enums.GoogleProtoDataTypeEnum.STRING_NULL:
                                if (x.IS_KEY)
                                {
                                    builder.Append($"                {x.COLUMN_NAME_MODEL} = \"{x.COLUMN_NAME_MODEL}\"");
                                }
                                else
                                {
                                    builder.Append($"                {x.COLUMN_NAME_MODEL} = \"{x.COLUMN_NAME_MODEL}-U\"");
                                }
                                break;

                            case Enums.GoogleProtoDataTypeEnum.TIMESTAMP:
                                builder.Append($"                {x.COLUMN_NAME_MODEL} = DateTimeOffset.Parse(\"2018-01-02T00:00:00+08:00\").ToTimestamp()");
                                break;

                            case Enums.GoogleProtoDataTypeEnum.NONE:
                            default:
                                if (x.IS_KEY)
                                {
                                    builder.Append($"                {x.COLUMN_NAME_MODEL} = \"{x.COLUMN_NAME_MODEL}\"");
                                }
                                else
                                {
                                    builder.Append($"                {x.COLUMN_NAME_MODEL} = \"{x.COLUMN_NAME_MODEL}-U\"");
                                }
                                break;
                        }
                    }
                });
            }

            return builder.ToString();
        }
    }
}