﻿using System.Text;
using WorkHelp.Wpf.Models;

namespace WorkHelp.Wpf.Services
{
    public class Win401WpfTestService
    {
        public static void GenerateContent(SchemaModel sModel, EntityModel eModel)
        {
            StringBuilder builder = new StringBuilder(2560);
            AddContents(builder, sModel, eModel);
            sModel.FileContent = builder.ToString();
        }

        private static void AddContents(StringBuilder builder, SchemaModel sModel, EntityModel eModel)
        {
            builder.AppendLine("using Google.Protobuf.WellKnownTypes;");
            builder.AppendLine("using Microsoft.Extensions.Logging;");
            builder.AppendLine("using NUnit.Framework;");
            builder.AppendLine("using System;");
            builder.AppendLine($"using {sModel.NameSpaceName}.Services.Data;");
            builder.AppendLine($"using {sModel.NameSpaceName}.Services.{sModel.ControllerName};");
            builder.AppendLine($"using {sModel.NameSpaceName}.Test.Integration;");
            builder.AppendLine($"using Ticket.Models.{sModel.ControllerName};");
            builder.AppendLine("");
            builder.AppendLine($"namespace {sModel.NameSpaceName}.Test.Services.{sModel.ControllerName}");
            builder.AppendLine("{");
            builder.AppendLine("    /// <summary>");
            builder.AppendLine($"    /// 測試 <see cref=\"I{sModel.MainName}Service\"/>");
            builder.AppendLine("    /// </summary>");
            builder.AppendLine($"    public class I{sModel.MainName}ServiceTests : AutofacRunner<UnitTestModule>");
            builder.AppendLine("    {");
            builder.AppendLine("        /// <summary>");
            builder.AppendLine("        /// 語系服務");
            builder.AppendLine("        /// </summary>");
            builder.AppendLine("        public ICultureService CultureService { get; set; }");
            builder.AppendLine("");
            builder.AppendLine("        /// <summary>");
            builder.AppendLine($"        /// {sModel.TableDescription}服務"); ;
            builder.AppendLine("        /// </summary>");
            builder.AppendLine($"        public I{sModel.MainName}Service {sModel.MainName}Service " + "{ get; set; }");
            builder.AppendLine("");
            builder.AppendLine("        /// <summary>");
            builder.AppendLine($"        /// 新增{sModel.TableDescription}");
            builder.AppendLine("        /// </summary>");
            builder.AppendLine("        [Test]");
            builder.AppendLine($"        public void Add{sModel.SubMain}Test()");
            builder.AppendLine("        {");
            builder.AppendLine($"            bool result = {sModel.MainName}Service.Save{sModel.SubMain}(new {sModel.MainName}Model");
            builder.AppendLine("            {");
            builder.AppendLine(sModel.ColumEntityLines_GoogleProto);
            builder.AppendLine("            });");
            builder.AppendLine("");
            builder.AppendLine("            Assert.IsTrue(result);");
            builder.AppendLine("");
            builder.AppendLine("            Logger.Debug($\"{result}\");");
            builder.AppendLine("        }");
            builder.AppendLine("");
            builder.AppendLine("        /// <summary>");
            builder.AppendLine($"        /// 取得{sModel.TableDescription}項目測試");
            builder.AppendLine("        /// </summary>");
            builder.AppendLine("        [Test]");
            builder.AppendLine($"        public void Item{sModel.SubMain}Test1()");
            builder.AppendLine("        {");
            builder.AppendLine($"            var item = {sModel.MainName}Service.Item{sModel.SubMain}({sModel.KeyParamValue});");
            builder.AppendLine("");
            builder.AppendLine("            Assert.IsNotNull(item);");
            builder.AppendLine("");
            builder.AppendLine("            Logger.Debug($\"{item}\");");
            builder.AppendLine("        }");
            builder.AppendLine("");
            builder.AppendLine("        /// <summary>");
            builder.AppendLine($"        /// 取得{sModel.TableDescription}清單測試");
            builder.AppendLine("        /// </summary>");
            builder.AppendLine("        [Test]");
            builder.AppendLine($"        public void List{sModel.SubMain}Test()");
            builder.AppendLine("        {");
            builder.AppendLine($"            var list = {sModel.MainName}Service.List{sModel.SubMain}({sModel.KeyParamValue});");
            builder.AppendLine("");
            builder.AppendLine("            Assert.IsNotNull(list);");
            builder.AppendLine("");
            builder.AppendLine("            //Logger.Debug($\"{list}\");");
            builder.AppendLine("        }");
            builder.AppendLine("");
            builder.AppendLine("        /// <summary>");
            builder.AppendLine($"        /// 修改{sModel.TableDescription}");
            builder.AppendLine("        /// </summary>");
            builder.AppendLine("        [Test]");
            builder.AppendLine($"        public void Modify{sModel.SubMain}Test()");
            builder.AppendLine("        {");
            builder.AppendLine($"            bool result = {sModel.MainName}Service.Save{sModel.SubMain}(new {sModel.MainName}Model");
            builder.AppendLine("            {");
            builder.AppendLine(sModel.ColumEntityLines_GoogleProto_Update);
            builder.AppendLine("            });");
            builder.AppendLine("");
            builder.AppendLine("            Assert.IsTrue(result);");
            builder.AppendLine("");
            builder.AppendLine("            Logger.Debug($\"{result}\");");
            builder.AppendLine("        }");
            builder.AppendLine("");
            builder.AppendLine("        /// <summary>");
            builder.AppendLine($"        /// 刪除{sModel.TableDescription}");
            builder.AppendLine("        /// </summary>");
            builder.AppendLine("        [Test]");
            builder.AppendLine($"        public void Remove{sModel.SubMain}Test()");
            builder.AppendLine("        {");
            builder.AppendLine($"            bool result = {sModel.MainName}Service.Remove{sModel.SubMain}(new {sModel.MainName}Model");
            builder.AppendLine("            {");
            builder.AppendLine(sModel.KeyEntityLines_GoogleProto);
            builder.AppendLine("            });");
            builder.AppendLine("");
            builder.AppendLine("            Assert.IsTrue(result);");
            builder.AppendLine("");
            builder.AppendLine("            Logger.Debug($\"{result}\");");
            builder.AppendLine("        }");
            builder.AppendLine("    }");
            builder.AppendLine("}");
        }
    }
}