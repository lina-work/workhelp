﻿using System.Text;
using WorkHelp.Wpf.Models;

namespace WorkHelp.Wpf.Services
{
    public class Win401ColumnService
    {
        public static void GenerateContent(SchemaModel sModel, EntityModel eModel)
        {
            StringBuilder builder = new StringBuilder(2560);
            AddContents(builder, sModel, eModel);
            sModel.FileContent = builder.ToString();
        }

        private static void AddContents(StringBuilder builder, SchemaModel sModel, EntityModel eModel)
        {
            if (sModel.ColumnList != null && sModel.ColumnList.Count > 0)
            {
                sModel.ColumnList.ForEach(x =>
                {
                    builder.AppendLine($"    欄位敘述: {x.COLUMN_DESCRIPTION}");
                    builder.AppendLine($"    資料表欄位: {x.COLUMN_NAME}");
                    builder.AppendLine($"    Entity 欄位: {x.COLUMN_NAME_ENTITY}");
                    builder.AppendLine($"    Model 欄位: {x.COLUMN_NAME_MODEL}");
                    builder.AppendLine($"    Proto 欄位: {x.COLUMN_NAME_PROTO}");
                    builder.AppendLine($"    Param 欄位: {x.COLUMN_NAME_PARAM}");
                    builder.AppendLine($"    型別標示: {x.DATA_TYPE_CSHARP_STRING} {x.COLUMN_NAME_PARAM}");
                    builder.AppendLine($"    NULL 型別標示: {x.DATA_TYPE_CSHARP_STRING_NULL} {x.COLUMN_NAME_PARAM}");
                    builder.AppendLine($"    /// <param name=\"{x.COLUMN_NAME_PARAM}\">{GetKeyMemo(x)}</param>");
                    builder.AppendLine();
                });
            }
        }

        private static string GetKeyMemo(ColumnModel column)
        {
            if (column.IS_KEY)
            {
                return $"鍵值，{column.COLUMN_DESCRIPTION}";
            }
            else
            {
                return column.COLUMN_DESCRIPTION;
            }
        }
    }
}