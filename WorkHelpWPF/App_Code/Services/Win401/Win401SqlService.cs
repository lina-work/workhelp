﻿using System;
using System.Collections.Generic;
using System.Text;
using WorkHelp.Wpf.Models;

namespace WorkHelp.Wpf.Services
{
    public class Win401SqlService
    {
        public static void GenerateContent(SchemaModel sModel, EntityModel eModel)
        {
            StringBuilder builder = new StringBuilder(2560);
            AddContents(builder, sModel, eModel);
            sModel.FileContent = builder.ToString();
        }

        private static void AddContents(StringBuilder builder, SchemaModel sModel, EntityModel eModel)
        {
            DateTime dtNow = DateTime.Now;
            string auth = "auth";

            string columns = GetColumns(sModel.ColumnList);
            string prmDecription = GetParameters(sModel.ColumnList);

            string prmDeclareKey = GetParametersDeclare(sModel.KeyList);
            string prmDeclareAll = GetParametersDeclare(sModel.ColumnList);
            string prmDeclareOrg = GetParametersDeclare(sModel.KeyList, isOrgColumn: true);

            string prmInsert = GetParametersInsert(sModel.ColumnList);
            string prmUpdate = GetParametersUpdate(sModel.ColumnList);

            string prmWhere = GetParametersWhere(sModel.KeyList);
            string prmWhereOrg = GetParametersWhere(sModel.KeyList, isOrgColumn: true);

            builder.AppendLine($"USE [" + sModel.DatabaseName + "]");
            builder.AppendLine($"GO");
            builder.AppendLine($"-- =============================================");
            builder.AppendLine($"-- Description: 基本操作指令");
            builder.AppendLine($"-- Author:      {auth}");
            builder.AppendLine($"-- Create date: {dtNow.ToString("yyyy/MM/dd")}");
            builder.AppendLine($"-- Modify date: {dtNow.ToString("yyyy/MM/dd")}");
            builder.AppendLine($"-- Parameters:");
            builder.Append(prmDecription);
            builder.AppendLine($"-- =============================================");

            builder.AppendLine($"SELECT TOP 3");
            builder.Append(columns);
            builder.AppendLine($"FROM");
            builder.AppendLine($"    [dbo].[{sModel.TableName}] WITH(NOLOCK)");
            builder.AppendLine($"WHERE");
            builder.AppendLine($"    1 = 1");

            builder.AppendLine();
            builder.AppendLine($"-- =============================================");
            builder.AppendLine($"-- Description: 條件查詢");
            builder.AppendLine($"/*");
            builder.Append(prmDeclareKey);
            builder.AppendLine($"SELECT");
            builder.Append(columns);
            builder.AppendLine($"FROM");
            builder.AppendLine($"    [dbo].[{sModel.TableName}] WITH(NOLOCK)");
            builder.AppendLine($"WHERE");
            builder.Append(prmWhere);
            builder.AppendLine($"*/");

            builder.AppendLine();
            builder.AppendLine($"-- =============================================");
            builder.AppendLine($"-- Description: 新增");
            builder.AppendLine($"/*");
            builder.Append(prmDeclareAll);
            builder.AppendLine($"INSERT INTO [dbo].[{sModel.TableName}]");
            builder.AppendLine($"(");
            builder.Append(columns);
            builder.AppendLine($")");
            builder.AppendLine($"VALUES");
            builder.AppendLine($"(");
            builder.Append(prmInsert);
            builder.AppendLine($")");
            builder.AppendLine($"*/");

            builder.AppendLine();
            builder.AppendLine($"-- =============================================");
            builder.AppendLine($"-- Description: 修改");
            builder.AppendLine($"/*");
            builder.Append(prmDeclareOrg);
            builder.Append(prmDeclareAll);
            builder.AppendLine($"UPDATE [dbo].[{sModel.TableName}] SET");
            builder.Append(prmUpdate);
            builder.AppendLine($"WHERE");
            builder.Append(prmWhereOrg);
            builder.AppendLine($"*/");

            builder.AppendLine();
            builder.AppendLine($"-- =============================================");
            builder.AppendLine($"-- Description: 刪除");
            builder.AppendLine($"/*");
            builder.Append(prmDeclareKey);
            builder.AppendLine($"--DELETE FROM");
            builder.AppendLine($"    [dbo].[{sModel.TableName}]");
            builder.AppendLine($"WHERE");
            builder.Append(prmWhere);
            builder.AppendLine($"*/");

            builder.AppendLine();
            builder.AppendLine($"-- =============================================");
            builder.AppendLine($"-- Description: 欄位敘述");
            builder.AppendLine($"/*");
            builder.AppendLine($"SELECT");
            builder.AppendLine($"    t1.TABLE_NAME                as '表格名稱',");
            builder.AppendLine($"    t2.COLUMN_NAME               as '欄位名稱',");
            builder.AppendLine($"    t2.DATA_TYPE                 as '資料型別',");
            builder.AppendLine($"    t2.CHARACTER_MAXIMUM_LENGTH  as '最大長度',");
            builder.AppendLine($"    'NO'                         as '主鍵否',");
            builder.AppendLine($"    t2.COLUMN_DEFAULT            as '預設值',");
            builder.AppendLine($"    t2.IS_NULLABLE               as '允許空值',");
            builder.AppendLine($"    (");
            builder.AppendLine($"        SELECT");
            builder.AppendLine($"            value");
            builder.AppendLine($"        FROM");
            builder.AppendLine($"            fn_listextendedproperty (NULL, 'schema', 'dbo', 'table', ");
            builder.AppendLine($"                                     t1.TABLE_NAME, 'column', default)");
            builder.AppendLine($"        WHERE");
            builder.AppendLine($"            name = 'MS_Description'");
            builder.AppendLine($"            AND objtype = 'COLUMN'");
            builder.AppendLine($"            AND objname Collate Chinese_Taiwan_Stroke_CI_AS = t2.COLUMN_NAME");
            builder.AppendLine($"    ) AS '欄位備註'");
            builder.AppendLine($"FROM");
            builder.AppendLine($"    INFORMATION_SCHEMA.TABLES t1");
            builder.AppendLine($"    LEFT JOIN INFORMATION_SCHEMA.COLUMNS t2 ON (t1.TABLE_NAME = t2.TABLE_NAME)");
            builder.AppendLine($"WHERE");
            builder.AppendLine($"    t1.TABLE_TYPE = 'BASE TABLE'");
            builder.AppendLine($"    AND t1.TABLE_NAME = '{sModel.TableName}'");
            builder.AppendLine($"ORDER BY");
            builder.AppendLine($"    t1.TABLE_NAME, ORDINAL_POSITION");
            builder.AppendLine($"*/");
        }
        private static string GetParameters(List<ColumnModel> columnList)
        {
            // format
            //     @Platform: 通路平台代碼
            StringBuilder builder = new StringBuilder(512);
            if (columnList != null && columnList.Count > 0)
            {
                columnList.ForEach(x =>
                {
                    if (x.IS_OUTPUT)
                    {
                        builder.AppendLine($"--              @{x.COLUMN_NAME}: {x.COLUMN_DESCRIPTION}");
                    }
                });
            }
            return builder.ToString();
        }

        private static string GetColumns(List<ColumnModel> columnList)
        {
            // format
            //     @Platform
            StringBuilder builder = new StringBuilder(512);
            if (columnList != null && columnList.Count > 0)
            {
                columnList.ForEach(x =>
                {
                    string name = x.COLUMN_NAME.ToUpper();

                    if (x.IS_OUTPUT && name != "MODIFYUSER" && name != "MODIFYTIME")
                    {
                        if (builder.Length > 0)
                        {
                            builder.Append(",");
                            builder.AppendLine();
                        }
                        builder.Append($"    [{x.COLUMN_NAME}]");
                    }
                });
                builder.AppendLine();
            }
            return builder.ToString();
        }

        private static string GetParametersDeclare(List<ColumnModel> columnList, bool isOrgColumn = false)
        {
            // format
            //     @Platform VARCHAR(20),
            StringBuilder builder = new StringBuilder(512);
            string now = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
            string org = isOrgColumn ? "_Org" : string.Empty;

            if (columnList != null && columnList.Count > 0)
            {
                columnList.ForEach(x =>
                {
                    if (x.IS_OUTPUT)
                    {
                        switch (x.DATA_TYPE_SQL)
                        {
                            case Enums.SqlDataTypeEnum.CHAR:
                            case Enums.SqlDataTypeEnum.NCHAR:
                            case Enums.SqlDataTypeEnum.VARCHAR:
                            case Enums.SqlDataTypeEnum.NVARCHAR:
                                if (x.CHARACTER_MAXIMUM_LENGTH == -1)
                                {
                                    builder.AppendLine($"DECLARE @{x.COLUMN_NAME}{org} {x.DATA_TYPE_UPPER}(MAX) = '字串';");
                                }
                                else
                                {
                                    builder.AppendLine($"DECLARE @{x.COLUMN_NAME}{org} {x.DATA_TYPE_UPPER}({x.CHARACTER_MAXIMUM_LENGTH}) = '字串';");
                                }
                                break;

                            case Enums.SqlDataTypeEnum.DATETIME:
                            case Enums.SqlDataTypeEnum.DATETIME2:
                            case Enums.SqlDataTypeEnum.DATETIMEOFFSET:
                                builder.AppendLine($"DECLARE @{x.COLUMN_NAME}{org} {x.DATA_TYPE_UPPER} = '{now}';");
                                break;

                            case Enums.SqlDataTypeEnum.UNIQUEIDENTIFIER:
                                builder.AppendLine($"DECLARE @{x.COLUMN_NAME}{org} {x.DATA_TYPE_UPPER} = '{Guid.NewGuid()}';");
                                break;

                            default:
                                builder.AppendLine($"DECLARE @{x.COLUMN_NAME}{org} {x.DATA_TYPE_UPPER} = -123456;");
                                break;
                        }
                    }
                });
                builder.AppendLine();
            }
            return builder.ToString();
        }

        private static string GetParametersWhere(List<ColumnModel> keyList, bool isOrgColumn = false)
        {
            // format
            //     [Platform] = @Platform
            //     AND [Platform] = @Platform
            StringBuilder builder = new StringBuilder(512);
            string org = isOrgColumn ? "_Org" : string.Empty;

            if (keyList != null && keyList.Count > 0)
            {
                string part = string.Empty;
                keyList.ForEach(x =>
                {
                    if (builder.Length > 0)
                    {
                        part = "AND ";
                    }

                    builder.AppendLine($"    {part}[{x.COLUMN_NAME}] = @{x.COLUMN_NAME}{org}");
                });
            }
            return builder.ToString();
        }

        private static string GetParametersInsert(List<ColumnModel> columnList)
        {
            // format
            //     @Platform
            StringBuilder builder = new StringBuilder(512);
            if (columnList != null && columnList.Count > 0)
            {
                columnList.ForEach(x =>
                {
                    string name = x.COLUMN_NAME.ToUpper();

                    if (x.IS_OUTPUT && name != "MODIFYUSER" && name != "MODIFYTIME")
                    {
                        if (builder.Length > 0)
                        {
                            builder.Append(",");
                            builder.AppendLine();
                        }
                        builder.Append($"    @{x.COLUMN_NAME}");
                    }
                });
                builder.AppendLine();
            }
            return builder.ToString();
        }

        private static string GetParametersUpdate(List<ColumnModel> columnList)
        {
            // format
            //     [Platform] = @Platform
            StringBuilder builder = new StringBuilder(512);
            if (columnList != null && columnList.Count > 0)
            {
                columnList.ForEach(x =>
                {
                    string name = x.COLUMN_NAME.ToUpper();

                    if (x.IS_OUTPUT && name != "CREATEUSER" && name != "CREATETIME")
                    {
                        if (builder.Length > 0)
                        {
                            builder.Append(",");
                            builder.AppendLine();
                        }
                        builder.Append($"    [{x.COLUMN_NAME}] = @{x.COLUMN_NAME}");
                    }
                });
                builder.AppendLine();
            }
            return builder.ToString();
        }
    }
}