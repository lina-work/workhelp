﻿using System.Text;
using WorkHelp.Wpf.Models;

namespace WorkHelp.Wpf.Services
{
    public class Win401ControllerService
    {
        public static void GenerateContent(SchemaModel sModel, EntityModel eModel)
        {
            StringBuilder builder = new StringBuilder(2560);
            AddContents(builder, sModel, eModel);
            sModel.FileContent = builder.ToString();
        }

        private static void AddContents(StringBuilder builder, SchemaModel sModel, EntityModel eModel)
        {
            builder.AppendLine("using Microsoft.AspNetCore.Mvc;");
            builder.AppendLine("using System.Collections.Generic;");
            builder.AppendLine("using System.Net;");
            builder.AppendLine("using System.Threading.Tasks;");
            builder.AppendLine($"using {sModel.NameSpaceName}.Services.Data;");
            builder.AppendLine($"using {sModel.NameSpaceName}.Services.{sModel.ControllerName};");
            builder.AppendLine($"using {sModel.NameSpaceName}.Web.Modules.Data;");
            builder.AppendLine($"using Ticket.Models.{sModel.ControllerName};");
            builder.AppendLine("");
            builder.AppendLine($"namespace {sModel.NameSpaceName}.Web.Controllers.{sModel.ControllerName}");
            builder.AppendLine("{");
            builder.AppendLine("    /// <summary>");
            builder.AppendLine($"    /// {sModel.TableDescription} API");
            builder.AppendLine("    /// </summary>");
            builder.AppendLine($"    [Route(\"{sModel.MainRouteName}\")]");
            builder.AppendLine($"    public class {sModel.MainName}Controller : IApiController");
            builder.AppendLine("    {");
            builder.AppendLine("        /// <summary>");
            builder.AppendLine("        /// 語系服務");
            builder.AppendLine("        /// </summary>");
            builder.AppendLine("        public ICultureService CultureService { get; set; }");
            builder.AppendLine("");
            builder.AppendLine("        /// <summary>");
            builder.AppendLine($"        /// {sModel.TableDescription}服務");
            builder.AppendLine("        /// </summary>");
            builder.AppendLine($"        public I{sModel.MainName}Service {sModel.MainName}Service " + "{ get; set; }");
            builder.AppendLine("");
            builder.AppendLine("        /// <summary>");
            builder.AppendLine($"        /// 取得{sModel.TableDescription}列表");
            builder.AppendLine("        /// </summary>");
            builder.Append(sModel.KeyParamSummary);
            builder.AppendLine("        /// <param name=\"useCache\">先由快取找值，預設為 <code>false</code></param>");
            builder.AppendLine($"        /// <returns>回傳{sModel.TableDescription}列表</returns>");
            builder.AppendLine("        [HttpPost]");
            builder.Append(sModel.KeyRoute);
            builder.AppendLine($"        public async Task<IEnumerable<{sModel.MainName}Model>> List{sModel.SubMain}({sModel.KeyParamDataType}, bool? useCache = false)");
            builder.AppendLine("        {");
            builder.AppendLine($"            return await {sModel.MainName}Service.List{sModel.SubMain}({sModel.KeyParamDynamic}, useCache: useCache.Value);");
            builder.AppendLine("        }");
            builder.AppendLine("");
            builder.AppendLine("        /// <summary>");
            builder.AppendLine($"        /// 取得{sModel.TableDescription}項目");
            builder.AppendLine("        /// </summary>");
            builder.Append(sModel.KeyParamSummary);
            builder.AppendLine("        /// <param name=\"useCache\">先由快取找值，預設為 <code>true</code></param>");
            builder.AppendLine($"        /// <returns>回傳{sModel.TableDescription}項目</returns>");
            builder.AppendLine("        [HttpPost]");
            builder.Append(sModel.KeyRouteItem);
            builder.AppendLine($"        public async Task<{sModel.MainName}Model> Item{sModel.SubMain}({sModel.KeyParamDataType}, bool? useCache = true)");
            builder.AppendLine("        {");
            builder.AppendLine($"            return await {sModel.MainName}Service.Item({sModel.SubMain}{sModel.KeyParamDynamic}, useCache: useCache.Value);");
            builder.AppendLine("        }");
            builder.AppendLine("");
            builder.AppendLine("        /// <summary>");
            builder.AppendLine($"        /// 儲存{sModel.TableDescription}");
            builder.AppendLine("        /// </summary>");
            builder.AppendLine($"        /// <param name=\"list\">要儲存的{sModel.TableDescription}內容</param>");
            builder.AppendLine("        [HttpPost]");
            builder.AppendLine($"        [Route(\"{sModel.SubRouteName}/save\")]");
            builder.AppendLine($"        public async Task<object> Save{sModel.SubMain}([FromBody] {sModel.MainName}Model[] list)");
            builder.AppendLine("        {");
            builder.AppendLine($"            await {sModel.MainName}Service.Save{sModel.SubMain}(list);");
            builder.AppendLine("            return HttpStatusCode.OK.AsMessage();");
            builder.AppendLine("        }");
            builder.AppendLine("");
            builder.AppendLine("        /// <summary>");
            builder.AppendLine($"        /// 刪除{sModel.TableDescription}");
            builder.AppendLine("        /// </summary>");
            builder.AppendLine($"        /// <param name=\"list\">要刪除的{sModel.TableDescription}內容</param>");
            builder.AppendLine("        [HttpPost]");
            builder.AppendLine($"        [Route(\"{sModel.SubRouteName}/remove\")]");
            builder.AppendLine($"        public async Task<object> Remove{sModel.SubMain}([FromBody] {sModel.MainName}Model[] list)");
            builder.AppendLine("        {");
            builder.AppendLine($"            await {sModel.MainName}Service.Remove{sModel.SubMain}(list);");
            builder.AppendLine("            return HttpStatusCode.OK.AsMessage();");
            builder.AppendLine("        }");
            builder.AppendLine("");
            builder.AppendLine("    }");
            builder.AppendLine("}");
        }
    }
}