﻿using System.Text;
using WorkHelp.Wpf.Models;

namespace WorkHelp.Wpf.Services
{
    public class Win401DbContextService
    {
        public static void GenerateContent(SchemaModel sModel, EntityModel eModel)
        {
            StringBuilder builder = new StringBuilder(2560);
            AddContents(builder, sModel, eModel);
            sModel.FileContent = builder.ToString();
        }

        private static void AddContents(StringBuilder builder, SchemaModel sModel, EntityModel eModel)
        {
            builder.AppendLine($"using {sModel.NameSpaceName}.Entities.{sModel.ControllerName};");
            builder.AppendLine();

            builder.AppendLine("/// <summary>");
            builder.AppendLine("/// " + sModel.TableDescription);
            builder.AppendLine("/// </summary>");
            builder.AppendLine($"public virtual DbSet<{sModel.MainName}> {sModel.MainName}s" + " { get; set; }");
        }
    }
}