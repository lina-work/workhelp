﻿using System;
using System.Collections.Generic;
using System.Text;
using WorkHelp.Wpf.Models;

namespace WorkHelp.Wpf.Services
{
    public class Win401SqlService_CreateProcedure
    {
        public static void GenerateContent(SchemaModel sModel, EntityModel eModel)
        {
            StringBuilder builder = new StringBuilder(2560);
            AddContents(builder, sModel, eModel);
            sModel.FileContent = builder.ToString();
        }

        private static void AddContents(StringBuilder builder, SchemaModel sModel, EntityModel eModel)
        {
            DateTime dtNow = DateTime.Now;
            string auth = "auth";

            ColumnModel lastKey = GetLastKey(sModel.KeyList);
            SqlColumnModel sqlKey = GetSqlKeyModel(lastKey);

            string prmDecription = GetParameters(sModel.ColumnList);
            string prmDeclare = GetParametersDeclare(sModel.ColumnList);
            string prmWhere = GetParametersWhere(sModel.KeyList);
            string prmPrimary = GetParametersPrimary(sModel, sqlKey);
            string prmInsertA = GetParametersInsertA(sModel.ColumnList);
            string prmInsertB = GetParametersInsertB(sModel.ColumnList);
            string prmUpdate = GetParametersUpdate(sModel.ColumnList);

            builder.AppendLine($"USE [" + sModel.DatabaseName + "]");
            builder.AppendLine($"GO");
            builder.AppendLine($"SET ANSI_NULLS ON");
            builder.AppendLine($"GO");
            builder.AppendLine($"SET QUOTED_IDENTIFIER ON");
            builder.AppendLine($"GO");
            builder.AppendLine($"-- =============================================");
            builder.AppendLine($"-- Name:        usp_Save{sModel.MainName}");
            builder.AppendLine($"-- Description: 儲存[{sModel.TableDescription}]資料");
            builder.AppendLine($"-- Author:      {auth}");
            builder.AppendLine($"-- Create date: {dtNow.ToString("yyyy/MM/dd")}");
            builder.AppendLine($"-- Modify date: {dtNow.ToString("yyyy/MM/dd")}");
            builder.AppendLine($"-- Change log:");
            builder.AppendLine($"--              *{dtNow.ToString("yyyy/MM/dd")} [後台] 儲存[{sModel.TableDescription}]資料 - {auth};");
            builder.AppendLine($"-- Parameters:");
            builder.Append(prmDecription);
            builder.AppendLine($"-- Flow Var:");
            builder.AppendLine($"--              @RET: 執行結果, 預設為 0, 未執行");
            builder.AppendLine($"-- Flow:");
            builder.AppendLine($"--              *檢查資料是否已建檔");
            builder.AppendLine($"--                  \"否\" => 取得最大值+1, 新增[{sModel.TableDescription}]資料，[執行結果]註記為 {sqlKey.KeyDeclare}");
            builder.AppendLine($"--                  \"是\" => 修改[{sModel.TableDescription}]資料，[執行結果]註記為 {sqlKey.KeyDeclare}");
            builder.AppendLine($"-- =============================================");
            builder.AppendLine($"CREATE PROCEDURE [dbo].[usp_Save{sModel.MainName}] ");
            builder.AppendLine($"(");
            builder.Append(prmDeclare);
            builder.AppendLine($")");
            builder.AppendLine($"AS");
            builder.AppendLine($"BEGIN");
            builder.AppendLine($"    -- Description: 停止在部分結果集中傳回顯示 Transact-SQL 陳述式或預存程序所影響之資料列數的訊息。");
            builder.AppendLine($"    -- Reason:      提升預存程序效能");
            builder.AppendLine($"    SET NOCOUNT ON;");
            builder.AppendLine($"-- ======================================================== --");
            builder.AppendLine($"-- ======================= 變數宣告區 ======================= --");
            builder.AppendLine($"-- ======================================================== --");
            builder.AppendLine($"    DECLARE @RET {sqlKey.KeyType};");
            builder.AppendLine($"-- ======================================================== --");
            builder.AppendLine($"-- ======================= 流程執行區 ======================= --");
            builder.AppendLine($"-- ======================================================== --");
            builder.AppendLine($"--  *檢查資料是否已建檔");
            builder.AppendLine($"--      \"否\" => 取得最大值+1, 新增[{sModel.TableDescription}]資料，[執行結果]註記為 {sqlKey.KeyDeclare}");
            builder.AppendLine($"--      \"是\" => 修改[{sModel.TableDescription}]資料，[執行結果]註記為 {sqlKey.KeyDeclare}");
            builder.AppendLine($"    IF NOT EXISTS");
            builder.AppendLine($"    (");
            builder.AppendLine($"        SELECT ");
            builder.AppendLine($"            1 ");
            builder.AppendLine($"        FROM ");
            builder.AppendLine($"            [dbo].[{sModel.TableName}]");
            builder.AppendLine($"        WHERE");
            builder.Append(prmWhere);
            builder.AppendLine($"    )");
            builder.AppendLine($"    BEGIN");
            builder.AppendLine($"    ------- BEGIN PRIMARY KEY -------");
            builder.AppendLine(prmPrimary);
            builder.AppendLine($"    ------- END PRIMARY KEY -------");
            builder.AppendLine($"        INSERT INTO [dbo].[{sModel.TableName}]");
            builder.AppendLine($"        (");
            builder.Append(prmInsertA);
            builder.AppendLine($"        )");
            builder.AppendLine($"        VALUES");
            builder.AppendLine($"        (");
            builder.Append(prmInsertB);
            builder.AppendLine($"        );");
            builder.AppendLine();
            builder.AppendLine($"        SET @RET = {sqlKey.KeyDeclare};");
            builder.AppendLine();
            builder.AppendLine($"    END ELSE BEGIN");
            builder.AppendLine($"        UPDATE [dbo].[{sModel.TableName}] SET");
            builder.Append(prmUpdate);
            builder.AppendLine($"        WHERE");
            builder.Append(prmWhere);
            builder.AppendLine();
            builder.AppendLine($"        SET @RET = {sqlKey.KeyDeclare};");
            builder.AppendLine();
            builder.AppendLine($"    END;");
            builder.AppendLine($"-- ======================================================== --");
            builder.AppendLine($"    SELECT @RET;");
            builder.AppendLine();
            builder.AppendLine($"END;");
        }

        private static ColumnModel GetLastKey(List<ColumnModel> keyList)
        {
            if (keyList != null && keyList.Count > 0)
            {
                return keyList[keyList.Count - 1];
            }
            else
            {
                return new ColumnModel { };
            }
        }

        private static string GetParameters(List<ColumnModel> columnList)
        {
            // format
            //     @Platform: 通路平台代碼
            StringBuilder builder = new StringBuilder(512);
            if (columnList != null && columnList.Count > 0)
            {
                columnList.ForEach(x =>
                {
                    if (x.IS_OUTPUT)
                    {
                        builder.AppendLine($"--              @{x.COLUMN_NAME}: {x.COLUMN_DESCRIPTION}");
                    }
                });
            }
            return builder.ToString();
        }

        private static string GetParametersDeclare(List<ColumnModel> columnList)
        {
            // format
            //     @Platform VARCHAR(20),
            StringBuilder builder = new StringBuilder(512);
            if (columnList != null && columnList.Count > 0)
            {
                columnList.ForEach(x =>
                {
                    if (x.IS_OUTPUT)
                    {
                        if (builder.Length > 0)
                        {
                            builder.Append(",");
                            builder.AppendLine();
                        }
                        switch (x.DATA_TYPE_SQL)
                        {
                            case Enums.SqlDataTypeEnum.CHAR:
                            case Enums.SqlDataTypeEnum.VARCHAR:
                            case Enums.SqlDataTypeEnum.NVARCHAR:
                                if (x.CHARACTER_MAXIMUM_LENGTH == -1)
                                {
                                    builder.Append($"    @{x.COLUMN_NAME} {x.DATA_TYPE}(MAX)");
                                }
                                else
                                {
                                    builder.Append($"    @{x.COLUMN_NAME} {x.DATA_TYPE}({x.CHARACTER_MAXIMUM_LENGTH})");
                                }
                                break;

                            default:
                                builder.Append($"    @{x.COLUMN_NAME} {x.DATA_TYPE}");
                                break;
                        }
                    }
                });
                builder.AppendLine();
            }
            return builder.ToString();
        }

        private static string GetParametersWhere(List<ColumnModel> keyList)
        {
            // format
            //     [Platform] = @Platform
            //     AND [Platform] = @Platform
            StringBuilder builder = new StringBuilder(512);
            if (keyList != null && keyList.Count > 0)
            {
                string part = string.Empty;
                keyList.ForEach(x =>
                {
                    if (builder.Length > 0)
                    {
                        part = "AND ";
                    }

                    builder.AppendLine($"            {part}[{x.COLUMN_NAME}] = @{x.COLUMN_NAME}");
                });
            }
            return builder.ToString();
        }

        private static string GetParametersInsertA(List<ColumnModel> columnList)
        {
            // format
            //     [Platform]
            StringBuilder builder = new StringBuilder(512);
            if (columnList != null && columnList.Count > 0)
            {
                columnList.ForEach(x =>
                {
                    string name = x.COLUMN_NAME.ToUpper();

                    if (x.IS_OUTPUT && name != "MODIFYUSER" && name != "MODIFYTIME")
                    {
                        if (builder.Length > 0)
                        {
                            builder.Append(",");
                            builder.AppendLine();
                        }
                        builder.Append($"            [{x.COLUMN_NAME}]");
                    }
                });
                builder.AppendLine();
            }
            return builder.ToString();
        }

        private static string GetParametersInsertB(List<ColumnModel> columnList)
        {
            // format
            //     @Platform
            StringBuilder builder = new StringBuilder(512);
            if (columnList != null && columnList.Count > 0)
            {
                columnList.ForEach(x =>
                {
                    string name = x.COLUMN_NAME.ToUpper();

                    if (x.IS_OUTPUT && name != "MODIFYUSER" && name != "MODIFYTIME")
                    {
                        if (builder.Length > 0)
                        {
                            builder.Append(",");
                            builder.AppendLine();
                        }
                        builder.Append($"            @{x.COLUMN_NAME}");
                    }
                });
                builder.AppendLine();
            }
            return builder.ToString();
        }

        private static string GetParametersUpdate(List<ColumnModel> columnList)
        {
            // format
            //     [Platform] = @Platform
            StringBuilder builder = new StringBuilder(512);
            if (columnList != null && columnList.Count > 0)
            {
                columnList.ForEach(x =>
                {
                    string name = x.COLUMN_NAME.ToUpper();

                    if (!x.IS_KEY && x.IS_OUTPUT && name != "CREATEUSER" && name != "CREATETIME")
                    {
                        if (builder.Length > 0)
                        {
                            builder.Append(",");
                            builder.AppendLine();
                        }
                        builder.Append($"            [{x.COLUMN_NAME}] = @{x.COLUMN_NAME}");
                    }
                });
                builder.AppendLine();
            }
            return builder.ToString();
        }

        #region SqlColumnModel

        public static SqlColumnModel GetSqlKeyModel(ColumnModel key)
        {
            var result = new SqlColumnModel
            {
                ColumnData = key,
                KeyName = key.COLUMN_NAME,
                KeyDeclare = "@" + key.COLUMN_NAME,
                KeyType = key.DATA_TYPE,
                KeyLength = key.CHARACTER_MAXIMUM_LENGTH ?? 0
            };

            switch (key.DATA_TYPE_SQL)
            {
                case Enums.SqlDataTypeEnum.CHAR:
                case Enums.SqlDataTypeEnum.VARCHAR:
                case Enums.SqlDataTypeEnum.NVARCHAR:
                    if (key.CHARACTER_MAXIMUM_LENGTH == -1)
                    {
                        result.KeyType = $"{key.DATA_TYPE} (MAX)";
                    }
                    else
                    {
                        result.KeyType = $"{key.DATA_TYPE} ({key.CHARACTER_MAXIMUM_LENGTH})";
                    }
                    break;

                default:
                    break;
            }
            return result;
        }

        private static string GetParametersPrimary(SchemaModel sModel, SqlColumnModel key)
        {
            switch (key.ColumnData.DATA_TYPE_SQL)
            {
                case Enums.SqlDataTypeEnum.CHAR:
                case Enums.SqlDataTypeEnum.VARCHAR:
                case Enums.SqlDataTypeEnum.NVARCHAR:
                    return GetPrimaryKeyStr(sModel, key);

                default:
                    return GetPrimaryKeyInt(sModel, key);
            }
        }

        private static string GetPrimaryKeyStr(SchemaModel sModel, SqlColumnModel key)
        {
            string prmWhere = GetParametersWhere2(sModel.KeyList);

            StringBuilder builder = new StringBuilder(512);
            builder.AppendLine($"        DECLARE @N INT;");
            builder.AppendLine();
            builder.AppendLine($"        SELECT ");
            builder.AppendLine($"            @N = CAST(MAX({key.KeyName}) AS INT) + 1 ");
            builder.AppendLine($"        FROM ");
            builder.AppendLine($"            [dbo].[{sModel.TableName}] ");
            builder.AppendLine($"        WHERE ");
            builder.Append(prmWhere);
            builder.AppendLine();
            builder.AppendLine($"        IF (@N IS NULL OR @N = 0) SET @N = 1;");
            builder.AppendLine();
            builder.AppendLine($"        SELECT {key.KeyDeclare} = RIGHT(REPLICATE('0', 6) + CAST(@N as VARCHAR({key.KeyLength})), 6);");
            return builder.ToString();
        }

        private static string GetPrimaryKeyInt(SchemaModel sModel, SqlColumnModel key)
        {
            string prmWhere = GetParametersWhere2(sModel.KeyList);

            StringBuilder builder = new StringBuilder(512);
            builder.AppendLine($"        DECLARE @N INT;");
            builder.AppendLine();
            builder.AppendLine($"        SELECT ");
            builder.AppendLine($"            @N = MAX([{key.KeyName}]) + 1 ");
            builder.AppendLine($"        FROM ");
            builder.AppendLine($"            [dbo].[{sModel.TableName}] ");
            builder.AppendLine($"        WHERE ");
            builder.Append(prmWhere);
            builder.AppendLine();
            builder.AppendLine($"        IF (@N IS NULL OR @N = 0) SET @N = 1;");
            builder.AppendLine();
            builder.AppendLine($"        SELECT {key.KeyDeclare} = @N;");
            return builder.ToString();
        }

        private static string GetParametersWhere2(List<ColumnModel> keyList)
        {
            // format
            //     [Platform] = @Platform
            //     AND [Platform] = @Platform
            StringBuilder builder = new StringBuilder(512);
            if (keyList != null && keyList.Count > 0)
            {
                string part = string.Empty;
                for (int i = 0; i < keyList.Count - 1; i++)
                {
                    ColumnModel x = keyList[i];
                    if (builder.Length > 0)
                    {
                        part = "AND ";
                    }

                    builder.AppendLine($"            {part}[{x.COLUMN_NAME}] = @{x.COLUMN_NAME}");
                }
            }
            return builder.ToString();
        }

        #endregion SqlColumnModel
    }
}