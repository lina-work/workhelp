﻿using System.Text;
using WorkHelp.Wpf.Models;

namespace WorkHelp.Wpf.Services
{
    public class Win401WpfService
    {
        public static void GenerateContent(SchemaModel sModel, EntityModel eModel)
        {
            StringBuilder builder = new StringBuilder(2560);
            AddContents(builder, sModel, eModel);
            sModel.FileContent = builder.ToString();
        }

        private static void AddContents(StringBuilder builder, SchemaModel sModel, EntityModel eModel)
        {
            builder.AppendLine("using NLog;");
            builder.AppendLine("using System;");
            builder.AppendLine("using System.Collections.Generic;");
            builder.AppendLine("using System.Linq;");
            builder.AppendLine($"using {sModel.NameSpaceName}.Errors;");
            builder.AppendLine($"using {sModel.NameSpaceName}.Models;");
            builder.AppendLine($"using {sModel.NameSpaceName}.Services.Data;");
            builder.AppendLine($"using Ticket.Models.{sModel.ControllerName};");
            builder.AppendLine("");
            builder.AppendLine($"namespace {sModel.NameSpaceName}.Services.{sModel.ControllerName}.Impl");
            builder.AppendLine("{");
            builder.AppendLine("    /// <summary>");
            builder.AppendLine($"    /// {sModel.TableDescription}服務實作");
            builder.AppendLine("    /// </summary>");
            builder.AppendLine($"    internal class {sModel.MainName}Service : I{sModel.MainName}Service");
            builder.AppendLine("    {");
            builder.AppendLine("        /// <summary>");
            builder.AppendLine("        /// 日誌");
            builder.AppendLine("        /// </summary>");
            builder.AppendLine("        public Logger Logger => LogManager.GetCurrentClassLogger();");
            builder.AppendLine("");
            builder.AppendLine("        /// <summary>");
            builder.AppendLine("        /// 快取服務");
            builder.AppendLine("        /// </summary>");
            builder.AppendLine("        public ICacheService CacheService { get; set; }");
            builder.AppendLine("");
            builder.AppendLine("        /// <summary>");
            builder.AppendLine("        /// 語系服務");
            builder.AppendLine("        /// </summary>");
            builder.AppendLine("        public ICultureService CultureService { get; set; }");
            builder.AppendLine("");
            builder.AppendLine("        /// <summary>");
            builder.AppendLine($"        /// 取得{sModel.TableDescription}清單");
            builder.AppendLine("        /// </summary>");
            builder.Append(sModel.KeyParamSummary);
            builder.AppendLine("        /// <param name=\"useCache\">先由快取找值</param>");
            builder.AppendLine($"        /// <returns>回傳{sModel.TableDescription}清單</returns>");
            builder.AppendLine($"        public List<{sModel.MainName}Model> List{sModel.SubMain}({sModel.KeyParamDataType}, bool useCache = true)");
            builder.AppendLine("        {");
            builder.AppendLine("            // 參數驗證");
            builder.AppendLine("            InvalidInputExceptionBuilder.Create()");
            builder.Append(sModel.KeyParamValidate);
            builder.AppendLine("                .ThrowIfError();");
            builder.AppendLine("");
            builder.AppendLine("            // 快取 key 值");
            builder.AppendLine($"            string name = $\"{sModel.MainName}#{sModel.KeyParamName}\";");
            builder.AppendLine("");
            builder.AppendLine("            // 由 cache 取值回傳");
            builder.AppendLine("            if (useCache && CacheService.Find(name))");
            builder.AppendLine("            {");
            builder.AppendLine($"                return CacheService.Matchs<List<{sModel.MainName}Model>>(name);");
            builder.AppendLine("            }");
            builder.AppendLine("");
            builder.AppendLine("            // 查詢資料");
            builder.AppendLine($"            var values = RestFactory.Default.List<{sModel.MainName}Model>(resource: $\"{sModel.KeyApiRouteList}\");");
            builder.AppendLine("");
            builder.AppendLine("            if (values != null)");
            builder.AppendLine("            {");
            builder.AppendLine("                // 暫存快取");
            builder.AppendLine("                CacheService.Set(name, values);");
            builder.AppendLine("            }");
            builder.AppendLine("");
            builder.AppendLine("            return values;");
            builder.AppendLine("        }");
            builder.AppendLine("");
            builder.AppendLine("        /// <summary>");
            builder.AppendLine($"        /// 取得{sModel.TableDescription}項目");
            builder.AppendLine("        /// </summary>");
            builder.Append(sModel.KeyParamSummary);
            builder.AppendLine("        /// <param name=\"useCache\">先由快取找值</param>");
            builder.AppendLine($"        /// <returns>回傳{sModel.TableDescription}項目</returns>");
            builder.AppendLine($"        public {sModel.MainName}Model Item{sModel.SubMain}({sModel.KeyParamDataType}, bool useCache = true)");
            builder.AppendLine("        {");
            builder.AppendLine("            // 參數驗證");
            builder.AppendLine("            InvalidInputExceptionBuilder.Create()");
            builder.Append(sModel.KeyParamValidate);
            builder.AppendLine("                .ThrowIfError();");
            builder.AppendLine("");
            builder.AppendLine("            // 快取 key 值");
            builder.AppendLine($"            string name = $\"{sModel.MainName}#{sModel.KeyParamName}\";");
            builder.AppendLine("");
            builder.AppendLine("            // 由 cache 取值回傳");
            builder.AppendLine("            if (useCache && CacheService.Find(name))");
            builder.AppendLine("            {");
            builder.AppendLine($"                return CacheService.Matchs<{sModel.MainName}Model>(name);");
            builder.AppendLine("            }");
            builder.AppendLine("");
            builder.AppendLine("            // 查詢資料");
            builder.AppendLine($"            var value = RestFactory.Default.Item<{sModel.MainName}Model>(resource: $\"{sModel.KeyApiRouteItem}\");");
            builder.AppendLine("");
            builder.AppendLine("            if (value != null)");
            builder.AppendLine("            {");
            builder.AppendLine("                // 暫存快取");
            builder.AppendLine("                CacheService.Set(name, value);");
            builder.AppendLine("            }");
            builder.AppendLine("");
            builder.AppendLine("            return value;");
            builder.AppendLine("        }");
            builder.AppendLine("");

            builder.AppendLine("        /// <summary>");
            builder.AppendLine($"        /// 儲存{sModel.TableDescription}");
            builder.AppendLine("        /// </summary>");
            builder.AppendLine($"        /// <param name=\"list\">要儲存的{sModel.TableDescription}內容</param>");
            builder.AppendLine($"        public bool Save{sModel.SubMain}(params {sModel.MainName}Model[] list)");
            builder.AppendLine("        {");
            builder.AppendLine("            if (!list.Any()) return false;");
            builder.AppendLine("");
            builder.AppendLine($"            return RestFactory.Default.Execute(resource: $\"{sModel.KeyApiRouteSave}\", addParameter: (request) =>");
            builder.AppendLine("            {");
            builder.AppendLine("                request.AddJsonBody(list);");
            builder.AppendLine("            });");

            builder.AppendLine("        }");
            builder.AppendLine("");
            builder.AppendLine("        /// <summary>");
            builder.AppendLine($"        /// 刪除{sModel.TableDescription}");
            builder.AppendLine("        /// </summary>");
            builder.AppendLine($"        /// <param name=\"list\">要刪除的{sModel.TableDescription}內容</param>");
            builder.AppendLine($"        public bool Remove{sModel.SubMain}(params {sModel.MainName}Model[] list)");
            builder.AppendLine("        {");
            builder.AppendLine("            if (!list.Any()) return false;");
            builder.AppendLine("");
            builder.AppendLine($"            return RestFactory.Default.Execute(resource: $\"{sModel.KeyApiRouteRemove}\", addParameter: (request) =>");
            builder.AppendLine("            {");
            builder.AppendLine("                request.AddJsonBody(list);");
            builder.AppendLine("            });");
            builder.AppendLine("        }");
            builder.AppendLine("    }");
            builder.AppendLine("}");
        }
    }
}