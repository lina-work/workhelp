﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using WorkHelp.Wpf.Models;

namespace WorkHelp.Wpf.Services
{
    public class Win401SqlService_Aras
    {
        public static void GenerateContent(SchemaModel sModel, EntityModel eModel)
        {
            StringBuilder builder = new StringBuilder(2560);
            AddContents(builder, sModel, eModel);
            sModel.FileContent = builder.ToString();
        }

        private static void AddContents(StringBuilder builder, SchemaModel sModel, EntityModel eModel)
        {
            string[] sysColums = new string[] 
            {
"classification",
"config_id",
"created_by_id",
"created_on",
"css",
"current_state",
"generation",
"id",
"in_company",
"is_current",
"is_released",
"keyed_name",
"locked_by_id",
"major_rev",
"managed_by_id",
"minor_rev",
"modified_by_id",
"modified_on",
"new_version",
"not_lockable",
"owned_by_id",
"permission_id",
"state",
"team_id"
            };

            string columns = GetColumns(sModel.ColumnList, sysColums);

            builder.AppendLine($"SELECT");
            builder.AppendLine($"    TOP 10");
            builder.AppendLine($"    [id],");
            builder.Append(columns);
            builder.AppendLine($"FROM");
            builder.AppendLine($"    [Innovator].[{sModel.TableName}]");
        }

        private static string GetColumns(List<ColumnModel> columnList, string[] sysColums)
        {
            StringBuilder builder = new StringBuilder(512);
            if (columnList != null && columnList.Count > 0)
            {
                columnList.ForEach(x =>
                {
                    string name = x.COLUMN_NAME.ToLower();

                    if (!sysColums.Contains(name))
                    {
                        if (builder.Length > 0)
                        {
                            builder.Append(",");
                            builder.AppendLine();
                        }
                        builder.Append($"    [{name}]");
                    }
                });
                builder.AppendLine();
            }
            return builder.ToString();
        }
    }
}