﻿using System.Collections.Generic;
using System.Text;
using WorkHelp.Wpf.Models;

namespace WorkHelp.Wpf.Services
{
    public class Win401MapperService
    {
        public static void GenerateContent(SchemaModel sModel, EntityModel eModel)
        {
            StringBuilder builder = new StringBuilder(2560);
            AddContents(builder, sModel, eModel);
            sModel.FileContent = builder.ToString();
        }

        private static void AddContents(StringBuilder builder, SchemaModel sModel, EntityModel eModel)
        {
            builder.AppendLine("using Google.Protobuf.WellKnownTypes;");
            builder.AppendLine($"using {sModel.NameSpaceName}.Entities.{sModel.ControllerName};");
            builder.AppendLine($"using Ticket.Models.{sModel.ControllerName};");
            builder.AppendLine();

            builder.AppendLine($"namespace Ticket.Api.Mappers.{sModel.ControllerName}");
            builder.AppendLine("{");
            builder.AppendLine("    /// <summary>");
            builder.AppendLine("    /// <see cref=\"" + sModel.MainName + "\"/> 的轉換功能");
            builder.AppendLine("    /// </summary>");
            builder.AppendLine($"    public static class {sModel.MainName}Mapper");
            builder.AppendLine("    {");
            builder.AppendLine("         /// <summary>");
            builder.AppendLine($"         /// <see cref=\"{sModel.MainName}Model\"/> 轉換為 <see cref=\"{sModel.MainName}\"/>");
            builder.AppendLine("         /// </summary>");
            builder.AppendLine("         /// <param name=\"model\"></param>");
            builder.AppendLine("         /// <returns></returns>");
            builder.AppendLine($"         public static {sModel.MainName} AsEntity(this {sModel.MainName}Model model)");
            builder.AppendLine("         {");
            builder.AppendLine($"             return new {sModel.MainName}");
            builder.AppendLine("             {");

            ColumnList1(builder, sModel.ColumnList);

            builder.AppendLine("             };");
            builder.AppendLine("         }");
            builder.AppendLine();
            builder.AppendLine("         /// <summary>");
            builder.AppendLine($"         /// <see cref=\"{sModel.MainName}\"/> 轉換為 <see cref=\"{sModel.MainName}Model\"/>");
            builder.AppendLine("         /// </summary>");
            builder.AppendLine("         /// <param name=\"entity\"></param>");
            builder.AppendLine("         /// <returns></returns>");
            builder.AppendLine($"         public static {sModel.MainName}Model AsModel(this {sModel.MainName} entity)");
            builder.AppendLine("         {");
            builder.AppendLine($"             return new {sModel.MainName}Model");
            builder.AppendLine("             {");

            ColumnList2(builder, sModel.ColumnList);

            builder.AppendLine("             };");
            builder.AppendLine("         }");
            builder.AppendLine("    }");
            builder.AppendLine("}");
        }

        private static void ColumnList1(StringBuilder builder, List<ColumnModel> list)
        {
            list.ForEach(column =>
            {
                if (column.IS_OUTPUT)
                {
                    switch (column.DATA_TYPE_CSHARP)
                    {
                        case Enums.CSharpDataTypeEnum.DATETIME:
                        case Enums.CSharpDataTypeEnum.DATETIME_NULL:
                        case Enums.CSharpDataTypeEnum.DATETIMEOFFSET:
                        case Enums.CSharpDataTypeEnum.DATETIMEOFFSET_NULL:
                            builder.AppendLine($"                 {column.COLUMN_NAME_ENTITY} = model.{column.COLUMN_NAME_MODEL}?.ToDateTimeOffset(),");
                            break;

                        case Enums.CSharpDataTypeEnum.BOOLEAN:
                        case Enums.CSharpDataTypeEnum.BOOLEAN_NULL:
                        case Enums.CSharpDataTypeEnum.INT16:
                        case Enums.CSharpDataTypeEnum.INT16_NULL:
                        case Enums.CSharpDataTypeEnum.INT32:
                        case Enums.CSharpDataTypeEnum.INT32_NULL:
                        case Enums.CSharpDataTypeEnum.INT64:
                        case Enums.CSharpDataTypeEnum.INT64_NULL:
                        case Enums.CSharpDataTypeEnum.STRING:
                        case Enums.CSharpDataTypeEnum.SINGLE:
                        case Enums.CSharpDataTypeEnum.DOUBLE:
                        case Enums.CSharpDataTypeEnum.DOUBLE_NULL:
                        case Enums.CSharpDataTypeEnum.DECIMAL:
                        case Enums.CSharpDataTypeEnum.DECIMAL_NULL:
                        case Enums.CSharpDataTypeEnum.BYTE:
                        case Enums.CSharpDataTypeEnum.BYTE_NULL:
                        case Enums.CSharpDataTypeEnum.BYTEARRAY:
                        case Enums.CSharpDataTypeEnum.OBJECT:
                        case Enums.CSharpDataTypeEnum.SQLGEOGRAPHY:
                        case Enums.CSharpDataTypeEnum.SQLGEOMETRY:
                        case Enums.CSharpDataTypeEnum.SQLHIERARCHYID:
                        case Enums.CSharpDataTypeEnum.GUID:
                        default:
                            builder.AppendLine($"                 {column.COLUMN_NAME_ENTITY} = model.{column.COLUMN_NAME_MODEL},");
                            break;
                    }
                }
            });
        }

        private static void ColumnList2(StringBuilder builder, List<ColumnModel> list)
        {
            list.ForEach(column =>
            {
                if (column.IS_OUTPUT)
                {
                    switch (column.DATA_TYPE_CSHARP)
                    {
                        case Enums.CSharpDataTypeEnum.DATETIME:
                        case Enums.CSharpDataTypeEnum.DATETIME_NULL:
                        case Enums.CSharpDataTypeEnum.DATETIMEOFFSET:
                        case Enums.CSharpDataTypeEnum.DATETIMEOFFSET_NULL:
                            builder.AppendLine($"                 {column.COLUMN_NAME_MODEL} = entity.{column.COLUMN_NAME_ENTITY}?.ToTimestamp(),");
                            break;

                        case Enums.CSharpDataTypeEnum.BOOLEAN:
                        case Enums.CSharpDataTypeEnum.BOOLEAN_NULL:
                        case Enums.CSharpDataTypeEnum.INT16:
                        case Enums.CSharpDataTypeEnum.INT16_NULL:
                        case Enums.CSharpDataTypeEnum.INT32:
                        case Enums.CSharpDataTypeEnum.INT32_NULL:
                        case Enums.CSharpDataTypeEnum.INT64:
                        case Enums.CSharpDataTypeEnum.INT64_NULL:
                        case Enums.CSharpDataTypeEnum.STRING:
                        case Enums.CSharpDataTypeEnum.SINGLE:
                        case Enums.CSharpDataTypeEnum.DOUBLE:
                        case Enums.CSharpDataTypeEnum.DOUBLE_NULL:
                        case Enums.CSharpDataTypeEnum.DECIMAL:
                        case Enums.CSharpDataTypeEnum.DECIMAL_NULL:
                        case Enums.CSharpDataTypeEnum.BYTE:
                        case Enums.CSharpDataTypeEnum.BYTE_NULL:
                        case Enums.CSharpDataTypeEnum.BYTEARRAY:
                        case Enums.CSharpDataTypeEnum.OBJECT:
                        case Enums.CSharpDataTypeEnum.SQLGEOGRAPHY:
                        case Enums.CSharpDataTypeEnum.SQLGEOMETRY:
                        case Enums.CSharpDataTypeEnum.SQLHIERARCHYID:
                        case Enums.CSharpDataTypeEnum.GUID:
                        default:
                            builder.AppendLine($"                 {column.COLUMN_NAME_MODEL} = entity.{column.COLUMN_NAME_ENTITY},");
                            break;
                    }
                }
            });
        }
    }
}