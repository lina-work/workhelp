﻿using WorkHelp.Wpf.Models.Windows;

namespace WorkHelp.Wpf.Services
{
    public class ServiceUtility
    {

        /// <summary>
        /// 取得 master 連線字串
        /// </summary>
        public static string MasterConnectionString(Config102 config)
        {
            return GetConnectionString(config.ServerName, "master", config.UserId, config.Password);
        }

        /// <summary>
        /// 取得 master 連線字串
        /// </summary>
        public static string MasterConnectionString(Config411 config)
        {
            return GetConnectionString(config.ServerName, "master", config.UserId, config.Password);
        }

        /// <summary>
        /// 取得 master 連線字串
        /// </summary>
        public static string MasterConnectionString(Config412 config)
        {
            return GetConnectionString(config.ServerName, "master", config.UserId, config.Password);
        }

        /// <summary>
        /// 取得資料庫連線字串
        /// </summary>
        public static string GetConnectionString(Config102 config)
        {
            return GetConnectionString(config.ServerName, config.DatabaseName, config.UserId, config.Password);
        }

        /// <summary>
        /// 取得資料庫連線字串
        /// </summary>
        public static string GetConnectionString(Config411 config)
        {
            return GetConnectionString(config.ServerName, config.DatabaseName, config.UserId, config.Password);
        }

        /// <summary>
        /// 取得資料庫連線字串
        /// </summary>
        public static string GetConnectionString(Config412 config)
        {
            return GetConnectionString(config.ServerName, config.DatabaseName, config.UserId, config.Password);
        }

        /// <summary>
        /// 取得資料庫連線字串
        /// </summary>
        public static string GetArasConnectionString(Config412 config, string db)
        {
            return GetConnectionString(config.ServerName, db, config.UserId, config.Password);
        }

        /// <summary>
        /// 取得連線字串
        /// </summary>
        private static string GetConnectionString(string svr, string db, string uid, string pwd)
        {
            return $"SERVER={svr};DATABASE={db};UID={uid};PWD={pwd}";
        }
    }
}
