﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using WorkHelp.Wpf.Models.Main;

namespace WorkHelp.Wpf.Services
{
    /// <summary>
    /// Win502 服務
    /// </summary>
    public class Win502Service
    {
        /// <summary>
        /// 建構元
        /// </summary>
        protected Win502Service() { }

        /// <summary>
        /// 相片資料夾
        /// </summary>
        /// <returns></returns>
        public static List<MenuNode> GetMenuNodes(string dir, bool isAppendImage = true)
        {
            List<MenuNode> result = new List<MenuNode>();

            if (Directory.Exists(dir))
            {
                DirectoryInfo rootDir = new DirectoryInfo(dir);

                MenuNode root = new MenuNode
                {
                    Id = Guid.NewGuid().ToString(),
                    Name = rootDir.Name,
                };

                root.Members = new List<MenuNodeMember>();
                try
                {
                    // 會因權限不足發生錯誤
                    AppendFolders(root, rootDir.GetDirectories());
                    if (isAppendImage)
                    {
                        AppendImages(root, rootDir.GetFiles());
                    }
                    else
                    {
                        AppendXmls(root, rootDir.GetFiles());
                    }
                }
                catch (Exception ex)
                {
                    WorkHelp.Logging.TLog.Watch(message: ex.ToString());
                }

                result.Add(root);
            }

            return result;
        }

        /// <summary>
        /// 附加資料夾節點
        /// </summary>
        private static void AppendFolders(MenuNode node, DirectoryInfo[] dirs)
        {
            if (dirs != null && dirs.Length > 0)
            {
                foreach (DirectoryInfo dir in dirs)
                {
                    MenuNodeMember member = new MenuNodeMember
                    {
                        ParentId = node.Id,
                        Id = Guid.NewGuid().ToString(),
                        Name = dir.Name,
                        FullName = dir.FullName,
                        IsFolder = true,
                        Photo = "/WorkHelp.Wpf;component/Images/folder.png"
                    };

                    node.Members.Add(member);
                }
            }
        }

        private static string[] ImageExts = new[] { "jpg", "png", "bmp", "jpeg", "gif" };

        /// <summary>
        /// 附加圖片節點
        /// </summary>
        private static void AppendImages(MenuNode node, FileInfo[] files)
        {
            if (files != null && files.Length > 0)
            {
                foreach (FileInfo file in files)
                {
                    if (FilterFiles(ImageExts, file.Name))
                    {
                        MenuNodeMember member = new MenuNodeMember
                        {
                            ParentId = node.Id,
                            Id = Guid.NewGuid().ToString(),
                            Name = file.Name,
                            FullName = file.FullName,
                            IsFolder = false,
                            Photo = "/WorkHelp.Wpf;component/Images/icon-picture.png"
                        };

                        node.Members.Add(member);
                    }
                }
            }
        }

        /// <summary>
        /// 附加 XML 檔案節點
        /// </summary>
        private static void AppendXmls(MenuNode node, FileInfo[] files)
        {
            if (files != null && files.Length > 0)
            {
                foreach (FileInfo file in files)
                {
                    MenuNodeMember member = new MenuNodeMember
                    {
                        ParentId = node.Id,
                        Id = Guid.NewGuid().ToString(),
                        Name = file.Name,
                        FullName = file.FullName,
                        IsFolder = false,
                        Photo = "/WorkHelp.Wpf;component/Images/icon-picture.png"
                    };

                    node.Members.Add(member);
                }
            }
        }

        /// <summary>
        /// 過濾副檔名
        /// </summary>
        private static bool FilterFiles(string[] imageExts, string path)
        {
            return imageExts.Any(x => path.EndsWith(x, StringComparison.OrdinalIgnoreCase));
        }
    }
}