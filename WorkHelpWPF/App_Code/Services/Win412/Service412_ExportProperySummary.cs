﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkHelp.Utilities;
using WorkHelp.Wpf.Models.Aras;
using WorkHelp.Wpf.Models.Windows;
using WorkHelp.Wpf.Repository;
using WorkHelp.Wpf.Services.Win411;

namespace WorkHelp.Wpf.Services.Win412
{
    public partial class Service412
    {
        public static void ExportProperySummary(Config412 config, Storage storage)
        {
            var list = Repository411.GetPropertyList<PropertyInstance>(config.KeyedName, storage.ConnectionString);

            if (list == null || list.Count == 0)
            {
                storage.Message = "查無欄位";
            }
            else
            {
                storage.Message = $"共 {list.Count} 個欄位";
            }

            foreach (var item in list)
            {
                storage.FolderName = $"{config.FolderName}\\{storage.DatabaseName}\\Property";
                storage.FileName = $"{config.KeyedName}.cs";
                storage.FileContent = Win411Service.ReportPropertyContents(storage.DatabaseName, config.KeyedName, list);

                FileUtility.WriteFile(storage.FolderName, storage.FileName, storage.FileContent);
            }
        }

    }
}
