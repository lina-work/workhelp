﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkHelp.Utilities;
using WorkHelp.Wpf.Models.Aras;
using WorkHelp.Wpf.Models.Windows;
using WorkHelp.Wpf.Repository;

namespace WorkHelp.Wpf.Services.Win412
{
    public partial class Service412
    {
        public static void QueryRunningMeeting(Config412 config, Storage storage)
        {
            var list = Repository412.GetMeetingList(storage.ConnectionString);

            if (list == null || list.Count == 0)
            {
                storage.Message = "無";
            }
            else
            {
                storage.Message = $"共 {list.Count} 筆";
            }

            StringBuilder builder = new StringBuilder(512);

            builder.AppendLine();

            string format = "yyyy/MM/dd";

            foreach (var item in list)
            {
                builder.AppendLine($"        - 賽事名稱: `{item.in_title}`");
                builder.AppendLine($"            - 賽事編號: {item.id}");
                builder.AppendLine($"            - 賽事開始: {item.in_date_s.ToString(format)}");
                builder.AppendLine($"            - 賽事結束: {item.in_date_e.ToString(format)}");
                builder.AppendLine($"            - 報名開始日: {item.in_state_time_start.ToString(format)}");
                builder.AppendLine($"            - 報名截止日: `{item.in_state_time_end.ToString(format)}`");
                builder.AppendLine($"            - 專屬展示網址: {item.in_url}");
                builder.AppendLine($"            - 專屬報名網址: `{item.in_register_url}`");
                builder.AppendLine($"            - 正取人數: {item.in_taking}");
                builder.AppendLine($"            - 實際正取: {item.in_real_taking}");
            }

            storage.Message += builder.ToString();
        }
    }
}
