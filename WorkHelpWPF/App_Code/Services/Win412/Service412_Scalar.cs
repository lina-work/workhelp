﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkHelp.Utilities;
using WorkHelp.Wpf.Models.Aras;
using WorkHelp.Wpf.Models.Windows;
using WorkHelp.Wpf.Repository;

namespace WorkHelp.Wpf.Services.Win412
{
    public partial class Service412
    {
        public static void Scalar(Config412 config, Storage storage)
        {
            storage.Message = Repository412.Scalar(config.KeyedName, storage.ConnectionString);
        }
    }
}
