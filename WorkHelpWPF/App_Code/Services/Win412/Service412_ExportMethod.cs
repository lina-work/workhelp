﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkHelp.Utilities;
using WorkHelp.Wpf.Models.Aras;
using WorkHelp.Wpf.Models.Windows;
using WorkHelp.Wpf.Repository;

namespace WorkHelp.Wpf.Services.Win412
{
    public partial class Service412
    {
        public static void ExportMethod(Config412 config, Storage storage)
        {
            var methodlist = Repository412.GetAllMethodTypeList(config.KeyedName, storage.ConnectionString);

            if (methodlist == null || methodlist.Count == 0)
            {
                storage.Message = "不存在";
            }
            else
            {
                storage.Message = $"共 {methodlist.Count} 個";
            }

            foreach (var method in methodlist)
            {
                var code = string.IsNullOrWhiteSpace(method.METHOD_CODE) ? "" : method.METHOD_CODE;

                StringBuilder builder = new StringBuilder(method.METHOD_CODE.Length + 256);
                builder.AppendLine($"Method Name: {method.NAME}");
                builder.AppendLine($"Method Comments: {method.COMMENTS}");
                builder.AppendLine();
                builder.Append(code);

                storage.FolderName = $"{config.FolderName}\\{storage.DatabaseName}\\Method";
                storage.FileName = $"{method.NAME}.cs";
                storage.FileContent = builder.ToString();

                FileUtility.WriteFile(storage.FolderName, storage.FileName, storage.FileContent);
            }
        }
    }
}
