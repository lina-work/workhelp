﻿using DocumentFormat.OpenXml.Presentation;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TemplateEngine.Docx;
using WorkHelp.Wpf.Enums.Extends;
using WorkHelp.Wpf.Models.Aras;
using WorkHelp.Wpf.Models.Windows;

namespace WorkHelp.Wpf.Services.Win412
{
    public partial class Service412
    {
        public static string Main(Config412 config, Action<Config412, Storage> targetAction)
        {
            config.ExecuteTimeStart = DateTime.Now;
            config.IsExcuted = false;

            StringBuilder builderSub = new StringBuilder(1024);
            foreach (var storage in config.StorageList)
            {
                targetAction(config, storage);
                builderSub.AppendLine($"    - {storage.DatabaseName}{GetDatabaseDesc(storage.DatabaseName)}: {storage.Message}");
            }

            config.ExecuteTimeEnd = DateTime.Now;
            config.ExecuteTimeSpan = config.ExecuteTimeEnd - config.ExecuteTimeStart;
            config.IsExcuted = true;

            StringBuilder builder = new StringBuilder(1024);
            builder.AppendLine($"伺服器: `{config.ServerName}`");
            builder.AppendLine($"類型: {config.ArasType}");
            builder.AppendLine($"功能: {config.ArasTypeFunction}({ArasTypeFunctionExtend.GetMenuName(config.ArasTypeFunction)})");
            
            if (!string.IsNullOrWhiteSpace(config.KeyedName))
            { 
                builder.AppendLine($"鍵名: `{config.KeyedName}`");
            }

            builder.AppendLine($"執行時間: {config.ExecuteTimeStart}");
            builder.AppendLine($"執行秒數: {config.ExecuteTimeSpan.TotalSeconds}");
            builder.AppendLine();
            builder.AppendLine("- 資料庫清單");

            builder.Append(builderSub);

            return builder.ToString();
        }

        private static string GetDatabaseDesc(string db)
        {
            switch (db.ToLower())
            {
                case "act": return "(跆拳道)";
                case "cta": return "(中華民國跆拳道協會)";
                case "taekwondo": return "(跆拳道 UTC)";

                case "judo": return "(柔道)";
                case "twjudo": return "(台灣柔道)";
                case "tjjf": return "(柔術)";

                case "badminton": return "(羽球)";
                case "rowing": return "(划船)";
                case "ctoa": return "(定向越野)";

                case "drbb": return "(籃球)";
                case "cta_china": return "(china 跆拳道)";
                    
                case "wrestling": return "(角力)";
                case "boxing": return "(拳擊)"; 
                case "tabletennis": return "(桌球)"; 

                case "funplanet": return "(天悅-Fun電星球)";

                case "cta_test": return "(合併版)";

                default: return "(未定義)";

            }
        }

        public static void ExportDocx()
        {
            var fillContent = new Content();

            RepeatContent repeatContent = new RepeatContent("@repeat");

            DateTime dt = DateTime.Today;
            string chineseDay = $"{dt.Year - 1911}年{dt.Month}月{dt.Day}日"; 

            for (int i = 0; i < 2; i++)
            {
                List<IContentItem>  fields = new List<IContentItem>();
                fields.Add(new FieldContent("@title1", $"新北市三重高級中學"));
                fields.Add(new FieldContent("@itemNumber1", $"IMP-00004686"));
                fields.Add(new FieldContent("@currentOrg1", $"新北市三重高級中學"));
                fields.Add(new FieldContent("@chineseDate1", chineseDay));
                fields.Add(new FieldContent("@summary1", $"110年全國春季室內划船錦標賽報名費"));
                fields.Add(new FieldContent("@expense1", $"900"));
                fields.Add(new FieldContent("@chineseExpense1", $"玖佰元"));
                fields.Add(new FieldContent("@caseNumber1", $"0910008475"));
                fields.Add(new FieldContent("@companyNumber1", $"92019300"));
                fields.Add(new FieldContent("@companyAddress1", $"114台北市中山區朱崙街20號707室"));
                fields.Add(new FieldContent("@companyTel1", $"(02)2773-5755"));
                fields.Add(new FieldContent("@companyFax1", $"(02)2773-5756"));

                fields.Add(new FieldContent("@title2", $"新北市三重高級中學"));
                fields.Add(new FieldContent("@itemNumber2", $"IMP-00004686"));
                fields.Add(new FieldContent("@currentOrg2", $"新北市三重高級中學"));
                fields.Add(new FieldContent("@chineseDate2", chineseDay));
                fields.Add(new FieldContent("@summary2", $"110年全國春季室內划船錦標賽報名費"));
                fields.Add(new FieldContent("@expense2", $"900"));
                fields.Add(new FieldContent("@chineseExpense2", $"玖佰元"));
                fields.Add(new FieldContent("@caseNumber2", $"0910008475"));
                fields.Add(new FieldContent("@companyNumber2", $"92019300"));
                fields.Add(new FieldContent("@companyAddress2", $"114台北市中山區朱崙街20號707室"));
                fields.Add(new FieldContent("@companyTel2", $"(02)2773-5755"));
                fields.Add(new FieldContent("@companyFax2", $"(02)2773-5756"));

                repeatContent.AddItem(fields.ToArray());
            }
            

            fillContent.Repeats.Add(repeatContent);

            var sourceFileName = @"D:\source\lina repos\workhelp\WorkHelpWPF\Samples\Win41200\receipt-template-20200525.docx";
            var destFileName = @"D:\source\lina repos\workhelp\WorkHelpWPF\Samples\Win41200\output-template-20200525.docx";
            System.IO.File.Copy(sourceFileName, destFileName, true);

            //將Content丟給TemplateProcessor處理，SetRemoveContentControls=true表示要清除標籤內文字，如不要清除則設為false
            using (var outputDocument = new TemplateProcessor(destFileName).SetRemoveContentControls(true))
            {
                outputDocument.FillContent(fillContent);
                outputDocument.SaveChanges(); // 儲存變更檔案
            }
        }

    }
}
