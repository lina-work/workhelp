﻿using System.Collections.Generic;
using System.Text;
using WorkHelp.Wpf.Models;

namespace WorkHelp.Wpf.Services
{
    public class Win405EntityService
    {
        public static void GenerateContent(SchemaModel sModel, EntityModel eModel)
        {
            StringBuilder builder = new StringBuilder(2560);
            AddContents(builder, sModel, eModel);
            sModel.FileContent = builder.ToString();
        }

        private static void AddContents(StringBuilder builder, SchemaModel sModel, EntityModel eModel)
        {
            builder.AppendLine("using System;");
            builder.AppendLine("using System.ComponentModel.DataAnnotations;");
            builder.AppendLine("using System.ComponentModel.DataAnnotations.Schema;");
            builder.AppendLine();
            builder.AppendLine($"namespace {sModel.NameSpaceName}.Entities.{sModel.ControllerName}");
            builder.AppendLine("{");
            builder.AppendLine($"    /// <summary>");
            builder.AppendLine($"    /// {sModel.TableDescription}");
            builder.AppendLine($"    /// </summary>");
            builder.AppendLine($"    [Table(\"{sModel.TableName}\", Schema = \"{sModel.TableSchema}\")]");
            builder.AppendLine($"    public partial class {sModel.MainName}");
            builder.AppendLine("    {");

            ColumnList(builder, sModel.ColumnList);

            builder.AppendLine("    }");
            builder.AppendLine("}");
        }

        private static void ColumnList(StringBuilder builder, List<ColumnModel> list)
        {
            list.ForEach(column =>
            {
                if (column.IS_OUTPUT)
                {
                    SetColumnBefore(builder, column);
                    switch (column.DATA_TYPE_CSHARP)
                    {
                        case Enums.CSharpDataTypeEnum.BOOLEAN:
                            BoolValue(builder, column);
                            break;

                        case Enums.CSharpDataTypeEnum.BOOLEAN_NULL:
                            BoolNullValue(builder, column);
                            break;

                        case Enums.CSharpDataTypeEnum.DATETIME:
                            DateTimeValue(builder, column);
                            break;

                        case Enums.CSharpDataTypeEnum.DATETIME_NULL:
                            DateTimeNullValue(builder, column);
                            break;

                        case Enums.CSharpDataTypeEnum.DATETIMEOFFSET:
                            DateTimeOffsetValue(builder, column);
                            break;

                        case Enums.CSharpDataTypeEnum.DATETIMEOFFSET_NULL:
                            DateTimeOffsetNullValue(builder, column);
                            break;

                        case Enums.CSharpDataTypeEnum.INT16:
                            Int16Value(builder, column);
                            break;

                        case Enums.CSharpDataTypeEnum.INT16_NULL:
                            Int16NullValue(builder, column);
                            break;

                        case Enums.CSharpDataTypeEnum.INT32:
                            Int32Value(builder, column);
                            break;

                        case Enums.CSharpDataTypeEnum.INT32_NULL:
                            Int32NullValue(builder, column);
                            break;

                        case Enums.CSharpDataTypeEnum.INT64:
                            Int64Value(builder, column);
                            break;

                        case Enums.CSharpDataTypeEnum.INT64_NULL:
                            Int64NullValue(builder, column);
                            break;

                        case Enums.CSharpDataTypeEnum.STRING:
                            StringValue(builder, column);
                            break;

                        case Enums.CSharpDataTypeEnum.SINGLE:
                            DoubleValue(builder, column);
                            break;

                        case Enums.CSharpDataTypeEnum.DOUBLE:
                            DoubleNullValue(builder, column);
                            break;

                        case Enums.CSharpDataTypeEnum.DOUBLE_NULL:
                            DoubleNullValue(builder, column);
                            break;

                        case Enums.CSharpDataTypeEnum.DECIMAL:
                            DecimalValue(builder, column);
                            break;

                        case Enums.CSharpDataTypeEnum.DECIMAL_NULL:
                            DecimalNullValue(builder, column);
                            break;

                        case Enums.CSharpDataTypeEnum.BYTE:
                            ByteValue(builder, column);
                            break;

                        case Enums.CSharpDataTypeEnum.BYTE_NULL:
                            ByteNullValue(builder, column);
                            break;

                        case Enums.CSharpDataTypeEnum.BYTEARRAY:
                            ByteArrayValue(builder, column);
                            break;

                        case Enums.CSharpDataTypeEnum.OBJECT:
                            break;

                        case Enums.CSharpDataTypeEnum.SQLGEOGRAPHY:
                            break;

                        case Enums.CSharpDataTypeEnum.SQLGEOMETRY:
                            break;

                        case Enums.CSharpDataTypeEnum.SQLHIERARCHYID:
                            break;

                        case Enums.CSharpDataTypeEnum.TIMESTAMP:
                            TimestampValue(builder, column);
                            break;

                        case Enums.CSharpDataTypeEnum.GUID:
                            GUIDValue(builder, column);
                            break;
                    }
                    SetColumnAfter(builder, column);
                }
            });
        }

        private static void SetColumnBefore(StringBuilder builder, ColumnModel column)
        {
            builder.AppendLine("        /// <summary>");
            builder.AppendLine("        /// " + GetColumnDescription(column));
            builder.AppendLine("        /// </summary>");

            if (column.IS_KEY)
            {
                builder.AppendLine("        [Key]");
            }
        }

        private static void SetColumnMiddle(StringBuilder builder, ColumnModel column)
        {
            builder.AppendLine($"        [Column(\"{column.COLUMN_NAME}\")]");
        }

        private static void SetColumnAfter(StringBuilder builder, ColumnModel column)
        {
            builder.AppendLine();
        }

        #region DataType

        private static void StringValue(StringBuilder builder, ColumnModel column)
        {
            if (column.CHARACTER_MAXIMUM_LENGTH == -1)
            {
                builder.AppendLine("        [MaxLength]");
            }
            else
            {
                builder.AppendLine($"        [StringLength({column.CHARACTER_MAXIMUM_LENGTH})]");
            }

            SetColumnMiddle(builder, column);
            builder.AppendLine("        public string " + GetColumnName(column) + " { get; set; }");
        }

        private static void BoolValue(StringBuilder builder, ColumnModel column)
        {
            SetColumnMiddle(builder, column);
            builder.AppendLine("        public bool " + GetColumnName(column) + " { get; set; }");
        }

        private static void BoolNullValue(StringBuilder builder, ColumnModel column)
        {
            SetColumnMiddle(builder, column);
            builder.AppendLine("        public bool? " + GetColumnName(column) + " { get; set; }");
        }

        private static void Int16Value(StringBuilder builder, ColumnModel column)
        {
            SetColumnMiddle(builder, column);
            builder.AppendLine("        public short " + GetColumnName(column) + " { get; set; }");
        }

        private static void Int16NullValue(StringBuilder builder, ColumnModel column)
        {
            SetColumnMiddle(builder, column);
            builder.AppendLine("        public short? " + GetColumnName(column) + " { get; set; }");
        }

        private static void Int32Value(StringBuilder builder, ColumnModel column)
        {
            SetColumnMiddle(builder, column);
            builder.AppendLine("        public int " + GetColumnName(column) + " { get; set; }");
        }

        private static void Int32NullValue(StringBuilder builder, ColumnModel column)
        {
            SetColumnMiddle(builder, column);
            builder.AppendLine("        public int? " + GetColumnName(column) + " { get; set; }");
        }

        private static void Int64Value(StringBuilder builder, ColumnModel column)
        {
            SetColumnMiddle(builder, column);
            builder.AppendLine("        public long " + GetColumnName(column) + " { get; set; }");
        }

        private static void Int64NullValue(StringBuilder builder, ColumnModel column)
        {
            SetColumnMiddle(builder, column);
            builder.AppendLine("        public long? " + GetColumnName(column) + " { get; set; }");
        }

        private static void DateTimeValue(StringBuilder builder, ColumnModel column)
        {
            SetColumnMiddle(builder, column);
            builder.AppendLine("        public DateTime " + GetColumnName(column) + " { get; set; }");
        }

        private static void DateTimeNullValue(StringBuilder builder, ColumnModel column)
        {
            SetColumnMiddle(builder, column);
            builder.AppendLine("        public DateTime? " + GetColumnName(column) + " { get; set; }");
        }

        private static void DateTimeOffsetValue(StringBuilder builder, ColumnModel column)
        {
            SetColumnMiddle(builder, column);
            builder.AppendLine("        public DateTimeOffset " + GetColumnName(column) + " { get; set; }");
        }

        private static void DateTimeOffsetNullValue(StringBuilder builder, ColumnModel column)
        {
            SetColumnMiddle(builder, column);
            builder.AppendLine("        public DateTimeOffset? " + GetColumnName(column) + " { get; set; }");
        }

        private static void DoubleValue(StringBuilder builder, ColumnModel column)
        {
            SetColumnMiddle(builder, column);
            builder.AppendLine("        public double " + GetColumnName(column) + " { get; set; }");
        }

        private static void DoubleNullValue(StringBuilder builder, ColumnModel column)
        {
            SetColumnMiddle(builder, column);
            builder.AppendLine("        public double? " + GetColumnName(column) + " { get; set; }");
        }

        private static void DecimalValue(StringBuilder builder, ColumnModel column)
        {
            SetColumnMiddle(builder, column);
            builder.AppendLine("        public decimal " + GetColumnName(column) + " { get; set; }");
        }

        private static void DecimalNullValue(StringBuilder builder, ColumnModel column)
        {
            SetColumnMiddle(builder, column);
            builder.AppendLine("        public decimal? " + GetColumnName(column) + " { get; set; }");
        }

        private static void ByteValue(StringBuilder builder, ColumnModel column)
        {
            SetColumnMiddle(builder, column);
            builder.AppendLine("        public byte " + GetColumnName(column) + " { get; set; }");
        }

        private static void ByteNullValue(StringBuilder builder, ColumnModel column)
        {
            SetColumnMiddle(builder, column);
            builder.AppendLine("        public byte? " + GetColumnName(column) + " { get; set; }");
        }

        private static void ByteArrayValue(StringBuilder builder, ColumnModel column)
        {
            SetColumnMiddle(builder, column);
            builder.AppendLine("        public byte[] " + GetColumnName(column) + " { get; set; }");
        }

        private static void GUIDValue(StringBuilder builder, ColumnModel column)
        {
            SetColumnMiddle(builder, column);
            builder.AppendLine("        public GUID " + GetColumnName(column) + " { get; set; }");
        }

        private static void TimestampValue(StringBuilder builder, ColumnModel column)
        {
            SetColumnMiddle(builder, column);
            builder.AppendLine("        [Timestamp]");
            builder.AppendLine("        public byte[] " + GetColumnName(column) + " { get; set; }");
        }

        #endregion DataType

        private static string GetColumnName(ColumnModel column)
        {
            return column.COLUMN_NAME_ENTITY;
        }

        private static string GetColumnDescription(ColumnModel column)
        {
            if (string.IsNullOrWhiteSpace(column.COLUMN_DESCRIPTION))
            {
                return string.Empty;
            }
            else
            {
                return column.COLUMN_DESCRIPTION.Replace("\r\n", "； ");
            }
        }
    }
}