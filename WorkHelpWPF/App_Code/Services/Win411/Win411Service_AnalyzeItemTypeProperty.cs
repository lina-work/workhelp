﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WorkHelp.Wpf.Models;
using WorkHelp.Wpf.Models.Aras;
using WorkHelp.Wpf.Repository;

namespace WorkHelp.Wpf.Services.Win411
{
    public partial class Win411Service
    {
        public static string AnalyzeItemTypeProperty(Win401Model wModel, SchemaModel sModel)
        {
            var list = Repository411.GetDistinctPropertySQLList(wModel.ConnectionString);

            if (list == null || !list.Any()) return string.Empty;

            var builder = new StringBuilder();
            foreach (var x in list)
            {
                builder.AppendLine(x.Statement);
                builder.AppendLine();
            }
            return builder.ToString();
        }
    }
}
