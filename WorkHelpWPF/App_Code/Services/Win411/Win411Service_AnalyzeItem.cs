﻿using System.Linq;
using System.Text;
using WorkHelp.Wpf.Models;
using WorkHelp.Wpf.Repository;

namespace WorkHelp.Wpf.Services.Win411
{
    public partial class Win411Service
    {
        public static string AnalyzeItemType(Win401Model wModel, SchemaModel sModel)
        {
            var list = Repository411.GetItemTypeList(wModel.ConnectionString);
            var relatedList = Repository411.GetRelationShipTypeList(wModel.ConnectionString);
            if (list == null || !list.Any()) return string.Empty;

            var builder = new StringBuilder();
            list.ForEach(x =>
            {
                string upper_name = x.NAME.ToUpper();

                string old_name = upper_name.Contains("_CLA_") ? upper_name.Replace("_CLA_", "_") : upper_name;
                string new_name = upper_name.Contains("_CLA_") ? upper_name : upper_name.Replace("IN_", "IN_CLA_");

                old_name = GetNewTableName(old_name);
                new_name = GetNewTableName(new_name);

                string is_changed = upper_name.Contains("_CLA_") ? "是" : "否";

                builder.AppendLine($"    - `{new_name}`");
                builder.AppendLine($"        - 原名: {old_name}");

                if (!string.IsNullOrWhiteSpace(x.LABEL_ZT))
                {
                    builder.AppendLine($"        - 功能: {x.LABEL_ZT}");
                }

                if (x.IS_RELATIONSHIP == "1")
                {
                    var relation = relatedList.FirstOrDefault(y => y.ID == x.ID);
                    if (relation != null)
                    {
                        if (string.IsNullOrWhiteSpace(x.LABEL_ZT))
                        {
                            builder.AppendLine($"        - 功能: {relation.LABEL_ZT}");
                        }
                        builder.AppendLine($"        - Source Name: {relation.SOURCE_NAME}");

                        if (string.IsNullOrWhiteSpace(relation.RELATED_NAME))
                        {
                            builder.AppendLine($"        - Related Name: 無");
                        }
                        else
                        {
                            builder.AppendLine($"        - Related Name: {relation.RELATED_NAME}");
                        }
                    }
                }
                else
                {

                }

                builder.AppendLine($"        - 變更: `{is_changed}`");

                builder.AppendLine();
            });

            var builderMain = new StringBuilder();
            builderMain.AppendLine("- 物件類型 ItemType");
            builderMain.AppendLine($"    - count: {list.Count}");
            builderMain.AppendLine();
            builderMain.Append(builder);
            return builderMain.ToString();
        }
    }
}
