﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WorkHelp.Wpf.Models;
using WorkHelp.Wpf.Models.Aras;
using WorkHelp.Wpf.Repository;

namespace WorkHelp.Wpf.Services.Win411
{
    public partial class Win411Service
    {
        public static string AnalyzeMethodDetail(Win401Model wModel, SchemaModel sModel)
        {
            var itemlist = Repository411.GetItemTypeList(wModel.ConnectionString);
            var methodAlllist = FilterMethodList(wModel, sModel);

            var methodlist = Repository411.GetMethodTypeList(wModel.ConnectionString, wModel.TargetName);

            if (itemlist == null || !itemlist.Any()) return string.Empty;
            if (methodAlllist == null || !methodAlllist.Any()) return string.Empty;
            if (methodlist == null || !methodlist.Any()) return string.Empty;

            foreach (var item in itemlist)
            {
                SetNames(item);
            }

            foreach (var item in methodAlllist)
            {
                SetNames(item);
            }

            foreach (var item in methodlist)
            {
                SetNames(item);
            }

            var target = methodlist.FirstOrDefault();

            var builder = new StringBuilder();
            builder.AppendLine($"- `{target.NEW_NAME}`");
            builder.AppendLine($"    - 原名: {target.OLD_NAME}");
            builder.AppendLine($"    - 名稱已變更: {target.IS_CHANGED}");

            var lines = target.METHOD_CODE.Split(new char[] { '\r' }, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < lines.Length; i++)
            {
                var line = lines[i].Replace("\n", "");

                var result = target.IS_CHANGED == "是";
                var result1 = LogItemTypeCheck(builder, itemlist, line, i);
                var result2 = LogItemTypeCheck(builder, methodAlllist, line, i);

                if (result && result1 && result2)
                {
                    builder.AppendLine($"    - 結果: 已完成變更");

                }
            }

            return builder.ToString();
        }

        private static bool LogItemTypeCheck(StringBuilder build, List<ItemType> list, string line, int idx, bool checkName = false)
        {
            bool isOK = false;
            foreach (var item in list)
            {
                if (IsContain(line, item.OLD_TAG1))
                {
                    build.AppendLine($"    - 行號: {idx}, 舊值: {item.OLD_TAG1}, 新值: {item.NEW_NAME}");
                }
                else if (IsContain(line, item.OLD_TAG2))
                {
                    build.AppendLine($"    - 行號: {idx}, 舊值: {item.OLD_TAG2}, 新值: {item.NEW_NAME}");
                }
                else if (IsContain(line, item.OLD_TAG3))
                {
                    build.AppendLine($"    - 行號: {idx}, 舊值: {item.OLD_TAG3}, 新值: {item.NEW_NAME}");
                }
                else if (IsContain(line, item.OLD_TAG4))
                {
                    build.AppendLine($"    - 行號: {idx}, 舊值: {item.OLD_TAG4}, 新值: {item.NEW_NAME}");
                }
                else if (checkName)
                {
                    if (IsContain(line, item.OLD_NAME))
                    {
                        build.AppendLine($"    - 行號: {idx}, 舊值: {item.OLD_NAME}, 新值: {item.NEW_NAME}");
                    }
                    else
                    {
                        isOK = true;
                    }
                }
                else
                {
                    isOK = true;
                }
            }
            return isOK;
        }
    }
}
