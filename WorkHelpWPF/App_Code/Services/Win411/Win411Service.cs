﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WorkHelp.Utilities;
using WorkHelp.Wpf.Models;
using WorkHelp.Wpf.Models.Aras;
using WorkHelp.Wpf.Repository;

namespace WorkHelp.Wpf.Services.Win411
{
    public partial class Win411Service
    {
        public static string GetNewTableName(string table)
        {
            bool isFirst = true;
            string newTableName = string.Empty;
            for (int i = 0; i < table.Length; i++)
            {
                char c = table[i];

                if (c == '_')
                {
                    newTableName += c;
                    isFirst = true;
                }
                else
                {
                    if (isFirst)
                    {
                        newTableName += c.ToString().ToUpper();
                        isFirst = false;
                    }
                    else
                    {
                        newTableName += c.ToString().ToLower();
                    }
                }
            }
            return newTableName;
        }
        
        public static void SetNames(ItemType item)
        {
            string upper_name = item.NAME.ToUpper();
            string old_name = upper_name.Contains("_CLA_") ? upper_name.Replace("_CLA_", "_") : upper_name;
            string new_name = upper_name.Contains("_CLA_") ? upper_name : upper_name.Replace("IN_", "IN_CLA_");

            item.IS_CHANGED = upper_name.Contains("_CLA_") ? "是" : "否";

            item.OLD_NAME = GetNewTableName(old_name);
            item.NEW_NAME = GetNewTableName(new_name);

            item.OLD_TAG1 = "\"" + item.OLD_NAME + "\"";
            item.NEW_TAG1 = "\"" + item.NEW_NAME + "\"";

            item.OLD_TAG2 = "'" + item.OLD_NAME + "'";
            item.NEW_TAG2 = "'" + item.NEW_NAME + "'";

            item.OLD_TAG3 = "[" + item.OLD_NAME + "]";
            item.NEW_TAG3 = "[" + item.NEW_NAME + "]";

            item.OLD_TAG4 = item.OLD_NAME + " ";
            item.NEW_TAG4 = item.NEW_NAME + " ";
        }

        public static void SetNames2(ItemType item)
        {
            string upper_name = item.NAME.ToUpper();
            string old_name = upper_name;
            string new_name = upper_name;

            item.IS_CHANGED = upper_name.Contains("_CLA_") ? "是" : "否";

            item.OLD_NAME = item.NAME;
            item.NEW_NAME = item.NAME;

            item.OLD_TAG1 = "\"" + item.OLD_NAME + "\"";
            item.NEW_TAG1 = "\"" + item.NEW_NAME + "\"";

            item.OLD_TAG2 = "'" + item.OLD_NAME + "'";
            item.NEW_TAG2 = "'" + item.NEW_NAME + "'";

            item.OLD_TAG3 = "[" + item.OLD_NAME + "]";
            item.NEW_TAG3 = "[" + item.NEW_NAME + "]";

            item.OLD_TAG4 = item.OLD_NAME + " ";
            item.NEW_TAG4 = item.NEW_NAME + " ";
        }


        private static bool IsContain(ItemType x, string value)
        {
            return x.METHOD_CODE.IndexOf(value, StringComparison.OrdinalIgnoreCase) >= 0;
        }

        private static bool IsContain(string line, string value)
        {
            return line.IndexOf(value, StringComparison.OrdinalIgnoreCase) >= 0;
        }

        private static void AppendItemTypeCheck(StringBuilder build, List<ItemType> list, ItemType target)
        {
            if (target.HasFinished)
            {
                foreach (var item in list)
                {
                    if (IsContain(target, item.OLD_TAG1))
                    {
                        target.NeedChanged = true;
                        target.HasFinished = false;
                    }
                    else if (IsContain(target, item.OLD_TAG2))
                    {
                        target.NeedChanged = true;
                        target.HasFinished = false;
                    }
                    else if (IsContain(target, item.OLD_TAG3))
                    {
                        target.NeedChanged = true;
                        target.HasFinished = false;
                    }
                    else if (IsContain(target, item.OLD_TAG4))
                    {
                        target.NeedChanged = true;
                        target.HasFinished = false;
                    }
                    else if (IsContain(target, item.NEW_TAG1))
                    {
                        target.NeedChanged = true;
                    }
                    else if (IsContain(target, item.NEW_TAG2))
                    {
                        target.NeedChanged = true;
                    }
                    else if (IsContain(target, item.NEW_TAG3))
                    {
                        target.NeedChanged = true;
                    }
                    else if (IsContain(target, item.NEW_TAG4))
                    {
                        target.NeedChanged = true;
                    }


                    if (target.NeedChanged)
                    {
                        break;
                    }
                }
            }
        }

        public static List<ItemType> FilterMethodList(Win401Model wModel, SchemaModel sModel, bool isGetFilterMethod = false)
        {
            List<ItemType> result = new List<ItemType>();

            var methodlist = Repository411.GetAllMethodTypeList(wModel.ConnectionString);
            var ignoreArray = FileUtility.GetArray("Samples\\Win41100", "Ignore_Method.txt");

            if (ignoreArray == null || ignoreArray.Length == 0)
            {
                return result;
            }

            foreach (var item in methodlist)
            {
                if (isGetFilterMethod) 
                { 
                    if (ignoreArray.Contains(item.NAME))
                    {
                        result.Add(item);
                    }
                }
                else
                {
                    if (!ignoreArray.Contains(item.NAME))
                    {
                        result.Add(item);
                    }
                }
            }

            return result;
        }
    }
}
