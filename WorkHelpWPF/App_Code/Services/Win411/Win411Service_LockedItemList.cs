﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WorkHelp.Wpf.Models;
using WorkHelp.Wpf.Models.Aras;
using WorkHelp.Wpf.Repository;

namespace WorkHelp.Wpf.Services.Win411
{
    public partial class Win411Service
    {
        public static string LockedItemList(Win401Model wModel, SchemaModel sModel)
        {
            var list = Repository411.GetLockItemList<ItemLock>(wModel.ConnectionString);

            if (list == null || !list.Any()) return string.Empty;

            var builder = new StringBuilder();
            builder.AppendLine($"# 資料庫 {wModel.DatabaseName} #");
            builder.AppendLine();

            var name = string.Empty;
            foreach (var item in list)
            {
                if (name != item.C1)
                {
                    if (!string.IsNullOrWhiteSpace(name))
                    {
                        builder.AppendLine();
                    }
                    builder.AppendLine($"- {item.C1}");

                    name = item.C1;
                }

                var label = string.IsNullOrEmpty(item.C3) ? string.Empty : $"({item.C3})";
                builder.AppendLine($"    - `{item.C2}` {label}");
            }

            return builder.ToString();
        }
    }
}
