﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WorkHelp.Wpf.Models.Aras;
using WorkHelp.Wpf.Models;
using WorkHelp.Wpf.Repository;

namespace WorkHelp.Wpf.Services.Win411
{
    public partial class Win411Service
    {
        public static string AnalyzePropertyDataType(Win401Model wModel, SchemaModel sModel)
        {
            var properties = Repository411.GetAllProperty<ForeignProperty>(wModel.ConnectionString);
            if (properties == null || !properties.Any()) return string.Empty;

            string key = wModel.TargetName.ToUpper();

            var targets = properties.Where(x => IsContain(x.Table_Name, key) && (x.Data_Type.ToUpper() == "FOREIGN" || x.Data_Type.ToUpper() == "ITEM"));
            if (targets == null || !targets.Any()) return string.Empty;

            var name = string.Empty;
            var builder = new StringBuilder();

            builder.AppendLine($"# 資料庫 {wModel.DatabaseName} #");
            builder.AppendLine();

            foreach (var item in targets)
            {
                if (name != item.Table_Name)
                {
                    if(!string.IsNullOrWhiteSpace(name))
                    { 
                        builder.AppendLine();
                    }
                    name = item.Table_Name;

                    var tableLabel = string.IsNullOrEmpty(item.Table_Label) ? string.Empty : $"({item.Table_Label})";
                    builder.AppendLine($"- {item.Table_Name} {tableLabel}");
                }

                var dataType = item.Data_Type.ToUpper();
                var fieldLabel = string.IsNullOrEmpty(item.Field_Label) ? string.Empty : $"({item.Field_Label})";

                builder.AppendLine($"    - `{item.Field_Name}` {fieldLabel}");
                builder.AppendLine($"        - Data Type: {dataType}");

                if (dataType == "FOREIGN")
                {
                    var dataSourceProperty = properties.SingleOrDefault(x => x.Field_Id == item.Data_Source);
                    var foreignProperty = properties.SingleOrDefault(x => x.Field_Id == item.Foreign_Propery);
                    if (dataSourceProperty != null)
                    {
                        builder.AppendLine($"        - Data Source: `{dataSourceProperty.Field_Name}` {dataSourceProperty.Field_Label} ({dataSourceProperty.Table_Name})");
                    }
                    if (foreignProperty != null)
                    {
                        builder.AppendLine($"        - Foreign Property: `{foreignProperty.Field_Name}` {foreignProperty.Field_Label} ({foreignProperty.Table_Name})");
                    }
                }
                else if (dataType == "ITEM")
                {
                    var itemTypeProperty = properties.FirstOrDefault(x => x.Table_Id == item.Data_Source);
                    if (itemTypeProperty != null)
                    {
                        builder.AppendLine($"        - Data Source: `{itemTypeProperty.Table_Name}` {itemTypeProperty.Table_Label}");
                    }
                }
            }

            return builder.ToString();
        }

        public static string FindPropertyUnOk(Win401Model wModel, SchemaModel sModel)
        {
            var targets = Repository411.GetUnOkProperty<ForeignProperty>(wModel.ConnectionString);

            var name = string.Empty;
            var builder = new StringBuilder();
            
            builder.AppendLine($"# 資料庫 {wModel.DatabaseName} #");
            builder.AppendLine();

            foreach (var item in targets)
            {
                if (name != item.Table_Name)
                {
                    if (!string.IsNullOrWhiteSpace(name))
                    {
                        builder.AppendLine();
                    }
                    name = item.Table_Name;

                    var tableLabel = string.IsNullOrEmpty(item.Table_Label) ? string.Empty : $"({item.Table_Label})";
                    builder.AppendLine($"- {item.Table_Name} {tableLabel}");
                }

                var dataType = item.Data_Type.ToUpper();
                var fieldLabel = string.IsNullOrEmpty(item.Field_Label) ? string.Empty : $"({item.Field_Label})";

                builder.AppendLine($"    - `{item.Field_Name}` {fieldLabel}");
                builder.AppendLine($"        - Data Type: {dataType}");

            }

            return builder.ToString();
        }

        public static string DebuggerBreakMethod(Win401Model wModel, SchemaModel sModel, string keyword)
        {
            var methods = Repository411.METHOD_LIKE<ItemType>(keyword, wModel.ConnectionString);
            if (methods == null || !methods.Any()) return string.Empty;

            var bdrContents = new StringBuilder();
            var count = 0;

            foreach (var method in methods)
            {
                if (IsErrorDebuggerBreak(method.METHOD_CODE.Trim(), keyword))
                {
                    count++;
                    bdrContents.AppendLine($" - {method.NAME}");
                }
            }

            var builder = new StringBuilder();
            builder.AppendLine($"# 資料庫 {wModel.DatabaseName} #");
            builder.AppendLine($" - Method Count: {count}");
            builder.AppendLine();
            builder.Append(bdrContents);
            return builder.ToString();
        }

        public static bool IsErrorDebuggerBreak(string line, string key)
        {
            int pos = line.IndexOf(key);
            if (pos == -1)
            {
                return false;
            }
            else if (pos < 2)
                {
                return true;
            }
            else 
            {
                string prefix = string.Empty;

                if (pos == 2)
                {
                    prefix = line.Substring(pos - 2, 2);
                }
                else 
                { 
                    prefix = line.Substring(pos - 3, 3);
                }
                if (!prefix.Contains("//"))
                {
                    return true;
                }
                else
                {
                    return IsErrorDebuggerBreak(line.Substring(pos + key.Length + 1, line.Length - (pos + key.Length) -1 ), key);
                }
            }
        }


    }
}
