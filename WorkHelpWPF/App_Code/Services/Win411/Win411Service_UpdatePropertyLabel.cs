﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WorkHelp.Wpf.Models;
using WorkHelp.Wpf.Models.Aras;
using WorkHelp.Wpf.Repository;

namespace WorkHelp.Wpf.Services.Win411
{
    public partial class Win411Service
    {
        public static string UpdatePropertyLabel(Win401Model wModel, SchemaModel sModel)
        {
            var list = Repository411.GetUpdatePropertySQLList<ItemType>(wModel.ConnectionString);

            if (list == null || !list.Any()) return string.Empty;

            foreach (var item in list)
            {
                SetNames(item);
            }

            var name = string.Empty;
            var builder = new StringBuilder();
            foreach (var item in list)
            {
                if (name != item.NAME)
                {
                    if(builder.Length > 0)
                    {
                        builder.AppendLine();
                    }

                    name = item.NAME;
                    builder.AppendLine($"-- `{name}`");
                }
                var statement = item.Statement.Replace(item.OLD_NAME, item.NEW_NAME);
                builder.AppendLine($"      {statement}");
            }
            return builder.ToString();
        }

        public static string SQL_UpdatePropertyLabel(Win401Model wModel, SchemaModel sModel, string table_name)
        {
            var list = Repository411.GetUpdatePropertySQLList<ItemType>(wModel.ConnectionString, table_name);

            if (list == null || !list.Any()) return string.Empty;

            var name = string.Empty;
            var builder = new StringBuilder();
            foreach (var item in list)
            {
                if (name != item.NAME)
                {
                    if (builder.Length > 0)
                    {
                        builder.AppendLine();
                    }

                    name = item.NAME;
                    builder.AppendLine($"-- `{name}`");
                }
                var statement = item.Statement;
                builder.AppendLine($"      {statement}");
            }
            return builder.ToString();
        }
    }
}
