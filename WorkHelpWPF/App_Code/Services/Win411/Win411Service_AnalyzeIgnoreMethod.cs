﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WorkHelp.Utilities;
using WorkHelp.Wpf.Models;
using WorkHelp.Wpf.Models.Aras;
using WorkHelp.Wpf.Repository;

namespace WorkHelp.Wpf.Services.Win411
{
    public partial class Win411Service
    {
        public static string AnalyzeIgnoreMethod(Win401Model wModel, SchemaModel sModel)
        {
            var all_method_list = FilterMethodList(wModel, sModel, isGetFilterMethod: true);
            var class_method_list = FilterMethodList(wModel, sModel, isGetFilterMethod: false);

            if (all_method_list == null || !all_method_list.Any()) return string.Empty;
            if (class_method_list == null || !class_method_list.Any()) return string.Empty;

            StringBuilder builder = new StringBuilder();
            foreach (var item in all_method_list)
            {
                SetNames(item);
            }
            foreach (var item in class_method_list)
            {
                SetNames(item);
            }

            foreach (var x in all_method_list)
            {
                x.NeedChanged = false;
                x.HasFinished = true;
                if (string.IsNullOrWhiteSpace(x.METHOD_CODE))
                {
                    continue;
                }

                foreach (var y in class_method_list)
                {
                    if (x.NAME == y.NAME)
                    {

                    }
                    else if (IsContain(x.METHOD_CODE, y.OLD_TAG1))
                    {
                        x.NeedChanged = true;
                        x.HasFinished = false;
                    }
                    else if (IsContain(x.METHOD_CODE, y.OLD_TAG2))
                    {
                        x.NeedChanged = true;
                        x.HasFinished = false;
                    }
                    else if (IsContain(x.METHOD_CODE, y.OLD_TAG3))
                    {
                        x.NeedChanged = true;
                        x.HasFinished = false;
                    }
                    else if (IsContain(x.METHOD_CODE, y.OLD_TAG4))
                    {
                        x.NeedChanged = true;
                        x.HasFinished = false;
                    }
                    else if (IsContain(x.METHOD_CODE, y.OLD_NAME))
                    {
                        x.NeedChanged = true;
                        x.HasFinished = false;
                    }
                    else if (IsContain(x.METHOD_CODE, y.NEW_TAG1))
                    {
                        x.NeedChanged = true;
                    }
                    else if (IsContain(x.METHOD_CODE, y.NEW_TAG2))
                    {
                        x.NeedChanged = true;
                    }
                    else if (IsContain(x.METHOD_CODE, y.NEW_TAG3))
                    {
                        x.NeedChanged = true;
                    }
                    else if (IsContain(x.METHOD_CODE, y.NEW_TAG4))
                    {
                        x.NeedChanged = true;
                    }
                }

                if (x.NeedChanged)
                {
                    if (x.HasFinished)
                    {
                        builder.AppendLine($"    - {x.NEW_NAME}");
                    }
                    else
                    {
                        builder.AppendLine($"    - `{x.NEW_NAME}`");
                    }
                    builder.AppendLine($"        - 原名: {x.OLD_NAME}");
                    builder.AppendLine($"        - 備註: {x.COMMENTS}");
                }
            }

            var needChangedList = all_method_list.Where(x => x.NeedChanged).ToList();
            var hasFinishedList = needChangedList.Where(x => x.HasFinished).ToList();

            int total_count = needChangedList.Count;
            int finished_count = hasFinishedList.Count;

            var builderMain = new StringBuilder();
            builderMain.AppendLine("- 方法 Method (包含新 Method 名稱)");
            builderMain.AppendLine($"    - 總數: {total_count}");
            builderMain.AppendLine($"    - 已處理: {finished_count}");
            builderMain.AppendLine($"    - 待處理: {total_count - finished_count}");
            builderMain.AppendLine();
            builderMain.Append(builder);
            return builderMain.ToString();
        }


    }
}
