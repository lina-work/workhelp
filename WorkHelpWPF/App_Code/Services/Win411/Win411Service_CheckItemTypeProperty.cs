﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WorkHelp.Wpf.Models;
using WorkHelp.Wpf.Models.Aras;
using WorkHelp.Wpf.Repository;

namespace WorkHelp.Wpf.Services.Win411
{
    public partial class Win411Service
    {
        public static string CheckItemTypeProperty(Win401Model wModel, SchemaModel sModel)
        {
            var list = Repository411.GetDistinctPropertySQLList(wModel.ConnectionString);
            var newItemTypeList = Repository411.GetItemTypeList(wModel.ConnectionString);

            if (list == null || !list.Any()) return string.Empty;
            if (newItemTypeList == null || !newItemTypeList.Any()) return string.Empty;

            foreach (var item in newItemTypeList)
            {
                SetNames(item);
            }

            var builder = new StringBuilder();
            foreach (var x in list)
            {
                var values = Repository411.GetValueList(x.Statement, wModel.ConnectionString);
                if (values == null || !values.Any())
                {
                    continue;
                }

                var builder2 = new StringBuilder();
                foreach (var value in values)
                {
                    foreach (var item in newItemTypeList)
                    {
                        if (IsContain(value, item.OLD_NAME))
                        {
                            builder2.AppendLine($"        - 包含 {item.OLD_NAME}"); ;
                        }
                    }
                }

                if (builder2.Length > 0)
                {
                    builder.AppendLine($"- {x.TableName}"); ;
                    builder.AppendLine($"    - {x.FieldName}"); ;
                    builder.AppendLine($"    - statement: {x.Statement}"); ;
                    builder.Append(builder2); ;
                    builder.AppendLine();
                }
            }
            return builder.ToString();
        }
    }
}
