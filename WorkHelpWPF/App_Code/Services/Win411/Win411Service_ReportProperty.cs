﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WorkHelp.Wpf.Models;
using WorkHelp.Wpf.Models.Aras;
using WorkHelp.Wpf.Repository;

namespace WorkHelp.Wpf.Services.Win411
{
    public partial class Win411Service
    {        
        public static string ReportProperty(Win401Model wModel, SchemaModel sModel, string tableName)
        {
            var list = Repository411.GetPropertyList<PropertyInstance>(tableName, wModel.ConnectionString);

            if (list == null || !list.Any()) return string.Empty;

            return ReportPropertyContents(wModel.DatabaseName, tableName, list);
        }

        public static string ReportPropertyContents(string dbName, string tableName, List<PropertyInstance> list)
        {
            var builder = new StringBuilder();
            foreach (var item in list)
            {
                if (builder.Length > 0)
                {
                    builder.AppendLine();
                }

                builder.AppendLine("- 欄位: " + item.name);
                builder.AppendLine("    - 英文標籤: " + item.label);
                builder.AppendLine("    - 繁中類型: " + item.label_zt);
                builder.AppendLine("    - 資料類型: " + item.data_type);
                builder.AppendLine("    - 長度限制: " + item.stored_length);

                builder.AppendLine("    - 英文預設: " + item.default_value);
                builder.AppendLine("    - 繁中預設: " + item.default_value_zt);

                builder.AppendLine("    - 是否為必填: " + item.is_required);
                builder.AppendLine("    - 是否為索引: " + item.is_indexed);
                builder.AppendLine("    - 是否為唯一: " + item.is_keyed);

                builder.AppendLine("    - 鍵名排序: " + item.keyed_name_order);
                builder.AppendLine("    - 欄位序號: " + item.sort_order);

                builder.AppendLine("    - 簡易表單: " + item.in_web_view);
                builder.AppendLine("    - 行動版新增: " + item.in_web_add);
                builder.AppendLine("    - 行動版編輯: " + item.in_web_edit);

                builder.AppendLine("    - 式樣: " + item.pattern);
            }

            var builderMain = new StringBuilder();
            builderMain.AppendLine("- 資料庫: " + dbName);
            builderMain.AppendLine("- 資料表: " + tableName);
            builderMain.AppendLine("- 欄位數: " + list.Count);
            builderMain.AppendLine();
            builderMain.Append(builder);

            return builderMain.ToString();
        }

        public static string GeneratePropertyClass(Win401Model wModel, SchemaModel sModel, string tableName)
        {
            var list = Repository411.GetPropertyList<PropertyEntity>(tableName, wModel.ConnectionString);

            if (list == null || !list.Any()) return string.Empty;

            var builder = new StringBuilder();

            foreach (var item in list)
            {
                if (builder.Length > 0)
                {
                    builder.AppendLine();
                }

                string name = item.NAME;
                string type = item.DATA_TYPE.ToLower();
                string label = string.IsNullOrWhiteSpace(item.LABEL_ZT) ? (string.IsNullOrWhiteSpace(item.LABEL) ? "未輸入" : item.LABEL) : item.LABEL_ZT;

                string cstype = string.Empty;
                switch (type)
                {
                    //case "string": cstype = "string"; break;
                    //case "boolean": cstype = "bool?"; break;
                    //case "integer": cstype = "int?"; break;

                    //case "text": cstype = "string"; break;

                    //case "float": cstype = "double?"; break;
                    //case "date": cstype = "DateTime?"; break;
                    //case "item": cstype = "string"; break;
                    //case "list": cstype = "string"; break;
                    //case "ml_string": cstype = "string"; break;
                    default: cstype = "string"; break;
                }

                builder.AppendLine("        /// <summary>");
                builder.AppendLine("        /// " + label);
                builder.AppendLine("        /// </summary>");
                builder.AppendLine("        public " + cstype + " " + name + " { get; set; }");
            }

            var builderMain= new StringBuilder();
            builderMain.AppendLine("using System;");
            builderMain.AppendLine("using System.Collections.Generic;");
            builderMain.AppendLine("using System.Linq;");
            builderMain.AppendLine("using System.Text;");
            builderMain.AppendLine();

            builderMain.AppendLine("namespace WorkHelp.Wpf.Models.Aras");
            builderMain.AppendLine("{");
            builderMain.AppendLine("    public class " + tableName + "_Model");
            builderMain.AppendLine("    {");
            builderMain.Append(builder);
            builderMain.AppendLine("    }");
            builderMain.AppendLine("}");
            return builderMain.ToString();
        }
    }
}
