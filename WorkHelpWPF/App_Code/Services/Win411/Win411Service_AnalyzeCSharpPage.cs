﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WorkHelp.Wpf.Models;
using WorkHelp.Wpf.Models.Aras;
using WorkHelp.Wpf.Repository;

namespace WorkHelp.Wpf.Services.Win411
{
    public partial class Win411Service
    {
        public static string AnalyzeCSharpPage(Win401Model wModel, SchemaModel sModel, string fileName, string[] lines)
        {
            var itemlist = Repository411.GetItemTypeList(wModel.ConnectionString);
            var methodlist = FilterMethodList(wModel, sModel, isGetFilterMethod: false);

            if (itemlist == null || !itemlist.Any()) return string.Empty;
            if (methodlist == null || !methodlist.Any()) return string.Empty;

            foreach (var item in itemlist)
            {
                SetNames(item);
            }

            foreach (var item in methodlist)
            {
                SetNames(item);
            }


            var builder = new StringBuilder();
            builder.AppendLine($"- `{fileName}`"); ;

            var ok_name = fileName.ToUpper().Contains("_CLA");
            var ok_itemType = true;
            var ok_method = true;

            for (int i = 0; i < lines.Length; i++)
            {
                var line = lines[i].Replace("\n", "");

                ok_itemType &= LogItemTypeCheck(builder, itemlist, line, i, checkName: true);
                ok_method &= LogItemTypeCheck(builder, methodlist, line, i, checkName: true);
            }

            if (ok_name && ok_itemType && ok_method)
            {
                builder.AppendLine($"    - 結果: 已完成變更");
            }

            return builder.ToString();
        }


    }
}
