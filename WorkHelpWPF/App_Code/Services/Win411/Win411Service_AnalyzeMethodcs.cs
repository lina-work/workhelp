﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WorkHelp.Utilities;
using WorkHelp.Wpf.Models;
using WorkHelp.Wpf.Models.Aras;
using WorkHelp.Wpf.Repository;

namespace WorkHelp.Wpf.Services.Win411
{
    public partial class Win411Service
    {

        public static string AnalyzeMethod(Win401Model wModel, SchemaModel sModel)
        {
            var itemlist = Repository411.GetItemTypeList(wModel.ConnectionString);
            var methodlist = FilterMethodList(wModel, sModel);

            if (itemlist == null || !itemlist.Any()) return string.Empty;
            if (methodlist == null || !methodlist.Any()) return string.Empty;

            foreach (var item in itemlist)
            {
                SetNames(item);
            }

            foreach (var item in methodlist)
            {
                SetNames(item);
            }

            var builder = new StringBuilder();

            var manualArray = FileUtility.GetArray("Samples\\Win41100", "Manual_Method.txt");
            if (manualArray == null || manualArray.Length == 0)
            {
                manualArray = new string[] { string.Empty };
            }
            foreach (var x in methodlist)
            {
                if (manualArray.Contains(x.NAME))
                {
                    x.NeedChanged = true;
                    x.HasFinished = true;

                    builder.AppendLine($"    - {x.NEW_NAME}");
                    builder.AppendLine($"        - 原名: {x.OLD_NAME}");
                    builder.AppendLine($"        - 備註: {x.COMMENTS}");
                    builder.AppendLine($"        - 內容變更: 手動排除");
                }
                else 
                { 
                    x.NeedChanged = false;
                    x.HasFinished = true;

                    if (string.IsNullOrWhiteSpace(x.METHOD_CODE)) continue;

                    AppendItemTypeCheck(builder, itemlist, x);
                    AppendItemTypeCheck(builder, methodlist, x);

                    string has_finished = x.HasFinished ? "是" : "否";
                    if (x.NeedChanged)
                    {
                        if(x.HasFinished)
                        {
                            builder.AppendLine($"    - {x.NEW_NAME}");
                        }
                        else
                        {
                            builder.AppendLine($"    - `{x.NEW_NAME}`");
                        }
                        builder.AppendLine($"        - 原名: {x.OLD_NAME}");
                        builder.AppendLine($"        - 備註: {x.COMMENTS}");
                        builder.AppendLine($"        - 名稱變更: `{x.IS_CHANGED}`");
                        builder.AppendLine($"        - 內容變更: `{has_finished}`");
                    }
                }
            }

            var needChangedList = methodlist.Where(x => x.NeedChanged).ToList();
            var hasFinishedList = needChangedList.Where(x => x.HasFinished).ToList();

            int total_count = needChangedList.Count;
            int finished_count = hasFinishedList.Count;

            var builderMain = new StringBuilder();
            builderMain.AppendLine("- 方法 Method");
            builderMain.AppendLine($"    - 總數: {total_count}");
            builderMain.AppendLine($"    - 已處理: {finished_count}");
            builderMain.AppendLine($"    - 待處理: {total_count - finished_count}");
            builderMain.AppendLine();
            builderMain.Append(builder);
            return builderMain.ToString();
        }
    }
}
