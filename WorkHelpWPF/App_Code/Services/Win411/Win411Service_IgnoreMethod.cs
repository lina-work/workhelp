﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WorkHelp.Wpf.Models;
using WorkHelp.Wpf.Models.Aras;
using WorkHelp.Wpf.Repository;

namespace WorkHelp.Wpf.Services.Win411
{
    public partial class Win411Service
    {
        public static string IgnoreMethod(Win401Model wModel, SchemaModel sModel)
        {
            var itemlist = Repository411.GetItemTypeList(wModel.ConnectionString);
            var methodlist = Repository411.GetAllMethodTypeList(wModel.ConnectionString);

            if (itemlist == null || !itemlist.Any()) return string.Empty;
            if (methodlist == null || !methodlist.Any()) return string.Empty;

            foreach (var item in itemlist)
            {
                SetNames(item);
            }

            var builder = new StringBuilder();
            foreach (var x in methodlist)
            {
                x.NeedChanged = false;
                x.HasFinished = true;

                if (string.IsNullOrWhiteSpace(x.METHOD_CODE))
                {
                    x.NeedChanged = false;
                }
                else
                {
                    AppendItemTypeCheck(builder, itemlist, x);
                }

                if(!x.NeedChanged)
                {
                    builder.AppendLine(x.NAME);
                }
            }
            return builder.ToString();
        }
    }
}
