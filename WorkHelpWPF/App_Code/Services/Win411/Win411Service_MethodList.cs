﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WorkHelp.Utilities;
using WorkHelp.Wpf.Models;
using WorkHelp.Wpf.Models.Aras;
using WorkHelp.Wpf.Repository;

namespace WorkHelp.Wpf.Services.Win411
{
    public partial class Win411Service
    {
        public static string MethodList(Win401Model wModel, SchemaModel sModel, string keyword)
        {
            var methods = Repository411.MethodList<ItemType>(keyword, wModel.ConnectionString);
            if (methods == null || !methods.Any()) return string.Empty;

            foreach (var method in methods)
            {
                FileUtility.WriteFile(sModel.FolderName, method.NAME + ".cs", method.METHOD_CODE);
            }

            return "";
        }

        public static string MethodID(Win401Model wModel, SchemaModel sModel, string keyword)
        {
            string[] arr = keyword.Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
            string id = arr.Last();
            string name = arr.First();

            var methods = Repository411.MethodID<ItemType>(id, name, wModel.ConnectionString);
            if (methods == null || !methods.Any()) return string.Empty;

            foreach (var method in methods)
            {
                FileUtility.WriteFile(sModel.FolderName, method.NAME + ".cs", method.METHOD_CODE);
            }

            return "";
        }

        public static string MethodToday(Win401Model wModel, SchemaModel sModel, string keyword)
        {
            var methods = Repository411.MethodToday<ItemType>(keyword, wModel.ConnectionString);

            if (methods == null || !methods.Any()) return string.Empty;

            foreach (var method in methods)
            {
                FileUtility.TargetFile(sModel.FolderName, method.NAME + ".cs", method.METHOD_CODE);
            }

            return "";
        }

        public static string MethodListBySql(Win401Model wModel, SchemaModel sModel, string command)
        {
            var methods = Repository411.MethodListBySql<ItemType>(command, wModel.ConnectionString);

            if (methods == null || !methods.Any()) return string.Empty;

            foreach (var method in methods)
            {
                FileUtility.TargetFile(sModel.FolderName, method.NAME + ".cs", method.METHOD_CODE);
            }

            return "";
        }

        public static string MethodUseSuperUser(Win401Model wModel, SchemaModel sModel, string keyword)
        {
            var methods = Repository411.METHOD_LIKE<ItemType>("Aras.Server.Security.Identity.GetByName(\"Super User\")", wModel.ConnectionString);
            if (methods == null || !methods.Any()) return string.Empty;

            var bdrContents = new StringBuilder();

            var count = 0;
            foreach (var method in methods)
            {
                if (method.METHOD_CODE.Contains("Aras.Server.Security.Permissions.RevokeIdentity"))
                {
                }
                else
                {
                    count++;
                    bdrContents.AppendLine($" - [ ] {method.NAME}");
                }
            }

            var builder = new StringBuilder();
            builder.AppendLine($"# 資料庫 {wModel.DatabaseName} #");
            builder.AppendLine($" - Method Count: {count}");
            builder.AppendLine();
            builder.Append(bdrContents);
            return builder.ToString();
        }

    }
}
