﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using WorkHelp.Wpf.Models.Main;
using WorkHelp.Wpf.Models.Main.Extends;
using WorkHelp.Wpf.Models.ViewSplit;
using WorkHelp.Wpf.Models.ViewSplit.Extends;
using WorkHelp.Wpf.Utility;

namespace WorkHelp.Wpf.Services
{
    /// <summary>
    /// Win501 系列服務
    /// </summary>
    public class Win501Service
    {
        /// <summary>
        /// 建構元
        /// </summary>
        protected Win501Service() { }

        /// <summary>
        /// 視窗載入
        /// </summary>
        public static void WindowLoaded(object windowTag, Action<TMenuItem> setMenuItem, Action<TMenuItem> importAction, Action defaultAction)
        {
            // 視窗標籤資料
            string tag = (string)windowTag;

            // 主選單項目資料
            TMenuItem menu = TMenuItemExtend.GetItem(tag);

            if (menu == null || menu.Code == 0)
            {
                System.Windows.Forms.MessageBox.Show("視窗標籤資料發生錯誤");
                return;
            }

            if (!string.IsNullOrWhiteSpace(menu.ConfigFile))
            {
                setMenuItem(menu);
            }

            if (!string.IsNullOrWhiteSpace(menu.Value))
            {
                // 載入[外部傳入]預設的畫面分割組態名稱
                importAction(menu);
            }
            else
            {
                // 載入[組態檔內]預設的畫面分割組態代碼
                defaultAction();
            }
        }

        /// <summary>
        /// 畫面組態選單變更
        /// </summary>
        public static void ChangeViewConfig(ComboBox cbx, Func<ViewConfig> readConfig, Action<ViewParting> changeData)
        {
            int index = cbx.SelectedIndex;
            if (index >= 0)
            {
                ViewConfig config = readConfig();
                if (config.Items == null || config.Items.Count == 0 || index >= config.Items.Count)
                {
                    System.Windows.Forms.MessageBox.Show("選單發生錯誤");
                }
                else
                {
                    ViewParting selected = config.Items[index];
                    changeData(selected);
                }
            }
        }

        /// <summary>
        /// 讀取組態
        /// </summary>
        public static ViewConfig ReadConfig(string configFile)
        {
            // 載入組態檔
            ViewConfig config = ConfigUtility.Read<ViewConfig>(configFile);

            if (config == null)
            {
                config = new ViewConfig();
            }

            if (config.Items == null)
            {
                config.Items = new List<ViewParting>();
            }

            return config;
        }

        #region 生活常用

        /// <summary>
        /// 生活常用
        /// </summary>
        /// <returns></returns>
        public static MenuNode GetNodeCommon(string configFile)
        {
            MenuNode result = new MenuNode
            {
                Id = Guid.NewGuid().ToString(),
                Name = "生活常用",
                Members = new List<MenuNodeMember>(),
            };

            result.Members.Add(GetMemberWeather(result, configFile));
            result.Members.Add(GetMemberDictionary(result, configFile));

            return result;
        }

        /// <summary>
        /// 天氣預報
        /// </summary>
        /// <returns></returns>
        public static MenuNodeMember GetMemberWeather(MenuNode node, string configFile)
        {
            ViewPartingType e = ViewPartingType.Row1Column3;

            return new MenuNodeMember()
            {
                ParentId = node.Id,
                Id = Guid.NewGuid().ToString(),
                Name = "天氣預報",
                ModeType = e,
                ModeCode = (int)e,
                ModeName = e.Name(),
                ConfigFile = configFile
            };
        }

        /// <summary>
        /// 常用辭典
        /// </summary>
        /// <returns></returns>
        public static MenuNodeMember GetMemberDictionary(MenuNode node, string configFile)
        {
            ViewPartingType e = ViewPartingType.Row2Column2;

            return new MenuNodeMember()
            {
                ParentId = node.Id,
                Id = Guid.NewGuid().ToString(),
                Name = "常用辭典",
                ModeType = e,
                ModeCode = (int)e,
                ModeName = e.Name(),
                ConfigFile = configFile
            };
        }

        #endregion 生活常用

        #region 程式開發

        /// <summary>
        /// 程式開發
        /// </summary>
        /// <returns></returns>
        public static MenuNode GetNodeProgram(string configFile)
        {
            MenuNode result = new MenuNode
            {
                Id = Guid.NewGuid().ToString(),
                Name = "程式開發",
                Members = new List<MenuNodeMember>(),
            };

            result.Members.Add(GetMemberRule(result, configFile));
            result.Members.Add(GetMemberSearch(result, configFile));

            return result;
        }

        /// <summary>
        /// 開發規範
        /// </summary>
        /// <returns></returns>
        public static MenuNodeMember GetMemberRule(MenuNode node, string configFile)
        {
            ViewPartingType e = ViewPartingType.Row1Column3;

            return new MenuNodeMember()
            {
                ParentId = node.Id,
                Id = Guid.NewGuid().ToString(),
                Name = "開發規範",
                ModeType = e,
                ModeCode = (int)e,
                ModeName = e.Name(),
                ConfigFile = configFile
            };
        }

        /// <summary>
        /// 編程搜尋
        /// </summary>
        /// <returns></returns>
        public static MenuNodeMember GetMemberSearch(MenuNode node, string configFile)
        {
            ViewPartingType e = ViewPartingType.Row2Column2;

            return new MenuNodeMember()
            {
                ParentId = node.Id,
                Id = Guid.NewGuid().ToString(),
                Name = "編程搜尋",
                ModeType = e,
                ModeCode = (int)e,
                ModeName = e.Name(),
                ConfigFile = configFile
            };
        }

        #endregion 程式開發
    }
}