﻿using System;
using System.Text;
using WorkHelp.Wpf.Models.Windows;

namespace WorkHelp.Wpf.Services
{
    /// <summary>
    /// Window 102 服務功能
    /// </summary>
    public partial class Service102
    {
        /// <summary>
        /// TypeScript 類別轉換為 C# 類別
        /// </summary>
        /// <returns></returns>
        public static string TypeScriptToCSharp(string source, Config102 config)
        {
            (string fileName, string contents) = TsToCs(source);

            config.OutputFile = GetValidFileName(fileName) + ".cs";

            return contents;
        }

        /// <summary>
        /// TypeScript 類別轉換為 C# 類別
        /// </summary>
        /// <returns></returns>
        private static (string fileName, string contents) TsToCs(string source)
        {
            if (string.IsNullOrWhiteSpace(source))
            {
                return (string.Empty, string.Empty);
            }

            StringBuilder contentsBuilder = new StringBuilder(source);
            contentsBuilder
.Replace(@"  /**
   * ", "  /** ")
.Replace(@"
   */", " */")
.Replace(@"/**
 * ", "/** ")
.Replace(@"
 */", " */");

            string contents = contentsBuilder.ToString();
            string[] args = contents.Split(new char[] { '\n' }, StringSplitOptions.None);
            if (args == null || args.Length == 0)
            {
                return (string.Empty, string.Empty);
            }

            string fileName = string.Empty;
            bool isStart = false;
            bool isClass = false;
            StringBuilder builder = new StringBuilder(contents.Length);
            builder.AppendLine("namespace Ticket.Api.Models");
            builder.AppendLine("{");

            foreach (string s in args)
            {
                if (s == null) continue;
                string v = s.Replace("\r", string.Empty);

                if (v.Contains("  /** "))
                {
                    builder.AppendLine("        /// <summary>");
                    builder.AppendLine("        /// " + v.Replace("  /** ", string.Empty).Replace(" */", string.Empty));
                    builder.AppendLine("        /// </summary>");
                }
                else if (v.Contains("/** "))
                {
                    builder.AppendLine("    /// <summary>");
                    builder.AppendLine("    /// " + v.Replace("/** ", string.Empty).Replace(" */", string.Empty));
                    builder.AppendLine("    /// </summary>");
                }
                else if (v.Contains("export interface "))
                {
                    string temp = v.Replace("export interface ", string.Empty).Replace("{", string.Empty).Trim();
                    fileName = GetCSharpClassName(temp);
                    builder.AppendLine("    public class " + fileName);
                    builder.AppendLine("    {");
                }
                else if (v.Contains("}"))
                {
                }
                else if (v.Contains(":"))
                {
                    string[] comments = v.Trim().Split(new char[] { ':' }, StringSplitOptions.None);
                    if (comments.Length == 2)
                    {
                        (string colType, string colName) = GetCSharpField(comments[0], comments[1]);
                        builder.AppendLine("        public " + colType + " " + colName + " { get; set; }");
                        builder.AppendLine();
                    }
                }
            }
            builder.AppendLine("    }");
            builder.AppendLine("}");
            return (fileName, builder.ToString().Trim());
        }

        private static string GetCSharpClassName(string interfaceName)
        {
            string result = "";
            bool nextChange = true;
            foreach (char c in interfaceName)
            {
                if (c == '-' || c == '_')
                {
                    nextChange = true;
                }
                else if (nextChange)
                {
                    result += c.ToString().ToUpper();
                    nextChange = false;
                }
                else
                {
                    result += c;
                }
            }
            return result;
        }

        private static (string colType, string colName) GetCSharpField(string name, string type)
        {
            bool isNull = name.Contains("?");

            string fieldType = type.Replace(";", string.Empty).ToLower().Trim();
            string fieldName = GetCSharpClassName(name).Replace("?", string.Empty).Trim();

            string colType = fieldType;
            string colName = fieldName;

            switch (fieldType)
            {
                case "string":
                    colType = "string";
                    break;

                case "number":
                    colType = isNull ? "int?" : "int";
                    break;

                case "boolean":
                    colType = isNull ? "bool?" : "boll";
                    break;

                case "string | moment":
                    colType = isNull ? "DateTime?" : "DateTime";
                    break;

                case "number[]":
                    colType = isNull ? "int?[]" : "int[]";
                    break;

                case "string[]":
                    colType = "string[]";
                    break;

                default:
                    colType = type.Replace(";", string.Empty).Trim();
                    break;
            }

            return (colType, colName);
        }
    }
}