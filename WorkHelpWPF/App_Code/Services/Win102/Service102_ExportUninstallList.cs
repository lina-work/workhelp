﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WorkHelp.Wpf.Models;
using WorkHelp.Wpf.Models.Windows;

namespace WorkHelp.Wpf.Services
{
    /// <summary>
    /// Window 102 服務功能
    /// </summary>
    public partial class Service102
    {
        /// <summary>
        /// 匯出具有反安裝程式的軟體清單
        /// </summary>
        public static string ExportUninstallList(string contents, Config102 config)
        {
            List<RegModel> source = GetUninstallList();
            List<RegModel> list = FilterUninstallList(source);
            IOrderedEnumerable<RegModel> sortList = list.OrderBy(x => x.DisplayName);

            StringBuilder outputContents = UninstallListToStringbuilder(sortList);

            return outputContents.ToString();
        }

        /// <summary>
        /// 取得反安裝程式清單
        /// </summary>
        /// <returns></returns>
        private static List<RegModel> GetUninstallList()
        {
            List<RegModel> result = new List<RegModel>();

            RegistryKey currentUserKey = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall");
            SetUninstallList(result, currentUserKey);

            RegistryKey root = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine,
                          Environment.Is64BitOperatingSystem
                                     ? RegistryView.Registry64
                                     : RegistryView.Registry32);

            RegistryKey localMachineKey = root.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall");
            SetUninstallList(result, localMachineKey);

            RegistryKey localMachineKey64 = root.OpenSubKey(@"SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall");
            SetUninstallList(result, localMachineKey64);

            return result;
        }

        /// <summary>
        /// 取得反安裝程式清單
        /// </summary>
        /// <returns></returns>
        private static void SetUninstallList(List<RegModel> list, RegistryKey regKey)
        {
            string[] subKeyNames = regKey.GetSubKeyNames().OrderByDescending(x => x).ToArray();

            foreach (string keyName in subKeyNames)
            {
                RegistryKey subKey = regKey.OpenSubKey(keyName);
                object obj = subKey.GetValue("DisplayName");
                if (obj != null)
                {
                    RegModel item = new RegModel
                    {
                        DisplayName = $"{subKey.GetValue("DisplayName")}",
                        InstallDate = $"{subKey.GetValue("InstallDate")}",
                        InstallLocation = $"{subKey.GetValue("InstallLocation")}",
                        DisplayVersion = $"{subKey.GetValue("DisplayVersion")}",
                        Publisher = $"{subKey.GetValue("Publisher")}",
                        DisplayIcon = $"{subKey.GetValue("DisplayIcon")}",
                        UninstallString = $"{subKey.GetValue("UninstallString")}",
                        ModifyPath = $"{subKey.GetValue("ModifyPath")}",
                        RepairPath = $"{subKey.GetValue("RepairPath")}",
                    };

                    list.Add(item);
                }
            }
        }

        /// <summary>
        /// 過濾反安裝程式清單
        /// </summary>
        /// <returns></returns>
        private static List<RegModel> FilterUninstallList(List<RegModel> source)
        {
            List<RegModel> result = new List<RegModel>();

            var query = from e in source
                        group e by new
                        {
                            e.DisplayName,
                            e.InstallDate,
                            e.InstallLocation,
                            e.DisplayVersion,
                            e.Publisher
                        };

            foreach (var q in query)
            {
                result.Add(new RegModel
                {
                    DisplayName = q.Key.DisplayName,
                    InstallDate = q.Key.InstallDate,
                    InstallLocation = q.Key.InstallLocation,
                    DisplayVersion = q.Key.DisplayVersion,
                    Publisher = q.Key.Publisher,
                });
            }

            return result;
        }

        /// <summary>
        /// 將反安裝程式清單轉為 csv 格式字串
        /// </summary>
        /// <param name="sortList"></param>
        /// <returns></returns>
        private static StringBuilder UninstallListToStringbuilder(IOrderedEnumerable<RegModel> sortList)
        {
            StringBuilder builder = new StringBuilder(1024);
            builder.Append("DisplayName" + "\t");
            builder.Append("InstallDate" + "\t");
            builder.Append("InstallLocation" + "\t");
            builder.Append("DisplayVersion" + "\t");
            builder.Append("Publisher" + "\t");
            //builder.Append("DisplayIcon" + "\t");
            //builder.Append("UninstallString" + "\t");
            //builder.Append("ModifyPath" + "\t");
            //builder.Append("RepairPath" + "\t");
            builder.AppendLine();

            foreach (RegModel model in sortList)
            {
                builder.Append($"{model.DisplayName}" + "\t");
                builder.Append($"{model.InstallDate}" + "\t");
                builder.Append($"{model.InstallLocation}" + "\t");
                builder.Append($"{model.DisplayVersion}" + "\t");
                builder.Append($"{model.Publisher}" + "\t");
                //builder.Append($"{model.DisplayIcon}" + "\t");
                //builder.Append($"{model.UninstallString}" + "\t");
                //builder.Append($"{model.ModifyPath}" + "\t");
                //builder.Append($"{model.RepairPath}" + "\t");

                builder.AppendLine();
            }

            return builder;
        }
    }
}