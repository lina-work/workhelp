﻿using System;
using System.Text;
using WorkHelp.Wpf.Models;
using WorkHelp.Wpf.Models.Windows;

namespace WorkHelp.Wpf.Services
{
    /// <summary>
    /// Window 102 服務功能
    /// </summary>
    public partial class Service102
    {
        /// <summary>
        /// 產生 C# 類別
        /// </summary>
        public static string CSharpClass(string contents, Config102 config)
        {
            var rows = contents.Split(new string[] { "\r\n" }, StringSplitOptions.None);
            if (rows == null || rows.Length == 0)
            {
                return string.Empty;
            }

            var list = GetCSharpColumns(rows);
            if (list == null || list.Count == 0)
            {
                return string.Empty;
            }

            var sbBody = new StringBuilder();

            foreach (ColumnModel item in list)
            {
                if (sbBody.Length > 0)
                {
                    sbBody.AppendLine();
                }

                sbBody.AppendLine("        /// <summary>");
                sbBody.AppendLine("        /// " + item.COLUMN_DESCRIPTION);
                sbBody.AppendLine("        /// </summary>");
                sbBody.AppendLine("        public " + item.DATA_TYPE_CSHARP_STRING + " " + item.COLUMN_NAME_ENTITY + " { get; set; }");
            }

            if (config.IsGenerateClass)
            {
                var sbMain = new StringBuilder(sbBody.Length + 256);
                sbMain.AppendLine("using System;");
                sbMain.AppendLine();
                sbMain.AppendLine("namespace SolutionName.ProjectName.Models");
                sbMain.AppendLine("{");

                sbMain.AppendLine("    /// <summary>");
                sbMain.AppendLine("    /// 類別註解");
                sbMain.AppendLine("    /// </summary>");
                sbMain.AppendLine("    public class TempViewModel");
                sbMain.AppendLine("    {");
                sbMain.Append(sbBody);
                sbMain.AppendLine();
                sbMain.AppendLine("    }");
                sbMain.AppendLine("}");

                return sbMain.ToString();
            }
            else
            {
                return sbBody.ToString();
            }
        }
    }
}