﻿using System;
using System.Text;
using WorkHelp.Wpf.Models;
using WorkHelp.Wpf.Models.Windows;

namespace WorkHelp.Wpf.Services
{
    /// <summary>
    /// Window 102 服務功能
    /// </summary>
    public partial class Service102
    {
        /// <summary>
        /// 產生 TypeScript 類別
        /// </summary>
        public static string TypeScriptClass(string contents, Config102 config)
        {
            var rows = contents.Split(new string[] { "\r\n" }, StringSplitOptions.None);
            if (rows == null || rows.Length == 0)
            {
                return string.Empty;
            }
            var list = GetCSharpColumns(rows);
            if (list == null || list.Count == 0)
            {
                return string.Empty;
            }

            var sbBody = new StringBuilder();

            foreach (ColumnModel item in list)
            {
                if (sbBody.Length > 0)
                {
                    sbBody.AppendLine();
                }

                sbBody.AppendLine("  /** " + item.COLUMN_DESCRIPTION + " */");
                sbBody.AppendLine("  " + item.COLUMN_NAME_TYPESCRIPT + "?: " + item.DATA_TYPE_TYPESCRIPT_STRING + ";");
            }

            var sbMain = new StringBuilder(sbBody.Length + 256);

            sbMain.AppendLine("/** 類別註解 */");
            sbMain.AppendLine("export interface TempInfo {");
            sbMain.Append(sbBody);
            sbMain.AppendLine("}");
            return sbMain.ToString();
        }
    }
}