﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using WorkHelp.Wpf.Models.Windows;

namespace WorkHelp.Wpf.Services
{
    /// <summary>
    /// Window 102 服務功能
    /// </summary>
    public partial class Service102
    {
        /// <summary>
        /// 產生 Table 64 支籤
        /// </summary>
        public static string Table_Bracket64(string contents, Config102 config)
        {
            var team_cols = (3 + 2) * 2;
            var round_cols = 6 * 2 + 3; //

            var rows = 64 * 4 - 2;//最後一列不需 4 列
            var cols = team_cols + round_cols;

            var head = new StringBuilder();
            head.AppendLine("<thead>");
            for (int i = 1; i <= 1; i++)
            {
                head.AppendLine("<tr>");
                for (int j = 1; j <= cols; j++)
                {
                    head.Append("<th data-value='" + i + " x " + j + "' onclick='Bracket_Click(this)'>");
                    head.Append("");
                    head.Append("</th>");
                    head.AppendLine();
                }
                head.AppendLine("</tr>");
            }
            head.AppendLine("</thead>");

            var body = new StringBuilder();
            body.AppendLine("<tbody>");
            for (int i = 1; i <= rows; i++ )
            {
                body.AppendLine("<tr>");
                for (int j = 1; j <= cols; j++)
                {
                    body.Append("<td data-value='" + i + " x " + j  + "' onclick='Bracket_Click(this)'>");
                    body.Append("");
                    body.Append("</td>");
                    body.AppendLine();
                }
                body.AppendLine("</tr>");
            }
            body.AppendLine("</tbody>");

            var builder = new StringBuilder();

            builder.AppendLine("<table class='tb-box'>");
            builder.Append(head);
            builder.Append(body);
            builder.AppendLine("</table>");

            return builder.ToString();

        }

    }
}