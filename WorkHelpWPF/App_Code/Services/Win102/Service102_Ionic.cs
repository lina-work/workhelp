﻿using System;
using System.Text;
using WorkHelp.Wpf.Models;
using WorkHelp.Wpf.Models.Windows;

namespace WorkHelp.Wpf.Services
{
    /// <summary>
    /// Window 102 服務功能
    /// </summary>
    public partial class Service102
    {
        /// <summary>
        /// 產生 Ionic Page 指令
        /// </summary>
        public static string IonicPageCli(string contents, Config102 config)
        {
            Action<StringBuilder, string, string> action = (sb, row, endMark) =>
            {
                sb.Append("ionic generate page " + row + endMark);
            };

            return BaseCore(contents, action);
        }

        /// <summary>
        /// 產生 Ionic Form 程式碼片段
        /// </summary>
        public static string IonicFormSnippets(string contents, Config102 config)
        {
            StringBuilder sbTsInitial = new StringBuilder();
            StringBuilder sbTsInitial2 = new StringBuilder();
            StringBuilder sbTsData = new StringBuilder();
            StringBuilder sbTsData2 = new StringBuilder();

            StringBuilder sbHtmlItem = new StringBuilder();

            Action<ColumnModel> action = (item) =>
            {
                string name = item.COLUMN_NAME_TYPESCRIPT;
                string desc = item.COLUMN_DESCRIPTION;

                sbTsInitial.AppendLine("      " + name + ": [null, Validators.required],");
                sbTsInitial2.AppendLine("      " + name + ": [this.selectedItem." + name + ", Validators.required],");

                sbTsData.AppendLine("      MOCKDATA." + name + " = formData." + name + ";");
                sbTsData2.AppendLine("      " + name + ": formData." + name + ",");

                sbHtmlItem.AppendLine("  <ion-item>");
                sbHtmlItem.AppendLine("    <ion-label position=\"floating\">" + desc + "</ion-label>");
                sbHtmlItem.AppendLine("    <ion-input formControlName=\"" + name + "\" type=\"text\"></ion-input>");
                sbHtmlItem.AppendLine("  </ion-item>");
            };

            BaseCore(contents, action);

            string formName = config.FormName ?? "viewForm";

            StringBuilder sbTsMain = new StringBuilder();

            #region Typescript

            sbTsMain.AppendLine("import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';");
            sbTsMain.AppendLine();

            sbTsMain.AppendLine("  /** 表單群組 */");
            sbTsMain.AppendLine("  " + formName + ": FormGroup;");
            sbTsMain.AppendLine("  /** 已選取的資料 */");
            sbTsMain.AppendLine("  selectedItem: TypeInfo = MockData;");
            sbTsMain.AppendLine();

            sbTsMain.AppendLine("  /** 構造函數 */");
            sbTsMain.AppendLine("  constructor(");
            sbTsMain.AppendLine("    /** 表單建構器 */");
            sbTsMain.AppendLine("    private formBuilder: FormBuilder,");
            sbTsMain.AppendLine("  ) {");
            sbTsMain.AppendLine();
            sbTsMain.AppendLine("    this." + formName + " = this.formBuilder.group({");
            sbTsMain.Append(sbTsInitial);
            sbTsMain.AppendLine("    });");
            sbTsMain.AppendLine();
            sbTsMain.AppendLine("  }");
            sbTsMain.AppendLine();

            sbTsMain.AppendLine("  /** 構造函數 */");
            sbTsMain.AppendLine("  constructor(");
            sbTsMain.AppendLine("    /** 表單建構器 */");
            sbTsMain.AppendLine("    private formBuilder: FormBuilder,");
            sbTsMain.AppendLine("  ) {");
            sbTsMain.AppendLine();
            sbTsMain.AppendLine("    this." + formName + " = this.formBuilder.group({");
            sbTsMain.Append(sbTsInitial2);
            sbTsMain.AppendLine("    });");
            sbTsMain.AppendLine();
            sbTsMain.AppendLine("  }");
            sbTsMain.AppendLine();

            sbTsMain.AppendLine("  /** 組件初始化 */");
            sbTsMain.AppendLine("  ngOnInit() {");
            sbTsMain.AppendLine("  }");
            sbTsMain.AppendLine();

            sbTsMain.AppendLine("  /** 送出表單 */");
            sbTsMain.AppendLine("  onFormSubmit() {");
            sbTsMain.AppendLine("    if (confirm(\"是否確認修改【資料】？\")) {");
            sbTsMain.AppendLine("      let formData = this." + formName + ".value;");
            sbTsMain.Append(sbTsData);
            sbTsMain.AppendLine("    }");
            sbTsMain.AppendLine("    alert(\"儲存成功!!即將回到會員中心...\");");
            sbTsMain.AppendLine("    this.router.navigateByUrl(\"/member\");");
            sbTsMain.AppendLine("  }");
            sbTsMain.AppendLine();

            sbTsMain.AppendLine("  /** 取得表單資料 */");
            sbTsMain.AppendLine("  getFormItem(): TypeInfo {");
            sbTsMain.AppendLine("    let formData = this." + formName + ".value;");
            sbTsMain.AppendLine("    let viewItem: TypeInfo = {");
            sbTsMain.Append(sbTsData2);
            sbTsMain.AppendLine("    }");
            sbTsMain.AppendLine("    return viewItem;");
            sbTsMain.AppendLine("  }");

            sbTsMain.AppendLine();

            #endregion Typescript

            StringBuilder sbHtmlMain = new StringBuilder();

            #region Html

            sbHtmlMain.AppendLine("  <form [formGroup]=\"" + formName + "\" (ngSubmit)=\"onFormSubmit()\">");
            sbHtmlMain.Append(sbHtmlItem);
            sbHtmlMain.AppendLine("    <ion-button expand=\"full\" color=\"primary\" type=\"submit\" [disabled]=\"" + formName + ".invalid\" >");
            sbHtmlMain.AppendLine("      確定");
            sbHtmlMain.AppendLine("    </ion-button> ");
            sbHtmlMain.AppendLine("  </form>");

            #endregion Html

            StringBuilder sbMain = new StringBuilder();
            sbMain.AppendLine("【Module】");
            sbMain.AppendLine();
            sbMain.AppendLine("import { FormsModule, ReactiveFormsModule } from '@angular/forms';");
            sbMain.AppendLine();
            sbMain.AppendLine("    FormsModule,");
            sbMain.AppendLine("    ReactiveFormsModule,");
            sbMain.AppendLine();
            sbMain.AppendLine("=================================");
            sbMain.AppendLine("【Page】");
            sbMain.AppendLine();
            sbMain.Append(sbTsMain);
            sbMain.AppendLine();
            sbMain.AppendLine("=================================");
            sbMain.AppendLine("【Html】");
            sbMain.AppendLine();
            sbMain.Append(sbHtmlMain);

            return sbMain.ToString();
        }

        /// <summary>
        /// 產生 Ionic Form 程式碼片段
        /// </summary>
        public static string IonicFormSnippetsFromInterface(string source, Config102 config)
        {
            if (string.IsNullOrWhiteSpace(source))
            {
                return string.Empty;
            }

            StringBuilder contentsBuilder = new StringBuilder(source);
            contentsBuilder
.Replace(@"  /**
   * ", "  /** ")
.Replace(@"
   */", " */")
.Replace(@"/**
 * ", "/** ")
.Replace(@"
 */", " */");

            string contents = contentsBuilder.ToString();
            string[] args = contents.Split(new string[] { "\r\n" }, StringSplitOptions.None);
            if (args == null || args.Length == 0)
            {
                return string.Empty;
            }

            StringBuilder builder = new StringBuilder(contents.Length);

            string defaultDesc = "欄位說明";
            string lastDesc = defaultDesc;

            foreach (string s in args)
            {
                if (s == null) continue;
                string v = s;

                if (v.Contains("  /** "))
                {
                    lastDesc = v.Replace("  /** ", string.Empty).Replace(" */", string.Empty);
                }
                else if (v.Contains("/** "))
                {
                    lastDesc = v.Replace("/** ", string.Empty).Replace(" */", string.Empty);
                }
                else if (v.Contains(":"))
                {
                    string[] comments = v.Trim().Split(new char[] { ':' }, StringSplitOptions.None);
                    if (comments.Length == 2)
                    {
                        (string colType, string colName) = GetCSharpField(comments[0], comments[1]);
                        builder.AppendLine($"{colName} {lastDesc} {colType}");
                        lastDesc = defaultDesc;
                    }
                }
            }
            if (builder.Length == 0)
            {
                return string.Empty;
            }
            else
            {
                return IonicFormSnippets(builder.ToString(), config);
            }
        }
    }
}