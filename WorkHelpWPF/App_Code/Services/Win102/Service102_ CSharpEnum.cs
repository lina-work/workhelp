﻿using System;
using System.Text;
using WorkHelp.Wpf.Models.Windows;

namespace WorkHelp.Wpf.Services
{
    /// <summary>
    /// Window 102 服務功能
    /// </summary>
    public partial class Service102
    {
        /// <summary>
        /// 產生 C# 列舉類別
        /// </summary>
        public static string CSharpEnum(string contents, Config102 config)
        {
            var rows = contents.Split(new string[] { "\r\n" }, StringSplitOptions.None);
            if (rows == null || rows.Length == 0)
            {
                return string.Empty;
            }

            var list = GetCSharpEnumColumns(rows);
            if (list == null || list.Count == 0)
            {
                return string.Empty;
            }

            var sbBody = new StringBuilder();

            foreach (var item in list)
            {
                if (sbBody.Length > 0)
                {
                    sbBody.AppendLine();
                }

                sbBody.AppendLine("        /// <summary>");
                sbBody.AppendLine("        /// " + item.Description);
                sbBody.AppendLine("        /// </summary>");
                sbBody.AppendLine("        [Description(\"" + item.Description + "\")]");
                sbBody.AppendLine("        " + item.EnumName + " = " + item.Code + ",");
            }

            if (config.IsGenerateClass)
            {
                var typeName = list[0].TypeName;
                var sbMain = new StringBuilder(sbBody.Length + 256);
                sbMain.AppendLine("namespace SolutionName.ProjectName.Enums");
                sbMain.AppendLine("{");

                sbMain.AppendLine("    /// <summary>");
                sbMain.AppendLine("    /// 列舉型別註解");
                sbMain.AppendLine("    /// </summary>");
                sbMain.AppendLine("    public enum " + typeName);
                sbMain.AppendLine("    {");
                sbMain.Append(sbBody);
                sbMain.AppendLine();
                sbMain.AppendLine("    }");
                sbMain.AppendLine("}");

                return sbMain.ToString();
            }
            else
            {
                return sbBody.ToString();
            }
        }
    }
}