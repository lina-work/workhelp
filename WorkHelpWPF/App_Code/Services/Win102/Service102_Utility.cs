﻿using System;
using System.Collections.Generic;
using System.Text;
using WorkHelp.Wpf.Models;

namespace WorkHelp.Wpf.Services
{
    /// <summary>
    /// Window 102 服務功能
    /// </summary>
    public partial class Service102
    {
        /// <summary>
        /// C# 快速剔除字符串中不合法的檔案名稱或檔案路徑
        /// </summary>
        /// <returns></returns>
        public static string GetValidFileName(string name)
        {
            StringBuilder builder = new StringBuilder(name);
            foreach (char c in System.IO.Path.GetInvalidPathChars())
            {
                builder.Replace(c.ToString(), string.Empty);
            }
            return builder.ToString();
        }

        /// <summary>
        /// 取得 CS 欄位清單
        /// </summary>
        /// <param name="rows"></param>
        /// <returns></returns>
        private static List<ColumnModel> GetCSharpColumns(string[] rows)
        {
            List<ColumnModel> result = new List<ColumnModel>();
            foreach (string row in rows)
            {
                if (!string.IsNullOrWhiteSpace(row))
                {
                    result.Add(GetCSharpColumn(row));
                }
            }
            return result;
        }

        /// <summary>
        /// 取得 CS 欄位項目
        /// </summary>
        /// <param name="rows"></param>
        /// <returns></returns>
        private static ColumnModel GetCSharpColumn(string row)
        {
            // Format
            // 欄位名稱 欄位註解 欄位類型

            ColumnModel result = new ColumnModel();
            string[] cols = row.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            if (cols != null && cols.Length > 0)
            {
                result.IS_NULLABLE = row.Contains("?") ? "YES" : "NO";

                if (cols.Length > 0)
                {
                    result.COLUMN_NAME = cols[0];
                }
                if (cols.Length > 1)
                {
                    result.COLUMN_DESCRIPTION = cols[1];
                }
                if (cols.Length > 2)
                {
                    result.DATA_TYPE = cols[2];
                }

                if (string.IsNullOrWhiteSpace(result.COLUMN_DESCRIPTION))
                {
                    result.COLUMN_DESCRIPTION = "欄位說明";
                }
                if (string.IsNullOrWhiteSpace(result.DATA_TYPE))
                {
                    result.DATA_TYPE = "nvarchar";
                }

                result.DATA_TYPE = result.DATA_TYPE.ToLower();

                Win401Service.AnalysisColumn(result);
                Win401Service.CSharpMapperColumn(result);
            }
            return result;
        }

        /// <summary>
        /// 取得 CS 列舉項目
        /// </summary>
        /// <returns></returns>
        private static List<EnumColumnModel> GetCSharpEnumColumns(string[] rows)
        {
            List<EnumColumnModel> result = new List<EnumColumnModel>();
            foreach (string row in rows)
            {
                if (!string.IsNullOrWhiteSpace(row))
                {
                    result.Add(GetCSharpEnumColumn(row));
                }
            }
            return result;
        }

        /// <summary>
        /// 取得 CS 列舉項目
        /// </summary>
        /// <param name="rows"></param>
        /// <returns></returns>
        private static EnumColumnModel GetCSharpEnumColumn(string row)
        {
            // Format
            // 欄位名稱 欄位註解 欄位類型

            EnumColumnModel result = new EnumColumnModel();
            string[] cols = row.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            if (cols != null && cols.Length > 0)
            {
                if (cols.Length > 0)
                {
                    result.TypeName = cols[0];
                }
                if (cols.Length > 1)
                {
                    result.EnumName = cols[1];
                }
                if (cols.Length > 2)
                {
                    result.Code = Convert.ToInt32(cols[2]);
                }
                if (cols.Length > 3)
                {
                    result.Description = cols[3];
                }
                if (string.IsNullOrWhiteSpace(result.Description))
                {
                    result.Description = "列舉註解";
                }
            }
            return result;
        }

        /// <summary>
        /// 簡易版型
        /// </summary>
        private static string BaseCore(string contents, Action<StringBuilder, string, string> appendAction)
        {
            var rows = contents.Split(new string[] { "\r\n" }, StringSplitOptions.None);
            if (rows == null || rows.Length == 0)
            {
                return string.Empty;
            }

            var sb = new StringBuilder();
            var currentIndex = -1;
            var endIndex = rows?.Length - 1;
            var endMark = "\r\n";

            foreach (string row in rows)
            {
                currentIndex++;

                if (currentIndex == endIndex)
                {
                    endMark = string.Empty;
                }

                if (string.IsNullOrWhiteSpace(row))
                {
                    sb.Append(row + endMark);
                    continue;
                }

                appendAction(sb, row, endMark);
            }

            return sb.ToString();
        }

        ///// <summary>
        ///// 簡易版型
        ///// </summary>
        //private static void BaseCore(string contents, Action<string, string> appendAction)
        //{
        //    var rows = contents.Split(new string[] { "\r\n" }, StringSplitOptions.None);
        //    if (rows != null && rows.Length > 0)
        //    {
        //        var currentIndex = -1;
        //        var endIndex = rows?.Length - 1;
        //        var endMark = "\r\n";

        //        foreach (string row in rows)
        //        {
        //            currentIndex++;

        //            if (currentIndex == endIndex)
        //            {
        //                endMark = string.Empty;
        //            }

        //            appendAction(row, endMark);
        //        }
        //    }
        //}

        /// <summary>
        /// 簡易版型
        /// </summary>
        private static void BaseCore(string contents, Action<ColumnModel> appendAction)
        {
            var rows = contents.Split(new string[] { "\r\n" }, StringSplitOptions.None);
            if (rows != null && rows.Length > 0)
            {
                var list = GetCSharpColumns(rows);

                foreach (var item in list)
                {
                    appendAction(item);
                }
            }
        }
    }
}