﻿using System;
using System.Text;
using WorkHelp.Wpf.Models.Windows;

namespace WorkHelp.Wpf.Services
{
    /// <summary>
    /// Window 102 服務功能
    /// </summary>
    public partial class Service102
    {
        /// <summary>
        /// 等號左右交換
        /// </summary>
        public static string EqualSignExchange(string contents, Config102 config)
        {
            var rows = contents.Split(new string[] { "\r\n" }, StringSplitOptions.None);
            if (rows == null || rows.Length == 0)
            {
                return string.Empty;
            }

            var sb = new StringBuilder();
            var currentIndex = -1;
            var endIndex = rows?.Length - 1;
            var endMark = "\r\n";

            foreach (string row in rows)
            {
                currentIndex++;

                if (currentIndex == endIndex)
                {
                    endMark = string.Empty;
                }

                if (string.IsNullOrWhiteSpace(row))
                {
                    sb.Append(row + endMark);
                    continue;
                }

                var tags = row.Replace(";", string.Empty).Split('=');

                if (tags == null || tags.Length != 2)
                {
                    sb.Append(row + endMark);
                    continue;
                }

                string left = config.IsCheckTrim
                    ? tags[1].Trim().Replace("Trim()", string.Empty)
                    : tags[1].Trim();

                string right = config.IsCheckTrim
                    ? tags[0].Trim() + ".Trim()"
                    : tags[0].Trim();

                sb.Append(left + " = " + right + ";" + endMark);
            }

            return sb.ToString();
        }
    }
}