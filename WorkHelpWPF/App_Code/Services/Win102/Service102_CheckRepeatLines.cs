﻿using System;
using System.Collections.Generic;
using System.Text;
using WorkHelp.Wpf.Models.Windows;

namespace WorkHelp.Wpf.Services
{
    /// <summary>
    /// Window 102 服務功能
    /// </summary>
    public partial class Service102
    {
        /// <summary>
        /// 檢查重複行
        /// </summary>
        public static string CheckRepeatLines(string contents, Config102 config)
        {
            var args = contents.Split(new string[] { "\r\n" }, StringSplitOptions.None);
            var list = new List<string>();
            var sbRepeats = new StringBuilder();
            var sbClears = new StringBuilder();
            foreach (var s in args)
            {
                var tmp = s.Trim();
                if (!string.IsNullOrEmpty(tmp))
                {
                    if (list.Exists(item => item.Equals(tmp, StringComparison.CurrentCultureIgnoreCase)))
                    {
                        // 重複的資料行
                        sbRepeats.AppendLine(tmp);
                    }
                    else
                    {
                        // 過濾重複後的資料
                        sbClears.AppendLine(tmp);

                        list.Add(tmp);
                    }
                }
            }

            if (sbRepeats.Length == 0)
            {
                return "查無重複行";
            }
            else
            {
                var sb = new StringBuilder(sbRepeats.Length + sbClears.Length + 128);
                sb.AppendLine("重複的資料：");
                sb.Append(sbRepeats);

                sb.AppendLine();
                sb.AppendLine("=============================");
                sb.AppendLine();
                sb.AppendLine("過濾後的資料：");
                sb.Append(sbClears);

                return sb.ToString();
            }
        }
    }
}