﻿using System;
using System.Text;
using WorkHelp.Wpf.Models.Windows;

namespace WorkHelp.Wpf.Services
{
    /// <summary>
    /// Window 102 服務功能
    /// </summary>
    public partial class Service102
    {
        /// <summary>
        /// 產生 C# 列舉類別
        /// </summary>
        public static string PdfReturn(string contents, Config102 config)
        {
            var rows = contents.Split(new string[] { "\r\n" }, StringSplitOptions.None);
            if (rows == null || rows.Length == 0)
            {
                return string.Empty;
            }

            var builder = new StringBuilder();

            foreach (var row in rows)
            {
                var data = row.Trim();
                if (data.EndsWith("."))
                {
                    builder.Append(row);
                    builder.AppendLine();
                    builder.AppendLine();
                }
                else
                {
                    builder.Append(row + " ");
                }
            }
            return builder.ToString();

        }
    }
}