﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using WorkHelp.Wpf.Models.Windows;

namespace WorkHelp.Wpf.Services
{
    /// <summary>
    /// Window 102 服務功能
    /// </summary>
    public partial class Service102
    {
        /// <summary>
        /// 產生 C# 列舉類別
        /// </summary>
        public static string HtmlTag(string contents, Config102 config)
        {
            var builder = new StringBuilder();
            IEnumerable<Tuple<string, string>> list = ExeSplitHtmlTag(contents);
            foreach (var row in list)
            {
                builder.AppendLine(row.Item1 + "\t\t" + row.Item2);
            }
            return builder.ToString();

        }

        private static IEnumerable<Tuple<string, string>> ExeSplitHtmlTag(string html)
        {
            Regex r = new Regex(@"<a.*?href=(""|')(?<href>.*?)(""|').*?>(?<value>.*?)</a>");

            foreach (Match match in r.Matches(html))
            {
                yield return new Tuple<string, string>(
                    match.Groups["href"].Value, match.Groups["value"].Value);
            }
        }

    }
}