﻿using System;
using System.Text;
using WorkHelp.Wpf.Models.Windows;

namespace WorkHelp.Wpf.Services
{
    /// <summary>
    /// Window 102 服務功能
    /// </summary>
    public partial class Service102
    {
        /// <summary>
        /// 產生樣板字串
        /// </summary>
        public static string Format(string contents, Config102 config)
        {
            var rows = contents.Split(new string[] { "\r\n" }, StringSplitOptions.None);
            if (rows == null || rows.Length == 0)
            {
                return string.Empty;
            }

            var sbBody = new StringBuilder();
            var prefix = config.DefaultValue;
            var empty = prefix.Replace("{}", string.Empty);

            if (empty == "builder.AppendLine($\"\");")
            {
                empty = "builder.AppendLine();";
            }
            else if (empty == "builder.AppendLine(\"\");")
            {
                empty = "builder.AppendLine();";
            }

            foreach (var row in rows)
            {
                var temp = row.Replace("\t", "    ").Replace("\"", "\\\"");

                if (string.IsNullOrWhiteSpace(temp))
                {
                    sbBody.AppendLine(empty);
                }
                else
                {
                    if (config.IsCheckFormat)
                    {
                        if (temp.IndexOf('{') > -1 || temp.IndexOf('}') > -1)
                        {
                            sbBody.AppendLine(prefix.Replace("$\"{}\"", "\"" + temp + "\""));
                        }
                        else
                        {
                            sbBody.AppendLine(prefix.Replace("{}", temp));
                        }
                    }
                    else
                    {
                        sbBody.AppendLine(prefix.Replace("{}", temp));
                    }
                }
            }

            return sbBody.ToString();
        }

        /// <summary>
        /// 產生書名
        /// </summary>
        public static string BookName(string contents, Config102 config)
        {
            return contents;
        }
    }
}