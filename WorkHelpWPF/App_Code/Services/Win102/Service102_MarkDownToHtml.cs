﻿using Markdig;
using System;
using System.Text;
using WorkHelp.Wpf.Models.Windows;

namespace WorkHelp.Wpf.Services
{
    /// <summary>
    /// Window 102 服務功能
    /// </summary>
    public partial class Service102
    {
        /// <summary>
        /// MarkDown 轉為 Html
        /// </summary>
        public static string MarkDownToHtml(string contents, Config102 config)
        {
            string htmlContents = string.Empty;
            string result = string.Empty;
            string disabled = config.IsDisabled ? "disabled='disabled'" : string.Empty;

            try
            {
                htmlContents = Markdown.ToHtml(contents);
            }
            catch (Exception ex)
            {
                htmlContents = string.Empty;
                result = ex.Message;
            }

            if (!string.IsNullOrWhiteSpace(htmlContents))
            {
                StringBuilder buider = new StringBuilder(htmlContents);
                buider.Replace("[x] ", $"<input type='checkbox' checked='checked' {disabled}>");
                buider.Replace("[ ] ", $"<input type='checkbox' {disabled}>");
                result = buider.ToString();
            }

            return result;
        }
    }
}