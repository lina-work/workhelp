﻿using System;
using System.Text;
using WorkHelp.Wpf.Models.Windows;

namespace WorkHelp.Wpf.Services
{
    /// <summary>
    /// Window 102 服務功能
    /// </summary>
    public partial class Service102
    {
        /// <summary>
        /// 產生 C# 列舉類別
        /// </summary>
        public static string SqlCastDateTime(string contents, Config102 config)
        {
            var rows = contents.Split(new string[] { "\r\n" }, StringSplitOptions.None);
            if (rows == null || rows.Length == 0)
            {
                return string.Empty;
            }

            var builder = new StringBuilder();

            foreach (var row in rows)
            {
                string message = row.Trim();
                builder.AppendLine(SqlCastDateTime1(message));
            }
            return builder.ToString();

        }

        public static string SqlCastDateTime1(string message)
        {
            var builder = new StringBuilder(256);
            if (!string.IsNullOrWhiteSpace(message) && message.Contains("CAST"))
            {
                string[] args = message.Split(new string[] { "CAST" }, StringSplitOptions.RemoveEmptyEntries);
                if (args == null || args.Length == 0)
                {
                    return message;
                }

                foreach (string piece in args)
                {
                    if (builder.Length > 0)
                    {
                        builder.Append("CAST");
                    }
                    if (piece.Contains("AS DateTime"))
                    {
                        //(N'2019-03-13T092549.380' AS DateTime), 
                        string hh = piece.Substring(14, 2);
                        string mm = piece.Substring(16, 2);
                        string ss = piece.Substring(18, 2);
                        string end = piece.Substring(20, piece.Length - 20);
                        string newPiece = piece.Substring(0, 14) + hh + ":" + mm + ":" + ss + end;
                        builder.Append(newPiece);
                    }
                    else
                    {
                        builder.Append(piece);
                    }
                }
                return builder.ToString();
            }
            else
            {
                return message;
            }
        }
    }
}