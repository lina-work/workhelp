﻿using System.Text;
using WorkHelp.Wpf.Models;

namespace WorkHelp.Wpf.Services
{
    /// <summary>
    /// Window 102 服務功能
    /// </summary>
    public partial class Service102
    {
        /// <summary>
        /// 產生欄位清單
        /// </summary>
        public static void GenerateColumns(SchemaModel model)
        {
            StringBuilder sb = new StringBuilder();

            model.ColumnList.ForEach(x =>
            {
                if (x.COLUMN_DESCRIPTION == null)
                {
                    sb.AppendLine($"{x.COLUMN_NAME_TYPESCRIPT} 欄位說明 {x.DATA_TYPE}");
                }
                else
                {
                    sb.AppendLine($"{x.COLUMN_NAME_TYPESCRIPT} {x.COLUMN_DESCRIPTION.Replace(" ", "_")} {x.DATA_TYPE}");
                }
            });

            model.FileContent = sb.ToString();
        }

        /// <summary>
        /// 產生查詢指令
        /// </summary>
        public static void GenerateSelectScript(SchemaModel model)
        {
            StringBuilder sb = new StringBuilder();
            int endIndex = model.ColumnList.Count - 1;

            sb.AppendLine($"SELECT ");
            for (int i = 0; i < model.ColumnList.Count; i++)
            {
                ColumnModel x = model.ColumnList[i];
                if (i != endIndex)
                {
                    sb.AppendLine($"    [{x.COLUMN_NAME}],");
                }
                else
                {
                    sb.AppendLine($"    [{x.COLUMN_NAME}]");
                }
            }

            sb.AppendLine($"FROM [{model.MainName}] WITH(NOLOCK)");

            model.FileContent = sb.ToString();
        }
    }
}