﻿using System;
using System.Text;
using WorkHelp.Wpf.Models.Windows;

namespace WorkHelp.Wpf.Services
{
    /// <summary>
    /// Window 102 服務功能
    /// </summary>
    public partial class Service102
    {
        /// <summary>
        /// C# 類別轉換為 TypeScript 類別
        /// </summary>
        /// <returns></returns>
        public static string CSharpToTypeScript(string contents, Config102 config)
        {
            if (string.IsNullOrWhiteSpace(contents))
            {
                return string.Empty;
            }

            string[] args = contents.Split(new char[] { '\n' }, StringSplitOptions.None);
            if (args == null || args.Length == 0)
            {
                return string.Empty;
            }
            bool isStart = false;
            bool isClass = false;
            StringBuilder builder = new StringBuilder(contents.Length);
            foreach (string s in args)
            {
                if (s == null) continue;
                string v = s.Replace("\r", string.Empty).Trim();
                if (v.Contains("namespace "))
                {
                    // 不處理
                }
                else if (v.Contains("using"))
                {
                    // 不處理，C# 函式庫
                }
                else if (v.StartsWith("["))
                {
                    // 不處理，Entity Attributes
                }
                else if (v.Contains("public class "))
                {
                    string className = v.Replace("public class ", string.Empty).Trim();

                    builder.AppendLine("export interface " + GetTsClassName(className));
                }
                else if (v.Contains("public partial class "))
                {
                    string className = v.Replace("public partial class ", string.Empty).Trim();

                    builder.AppendLine("export interface " + GetTsClassName(className));
                }
                else if (v.Contains("/// <summary>"))
                {
                    builder.AppendLine(v.Replace("/// <summary>", "/**"));
                }
                else if (v.Contains("/// </summary>"))
                {
                    builder.AppendLine(v.Replace("/// </summary>", " */"));
                }
                else if (v.Contains("/// "))
                {
                    builder.AppendLine(v.Replace("/// ", " * "));
                }
                else if (v.Contains("{ get; set; }"))
                {
                    string fieldComment = v.Replace(" { get; set; }", string.Empty).Replace("public ", string.Empty).Trim();
                    string[] comments = fieldComment.Split(new char[] { ' ' }, StringSplitOptions.None);
                    if (comments.Length == 2)
                    {
                        builder.AppendLine("  " + GetTypeScriptField(comments[0], comments[1]));
                    }
                }
                else if (v.Contains("{ set; get; }"))
                {
                    string fieldComment = v.Replace(" { set; get; }", string.Empty).Replace("public ", string.Empty).Trim();
                    string[] comments = fieldComment.Split(new char[] { ' ' }, StringSplitOptions.None);
                    if (comments.Length == 2)
                    {
                        builder.AppendLine("  " + GetTypeScriptField(comments[0], comments[1]));
                    }
                }
                else
                {
                    builder.AppendLine(v);
                }
            }
            builder
.Replace(@"
/**
 * ", "  /** ")
.Replace(@"
 */", " */")
.Replace(@"}
}", "}")
.Replace("{  /**", "  /**");

            return builder.ToString().Trim();
        }

        private static string GetTsClassName(string name)
        {
            string result = "";
            foreach (char c in name)
            {
                if (result.Length > 0)
                {
                    if (char.IsUpper(c))
                    {
                        result += "-";
                    }
                }
                result += c.ToString().ToLower();
            }
            return result;
        }

        private static string GetTypeScriptField(string type, string name)
        {
            string col = GetTypeScriptColName(name);
            switch (type.ToLower())
            {
                case "string":
                    return $"{col}?: {type};";

                case "bool":
                    return $"{col}?: boolean;";
                case "bool?":
                    return $"{col}?: boolean;";

                case "int":
                    return $"{col}?: number;";
                case "int?":
                    return $"{col}?: number;";
                case "double":
                    return $"{col}?: number;";
                case "double?":
                    return $"{col}?: number;";
                case "decimal":
                    return $"{col}?: number;";
                case "decimal?":
                    return $"{col}?: number;";

                case "datetime":
                    return $"{col}?: string | Moment;";
                case "datetime?":
                    return $"{col}?: string | Moment;";

                case "guid":
                    return $"{col}?: string;";

                case "timestamp":
                case "byte[]":
                    return $"{col}?: any;";

                default:
                    return $"{col}?: {type};";
            }
            //string MerchantID
        }

        private static string GetTypeScriptColName(string name)
        {
            bool isAllUp = true;
            foreach (char c in name)
            {
                if (char.IsLetter(c) && char.IsLower(c))
                {
                    isAllUp = false;
                    break;
                }
            }

            string col = isAllUp
                ? name.ToLower()
                : name[0].ToString().ToLower() + name.Substring(1, name.Length - 1);
            return col;
        }
    }
}