﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using WorkHelp.Wpf.Models.Windows;

namespace WorkHelp.Wpf.Services
{
    /// <summary>
    /// Window 102 服務功能
    /// </summary>
    public partial class Service102
    {
        private class TGam01Lv1
        { 
            public string in_l1 { get; set; }
            public List<TGam01Lv2> Nodes { get; set; }
        }

        private class TGam01Lv2
        {
            public string in_l2 { get; set; }
            public string player_count { get; set; }
            public List<TGam01Player> Nodes { get; set; }
        }

        private class TGam01Player
        {
            public string in_name { get; set; }
            public string in_current_org { get; set; }
            public string memo { get; set; }
        }

        private static string GetGame01Val(string[] arr, int idx)
        {
            if (arr == null || arr.Length == 0 || idx >= arr.Length) return "";
            return arr[idx].Trim();
        }

        /// <summary>
        /// 秩序冊轉檔
        /// </summary>
        public static string Game01(string contents, Config102 config)
        {
            var rows = contents.Split(new string[] { "\r\n" }, StringSplitOptions.None);
            if (rows == null || rows.Length == 0)
            {
                return string.Empty;
            }

            var l2s = new string[] 
            {
                "第一級",
                "第二級",
                "第三級",
                "第四級",
                "第五級",
                "第六級",
                "第七級",
                "第八級",
                "第九級",
                "第十級",
                "第十一級",
                "第十二級",
                "初段",
                "貳段",
            };

            TGam01Lv1 obj = new TGam01Lv1
            {
                in_l1 = "",
                Nodes = new List<TGam01Lv2>(),
            };

            TGam01Lv2 obj2 = null;


            int no = 0;
            foreach (var row in rows)
            {
                var cols = row.Split(new string[] { "\t" }, StringSplitOptions.None);
                if (cols == null || cols.Length == 0) continue;

                if (no == 0)
                {
                    obj.in_l1 = GetGame01Val(cols, 0);
                }
                else
                {
                    string in_l2 = GetGame01Val(cols, 0).Trim();
                    string count = GetGame01Val(cols, 1).Trim();
                    if (l2s.Contains(in_l2))
                    {
                        if (obj2 == null || obj2.in_l2 != in_l2)
                        {
                            obj2 = new TGam01Lv2
                            {
                                in_l2 = in_l2,
                                player_count = count,
                                Nodes = new List<TGam01Player>()
                            };
                            obj.Nodes.Add(obj2);
                        }
                    }
                    else
                    {
                        string org = cols[0].Trim();

                        for (int i = 1; i < cols.Length; i++)
                        {
                            string names = cols[i];
                            string name = names;
                            string memo = "";

                            if (names.Contains("/"))
                            {
                                var nr = names.Split('/');
                                if (nr != null && nr.Length > 1)
                                {
                                    name = nr[0].Trim();
                                    memo = nr[1].Trim();
                                }
                            }

                            if (name != "")
                            {
                                obj2.Nodes.Add(new TGam01Player
                                {
                                    in_current_org = org,
                                    in_name = name,
                                    memo = memo
                                });
                            }
                        }
                    }
                }
                no++;
            }

            var builder = new StringBuilder();

            for(int i = 0; i < obj.Nodes.Count; i++)
            {
                var lv2 = obj.Nodes[i];
                for (int j = 0; j < lv2.Nodes.Count; j++)
                {
                    var pno = j + 1;
                    var player = lv2.Nodes[j];
                    builder.AppendLine(obj.in_l1 
                        + "\t" + lv2.in_l2 
                        + "\t" + lv2.player_count 
                        + "\t" + pno
                        + "\t" + player.in_current_org 
                        + "\t" + player.in_name
                        + "\t" + player.memo
                        );
                }
            }
            builder.AppendLine();

            return builder.ToString();

        }

    }
}