﻿using System;
using System.Text;
using WorkHelp.Wpf.Models.Windows;

namespace WorkHelp.Wpf.Services
{
    /// <summary>
    /// Window 102 服務功能
    /// </summary>
    public partial class Service102
    {
        /// <summary>
        /// 輸出明年行事曆
        /// </summary>
        public static string YearCalendar(string contents, Config102 config)
        {
            DateTime dt = new DateTime(DateTime.Now.Year + 1, 1, 1);
            DateTime dtS = dt.AddDays(-1 * dt.DayOfWeek.GetHashCode());
            DateTime dtE = dt.AddYears(1);

            DateTime dtCurrent = dtS.AddDays(-1);

            var sb = new StringBuilder();

            while (dtCurrent < dtE)
            {
                for (int i = 0; i < 7; i++)
                {
                    dtCurrent = dtCurrent.AddDays(1);
                    sb.Append(dtCurrent.ToString("yyyy/MM/dd") + "\t");
                }
                sb.AppendLine();
                sb.AppendLine();
            }
            return sb.ToString();
        }
    }
}