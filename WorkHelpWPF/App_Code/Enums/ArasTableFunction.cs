﻿namespace WorkHelp.Wpf.Enums
{
    /// <summary>
    /// 資料表功能列舉
    /// </summary>
    public enum ArasTableFunction
    {
        /// <summary>
        /// 未選取
        /// </summary>
        None = 0,

        /// <summary>
        /// 分析 ItemType
        /// </summary>
        AnalyzeItemType = 1,

        /// <summary>
        /// 過濾出不要的 Method
        /// </summary>
        IgnoreMethod = 2,

        /// <summary>
        /// 分析 Method
        /// </summary>
        AnalyzeMethod = 11,

        /// <summary>
        /// 分析 Ignore Method 是否包含新 Method
        /// </summary>
        AnalyzeIgnoreMethod = 12,

        /// <summary>
        /// 分析 Method 並輸出細項
        /// </summary>
        AnalyzeMethodDetail = 21,

        /// <summary>
        /// 分析 C# 並輸出細項
        /// </summary>
        AnalyzeCSharpPage = 31,

        /// <summary>
        /// 分析 ItemType Property 內容
        /// </summary>
        AnalyzeItemTypeProperty = 32,

        /// <summary>
        /// 檢查並輸出 ItemType Property 異常內容
        /// </summary>
        CheckItemTypeProperty = 33,

        /// <summary>
        /// 產生更新 ItemType Property 標籤的指令
        /// </summary>
        UpdatePropertyLabel = 41,

        /// <summary>
        /// 分析 Property 資料型別 (Item 與 Foreign)
        /// </summary>
        AnalyzePropertyDataType = 42,

        /// <summary>
        /// 找出 Property Data Source 異常
        /// </summary>
        FindPropertyUnOk= 43,

        /// <summary>
        /// 找出 DebuggerBreakMethod 異常
        /// </summary>
        DebuggerBreakMethod = 44,

        /// <summary>
        /// 找出 CCO WriteDebug 異常
        /// </summary>
        CCOWriteDebug = 45,

        /// <summary>
        /// 列出被鎖定項目
        /// </summary>
        LockedItemList = 46,

        /// <summary>
        /// 列出 Propery 資訊
        /// </summary>
        ReportProperty = 51,

        /// <summary>
        /// 產生 Property C# Class
        /// </summary>
        GeneratePropertyClass = 52,

        /// <summary>
        /// 更新 Property SQL 指令
        /// </summary>
        SQL_Property = 61,

        /// <summary>
        /// 產生 Method (LIKE)
        /// </summary>
        METHOD_List = 71,

        /// <summary>
        /// 產生 Method (ID)
        /// </summary>
        METHOD_ID = 72,

        /// <summary>
        /// 產生當日異動的 Method
        /// </summary>
        METHOD_Today = 81,

        /// <summary>
        /// 產生批次 Method
        /// </summary>
        METHOD_List_By_Sql = 82,

        /// <summary>
        /// 查找換成 Super User 沒換回來者
        /// </summary>
        Super_User = 101,

    }
}