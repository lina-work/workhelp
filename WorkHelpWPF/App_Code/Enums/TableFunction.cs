﻿namespace WorkHelp.Wpf.Enums
{
    /// <summary>
    /// 資料表功能列舉
    /// </summary>
    public enum TableFunction
    {
        /// <summary>
        /// 未選取
        /// </summary>
        None = 0,

        /// <summary>
        /// 產生 Entity
        /// </summary>
        GenerateEntity = 1,

        /// <summary>
        /// 產生欄位清單
        /// </summary>
        GenerateColumns = 2,

        /// <summary>
        /// 產生查詢指令
        /// </summary>
        SelectScript = 101,
    }
}