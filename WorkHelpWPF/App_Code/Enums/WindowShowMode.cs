﻿namespace WorkHelp.Wpf.Enums
{
    /// <summary>
    /// 視窗呈現模式
    /// </summary>
    public enum WindowShowMode
    {
        /// <summary>
        /// 強制唯一
        /// </summary>
        ShowDialog = 0,

        /// <summary>
        /// 可多開視窗
        /// </summary>
        MultiDialog = 1,
    }
}