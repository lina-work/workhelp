﻿namespace WorkHelp.Wpf.Enums
{
    /// <summary>
    /// Google Proto DataType
    /// </summary>
    public enum GoogleProtoDataTypeEnum
    {
        NONE,
        BOOL,
        BOOL_NULL,
        STRING,
        STRING_NULL,
        INT16,
        INT16_NULL,
        INT32,
        INT32_NULL,
        INT64,
        INT64_NULL,
        DOUBLE,
        DOUBLE_NULL,
        DECIMAL,
        DECIMAL_NULL,
        TIMESTAMP,
        REPEATED
    }
}