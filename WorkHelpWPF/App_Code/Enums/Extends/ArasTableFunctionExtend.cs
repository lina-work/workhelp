﻿using System.Collections.Generic;

namespace WorkHelp.Wpf.Enums.Extends
{
    /// <summary>
    /// Aras TableFunction Extend
    /// </summary>
    public class ArasTableFunctionExtend
    {
        /// <summary>
        /// 建構元
        /// </summary>
        protected ArasTableFunctionExtend() { }

        /// <summary>
        /// 取得功能選單
        /// </summary>
        public static Dictionary<int, string> GetMenuList()
        {
            Dictionary<int, string> result = new Dictionary<int, string>
            {
                { (int)ArasTableFunction.LockedItemList, GetMenuName(ArasTableFunction.LockedItemList)},
                { (int)ArasTableFunction.DebuggerBreakMethod, GetMenuName(ArasTableFunction.DebuggerBreakMethod)},
                { (int)ArasTableFunction.CCOWriteDebug, GetMenuName(ArasTableFunction.CCOWriteDebug)},
                { (int)ArasTableFunction.ReportProperty, GetMenuName(ArasTableFunction.ReportProperty)},
                { (int)ArasTableFunction.GeneratePropertyClass, GetMenuName(ArasTableFunction.GeneratePropertyClass)},

                { (int)ArasTableFunction.AnalyzeItemType, GetMenuName(ArasTableFunction.AnalyzeItemType)},
                { (int)ArasTableFunction.IgnoreMethod, GetMenuName(ArasTableFunction.IgnoreMethod)},
                { (int)ArasTableFunction.AnalyzeMethod, GetMenuName(ArasTableFunction.AnalyzeMethod)},
                { (int)ArasTableFunction.AnalyzeIgnoreMethod, GetMenuName(ArasTableFunction.AnalyzeIgnoreMethod)},
                { (int)ArasTableFunction.AnalyzeMethodDetail, GetMenuName(ArasTableFunction.AnalyzeMethodDetail)},
                { (int)ArasTableFunction.AnalyzeCSharpPage, GetMenuName(ArasTableFunction.AnalyzeCSharpPage)},
                { (int)ArasTableFunction.AnalyzeItemTypeProperty, GetMenuName(ArasTableFunction.AnalyzeItemTypeProperty)},
                { (int)ArasTableFunction.CheckItemTypeProperty, GetMenuName(ArasTableFunction.CheckItemTypeProperty)},
                { (int)ArasTableFunction.UpdatePropertyLabel, GetMenuName(ArasTableFunction.UpdatePropertyLabel)},
                { (int)ArasTableFunction.AnalyzePropertyDataType, GetMenuName(ArasTableFunction.AnalyzePropertyDataType)},
                { (int)ArasTableFunction.FindPropertyUnOk, GetMenuName(ArasTableFunction.FindPropertyUnOk)},

                { (int)ArasTableFunction.SQL_Property, GetMenuName(ArasTableFunction.SQL_Property)},
                { (int)ArasTableFunction.METHOD_List, GetMenuName(ArasTableFunction.METHOD_List)},
                { (int)ArasTableFunction.METHOD_ID, GetMenuName(ArasTableFunction.METHOD_ID)},
                { (int)ArasTableFunction.METHOD_Today, GetMenuName(ArasTableFunction.METHOD_Today)},
                { (int)ArasTableFunction.METHOD_List_By_Sql, GetMenuName(ArasTableFunction.METHOD_List_By_Sql)},

                { (int)ArasTableFunction.Super_User, GetMenuName(ArasTableFunction.Super_User)},

            };
            return result;
        }

        /// <summary>
        /// 取得功能名稱
        /// </summary>
        public static string GetMenuName(ArasTableFunction e)
        {
            switch (e)
            {
                case ArasTableFunction.AnalyzeItemType:
                    return "分析 ItemType";
                case ArasTableFunction.IgnoreMethod:
                    return "過濾出不要的 Method ";
                case ArasTableFunction.AnalyzeMethod:
                    return "分析新 Method";
                case ArasTableFunction.AnalyzeIgnoreMethod:
                    return "分析 Ignore Method 是否包含新 Method";
                case ArasTableFunction.AnalyzeMethodDetail:
                    return "分析 Method 並輸出細項";
                case ArasTableFunction.AnalyzeCSharpPage:
                    return "分析 C# Page 並輸出細項";
                case ArasTableFunction.AnalyzeItemTypeProperty:
                    return "分析 ItemType Property 內容";
                case ArasTableFunction.CheckItemTypeProperty:
                    return "檢查並輸出 ItemType Property 異常內容";
                case ArasTableFunction.UpdatePropertyLabel:
                    return "產生更新 ItemType Property 標籤的指令(IN_MEETING)"; 
                case ArasTableFunction.AnalyzePropertyDataType:
                    return "分析 Property 資料型別 (Item 與 Foreign)";
                case ArasTableFunction.FindPropertyUnOk:

                    return "列出[Property Data Source]異常";
                case ArasTableFunction.DebuggerBreakMethod:
                    return "列出[DebuggerBreakMethod]異常";
                case ArasTableFunction.CCOWriteDebug:
                    return "列出[WriteDebug]異常";
                case ArasTableFunction.LockedItemList:
                    return "列出[被鎖定項目]";
                case ArasTableFunction.ReportProperty:
                    return "列出[欄位訊息]";

                case ArasTableFunction.GeneratePropertyClass:
                    return "產生 C# Class (Property)";

                case ArasTableFunction.SQL_Property:
                    return "[SQL 指令]Property 更新中文描述";

                case ArasTableFunction.METHOD_List:
                    return "[檔案]產生 Method (LIKE)";

                case ArasTableFunction.METHOD_ID:
                    return "[檔案]產生 Method:ID";

                case ArasTableFunction.METHOD_Today:
                    return "[檔案]產生當日異動 Method";

                case ArasTableFunction.METHOD_List_By_Sql:
                    return "[檔案]產生批次 Method";

                case ArasTableFunction.Super_User:
                    return "列出[換成 Super User 沒換回來]的 Method";

                default:
                    return string.Empty;
            }
        }

        /// <summary>
        /// 取得功能注意事項
        /// </summary>
        public static string GetMenuNotice(ArasTableFunction e)
        {
            switch (e)
            {
                default:
                    return string.Empty;
            }
        }
    }
}