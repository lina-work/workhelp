﻿using System.Collections.Generic;
using WorkHelp.Wpf.Models.Windows;

namespace WorkHelp.Wpf.Enums.Extends
{
    /// <summary>
    /// Aras Type Function Extend
    /// </summary>
    public class ArasTypeFunctionExtend
    {
        /// <summary>
        /// 建構元
        /// </summary>
        protected ArasTypeFunctionExtend() { }

        /// <summary>
        /// 取得功能選單
        /// </summary>
        public static Dictionary<int, string> GetMenuList()
        {
            Dictionary<int, string> result = new Dictionary<int, string>
            {
                { (int)ArasTypeFunction.ExportProperySummary, GetMenuName(ArasTypeFunction.ExportProperySummary)},
                { (int)ArasTypeFunction.ExportMethod, GetMenuName(ArasTypeFunction.ExportMethod)},
                { (int)ArasTypeFunction.Scalar, GetMenuName(ArasTypeFunction.Scalar)},
                { (int)ArasTypeFunction.QueryRunningMeeting, GetMenuName(ArasTypeFunction.QueryRunningMeeting)},


            };
            return result;
        }

        /// <summary>
        /// 取得功能名稱
        /// </summary>
        public static string GetMenuName(ArasTypeFunction e)
        {
            switch (e)
            {
                case ArasTypeFunction.ExportProperySummary:
                    return "輸出 Propery 摘要檔案";

                case ArasTypeFunction.ExportMethod:
                    return "輸出 Method 檔案";

                case ArasTypeFunction.Scalar:
                    return "Scalar";

                case ArasTypeFunction.QueryRunningMeeting:
                    return "列出報名中的賽事資料";

                default:
                    return string.Empty;
            }
        }

        /// <summary>
        /// 取得功能注意事項
        /// </summary>
        public static string GetMenuNotice(ArasTypeFunction e)
        {
            switch (e)
            {
                default:
                    return string.Empty;
            }
        }
    }
}