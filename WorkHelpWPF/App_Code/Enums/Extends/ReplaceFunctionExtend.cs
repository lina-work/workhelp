﻿using System;
using System.Collections.Generic;
using WorkHelp.Wpf.Models.Windows;
using WorkHelp.Wpf.Services;

namespace WorkHelp.Wpf.Enums.Extends
{
    /// <summary>
    /// ReplaceFunction Extend
    /// </summary>
    public class ReplaceFunctionExtend
    {
        /// <summary>
        /// 建構元
        /// </summary>
        protected ReplaceFunctionExtend() { }

        /// <summary>
        /// 取得功能選單
        /// </summary>
        public static Dictionary<int, string> GetMenuList()
        {
            Dictionary<int, string> result = new Dictionary<int, string>
            {
                { (int)ReplaceFunction.Year_Calendar, GetMenuName(ReplaceFunction.Year_Calendar)},

                { (int)ReplaceFunction.Format, GetMenuName(ReplaceFunction.Format)},
                { (int)ReplaceFunction.BookName, GetMenuName(ReplaceFunction.BookName)},

                { (int)ReplaceFunction.EqualSignExchange, GetMenuName(ReplaceFunction.EqualSignExchange)},
                { (int)ReplaceFunction.CheckRepeatLines, GetMenuName(ReplaceFunction.CheckRepeatLines)},

                { (int)ReplaceFunction.CSharpClass, GetMenuName(ReplaceFunction.CSharpClass)},
                { (int)ReplaceFunction.CSharpEnum, GetMenuName(ReplaceFunction.CSharpEnum)},
                { (int)ReplaceFunction.TypeScriptClass, GetMenuName(ReplaceFunction.TypeScriptClass)},

                { (int)ReplaceFunction.CSharpToTypeScript, GetMenuName(ReplaceFunction.CSharpToTypeScript)},
                { (int)ReplaceFunction.TypeScriptToCSharp, GetMenuName(ReplaceFunction.TypeScriptToCSharp)},
                { (int)ReplaceFunction.MarkdownToHtml, GetMenuName(ReplaceFunction.MarkdownToHtml)},

                { (int)ReplaceFunction.ExportUninstallList, GetMenuName(ReplaceFunction.ExportUninstallList)},

                { (int)ReplaceFunction.IonicPageCli, GetMenuName(ReplaceFunction.IonicPageCli)},
                { (int)ReplaceFunction.IonicFormSnippets, GetMenuName(ReplaceFunction.IonicFormSnippets)},
                { (int)ReplaceFunction.IonicFormSnippetsFromInterface, GetMenuName(ReplaceFunction.IonicFormSnippetsFromInterface)},

                { (int)ReplaceFunction.PdfReturn, GetMenuName(ReplaceFunction.PdfReturn)},

                { (int)ReplaceFunction.SqlCastDateTime, GetMenuName(ReplaceFunction.SqlCastDateTime)},
                { (int)ReplaceFunction.HtmlTag, GetMenuName(ReplaceFunction.HtmlTag)},

                { (int)ReplaceFunction.Table_Bracket64, GetMenuName(ReplaceFunction.Table_Bracket64)},
                { (int)ReplaceFunction.Game01, GetMenuName(ReplaceFunction.Game01)},

            };
            return result;
        }

        /// <summary>
        /// 取得功能名稱
        /// </summary>
        public static string GetMenuName(ReplaceFunction e)
        {
            switch (e)
            {
                case ReplaceFunction.Year_Calendar:
                    return "輸出明年行事曆";

                case ReplaceFunction.EqualSignExchange:
                    return "等號左右交換";

                case ReplaceFunction.CheckRepeatLines:
                    return "檢查重複行";

                case ReplaceFunction.CSharpClass:
                    return "產生 C# 類別";

                case ReplaceFunction.CSharpEnum:
                    return "產生 C# 列舉類別";

                case ReplaceFunction.TypeScriptClass:
                    return "產生 TypeScript 類別";

                case ReplaceFunction.CSharpToTypeScript:
                    return "C# 類別轉換為 TypeScript 類別";

                case ReplaceFunction.TypeScriptToCSharp:
                    return "TypeScript 類別轉換為 C# 類別";

                case ReplaceFunction.MarkdownToHtml:
                    return "Markdown 內容轉換為 Html 內容";

                case ReplaceFunction.ExportUninstallList:
                    return "匯出具有反安裝程式的軟體清單";

                case ReplaceFunction.IonicPageCli:
                    return "產生 Ionic Page 指令";

                case ReplaceFunction.IonicFormSnippets:
                    return "產生 Ionic Form 程式碼片段";

                case ReplaceFunction.IonicFormSnippetsFromInterface:
                    return "產生 Ionic Form 程式碼片段 (解析 TS interface)";

                case ReplaceFunction.Format:
                    return "輸出文字樣板";

                case ReplaceFunction.BookName:
                    return "書名格式";

                case ReplaceFunction.PdfReturn:
                    return "PDF 換行處理";

                case ReplaceFunction.SqlCastDateTime:
                    return "資料庫日期 Cast 處理";

                case ReplaceFunction.HtmlTag:
                    return "切割 HTML TAG";

                case ReplaceFunction.Table_Bracket64:
                    return "賽程圖(64支籤)";

                case ReplaceFunction.Game01:
                    return "秩序冊轉檔";

                default:
                    return string.Empty;
            }
        }

        /// <summary>
        /// 取得功能注意事項
        /// </summary>
        public static string GetMenuNotice(ReplaceFunction e)
        {
            switch (e)
            {
                case ReplaceFunction.EqualSignExchange:
                    return "要交換的資料必須包含等號";

                case ReplaceFunction.CheckRepeatLines:
                    return "請輸入要檢查的文字";

                case ReplaceFunction.CSharpClass:
                    return "由於資料使用空白字元分隔，註解不允許包含空白字元";

                case ReplaceFunction.CSharpEnum:
                    return "由於資料使用空白字元分隔，註解不允許包含空白字元";

                case ReplaceFunction.TypeScriptClass:
                    return "由於資料使用空白字元分隔，註解不允許包含空白字元";

                case ReplaceFunction.CSharpToTypeScript:
                    return "輸入的資料必須符合 C# Class";

                case ReplaceFunction.TypeScriptToCSharp:
                    return "輸入的資料必須符合 TypeScript Class";

                case ReplaceFunction.MarkdownToHtml:
                    return "輸入的資料必須符合 Markdown";

                case ReplaceFunction.ExportUninstallList:
                    return "請在【輸入】文字區塊鍵入【confirm】避免誤動作。 (匯出檔格式為 CSV, 以 Tab 分隔)";

                case ReplaceFunction.IonicPageCli:
                    return "請注意切換至 Pages 資料夾";

                case ReplaceFunction.IonicFormSnippets:
                    return string.Empty;

                case ReplaceFunction.IonicFormSnippetsFromInterface:
                    return "輸入的資料必須符合 TS interface";

                case ReplaceFunction.SqlCastDateTime:
                    return string.Empty;

                default:
                    return string.Empty;
            }
        }

        /// <summary>
        /// 設定置換組態
        /// </summary>
        public static void Config(Config102 config)
        {
            switch (config.ReplaceType)
            {
                case ReplaceFunction.Year_Calendar:
                    config.SampleFile = "Year_Calendar.txt";
                    config.OutputFile = "Year_Calendar.txt";
                    config.Transform = Service102.YearCalendar;
                    break;

                case ReplaceFunction.EqualSignExchange:
                    config.SampleFile = "EqualSignExchange.txt";
                    config.OutputFile = "EqualSignExchange.txt";
                    config.Transform = Service102.EqualSignExchange;
                    break;

                case ReplaceFunction.CheckRepeatLines:
                    config.SampleFile = "CheckRepeatLines.txt";
                    config.OutputFile = "CheckRepeatLines.txt";
                    config.Transform = Service102.CheckRepeatLines;
                    break;

                case ReplaceFunction.CSharpClass:
                    config.SampleFile = "CSharpClass.txt";
                    config.OutputFile = "TempViewModel.cs";
                    config.Transform = Service102.CSharpClass;
                    break;

                case ReplaceFunction.CSharpEnum:
                    config.SampleFile = "CSharpEnum.txt";
                    config.OutputFile = "TempEnum.cs";
                    config.Transform = Service102.CSharpEnum;
                    break;

                case ReplaceFunction.TypeScriptClass:
                    config.SampleFile = "TypeScriptClass.txt";
                    config.OutputFile = "temp-info.d.ts";
                    config.Transform = Service102.TypeScriptClass;
                    break;

                case ReplaceFunction.CSharpToTypeScript:
                    config.SampleFile = "CSharpToTypeScript.txt";
                    config.OutputFile = "class-info.d.ts";
                    config.Transform = Service102.CSharpToTypeScript;
                    break;

                case ReplaceFunction.TypeScriptToCSharp:
                    config.SampleFile = "TypeScriptToCSharp.txt";
                    // config.OutputFile 經由分析產生
                    config.Transform = Service102.TypeScriptToCSharp;
                    break;

                case ReplaceFunction.MarkdownToHtml:
                    config.SampleFile = "MarkdownToHtml.txt";
                    config.OutputFile = "NewPage.html";
                    config.Transform = Service102.MarkDownToHtml;
                    break;

                case ReplaceFunction.ExportUninstallList:
                    config.OutputFile = $"UninstallList_{DateTime.Now.ToString("yyyyMMdd_hhmmss")}.txt";
                    config.Transform = Service102.ExportUninstallList;
                    break;

                case ReplaceFunction.IonicPageCli:
                    config.SampleFile = "IonicPageCli.txt";
                    config.OutputFile = "IonicPageCli.txt";
                    config.Transform = Service102.IonicPageCli;
                    break;

                case ReplaceFunction.IonicFormSnippets:
                    config.SampleFile = "IonicFormSnippets.txt";
                    config.OutputFile = "IonicFormSnippets.txt";
                    config.Transform = Service102.IonicFormSnippets;
                    break;

                case ReplaceFunction.IonicFormSnippetsFromInterface:
                    config.SampleFile = "TypeScriptInterface.txt";
                    config.OutputFile = "TypeScriptInterface.txt";
                    config.Transform = Service102.IonicFormSnippetsFromInterface;
                    break;

                case ReplaceFunction.Format:
                    config.SampleFile = "Format.txt";
                    config.OutputFile = "Format.txt";
                    config.Transform = Service102.Format;
                    break;

                case ReplaceFunction.BookName:
                    config.SampleFile = "BookName.txt";
                    config.OutputFile = "BookName.txt";
                    config.Transform = Service102.BookName;
                    break;

                case ReplaceFunction.PdfReturn:
                    config.SampleFile = "PdfReturn.txt";
                    config.OutputFile = "PdfReturn.txt";
                    config.Transform = Service102.PdfReturn;
                    break;

                case ReplaceFunction.SqlCastDateTime:
                    config.SampleFile = "SqlCastDateTime.txt";
                    config.OutputFile = "SqlCastDateTime.txt";
                    config.Transform = Service102.SqlCastDateTime;
                    break;

                case ReplaceFunction.HtmlTag:
                    config.SampleFile = "HtmlTag.txt";
                    config.OutputFile = "HtmlTag.txt";
                    config.Transform = Service102.HtmlTag;
                    break;

                case ReplaceFunction.Table_Bracket64:
                    config.SampleFile = "Bracket64.txt";
                    config.OutputFile = "Bracket64.txt";
                    config.Transform = Service102.Table_Bracket64;
                    break;

                case ReplaceFunction.Game01:
                    config.SampleFile = "Game01.txt";
                    config.OutputFile = "Game01.txt";
                    config.Transform = Service102.Game01;
                    break;

                case ReplaceFunction.None:
                    System.Windows.Forms.MessageBox.Show("請選取功能");
                    config.IsExecute = false;
                    break;

                default:
                    config.IsExecute = false;
                    break;
            }
        }
    }
}