﻿using System.Collections.Generic;

namespace WorkHelp.Wpf.Enums.Extends
{
    /// <summary>
    /// TableFunction Extend
    /// </summary>
    public class TableFunctionExtend
    {
        /// <summary>
        /// 建構元
        /// </summary>
        protected TableFunctionExtend() { }

        /// <summary>
        /// 取得功能選單
        /// </summary>
        public static Dictionary<int, string> GetMenuList()
        {
            Dictionary<int, string> result = new Dictionary<int, string>
            {
                { (int)TableFunction.GenerateEntity, GetMenuName(TableFunction.GenerateEntity)},
                { (int)TableFunction.GenerateColumns, GetMenuName(TableFunction.GenerateColumns)},

                { (int)TableFunction.SelectScript, GetMenuName(TableFunction.SelectScript)},
            };
            return result;
        }

        /// <summary>
        /// 取得功能名稱
        /// </summary>
        public static string GetMenuName(TableFunction e)
        {
            switch (e)
            {
                case TableFunction.GenerateEntity:
                    return "產生 Entity";

                case TableFunction.GenerateColumns:
                    return "產生 欄位清單";

                case TableFunction.SelectScript:
                    return "產生 查詢指令";

                default:
                    return string.Empty;
            }
        }

        /// <summary>
        /// 取得功能注意事項
        /// </summary>
        public static string GetMenuNotice(TableFunction e)
        {
            switch (e)
            {
                default:
                    return string.Empty;
            }
        }
    }
}