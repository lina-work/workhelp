﻿using System.Collections.Generic;

namespace WorkHelp.Wpf.Enums.Extends
{
    /// <summary>
    /// Aras 主要類別 Extend
    /// </summary>
    public class ArasTypeExtend
    {
        /// <summary>
        /// 建構元
        /// </summary>
        protected ArasTypeExtend() { }

        /// <summary>
        /// 取得功能選單
        /// </summary>
        public static Dictionary<int, string> GetMenuList()
        {
            Dictionary<int, string> result = new Dictionary<int, string>
            {
                { (int)ArasType.Property, GetMenuName(ArasType.Property)},
                { (int)ArasType.Method, GetMenuName(ArasType.Method)},
                { (int)ArasType.Data, GetMenuName(ArasType.Data)},
             
            };
            return result;
        }

        /// <summary>
        /// 取得功能名稱
        /// </summary>
        public static string GetMenuName(ArasType e)
        {
            switch (e)
            {
                case ArasType.Property:
                    return "Property(屬性)";
                case ArasType.Method:
                    return "Mothod(方法)";
                case ArasType.Data:
                    return "Data(數據)";
                default:
                    return string.Empty;
            }
        }

        /// <summary>
        /// 取得功能注意事項
        /// </summary>
        public static string GetMenuNotice(ArasType e)
        {
            switch (e)
            {
                default:
                    return string.Empty;
            }
        }
    }
}