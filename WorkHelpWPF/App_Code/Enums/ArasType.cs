﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkHelp.Wpf.Enums
{
    /// <summary>
    /// Aras 主要類別
    /// </summary>
    public enum ArasType
    {
        /// <summary>
        /// 未選取
        /// </summary>
        None = 0,

        /// <summary>
        /// 屬性
        /// </summary>
        Property = 2,

        /// <summary>
        /// 方法
        /// </summary>
        Method = 11,

        /// <summary>
        /// 數據
        /// </summary>
        Data = 21,

    }
}
