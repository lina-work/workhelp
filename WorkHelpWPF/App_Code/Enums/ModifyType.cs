﻿namespace WorkHelp.Wpf.Enums
{
    public enum UiStatus
    {
        /// <summary>
        /// 編輯模式
        /// </summary>
        Edit = 1,

        /// <summary>
        /// 檢視模式
        /// </summary>
        View = 2,
    }
}