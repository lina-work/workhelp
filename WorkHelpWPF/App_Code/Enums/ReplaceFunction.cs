﻿namespace WorkHelp.Wpf.Enums
{
    /// <summary>
    /// 置換功能列舉
    /// </summary>
    public enum ReplaceFunction
    {
        /// <summary>
        /// 未選取
        /// </summary>
        None = 0,

        /// <summary>
        /// 等號左右交換
        /// </summary>
        EqualSignExchange = 1,

        /// <summary>
        /// 檢查重複行
        /// </summary>
        CheckRepeatLines = 2,

        /// <summary>
        /// 產生 C# 類別
        /// </summary>
        CSharpClass = 101,

        /// <summary>
        /// 產生 C# 列舉類別
        /// </summary>
        CSharpEnum = 102,

        /// <summary>
        /// 產生 TypeScript 類別
        /// </summary>
        TypeScriptClass = 103,

        /// <summary>
        /// C# 類別轉換為 TypeScript 類別
        /// </summary>
        CSharpToTypeScript = 201,

        /// <summary>
        /// TypeScript 類別轉換為 C# 類別
        /// </summary>
        TypeScriptToCSharp = 202,

        /// <summary>
        /// Markdown 內容轉換為 Html 內容
        /// </summary>
        MarkdownToHtml = 203,

        /// <summary>
        /// 匯出具有反安裝程式的軟體清單
        /// </summary>
        ExportUninstallList = 301,

        /// <summary>
        /// 產生 Ionic Page 指令
        /// </summary>
        IonicPageCli = 401,

        /// <summary>
        /// 產生 Ionic Form 程式碼片段
        /// </summary>
        IonicFormSnippets = 402,

        /// <summary>
        /// 產生 Ionic Form 程式碼片段 (解析 TS interface)
        /// </summary>
        IonicFormSnippetsFromInterface = 403,

        /// <summary>
        /// 輸出文字樣板
        /// </summary>
        Format = 1001,

        /// <summary>
        /// 書名格式
        /// </summary>
        BookName = 1002,

        /// <summary>
        /// PDF 換行處理
        /// </summary>
        PdfReturn = 1003,

        /// <summary>
        /// 資料庫日期 Cast 處理
        /// </summary>
        SqlCastDateTime = 2001,

        /// <summary>
        /// 切割 HTML TAG
        /// </summary>
        HtmlTag = 3001,

        /// <summary>
        /// 賽程圖(64支籤)
        /// </summary>
        Table_Bracket64 = 4064,

        /// <summary>
        /// 輸出明年行事曆
        /// </summary>
        Year_Calendar = 5001,

        /// <summary>
        /// 秩序冊
        /// </summary>
        Game01 = 6001,
    }
}