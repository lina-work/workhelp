﻿namespace WorkHelp.Wpf.Enums
{
    /// <summary>
    /// Aras Type Function
    /// </summary>
    public enum ArasTypeFunction
    {
        /// <summary>
        /// 未選取
        /// </summary>
        None = 0,

        /// <summary>
        /// 輸出 Propery 摘要檔案
        /// </summary>
        ExportProperySummary = 2,

        /// <summary>
        /// 輸出 Method 檔案
        /// </summary>
        ExportMethod = 11,

        /// <summary>
        /// 純量
        /// </summary>
        Scalar = 21,

        /// <summary>
        /// 列出報名中的賽事資料
        /// </summary>
        QueryRunningMeeting = 31
    }
}