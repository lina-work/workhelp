﻿using System.Windows;

namespace WorkHelp.Wpf
{
    /// <summary>
    /// App.xaml 的互動邏輯
    /// </summary>
    public partial class App : Application
    {
        /// <summary>
        /// 建構元
        /// </summary>
        public App()
        {
            WorkHelp.Logging.TLog.Start();
        }

        /// <summary>
        /// 在啟動時
        /// </summary>
        /// <param name="e">啟動事件參數</param>
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            // 引導程序
            Bootstrapper bootstrapper = new Bootstrapper();
            bootstrapper.Run();
        }
    }
}