﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Drawing;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.CvEnum;
using System.IO;
using System.Windows;
using System.ComponentModel;
using System.Timers;
using System.Runtime.CompilerServices;
using Emgu.CV.Face;
using Emgu.CV.Util;
using Microsoft.Win32;

namespace WorkHelp.FaceRecognition
{
    class Program
    {
        static void Main(string[] args)
        {
            Recognition();
            Console.Read();
        }

        static void Recognition()
        {
            try
            {
                //string tgtFile = "face3.bmp";
                //string tgtPath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Source\\Faces");
                //string tgtPhoto = System.IO.Path.Combine(tgtPath, tgtFile);

                var tgtDir = new System.IO.DirectoryInfo(@"C:\test");
                var tgtFile = tgtDir.GetFiles().First();
                var tgtPhoto = tgtFile.FullName;

                Console.WriteLine($"目標影像: {tgtPhoto}");

                if (!Directory.Exists(Config.FacePhotosPath))
                {
                    Console.WriteLine($"資料夾不存在: {Config.FacePhotosPath}");
                    return;
                }

                if (!File.Exists(tgtPhoto))
                {
                    Console.WriteLine($"目標影像不存在: {tgtPhoto}");
                    return;
                }

                List<FaceData> faceList = new List<FaceData>();
                VectorOfMat imageList = new VectorOfMat();
                List<string> nameList = new List<string>();
                VectorOfInt labelList = new VectorOfInt();

                var dir = new System.IO.DirectoryInfo(Config.FacePhotosPath);
                var files = dir.GetFiles();
                foreach (var file in files)
                {
                    var faceSrc1 = new Image<Bgr, byte>(file.FullName);
                    
                    //調整大小 (必須一致)
                    var faceSrc2 = faceSrc1.Resize(100, 100, Inter.Cubic);

                    //灰化
                    FaceData face = new FaceData();
                    face.FaceImage = faceSrc2.Convert<Gray, Byte>();
                    face.PersonName = file.Name;
                    faceList.Add(face);
                }

                var i = 0;
                foreach(var face in faceList)
                {
                    imageList.Push(face.FaceImage.Mat);
                    nameList.Add(face.PersonName);
                    labelList.Push(new[] { i++ });
                }

                EigenFaceRecognizer recognizer = new EigenFaceRecognizer(imageList.Size);
                recognizer.Train(imageList, labelList);

                var detectedFaceSrc1 = new Image<Bgr, byte>(tgtPhoto);
                var detectedFaceSrc2 = detectedFaceSrc1.Resize(100, 100, Inter.Cubic);
                var detectedFace = detectedFaceSrc2.Convert<Gray, Byte>();

                FaceRecognizer.PredictionResult result = recognizer.Predict(detectedFace);
                if (result.Label > -1)
                {
                    var FaceName = nameList[result.Label];
                    Console.WriteLine($"最似結果：{FaceName}, {result.Distance} (Distance)");
                }
                else
                {
                    Console.WriteLine("NOT FIND.");
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Logging.TLog.Watch(message: ex.ToString());

            }
        }
    }
}
