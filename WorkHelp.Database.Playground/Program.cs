﻿using System;
using System.Linq;
using System.Diagnostics;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Xml;
using System.Xml.Serialization;
using Tesseract;
using System.IO;
using System.Drawing;
using System.Text.RegularExpressions;
using Newtonsoft.Json;

namespace WorkHelp.Database.Playground
{
    class Program
    {
        static void Main(string[] args)
        {
            var folder = new System.IO.DirectoryInfo(@"D:\新增資料夾\20230328 - 112年歡樂兒童節定向越野體驗賽-順點賽");
            var files = folder.GetFiles();
            foreach(var file in files)
            {
                var name = file.Name.ToLower();
                if (name == "changes.dat") continue;
                if (name == "personal.dat") continue;
                if (name == "network.log") continue;
                if (name == "operations.log") continue;

                var model = GetEncodingByUDE(file.FullName);

                if (model.IsError)
                {

                    Console.WriteLine("ERROR");
                }
                else
                {
                    Console.WriteLine(file.Name + " = " + model.Mode.BodyName + " | " + model.Charset);
                    Console.WriteLine(model.Contents);
                }
            }

            Console.ReadLine();
        }

        public class TEncModel
        {
            public bool IsError { get; set; }
            public System.Text.Encoding Mode { get; set; }
            public string Contents { get; set; }
            public string Charset { get; set; }
        }

        private static object _AppFileMethodLock = new object();

        public static TEncModel GetEncodingByUDE(string filename)
        {
            var result = new TEncModel { IsError = false };
            lock(_AppFileMethodLock)
            {
                using (var fs = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    Ude.CharsetDetector cdet = new Ude.CharsetDetector();
                    cdet.Feed(fs);
                    cdet.DataEnd();

                    if (cdet.Charset != null)
                    {
                        result.Charset = cdet.Charset;
                        switch (cdet.Charset)
                        {
                            case "KOI8-R": result.Mode = System.Text.Encoding.GetEncoding(950); break;
                            case "Big5": result.Mode = System.Text.Encoding.GetEncoding(950); break;
                            case "windows-1252": result.Mode = System.Text.Encoding.GetEncoding(950); break;
                            //case "windows-1252": result.Mode = System.Text.Encoding.GetEncoding("Windows-1252"); break;
                            //case "windows-1252": result.Mode = System.Text.Encoding.UTF8; break;
                            //case "windows-1252": result.Mode = System.Text.Encoding.ASCII; break;

                            case "UTF-16LE": result.Mode = System.Text.Encoding.Unicode; break;
                            case "UTF-16BE": result.Mode = System.Text.Encoding.Unicode; break;
                            case "UTF-16": result.Mode = System.Text.Encoding.Unicode; break;
                            default: 
                                if (cdet.Charset.Contains("UTF")) 
                                {
                                    result.Mode = System.Text.Encoding.UTF8; 
                                }
                                else
                                {
                                    result.Mode = System.Text.Encoding.GetEncoding(950);
                                }
                                break;
                        }
                    }
                    else
                    {
                        result.IsError = true;
                    }
                }
            }

            if (!result.IsError)
            {
                lock (_AppFileMethodLock)
                {
                    result.Contents = System.IO.File.ReadAllText(filename, result.Mode);
                }
            }

            return result;
        }

        public static byte[] ReadFile(string filePath)
        {
            byte[] buffer;
            FileStream fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read);
            try
            {
                int length = (int)fileStream.Length;  // get file length    
                buffer = new byte[length];            // create buffer     
                int count;                            // actual number of bytes read     
                int sum = 0;                          // total number of bytes read    

                // read until Read method returns 0 (end of the stream has been reached)    
                while ((count = fileStream.Read(buffer, sum, length - sum)) > 0)
                    sum += count;  // sum is a buffer offset for next reading
            }
            finally
            {
                fileStream.Close();
            }
            return buffer;
        }

        public static void ShowEncodingByUDE(string name, string filename)
        {
            using (FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                Ude.CharsetDetector cdet = new Ude.CharsetDetector();
                cdet.Feed(fs);
                cdet.DataEnd();

                if (cdet.Charset != null)
                {
                    Console.WriteLine("{0} = Charset: {1}, confidence: {2}",
                         name, cdet.Charset, cdet.Confidence);
                }
                else
                {
                    Console.WriteLine($"{name} = Detection failed.");
                }
            }
        }

        /// <summary>
        /// Determines a text file's encoding by analyzing its byte order mark (BOM).
        /// Defaults to ASCII when detection of the text file's endianness fails.
        /// </summary>
        /// <param name="filename">The text file to analyze.</param>
        /// <returns>The detected encoding.</returns>
        public static System.Text.Encoding GetEncoding(string filename)
        {
            // Read the BOM
            var bom = new byte[4];
            using (var file = new FileStream(filename, FileMode.Open, FileAccess.Read))
            {
                file.Read(bom, 0, 4);
            }

            // Analyze the BOM
            if (bom[0] == 0x2b && bom[1] == 0x2f && bom[2] == 0x76) return System.Text.Encoding.UTF7;
            if (bom[0] == 0xef && bom[1] == 0xbb && bom[2] == 0xbf) return System.Text.Encoding.UTF8;
            if (bom[0] == 0xff && bom[1] == 0xfe && bom[2] == 0 && bom[3] == 0) return System.Text.Encoding.UTF32; //UTF-32LE
            if (bom[0] == 0xff && bom[1] == 0xfe) return System.Text.Encoding.Unicode; //UTF-16LE
            if (bom[0] == 0xfe && bom[1] == 0xff) return System.Text.Encoding.BigEndianUnicode; //UTF-16BE
            if (bom[0] == 0 && bom[1] == 0 && bom[2] == 0xfe && bom[3] == 0xff) return new System.Text.UTF32Encoding(true, true);  //UTF-32BE

            // We actually have no idea what the encoding is if we reach this point, so
            // you may wish to return null instead of defaulting to ASCII
            return System.Text.Encoding.ASCII;
        }

        static void MDOIQOIQHOHQ()
        {
            var data = "0040100205890";
            var punch = data.Substring(0, 4);
            var time = data.Substring(4, 9);
            var hh = data.Substring(4, 2);
            var mm = data.Substring(6, 2);
            var ss = data.Substring(8, 2);
            var ff = data.Substring(10, 3);

            Console.WriteLine(time);
            Console.WriteLine($"{hh}:{mm}:{ss}.{ff}");
            Console.Read();
        }

        static void XXXXX()
        {
            try
            {
                var state = "11101111";
            }
            catch (Exception ex)
            {
            }
        }

        //static KeyValuePair<int, int> AnalysisCount(string value)
        //{

        //    if (string.IsNullOrWhiteSpace(value)) return new KeyValuePair(1, 1);

        //    for (int i = 0; i < value.Length; i++)
        //    {

        //    }
        //}

        static void QOIWOQ()
        {
            try
            {

                var contents = XmlStr();

                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(contents);

                var jsonText = JsonConvert.SerializeXmlNode(xmlDoc);
                var a =  xmlDoc.GetElementsByTagName("faultstring")[0].InnerText;

                Console.WriteLine(contents);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            Console.ReadLine();
        }

        internal class Param
        {
            [JsonProperty(PropertyName = "@name")]
            string name;
            [JsonProperty(PropertyName = "#text")]
            string text;
        }

        static string XmlStr()
        {
            var path = @"C:\temp\a.txt";
            return System.IO.File.ReadAllText(path, System.Text.Encoding.UTF8);
        }

        static void XMSKMLK()
        {
            var value = "[$in_official_leave_code$]";
            var list = GetCellProperties(value);
            Console.Read();
        }

        private static List<string> GetCellProperties(string value)
        {
            var result = new List<string>();
            AddCellProperty(result, value);
            return result;
        }

        private static void AddCellProperty(List<string> list, string value)
        {
            int posS = value.IndexOf("[$");
            int posE = value.IndexOf("$]");

            if (posS == -1 || posE == -1)
            {
                return;
            }
            else
            {
                list.Add(value.Substring(posS + 2, posE - posS - 2));

                var idx = posE + 2;
                var other = value.Substring(idx, value.Length - idx);
                AddCellProperty(list, other);
            }
        }

        static void XOXOXOX()
        {
            var path1 = @"C:\Users\Administrator\Downloads\ABC.png";
            var path2 = @"C:\Users\Administrator\Downloads\IMG_1842.JPG";
            var result = GetProductNumberFromImage(path2);
            Console.WriteLine(string.Join("\r\n", result));
            Console.ReadLine();
        }

        static List<string> GetProductNumberFromImage(string imagePath)
        {
            var list = new List<string>();

            var datapath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "tessdata");

            using (var ocr = new TesseractEngine(datapath, "chi_tra", EngineMode.Default))
            {
                var pix = PixConverter.ToPix(new Bitmap(imagePath));

                using (var page = ocr.Process(pix))
                {
                    var text = page.GetText().Replace(" ", "");
                    list.Add(text);

                    //if (!string.IsNullOrEmpty(text))
                    //{
                    //    var pattern = @"品號([\s\S])(\d+)";
                    //    var regex = new Regex(pattern);
                    //    var mathResult = regex.Matches(text);

                    //    foreach (Match item in mathResult)
                    //    {
                    //        if (item.Groups.Count >= 2)
                    //        {
                    //            resultList.Add(item.Groups[2].Value);
                    //        }
                    //        else
                    //        {
                    //            resultList.Add(item.Value);
                    //        }
                    //    }
                    //}
                }
            }
            return list;
        }

    }
}
