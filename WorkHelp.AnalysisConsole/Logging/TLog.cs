﻿using System;
using System.Linq;

namespace WorkHelp.AnalysisConsole.Logging
{
    /// <summary>
    /// 日誌
    /// </summary>
    public class TLog
    {
        /// <summary>
        /// 建構元
        /// </summary>
        protected TLog() { }

        /// <summary>
        /// 應用程式根目錄
        /// </summary>
        private static string baseName = string.Empty;

        /// <summary>
        /// 應用程式根目錄
        /// </summary>
        private static string baseDirectory = AppDomain.CurrentDomain.BaseDirectory;

        /// <summary>
        /// 鎖
        /// </summary>
        private static object _AppSeetingLock = new object();

        /// <summary>
        /// 應用程式名稱初始化
        /// </summary>
        private static void InitialAppName()
        {
            if (string.IsNullOrWhiteSpace(baseName))
            {
                string appName = System.Reflection.Assembly.GetExecutingAssembly().FullName.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)?.First();
                if (string.IsNullOrWhiteSpace(appName))
                {
                    baseName = "AppLog";
                }
                else
                {
                    baseName = appName;
                }
            }
        }

        /// <summary>
        /// 取得時間耗用計算器
        /// </summary>
        /// <returns></returns>
        public static System.Diagnostics.Stopwatch GetStopwatch()
        {
            System.Diagnostics.Stopwatch result = new System.Diagnostics.Stopwatch();
            result.Reset();
            result.Start();
            return result;
        }

        /// <summary>
        /// 變更根目錄
        /// </summary>
        /// <param name="webRootPath"></param>
        public static void ChangeWebRootPath(string webRootPath)
        {
            if (!string.IsNullOrWhiteSpace(baseDirectory))
            {
                baseDirectory = webRootPath;
            }
        }

        /// <summary>
        /// 日誌
        /// </summary>
        /// <param name="stopWatch">時間耗用計算器</param>
        /// <param name="message">訊息</param>
        public static void Watch(string folder, string message = null)
        {
            InitialAppName();

            string file = $"{baseName}.{DateTime.Now.ToString("yyyyMMdd")}.log";
            string path = System.IO.Path.Combine(baseDirectory, "zlogs");
            path = System.IO.Path.Combine(path, folder);

            if (!System.IO.Directory.Exists(path))
            {
                System.IO.Directory.CreateDirectory(path);
            }

            string fullfile = System.IO.Path.Combine(path, file);
            string contents = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff")
                + " | " + message + "\r\n";

            lock (_AppSeetingLock)
            {
                System.IO.File.AppendAllText(fullfile, contents, System.Text.Encoding.UTF8);
            }
        }
    }
}