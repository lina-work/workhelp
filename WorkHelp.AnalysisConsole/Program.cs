﻿
using CliWrap;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace WorkHelp.AnalysisConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            var processes = System.Diagnostics.Process.GetProcessesByName("WorkHelp.Wpf");
            if (processes == null || processes.Length == 0)
            {
                Console.WriteLine($"工作管理員中查無執行檔: WorkHelp.Wpf");
            }
            else
            {
                foreach (var p in processes)
                {
                    Console.WriteLine($"殺死執行檔: {p.ProcessName}");
                    p.Kill();
                }
            }
        }
        static void DJIONOEW()
        {
            var ps = new List<string>();
            var list = System.Diagnostics.Process.GetProcesses();
            foreach(var row in list)
            {
                ps.Add(row.ProcessName);
            }

            var sorted_list = ps.OrderBy(x => x);
            foreach (var row in sorted_list)
            {
                Console.WriteLine(row);
            }
            Console.ReadKey();
        }

        static void XOPOPO()
        {
            for (var i = 0; i < 99; i++)
            {
                Draw();
            }
            Console.ReadKey();

        }
        static void Draw()
        {
            int min = 1; // 最小值
            int max = 10; // 最大值
            int[] numbers = new int[max - min + 1];
            for (int i = 0; i < numbers.Length; i++)
            {
                numbers[i] = min + i;
            }
            Shuffle(numbers);
            int result = numbers[0];
            Console.WriteLine("你抽中了: " + result);
        }

        static void Shuffle(int[] numbers)
        {
            Random random = new Random();
            for (int i = numbers.Length - 1; i >= 1; i--)
            {
                int j = random.Next(i + 1);
                int temp = numbers[i];
                numbers[i] = numbers[j];
                numbers[j] = temp;
            }
        }

        static void XXXX()
        {
            var dt = new DataTable();
            var c1 = new DataColumn("id");
            var c2 = new DataColumn("in_sysresult");
            dt.Columns.Add(c1);
            dt.Columns.Add(c2);

            for(var i = 1; i < 10; i++)
            {
                DataRow dr = dt.NewRow();
                dr["id"] = "A" + i.ToString().PadLeft(3, '0');
                dr["in_sysresult"] = i.ToString();
                dt.Rows.Add(dr);
            }

            Console.WriteLine(Newtonsoft.Json.JsonConvert.SerializeObject(dt));
            Console.Read();
        }

        static void XDDJIOWEJO()
        {
            WorkHelp.AnalysisConsole.Logging.TLog.Watch("Process", "IN");
            RunGit();
            WorkHelp.AnalysisConsole.Logging.TLog.Watch("Process", "OUT");
        }

        private  static void RunGit()
        {
            RunGitAdd();
            System.Threading.Thread.Sleep(500);
            RunGitCommit();
            System.Threading.Thread.Sleep(500);
            RunGitPush();
        }

        private async static void RunGitAdd()
        {
            try
            {
                WorkHelp.AnalysisConsole.Logging.TLog.Watch("Process", " ======= Add. ======= ");

                var cmd1 = Cli.Wrap("git")
                    .WithWorkingDirectory("C:/site/test_autocommit")
                    .WithArguments("add .");

                await cmd1.ExecuteAsync();
            }
            catch(Exception ex)
            {
                WorkHelp.AnalysisConsole.Logging.TLog.Watch("Process", ex.Message);
            }
        }

        private async static void RunGitCommit()
        {
            try
            {
                WorkHelp.AnalysisConsole.Logging.TLog.Watch("Process", " ======= Cmmt ======= ");

                var cmd2 = Cli.Wrap("git")
                    .WithWorkingDirectory("C:/site/test_autocommit")
                    .WithEnvironmentVariables(env => env
                        .Set("GIT_AUTHOR_NAME", "lina.tian")
                        .Set("GIT_AUTHOR_EMAIL", "lina.tian@innosoft.com.tw")
                    )
                    .WithValidation(CommandResultValidation.None)
                    .WithArguments("commit -m \"my commit5\"");

                //var cmd2 = Cli.Wrap("git")
                //    .WithWorkingDirectory("C:/site/test_autocommit")
                //    .WithCredentials(new Credentials(
                //        domain: "local",
                //        userName: "lina.tian@innosoft.com.tw",
                //        password: "ti@n363720",
                //        loadUserProfile: false
                //    ))
                //    .WithArguments("commit -m \"my commit5\"");

                //var cmd2 = Cli.Wrap("git")
                //    .WithWorkingDirectory("C:/site/test_autocommit")
                //    .WithValidation(CommandResultValidation.None)
                //    .WithArguments("commit -m \"my commit4\"");

                await cmd2.ExecuteAsync();
            }
            catch (Exception ex)
            {
                WorkHelp.AnalysisConsole.Logging.TLog.Watch("Process", ex.Message);
            }
        }

        private async static void RunGitPush()
        {
            try
            {
                WorkHelp.AnalysisConsole.Logging.TLog.Watch("Process", " ======= Push ======= ");

                var cmd3 = Cli.Wrap("git")
                    .WithWorkingDirectory("C:/site/test_autocommit")
                    .WithEnvironmentVariables(env => env
                        .Set("GIT_AUTHOR_NAME", "lina.tian")
                        .Set("GIT_AUTHOR_EMAIL", "lina.tian@innosoft.com.tw")
                    )
                    .WithValidation(CommandResultValidation.None)
                    .WithArguments("push");

                //var cmd3 = Cli.Wrap("git")
                //    .WithWorkingDirectory("C:/site/test_autocommit")
                //    .WithCredentials(new Credentials(
                //        domain: "local",
                //        userName: "lina.tian@innosoft.com.tw",
                //        password: "ti@n363720",
                //        loadUserProfile: false
                //    ))
                //    .WithArguments("push");

                //var cmd3 = Cli.Wrap("git")
                //    .WithWorkingDirectory("C:/site/test_autocommit")
                //    .WithValidation(CommandResultValidation.None)
                //    .WithArguments("push");

                await cmd3.ExecuteAsync();
            }
            catch (Exception ex)
            {
                WorkHelp.AnalysisConsole.Logging.TLog.Watch("Process", ex.Message);
            }
        }

        private static string GetOuputLine(System.Diagnostics.Process p)
        {
            try
            {
                return p.StandardOutput.ReadLine();
            }
            catch(System.Exception ex)
            {
                return ex.Message;
            }
        }

        //private TStrPic MapStrPic(string value)
        //{
        //    var result = new TStrPic { value = value };
        //    using (var image = new System.Drawing.Bitmap(1, 1))
        //    {
        //        using (var g = System.Drawing.Graphics.FromImage(image))
        //        {
        //            //單位
        //            g.PageUnit = System.Drawing.GraphicsUnit.Millimeter;
        //            g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
        //            //font
        //            var f = new System.Drawing.Font("標楷體", 18, System.Drawing.FontStyle.Regular);
        //            var kaiuSize = g.MeasureString(value, f, int.MaxValue, System.Drawing.StringFormat.GenericTypographic);
        //            result.width = kaiuSize.Width;
        //            result.height = kaiuSize.Height;
        //        }
        //    }
        //    return result;
        //}

        //private class TStrPic
        //{
        //    public string value { get; set; }
        //    public float width { get; set; }
        //    public float height { get; set; }
        //}

        //static void XDCDS()
        //{
        //    var fileSrc = @"C:\Aras\Vault\PLMCTA\1\0A\2DBDA7EB14554995CB18FB4648DB5\16169949456616611912608177253181.jpg";
        //    var fileNew = @"C:\Aras\Vault\PLMCTA\1\0A\2DBDA7EB14554995CB18FB4648DB5\16169949456616611912608177253181_new.jpg";

        //    var imgSrc = Image.FromFile(fileSrc);
        //    var imgNew = ResizeImg(imgSrc, 131, 175);
        //    imgNew.Save(fileNew);
        //}

        //static Bitmap ResizeImg(Image image, int width, int height)
        //{
        //    var destRect = new System.Drawing.Rectangle(0, 0, width, height);
        //    var destImage = new System.Drawing.Bitmap(width, height);

        //    destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

        //    using (var graphics = System.Drawing.Graphics.FromImage(destImage))
        //    {
        //        graphics.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceCopy;
        //        graphics.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
        //        graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
        //        graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
        //        graphics.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;

        //        using (var wrapMode = new System.Drawing.Imaging.ImageAttributes())
        //        {
        //            wrapMode.SetWrapMode(WrapMode.TileFlipXY);
        //            graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, System.Drawing.GraphicsUnit.Pixel, wrapMode);
        //        }
        //    }

        //    return destImage;
        //}

        //static void XEFRJIFR()
        //{
        //    for(var i =0; i <= 128; i++)
        //    {
        //        var n = GetSpotCount(i);

        //        Console.WriteLine($"參賽人數: {i}, 隨機抽磅人數: {n}");
        //    }

        //    Console.ReadLine();
        //}

        ////抽磅人數(根據參賽人數)
        //private static int GetSpotCount(int player_count)
        //{
        //    //TKD 版本
        //    if (player_count < 4)
        //    {
        //        return 0;
        //    }
        //    else if (player_count <= 8)
        //    {
        //        return 2;
        //    }
        //    else if (player_count <= 16)
        //    {
        //        return 4;
        //    }
        //    else if (player_count <= 32)
        //    {
        //        return 6;
        //    }
        //    else
        //    {
        //        return (int)(Math.Round(player_count * 0.2, 0, MidpointRounding.AwayFromZero));

        //        //return (int)(player_count * 0.2);
        //    }
        //}


        //static void XXXXXXX()
        //{
        //    int count = CountDays();
        //    Console.WriteLine($"Days: {count}");
        //    Console.ReadLine();
        //}

        //static int CountDays()
        //{
        //    var dtS = new DateTime(2022, 07, 13);
        //    var dtE = new DateTime(2022, 07, 17);

        //    if (dtS.Date == dtE.Date) return 1;

        //    int count = 0;
        //    for (var i = dtS.Date; i <= dtE.Date; i = i.AddDays(1))
        //    {
        //        count++;
        //    }
        //    return count;
        //}

        //static void AAAAAAAAAA()
        //{
        //    var days = 30;

        //    var app_type = "Application";
        //    var app_json = EventLogsJson(app_type, days);
        //    TLog.Watch(app_type, app_json);

        //    var sys_type = "System";
        //    var sys_json = EventLogsJson(sys_type, days);
        //    TLog.Watch(sys_type, sys_json);
        //}

        //static string EventLogsJson(string app_type, int days)
        //{
        //    try
        //    {
        //        var dtMin = DateTime.Now.AddDays(-1 * days);
        //        var dtMax = DateTime.Now;

        //        EventLog tLog = new EventLog();
        //        //選取Application類的Log
        //        tLog.Log = app_type;

        //        //選出Type為Warring與Error，且時間介於選擇區間的Log
        //        var entries =
        //                    from System.Diagnostics.EventLogEntry e2 in
        //                        tLog.Entries
        //                    where e2.EntryType == System.Diagnostics.EventLogEntryType.Error
        //                       && e2.TimeGenerated > dtMin && e2.TimeGenerated < dtMax
        //                       && e2.Message.Contains("關機")

        //                    select new
        //                    {
        //                        e2.Source,
        //                        e2.EntryType,
        //                        e2.EventID,
        //                        e2.InstanceId,
        //                        e2.Message,
        //                        e2.TimeGenerated,
        //                        e2.UserName,
        //                    };

        //        if (entries == null)
        //        {
        //            return string.Empty;
        //        }

        //        return Newtonsoft.Json.JsonConvert.SerializeObject(entries, Newtonsoft.Json.Formatting.Indented);
        //    }
        //    catch (Exception ex)
        //    {
        //        return ex.ToString();
        //    }
        //}


        ///// <summary>
        ///// 事件檢視器
        ///// </summary>
        //public class TEvt
        //{
        //    /// <summary>
        //    /// 來源
        //    /// </summary>
        //    public string Source { get; set; }
        //    /// <summary>
        //    /// 類型
        //    /// </summary>
        //    public int EntryType { get; set; }
        //    /// <summary>
        //    /// 事件識別碼
        //    /// </summary>
        //    public int EventID { get; set; }
        //    /// <summary>
        //    /// ID
        //    /// </summary>
        //    public long InstanceId { get; set; }
        //    /// <summary>
        //    /// 訊息
        //    /// </summary>
        //    public string Message { get; set; }
        //    /// <summary>
        //    /// 紀錄時間
        //    /// </summary>
        //    public DateTime TimeGenerated { get; set; }
        //    /// <summary>
        //    /// 使用者
        //    /// </summary>
        //    public string UserName { get; set; }
        //}
    }
}
