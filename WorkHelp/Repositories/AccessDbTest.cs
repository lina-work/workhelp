﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkHelp.Repositories
{
    public class AccessDbTest
    {
        //public static string GetConnectionString()
        //{
        //    return @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=G:\我的雲端硬碟\22.專案\新 CTA\02.測試區\Database1.accdb;Persist Security Info=False;";
        //}

        public static string GetConnectionString()
        {
            return @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={db_name};Persist Security Info=False;";
        }

        public static string GetConnectionString2007()
        {
            return @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source={db_name};Persist Security Info=False;";
        }


        public static string GetDbName()
        {
            return @"G:\我的雲端硬碟\22.專案\新 CTA\02.測試區\NorthWind.accdb";
        }

        public static System.Data.DataTable GetTableList(string connectionString)
        {
            System.Data.DataTable result = null;
            System.Data.OleDb.OleDbConnection conn = null;
            try
            {
                conn = new System.Data.OleDb.OleDbConnection(connectionString);
                conn.Open();
                result = conn.GetOleDbSchemaTable(System.Data.OleDb.OleDbSchemaGuid.Tables, new Object[] { null, null, null, "Table" });
                conn.Close();
            }
            catch (System.Exception ex)
            {
                result = new System.Data.DataTable();
                if (conn != null)
                {
                    conn.Close();
                }
            }
            return result;
        }

        public static System.Data.DataTable GetEmployees(string connectionString)
        {
            string selectCommandText = "SELECT * FROM [員工]";
            return GetDataTable(connectionString, selectCommandText);
        }

        /// <summary>
        /// 存取 Oledb
        /// </summary>
        /// <param name="connectionString">連線字串</param>
        /// <param name="selectCommandText">執行指令</param>
        /// <returns></returns>
        public static System.Data.DataTable GetDataTable(string connectionString, string selectCommandText)
        {
            try
            {
                System.Data.DataTable Result = null;
                using (System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection(connectionString))
                {
                    using (System.Data.OleDb.OleDbDataAdapter da = new System.Data.OleDb.OleDbDataAdapter(selectCommandText, conn))
                    {
                        try
                        {
                            Result = new System.Data.DataTable();
                            da.Fill(Result);
                        }
                        catch (System.Exception ex)
                        {
                            Result = null;
                        }
                    }
                }
                return Result;
            }
            catch (System.Exception ex)
            {
                return null;
            }
        }
    }
}
