﻿using System;

namespace WorkHelp.Extension
{
    /// <summary>
    /// 快捷方法 (from kotlin)
    /// </summary>
    public static class GenericExtension
    {
        /// <summary>
        /// Calls the specified function block with this value as its argument and returns its result.
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="source"></param>
        /// <param name="block"></param>
        /// <returns></returns>
        public static TResult Let<TSource, TResult>(this TSource source, Func<TSource, TResult> block)
        {
            return block.Invoke(source);
        }

        /// <summary>
        /// Calls the specified function block with this value as its argument and returns this value.
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <param name="source"></param>
        /// <param name="block"></param>
        /// <returns></returns>
        public static TSource Also<TSource>(this TSource source, Action<TSource> block)
        {
            block.Invoke(source);
            return source;
        }
    }
}