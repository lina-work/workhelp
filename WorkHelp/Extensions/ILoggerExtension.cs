﻿using Newtonsoft.Json;
using NLog;
using System;
using System.Text;

namespace WorkHelp.Extensions
{
    /// <summary>
    /// 日誌擴充
    /// </summary>
    public static class ILoggerExtension
    {
        /// <summary>
        /// 日誌內容序列化
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        private static string Serialize(object message)
        {
            if (message == null || message.GetType().IsPrimitive || message is string || message is StringBuilder)
            {
                return message?.ToString();
            }
            else
            {
                return JsonConvert.SerializeObject(message);
            }
        }

        /// <summary>
        /// 同 <see cref="Logger.Trace(Exception,string,object[])"/>
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        public static void StashTrace(this ILogger logger, object message, Exception exception = null)
        {
            logger.Trace(exception, Serialize(message));
        }

        /// <summary>
        /// 同 <see cref="Logger.Debug(Exception,string,object[])"/>
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        public static void StashDebug(this ILogger logger, object message, Exception exception = null)
        {
            logger.Debug(exception, Serialize(message));
        }

        /// <summary>
        /// 同 <see cref="Logger.Info(Exception,string,object[])"/>
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        public static void StashInformation(this ILogger logger, object message, Exception exception = null)
        {
            logger.Info(exception, Serialize(message));
        }

        /// <summary>
        /// 同 <see cref="Logger.Warn(Exception,string,object[])"/>
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        public static void StashWarning(this ILogger logger, object message, Exception exception = null)
        {
            logger.Warn(exception, Serialize(message));
        }

        /// <summary>
        /// 同 <see cref="Logger.Error(Exception,string,object[])"/>
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        public static void StashError(this ILogger logger, object message, Exception exception = null)
        {
            logger.Error(exception, Serialize(message));
        }

        /// <summary>
        /// 同 <see cref="Logger.Fatal(Exception,string,object[])"/>
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        public static void StashCritical(this ILogger logger, object message, Exception exception = null)
        {
            logger.Fatal(exception, Serialize(message));
        }
    }
}