﻿using System;
using System.IO;
using System.IO.Compression;
using System.Text;

namespace WorkHelp.Extensions
{
    /// <summary>
    /// 字串擴充函式
    /// </summary>
    public static class StringExt
    {
        /// <summary>
        /// 取字串的最右邊幾個字 (不區分中英文字元)
        /// </summary>
        /// <param name="val">字串</param>
        /// <param name="len">長度</param>
        /// <returns>回傳結果</returns>
        public static string Right(this string val, int len)
        {
            if (string.IsNullOrEmpty(val) || len < 1)
            {
                return string.Empty;
            }
            else if (val.Length > len)
            {
                return val.Substring(val.Length - len, len);
            }
            else
            {
                return val;
            }
        }

        /// <summary>
        /// 取字串的最左邊幾個字 (不區分中英文字元)
        /// </summary>
        /// <param name="val">字串</param>
        /// <param name="len">長度</param>
        /// <returns>回傳結果</returns>
        public static string Left(this string val, int len)
        {
            if (string.IsNullOrEmpty(val) || len < 1)
            {
                return string.Empty;
            }
            else if (val.Length > len)
            {
                return val.Substring(0, len);
            }
            else
            {
                return val;
            }
        }

        /// <summary>
        /// 使用字元切割字串
        /// </summary>
        public static Tuple<string, string> PartsFirst(this string val, char c)
        {
            int pos = val.IndexOf(c);
            if (pos == -1)
            {
                return Tuple.Create(val, string.Empty);
            }
            else
            {
                return Tuple.Create(val.Substring(0, pos), val.Substring(pos, val.Length - pos));
            }
        }

        #region Mapper

        /// <summary>
        /// DateTimeOffset 轉換為 日期字串
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="def">預設值</param>
        /// <param name="format">格式</param>
        /// <returns>回傳結果</returns>
        public static string StringValue(this DateTimeOffset? value, string def = "", string format = "yyyy/MM/dd")
        {
            if (value.HasValue)
            {
                return StringValue(value.Value, def, format);
            }
            else
            {
                return def;
            }
        }

        /// <summary>
        /// DateTimeOffset 轉換為 日期字串
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="def">預設值</param>
        /// <param name="format">格式</param>
        /// <returns>回傳結果</returns>
        public static string StringValue(this DateTimeOffset value, string def = "", string format = "yyyy/MM/dd")
        {
            if (value.LocalDateTime.Year != 1911)
            {
                return value.LocalDateTime.ToString(format);
            }
            else
            {
                return def;
            }
        }

        /// <summary>
        /// decimal
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="def">預設值</param>
        /// <param name="prefix">格式</param>
        /// <returns>回傳結果</returns>
        public static string StringValue(this decimal? value, string def = "0", string prefix = "")
        {
            if (value.HasValue)
            {
                return StringValue(value.Value, def, prefix);
            }
            else
            {
                return def;
            }
        }

        /// <summary>
        /// decimal
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="def">預設值</param>
        /// <param name="prefix">格式</param>
        /// <returns>回傳結果</returns>
        public static string StringValue(this decimal value, string def = "0", string prefix = "")
        {
            string result = value.ToString("#,##0.0000").TrimEnd('0').TrimEnd('.');
            if (string.IsNullOrWhiteSpace(prefix))
            {
                return result;
            }
            else
            {
                return $"{prefix} {result}";
            }
        }

        #endregion Mapper

        #region GZip

        /// <summary>將傳入的字串以GZip演算法壓縮後，傳回 Base64 編碼字串</summary>
        /// <param name="value">要壓縮的字串</param>
        /// <returns>壓縮後的字串(Base64)</returns>
        public static string Compress(this string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return string.Empty;
            }
            else
            {
                var bytes = Encoding.UTF8.GetBytes(value);
                return CompressBytes(bytes);
            }
        }

        private static string CompressBytes(byte[] bytes)
        {
            if (bytes == null || bytes.Length == 0)
            {
                return string.Empty;
            }
            else
            {
                var inArray = Compress(bytes);
                return CompressToBase64(inArray);
            }
        }

        private static string CompressToBase64(byte[] inArray)
        {
            if (inArray == null || inArray.Length == 0)
            {
                return string.Empty;
            }
            else
            {
                return Convert.ToBase64String(inArray);
            }
        }

        /// <summary>解壓縮(將傳入的二進位字串資料以GZip演算法)</summary>
        /// <param name="value">傳入經 GZip 壓縮後的二進位字串資料</param>
        /// <returns>傳回原後的未壓縮原始字串資料</returns>
        public static string Decompress(this string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return string.Empty;
            }
            else
            {
                try
                {
                    var zippedData = Convert.FromBase64String(value);
                    return DecompressZipped(zippedData);
                }
                catch
                {
                    return string.Empty;
                }
            }
        }

        private static string DecompressZipped(byte[] zippedData)
        {
            if (zippedData == null || zippedData.Length == 0)
            {
                return string.Empty;
            }
            else
            {
                var bytes = Decompress(zippedData);
                return DecompressBytes(bytes);
            }
        }

        private static string DecompressBytes(byte[] bytes)
        {
            if (bytes == null || bytes.Length == 0)
            {
                return string.Empty;
            }
            else
            {
                return Encoding.UTF8.GetString(bytes);
            }
        }

        /// <summary>GZip壓縮</summary>
        private static byte[] Compress(byte[] rawData)
        {
            byte[] result = null;

            using (var memoryStream = new MemoryStream())
            {
                var gZipStream = new GZipStream(memoryStream, CompressionMode.Compress, true);
                gZipStream.Write(rawData, 0, rawData.Length);
                gZipStream.Close();

                result = memoryStream.ToArray();
            }

            return result;
        }

        /// <summary>GZip解壓縮</summary>
        private static byte[] Decompress(byte[] zippedData)
        {
            byte[] result = null;

            using (var ms = new MemoryStream(zippedData))
            {
                var gzipstream = new GZipStream(ms, CompressionMode.Decompress);
                var outBuffer = new MemoryStream();
                var block = new byte[1024];
                while (true)
                {
                    int bytesRead = gzipstream.Read(block, 0, block.Length);
                    if (bytesRead <= 0)
                    {
                        break;
                    }
                    else
                    {
                        outBuffer.Write(block, 0, bytesRead);
                    }
                }
                gzipstream.Close();
                result = outBuffer.ToArray();
            }

            return result;
        }

        #endregion GZip
    }
}