﻿using System;
using System.Threading;

namespace WorkHelp.Context
{
    /// <summary>
    /// Coroutine 的例外處理
    /// </summary>
    public class AsyncSynchronizationContext : SynchronizationContext
    {
        /// <summary>摘要</summary>
        public override void Send(SendOrPostCallback d, object state)
        {
            try
            {
                d(state);
            }
            catch (Exception ex)
            {
                // Put your exception handling logic here.

                Console.WriteLine(ex);
            }
        }

        /// <summary>摘要</summary>
        public override void Post(SendOrPostCallback d, object state)
        {
            try
            {
                d(state);
            }
            catch (Exception ex)
            {
                // Put your exception handling logic here.

                Console.WriteLine(ex);
            }
        }
    }
}