﻿using System;
using System.Configuration;

namespace WorkHelp.Modules
{
    /// <summary>
    /// Web config 設定值
    /// </summary>
    public sealed class AppSettings
    {
        /// <summary>
        /// 建構元
        /// </summary>
        private AppSettings() { }

        /// <summary>
        /// 組態代碼
        /// </summary>
        public static string Mode => ConfigurationManager.AppSettings["app-env"];

        /// <summary>
        /// 通路平台代碼
        /// </summary>
        public static string Platform => ConfigurationManager.AppSettings["Platform"];

        /// <summary>
        /// Ticket Web 網址
        /// </summary>
        public static string WebServer => ConfigurationManager.AppSettings["WebServer"];

        /// <summary>
        /// Ticket Api 網址
        /// </summary>
        public static string ApiServer => ConfigurationManager.AppSettings["ApiServer"];

        /// <summary>
        /// Ticket Api Authorization Value
        /// </summary>
        public static string ApiAuthToken => ConfigurationManager.AppSettings["ApiAuthToken"];

        /// <summary>
        /// 快取資料是否過期的時間間隔，單位為分鐘
        /// </summary>
        public static int ExpireTimeInMinutes => Convert.ToInt32(ConfigurationManager.AppSettings["ExpireTimeInMinutes"]);

        /// <summary>
        /// 應用程序自定義組態檔
        /// </summary>
        public static string AppConfig => ConfigurationManager.AppSettings["AppConfig"];

        /// <summary>
        /// 登入者 Email 快取金鑰
        /// </summary>
        public static string AuthenticatedEmail => ConfigurationManager.AppSettings["EmailCacheName"];

        /// <summary>
        /// 登入者 Email 快取金鑰
        /// </summary>
        public static string AuthenticatedUser => ConfigurationManager.AppSettings["UserCacheName"];
    }
}