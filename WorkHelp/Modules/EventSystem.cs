﻿using System;
using System.Collections.Generic;

namespace WorkHelp.Modules
{
    /// <summary>
    /// 事件系統
    /// </summary>
    public enum EventTopic
    {
        /// <summary>
        /// 瀏覽器選單刷新
        /// </summary>
        ReloadBrowserMenu = 1,
    }

    /// <summary>
    /// 事件系統
    /// </summary>
    public class EventSystem
    {
        /// <summary>
        /// 建構元
        /// </summary>
        protected EventSystem() { }

        #region 簡易事件訂閱

        /// <summary>
        /// 已註冊的事件清單
        /// </summary>
        private static Dictionary<EventTopic, Action> RegistEvents = null;

        /// <summary>
        /// 註冊事件
        /// </summary>
        public static void Register(EventTopic topic, Action action)
        {
            if (RegistEvents == null)
            {
                RegistEvents = new Dictionary<EventTopic, Action>();
            }

            RegistEvents.Add(topic, action);
        }

        /// <summary>
        /// 向事件通報
        /// </summary>
        public static void Notify(EventTopic topic)
        {
            if (RegistEvents == null) return;

            if (RegistEvents.ContainsKey(topic))
            {
                Action action = RegistEvents[topic];
                action();
            }
        }

        #endregion 簡易事件訂閱
    }
}