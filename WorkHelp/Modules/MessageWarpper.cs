﻿namespace WorkHelp.Modules
{
    /// <summary>
    /// Web api 訊息封裝
    /// </summary>
    public class MessageWarpper
    {
        /// <summary>
        /// 建構元
        /// </summary>
        protected MessageWarpper() { }

        /// <summary>
        /// 回應無回傳值的訊息
        /// </summary>
        public static MessageWarpper<object> Success => new MessageWarpper<object>();

        /// <summary>
        /// 轉換為 <see cref="MessageWarpper{T}"/>
        /// </summary>
        /// <typeparam name="T">資料型別</typeparam>
        /// <param name="result">資料內容</param>
        /// <returns></returns>
        public static MessageWarpper<T> FromResult<T>(T result)
        {
            return new MessageWarpper<T>
            {
                Value = result
            };
        }
    }

    /// <summary>
    /// Web api 訊息封裝
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class MessageWarpper<T>
    {
        /// <summary>
        /// 狀態碼, 基本與 http 狀態碼一致
        /// </summary>
        public int Code { get; set; } = 200;

        /// <summary>
        /// 資料
        /// </summary>
        public T Value { get; set; }

        /// <summary>
        /// 回傳訊息
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// 錯誤堆疊
        /// </summary>
        public string Stack { get; set; }
    }
}