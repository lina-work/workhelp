﻿using System;
using System.Linq;

namespace WorkHelp.Logging
{
    /// <summary>
    /// 日誌
    /// </summary>
    public class TLog
    {
        /// <summary>
        /// 建構元
        /// </summary>
        protected TLog() { }

        /// <summary>
        /// 應用程式根目錄
        /// </summary>
        private static string baseName = string.Empty;

        /// <summary>
        /// 應用程式根目錄
        /// </summary>
        private static string baseDirectory = AppDomain.CurrentDomain.BaseDirectory;

        /// <summary>
        /// 鎖
        /// </summary>
        private static object _AppSeetingLock = new object();

        /// <summary>
        /// 應用程式名稱初始化
        /// </summary>
        private static void InitialAppName()
        {
            if (string.IsNullOrWhiteSpace(baseName))
            {
                string appName = System.Reflection.Assembly.GetExecutingAssembly().FullName.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)?.First();
                if (string.IsNullOrWhiteSpace(appName))
                {
                    baseName = "AppLog";
                }
                else
                {
                    baseName = appName;
                }
            }
        }

        /// <summary>
        /// 取得時間耗用計算器
        /// </summary>
        /// <returns></returns>
        public static System.Diagnostics.Stopwatch GetStopwatch()
        {
            System.Diagnostics.Stopwatch result = new System.Diagnostics.Stopwatch();
            result.Reset();
            result.Start();
            return result;
        }

        /// <summary>
        /// 變更根目錄
        /// </summary>
        /// <param name="webRootPath"></param>
        public static void ChangeWebRootPath(string webRootPath)
        {
            if (!string.IsNullOrWhiteSpace(baseDirectory))
            {
                baseDirectory = webRootPath;
            }
        }

        /// <summary>
        /// 起始
        /// </summary>
        public static void Start()
        {
            Watch(message: "-------------------------------");
        }

        /// <summary>
        /// 日誌
        /// </summary>
        /// <param name="stopWatch">時間耗用計算器</param>
        /// <param name="message">訊息</param>
        public static void Watch(System.Diagnostics.Stopwatch stopWatch = null, string message = null)
        {
            InitialAppName();

            DateTime now = DateTime.Now;
            double ticks = double.MinValue;
            double mseconds = double.MinValue;

            if (stopWatch != null)
            {
                stopWatch.Stop();
                ticks = stopWatch.ElapsedTicks;
                mseconds = stopWatch.Elapsed.TotalMilliseconds;
            }

            string file = $"{baseName}.{DateTime.Now.ToString("yyyyMMdd")}.log";
            string path = System.IO.Path.Combine(baseDirectory, "zlogs");

            if (!System.IO.Directory.Exists(path))
            {
                System.IO.Directory.CreateDirectory(path);
            }

            string fullfile = System.IO.Path.Combine(path, file);
            string contents = string.Empty;

            if (stopWatch != null)
            {
                contents = now.ToString("yyyy-MM-dd HH:mm:ss.fff")
                    + " | (ms)" + mseconds.ToString().PadLeft(15, '0').Insert(7, ",").Insert(4, ",").Insert(1, ",")
                    //+ " | (ns " + ticks.ToString().PadLeft(15, '0').Insert(9, ".").Insert(6, ",").Insert(3, ",")
                    + " | " + message + "\r\n";
            }
            else
            {
                contents = now.ToString("yyyy-MM-dd HH:mm:ss.fff")
                    + " | " + message + "\r\n";
            }

            lock (_AppSeetingLock)
            {
                System.IO.File.AppendAllText(fullfile, contents, System.Text.Encoding.UTF8);
            }
        }
    }
}