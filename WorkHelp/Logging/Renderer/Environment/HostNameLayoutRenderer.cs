﻿using NLog;
using NLog.LayoutRenderers;
using System.Text;

namespace WorkHelp.Logging.Renderer.Environment
{
    /// <summary>
    /// 取得主機名稱
    /// </summary>
    [LayoutRenderer("host-name")]
    public class HostNameLayoutRenderer : LayoutRenderer
    {
        /// <inheritdoc />
        protected override void Append(StringBuilder builder, LogEventInfo logEvent)
        {
            builder.Append(System.Environment.MachineName);
        }
    }
}