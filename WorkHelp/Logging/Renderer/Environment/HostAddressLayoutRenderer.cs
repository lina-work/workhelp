﻿using NLog;
using NLog.LayoutRenderers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;

namespace WorkHelp.Logging.Renderer.Environment
{
    /// <summary>
    /// 取得主機的 IP
    /// </summary>
    [LayoutRenderer("host-addresses")]
    public class HostAddressLayoutRenderer : LayoutRenderer
    {
        /// <summary>
        /// 暫存主機 IP
        /// </summary>
        private static Lazy<List<string>> Addresses { get; set; } = new Lazy<List<string>>(() =>
        {
            return NetworkInterface.GetAllNetworkInterfaces()
                .Where(ni => ni.NetworkInterfaceType == NetworkInterfaceType.Wireless80211 || ni.NetworkInterfaceType == NetworkInterfaceType.Ethernet)
                .SelectMany(nis => nis.GetIPProperties().UnicastAddresses)
                .Where(nis => nis.Address.AddressFamily == AddressFamily.InterNetwork)
                .Select(a => $"{a.Address}")
                .ToList();
        });

        /// <inheritdoc />
        protected override void Append(StringBuilder builder, LogEventInfo logEvent)
        {
            builder.Append(string.Join(", ", Addresses.Value));
        }
    }
}