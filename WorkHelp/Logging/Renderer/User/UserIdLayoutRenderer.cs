﻿using NLog;
using NLog.LayoutRenderers;
using System.Text;

namespace WorkHelp.Wpf.Logging.Renderer.User
{
    /// <summary>
    /// 使用者代號
    /// </summary>
    [LayoutRenderer("user-id")]
    public class UserIdLayoutRenderer : LayoutRenderer
    {
        protected override void Append(StringBuilder builder, LogEventInfo logEvent)
        {
            builder.Append(string.Empty);
        }
    }
}