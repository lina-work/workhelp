﻿using NLog;
using NLog.LayoutRenderers;
using System.Text;

namespace WorkHelp.Logging.Renderer.Config
{
    /// <summary>
    /// 取得設定的應用程式代號
    /// </summary>
    [LayoutRenderer("app-id")]
    public class AppIdLayoutRenderer : LayoutRenderer
    {
        /// <inheritdoc />
        protected override void Append(StringBuilder builder, LogEventInfo logEvent)
        {
            builder.Append(System.Configuration.ConfigurationManager.AppSettings["app-id"] ?? string.Empty);
        }
    }
}