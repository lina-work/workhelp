﻿using NLog;
using NLog.LayoutRenderers;
using System.Text;

namespace WorkHelp.Logging.Renderer.Config
{
    /// <summary>
    /// 取得設定的應用程式環境
    /// </summary>
    [LayoutRenderer("app-env")]
    public class AppEnvLayoutRenderer : LayoutRenderer
    {
        /// <inheritdoc />
        protected override void Append(StringBuilder builder, LogEventInfo logEvent)
        {
            builder.Append(GetEnvironment());
        }

        private string GetEnvironment()
        {
#if DEBUG
            return System.Environment.UserName;
#else
            string env = System.Configuration.ConfigurationManager.AppSettings["app-env"];
            return string.IsNullOrWhiteSpace(env) ? System.Environment.UserName : env;
#endif
        }
    }
}