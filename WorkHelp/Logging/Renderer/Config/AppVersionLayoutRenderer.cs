﻿using NLog;
using NLog.LayoutRenderers;
using System.Text;

namespace WorkHelp.Logging.Renderer.Config
{
    /// <summary>
    /// 取得設定的應用程式版本
    /// </summary>
    [LayoutRenderer("app-version")]
    public class AppVersionLayoutRenderer : LayoutRenderer
    {
        /// <inheritdoc />
        protected override void Append(StringBuilder builder, LogEventInfo logEvent)
        {
            builder.Append(System.Configuration.ConfigurationManager.AppSettings["app-version"] ?? string.Empty);
        }
    }
}