﻿using NLog;
using NLog.LayoutRenderers;
using System.Text;

namespace WorkHelp.Logging.Renderer.Config
{
    /// <summary>
    /// 取得設定的程式名稱
    /// </summary>
    [LayoutRenderer("app-name")]
    public class AppNameLayoutRenderer : LayoutRenderer
    {
        /// <inheritdoc />
        protected override void Append(StringBuilder builder, LogEventInfo logEvent)
        {
            builder.Append(System.Configuration.ConfigurationManager.AppSettings["app-name"] ?? string.Empty);
        }
    }
}