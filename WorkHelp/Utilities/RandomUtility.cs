﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace WorkHelp.Utilities
{
    /// <summary>
    /// 隨機函式
    /// </summary>
    public static class RandomUtility
    {
        /// <summary>
        /// 取亂數
        /// </summary>
        /// <param name="length">亂數長度</param>
        /// <returns></returns>
        public static string RandomKey(int length)
        {
            RNGCryptoServiceProvider csp = new RNGCryptoServiceProvider();
            byte[] byteCsp = new byte[4];

            StringBuilder sb = new StringBuilder();
            char[] chars = "0123456789abcdefghijklmnopqrstuvwxyz".ToCharArray();
            int charsMax = chars.Length - 1;

            for (int i = 0; i < length; i++)
            {
                csp.GetBytes(byteCsp);
                int value = BitConverter.ToInt32(byteCsp, 0);
                value = value % (charsMax + 1);
                if (value < 0)
                {
                    value = -value;
                }

                sb.Append(chars[value]);
            }

            return sb.ToString();
        }

        /// <summary>
        /// 取得亂數字串 (數字, 避免英數混雜)
        /// </summary>
        /// <param name="length">回傳字串長度</param>
        /// <returns></returns>
        public static string RandomAzNumber(int length)
        {
            var r = new System.Random();
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < length; i++)
            {
                sb.Append(r.Next(0, 10));
            }
            return sb.ToString();
        }

        /// <summary>
        /// 取得亂數數字
        /// </summary>
        /// <param name="length">回傳字串長度</param>
        /// <param name="length">回傳字串長度</param>
        /// <returns></returns>
        public static string RandomNumber(int min, int max)
        {
            var r = new System.Random();
            return r.Next(min, max).ToString();
        }
    }
}