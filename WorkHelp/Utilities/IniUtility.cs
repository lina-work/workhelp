﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;

namespace WorkHelp.Utilities
{
    /// <summary>
    /// ini 組態檔通用函式
    /// </summary>
    public class IniUtility
    {
        /// <summary>
        /// 建構元
        /// </summary>
        protected IniUtility() { }

        /// <summary>
        /// 組態檔路徑 (內部)
        /// </summary>
        private static string _iniPath { get; set; }

        /// <summary>
        /// 組態檔路徑
        /// </summary>
        public static string IniPath
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_iniPath))
                {
                    _iniPath = AppDomain.CurrentDomain.BaseDirectory.TrimEnd('\\') + "\\" + "config.ini";
                    if (!File.Exists(_iniPath))
                    {
                        File.AppendAllText(_iniPath, string.Empty, Encoding.UTF8);
                    }
                }
                return _iniPath;
            }
        }

        [DllImport("kernel32", CharSet = CharSet.Unicode)]
        private static extern long WritePrivateProfileString(string section, string key, string val, string filePath);

        [DllImport("kernel32", CharSet = CharSet.Unicode)]
        private static extern int GetPrivateProfileString(string section, string key, string def, StringBuilder retVal, int size, string filePath);

        /// <summary>
        /// 寫入組態檔
        /// </summary>
        private static void IniWriteValue(string section, string key, string value)
        {
            WritePrivateProfileString(section, key, value, IniPath);
        }

        /// <summary>
        /// 讀取組態檔
        /// </summary>
        private static string IniReadValue(string section, string key)
        {
            StringBuilder temp = new StringBuilder(255);
            int i = GetPrivateProfileString(section, key, "", temp, 255, IniPath);
            return temp.ToString();
        }

        /// <summary>
        /// 讀取組態檔 - 字串
        /// </summary>
        public static string ReadStr(string section, string key, string def = "")
        {
            string value = IniReadValue(section, key);

            if (!string.IsNullOrWhiteSpace(value))
            {
                return value;
            }
            else
            {
                return def;
            }
        }

        /// <summary>
        /// 讀取組態檔 - 布林值
        /// </summary>
        public static bool ReadBool(string section, string key, bool def = false)
        {
            string value = IniReadValue(section, key);

            if (bool.TryParse(value, out bool result))
            {
                return result;
            }
            else
            {
                return def;
            }
        }

        /// <summary>
        /// 讀取組態檔 - int
        /// </summary>
        public static int ReadInt32(string section, string key, int def = 0)
        {
            string value = IniReadValue(section, key);

            if (int.TryParse(value, out int result))
            {
                return result;
            }
            else
            {
                return def;
            }
        }

        /// <summary>
        /// 寫入組態檔 - 字串
        /// </summary>
        public static void WriteStr(string section, string key, string value)
        {
            IniWriteValue(section, key, value);
        }
    }
}