﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace WorkHelp.Utilities
{
    /// <summary>
    /// 文件通用函式
    /// </summary>
    public class FileUtility
    {
        /// <summary>
        /// 建構元
        /// </summary>
        protected FileUtility() { }

        /// <summary>
        /// 取得檔案
        /// </summary>
        public static string[] GetArray(string folderName, string fileName)
        {
            // 檔案資料夾
            string folder = CheckFolder(folderName);
            // 檔案完整路徑 (含副檔名)
            string path = Path.Combine(folder, fileName);
            
            return File.ReadAllLines(path, Encoding.UTF8);
        }

        /// <summary>
        /// 寫入檔案 (複寫)
        /// </summary>
        public static void WriteFile(string folderName, string fileName, string contents)
        {
            // 檔案資料夾
            string folder = CheckFolder(folderName);
            // 檔案完整路徑 (含副檔名)
            string path = Path.Combine(folder, fileName);
            // 複寫檔案
            File.WriteAllText(path, contents, Encoding.UTF8);
        }


        /// <summary>
        /// 寫入檔案 (複寫) (外部路徑)
        /// </summary>
        public static void TargetFile(string folder, string fileName, string contents)
        {
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }

            // 檔案完整路徑 (含副檔名)
            string path = Path.Combine(folder, fileName);
            // 複寫檔案
            File.WriteAllText(path, contents, Encoding.UTF8);
        }

        /// <summary>
        /// 寫入檔案 (附加)
        /// </summary>
        public static void AppendFile(string folderName, string fileName, string contents)
        {
            // 檔案資料夾
            string folder = CheckFolder(folderName);
            // 檔案完整路徑 (含副檔名)
            string path = Path.Combine(folder, fileName);
            // 附加檔案
            File.AppendAllText(path, contents, Encoding.UTF8);
        }

        /// <summary>
        /// 檢查資料夾
        /// </summary>
        private static string CheckFolder(string folderName)
        {
            // 應用程式路徑
            string app = AppDomain.CurrentDomain.BaseDirectory;
            // 檔案資料夾
            string folder = Path.Combine(app, folderName);

            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }

            return folder;
        }

        /// <summary>
        /// 開啟檔案總管
        /// </summary>
        public static void Explorer(string folderName)
        {
            // 應用程式路徑
            string app = AppDomain.CurrentDomain.BaseDirectory;
            // 檔案資料夾
            string folder = Path.Combine(app, folderName);
            // 檔案總管
            string exe = @"C:\Users\Administrator\source\lina repos\workhelp\WorkHelpWPF\bin\Debug\WorkHelp.Wpf.exe";

            Process.Start(exe);
        }

        /// <summary>
        /// 開啟檔案總管
        /// </summary>
        public static void ExplorerSingle(string folderName)
        {
            Process[] plist = Process.GetProcessesByName("explorer");

            if (plist == null || plist.Length == 0)
            {
                SingleExplorer(folderName);
            }
            else
            {
                bool exists = false;
                string key = folderName.ToLower();
                foreach (Process p in plist)
                {
                    string pname = p.MainWindowTitle?.ToLower();
                    if (!string.IsNullOrWhiteSpace(pname) && key.Contains(pname))
                    {
                        exists = true;
                        //// 取得目前 Shell 的所有視窗
                        //SHDocVw.ShellWindows shellWindows = new SHDocVw.ShellWindowsClass();
                        //foreach (SHDocVw.InternetExplorer ie in shellWindows)
                        //{
                        //    // 判斷視窗是否為 iexplore
                        //    if (Path.GetFileNameWithoutExtension(ie.FullName).ToLower().Equals("iexplore"))
                        //    {
                        //        // 判斷此 Internet Explorer 是否為正在使用視窗[前景]
                        //        if (ie.HWND == GetForegroundWindow().ToInt32())
                        //        {
                        //            this.txtURL.Text = ie.LocationURL;
                        //        }

                        //        this.lbURL.Items.Add(ie.LocationURL);
                        //    }
                        //}
                    }
                }

                if (!exists)
                {
                    SingleExplorer(folderName);
                }
            }
        }

        private static void SingleExplorer(string folderName)
        {
            // 應用程式路徑
            string app = AppDomain.CurrentDomain.BaseDirectory;
            // 檔案資料夾
            string folder = Path.Combine(app, folderName);
            // 檔案總管
            string exe = @"C:\Windows\explorer.exe";

            System.Diagnostics.Process process = new System.Diagnostics.Process();

            process.StartInfo.UseShellExecute = true;

            process.StartInfo.FileName = exe;

            process.StartInfo.Arguments = $" {folder}";

            process.Start();
        }
    }
}