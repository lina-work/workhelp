﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;

namespace WorkHelp.Errors
{
    /// <summary>
    /// 輸入參數錯誤
    /// </summary>
    public class InvalidInputException : Exception
    {
        /// <summary>
        /// 驗證失敗的內容
        /// </summary>
        public Dictionary<string, string> Results { get; } = new Dictionary<string, string>();

        /// <summary>
        /// 建構元
        /// </summary>
        public InvalidInputException()
        {
        }

        /// <summary>
        /// 建構元
        /// </summary>
        /// <param name="message">錯誤訊息</param>
        public InvalidInputException(string message) : base(message)
        {
        }

        /// <summary>
        /// 建構元
        /// </summary>
        /// <param name="message">錯誤訊息</param>
        /// <param name="innerException">例外物件</param>
        public InvalidInputException(string message, Exception innerException) : base(message, innerException)
        {
        }

        /// <summary>
        /// 建構元
        /// </summary>
        /// <param name="info">序列化訊息物件</param>
        /// <param name="context">串流上下文</param>
        protected InvalidInputException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        /// <summary>
        /// 轉換為字串
        /// </summary>
        /// <returns>回傳字串</returns>
        public override string ToString()
        {
            string error = string.Join(Environment.NewLine, Results.Keys.Select(key =>
            {
                string message = string.Join(", ", Results[key]);
                return $"{key}: {message}";
            }));
            return $"{error}{Environment.NewLine}{base.ToString()}";
        }
    }

    /// <summary>
    /// 驗證處理工廠
    /// </summary>
    public class InvalidInputExceptionBuilder
    {
        /// <summary>
        /// 建立驗證處理工廠
        /// </summary>
        /// <returns></returns>
        public static InvalidInputExceptionBuilder Create() => new InvalidInputExceptionBuilder();

        /// <summary>
        /// 錯誤內容
        /// </summary>
        protected Dictionary<string, HashSet<string>> Result { get; } = new Dictionary<string, HashSet<string>>();

        /// <summary>
        /// 建構元
        /// </summary>
        protected InvalidInputExceptionBuilder()
        {
        }

        /// <summary>
        /// 驗證
        /// </summary>
        /// <param name="valid">驗證結果</param>
        /// <param name="name">參數名稱</param>
        /// <param name="message">訊息</param>
        /// <returns></returns>
        public InvalidInputExceptionBuilder Validate(bool valid, string name, string message = null)
        {
            if (!valid)
            {
                HashSet<string> messages = Result.ContainsKey(name) ? Result[name] : new HashSet<string>();
                messages.Add(message ?? $"參數 {name} 錯誤");
                Result[name] = messages;
            }

            return this;
        }

        /// <summary>
        /// 驗證
        /// </summary>
        /// <param name="valid">驗證結果</param>
        /// <param name="name">參數名稱</param>
        /// <param name="message">訊息</param>
        /// <returns></returns>
        public InvalidInputExceptionBuilder Validate(Func<bool> valid, string name, string message = null)
        {
            return Validate(valid.Invoke(), name, message);
        }

        /// <summary>
        /// 驗證資料有值
        /// </summary>
        /// <param name="value">資料物件</param>
        /// <param name="name">參數名稱</param>
        /// <param name="message">訊息</param>
        /// <param name="block"></param>
        /// <returns></returns>
        public InvalidInputExceptionBuilder ValidateNotNull<TSource>(TSource value, string name, string message = null, Action<TSource, InvalidInputExceptionBuilder> block = null)
        {
            if (value == null)
            {
                return Validate(false, name, message);
            }
            else
            {
                block?.Invoke(value, this);
                return this;
            }
        }

        /// <summary>
        /// 驗證資料有值
        /// </summary>
        /// <typeparam name="TSource">資料型別</typeparam>
        /// <param name="value">資料物件</param>
        /// <param name="name">參數名稱</param>
        /// <param name="message">訊息</param>
        /// <param name="block"></param>
        /// <returns></returns>
        public InvalidInputExceptionBuilder VlidateNotNull<TSource>(TSource? value, string name, string message = null, Action<TSource, InvalidInputExceptionBuilder> block = null) where TSource : struct
        {
            if (!value.HasValue)
            {
                return Validate(false, name, message);
            }
            else
            {
                block?.Invoke(value.Value, this);
                return this;
            }
        }

        #region 文字/格式驗證

        /// <summary>
        /// 驗證字串是否不為空字串或空白
        /// </summary>
        /// <param name="value">參數值</param>
        /// <param name="name">參數名稱</param>
        /// <param name="message">訊息</param>
        /// <returns></returns>
        public InvalidInputExceptionBuilder ValidateNotNullOrEmpty(string value, string name, string message = null)
        {
            return Validate(!string.IsNullOrWhiteSpace(value), name, message ?? $"{name} 為必要參數");
        }

        /// <summary>
        /// 驗證文字格式
        /// </summary>
        /// <param name="value">文字內容</param>
        /// <param name="pattern">正則表達式</param>
        /// <param name="name">參數名稱</param>
        /// <param name="message">訊息</param>
        /// <returns></returns>
        public InvalidInputExceptionBuilder ValidateFormat(string value, Regex pattern, string name, string message = null)
        {
            if (pattern == null) throw new ArgumentNullException("pattern");
            return string.IsNullOrWhiteSpace(value) ? this : Validate(pattern.IsMatch(value), name, message);
        }

        /// <summary>
        /// 驗證文字格式
        /// </summary>
        /// <param name="value">文字內容</param>
        /// <param name="pattern">正則表達式</param>
        /// <param name="name">參數名稱</param>
        /// <param name="message">訊息</param>
        /// <returns></returns>
        public InvalidInputExceptionBuilder ValidateFormat(string value, string pattern, string name, string message = null)
        {
            if (string.IsNullOrWhiteSpace(pattern)) throw new ArgumentNullException("pattern");
            return string.IsNullOrWhiteSpace(value) ? this : Validate(new Regex(pattern).IsMatch(value), name, message);
        }

        /// <summary>
        /// 驗證電子郵件格式
        /// </summary>
        /// <param name="email">電子郵件</param>
        /// <param name="name">參數名稱</param>
        /// <param name="message">訊息</param>
        /// <returns></returns>
        public InvalidInputExceptionBuilder ValidateEmail(string email, string name, string message = null)
        {
            Regex regex = new Regex(@"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$");
            return string.IsNullOrWhiteSpace(email) ? this : ValidateFormat(email, regex, name, message ?? $"{name} 電子郵件格式錯誤");
        }

        #endregion 文字/格式驗證

        #region 數值驗證

        #region 最大值

        /// <summary>
        /// 驗證數值最大值
        /// </summary>
        /// <param name="value">數值</param>
        /// <param name="max">最大值</param>
        /// <param name="name">參數名稱</param>
        /// <param name="message">訊息</param> <returns></returns>
        public InvalidInputExceptionBuilder ValidateMaximum(sbyte? value, int max, string name, string message = null)
        {
            return Validate(!value.HasValue || value <= max, name, message ?? $"{name} 的值必須小於等於 {max}");
        }

        /// <summary>
        /// 驗證數值最大值
        /// </summary>
        /// <param name="value">數值</param>
        /// <param name="max">最大值</param>
        /// <param name="name">參數名稱</param>
        /// <param name="message">訊息</param>
        /// <returns></returns>
        public InvalidInputExceptionBuilder ValidateMaximum(byte? value, int max, string name, string message = null)
        {
            return Validate(!value.HasValue || value <= max, name, message ?? $"{name} 的值必須小於等於 {max}");
        }

        /// <summary>
        /// 驗證數值最大值
        /// </summary>
        /// <param name="value">數值</param>
        /// <param name="max">最大值</param>
        /// <param name="name">參數名稱</param>
        /// <param name="message">訊息</param>
        /// <returns></returns>
        public InvalidInputExceptionBuilder ValidateMaximum(short? value, int max, string name, string message = null)
        {
            return Validate(!value.HasValue || value <= max, name, message ?? $"{name} 的值必須小於等於 {max}");
        }

        /// <summary>
        /// 驗證數值最大值
        /// </summary>
        /// <param name="value">數值</param>
        /// <param name="max">最大值</param>
        /// <param name="name">參數名稱</param>
        /// <param name="message">訊息</param>
        /// <returns></returns>
        public InvalidInputExceptionBuilder ValidateMaximum(int? value, int max, string name, string message = null)
        {
            return Validate(!value.HasValue || value <= max, name, message ?? $"{name} 的值必須小於等於 {max}");
        }

        /// <summary>
        /// 驗證數值最大值
        /// </summary>
        /// <param name="value">數值</param>
        /// <param name="max">最大值</param>
        /// <param name="name">參數名稱</param>
        /// <param name="message">訊息</param>
        /// <returns></returns>
        public InvalidInputExceptionBuilder ValidateMaximum(long? value, long max, string name, string message = null)
        {
            return Validate(!value.HasValue || value <= max, name, message ?? $"{name} 的值必須小於等於 {max}");
        }

        /// <summary>
        /// 驗證數值最大值
        /// </summary>
        /// <param name="value">數值</param>
        /// <param name="max">最大值</param>
        /// <param name="name">參數名稱</param>
        /// <param name="message">訊息</param>
        /// <returns></returns>
        public InvalidInputExceptionBuilder ValidateMaximum(float? value, double max, string name, string message = null)
        {
            return Validate(!value.HasValue || value <= max, name, message ?? $"{name} 的值必須小於等於 {max}");
        }

        /// <summary>
        /// 驗證數值最大值
        /// </summary>
        /// <param name="value">數值</param>
        /// <param name="max">最大值</param>
        /// <param name="name">參數名稱</param>
        /// <param name="message">訊息</param>
        /// <returns></returns>
        public InvalidInputExceptionBuilder ValidateMaximum(double? value, double max, string name, string message = null)
        {
            return Validate(!value.HasValue || value <= max, name, message ?? $"{name} 的值必須小於等於 {max}");
        }

        /// <summary>
        /// 驗證數值最大值
        /// </summary>
        /// <param name="value">數值</param>
        /// <param name="max">最大值</param>
        /// <param name="name">參數名稱</param>
        /// <param name="message">訊息</param>
        /// <returns></returns>
        public InvalidInputExceptionBuilder ValidateMaximum(decimal? value, decimal max, string name, string message = null)
        {
            return Validate(!value.HasValue || value <= max, name, message ?? $"{name} 的值必須小於等於 {max}");
        }

        #endregion 最大值

        #region 最小值

        /// <summary>
        /// 驗證數值最小值
        /// </summary>
        /// <param name="value">數值</param>
        /// <param name="min">最小值</param>
        /// <param name="name">參數名稱</param>
        /// <param name="message">訊息</param>
        /// <returns></returns>
        public InvalidInputExceptionBuilder ValidateMinimum(sbyte? value, int min, string name, string message = null)
        {
            return Validate(!value.HasValue || value >= min, name, message ?? $"{name} 的值必須大於等於 {min}");
        }

        /// <summary>
        /// 驗證數值最小值
        /// </summary>
        /// <param name="value">數值</param>
        /// <param name="min">最小值</param>
        /// <param name="name">參數名稱</param>
        /// <param name="message">訊息</param>
        /// <returns></returns>
        public InvalidInputExceptionBuilder ValidateMinimum(byte? value, int min, string name, string message = null)
        {
            return Validate(!value.HasValue || value >= min, name, message ?? $"{name} 的值必須大於等於 {min}");
        }

        /// <summary>
        /// 驗證數值最小值
        /// </summary>
        /// <param name="value">數值</param>
        /// <param name="min">最小值</param>
        /// <param name="name">參數名稱</param>
        /// <param name="message">訊息</param>
        /// <returns></returns>
        public InvalidInputExceptionBuilder ValidateMinimum(short? value, int min, string name, string message = null)
        {
            return Validate(!value.HasValue || value >= min, name, message ?? $"{name} 的值必須大於等於 {min}");
        }

        /// <summary>
        /// 驗證數值最小值
        /// </summary>
        /// <param name="value">數值</param>
        /// <param name="min">最小值</param>
        /// <param name="name">參數名稱</param>
        /// <param name="message">訊息</param>
        /// <returns></returns>
        public InvalidInputExceptionBuilder ValidateMinimum(int? value, int min, string name, string message = null)
        {
            return Validate(!value.HasValue || value >= min, name, message ?? $"{name} 的值必須大於等於 {min}");
        }

        /// <summary>
        /// 驗證數值最小值
        /// </summary>
        /// <param name="value">數值</param>
        /// <param name="min">最小值</param>
        /// <param name="name">參數名稱</param>
        /// <param name="message">訊息</param>
        /// <returns></returns>
        public InvalidInputExceptionBuilder ValidateMinimum(long? value, long min, string name, string message = null)
        {
            return Validate(!value.HasValue || value >= min, name, message ?? $"{name} 的值必須大於等於 {min}");
        }

        /// <summary>
        /// 驗證數值最小值
        /// </summary>
        /// <param name="value">數值</param>
        /// <param name="min">最小值</param>
        /// <param name="name">參數名稱</param>
        /// <param name="message">訊息</param>
        /// <returns></returns>
        public InvalidInputExceptionBuilder ValidateMinimum(float? value, double min, string name, string message = null)
        {
            return Validate(!value.HasValue || value >= min, name, message ?? $"{name} 的值必須大於等於 {min}");
        }

        /// <summary>
        /// 驗證數值最小值
        /// </summary>
        /// <param name="value">數值</param>
        /// <param name="min">最小值</param>
        /// <param name="name">參數名稱</param>
        /// <param name="message">訊息</param>
        /// <returns></returns>
        public InvalidInputExceptionBuilder ValidateMinimum(double? value, double min, string name, string message = null)
        {
            return Validate(!value.HasValue || value >= min, name, message ?? $"{name} 的值必須大於等於 {min}");
        }

        /// <summary>
        /// 驗證數值最小值
        /// </summary>
        /// <param name="value">數值</param>
        /// <param name="min">最小值</param>
        /// <param name="name">參數名稱</param>
        /// <param name="message">訊息</param>
        /// <returns></returns>
        public InvalidInputExceptionBuilder ValidateMinimum(decimal? value, decimal min, string name, string message = null)
        {
            return Validate(!value.HasValue || value >= min, name, message ?? $"{name} 的值必須大於等於 {min}");
        }

        #endregion 最小值

        #region 範圍

        /// <summary>
        /// 驗證數值最小值
        /// </summary>
        /// <param name="value">數值</param>
        /// <param name="min">最小值</param>
        /// <param name="max">最大值</param>
        /// <param name="name">參數名稱</param>
        /// <param name="message">訊息</param>
        /// <returns></returns>
        public InvalidInputExceptionBuilder ValidateRange(sbyte? value, int min, int max, string name, string message = null)
        {
            return Validate(!value.HasValue || (value >= min && value <= max), name, message ?? $"{name} 的值需介於 {min} 與 {max} 之間");
        }

        /// <summary>
        /// 驗證數值最小值
        /// </summary>
        /// <param name="value">數值</param>
        /// <param name="min">最小值</param>
        /// <param name="max">最大值</param>
        /// <param name="name">參數名稱</param>
        /// <param name="message">訊息</param>
        /// <returns></returns>
        public InvalidInputExceptionBuilder ValidateRange(byte? value, int min, int max, string name, string message = null)
        {
            return Validate(!value.HasValue || (value >= min && value <= max), name, message ?? $"{name} 的值需介於 {min} 與 {max} 之間");
        }

        /// <summary>
        /// 驗證數值最小值
        /// </summary>
        /// <param name="value">數值</param>
        /// <param name="min">最小值</param>
        /// <param name="max">最大值</param>
        /// <param name="name">參數名稱</param>
        /// <param name="message">訊息</param>
        /// <returns></returns>
        public InvalidInputExceptionBuilder ValidateRange(short? value, int min, int max, string name, string message = null)
        {
            return Validate(!value.HasValue || (value >= min && value <= max), name, message ?? $"{name} 的值需介於 {min} 與 {max} 之間");
        }

        /// <summary>
        /// 驗證數值最小值
        /// </summary>
        /// <param name="value">數值</param>
        /// <param name="min">最小值</param>
        /// <param name="max">最大值</param>
        /// <param name="name">參數名稱</param>
        /// <param name="message">訊息</param>
        /// <returns></returns>
        public InvalidInputExceptionBuilder ValidateRange(int? value, int min, int max, string name, string message = null)
        {
            return Validate(!value.HasValue || (value >= min && value <= max), name, message ?? $"{name} 的值需介於 {min} 與 {max} 之間");
        }

        /// <summary>
        /// 驗證數值最小值
        /// </summary>
        /// <param name="value">數值</param>
        /// <param name="min">最小值</param>
        /// <param name="max">最大值</param>
        /// <param name="name">參數名稱</param>
        /// <param name="message">訊息</param>
        /// <returns></returns>
        public InvalidInputExceptionBuilder ValidateRange(long? value, long min, long max, string name, string message = null)
        {
            return Validate(!value.HasValue || (value >= min && value <= max), name, message ?? $"{name} 的值需介於 {min} 與 {max} 之間");
        }

        /// <summary>
        /// 驗證數值最小值
        /// </summary>
        /// <param name="value">數值</param>
        /// <param name="min">最小值</param>
        /// <param name="max">最大值</param>
        /// <param name="name">參數名稱</param>
        /// <param name="message">訊息</param>
        /// <returns></returns>
        public InvalidInputExceptionBuilder ValidateRange(float? value, double min, double max, string name, string message = null)
        {
            return Validate(!value.HasValue || (value >= min && value <= max), name, message ?? $"{name} 的值需介於 {min} 與 {max} 之間");
        }

        /// <summary>
        /// 驗證數值最小值
        /// </summary>
        /// <param name="value">數值</param>
        /// <param name="min">最小值</param>
        /// <param name="max">最大值</param>
        /// <param name="name">參數名稱</param>
        /// <param name="message">訊息</param>
        /// <returns></returns>
        public InvalidInputExceptionBuilder ValidateRange(double? value, double min, double max, string name, string message = null)
        {
            return Validate(!value.HasValue || (value >= min && value <= max), name, message ?? $"{name} 的值需介於 {min} 與 {max} 之間");
        }

        /// <summary>
        /// 驗證數值最小值
        /// </summary>
        /// <param name="value">數值</param>
        /// <param name="min">最小值</param>
        /// <param name="max">最大值</param>
        /// <param name="name">參數名稱</param>
        /// <param name="message">訊息</param>
        /// <returns></returns>
        public InvalidInputExceptionBuilder ValidateRange(decimal? value, decimal min, decimal max, string name, string message = null)
        {
            return Validate(!value.HasValue || (value >= min && value <= max), name, message ?? $"{name} 的值需介於 {min} 與 {max} 之間");
        }

        #endregion 範圍

        #endregion 數值驗證

        /// <summary>
        /// 驗證多個項目
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <param name="sources"></param>
        /// <param name="block"></param>
        /// <returns></returns>
        public InvalidInputExceptionBuilder ValidateAll<TSource>(IEnumerable<TSource> sources, Action<TSource, InvalidInputExceptionBuilder> block)
        {
            if (sources != null)
            {
                foreach (TSource source in sources)
                {
                    block?.Invoke(source, this);
                }
            }

            return this;
        }

        /// <summary>
        /// 驗證失敗時拋出錯誤
        /// </summary>
        /// <returns></returns>
        public virtual void ThrowIfError()
        {
            if (Result.Count > 0)
            {
                InvalidInputException error = new InvalidInputException();

                foreach (string name in Result.Keys)
                {
                    error.Results[name] = string.Join(Environment.NewLine, Result[name]);
                }

                Console.WriteLine(error);

                throw error;
            }
        }
    }
}