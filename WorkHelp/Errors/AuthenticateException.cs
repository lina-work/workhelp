﻿using System;
using System.Runtime.Serialization;

namespace WorkHelp.Errors
{
    /// <summary>
    /// 認證失敗例外
    /// </summary>
    public class AuthenticateException : Exception
    {
        /// <summary>
        /// 建構元
        /// </summary>
        public AuthenticateException()
        {
        }

        /// <summary>
        /// 建構元
        /// </summary>
        /// <param name="message">錯誤訊息</param>
        public AuthenticateException(string message) : base(message)
        {
        }

        /// <summary>
        /// 建構元
        /// </summary>
        /// <param name="message">錯誤訊息</param>
        /// <param name="innerException">例外物件</param>
        public AuthenticateException(string message, Exception innerException) : base(message, innerException)
        {
        }

        /// <summary>
        /// 建構元
        /// </summary>
        /// <param name="info">序列化訊息物件</param>
        /// <param name="context">串流上下文</param>
        protected AuthenticateException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}