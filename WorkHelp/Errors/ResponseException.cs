﻿using System;

namespace WorkHelp.Errors
{
    /// <summary>
    /// Response 例外
    /// </summary>
    public class ResponseException : Exception
    {
        /// <summary>
        /// 狀態
        /// </summary>
        public int StatusCode { get; set; }

        /// <summary>
        /// 建構元
        /// </summary>
        public ResponseException() : base()
        {
        }

        /// <summary>
        /// 建構元
        /// </summary>
        /// <param name="message">錯誤訊息</param>
        public ResponseException(string message) : base(message)
        {
        }
    }
}