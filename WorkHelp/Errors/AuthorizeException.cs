﻿using System;
using System.Runtime.Serialization;

namespace WorkHelp.Errors
{
    /// <summary>
    /// 授權失敗例外
    /// </summary>
    public class AuthorizeException : Exception
    {
        /// <summary>
        /// 建構元
        /// </summary>
        public AuthorizeException()
        {
        }

        /// <summary>
        /// 建構元
        /// </summary>
        /// <param name="message">錯誤訊息</param>
        public AuthorizeException(string message) : base(message)
        {
        }

        /// <summary>
        /// 建構元
        /// </summary>
        /// <param name="message">錯誤訊息</param>
        /// <param name="innerException">例外物件</param>
        public AuthorizeException(string message, Exception innerException) : base(message, innerException)
        {
        }

        /// <summary>
        /// 建構元
        /// </summary>
        /// <param name="info">序列化訊息物件</param>
        /// <param name="context">串流上下文</param>
        protected AuthorizeException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}