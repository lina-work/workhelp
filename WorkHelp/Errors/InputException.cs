﻿using System.Collections.Generic;

namespace WorkHelp.Errors
{
    /// <summary>
    /// Input 例外
    /// </summary>
    public class InputException : ResponseException
    {
        /// <summary>
        /// 建構元
        /// </summary>
        protected InputException()
        {
        }

        /// <summary>
        /// 建構元
        /// </summary>
        /// <param name="errors">錯誤訊息字典</param>
        public InputException(Dictionary<string, string> errors) : base()
        {
            if (errors != null)
            {
                foreach (string key in errors.Keys)
                {
                    Errors[key] = errors[key];
                }
            }
        }

        /// <summary>
        /// 自定義錯誤Dictionary
        /// </summary>
        public Dictionary<string, string> Errors { get; } = new Dictionary<string, string>();
    }
}