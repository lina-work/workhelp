﻿using System;
using System.Runtime.Serialization;

namespace WorkHelp.Errors
{
    /// <summary>
    /// 設定檔錯誤
    /// </summary>
    public class ConfigException : Exception
    {
        /// <summary>
        /// 建構元
        /// </summary>
        public ConfigException()
        {
        }

        /// <summary>
        /// 建構元
        /// </summary>
        /// <param name="message">錯誤訊息</param>
        public ConfigException(string message) : base(message)
        {
        }

        /// <summary>
        /// 建構元
        /// </summary>
        /// <param name="message">錯誤訊息</param>
        /// <param name="innerException">例外物件</param>
        public ConfigException(string message, Exception innerException) : base(message, innerException)
        {
        }

        /// <summary>
        /// 建構元
        /// </summary>
        /// <param name="info">序列化訊息物件</param>
        /// <param name="context">串流上下文</param>
        protected ConfigException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}