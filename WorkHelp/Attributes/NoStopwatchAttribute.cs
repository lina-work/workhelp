﻿using System;

namespace WorkHelp.Wpf.Attributes
{
    /// <summary>
    /// 標示為不處理計時
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class NoStopwatchAttribute : Attribute { }
}