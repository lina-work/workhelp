﻿using System;
using System.IO;

namespace WorkHelp.HtmlVue
{
    /// <summary>
    /// 分析模型
    /// </summary>
    public partial class VueAnalysis
    {
        /// <summary>
        /// 預替換的參數資料
        /// </summary>
        /// <param name="name">參數名稱</param>
        /// <param name="value">參數內容</param>
        public void Parameter(string name, string value)
        {

        }

        /// <summary>
        /// 執行替換
        /// </summary>
        public void Execute()
        {

        }

        /// <summary>
        /// 生成
        /// </summary>
        public void RenderFile(string fileName = "")
        {
            if (string.IsNullOrWhiteSpace(fileName))
            {

                CreateOutputFile(GetAutoFileName());
            }
            else
            {
                CreateOutputFile(fileName);
            }
        }

        /// <summary>
        /// 建立輸出檔案
        /// </summary>
        /// <param name="file">完整檔案名稱</param>
        private void CreateOutputFile(string path)
        {
            string contents = string.Empty;

            File.WriteAllText(path, contents, System.Text.Encoding.UTF8);
        }

        /// <summary>
        /// 取得自動生成的檔案名稱
        /// </summary>
        /// <returns></returns>
        private string GetAutoFileName()
        {
            var new_time = DateTime.Now.ToString("yyyyMMdd_hhmmss");
            var new_path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "html\\output");
            var new_file = Path.Combine(new_path, $"vue_{new_time}.html");
            if (!Directory.Exists(new_path))
            {
                Directory.CreateDirectory(new_path);
            }
            if (File.Exists(new_file))
            {
                File.Delete(new_file);
            }
            return new_file;
        }
    }
}
