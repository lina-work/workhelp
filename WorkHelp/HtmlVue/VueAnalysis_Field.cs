﻿using System.Collections.Generic;

namespace WorkHelp.HtmlVue
{
    /// <summary>
    /// 分析模型
    /// </summary>
    public partial class VueAnalysis
    {
        /// <summary>
        /// 原始樣板
        /// </summary>
        public string OriginTemplate { get; set; }

        /// <summary>
        /// 是否有 app tag
        /// </summary>
        public bool HasAppTag { get; set; }

        /// <summary>
        /// 是否有 ctrl tag
        /// </summary>
        public bool HasCtrlTag { get; set; }

        /// <summary>
        /// 控制器內容
        /// </summary>
        public string CtrlContent { get; set; }

        /// <summary>
        /// 控制器別名
        /// </summary>
        public string CtrlName { get; set; }

        /// <summary>
        /// 參數列表
        /// </summary>
        public List<VueParameter> Parameters { get; set; }
    }
}
