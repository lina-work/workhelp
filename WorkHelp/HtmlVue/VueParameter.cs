﻿namespace WorkHelp.HtmlVue
{
    /// <summary>
    /// 參數
    /// </summary>
    public class VueParameter
    {
        /// <summary>
        /// 原始參數名
        /// </summary>
        public string OriginName { get; set; }

        /// <summary>
        /// 目標參數名
        /// </summary>
        public string TargetName { get; set; }
    }
}
