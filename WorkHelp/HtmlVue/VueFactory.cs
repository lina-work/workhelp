﻿using System;
using System.IO;
using System.Linq;

namespace WorkHelp.HtmlVue
{
    /// <summary>
    /// 工廠
    /// </summary>
    public class NgFactory
    {
        /// <summary>
        /// 分析結果
        /// </summary>
        /// <param name="file">完整檔案路徑</param>
        /// <returns></returns>
        public static VueAnalysis Analysis(string file)
        {
            string template = File.ReadAllText(file, System.Text.Encoding.UTF8);

            VueAnalysis result = new VueAnalysis();
            result.OriginTemplate = string.Empty;
            result.HasAppTag = template.Contains("ng-app");
            result.HasCtrlTag = template.Contains("ng-app");

            AnalysisCtrl(result, template);

            return result;
        }

        /// <summary>
        /// 定位控制器位置
        /// </summary>
        private static void AnalysisCtrl(VueAnalysis analysis, string template)
        {
            string contents = CutTag(template, "ng-controller");
            string name = GetTail(contents, " as ");

            analysis.CtrlContent = contents;
            analysis.CtrlName = name;
        }

        /// <summary>
        /// 定位字串位置
        /// </summary>
        public static string CutTag(string template, string tag)
        {
            int ctrlIndex = template.IndexOf("ng-controller");
            int startIndex = template.IndexOf("\"", ctrlIndex + 1);
            int endIndex = template.IndexOf("\"", startIndex + 1);

            return template.Substring(startIndex + 1, endIndex - startIndex - 1);
        }

        /// <summary>
        /// 取尾綴
        /// </summary>
        public static string GetTail(string content, string split)
        {
            string[] args = content.Split(new string[] { split }, StringSplitOptions.RemoveEmptyEntries);
            return args.Last().Trim();
        }
    }
}
