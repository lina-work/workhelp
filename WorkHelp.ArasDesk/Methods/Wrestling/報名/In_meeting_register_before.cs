﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;
using System.Web.UI;
using Spire.Doc;

namespace WorkHelp.ArasDesk.Methods.Wresling.Register
{
    public class in_meeting_register_before : Item
    {
        public in_meeting_register_before(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 確認報名
                日誌: 
                    - 2024-06-14: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "in_meeting_register_before";

            var itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, itmR.dom.InnerXml);

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                strIdentityId = inn.getUserAliases(),
                meeting_id = itmR.getProperty("meeting_id", ""),
                in_creator_sno = itmR.getProperty("in_creator_sno", ""),
                in_current_org = itmR.getProperty("in_current_org", ""),
                muid = itmR.getProperty("muid", ""),
                scene = itmR.getProperty("scene", ""),
            };

            switch (cfg.scene)
            {
                case "org_view":
                    OrgView(cfg, itmR);
                    break;

                case "query_payment":
                    QueryPayment(cfg, itmR);
                    break;

                case "create_payment":
                    CreatePayment(cfg, itmR);
                    break;

                case "edit_photo_pay":
                    EditMeetingUserPhotoPay(cfg, itmR);
                    break;

                case "show_pay_photo_modal":
                    ShowPayPhotoModal(cfg, itmR);
                    break;

                case "create_survey_options":
                    CreateSurveyOptions(cfg, itmR);
                    break;

                case "create_org_map":
                    CreateOrgMap(cfg, itmR);
                    break;

                case "check_register":
                    CheckRegister(cfg, itmR);
                    break;

                case "creator_org_players":
                    GetCreatorOrgPlayers(cfg, itmR);
                    break;

                case "save_insurance":
                    SaveInSurance(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void OrgView(TConfig cfg, Item itmReturn)
        {
            cfg.itmMeeting = cfg.inn.applySQL("SELECT in_title FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            if (cfg.itmMeeting.isError() || cfg.itmMeeting.getResult() == "")
            {
                return;
                //throw new Exception("賽事資料錯誤");
            }

            itmReturn.setProperty("in_title", cfg.itmMeeting.getProperty("in_title", ""));

            var items = GetMeetingUsersByAdmin(cfg);
            var orgs = MapOrgs(cfg, items);

            var head = new StringBuilder();
            head.Append("<th class='text-center' style='width: 60px'>&nbsp;</th>");
            head.Append("<th class='text-center'>編號</th>");
            head.Append("<th class='text-center'>分類</th>");
            head.Append("<th class='text-center'>單位</th>");
            head.Append("<th class='text-center'>簡稱</th>");
            head.Append("<th class='text-center'>隊職員<br>數</th>");
            head.Append("<th class='text-center'>項目<br>數</th>");
            head.Append("<th class='text-center'>選手<br>數</th>");
            head.Append("<th class='text-center'>保險代表</th>");
            head.Append("<th class='text-center'>檢查</th>");

            var body = new StringBuilder();
            for (var i = 0; i < orgs.Count; i++)
            {
                var org = orgs[i];
                var trId = "org-row-" + org.in_stuff_b1;
                var btn = "<span class='event-btn' onclick='ToggleSubRowClick(this)'  data-state='hide' data-id='" + trId + "'> &nbsp; <i class='fa fa-plus'></i> &nbsp; </span>";
                //var btn = "<span class='event-btn' onclick='ToggleSubRowClick(this)'  data-state='show' data-id='" + trId + "'> &nbsp; <i class='fa fa-minus'></i> &nbsp; </span>";

                var btn2 = " <button class='btn btn-default'"
                    + " onclick='CallCreatorModal(this)'"
                    + " data-org='" + org.in_short_org + "'"
                    + " data-name='" + org.in_creator + "'"
                    + " data-tel='" + org.in_creator_tel + "'"
                    + " data-email='" + org.in_creator_email + "'"
                    + " > <i class='fa fa-phone'></i> </button>";

                var insurer = org.insurances.Count > 0 ? org.insurances[0].in_name : "";
                var isPlayer = "<span style='background-color:red; color: white'>Ｘ</span>";
                if (org.players.Count > 0 && insurer != "")
                {
                    var exists = org.players.Find(x => x.in_name == insurer);
                    if (exists != null) isPlayer = "Ｏ";
                }
                var orgShort = org.in_current_org == org.in_short_org ? "同全稱" : org.in_short_org;
                body.Append("<tr class='org-table-row'>");
                body.Append("<td class='text-center' style='vertical-align: middle'>" + btn + "</td>");
                body.Append("<td class='text-center'>【" + (i + 1) + "】</td>");
                body.Append("<td class='text-left'>" + org.org_type + " </td>");
                body.Append("<td class='text-left'>" + org.in_current_org + btn2 + " </td>");
                body.Append("<td class='text-left'>" + orgShort + " </td>");
                body.Append("<td class='text-center'>" + org.staffs.Count + " </td>");
                body.Append("<td class='text-center'>" + org.items.Count + " </td>");
                body.Append("<td class='text-center'>" + org.players.Count + " </td>");
                body.Append("<td class='text-center'>" + org.insurances.Count + " </td>");
                body.Append("<td class='text-center'>" + isPlayer + " </td>");
                body.Append("</tr>");

                body.Append("<tr id='" + trId + "' style='display: none'>");
                //body.Append("<tr id='" + trId + "'>");

                body.Append("<td class='text-center' colspan='10'>" + AppendSubTable(cfg, org) + "</td>");
                body.Append("</tr>");
            }

            var table = new StringBuilder();
            table.Append("<table id='org-table' class='table table-bordered table-rwd rwd'>");
            table.Append("<thead>");
            table.Append("<tr>");
            table.Append(head);
            table.Append("</tr>");
            table.Append("</thead>");
            table.Append("<tbody>");
            table.Append(body);
            table.Append("</tbody>");
            table.Append("</table>");

            itmReturn.setProperty("inn_table", table.ToString());
        }

        private string AppendSubTable(TConfig cfg, TOrg org)
        {
            var body = new StringBuilder();
            AppendSubTable(cfg, org, org.leaders, body, AppendStaffTableRow);
            AppendSubTable(cfg, org, org.managers, body, AppendStaffTableRow);
            AppendSubTable(cfg, org, org.coaches_m, body, AppendStaffTableRow);
            AppendSubTable(cfg, org, org.coaches_w, body, AppendStaffTableRow);
            AppendSubTable(cfg, org, org.coaches_n, body, AppendStaffTableRow);
            AppendSubTable(cfg, org, org.insurances, body, AppendInsuranceTableRow);

            AppendSubTable2(cfg, org, org.players, body);

            var table = new StringBuilder();
            table.Append("<table id='org-table-" + org.in_stuff_b1 + "' class='table table-bordered table-rwd rwd'>");
            table.Append("<tbody>");
            table.Append(body);
            table.Append("</tbody>");
            table.Append("</table>");
            return table.ToString();
        }

        private void AppendSubTable2(TConfig cfg, TOrg org, List<TPerson> list, StringBuilder body)
        {
            for (var i = 0; i < list.Count; i++)
            {
                var x = list[i];
                var item = x.value;
                for (var j = 0; j < x.items.Count; j++)
                {
                    body.Append("<tr>");
                    if (j == 0)
                    {
                        AppendPlayerTableRowA(cfg, body, x.items[j], j + 1);
                    }
                    else
                    {
                        AppendPlayerTableRowB(cfg, body, x.items[j], j + 1);
                    }
                    body.Append("</tr>");
                }
            }
        }

        private void AppendSubTable(TConfig cfg, TOrg org, List<TPerson> list, StringBuilder body, Action<TConfig, StringBuilder, Item> AppendTableTr)
        {
            for (var i = 0; i < list.Count; i++)
            {
                var x = list[i];
                var item = x.value;
                body.Append("<tr>");
                AppendTableTr(cfg, body, item);
                body.Append("</tr>");
            }
        }

        private void AppendStaffTableRow(TConfig cfg, StringBuilder body, Item item)
        {
            body.Append("<td class='text-left'>&nbsp;</td>");
            body.Append("<td class='text-left'>" + item.getProperty("in_l2") + "</td>");
            body.Append("<td class='text-left'><label>" + item.getProperty("in_name") + "</label></td>");
            body.Append("<td class='text-left'>&nbsp;</td>");
            body.Append("<td class='text-left'>&nbsp;</td>");
            body.Append("<td class='text-left'>&nbsp;</td>");
            body.Append("<td class='text-left'>&nbsp;</td>");
            body.Append("<td class='text-left'>&nbsp;</td>");
        }

        private void AppendInsuranceTableRow(TConfig cfg, StringBuilder body, Item item)
        {
            body.Append("<td class='text-left'>&nbsp;</td>");
            body.Append("<td class='text-left'>" + item.getProperty("in_l2") + "</td>");
            body.Append("<td class='text-left'><label>" + item.getProperty("in_name") + "</label></td>");
            body.Append("<td class='text-left'> / " + item.getProperty("in_emrg_contact1") + "</td>");
            body.Append("<td class='text-left'> / " + item.getProperty("in_emrg_relation1") + "</td>");
            body.Append("<td class='text-left'>&nbsp;</td>");
            body.Append("<td class='text-left'>&nbsp;</td>");
            body.Append("<td class='text-left'>&nbsp;</td>");
        }

        private void AppendPlayerTableRowA(TConfig cfg, StringBuilder body, Item item, int index)
        {
            body.Append("<td class='text-left'>"+ index + "</td>");
            body.Append("<td class='text-left'>" + "選手" + "</td>");
            body.Append("<td class='text-left'><label>" + item.getProperty("in_name") + "</label></td>");
            body.Append("<td class='text-left'>" + item.getProperty("in_l1") + "</td>");
            body.Append("<td class='text-left'>" + item.getProperty("in_l2_label") + "</td>");
            body.Append("<td class='text-left'>" + item.getProperty("in_l3") + "</td>");
            body.Append("<td class='text-left'>" + item.getProperty("in_paynumber") + "</td>");
            body.Append("<td class='text-left'>" + item.getProperty("pay_bool") + "</td>");
        }

        private void AppendPlayerTableRowB(TConfig cfg, StringBuilder body, Item item, int index)
        {
            body.Append("<td class='text-left'>&nbsp;</td>");
            body.Append("<td class='text-left'>&nbsp;</td>");
            body.Append("<td class='text-left'>&nbsp;</td>");
            body.Append("<td class='text-left'>" + item.getProperty("in_l1") + "</td>");
            body.Append("<td class='text-left'>" + item.getProperty("in_l2_label") + "</td>");
            body.Append("<td class='text-left'>" + item.getProperty("in_l3") + "</td>");
            body.Append("<td class='text-left'>" + item.getProperty("in_paynumber") + "</td>");
            body.Append("<td class='text-left'>" + item.getProperty("pay_bool") + "</td>");
        }

        private void QueryPayment(TConfig cfg, Item itmReturn)
        {
            var sql = "SELECT TOP 1 id, in_name, in_sno, in_group FROM IN_RESUME WITH(NOLOCK) WHERE in_user_id = '" + cfg.strUserId + "'";
            var itmResume = cfg.inn.applySQL(sql);

            itmReturn.setProperty("pay_count", "0");

            AppendPayList(cfg, itmReturn);
        }

        private void CreatePayment(TConfig cfg, Item itmReturn)
        {
            var sql = "SELECT TOP 1 id, in_name, in_sno, in_group FROM IN_RESUME WITH(NOLOCK) WHERE in_user_id = '" + cfg.strUserId + "'";
            var itmResume = cfg.inn.applySQL(sql);

            itmReturn.setProperty("pay_count", "0");

            Item itmMeetingPay = cfg.inn.newItem();
            itmMeetingPay.setType("In_Meeting_Pay");
            itmMeetingPay.setProperty("meeting_id", cfg.meeting_id);
            itmMeetingPay.setProperty("in_creator_sno", cfg.in_creator_sno);
            itmMeetingPay.setProperty("in_group", itmResume.getProperty("in_group", ""));
            itmMeetingPay.apply("In_Close_GymReg_And_Pay");

            AppendPayList(cfg, itmReturn);
        }

        private void EditMeetingUserPhotoPay(TConfig cfg, Item itmReturn)
        {
            var sql = "";

            sql = "SELECT TOP 1 id, in_name, in_sno FROM IN_RESUME WITH(NOLOCK) WHERE in_user_id = '" + cfg.strUserId + "'";
            var itmResume = cfg.inn.applySQL(sql);

            sql = "SELECT TOP 1 in_photo_pay FROM IN_MEETING_USER WITH(NOLOCK) WHERE id = '" + cfg.muid + "'";
            var itmMUser = cfg.inn.applySQL(sql);

            var in_creator_sno = itmResume.getProperty("in_sno", "");
            var in_photo_pay = itmMUser.getProperty("in_photo_pay", "");

            sql = "UPDATE IN_MEETING_USER SET in_photo_pay = '" + in_photo_pay + "' WHERE source_id = '" + cfg.meeting_id + "' AND in_creator_sno = '" + in_creator_sno + "'";
            cfg.inn.applySQL(sql);
        }


        private void ShowPayPhotoModal(TConfig cfg, Item itmReturn)
        {
            var sql = "SELECT TOP 1 id, in_name, in_sno FROM IN_RESUME WITH(NOLOCK) WHERE in_user_id = '" + cfg.strUserId + "'";
            var itmResume = cfg.inn.applySQL(sql);

            cfg.in_creator_sno = itmResume.getProperty("in_sno", "");
            cfg.in_current_org = "";

            var items = GetMeetingUsers(cfg);
            if (items.getResult() == "") return;

            var itmAmount = GetExpenseItem(cfg);
            if (itmAmount.getResult() == "") itmAmount = cfg.inn.newItem();

            var total_amount = itmAmount.getProperty("total_amount", "0");

            var itmMUser = items.getItemByIndex(0);

            itmReturn.setProperty("id", itmMUser.getProperty("id", ""));
            itmReturn.setProperty("in_creator", itmMUser.getProperty("in_creator", ""));
            itmReturn.setProperty("in_pay_amount_exp", total_amount);
            itmReturn.setProperty("in_pay_photo", GetMUserPhotoPay(cfg));

        }

        private string GetMUserPhotoPay(TConfig cfg)
        {
            var sql = @"
            	SELECT TOP 1
            		t1.in_photo_pay
            	FROM
            		IN_MEETING_USER t1 WITH(NOLOCK)
            	WHERE
            		t1.source_id = '{#meeting_id}'
            		AND t1.in_creator_sno = '{#in_creator_sno}'
            		AND ISNULL(t1.in_photo_pay, '') <> ''
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_creator_sno}", cfg.in_creator_sno);

            var item = cfg.inn.applySQL(sql);
            if (item.getResult() == "") return "";
            return item.getProperty("in_photo_pay", "");
        }

        private Item GetExpenseItem(TConfig cfg)
        {
            var sql = @"
                SELECT
                	SUM(in_expense) AS 'total_amount'
                FROM
                (
                	SELECT DISTINCT
                		t1.in_current_org
                		, t1.in_l1
                		, t1.in_l2
                		, t1.in_l3
                		, t1.in_index
                		, t1.in_expense
                	FROM
                		IN_MEETING_USER t1 WITH(NOLOCK)
                	WHERE
                		t1.source_id = '{#meeting_id}'
                		AND t1.in_creator_sno = '{#in_creator_sno}'
                		AND ISNULL(t1.in_expense, '') NOT IN ('', '0')
                ) t101
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_creator_sno}", cfg.in_creator_sno);

            return cfg.inn.applySQL(sql);
        }

        private void CreateSurveyOptions(TConfig cfg, Item itmReturn)
        {
            var mx = 166;
            for (var i = 1; i <= mx; i++)
            {
                var itmNew = cfg.inn.newItem("In_Survey_Option", "add");
                itmNew.setProperty("sort_order", i.ToString());
                itmNew.setProperty("source_id", "D645D47F2609439098668CD68A084C78");
                itmNew.setProperty("in_label", i.ToString());
                itmNew.setProperty("in_value", i.ToString());
                itmNew.apply();
            }
        }

        private void CreateOrgMap(TConfig cfg, Item itmReturn)
        {
            var mx = 166;
            for (var i = 1; i <= mx; i++)
            {
                var itmNew = cfg.inn.newItem("In_Org_Map", "add");
                itmNew.setProperty("in_code", i.ToString());
                itmNew.setProperty("in_current_org", i.ToString());
                itmNew.apply();
            }
        }

        private void SaveInSurance(TConfig cfg, Item itmReturn)
        {
            cfg.in_l1 = "保險代表人";

            var itmResume = cfg.inn.applySQL("SELECT TOP 1 * FROM IN_RESUME WITH(NOLOCK) WHERE login_name = '" + cfg.in_creator_sno + "'");
            if (itmResume.isError() || itmResume.getResult() == "")
            {
                throw new Exception("查無講師履歷: " + cfg.in_creator_sno);
            }

            var itmMUser = cfg.inn.applySQL("SELECT TOP 1 * FROM IN_MEETING_USER WITH(NOLOCK) WHERE id = '" + cfg.muid + "'");
            if (itmMUser.isError() || itmMUser.getResult() == "")
            {
                throw new Exception("查無選手資料歷: " + cfg.muid);
            }

            var sql_del = @"
	            DELETE FROM 
	                IN_MEETING_USER
                WHERE
	                source_id = '{#meeting_id}'
	                AND in_creator_sno = '{#in_creator_sno}'
	                AND in_current_org = '{#in_current_org}'
	                AND in_l1 = N'保險代表人'
            ";

            sql_del = sql_del.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_creator_sno}", cfg.in_creator_sno)
                .Replace("{#in_current_org}", itmMUser.getProperty("in_current_org", ""));

            cfg.inn.applySQL(sql_del);


            var muser_sno = itmMUser.getProperty("in_sno", "");

            var itmSvys = GetMeetingSurveys(cfg);
            var map = SurveyMap(cfg, itmSvys);

            var itmNew = cfg.inn.newItem("In_Meeting_User");
            itmNew.setProperty("isUserId", cfg.strUserId);
            itmNew.setProperty("isIndId", cfg.strIdentityId);
            itmNew.setProperty("login_resume_id", itmResume.getProperty("id", ""));
            itmNew.setProperty("login_resume_current_org", itmResume.getProperty("in_current_org", ""));
            itmNew.setProperty("login_resume_group", itmResume.getProperty("in_group", ""));
            itmNew.setProperty("register_section_name", cfg.in_l1);
            itmNew.setProperty("surveytype", "1");
            itmNew.setProperty("meeting_id", cfg.meeting_id);
            itmNew.setProperty("method", "in_meeting_register_before");
            itmNew.setProperty("OnlyInsMUser", "0");
            itmNew.setProperty("muid", "");
            itmNew.setProperty("email", muser_sno + "-" + cfg.in_l1);
            itmNew.setProperty("in_index", "");
            itmNew.setProperty("in_l1", cfg.in_l1);
            itmNew.setProperty("in_l2", cfg.in_l1);
            itmNew.setProperty("in_l3", cfg.in_l1);
            itmNew.setProperty("client_user_index", "1");
            itmNew.setProperty("in_creator", itmResume.getProperty("in_name", ""));
            itmNew.setProperty("current_user_name_sno", cfg.in_creator_sno);
            itmNew.setProperty("in_locked", "1");
            itmNew.setProperty("in_locked_id", muser_sno);
            itmNew.setProperty("parameter", getParameters(cfg, map, itmMUser, itmResume, itmReturn));
            itmNew.apply("In_meeting_register");
        }

        private string getParameters(TConfig cfg, Dictionary<string, Item> map, Item itmMUser, Item itmResume, Item itmReturn)
        {
            var in_emrg_contact1 = itmReturn.getProperty("in_emrg_contact1", "");
            var in_emrg_relation1 = itmReturn.getProperty("in_emrg_relation1", "");

            var in_birth = itmMUser.getProperty("in_birth", "");
            in_birth = GetDtm(in_birth).AddHours(8).ToString("yyyy-MM-dd");

            var list = new List<string>();
            AppendParameter(cfg, map, list, "in_name", itmMUser.getProperty("in_name", ""));
            AppendParameter(cfg, map, list, "in_gender", itmMUser.getProperty("in_gender", ""));
            AppendParameter(cfg, map, list, "in_sno", itmMUser.getProperty("in_sno", ""));
            AppendParameter(cfg, map, list, "in_birth", in_birth);
            AppendParameter(cfg, map, list, "in_current_org", cfg.in_current_org);
            AppendParameter(cfg, map, list, "in_l1", cfg.in_l1);
            AppendParameter(cfg, map, list, "in_l2", cfg.in_l1);
            AppendParameter(cfg, map, list, "in_l3", cfg.in_l1);
            AppendParameter(cfg, map, list, "in_emrg_contact1", in_emrg_contact1);
            AppendParameter(cfg, map, list, "in_emrg_relation1", in_emrg_relation1);
            return string.Join("&", list);
        }

        private void AppendParameter(TConfig cfg, Dictionary<string, Item> map, List<string> list, string in_property, string value)
        {
            if (!map.ContainsKey(in_property)) return;
            var item = map[in_property];
            var key = item.getProperty("id", "");
            list.Add(key + "=" + value);
        }

        private void GetCreatorOrgPlayers(TConfig cfg, Item itmReturn)
        {
            var items = GetMeetingUsers(cfg);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                item.setType("inn_muser");
                itmReturn.addRelationship(item);
            }
            var sql = @"
                SELECT TOP 1
	                id
	                , in_name
	                , in_emrg_contact1
	                , in_emrg_relation1
	                , in_emrg_tel1
	                , in_creator_sno
	                , in_paynumber
                FROM
	                IN_MEETING_USER WITH(NOLOCK)
                WHERE
	                source_id = '{#meeting_id}'
	                AND in_creator_sno = '{#in_creator_sno}'
	                AND in_current_org = '{#in_current_org}'
	                AND in_l1 = N'保險代表人'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_creator_sno}", cfg.in_creator_sno)
                .Replace("{#in_current_org}", cfg.in_current_org);

            var itmSelect = cfg.inn.applySQL(sql);
            if (itmSelect.getResult() == "")
            {
                itmSelect = cfg.inn.newItem();
            }

            itmReturn.setProperty("insurance_muid", itmSelect.getProperty("id", ""));
            itmReturn.setProperty("insurance_name", itmSelect.getProperty("in_name", ""));
            itmReturn.setProperty("insurance_contact", itmSelect.getProperty("in_emrg_contact1", ""));
            itmReturn.setProperty("insurance_relation", itmSelect.getProperty("in_emrg_relation1", ""));
            itmReturn.setProperty("insurance_paynumber", itmSelect.getProperty("in_paynumber", ""));
        }

        private void CheckRegister(TConfig cfg, Item itmReturn)
        {
            itmReturn.setProperty("inn_errors", "");
            itmReturn.setProperty("inn_need_create_pay", "1");

            var items = GetMeetingUsers(cfg);
            var orgs = MapOrgs(cfg, items);
            var response = CheckOrgRegister(cfg, orgs);

            if (response.all_has_pay_number)
            {
                itmReturn.setProperty("inn_need_create_pay", "0");
            }

            if (response.errors.Count > 0)
            {
                itmReturn.setProperty("inn_errors", string.Join("<br / >", response.errors));
            }
        }

        private void AppendPayList(TConfig cfg, Item itmReturn)
        {
            var sql = @"
                SELECT DISTINCT
	                t1.in_stuff_b1
	                , t2.in_current_org
	                , t2.item_number
	                , t2.in_pay_amount_exp
	                , t2.pay_bool
	                , t2.in_pay_photo
	                , t2.in_real_stuff
	                , t2.in_real_player
	                , t2.in_real_items
                FROM
	                IN_MEETING_USER t1 WITH(NOLOCK)
                LEFT OUTER JOIN
	                IN_MEETING_PAY t2 WITH(NOLOCK)
	                ON t2.item_number = t1.in_paynumber
                WHERE
	                t1.source_id = '{#meeting_id}'
	                AND t1.in_creator_sno = '{#in_creator_sno}'
                ORDER BY
	                t1.in_stuff_b1
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_creator_sno}", cfg.in_creator_sno);

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                item.setType("inn_pay");
                itmReturn.addRelationship(item);
            }

            if (count > 0)
            {
                itmReturn.setProperty("pay_count", "1");
            }
        }

        private TResponse CheckOrgRegister(TConfig cfg, List<TOrg> orgs)
        {
            var result = new TResponse { all_has_pay_number = true };

            for (var i = 0; i < orgs.Count; i++)
            {
                var org = orgs[i];
                if (org.payment_status_n.Count > 0)
                {
                    result.all_has_pay_number = false;
                }

                if (org.leaders.Count > 1)
                {
                    org.errors.Add(" - [領隊]限定1人");
                }
                if (org.managers.Count > 1)
                {
                    org.errors.Add(" - [管理]限定1人");
                }
                if (org.coaches_m.Count > 2)
                {
                    org.errors.Add(" - [男子隊教練]限定2人");
                }
                if (org.coaches_w.Count > 2)
                {
                    org.errors.Add(" - [女子隊教練]限定2人");
                }
                if (org.coaches_n.Count > 2)
                {
                    org.errors.Add(" - [教練]限定2人");
                }
                if (org.insurances.Count > 1)
                {
                    org.errors.Add(" - [保險代表人]限定1人");
                }
                if (org.insurances.Count == 0)
                {
                    var button = " <button class='btn btn-sm btn-default' data-org='" + org.in_current_org + "' onclick='select_insurance_player(this)'>填報</button>";
                    org.errors.Add(" - [保險代表人]尚未填報" + button);
                }
            }

            var errors = new List<string>();
            for (var i = 0; i < orgs.Count; i++)
            {
                var org = orgs[i];
                if (org.errors.Count == 0) continue;
                errors.Add("<div style='text-align: left'><span style='font-weight: bold; color: red'>" + org.in_current_org + "</span><br / >" + string.Join("<br / >", org.errors) + "</div>");
            }
            result.errors = errors;

            return result;
        }

        private List<TOrg> MapOrgs(TConfig cfg, Item items)
        {
            var orgs = new List<TOrg>();
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var org = FindAndAdd(cfg, orgs, item);
                AnalysisMUser(cfg, org, item);
                AnalysisMPay(cfg, org, item);
            }
            return orgs;
        }

        private void AnalysisMPay(TConfig cfg, TOrg org, Item item)
        {
            var id = item.getProperty("id", "");
            var in_paynumber = item.getProperty("in_paynumber", "");
            org.payment_status_a.Add(id);
            if (in_paynumber == "")
            {
                org.payment_status_n.Add(id);
            }
            else
            {
                org.payment_status_y.Add(id);
            }
        }

        private void AnalysisMUser(TConfig cfg, TOrg org, Item item)
        {
            var in_l1 = item.getProperty("in_l1", "");
            switch (in_l1)
            {
                case "隊職員":
                    AddPerson(cfg, org, org.staffs, item);
                    AnalysisStaff(cfg, org, item);
                    break;
                case "保險代表人":
                    AddPerson(cfg, org, org.insurances, item);
                    break;
                default:
                    AddPerson(cfg, org, org.items, item);
                    AddPlayer(cfg, org, org.players, item);
                    break;
            }
        }

        private void AnalysisStaff(TConfig cfg, TOrg org, Item item)
        {
            var in_l2 = item.getProperty("in_l2", "");
            var in_gender = item.getProperty("in_gender", "");
            switch (in_l2)
            {
                case "領隊":
                    AddPerson(cfg, org, org.leaders, item);
                    break;
                case "管理":
                    AddPerson(cfg, org, org.managers, item);
                    break;
                case "男教練":
                case "男子隊教練":
                    AddPerson(cfg, org, org.coaches_m, item);
                    break;
                case "女教練":
                case "女子隊教練":
                    AddPerson(cfg, org, org.coaches_w, item);
                    break;
                case "教練":
                    AddPerson(cfg, org, org.coaches_n, item);
                    break;
            }
        }

        private void AddPerson(TConfig cfg, TOrg org, List<TPerson> list, Item item)
        {
            var in_name = item.getProperty("in_name", "");
            list.Add(new TPerson
            {
                in_name = in_name,
                value = item
            });
        }

        private void AddPlayer(TConfig cfg, TOrg org, List<TPerson> list, Item item)
        {
            var in_sno = item.getProperty("in_sno", "").ToUpper();
            var in_name = item.getProperty("in_name", "");
            var player = list.Find(x => x.in_sno == in_sno);
            if (player == null)
            {
                player = new TPerson
                {
                    in_sno = in_sno,
                    in_name = in_name,
                    items = new List<Item>(),
                };
                list.Add(player);
            }
            player.items.Add(item);
        }

        private TOrg FindAndAdd(TConfig cfg, List<TOrg> orgs, Item item)
        {
            var in_current_org = item.getProperty("in_current_org", "");
            var org = orgs.Find(x => x.in_current_org == in_current_org);
            if (org == null)
            {
                org = new TOrg
                {
                    in_current_org = in_current_org,
                    in_stuff_b1 = item.getProperty("in_stuff_b1", ""),
                    in_short_org = item.getProperty("in_short_org", ""),
                    org_type = item.getProperty("org_type", ""),
                    in_creator = item.getProperty("in_creator", ""),
                    in_creator_sno = item.getProperty("in_creator_sno", ""),
                    in_creator_tel = item.getProperty("in_creator_tel", ""),
                    in_creator_email = item.getProperty("in_creator_email", ""),
                    managers = new List<TPerson>(),
                    leaders = new List<TPerson>(),
                    coaches_w = new List<TPerson>(),
                    coaches_m = new List<TPerson>(),
                    coaches_n = new List<TPerson>(),
                    insurances = new List<TPerson>(),
                    staffs = new List<TPerson>(),
                    players = new List<TPerson>(),
                    items = new List<TPerson>(),
                    payment_status_a = new List<string>(),
                    payment_status_y = new List<string>(),
                    payment_status_n = new List<string>(),
                    errors = new List<string>(),
                };
                orgs.Add(org);
            }
            return org;
        }

        private Dictionary<string, Item> SurveyMap(TConfig cfg, Item items)
        {
            var map = new Dictionary<string, Item>();
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var key = item.getProperty("in_property", "");
                if (map.ContainsKey(key)) continue;
                map.Add(key, item);
            }
            return map;
        }

        private Item GetMeetingSurveys(TConfig cfg)
        {
            var sql = @"
                SELECT
	                t2.id
	                , t2.in_questions
	                , t2.in_question_type
	                , t2.in_property
                FROM
	                IN_MEETING_SURVEYS t1 WITH(NOLOCK)
                INNER JOIN
	                IN_SURVEY t2 WITH(NOLOCK)
	                ON t1.related_id = t2.id
                WHERE
	                t1.source_id = '{#meeting_id}'
                ORDER BY
	                t1.sort_order
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            return cfg.inn.applySQL(sql);
        }

        private Item GetMeetingUsersByAdmin(TConfig cfg)
        {
            var sql = @"
                SELECT 
	                t1.id
	                , t1.in_creator
	                , t1.in_creator_sno
	                , t1.in_name
	                , t1.in_sno
	                , t1.in_l1
	                , t1.in_l2
	                , t1.in_l3
	                , t1.in_paynumber
	                , t1.in_paytime
	                , t1.in_stuff_b1
	                , t1.in_current_org
                	, t1.in_emrg_contact1
                	, t1.in_emrg_relation1
                	, t2.in_type      AS 'org_type'
                	, t2.in_short_org
					, t3.in_tel       AS 'in_creator_tel'
					, t3.in_email     AS 'in_creator_email'
					, t4.in_pay_amount_exp
					, t4.pay_bool
					, t12.in_label		AS 'in_l2_label'
                FROM 
	                IN_MEETING_USER t1 WITH(NOLOCK)
                LEFT OUTER JOIN
	                IN_ORG_MAP t2 WITH(NOLOCK)
                    ON t2.in_stuff_b1 = t1.in_stuff_b1
                LEFT OUTER JOIN
	                IN_RESUME t3 WITH(NOLOCK)
                    ON t3.in_sno = t1.in_creator_sno
                LEFT OUTER JOIN
	                IN_MEETING_PAY t4 WITH(NOLOCK)
                    ON t4.in_meeting = t1.source_id
                    AND t4.item_number = t1.in_paynumber
                LEFT OUTER JOIN
                    VU_MEETING_SVY_L1 t11
                    ON t11.source_id = t1.source_id
                    AND t11.in_value = t1.in_l1
                LEFT OUTER JOIN
                    VU_MEETING_SVY_L2 t12
                    ON t12.source_id = t1.source_id
                    AND t12.in_filter = t1.in_l1
                    AND t12.in_value = t1.in_l2
                LEFT OUTER JOIN
                    VU_MEETING_SVY_L3 t13
                    ON t13.source_id = t1.source_id
                    AND t13.in_grand_filter = t1.in_l1
                    AND t13.in_filter = t1.in_l2
                    AND t13.in_value = t1.in_l3
                WHERE 
	                t1.source_id = '{#meeting_id}'
                ORDER BY
	                t1.in_stuff_b1
	                , t11.sort_order
	                , t12.sort_order
	                , t13.sort_order
	                , t1.in_name
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        private Item GetMeetingUsers(TConfig cfg)
        {
            var orgCond = cfg.in_current_org == ""
                ? ""
                : "AND t1.in_current_org = N'" + cfg.in_current_org + "' AND t1.in_l1 NOT IN ('隊職員', '保險代表人')";

            var sql = @"
                SELECT 
	                t1.id
	                , t1.in_creator
	                , t1.in_creator_sno
	                , t1.in_name
	                , t1.in_sno
	                , t1.in_l1
	                , t1.in_l2
	                , t1.in_l3
	                , t1.in_paynumber
	                , t1.in_paytime
	                , t1.in_current_org
                FROM 
	                IN_MEETING_USER t1 WITH(NOLOCK)
                LEFT OUTER JOIN
	                IN_ORG_MAP t2 WITH(NOLOCK)
                    ON t2.in_stuff_b1 = t1.in_stuff_b1
                LEFT OUTER JOIN
                    VU_MEETING_SVY_L3 t5
                    ON t5.source_id = t1.source_id
                    AND t5.in_grand_filter = t1.in_l1
                    AND t5.in_filter = t1.in_l2
                    AND t5.in_value = t1.in_l3
                WHERE 
	                t1.source_id = '{#meeting_id}'
	                AND t1.in_creator_sno = '{#in_creator_sno}'
                    {#orgCond}
                ORDER BY
	                t1.in_stuff_b1
	                , t5.sort_order
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_creator_sno}", cfg.in_creator_sno)
                .Replace("{#orgCond}", orgCond);

            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }
            public string meeting_id { get; set; }
            public string in_creator_sno { get; set; }
            public string in_current_org { get; set; }
            public string muid { get; set; }
            public string scene { get; set; }
            public string in_l1 { get; set; }

            public Item itmMeeting { get; set; }
        }

        private class TOrg
        {
            public string in_stuff_b1 { get; set; }
            public string in_current_org { get; set; }
            public string in_short_org { get; set; }
            public string org_type { get; set; }
            public string in_creator { get; set; }
            public string in_creator_sno { get; set; }
            public string in_creator_tel { get; set; }
            public string in_creator_email { get; set; }
            public List<TPerson> managers { get; set; }
            public List<TPerson> leaders { get; set; }
            public List<TPerson> coaches_n { get; set; }
            public List<TPerson> coaches_w { get; set; }
            public List<TPerson> coaches_m { get; set; }
            public List<TPerson> insurances { get; set; }
            public List<TPerson> staffs { get; set; }
            public List<TPerson> players { get; set; }
            public List<TPerson> items { get; set; }

            public List<string> payment_status_a { get; set; }
            public List<string> payment_status_y { get; set; }
            public List<string> payment_status_n { get; set; }

            public List<string> errors { get; set; }
        }

        private class TPerson
        {
            public string in_name { get; set; }
            public string in_sno { get; set; }
            public Item value { get; set; }
            public List<Item> items { get; set; }
        }

        private class TResponse
        {
            public List<string> errors { get; set; }
            public bool all_has_pay_number { get; set; }
        }

        private DateTime GetDtm(string value)
        {
            if (value == "") return DateTime.MinValue;
            DateTime result = DateTime.MinValue;
            DateTime.TryParse(value, out result);
            return result;
        }
    }
}