﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Wresling.報名
{
    public class In_MeetingUserResponse : Item
    {
        public In_MeetingUserResponse(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var _InnH = new Innosoft.InnovatorHelper(inn);
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "In_MeetingUserResponse";

            var itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);
            
            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                strIdentityId = inn.getUserAliases(),

                meeting_id = itmR.getProperty("in_meetingid", ""),
                scene = itmR.getProperty("scene", ""),
            };

            //偷換網址

            //2020-07-14: 將 3ECBFFB153D54E2C8509E807F629C9A6 與 EA4B36A29BDC4003A59BE5F06958FD92 對調
            //if(MeetingId=="3ECBFFB153D54E2C8509E807F629C9A6")
            //    MeetingId = "EA4B36A29BDC4003A59BE5F06958FD92";
            //    
            //if(MeetingId=="EA4B36A29BDC4003A59BE5F06958FD92")
            //    MeetingId = "3ECBFFB153D54E2C8509E807F629C9A6";

            //偷換網址

            string flag = "";
            string MeetingUserName = "";
            string ResponseTime = "";
            string MeetingUserAns = "";
            string MeetingMaster = "";
            string BackupEmail = "";
            string CurrentTime = "";

            string sql = "";

            string OuterInnovatorURL = _InnH.GetInVariable("app_url").getProperty("in_value", "");
            /*
                如果參數是in_meeting_userid和ConfirmAction
                那麼就在MeetingUser.apply()後把ConfirmAction的值複製到flag上
                以便讓notify_message發生作用
            */

            if (cfg.meeting_id == null || cfg.meeting_id == "")
            {

                string MeetingUserId = this.getProperty("in_meeting_userid", "");
                MeetingUserId = MeetingUserId.Replace("/pages/b.aspx", "");
                string ConfirmAction = this.getProperty("ConfirmAction", "");
                ConfirmAction = ConfirmAction.Replace("/pages/b.aspx", "");
                //CCO.Utilities.WriteDebug("In_MeetingUserResponse", "ConfirmAction:" + ConfirmAction);
                //CCO.Utilities.WriteDebug("In_MeetingUserResponse", "MeetingUserId:" + MeetingUserId);
                Item ItmMeetingUser = inn.getItemById("In_Meeting_User", MeetingUserId);
                BackupEmail = ItmMeetingUser.getProperty("in_mail", "");
                DateTime CurrentTimes = System.DateTime.Now;
                CurrentTime = CurrentTimes.ToString("yyyyMMddHHmmssfff");


                Item MeetingUser = inn.newItem("In_Meeting_User", "edit");
                MeetingUser.setAttribute("where", "[In_Meeting_User].id='" + MeetingUserId + "'");
                switch (ConfirmAction)
                {
                    case "nogo":
                        MeetingUser.setProperty("in_ans", "reject");
                        //MeetingUser.setProperty("in_note_state", "cancel");
                        //MeetingUser.setProperty("in_note",BackupEmail);
                        //MeetingUser.setProperty("in_mail",CurrentTime);
                        break;
                    case "go":
                        MeetingUser.setProperty("in_ans", "agree");
                        break;
                    default:

                        break;
                }
                MeetingUser = MeetingUser.apply();
                flag = ConfirmAction;
                cfg.meeting_id = MeetingUser.getProperty("source_id", "");
                /******************************************************************************/
                //這裡的變數為In_Meeting_User上的Property並對應到Mail中的欄位
                MeetingUserName = MeetingUser.getProperty("in_name", "");
                DateTime TodayTime = System.DateTime.Now;
                ResponseTime = TodayTime.ToString("yyyy/MM/dd HH:mm");//取系統時間做為建立時間
                /******************************************************************************/
                //這裡不直接用switch case做英文轉中文
                /*
                 <AML>
                    <Item type="List" action="get">
                        <name>in_meeting_ans</name>
                        <Relationships>
                            <Item type="Value" action="get">
                            </Item>
                        </Relationships>
                    </Item>
                </AML>
                */
                //以上AML可知agree,reject,null為物件類型Value上的Property,如<value>agree</value>
                //而對應的中文字同在物件類型Value上的Property,如<label>要參加</label>
                MeetingUserAns = MeetingUser.getProperty("in_ans", "");
                Item itmList = inn.newItem("List", "get");
                itmList.setProperty("name", "in_meeting_ans");
                itmList = itmList.apply();
                string ListId = itmList.getID();
                Item itmValue = newItem("Value", "get");
                itmValue.setProperty("source_id", ListId);
                itmValue.setProperty("value", MeetingUserAns);
                itmValue = itmValue.apply("get");
                if (!itmValue.isError())
                    MeetingUserAns = itmValue.getProperty("label");//應該要是"要參加,不參加,沒回覆其一"
                /******************************************************************************/

            }
            //此處挖到relationship的理由是為了讓In_Meeting_info上有資料
            Item Meeting = inn.getItemById("In_Meeting", cfg.meeting_id);
            // Item MeetingRelated = Meeting.fetchRelationships("In_Meeting_Agenda");
            // Item MeetingRelationships = MeetingRelated.getRelationships();
            Meeting = Meeting.apply("get");
            string in_title = Meeting.getProperty("in_title", "");

            string date_s_f = Meeting.getProperty("in_date_s");
            string date_e_f = Meeting.getProperty("in_date_e");
            string in_date_s_f = DateTime.Parse(date_s_f).ToString("yyyy-MM-dd HH:mm");
            string in_date_e_f = DateTime.Parse(date_s_f).ToString("yyyy-MM-dd HH:mm");

            Meeting.setProperty("in_date_s_f", in_date_s_f);
            Meeting.setProperty("in_date_e_f", in_date_e_f);


            string Send = Meeting.getProperty("in_state_time", System.DateTime.Today.ToString("yyyy-MM-dd"));//取得結束時間
            DateTime Timer_end = Convert.ToDateTime(Send);//將結束時間轉型
            if (Timer_end < System.DateTime.Now)//結束時間<現在時間
            {
                Meeting.setProperty("is_cutoff", "1");
            }
            else
            {
                Meeting.setProperty("is_cutoff", "0");
            }


            if (flag != "")//此處是對應in_meeting_userid和ConfirmAction的作法
            {
                switch (flag)
                {
                    case "nogo":
                        Meeting.setProperty("notify_message", "已取消報名,您可以至會議列表中重新選擇其他場次");
                        break;
                    case "go":
                        Meeting.setProperty("notify_message", "已確認參加,當天請準時出席");
                        break;
                    default:
                        break;
                }
                string MeetingTitle = Meeting.getProperty("in_title", "");//會議名稱
                MeetingMaster = OuterInnovatorURL + "pages/c.aspx?method=In_Get_SingleItemInfoGeneral&itemtype=in_meeting&itemid=" + MeetingId + "&fetchproperty=4&mode=direct&page=in_Meeting_Master.html&inn_editor_type=edit&rels=In_Meeting_User";

                string in_contract = Meeting.getProperty("in_contract", "");//聯繫窗口
                string in_mail = Meeting.getProperty("in_mail", "");//聯繫郵件
                                                                    //這裡的str為"在報名者按下確定參加或取消報名後，寄送給系統管理員的通知"
                string str = @"
    <html>
    <body>
    <table border=0 cellpadding=0 cellspacing=0 width=203 style='border-collapse:
    collapse;table-layout:fixed;width:152pt'>
    <h1>系統管理員通知</h1>
    <table border=""1"" cellspacing=""0"" width=""250px"">
    <tr>
    <td style=""min-width:0px;"" width=""60"">姓名:</td>
    <td>@MeetingUserName</td>
    </tr>
    
    
    <tr>
    <td style=""min-width:0px;"" width=""60"">回復時間:</td>
    <td>@ResponseTime</td>
    </tr>
    
    <tr>
    <td style=""min-width:0px;"" width=""60"">會議:</td>
    <td>@MeetingTitle</td>
    </tr>
    
    <tr>
    <td style=""min-width:0px;"" width=""60"">回復意願</td>
    <td>@MeetingAns</td>
    </tr>
    
    <tr>
    <td style=""min-width:0px;"" width=""60"">報名列表:</td>
    <td><a href=""@MeetingMaster"">請點選此連結</a></td>
    </tr>
    </table>
    </table>
    </body>
    </html>"
                           .Replace("@MeetingUserName", MeetingUserName)
                           .Replace("@ResponseTime", ResponseTime)
                           .Replace("@MeetingTitle", MeetingTitle)
                           .Replace("@MeetingAns", MeetingUserAns)
                           .Replace("@MeetingMaster", MeetingMaster)
                           ;


                System.Net.Mail.MailAddress from = new System.Net.Mail.MailAddress(in_mail, in_contract);
                System.Net.Mail.MailAddress to = new System.Net.Mail.MailAddress(in_mail, in_contract);
                System.Net.Mail.MailMessage myMail = new System.Net.Mail.MailMessage(from, to);
                myMail.Subject = "[學員意願回覆]-" + MeetingTitle;
                myMail.SubjectEncoding = System.Text.Encoding.UTF8;
                myMail.Body = str;
                myMail.BodyEncoding = System.Text.Encoding.UTF8;
                myMail.IsBodyHtml = true;
                CCO.Email.SetupSmtpMailServerAndSend(myMail);
            }

            //為了要遷就In_Get_MeetingList的效果,所以要把同主場的 報名資訊也一併列出
            string RefMeetingID = Meeting.getProperty("in_refmeeting", Meeting.getID());
            string aml = "<AML>";
            aml += "<Item type='in_meeting' action='get'>";
            aml += "<and>";
            aml += "<in_refmeeting>" + RefMeetingID + "</in_refmeeting>";
            aml += "<id condition='ne'>" + RefMeetingID + "</id>";
            aml += "</and>";
            aml += "</Item>";
            aml += "</AML>";

            Item itmMeetings = inn.applyAML(aml);
            //Item itmMeetings = Meeting;
            for (int i = 0; i < itmMeetings.getItemCount(); i++)
            {
                Item itmMeeting = itmMeetings.getItemByIndex(i);
                itmMeeting.setType("Inn_SearchResult");

                //為了處理 ios 的瀏覽器 會誤判時區,所以在SERVER端就把時間轉好,為了怕影響其他的變數,所以創了一個新變數
                string FormattedDate_S = DateTime.Parse(itmMeeting.getProperty("in_date_s")).ToString("yyyy-MM-dd HH:mm");
                string FormattedDate_E = DateTime.Parse(itmMeeting.getProperty("in_date_e")).ToString("yyyy-MM-dd HH:mm");

                itmMeeting.setProperty("in_date_s_f", FormattedDate_S);
                itmMeeting.setProperty("in_date_e_f", FormattedDate_E);

                Meeting.addRelationship(itmMeeting);

                if (itmMeeting.getID() == Meeting.getID())
                {
                    Meeting.setProperty("in_date_s_f", FormattedDate_S);
                    Meeting.setProperty("in_date_e_f", FormattedDate_E);
                }

            }
            Meeting.fetchRelationships("In_Meeting_File");
            Item MeetingFiles = Meeting.getRelationships("In_Meeting_File");
            int MeetingFileCount = MeetingFiles.getItemCount();
            for (int mf = 0; mf < MeetingFileCount; mf++)
            {
                Item MeetingFile = MeetingFiles.getItemByIndex(mf);
                //如果Vault沒有搬過來,那 MeetingFile.getRelatedItem() 會是 null,所以要避開
                if (MeetingFile.getRelatedItem() != null)
                {
                    MeetingFile.setProperty("related_id.keyed_name", MeetingFile.getRelatedItem().getProperty("keyed_name", ""));
                    MeetingFile.setProperty("related_id.mimetype", MeetingFile.getRelatedItem().getProperty("mimetype", ""));
                }
            }

            string meeting_game_name = inn.getItemByKeyedName("in_variable", "meeting_game_name").getProperty("in_value", "");
            Meeting.setProperty("meeting_game_name", meeting_game_name);

            Item itmLoginResume = GetLoginResume(CCO, strMethodName, inn, this);
            Meeting.setProperty("inn_sno", itmLoginResume.getProperty("in_sno", ""));

            //lina 2020.10.23 增加審核繳費 tab (預設關閉)
            EnabledTabByFlag(Meeting, "review_tab", "0");

            //lina 2020.09.10 增加使用者承諾
            AppendUserAgreement(CCO, strMethodName, inn, itmLoginResume, Meeting);

            //設定頁籤
            SetMeetingTabs(CCO, strMethodName, inn, itmLoginResume, Meeting);

            return Meeting;

        }

        //取得登入者 Resume
        private Item GetLoginResume(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmThis)
        {
            string ssn_loginid = itmThis.getProperty("ssn_loginid", "");

            if (ssn_loginid == "")
            {
                return inn.newItem();
            }
            else
            {
                string sql = "SELECT TOP 1 * FROM In_Resume WITH(NOLOCK) WHERE in_sno = N'{#in_sno}'"
                    .Replace("{#in_sno}", ssn_loginid);

                return inn.applySQL(sql);
            }
        }

        //設定使用者承諾
        private void AppendUserAgreement(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmResume, Item itmReturn)
        {
            string sql = "";

            string meeting_id = itmReturn.getProperty("id", "");
            if (meeting_id == "")
            {
                itmReturn.setProperty("has_agreed", "");
                return;
            }

            if (itmResume.isError() || itmResume.getResult() == "")
            {
                itmReturn.setProperty("has_agreed", "");
                return;
            }

            string in_type = "in_meeting";
            string in_item = "privacy";
            string in_user_id = itmResume.getProperty("in_user_id", "");
            string in_is_teacher = itmResume.getProperty("in_is_teacher", "");
            string login_name = itmResume.getProperty("login_name", "");

            sql = "SELECT TOP 1 id FROM In_Agreement WITH(NOLOCK) WHERE in_type = '{#in_type}' AND in_id = '{#meeting_id}' AND in_item = N'{#in_item}' AND in_user_id = '{#in_user_id}'"
                .Replace("{#in_type}", in_type)
                .Replace("{#meeting_id}", meeting_id)
                .Replace("{#in_item}", in_item)
                .Replace("{#in_user_id}", in_user_id);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item itmAgreement = inn.applySQL(sql);

            if (itmAgreement.isError() || itmAgreement.getResult() == "")
            {
                itmReturn.setProperty("has_agreed", "0");
                return;
            }

            itmReturn.setProperty("has_agreed", "1");
        }

        //設定頁籤
        private void SetMeetingTabs(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmResume, Item itmReturn)
        {
            //登入者是否為講師
            string in_is_teacher = itmResume.getProperty("in_is_teacher", "");
            //賽事 id
            string meeting_id = itmReturn.getProperty("id", "");
            //是否同意站台規範與隱私權
            string has_agreed = itmReturn.getProperty("has_agreed", "");

            string hide = "item_show_0";
            string show = "item_show_1";

            //審核繳費
            string review_tab = hide;
            //工作人員
            string pro_staff_tab = hide;

            //公開資訊
            EnabledTabByValue(itmReturn, "activity_tab", itmReturn.getProperty("in_propaganda_2", ""));
            EnabledTabByValue(itmReturn, "schedule_tab", itmReturn.getProperty("in_propaganda_3", ""));
            EnabledTabByValue(itmReturn, "business_tab", itmReturn.getProperty("in_propaganda_4", ""));
            EnabledTabByValue(itmReturn, "notice_tab", itmReturn.getProperty("in_propaganda_5", ""));


            //隱私資訊
            EnabledTabByFlag(itmReturn, "registry_tab", has_agreed);
            EnabledTabByFlag(itmReturn, "payment_tab", has_agreed);
            EnabledTabByFlag(itmReturn, "print_tab", has_agreed);
            EnabledTabByFlag(itmReturn, "score_tab", "0");

            if (in_is_teacher == "1")
            {
                //取得角色權限
                Item itmPermit = GetLoginIdentity(CCO, strMethodName, inn, itmResume, meeting_id);
                string isAdmin = itmPermit.getProperty("isAdmin", "");
                if (isAdmin == "1")
                {
                    review_tab = show;
                    pro_staff_tab = show;
                    //繳費資訊
                    itmReturn.setProperty("payment_tab", hide);
                }
            }

            //審核繳費
            itmReturn.setProperty("review_tab", review_tab);
            //工作人員
            itmReturn.setProperty("pro_staff_tab", pro_staff_tab);
        }

        private Item GetLoginIdentity(TConfig cfg, Item itmResume, string meeting_id)
        {
            Item itmResult = cfg.inn.newItem();

            string resume_id = itmResume.getProperty("id", "");
            string in_user_id = itmResume.getProperty("in_user_id", "");

            var is_admin = false;
            if (in_user_id != "")
            {
                string sql = @"
                    SELECT
                        t3.id
                        , t3.name
                        , t1.in_number
                    FROM
                        [IDENTITY] t1 WITH(NOLOCK)
                    INNER JOIN
                        [MEMBER] t2 WITH(NOLOCK)
                        ON t2.related_id = t1.id
                    INNER JOIN
                        [IDENTITY] t3 WITH(NOLOCK)
                        ON t3.id = t2.source_id
                    WHERE
                        t1.in_user = '{#in_user}'
                        AND t3.name in (N'Administrator', N'MeetingAdmin')
                ";

                sql = sql.Replace("{#in_user}", in_user_id);
                //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

                Item itmIdentities = cfg.inn.applySQL(sql);
                //CCO.Utilities.WriteDebug(strMethodName, "dom: " +itmMAdmins.dom.InnerXml);

                var count = itmIdentities.getItemCount();
                for (int i = 0; i < count; i++)
                {
                    var itmIdentity = itmIdentities.getItemByIndex(i);
                    var idt_name = itmIdentity.getProperty("name", "").ToLower();
                    switch (idt_name)
                    {
                        case "administrator":
                        case "meetingadmin":
                            is_admin = true;
                            itmResult.setProperty("isAdmin", "1");
                            break;

                        default:
                            break;
                    }

                    if (is_admin)
                    {
                        break;
                    }
                }
            }

            //共同講師
            if (!is_admin)
            {
                string sql_open = @"
                    SELECT
                        t2.id
                    FROM 
                        In_Meeting_Resumelist t1 WITH(NOLOCK) 
                    INNER JOIN 
                        IN_RESUME t2 WITH(NOLOCK) 
                        ON t2.id = t1.related_id    
                    WHERE 
                        t1.source_id = '{#meeting_id}' 
                        AND t2.id = '{#resume_id}'
                ";

                sql_open = sql_open.Replace("{#meeting_id}", meeting_id)
                    .Replace("{#resume_id}", resume_id);

                //CCO.Utilities.WriteDebug("[PLMCTA]In_Cla_MeetingUserResponse",  "sql_open: " + sql_open);

                Item item = cfg.inn.applySQL(sql_open);

                if (item.getResult() != "")
                {
                    itmResult.setProperty("isAdmin", "1");
                }
            }

            return itmResult;
        }

        private void EnabledTabByFlag(Item itmReturn, string property, string value)
        {
            if (value != "1")
            {
                itmReturn.setProperty(property, "item_show_0");
            }
            else
            {
                itmReturn.setProperty(property, "item_show_1");
            }
        }

        private void EnabledTabByValue(Item itmReturn, string property, string value)
        {
            if (value != "")
            {
                itmReturn.setProperty(property, "item_show_1");
            }
            else
            {
                itmReturn.setProperty(property, "item_show_0");
            }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }

            public string meeting_id { get; set; }
            public string scene { get; set; }
        }

    }
}