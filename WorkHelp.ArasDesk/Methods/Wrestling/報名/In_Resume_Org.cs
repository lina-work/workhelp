﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Wresling.報名
{
    public class In_Resume_Org : Item
    {
        public In_Resume_Org(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
    目的: 可報單位列表
    日誌: 
        - 2024-06-19: 創建 (lina)
*/

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "In_Resume_Org";

            var itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                strIdentityId = inn.getUserAliases(),
                scene = itmR.getProperty("scene", ""),
            };

            switch (cfg.scene)
            {
                case "page":
                    Page(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void Page(TConfig cfg, Item itmReturn)
        {
            var sql = "SELECT id, in_name, in_sno, in_current_org FROM IN_RESUME WITH(NOLOCK) WHERE in_user_id = '" + cfg.strUserId + "'";
            cfg.itmResume = cfg.inn.applySQL(sql);

            var resume_id = cfg.itmResume.getProperty("id", "");
            var in_current_org = cfg.itmResume.getProperty("in_current_org", "");

            sql = @"
                SELECT 
	                t1.id
                    , t2.in_type
                    , t1.in_current_org
                FROM 
	                IN_RESUME_ORG t1 WITH(NOLOCK) 
                LEFT OUTER JOIN
	                IN_ORG_MAP t2 WITH(NOLOCK)
	                ON t2.in_current_org = t1.in_current_org
                WHERE 
	                t1.source_id = '{#resume_id}'
                ORDER BY
	                t2.in_stuff_b1
            ";

            sql = sql.Replace("{#resume_id}", resume_id);

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            if (count <= 0)
            {
                AddSelfCurrentOrg(cfg, resume_id);
                items = cfg.inn.applySQL(sql);
                count = items.getItemCount();
            }

            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var _org = item.getProperty("in_current_org", "");
                var disabled = _org == in_current_org ? "disabled" : "";

                item.setType("inn_org");
                item.setProperty("no", (i + 1).ToString());
                item.setProperty("disabled", disabled);
                itmReturn.addRelationship(item);
            }

            AppendOrgMenu(cfg, itmReturn);
        }

        private void AddSelfCurrentOrg(TConfig cfg, string resume_id)
        {
            var itmNew = cfg.inn.newItem("In_Resume_Org", "add");
            itmNew.setProperty("source_id", resume_id);
            itmNew.setProperty("in_current_org", cfg.itmResume.getProperty("in_current_org", ""));
            itmNew.apply();
        }

        private void AppendOrgMenu(TConfig cfg, Item itmReturn)
        {
            var nodes = new List<TNode>();
            var sql = "SELECT in_type, in_current_org FROM IN_ORG_MAP ORDER BY in_stuff_b1";
            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var key1 = item.getProperty("in_type", "");
                var key2 = item.getProperty("in_current_org", "");
                var n1 = nodes.Find(x => x.name == key1);
                if (n1 == null)
                {
                    n1 = new TNode
                    {
                        name = key1,
                        nodes = new List<TNode>(),
                    };
                    nodes.Add(n1);
                }
                n1.nodes.Add(new TNode { name = key2 });
            }
            itmReturn.setProperty("inn_org_json", Newtonsoft.Json.JsonConvert.SerializeObject(nodes));
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }
            public string scene { get; set; }
            public Item itmResume { get; set; }
        }

        private class TNode
        {
            public string name { get; set; }
            public List<TNode> nodes { get; set; }
        }
    }
}