﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Wrestling.場地分配
{
    internal class in_meeting_program_event : Item
    {
        public in_meeting_program_event(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 賽程安排
                輸入: meeting_id
                人員: lina
                日期: 2020/07/09~07/17
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]in_meeting_program_event";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "input dom: " + itmR.dom.InnerXml);

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                meeting_id = itmR.getProperty("meeting_id", "").Trim(),
                mode = itmR.getProperty("mode", "").Trim(),
            };

            if (cfg.meeting_id == "")
            {
                throw new Exception("賽事 id 不得為空白");
            }

            //檢查頁面權限
            Item itmCheckIdentity = inn.applyMethod("In_CheckIdentity", "<method>" + strMethodName + "</method><code>" + "ALL" + "</code>");
            bool isMeetingAdmin = itmCheckIdentity.getProperty("isMeetingAdmin", "") == "1";
            if (!isMeetingAdmin) throw new Exception("您無權限瀏覽此頁面");

            switch(cfg.mode)
            {
                case "allocate":
                    //更新賽事場地資料
                    UpdateMeeting(cfg, itmR);
                    //配置場地編號
                    AllocateSite(cfg, itmR);
                    break;

                case "modal":
                    //跳出視窗
                    AppendSiteModal(cfg, itmR);
                    break;

                case "update":
                    //更新場次編號
                    UpdateTreeNo1(cfg, itmR);
                    break;

                case "update2":
                    //更新場次編號
                    UpdateTreeNo2(cfg, itmR);
                    break;

                default:
                    Query(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void UpdateTreeNo2(TConfig cfg, Item itmReturn)
        {
            string program_id = itmReturn.getProperty("program_id", "");
            string event_id = itmReturn.getProperty("event_id", "");
            string fight_id = itmReturn.getProperty("fight_id", "");
            string tree_no = itmReturn.getProperty("tree_no", "").Trim();

            if (event_id == "" && fight_id == "") throw new Exception("場次 id 不可為空");

            if (tree_no == "NA") tree_no = "0";

            int temp_no = 0;
            if (tree_no != "")
            {
                if (!int.TryParse(tree_no, out temp_no))
                {
                    throw new Exception("場次編號必須為整數");
                }
            }

            var sql = event_id == ""
                ? "SELECT id, in_site, in_date_key FROM IN_MEETING_PEVENT WITH(NOLOCK) WHERE source_id = '" + program_id + "' AND in_fight_id = '" + fight_id + "'"
                : "SELECT id, in_site, in_date_key FROM IN_MEETING_PEVENT WITH(NOLOCK) WHERE id = '" + event_id + "'";

            var itmEvent = cfg.inn.applySQL(sql);
            if (itmEvent.isError() || itmEvent.getResult() == "") throw new Exception("場次資料異常");

            var eid = itmEvent.getProperty("id", "");
            var site_id = itmEvent.getProperty("in_site", "");
            var in_date_key = itmEvent.getProperty("in_date_key", "");

            var itmSite = cfg.inn.applySQL("SELECT * FROM IN_MEETING_SITE WITH(NOLOCK) WHERE id = '" + site_id + "'");
            if (itmSite.isError() || itmSite.getResult() == "") throw new Exception("場地資料異常");

            var site_name = itmSite.getProperty("in_code_en", "");
            var new_tree_no = tree_no == "" || tree_no == "0" ? "NULL" : "'" + tree_no + "'";

            var number_repeat_check = GetMeetingVariable(cfg, "number_repeat_check");

            if (new_tree_no != "NULL")
            {
                var sql_qry = "SELECT id FROM IN_MEETING_PEVENT WITH(NOLOCK) WHERE in_meeting = '" + cfg.meeting_id + "' AND in_date_key = '" + in_date_key + "' AND in_site ='" + site_id + "' AND in_tree_no = " + new_tree_no;
                var itmQry = cfg.inn.applySQL(sql_qry);
                var count = itmQry.getItemCount();
                var needCheck = count >= 1;

                if (count == 1)
                {
                    var old_id = itmQry.getProperty("id", "");
                    if (old_id == eid) needCheck = false;
                }

                sql_qry = "SELECT max(in_tree_no) AS 'mx' FROM IN_MEETING_PEVENT WITH(NOLOCK) WHERE in_meeting = '" + cfg.meeting_id + "' AND in_date_key = '" + in_date_key + "' AND in_site = '" + site_id + "' AND id <> '" + eid + "'";
                var itmMx = cfg.inn.applySQL(sql_qry);
                var mx_tree_no = GetIntVal(itmMx.getProperty("mx", "0")) + 1;

                if (needCheck)
                {
                    if (number_repeat_check == "Y")
                    {
                        throw new Exception(in_date_key + " " + site_name + " " + new_tree_no + " 已被使用，建議使用 " + mx_tree_no);
                    }
                }
                else
                {
                    var a = mx_tree_no;
                    var b = temp_no;
                    var numberSpan = System.Math.Abs(a - b);
                    if (numberSpan >= 1)
                    {
                        var inn_suggestion = in_date_key + " " + site_name + " " + new_tree_no + " 跳號，建議使用 " + mx_tree_no;
                        itmReturn.setProperty("inn_suggestion", inn_suggestion);
                    }
                }
            }

            var sql_upd = "UPDATE IN_MEETING_PEVENT SET"
                + "  in_tree_no = " + new_tree_no
                + ", in_show_serial = " + new_tree_no
                + ", in_site_move = NULL"
                + ", in_site_time = NULL"
                + " WHERE id = '" + eid + "'";

            cfg.inn.applySQL(sql_upd);
        }


        private void UpdateTreeNo1(TConfig cfg, Item itmReturn)
        {
            string sql = "";
            Item itmSQL = null;

            string program_id = itmReturn.getProperty("program_id", "").Trim();
            string tree_id = itmReturn.getProperty("tree_id", "").Trim();
            string tree_no = itmReturn.getProperty("tree_no", "").Trim();
            string auto_shift = itmReturn.getProperty("auto_shift", "").Trim();

            if (tree_no == "NA") tree_no = "0";

            if (tree_no != "")
            {
                int temp_no = 0;
                if (!Int32.TryParse(tree_no, out temp_no))
                {
                    throw new Exception("場次編號必須為整數");
                }
            }

            sql = "SELECT * FROM IN_MEETING_PEVENT WITH(NOLOCK) WHERE source_id = '" + program_id + "' AND in_tree_id = '" + tree_id + "'";
            Item itmEvent = cfg.inn.applySQL(sql);
            int event_count = itmEvent.getItemCount();
            if (itmEvent.isError() || event_count != 1) throw new Exception("場次資料異常");

            string in_site = itmEvent.getProperty("in_site", "");
            string in_date_key = itmEvent.getProperty("in_date_key", "");

            if (auto_shift == "1")
            {
                //大於等於 new in_tree_no 者，往後順延

                sql = @"
                    UPDATE t2 SET
            	        t2.in_tree_no = t2.in_tree_no + 1
                    FROM
            	        IN_MEETING_PROGRAM t1 WITH(NOLOCK) 
                    INNER JOIN 
            	        IN_MEETING_PEVENT t2 WITH(NOLOCK)
            	        ON t2.source_id = t1.id 
                    WHERE
            	        t1.in_meeting = '{#meeting_id}'
            	        AND t2.in_site = '{#in_site}' 
            	        AND t2.in_date_key = '{#in_date_key}'
            	        AND ISNULL(t2.in_tree_no, 0) <> 0
            	        AND t2.in_tree_no >= {#new_tree_no}
                ";

                sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                    .Replace("{#in_site}", in_site)
                    .Replace("{#in_date_key}", in_date_key)
                    .Replace("{#new_tree_no}", tree_no);

                itmSQL = cfg.inn.applySQL(sql);
                if (itmSQL.isError())
                {
                    throw new Exception("場次編號更新失敗");
                }
            }

            var sql_upd = "UPDATE IN_MEETING_PEVENT SET"
                + "  in_tree_no = " + tree_no
                + ", in_show_serial = " + tree_no
                + ", in_site_move = NULL"
                + ", in_site_time = NULL"
                + " WHERE source_id = '" + program_id + "' AND in_tree_id = '" + tree_id + "'";

            cfg.inn.applySQL(sql_upd);

            itmSQL = cfg.inn.applySQL(sql);

            if (itmSQL.isError())
            {
                throw new Exception("場次編號更新失敗");
            }

            // if (in_type != "s")
            // {
            //     if (auto_shift == "1")
            //     {
            //         ResetSubTreeNo(cfg, meeting_id, in_date_key, in_site);
            //     }
            //     else
            //     {
            //         ResetSubTreeNo(cfg, meeting_id, event_id);
            //     }
            // }
        }

        //子場次全部重編
        private void ResetSubTreeNo(TConfig cfg, string event_id)
        {
            string sql = "";
            Item itmSQL = null;

            sql = @"
                UPDATE t1 SET
                    t1.in_tree_no = t2.in_tree_no * 100 + t1.in_sub_id
                FROM 
                    IN_MEETING_PEVENT t1 WITH(NOLOCK) 
                INNER JOIN
                    IN_MEETING_PEVENT t2 WITH(NOLOCK) 
                    ON t2.id = t1.in_parent
                WHERE
                    t2.id = '{#event_id}'
                    AND ISNULL(t2.in_tree_no, 0) <> 0
            ";

            sql = sql.Replace("{#event_id}", event_id);

            cfg.inn.applySQL(sql);
        }

        //子場次全部重編
        private void ResetSubTreeNo(TConfig cfg, string in_date_key, string in_site)
        {
            string sql = "";
            Item itmSQL = null;

            sql = @"
                UPDATE t1 SET
                    t1.in_tree_no = t2.in_tree_no * 100 + t1.in_sub_id
                FROM 
                    IN_MEETING_PEVENT t1 WITH(NOLOCK) 
                INNER JOIN
                    IN_MEETING_PEVENT t2 WITH(NOLOCK) 
                    ON t2.id = t1.in_parent
                WHERE
                    t1.in_meeting = '{#meeting_id}'
                    AND t1.in_tree_name = 'sub'
                    AND t1.in_date_key = '{#in_date_key}'
                    AND t1.in_site = '{#in_site}'
                    AND ISNULL(t2.in_tree_no, 0) <> 0
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_date_key}", in_date_key)
                .Replace("{#in_site}", in_site);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            cfg.inn.applySQL(sql);
        }

        private void AppendSiteModal(TConfig cfg, Item itmReturn)
        {
            string in_site_count = itmReturn.getProperty("in_site_count", "").Trim();
            string in_site_code = itmReturn.getProperty("in_site_code", "").Trim();

            if (in_site_count == "")
            {
                throw new Exception("請選擇 場地數量");
            }

            if (in_site_code == "")
            {
                throw new Exception("請選擇 場地編碼");
            }

            int site_count = 0;
            if (!int.TryParse(in_site_count, out site_count))
            {
                throw new Exception("場地數量錯誤");
            }

            //更新組別的場地編碼與場地數量
            string sql = "UPDATE IN_MEETING SET in_site_count = '{#in_site_count}', in_site_code = N'{#in_site_code}', in_site_mode = N'{#in_site_mode}' WHERE id = '{#meeting_id}'";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_site_count}", in_site_count)
                .Replace("{#in_site_code}", in_site_code)
                .Replace("{#in_site_mode}", "assign");

            cfg.inn.applySQL(sql);

            //賽事組別清單
            Item itmPrograms = GetPrograms(cfg);

            int count = itmPrograms.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item itmProgram = itmPrograms.getItemByIndex(i);

                Item item = cfg.inn.newItem();
                item.setType("inn_edit_program");
                item.setProperty("no", (i + 1).ToString());
                item.setProperty("program_id", itmProgram.getProperty("id", ""));
                item.setProperty("program_display", itmProgram.getProperty("in_display", ""));
                item.setProperty("program_site_code", itmProgram.getProperty("in_site_code", ""));
                itmReturn.addRelationship(item);
            }

            //場地清單
            string[] site_codes = GetSiteCodeArray(site_count, in_site_code);

            Item itmEmptySiteCode = cfg.inn.newItem();
            itmEmptySiteCode.setType("inn_site_code");
            itmEmptySiteCode.setProperty("in_label", "指定場地");
            itmEmptySiteCode.setProperty("in_value", "");
            itmEmptySiteCode.setProperty("in_selected", "selected");
            itmReturn.addRelationship(itmEmptySiteCode);

            for (int i = 0; i < site_count; i++)
            {
                string site_code = site_codes[i];

                Item item = cfg.inn.newItem();
                item.setType("inn_site_code");
                item.setProperty("in_label", "場地 " + site_code);
                item.setProperty("in_value", site_code);
                itmReturn.addRelationship(item);
            }
        }

        //配置場地編號
        private void AllocateSite(TConfig cfg, Item itmReturn)
        {
            string in_site_count = itmReturn.getProperty("site_count", "");
            string in_site_mode = itmReturn.getProperty("site_mode", "");

            int site_count = GetIntVal(in_site_count);

            if (site_count < 1)
            {
                throw new Exception("場地數量錯誤");
            }

            if (site_count > 26)
            {
                throw new Exception("場地數量不可大於26");
            }

            if (in_site_mode == "cycle")
            {
                //不分場地模式
                UpdateFightsByCycle(cfg, itmReturn);
            }
            else if (in_site_mode == "assign")
            {
                //指定場地模式
                UpdateFightsByAssign(cfg, itmReturn);
            }
        }

        /// <summary>
        /// 不分場地模式
        /// </summary>
        private void UpdateFightsByCycle(TConfig cfg, Item itmReturn)
        {
            string mt_site_count = itmReturn.getProperty("site_count", "");
            string mt_site_code = itmReturn.getProperty("site_code", "");

            int site_count = GetIntVal(mt_site_count);

            string[] site_codes = GetSiteCodeArray(site_count, mt_site_code);

            Item items = GetEvents(cfg);

            string sql = @"
                UPDATE
                    IN_MEETING_PEVENT
                SET
                    in_site_code = '{#in_site_code}'
                    , in_site_no = '{#in_site_no}'
                    , in_site_id = '{#in_site_id}'
                    , in_site_allocate = '1'
                WHERE
                    id = '{#event_id}'
                ";

            int count = items.getItemCount();
            int index = 0;

            for (int i = 0; i < count; i++)
            {
                if (i % site_count == 0)
                {
                    index++;
                }

                Item item = items.getItemByIndex(i);
                string event_id = item.getProperty("event_id", "");
                string in_site_code = site_codes[i % site_count];
                string in_site_no = index.ToString();
                string in_site_id = in_site_code + in_site_no.PadLeft(3, '0');

                string sql_update = sql.Replace("{#event_id}", event_id)
                    .Replace("{#in_site_code}", in_site_code)
                    .Replace("{#in_site_no}", in_site_no)
                    .Replace("{#in_site_id}", in_site_id);

                //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql_update);

                Item itmSQL = cfg.inn.applySQL(sql_update);

                if (itmSQL.isError())
                {
                    cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "[不分場地模式]配置場地編號發生錯誤 sql: " + sql_update);
                }
            }
        }

        /// <summary>
        /// 指定場地模式
        /// </summary>
        private void UpdateFightsByAssign(TConfig cfg, Item itmReturn)
        {
            Item itmPrograms = GetPrograms(cfg);

            int count_program = itmPrograms.getItemCount();

            //組別與場地編號
            Dictionary<string, string> group_codes = new Dictionary<string, string>();
            //取號器
            Dictionary<string, int> take_numbers = new Dictionary<string, int>();

            //未指定場地組別清單
            List<string> nonassign_groups = new List<string>();

            for (int i = 0; i < count_program; i++)
            {
                Item itmProgram = itmPrograms.getItemByIndex(i);
                string program_id = itmProgram.getProperty("id", "");
                string program_display = itmProgram.getProperty("in_display", "");
                string in_site_code = itmProgram.getProperty("in_site_code", "");

                if (in_site_code == "")
                {
                    nonassign_groups.Add(program_display);
                    continue;
                }

                if (!group_codes.ContainsKey(program_id))
                {
                    group_codes.Add(program_id, in_site_code);
                }
                if (!take_numbers.ContainsKey(in_site_code))
                {
                    take_numbers.Add(in_site_code, 1);
                }
            }

            if (nonassign_groups.Count > 0)
            {
                throw new Exception("以下組別尚未指定場地: \n" + string.Join("\n", nonassign_groups));
            }

            var sql = @"
                UPDATE
                    IN_MEETING_PEVENT
                SET
                    in_site_code = '{#in_site_code}'
                    , in_site_no = '{#in_site_no}'
                    , in_site_id = '{#in_site_id}'
                    , in_site_allocate = '1'
                WHERE
                    id = '{#event_id}'
                ";

            var items = GetEvents(cfg);
            var count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var program_id = item.getProperty("program_id", "");
                var program_display = item.getProperty("program_display", "");
                var event_id = item.getProperty("event_id", "");

                if (program_display == "" || !group_codes.ContainsKey(program_id))
                {
                    cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "[指定場地模式]查無組別: " + program_display);
                    continue;
                }

                var in_site_code = group_codes[program_id];
                if (!take_numbers.ContainsKey(in_site_code))
                {
                    cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "[指定場地模式]查無場地編號: " + program_display + ", site_code: " + in_site_code);
                    continue;
                }

                var number = take_numbers[in_site_code];

                var in_site_no = number.ToString();
                var in_site_id = in_site_code + in_site_no.PadLeft(3, '0');

                var sql_update = sql.Replace("{#event_id}", event_id)
                    .Replace("{#in_site_code}", in_site_code)
                    .Replace("{#in_site_no}", in_site_no)
                    .Replace("{#in_site_id}", in_site_id);

                //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql_update);

                var itmSQL = cfg.inn.applySQL(sql_update);
                if (itmSQL.isError())
                {
                    cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "[指定場地模式]配置場地編號發生錯誤 sql: " + sql_update);
                }
                else
                {
                    take_numbers[in_site_code] = number + 1;
                }
            }
        }

        //更新賽事場地資料
        private void UpdateMeeting(TConfig cfg, Item itmReturn)
        {
            string meeting_id = itmReturn.getProperty("meeting_id", "");
            string site_count = itmReturn.getProperty("site_count", "");
            string site_code = itmReturn.getProperty("site_code", "");
            string site_mode = itmReturn.getProperty("site_mode", "");

            string sql = @"
                UPDATE
                    IN_MEETING
                SET
                    in_site_count = '{#site_count}'
                    , in_site_code = '{#site_code}'
                    , in_site_mode = '{#site_mode}'
                WHERE
                    id = '{#meeting_id}'
                ";

            sql = sql.Replace("{#meeting_id}", meeting_id)
                .Replace("{#site_count}", site_count)
                .Replace("{#site_code}", site_code)
                .Replace("{#site_mode}", site_mode);

            cfg.inn.applySQL(sql);
        }

        //查詢
        private void Query(TConfig cfg, Item itmReturn)
        {
            string meeting_id = itmReturn.getProperty("meeting_id", "");

            Item itmMeeting = GetMeeting(cfg);
            string in_site_mode = itmMeeting.getProperty("in_site_mode", "");

            itmReturn.setProperty("id", itmMeeting.getProperty("id", ""));
            itmReturn.setProperty("in_title", itmMeeting.getProperty("keyed_name", ""));
            itmReturn.setProperty("in_battle_type", itmMeeting.getProperty("in_battle_type", ""));
            itmReturn.setProperty("in_site_count", itmMeeting.getProperty("in_site_count", ""));
            itmReturn.setProperty("in_site_code", itmMeeting.getProperty("in_site_code", ""));
            itmReturn.setProperty("in_site_mode", itmMeeting.getProperty("in_site_mode", ""));

            if (in_site_mode == "")
            {
                Item itmPrograms = GetPrograms(cfg);

                int event_sum = 0;
                for (int i = 0; i < itmPrograms.getItemCount(); i++)
                {
                    Item itmProgram = itmPrograms.getItemByIndex(i);
                    string in_round_code = itmProgram.getProperty("in_round_code", "");

                    event_sum += GetIntVal(itmProgram.getProperty("event_count", "0"));

                    itmProgram.setType("inn_program");
                    itmProgram.setProperty("no", (i + 1).ToString());
                    itmProgram.setProperty("template", "In_Competition_Preview" + in_round_code + ".html");
                    itmReturn.addRelationship(itmProgram);

                }
                itmReturn.setProperty("event_sum", "總場次數量: " + event_sum.ToString("###,###"));

                itmReturn.setProperty("hide_program_table", "");
                itmReturn.setProperty("hide_team_table", "item_show_0");

            }
            else
            {
                Item itmTeams = GetTeams(cfg);
                Dictionary<string, Dictionary<string, Item>> dictionary = MapTeams(cfg, itmTeams);

                Item itmDetails = GetEventDetails(cfg);

                string last_event_id = "";

                for (int i = 0; i < itmDetails.getItemCount(); i++)
                {
                    Item itmDetail = itmDetails.getItemByIndex(i);

                    string program_id = itmDetail.getProperty("program_id", "");
                    string event_id = itmDetail.getProperty("event_id", "");
                    string program_display = itmDetail.getProperty("program_display", "");
                    string program_round_code = itmDetail.getProperty("program_round_code", "");
                    string program_team_count = itmDetail.getProperty("program_team_count", "");
                    string template = "In_Competition_Preview" + program_round_code + ".html";

                    string in_round = itmDetail.getProperty("in_round", "");
                    string in_round_code = itmDetail.getProperty("in_round_code", "");
                    string in_site_id = itmDetail.getProperty("in_site_id", "");

                    if (last_event_id == event_id)
                    {
                        continue;
                    }

                    last_event_id = event_id;


                    string in_sign_no = itmDetail.getProperty("in_sign_no", "");
                    string in_sign_foot = itmDetail.getProperty("in_sign_foot", "");

                    string in_target_no = itmDetail.getProperty("in_target_no", "");
                    string in_target_foot = in_sign_foot == "1" ? "2" : "1";

                    Item itmCurrent = GetTeam(cfg, dictionary, program_id, in_sign_no);
                    Item itmTarget = GetTeam(cfg, dictionary, program_id, in_target_no);

                    Item itmLeft = in_sign_foot == "1" ? itmCurrent : itmTarget;
                    Item itmRight = in_sign_foot == "2" ? itmCurrent : itmTarget;

                    Item itmEvent = cfg.inn.newItem();
                    itmEvent.setType("inn_event");
                    itmEvent.setProperty("no", (i + 1).ToString());
                    itmEvent.setProperty("in_meeting", meeting_id);
                    itmEvent.setProperty("template", template);
                    itmEvent.setProperty("program_id", program_id);
                    itmEvent.setProperty("program_display", program_display);
                    itmEvent.setProperty("program_team_count", program_team_count);
                    itmEvent.setProperty("program_round_code", program_round_code);

                    itmEvent.setProperty("in_round", in_round);
                    itmEvent.setProperty("in_round_code", in_round_code);
                    itmEvent.setProperty("in_site_id", in_site_id);

                    SetEventProperty(itmEvent, itmLeft, "left");
                    SetEventProperty(itmEvent, itmRight, "right");

                    itmReturn.addRelationship(itmEvent);
                }

                itmReturn.setProperty("hide_program_table", "item_show_0");
                itmReturn.setProperty("hide_team_table", "");

            }
        }

        private void SetEventProperty(Item itmEvent, Item item, string rl)
        {
            itmEvent.setProperty(rl + "_sign_no", item.getProperty("in_sign_no", ""));
            itmEvent.setProperty(rl + "_current_org", item.getProperty("in_current_org", ""));
            itmEvent.setProperty(rl + "_short_org", item.getProperty("in_short_org", ""));
            itmEvent.setProperty(rl + "_name", item.getProperty("in_name", ""));
            itmEvent.setProperty(rl + "_sno", item.getProperty("in_sno", ""));
        }

        //取得賽事資料
        private Item GetMeeting(TConfig cfg)
        {
            string aml = @"
                <AML>
                  <Item type='In_Meeting' action='get' id='{#meeting_id}' select='keyed_name,in_draw_status,in_battle_type,in_site_count,in_site_code,in_site_mode'>
                  </Item>
                </AML>
                ".Replace("{#meeting_id}", cfg.meeting_id);

            Item itmMeeting = cfg.inn.applyAML(aml);

            if (itmMeeting.isError())
            {
                throw new Exception("取得賽事資料發生錯誤");
            }

            return itmMeeting;
        }

        //取得賽事資料
        private Item GetPrograms(TConfig cfg)
        {
            string sql = @"
                SELECT
                    t1.*
                    , t2.event_count
                FROM
                    IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                LEFT OUTER JOIN
                    (
                        SELECT 
                            source_id
                            , count(*) AS 'event_count'
                        FROM
                            IN_MEETING_PEVENT WITH(NOLOCK)
                        GROUP BY
                            source_id
                    ) t2 ON t2.source_id = t1.id
                WHERE
                    t1.in_meeting = '{#meeting_id}'
                    AND ISNULL(in_program, '') = ''
                ORDER BY
                    t1.in_sort_order
                ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            return cfg.inn.applySQL(sql);
        }

        //取得賽次資料
        private Item GetEvents(TConfig cfg)
        {
            string sql = @"
                SELECT 
                    t1.id              AS 'program_id'
                    , t1.in_name       AS 'program_name'
                    , t1.in_display    AS 'program_display'
                    , t1.in_sort_order AS 'program_sort'
                    , t2.id            AS 'event_id'
                    , t2.in_tree_name
                    , t2.in_round_code
                    , t2.in_round_id
                FROM 
                    IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIn
                    IN_MEETING_PEVENT t2 WITH(NOLOCK)
                    ON t2.source_id = t1.id
                WHERE
                    t1.in_meeting = '{#meeting_id}'
                    AND t2.in_tree_name = 'main'
                    AND ISNULL(t2.in_bypass_foot, '') = ''
                ORDER BY
                    t2.in_tree_name
                    , t2.in_round_code desc
                    , t1.in_sort_order
                    , t2.in_round_id
                ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        //取得賽次資料
        private Item GetEventDetails(TConfig cfg)
        {
            string sql = @"
                SELECT 
                    t5.id               AS 'program_id'
                    , t5.in_display     AS 'program_display'
                    , t5.in_round_code  AS 'program_round_code'
                    , t5.in_team_count  AS 'program_team_count'
                    , t1.id             AS 'event_id'
                    , t1.in_tree_name
                    , t1.in_round_code
                    , t1.in_round_id
                    , t1.in_round
                    , t1.in_site_id
                    , t2.id          AS 'detail_id'
                    , t2.in_sign_no
                    , t2.in_sign_foot
                    , t2.in_target_no
                FROM 
                    IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
                    IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK)
                    ON t2.source_id = t1.id
                INNER JOIN
                    IN_MEETING_PROGRAM t5 WITH(NOLOCK)
                    ON t5.id = t1.source_id
                WHERE
                    t5.in_meeting = '{#meeting_id}'
                    AND t1.in_tree_name = 'main'
                    AND ISNULL(t1.in_bypass_foot, '') = ''
                ORDER BY
                    t1.in_tree_name
                    , t1.in_round_code desc
                    , t5.in_sort_order
                    , t1.in_round_id
                    , t2.in_sign_foot
                ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        private Item GetTeam(TConfig cfg, Dictionary<string, Dictionary<string, Item>> dictionary, string program_id, string in_sign_no)
        {
            Item itmResult = null;
            if (in_sign_no != "" && dictionary.ContainsKey(program_id))
            {
                Dictionary<string, Item> search = dictionary[program_id];
                if (search.ContainsKey(in_sign_no))
                {
                    itmResult = search[in_sign_no];
                }
            }
            if (itmResult == null)
            {
                itmResult = cfg.inn.newItem();
            }
            return itmResult;
        }

        //轉換賽事組別隊伍資料
        private Dictionary<string, Dictionary<string, Item>> MapTeams(TConfig cfg, Item items)
        {
            Dictionary<string, Dictionary<string, Item>> dictionary = new Dictionary<string, Dictionary<string, Item>>();

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string program_id = item.getProperty("program_id", "");
                string in_sign_no = item.getProperty("in_sign_no", "");
                if (in_sign_no == "")
                {
                    continue;
                }

                Dictionary<string, Item> search = null;
                if (dictionary.ContainsKey(program_id))
                {
                    search = dictionary[program_id];
                }
                else
                {
                    search = new Dictionary<string, Item>();
                    dictionary.Add(program_id, search);
                }

                if (search.ContainsKey(in_sign_no))
                {
                    cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "籤號重複: " + item.dom.InnerXml);
                }
                else
                {
                    search.Add(in_sign_no, item);
                }
            }

            return dictionary;
        }

        //取得賽事組別隊伍資料
        private Item GetTeams(TConfig cfg)
        {
            string sql = @"
                SELECT 
                    t2.id            AS 'program_id'
                    , t2.in_display  AS 'program_display'
                    , t1.*
                FROM 
                    IN_MEETING_PTEAM t1
                INNER JOIN
                    IN_MEETING_PROGRAM t2
                    ON t2.id = t1.source_id
                WHERE
                    t2.in_meeting = '{#meeting_id}'
                ORDER BY
                    t2.in_sort_order
                    , t1.in_sign_no
                ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);
            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);
            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// 取得場地編碼陣列
        /// </summary>
        private static string[] GetSiteCodeArray(int site_count, string site_code)
        {
            bool is_english = site_code.Contains("A");

            string[] result = new string[site_count];

            for (int i = 1; i <= site_count; i++)
            {
                if (is_english)
                {
                    result[i - 1] = ((char)(i + 64)).ToString();
                }
                else
                {
                    result[i - 1] = i.ToString();
                }
            }

            return result;
        }

        private string GetMeetingVariable(TConfig cfg, string in_key)
        {
            var sql = "SELECT id, in_value FROM IN_MEETING_VARIABLE WITH(NOLOCK)"
                + " WHERE source_id = '" + cfg.meeting_id + "'"
                + " AND in_key = '" + in_key + "'";

            var itmVariable = cfg.inn.applySQL(sql);
            if (itmVariable.isError() || itmVariable.getResult() == "") return "";
            return itmVariable.getProperty("in_value", "");
        }

        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string meeting_id { get; set; }
            public string mode { get; set; }
        }

        private int GetIntVal(string value, int def = 0)
        {
            int result = def;
            Int32.TryParse(value, out result);
            return result;
        }
    }
}