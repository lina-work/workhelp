﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Wrestling.場地分配
{
    public class in_meeting_allocation : Item
    {
        public in_meeting_allocation(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
    目的: 場地分配
    輸入: meeting_id
    日期: 
        2020-11-30: 創建 (lina)
*/

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]in_meeting_allocation";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "input dom: " + itmR.dom.InnerXml);

            string meeting_id = itmR.getProperty("meeting_id", "");
            string mode = itmR.getProperty("mode", "");
            string scene = itmR.getProperty("scene", "");
            string link = itmR.getProperty("link", "");
            string menu = itmR.getProperty("menu", "");

            if (meeting_id == "")
            {
                itmR.setProperty("error_message", "賽事 id 不得為空白");
                return itmR;
            }

            if (menu == "no")
            {
                itmR.setProperty("hide_menu", "item_show_0");
            }

            //檢查頁面權限
            Item itmPermit = inn.applyMethod("In_CheckIdentity", "<method>" + strMethodName + "</method><code>" + "ALL" + "</code>");
            bool isMeetingAdmin = itmPermit.getProperty("isMeetingAdmin", "") == "1";
            bool isMeetingLeader = itmPermit.getProperty("isMeetingLeader", "") == "1";

            bool can_view = isMeetingAdmin || isMeetingLeader;
            if (!can_view)
            {
                itmR.setProperty("error_message", "您無權限瀏覽此頁面");
                return itmR;
            }

            switch (mode)
            {
                case "edit":
                    Query(CCO, strMethodName, inn, itmR, isEdit: true, isUpload: false, linkWeight: link == "weight");
                    AppendDateMenu(CCO, strMethodName, inn, itmR);
                    break;

                case "save":
                    Save(CCO, strMethodName, inn, itmR);
                    break;

                case "remove":
                    Remove(CCO, strMethodName, inn, itmR);
                    break;

                case "removeDate":
                    RemoveDate(CCO, strMethodName, inn, itmR);
                    break;

                case "export":
                    Export(CCO, strMethodName, inn, itmR);
                    break;

                case "allocate":
                    Allocate(CCO, strMethodName, inn, itmR);
                    break;

                case "fix_judo_site": //編制
                    FixTreeNo(CCO, strMethodName, inn, itmR);
                    break;

                default:
                    Query(CCO, strMethodName, inn, itmR, isEdit: false, isUpload: scene == "upload", linkWeight: link == "weight");
                    AppendDateMenu(CCO, strMethodName, inn, itmR);
                    break;
            }

            return itmR;
        }

        //附加日期選單
        private void AppendDateMenu(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            string meeting_id = itmReturn.getProperty("meeting_id", "");

            Item itmEmpty = inn.newItem();
            itmEmpty.setType("inn_date");
            itmEmpty.setProperty("value", "");
            itmEmpty.setProperty("label", "請選擇");
            itmReturn.addRelationship(itmEmpty);

            string sql = "SELECT DISTINCT in_date_key FROM IN_MEETING_ALLOCATION WITH(NOLOCK) WHERE in_meeting = '{#meeting_id}' ORDER BY in_date_key";
            sql = sql.Replace("{#meeting_id}", meeting_id);
            Item items = inn.applySQL(sql);
            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_date_key = item.getProperty("in_date_key", "");

                item.setType("inn_date");
                item.setProperty("value", in_date_key);
                item.setProperty("label", in_date_key);
                itmReturn.addRelationship(item);
            }
        }

        //編制
        private void FixTreeNo(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            string meeting_id = itmReturn.getProperty("meeting_id", "");
            string in_date_source = itmReturn.getProperty("in_date", "");
            string in_date_key = GetDateTimeVal(in_date_source, "yyyy-MM-dd");
            string in_sort = itmReturn.getProperty("in_sort", "");
            string is_rebuild_events = itmReturn.getProperty("is_rebuild_events", "");

            if (meeting_id == "") throw new Exception("賽事 id 不得為空白");
            if (in_date_source == "") throw new Exception("比賽日期 不得為空白");
            if (in_sort == "") throw new Exception("排列方式 不得為空白");

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                strMethodName = strMethodName,
                inn = inn,
                meeting_id = meeting_id,
                in_date_key = in_date_key,
                in_sort = in_sort,
            };

            //取得賽事資料
            cfg.itmMeeting = inn.applySQL("SELECT id, in_battle_type, in_robin_player FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            cfg.mt_battle_type = cfg.itmMeeting.getProperty("in_battle_type", "");
            cfg.is_challenge = cfg.mt_battle_type == "Challenge";
            cfg.robin_player = GetIntVal(cfg.itmMeeting.getProperty("in_robin_player", "0"));

            //分析該日全部組別的賽制(EX: 中正盃  團體賽: 單淘、個人賽: 四柱復活賽)
            cfg.day_battle_type = GetDayBattleType(cfg, cfg.mt_battle_type);

            //更新組別-場地場次序
            FixProgramMatNo(cfg);

            //重設場次編號(每個組別 reset tree_no)
            Item itmData = inn.newItem("In_Meeting_Program");
            itmData.setProperty("meeting_id", cfg.meeting_id);
            itmData.setProperty("program_id", "");
            itmData.setProperty("in_date", in_date_key);
            itmData.apply("in_meeting_pevent_reset");

            //重算獎牌戰類型、賽別時程、出賽人數
            itmData.apply("in_meeting_program_medal");

            string sql_update = "";

            //清空特殊場次編號
            sql_update = "UPDATE IN_MEETING_PEVENT SET in_tree_no = NULL"
                + " WHERE in_meeting = '" + cfg.meeting_id + "'"
                + " AND in_date_key = '" + in_date_key + "'"
                + " AND in_tree_name IN ('rank34', 'rank56', 'rank78', 'sub')";
            inn.applySQL(sql_update);

            //清空敗部決賽場次編號
            sql_update = "UPDATE IN_MEETING_PEVENT SET in_tree_no = NULL"
                + " WHERE in_meeting = '" + cfg.meeting_id + "'"
                + " AND in_date_key = '" + in_date_key + "'"
                + " AND in_tree_name = 'repechage' AND in_round_code = 2";
            inn.applySQL(sql_update);

            //紀錄初始 TreeNo
            sql_update = "UPDATE IN_MEETING_PEVENT SET in_tree_sno = in_tree_no"
                + " WHERE in_meeting = '" + cfg.meeting_id + "'"
                + " AND in_date_key = '" + in_date_key + "'"
                + " AND ISNULL(in_tree_sno, 0) = 0";
            inn.applySQL(sql_update);

            //取得場地資料
            Item itmSites = inn.applySQL("SELECT * FROM IN_MEETING_SITE WITH(NOLOCK)"
                + " WHERE in_meeting = '" + cfg.meeting_id + "' ORDER BY in_code");

            switch (in_sort)
            {
                case "allocate08"://指定場地模式 該組打完  8強 再換下一組
                case "allocate16"://指定場地模式 該組打完 16強 再換下一組
                case "allocateST"://指定場地模式 該組打完再換下一組
                    AllocateTreeNo(cfg, itmSites);
                    break;

                case "cycle"://不分場地模式
                    CycyleTreeNo(cfg, itmSites);
                    break;
            }

            //清除暫存資訊(三階選單)
            ClearCache(cfg, itmReturn);

            //儲存排列模式
            UpdMeetingVariable(cfg, in_sort);
        }

        //儲存排列模式
        private void UpdMeetingVariable(TConfig cfg, string in_sort)
        {
            Item itmData = cfg.inn.newItem();
            itmData.setType("In_Meeting_Variable");
            itmData.setProperty("meeting_id", cfg.meeting_id);
            itmData.setProperty("scene", "update");
            itmData.setProperty("name", "allocate_mode");
            itmData.setProperty("value", in_sort);
            itmData.apply("In_Meeting_Variable");
        }

        //清除暫存資訊(三階選單)
        private void ClearCache(TConfig cfg, Item itmReturn)
        {
            Item itmData = cfg.inn.newItem();
            itmData.setType("In_Meeting");
            itmData.setProperty("meeting_id", cfg.meeting_id);
            itmData.setProperty("scene", "clear");
            itmData.apply("in_meeting_program_options");

            Item itmData2 = cfg.inn.newItem();
            itmData2.setType("In_Meeting");
            itmData2.setProperty("meeting_id", cfg.meeting_id);
            itmData2.setProperty("scene", "clear");
            itmData2.apply("in_meeting_day_options");
        }

        private string GetDayBattleType(TConfig cfg, string mt_battle_type)
        {
            string result = "";

            string sql = @"
                 SELECT
					DISTINCT t1.in_battle_type AS 'battle_type' 
                 FROM
                    IN_MEETING_PROGRAM t1
                 INNER JOIN
             	    IN_MEETING_ALLOCATION t2
             	    ON t2.in_program = t1.id
                 WHERE
                    t1.in_meeting = '{#meeting_id}'
             	    AND t2.in_date_key = '{#in_date}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_date}", cfg.in_date_key);

            Item items = cfg.inn.applySQL(sql);
            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string battle_type = item.getProperty("battle_type", "");
                if (battle_type.Contains("Robin"))
                {
                    continue;
                }
                else
                {
                    result = battle_type;
                }
            }

            if (result == "")
            {
                result = mt_battle_type;
            }

            return result;
        }

        //修正組別場地場次序
        private void FixProgramMatNo(TConfig cfg)
        {
            string sql = @"
                UPDATE t1 SET
                	t1.in_site_mat = in_code_en + '-' + CAST(rn AS VARCHAR)
                	, t1.in_site_mat2 = in_code_en + '-' + RIGHT(REPLICATE('0', 2) + CAST(rn as VARCHAR), 2)
                FROM
                	IN_MEETING_PROGRAM t1
                INNER JOIN
                (
                	SELECT
                		t11.in_program
                		, t12.in_code_en
                		, ROW_NUMBER() OVER (PARTITION BY t12.in_code ORDER BY t11.created_on) AS 'rn'
                	FROM
                		IN_MEETING_ALLOCATION t11 WITH(NOLOCK)
                	INNER JOIN
                		IN_MEETING_SITE t12 WITH(NOLOCK)
                		ON t12.id = t11.in_site
                	WHERE
                		t11.in_meeting = '{#meeting_id}'
                		AND t11.in_date_key = '{#in_date_key}'
                ) t2 ON t2.in_program = t1.id
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_date_key}", cfg.in_date_key);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item itmSQL = cfg.inn.applySQL(sql);

            sql = @"UPDATE t1 SET 
            	t1.in_fight_day = t2.in_date_key
            	, t1.in_fight_site = t2.in_place
            	, t1.in_site = t2.in_site
            FROM
            	IN_MEETING_PROGRAM t1
            INNER JOIN
            	IN_MEETING_ALLOCATION t2
            	ON t2.in_program = t1.id
            WHERE
            	t2.in_meeting = '{#meeting_id}'
            	AND t2.in_date_key = '{#in_date_key}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_date_key}", cfg.in_date_key);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            itmSQL = cfg.inn.applySQL(sql);

        }

        #region 不分場地模式

        //不分場地模式
        private void CycyleTreeNo(TConfig cfg, Item itmSites)
        {
            //int site_count = itmSites.getItemCount();

            ////場地清單
            //string[] site_codes = GetSiteCodeArray(site_count, "");

            //Item items = GetEvents(cfg.CCO, cfg.strMethodName, cfg.inn, cfg.meeting_id, cfg.in_date_key);

            //string sql = @"
            //    UPDATE
            //        IN_MEETING_PEVENT
            //    SET
            //        in_site_code = '{#in_site_code}'
            //        , in_site_no = '{#in_site_no}'
            //        , in_site_id = '{#in_site_id}'
            //        , in_site_allocate = '1'
            //    WHERE
            //        id = '{#event_id}'
            //    ";

            //int count = items.getItemCount();
            //int index = 0;

            //for (int i = 0; i < count; i++)
            //{
            //    if (i % site_count == 0)
            //    {
            //        index++;
            //    }

            //    Item item = items.getItemByIndex(i);
            //    string event_id = item.getProperty("event_id", "");
            //    string in_site_code = site_codes[i % site_count];
            //    string in_site_no = index.ToString();
            //    string in_site_id = in_site_code + in_site_no.PadLeft(3, '0');

            //    string sql_update = sql.Replace("{#event_id}", event_id)
            //        .Replace("{#in_site_code}", in_site_code)
            //        .Replace("{#in_site_no}", in_site_no)
            //        .Replace("{#in_site_id}", in_site_id);

            //    //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql_update);

            //    Item itmSQL = inn.applySQL(sql_update);

            //    if (itmSQL.isError())
            //    {
            //        CCO.Utilities.WriteDebug(strMethodName, "[不分場地模式]配置場地編號發生錯誤 sql: " + sql_update);
            //    }
            //}
        }

        //取得賽次資料
        private Item GetEvents(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string meeting_id, string in_date_key)
        {
            string sql = @"
                SELECT 
                    t1.id              AS 'program_id'
                    , t1.in_name       AS 'program_name'
                    , t1.in_display    AS 'program_display'
                    , t1.in_sort_order AS 'program_sort'
                    , t2.id            AS 'event_id'
                    , t2.in_tree_name
                    , t2.in_round_code
                    , t2.in_round_id
                FROM 
                    IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIn
                    IN_MEETING_PEVENT t2 WITH(NOLOCK)
                    ON t2.source_id = t1.id
                WHERE
                    t1.in_meeting = '{#meeting_id}'
	                AND t2.in_date_key = '{#in_date_key}'
                    AND t2.in_tree_name = 'main'
                    AND ISNULL(t2.in_bypass_foot, '') = ''
                ORDER BY
                    t2.in_tree_name
                    , t2.in_round_code desc
                    , t1.in_sort_order
                    , t2.in_round_id
                ";

            sql = sql.Replace("{#meeting_id}", meeting_id)
                .Replace("{#in_date_key}", in_date_key);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return inn.applySQL(sql);
        }

        /// <summary>
        /// 取得場地編碼陣列
        /// </summary>
        private static string[] GetSiteCodeArray(int site_count, string site_code)
        {
            bool is_english = site_code.Contains("A");

            string[] result = new string[site_count];

            for (int i = 1; i <= site_count; i++)
            {
                if (is_english)
                {
                    result[i - 1] = ((char)(i + 64)).ToString();
                }
                else
                {
                    result[i - 1] = i.ToString();
                }
            }

            return result;
        }
        #endregion 不分場地模式

        #region 指定場地模式

        //指定場地模式
        private void AllocateTreeNo(TConfig cfg, Item itmSites)
        {
            //更新場次的比賽日期與場地 (如果是自動配發場地 一個量級可能分散在多個場地)
            UpdateDateAndSite(cfg.CCO, cfg.strMethodName, cfg.inn, cfg.meeting_id, cfg.in_date_key);

            int site_count = itmSites.getItemCount();

            for (int i = 0; i < site_count; i++)
            {
                Item itmSite = itmSites.getItemByIndex(i);

                switch (cfg.in_sort)
                {
                    case "allocate08"://打到8強換組
                        UpdateMRTreeNo_08(cfg, itmSite);
                        break;

                    case "allocate16"://打到16強換組
                        UpdateMRTreeNo_16(cfg, itmSite);
                        break;

                    case "allocateST"://該組打完再換下一組
                        UpdateMRTreeNo_ST(cfg, itmSite);
                        break;

                    case "allocate99"://先打人多的
                        UpdateMRTreeNo_99(cfg, itmSite);
                        break;
                }

                if (cfg.is_challenge)
                {
                    //挑戰賽
                    UpdateCTreeNo(cfg, itmSite);
                }
            }
        }

        //打到8強換組
        private void UpdateMRTreeNo_08(TConfig cfg, Item itmSite)
        {
            string site_id = itmSite.getProperty("id", "");

            List<Item> list = new List<Item>();

            //勝部 8 強以前
            AppendList(list, GetSiteEvents(cfg, EventFilterEnum.main_pre_semi, site_id));
            //循環賽第 1 回合
            AppendList(list, GetSiteRobinEvents(cfg, site_id, round: "1"));
            //勝部 4 強
            AppendList(list, GetSiteEvents(cfg, EventFilterEnum.main_semi, site_id));
            //循環賽第 2 回合
            AppendList(list, GetSiteRobinEvents(cfg, site_id, round: "2"));

            //敗部 128 強
            AppendList(list, GetSiteEvents(cfg, EventFilterEnum.rpc_128, site_id));
            //敗部 64 強
            AppendList(list, GetSiteEvents(cfg, EventFilterEnum.rpc_64, site_id));
            //敗部 32 強
            AppendList(list, GetSiteEvents(cfg, EventFilterEnum.rpc_32, site_id));
            //敗部 16 強
            AppendList(list, GetSiteEvents(cfg, EventFilterEnum.rpc_16, site_id));
            //敗部 8 強
            AppendList(list, GetSiteEvents(cfg, EventFilterEnum.rpc_8, site_id));
            //循環賽第 3 回合
            AppendList(list, GetSiteRobinEvents(cfg, site_id, round: "3"));
            //循環賽第 4 回合
            AppendList(list, GetSiteRobinEvents(cfg, site_id, round: "4"));

            if (cfg.is_challenge)
            {
                //七八名
                AppendList(list, GetSiteEvents(cfg, EventFilterEnum.rank78, site_id));
                //循環賽第 5 回合
                AppendList(list, GetSiteRobinEvents(cfg, site_id, round: "5"));
                //敗部 4 強(銅牌戰)
                AppendList(list, GetSiteEvents(cfg, EventFilterEnum.rpc_4, site_id));
                //三四名
                AppendList(list, GetSiteEvents34(cfg, EventFilterEnum.rank34, site_id));
                //五六名(如果出賽人數 = 7，等三四名打完)
                AppendList(list, GetSiteEvents(cfg, EventFilterEnum.rank56, site_id));
                //勝部決賽(金牌戰)
                AppendList(list, GetSiteEvents(cfg, EventFilterEnum.main_final, site_id));

                //此處不排後續挑戰賽與三戰兩勝
            }
            else
            {
                //七八名
                AppendList(list, GetSiteEvents(cfg, EventFilterEnum.rank78, site_id));
                //循環賽第 5 回合
                AppendList(list, GetSiteRobinEvents(cfg, site_id, round: "5"));
                //敗部 4 強(非銅牌戰，如果人數 = 5)
                AppendList(list, GetSiteEvents(cfg, EventFilterEnum.rpc_4_fake, site_id));
                //三戰兩勝-第 1 場
                AppendList(list, GetSiteRobinEvents(cfg, site_id, "11"));
                //敗部 4 強(真銅牌戰，如果人數 <> 5)
                AppendList(list, GetSiteEvents(cfg, EventFilterEnum.rpc_4_true, site_id));
                //三四名(如果人數 = 5)
                AppendList(list, GetSiteEvents(cfg, EventFilterEnum.rpc_final, site_id));
                //五六名(如果人數 = 7，等三四名打完)
                AppendList(list, GetSiteEvents(cfg, EventFilterEnum.rank56, site_id));
                //三戰兩勝-第 2 場
                AppendList(list, GetSiteRobinEvents(cfg, site_id, "12"));
                //勝部決賽(金牌戰)
                AppendList(list, GetSiteEvents(cfg, EventFilterEnum.main_final, site_id));
                //三戰兩勝-第 3 場
                AppendList(list, GetSiteRobinEvents(cfg, site_id, "13"));
            }


            //全部重編
            ResetTreeNo(cfg, list);
            //單淘不分預賽決賽
            ResetPeriod(cfg);
            //子場次全部重編
            ResetSubTreeNo(cfg);
            //子場次量級重設
            ResetSubSects(cfg);
        }

        //打到16強換組
        private void UpdateMRTreeNo_16(TConfig cfg, Item itmSite)
        {
            string site_id = itmSite.getProperty("id", "");

            List<Item> list = new List<Item>();

            //勝部 16 強以前(含16強)
            AppendList(list, GetSiteEvents(cfg, EventFilterEnum.main_pre_quarterfinals, site_id));
            //勝部 8 強
            AppendList(list, GetSiteEvents(cfg, EventFilterEnum.main_r008, site_id));
            //循環賽第 1 回合
            AppendList(list, GetSiteRobinEvents(cfg, site_id, round: "1"));
            //勝部 4 強
            AppendList(list, GetSiteEvents(cfg, EventFilterEnum.main_semi, site_id));
            //循環賽第 2 回合
            AppendList(list, GetSiteRobinEvents(cfg, site_id, round: "2"));

            //敗部 128 強
            AppendList(list, GetSiteEvents(cfg, EventFilterEnum.rpc_128, site_id));
            //敗部 64 強
            AppendList(list, GetSiteEvents(cfg, EventFilterEnum.rpc_64, site_id));
            //敗部 32 強
            AppendList(list, GetSiteEvents(cfg, EventFilterEnum.rpc_32, site_id));
            //敗部 16 強
            AppendList(list, GetSiteEvents(cfg, EventFilterEnum.rpc_16, site_id));
            //敗部 8 強
            AppendList(list, GetSiteEvents(cfg, EventFilterEnum.rpc_8, site_id));
            //循環賽第 3 回合
            AppendList(list, GetSiteRobinEvents(cfg, site_id, round: "3"));
            //循環賽第 4 回合
            AppendList(list, GetSiteRobinEvents(cfg, site_id, round: "4"));

            if (cfg.is_challenge)
            {
                //七八名
                AppendList(list, GetSiteEvents(cfg, EventFilterEnum.rank78, site_id));
                //循環賽第 5 回合
                AppendList(list, GetSiteRobinEvents(cfg, site_id, round: "5"));
                //敗部 4 強(銅牌戰)
                AppendList(list, GetSiteEvents(cfg, EventFilterEnum.rpc_4, site_id));
                //三四名
                AppendList(list, GetSiteEvents34(cfg, EventFilterEnum.rank34, site_id));
                //五六名(如果出賽人數 = 7，等三四名打完)
                AppendList(list, GetSiteEvents(cfg, EventFilterEnum.rank56, site_id));
                //勝部決賽(金牌戰)
                AppendList(list, GetSiteEvents(cfg, EventFilterEnum.main_final, site_id));

                //此處不排後續挑戰賽與三戰兩勝
            }
            else
            {
                //七八名
                AppendList(list, GetSiteEvents(cfg, EventFilterEnum.rank78, site_id));
                //循環賽第 5 回合
                AppendList(list, GetSiteRobinEvents(cfg, site_id, round: "5"));
                //敗部 4 強(非銅牌戰，如果人數 = 5)
                AppendList(list, GetSiteEvents(cfg, EventFilterEnum.rpc_4_fake, site_id));
                //三戰兩勝-第 1 場
                AppendList(list, GetSiteRobinEvents(cfg, site_id, "11"));
                //敗部 4 強(真銅牌戰，如果人數 <> 5)
                AppendList(list, GetSiteEvents(cfg, EventFilterEnum.rpc_4_true, site_id));
                //三四名(如果人數 = 5)
                AppendList(list, GetSiteEvents(cfg, EventFilterEnum.rpc_final, site_id));
                //五六名(如果人數 = 7，等三四名打完)
                AppendList(list, GetSiteEvents(cfg, EventFilterEnum.rank56, site_id));
                //三戰兩勝-第 2 場
                AppendList(list, GetSiteRobinEvents(cfg, site_id, "12"));
                //勝部決賽(金牌戰)
                AppendList(list, GetSiteEvents(cfg, EventFilterEnum.main_final, site_id));
                //三戰兩勝-第 3 場
                AppendList(list, GetSiteRobinEvents(cfg, site_id, "13"));
            }


            //全部重編
            ResetTreeNo(cfg, list);
            //單淘不分預賽決賽
            ResetPeriod(cfg);
            //子場次全部重編
            ResetSubTreeNo(cfg);
            //子場次量級重設
            ResetSubSects(cfg);
        }

        //該組打完再換下一組(單淘-全柔錦)
        private void UpdateMRTreeNo_ST(TConfig cfg, Item itmSite)
        {
            string site_id = itmSite.getProperty("id", "");

            List<Item> list = new List<Item>();

            AppendList(list, GetSiteEventsST(cfg, site_id));

            //全部重編
            ResetTreeNo(cfg, list);
            //單淘不分預賽決賽
            ResetPeriod(cfg);
            //子場次全部重編
            ResetSubTreeNo(cfg);
            //子場次量級重設
            ResetSubSects(cfg);
        }

        //人多先打
        private void UpdateMRTreeNo_99(TConfig cfg, Item itmSite)
        {
            string site_id = itmSite.getProperty("id", "");

            List<Item> list = new List<Item>();

            //勝部 128 人
            AppendList(list, GetSiteEvents(cfg, EventFilterEnum.main_r128, site_id));
            //勝部 64 人
            AppendList(list, GetSiteEvents(cfg, EventFilterEnum.main_r064, site_id));
            //勝部 32 人
            AppendList(list, GetSiteEvents(cfg, EventFilterEnum.main_r032, site_id));
            //勝部 16 強
            AppendList(list, GetSiteEvents(cfg, EventFilterEnum.main_r016, site_id));
            //勝部 8 強
            AppendList(list, GetSiteEvents(cfg, EventFilterEnum.main_r008, site_id));
            //循環賽第 1 回合
            AppendList(list, GetSiteRobinEvents(cfg, site_id, round: "1"));
            //敗部 128 強
            AppendList(list, GetSiteEvents(cfg, EventFilterEnum.rpc_128, site_id));
            //敗部 64 強
            AppendList(list, GetSiteEvents(cfg, EventFilterEnum.rpc_64, site_id));
            //敗部 32 強
            AppendList(list, GetSiteEvents(cfg, EventFilterEnum.rpc_32, site_id));
            //敗部 16 強
            AppendList(list, GetSiteEvents(cfg, EventFilterEnum.rpc_16, site_id));

            //循環賽第 2 回合
            AppendList(list, GetSiteRobinEvents(cfg, site_id, round: "2"));
            //準決賽: 勝部 > 敗部，下一組
            AppendList(list, GetSiteEvents3(cfg, EventFilterEnum.rpc_16, site_id));
            //循環賽第 3 回合
            AppendList(list, GetSiteRobinEvents(cfg, site_id, round: "3"));
            //循環賽第 4 回合
            AppendList(list, GetSiteRobinEvents(cfg, site_id, round: "4"));
            //循環賽第 5 回合
            AppendList(list, GetSiteRobinEvents(cfg, site_id, round: "5"));

            //三戰兩勝-第 1 場
            AppendList(list, GetSiteRobinEvents(cfg, site_id, "11"));
            //敗部 4 強
            AppendList(list, GetSiteEvents(cfg, EventFilterEnum.rpc_4, site_id));

            //敗部決賽
            AppendList(list, GetSiteEvents(cfg, EventFilterEnum.rpc_final, site_id));

            //三戰兩勝-第 2 場
            AppendList(list, GetSiteRobinEvents(cfg, site_id, "12"));
            //勝部決賽
            AppendList(list, GetSiteEvents(cfg, EventFilterEnum.main_final, site_id));

            //三戰兩勝-第 3 場
            AppendList(list, GetSiteRobinEvents(cfg, site_id, "13"));

            //挑戰賽要打三四名
            if (cfg.is_challenge)
            {
                //三四名
                AppendList(list, GetSiteEvents34(cfg, EventFilterEnum.rank34, site_id));
            }
            else
            {
                //五六名
                AppendList(list, GetSiteEvents(cfg, EventFilterEnum.rank56, site_id));
            }

            //全部重編
            ResetTreeNo(cfg, list);
            //單淘不分預賽決賽
            ResetPeriod(cfg);
            //子場次全部重編
            ResetSubTreeNo(cfg);
            //子場次量級重設
            ResetSubSects(cfg);
        }

        //全部重編
        private void ResetTreeNo(TConfig cfg, List<Item> list)
        {
            for (int i = 0; i < list.Count; i++)
            {
                int no = i + 1;

                Item item = list[i];
                string event_id = item.getProperty("event_id", "");
                string in_tree_no = no.ToString();

                string sql = "UPDATE IN_MEETING_PEVENT SET"
                    + " in_site_allocate = '1'"
                    + " , in_tree_no = '" + in_tree_no + "'"
                    + " WHERE id = '" + event_id + "'";

                Item itmSQL = cfg.inn.applySQL(sql);

                if (itmSQL.isError()) throw new Exception("error");
            }
        }

        //子場次量級重設
        private void ResetSubSects(TConfig cfg)
        {
            string sql = "";
            Item itmSQL = null;

            sql = @"
                UPDATE t1 SET
                	t1.in_sub_sect = t2.in_name
                FROM
                	IN_MEETING_PEVENT t1
                INNER JOIN
                	IN_MEETING_PSECT t2
                	ON t2.IN_PROGRAM = t1.source_id
                	AND t1.in_tree_name = 'sub'
                	AND t2.in_sub_id = t1.in_sub_id
                WHERE
                	t1.in_meeting = '{#meeting_id}'
                	AND t1.in_date_key = '{#in_date_key}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_date_key}", cfg.in_date_key);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            itmSQL = cfg.inn.applySQL(sql);
        }

        //單淘不分預賽決賽
        private void ResetPeriod(TConfig cfg)
        {
            string mt_battle_type = cfg.itmMeeting.getProperty("in_battle_type", "");
            if (mt_battle_type != "TopTwo")
            {
                return;
            }

            string sql = "";
            Item itmSQL = null;

            sql = @"
                UPDATE IN_MEETING_PEVENT SET
	                in_period = 1
                WHERE
	                in_meeting = '{#meeting_id}'
	                AND in_date_key = '{#in_date_key}'
	                AND ISNULL(in_period, 0) <> 1
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_date_key}", cfg.in_date_key);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            itmSQL = cfg.inn.applySQL(sql);
        }

        //子場次全部重編
        private void ResetSubTreeNo(TConfig cfg)
        {
            string sql = "";
            Item itmSQL = null;

            sql = @"
                UPDATE t1 SET
	                t1.in_tree_no = t2.in_tree_no * 100 + t1.in_sub_id
	                , t1.in_period = t2.in_period
	                , t1.in_medal = t2.in_medal
                FROM 
	                IN_MEETING_PEVENT t1 WITH(NOLOCK) 
                INNER JOIN
	                IN_MEETING_PEVENT t2 WITH(NOLOCK) 
	                ON t2.id = t1.in_parent
                WHERE
	                t1.in_meeting = '{#meeting_id}'
	                AND t1.in_tree_name = 'sub'
	                AND t1.in_date_key = '{#in_date_key}'
	                AND ISNULL(t2.in_tree_no, 0) <> 0
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_date_key}", cfg.in_date_key);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            itmSQL = cfg.inn.applySQL(sql);
        }

        //批次更新場地與比賽日期
        private void UpdateDateAndSite(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string meeting_id, string in_date_key)
        {
            string sql = @"
                 UPDATE t1 SET
             	    t1.in_date_key = t3.in_date_key
             	    , t1.in_site = t4.id
             	    , t1.in_site_code = t4.in_code
                 FROM
             	    IN_MEETING_PEVENT t1
                 INNER JOIN
                    IN_MEETING_PROGRAM t2
                    ON t2.id = t1.source_id
                 INNER JOIN
             	    IN_MEETING_ALLOCATION t3
             	    ON t3.in_program = t2.id
                 INNER JOIN
             	    IN_MEETING_SITE t4
             	    ON t4.id = t3.in_site
                 WHERE
                    t2.in_meeting = '{#meeting_id}'
             	    AND t3.in_date_key = '{#in_date_key}'
            ";

            sql = sql.Replace("{#meeting_id}", meeting_id)
                .Replace("{#in_date_key}", in_date_key);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item itmSQL = inn.applySQL(sql);
        }

        //更新場地序號(挑戰賽)
        private void UpdateCTreeNo(TConfig cfg, Item itmSite)
        {
            string site_id = itmSite.getProperty("id", "");

            List<Item> list = new List<Item>();

            //三戰兩勝-第 1 場
            AppendList(list, GetSiteRobinEvents(cfg, site_id, "11"));
            //挑戰賽 R1
            AppendList(list, GetSiteEvents(cfg, EventFilterEnum.clg_a1, site_id));
            //三戰兩勝-第 2 場
            AppendList(list, GetSiteRobinEvents(cfg, site_id, "12"));
            //挑戰賽 R2
            AppendList(list, GetSiteEvents(cfg, EventFilterEnum.clg_a2, site_id));
            //三戰兩勝-第 2 場
            AppendList(list, GetSiteRobinEvents(cfg, site_id, "13"));
            //挑戰賽 R3
            AppendList(list, GetSiteEvents(cfg, EventFilterEnum.clg_b1, site_id));

            //盟主賽 R1
            AppendList(list, GetSiteEventsByTid(cfg, site_id, "KA01"));
            //盟主賽 R2
            AppendList(list, GetSiteEventsByTid(cfg, site_id, "KB01"));

            string sql = @"
                SELECT 
                	MAX(in_tree_no) AS 'max_tree_no'
                FROM 
                	IN_MEETING_PEVENT WITH(NOLOCK)
                WHERE 
                	in_meeting = '{#meeting_id}' 
                	AND in_site = '{#site_id}'
                	AND in_date_key = '{#in_date_key}'
	                AND in_tree_id NOT IN ('CA01', 'CA02', 'CB01', 'rank56', 'KA01', 'KB01')
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#site_id}", site_id)
                .Replace("{#in_date_key}", cfg.in_date_key);

            int max_tree_no = 0;
            Item itmTreeNo = cfg.inn.applySQL(sql);
            if (!itmTreeNo.isError() && itmTreeNo.getResult() != "")
            {
                string tree_no = itmTreeNo.getProperty("max_tree_no", "0");
                max_tree_no = GetIntVal(tree_no);
            }

            for (int i = 0; i < list.Count; i++)
            {
                int no = i + 1 + max_tree_no;

                Item item = list[i];
                string event_id = item.getProperty("event_id", "");
                string in_site_code = item.getProperty("site_code", "");
                string in_site_no = no.ToString();
                string in_site_id = in_site_code + in_site_no.PadLeft(3, '3');

                sql = "UPDATE IN_MEETING_PEVENT SET"
                    + " in_site_allocate = '1'"
                    + " , in_site_no = '" + in_site_no + "'"
                    + " , in_site_id = '" + in_site_id + "'"
                    + " , in_tree_no = '" + in_site_no + "'"
                    + " WHERE id = '" + event_id + "'";

                Item itmSQL = cfg.inn.applySQL(sql);

                if (itmSQL.isError()) throw new Exception("error");
            }
        }

        #endregion 指定場地模式

        private void AppendList(List<Item> list, Item items)
        {
            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                list.Add(item);
            }
        }

        private enum EventFilterEnum
        {
            /// <summary>
            /// 勝部-16強賽以前
            /// </summary>
            main_pre_quarterfinals = 5,
            /// <summary>
            /// 勝部-8強賽以前
            /// </summary>
            main_pre_semi = 10,
            /// <summary>
            /// 勝部-準決賽(4強)
            /// </summary>
            main_semi = 20,
            /// <summary>
            /// 勝部-準決賽以前
            /// </summary>
            main_semi_all = 21,
            /// <summary>
            /// 勝部-決賽 (金牌戰)
            /// </summary>
            main_final = 30,

            /// <summary>
            /// 敗部-128強
            /// </summary>
            rpc_128 = 105,
            /// <summary>
            /// 敗部-64強
            /// </summary>
            rpc_64 = 110,
            /// <summary>
            /// 敗部-32強
            /// </summary>
            rpc_32 = 120,
            /// <summary>
            /// 敗部-16強
            /// </summary>
            rpc_16 = 130,
            /// <summary>
            /// 敗部-8強
            /// </summary>
            rpc_8 = 140,

            /// <summary>
            /// 敗部-4強 (銅牌戰)
            /// </summary>
            rpc_4 = 150,

            /// <summary>
            /// 敗部-4強 (銅牌戰-West)
            /// </summary>
            rpc_4W = 151,

            /// <summary>
            /// 敗部-4強 (銅牌戰-East)
            /// </summary>
            rpc_4E = 152,

            /// <summary>
            /// 敗部-4強以前
            /// </summary>
            rpc_4_all = 153,

            /// <summary>
            /// 敗部-4強 (人數不足，非銅牌戰)
            /// </summary>
            rpc_4_fake = 155,

            /// <summary>
            /// 敗部-4強 (真銅牌戰)
            /// </summary>
            rpc_4_true = 156,

            /// <summary>
            /// 敗部-決賽 (三四名戰)
            /// </summary>
            rpc_final = 160,
            /// <summary>
            /// 復活賽-第1場 (3挑戰2)
            /// </summary>
            clg_a1 = 310,
            /// <summary>
            /// 復活賽-第2場 (2挑戰1)
            /// </summary>
            clg_a2 = 320,
            /// <summary>
            /// 復活賽-第3場 (2挑戰1) (原第1名落敗)
            /// </summary>
            clg_b1 = 330,
            /// <summary>
            /// 循環賽
            /// </summary>
            round = 510,
            /// <summary>
            /// 三四名 (只有四人的三四名戰)
            /// </summary>
            rank34 = 540,
            /// <summary>
            /// 五六名
            /// </summary>
            rank56 = 560,
            /// <summary>
            /// 七八名
            /// </summary>
            rank78 = 580,
            /// <summary>
            /// 勝部-8強
            /// </summary>
            main_r008 = 1008,
            /// <summary>
            /// 勝部-16強
            /// </summary>
            main_r016 = 1016,
            /// <summary>
            /// 勝部-32強
            /// </summary>
            main_r032 = 1032,
            /// <summary>
            /// 勝部-64強
            /// </summary>
            main_r064 = 1064,
            /// <summary>
            /// 勝部-128強
            /// </summary>
            main_r128 = 1128,
            /// <summary>
            /// 盟主賽
            /// </summary>
            ka01 = 2100,
            /// <summary>
            /// 盟主賽加賽
            /// </summary>
            kb01 = 2200,
        }

        private Item GetSiteEventsSemi(TConfig cfg, EventFilterEnum filter, string site_id)
        {
            string in_battle_type = "";
            string in_tree_name = "";
            string round_code_filter = "";
            in_battle_type = cfg.day_battle_type;

            string sql = @"
                SELECT
	                t2.id				AS 'site_id'
	                , t2.in_code		AS 'site_code'
	                , t2.in_name		AS 'site_name'
	                , t1.in_date_key
                    , t3.id             AS 'program_id'
	                , t3.in_name3       AS 'program_name'
	                , t3.in_battle_type
	                , t4.id             AS 'event_id'
	                , t4.in_round_code
	                , t4.in_tree_id
                FROM 
	                IN_MEETING_ALLOCATION t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_SITE t2 WITH(NOLOCK)
	                ON t2.id = t1.in_site
                INNER JOIN
	                IN_MEETING_PROGRAM t3 WITH(NOLOCK)
	                ON t3.id = t1.in_program
                INNER JOIN
	                IN_MEETING_PEVENT t4 WITH(NOLOCK)
	                ON t4.source_id = t3.id
                WHERE
	                t1.in_meeting = '{#meeting_id}'
	                AND t1.in_date_key = '{#in_date_key}'
	                AND t1.in_site = '{#site_id}'
	                AND t3.in_battle_type = '{#in_battle_type}'
                    AND ISNULL(t4.in_tree_no, 0) > 0
	                AND (
                        (t4.in_tree_name = N'main' AND t4.in_round_code >= 4)
                        OR
                        (t4.in_tree_name = N'repechage' AND t4.in_round_code > 4)
                    )
                ORDER BY
	                t2.in_code
	                , t1.created_on
	                , t4.in_tree_sort
	                , t4.in_round_code DESC
	                , t4.in_tree_sno
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_date_key}", cfg.in_date_key)
                .Replace("{#site_id}", site_id)
                .Replace("{#in_battle_type}", in_battle_type)
                .Replace("{#in_tree_name}", in_tree_name)
                .Replace("{#round_code_filter}", round_code_filter);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        private Item GetSiteRobinEvents(TConfig cfg, string site_id, string round)
        {
            //五人: 10場、四人: 6場、三人: 3場
            List<string> ors = new List<string>();

            switch (round)
            {
                case "1":
                    ors.Add("t3.in_team_count = 5 AND ISNULL(t4.in_tree_sno, 0) IN (1, 2)");
                    ors.Add("t3.in_team_count = 4 AND ISNULL(t4.in_tree_sno, 0) IN (1, 2)");
                    ors.Add("t3.in_team_count = 3 AND ISNULL(t4.in_tree_sno, 0) IN (1)");
                    break;

                case "2":
                    ors.Add("t3.in_team_count = 5 AND ISNULL(t4.in_tree_sno, 0) IN (3, 4)");
                    ors.Add("t3.in_team_count = 4 AND ISNULL(t4.in_tree_sno, 0) IN (3, 4)");
                    ors.Add("t3.in_team_count = 3 AND ISNULL(t4.in_tree_sno, 0) IN (2)");
                    break;

                case "3":
                    ors.Add("t3.in_team_count = 5 AND ISNULL(t4.in_tree_sno, 0) IN (5, 6)");
                    ors.Add("t3.in_team_count = 4 AND ISNULL(t4.in_tree_sno, 0) IN (5)");
                    ors.Add("t3.in_team_count = 3 AND ISNULL(t4.in_tree_sno, 0) IN (3)");
                    break;

                case "4":
                    ors.Add("t3.in_team_count = 5 AND ISNULL(t4.in_tree_sno, 0) IN (7, 8)");
                    ors.Add("t3.in_team_count = 4 AND ISNULL(t4.in_tree_sno, 0) IN (6)");
                    break;

                case "5":
                    ors.Add("t3.in_team_count = 5 AND ISNULL(t4.in_tree_sno, 0) IN (9, 10)");
                    break;

                case "11":
                    ors.Add("t3.in_team_count = 2 AND in_round = 1");
                    break;

                case "12":
                    ors.Add("t3.in_team_count = 2 AND in_round = 2");
                    break;

                case "13":
                    ors.Add("t3.in_team_count = 2 AND in_round = 3");
                    break;
            }

            string sql = @"
                SELECT
	                t2.id				AS 'site_id'
	                , t2.in_code		AS 'site_code'
	                , t2.in_name		AS 'site_name'
	                , t1.in_date_key
                    , t3.id             AS 'program_id'
	                , t3.in_name3       AS 'program_name'
	                , t3.in_battle_type
	                , t4.id             AS 'event_id'
	                , t4.in_round_code
	                , t4.in_tree_id
                FROM 
	                IN_MEETING_ALLOCATION t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_SITE t2 WITH(NOLOCK)
	                ON t2.id = t1.in_site
                INNER JOIN
	                IN_MEETING_PROGRAM t3 WITH(NOLOCK)
	                ON t3.id = t1.in_program
                INNER JOIN
	                IN_MEETING_PEVENT t4 WITH(NOLOCK)
	                ON t4.source_id = t3.id
                WHERE
	                t1.in_meeting = '{#meeting_id}'
	                AND t1.in_date_key = '{#in_date_key}'
	                AND t1.in_site = '{#site_id}'
	                AND t3.in_battle_type = 'SingleRoundRobin'
	                AND t4.in_tree_name = 'main'
	                AND (
                        {#ors}
                    )
                ORDER BY
	                t2.in_code
	                , t1.created_on
	                , t4.in_round_code DESC
	                , t4.in_tree_sno
            ";

            var conds = ors.Select(x => "( " + x + " )");

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_date_key}", cfg.in_date_key)
                .Replace("{#site_id}", site_id)
                .Replace("{#ors}", string.Join(" OR ", conds));

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        private Item GetSiteEventsST(TConfig cfg, string site_id)
        {
            string in_battle_type = "";
            string in_tree_name = "";

            string sql = @"
                SELECT
	                t2.id				AS 'site_id'
	                , t2.in_code		AS 'site_code'
	                , t2.in_name		AS 'site_name'
	                , t1.in_date_key
                    , t3.id             AS 'program_id'
	                , t3.in_name3       AS 'program_name'
	                , t3.in_battle_type
	                , t4.id             AS 'event_id'
	                , t4.in_round_code
	                , t4.in_tree_id
                FROM 
	                IN_MEETING_ALLOCATION t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_SITE t2 WITH(NOLOCK)
	                ON t2.id = t1.in_site
                INNER JOIN
	                IN_MEETING_PROGRAM t3 WITH(NOLOCK)
	                ON t3.id = t1.in_program
                INNER JOIN
	                IN_MEETING_PEVENT t4 WITH(NOLOCK)
	                ON t4.source_id = t3.id
                WHERE
	                t1.in_meeting = '{#meeting_id}'
	                AND t1.in_date_key = '{#in_date_key}'
	                AND t1.in_site = '{#site_id}'
	                AND ISNULL(t4.in_tree_no, 0) > 0
                ORDER BY
	                t2.in_code
	                , t1.created_on
	                , t4.in_round_code DESC
	                , t4.in_tree_sno

            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_date_key}", cfg.in_date_key)
                .Replace("{#site_id}", site_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        private Item GetSiteEvents(TConfig cfg, EventFilterEnum filter, string site_id)
        {
            string in_battle_type = "";
            string in_tree_name = "";
            string round_code_filter = "";

            switch (filter)
            {
                case EventFilterEnum.main_pre_quarterfinals:
                    in_battle_type = cfg.day_battle_type;
                    in_tree_name = "main";
                    round_code_filter = "AND t4.in_round_code > 8 AND ISNULL(t4.in_tree_no, 0) > 0";
                    break;

                case EventFilterEnum.main_pre_semi:
                    in_battle_type = cfg.day_battle_type;
                    in_tree_name = "main";
                    round_code_filter = "AND t4.in_round_code > 4 AND ISNULL(t4.in_tree_no, 0) > 0";
                    break;

                case EventFilterEnum.main_r128:
                    in_battle_type = cfg.day_battle_type;
                    in_tree_name = "main";
                    round_code_filter = "AND t4.in_round_code = 128 AND ISNULL(t4.in_tree_no, 0) > 0";
                    break;

                case EventFilterEnum.main_r064:
                    in_battle_type = cfg.day_battle_type;
                    in_tree_name = "main";
                    round_code_filter = "AND t4.in_round_code = 64 AND ISNULL(t4.in_tree_no, 0) > 0";
                    break;

                case EventFilterEnum.main_r032:
                    in_battle_type = cfg.day_battle_type;
                    in_tree_name = "main";
                    round_code_filter = "AND t4.in_round_code = 32 AND ISNULL(t4.in_tree_no, 0) > 0";
                    break;

                case EventFilterEnum.main_r016:
                    in_battle_type = cfg.day_battle_type;
                    in_tree_name = "main";
                    round_code_filter = "AND t4.in_round_code = 16 AND ISNULL(t4.in_tree_no, 0) > 0";
                    break;

                case EventFilterEnum.main_r008:
                    in_battle_type = cfg.day_battle_type;
                    in_tree_name = "main";
                    round_code_filter = "AND t4.in_round_code = 8 AND ISNULL(t4.in_tree_no, 0) > 0";
                    break;

                case EventFilterEnum.main_semi:
                    in_battle_type = cfg.day_battle_type;
                    in_tree_name = "main";
                    round_code_filter = "AND t4.in_round_code = 4 AND ISNULL(t4.in_tree_no, 0) > 0";
                    break;

                case EventFilterEnum.main_semi_all:
                    in_battle_type = cfg.day_battle_type;
                    in_tree_name = "main";
                    round_code_filter = "AND t4.in_round_code >= 4 AND ISNULL(t4.in_tree_no, 0) > 0";
                    break;

                case EventFilterEnum.main_final:
                    in_battle_type = cfg.day_battle_type;
                    in_tree_name = "main";
                    round_code_filter = "AND t4.in_round_code = 2 AND ISNULL(t4.in_tree_no, 0) > 0";
                    break;

                case EventFilterEnum.rpc_128:
                    in_battle_type = cfg.day_battle_type;
                    in_tree_name = "repechage";
                    round_code_filter = "AND t4.in_round_code = 128 AND ISNULL(t4.in_tree_no, 0) > 0";
                    break;

                case EventFilterEnum.rpc_64:
                    in_battle_type = cfg.day_battle_type;
                    in_tree_name = "repechage";
                    round_code_filter = "AND t4.in_round_code = 64 AND ISNULL(t4.in_tree_no, 0) > 0";
                    break;

                case EventFilterEnum.rpc_32:
                    in_battle_type = cfg.day_battle_type;
                    in_tree_name = "repechage";
                    round_code_filter = "AND t4.in_round_code = 32 AND ISNULL(t4.in_tree_no, 0) > 0";
                    break;

                case EventFilterEnum.rpc_16:
                    in_battle_type = cfg.day_battle_type;
                    in_tree_name = "repechage";
                    round_code_filter = "AND t4.in_round_code = 16 AND ISNULL(t4.in_tree_no, 0) > 0";
                    break;

                case EventFilterEnum.rpc_8:
                    in_battle_type = cfg.day_battle_type;
                    in_tree_name = "repechage";
                    round_code_filter = "AND t4.in_round_code = 8 AND ISNULL(t4.in_tree_no, 0) > 0";
                    break;

                case EventFilterEnum.rpc_4_all:
                    in_battle_type = cfg.day_battle_type;
                    in_tree_name = "repechage";
                    round_code_filter = "AND t4.in_round_code > 4 AND ISNULL(t4.in_tree_no, 0) > 0";
                    break;

                case EventFilterEnum.rpc_4:
                    in_battle_type = cfg.day_battle_type;
                    in_tree_name = "repechage";
                    round_code_filter = "AND t4.in_round_code = 4";
                    break;

                case EventFilterEnum.rpc_4_fake:
                    in_battle_type = cfg.day_battle_type;
                    in_tree_name = "repechage";
                    round_code_filter = "AND ISNULL(t3.in_rank_count, 0) = 5 AND t4.in_round_code = 4";
                    break;

                case EventFilterEnum.rpc_4_true:
                    in_battle_type = cfg.day_battle_type;
                    in_tree_name = "repechage";
                    round_code_filter = "AND ISNULL(t3.in_rank_count, 0) <> 5 AND t4.in_round_code = 4";
                    break;

                case EventFilterEnum.rpc_final:
                    in_battle_type = cfg.day_battle_type;
                    in_tree_name = "repechage";
                    round_code_filter = "AND t4.in_round_code = 2 AND ISNULL(t4.in_tree_no, 0) > 0";
                    break;

                case EventFilterEnum.clg_a1:
                    in_battle_type = "Challenge";
                    in_tree_name = "challenge-a";
                    round_code_filter = "AND t4.in_tree_id = 'CA01'";
                    break;

                case EventFilterEnum.clg_a2:
                    in_battle_type = "Challenge";
                    in_tree_name = "challenge-a";
                    round_code_filter = "AND t4.in_tree_id = 'CA02'";
                    break;

                case EventFilterEnum.clg_b1:
                    in_battle_type = "Challenge";
                    in_tree_name = "challenge-b";
                    break;

                case EventFilterEnum.round:
                    in_battle_type = "SingleRoundRobin";
                    in_tree_name = "main";
                    break;

                case EventFilterEnum.rank34:
                    in_battle_type = cfg.day_battle_type;
                    in_tree_name = "rank34";
                    break;

                case EventFilterEnum.rank56: // 7 取 5
                    in_battle_type = cfg.day_battle_type;
                    in_tree_name = "rank56";
                    round_code_filter = "AND ISNULL(t3.in_rank_count, 0) = 7";
                    break;

                case EventFilterEnum.rank78: // 9 取 7
                    in_battle_type = cfg.day_battle_type;
                    in_tree_name = "rank78";
                    round_code_filter = "AND ISNULL(t3.in_rank_count, 0) = 9";
                    break;
            }

            string sql = @"
                SELECT
	                t2.id				AS 'site_id'
	                , t2.in_code		AS 'site_code'
	                , t2.in_name		AS 'site_name'
	                , t1.in_date_key
                    , t3.id             AS 'program_id'
	                , t3.in_name3       AS 'program_name'
	                , t3.in_battle_type
	                , t4.id             AS 'event_id'
	                , t4.in_round_code
	                , t4.in_tree_id
                FROM 
	                IN_MEETING_ALLOCATION t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_SITE t2 WITH(NOLOCK)
	                ON t2.id = t1.in_site
                INNER JOIN
	                IN_MEETING_PROGRAM t3 WITH(NOLOCK)
	                ON t3.id = t1.in_program
                INNER JOIN
	                IN_MEETING_PEVENT t4 WITH(NOLOCK)
	                ON t4.source_id = t3.id
                WHERE
	                t1.in_meeting = '{#meeting_id}'
	                AND t1.in_date_key = '{#in_date_key}'
	                AND t1.in_site = '{#site_id}'
	                AND t3.in_battle_type = '{#in_battle_type}'
	                AND t4.in_tree_name = N'{#in_tree_name}'
	                {#round_code_filter}
                ORDER BY
	                t2.in_code
	                , t1.created_on
	                , t4.in_round_code DESC
	                , t4.in_tree_sno
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_date_key}", cfg.in_date_key)
                .Replace("{#site_id}", site_id)
                .Replace("{#in_battle_type}", in_battle_type)
                .Replace("{#in_tree_name}", in_tree_name)
                .Replace("{#round_code_filter}", round_code_filter);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        private Item GetSiteEvents2(TConfig cfg, EventFilterEnum filter)
        {
            string in_battle_type = "";
            string in_tree_name = "";
            string round_code_filter = "";

            switch (filter)
            {
                case EventFilterEnum.main_final:
                    in_battle_type = cfg.day_battle_type;
                    in_tree_name = "main";
                    round_code_filter = "AND t1.in_round_code = 2";
                    break;

                case EventFilterEnum.rpc_4W:
                    in_battle_type = cfg.day_battle_type;
                    in_tree_name = "repechage";
                    round_code_filter = "AND t1.in_round_code = 4 AND t1.in_round_id = '1'";
                    break;

                case EventFilterEnum.rpc_4E:
                    in_battle_type = cfg.day_battle_type;
                    in_tree_name = "repechage";
                    round_code_filter = "AND t1.in_round_code = 4 AND t1.in_round_id = '2'";
                    break;
            }

            string sql = @"
                SELECT
	                t1.id             AS 'event_id'
	                , t1.in_tree_id
	                , t1.in_date_key
	                , t1.in_round_code
                    , t2.id             AS 'program_id'
	                , t2.in_name3       AS 'program_name'
	                , t2.in_battle_type
	                , t11.id			AS 'site_id'
	                , t11.in_code		AS 'site_code'
	                , t11.in_name		AS 'site_name'
                FROM
	                IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PROGRAM t2 WITH(NOLOCK)
	                ON t2.id = t1.source_id
                LEFT OUTER JOIN
	                IN_MEETING_SITE t11 WITH(NOLOCK)
	                ON t11.id = t1.in_site
                WHERE
	                t1.in_meeting = '{#meeting_id}'
	                AND t1.in_date_key = '{#in_date_key}'
	                AND t2.in_battle_type = '{#in_battle_type}'
	                AND t1.in_tree_name = N'{#in_tree_name}'
	                {#round_code_filter}
                ORDER BY
	                t2.in_medal_sort
	                , t1.in_tree_sno

            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_date_key}", cfg.in_date_key)
                .Replace("{#in_battle_type}", in_battle_type)
                .Replace("{#in_tree_name}", in_tree_name)
                .Replace("{#round_code_filter}", round_code_filter);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        private Item GetSiteEvents3(TConfig cfg, EventFilterEnum filter, string site_id)
        {
            string in_battle_type = cfg.day_battle_type;

            string sql = @"
				SELECT
	                t1.id             AS 'event_id'
	                , t1.in_tree_id
	                , t1.in_date_key
	                , t1.in_round_code
                    , t2.id             AS 'program_id'
	                , t2.in_name3       AS 'program_name'
	                , t2.in_battle_type
	                , t11.id			AS 'site_id'
	                , t11.in_code		AS 'site_code'
	                , t11.in_name		AS 'site_name'
                FROM
	                IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PROGRAM t2 WITH(NOLOCK)
	                ON t2.id = t1.source_id
                LEFT OUTER JOIN
	                IN_MEETING_SITE t11 WITH(NOLOCK)
	                ON t11.id = t1.in_site
                WHERE
	                t1.in_meeting = '{#meeting_id}'
	                AND t1.in_site = '{#site_id}'
	                AND t1.in_date_key = '{#in_date_key}'
	                AND t2.in_battle_type = '{#in_battle_type}'
	                AND 
					((t1.in_tree_name = 'main' AND t1.in_round_code = '4')
						OR
					(t1.in_tree_name = 'repechage' AND t1.in_round_code = '8'))
                ORDER BY
	                t2.in_sort_order
	                , t1.in_tree_sort
					, t1.in_tree_sno
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#site_id}", site_id)
                .Replace("{#in_date_key}", cfg.in_date_key)
                .Replace("{#in_battle_type}", in_battle_type);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        private Item GetSiteEvents34(TConfig cfg, EventFilterEnum filter, string site_id)
        {
            string in_battle_type = cfg.day_battle_type;

            string sql = @"
				SELECT
	                t1.id             AS 'event_id'
	                , t1.in_tree_id
	                , t1.in_date_key
	                , t1.in_round_code
                    , t2.id             AS 'program_id'
	                , t2.in_name3       AS 'program_name'
	                , t2.in_battle_type
	                , t11.id			AS 'site_id'
	                , t11.in_code		AS 'site_code'
	                , t11.in_name		AS 'site_name'
                FROM
	                IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PROGRAM t2 WITH(NOLOCK)
	                ON t2.id = t1.source_id
                LEFT OUTER JOIN
	                IN_MEETING_SITE t11 WITH(NOLOCK)
	                ON t11.id = t1.in_site
                WHERE
	                t1.in_meeting = '{#meeting_id}'
	                AND t1.in_site = '{#site_id}'
	                AND t1.in_date_key = '{#in_date_key}'
	                AND t2.in_battle_type = '{#in_battle_type}'
	                AND 
					((t1.in_tree_name = 'repechage' AND t1.in_round_code = 2 AND ISNULL(t2.in_rank_count, 0) >= 4)
						OR
					(t1.in_tree_name = 'rank34'))
                ORDER BY
	                t2.in_sort_order
	                , t1.in_tree_sort
					, t1.in_tree_sno
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#site_id}", site_id)
                .Replace("{#in_date_key}", cfg.in_date_key)
                .Replace("{#in_battle_type}", in_battle_type);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        private Item GetSiteEventsByTid(TConfig cfg, string site_id, string tree_id)
        {
            string in_battle_type = cfg.day_battle_type;

            string sql = @"
				SELECT
	                t1.id             AS 'event_id'
	                , t1.in_tree_id
	                , t1.in_date_key
	                , t1.in_round_code
                    , t2.id             AS 'program_id'
	                , t2.in_name3       AS 'program_name'
	                , t2.in_battle_type
	                , t11.id			AS 'site_id'
	                , t11.in_code		AS 'site_code'
	                , t11.in_name		AS 'site_name'
                FROM
	                IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PROGRAM t2 WITH(NOLOCK)
	                ON t2.id = t1.source_id
                LEFT OUTER JOIN
	                IN_MEETING_SITE t11 WITH(NOLOCK)
	                ON t11.id = t1.in_site
                WHERE
	                t1.in_meeting = '{#meeting_id}'
	                AND t1.in_date_key = '{#in_date_key}'
	                AND t1.in_site = '{#site_id}'
	                AND t1.in_tree_id = '{#tree_id}'
                ORDER BY
	                t2.in_sort_order
	                , t1.in_tree_sort
					, t1.in_tree_sno
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_date_key}", cfg.in_date_key)
                .Replace("{#site_id}", site_id)
                .Replace("{#tree_id}", tree_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        #region 匯出
        //匯出
        private void Export(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            string meeting_id = itmReturn.getProperty("meeting_id", "");

            //取得賽事資訊
            Item itmMeeting = GetMeeting(CCO, strMethodName, inn, meeting_id);
            if (itmMeeting.isError())
            {
                itmReturn.setProperty("error_message", "取得賽事資料發生錯誤");
                return;
            }

            //取得場地資訊
            Item itmSites = GetMeetingSites(CCO, strMethodName, inn, meeting_id);
            if (itmSites.isError())
            {
                itmReturn.setProperty("error_message", "取得場地資訊發生錯誤");
                return;
            }

            //取得場地分配資訊
            Item itmAllocations = GetMeetingAllocations(CCO, strMethodName, inn, meeting_id);
            if (itmAllocations.isError())
            {
                itmReturn.setProperty("error_message", "取得場地分配資訊發生錯誤");
                return;
            }

            //轉換場地分配
            var map = MapAllocation(inn, itmAllocations, itmSites);

            //設定匯出資訊
            string main_name = itmMeeting.getProperty("in_title", "");
            string sub_name = "場次分配表";
            TExport export = GetExportInfo(CCO, strMethodName, inn, main_name, sub_name);

            TConfig cfg = new TConfig
            {
                FontSize = 12,
                FontName = "標楷體",
                RowStart = 3,
                ColStart = 1,
                Title = itmMeeting.getProperty("in_title", ""),
            };

            ClosedXML.Excel.XLWorkbook workbook = new ClosedXML.Excel.XLWorkbook();

            foreach (var kv in map.Allocations)
            {
                var allocation = kv.Value;
                AppendAllocationSheet(CCO, strMethodName, inn, workbook, cfg, map, allocation);
            }

            workbook.SaveAs(export.File);

            itmReturn.setProperty("xls_name", export.Url);
        }

        //場次分配表
        private void AppendAllocationSheet(Aras.Server.Core.CallContext CCO
            , string strMethodName
            , Innovator inn
            , ClosedXML.Excel.XLWorkbook workbook
            , TConfig cfg
            , TMap map
            , TAllocation allocation)
        {
            ClosedXML.Excel.IXLWorksheet sheet = workbook.Worksheets.Add(allocation.DisplayDate);
            sheet.PageSetup.SetPaperSize(ClosedXML.Excel.XLPaperSize.A4Paper);
            sheet.PageSetup.SetPageOrientation(ClosedXML.Excel.XLPageOrientation.Landscape);
            sheet.PageSetup.Margins.SetTop(0.5);
            sheet.PageSetup.Margins.SetRight(1);
            sheet.PageSetup.Margins.SetLeft(1);
            sheet.PageSetup.FitToPages(1, 0);

            //賽事標題
            ClosedXML.Excel.IXLCell cell_title = sheet.Cell(1, cfg.ColStart);
            cell_title.Value = cfg.Title + "場地分配表";
            cell_title.Style.Font.Bold = true;
            cell_title.Style.Font.FontSize = 16;
            cell_title.Style.Font.FontName = cfg.FontName;
            cell_title.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
            cell_title.Style.Alignment.Vertical = ClosedXML.Excel.XLAlignmentVerticalValues.Center;

            sheet.Range(1, cfg.ColStart, 1, cfg.ColStart + map.Cols - 1)
                .Merge()
                .Style.Alignment.Vertical = ClosedXML.Excel.XLAlignmentVerticalValues.Center;

            sheet.Row(1).Height = 50;


            //比賽日期
            ClosedXML.Excel.IXLCell cell_date = sheet.Cell(2, cfg.ColStart);
            cell_date.Value = allocation.ChineseDate;
            cell_date.Style.Font.FontSize = 14;
            cell_date.Style.Font.FontName = cfg.FontName;
            cell_date.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
            cell_date.Style.Alignment.Vertical = ClosedXML.Excel.XLAlignmentVerticalValues.Center;

            sheet.Range(2, cfg.ColStart, 2, cfg.ColStart + map.Cols - 1)
                .Merge()
                .Style.Alignment.Vertical = ClosedXML.Excel.XLAlignmentVerticalValues.Center;

            sheet.Row(2).Height = 30;


            int wsRow = cfg.RowStart;
            int wsCol = cfg.ColStart;

            for (int i = 0; i < map.Rows; i++)
            {
                for (int j = 0; j < map.Cols; j++)
                {
                    string key = i + "_" + j;
                    string value = "";
                    if (map.Sites.ContainsKey(key))
                    {
                        value = map.Sites[key].Name;
                    }
                    SetCell(sheet, wsRow, wsCol + j, cfg, value, format: "head");
                }
                wsRow++;

                for (int j = 0; j < map.Cols; j++)
                {
                    string key = i + "_" + j;
                    if (allocation.SiteGroups.ContainsKey(key))
                    {
                        var list = allocation.SiteGroups[key];
                        string value = string.Join(Environment.NewLine, list.Select(x => x.Label2));
                        SetCell(sheet, wsRow, wsCol + j, cfg, value, format: "wrap");
                    }
                }
                wsRow++;
            }

            var width = (int)(84 / map.Cols);
            for (int j = 0; j < map.Cols; j++)
            {
                sheet.Column(wsCol + j).Width = width;
            }
        }

        //設定物件與資料列
        private void SetCell(ClosedXML.Excel.IXLWorksheet sheet, int wsRow, int wsCol, TConfig cfg, string value, string format = "")
        {
            ClosedXML.Excel.IXLCell cell = sheet.Cell(wsRow, wsCol);

            cell.Style.Font.FontSize = cfg.FontSize;
            cell.Style.Font.FontName = cfg.FontName;

            cell.Style.Border.SetRightBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetTopBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetBottomBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetLeftBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);

            switch (format)
            {
                case "head":
                    cell.Value = value;
                    cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    cell.Style.Font.Bold = true;
                    cell.DataType = ClosedXML.Excel.XLDataType.Text;
                    //cell.Style.Fill.BackgroundColor = ClosedXML.Excel.XLColor.FromHtml("#295C90");
                    //cell.Style.Font.FontColor = ClosedXML.Excel.XLColor.White;
                    break;

                case "wrap":
                    cell.Value = value;
                    cell.Style.Alignment.Vertical = ClosedXML.Excel.XLAlignmentVerticalValues.Top;
                    cell.Style.Alignment.WrapText = true;
                    sheet.Columns(wsRow, wsCol).AdjustToContents();
                    break;

                case "center":
                    cell.Value = value;
                    cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    break;

                case "yyyy/MM/dd":
                case "yyyy/MM/dd HH:mm":
                    cell.Value = GetDateTimeVal(value, format);
                    cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    break;

                case "$ #,##0":
                    cell.Value = value;
                    cell.Style.NumberFormat.Format = "$ #,##0";
                    break;

                case "tel":
                    if (value.StartsWith("09"))
                    {
                        cell.Value = value.Replace("-", "");
                        cell.Style.NumberFormat.Format = "0000-000-000";
                        cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    }
                    else
                    {
                        cell.Value = "'" + value;
                        cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    }
                    break;

                case "text":
                    cell.Value = "'" + value;
                    cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    break;

                default:
                    cell.Value = value;
                    break;
            }
        }

        /// <summary>
        /// 匯出資料模型
        /// </summary>
        private class TExport
        {
            /// <summary>
            /// 樣板來源完整路徑
            /// </summary>
            public string Source { get; set; }

            /// <summary>
            /// 檔案名稱
            /// </summary>
            public string File { get; set; }

            /// <summary>
            /// 檔案網址
            /// </summary>
            public string Url { get; set; }
        }

        /// <summary>
        /// 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public Innovator inn { get; set; }

            public string meeting_id { get; set; }
            //public string site_id { get; set; }
            public string in_date_key { get; set; }
            public string in_sort { get; set; }

            public Item itmMeeting { get; set; }
            public string mt_battle_type { get; set; }
            public bool is_challenge { get; set; }
            public int robin_player { get; set; }

            public string day_battle_type { get; set; }

            /// <summary>
            /// 字型名稱
            /// </summary>
            public string FontName { get; set; }

            /// <summary>
            /// 字型大小
            /// </summary>
            public int FontSize { get; set; }

            /// <summary>
            /// 列起始位置
            /// </summary>
            public int RowStart { get; set; }

            /// <summary>
            /// 欄起始位置
            /// </summary>
            public int ColStart { get; set; }

            /// <summary>
            /// 當前列位置
            /// </summary>
            public int CurrentRow { get; set; }

            /// <summary>
            /// 賽事名稱
            /// </summary>
            public string Title { get; set; }
        }

        /// <summary>
        /// 取得匯出設定資訊
        /// </summary>
        private TExport GetExportInfo(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string main_name, string sub_name)
        {
            Item itmPath = GetXlsPaths(CCO, strMethodName, inn);

            string export_path = itmPath.getProperty("export_path", "").TrimEnd('\\');
            string ext_name = ".xlsx";
            string doc_name = main_name + "_" + sub_name + "_" + DateTime.Now.ToString("yyyyMMdd_HHmmss");

            string doc_file = export_path + "\\" + doc_name + ext_name;
            string doc_url = doc_name + ext_name;

            return new TExport
            {
                Source = itmPath.getProperty("template_path", ""),
                File = doc_file,
                Url = doc_url,
            };
        }

        /// <summary>
        /// 取得樣板與匯出路徑
        /// </summary>
        private Item GetXlsPaths(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string in_name = "")
        {
            Item itmResult = inn.newItem();

            //Excel 匯出路徑 (from 原創.設定參數)
            string sql = @"
        SELECT 
            t2.in_name AS 'variable'
            , t1.in_name
            , t1.in_value 
        FROM 
            In_Variable_Detail t1 WITH(NOLOCK)
        INNER JOIN 
            In_Variable t2 WITH(NOLOCK) 
            ON t2.ID = t1.SOURCE_ID 
        WHERE 
            t2.in_name = N'meeting_excel'  
            AND t1.in_name IN (N'export_path', N'{#in_name}')
        ";

            sql = sql.Replace("{#in_name}", in_name);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item items = inn.applySQL(sql);

            if (items.isError() || items.getItemCount() < 1)
            {
                throw new Exception("未設定樣板與匯出路徑 _# " + in_name);
            }

            for (int i = 0; i < items.getItemCount(); i++)
            {
                Item item = items.getItemByIndex(i);
                string xls_name = item.getProperty("in_name", "");
                string xls_value = item.getProperty("in_value", "");

                if (xls_name == "export_path")
                {
                    itmResult.setProperty("export_path", xls_value);
                }
                else
                {
                    itmResult.setProperty("template_path", xls_value);
                }
            }

            return itmResult;
        }
        #endregion 匯出

        //移除當日場地分配
        private void RemoveDate(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            string sql = "";
            Item itmSQL = null;

            string meeting_id = itmReturn.getProperty("meeting_id", "");
            string in_date_key = itmReturn.getProperty("in_date_key", "");

            sql = "DELETE FROM IN_MEETING_ALLOCATION WHERE in_meeting = '" + meeting_id + "' AND in_date_key = '" + in_date_key + "'";
            itmSQL = inn.applySQL(sql);
            if (itmSQL.isError()) throw new Exception("移除失敗");


            sql = "UPDATE IN_MEETING_PEVENT SET in_date_key = NULL, in_site = NULL, in_site_code = NULL WHERE in_meeting = '" + meeting_id + "' AND ISNULL(in_date_key, '') = '" + in_date_key + "'";
            itmSQL = inn.applySQL(sql);
            if (itmSQL.isError()) throw new Exception("移除失敗");
        }

        //移除
        private void Remove(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            string sql = "";
            Item itmSQL = null;

            string allocation_id = itmReturn.getProperty("allocation_id", "");

            Item itmAllocation = inn.applySQL("SELECT * FROM IN_MEETING_ALLOCATION WITH(NOLOCK) WHERE id = '" + allocation_id + "'");
            if (itmAllocation.isError() || itmAllocation.getResult() == "" || itmAllocation.getItemCount() != 1)
            {
                throw new Exception("查無資料");
            }

            sql = "DELETE FROM IN_MEETING_ALLOCATION WHERE id = '" + allocation_id + "'";
            itmSQL = inn.applySQL(sql);
            if (itmSQL.isError())
            {
                throw new Exception("移除失敗");
            }

            string meeting_id = itmAllocation.getProperty("in_meeting", "");
            string in_date_key = itmAllocation.getProperty("in_date_key", "");
            string in_program = itmAllocation.getProperty("in_program", "");

            sql = "UPDATE IN_MEETING_PEVENT SET in_date_key = NULL, in_site = NULL, in_site_code = NULL WHERE source_id = '" + in_program + "'";
            itmSQL = inn.applySQL(sql);
            if (itmSQL.isError()) throw new Exception("移除失敗");
        }

        //儲存
        private void Save(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            string in_l2 = itmReturn.getProperty("in_l2", "");
            string in_l2_list = itmReturn.getProperty("in_l2_list", "");

            if (in_l2 != "")
            {
                RunSave(CCO, strMethodName, inn, in_l2, itmReturn);
            }
            else if (in_l2_list != "")
            {
                var arr = in_l2_list.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                if (arr != null && arr.Length > 0)
                {
                    foreach (var x in arr)
                    {
                        RunSave(CCO, strMethodName, inn, x, itmReturn);
                        System.Threading.Thread.Sleep(100);
                    }
                }
            }
        }

        //儲存
        private void RunSave(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string in_l2, Item itmReturn)
        {
            string sql = "";
            Item itmSQL = null;

            string meeting_id = itmReturn.getProperty("meeting_id", "");
            string in_date_source = itmReturn.getProperty("in_date", "");
            string in_place = itmReturn.getProperty("in_place", "");
            string in_l1 = itmReturn.getProperty("in_l1", "");

            string in_date = GetDateTimeVal(in_date_source, "yyyy-MM-ddTHH:mm:ss", add_hours: 0);
            string in_date_key = GetDateTimeVal(in_date_source, "yyyy-MM-dd");

            sql = @"
                SELECT
        	        *
                FROM
        	        IN_MEETING_ALLOCATION WITH(NOLOCK)
                WHERE
        	        in_meeting = '{#meeting_id}'
        	        AND in_date_key = '{#in_date_key}'
        	        AND in_l1 = N'{#in_l1}'
        	        AND in_l2 = N'{#in_l2}'
            ";

            sql = sql.Replace("{#meeting_id}", meeting_id)
                .Replace("{#in_date_key}", in_date_key)
                .Replace("{#in_l1}", in_l1)
                .Replace("{#in_l2}", in_l2);

            itmSQL = inn.applySQL(sql);
            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            if (itmSQL.getItemCount() > 0)
            {
                throw new Exception("該組別已加入");
            }

            Item itmAllocation = inn.newItem("In_Meeting_Allocation");
            itmAllocation.setProperty("in_meeting", meeting_id);
            itmAllocation.setProperty("in_type", "category");
            itmAllocation.setProperty("in_date_key", in_date_key);
            itmAllocation.setProperty("in_date", in_date);
            itmAllocation.setProperty("in_place", in_place);
            itmAllocation.setProperty("in_l1", in_l1);
            itmAllocation.setProperty("in_l2", in_l2);
            itmAllocation = itmAllocation.apply("add");
        }

        //儲存
        private void Allocate(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            string sql = "";
            Item itmSQL = null;

            string meeting_id = itmReturn.getProperty("meeting_id", "");
            string in_date_source = itmReturn.getProperty("in_date_key", "");
            string in_site = itmReturn.getProperty("in_site", "");
            string in_program = itmReturn.getProperty("in_program", "");

            string in_date = GetDateTimeVal(in_date_source, "yyyy-MM-ddTHH:mm:ss", add_hours: 0);
            string in_date_key = in_date_source;

            string in_place = "";

            sql = @"
                SELECT 
                	TOP 1 * 
                FROM 
                	IN_MEETING_ALLOCATION WITH(NOLOCK) 
                WHERE 
                	in_type = 'category' 
                	AND in_meeting =  '{#meeting_id}' 
                	And in_date_key = '{#in_date_key}'
            ";

            sql = sql.Replace("{#meeting_id}", meeting_id)
                .Replace("{#in_date_key}", in_date_key);

            Item itmCategory = inn.applySQL(sql);
            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            if (!itmCategory.isError() && itmCategory.getResult() != "")
            {
                in_place = itmCategory.getProperty("in_place", "");
            }

            sql = @"
                SELECT
        	        *
                FROM
        	        IN_MEETING_ALLOCATION WITH(NOLOCK)
                WHERE
        	        in_meeting = '{#meeting_id}'
        	        AND in_date_key = '{#in_date_key}'
        	        AND in_site = N'{#in_site}'
        	        AND in_program = N'{#in_program}'
            ";

            sql = sql.Replace("{#meeting_id}", meeting_id)
                .Replace("{#in_date_key}", in_date_key)
                .Replace("{#in_site}", in_site)
                .Replace("{#in_program}", in_program);

            itmSQL = inn.applySQL(sql);
            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            if (itmSQL.getItemCount() > 0)
            {
                throw new Exception("該組別已加入");
            }

            Item itmAllocation = inn.newItem("In_Meeting_Allocation");
            itmAllocation.setProperty("in_meeting", meeting_id);
            itmAllocation.setProperty("in_type", "fight");
            itmAllocation.setProperty("in_date_key", in_date_key);
            itmAllocation.setProperty("in_date", in_date);
            itmAllocation.setProperty("in_site", in_site);
            itmAllocation.setProperty("in_program", in_program);
            itmAllocation.setProperty("in_place", in_place);
            itmAllocation = itmAllocation.apply("add");

            if (itmAllocation.isError())
            {
                throw new Exception("加入失敗");
            }
            else
            {
                Item itmSite = inn.applySQL("SELECT * FROM IN_MEETING_SITE WITH(NOLOCK) WHERE id = '" + in_site + "'");
                string in_site_code = itmSite.getProperty("in_code");

                string allocation_id = itmAllocation.getProperty("id", "");
                string program_id = itmAllocation.getProperty("in_program", "");

                sql = "UPDATE IN_MEETING_PROGRAM SET"
                    + " in_site_code = '" + in_site_code + "'"
                    + ", in_allocation = '" + allocation_id + "'"
                    + ", in_fight_day = '" + in_date_key + "'"
                    + ", in_fight_site = N'" + in_place + "'"
                    + " WHERE id = '" + program_id + "'";

                inn.applySQL(sql);

                //修補場次日期
                FixDate(CCO, strMethodName, inn, program_id);


                Item itmProgram = inn.applySQL("SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + program_id + "'");

                itmReturn.setProperty("allocation_id", allocation_id);
                itmReturn.setProperty("program_id", program_id);
                itmReturn.setProperty("program_name2", itmProgram.getProperty("in_name2", ""));
                itmReturn.setProperty("program_team_count", itmProgram.getProperty("in_event_count", ""));
            }
        }

        //修正日期
        private void FixDate(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string program_id)
        {
            string sql = "";
            Item itmSQL = null;

            sql = @"
                 UPDATE t1 SET
             	    t1.in_date_key = t3.in_date_key
             	    , t1.in_site = t4.id
             	    , t1.in_site_code = t4.in_code
                 FROM
             	    IN_MEETING_PEVENT t1
                 INNER JOIN
                    IN_MEETING_PROGRAM t2
                    ON t2.id = t1.source_id
                 INNER JOIN
             	    IN_MEETING_ALLOCATION t3
             	    ON t3.in_program = t2.id
                 INNER JOIN
             	    IN_MEETING_SITE t4
             	    ON t4.id = t3.in_site
                 WHERE
                    t2.id = '{#program_id}'
            ";

            sql = sql.Replace("{#program_id}", program_id);

            itmSQL = inn.applySQL(sql);
        }

        //查詢
        private void Query(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn, bool isEdit = false, bool isUpload = false, bool linkWeight = false)
        {
            string meeting_id = itmReturn.getProperty("meeting_id", "");

            Item itmMeeting = GetMeeting(CCO, strMethodName, inn, meeting_id);
            if (itmMeeting.isError())
            {
                itmReturn.setProperty("error_message", "取得賽事資料發生錯誤");
                return;
            }

            //附加賽事資訊
            AppendMeeting(itmMeeting, itmReturn);

            //附加賽會參數
            Item itmMtVrl = inn.applyMethod("In_Meeting_Variable"
                    , "<meeting_id>" + meeting_id + "</meeting_id>"
                    + "<name>allocate_mode</name>"
                    + "<scene>get</scene>");
            itmReturn.setProperty("allocate_mode", itmMtVrl.getProperty("inn_result", ""));

            if (isEdit)
            {
                //附加賽事選單
                Item itmJson = inn.newItem("In_Meeting");
                itmJson.setProperty("meeting_id", meeting_id);
                itmJson.setProperty("need_id", "1");
                itmJson = itmJson.apply("in_meeting_program_options");
                itmReturn.setProperty("in_level_json", itmJson.getProperty("json", ""));
                itmReturn.setProperty("in_level_count", itmJson.getProperty("total", ""));
            }

            //取得場地資訊
            Item itmSites = GetMeetingSites(CCO, strMethodName, inn, meeting_id);
            if (itmSites.isError())
            {
                itmReturn.setProperty("error_message", "取得場地資訊發生錯誤");
                return;
            }

            //取得場地分配資訊
            Item itmAllocations = GetMeetingAllocations(CCO, strMethodName, inn, meeting_id);
            if (itmAllocations.isError())
            {
                itmReturn.setProperty("error_message", "取得場地分配資訊發生錯誤");
                return;
            }

            bool noMenu = itmReturn.getProperty("menu", "") == "no";
            var map = MapAllocation(inn, itmAllocations, itmSites, isEdit, isUpload, linkWeight, noMenu);

            //附加場地分配資訊
            AppendAllocations(CCO, strMethodName, inn, map, itmReturn);
        }

        //附加場地分配資訊
        private void AppendAllocations(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, TMap map, Item itmReturn)
        {
            StringBuilder tabs = new StringBuilder();
            StringBuilder contents = new StringBuilder();

            //頁籤
            int no = 1;
            tabs.Append("<ul class='nav nav-pills nav-site' id='pills-tab' role='tablist'>");
            foreach (var kv in map.Allocations)
            {
                var active = no == 1 ? "class='nav-item active'" : "nav-item";
                var allocation = kv.Value;
                var ele_id = "tab_" + allocation.Key.Replace("-", "");

                tabs.Append("<li " + active + ">"
                    + " <a class='nav-link'"
                    + " id='" + ele_id + "'"
                    + " data-toggle='pill' role='tab' aria-controls='pills-home' aria-selected='true'"
                    + " href='#" + allocation.Key + "'"
                    + " onclick='StoreTab_Click(this)'><i class='fa fa-calendar' aria-hidden='true'></i>" + allocation.DisplayDate + "</a></li>");

                no++;
            }
            tabs.Append("</ul>");

            no = 1;
            contents.Append("<div class='tab-content page-tab-content' id='pills-tabContent'>");
            foreach (var kv in map.Allocations)
            {
                var active = no == 1 ? "active" : "";
                var allocation = kv.Value;

                contents.Append("<div id='" + allocation.Key + "' class='tab-pane fade in " + active + "' role='tabpanel'>");
                AppendAllocationTable(contents, map, allocation);
                contents.Append("</div>");

                no++;
            }
            contents.Append("</div>");

            StringBuilder builder = new StringBuilder();
            builder.Append("<div class='col-lg-12'>");
            builder.Append(tabs);
            builder.Append(contents);
            builder.Append("</div>");
            itmReturn.setProperty("inn_tables", builder.ToString());
        }

        private void AppendAllocationTable(StringBuilder builder, TMap map, TAllocation allocation)
        {
            StringBuilder body = new StringBuilder();

            string remove_icon = " <i class='fa fa-remove' style='color: red;' data-type='category' data-aid='{#allocation_id}' onclick='Remove_Allocation(this)' ></i>";
            string upload_icon = " <i class='fa fa-cloud-upload' style='color: red;' data-type='category' data-aid='{#allocation_id}' onclick='Upload_Click(this)' ></i>";

            if (!map.IsEdit) remove_icon = "";
            if (!map.IsUpload) upload_icon = "";

            for (int i = 0; i < map.Rows; i++)
            {
                //標題
                body.Append("  <tr>");
                for (int j = 0; j < map.Cols; j++)
                {
                    string key = i + "_" + j;
                    if (map.Sites.ContainsKey(key))
                    {
                        TSite site = map.Sites[key];

                        string td_css = " class='text-center btn-primary site-name'";
                        string td_eid = " id='TH_" + site.Id + "'";
                        string td_val = " data-val='" + site.Name + "'";

                        body.Append("<td "
                            + td_css
                            + td_eid
                            + td_val
                            + "><b>" + site.Name + GetGroupCount(map, site, allocation) + "</b>"
                            + "</td>");
                    }
                    else
                    {
                        body.Append("    <td class='text-center'></td>");
                    }
                }
                body.Append("  </tr>");

                //當日對打量級
                body.Append("  <tr>");
                for (int j = 0; j < map.Cols; j++)
                {
                    string key = i + "_" + j;

                    if (map.Sites.ContainsKey(key))
                    {
                        TSite site = map.Sites[key];
                        string td_eid = " id='TD_" + site.Id + "'";
                        body.Append("<td class='text-center'" + td_eid + ">");
                        body.Append(GetGroupSelectCtrl(map, site, allocation));
                        body.Append(GetGroupSpanCtrl(map, site, allocation));
                        body.Append("</td>");
                    }
                    else
                    {
                        body.Append("<td class='text-center'></td>");
                    }
                }
                body.Append("  </tr>");
            }


            builder.AppendLine("<div class='box-body row container-row' >");
            builder.AppendLine("    <div class='box box-fix'>");

            builder.AppendLine("      <div class='box-header with-border'>");
            builder.AppendLine("        <h3 class='box-title'>");
            builder.AppendLine("          <b>" + allocation.ChineseDate + GetRemoveDateCtrl(map, allocation) + "</b>");
            builder.AppendLine("        </h3>");

            if (map.IsEdit)
            {
                //當日對打組別
                for (int i = 0; i < allocation.Categories.Count; i++)
                {
                    TCategory category = allocation.Categories[i];
                    builder.Append("<p class='in_category' data-inl1='" + category.InL1 + "' data-inl2='" + category.InL2 + "' style='display:none'>"
                        + category.Label + remove_icon.Replace("{#allocation_id}", category.Id)
                        + "</p>");
                }
            }

            builder.AppendLine("      </div>");

            builder.AppendLine("      <div class='box-body'>");
            builder.AppendLine("        <table class='table table-site' data-toggle='table' style='background-color: #fff;'>");
            builder.Append("              <tbody>");
            builder.Append(body);
            builder.Append("              </tbody>");
            builder.AppendLine("        </table>");
            builder.AppendLine("      </div>");
            builder.AppendLine("    </div>");
            builder.AppendLine("</div>");
        }

        private string GetRemoveDateCtrl(TMap map, TAllocation allocation)
        {
            if (map.IsEdit)
            {
                return " <i class='fa fa-remove' style='color: red'"
                    + " data-did='" + allocation.Key + "'"
                    + " onclick='Remove_Date(this)'></i>";
            }
            else
            {
                return "";
            }
        }

        private string GetGroupSpanCtrl(TMap map, TSite site, TAllocation allocation)
        {
            List<TGroup> list = null;
            if (allocation.SiteGroups.ContainsKey(site.Key))
            {
                list = allocation.SiteGroups[site.Key];
            }

            if (list == null || list.Count == 0)
            {
                return "";
            }

            StringBuilder builder = new StringBuilder();
            foreach (var group in list)
            {
                builder.Append("<p>");
                if (map.IsLinkWeight)
                {
                    builder.Append("<a class='grp_cnt' target='_blank' onclick='GoFightTreePage(this)'"
                        + " data-l1='" + group.in_l1 + "'"
                        + " data-l2='" + group.in_l2 + "'"
                        + " data-l3='" + group.in_l3 + "'"
                        + " data-cnt='" + group.ECnt + "'"
                        + " >" + group.LabelCnt + "</a>");
                }
                else if (map.NoMenu)
                {
                    builder.Append("<a data-pid='" + group.ProgramId + "' onclick='GoFightDraw(this)'>" + group.LabelCnt + "</a>");
                }
                else
                {
                    builder.Append("<a data-pid='" + group.ProgramId + "' onclick='EditTreeNo(this)'>" + group.LabelCnt + "</a>");
                }

                if (map.IsEdit)
                {
                    builder.Append(" <i class='fa fa-remove' style='color: red'"
                        + " data-aid='" + group.Id + "'"
                        + " data-pid='" + group.ProgramId + "'"
                        + " data-sid='" + group.SiteId + "'"
                        + " onclick='Remove_Allocation(this)'></i>");
                }
                else if (map.IsUpload)
                {
                    builder.Append(" <i class='fa fa-cloud-upload' style='color: blue' data-aid='" + group.Id + "' data-pid='" + group.ProgramId + "' onclick='Upload_Click(this)'></i>");
                }

                builder.Append("</p>");
            }
            return builder.ToString();
        }

        private string GetGroupCount(TMap map, TSite site, TAllocation allocation)
        {
            List<TGroup> list = null;
            if (allocation.SiteGroups.ContainsKey(site.Key))
            {
                list = allocation.SiteGroups[site.Key];
            }

            if (list == null || list.Count == 0)
            {
                return "";
            }

            int total = 0;
            foreach (var group in list)
            {
                int cnt = GetIntVal(group.ECnt);
                total += cnt;
            }
            return " (" + total + ")";
        }

        private string GetGroupSelectCtrl(TMap map, TSite site, TAllocation allocation)
        {
            if (map.IsEdit)
            {
                StringBuilder builder = new StringBuilder();
                builder.Append("<select class='form-control ctrl_program' data-sid='" + site.Id + "' data-did='" + allocation.Key + "' onchange='Program_Change(this)'>");
                builder.Append("<option value=''>請選擇</option>");
                builder.Append("</select>");
                return builder.ToString();
            }
            else
            {
                return "";
            }
        }

        /// <summary>
        /// 轉換場地分配資訊
        /// </summary>
        private TMap MapAllocation(Innovator inn
            , Item itmAllocations
            , Item itmSites
            , bool isEdit = false
            , bool isUpload = false
            , bool linkWeight = false
            , bool noMenu = false)
        {
            int site_count = itmSites.getItemCount();

            Item itmFirstSite = null;

            if (site_count <= 0)
            {
                itmFirstSite = inn.newItem();
            }
            else
            {
                itmFirstSite = itmSites.getItemByIndex(0);
            }

            TMap map = new TMap
            {
                NoMenu = noMenu,
                IsEdit = isEdit,
                IsUpload = isUpload,
                IsLinkWeight = linkWeight,
                Rows = GetIntVal(itmFirstSite.getProperty("in_rows", "")),
                Cols = GetIntVal(itmFirstSite.getProperty("in_cols", "")),
                Sites = new Dictionary<string, TSite>(),
                Allocations = new Dictionary<string, TAllocation>(),
            };

            for (int i = 0; i < site_count; i++)
            {
                Item itmSite = itmSites.getItemByIndex(i);
                string id = itmSite.getProperty("id", "");
                string in_code = itmSite.getProperty("in_code", "");
                string in_name = itmSite.getProperty("in_name", "");
                string in_row_index = itmSite.getProperty("in_row_index", "");
                string in_col_index = itmSite.getProperty("in_col_index", "");
                string key = in_row_index + "_" + in_col_index;

                if (!map.Sites.ContainsKey(key))
                {
                    map.Sites.Add(key, new TSite
                    {
                        Key = key,
                        Id = id,
                        Code = in_code,
                        Name = in_name,
                    });
                }
            }

            int allocate_count = itmAllocations.getItemCount();

            for (int i = 0; i < allocate_count; i++)
            {
                Item itmAllocation = itmAllocations.getItemByIndex(i);
                string id = itmAllocation.getProperty("id", "");

                string in_type = itmAllocation.getProperty("in_type", "");
                string in_date_key = itmAllocation.getProperty("in_date_key", "");
                string in_date = GetDateTimeVal(itmAllocation.getProperty("in_date", ""), "yyy-MM-dd", 8);
                string chinese_date = GetDateTimeVal(itmAllocation.getProperty("in_date", ""), "chinese", 8);

                //分類用
                string in_l1 = itmAllocation.getProperty("in_l1", "");
                string in_l2 = itmAllocation.getProperty("in_l2", "");

                //場地分配
                string sid = itmAllocation.getProperty("sid", "");
                string in_row_index = itmAllocation.getProperty("in_row_index", "");
                string in_col_index = itmAllocation.getProperty("in_col_index", "");
                string skey = in_row_index + "_" + in_col_index;

                //組別分配
                string pid = itmAllocation.getProperty("pid", "");
                string pl1 = itmAllocation.getProperty("pl1", "");
                string pl2 = itmAllocation.getProperty("pl2", "");
                string pl3 = itmAllocation.getProperty("pl3", "");
                string pname = itmAllocation.getProperty("pname", "");
                string pname2 = itmAllocation.getProperty("pname2", "");
                string pname3 = itmAllocation.getProperty("pname3", "");
                string pcnt = itmAllocation.getProperty("pcnt", "");
                string ecnt = itmAllocation.getProperty("ecnt", "");

                TAllocation entity = null;
                if (map.Allocations.ContainsKey(in_date_key))
                {
                    entity = map.Allocations[in_date_key];
                }
                else
                {
                    entity = new TAllocation
                    {
                        Key = in_date_key,
                        DisplayDate = in_date,
                        ChineseDate = chinese_date,
                        Categories = new List<TCategory>(),
                        SiteGroups = new Dictionary<string, List<TGroup>>(),
                    };
                    map.Allocations.Add(in_date_key, entity);
                }

                if (in_type == "category")
                {
                    entity.Categories.Add(new TCategory
                    {
                        Id = id,
                        InL1 = in_l1,
                        InL2 = in_l2,
                        Label = in_l1 + "-" + in_l2,
                    }); ;
                }
                else
                {
                    List<TGroup> groups = null;
                    if (entity.SiteGroups.ContainsKey(skey))
                    {
                        groups = entity.SiteGroups[skey];
                    }
                    else
                    {
                        groups = new List<TGroup>();
                        entity.SiteGroups.Add(skey, groups);
                    }

                    groups.Add(new TGroup
                    {
                        Id = id,
                        SiteId = sid,
                        ProgramId = pid,
                        DateKey = in_date_key,
                        Label = pname2,
                        Label2 = pname3,
                        PCnt = pcnt,
                        ECnt = ecnt,
                        LabelCnt = pname2 + " (" + ecnt + ")",
                        in_l1 = pl1,
                        in_l2 = pl2,
                        in_l3 = pl3,
                    });
                }
            }

            return map;
        }

        /// <summary>
        /// 場地分配資料模型
        /// </summary>
        private class TMap
        {
            public bool NoMenu { get; set; }

            /// <summary>
            /// 是否為編輯狀態
            /// </summary>
            public bool IsEdit { get; set; }

            /// <summary>
            /// 是否為上傳圖片狀態
            /// </summary>
            public bool IsUpload { get; set; }

            /// <summary>
            /// 是否連結至組別過磅
            /// </summary>
            public bool IsLinkWeight { get; set; }

            /// <summary>
            /// 列數
            /// </summary>
            public int Rows { get; set; }

            /// <summary>
            /// 欄數
            /// </summary>
            public int Cols { get; set; }

            /// <summary>
            /// 場地資料
            /// </summary>
            public Dictionary<string, TSite> Sites { get; set; }

            /// <summary>
            /// 日期資料
            /// </summary>
            public Dictionary<string, TAllocation> Allocations { get; set; }
        }

        private class TSite
        {
            /// <summary>
            /// 鍵
            /// </summary>
            public string Key { get; set; }

            /// <summary>
            /// 系統編號
            /// </summary>
            public string Id { get; set; }

            /// <summary>
            /// 場地代號
            /// </summary>
            public string Code { get; set; }

            /// <summary>
            /// 場地名稱
            /// </summary>
            public string Name { get; set; }

        }

        private class TAllocation
        {
            /// <summary>
            /// 鍵
            /// </summary>
            public string Key { get; set; }

            /// <summary>
            /// 日期呈現 ex: 2020-11-16
            /// </summary>
            public string DisplayDate { get; set; }

            /// <summary>
            /// 日期呈現 ex: 109年11月20日(星期五)
            /// </summary>
            public string ChineseDate { get; set; }

            /// <summary>
            /// 當日比賽組別
            /// </summary>
            public List<TCategory> Categories { get; set; }

            /// <summary>
            /// 當日比賽場地組別
            /// </summary>
            public Dictionary<string, List<TGroup>> SiteGroups { get; set; }
        }

        private class TCategory
        {
            public string Id { get; set; }
            public string InL1 { get; set; }
            public string InL2 { get; set; }
            public string Label { get; set; }
        }

        private class TGroup
        {
            /// <summary>
            /// Allocation Id
            /// </summary>
            public string Id { get; set; }

            /// <summary>
            /// Site Id
            /// </summary>
            public string SiteId { get; set; }

            /// <summary>
            /// Program Id
            /// </summary>
            public string ProgramId { get; set; }

            /// <summary>
            /// Date Key
            /// </summary>
            public string DateKey { get; set; }

            /// <summary>
            /// 組別-量級
            /// </summary>
            public string Label { get; set; }
            public string Label2 { get; set; }

            /// <summary>
            /// 組別-人數
            /// </summary>
            public string PCnt { get; set; }

            /// <summary>
            /// 組別-場次數
            /// </summary>
            public string ECnt { get; set; }

            /// <summary>
            /// 組別-量級
            /// </summary>
            public string LabelCnt { get; set; }

            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }
        }

        //附加賽事資訊
        private void AppendMeeting(Item itmMeeting, Item itmReturn)
        {
            itmReturn.setProperty("in_title", itmMeeting.getProperty("in_title", ""));
            itmReturn.setProperty("in_battle_type", itmMeeting.getProperty("in_battle_type", ""));
            itmReturn.setProperty("in_surface_code", itmMeeting.getProperty("in_surface_code", ""));
            itmReturn.setProperty("banner_file", itmMeeting.getProperty("in_banner_photo", ""));
            itmReturn.setProperty("banner_file2", itmMeeting.getProperty("in_banner_photo2", ""));
        }

        //取得賽事資訊
        private Item GetMeeting(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string meeting_id)
        {
            string aml = @"
                <AML>
                    <Item type='In_Meeting' action='get' id='{#meeting_id}' select='in_title,in_battle_type,in_surface_code,in_banner_photo,in_banner_photo2'>
                    </Item>
                </AML>
            ".Replace("{#meeting_id}", meeting_id);

            return inn.applyAML(aml);
        }

        //取得分配場地資訊
        private Item GetMeetingAllocations(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string meeting_id)
        {
            string sql = @"
                SELECT
                    t1.*
			        , t2.id AS 'sid'
			        , t2.in_row_index
			        , t2.in_col_index
			        , t3.id    AS 'pid'
			        , t3.in_l1 AS 'pl1'
			        , t3.in_l2 AS 'pl2'
			        , t3.in_l3 AS 'pl3'
			        , t3.in_name AS 'pname'
			        , t3.in_name2 AS 'pname2'
			        , t3.in_name3 AS 'pname3'
			        , t3.in_team_count AS 'pcnt'
			        , t3.in_event_count AS 'ecnt'
                FROM
                    IN_MEETING_ALLOCATION t1 WITH(NOLOCK)
		        LEFT OUTER JOIN
			        IN_MEETING_SITE t2 WITH(NOLOCK)
			        ON t2.in_meeting = t1.in_meeting
			        AND t2.id = t1.in_site
		        LEFT OUTER JOIN
			        IN_MEETING_PROGRAM t3 WITH(NOLOCK)
			        ON t3.in_meeting = t1.in_meeting
			        AND t3.id = t1.in_program
                WHERE
                    t1.in_meeting = '{#meeting_id}'
                ORDER BY
                    t1.in_date
                    , t1.created_on
            ";

            sql = sql.Replace("{#meeting_id}", meeting_id);

            return inn.applySQL(sql);
        }

        //取得賽事場地資訊
        private Item GetMeetingSites(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string meeting_id)
        {
            string sql = @"SELECT * FROM IN_MEETING_SITE WITH(NOLOCK) WHERE in_meeting = '" + meeting_id + "'";
            return inn.applySQL(sql);
        }

        private string GetJsonValue(Newtonsoft.Json.Linq.JObject obj, string key)
        {
            Newtonsoft.Json.Linq.JToken value;
            if (obj.TryGetValue(key, out value))
            {
                return value.ToString();
            }
            return "";
        }

        private int GetIntVal(string value, int def = 0)
        {
            int result = def;
            Int32.TryParse(value, out result);
            return result;
        }

        private string GetDateTimeVal(string value, string format, int add_hours = 0)
        {
            DateTime result = DateTime.MinValue;
            DateTime.TryParse(value, out result);
            if (result != DateTime.MinValue)
            {
                if (format == "chinese")
                {
                    return ChineseDateTime(result.AddHours(add_hours));
                }
                else
                {
                    return result.AddHours(add_hours).ToString(format);
                }
            }
            else
            {
                return "";
            }
        }

        /// <summary>
        /// 轉換為民國年月日(星期)
        /// </summary>
        private string ChineseDateTime(DateTime value)
        {
            var y = value.Year - 1911;
            var m = value.Month.ToString().PadLeft(2, '0');
            var d = value.Day.ToString().PadLeft(2, '0');
            var w = "";

            switch (value.DayOfWeek)
            {
                case DayOfWeek.Monday:
                    w = "星期一";
                    break;
                case DayOfWeek.Tuesday:
                    w = "星期二";
                    break;
                case DayOfWeek.Wednesday:
                    w = "星期三";
                    break;
                case DayOfWeek.Thursday:
                    w = "星期四";
                    break;
                case DayOfWeek.Friday:
                    w = "星期五";
                    break;
                case DayOfWeek.Saturday:
                    w = "星期六";
                    break;
                case DayOfWeek.Sunday:
                    w = "星期日";
                    break;
                default:
                    break;
            }

            return y + "年" + m + "月" + d + "日" + "（" + w + "）";
        }
    }
}