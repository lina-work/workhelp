﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;
using WorkHelp.ArasDesk.Samples.SqlClient;
using System.Data;
using System.Security.Cryptography;
using System.Web.Management;
using System.Web.Caching;

namespace WorkHelp.ArasDesk.Methods.Wrestling.Common
{
    public class In_Wmg_Register_FightEvent : Item
    {
        public In_Wmg_Register_FightEvent(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 世壯運-排賽程
                日誌: 
                    - 2024-08-21: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "In_Wmg_Register_FightEvent";

            var itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                meeting_id = itmR.getProperty("meeting_id", ""),
                fight_day = itmR.getProperty("fight_day", ""),
                site_code = itmR.getProperty("site_code", ""),
                scene = itmR.getProperty("scene", ""),
            };

            cfg.itmMeeting = cfg.inn.applySQL("SELECT id, in_event_status FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            if (cfg.itmMeeting.getResult() == "") return itmR;

            cfg.in_event_status = cfg.itmMeeting.getProperty("in_event_status", "");

            switch (cfg.scene)
            {
                case "page":
                    Page(cfg, itmR);
                    break;
                case "edit-tree-no":
                    EditTreeNoList(cfg, itmR);
                    break;
                case "clear-event-status":
                    ClearEventStatus(cfg, itmR);
                    break;
                default:
                    AppendFightDayMenu(cfg, itmR);
                    AppendFightSiteMenu(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void ClearEventStatus(TConfig cfg, Item itmReturn)
        {
            var status = itmReturn.getProperty("status", "");
            var in_event_status = status == "stack" ? "NULL" : "'setted'";
            var sql = "UPDATE IN_MEETING SET in_event_status = " + in_event_status + " WHERE id = '" + cfg.meeting_id + "'";
            cfg.inn.applySQL(sql);
        }

        private void EditTreeNoList(TConfig cfg, Item itmReturn)
        {
            var value = itmReturn.getProperty("value", "");
            var evts = Newtonsoft.Json.JsonConvert.DeserializeObject<List<TEvt>>(value);
            if (evts == null || evts.Count == 0)
            {
                throw new Exception("資料不可為空值");
            }

            for (var i = 0; i < evts.Count; i++)
            {
                var evt = evts[i];
                EditTreeNo(cfg, evt);
            }

            var sql = "UPDATE IN_MEETING SET in_event_status = 'setted' WHERE id = '" + cfg.meeting_id + "'";
            cfg.inn.applySQL(sql);
        }

        private void EditTreeNo(TConfig cfg, TEvt evt)
        {
            var sql = "UPDATE IN_MEETING_PEVENT SET"
                + "  in_tree_no = '" + evt.tree_no + "'"
                + ", in_show_serial = '" + evt.tree_no + "'"
                + " WHERE id = '" + evt.id + "'";

            cfg.inn.applySQL(sql);

            //子場次全部重編
            ResetSubTreeNo(cfg, evt.id);

        }

        //子場次全部重編
        private void ResetSubTreeNo(TConfig cfg, string event_id)
        {
            string sql = "";
            Item itmSQL = null;

            sql = @"
                UPDATE t1 SET
                    t1.in_tree_no = t2.in_tree_no * 100 + t1.in_sub_id
	                , t1.in_period = t2.in_period
	                , t1.in_medal = t2.in_medal
	                , t1.in_show_site = t2.in_show_site
	                , t1.in_show_serial = t2.in_show_serial * 100 + t1.in_sub_id
                FROM 
                    IN_MEETING_PEVENT t1 WITH(NOLOCK) 
                INNER JOIN
                    IN_MEETING_PEVENT t2 WITH(NOLOCK) 
                    ON t2.id = t1.in_parent
                WHERE
                    t1.in_parent = '{#event_id}'
                    AND ISNULL(t2.in_tree_no, 0) > 0
            ";

            sql = sql.Replace("{#event_id}", event_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            itmSQL = cfg.inn.applySQL(sql);
        }

        private void SetEventList(TConfig cfg, Item itmReturn)
        {
            cfg.colors = GenerateColorList();
            cfg.CodeList = new List<TEvt>();
            cfg.WaitList = new List<TEvt>();

            var items = cfg.in_event_status == "" ? GetEventItems(cfg) : GetEventSorts(cfg);
            var count = items.getItemCount();
            if (count == 0) return;

            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var evt = MapEventRow(cfg, item, i);
                if (evt.tree_no == "" || evt.tree_no == "0")
                {
                    cfg.WaitList.Add(evt);
                }
                else
                {
                    cfg.CodeList.Add(evt);
                }
            }
        }

        private Dictionary<string, int> GetSectMap(TConfig cfg)
        {
            var map = new Dictionary<string, int>();
            var sql = @"
                SELECT 
	                t2.id
	                , t2.in_short_name
                FROM
	                IN_MEETING_ALLOCATION t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PROGRAM t2 WITH(NOLOCK)
	                ON t2.id = t1.in_program
                WHERE
                    t1.in_meeting = '{#meeting_id}'
				ORDER BY
					t1.created_on
			";
            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var sect_name = item.getProperty("in_short_name", "");
                map.Add(sect_name, i);
            }
            return map;
        }

        private void AppendFightDayMenu(TConfig cfg, Item itmReturn)
        {
            var sql = @"
                SELECT DISTINCT 
	                t1.in_date_key AS 'in_fight_day'
                FROM
	                IN_MEETING_ALLOCATION t1 WITH(NOLOCK)
                WHERE
                    t1.in_meeting = '{#meeting_id}'
				ORDER BY
					t1.in_date_key
            ";
            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);
            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            var today = DateTime.Now.ToString("yyyy-MM-dd");
            var selected = "";

            var itmEmpty = cfg.inn.newItem();
            itmEmpty.setType("inn_day");
            itmEmpty.setProperty("value", "");
            itmEmpty.setProperty("text", "比賽日期");
            itmReturn.addRelationship(itmEmpty);

            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var value = item.getProperty("in_fight_day", "");

                item.setType("inn_day");
                item.setProperty("value", value);
                item.setProperty("text", value);
                itmReturn.addRelationship(item);

                if (i == 0) selected = value;
                if (today == value) selected = today;
            }

            if (cfg.fight_day == "")
            {
                cfg.fight_day = selected;
                itmReturn.setProperty("fight_day", selected);
            }
        }

        private void AppendFightSiteMenu(TConfig cfg, Item itmReturn)
        {
            var sql = @"
                SELECT 
					t1.id
					, t1.in_code
					, t1.in_code_en
					, t1.in_name
                FROM
	                IN_MEETING_SITE t1 WITH(NOLOCK)
                WHERE
                    t1.in_meeting = '{#meeting_id}'
				ORDER BY
					t1.in_code
            ";
            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);
            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            var selected = "";

            var itmEmpty = cfg.inn.newItem();
            itmEmpty.setType("inn_site");
            itmEmpty.setProperty("value", "");
            itmEmpty.setProperty("text", "請選擇");
            itmReturn.addRelationship(itmEmpty);

            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var value = item.getProperty("in_code", "");
                var text = item.getProperty("in_name", "");

                item.setType("inn_site");
                item.setProperty("value", value);
                item.setProperty("text", text);
                itmReturn.addRelationship(item);

                if (i == 0) selected = value;
            }

            if (cfg.site_code == "")
            {
                cfg.site_code = selected;
                itmReturn.setProperty("site_code", selected);
            }
        }

        private void Page(TConfig cfg, Item itmReturn)
        {
            cfg.map = GetSectMap(cfg);

            SetEventList(cfg, itmReturn);

            var contents = new StringBuilder();
            contents.Append("<div class='col-12 col-lg-6'>");
            contents.Append("<h3>場次資訊</h3>");
            contents.Append(GetTableAttribute("data-table", "number"));
            contents.Append("<thead>");
            contents.Append("  <tr>");
            contents.Append(GenerateHeads(cfg));
            contents.Append("  </tr>");
            contents.Append("</thead>");
            contents.Append("<tbody class='inno-sort-container'>");
            contents.Append(GenerateBody(cfg, cfg.CodeList));
            contents.Append("</tbody>");
            contents.Append("</table>");
            contents.Append("</div>");

            contents.Append("<div class='col-12 col-lg-6'>");
            contents.Append("<h3>待選場次</h3>");
            contents.Append(GetTableAttribute("empty-table", "wait"));
            contents.Append("<thead>");
            contents.Append("  <tr>");
            contents.Append(GenerateHeads(cfg));
            contents.Append("  </tr>");
            contents.Append("</thead>");
            contents.Append("<tbody class='inno-sort-container'>");
            contents.Append("<tr class='inno-static-row' style='background-color: white'>");
            contents.Append("  <td class='text-center' colspan='5'>暫存區</td>");
            contents.Append("</tr>");
            contents.Append(GenerateBody(cfg, cfg.WaitList));
            contents.Append("</tbody>");
            contents.Append("</table>");
            contents.Append("</div>");

            itmReturn.setProperty("inn_tables", contents.ToString());
        }

        private StringBuilder GenerateHeads(TConfig cfg)
        {
            var headCss = "text-center";
            var head = new StringBuilder();
            head.Append("<th class='" + headCss + "'>組序</th>");
            head.Append("<th class='" + headCss + "'>量級</th>");
            head.Append("<th class='" + headCss + "'>人隊數</th>");
            head.Append("<th class='" + headCss + "'>賽別</th>");
            // head.Append("<th class='" + headCss + "'>選手</th>");
            head.Append("<th class='" + headCss + "'>次序</th>");
            return head;
        }

        private StringBuilder GenerateBody(TConfig cfg, List<TEvt> evts)
        {
            var bodyCss = "text-center";
            var body = new StringBuilder();
            for (var i = 0; i < evts.Count; i++)
            {
                var evt = evts[i];
                body.Append("<tr class='inno-drag-row' data-id='" + evt.id + "' style='background-color: " + evt.color + "'>");
                body.Append("  <td class='" + bodyCss + "'>級組" + (evt.sect_index + 1) + "</td>");
                body.Append("  <td class='" + bodyCss + "'>" + evt.sect_name + "</td>");
                body.Append("  <td class='" + bodyCss + "'>" + evt.team_count + "</td>");
                body.Append("  <td class='" + bodyCss + "'>" + evt.text + "</td>");
                // body.Append("  <td class='" + bodyCss + "'>" + evt.players + "</td>");
                body.Append("  <td class='" + bodyCss + "'>" + evt.number + "</td>");
                body.Append("</tr>");
            }
            return body;
        }

        private TEvt MapEventRow(TConfig cfg, Item x, int index)
        {
            var evt = NewEvtRow(cfg, x);
            if (!cfg.map.ContainsKey(evt.sect_name)) throw new Exception("組別異常");

            evt.sect_index = cfg.map[evt.sect_name];
            if (evt.sect_index >= cfg.colors.Count)
            {
                evt.color = "#FFFFFF";
            }
            else
            {
                evt.color = cfg.colors[evt.sect_index];
            }
            evt.number = index + 1;

            switch (evt.w1)
            {
                case "CRS6": MapCRS6(cfg, evt); break;
                case "CRS7": MapCRS7(cfg, evt); break;

                case "RBN2": MapRBN2(cfg, evt); break;
                case "RBN3": MapRBN3(cfg, evt); break;
                case "RBN4": MapRBN4(cfg, evt); break;
                case "RBN5": MapRBN5(cfg, evt); break;
                case "M128": MapMain(cfg, evt); break;
                case "M064": MapMain(cfg, evt); break;
                case "M032": MapMain(cfg, evt); break;
                case "M016": MapMain(cfg, evt); break;
                case "M008": MapMain(cfg, evt); break;
                case "M004": MapMain(cfg, evt); break;
                case "M002": MapMain(cfg, evt); break;

                case "R128": MapRepechage(cfg, evt); break;
                case "R064": MapRepechage(cfg, evt); break;
                case "R032": MapRepechage(cfg, evt); break;
                case "R016": MapRepechage(cfg, evt); break;
                case "R008": MapRepechage(cfg, evt); break;
                case "R004": MapRepechage(cfg, evt); break;
                case "R002": MapRepechage(cfg, evt); break;

                case "CA01": MapCA01(cfg, evt); break;
                case "CA02": MapCA02(cfg, evt); break;
                case "CB01": MapCB01(cfg, evt); break;
                case "RNK78": MapRNK78(cfg, evt); break;
            }

            ResetPlayers(cfg, evt);

            return evt;
        }

        private void ResetPlayers(TConfig cfg, TEvt evt)
        {
            if (evt.is_team)
            {
                evt.players = evt.f1_org + (evt.f1_team == "" ? "" : " " + evt.f1_team)
                    + " vs "
                    + evt.f2_org + (evt.f2_team == "" ? "" : " " + evt.f2_team)
                    ;
            }
            else
            {
                evt.players = evt.f1_name + (evt.f1_org == evt.f1_name ? "" : "(" + evt.f1_org + ")")
                    + " vs "
                    + evt.f2_name + (evt.f2_org == evt.f2_name ? "" : "(" + evt.f2_org + ")")
                    ;
            }
        }

        private void MapCA01(TConfig cfg, TEvt evt)
        {
            evt.category = "挑戰賽";
            evt.content = "3 vs 2";
            evt.text = evt.category + "-" + evt.content;
        }
        private void MapCA02(TConfig cfg, TEvt evt)
        {
            evt.category = "挑戰賽";
            evt.content = "2 vs 1";
            evt.text = evt.category + "-" + evt.content;
        }
        private void MapCB01(TConfig cfg, TEvt evt)
        {
            evt.category = "挑戰賽";
            evt.content = "加賽";
            evt.text = evt.category + "-" + evt.content;
        }
        private void MapRNK78(TConfig cfg, TEvt evt)
        {
            evt.category = "Final 7-8";
            evt.content = "";
            evt.text = evt.category;
        }
        private void MapRepechage(TConfig cfg, TEvt evt)
        {
            evt.category = "敗部";
            var a = evt.w1.TrimStart('R').TrimStart('0');
            var b = evt.w2.TrimStart('0');
            if (a == "2")
            {
                evt.content = "三四名";
                evt.text = "三四名";
            }
            else if (a == "4")
            {
                var ws = b == "1" ? "W" : "E";
                evt.content = "銅牌戰-" + ws;
                evt.text = evt.category + "-" + evt.content;
            }
            else if (a == "8")
            {
                var ws = b == "1" ? "W" : "E";
                evt.content = "第 " + b + " 場" + "-" + ws;
                evt.text = evt.category + "-" + evt.content;
            }
            else
            {
                var ws = b == "1" || b == "2" ? "W" : "E";
                evt.content = "第 " + b + " 場" + "-" + ws;
                evt.text = evt.category + "-" + evt.content;
            }
        }

        private void MapMain(TConfig cfg, TEvt evt)
        {
            evt.category = "勝部";
            var a = evt.w1.TrimStart('M').TrimStart('0');
            var b = evt.w2.TrimStart('0');
            if (a == "2")
            {
                evt.content = "金牌戰";
                evt.text = "金牌戰";
            }
            else if (a == "4")
            {
                var ws = b == "1" ? "W" : "E";
                evt.content = "準決賽-" + ws;
                evt.text = evt.category + "-" + evt.content;
            }
            else
            {
                evt.content = a + "強-第 " + b + " 場";
                evt.text = evt.category + "-" + evt.content;
            }
            if (evt.is_cross)
            {
                evt.text = "分組-" + evt.text;
            }
        }

        private void MapRBN2(TConfig cfg, TEvt evt)
        {
            var tag = evt.is_team ? "隊" : "人";
            evt.category = "兩" + tag + "對戰";
            switch (evt.w2)
            {
                case "01": evt.content = "第 1 場"; break;
                case "02": evt.content = "第 2 場"; break;
                case "03": evt.content = "加賽"; break;
                default: evt.content = "異常"; break;
            }
            evt.text = evt.category + " - " + evt.content;
        }

        private void MapRBN3(TConfig cfg, TEvt evt)
        {
            evt.category = "循環賽";
            switch (evt.w2)
            {
                case "01": evt.content = "第 1 場"; break;
                case "02": evt.content = "第 2 場"; break;
                case "03": evt.content = "第 3 場"; break;
                default: evt.content = "異常"; break;
            }
            evt.text = evt.category + " - " + evt.content;
        }

        private void MapRBN4(TConfig cfg, TEvt evt)
        {
            evt.category = "循環賽";
            switch (evt.w2)
            {
                case "01": evt.content = "第 1 場 / R1"; break;
                case "02": evt.content = "第 2 場 / R1"; break;
                case "03": evt.content = "第 3 場 / R2"; break;
                case "04": evt.content = "第 4 場 / R2"; break;
                case "05": evt.content = "第 5 場 / R3"; break;
                case "06": evt.content = "第 6 場 / R3"; break;
                default: evt.content = "異常"; break;
            }
            evt.text = evt.category + " - " + evt.content;
        }

        private void MapRBN5(TConfig cfg, TEvt evt)
        {
            evt.category = "循環賽";
            switch (evt.w2)
            {
                case "01": evt.content = "第 1 場 / R1"; break;
                case "02": evt.content = "第 2 場 / R1"; break;
                case "03": evt.content = "第 3 場 / R2"; break;
                case "04": evt.content = "第 4 場 / R2"; break;
                case "05": evt.content = "第 5 場 / R3"; break;
                case "06": evt.content = "第 6 場 / R3"; break;
                case "07": evt.content = "第 7 場 / R4"; break;
                case "08": evt.content = "第 8 場 / R4"; break;
                case "09": evt.content = "第 9 場 / R5"; break;
                case "10": evt.content = "第 10 場 / R5"; break;
                default: evt.content = "異常"; break;
            }
            evt.text = evt.category + " - " + evt.content;
        }

        private void MapCRS6(TConfig cfg, TEvt evt)
        {
            evt.category = "交叉對戰";
            switch (evt.w2)
            {
                case "01": evt.content = "A組01 / R1"; break;
                case "04": evt.content = "B組01 / R1"; break;
                case "02": evt.content = "A組02 / R2"; break;
                case "05": evt.content = "B組02 / R2"; break;
                case "03": evt.content = "A組03 / R3"; break;
                case "06": evt.content = "B組03 / R3"; break;
                default: evt.content = "異常"; break;
            }
            evt.text = evt.category + " - " + evt.content;
        }

        private void MapCRS7(TConfig cfg, TEvt evt)
        {
            evt.category = "交叉對戰";
            switch (evt.w2)
            {
                case "01": evt.content = "A組01 / R1"; break;
                case "02": evt.content = "A組02 / R1"; break;
                case "07": evt.content = "B組01 / R1"; break;

                case "03": evt.content = "A組03 / R2"; break;
                case "04": evt.content = "A組04 / R2"; break;
                case "08": evt.content = "B組02 / R2"; break;

                case "05": evt.content = "A組05 / R3"; break;
                case "06": evt.content = "A組06 / R3"; break;
                case "09": evt.content = "B組03 / R3"; break;

                default: evt.content = "異常"; break;
            }
            evt.text = evt.category + " - " + evt.content;
        }

        private string GetTableAttribute(string table_name, string status)
        {
            return "<table id='" + table_name + "'"
                + " class='table table-hover table-bordered table-rwd rwd rwdtable'"
                + " data-toggle='table'"
                + " data-show-toggle='false'"
                + " data-show-columns='false'"
                + " data-search-align='left'"
                + " data-sort-stable='true'"
                + " data-search='false'"
                + " data-tree-status='" + status + "'"
                + ">";
        }

        private Item GetEventItems(TConfig cfg)
        {
            var sql = @"
                SELECT 
	                t2.id
	                , t2.in_fight_day
	                , t2.in_site_code
	                , t2.in_l1
	                , t2.in_l2
	                , t2.in_l3
	                , t2.in_short_name
	                , t2.in_team_count
	                , t2.in_event_count
	                , t3.id AS 'event_id'
	                , t3.in_tree_id
	                , t3.in_fight_id
	                , t3.in_tree_no
	                , t3.in_tree_sno
	                , t3.in_site_code
	                , t11.in_sign_no		AS 'f1_no'
	                , t21.map_short_org		AS 'f1_org'
	                , t21.in_name			AS 'f1_name'
	                , t21.in_team			AS 'f1_team'
	                , t12.in_sign_no		AS 'f2_no'
	                , t22.map_short_org		AS 'f2_org'
	                , t22.in_name			AS 'f2_name'
	                , t22.in_team			AS 'f2_team'
                FROM
	                IN_MEETING_ALLOCATION t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PROGRAM t2 WITH(NOLOCK)
	                ON t2.id = t1.in_program
                INNER JOIN
	                IN_MEETING_PEVENT t3 WITH(NOLOCK)
	                ON t3.source_id = t1.in_program
                INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t11 WITH(NOLOCK)
	                ON t11.source_id = t3.id
	                AND t11.in_sign_foot = 1
                INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t12 WITH(NOLOCK)
	                ON t12.source_id = t3.id
	                AND t12.in_sign_foot = 2
                LEFT OUTER JOIN
	                VU_MEETING_PTEAM t21
	                ON t21.source_id = t1.in_program
	                AND t21.in_sign_no = t11.in_sign_no
	                AND ISNULL(t21.in_type, '') IN ('', 'p')
                LEFT OUTER JOIN
	                VU_MEETING_PTEAM t22
	                ON t22.source_id = t1.in_program
	                AND t22.in_sign_no = t12.in_sign_no
	                AND ISNULL(t22.in_type, '') IN ('', 'p')
                WHERE
	                t1.in_meeting = '{#meeting_id}'
					AND t3.in_date_key = '{#fight_day}'
					AND t3.in_site_code = '{#site_code}'
	                AND ISNULL(t3.in_type, '') IN ('', 'p')
	                AND ISNULL(t3.in_tree_sno, 0) > 0
                ORDER BY
	                t1.created_on
	                , t3.in_tree_no
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#fight_day}", cfg.fight_day)
                .Replace("{#site_code}", cfg.site_code);

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        private Item GetEventSorts(TConfig cfg)
        {
            var sql = @"
                SELECT 
	                t2.id
	                , t2.in_fight_day
	                , t2.in_site_code
	                , t2.in_l1
	                , t2.in_l2
	                , t2.in_l3
	                , t2.in_short_name
	                , t2.in_team_count
	                , t2.in_event_count
	                , t3.id AS 'event_id'
	                , t3.in_tree_id
	                , t3.in_fight_id
	                , t3.in_tree_no
	                , t3.in_tree_sno
	                , t3.in_round
	                , t3.in_site_code
	                , t11.in_sign_no		AS 'f1_no'
	                , t21.map_short_org		AS 'f1_org'
	                , t21.in_name			AS 'f1_name'
	                , t21.in_team			AS 'f1_team'
	                , t12.in_sign_no		AS 'f2_no'
	                , t22.map_short_org		AS 'f2_org'
	                , t22.in_name			AS 'f2_name'
	                , t22.in_team			AS 'f2_team'
                FROM
	                IN_MEETING_ALLOCATION t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PROGRAM t2 WITH(NOLOCK)
	                ON t2.id = t1.in_program
                INNER JOIN
	                IN_MEETING_PEVENT t3 WITH(NOLOCK)
	                ON t3.source_id = t1.in_program
                INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t11 WITH(NOLOCK)
	                ON t11.source_id = t3.id
	                AND t11.in_sign_foot = 1
                INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t12 WITH(NOLOCK)
	                ON t12.source_id = t3.id
	                AND t12.in_sign_foot = 2
                LEFT OUTER JOIN
	                VU_MEETING_PTEAM t21
	                ON t21.source_id = t1.in_program
	                AND t21.in_sign_no = t11.in_sign_no
	                AND ISNULL(t21.in_type, '') IN ('', 'p')
                LEFT OUTER JOIN
	                VU_MEETING_PTEAM t22
	                ON t22.source_id = t1.in_program
	                AND t22.in_sign_no = t12.in_sign_no
	                AND ISNULL(t22.in_type, '') IN ('', 'p')
                WHERE
	                t1.in_meeting = '{#meeting_id}'
					AND t3.in_date_key = '{#fight_day}'
					AND t3.in_site_code = '{#site_code}'
	                AND ISNULL(t3.in_type, '') IN ('', 'p')
	                AND ISNULL(t3.in_tree_sno, 0) > 0
                ORDER BY
	                t3.in_tree_no
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#fight_day}", cfg.fight_day)
                .Replace("{#site_code}", cfg.site_code);

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string meeting_id { get; set; }
            public string fight_day { get; set; }
            public string site_code { get; set; }
            public string scene { get; set; }
            public Dictionary<string, int> map { get; set; }
            public List<string> colors { get; set; }
            public Item itmMeeting { get; set; }
            public string in_event_status { get; set; }
            public List<TEvt> CodeList { get; set; }
            public List<TEvt> WaitList { get; set; }
        }

        private class TEvt
        {
            public string id { get; set; }
            public string fight_day { get; set; }
            public string site_code { get; set; }
            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }
            public string battle_type { get; set; }
            public string sect_name { get; set; }
            public int sect_index { get; set; }
            public int team_count { get; set; }
            public string fight_id { get; set; }
            public string tree_id { get; set; }
            public string tree_no { get; set; }

            public string f1_org { get; set; }
            public string f1_name { get; set; }
            public string f1_team { get; set; }
            public string f2_org { get; set; }
            public string f2_name { get; set; }
            public string f2_team { get; set; }

            public string category { get; set; }
            public string content { get; set; }
            public string text { get; set; }
            public string w1 { get; set; }
            public string w2 { get; set; }
            public string players { get; set; }
            public bool is_cross { get; set; }
            public bool is_team { get; set; }

            public string color { get; set; }
            public int number { get; set; }
        }

        private TEvt NewEvtRow(TConfig cfg, Item x)
        {
            var evt = new TEvt
            {
                id = x.getProperty("event_id", ""),
                fight_day = x.getProperty("in_fight_day", ""),
                site_code = x.getProperty("in_site_code", ""),
                in_l1 = x.getProperty("in_l1", ""),
                battle_type = x.getProperty("in_battle_type", ""),
                sect_name = x.getProperty("in_short_name", ""),
                team_count = GetInt(x.getProperty("in_team_count", "0")),
                fight_id = x.getProperty("in_fight_id", ""),
                tree_no = x.getProperty("in_tree_no", ""),
                f1_org = x.getProperty("f1_org", ""),
                f1_name = x.getProperty("f1_name", ""),
                f1_team = x.getProperty("f1_team", ""),
                f2_org = x.getProperty("f2_org", ""),
                f2_name = x.getProperty("f2_name", ""),
                f2_team = x.getProperty("f2_team", ""),
                category = "",
                content = "",
                text = "",
                players = "",
                w1 = "",
                w2 = "",
            };

            evt.is_cross = evt.battle_type.Contains("Players");
            evt.is_team = evt.in_l1 == "團體組";

            var words = evt.fight_id.Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);
            evt.w1 = words[0];
            evt.w2 = words.Length > 1 ? words[1] : "";

            return evt;
        }

        private List<string> GenerateColorList()
        {
            var list = new List<string>
            {
                "#F0F5F7",//1
                "#E8F5EA",//2
                "#FCDBDB",//3
                "#CBE3F5",//4
                "#FFDDAA",//5
                "#FFEE99",//6
                "#FFFFBB",//7
                "#EEFFBB",//8
                "#CCFF99",//9
                "#99FF99",//10
                "#BBFFEE",//11
                "#AAFFEE",//12
                "#99FFFF",//13
                "#CCEEFF",//14
                "#CCDDFF",//15
                "#CCCCFF",//16
                "#CCBBFF",//17
                "#D1BBFF",//18
                "#E8CCFF",//19
                "#F0BBFF",//20
                "#FFB3FF",//21
                "#DDDDDD",//22
                "#FF88C2",//23
                "#FF8888",//24
                "#FFA488",//25
                "#FFBB66",//26
                "#FFDD55",//27
                "#FFFF77",//28
                "#DDFF77",//29
                "#BBFF66",//30
                "#7FFF00",//31
                "#B8860B",//32
                "#FFA500",//33
            };
            return list;
        }
        private int GetInt(string value, int def = 0)
        {
            int result;
            int.TryParse(value, out result);
            return result;
        }
    }
}
