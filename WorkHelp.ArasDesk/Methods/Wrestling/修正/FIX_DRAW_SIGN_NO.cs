﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;
using System.ComponentModel;
using Aras.Server.Core;

namespace WorkHelp.ArasDesk.Methods.Wrestling.修正
{
    public class FIX_DRAW_SIGN_NO : Item
    {
        public FIX_DRAW_SIGN_NO(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 補齊缺少的對戰表
                日誌: 
                    - 2024-10-09: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "In_Resume_Query";

            var itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                scene = itmR.getProperty("scene", ""),
            };

            cfg.scene = "Fix_DrawSheetEvent";
            switch (cfg.scene)
            {
                case "Fix_DrawSheetEvent":
                    Fix_DrawSheet(cfg, itmR);
                    break;
                case "Fix_DrawSignNo":
                    Fix_DrawSignNo(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void Fix_DrawSheet(TConfig cfg, Item itmReturn)
        {
            var itmData = cfg.inn.applySQL("SELECT * FROM IN_MEETING_DRAWSHEET WITH(NOLOCK) WHERE id = '6ABF10BDC08D4DC18B77DB05B6875621'");

            var min = 35;
            var max = 35;
            for (var i = min; i <= max; i++)
            {
                var in_mode = "uww";
                var in_team_count = i;
                var in_event_count = 31 + (in_team_count - 32) + 8;

                var itmNew = cfg.inn.newItem("In_Meeting_DrawSheet", "merge");
                itmNew.setAttribute("where", "in_mode = '" + in_mode + "' and in_team_count = '" + in_team_count + "'");
                itmNew.setProperty("in_mode", in_mode);
                itmNew.setProperty("in_team_count", in_team_count.ToString());
                itmNew.setProperty("in_event_count", in_event_count.ToString());
                itmNew.setProperty("in_title", itmData.getProperty("in_title", ""));
                itmNew.setProperty("in_day", itmData.getProperty("in_day", ""));
                itmNew.setProperty("in_weight", itmData.getProperty("in_weight", ""));
                itmNew.setProperty("in_address", itmData.getProperty("in_address", ""));
                itmNew.setProperty("in_site", itmData.getProperty("in_site", ""));
                itmNew.setProperty("in_count", itmData.getProperty("in_count", ""));
                itmNew.setProperty("in_cancel_reference", itmData.getProperty("in_cancel_reference", ""));
                itmNew = itmNew.apply();

                var source_id = itmNew.getProperty("id", "");
                Fix_DrawSheetEvents(cfg, source_id, in_team_count, in_event_count);
            }
        }

        private void Fix_DrawSheetEvents(TConfig cfg, string source_id, int in_team_count, int in_event_count)
        {
            var arr = GetFightIdList(cfg, in_team_count);

            for (var i = 1; i <= in_event_count; i++)
            {
                var sort_order = i;
                var in_fight_id = arr[i - 1];
                var in_word = GetNumberWord(sort_order);

                var itmNew = cfg.inn.newItem("In_Meeting_DrawSheet_Event", "merge");
                itmNew.setAttribute("where", "source_id = '" + source_id + "' and sort_order = '" + sort_order + "'");
                itmNew.setProperty("source_id", source_id);
                itmNew.setProperty("sort_order", sort_order.ToString());
                itmNew.setProperty("in_fight_id", in_fight_id);
                itmNew.setProperty("in_word", in_word);
                itmNew.apply();
            }
        }

        private static string[] NumStr = new string[] { "一", "二", "三", "四", "五", "六", "七", "八", "九" };

        private string GetNumberWord(int sort_order)
        {
            if (sort_order < 10) return NumStr[sort_order - 1];
            if (sort_order == 10) return "十";
            if (sort_order == 20) return "二十";
            if (sort_order == 30) return "三十";
            if (sort_order == 40) return "四十";
            if (sort_order == 50) return "五十";
            if (sort_order == 60) return "六十";
            if (sort_order == 70) return "七十";
            if (sort_order == 80) return "八十";
            if (sort_order == 90) return "九十";

            var a = sort_order / 10;
            var b = sort_order % 10;
            var c = NumStr[b - 1];

            if (a == 1)
            {
                return "十" + c;
            }
            else
            {
                return NumStr[a - 1] + c;
            }
        }

        private List<string> GetFightIdList(TConfig cfg, int in_team_count)
        {
            var list = new List<string>();
            var sql = @"
                SELECT 
	                DISTINCT in_fight_id
                FROM 
	                IN_MEETING_DRAWSIGNNO WITH(NOLOCK)
                WHERE 
	                in_mode = 'uww' 
	                AND in_team_count = {#in_team_count}
	                AND in_fight_id LIKE 'M064%'
	                AND in_available = 2
                ORDER BY
	                in_fight_id
            ";

            sql = sql.Replace("{#in_team_count}", in_team_count.ToString());
            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var in_fight_id = item.getProperty("in_fight_id", "");
                list.Add(in_fight_id);
            }

            list.Add("M032-01");
            list.Add("M032-02");
            list.Add("M032-03");
            list.Add("M032-04");
            list.Add("M032-05");
            list.Add("M032-06");
            list.Add("M032-07");
            list.Add("M032-08");
            list.Add("M032-09");
            list.Add("M032-10");
            list.Add("M032-11");
            list.Add("M032-12");
            list.Add("M032-13");
            list.Add("M032-14");
            list.Add("M032-15");
            list.Add("M032-16");
            list.Add("M016-01");
            list.Add("M016-02");
            list.Add("M016-03");
            list.Add("M016-04");
            list.Add("M016-05");
            list.Add("M016-06");
            list.Add("M016-07");
            list.Add("M016-08");
            list.Add("M008-01");
            list.Add("M008-02");
            list.Add("M008-03");
            list.Add("M008-04");
            list.Add("M004-01");
            list.Add("M004-02");
            list.Add("R032-01");
            list.Add("R032-02");
            list.Add("R016-01");
            list.Add("R016-02");
            list.Add("R008-01");
            list.Add("R008-02");
            list.Add("R004-01");
            list.Add("R004-02");
            list.Add("M002-01");
            return list;
        }

        private void Fix_DrawSignNo(TConfig cfg, Item itmReturn)
        {
            var sql = @"
                SELECT 
    	            *  
                FROM 
    	            IN_MEETING_DRAWSIGNNO
                WHERE 
    	            in_mode = 'knockout' 
    	            AND in_team_count > 32
                ORDER BY 
    	            in_no
            ";

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var in_mode = "uww";
                var in_no = item.getProperty("in_no", "");

                var itmNew = cfg.inn.newItem("In_Meeting_DrawSignNo", "merge");
                itmNew.setAttribute("where", "in_mode = '" + in_mode + "' and in_no = '" + in_no + "'");
                itmNew.setProperty("in_mode", in_mode);
                itmNew.setProperty("in_no", in_no);
                itmNew.setProperty("in_team_count", item.getProperty("in_team_count", ""));
                itmNew.setProperty("in_fight_id", item.getProperty("in_fight_id", ""));
                itmNew.setProperty("in_sign_foot", item.getProperty("in_sign_foot", ""));
                itmNew.setProperty("in_sign_no", item.getProperty("in_sign_no", ""));
                itmNew.setProperty("in_bypass_status", item.getProperty("in_bypass_status", ""));
                itmNew.setProperty("in_f1_no", item.getProperty("in_f1_no", ""));
                itmNew.setProperty("in_f2_no", item.getProperty("in_f2_no", ""));
                itmNew.setProperty("in_available", item.getProperty("in_available", ""));
                itmNew.apply();
            }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string scene { get; set; }
        }
    }
}