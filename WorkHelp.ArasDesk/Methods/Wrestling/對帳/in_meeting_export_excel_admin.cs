﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;
using System.Globalization;
using System.Management.Instrumentation;
using System.Data;

namespace WorkHelp.ArasDesk.Methods.Wrestling.對帳
{
    public class AAA_Method : Item
    {
        public AAA_Method(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 學員清冊 (Admin 專用)
                說明:跟一般教練所顯示的方式不同
                    教練: 依據[所屬群組]區分各[L1]並依[性別]放在不同工作頁(所屬單位_L1_姓別)
                    AdminL 第一頁總表依據[所屬群組]算出各L1數量, 之後工作頁則不分群組依據[L1]放一起
                參數:
                    in_value: meeting_excel
                    --export
                    --template
                輸入:meeting_id
                輸出:
                    excel的位置
                位置:
                    1.meeting_import.html
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]in_meeting_export_excel_admin";

            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            string aml = "";
            string sql = "";


            string strError = "";
            string strUserId = inn.getUserID();

            Item itmR = this;

            string meeting_id = this.getProperty("meeting_id", "");
            string export_type = this.getProperty("export_type", "excel");

            if (meeting_id == "")
            {
                throw new Exception("賽事 id 不得為空白");
            }

            Item tmp = null;

            //如果是屬於 ACT_GymOwner 則只能看到自己道館的資料
            string SQL_OnlyMyGym = "";
            aml = "<AML>" +
                "<Item type='In_Resume' action='get'>" +
                "<in_user_id>" + strUserId + "</in_user_id>" +
                "</Item></AML>";
            Item Resume = inn.applyAML(aml);

            if (CCO.Permissions.IdentityListHasId(Aras.Server.Security.Permissions.Current.IdentitiesList, "7AC8B33DA0F246DDA70BB244AB231E29"))
            {
                SQL_OnlyMyGym = "";
            }

            bool IsTemplate = false;

            aml = "<AML>" +
                "<Item type='In_Meeting' action='get' id='" + meeting_id + "'>" +
                "<Relationships>" +

                "<Item type='In_Meeting_Surveys' action='get' select='related_id(id,in_questions,in_request,in_selectoption,in_question_type,in_property,in_expense,in_is_pkey)'>" +
                "<in_surveytype>1</in_surveytype>" +
                "<in_property condition='in'>'in_l1','in_l2','in_l3'</in_property>" +
                "</Item>" +


                "</Relationships>" +
            "</Item></AML>";

            //1.取得課程主檔
            Item Meeting = inn.applyAML(aml);
            //CCO.Utilities.WriteDebug(strMethodName, "2");

            //活動類型(game(比賽),seminar(講習)) 
            if (Meeting.getProperty("in_meeting_type", "") == "seminar")
            {
                Item itmApply = this.apply("in_meeting_export_exce_seminar");
                itmApply.setProperty("meeting_id", Meeting.getID());
                itmApply.setProperty("in_register_url", Meeting.getProperty("in_register_url", ""));
                itmApply.setProperty("inn_meeting_admin", "1");
                return itmApply;
            }

            //建立 Students 的欄位
            Dictionary<string, Item> StudentFields = new Dictionary<string, Item>();
            int ColIndex = 1;

            //報名表問項
            string ExpenseProperty = ""; //紀錄本賽事的費用層次
            int Meeting_Surveys_Count = Meeting.getRelationships("In_Meeting_Surveys").getItemCount();
            for (int i = 0; i < Meeting_Surveys_Count; i++)
            {
                Item In_Meeting_Survey = Meeting.getRelationships("In_Meeting_Surveys").getItemByIndex(i);
                tmp = In_Meeting_Survey.getPropertyItem("related_id");
                tmp.setProperty("col_index", (ColIndex++).ToString());
                tmp.setProperty("inn_id", In_Meeting_Survey.getProperty("related_id", ""));
                StudentFields.Add(tmp.getProperty("inn_id"), tmp);

                if (tmp.getProperty("in_expense", "") == "1")
                    ExpenseProperty = tmp.getProperty("in_property", ""); //可能是 in_l1, in_l2, in_l3

            }
            Meeting_Surveys_Count++;
            tmp = inn.newItem("In_Meeting_Surveys");
            tmp.setProperty("inn_id", "expense");
            tmp.setProperty("in_questions", "金額");
            tmp.setProperty("in_request", "0");
            tmp.setProperty("col_index", (ColIndex++).ToString());
            tmp.setProperty("in_selectoption", "");
            tmp.setProperty("in_question_type", "number");
            StudentFields.Add(tmp.getProperty("inn_id"), tmp);

            //開始整理EXCEL Template
            aml = "<AML>" +
                "<Item type='In_Variable' action='get'>" +
                "<in_name>meeting_excel</in_name>" +
                "<Relationships>" +
                "<Item type='In_Variable_Detail' action='get'/>" +
                "</Relationships>" +
                "</Item></AML>";
            Item Vairable = inn.applyAML(aml);

            string Template_Path = "";
            string Export_Path = "";
            for (int i = 0; i < Vairable.getRelationships("In_Variable_Detail").getItemCount(); i++)
            {
                Item In_Variable_Detail = Vairable.getRelationships("In_Variable_Detail").getItemByIndex(i);
                if (In_Variable_Detail.getProperty("in_name", "") == "template_path")
                    Template_Path = In_Variable_Detail.getProperty("in_value", "");

                if (In_Variable_Detail.getProperty("in_name", "") == "export_path")
                    Export_Path = In_Variable_Detail.getProperty("in_value", "");
                if (!Export_Path.EndsWith(@"\"))
                    Export_Path = Export_Path + @"\";
            }

            ClosedXML.Excel.XLWorkbook wb = new ClosedXML.Excel.XLWorkbook(Template_Path);
            ClosedXML.Excel.IXLWorksheet ClassWS = wb.Worksheet(1);
            ClosedXML.Excel.IXLWorksheet StudentsWS = wb.Worksheet(2);
            ClosedXML.Excel.IXLWorksheet GroupWS = wb.Worksheet(3);
            ClosedXML.Excel.IXLWorksheet DoublesWS = wb.Worksheet(4);

            ClosedXML.Excel.IXLCell foundCell;
            ClosedXML.Excel.IXLCell Org_foundCell;
            ClosedXML.Excel.IXLCell doubles_foundCell;



            //先處理students的欄位
            int WSRow = 3;
            int WSCol = 0;

            string L1Name = "";
            string L2Name = "";
            string L3Name = "";
            foreach (KeyValuePair<string, Item> StudentField in StudentFields)
            {
                tmp = StudentField.Value;
                WSCol = int.Parse(tmp.getProperty("col_index", "0")) + 1;

                //foundCell = StudentsWS.Cell(3,WSCol);
                //SetCellTextValue(ref foundCell,tmp.getProperty("in_questions",""));

                if (tmp.getProperty("in_property", "") == "in_l1")
                    L1Name = tmp.getProperty("in_questions", "");

                if (tmp.getProperty("in_property", "") == "in_l2")
                    L2Name = tmp.getProperty("in_questions", "");

                if (tmp.getProperty("in_property", "") == "in_l3")
                    L3Name = tmp.getProperty("in_questions", "");

            }

            //身分證字號	姓名	性別	西元生日	所屬道館	隊別	in_l1	in_l2   in_l3 	金額

            foundCell = StudentsWS.Cell(3, 9);
            SetCellTextValue(ref foundCell, ClearPrefiex(L1Name));

            foundCell = StudentsWS.Cell(3, 10);
            SetCellTextValue(ref foundCell, ClearPrefiex(L2Name));

            foundCell = StudentsWS.Cell(3, 11);
            SetCellTextValue(ref foundCell, ClearPrefiex(L3Name));

            //算出有幾間群組
            sql = "select distinct CONCAT(in_current_org,'_',in_l1,'_',in_gender) as c1 from in_meeting_user WITH(NOLOCK) where source_id='" + meeting_id + "' and in_note_state='official'" + SQL_OnlyMyGym;

            Item Orgs = inn.applySQL(sql);
            Dictionary<string, Item> DicOrgs = new Dictionary<string, Item>();


            int SheetIndex = 5;
            for (int i = 0; i < Orgs.getItemCount(); i++)
            {
                Item Org = Orgs.getItemByIndex(i);
                string C1 = Org.getProperty("c1", "");
                L1Name = C1.Split('_')[1];
                string OrgName = C1.Split('_')[0];
                string gender = C1.Split('_')[2];

                OrgName = GetClearSheetName(L1Name, OrgName, gender);

                if (!DicOrgs.ContainsKey(OrgName))
                {
                    Item orgnode = inn.newItem();


                    orgnode.setProperty("last_row", "3");
                    orgnode.setProperty("sheet_index", SheetIndex.ToString());
                    SheetIndex++;
                    //隊職員數	選手人數	參賽項目總計	報名費用
                    orgnode.setProperty("players", "0");
                    orgnode.setProperty("stuffs", "0");
                    orgnode.setProperty("items", "0");
                    orgnode.setProperty("gameitems", ",");
                    orgnode.setProperty("expenses", "0");

                    //Org.setProperty("member_names",",");
                    orgnode.setProperty("stuff_names", ",");
                    orgnode.setProperty("player_names", ",");

                    orgnode.setProperty("single_items", "0");
                    orgnode.setProperty("due_items", "0");
                    orgnode.setProperty("mix_items", "0");
                    orgnode.setProperty("group_items", "0");

                    orgnode.setProperty("in_creator", ",");
                    orgnode.setProperty("in_tel", ",");

                    DicOrgs.Add(OrgName, orgnode);
                    if (OrgName.Contains("團體"))
                    {
                        GroupWS.CopyTo(OrgName);
                    }
                    else if (OrgName.Contains("混合"))
                    {
                        DoublesWS.CopyTo(OrgName);
                    }
                    else if (OrgName.Contains("雙打"))
                    {
                        DoublesWS.CopyTo(OrgName);
                    }
                    else
                    {
                        StudentsWS.CopyTo(OrgName);
                    }
                }
            }

            //處理學員
            WSCol = 1;
            WSRow = 4;
            //身分證字號	姓名	性別	西元生日	所屬道館	隊別	in_l1	in_l2   in_l3 	金額
            //string[] single_mufields = {"in_index","in_sno","in_original_org","in_name","in_gender","in_birth","in_current_org","in_team","in_l1","in_l2","in_l3"};
            string[] single_mufields = { "in_sno", "in_name", "in_gender", "in_birth", "in_current_org", "in_team", "in_index", "in_l1", "in_l2", "in_l3", "expense", "in_creator", "in_creator_sno", "in_paynumber", "in_paytime" };

            //雙打
            string[] single_mufields_doubles = { "in_sno", "in_name", "in_gender", "in_birth", "in_current_org", "in_team", "in_index", "in_l1", "in_l2", "in_l3", "expense", "in_creator", "in_creator_sno", "in_paynumber", "in_paytime" };

            string[] group_fields = { "in_index", "in_l1", "in_current_org", "in_stuff_a1", "in_stuff_b1", "in_stuff_c1", "in_stuff_c2", "in_stuff_c3", "in_name", "in_name1", "in_name2", "in_name3", "in_name4", "in_name5", "in_name6", "in_name7", "in_name8", "in_name9" };

            //計算總費用的方式:同一個費用層次且同一序號,只能算一個費用,例如雙打/2000, 代表 兩個人總共2000

            string CommanExpenseProperty = "";
            if (ExpenseProperty != "in_l1")
                CommanExpenseProperty = "," + ExpenseProperty;

            string sql_users = "";
            string _meeting_id = Meeting.getID();
            string orderBy = "in_current_org, in_l1, in_index, in_name";
            if (CommanExpenseProperty.Contains("in_l2"))
            {
                orderBy = "in_current_org, in_l1, in_l2, in_index, in_name";
            }
            else if (CommanExpenseProperty.Contains("in_l3"))
            {
                orderBy = "in_current_org, in_l1, in_l2, in_l3, in_index, in_name";
            }

            //pay 走繳費
            if (export_type == "pay")
            {
                sql_users = "SELECT * FROM In_Meeting_User WITH(NOLOCK) WHERE source_id = '" + _meeting_id + "' AND in_note_state = N'official' AND ISNULL(in_paynumber, '') = '' ";
                sql_users += SQL_OnlyMyGym;
                sql_users += " ORDER BY " + orderBy;
            }
            else
            {
                sql_users = "SELECT * FROM In_Meeting_User WITH(NOLOCK) WHERE source_id = '" + _meeting_id + "' AND in_note_state = N'official' ";
                sql_users += SQL_OnlyMyGym;
                sql_users += " ORDER BY " + orderBy;
            }


            Item MeetingUsers = inn.applySQL(sql_users);
            int countMeetingUsers = MeetingUsers.getItemCount();

            string PreExpenseKey = "";
            string ThisExpenseKey = "";
            Dictionary<string, Item> dictionary_creators = GetCreators(CCO, strMethodName, inn, _meeting_id);

            for (int i = 0; i < countMeetingUsers; i++)
            {

                WSRow = i + 4;
                Item In_Meeting_User = MeetingUsers.getItemByIndex(i);
                ThisExpenseKey = In_Meeting_User.getProperty(ExpenseProperty, "") + In_Meeting_User.getProperty("in_index", "");
                string OrgName = In_Meeting_User.getProperty("in_current_org");
                // 
                string _in_l1 = In_Meeting_User.getProperty("in_l1", "");
                string _in_gender = In_Meeting_User.getProperty("in_gender", "");

                OrgName = GetClearSheetName(_in_l1, OrgName, _in_gender);

                if (!DicOrgs.ContainsKey(OrgName))
                {
                    CCO.Utilities.WriteDebug(strMethodName, "DicOrgs doesn't contain key: " + OrgName);
                    continue;
                }


                Item DicOrg = DicOrgs[OrgName];
                string in_sheet_index = DicOrg.getProperty("sheet_index");


                ClosedXML.Excel.IXLWorksheet OrgWS = wb.Worksheet(int.Parse(in_sheet_index));
                int OrgWSRow = int.Parse(DicOrg.getProperty("last_row"));
                OrgWSRow = OrgWSRow + 1;
                DicOrg.setProperty("last_row", OrgWSRow.ToString());
                //更新統計數字
                int players = int.Parse(DicOrg.getProperty("players", "0"));
                int stuffs = int.Parse(DicOrg.getProperty("stuffs", "0"));
                int insurances = int.Parse(DicOrg.getProperty("insurances", "0"));
                int items = int.Parse(DicOrg.getProperty("items", "0"));
                double expenses = 0;
                expenses = double.Parse(DicOrg.getProperty("expenses", "0"));
                string gameitems = DicOrg.getProperty("gameitems", "0");

                string stuff_names = DicOrg.getProperty("stuff_names", "");
                string insurance_names = DicOrg.getProperty("insurance_names", "");
                string player_names = DicOrg.getProperty("player_names", "");

                string in_creator = DicOrg.getProperty("in_creator", "");
                string in_tel = DicOrg.getProperty("in_tel", "");

                //羽球:如果是團體的話,要拉成橫的
                string CurrentIndex = In_Meeting_User.getProperty("in_index", "");
                string NextIndex = "";

                var _name = In_Meeting_User.getProperty("in_name", "");
                var _l1 = In_Meeting_User.getProperty("in_l1", "");
                if (_l1.Contains("隊職員"))
                {
                    if (!stuff_names.Contains("," + _name + ","))
                    {
                        stuffs++;
                        stuff_names += _name + ",";
                    }
                }
                else if (_l1.Contains("保險代表人"))
                {
                    if (!insurance_names.Contains("," + _name + ","))
                    {
                        insurances++;
                        insurance_names += _name + ",";
                    }
                }
                else
                {
                    if (!player_names.Contains("," + _name + ","))
                    {
                        players++;
                        player_names += _name + ",";
                    }

                    if (ThisExpenseKey != PreExpenseKey)
                    {
                        expenses = expenses + double.Parse(In_Meeting_User.getProperty("in_expense", "0"));
                        PreExpenseKey = ThisExpenseKey;
                        items++;

                        string item_count_name = "";
                        switch (GetSheetName(In_Meeting_User.getProperty("in_l1", "")))
                        {
                            case "單打":
                                item_count_name = "single_items";
                                break;
                            case "雙打":
                                item_count_name = "due_items";
                                break;
                            case "混雙":
                                item_count_name = "mix_items";
                                break;
                            case "團體":
                                item_count_name = "group_items";
                                break;
                            default:
                                item_count_name = "single_items";
                                break;

                        }

                        int item_count = int.Parse(DicOrg.getProperty(item_count_name, "0"));
                        item_count = item_count + 1;
                        DicOrg.setProperty(item_count_name, item_count.ToString());

                    }
                }



                string in_c = In_Meeting_User.getProperty("in_creator");
                string in_o = In_Meeting_User.getProperty("in_current_org");
                //string in_g = In_Meeting_User.getProperty("in_group");
                string in_c_s = In_Meeting_User.getProperty("in_creator_sno");

                sql = "select * from In_Resume where in_sno=N'" + in_c_s + "'";
                Item tests = inn.applySQL(sql);
                if (tests.getResult() != "")
                {
                    for (int c = 0; c < tests.getItemCount(); c++)
                    {
                        Item test = tests.getItemByIndex(c);
                        if (!in_creator.Contains("," + In_Meeting_User.getProperty("in_creator", "") + ","))
                        {

                            in_creator += In_Meeting_User.getProperty("in_creator", "") + ",";
                        }

                        if (!in_tel.Contains("," + test.getProperty("in_tel", "") + ","))
                        {

                            in_tel += test.getProperty("in_tel", "") + ",";
                        }
                    }
                }



                if (In_Meeting_User.getProperty("in_l1", "").Contains("團體"))
                {
                    //計算隊職員人數
                    List<string> Gstuffs = new List<string>();
                    Gstuffs.Add(In_Meeting_User.getProperty("in_stuff_a1", ""));
                    Gstuffs.Add(In_Meeting_User.getProperty("in_stuff_b1", ""));
                    Gstuffs.Add(In_Meeting_User.getProperty("in_stuff_c1", ""));
                    Gstuffs.Add(In_Meeting_User.getProperty("in_stuff_c2", ""));
                    Gstuffs.Add(In_Meeting_User.getProperty("in_stuff_c3", ""));
                    for (int k = 0; k < Gstuffs.Count; k++)
                    {
                        if (Gstuffs[k] == "")
                            continue;

                        if (!stuff_names.Contains("," + Gstuffs[k] + ","))
                        {
                            stuffs++;
                            stuff_names += Gstuffs[k] + ",";
                        }
                    }

                    //代表是團體,所以要把屬性拉成橫的
                    for (int k = 1; k <= 9; k++)
                    {
                        if ((i + 1) >= MeetingUsers.getItemCount())
                            break; //代表已經超過 MeetingUsers 了

                        Item G_Meeting_User = MeetingUsers.getItemByIndex(i + 1);
                        if ((G_Meeting_User.getProperty("in_l1", "") + G_Meeting_User.getProperty("in_index", "")) != (In_Meeting_User.getProperty("in_l1", "") + In_Meeting_User.getProperty("in_index", "")))
                            break; //代表已經不同隊了
                        if (G_Meeting_User.getProperty("in_name", "") == "")
                            continue;
                        i = i + 1;
                        if (!player_names.Contains("," + G_Meeting_User.getProperty("in_name", "") + ","))
                        {
                            players++;
                            player_names += G_Meeting_User.getProperty("in_name", "") + ",";
                        }

                        In_Meeting_User.setProperty("in_name" + k.ToString(), G_Meeting_User.getProperty("in_name", ""));

                    }
                }


                DicOrg.setProperty("players", players.ToString());
                DicOrg.setProperty("stuffs", stuffs.ToString());
                DicOrg.setProperty("insurances", insurances.ToString());
                DicOrg.setProperty("items", items.ToString());
                DicOrg.setProperty("gameitems", gameitems);
                DicOrg.setProperty("expenses", expenses.ToString());
                DicOrg.setProperty("player_names", player_names);
                DicOrg.setProperty("stuff_names", stuff_names);
                DicOrg.setProperty("insurance_names", insurance_names);
                DicOrg.setProperty("in_current_org", In_Meeting_User.getProperty("in_current_org"));

                DicOrg.setProperty("in_creator", in_creator);//協助報名者
                DicOrg.setProperty("in_tel", in_tel);//電話



                //更新統計數字
                string[] mufields;
                if (OrgName.Contains("團體"))
                {
                    mufields = group_fields;
                }
                else if (OrgName.Contains("混合"))
                {
                    mufields = single_mufields_doubles;
                }
                else if (OrgName.Contains("雙打"))
                {
                    mufields = single_mufields_doubles;
                }
                else
                {
                    mufields = single_mufields;
                }

                for (int k = 0; k < mufields.Length; k++)
                {
                    WSCol = k + 2;

                    foundCell = StudentsWS.Cell(WSRow, WSCol);
                    Org_foundCell = OrgWS.Cell(OrgWSRow, WSCol);
                    doubles_foundCell = DoublesWS.Cell(WSRow, WSCol);

                    SetCellBorder(foundCell);
                    SetCellBorder(Org_foundCell);
                    SetCellBorder(doubles_foundCell);

                    string ans = In_Meeting_User.getProperty(mufields[k], "");

                    switch (mufields[k])
                    {
                        case "in_birth":
                            if (ans != "")
                            {
                                DateTime in_birth_dt = Convert.ToDateTime(ans);
                                ans = in_birth_dt.AddHours(8).ToString("yyyy-MM-ddTHH:mm:ss");//sql用-8H
                                ans = ans.Split('T')[0];
                            }

                            SetCellTextValue(ref foundCell, ans);
                            SetCellTextValue(ref Org_foundCell, ans);
                            SetCellTextValue(ref doubles_foundCell, ans);
                            break;

                        case "expense":
                            ans = In_Meeting_User.getProperty("in_expense", "0");
                            foundCell.Value = ans;
                            Org_foundCell.Value = ans;
                            doubles_foundCell.Value = ans;
                            foundCell.Style.NumberFormat.Format = "#,##0";
                            Org_foundCell.Style.NumberFormat.Format = "#,##0";
                            doubles_foundCell.Style.NumberFormat.Format = "#,##0";
                            break;

                        case "in_creator_sno":
                            ans = In_Meeting_User.getProperty("in_creator_sno", "");
                            string tel = GetCreatorProperty(dictionary_creators, ans, "in_tel");

                            foundCell.Value = tel;
                            Org_foundCell.Value = tel;
                            foundCell.Style.NumberFormat.Format = "0#########";//數值格式
                            Org_foundCell.Style.NumberFormat.Format = "0#########";//數值格式
                            break;

                        case "in_paytime":
                            if (ans != "")
                            {
                                DateTime in_birth_dt = Convert.ToDateTime(ans);
                                ans = in_birth_dt.AddHours(8).ToString("yyyy-MM-ddTHH:mm:ss");
                                ans = ans.Split('T')[0];
                            }
                            SetCellTextValue(ref foundCell, ans);
                            SetCellTextValue(ref Org_foundCell, ans);
                            SetCellTextValue(ref doubles_foundCell, ans);
                            break;

                        default:
                            SetCellTextValue(ref foundCell, ans);
                            SetCellTextValue(ref Org_foundCell, ans);
                            SetCellTextValue(ref doubles_foundCell, ans);
                            break;

                    }


                }


            }

            //補上Footer

            //重新整併各道館資訊 (重點是要重算隊職員與選手數字)
            double TotalExpense = 0;
            int due_items = 0;
            int single_items = 0;
            int mix_items = 0;
            int group_items = 0;
            Dictionary<string, Item> DicMergedOrgs = new Dictionary<string, Item>();
            foreach (KeyValuePair<string, Item> DicOrg in DicOrgs)
            {
                Item Org = DicOrg.Value;
                string OrgName = Org.getProperty("in_current_org", "");
                string in_creator = Org.getProperty("in_creator", "");
                string in_tel = Org.getProperty("in_tel", "");

                bool IsNewOrgName = false;
                if (!DicMergedOrgs.ContainsKey(OrgName))
                {
                    Item orgnode = inn.newItem();

                    //隊職員數	選手人數	參賽項目總計	報名費用
                    orgnode.setProperty("players", "0");
                    orgnode.setProperty("stuffs", "0");
                    orgnode.setProperty("insurances", "0");
                    orgnode.setProperty("items", "0");
                    orgnode.setProperty("gameitems", ",");
                    orgnode.setProperty("expenses", "0");

                    //Org.setProperty("member_names",",");
                    orgnode.setProperty("stuff_names", ",");
                    orgnode.setProperty("insurance_names", ",");
                    orgnode.setProperty("player_names", ",");

                    orgnode.setProperty("single_items", "0");
                    orgnode.setProperty("due_items", "0");
                    orgnode.setProperty("mix_items", "0");
                    orgnode.setProperty("group_items", "0");
                    orgnode.setProperty("in_current_org", OrgName);

                    orgnode.setProperty("in_creator", in_creator);
                    orgnode.setProperty("in_tel", in_tel);

                    DicMergedOrgs.Add(OrgName, orgnode);
                    IsNewOrgName = true;
                }

                Item MergedOrg = DicMergedOrgs[OrgName];

                int stuffs = int.Parse(MergedOrg.getProperty("stuffs", "0"));
                int insurances = int.Parse(MergedOrg.getProperty("insurances", "0"));
                int Players = int.Parse(MergedOrg.getProperty("players", "0"));
                int merged_expenses = int.Parse(MergedOrg.getProperty("expenses", "0"));



                string merged_stuff_names = MergedOrg.getProperty("stuff_names", "");
                string merged_insurance_names = MergedOrg.getProperty("insurance_names", "");
                string merged_player_names = MergedOrg.getProperty("player_names", "");

                string stuff_names = Org.getProperty("stuff_names", "");
                string[] stuff_names_arr = stuff_names.Split(',');
                for (int k = 0; k < stuff_names_arr.Length; k++)
                {
                    if (stuff_names_arr[k] == "")
                        continue;
                    if (!merged_stuff_names.Contains("," + stuff_names_arr[k] + ","))
                    {
                        stuffs++;
                        merged_stuff_names += stuff_names_arr[k] + ",";
                    }
                }
                MergedOrg.setProperty("stuff_names", merged_stuff_names);
                MergedOrg.setProperty("stuffs", stuffs.ToString());

                string insurance_names = Org.getProperty("insurance_names", "");
                string[] insurance_names_arr = insurance_names.Split(',');
                for (int k = 0; k < insurance_names_arr.Length; k++)
                {
                    if (insurance_names_arr[k] == "")
                        continue;
                    if (!merged_insurance_names.Contains("," + insurance_names_arr[k] + ","))
                    {
                        insurances++;
                        merged_insurance_names += insurance_names_arr[k] + ",";
                    }
                }
                MergedOrg.setProperty("insurance_names", merged_insurance_names);
                MergedOrg.setProperty("insurances", insurances.ToString());

                string player_names = Org.getProperty("player_names", "");
                string[] player_names_arr = player_names.Split(',');
                for (int k = 0; k < player_names_arr.Length; k++)
                {
                    if (player_names_arr[k] == "")
                        continue;
                    if (!merged_player_names.Contains("," + player_names_arr[k] + ","))
                    {
                        Players++;
                        merged_player_names += player_names_arr[k] + ",";
                    }
                }
                MergedOrg.setProperty("player_names", merged_player_names);
                MergedOrg.setProperty("players", Players.ToString());



                //組數計算
                if (Org.getProperty("single_items", "0") != "0")
                {
                    single_items = Int32.Parse(MergedOrg.getProperty("single_items", "0"));
                    single_items += Int32.Parse(Org.getProperty("single_items", "0"));
                    MergedOrg.setProperty("single_items", single_items.ToString());
                }



                if (Org.getProperty("due_items", "0") != "0")
                {
                    due_items = Int32.Parse(MergedOrg.getProperty("due_items", "0"));
                    due_items += Int32.Parse(Org.getProperty("due_items", "0"));
                    MergedOrg.setProperty("due_items", due_items.ToString());
                }

                if (Org.getProperty("mix_items", "0") != "0")
                {
                    mix_items = Int32.Parse(MergedOrg.getProperty("mix_items", "0"));
                    mix_items += Int32.Parse(Org.getProperty("mix_items", "0"));
                    MergedOrg.setProperty("mix_items", mix_items.ToString());
                }

                if (Org.getProperty("group_items", "0") != "0")
                {
                    group_items = Int32.Parse(MergedOrg.getProperty("group_items", "0"));
                    group_items += Int32.Parse(Org.getProperty("group_items", "0"));
                    MergedOrg.setProperty("group_items", group_items.ToString());

                }



                int Items = int.Parse(MergedOrg.getProperty("single_items", "0")) +
                         int.Parse(MergedOrg.getProperty("due_items", "0")) +
                          int.Parse(MergedOrg.getProperty("mix_items", "0")) +
                           int.Parse(MergedOrg.getProperty("group_items", "0"));
                MergedOrg.setProperty("items", Items.ToString());

                merged_expenses += int.Parse(Org.getProperty("expenses", "0"));
                MergedOrg.setProperty("expenses", merged_expenses.ToString());


                //補上每個道館的 Header
                TotalExpense = TotalExpense + double.Parse(Org.getProperty("expenses", "0"));
                ClosedXML.Excel.IXLWorksheet OrgWS = wb.Worksheet(int.Parse(Org.getProperty("sheet_index")));


                foundCell = OrgWS.Cell(2, 4);
                foundCell.Value = Org.getProperty("in_current_org", "");

                foundCell = OrgWS.Cell(2, 7);
                foundCell.Value = Org.getProperty("expenses", "0");
                foundCell.Style.NumberFormat.Format = "#,##0";

            }

            //處理表頭


            WSCol = 2;
            WSRow = 11;
            int sum = 0;
            foreach (KeyValuePair<string, Item> DicOrg in DicMergedOrgs)
            {
                Item Org = DicOrg.Value;

                foundCell = ClassWS.Cell(WSRow, WSCol);
                SetCellBorder(foundCell);
                foundCell.Value = Org.getProperty("in_current_org", "");

                foundCell = ClassWS.Cell(WSRow, WSCol + 1);
                SetCellBorder(foundCell);
                foundCell.Value = "";

                foundCell = ClassWS.Cell(WSRow, WSCol + 2);
                SetCellBorder(foundCell);
                foundCell.Value = Org.getProperty("stuffs", "0");

                foundCell = ClassWS.Cell(WSRow, WSCol + 3);
                SetCellBorder(foundCell);
                foundCell.Value = Org.getProperty("players", "0");

                foundCell = ClassWS.Cell(WSRow, WSCol + 4);
                SetCellBorder(foundCell);
                foundCell.Value = Org.getProperty("items", "0");

                foundCell = ClassWS.Cell(WSRow, WSCol + 5);
                SetCellBorder(foundCell);
                foundCell.Value = Org.getProperty("single_items", "0");

                foundCell = ClassWS.Cell(WSRow, WSCol + 6);
                SetCellBorder(foundCell);
                foundCell.Value = Org.getProperty("due_items", "0");

                foundCell = ClassWS.Cell(WSRow, WSCol + 7);
                SetCellBorder(foundCell);
                foundCell.Value = Org.getProperty("mix_items", "0");

                foundCell = ClassWS.Cell(WSRow, WSCol + 8);
                SetCellBorder(foundCell);
                foundCell.Value = Org.getProperty("group_items", "0");


                foundCell = ClassWS.Cell(WSRow, WSCol + 9);
                SetCellBorder(foundCell);
                foundCell.Style.NumberFormat.Format = "#,##0";
                foundCell.Value = Org.getProperty("expenses", "0");

                //協助報名者
                foundCell = ClassWS.Cell(WSRow, WSCol + 10);
                SetCellBorder(foundCell);
                foundCell.Value = Org.getProperty("in_creator", "").Trim(',').Replace(",", "\n");
                foundCell.Style.Font.FontName = "Calibri";
                foundCell.Style.Alignment.WrapText = true;//自動換行


                //連絡電話
                foundCell = ClassWS.Cell(WSRow, WSCol + 11);
                SetCellBorder(foundCell);
                foundCell.Value = Org.getProperty("in_tel", "").Trim(',').Replace(",", " ");
                foundCell.Style.NumberFormat.Format = "0#########";//數值格式
                foundCell.Style.Font.FontName = "Calibri";//字型
                foundCell.Style.Alignment.WrapText = true;//自動換行


                sum += Int32.Parse(Org.getProperty("expenses", "0"));

                WSRow++;
                Org.setProperty("sum_expenses", sum.ToString());
                Org.setType("in_org_excel");
                itmR.addRelationship(Org);

            }
            itmR.setProperty("sum_expenses", sum.ToString());

            foreach (KeyValuePair<string, Item> DicOrg in DicMergedOrgs)
            {
                Item Org = DicOrg.Value;
                doubles_foundCell = DoublesWS.Cell(WSRow, WSCol);
                SetCellBorder(doubles_foundCell);
                doubles_foundCell.Value = Org.getProperty("in_current_org", "");

                doubles_foundCell = DoublesWS.Cell(WSRow, WSCol + 1);
                SetCellBorder(doubles_foundCell);
                doubles_foundCell.Value = "";

                doubles_foundCell = DoublesWS.Cell(WSRow, WSCol + 2);
                SetCellBorder(doubles_foundCell);
                doubles_foundCell.Value = Org.getProperty("stuffs", "0");

                doubles_foundCell = DoublesWS.Cell(WSRow, WSCol + 3);
                SetCellBorder(doubles_foundCell);
                doubles_foundCell.Value = Org.getProperty("players", "0");

                doubles_foundCell = DoublesWS.Cell(WSRow, WSCol + 4);
                SetCellBorder(doubles_foundCell);
                doubles_foundCell.Value = Org.getProperty("items", "0");

                doubles_foundCell = DoublesWS.Cell(WSRow, WSCol + 5);
                SetCellBorder(doubles_foundCell);
                doubles_foundCell.Value = Org.getProperty("single_items", "0");

                doubles_foundCell = DoublesWS.Cell(WSRow, WSCol + 6);
                SetCellBorder(doubles_foundCell);
                doubles_foundCell.Value = Org.getProperty("due_items", "0");

                doubles_foundCell = DoublesWS.Cell(WSRow, WSCol + 7);
                SetCellBorder(doubles_foundCell);
                doubles_foundCell.Value = Org.getProperty("mix_items", "0");

                doubles_foundCell = DoublesWS.Cell(WSRow, WSCol + 8);
                SetCellBorder(doubles_foundCell);
                doubles_foundCell.Value = Org.getProperty("group_items", "0");


                doubles_foundCell = DoublesWS.Cell(WSRow, WSCol + 9);
                SetCellBorder(doubles_foundCell);
                doubles_foundCell.Style.NumberFormat.Format = "#,##0";
                doubles_foundCell.Value = Org.getProperty("expenses", "0");

                WSRow++;
            }
            //Footer
            WSRow += 3;
            foundCell = ClassWS.Cell(WSRow, 2);
            foundCell.Value = "總教練簽名________________________";

            foundCell = ClassWS.Cell(WSRow, 6);
            foundCell.Value = "最後修改日期:" + System.DateTime.Now.ToString("yyyy-MM-dd");
            //Footer

            string[] ClassFields = { "in_group", "in_name", "in_tel", "in_add", "total_amount", "in_bank_1", "in_bank_2", "in_bank_account", "in_bank_name", "in_bank_note", "in_customer_tel" };

            Resume.setProperty("total_amount", TotalExpense.ToString());
            itmR.setProperty("test_total", TotalExpense.ToString());
            itmR.setProperty("in_group", Resume.getProperty("in_group", ""));
            for (int i = 0; i < ClassFields.Length; i++)
            {
                foundCell = ClassWS.Search("{#" + ClassFields[i] + "}", CompareOptions.OrdinalIgnoreCase).FirstOrDefault();
                if (foundCell != null)
                {
                    string FieldValue = Resume.getProperty(ClassFields[i], "");
                    if (i > 4) //第四個之後,就要改抓 Meeting 的屬性
                        FieldValue = Meeting.getProperty(ClassFields[i], "");

                    if (ClassFields[i] == "total_amount")
                    {
                        foundCell.Value = FieldValue;
                        foundCell.Style.NumberFormat.Format = "#,##0";
                    }
                    else
                    {
                        SetCellTextValue(ref foundCell, FieldValue);
                    }
                }
            }

            //StudentsWS
            foundCell = StudentsWS.Cell(2, 3);
            foundCell.Value = Resume.getProperty("in_group", "");

            foundCell = StudentsWS.Cell(2, 7);
            foundCell.Value = TotalExpense.ToString();

            // 	//移除兩個Sheet
            // 	GroupWS.Delete();
            // 	StudentsWS.Delete();
            // 	DoublesWS.Delete();

            string xlsName = Meeting.getProperty("in_title", "");
            if (export_type == "excel")
            {
                GroupWS.Delete();
                StudentsWS.Delete();
                DoublesWS.Delete();

                xlsName = xlsName.Replace("+", "")
                            .Replace("-", "")
                            .Replace(".", "")
                            .Replace(" ", "");
                wb.SaveAs(Export_Path + xlsName + ".xlsx");
                itmR.setProperty("xls_name", xlsName + ".xlsx");
            }
            else if (export_type == "pay")
            {
                xlsName = xlsName.Replace("+", "")
                            .Replace("-", "")
                            .Replace(".", "")
                            .Replace(" ", "");
                //wb.SaveAs(Export_Path+xlsName+".xlsx");
                //itmR.setProperty("xls_name",xlsName+".xlsx");
            }
            else
            {
                //ex: xml

                //Excel To Item
                Item ExceItem = inn.newItem();
                Item OrgData;

                for (int i = 0; i < ClassFields.Length; i++)
                {

                    string FieldValue = Resume.getProperty(ClassFields[i], "");
                    if (i > 4) //第四個之後,就要改抓 Meeting 的屬性
                        FieldValue = Meeting.getProperty(ClassFields[i], "");

                    ExceItem.setProperty(ClassFields[i], FieldValue);
                }


                //報名總表
                int ExcelItemColCounts = 13;//抓excel的欄
                int StartRowIndex = 11;//抓excel的列
                ClosedXML.Excel.IXLWorksheet CoverpageWS = wb.Worksheet(1);
                for (int WSRow2 = StartRowIndex; WSRow2 <= (StartRowIndex + DicOrgs.Count); WSRow2++)
                {
                    OrgData = inn.newItem("coverpage");
                    for (int WSCol2 = 2; WSCol2 <= ExcelItemColCounts; WSCol2++)
                    {
                        foundCell = CoverpageWS.Cell(WSRow2, WSCol2);
                        OrgData.setProperty("col_" + WSCol2, foundCell.Value.ToString());
                    }

                    //2020.08.24 如果col_2(所屬單位)沒有值則不秀出
                    if (OrgData.getProperty("col_2", "") != "")
                    {
                        ExceItem.addRelationship(OrgData);
                    }
                }

                //總表
                ExcelItemColCounts = 12;
                StartRowIndex = 4;

                //總表表頭
                ClosedXML.Excel.IXLWorksheet SumpageWS = wb.Worksheet(2);
                OrgData = inn.newItem("sumpage_header");
                for (int WSCol4 = 2; WSCol4 <= ExcelItemColCounts; WSCol4++)
                {
                    foundCell = SumpageWS.Cell(3, WSCol4);
                    OrgData.setProperty("col_" + WSCol4, foundCell.Value.ToString());
                }
                OrgData.setProperty("org_name", SumpageWS.Cell(2, 3).Value.ToString());
                OrgData.setProperty("org_expense", SumpageWS.Cell(2, 7).Value.ToString());
                ExceItem.addRelationship(OrgData);



                for (int WSRow3 = StartRowIndex; WSRow3 <= (StartRowIndex + MeetingUsers.getItemCount()); WSRow3++)
                {
                    OrgData = inn.newItem("sumpage");
                    for (int WSCol3 = 2; WSCol3 <= ExcelItemColCounts; WSCol3++)
                    {
                        foundCell = SumpageWS.Cell(WSRow3, WSCol3);
                        OrgData.setProperty("col_" + WSCol3, foundCell.Value.ToString());
                    }
                    ExceItem.addRelationship(OrgData);
                }



                ExcelItemColCounts = 12;
                StartRowIndex = 4;
                foreach (KeyValuePair<string, Item> DicOrg in DicOrgs)
                {
                    Item Org = DicOrg.Value;
                    string in_sheet_index = Org.getProperty("sheet_index");

                    ClosedXML.Excel.IXLWorksheet OrgWS = wb.Worksheet(int.Parse(in_sheet_index));
                    int OrgWSRow = int.Parse(Org.getProperty("last_row"));

                    ExceItem.setProperty("sheet_name_" + Org.getProperty("sheet_index"), Org.getProperty("in_current_org"));
                    ExceItem.setProperty("sheet_show_" + Org.getProperty("sheet_index"), "1");


                    //各單位表頭
                    OrgData = inn.newItem("sheet_header_" + Org.getProperty("sheet_index"));
                    for (int WSCol1 = 2; WSCol1 <= ExcelItemColCounts; WSCol1++)
                    {
                        foundCell = OrgWS.Cell(3, WSCol1);
                        OrgData.setProperty("col_" + WSCol1, foundCell.Value.ToString());
                    }
                    OrgData.setProperty("org_name", OrgWS.Cell(2, 3).Value.ToString());
                    OrgData.setProperty("org_expense", OrgWS.Cell(2, 7).Value.ToString());
                    ExceItem.addRelationship(OrgData);


                    for (int WSRow1 = StartRowIndex; WSRow1 <= OrgWSRow; WSRow1++)
                    {
                        OrgData = inn.newItem("sheet_content_" + Org.getProperty("sheet_index"));
                        for (int WSCol1 = 2; WSCol1 <= ExcelItemColCounts; WSCol1++)
                        {
                            foundCell = null;
                            foundCell = OrgWS.Cell(WSRow1, WSCol1);
                            OrgData.setProperty("col_" + WSCol1, foundCell.Value.ToString());
                            OrgData.setProperty("col_index", WSCol1.ToString());
                            OrgData.setProperty("row_index", WSRow1.ToString());


                        }
                        OrgData.setProperty("in_current_org", OrgWS.Cell(2, 3).Value.ToString());

                        ExceItem.addRelationship(OrgData);

                    }
                }

                //補上其他資料
                ExceItem.setProperty("print_datetime", DateTime.Now.ToString("yyyy/MM/dd HH:mm"));
                ExceItem.setProperty("print_user", Resume.getProperty("in_name", ""));


                itmR = ExceItem;

            }

            sql = "SELECT in_value FROM in_variable WITH(NOLOCK) WHERE in_name = 'meeting_game_name'";
            Item itmVariable = inn.applySQL(sql);
            string game_name = itmVariable.getResult() != "" ? itmVariable.getProperty("in_value", "") : "game_name";

            itmR.setProperty("game_name", game_name);
            itmR.setProperty("meeting_id", Meeting.getID());
            itmR.setProperty("in_register_url", Meeting.getProperty("in_register_url", ""));
            itmR.setProperty("inn_meeting_admin", "1");

            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

            return itmR;
        }

        private void SetCellTextValue(ref ClosedXML.Excel.IXLCell Cell, string TextValue)
        {

            DateTime dtTmp;
            double dblTmp;


            if (DateTime.TryParse(TextValue, out dtTmp))
            {
                Cell.Value = "'" + TextValue;
            }
            else if (double.TryParse(TextValue, out dblTmp))
            {
                Cell.Value = "'" + TextValue;
            }
            else
            {
                Cell.Value = TextValue;
            }
            Cell.DataType = ClosedXML.Excel.XLDataType.Text;

        }

        private string GetSheetName(string TheString)
        {

            string[] Single_Names = { "單人", "單打" };
            string[] Mix_Names = { "混雙", "混合" };
            string[] Due_Names = { "雙人", "雙打" };
            string[] Group_Names = { "團體" };

            for (int i = 0; i < Single_Names.Length; i++)
            {
                if (TheString.Contains(Single_Names[i]))
                    return "單打";
            }

            for (int i = 0; i < Mix_Names.Length; i++)
            {
                if (TheString.Contains(Mix_Names[i]))
                    return "混雙";
            }

            for (int i = 0; i < Due_Names.Length; i++)
            {
                if (TheString.Contains(Due_Names[i]))
                    return "雙打";
            }

            for (int i = 0; i < Group_Names.Length; i++)
            {
                if (TheString.Contains(Group_Names[i]))
                    return "團體";
            }

            return TheString;
        }

        //Excel Sheet Name 限制 255 字符長度，但系統建議不用超過 31 個字符
        private string GetClearSheetName(string l1, string orgName, string gender)
        {
            string result = "";

            if (l1.Contains("隊職員"))
            {
                result = orgName + "_" + gender + l1;
            }
            else if (l1.Contains("保險代表人"))
            {
                result = orgName + "_" + gender + l1;
            }
            else if (l1.Contains("混合"))
            {
                result = orgName + "_" + l1.Split('/')[0];
            }
            else
            {
                result = orgName + "_" + gender + "子" + GetSheetName(l1);
            }

            return ClearSheetName(result);
        }

        private string GetClearSheetName2(string c1)
        {
            string result = "";

            if (c1.Contains("隊職員"))
            {
                result = c1.Split('-')[0];
            }
            else if (c1.Contains("保險代表人"))
            {
                result = c1.Split('-')[0];
            }
            else
            {
                result = c1.Split('/')[0];
            }

            return ClearSheetName(result);
        }

        private string ClearSheetName(string value)
        {
            string result = value.Replace(":", "_")
                           .Replace(" ", "_")
                           .Replace("/", "_")
                           .Replace("：", "_");

            if (result.Length > 31)
            {
                result = result.Substring(0, 31);
            }

            return result;
        }

        private string ClearPrefiex(string value)
        {
            if (!value.Contains("."))
            {
                return value;
            }

            string[] array = value.Split('.');
            if (array == null || array.Length == 0)
            {
                return value;
            }
            return array[array.Length - 1].Trim();

        }

        private void SetCellBorder(ClosedXML.Excel.IXLCell cell)
        {
            cell.Style.Border.SetRightBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetTopBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetBottomBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetLeftBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
        }

        private string GetCreatorProperty(Dictionary<string, Item> creators, string in_sno, string property)
        {
            if (in_sno == "")
            {
                return "";
            }

            if (creators.ContainsKey(in_sno))
            {
                return creators[in_sno].getProperty(property, "");
            }
            else
            {
                return "";
            }
        }
        private Dictionary<string, Item> GetCreators(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string meeting_id)
        {
            string sql = "SELECT * FROM IN_RESUME WITH(NOLOCK) WHERE IN_SNO IN (SELECT in_creator_sno FROM IN_MEETING_USER WITH(NOLOCK) WHERE source_id = '{#meeting_id}')"
                .Replace("{#meeting_id}", meeting_id);

            Item items = inn.applySQL(sql);

            if (items.isError() || items.getItemCount() <= 0)
            {
                //throw new Exception("取得講師履歷發生錯誤");
            }

            Dictionary<string, Item> result = new Dictionary<string, Item>();

            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_sno = item.getProperty("in_sno", "");
                if (result.ContainsKey(in_sno))
                {
                    CCO.Utilities.WriteDebug(strMethodName, "講師履歷身分證號重覆: " + in_sno);
                }
                else
                {
                    result.Add(in_sno, item);
                }
            }

            return result;

        }
    }
}