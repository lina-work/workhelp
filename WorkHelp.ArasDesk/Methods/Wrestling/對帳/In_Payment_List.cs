﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Wrestling.對帳
{
    public class In_Payment_List : Item
    {
        public In_Payment_List(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的:列出繳費清單
                日期: 
                    - 2024-09-18 保險代表人調整 (lina)
                    - 2021-11-25 調整 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]In_Payment_List";

            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            string aml = "";
            string sql = "";
            string strError = "";
            string strUserId = inn.getUserID();

            Item itmR = this;

            //取得賽事ID
            string meeting_id = this.getProperty("meeting_id", "");
            string scene = this.getProperty("scene", "");

            Item itmPermit = inn.applyMethod("In_CheckIdentity", "<method>" + strMethodName + "</method><code>" + "ALL" + "</code>");
            bool isMeetingAdmin = itmPermit.getProperty("isMeetingAdmin", "") == "1";
            bool isGymOwner = itmPermit.getProperty("isGymOwner", "") == "1";
            bool isGymAssistant = itmPermit.getProperty("isGymAssistant", "") == "1";

            //是否為共同講師
            bool open = GetUserResumeListStatus(inn, meeting_id, strUserId);
            if (open) isMeetingAdmin = true;

            if (scene == "inspect" && !isMeetingAdmin)
            {
                itmR.setProperty("permission_type", "1");
                itmR.setProperty("permission_message", "您無權限瀏覽此頁面");
                return itmR;
            }

            Item PaymentList = null;

            aml = "<AML>" +
                "<Item type='in_meeting' action='get' id='" + meeting_id + "' select='in_title,in_url,in_register_url,in_need_receipt,in_meeting_type,in_verify_mode'>" +
                "</Item></AML>";
            Item itmMeeting = inn.applyAML(aml);

            string meeting_name = itmMeeting.getProperty("in_title", "");
            string in_url = itmMeeting.getProperty("in_url", "");
            string in_register_url = itmMeeting.getProperty("in_register_url", "");
            string in_meeting_type = itmMeeting.getProperty("in_meeting_type", "");
            string in_need_receipt = itmMeeting.getProperty("in_need_receipt", "");
            string in_verify_mode = itmMeeting.getProperty("in_verify_mode", "");

            //張數的總計
            int sheet_content_a_count = 0;
            int sheet_content_b_count = 0;
            int sheet_content_c_count = 0;

            //2020.10.26 補上各大項的費用總計
            int content_a_sum = 0;//尚未產生
            int content_b_sum = 0;//未付款
            int content_c_sum = 0;//已付款

            switch (in_meeting_type)
            {
                case "game":
                    itmR.setProperty("hide_stuff", "");
                    itmR.setProperty("hide_item", "");
                    itmR.setProperty("name_head", "選手數");
                    break;
                case "payment":
                    itmR.setProperty("hide_stuff", "data-visible=\"false\"");
                    itmR.setProperty("hide_item", "data-visible=\"false\"");
                    if (isMeetingAdmin)
                    {
                        itmR.setProperty("hide_cancel", "");
                        itmR.setProperty("hide_cancel_btn", "");
                    }
                    else
                    {
                        itmR.setProperty("hide_cancel", "data-visible=\"false\"");
                        itmR.setProperty("hide_cancel_btn", "item_show_0");
                    }
                    itmR.setProperty("name_head", "報名人數");
                    break;
                default:
                    itmR.setProperty("hide_stuff", "data-visible=\"false\"");
                    itmR.setProperty("hide_item", "data-visible=\"false\"");
                    itmR.setProperty("name_head", "報名人數");
                    break;
            }

            if (in_need_receipt == "1")
            {
                itmR.setProperty("show_receipt", "item_show_1");
            }
            else
            {
                itmR.setProperty("show_receipt", "item_show_0");
            }

            //取得登入者資訊
            sql = "Select *";
            sql += " from In_Resume WITH(NOLOCK) ";
            sql += " where in_user_id='" + strUserId + "'";
            Item itmResume = inn.applySQL(sql);

            //測試用***************************************************************
            string in_creator_sno = itmResume.getProperty("in_sno", "");//協助報名者
            string in_group = itmResume.getProperty("in_group", "");//所屬單位
            string in_member_type = itmResume.getProperty("in_member_type", "");
            int in_staffs = 0;//隊職員數
            int in_players = 0;//選手人數
            int in_items = 0;//項目總計
            int in_expense = 0;//費用
            string blank = " ";
            //應繳日期&最後繳費日期為[今日+7天]
            DateTime CurrentTime = System.DateTime.Today;
            string AfterDay = CurrentTime.AddDays(7).ToString("yyyy-MM-ddTHH:mm:ss");

            string s_type = this.getProperty("s_type", "");//測試-顯示未繳費的單子

            //新報名的人[繳費單號]預設會是Null要更新成空白
            sql = "Update [In_Meeting_User] set ";
            sql += "in_paynumber=N'" + blank + "'";
            sql += " where in_creator_sno = N'" + in_creator_sno + "' AND in_paynumber IS NULL ";
            Item UserNumber = inn.applySQL(sql);

            //計算參賽項目A
            sql = "select distinct in_gameunit from in_meeting_user WITH(NOLOCK) where source_id = '" + meeting_id + "' and in_creator_sno = N'" + in_creator_sno + "' and in_paynumber=N'" + blank + "'";
            Item items_a = inn.applySQL(sql);

            //計算參賽項目B
            sql = "select distinct in_gameunit from in_meeting_user WITH(NOLOCK) where source_id = '" + meeting_id + "' and in_creator_sno = N'" + in_creator_sno + "'";
            Item items_b = inn.applySQL(sql);

            string SQL_OnlyMyGym = " and [in_meeting_user].in_creator_sno = N'" + in_creator_sno + "'";
            bool usertype = false;
            Item in_org_excels = null;
            string open_function = "";
            string method_type = "";

            //依據登入者身分的權限 會有判定條件的不同
            string group = "";
            if (isMeetingAdmin)
            {
                if (in_member_type == "vip_mbr" || in_member_type == "vip_minority")
                {
                    //協會成員並且為個人會員: 只看自己的單
                    //道館主(ACT_GymOwner) 群組判定  只能看見自己的
                    group = "<in_creator_sno>" + in_creator_sno + "</in_creator_sno>";
                    open_function = "X";
                    itmR.setProperty("open_function", "X");
                    itmR.setProperty("pring_show", "0");//不是管理者等級不能看到列印按鈕
                    method_type = "in_meeting_export_excel_gym";
                }
                else
                {
                    //最高負責人(MeetingAdmin) 不使用群組判定
                    group = "";
                    open_function = "O";
                    itmR.setProperty("open_function", "O");
                    itmR.setProperty("pring_show", "1");//管理者等級可以看到列印按鈕
                    method_type = "in_meeting_export_excel_admin";
                }
            }
            else if (isGymOwner)
            {
                //道館主(ACT_GymOwner) 群組判定  只能看見自己的
                group = "<in_creator_sno>" + in_creator_sno + "</in_creator_sno>";
                open_function = "X";
                itmR.setProperty("open_function", "X");
                itmR.setProperty("pring_show", "0");//不是管理者等級不能看到列印按鈕
                method_type = "in_meeting_export_excel_gym";
            }
            else
            {
                throw new Exception("您無權限瀏覽此頁面");
            }

            //判斷現在的登入者是否為此課程的共同講師 
            aml = "<AML>" +
                    "<Item type='In_Meeting_Resumelist' action='get'>" +
                    "<source_id>" + meeting_id + "</source_id>" +
                    "</Item></AML>";
            Item Meeting_Resumelists = inn.applyAML(aml);

            for (int i = 0; i < Meeting_Resumelists.getItemCount(); i++)
            {
                Item Meeting_Resumelist = Meeting_Resumelists.getItemByIndex(i);
                //如果有包含在共同講師裡面就秀出按鈕
                if (strUserId.Contains(Meeting_Resumelist.getProperty("in_user_id", "")))
                {
                    itmR.setProperty("pring_show", "1");//協辦者也要可以看到按鈕
                    usertype = true;
                }
            }

            //呼叫excel method取得計算後的人數 項目數 費用
            this.setProperty("meeting_id", meeting_id);
            this.setProperty("export_type", "pay");

            // 協辦者走 Admin
            if (isMeetingAdmin || usertype == true)
            {
                in_org_excels = this.apply("in_meeting_export_excel_admin");
            }
            else
            {
                in_org_excels = this.apply("in_meeting_export_excel_gym");
            }

            in_org_excels = in_org_excels.getRelationships("in_org_excel");

            //丟給前台顯示的資訊(未產生繳費單)
            for (int i = 0; i < in_org_excels.getItemCount(); i++)
            {
                Item in_org_excel = in_org_excels.getItemByIndex(i);

                if (in_org_excel.getProperty("in_current_org", "") != "")
                {
                    PaymentList = inn.newItem("sheet_content_a");
                    PaymentList.setProperty("in_current_org_a", in_org_excel.getProperty("in_current_org", ""));//所屬單位
                    PaymentList.setProperty("in_staffs_a", in_org_excel.getProperty("stuffs", ""));//隊職員數
                    PaymentList.setProperty("in_players_a", in_org_excel.getProperty("players", ""));//選手人數
                    PaymentList.setProperty("in_items_a", in_org_excel.getProperty("items", ""));//參賽總計
                    PaymentList.setProperty("in_expense_a", in_org_excel.getProperty("expenses", ""));//報名費用

                    PaymentList.setProperty("hide_stuff", itmR.getProperty("hide_stuff", ""));//隱藏隊職員數

                    PaymentList.setProperty("in_creator_a", in_org_excel.getProperty("in_creator", "").Trim(',').Replace(",", "<br>"));    //協助報名者姓名
                    PaymentList.setProperty("in_creator_tel_a", in_org_excel.getProperty("in_tel", "").Trim(',').Replace(",", "<br>"));//協助報名者電話

                    //CCO.Utilities.WriteDebug("In_Payment_List", "PaymentList:"+PaymentList.dom.InnerXml);

                    itmR.addRelationship(PaymentList);

                    sheet_content_a_count++;
                    content_a_sum += Int32.Parse(in_org_excel.getProperty("expenses", ""));
                }
            }

            //取得繳費單(依據[所屬群組],[賽事ID],[繳費單狀態-未繳費],[繳費單狀態-已繳費])
            aml = @"<AML>
	            <Item type='In_Meeting_pay' action='get' orderBy='item_number'>
		            <in_meeting>#meeting_id</in_meeting>
		            <or>
                        <pay_bool condition='like'>未繳費</pay_bool>
                        <pay_bool condition='like'>已繳費</pay_bool>
		            </or>
		        " + group + "</Item></AML>";

            aml = aml.Replace("#meeting_id", meeting_id);

            //CCO.Utilities.WriteDebug("In_Payment_List", "aml: " + aml);

            Item documents = inn.applyAML(aml);

            //CCO.Utilities.WriteDebug("In_Payment_List", "documents:"+documents.getItemCount().ToString());

            if (documents.getItemCount() != 0)
            {
                for (int i = 0; i < documents.getItemCount(); i++)
                {
                    Item document = documents.getItemByIndex(i);
                    //如果[實際收款金額為空] 就是尚未付款
                    if (document.getProperty("in_pay_amount_real", "") != "")
                    {
                        //丟給前台顯示的資訊(已付款繳費單)
                        PaymentList = inn.newItem("sheet_content_c");
                        PaymentList.setProperty("in_group_c", document.getProperty("in_group", ""));//群組
                        PaymentList.setProperty("in_current_org_c", document.getProperty("in_current_org", ""));//單位
                        PaymentList.setProperty("in_creator_c", document.getProperty("in_creator", ""));//協助報名者
                        PaymentList.setProperty("in_creator_sno", document.getProperty("in_creator_sno", ""));//協助報名者ID
                        PaymentList.setProperty("in_real_stuff_c", document.getProperty("in_real_stuff", ""));//隊職員數
                        PaymentList.setProperty("in_real_player_c", document.getProperty("in_real_player", ""));//選手人數
                        PaymentList.setProperty("in_real_items_c", document.getProperty("in_real_items", ""));//參賽總計
                        PaymentList.setProperty("in_pay_amount_c", document.getProperty("in_pay_amount_exp", ""));//報名費用
                        //PaymentList.setProperty("item_number_c","<span style='color: green;'> <i class='fa fa-check'></i> " + document.getProperty("item_number",""));//繳費編號(組顏色+圖示)
                        PaymentList.setProperty("item_number_c", document.getProperty("item_number", ""));//繳費編號(最乾淨)
                        string in_pay_date_c = document.getProperty("in_pay_date_exp", "").Split('T')[0];
                        PaymentList.setProperty("in_pay_date_c", in_pay_date_c.Replace('-', '/'));//繳費日期
                        PaymentList.setProperty("in_pay_amount1_c", document.getProperty("in_pay_amount_real", ""));//已繳費用
                        PaymentList.setProperty("in_code_1_c", document.getProperty("in_code_1", ""));//條碼1
                        PaymentList.setProperty("in_code_2_c", GetCodeMask(document.getProperty("in_code_2", "")));//條碼2
                        PaymentList.setProperty("in_code_3_c", document.getProperty("in_code_3", ""));//條碼3
                        PaymentList.setProperty("in_pay_date_exp1_c", document.getProperty("in_pay_date_exp1", ""));// 最後繳費期限
                        PaymentList.setProperty("index_number_c", document.getProperty("index_number", ""));// 業主自訂編號
                        PaymentList.setProperty("pay_bool_c", document.getProperty("pay_bool", ""));// 繳費狀態
                        PaymentList.setProperty("invoice_up_c", document.getProperty("invoice_up", ""));// 抬頭發票
                        PaymentList.setProperty("uniform_numbers_c", document.getProperty("uniform_numbers", ""));// 統一編號
                        PaymentList.setProperty("meeting_id", meeting_id);//賽事ID
                        PaymentList.setProperty("open_function", open_function);
                        PaymentList.setProperty("method_type", method_type);//名單method
                        PaymentList.setProperty("in_pay_type_c", "<span style='color: green;'><i class='fa fa-list-alt'></i> " + "已繳費");//繳費狀態(顏色+圖示)

                        PaymentList.setProperty("hide_stuff", itmR.getProperty("hide_stuff", ""));//隱藏隊職員數
                        if (in_need_receipt == "1")
                        {
                            PaymentList.setProperty("show_receipt", "item_show_1");
                        }
                        else
                        {
                            PaymentList.setProperty("show_receipt", "item_show_0");
                        }

                        //退款金額
                        PaymentList.setProperty("in_refund_amount", document.getProperty("in_refund_amount", ""));
                        string in_amount = (Convert.ToInt32(document.getProperty("in_pay_amount_real", "0")) - Convert.ToInt32(document.getProperty("in_refund_amount", "0"))).ToString();
                        PaymentList.setProperty("in_amount_c", in_amount);

                        itmR.addRelationship(PaymentList);
                        sheet_content_c_count++;
                        content_c_sum += Int32.Parse(document.getProperty("in_pay_amount_exp", ""));
                    }
                    else
                    {

                        if (s_type == "1")
                        {
                            if (document.getProperty("in_pay_photo", "") == "")
                            {
                                continue;
                            }
                        }

                        //丟給前台顯示的資訊(未付款繳費單)
                        PaymentList = inn.newItem("sheet_content_b");
                        PaymentList.setProperty("in_group_b", document.getProperty("in_group", ""));//群組
                        PaymentList.setProperty("in_current_org_b", document.getProperty("in_current_org", ""));//單位
                        PaymentList.setProperty("in_creator_sno", document.getProperty("in_creator_sno", ""));//協助報名者
                        PaymentList.setProperty("in_real_stuff_b", document.getProperty("in_real_stuff", ""));//隊職員數
                        PaymentList.setProperty("in_real_player_b", document.getProperty("in_real_player", ""));//選手人數
                        PaymentList.setProperty("in_real_items_b", document.getProperty("in_real_items", ""));//參賽總計
                        PaymentList.setProperty("in_pay_amount_b", document.getProperty("in_pay_amount_exp", ""));//報名費用
                        //PaymentList.setProperty("item_number_b","<span style='color: red;'> <i class='fa fa-times'></i> " + document.getProperty("item_number",""));//繳費編號(組顏色+圖示)
                        PaymentList.setProperty("item_number_b", document.getProperty("item_number", ""));//繳費編號(最乾淨)
                        string in_pay_date_b = document.getProperty("in_pay_date_exp", "").Split('T')[0];
                        PaymentList.setProperty("in_pay_date_b", in_pay_date_b.Replace('-', '/'));//繳費日期
                        PaymentList.setProperty("in_code_1_b", document.getProperty("in_code_1", ""));//條碼1
                        PaymentList.setProperty("in_code_2_b", GetCodeMask(document.getProperty("in_code_2", "")));//條碼2
                        PaymentList.setProperty("in_code_3_b", document.getProperty("in_code_3", ""));//條碼3
                        PaymentList.setProperty("in_pay_date_exp1_b", document.getProperty("in_pay_date_exp1", ""));// 最後繳費期限
                        PaymentList.setProperty("index_number_b", document.getProperty("index_number", ""));// 業主自訂編號
                        PaymentList.setProperty("pay_bool_b", document.getProperty("pay_bool", ""));// 繳費狀態
                        PaymentList.setProperty("invoice_up_b", document.getProperty("invoice_up", ""));// 抬頭發票
                        PaymentList.setProperty("uniform_numbers_b", document.getProperty("uniform_numbers", ""));// 統一編號
                        PaymentList.setProperty("meeting_id", meeting_id);//賽事ID
                        PaymentList.setProperty("in_pay_photo_b", document.getProperty("in_pay_photo", ""));//繳費照片
                        PaymentList.setProperty("method_type", method_type);//名單method

                        //檢查[繳費照片]是否有值

                        if (document.getProperty("in_pay_photo", "") != "" && document.getProperty("in_return_mark", "") == "1")
                        {
                            //有上傳照片且退回標記被打勾為[審核退回]
                            PaymentList.setProperty("in_pay_type_b", "<span style='color: red;'><i class='fa fa-list-alt'></i> " + "審核退回");//繳費狀態(顏色+圖示)
                        }
                        else if (document.getProperty("in_pay_photo", "") != "")
                        {
                            //有上傳照片為[審核中]
                            PaymentList.setProperty("in_pay_type_b", "<span style='color: green;'><i class='fa fa-list-alt'></i> " + "審核中");//繳費狀態(顏色+圖示)
                        }
                        else
                        {
                            //沒有照片為[未繳費]
                            PaymentList.setProperty("in_pay_type_b", "<span style='color: red;'><i class='fa fa-list-alt'></i> " + "未繳費");//繳費狀態(顏色+圖示)
                        }

                        //檢查[代收機構]是否有值(有代收機構=有拿到此單的notice檔案)
                        if (document.getProperty("in_collection_agency", "") != "")
                        {
                            PaymentList.setProperty("in_pay_type_b", "<span style='color: green;'><i class='fa fa-list-alt'></i> " + "已繳費.");//繳費狀態(顏色+圖示)
                        }

                        PaymentList.setProperty("hide_stuff", itmR.getProperty("hide_stuff", ""));//隱藏隊職員數
                        if (in_need_receipt == "1")
                        {
                            PaymentList.setProperty("show_receipt", "item_show_1");
                        }
                        else
                        {
                            PaymentList.setProperty("show_receipt", "item_show_0");
                        }

                        PaymentList.setProperty("in_creator_b", document.getProperty("in_creator", ""));    //協助報名者姓名

                        sql = "SELECT TOP 1 * FROM IN_RESUME WITH(NOLOCK) WHERE in_sno = '" + document.getProperty("in_creator_sno", "") + "'";
                        Item itmMUserResume = inn.applySQL(sql);
                        if (itmMUserResume.getItemCount() == 1)
                        {
                            PaymentList.setProperty("in_creator_tel_b", itmMUserResume.getProperty("in_tel", ""));//協助報名者電話
                        }

                        itmR.addRelationship(PaymentList);
                        sheet_content_b_count++;
                        content_b_sum += Int32.Parse(document.getProperty("in_pay_amount_exp", ""));
                    }
                }
            }

            //2020.10.30若無未產生+未付款+已付款 代表此單位沒有任何繳費資訊 丟出特徵讓前端將使用者導向info
            if (documents.getItemCount() == 0 && in_org_excels.getItemCount() == 0)
            {
                itmR.setProperty("in_error", "X");
            }

            //附加申請異動清單
            AppendApplyList(CCO, strMethodName, inn, itmR);

            itmR.setProperty("meeting_name", meeting_name);
            itmR.setProperty("id", meeting_id);
            itmR.setProperty("player_group", in_group);
            itmR.setProperty("method_type", method_type);
            itmR.setProperty("inn_meeting_admin", isMeetingAdmin ? "1" : "0");
            itmR.setProperty("inn_excel_method", method_type);
            itmR.setProperty("in_verify_mode", in_verify_mode);

            itmR.setProperty("sheet_count_a", sheet_content_a_count.ToString() + "張單");
            itmR.setProperty("sheet_sum_a", ",共" + content_a_sum.ToString("N0") + "元");//千分位格式 會補0

            itmR.setProperty("sheet_count_b", sheet_content_b_count.ToString() + "張單");
            itmR.setProperty("sheet_sum_b", ",共" + content_b_sum.ToString("N0") + "元");

            itmR.setProperty("sheet_count_c", sheet_content_c_count.ToString() + "張單");
            itmR.setProperty("sheet_sum_c", ",共" + content_c_sum.ToString("N0") + "元");



            itmR.setProperty("in_title", meeting_name);
            itmR.setProperty("in_url", in_url);
            itmR.setProperty("in_register_url", in_register_url);
            itmR.setProperty("in_group", in_group);

            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);
            //CCO.Utilities.WriteDebug(strMethodName, "itmR:" + itmR.dom.InnerXml);

            return itmR;
        }

        //附加申請異動清單
        private void AppendApplyList(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            Dictionary<string, TPayGroup> map = GetApplyPayList(CCO, strMethodName, inn, itmReturn);

            TPayGroup refund_group = map.ContainsKey("退費申請中") ? map["退費申請中"] : EmptyPayGroup("退費申請中");
            TPayGroup exchange_group = map.ContainsKey("變更申請中") ? map["變更申請中"] : EmptyPayGroup("變更申請中");
            TPayGroup lvchange_group = map.ContainsKey("換組申請中") ? map["換組申請中"] : EmptyPayGroup("換組申請中");

            itmReturn.setProperty("refund_list", refund_group.RowCount.ToString() + "張單");
            itmReturn.setProperty("refund_list_sum", ",共" + refund_group.AmountTotal.ToString("N0") + "元");

            itmReturn.setProperty("exchange_list", exchange_group.RowCount.ToString() + "張單");
            itmReturn.setProperty("lvchange_list", lvchange_group.RowCount.ToString() + "張單");

            //取得該賽事有[申請退費的繳費單資訊]  (退費申請)
            AppendPayRows(CCO, strMethodName, inn, refund_group, "refundlist", itmReturn);
            //取得該賽事有[申請變更的繳費單資訊]  (變更申請)
            AppendPayRows(CCO, strMethodName, inn, exchange_group, "exchangelist", itmReturn);
            //取得該賽事有[申請換組的繳費單資訊]  (換組申請)
            AppendPayRows(CCO, strMethodName, inn, lvchange_group, "lvchangelist", itmReturn);
        }

        private void AppendPayRows(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, TPayGroup group, string type, Item itmReturn)
        {
            string meeting_id = itmReturn.getProperty("meeting_id", "");

            for (int i = 0; i < group.Teams.Count; i++)
            {
                TTeam team = group.Teams[i];
                Item itmTeam = team.Value;

                Item item = inn.newItem(type);
                item.setProperty("inn_no", (i + 1).ToString());//項次
                item.setProperty("id", itmTeam.getProperty("news_id", ""));//news_id
                item.setProperty("item_number", itmTeam.getProperty("item_number", ""));//繳費單號
                item.setProperty("in_pay_amount", itmTeam.getProperty("in_pay_amount", ""));//費用
                item.setProperty("in_section_name", itmTeam.getProperty("in_section_name", ""));//組名
                item.setProperty("in_index", itmTeam.getProperty("in_index", ""));//序號
                item.setProperty("in_cancel_memo", itmTeam.getProperty("in_cancel_memo", ""));//退費說明
                item.setProperty("in_new_levels", itmTeam.getProperty("in_new_levels", "").Replace("@", "-"));//新組別
                item.setProperty("in_refund_applicant", itmTeam.getProperty("last_name", ""));//退費申請人
                item.setProperty("in_name", itmTeam.getProperty("in_name", ""));//選手
                item.setProperty("meeting_id", meeting_id);//賽事ID(超連結用)

                itmReturn.addRelationship(item);
            }
        }

        private Dictionary<string, TPayGroup> GetApplyPayList(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            string meeting_id = itmReturn.getProperty("meeting_id", "");

            string sql = @"
                SELECT
                    t1.id AS 'news_id'
                    , ISNULL(t1.in_l1, '') + '-' + ISNULL(t1.in_l2, '') + '-' + ISNULL(t1.in_l3, '') + '-' + t1.in_index AS 'team_key'
                    , t1.in_pay_amount
                    , t1.in_section_name
                    , t1.in_index
                    , t1.in_cancel_status
                    , t1.in_cancel_memo
                    , t1.in_cancel_by_id
                    , t1.in_new_levels
                    , t1.in_old_sections
                    , t1.in_name
                    , t2.item_number
                    , t3.last_name
                FROM 
                    IN_MEETING_NEWS t1 WITH(NOLOCK) 
                INNER JOIN 
                    IN_MEETING_PAY t2 WITH(NOLOCK)
                    ON t2.id = t1.source_id 
                LEFT OUTER JOIN
                    [USER] t3 WITH(NOLOCK)
                    ON t3.owned_by_id = t1.in_cancel_by_id
                WHERE
                    t2.in_meeting = '{#meeting_id}'
                    AND ISNULL(t1.in_cancel_status, '') <> ''
                ORDER BY
                    t1.in_cancel_time
                    , team_key
            ";

            sql = sql.Replace("{#meeting_id}", meeting_id);

            Dictionary<string, TPayGroup> map = new Dictionary<string, TPayGroup>();
            Item items = inn.applySQL(sql);
            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_cancel_status = item.getProperty("in_cancel_status", "");
                string team_key = item.getProperty("team_key", "");

                TPayGroup group = null;
                if (map.ContainsKey(in_cancel_status))
                {
                    group = map[in_cancel_status];
                }
                else
                {
                    group = EmptyPayGroup(in_cancel_status);
                    map.Add(in_cancel_status, group);
                }

                TTeam team = group.Teams.Find(x => x.key == team_key);

                if (team == null)
                {
                    team = new TTeam
                    {
                        key = team_key,
                        in_pay_amount = item.getProperty("in_pay_amount", "0"),
                        Value = item,
                        Players = new List<Item>(),
                    };

                    team.amount = GetIntVal(team.in_pay_amount);

                    group.Teams.Add(team);
                }

                team.Players.Add(item);
            }

            foreach (var kv in map)
            {
                var group = kv.Value;
                group.RowCount = group.Teams.Count;
                group.AmountTotal = group.Teams.Sum(x => x.amount);
            }

            return map;
        }

        private TPayGroup EmptyPayGroup(string in_cancel_status)
        {
            return new TPayGroup
            {
                in_cancel_status = in_cancel_status,
                RowCount = 0,
                AmountTotal = 0,
                Teams = new List<TTeam>(),
            };
        }

        private class TPayGroup
        {
            public string in_cancel_status { get; set; }
            public int RowCount { get; set; }
            public int AmountTotal { get; set; }
            public List<TTeam> Teams { get; set; }
        }

        private class TTeam
        {
            public string key { get; set; }
            public string in_pay_amount { get; set; }
            public int amount { get; set; }
            public Item Value { get; set; }
            public List<Item> Players { get; set; }
        }

        //取得共同講師 (判斷現在的登入者是否為此課程的共同講師)
        private bool GetUserResumeListStatus(Innovator inn, string meeting_id, string strUserId)
        {
            string sql = @"
                SELECT
                    t2.id
                FROM 
                    In_Meeting_Resumelist t1 WITH(NOLOCK) 
                INNER JOIN 
                    IN_RESUME t2 WITH(NOLOCK) 
                    ON t2.id = t1.related_id    
                WHERE 
                    t1.source_id = '{#meeting_id}' 
                    AND t2.in_user_id = '{#in_user_id}';    
            ";

            sql = sql.Replace("{#meeting_id}", meeting_id)
                .Replace("{#in_user_id}", strUserId);

            Item item = inn.applySQL(sql);

            if (item.getResult() != "")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //in_meeting_pay 條碼2 需加上遮罩
        private string GetCodeMask(string value)
        {
            if (value.Length == 16)
            {
                return value.Substring(0, 4) + "*******" + value.Substring(value.Length - 5, 5);
            }
            return "";
        }

        private int GetIntVal(string value)
        {
            if (value == "" || value == "0") return 0;

            int result = 0;
            int.TryParse(value, out result);
            return result;
        }
    }
}