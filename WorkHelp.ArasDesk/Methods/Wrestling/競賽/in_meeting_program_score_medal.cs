﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;
using Xceed.Document.NET;

namespace WorkHelp.ArasDesk.Methods.Wresling.Common
{
    public class in_meeting_program_score_medal : Item
    {
        public in_meeting_program_score_medal(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 角力 獎狀
                日誌: 
                    - 2024-05-24: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "in_meeting_program_score_medal";

            var itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                meeting_id = itmR.getProperty("meeting_id", ""),
                program_id = itmR.getProperty("program_id", ""),
                team_id = itmR.getProperty("team_id", ""),
                scene = itmR.getProperty("scene", ""),
            };

            switch (cfg.scene)
            {
                case "group_download":
                    GroupDownload(cfg, itmR);
                    break;
                case "download":
                    SoloDownload(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void GroupDownload(TConfig cfg, Item itmReturn)
        {
            cfg.itmMeeting = cfg.inn.applySQL("SELECT in_title, item_number, in_document_number FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            cfg.mt_title = cfg.itmMeeting.getProperty("in_title", "");
            cfg.mt_number = cfg.itmMeeting.getProperty("item_number", "");
            cfg.mt_document_number = cfg.itmMeeting.getProperty("in_document_number", "");

            cfg.in_l1 = itmReturn.getProperty("in_l1", "");
            cfg.in_l2 = itmReturn.getProperty("in_l2", "");
            cfg.org_group = itmReturn.getProperty("org_group", "");
            cfg.org_name = itmReturn.getProperty("org_name", "");
            cfg.org_rank = itmReturn.getProperty("org_rank", "");
            cfg.in_fight_day = GetMaxFightDay(cfg);
            if (cfg.in_fight_day == "") throw new Exception("查無比賽日期");

            var xls_parm_name = "rank_medal_path";
            cfg.itmXlsx = cfg.inn.applyMethod("In_Variable_File", "<in_name>" + xls_parm_name + "</in_name>");
            cfg.template_path = cfg.itmXlsx.getProperty("template_path", "");
            cfg.export_path = cfg.itmXlsx.getProperty("export_path", "");

            var name1 = "meeting_rank_medal";
            var name2 = "meeting_group_medal_" + cfg.mt_number;
            cfg.template_path = cfg.template_path.Replace(name1, name2);

            //匯出檔名
            var xlsName = cfg.mt_title
                + "_獎狀"
                + "_團體組"
                + "_" + cfg.org_group
                + "_" + cfg.org_name
                + "_" + System.DateTime.Now.ToString("HHmmss");

            xlsName = xlsName.Replace("+", "")
                            .Replace("-", "")
                            .Replace(".", "")
                            .Replace(" ", "");

            string extName = ".docx";
            string xls_file = cfg.export_path + xlsName + extName;
            string xls_name = xlsName + extName;

            if (!System.IO.Directory.Exists(cfg.export_path)) System.IO.Directory.CreateDirectory(cfg.export_path);

            //載入模板
            using (var doc = Xceed.Words.NET.DocX.Load(cfg.template_path))
            {
                AppendGroupWordTable(cfg, doc);
                doc.Tables[0].Remove();
                doc.RemoveParagraphAt(0);
                doc.SaveAs(xls_file);
            }

            //下載路徑
            itmReturn.setProperty("xls_name", xls_name);
        }

        private string GetMaxFightDay(TConfig cfg)
        {
            var sql = @"
                SELECT 
	                MAX(in_fight_day) AS 'mx_fight_day'
                FROM 
	                IN_MEETING_PROGRAM WITH(NOLOCK) 
                WHERE 
	                in_meeting = '{#meeting_id}'
	                AND in_l1 = '{#in_l1}'
	                AND in_l2 = '{#in_l2}'
	                AND ISNULL(in_fight_day, '') <> ''
            ";
            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_l1}", cfg.in_l1)
                .Replace("{#in_l2}", cfg.in_l2);
            var itmResult = cfg.inn.applySQL(sql);
            if (itmResult.getResult() == "") return "";
            return itmResult.getProperty("mx_fight_day", "");
        }

        private void SoloDownload(TConfig cfg, Item itmReturn)
        {
            cfg.itmMeeting = cfg.inn.applySQL("SELECT in_title, item_number, in_document_number FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            cfg.mt_title = cfg.itmMeeting.getProperty("in_title", "");
            cfg.mt_number = cfg.itmMeeting.getProperty("item_number", "");
            cfg.mt_document_number = cfg.itmMeeting.getProperty("in_document_number", "");

            cfg.itmProgram = cfg.inn.applySQL("SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + cfg.program_id + "'");
            cfg.in_l1 = cfg.itmProgram.getProperty("in_l1", "");
            cfg.in_l2 = cfg.itmProgram.getProperty("in_l2", "");
            cfg.in_l3 = cfg.itmProgram.getProperty("in_l3", "");
            cfg.in_name3 = cfg.itmProgram.getProperty("in_name3", "");
            cfg.in_fight_day = cfg.itmProgram.getProperty("in_fight_day", "");
            cfg.in_n2 = cfg.itmProgram.getProperty("in_n2", "");
            cfg.in_n3 = cfg.itmProgram.getProperty("in_n3", "");
            cfg.in_short_name = cfg.itmProgram.getProperty("in_short_name", "");
            cfg.sect_file_name = cfg.in_l1 + cfg.in_n2 + cfg.in_n3;
            cfg.tw_day = MapTwDay(cfg.in_fight_day);

            var xls_parm_name = "rank_medal_path";
            cfg.itmXlsx = cfg.inn.applyMethod("In_Variable_File", "<in_name>" + xls_parm_name + "</in_name>");
            cfg.template_path = cfg.itmXlsx.getProperty("template_path", "");
            cfg.export_path = cfg.itmXlsx.getProperty("export_path", "");

            var name1 = "meeting_rank_medal";
            var name2 = "meeting_rank_medal_" + cfg.mt_number;
            cfg.template_path = cfg.template_path.Replace(name1, name2);

            var players = MaoRankPlayers(cfg);
            if (players.Count == 0) throw new Exception("查無選手");

            //匯出檔名
            var xlsName = cfg.mt_title
                + "_獎狀"
                + "_" + cfg.sect_file_name
                + "_" + System.DateTime.Now.ToString("HHmmss");

            if (cfg.team_id != "")
            {
                xlsName = cfg.mt_title
                    + "_獎狀"
                    + "_" + cfg.sect_file_name
                    + "_" + players[0].in_current_org + "_" + players[0].in_name
                    + "_" + System.DateTime.Now.ToString("HHmmss");
            }

            xlsName = xlsName.Replace("+", "")
                            .Replace("-", "")
                            .Replace(".", "")
                            .Replace(" ", "");

            string extName = ".docx";
            string xls_file = cfg.export_path + xlsName + extName;
            string xls_name = xlsName + extName;

            if (!System.IO.Directory.Exists(cfg.export_path)) System.IO.Directory.CreateDirectory(cfg.export_path);

            //載入模板
            using (var doc = Xceed.Words.NET.DocX.Load(cfg.template_path))
            {
                int end_idx = players.Count - 1;
                for (int i = 0; i < players.Count; i++)
                {
                    AppendWordTable(cfg, doc, players[i], i == end_idx);
                }

                doc.Tables[0].Remove();
                doc.RemoveParagraphAt(0);

                //儲存檔案
                doc.SaveAs(xls_file);
            }

            //下載路徑
            itmReturn.setProperty("xls_name", xls_name);
        }
        
        private void AppendGroupWordTable(TConfig cfg, Xceed.Words.NET.DocX doc)
        {
            //取得模板表格
            var template_table = doc.Tables[0];

            var table = doc.InsertTable(template_table);

            var fontName = "標楷體";

            //單位
            var orgCell = table.Rows[7].Cells[1].Paragraphs.First();
            //組別
            var sectCell = table.Rows[8].Cells[1].Paragraphs.First();
            //名次
            var rankCell = table.Rows[9].Cells[1].Paragraphs.First();
            //日期
            //var dayCell = table.Rows[15].Cells[0].Paragraphs.First();

            orgCell.Append(cfg.org_name).Font(fontName).FontSize(22).Bold();
            sectCell.Append(cfg.org_group).Font(fontName).FontSize(22).Bold();
            rankCell.Append(cfg.org_rank).Font(fontName).FontSize(22).Bold();
            //dayCell.ReplaceText("[$day_info$]", cfg.tw_day);
        }

        private void AppendWordTable(TConfig cfg, Xceed.Words.NET.DocX doc, TPlayer player, bool is_end)
        {
            //以下 Cell 位置從分析 Word 得出
            //Table[0] Rows[1] Cells[0] = 中華民國角力協會
            //Table[0] Rows[3] Cells[0] = 獎 狀
            //Table[0] Rows[4] Cells[0] = 臺教體署競(二)字第 1130001200號函
            //Table[0] Rows[5] Cells[0] = 中華民國 113 年度
            //Table[0] Rows[6] Cells[0] = 金德盃全國小學生角力錦標賽
            //Table[0] Rows[7] Cells[1] = 單位：
            //Table[0] Rows[8] Cells[1] = 姓名：
            //Table[0] Rows[9] Cells[1] = 組別：
            //Table[0] Rows[10] Cells[1] = 級別：
            //Table[0] Rows[11] Cells[1] = 名次：
            //Table[0] Rows[13] Cells[0] = 大 會 會 長
            //Table[0] Rows[13] Cells[1] = 張 聰 榮
            //Table[0] Rows[15] Cells[0] = 中華民國113年5月25日

            //取得模板表格
            var template_table = doc.Tables[0];

            var table = doc.InsertTable(template_table);

            //單位
            var orgCell = table.Rows[7].Cells[1].Paragraphs.First();
            //姓名
            var nameCell = table.Rows[8].Cells[1].Paragraphs.First();
            //組別
            var sectCell = table.Rows[9].Cells[1].Paragraphs.First();
            //級別
            var gradeCell = table.Rows[10].Cells[1].Paragraphs.First();
            //名次
            var rankCell = table.Rows[11].Cells[1].Paragraphs.First();
            //日期
            var dayCell = table.Rows[15].Cells[0].Paragraphs.First();

            var fontName = "標楷體";


            var sect = cfg.in_l1 == "個人組"
                ? cfg.in_l2
                : cfg.in_l1.Replace(" ", "") + cfg.in_l2;

            var arr = cfg.in_name3.Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);
            //var weight = arr != null && arr.Length > 2 ? arr[1] + "-" + arr[2] : cfg.in_l3;
            var weight = cfg.in_l3.Replace(" ", "").Replace("公斤以下", "kg以下");

            orgCell.Append(player.in_current_org).Font(fontName).FontSize(22).Bold();
            nameCell.Append(player.in_name).Font(fontName).FontSize(22).Bold();
            sectCell.Append(sect).Font(fontName).FontSize(22).Bold();
            //gradeCell.Append(cfg.in_l3).Font(fontName).FontSize(22).Bold();
            gradeCell.Append(weight).Font(fontName).FontSize(22).Bold();
            rankCell.Append(player.rank_text).Font(fontName).FontSize(22).Bold();
            dayCell.ReplaceText("[$day_info$]", cfg.tw_day);
        }

        private void LoadDocPosition(TConfig cfg, Xceed.Words.NET.DocX doc)
        {
            var content = new StringBuilder();

            var tableCount = doc.Tables.Count;
            for (var i = 0; i < tableCount; i++)
            {
                var table = doc.Tables[i];
                var rows = table.Rows;
                for (var j = 0; j < rows.Count; j++)
                {
                    var row = rows[j];
                    var cells = row.Cells;
                    for (var k = 0; k < cells.Count; k++)
                    {
                        var cell = cells[k];
                        foreach (var p in cell.Paragraphs)
                        {
                            string text = p.Text;
                            if (string.IsNullOrWhiteSpace(text)) continue;
                            content.AppendLine("Table[" + i + "] Rows[" + j + "] Cells[" + k + "] = " + text);
                        }
                    }
                }
            }

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, content.ToString());
        }


        private List<TPlayer> MaoRankPlayers(TConfig cfg)
        {
            var rows = new List<TPlayer>();
            var items = GetRankPlayers(cfg);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var row = new TPlayer
                {
                    in_current_org = item.getProperty("map_short_org", ""),
                    in_name = item.getProperty("in_name", ""),
                    in_final_rank = item.getProperty("in_final_rank", ""),
                };
                row.rank_text = GetRankText(row.in_final_rank);
                if (row.rank_text == "") continue;
                rows.Add(row);
            }
            return rows;
        }

        private string GetRankText(string value)
        {
            switch (value)
            {
                case "1": return "第一名";
                case "2": return "第二名";
                case "3": return "第三名";
                case "4": return "第四名";
                case "5": return "第五名";
                case "6": return "第六名";
                case "7": return "第七名";
                case "8": return "第八名";
                case "9": return "第九名";
                case "10": return "第十名";
                default: return "";
            }
        }

        private Item GetRankPlayers(TConfig cfg)
        {
            var cond = cfg.team_id != "" ? "AND id = '" + cfg.team_id + "'" : "";

            var sql = @"
                SELECT
	                in_current_org
	                , map_short_org
	                , in_name
	                , in_final_rank
                FROM
	                VU_MEETING_PTEAM WITH(NOLOCK)
                WHERE
	                source_id = '{#program_id}'
	                AND ISNULL(in_final_rank, 0) > 0
	                {#cond}
                ORDER BY
	                in_final_rank
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#program_id}", cfg.program_id)
                .Replace("{#cond}", cond);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string meeting_id { get; set; }
            public string program_id { get; set; }
            public string team_id { get; set; }
            public string scene { get; set; }
            public Item itmMeeting { get; set; }
            public Item itmProgram { get; set; }
            public Item itmXlsx { get; set; }
            public string mt_title { get; set; }
            public string mt_number { get; set; }
            public string mt_document_number { get; set; }
            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }
            public string in_name3 { get; set; }
            public string in_n2 { get; set; }
            public string in_n3 { get; set; }
            public string in_fight_day { get; set; }
            public string in_short_name { get; set; }
            public string template_path { get; set; }
            public string export_path { get; set; }

            public string sect_file_name { get; set; }
            public string tw_day { get; set; }

            public string org_group { get; set; }
            public string org_name { get; set; }
            public string org_rank { get; set; }
        }

        public string MapTwDay(string value)
        {
            var arr = value.Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);
            if (arr == null || arr.Length != 3) return "";

            var y = GetInt(arr[0]) - 1911;
            var m = GetInt(arr[1]);
            var d = GetInt(arr[2]);

            return "中華民國" + y + "年" + m + "月" + d + "日";
        }
        private class TPlayer
        {
            public string in_current_org { get; set; }
            public string in_name { get; set; }
            public string in_final_rank { get; set; }
            public string rank_text { get; set; }
        }

        private int GetInt(string value, int def = 0)
        {
            int result = def;
            int.TryParse(value, out result);
            return result;
        }
    }
}