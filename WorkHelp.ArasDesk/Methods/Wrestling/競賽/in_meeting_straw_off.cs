﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Wrestling.Common
{
    public class in_meeting_straw_off : Item
    {
        public in_meeting_straw_off(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 過磅抽籤(請假)
                日期: 
                    - 2021/12/21: 移出名單改為參數化 (lina)
                    - 2021/09/10: 改為 Aras InnerXml 抽籤 (lina)
                    - 2021-01-28: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]in_meeting_straw_off";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, this.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                inn = inn,
                aras_vault_url = @"C:\Aras\Vault\" + strDatabaseName,

                scene = itmR.getProperty("scene", ""),
                meeting_id = itmR.getProperty("meeting_id", ""),
                in_l1 = itmR.getProperty("l1_value", ""),
                in_l2 = itmR.getProperty("l2_value", ""),
                in_l3 = itmR.getProperty("l3_value", ""),
                muid = itmR.getProperty("muid", ""),
                no_straw = itmR.getProperty("no_straw", ""),
            };

            cfg.itmMeeting = GetMeeting(cfg);
            cfg.mt_battle_type = cfg.itmMeeting.getProperty("in_battle_type", "");
            cfg.mt_robin_player = GetInt(cfg.itmMeeting.getProperty("in_robin_player", "0"));

            cfg.itmProgram = GetProgram(cfg);
            cfg.program_id = cfg.itmProgram.getProperty("id", "");
            cfg.old_team_count = GetInt(cfg.itmProgram.getProperty("in_team_count", "0"));
            cfg.old_battle_type = cfg.itmProgram.getProperty("in_battle_type", "");

            cfg.new_team_count = cfg.old_team_count;
            cfg.new_battle_type = cfg.old_battle_type;


            Straw(cfg, itmR);

            return itmR;
        }

        //抽籤
        private void Straw(TConfig cfg, Item itmReturn)
        {
            Item itmMUsers = GetMUsers(cfg);

            //更新選手狀態
            UpdateMPTeam(cfg, itmMUsers);

            cfg.new_team_count = GetMPTeamCount(cfg);

            //隊伍數變動 or 賽制變動
            if (cfg.old_team_count != cfg.new_team_count)
            {
                //清除暫存資訊(三階選單)
                ClearCache(cfg);

                //更新組別資訊
                UpdateProgram(cfg);

                Item item = cfg.inn.newItem();
                item.setType("In_Meeting_Program");
                item.setProperty("meeting_id", cfg.meeting_id);
                item.setProperty("program_id", cfg.program_id);
                item.setProperty("in_fight_day", "");
                item.setProperty("scene", "create");
                item.apply("In_Meeting_DrawEvent");

            }
        }

        //清除暫存資訊(三階選單)
        private void ClearCache(TConfig cfg)
        {
            Item itmData = cfg.inn.newItem();
            itmData.setType("In_Meeting");
            itmData.setProperty("meeting_id", cfg.meeting_id);
            itmData.setProperty("scene", "clear");
            itmData.apply("in_meeting_program_options");

            Item itmData2 = cfg.inn.newItem();
            itmData2.setType("In_Meeting");
            itmData2.setProperty("meeting_id", cfg.meeting_id);
            itmData2.setProperty("scene", "clear");
            itmData2.apply("in_meeting_day_options");
        }

        /// <summary>
        /// 自動晉級
        /// </summary>
        private void AutoUpgradeDQ(TConfig cfg)
        {
            Item item = cfg.inn.newItem();
            item.setType("In_Meeting_Program");
            item.setProperty("meeting_id", cfg.meeting_id);
            item.setProperty("program_id", cfg.program_id);
            item.setProperty("mode", "dq_upgrade");
            item.apply("in_meeting_program_score");
        }

        #region 抽籤

        //整組抽籤(迴圈執行)
        private void RunDraw(TConfig cfg)
        {
            Item itmPrograms = GetProgram(cfg);

            int count = itmPrograms.getItemCount();

            if (count <= 0)
            {
                return;
            }

            for (int i = 0; i < count; i++)
            {
                Item itmProgram = itmPrograms.getItemByIndex(i);
                string meeting_id = itmProgram.getProperty("in_meeting", "");
                string program_id = itmProgram.getProperty("id", "");

                itmProgram.setType("In_Meeting_Program");
                itmProgram.setProperty("draw_type", "2");     //組別抽籤
                itmProgram.setProperty("draw_integral", "Y"); //種子籤
                itmProgram.setProperty("draw_99", "N");       //99號籤
                itmProgram.setProperty("draw_sameteam", "Y"); //同隊分面

                itmProgram.setProperty("meeting_id", meeting_id);
                itmProgram.setProperty("program_id", program_id);
                itmProgram.setProperty("program_name", itmProgram.getProperty("in_name", ""));
                itmProgram.setProperty("program_name2", itmProgram.getProperty("in_name2", ""));
                itmProgram.setProperty("program_name3", itmProgram.getProperty("in_name3", ""));
                itmProgram.setProperty("team_count", itmProgram.getProperty("in_team_count", ""));

                Item itmMethodResult = itmProgram.apply("in_meeting_draw_run");

                if (itmMethodResult.isError())
                {
                    throw new Exception("抽籤失敗");
                }
            }
        }

        #endregion

        //重設場次編號與狀態
        private void ResetEvents(TConfig cfg)
        {
            Item item = cfg.inn.newItem();
            item.setType("In_Meeting_Program");
            item.setProperty("meeting_id", cfg.meeting_id);
            item.setProperty("program_id", cfg.program_id);
            item.apply("in_meeting_pevent_reset");
        }

        //重建場次
        private void RegenerateEvents(TConfig cfg)
        {
            Item item = cfg.inn.newItem();
            item.setType("In_Meeting_Program");
            item.setProperty("meeting_id", cfg.meeting_id);
            item.setProperty("program_id", cfg.program_id);
            item.setProperty("battle_type", cfg.new_battle_type);

            string method = "";
            switch (cfg.new_battle_type)
            {
                case "TopTwo"://單淘
                    method = "in_meeting_program_4column";
                    break;

                case "JudoTopFour"://四柱復活
                    method = "in_meeting_program_4column";
                    break;

                case "Challenge"://挑戰賽
                    method = "in_meeting_program_4column";
                    break;

                case "SingleRoundRobin"://單循環
                    method = "in_meeting_program_robin";
                    break;

                case "DoubleRoundRobin"://雙循環
                    method = "in_meeting_program_robin";
                    break;

                default:
                    break;
            }

            if (method != "")
            {
                item.apply(method);
            }
        }

        //更新組別資訊
        private void UpdateProgram(TConfig cfg)
        {


            string sql = @"
                UPDATE IN_MEETING_PROGRAM SET
                    in_team_count = '{#in_team_count}'
                    , in_rank_count = '{#in_team_count}'
                    , in_round_code = '{#in_round_code}'
                    , in_round_count = '{#in_round_count}'
                WHERE
                    id = '{#program_id}'
            ";

            sql = sql.Replace("{#program_id}", cfg.program_id)
                .Replace("{#in_team_count}", cfg.new_team_count.ToString())
                .Replace("{#in_battle_type}", cfg.new_battle_type)
                .Replace("{#in_round_code}", round_code)
                .Replace("{#in_round_count}", round_count);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item itmSQL = cfg.inn.applySQL(sql);
            if (itmSQL.isError())
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "執行失敗 sql: " + sql);
            }
        }

        //更新選手狀態
        private void UpdateMPTeam(TConfig cfg, Item itmMUsers)
        {
            string sql = "";

            int count = itmMUsers.getItemCount();

            //過磅狀態參數表
            var status_map = MergeWStatus(cfg);

            for (int i = 0; i < count; i++)
            {
                Item itmMUser = itmMUsers.getItemByIndex(i);
                string muid = itmMUser.getProperty("id", "");
                string in_l1 = itmMUser.getProperty("in_l1", "");
                string in_l2 = itmMUser.getProperty("in_l2", "");
                string in_l3 = itmMUser.getProperty("in_l3", "");
                string in_index = itmMUser.getProperty("in_index", "");
                string in_creator_sno = itmMUser.getProperty("in_creator_sno", "");
                string in_weight_result = itmMUser.getProperty("in_weight_result", "");
                string in_weight_status = itmMUser.getProperty("in_weight_status", "");

                List<string> keys = new List<string>
                {
                    in_l1,
                    in_l2,
                    in_l3,
                    in_index,
                    in_creator_sno,
                };

                var team_key = string.Join("-", keys);

                var is_include = IsInClude(status_map, in_weight_status);

                if (in_weight_status == "king")
                {
                    //盟主
                    sql = "UPDATE IN_MEETING_PTEAM SET source_id = '" + cfg.program_id + "', in_not_draw = 1 WHERE in_meeting = '" + cfg.meeting_id + "' AND in_team_key = N'" + team_key + "'";
                }
                else if (is_include)
                {
                    //恢復名單
                    sql = "UPDATE IN_MEETING_PTEAM SET source_id = '" + cfg.program_id + "', in_not_draw = 0 WHERE in_meeting = '" + cfg.meeting_id + "' AND in_team_key = N'" + team_key + "'";
                }
                else
                {
                    //移出名單
                    sql = "UPDATE IN_MEETING_PTEAM SET source_id = NULL, in_not_draw = 1 WHERE in_meeting = '" + cfg.meeting_id + "' AND in_team_key = N'" + team_key + "'";
                }

                var itmSQL = cfg.inn.applySQL(sql);
                if (itmSQL.isError())
                {
                    cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "執行失敗 sql: " + sql);
                }
            }
        }

        #region 過磅狀態

        //是否納入名單
        private bool IsInClude(Dictionary<string, TWStatus> map, string in_weight_status)
        {
            if (map.ContainsKey(in_weight_status))
            {
                return map[in_weight_status].in_is_remove == "0";
            }
            else
            {
                return true;
            }
        }

        private Dictionary<string, TWStatus> MergeWStatus(TConfig cfg)
        {
            //過磅狀態資料
            var items = GetWStatus(cfg);

            var map = MapWStatus(cfg, items);

            if (map.Count == 0)
            {
                map = DefaultWStatusMap(cfg);
            }

            map.Add("on", new TWStatus { value = "on", label = "過磅合格", in_is_remove = "0" });
            map.Add("", new TWStatus { value = "", label = "", in_is_remove = "0" });

            return map;
        }

        private Dictionary<string, TWStatus> DefaultWStatusMap(TConfig cfg)
        {
            var result = new Dictionary<string, TWStatus>();
            result.Add("off", new TWStatus { value = "off", label = "未到", in_is_remove = "0" });
            result.Add("leave", new TWStatus { value = "leave", label = "請假", in_is_remove = "1" });
            result.Add("dq", new TWStatus { value = "dq", label = "DQ", in_is_remove = "0" });
            return result;
        }

        private Dictionary<string, TWStatus> MapWStatus(TConfig cfg, Item items)
        {
            var result = new Dictionary<string, TWStatus>();

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);

                TWStatus obj = new TWStatus
                {
                    id = item.getProperty("id", ""),
                    value = item.getProperty("value", ""),
                    label = item.getProperty("label", ""),
                    in_is_remove = item.getProperty("in_is_remove", "0"),
                };

                result.Add(obj.value, obj);
            }

            return result;
        }

        private Item GetWStatus(TConfig cfg)
        {
            string sql = @"
                SELECT 
	                t1.id
                    , t1.in_status  AS 'value'
                    , t1.in_is_remove
	                , t2.label_zt   AS 'label'
                FROM 
	                IN_MEETING_PWEIGHT t1 WITH(NOLOCK)
                INNER JOIN
                (
	                SELECT 
		                t12.value, t12.label_zt, t12.sort_order 
	                FROM 
		                LIST t11 WITH(NOLOCK) 
	                INNER JOIN 
		                [VALUE] t12 WITH(NOLOCK) 
		                ON t12.source_id = t11.id 
	                WHERE 
		                t11.name = 'In_Weight_Status'
						AND t12.value <> 'on'
                ) t2 ON t2.value = t1.in_status
                WHERE
	                t1.in_meeting = '{#meeting_id}'
                ORDER BY
                    t2.sort_order
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            return cfg.inn.applySQL(sql);
        }

        #endregion 過磅狀態

        private Item GetMeeting(TConfig cfg)
        {
            string sql = "SELECT in_title, in_battle_type, in_robin_player FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'";
            return cfg.inn.applySQL(sql);
        }

        private Item GetProgram(TConfig cfg)
        {
            string sql = @"
                SELECT 
	                * 
                FROM 
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                WHERE
	                in_meeting = '{#meeting_id}'
	                AND in_l1 = N'{#in_l1}'
	                AND in_l2 = N'{#in_l2}'
	                AND in_l3 = N'{#in_l3}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_l1}", cfg.in_l1)
                .Replace("{#in_l2}", cfg.in_l2)
                .Replace("{#in_l3}", cfg.in_l3);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        private Item GetMUsers(TConfig cfg)
        {
            string sql = @"
                SELECT 
	                * 
                FROM 
	                IN_MEETING_USER t1 WITH(NOLOCK)
                WHERE
	                source_id = '{#meeting_id}'
	                AND in_l1 = N'{#in_l1}'
	                AND in_l2 = N'{#in_l2}'
	                AND in_l3 = N'{#in_l3}'
                ORDER BY
                    in_city_no
                    , in_current_org
                    , in_index
                    , in_team
                    , in_sno
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_l1}", cfg.in_l1)
                .Replace("{#in_l2}", cfg.in_l2)
                .Replace("{#in_l3}", cfg.in_l3);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        private int GetMPTeamCount(TConfig cfg)
        {
            string sql = @"
	            SELECT 
		            COUNT(*) AS 'cnt'
	            FROM 
		            IN_MEETING_PTEAM WITH(NOLOCK)
	            WHERE 
		            source_id = '{#program_id}'
                    AND ISNULL(in_not_draw, 0) = 0
	            GROUP BY
		            source_id
            ";

            sql = sql.Replace("{#program_id}", cfg.program_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item itmResult = cfg.inn.applySQL(sql);

            if (itmResult.isError() || itmResult.getResult() == "")
            {
                return 0;
            }

            return GetInt(itmResult.getProperty("cnt", "0"));
        }

        //取得賽程組別資訊(抽籤)
        private Item GetMTeams(TConfig cfg)
        {
            string sql = @"
                SELECT
                   t2.id           AS 'program_id'
                   , t2.in_name    AS 'program_name'
                   , t2.in_name2    AS 'program_name2'
                   , t2.in_name3    AS 'program_name3'
                   , t2.in_display AS 'program_display'
                   , t1.*
                FROM
                    VU_MEETING_PTEAM t1 WITH(NOLOCK)
                INNER JOIN
                    IN_MEETING_PROGRAM t2 WITH(NOLOCK)
                    ON t2.id = t1.source_id
                WHERE
                    t2.in_meeting = '{#meeting_id}'
                    AND t1.source_id = '{#program_id}'
		        ORDER BY
			        t1.in_team_index
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#program_id}", cfg.program_id);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public Innovator inn { get; set; }

            /// <summary>
            /// Aras Vault Site 絕對路徑
            /// </summary>
            public string aras_vault_url { get; set; }

            //外部輸入區

            public string scene { get; set; }
            public string meeting_id { get; set; }
            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }
            public string muid { get; set; }
            public string no_straw { get; set; }

            /// <summary>
            /// 活動
            /// </summary>
            public Item itmMeeting { get; set; }

            /// <summary>
            /// 組別
            /// </summary>
            public Item itmProgram { get; set; }

            //Meeting

            /// <summary>
            /// 賽事預設賽制
            /// </summary>
            public string mt_battle_type { get; set; }

            /// <summary>
            /// 循環賽人數(小於等於)
            /// </summary>
            public int mt_robin_player { get; set; }


            /// <summary>
            /// 組別 id
            /// </summary>
            public string program_id { get; set; }

            /// <summary>
            /// 原賽制
            /// </summary>
            public string old_battle_type { get; set; }

            /// <summary>
            /// 原隊伍人數
            /// </summary>
            public int old_team_count { get; set; }

            /// <summary>
            /// 新隊伍人數
            /// </summary>
            public int new_team_count { get; set; }

            /// <summary>
            /// 新賽制
            /// </summary>
            public string new_battle_type { get; set; }
        }

        private class TWStatus
        {
            public string id { get; set; }
            public string value { get; set; }
            public string label { get; set; }
            public string in_is_remove { get; set; }
        }

        /// <summary>
        /// 取得最大輪次代碼
        /// </summary>
        private static int[] GetRoundAndSum(int value, int code, int sum, int round)
        {
            if (value == 0)
            {
                return new int[] { 0, 0 };
            }
            else if (value > sum)
            {
                return GetRoundAndSum(value, code, code * sum, round + 1);
            }
            else
            {
                return new int[] { round, sum };
            }
        }

        /// <summary>
        /// 轉換為整數
        /// </summary>
        private int GetInt(string value, int def = 0)
        {
            int result = def;
            int.TryParse(value, out result);
            return result;
        }
    }
}