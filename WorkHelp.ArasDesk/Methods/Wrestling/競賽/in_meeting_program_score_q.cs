﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Wrestling.Common
{
    public class in_meeting_program_score_q : Item
    {
        public in_meeting_program_score_q(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 快速登錄成績
                日期: 
                    2020-09-30: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]in_meeting_program_score_q";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, this.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,

                meeting_id = itmR.getProperty("meeting_id", ""),
                program_id = itmR.getProperty("program_id", ""),
                event_id = itmR.getProperty("event_id", ""),
                mode = itmR.getProperty("mode", ""),
            };

            if (cfg.meeting_id == "")
            {
                itmR.setProperty("error_message", "賽事 id 不得為空白");
                return itmR;
            }

            Item itmDrawMode = cfg.inn.applySQL("SELECT in_value FROM IN_VARIABLE WITH(NOLOCK) WHERE in_name = 'draw_mode'");
            cfg.draw_mode = itmDrawMode.getProperty("in_value", "").ToLower();
            cfg.is_tkd_mode = cfg.draw_mode == "tkd";

            switch (cfg.mode)
            {
                case "":
                    Page(cfg, itmR);
                    break;

                case "modal":
                    if (cfg.is_tkd_mode)
                    {
                        ModalTkd(cfg, cfg.program_id, cfg.event_id, itmR);
                    }
                    else
                    {
                        ModalJudo(cfg, cfg.program_id, cfg.event_id, itmR);
                    }
                    break;

                case "modal2":
                    Modal2(cfg, cfg.program_id, cfg.event_id, itmR);
                    break;

                case "edit":
                    Edit(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void Edit(TConfig cfg, Item itmReturn)
        {
            var eid = itmReturn.getProperty("event_id", "");
            var in_win_local_time = itmReturn.getProperty("in_win_local_time", "");

            var sql = "UPDATE IN_MEETING_PEVENT SET in_win_local_time = '" + in_win_local_time + "' WHERE id = '" + eid + "'";
            cfg.inn.applySQL(sql);

            RunDetailEdit(cfg, "f1_", itmReturn);
            RunDetailEdit(cfg, "f2_", itmReturn);
        }

        private void RunDetailEdit(TConfig cfg, string prfx, Item itmReturn)
        {
            var id = itmReturn.getProperty(prfx + "id");

            var itmOld = cfg.inn.newItem("In_Meeting_PEvent_Detail", "merge");
            itmOld.setAttribute("where", "id = '" + id + "'");
            itmOld.setProperty("id", id);
            itmOld.setProperty("in_score", itmReturn.getProperty(prfx + "in_score"));
            itmOld.setProperty("in_score1", itmReturn.getProperty(prfx + "in_score1"));
            itmOld.setProperty("in_score2", itmReturn.getProperty(prfx + "in_score2"));
            itmOld.setProperty("in_score4", itmReturn.getProperty(prfx + "in_score4"));
            itmOld.setProperty("in_score5", itmReturn.getProperty(prfx + "in_score5"));
            itmOld.setProperty("in_score_c1", itmReturn.getProperty(prfx + "in_score_c1"));
            itmOld.setProperty("in_score_c2", itmReturn.getProperty(prfx + "in_score_c2"));
            itmOld.setProperty("in_cpts", itmReturn.getProperty(prfx + "in_cpts"));
            itmOld.setProperty("in_points", itmReturn.getProperty(prfx + "in_cpts"));
            itmOld = itmOld.apply();
        }

        private void Page(TConfig cfg, Item itmReturn)
        {
            if (cfg.program_id == "" && cfg.event_id == "")
            {
                AppendMeeting(cfg, itmReturn);
                AppendLevelOptions(cfg, itmReturn);
                itmReturn.setProperty("hide_preview", "item_show_0");
                return;
            }

            if (cfg.program_id != "")
            {
                if (cfg.event_id != "")
                {
                    AppendEventItem(cfg, cfg.event_id, itmReturn);
                    AppendLevelOptions(cfg, itmReturn);
                    AppendEventOptions(cfg, itmReturn);
                }
                else
                {
                    var itmFirstEvent = GetFirstEvent(cfg, itmReturn);
                    if (itmFirstEvent.isError() || itmFirstEvent.getResult() == "")
                    {
                        AppendProgram(cfg, itmReturn);
                    }
                    else
                    {
                        var first_evt_id = itmFirstEvent.getProperty("id", "");
                        AppendEventItem(cfg, first_evt_id, itmReturn);
                    }
                    AppendLevelOptions(cfg, itmReturn);
                    AppendEventOptions(cfg, itmReturn);
                }
            }
        }

        private void AppendMeeting(TConfig cfg, Item itmReturn)
        {
            string sql = "SELECT in_title FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'";

            Item itmMeeting = cfg.inn.applySQL(sql);

            itmReturn.setProperty("in_title", itmMeeting.getProperty("in_title", ""));
        }

        private void AppendProgram(TConfig cfg, Item itmReturn)
        {
            if (cfg.program_id == "")
            {
                itmReturn.setProperty("error_message", "組別 id 不得為空白");
                return;
            }

            string sql = @"
                SELECT
                    TOP 1
                    t3.in_title
                    , t2.in_l1
                    , t2.in_l2
                    , t2.in_l3
                    , t2.in_name        AS 'program_name'
                    , t2.in_name2       AS 'program_name2'
                    , t2.in_name3       AS 'program_name3'
                    , t2.in_name3       AS 'program_display'
                    , t2.in_short_name  AS 'program_short'
                    , t2.in_weight      AS 'program_weight'
                FROM
                    IN_MEETING_PROGRAM t2 WITH(NOLOCK)
                INNER JOIN
                    IN_MEETING t3 WITH(NOLOCK)
                    ON t3.id = t2.in_meeting
                WHERE
                    t2.id = '{#program_id}'
            ";

            sql = sql.Replace("{#program_id}", cfg.program_id);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item itmProgram = cfg.inn.applySQL(sql);

            itmReturn.setProperty("in_title", itmProgram.getProperty("in_title", ""));
            itmReturn.setProperty("in_l1", itmProgram.getProperty("in_l1", ""));
            itmReturn.setProperty("in_l2", itmProgram.getProperty("in_l2", ""));
            itmReturn.setProperty("in_l3", itmProgram.getProperty("in_l3", ""));
            itmReturn.setProperty("program_display", itmProgram.getProperty("program_display", ""));
            itmReturn.setProperty("program_name", itmProgram.getProperty("program_name", ""));
            itmReturn.setProperty("program_name2", itmProgram.getProperty("program_name2", ""));
            itmReturn.setProperty("program_name3", itmProgram.getProperty("program_name3", ""));
            itmReturn.setProperty("program_short", itmProgram.getProperty("program_short", ""));
            itmReturn.setProperty("program_weight", itmProgram.getProperty("program_weight", ""));
        }

        private Item GetFirstEvent(TConfig cfg, Item itmReturn)
        {
            string sql = @"
                SELECT
                    TOP 1 *
                FROM
                    IN_MEETING_PEVENT t1 WITH(NOLOCK)
                WHERE
                    source_id = '{#program_id}'
                    AND in_tree_name = 'main'
                    AND ISNULL(in_tree_no, 0) > 0
                ORDER BY
                    in_tree_no
            ";

            sql = sql.Replace("{#program_id}", cfg.program_id);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        private void AppendEventItem(TConfig cfg, string event_id, Item itmReturn)
        {
            var err = new List<string>();
            if (event_id == "") err.Add("場次 id 不得為空白");

            if (err.Count > 0)
            {
                itmReturn.setProperty("error_message", string.Join(" <br> ", err));
                return;
            }

            var sql = @"
                SELECT
                    TOP 1
                    t3.in_title
                    , t2.in_l1
                    , t2.in_l2
                    , t2.in_l3
                    , t2.in_name        AS 'program_name'
                    , t2.in_name2       AS 'program_name2'
                    , t2.in_name3       AS 'program_name3'
                    , t2.in_name3       AS 'program_display'
                    , t2.in_short_name  AS 'program_short'
                    , t2.in_weight      AS 'program_weight'
                    , t1.*
                FROM
                    IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
                    IN_MEETING_PROGRAM t2 WITH(NOLOCK)
                    ON t2.id = t1.source_id
                INNER JOIN
                    IN_MEETING t3 WITH(NOLOCK)
                    ON t3.id = t2.in_meeting
                WHERE
                    t1.id = '{#event_id}'
            ";

            sql = sql.Replace("{#event_id}", event_id);
            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item itmPEvent = cfg.inn.applySQL(sql);

            itmReturn.setProperty("in_title", itmPEvent.getProperty("in_title", ""));
            itmReturn.setProperty("in_l1", itmPEvent.getProperty("in_l1", ""));
            itmReturn.setProperty("in_l2", itmPEvent.getProperty("in_l2", ""));
            itmReturn.setProperty("in_l3", itmPEvent.getProperty("in_l3", ""));
            itmReturn.setProperty("program_name", itmPEvent.getProperty("program_name", ""));
            itmReturn.setProperty("program_name2", itmPEvent.getProperty("program_name2", ""));
            itmReturn.setProperty("program_name3", itmPEvent.getProperty("program_name3", ""));
            itmReturn.setProperty("program_display", itmPEvent.getProperty("program_display", ""));
            itmReturn.setProperty("in_tree_name", itmPEvent.getProperty("in_tree_name", ""));
            itmReturn.setProperty("in_round", itmPEvent.getProperty("in_round", ""));
            itmReturn.setProperty("in_tree_id", itmPEvent.getProperty("in_tree_id", ""));
        }

        private void Modal2(TConfig cfg, string program_id, string event_id, Item itmReturn)
        {
            string in_date_key = itmReturn.getProperty("inn_date", "");
            string in_code = itmReturn.getProperty("inn_site", "");
            string in_tree_no = itmReturn.getProperty("inn_no", "");

            string sql = "SELECT TOP 1 t1.source_id, t1.id FROM IN_MEETING_PEVENT t1 WITH(NOLOCK)"
                + " INNER JOIN IN_MEETING_SITE t2 WITH(NOLOCK)"
                + " ON t2.id = t1.in_site"
                + " WHERE t1.in_meeting = '" + cfg.meeting_id + "'"
                + " AND t1.in_date_key = '" + in_date_key + "'"
                + " AND t2.in_code = '" + in_code + "'"
                + " AND t1.in_tree_no = '" + in_tree_no + "'";

            Item item = cfg.inn.applySQL(sql);

            if (item.isError() || item.getResult() == "")
            {
                throw new Exception("查無該場次");
            }

            cfg.program_id = item.getProperty("source_id", "");
            cfg.event_id = item.getProperty("id", "");
            itmReturn.setProperty("program_id", cfg.program_id);
            itmReturn.setProperty("event_id", cfg.event_id);

            if (cfg.is_tkd_mode)
            {
                ModalTkd(cfg, cfg.program_id, cfg.event_id, itmReturn);
            }
            else
            {
                ModalJudo(cfg, cfg.program_id, cfg.event_id, itmReturn);
            }
        }

        private void ModalJudo(TConfig cfg, string program_id, string event_id, Item itmReturn)
        {
            List<string> err = new List<string>();
            if (event_id == "") err.Add("場次 id 不得為空白");

            if (err.Count > 0)
            {
                itmReturn.setProperty("error_message", string.Join(" <br> ", err));
                return;
            }

            //主成績框
            MainModal_Judo(cfg, program_id, event_id, itmReturn);

            //子場次選單
            SubEventMenu_Judo(cfg, program_id, event_id, itmReturn);
        }

        private void ModalTkd(TConfig cfg, string program_id, string event_id, Item itmReturn)
        {
            var err = new List<string>();
            if (event_id == "") err.Add("場次 id 不得為空白");

            if (err.Count > 0)
            {
                itmReturn.setProperty("error_message", string.Join(" <br> ", err));
                return;
            }

            var sql = @"
                SELECT
                    t1.id AS 'event_id'
                    , t1.in_tree_name
                    , t1.in_tree_id
                    , t1.in_tree_no
                    , t1.in_tree_alias
                    , t1.in_round
                    , t1.in_round_code
                    , t1.in_site_code
                    , t1.in_site_no
                    , t1.in_site_id
                    , t1.in_show_site
                    , t1.in_show_serial
                    , t1.in_fight_id
                    , t1.in_win_status
                    , t1.in_win_local_time
                    , t1.in_win_local_note
                    , t2.id AS 'detail_id'
                    , t3.in_sign_no
                    , t3.in_section_no
                    , t2.in_target_no
                    , t2.in_status
                    , t2.in_score_c1
                    , t2.in_score_c2
                    , t2.in_score1
                    , t2.in_score2
                    , t2.in_score3
                    , t2.in_score4
                    , t2.in_score5
                    , t2.in_score
                    , t2.in_cpts
                    , t2.in_points
                    , t2.in_points_type
                    , t2.in_points_text
                    , t3.id AS 'team_id'
                    , t3.in_current_org
                    , t3.in_short_org
                    , t3.in_team
                    , t3.in_name
                    , t3.in_sno
                    , t3.in_seeds
                    , t3.in_lottery_99
                    , t3.in_check_result
                    , t3.in_check_status
                    , t3.in_team_players
                    , t3.in_team_index
                    , t5.in_name        AS 'program_name'
                    , t5.in_name2       AS 'program_name2'
                    , t5.in_name3       AS 'program_name3'
                    , t5.in_name3       AS 'program_display'
                    , t5.in_battle_type AS 'program_battle_type'
                    , t5.in_team_count
                    , t11.in_photo      AS 'resume_photo'
                    , t12.in_photo      AS 'org_photo'
                FROM
                    IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
                    IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK)
                    ON t2.source_id = t1.id
                LEFT OUTER JOIN
                    IN_MEETING_PTEAM t3 WITH(NOLOCK)
                    ON t3.source_id = t1.source_id
                    AND ISNULL(t3.in_sign_no, '') NOT IN ('', '0')
                    AND t3.in_sign_no = t2.in_sign_no
                INNER JOIN
                    IN_MEETING_PROGRAM t5 WITH(NOLOCK)
                    ON t5.id = t1.source_id
                LEFT OUTER JOIN
                    IN_RESUME t11 WITH(NOLOCK)
                    ON t11.in_sno = t3.in_sno
                LEFT OUTER JOIN
                    IN_ORG t12 WITH(NOLOCK)
                    ON t12.in_group = t3.in_group
                    AND t12.in_current_org = t3.in_current_org
                WHERE
                    t1.id = '{#event_id}'
                ORDER BY
                    t2.in_sign_foot
            ";

            sql = sql.Replace("{#event_id}", event_id);
            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();

            if (count != 2)
            {
                return;
            }

            var itmFirst = items.getItemByIndex(0);

            string hide_points = ""; //積分
            string hide_scores = ""; //成績

            string event_status = GetEventStatus(itmFirst.getProperty("in_win_status", ""));

            itmReturn.setProperty("program_id", cfg.program_id);
            itmReturn.setProperty("hide_scores", hide_scores);
            itmReturn.setProperty("hide_points", hide_points);

            itmReturn.setProperty("event_id", itmFirst.getProperty("event_id", ""));
            itmReturn.setProperty("event_status", event_status);

            CopyProperty(itmReturn, itmFirst, "program_name");
            CopyProperty(itmReturn, itmFirst, "program_name2");
            CopyProperty(itmReturn, itmFirst, "program_name3");
            CopyProperty(itmReturn, itmFirst, "program_display");

            CopyProperty(itmReturn, itmFirst, "in_tree_name");
            CopyProperty(itmReturn, itmFirst, "in_tree_no");
            CopyProperty(itmReturn, itmFirst, "in_tree_id");
            CopyProperty(itmReturn, itmFirst, "in_tree_alias");

            var in_site_code = itmFirst.getProperty("in_site_code", "");
            var en_site = GetEnSite(in_site_code);
            var in_tree_no = itmFirst.getProperty("in_tree_no", "");

            CopyProperty(itmReturn, itmFirst, "in_site_id");
            itmReturn.setProperty("in_site_code", in_site_code);
            itmReturn.setProperty("en_site", en_site);

            //調場
            var in_show_site = itmFirst.getProperty("in_show_site", "");
            var en_show_site = GetEnSite(in_show_site);
            itmReturn.setProperty("site_change_info", en_site != en_show_site ? "(調至 " + en_show_site + " 場地)" : " ");


            itmReturn.setProperty("inn_no", itmFirst.getProperty("in_site_id", ""));
            CopyProperty(itmReturn, itmFirst, "in_round");
            CopyProperty(itmReturn, itmFirst, "in_fight_id");
            CopyProperty(itmReturn, itmFirst, "in_win_local_time");
            CopyProperty(itmReturn, itmFirst, "in_win_local_note");

            string in_site_allocate = itmFirst.getProperty("in_site_allocate", "");
            string in_site_id = itmFirst.getProperty("in_site_id", "");

            if (in_site_allocate == "1")
            {
                itmReturn.setProperty("inn_event_no", in_site_id);
            }
            else
            {
                itmReturn.setProperty("inn_event_no", in_tree_no);
            }

            var itmTeams = GetTeamItems(cfg, program_id);
            SetTkdModalItem(itmReturn, items.getItemByIndex(0), "left", "紅", itmTeams);
            SetTkdModalItem(itmReturn, items.getItemByIndex(1), "right", "藍", itmTeams);
        }

        private string GetEnSite(string v)
        {
            switch (v)
            {
                case "1": return "A";
                case "2": return "B";
                case "3": return "C";
                case "4": return "D";
                case "5": return "E";
                case "6": return "F";
                default: return v;
            }
        }

        private Item GetTeamItems(TConfig cfg, string program_id)
        {
            var sql = @"
                SELECT 
	                in_sign_no
	                , in_current_org
	                , in_short_org
	                , in_name
	                , in_team
	                , in_index
	                , in_lottery_99
                FROM 
	                IN_MEETING_PTEAM WITH(NOLOCK)
                WHERE
	                source_id = '{#program_id}'
	                AND ISNULL(in_sign_no, '') <> ''
                ORDER BY
	                CAST(in_sign_no AS int)
            ";

            sql = sql.Replace("{#program_id}", program_id);

            return cfg.inn.applySQL(sql);
        }

        private void SetTkdModalItem(Item itmReturn, Item source, string pfx, string color, Item itmTeams)
        {
            string detail_id = source.getProperty("detail_id", "");
            string in_sign_bypass = source.getProperty("in_sign_bypass", "");

            string in_team_players = source.getProperty("in_team_players", "");
            string resume_photo = source.getProperty("resume_photo", "");
            string org_photo = source.getProperty("org_photo", "");
            string in_team = source.getProperty("in_team", "　");
            string in_name = source.getProperty("in_name", "　");
            string in_sno = source.getProperty("in_sno", "　");
            string in_short_org = source.getProperty("in_short_org", "　");

            string in_sign_no = source.getProperty("in_sign_no", "　");
            string in_section_no = source.getProperty("in_section_no", "　");
            string in_lottery_99 = source.getProperty("in_lottery_99", "　");
            string in_check_result = source.getProperty("in_check_result", "");
            string in_status = source.getProperty("in_status", "");

            bool is_signle = true;
            string show_photo = is_signle ? resume_photo : org_photo;
            string in_show_name = in_name;

            if (in_name == "") in_name = "&nbsp;";

            itmReturn.setProperty(pfx + "_detail_id", detail_id);
            itmReturn.setProperty(pfx + "_photo", show_photo);
            itmReturn.setProperty(pfx + "_name", in_name + " (" + in_lottery_99 + ")");
            itmReturn.setProperty(pfx + "_sno", in_sno);
            itmReturn.setProperty(pfx + "_short_org", in_short_org);
            itmReturn.setProperty(pfx + "_show_name", in_show_name);

            itmReturn.setProperty(pfx + "_sign_no", in_section_no);

            itmReturn.setProperty(pfx + "_team_index", source.getProperty("in_team_index", ""));

            itmReturn.setProperty(pfx + "_score_c1", source.getProperty("in_score_c1", "0"));
            itmReturn.setProperty(pfx + "_score_c2", source.getProperty("in_score_c2", "0"));
            itmReturn.setProperty(pfx + "_score1", source.getProperty("in_score1", "0"));
            itmReturn.setProperty(pfx + "_score2", source.getProperty("in_score2", "0"));
            itmReturn.setProperty(pfx + "_score3", source.getProperty("in_score3", "0"));
            itmReturn.setProperty(pfx + "_score4", source.getProperty("in_score4", "0"));
            itmReturn.setProperty(pfx + "_score5", source.getProperty("in_score5", "0"));
            itmReturn.setProperty(pfx + "_score", source.getProperty("in_score", "0"));
            itmReturn.setProperty(pfx + "_cpts", source.getProperty("in_cpts", "0"));
            itmReturn.setProperty(pfx + "_points", source.getProperty("in_points", "0"));


            string in_win_status = source.getProperty("in_win_status", "");
            string in_check = source.getProperty("in_check", "");
            string in_points_text = source.getProperty("in_points_text", "");
            string in_team_count = source.getProperty("in_team_count", "");

            itmReturn.setProperty(pfx + "_check_btn", CheckinButton(detail_id, in_check));
            itmReturn.setProperty(pfx + "_player_btn", PlayerButton(detail_id, in_team_count, itmTeams));
            itmReturn.setProperty(pfx + "_rollback_btn", RollbackButton(detail_id));
            itmReturn.setProperty(pfx + "_cancel_btn", CancelkButton(detail_id, in_win_status));
            itmReturn.setProperty(pfx + "_status_btn", WinButton(detail_id, color, in_status));
            itmReturn.setProperty(pfx + "_dsq_btn", DQButton(detail_id, in_points_text));
        }

        private string PlayerButton(string detail_id, string in_team_count, Item itmTeams)
        {
            var id1 = ArasId();
            var id2 = ArasId();

            var css = "btn-outline-primary";

            var team_count = GetInt(in_team_count);
            var menu = "<select id='" + id1 + "' data-bid='" + id2 + "' data-did='" + detail_id + "' class='inno-draw-no' onchange='detailPlayerChange(this)' style='display:none'>";
            menu += "<option value=''>籤號</option>";
            menu += "<option value='close'>關閉</option>";
            menu += "<option value='clear'>清空</option>";

            var count = itmTeams.getItemCount();
            if (count > 0)
            {
                for (var i = 0; i < count; i++)
                {
                    var itmTeam = itmTeams.getItemByIndex(i);
                    var in_index = itmTeam.getProperty("in_index", "");
                    var in_short_org = itmTeam.getProperty("in_short_org", "");
                    var in_name = itmTeam.getProperty("in_name", "");
                    var in_sign_no = itmTeam.getProperty("in_sign_no", "");
                    var in_lottery_99 = itmTeam.getProperty("in_lottery_99", "");

                    var player = "(" + in_sign_no + "). " + in_index + " " + in_short_org + " " + in_name + " (" + in_lottery_99 + ")";

                    menu += "<option value='" + in_sign_no + "'>" + player + "</option>";
                }
            }
            else
            {
                for (var i = 1; i <= team_count; i++)
                {
                    menu += "<option value='" + i + "'>" + i + "</option>";
                }
            }
            menu += "</select>";

            var button = "<button id='" + id2 + "' data-sid='" + id1 + "'  type='button' class='btn btn-sm " + css + "' title = '更換選手'"
                + " onclick='showPlayerMenu(this)' >"
                + " <i class='fa fa-user'></i>"
                + "</button>";

            return button + menu;
        }

        /// <summary>
        /// 自動編號
        /// </summary>
        private string ArasId()
        {
            return Guid.NewGuid().ToString("N").ToUpper();
        }

        private string CancelkButton(string detail_id, string in_win_status)
        {
            string css = "btn-outline-primary";
            if (in_win_status == "cancel") css = "btn-danger";

            return "<button type='button' class='btn btn-sm " + css + "' title = '取消場次'"
                + " data-did='" + detail_id + "'"
                + " onclick='CancelEvent(this)' >"
                + " <i class='fa fa-remove'></i>"
                + "</button>";
        }

        private string RollbackButton(string detail_id)
        {
            string css = "btn-outline-primary";

            return "<button type='button' class='btn btn-sm " + css + "' title = '復原資料'"
                + " data-did='" + detail_id + "'"
                + " onclick='RollBackEvent(this)' >"
                + " <i class='fa fa-eraser'></i>"
                + "</button>";
        }

        private string CheckinButton(string detail_id, string in_check)
        {
            string text = "未檢錄";
            string css = "btn-outline-primary";//"btn-default";
            if (in_check == "V")
            {
                text = "已檢錄";
                css = "btn-success";
            }

            return "<button type='button' class='btn btn-sm " + css + "' title = '檢錄狀態' data-type='Rollcall'>"
                + "<i class='fa fa-lightbulb-o'></i>"
                + "</button>";
        }

        private string WinButton(string detail_id, string color, string in_status)
        {
            string text = color + "　勝";
            string css = "btn-outline-primary";//"btn-default";
            if (in_status == "1")
            {
                css = "btn-success";
            }

            return "<button type='button' class='btn btn-sm " + css + "' title = '執行勝出'"
                + " data-type='HWIN' data-text='勝出'"
                + " data-did='" + detail_id + "'"
                + " onclick='EvtAct(this)' >" + text + "</button>";
        }

        private string DQButton(string detail_id, string in_points_text)
        {
            string text = "執行棄權";
            string css = "btn-outline-primary";//"btn-default";
            if (in_points_text.Contains("棄權"))
            {
                text = "已棄權";
                css = "btn-danger";
            }

            return "<button type='button' class='btn btn-sm " + css + "' title = '" + text + "'"
                + " data-type='DQ' data-text='棄權'"
                + " data-did='" + detail_id + "'"
                + " onclick='EvtAct(this)' >"
                + "<i class='fa fa-flag-o'></i>"
                + "</button>";
        }

        //子場次選單
        private void SubEventMenu_Judo(TConfig cfg, string program_id, string tree_id, Item itmReturn)
        {
            string p_tree_id = tree_id.Substring(0, 4);

            var sql = @"
                SELECT
                    in_tree_id
                    , in_sub_id
                    , in_tree_alias
                FROM
                    IN_MEETING_PEVENT WITH(NOLOCK)
                WHERE
                    source_id = '{#program_id}'
                    AND in_tree_id LIKE '{#p_tree_id}%'
                    AND in_type = 's'
                ORDER BY
                    in_tree_no
            ";

            sql = sql.Replace("{#program_id}", program_id)
                .Replace("{#p_tree_id}", p_tree_id);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item items = cfg.inn.applySQL(sql);

            if (items.isError() || items.getResult() == "")
            {
                itmReturn.setProperty("sub_json", "[]");
                return;
            }

            List<TNode> nodes = new List<TNode>();

            var main = new TNode
            {
                Lbl = "主場",
                Val = p_tree_id,
            };
            nodes.Add(main);

            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string value = item.getProperty("in_tree_id", "");
                //string name = item.getProperty("in_tree_alias", "");
                string name = "第" + (i + 1) + "場";

                var node = new TNode
                {
                    Lbl = name,
                    Val = value,
                };

                nodes.Add(node);
            }

            itmReturn.setProperty("sub_json", Newtonsoft.Json.JsonConvert.SerializeObject(nodes));
        }

        //主成績框
        private void MainModal_Judo(TConfig cfg, string program_id, string event_id, Item itmReturn)
        {
            var sql = @"
                SELECT
                    t1.id               AS 'event_id'
                    , t1.in_type        AS 'event_type'
                    , t1.in_tree_name
                    , t1.in_tree_id
                    , t1.in_tree_no
                    , t1.in_tree_alias
                    , t1.in_round
                    , t1.in_round_code
                    , t1.in_site_code
                    , t1.in_site_no
                    , t1.in_site_id
                    , t1.in_site_allocate
                    , t1.in_site
                    , t1.in_date_key
                    , t1.in_win_status
                    , t1.in_win_local_time
                    , t1.in_sub_sect
                    , t1.in_fight_id
                    , t2.id AS 'detail_id'
                    , t2.in_sign_no
                    , t2.in_target_no
                    , t2.in_status
                    , t2.in_points
                    , t2.in_points_type
                    , t2.in_points_text
                    , t2.in_correct_count
                    , ISNULL(t3.id, '')                          AS 'team_id'
                    , ISNULL(t3.map_short_org, t2.in_player_org) AS 'player_org'
                    , ISNULL(t3.in_team, t2.in_player_team)      AS 'player_team'
                    , ISNULL(t3.in_name, t2.in_player_name)      AS 'player_name'
                    , ISNULL(t3.in_sno, t2.in_player_sno)        AS 'player_sno'
                    , ISNULL(t3.in_team_players, '1')            AS 'player_count'
                    , t3.in_check_result
                    , t3.in_check_status
                    , t3.in_section_no
                    , t5.in_name3       AS 'program_display'
                    , t5.in_name2       AS 'program_name2'
                    , t5.in_name3       AS 'program_name3'
                    , t5.in_battle_type AS 'program_battle_type'
                    , t5.in_site_mat    AS 'program_site_mat'
                    , t11.in_photo  AS 'player_photo'
                    , t12.in_photo  AS 'org_photo'
                    , t13.in_name   AS 'site_name'
                    , t14.in_uniform_color
                FROM
                    IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
                    IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK)
                    ON t2.source_id = t1.id
                LEFT OUTER JOIN
                    VU_MEETING_PTEAM t3 WITH(NOLOCK)
                    ON t3.source_id = t1.source_id
                    AND t3.in_sign_no = t2.in_sign_no
                INNER JOIN
                    IN_MEETING_PROGRAM t5 WITH(NOLOCK)
                    ON t5.id = t1.source_id
                LEFT OUTER JOIN
                    IN_RESUME t11 WITH(NOLOCK)
                    ON t11.in_sno = t3.in_sno
                LEFT OUTER JOIN
                    IN_ORG t12 WITH(NOLOCK)
                    ON t12.in_group = t3.in_group
                    AND t12.in_current_org = t3.in_current_org
                LEFT OUTER JOIN
                    IN_MEETING_SITE t13 WITH(NOLOCK)
                    ON t13.id = t1.in_site
                INNER JOIN
                    IN_MEETING t14 WITH(NOLOCK)
                    ON t14.id = t1.in_meeting
                WHERE
                    t1.id = '{#event_id}'
                ORDER BY
                    t2.in_sign_foot
            ";

            sql = sql.Replace("{#event_id}", event_id);

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            var items = cfg.inn.applySQL(sql);
            int count = items.getItemCount();

            if (count != 2)
            {
                return;
            }

            var itmFirst = items.getItemByIndex(0);
            var hide_points = ""; //積分
            var hide_scores = ""; //成績
            var program_battle_type = itmFirst.getProperty("program_battle_type", "");
            switch (program_battle_type)
            {
                case "SingleRoundRobin":
                case "DoubleRoundRobin":
                case "GroupSRoundRobin":
                case "GroupDRoundRobin":
                    hide_scores = "item_show_0";
                    break;

                default:
                    hide_points = "item_show_0";
                    break;
            }

            var event_status = GetEventStatus(itmFirst.getProperty("in_win_status", ""));

            //道服顏色: START
            var itmColor = cfg.inn.newItem("In_Meeting");
            itmColor.setProperty("in_uniform_color", itmFirst.getProperty("in_uniform_color", ""));
            itmColor = itmColor.apply("in_meeting_uniform_color");
            itmReturn.setProperty("f1_fight", itmColor.getProperty("f1_fight", ""));
            itmReturn.setProperty("f2_fight", itmColor.getProperty("f2_fight", ""));
            //道服顏色: END

            var program_site_mat = itmFirst.getProperty("program_site_mat", "");
            var program_site_code = program_site_mat.Contains("-")
                ? program_site_mat.Split('-').First().Replace("MAT ", "")
                : "";

            itmReturn.setProperty("program_id", program_id);
            itmReturn.setProperty("program_display", itmFirst.getProperty("program_display", ""));
            itmReturn.setProperty("program_name2", itmFirst.getProperty("program_name2", ""));
            itmReturn.setProperty("program_name3", itmFirst.getProperty("program_name3", ""));
            itmReturn.setProperty("program_site_code", program_site_code);
            itmReturn.setProperty("hide_scores", hide_scores);
            itmReturn.setProperty("hide_points", hide_points);

            itmReturn.setProperty("event_id", itmFirst.getProperty("event_id", ""));
            itmReturn.setProperty("event_status", event_status);

            string in_win_status = itmFirst.getProperty("in_win_status", "");
            string in_tree_no = itmFirst.getProperty("in_tree_no", "");
            if (in_win_status == "cancel")
            {
                in_tree_no += "(C)";
            }

            itmReturn.setProperty("in_tree_no", in_tree_no);
            itmReturn.setProperty("in_tree_id", itmFirst.getProperty("in_tree_id", ""));
            itmReturn.setProperty("in_site", itmFirst.getProperty("in_site", ""));
            itmReturn.setProperty("site_name", itmFirst.getProperty("site_name", ""));
            itmReturn.setProperty("in_date_key", itmFirst.getProperty("in_date_key", ""));
            itmReturn.setProperty("in_win_status", itmFirst.getProperty("in_win_status", ""));
            itmReturn.setProperty("in_win_local_time", itmFirst.getProperty("in_win_local_time", ""));

            itmReturn.setProperty("in_round", itmFirst.getProperty("in_round", ""));
            itmReturn.setProperty("in_fight_id", itmFirst.getProperty("in_fight_id", ""));

            SetModalItem(itmReturn, items.getItemByIndex(0), "f1");
            SetModalItem(itmReturn, items.getItemByIndex(1), "f2");

            //上一場
            SetLastEvent(cfg, itmFirst, itmReturn);

            //下一場
            SetNextEvent(cfg, itmFirst, itmReturn);
        }

        private void SetLastEvent(TConfig cfg, Item itmEvent, Item itmReturn)
        {
            string in_site = itmEvent.getProperty("in_site", "");
            string in_date_key = itmEvent.getProperty("in_date_key", "");
            string in_tree_no = itmEvent.getProperty("in_tree_no", "");
            string site_name = itmEvent.getProperty("site_name", "");

            if (in_tree_no == "" || in_tree_no == "0")
            {
                itmReturn.setProperty("last_event_title", "無上一場資料");
                itmReturn.setProperty("last_event_message", "");
                return;
            }

            if (in_tree_no == "1")
            {
                itmReturn.setProperty("last_event_title", "已無上一場資料");
                itmReturn.setProperty("last_event_message", "");
                return;
            }

            string sql = @"
                SELECT
                    MAX(in_tree_no) AS 'target_tree_no'
                FROM 
                    IN_MEETING_PEVENT WITH(NOLOCK)
                WHERE
                    in_meeting = '{#meeting_id}'
                    AND ISNULL(in_site, '') = '{#in_site}'
                    AND ISNULL(in_date_key, '') = '{#in_date_key}'
                    AND ISNULL(in_tree_no, 0) <> 0
                    AND in_tree_no < {#in_tree_no}
                    --AND in_tree_name NOT IN ('rank56', 'rank78')
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_site}", in_site)
                .Replace("{#in_date_key}", in_date_key)
                .Replace("{#in_tree_no}", in_tree_no);

            Item itmTreeNo = cfg.inn.applySQL(sql);
            string target_tree_no = itmTreeNo.getProperty("target_tree_no", "");

            if (target_tree_no == "")
            {
                itmReturn.setProperty("last_event_title", "已無上一場資料");
                itmReturn.setProperty("last_event_message", "");
                return;
            }

            Item itmTargetEvent = GetTargetEvent(cfg, itmEvent, target_tree_no);
            int event_count = itmTargetEvent.getItemCount();

            if (event_count <= 0)
            {
                itmReturn.setProperty("last_event_title", "已無上一場資料");
                itmReturn.setProperty("last_event_message", "");
            }
            else if (event_count > 1)
            {
                string event_title = "[" + site_name + "]場次編號(" + target_tree_no + ")有重複資料";
                string event_message = "";
                for (int i = 0; i < event_count; i++)
                {
                    Item item = itmTargetEvent.getItemByIndex(i);
                    string _program_id = item.getProperty("program_id", "");
                    string _program_name3 = item.getProperty("program_name3", "");
                    string _in_tree_id = item.getProperty("in_tree_id", "");
                    event_message += "<a data-pid='" + _program_id + "' data-rid='" + _in_tree_id + "' onclick='MEvent_click(this)'>" + _program_name3 + "</a><br>";
                }

                itmReturn.setProperty("last_event_title", event_title);
                itmReturn.setProperty("last_event_message", event_message);
            }
            else
            {
                itmReturn.setProperty("last_event_pid", itmTargetEvent.getProperty("program_id", ""));
                itmReturn.setProperty("last_event_pname", itmTargetEvent.getProperty("program_name3", ""));
                itmReturn.setProperty("last_event_rid", itmTargetEvent.getProperty("in_tree_id", ""));
                itmReturn.setProperty("last_event_title", "");
                itmReturn.setProperty("last_event_message", "");
            }
        }

        private void SetNextEvent(TConfig cfg, Item itmEvent, Item itmReturn)
        {
            string in_site = itmEvent.getProperty("in_site", "");
            string in_date_key = itmEvent.getProperty("in_date_key", "");
            string in_tree_no = itmEvent.getProperty("in_tree_no", "");
            string site_name = itmEvent.getProperty("site_name", "");

            if (in_tree_no == "" || in_tree_no == "0")
            {
                itmReturn.setProperty("last_event_title", "無下一場資料");
                itmReturn.setProperty("last_event_message", "");
                return;
            }

            string sql = @"
                SELECT
                    MIN(in_tree_no) AS 'target_tree_no'
                FROM 
                    IN_MEETING_PEVENT WITH(NOLOCK)
                WHERE
                    ISNULL(in_site, '') = '{#in_site}'
                    AND ISNULL(in_date_key, '') = '{#in_date_key}'
                    AND ISNULL(in_tree_no, 0) <> 0
                    AND in_tree_no > {#in_tree_no}
                    --AND in_tree_name NOT IN ('rank56', 'rank78')
            ";

            sql = sql.Replace("{#in_site}", in_site)
                .Replace("{#in_date_key}", in_date_key)
                .Replace("{#in_tree_no}", in_tree_no);

            Item itmTreeNo = cfg.inn.applySQL(sql);
            string target_tree_no = itmTreeNo.getProperty("target_tree_no", "");

            if (target_tree_no == "")
            {
                itmReturn.setProperty("next_event_title", "已無下一場資料");
                itmReturn.setProperty("next_event_message", "");
                return;
            }

            Item itmTargetEvent = GetTargetEvent(cfg, itmEvent, target_tree_no);
            int event_count = itmTargetEvent.getItemCount();

            if (event_count <= 0)
            {
                itmReturn.setProperty("next_event_title", "已無下一場資料");
                itmReturn.setProperty("next_event_message", "");
            }
            else if (event_count > 1)
            {
                string event_title = "[" + site_name + "]場次編號(" + target_tree_no + ")有重複資料";
                string event_message = "";

                for (int i = 0; i < event_count; i++)
                {
                    Item item = itmTargetEvent.getItemByIndex(i);
                    string _program_id = item.getProperty("program_id", "");
                    string _program_name3 = item.getProperty("program_name3", "");
                    string _in_tree_id = item.getProperty("in_tree_id", "");
                    event_message += "<a data-pid='" + _program_id + "' data-rid='" + _in_tree_id + "' onclick='MEvent_click(this)'>" + _program_name3 + "</a><br>";
                }

                itmReturn.setProperty("next_event_title", event_title);
                itmReturn.setProperty("next_event_message", event_message);
            }
            else
            {
                itmReturn.setProperty("next_event_pid", itmTargetEvent.getProperty("program_id", ""));
                itmReturn.setProperty("next_event_pname", itmTargetEvent.getProperty("program_name3", ""));
                itmReturn.setProperty("next_event_rid", itmTargetEvent.getProperty("in_tree_id", ""));
                itmReturn.setProperty("next_event_title", "");
                itmReturn.setProperty("next_event_message", "");
            }
        }

        //可能多筆
        private Item GetTargetEvent(TConfig cfg, Item itmEvent, string in_tree_no)
        {
            string program_id = itmEvent.getProperty("program_id", "");
            string in_site = itmEvent.getProperty("in_site", "");
            string in_date_key = itmEvent.getProperty("in_date_key", "");

            string sql = @"
                SELECT 
                    t1.id            AS 'program_id'
                    , t1.in_name3    AS 'program_name3'
                    , t2.id            AS 'event_id'
                    , t2.in_tree_id
                    , t2.in_tree_no
                FROM
                    IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN 
                    IN_MEETING_PEVENT t2 WITH(NOLOCK)
                    ON t2.source_id = t1.id
                WHERE
                    t1.in_meeting = '{#meeting_id}'
                    AND ISNULL(t2.in_site, '') = '{#in_site}'
                    AND ISNULL(t2.in_date_key, '') = '{#in_date_key}'
                    AND t2.in_tree_no = {#in_tree_no}
                    --AND t2.in_tree_name NOT IN ('rank56', 'rank78')
                ORDER BY
                    t1.in_sort_order
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_site}", in_site)
                .Replace("{#in_date_key}", in_date_key)
                .Replace("{#in_tree_no}", in_tree_no);

            return cfg.inn.applySQL(sql);
        }

        private void SetModalItem(Item itmReturn, Item source, string pfx)
        {
            string event_type = source.getProperty("event_type", "");

            if (event_type == "" || event_type == "p")
            {
                SetMainModalItem(itmReturn, source, pfx);
            }
            else
            {
                string sect = source.getProperty("program_name3", "")
                    + " (" + source.getProperty("in_sub_sect", "") + ")";

                itmReturn.setProperty("program_name3", sect);

                SetSubModalItem(itmReturn, source, pfx);
            }
        }

        //主場次
        private void SetMainModalItem(Item itmReturn, Item source, string pfx)
        {
            string detail_id = source.getProperty("detail_id", "");
            string in_sign_bypass = source.getProperty("in_sign_bypass", "");

            string player_count = source.getProperty("player_count", "");
            string player_photo = source.getProperty("player_photo", "");
            string org_photo = source.getProperty("org_photo", "");

            string player_org = source.getProperty("player_org", "　");
            string player_team = source.getProperty("player_team", "　");
            string player_name = source.getProperty("player_name", "　");
            string player_sno = source.getProperty("player_sno", "　");

            string in_sign_no = source.getProperty("in_sign_no", "　");
            string in_section_no = source.getProperty("in_section_no", "　");
            string in_check_result = source.getProperty("in_check_result", "");
            string in_status = source.getProperty("in_status", "");

            string in_points = source.getProperty("in_points", "");
            string in_points_type = source.getProperty("in_points_type", "");
            string in_points_text = source.getProperty("in_points_text", "");

            string in_correct_count = source.getProperty("in_correct_count", "0");

            bool is_signle = player_count == "1";
            string show_photo = is_signle ? player_photo : org_photo;
            string in_display = GetStatusDisplay(in_status);

            string in_show_name = player_name;

            if (!is_signle)
            {
                if (player_team == "")
                {
                    in_show_name = player_org;
                }
                else
                {
                    in_show_name = player_org + " " + player_team + "隊";
                }
            }

            if (player_name == "")
            {
                player_name = "&nbsp;";
            }

            string in_check = "";
            if (in_check_result == "1")
            {
                in_check = "btn-success";
            }
            else if (in_check_result == "0")
            {
                in_check = "btn-danger";
            }

            itmReturn.setProperty(pfx + "_detail_id", detail_id);
            itmReturn.setProperty(pfx + "_photo", show_photo);
            itmReturn.setProperty(pfx + "_name", player_name);
            itmReturn.setProperty(pfx + "_sno", player_sno);
            itmReturn.setProperty(pfx + "_short_org", player_org);
            itmReturn.setProperty(pfx + "_show_name", in_show_name);

            //itmReturn.setProperty(pfx + "_sign_no", in_sign_no);
            itmReturn.setProperty(pfx + "_sign_no", in_section_no);
            itmReturn.setProperty(pfx + "_check", in_check);
            itmReturn.setProperty(pfx + "_status", in_display);

            itmReturn.setProperty(pfx + "_correct_count", in_correct_count);

            if (in_points == "" || in_points == "0")
            {
                itmReturn.setProperty(pfx + "_points_1", "");
                itmReturn.setProperty(pfx + "_points_2", "");
                itmReturn.setProperty(pfx + "_points_3", "0");
            }
            else if (in_points == "1")
            {
                itmReturn.setProperty(pfx + "_points_1", "");
                itmReturn.setProperty(pfx + "_points_2", "");
                itmReturn.setProperty(pfx + "_points_3", "1");
            }
            else if (in_points == "10")
            {
                itmReturn.setProperty(pfx + "_points_1", "1");
                itmReturn.setProperty(pfx + "_points_2", "0");
                itmReturn.setProperty(pfx + "_points_3", "");
            }
            else if (in_points == "11")
            {
                itmReturn.setProperty(pfx + "_points_1", "1");
                itmReturn.setProperty(pfx + "_points_2", "1");
                itmReturn.setProperty(pfx + "_points_3", "");
            }
            itmReturn.setProperty(pfx + "_points_text", in_points_text);

            if (in_sign_bypass == "1")
            {
                itmReturn.setProperty(pfx + "_hide", "item_show_0");
            }
        }

        //子場次
        private void SetSubModalItem(Item itmReturn, Item source, string pfx)
        {
            string detail_id = source.getProperty("detail_id", "");
            string in_sign_bypass = source.getProperty("in_sign_bypass", "");

            string player_count = source.getProperty("player_count", "");
            string player_photo = source.getProperty("player_photo", "");
            string org_photo = source.getProperty("org_photo", "");

            string player_org = source.getProperty("player_org", "　");
            string player_team = source.getProperty("player_team", "　");
            string player_name = source.getProperty("player_name", "　");
            string player_sno = source.getProperty("player_sno", "　");

            string in_sign_no = source.getProperty("in_sign_no", "　");
            string in_section_no = source.getProperty("in_section_no", "　");
            string in_check_result = source.getProperty("in_check_result", "");
            string in_status = source.getProperty("in_status", "");

            string in_points = source.getProperty("in_points", "");
            string in_points_type = source.getProperty("in_points_type", "");
            string in_points_text = source.getProperty("in_points_text", "");

            string in_correct_count = source.getProperty("in_correct_count", "0");

            bool is_signle = player_count == "1";
            string show_photo = is_signle ? player_photo : org_photo;
            string in_display = GetStatusDisplay(in_status);

            string in_show_name = player_name;

            if (!is_signle)
            {
                if (player_team == "")
                {
                    in_show_name = player_org;
                }
                else
                {
                    in_show_name = player_org + " " + player_team + "隊";
                }
            }

            if (player_name == "")
            {
                player_name = "&nbsp;";
            }

            string in_check = "";
            if (in_check_result == "1")
            {
                in_check = "btn-success";
            }
            else if (in_check_result == "0")
            {
                in_check = "btn-danger";
            }

            itmReturn.setProperty(pfx + "_detail_id", detail_id);
            itmReturn.setProperty(pfx + "_photo", show_photo);
            itmReturn.setProperty(pfx + "_name", player_name);
            itmReturn.setProperty(pfx + "_sno", player_sno);
            itmReturn.setProperty(pfx + "_short_org", player_org);
            itmReturn.setProperty(pfx + "_show_name", in_show_name);

            //itmReturn.setProperty(pfx + "_sign_no", in_sign_no);
            //itmReturn.setProperty(pfx + "_sign_no", in_section_no);
            itmReturn.setProperty(pfx + "_check", in_check);
            itmReturn.setProperty(pfx + "_status", in_display);

            itmReturn.setProperty(pfx + "_correct_count", in_correct_count);

            if (in_points == "" || in_points == "0")
            {
                itmReturn.setProperty(pfx + "_points_1", "");
                itmReturn.setProperty(pfx + "_points_2", "");
                itmReturn.setProperty(pfx + "_points_3", "0");
            }
            else if (in_points == "1")
            {
                itmReturn.setProperty(pfx + "_points_1", "");
                itmReturn.setProperty(pfx + "_points_2", "");
                itmReturn.setProperty(pfx + "_points_3", "1");
            }
            else if (in_points == "10")
            {
                itmReturn.setProperty(pfx + "_points_1", "1");
                itmReturn.setProperty(pfx + "_points_2", "0");
                itmReturn.setProperty(pfx + "_points_3", "");
            }
            else if (in_points == "11")
            {
                itmReturn.setProperty(pfx + "_points_1", "1");
                itmReturn.setProperty(pfx + "_points_2", "1");
                itmReturn.setProperty(pfx + "_points_3", "");
            }
            itmReturn.setProperty(pfx + "_points_text", in_points_text);

            //if (in_sign_no == "" || in_sign_no == "0" || in_sign_bypass == "1")
            //{
            //    itmReturn.setProperty(pfx + "_hide", "item_show_0");
            //}
        }

        //取得勝敗呈現
        private string GetStatusDisplay(string value)
        {
            switch (value)
            {
                case "1": return "<span class='team_score_b'>1</span>";
                case "0": return "<span class='team_score_c'>0</span>";
                default: return "<span class='team_score_a'>0</span>";
            }
        }

        //取得場次勝敗狀態
        private string GetEventStatus(string value)
        {
            switch (value)
            {
                case "cancel": return "(本場次取消)";

                case "nofight": return "(一方取消)";

                case "bypass": return "(輪空)";

                default: return "";
            }
        }
        private void AppendLevelOptions(TConfig cfg, Item itmReturn)
        {
            Item itmJson = cfg.inn.newItem("In_Meeting");
            itmJson.setProperty("meeting_id", cfg.meeting_id);
            itmJson.setProperty("need_id", "1");
            itmJson = itmJson.apply("in_meeting_program_options");
            itmReturn.setProperty("in_level_json", itmJson.getProperty("json", ""));
            itmReturn.setProperty("in_level_count", itmJson.getProperty("total", ""));
        }

        private void AppendEventOptions(TConfig cfg, Item itmReturn)
        {
            string sql = @"
                SELECT 
                    DISTINCT 
                    in_tree_sort
                    , in_tree_name
                    , in_round
                    , in_tree_id
                    , in_tree_no
                    , in_tree_rank
                    , in_tree_alias
                    , in_win_status
                FROM
                    IN_MEETING_PEVENT WITH(NOLOCK)
                WHERE
                    source_id = '{#program_id}'
                    AND ISNULL(in_tree_no, '') <> '' 
                ORDER BY
                    in_tree_sort
                    , in_round
                    , in_tree_no
             ";

            sql = sql.Replace("{#program_id}", cfg.program_id);

            //CCO.Utilities.WriteDebug(strMethodName, sql);

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();

            List<TNode> nodes = new List<TNode>();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);

                TNode l1 = AddAndGetNode(nodes, item, "in_tree_name");

                if (string.IsNullOrEmpty(l1.Lbl))
                {
                    switch (l1.Val)
                    {
                        case "main":
                            l1.Lbl = "對戰表";
                            l1.Sort = GetInt(item.getProperty("in_tree_sort", ""));
                            break;

                        case "repechage":
                            l1.Lbl = "復活表";
                            l1.Sort = GetInt(item.getProperty("in_tree_sort", ""));
                            break;

                        case "challenge-a":
                            l1.Lbl = "挑戰賽";
                            l1.Sort = GetInt(item.getProperty("in_tree_sort", ""));
                            break;

                        case "challenge-b":
                            l1.Lbl = "挑戰賽-最終賽";
                            l1.Sort = GetInt(item.getProperty("in_tree_sort", ""));
                            break;

                        case "rank56":
                            l1.Lbl = "五六名對戰";
                            l1.Sort = GetInt(item.getProperty("in_tree_sort", ""));
                            break;

                        case "rank78":
                            l1.Lbl = "七八名對戰";
                            l1.Sort = GetInt(item.getProperty("in_tree_sort", ""));
                            break;

                        default:
                            l1.Lbl = l1.Val;
                            l1.Sort = 99999;
                            break;
                    }
                }

                TNode l2 = AddAndGetNode(l1.Nodes, item, "in_round");

                if (string.IsNullOrEmpty(l2.Lbl))
                {
                    l2.Lbl = "R" + l2.Val;
                }

                TNode l3 = AddAndGetNode(l2.Nodes, item, "in_tree_id");
                string in_tree_no = item.getProperty("in_tree_no", "");
                string in_win_status = item.getProperty("in_win_status", "");

                l3.Lbl = "第 " + in_tree_no + " 場";
                if (in_win_status != "")
                {
                    l3.Lbl += "(已登錄)";
                }
            }

            itmReturn.setProperty("in_event_json", Newtonsoft.Json.JsonConvert.SerializeObject(nodes.OrderBy(x => x.Sort)));
        }

        private TNode AddAndGetNode(List<TNode> nodes, Item item, string key)
        {
            string value = item.getProperty(key, "");

            TNode search = nodes.Find(x => x.Val == value);

            if (search == null)
            {
                search = new TNode
                {
                    Val = value,
                    Nodes = new List<TNode>()
                };

                nodes.Add(search);
            }

            return search;
        }

        private class TNode
        {
            public string Val { get; set; }
            public string Lbl { get; set; }
            public int Sort { get; set; }
            public List<TNode> Nodes { get; set; }
        }

        private class TSub
        {
            public string name { get; set; }
            public string org { get; set; }
        }

        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string meeting_id { get; set; }
            public string program_id { get; set; }
            public string event_id { get; set; }
            public string mode { get; set; }
            public string draw_mode { get; set; }
            public bool is_tkd_mode { get; set; }
        }

        private void CopyProperty(Item itmA, Item itmB, string propA, string propB = "")
        {
            if (propB == "")
            {
                itmA.setProperty(propA, itmB.getProperty(propA, ""));
            }
            else
            {
                itmA.setProperty(propA, itmB.getProperty(propB, ""));
            }
        }
        private int GetInt(string value, int def = 0)
        {
            if (value == "") return 0;
            int result = def;
            int.TryParse(value, out result);
            return result;
        }
    }
}