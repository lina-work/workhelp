﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Wresling.Fight
{
    public class in_meeting_allocation2 : Item
    {
        public in_meeting_allocation2(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 場地分配
                輸入: meeting_id
                日期: 
                    2020-11-30: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]in_meeting_allocation2";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "input dom: " + itmR.dom.InnerXml);

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                meeting_id = itmR.getProperty("meeting_id", ""),
                mode = itmR.getProperty("mode", ""),
                scene = itmR.getProperty("scene", ""),
                need_update_event = itmR.getProperty("need_update_event", ""),
                isJudo = true,
            };

            if (cfg.meeting_id == "")
            {
                itmR.setProperty("error_message", "賽事 id 不得為空白");
                return itmR;
            }

            //檢查頁面權限
            Item itmPermit = inn.applyMethod("In_CheckIdentity", "<method>" + strMethodName + "</method><code>" + "ALL" + "</code>");
            bool isMeetingAdmin = itmPermit.getProperty("isMeetingAdmin", "") == "1";
            bool isMeetingLeader = itmPermit.getProperty("isMeetingLeader", "") == "1";

            bool can_view = isMeetingAdmin || isMeetingLeader;
            if (!can_view)
            {
                itmR.setProperty("error_message", "您無權限瀏覽此頁面");
                return itmR;
            }

            switch (cfg.mode)
            {
                case "edit":
                    Query(cfg, itmR);
                    AppendDateMenu(cfg, itmR);
                    break;

                case "save_meeting":
                    SaveAllocationMeeting(cfg, itmR);
                    break;

                case "save_day":
                    SaveAllocationDay(cfg, itmR);
                    break;

                case "clear":
                    ClearAllocation(cfg, itmR);
                    break;

                default:
                    Query(cfg, itmR);
                    AppendDateMenu(cfg, itmR);
                    break;
            }

            return itmR;
        }

        //附加日期選單
        private void AppendDateMenu(TConfig cfg, Item itmReturn)
        {
            string meeting_id = itmReturn.getProperty("meeting_id", "");

            Item itmEmpty = cfg.inn.newItem();
            itmEmpty.setType("inn_date");
            itmEmpty.setProperty("value", "");
            itmEmpty.setProperty("label", "請選擇");
            itmReturn.addRelationship(itmEmpty);

            string sql = "SELECT DISTINCT in_date_key FROM IN_MEETING_ALLOCATION WITH(NOLOCK) WHERE in_meeting = '{#meeting_id}' ORDER BY in_date_key";
            sql = sql.Replace("{#meeting_id}", meeting_id);
            Item items = cfg.inn.applySQL(sql);
            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_date_key = item.getProperty("in_date_key", "");

                item.setType("inn_date");
                item.setProperty("value", in_date_key);
                item.setProperty("label", in_date_key);
                itmReturn.addRelationship(item);
            }
        }

        private void ClearAllocation(TConfig cfg, Item itmReturn)
        {
            var sql = "UPDATE IN_MEETING_PROGRAM SET"
                + "  in_fight_day = NULL"
                + ", in_site = NULL"
                + ", in_site_code = NULL"
                + ", in_site_mat = NULL"
                + ", in_site_mat2 = NULL"
                + " WHERE in_meeting = '" + cfg.meeting_id + "'";

            cfg.inn.applySQL(sql);

            sql = "DELETE FROM IN_MEETING_ALLOCATION WHERE in_meeting = '" + cfg.meeting_id + "'";
            cfg.inn.applySQL(sql);

            if (cfg.need_update_event == "1")
            {
                FixEventDayAndSiteByMeeting(cfg);
            }
        }

        private void SaveAllocationDay(TConfig cfg, Item itmReturn)
        {
            var in_fight_day = itmReturn.getProperty("in_fight_day", "");

            SaveAllocationProgram(cfg, itmReturn);

            MergeAllocationByDay(cfg, in_fight_day);

            if (cfg.need_update_event == "1")
            {
                FixEventDayAndSiteByDay(cfg, in_fight_day);
            }
        }

        private void SaveAllocationMeeting(TConfig cfg, Item itmReturn)
        {
            SaveAllocationProgram(cfg, itmReturn);
            
            MergeAllocationByMeeting(cfg);

            if (cfg.need_update_event == "1")
            {
                FixEventDayAndSiteByMeeting(cfg);
            }
        }

        private void SaveAllocationProgram(TConfig cfg, Item itmReturn)
        {
            var values = itmReturn.getProperty("values", "");
            var rows = Newtonsoft.Json.JsonConvert.DeserializeObject<List<TProgram>>(values);
            if (rows == null || rows.Count == 0) throw new Exception("無組別資料");

            var siteMap = GetMeetingSiteMap(cfg);

            for (var i = 0; i < rows.Count; i++)
            {
                var row = rows[i];
                var in_fight_day = string.IsNullOrWhiteSpace(row.in_fight_day) ? "NULL" : "'" + row.in_fight_day + "'";

                var in_site = "NULL";
                var in_site_code = "NULL";
                var in_site_mat = "NULL";
                var in_site_mat2 = "NULL";

                if (siteMap.ContainsKey(row.in_site_code))
                {
                    var itmSite = siteMap[row.in_site_code];
                    in_site = "'" + itmSite.getProperty("id", "") + "'";
                    in_site_code = "'" + itmSite.getProperty("in_code", "") + "'";
                    if (cfg.isJudo)
                    {
                        in_site_mat = "'MAT " + itmSite.getProperty("in_code", "") + "-" + row.in_site_serial + "'";
                        in_site_mat2 = "'MAT " + itmSite.getProperty("in_code", "") + "-" + row.in_site_serial.PadLeft(2, '0') + "'";
                    }
                    else
                    {
                        in_site_mat = "'" + itmSite.getProperty("in_code_en", "") + "-" + row.in_site_serial + "'";
                        in_site_mat2 = "'" + itmSite.getProperty("in_code_en", "") + "-" + row.in_site_serial.PadLeft(2, '0') + "'";
                    }
                }

                var sql = "UPDATE IN_MEETING_PROGRAM SET"
                    + "  in_fight_day = " + in_fight_day
                    + ", in_site = " + in_site
                    + ", in_site_code = " + in_site_code
                    + ", in_site_mat = " + in_site_mat
                    + ", in_site_mat2 = " + in_site_mat2
                    + " WHERE id = '" + row.id + "'";

                cfg.inn.applySQL(sql);
            }
        }

        private void MergeAllocationByDay(TConfig cfg, string in_fight_day)
        {
            var sql = "DELETE FROM IN_MEETING_ALLOCATION WHERE in_meeting = '" + cfg.meeting_id + "' AND in_date_key = '" + in_fight_day + "'";
            cfg.inn.applySQL(sql);

            var items = GetAllocationItems(cfg, in_fight_day);
            CreateMeetingAllocationCategory(cfg, items);
            CreateMeetingAllocationFight(cfg, items);
        }

        private void MergeAllocationByMeeting(TConfig cfg)
        {
            var sql = "DELETE FROM IN_MEETING_ALLOCATION WHERE in_meeting = '" + cfg.meeting_id + "'";
            cfg.inn.applySQL(sql);

            var items = GetAllocationItems(cfg, "");
            CreateMeetingAllocationCategory(cfg, items);
            CreateMeetingAllocationFight(cfg, items);
        }

        private Item GetAllocationItems(TConfig cfg, string in_fight_day)
        {
            var day_cond = in_fight_day == "" ? "" : "AND in_fight_day = '" + in_fight_day + "'";

            var sql = @"
                SELECT 
	                id
	                , in_fight_day
	                , in_site_code
	                , in_site_mat2
	                , in_name
	                , in_l1
	                , in_l2
	                , in_l3
	                , in_site
                FROM 
	                IN_MEETING_PROGRAM WITH(NOLOCK)
                WHERE 
	                in_meeting = '{#meeting_id}'
	                AND ISNULL(in_fight_day, '') <> ''
	                AND ISNULL(in_site, '') <> ''
                    {#day_cond}
                ORDER BY
	                in_fight_day
	                , in_site_code
	                , in_site_mat2
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#day_cond}", day_cond);

            return cfg.inn.applySQL(sql);
        }

        private void CreateMeetingAllocationCategory(TConfig cfg, Item items)
        {
            var list = new List<TCategory>();
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var in_fight_day = item.getProperty("in_fight_day", "");
                var in_l1 = item.getProperty("in_l1", "");
                var in_l2 = item.getProperty("in_l2", "");
                var key = in_fight_day + "-" + in_l1 + "-" + in_l2;
                var existed = list.Find(x => x.key == key);
                if (existed != null) continue;

                list.Add(new TCategory { key = key, value = item });
            }

            var sorted = list.OrderBy(x => x.key).ToList();
            var format = "yyyy-MM-ddTHH:mm:ss";
            for (var i = 0; i < sorted.Count; i++)
            {
                var item = sorted[i].value;
                var in_fight_day = item.getProperty("in_fight_day", "");
                var in_date = GetDateTimeVal(in_fight_day, format);
                var in_l1 = item.getProperty("in_l1", "");
                var in_l2 = item.getProperty("in_l2", "");

                var itmNew = cfg.inn.newItem("In_Meeting_Allocation");
                itmNew.setProperty("in_meeting", cfg.meeting_id);
                itmNew.setProperty("in_type", "category");
                itmNew.setProperty("in_date_key", in_fight_day);
                itmNew.setProperty("in_date", in_date);
                itmNew.setProperty("in_l1", in_l1);
                itmNew.setProperty("in_l2", in_l2);
                itmNew.setProperty("in_place", "");
                itmNew.apply("add");
            }
        }

        private void CreateMeetingAllocationFight(TConfig cfg, Item items)
        {
            var count = items.getItemCount();
            var list = new List<string>();

            var dtNow = DateTime.Now.AddHours(-10);
            var format = "yyyy-MM-ddTHH:mm:ss";
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var in_fight_day = item.getProperty("in_fight_day", "");
                var in_date = GetDateTimeVal(in_fight_day, format);

                var itmNew = cfg.inn.newItem("In_Meeting_Allocation");
                itmNew.setProperty("in_meeting", cfg.meeting_id);
                itmNew.setProperty("in_type", "fight");
                itmNew.setProperty("in_date_key", in_fight_day);
                itmNew.setProperty("in_date", in_date);
                itmNew.setProperty("in_site", item.getProperty("in_site", ""));
                itmNew.setProperty("in_program", item.getProperty("id", ""));
                itmNew.setProperty("in_place", "");
                itmNew = itmNew.apply("add");

                var id = itmNew.getProperty("id", "");
                var now = dtNow.ToString(format);
                list.Add("UPDATE IN_MEETING_ALLOCATION SET created_on = '" + now + "' WHERE id = '" + id + "'");
                dtNow = dtNow.AddSeconds(10);
            }

            for (var i = 0; i < list.Count; i++)
            {
                cfg.inn.applySQL(list[i]);
            }
        }

        private void FixEventDayAndSiteByDay(TConfig cfg, string in_fight_day)
        {
            var sql = @"
                 UPDATE t1 SET
             	    t1.in_date_key = t2.in_fight_day
             	    , t1.in_site = t2.in_site
             	    , t1.in_site_code = t2.in_site_code
             	    , t1.in_show_site = t2.in_site_code
             	    , t1.in_show_serial = t1.in_tree_no
             	    , t1.in_site_move = NULL
             	    , t1.in_site_time = NULL
                 FROM
             	    IN_MEETING_PEVENT t1
                 INNER JOIN
                    IN_MEETING_PROGRAM t2
                    ON t2.id = t1.source_id
                 WHERE
                    t2.in_meeting = '{#meeting_id}'
                    AND t2.in_fight_day = '{#in_fight_day}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_fight_day}", in_fight_day);

            cfg.inn.applySQL(sql);
        }

        private void FixEventDayAndSiteByMeeting(TConfig cfg)
        {
            var sql = @"
                 UPDATE t1 SET
             	    t1.in_date_key = t2.in_fight_day
             	    , t1.in_site = t2.in_site
             	    , t1.in_site_code = t2.in_site_code
             	    , t1.in_show_site = t2.in_site_code
             	    , t1.in_show_serial = t1.in_tree_no
             	    , t1.in_site_move = NULL
             	    , t1.in_site_time = NULL
                 FROM
             	    IN_MEETING_PEVENT t1
                 INNER JOIN
                    IN_MEETING_PROGRAM t2
                    ON t2.id = t1.source_id
                 WHERE
                    t2.in_meeting = '{#meeting_id}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            cfg.inn.applySQL(sql);
        }

        //查詢
        private void Query(TConfig cfg, Item itmReturn)
        {
            Item itmMeeting = GetMeeting(cfg);
            if (itmMeeting.isError())
            {
                itmReturn.setProperty("error_message", "取得賽事資料發生錯誤");
                return;
            }

            //附加賽事資訊
            AppendMeeting(itmMeeting, itmReturn);

            var in_day_json = Newtonsoft.Json.JsonConvert.SerializeObject(MapDayList(cfg));
            itmReturn.setProperty("in_day_json", in_day_json);

            var in_site_json = Newtonsoft.Json.JsonConvert.SerializeObject(MapSiteList(cfg));
            itmReturn.setProperty("in_site_json", in_site_json);

            var in_sect_json = Newtonsoft.Json.JsonConvert.SerializeObject(MapSectList(cfg));
            itmReturn.setProperty("in_sect_json", in_sect_json);
        }

        private List<TProgram> MapSectList(TConfig cfg)
        {
            var rows = new List<TProgram>();
            var items = cfg.inn.applySQL("SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE in_meeting = '" + cfg.meeting_id + "' ORDER BY in_fight_day, in_site_code, in_site_mat2, in_sort_order");
            if (items.isError() || items.getResult() == "") return rows;
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var row = new TProgram
                {
                    id = item.getProperty("id", ""),
                    in_name = item.getProperty("in_name", ""),
                    in_name2 = item.getProperty("in_name2", ""),
                    in_name3 = item.getProperty("in_name3", ""),
                    in_l1 = item.getProperty("in_l1", ""),
                    in_l2 = item.getProperty("in_l2", ""),
                    in_l3 = item.getProperty("in_l3", ""),
                    in_fight_day = item.getProperty("in_fight_day", ""),
                    in_site = item.getProperty("in_site", ""),
                    in_site_code = item.getProperty("in_site_code", ""),
                    in_site_mat = item.getProperty("in_site_mat", ""),
                    in_site_mat2 = item.getProperty("in_site_mat2", ""),
                    in_team_count = item.getProperty("in_team_count", "0"),
                    in_event_count = item.getProperty("in_event_count", "0"),
                };
                rows.Add(row);
            }
            return rows;
        }

        private class TProgram
        {
            public string id { get; set; }
            public string in_name { get; set; }
            public string in_name2 { get; set; }
            public string in_name3 { get; set; }
            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }
            public string in_fight_day { get; set; }
            public string in_site { get; set; }
            public string in_site_code { get; set; }
            public string in_site_mat { get; set; }
            public string in_site_mat2 { get; set; }
            public string in_site_serial { get; set; }
            public string in_team_count { get; set; }
            public string in_event_count { get; set; }
        }

        private List<TRow> MapDayList(TConfig cfg)
        {
            var rows = new List<TRow>();
            var item = cfg.inn.applySQL("SELECT TOP 1 in_value FROM IN_MEETING_VARIABLE WITH(NOLOCK) WHERE source_id = '" + cfg.meeting_id + "' AND in_key = 'fight_days'");
            if (item.isError() || item.getResult() == "") return rows;

            var in_value = item.getProperty("in_value", "");
            if (in_value == "") return rows;

            var days = in_value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            if (days == null || days.Length == 0) return rows;

            for (var i = 0; i < days.Length; i++)
            {
                var day = days[i];
                var row = new TRow
                {
                    id = day,
                    code = day,
                    text = day,
                    label = day,
                };
                rows.Add(row);
            }
            return rows;
        }

        private List<TRow> MapSiteList(TConfig cfg)
        {
            var rows = new List<TRow>();
            var items = GetMeetingSites(cfg);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var row = new TRow
                {
                    id = item.getProperty("id", ""),
                    code = item.getProperty("in_code", ""),
                    text = item.getProperty("in_code_en", ""),
                    label = item.getProperty("in_name", ""),
                };
                rows.Add(row);
            }
            return rows;
        }

        private class TRow
        {
            public string id { get; set; }
            public string code { get; set; }
            public string text { get; set; }
            public string label { get; set; }
        }

        //附加賽事資訊
        private void AppendMeeting(Item itmMeeting, Item itmReturn)
        {
            itmReturn.setProperty("in_title", itmMeeting.getProperty("in_title", ""));
            itmReturn.setProperty("in_battle_type", itmMeeting.getProperty("in_battle_type", ""));
            itmReturn.setProperty("in_surface_code", itmMeeting.getProperty("in_surface_code", ""));
            itmReturn.setProperty("banner_file", itmMeeting.getProperty("in_banner_photo", ""));
            itmReturn.setProperty("banner_file2", itmMeeting.getProperty("in_banner_photo2", ""));
        }

        //取得賽事資訊
        private Item GetMeeting(TConfig cfg)
        {
            string aml = @"
                <AML>
                    <Item type='In_Meeting' action='get' id='{#meeting_id}' select='in_title,in_battle_type,in_surface_code,in_banner_photo,in_banner_photo2'>
                    </Item>
                </AML>
            ".Replace("{#meeting_id}", cfg.meeting_id);

            return cfg.inn.applyAML(aml);
        }

        private Dictionary<string, Item> GetMeetingSiteMap(TConfig cfg)
        {
            var map = new Dictionary<string, Item>();
            var items = GetMeetingSites(cfg);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var code = item.getProperty("in_code", "");
                if (map.ContainsKey(code)) continue;
                map.Add(code, item);
            }
            return map;
        }

        //取得賽事場地資訊
        private Item GetMeetingSites(TConfig cfg)
        {
            string sql = @"SELECT * FROM IN_MEETING_SITE WITH(NOLOCK) WHERE in_meeting = '" + cfg.meeting_id + "'";
            return cfg.inn.applySQL(sql);
        }

        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public Innovator inn { get; set; }

            public string meeting_id { get; set; }
            public string mode { get; set; }
            public string scene { get; set; }
            public string need_update_event { get; set; }
            public bool isJudo { get; set; }

            public Item itmMeeting { get; set; }
            public string mt_battle_type { get; set; }
            public bool is_challenge { get; set; }
            public int robin_player { get; set; }

            public string day_battle_type { get; set; }

            /// <summary>
            /// 字型名稱
            /// </summary>
            public string FontName { get; set; }

            /// <summary>
            /// 字型大小
            /// </summary>
            public int FontSize { get; set; }

            /// <summary>
            /// 列起始位置
            /// </summary>
            public int RowStart { get; set; }

            /// <summary>
            /// 欄起始位置
            /// </summary>
            public int ColStart { get; set; }

            /// <summary>
            /// 當前列位置
            /// </summary>
            public int CurrentRow { get; set; }

            /// <summary>
            /// 賽事名稱
            /// </summary>
            public string Title { get; set; }
        }

        private class TCategory
        {
            public string key { get; set; }
            public Item value { get; set; }
        }

        private string GetJsonValue(Newtonsoft.Json.Linq.JObject obj, string key)
        {
            Newtonsoft.Json.Linq.JToken value;
            if (obj.TryGetValue(key, out value))
            {
                return value.ToString();
            }
            return "";
        }

        private int GetIntVal(string value, int def = 0)
        {
            int result = def;
            int.TryParse(value, out result);
            return result;
        }

        private DateTime GetDtmeVal(string value)
        {
            DateTime result = DateTime.MinValue;
            DateTime.TryParse(value, out result);
            return result;
        }

        private string GetDateTimeVal(string value, string format, int add_hours = 0)
        {
            DateTime result = DateTime.MinValue;
            DateTime.TryParse(value, out result);
            if (result != DateTime.MinValue)
            {
                if (format == "chinese")
                {
                    return ChineseDateTime(result.AddHours(add_hours));
                }
                else
                {
                    return result.AddHours(add_hours).ToString(format);
                }
            }
            else
            {
                return "";
            }
        }

        /// <summary>
        /// 轉換為民國年月日(星期)
        /// </summary>
        private string ChineseDateTime(DateTime value)
        {
            var y = value.Year - 1911;
            var m = value.Month.ToString().PadLeft(2, '0');
            var d = value.Day.ToString().PadLeft(2, '0');
            var w = "";
            switch (value.DayOfWeek)
            {
                case DayOfWeek.Monday: w = "星期一"; break;
                case DayOfWeek.Tuesday: w = "星期二"; break;
                case DayOfWeek.Wednesday: w = "星期三"; break;
                case DayOfWeek.Thursday: w = "星期四"; break;
                case DayOfWeek.Friday: w = "星期五"; break;
                case DayOfWeek.Saturday: w = "星期六"; break;
                case DayOfWeek.Sunday: w = "星期日"; break;
                default: break;

            }
            return y + "年" + m + "月" + d + "日" + "（" + w + "）";
        }
    }
}