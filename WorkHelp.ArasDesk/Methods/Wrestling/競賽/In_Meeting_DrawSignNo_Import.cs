﻿using Aras.IOM;
using System;
using System.Collections.Generic;
using System.Linq;

namespace WorkHelp.ArasDesk.Methods.Wrestling.Common
{
    public class In_Meeting_DrawSignNo_Import : Item
    {
        public In_Meeting_DrawSignNo_Import(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 籤表籤號匯入
                日誌: 2024-04-22: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "In_Meeting_DrawSignNo_Import";

            var itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                scene = itmR.getProperty("scene", ""),
                in_battle_mode = itmR.getProperty("in_battle_mode", ""),
            };

            if (cfg.in_battle_mode == "")
            {
                throw new Exception("請選擇活動賽制");
            }

            switch (cfg.in_battle_mode)
            {
                case "uww":
                    ImportUWW(cfg, itmR);
                    break;

                case "knockout":
                    ImportKnockout(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void ImportUWW(TConfig cfg, Item itmReturn)
        {
            var value = itmReturn.getProperty("value", "");
            if (value == "") throw new Exception("無匯入資料");

            var resource = MapList(cfg, value);
            if (resource == null || resource.Count == 0) throw new Exception("無匯入資料");

            var map = new Dictionary<string, TRow>();
            for (var i = 0; i < resource.Count; i++)
            {
                var x = resource[i];
                if (string.IsNullOrWhiteSpace(x.in_fight_id)) continue;
                if (string.IsNullOrWhiteSpace(x.in_sign_foot)) continue;
                var old = default(TRow);
                if (map.ContainsKey(x.in_fight_id))
                {
                    old = map[x.in_fight_id];
                }
                else
                {
                    old = new TRow
                    {
                        in_fight_id = x.in_fight_id,
                        in_available = 0,
                    };
                    map.Add(x.in_fight_id, old);
                }

                if (x.in_sign_foot == "1")
                {
                    old.in_f1_no = x.in_sign_no;
                    old.in_available++;
                }
                else if (x.in_sign_foot == "2")
                {
                    old.in_f2_no = x.in_sign_no;
                    old.in_available++;
                }
            }

            cfg.eventMap = map;
            
            for (var i = 0; i < resource.Count; i++)
            {
                var x = resource[i];
                x.in_no = x.in_team_count * 100 + (i + 1);

                var itmNew = cfg.inn.newItem("In_Meeting_DrawSignNo", "merge");
                itmNew.setAttribute("where", "in_mode = '" + cfg.in_battle_mode + "' AND in_no = '" + x.in_no + "'");
                itmNew.setProperty("in_mode", x.in_mode);
                itmNew.setProperty("in_team_count", x.in_team_count.ToString());
                itmNew.setProperty("in_no", x.in_no.ToString());
                itmNew.setProperty("in_fight_id", x.in_fight_id);
                itmNew.setProperty("in_sign_foot", x.in_sign_foot);
                itmNew.setProperty("in_sign_no", x.in_sign_no);
                itmNew.setProperty("in_bypass_status", x.in_bypass_status);

                if (x.in_bypass_status == "1")
                {
                    itmNew.setProperty("in_f1_no", "");
                    itmNew.setProperty("in_f2_no", "");
                    itmNew.setProperty("in_available", "0");
                }
                else if (cfg.eventMap.ContainsKey(x.in_fight_id))
                {
                    var evt = cfg.eventMap[x.in_fight_id];
                    if (!string.IsNullOrWhiteSpace(evt.in_f1_no))
                    {
                        itmNew.setProperty("in_f1_no", evt.in_f1_no);
                    }
                    if (!string.IsNullOrWhiteSpace(evt.in_f2_no))
                    {
                        itmNew.setProperty("in_f2_no", evt.in_f2_no);
                    }
                    itmNew.setProperty("in_available", evt.in_available.ToString());
                }
                else
                {
                    itmNew.setProperty("in_f1_no", "");
                    itmNew.setProperty("in_f2_no", "");
                    itmNew.setProperty("in_available", "0");
                }
                itmNew.apply();
            }
        }

        private void ImportKnockout(TConfig cfg, Item itmReturn)
        {
            var value = itmReturn.getProperty("value", "");
            if (value == "") throw new Exception("無匯入資料");

            var resource = MapList(cfg, value);
            if (resource == null || resource.Count == 0) throw new Exception("無匯入資料");

            ResetEventMap(cfg, resource);

            var rows = MapDrawSignNoRows(cfg, resource);
            for (var i = 0; i < rows.Count; i++)
            {
                var x = rows[i];
                var itmNew = cfg.inn.newItem("In_Meeting_DrawSignNo", "merge");
                itmNew.setAttribute("where", "in_mode = '" + x.in_mode + "' AND in_no = '" + x.in_no + "'");
                itmNew.setProperty("in_mode", x.in_mode);
                itmNew.setProperty("in_team_count", x.in_team_count.ToString());
                itmNew.setProperty("in_no", x.in_no.ToString());
                itmNew.setProperty("in_fight_id", x.in_fight_id);
                itmNew.setProperty("in_sign_foot", x.in_sign_foot);
                itmNew.setProperty("in_sign_no", x.in_sign_no);
                itmNew.setProperty("in_bypass_status", x.in_bypass_status);

                if (x.in_bypass)
                {
                    itmNew.setProperty("in_f1_no", "");
                    itmNew.setProperty("in_f2_no", "");
                    itmNew.setProperty("in_available", "0");
                }
                else if (cfg.eventMap.ContainsKey(x.in_fight_id))
                {
                    var evt = cfg.eventMap[x.in_fight_id];
                    itmNew.setProperty("in_f1_no", evt.in_f1_no);
                    itmNew.setProperty("in_f2_no", evt.in_f2_no);
                    itmNew.setProperty("in_available", evt.in_available.ToString());
                }
                else
                {
                    itmNew.setProperty("in_f1_no", "");
                    itmNew.setProperty("in_f2_no", "");
                    itmNew.setProperty("in_available", "0");
                }

                itmNew.apply();
            }
        }

        private void ResetEventMap(TConfig cfg, List<TRow> resource)
        {
            cfg.eventMap = new Dictionary<string, TRow>();
            for (var i = 0; i < resource.Count; i++)
            {
                var x = resource[i];
                x.in_mode = cfg.in_battle_mode;

                if (!string.IsNullOrWhiteSpace(x.in_sign_no))
                {
                    x.is_a = true;
                }
                else if (!string.IsNullOrWhiteSpace(x.in_from_id))
                {
                    x.is_b = true;
                }

                if (x.is_b)
                {
                    cfg.eventMap.Add(x.in_from_id, new TRow
                    {
                        in_fight_id = x.in_from_id,
                        in_f1_no = x.in_from_f1,
                        in_f2_no = x.in_from_f2,
                        in_available = 2,
                    });
                    continue;
                }

                if (!x.is_a) continue;

                var row = default(TRow);
                if (cfg.eventMap.ContainsKey(x.in_fight_id))
                {
                    row = cfg.eventMap[x.in_fight_id];
                }
                else
                {
                    row = new TRow
                    {
                        in_fight_id = x.in_fight_id,
                        in_f1_no = string.Empty,
                        in_f2_no = string.Empty,
                        in_available = 0,
                    };
                    cfg.eventMap.Add(x.in_fight_id, row);
                }

                if (x.in_sign_foot == "1")
                {
                    row.in_f1_no = x.in_sign_no;
                    row.in_available++;
                }
                else if (x.in_sign_foot == "2")
                {
                    row.in_f2_no = x.in_sign_no;
                    row.in_available++;
                }
            }
        }

        private List<TRow> MapDrawSignNoRows(TConfig cfg, List<TRow> resource)
        {
            var rows = new List<TRow>();
            var ns = new List<int>();
            for (var i = 0; i < resource.Count; i++)
            {
                var x = resource[i];
                if (x.is_b)
                {
                    ns.Add(GetInt(x.in_from_f1));
                    ns.Add(GetInt(x.in_from_f2));
                    continue;
                }
                else if (x.is_a)
                {
                    ns.Add(GetInt(x.in_sign_no));
                    x.in_no = x.in_team_count * 100 + rows.Count + 1;
                    x.in_bypass_status = "0";
                    rows.Add(x);
                }
            }

            for (var i = 0; i < ns.Count; i++)
            {
                var a = i + 1;
                var b = ns[i];
                if (a != b) throw new Exception("跳號: " + b);
            }

            var maxNumber = ns.Last();
            var teamCount = rows[0].in_team_count;
            if (teamCount != maxNumber) throw new Exception("人數與最大籤號無法匹配: " + teamCount + " vs " + maxNumber);

            for (var i = 0; i < resource.Count; i++)
            {
                var x = resource[i];
                if (string.IsNullOrWhiteSpace(x.in_sign_no))
                {
                    AppendFoots(cfg, rows, x);
                }
                else if (!string.IsNullOrWhiteSpace(x.in_from_id))
                {
                    AppendByPass(cfg, rows, x);
                }
            }
            return rows;
        }

        private void AppendByPass(TConfig cfg, List<TRow> rows, TRow x)
        {
            var foot = new TRow
            {
                in_no = x.in_team_count * 100 + rows.Count + 1,
                in_mode = x.in_mode,
                in_team_count = x.in_team_count,
                in_fight_id = x.in_from_id,
                in_sign_foot = "1",
                in_sign_no = x.in_sign_no,
                in_bypass_status = "1",
                in_bypass = true,
            };
            rows.Add(foot);
        }

        private void AppendFoots(TConfig cfg, List<TRow> rows, TRow x)
        {
            var f1 = new TRow
            {
                in_no = x.in_team_count * 100 + rows.Count + 1,
                in_mode = x.in_mode,
                in_team_count = x.in_team_count,
                in_fight_id = x.in_from_id,
                in_sign_foot = "1",
                in_sign_no = x.in_from_f1,
                in_bypass_status = "0",
            };
            rows.Add(f1);
            var f2 = new TRow
            {
                in_no = x.in_team_count * 100 + rows.Count + 1,
                in_mode = x.in_mode,
                in_team_count = x.in_team_count,
                in_fight_id = x.in_from_id,
                in_sign_foot = "2",
                in_sign_no = x.in_from_f2,
                in_bypass_status = "0",
            };
            rows.Add(f2);
        }

        private class TRow
        {
            public int in_no { get; set; }
            public string in_mode { get; set; }
            public int in_team_count { get; set; }
            public string in_fight_id { get; set; }
            public string in_sign_foot { get; set; }
            public string in_sign_no { get; set; }
            public string in_from_f1 { get; set; }
            public string in_from_f2 { get; set; }
            public string in_from_id { get; set; }
            public string in_bypass_status { get; set; }
            public string in_f1_no { get; set; }
            public string in_f2_no { get; set; }
            public int in_available { get; set; }
            public bool is_a { get; set; }
            public bool is_b { get; set; }
            public bool in_bypass { get; set; }
        }

        private List<TRow> MapList(TConfig cfg, string value)
        {
            try
            {
                var list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<TRow>>(value);
                return list;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string scene { get; set; }
            public string in_battle_mode { get; set; }
            public Dictionary<string, TRow> eventMap { get; set; }
        }

        private int GetInt(string value)
        {
            if (value == "" || value == "0") return 0;
            int result = 0;
            int.TryParse(value, out result);
            return result;
        }
    }
}