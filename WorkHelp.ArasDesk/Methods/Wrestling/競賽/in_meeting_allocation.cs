﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;
using Innosoft;
using System.Web.WebSockets;
using System.Security.Cryptography;
using System.ComponentModel;
using Aras.Server.Core;
using Microsoft.SqlServer.Server;

namespace WorkHelp.ArasDesk.Methods.Wrestling.Common
{
    public class in_meeting_allocation : Item
    {
        public in_meeting_allocation(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 場地分配
                輸入: meeting_id
                日期: 
                    2020-11-30: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]in_meeting_allocation";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "input dom: " + itmR.dom.InnerXml);

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                meeting_id = itmR.getProperty("meeting_id", ""),
                mode = itmR.getProperty("mode", ""),
                scene = itmR.getProperty("scene", ""),
                link = itmR.getProperty("link", ""),
                menu = itmR.getProperty("menu", ""),
                need_update_event = itmR.getProperty("need_update_event", ""),
            };

            if (cfg.meeting_id == "")
            {
                itmR.setProperty("error_message", "賽事 id 不得為空白");
                return itmR;
            }

            if (cfg.menu == "no")
            {
                itmR.setProperty("hide_menu", "item_show_0");
            }

            //檢查頁面權限
            Item itmPermit = inn.applyMethod("In_CheckIdentity", "<method>" + strMethodName + "</method><code>" + "ALL" + "</code>");
            bool isMeetingAdmin = itmPermit.getProperty("isMeetingAdmin", "") == "1";
            bool isMeetingLeader = itmPermit.getProperty("isMeetingLeader", "") == "1";

            bool can_view = isMeetingAdmin || isMeetingLeader;
            if (!can_view)
            {
                itmR.setProperty("error_message", "您無權限瀏覽此頁面");
                return itmR;
            }

            switch (cfg.mode)
            {
                case "edit":
                    Query(cfg, itmR, isEdit: true, isUpload: false, linkWeight: cfg.link == "weight");
                    AppendDateMenu(cfg, itmR);
                    break;

                case "save":
                    SaveAllocation(cfg, itmR);
                    break;

                case "clear":
                    ClearAllocation(cfg, itmR);
                    break;

                case "remove":
                    Remove(cfg, itmR);
                    break;

                case "removeDate":
                    RemoveDate(cfg, itmR);
                    break;

                case "export":
                    Export(cfg, itmR);
                    break;

                case "allocate":
                    Allocate(cfg, itmR);
                    break;

                default:
                    Query(cfg, itmR, isEdit: false, isUpload: cfg.scene == "upload", linkWeight: cfg.link == "weight");
                    AppendDateMenu(cfg, itmR);
                    break;
            }

            return itmR;
        }

        //附加日期選單
        private void AppendDateMenu(TConfig cfg, Item itmReturn)
        {
            string meeting_id = itmReturn.getProperty("meeting_id", "");

            Item itmEmpty = cfg.inn.newItem();
            itmEmpty.setType("inn_date");
            itmEmpty.setProperty("value", "");
            itmEmpty.setProperty("label", "請選擇");
            itmReturn.addRelationship(itmEmpty);

            string sql = "SELECT DISTINCT in_date_key FROM IN_MEETING_ALLOCATION WITH(NOLOCK) WHERE in_meeting = '{#meeting_id}' ORDER BY in_date_key";
            sql = sql.Replace("{#meeting_id}", meeting_id);
            Item items = cfg.inn.applySQL(sql);
            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_date_key = item.getProperty("in_date_key", "");

                item.setType("inn_date");
                item.setProperty("value", in_date_key);
                item.setProperty("label", in_date_key);
                itmReturn.addRelationship(item);
            }
        }

        #region 匯出
        //匯出
        private void Export(TConfig cfg, Item itmReturn)
        {
            //取得賽事資訊
            Item itmMeeting = GetMeeting(cfg);
            if (itmMeeting.isError())
            {
                itmReturn.setProperty("error_message", "取得賽事資料發生錯誤");
                return;
            }

            //取得場地資訊
            Item itmSites = GetMeetingSites(cfg);
            if (itmSites.isError())
            {
                itmReturn.setProperty("error_message", "取得場地資訊發生錯誤");
                return;
            }

            //取得場地分配資訊
            Item itmAllocations = GetMeetingAllocations(cfg);
            if (itmAllocations.isError())
            {
                itmReturn.setProperty("error_message", "取得場地分配資訊發生錯誤");
                return;
            }

            //轉換場地分配
            var map = MapAllocation(cfg, itmAllocations, itmSites);

            //設定匯出資訊
            string main_name = itmMeeting.getProperty("in_title", "");
            string sub_name = "場次分配表";
            TExport export = GetExportInfo(cfg, main_name, sub_name);

            cfg.FontSize = 12;
            cfg.FontName = "標楷體";
            cfg.RowStart = 3;
            cfg.ColStart = 1;
            cfg.Title = itmMeeting.getProperty("in_title", "");

            ClosedXML.Excel.XLWorkbook workbook = new ClosedXML.Excel.XLWorkbook();

            foreach (var kv in map.Allocations)
            {
                var allocation = kv.Value;
                AppendAllocationSheet(cfg, workbook, map, allocation);
            }

            workbook.SaveAs(export.File);

            itmReturn.setProperty("xls_name", export.Url);
        }

        //場次分配表
        private void AppendAllocationSheet(TConfig cfg
            , ClosedXML.Excel.XLWorkbook workbook
            , TMap map
            , TAllocation allocation)
        {
            ClosedXML.Excel.IXLWorksheet sheet = workbook.Worksheets.Add(allocation.DisplayDate);

            //賽事標題
            ClosedXML.Excel.IXLCell cell_title = sheet.Cell(1, cfg.ColStart);
            cell_title.Value = cfg.Title + "場地分配表";
            cell_title.Style.Font.Bold = true;
            cell_title.Style.Font.FontSize = 16;
            cell_title.Style.Font.FontName = cfg.FontName;
            cell_title.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
            cell_title.Style.Alignment.Vertical = ClosedXML.Excel.XLAlignmentVerticalValues.Center;

            sheet.Range(1, cfg.ColStart, 1, cfg.ColStart + map.Cols - 1)
                .Merge()
                .Style.Alignment.Vertical = ClosedXML.Excel.XLAlignmentVerticalValues.Center;

            sheet.Row(1).Height = 50;


            //比賽日期
            ClosedXML.Excel.IXLCell cell_date = sheet.Cell(2, cfg.ColStart);
            cell_date.Value = allocation.ChineseDate;
            cell_date.Style.Font.FontSize = 14;
            cell_date.Style.Font.FontName = cfg.FontName;
            cell_date.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
            cell_date.Style.Alignment.Vertical = ClosedXML.Excel.XLAlignmentVerticalValues.Center;

            sheet.Range(2, cfg.ColStart, 2, cfg.ColStart + map.Cols - 1)
                .Merge()
                .Style.Alignment.Vertical = ClosedXML.Excel.XLAlignmentVerticalValues.Center;

            sheet.Row(2).Height = 30;


            int wsRow = cfg.RowStart;
            int wsCol = cfg.ColStart;

            for (int i = 0; i < map.Rows; i++)
            {
                for (int j = 0; j < map.Cols; j++)
                {
                    string key = i + "_" + j;
                    string value = "";
                    if (map.Sites.ContainsKey(key))
                    {
                        value = map.Sites[key].Name;
                    }
                    SetCell(sheet, wsRow, wsCol + j, cfg, value, format: "head");
                }
                wsRow++;

                for (int j = 0; j < map.Cols; j++)
                {
                    string key = i + "_" + j;
                    if (allocation.SiteGroups.ContainsKey(key))
                    {
                        var list = allocation.SiteGroups[key];
                        string value = string.Join(Environment.NewLine, list.Select(x => x.Label));
                        SetCell(sheet, wsRow, wsCol + j, cfg, value, format: "wrap");
                    }
                }
                wsRow++;
            }

            var width = (int)(84 / map.Cols);
            for (int j = 0; j < map.Cols; j++)
            {
                sheet.Column(wsCol + j).Width = width;
            }
        }

        //設定物件與資料列
        private void SetCell(ClosedXML.Excel.IXLWorksheet sheet, int wsRow, int wsCol, TConfig cfg, string value, string format = "")
        {
            ClosedXML.Excel.IXLCell cell = sheet.Cell(wsRow, wsCol);

            cell.Style.Font.FontSize = cfg.FontSize;
            cell.Style.Font.FontName = cfg.FontName;

            cell.Style.Border.SetRightBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetTopBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetBottomBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetLeftBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);

            switch (format)
            {
                case "head":
                    cell.Value = value;
                    cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    cell.Style.Font.Bold = true;
                    cell.DataType = ClosedXML.Excel.XLDataType.Text;
                    //cell.Style.Fill.BackgroundColor = ClosedXML.Excel.XLColor.FromHtml("#295C90");
                    //cell.Style.Font.FontColor = ClosedXML.Excel.XLColor.White;
                    break;

                case "wrap":
                    cell.Value = value;
                    cell.Style.Alignment.Vertical = ClosedXML.Excel.XLAlignmentVerticalValues.Top;
                    cell.Style.Alignment.WrapText = true;
                    sheet.Columns(wsRow, wsCol).AdjustToContents();
                    break;

                case "center":
                    cell.Value = value;
                    cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    break;

                case "yyyy/MM/dd":
                case "yyyy/MM/dd HH:mm":
                    cell.Value = GetDateTimeVal(value, format);
                    cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    break;

                case "$ #,##0":
                    cell.Value = value;
                    cell.Style.NumberFormat.Format = "$ #,##0";
                    break;

                case "tel":
                    if (value.StartsWith("09"))
                    {
                        cell.Value = value.Replace("-", "");
                        cell.Style.NumberFormat.Format = "0000-000-000";
                        cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    }
                    else
                    {
                        cell.Value = "'" + value;
                        cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    }
                    break;

                case "text":
                    cell.Value = "'" + value;
                    cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    break;

                default:
                    cell.Value = value;
                    break;
            }
        }

        /// <summary>
        /// 匯出資料模型
        /// </summary>
        private class TExport
        {
            /// <summary>
            /// 樣板來源完整路徑
            /// </summary>
            public string Source { get; set; }

            /// <summary>
            /// 檔案名稱
            /// </summary>
            public string File { get; set; }

            /// <summary>
            /// 檔案網址
            /// </summary>
            public string Url { get; set; }
        }

        /// <summary>
        /// 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public Innovator inn { get; set; }

            public string meeting_id { get; set; }
            public string mode { get; set; }
            public string scene { get; set; }
            public string link { get; set; }
            public string menu { get; set; }
            public string need_update_event { get; set; }

            public string in_date_key { get; set; }
            public string in_sort { get; set; }


            public Item itmMeeting { get; set; }
            public string mt_battle_type { get; set; }
            public bool is_challenge { get; set; }
            public int robin_player { get; set; }

            public string day_battle_type { get; set; }

            /// <summary>
            /// 字型名稱
            /// </summary>
            public string FontName { get; set; }

            /// <summary>
            /// 字型大小
            /// </summary>
            public int FontSize { get; set; }

            /// <summary>
            /// 列起始位置
            /// </summary>
            public int RowStart { get; set; }

            /// <summary>
            /// 欄起始位置
            /// </summary>
            public int ColStart { get; set; }

            /// <summary>
            /// 當前列位置
            /// </summary>
            public int CurrentRow { get; set; }

            /// <summary>
            /// 賽事名稱
            /// </summary>
            public string Title { get; set; }
        }

        /// <summary>
        /// 取得匯出設定資訊
        /// </summary>
        private TExport GetExportInfo(TConfig cfg, string main_name, string sub_name)
        {
            Item itmPath = GetXlsPaths(cfg);

            string export_path = itmPath.getProperty("export_path", "").TrimEnd('\\');
            string ext_name = ".xlsx";
            string doc_name = main_name + "_" + sub_name + "_" + DateTime.Now.ToString("yyyyMMdd_HHmmss");

            string doc_file = export_path + "\\" + doc_name + ext_name;
            string doc_url = doc_name + ext_name;

            return new TExport
            {
                Source = itmPath.getProperty("template_path", ""),
                File = doc_file,
                Url = doc_url,
            };
        }

        /// <summary>
        /// 取得樣板與匯出路徑
        /// </summary>
        private Item GetXlsPaths(TConfig cfg, string in_name = "")
        {
            Item itmResult = cfg.inn.newItem();

            //Excel 匯出路徑 (from 原創.設定參數)
            string sql = @"
                SELECT 
                    t2.in_name AS 'variable'
                    , t1.in_name
                    , t1.in_value 
                FROM 
                    In_Variable_Detail t1 WITH(NOLOCK)
                INNER JOIN 
                    In_Variable t2 WITH(NOLOCK) 
                    ON t2.ID = t1.SOURCE_ID 
                WHERE 
                    t2.in_name = N'meeting_excel'  
                    AND t1.in_name IN (N'export_path', N'{#in_name}')
            ";

            sql = sql.Replace("{#in_name}", in_name);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item items = cfg.inn.applySQL(sql);

            if (items.isError() || items.getItemCount() < 1)
            {
                throw new Exception("未設定樣板與匯出路徑 _# " + in_name);
            }

            for (int i = 0; i < items.getItemCount(); i++)
            {
                Item item = items.getItemByIndex(i);
                string xls_name = item.getProperty("in_name", "");
                string xls_value = item.getProperty("in_value", "");

                if (xls_name == "export_path")
                {
                    itmResult.setProperty("export_path", xls_value);
                }
                else
                {
                    itmResult.setProperty("template_path", xls_value);
                }
            }

            return itmResult;
        }
        #endregion 匯出

        //移除當日場地分配
        private void RemoveDate(TConfig cfg, Item itmReturn)
        {
            string sql = "";
            Item itmSQL = null;

            string meeting_id = itmReturn.getProperty("meeting_id", "");
            string in_date_key = itmReturn.getProperty("in_date_key", "");

            sql = "DELETE FROM IN_MEETING_ALLOCATION WHERE in_meeting = '" + meeting_id + "' AND in_date_key = '" + in_date_key + "'";
            itmSQL = cfg.inn.applySQL(sql);
            if (itmSQL.isError()) throw new Exception("移除失敗");


            sql = "UPDATE IN_MEETING_PEVENT SET in_date_key = NULL, in_site = NULL, in_site_code = NULL WHERE in_meeting = '" + meeting_id + "' AND ISNULL(in_date_key, '') = '" + in_date_key + "'";
            itmSQL = cfg.inn.applySQL(sql);
            if (itmSQL.isError()) throw new Exception("移除失敗");
        }

        //移除
        private void Remove(TConfig cfg, Item itmReturn)
        {
            string sql = "";
            Item itmSQL = null;

            string allocation_id = itmReturn.getProperty("allocation_id", "");

            Item itmAllocation = cfg.inn.applySQL("SELECT * FROM IN_MEETING_ALLOCATION WITH(NOLOCK) WHERE id = '" + allocation_id + "'");
            if (itmAllocation.isError() || itmAllocation.getResult() == "" || itmAllocation.getItemCount() != 1)
            {
                throw new Exception("查無資料");
            }

            sql = "DELETE FROM IN_MEETING_ALLOCATION WHERE id = '" + allocation_id + "'";
            itmSQL = cfg.inn.applySQL(sql);
            if (itmSQL.isError())
            {
                throw new Exception("移除失敗");
            }

            string meeting_id = itmAllocation.getProperty("in_meeting", "");
            string in_date_key = itmAllocation.getProperty("in_date_key", "");
            string in_program = itmAllocation.getProperty("in_program", "");

            sql = "UPDATE IN_MEETING_PEVENT SET in_date_key = NULL, in_site = NULL, in_site_code = NULL WHERE source_id = '" + in_program + "'";
            itmSQL = cfg.inn.applySQL(sql);
            if (itmSQL.isError()) throw new Exception("移除失敗");
        }

        private void ClearAllocation(TConfig cfg, Item itmReturn)
        {
            var sql = "UPDATE IN_MEETING_PROGRAM SET"
                + "  in_fight_day = NULL"
                + ", in_site = NULL"
                + ", in_site_code = NULL"
                + ", in_site_mat = NULL"
                + ", in_site_mat2 = NULL"
                + " WHERE in_meeting = '" + cfg.meeting_id + "'";

            cfg.inn.applySQL(sql);

            sql = "DELETE FROM IN_MEETING_ALLOCATION WHERE in_meeting = '" + cfg.meeting_id + "'";
            cfg.inn.applySQL(sql);

            if (cfg.need_update_event == "1")
            {
                FixEventDayAndSite(cfg);
            }
        }

        private void SaveAllocation(TConfig cfg, Item itmReturn)
        {
            var values = itmReturn.getProperty("values", "");
            var rows = Newtonsoft.Json.JsonConvert.DeserializeObject<List<TProgram>>(values);
            if (rows == null || rows.Count == 0) throw new Exception("無組別資料");

            var siteMap = GetMeetingSiteMap(cfg);

            for (var i = 0; i < rows.Count; i++)
            {
                var row = rows[i];
                var in_fight_day = string.IsNullOrWhiteSpace(row.in_fight_day) ? "NULL" : "'" + row.in_fight_day + "'";

                var in_site = "NULL";
                var in_site_code = "NULL";
                var in_site_mat = "NULL";
                var in_site_mat2 = "NULL";

                if (siteMap.ContainsKey(row.in_site_code))
                {
                    var itmSite = siteMap[row.in_site_code];
                    in_site = "'" + itmSite.getProperty("id", "") + "'";
                    in_site_code = "'" + itmSite.getProperty("in_code", "") + "'";
                    in_site_mat = "'" + itmSite.getProperty("in_code_en", "") + "-" + row.in_site_serial + "'";
                    in_site_mat2 = "'" + itmSite.getProperty("in_code_en", "") + "-" + row.in_site_serial.PadLeft(2, '0') + "'";
                }

                var sql = "UPDATE IN_MEETING_PROGRAM SET"
                    + "  in_fight_day = " + in_fight_day
                    + ", in_site = " + in_site
                    + ", in_site_code = " + in_site_code
                    + ", in_site_mat = " + in_site_mat
                    + ", in_site_mat2 = " + in_site_mat2
                    + " WHERE id = '" + row.id + "'";

                cfg.inn.applySQL(sql);
            }

            MergeMeetingAllocation(cfg);

            if (cfg.need_update_event == "1")
            {
                FixEventDayAndSite(cfg);
            }
        }

        private void MergeMeetingAllocation(TConfig cfg)
        {
            var sql = "DELETE FROM IN_MEETING_ALLOCATION WHERE in_meeting = '" + cfg.meeting_id + "'";
            cfg.inn.applySQL(sql);

            sql = @"
                SELECT 
	                id
	                , in_fight_day
	                , in_site_code
	                , in_site_mat2
	                , in_name
	                , in_l1
	                , in_l2
	                , in_l3
	                , in_site
                FROM 
	                IN_MEETING_PROGRAM 
                WHERE 
	                in_meeting = '{#meeting_id}'
	                AND ISNULL(in_fight_day, '') <> ''
	                AND ISNULL(in_site, '') <> ''
                ORDER BY
	                in_fight_day
	                , in_site_code
	                , in_site_mat2
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            var items = cfg.inn.applySQL(sql);

            CreateMeetingAllocationCategory(cfg, items);
            CreateMeetingAllocationFight(cfg, items);
        }

        private void CreateMeetingAllocationCategory(TConfig cfg, Item items)
        {
            var list = new List<string>();
            var count = items.getItemCount();

            var format = "yyyy-MM-ddTHH:mm:ss";
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var in_fight_day = item.getProperty("in_fight_day", "");
                var in_date = GetDateTimeVal(in_fight_day, format, -8);
                var in_l1 = item.getProperty("in_l1", "");
                var in_l2 = item.getProperty("in_l2", "");
                var key = in_l1 + "-" + in_l2;
                if (list.Contains(key)) continue;

                var itmNew = cfg.inn.newItem("In_Meeting_Allocation");
                itmNew.setProperty("in_meeting", cfg.meeting_id);
                itmNew.setProperty("in_type", "category");
                itmNew.setProperty("in_date_key", in_fight_day);
                itmNew.setProperty("in_date", in_date);
                itmNew.setProperty("in_site", item.getProperty("in_site", ""));
                itmNew.setProperty("in_l1", in_l1);
                itmNew.setProperty("in_l2", in_l1);
                itmNew.setProperty("in_place", "");
                itmNew.apply("add");

                list.Add(key);
            }
        }

        private void CreateMeetingAllocationFight(TConfig cfg, Item items)
        {
            var count = items.getItemCount();
            var list = new List<string>();

            var dtNow = DateTime.Now.AddHours(-10);
            var format = "yyyy-MM-ddTHH:mm:ss";
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var in_fight_day = item.getProperty("in_fight_day", "");
                var in_date = GetDateTimeVal(in_fight_day, format, -8);

                var itmNew = cfg.inn.newItem("In_Meeting_Allocation");
                itmNew.setProperty("in_meeting", cfg.meeting_id);
                itmNew.setProperty("in_type", "fight");
                itmNew.setProperty("in_date_key", in_fight_day);
                itmNew.setProperty("in_date", in_date);
                itmNew.setProperty("in_site", item.getProperty("in_site", ""));
                itmNew.setProperty("in_program", item.getProperty("id", ""));
                itmNew.setProperty("in_place", "");
                itmNew = itmNew.apply("add");

                var id = itmNew.getProperty("id", "");
                var now = dtNow.ToString(format);
                list.Add("UPDATE IN_MEETING_ALLOCATION SET created_on = '" + now + "' WHERE id = '" + id + "'");
                dtNow = dtNow.AddSeconds(10);
            }

            for (var i = 0; i < list.Count; i++)
            {
                cfg.inn.applySQL(list[i]);
            }
        }

        private void FixEventDayAndSite(TConfig cfg)
        {
            var sql = @"
                 UPDATE t1 SET
             	    t1.in_date_key = t2.in_fight_day
             	    , t1.in_site = t2.in_site
             	    , t1.in_site_code = t4.in_site_code
             	    , t1.in_show_site = t4.in_site_code
             	    , t1.in_show_serial = t1.in_tree_no
             	    , t1.in_site_move = NULL
             	    , t1.in_site_time = NULL
                 FROM
             	    IN_MEETING_PEVENT t1
                 INNER JOIN
                    IN_MEETING_PROGRAM t2
                    ON t2.id = t1.source_id
                 WHERE
                    t2.in_meeting = '{#meeting_id}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            cfg.inn.applySQL(sql);
        }

        //儲存
        private void RunSave(TConfig cfg, string in_l2, Item itmReturn)
        {
            string sql = "";
            Item itmSQL = null;

            string meeting_id = itmReturn.getProperty("meeting_id", "");
            string in_date_source = itmReturn.getProperty("in_date", "");
            string in_place = itmReturn.getProperty("in_place", "");
            string in_l1 = itmReturn.getProperty("in_l1", "");

            string in_date = GetDateTimeVal(in_date_source, "yyyy-MM-ddTHH:mm:ss", add_hours: 0);
            string in_date_key = GetDateTimeVal(in_date_source, "yyyy-MM-dd");

            sql = @"
                SELECT
        	        *
                FROM
        	        IN_MEETING_ALLOCATION WITH(NOLOCK)
                WHERE
        	        in_meeting = '{#meeting_id}'
        	        AND in_date_key = '{#in_date_key}'
        	        AND in_l1 = N'{#in_l1}'
        	        AND in_l2 = N'{#in_l2}'
            ";

            sql = sql.Replace("{#meeting_id}", meeting_id)
                .Replace("{#in_date_key}", in_date_key)
                .Replace("{#in_l1}", in_l1)
                .Replace("{#in_l2}", in_l2);

            itmSQL = cfg.inn.applySQL(sql);
            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            if (itmSQL.getItemCount() > 0)
            {
                throw new Exception("該組別已加入");
            }

            Item itmAllocation = cfg.inn.newItem("In_Meeting_Allocation");
            itmAllocation.setProperty("in_meeting", meeting_id);
            itmAllocation.setProperty("in_type", "category");
            itmAllocation.setProperty("in_date_key", in_date_key);
            itmAllocation.setProperty("in_date", in_date);
            itmAllocation.setProperty("in_place", in_place);
            itmAllocation.setProperty("in_l1", in_l1);
            itmAllocation.setProperty("in_l2", in_l2);
            itmAllocation = itmAllocation.apply("add");
        }

        //儲存
        private void Allocate(TConfig cfg, Item itmReturn)
        {
            string sql = "";
            Item itmSQL = null;

            string in_date_source = itmReturn.getProperty("in_date_key", "");
            string in_site = itmReturn.getProperty("in_site", "");
            string in_program = itmReturn.getProperty("in_program", "");

            string in_date = GetDateTimeVal(in_date_source, "yyyy-MM-ddTHH:mm:ss", add_hours: 0);
            string in_date_key = in_date_source;

            string in_place = "";

            sql = @"
                SELECT 
                	TOP 1 * 
                FROM 
                	IN_MEETING_ALLOCATION WITH(NOLOCK) 
                WHERE 
                	in_type = 'category' 
                	AND in_meeting =  '{#meeting_id}' 
                	And in_date_key = '{#in_date_key}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_date_key}", in_date_key);

            Item itmCategory = cfg.inn.applySQL(sql);

            if (!itmCategory.isError() && itmCategory.getResult() != "")
            {
                in_place = itmCategory.getProperty("in_place", "");
            }

            sql = @"
                SELECT
        	        *
                FROM
        	        IN_MEETING_ALLOCATION WITH(NOLOCK)
                WHERE
        	        in_meeting = '{#meeting_id}'
        	        AND in_date_key = '{#in_date_key}'
        	        AND in_site = N'{#in_site}'
        	        AND in_program = N'{#in_program}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_date_key}", in_date_key)
                .Replace("{#in_site}", in_site)
                .Replace("{#in_program}", in_program);

            itmSQL = cfg.inn.applySQL(sql);

            if (itmSQL.getItemCount() > 0)
            {
                throw new Exception("該組別已加入");
            }

            Item itmAllocation = cfg.inn.newItem("In_Meeting_Allocation");
            itmAllocation.setProperty("in_meeting", cfg.meeting_id);
            itmAllocation.setProperty("in_type", "fight");
            itmAllocation.setProperty("in_date_key", in_date_key);
            itmAllocation.setProperty("in_date", in_date);
            itmAllocation.setProperty("in_site", in_site);
            itmAllocation.setProperty("in_program", in_program);
            itmAllocation.setProperty("in_place", in_place);
            itmAllocation = itmAllocation.apply("add");

            if (itmAllocation.isError())
            {
                throw new Exception("加入失敗");
            }
            else
            {
                Item itmSite = cfg.inn.applySQL("SELECT * FROM IN_MEETING_SITE WITH(NOLOCK) WHERE id = '" + in_site + "'");
                string in_site_code = itmSite.getProperty("in_code");

                string allocation_id = itmAllocation.getProperty("id", "");
                string program_id = itmAllocation.getProperty("in_program", "");

                sql = "UPDATE IN_MEETING_PROGRAM SET"
                    + " in_site_code = '" + in_site_code + "'"
                    + ", in_allocation = '" + allocation_id + "'"
                    + ", in_fight_day = '" + in_date_key + "'"
                    + ", in_fight_site = N'" + in_place + "'"
                    + " WHERE id = '" + program_id + "'";

                cfg.inn.applySQL(sql);

                //修補場次日期
                FixDate(cfg, program_id);


                Item itmProgram = cfg.inn.applySQL("SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + program_id + "'");

                itmReturn.setProperty("allocation_id", allocation_id);
                itmReturn.setProperty("program_id", program_id);
                itmReturn.setProperty("program_name2", itmProgram.getProperty("in_name2", ""));
                itmReturn.setProperty("program_team_count", itmProgram.getProperty("in_event_count", ""));
            }
        }

        //修正日期
        private void FixDate(TConfig cfg, string program_id)
        {
            string sql = "";
            Item itmSQL = null;

            sql = @"
                 UPDATE t1 SET
             	    t1.in_date_key = t3.in_date_key
             	    , t1.in_site = t4.id
             	    , t1.in_site_code = t4.in_code
                 FROM
             	    IN_MEETING_PEVENT t1
                 INNER JOIN
                    IN_MEETING_PROGRAM t2
                    ON t2.id = t1.source_id
                 INNER JOIN
             	    IN_MEETING_ALLOCATION t3
             	    ON t3.in_program = t2.id
                 INNER JOIN
             	    IN_MEETING_SITE t4
             	    ON t4.id = t3.in_site
                 WHERE
                    t2.id = '{#program_id}'
            ";

            sql = sql.Replace("{#program_id}", program_id);

            itmSQL = cfg.inn.applySQL(sql);
        }

        //查詢
        private void Query(TConfig cfg, Item itmReturn, bool isEdit = false, bool isUpload = false, bool linkWeight = false)
        {
            Item itmMeeting = GetMeeting(cfg);
            if (itmMeeting.isError())
            {
                itmReturn.setProperty("error_message", "取得賽事資料發生錯誤");
                return;
            }

            //附加賽事資訊
            AppendMeeting(itmMeeting, itmReturn);

            //附加賽會參數
            Item itmMtVrl = cfg.inn.applyMethod("In_Meeting_Variable"
                    , "<meeting_id>" + cfg.meeting_id + "</meeting_id>"
                    + "<name>allocate_mode</name>"
                    + "<scene>get</scene>");
            itmReturn.setProperty("allocate_mode", itmMtVrl.getProperty("inn_result", ""));

            if (isEdit)
            {
                //附加賽事選單
                Item itmJson = cfg.inn.newItem("In_Meeting");
                itmJson.setProperty("meeting_id", cfg.meeting_id);
                itmJson.setProperty("need_id", "1");
                itmJson = itmJson.apply("in_meeting_program_options");
                itmReturn.setProperty("in_level_json", itmJson.getProperty("json", ""));
                itmReturn.setProperty("in_level_count", itmJson.getProperty("total", ""));
            }

            var in_day_json = Newtonsoft.Json.JsonConvert.SerializeObject(MapDayList(cfg));
            itmReturn.setProperty("in_day_json", in_day_json);

            var in_site_json = Newtonsoft.Json.JsonConvert.SerializeObject(MapSiteList(cfg));
            itmReturn.setProperty("in_site_json", in_site_json);

            var in_sect_json = Newtonsoft.Json.JsonConvert.SerializeObject(MapSectList(cfg));
            itmReturn.setProperty("in_sect_json", in_sect_json);
        }

        private List<TProgram> MapSectList(TConfig cfg)
        {
            var rows = new List<TProgram>();
            var items = cfg.inn.applySQL("SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE in_meeting = '" + cfg.meeting_id + "' ORDER BY in_fight_day, in_site_code, in_site_mat2, in_sort_order");
            if (items.isError() || items.getResult() == "") return rows;
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var row = new TProgram
                {
                    id = item.getProperty("id", ""),
                    in_name = item.getProperty("in_name", ""),
                    in_name2 = item.getProperty("in_name2", ""),
                    in_name3 = item.getProperty("in_name3", ""),
                    in_l1 = item.getProperty("in_l1", ""),
                    in_l2 = item.getProperty("in_l2", ""),
                    in_l3 = item.getProperty("in_l3", ""),
                    in_fight_day = item.getProperty("in_fight_day", ""),
                    in_site = item.getProperty("in_site", ""),
                    in_site_code = item.getProperty("in_site_code", ""),
                    in_site_mat = item.getProperty("in_site_mat", ""),
                    in_site_mat2 = item.getProperty("in_site_mat2", ""),
                    in_team_count = item.getProperty("in_team_count", "0"),
                    in_event_count = item.getProperty("in_event_count", "0"),
                };
                rows.Add(row);
            }
            return rows;
        }

        private class TProgram
        {
            public string id { get; set; }
            public string in_name { get; set; }
            public string in_name2 { get; set; }
            public string in_name3 { get; set; }
            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }
            public string in_fight_day { get; set; }
            public string in_site { get; set; }
            public string in_site_code { get; set; }
            public string in_site_mat { get; set; }
            public string in_site_mat2 { get; set; }
            public string in_site_serial { get; set; }
            public string in_team_count { get; set; }
            public string in_event_count { get; set; }
        }

        private List<TRow> MapDayList(TConfig cfg)
        {
            var rows = new List<TRow>();
            var item = cfg.inn.applySQL("SELECT TOP 1 in_value FROM IN_MEETING_VARIABLE WITH(NOLOCK) WHERE source_id = '" + cfg.meeting_id + "' AND in_key = 'fight_days'");
            if (item.isError() || item.getResult() == "") return rows;

            var in_value = item.getProperty("in_value", "");
            if (in_value == "") return rows;

            var days = in_value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            if (days == null || days.Length == 0) return rows;

            for (var i = 0; i < days.Length; i++)
            {
                var day = days[i];
                var row = new TRow
                {
                    id = day,
                    code = day,
                    text = day,
                    label = day,
                };
                rows.Add(row);
            }
            return rows;
        }

        private List<TRow> MapSiteList(TConfig cfg)
        {
            var rows = new List<TRow>();
            var items = GetMeetingSites(cfg);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var row = new TRow
                {
                    id = item.getProperty("id", ""),
                    code = item.getProperty("in_code", ""),
                    text = item.getProperty("in_code_en", ""),
                    label = item.getProperty("in_name", ""),
                };
                rows.Add(row);
            }
            return rows;
        }

        private class TRow
        {
            public string id { get; set; }
            public string code { get; set; }
            public string text { get; set; }
            public string label { get; set; }
        }

        //附加場地分配資訊
        private void AppendAllocations(TConfig cfg, TMap map, Item itmReturn)
        {
            StringBuilder tabs = new StringBuilder();
            StringBuilder contents = new StringBuilder();

            //頁籤
            int no = 1;
            tabs.Append("<ul class='nav nav-pills nav-site' id='pills-tab' role='tablist'>");
            foreach (var kv in map.Allocations)
            {
                var active = no == 1 ? "class='nav-item active'" : "nav-item";
                var allocation = kv.Value;
                var ele_id = "tab_" + allocation.Key.Replace("-", "");

                tabs.Append("<li " + active + ">"
                    + " <a class='nav-link'"
                    + " id='" + ele_id + "'"
                    + " data-toggle='pill' role='tab' aria-controls='pills-home' aria-selected='true'"
                    + " href='#" + allocation.Key + "'"
                    + " onclick='StoreTab_Click(this)'><i class='fa fa-calendar' aria-hidden='true'></i>" + allocation.DisplayDate + "</a></li>");

                no++;
            }
            tabs.Append("</ul>");

            no = 1;
            contents.Append("<div class='tab-content page-tab-content' id='pills-tabContent'>");
            foreach (var kv in map.Allocations)
            {
                var active = no == 1 ? "active" : "";
                var allocation = kv.Value;

                contents.Append("<div id='" + allocation.Key + "' class='tab-pane fade in " + active + "' role='tabpanel'>");
                AppendAllocationTable(contents, map, allocation);
                contents.Append("</div>");

                no++;
            }
            contents.Append("</div>");

            StringBuilder builder = new StringBuilder();
            builder.Append("<div class='col-lg-12'>");
            builder.Append(tabs);
            builder.Append(contents);
            builder.Append("</div>");
            itmReturn.setProperty("inn_tables", builder.ToString());
        }

        private void AppendAllocationTable(StringBuilder builder, TMap map, TAllocation allocation)
        {
            StringBuilder body = new StringBuilder();

            string remove_icon = " <i class='fa fa-remove' style='color: red;' data-type='category' data-aid='{#allocation_id}' onclick='Remove_Allocation(this)' ></i>";
            string upload_icon = " <i class='fa fa-cloud-upload' style='color: red;' data-type='category' data-aid='{#allocation_id}' onclick='Upload_Click(this)' ></i>";

            if (!map.IsEdit) remove_icon = "";
            if (!map.IsUpload) upload_icon = "";

            for (int i = 0; i < map.Rows; i++)
            {
                //標題
                body.Append("  <tr>");
                for (int j = 0; j < map.Cols; j++)
                {
                    string key = i + "_" + j;
                    if (map.Sites.ContainsKey(key))
                    {
                        TSite site = map.Sites[key];

                        string td_css = " class='text-center btn-primary site-name'";
                        string td_eid = " id='TH_" + site.Id + "'";
                        string td_val = " data-val='" + site.Name + "'";

                        body.Append("<td "
                            + td_css
                            + td_eid
                            + td_val
                            + "><b>" + site.Name + GetGroupCount(map, site, allocation) + "</b>"
                            + "</td>");
                    }
                    else
                    {
                        body.Append("    <td class='text-center'></td>");
                    }
                }
                body.Append("  </tr>");

                //當日對打量級
                body.Append("  <tr>");
                for (int j = 0; j < map.Cols; j++)
                {
                    string key = i + "_" + j;

                    if (map.Sites.ContainsKey(key))
                    {
                        TSite site = map.Sites[key];
                        string td_eid = " id='TD_" + site.Id + "'";
                        body.Append("<td class='text-center'" + td_eid + ">");
                        body.Append(GetGroupSelectCtrl(map, site, allocation));
                        body.Append(GetGroupSpanCtrl(map, site, allocation));
                        body.Append("</td>");
                    }
                    else
                    {
                        body.Append("<td class='text-center'></td>");
                    }
                }
                body.Append("  </tr>");
            }


            builder.AppendLine("<div class='box-body row container-row' >");
            builder.AppendLine("    <div class='box box-fix'>");

            builder.AppendLine("      <div class='box-header with-border'>");
            builder.AppendLine("        <h3 class='box-title'>");
            builder.AppendLine("          <b>" + allocation.ChineseDate + GetRemoveDateCtrl(map, allocation) + "</b>");
            builder.AppendLine("        </h3>");

            if (map.IsEdit)
            {
                //當日對打組別
                for (int i = 0; i < allocation.Categories.Count; i++)
                {
                    var category = allocation.Categories[i];
                    var text = category.InL1 + "-" + category.InL2 + remove_icon.Replace("{#allocation_id}", category.Id);
                    builder.Append("<p class='in_category' data-inl1='" + category.InL1 + "' data-inl2='" + category.InL2 + "' style='display:none'>"
                        + text
                        + "</p>");
                }
            }

            builder.AppendLine("      </div>");

            builder.AppendLine("      <div class='box-body'>");
            builder.AppendLine("        <table class='table table-site' data-toggle='table' style='background-color: #fff;'>");
            builder.Append("              <tbody>");
            builder.Append(body);
            builder.Append("              </tbody>");
            builder.AppendLine("        </table>");
            builder.AppendLine("      </div>");
            builder.AppendLine("    </div>");
            builder.AppendLine("</div>");
        }

        private string GetRemoveDateCtrl(TMap map, TAllocation allocation)
        {
            if (map.IsEdit)
            {
                return " <i class='fa fa-remove' style='color: red'"
                    + " data-did='" + allocation.Key + "'"
                    + " onclick='Remove_Date(this)'></i>";
            }
            else
            {
                return "";
            }
        }

        private string GetGroupSpanCtrl(TMap map, TSite site, TAllocation allocation)
        {
            List<TGroup> list = null;
            if (allocation.SiteGroups.ContainsKey(site.Key))
            {
                list = allocation.SiteGroups[site.Key];
            }

            if (list == null || list.Count == 0)
            {
                return "";
            }

            StringBuilder builder = new StringBuilder();
            foreach (var group in list)
            {
                builder.Append("<p>");
                if (map.IsLinkWeight)
                {
                    builder.Append("<a class='grp_cnt' target='_blank' onclick='GoFightTreePage(this)'"
                        + " data-l1='" + group.in_l1 + "'"
                        + " data-l2='" + group.in_l2 + "'"
                        + " data-l3='" + group.in_l3 + "'"
                        + " data-cnt='" + group.ECnt + "'"
                        + " >" + group.LabelCnt + "</a>");
                }
                else if (map.NoMenu)
                {
                    builder.Append("<a data-pid='" + group.ProgramId + "' onclick='GoFightDraw(this)'>" + group.LabelCnt + "</a>");
                }
                else
                {
                    builder.Append("<a data-pid='" + group.ProgramId + "' onclick='EditTreeNo(this)'>" + group.LabelCnt + "</a>");
                }

                if (map.IsEdit)
                {
                    builder.Append(" <i class='fa fa-remove' style='color: red'"
                        + " data-aid='" + group.Id + "'"
                        + " data-pid='" + group.ProgramId + "'"
                        + " data-sid='" + group.SiteId + "'"
                        + " onclick='Remove_Allocation(this)'></i>");
                }
                else if (map.IsUpload)
                {
                    builder.Append(" <i class='fa fa-cloud-upload' style='color: blue' data-aid='" + group.Id + "' data-pid='" + group.ProgramId + "' onclick='Upload_Click(this)'></i>");
                }

                builder.Append("</p>");
            }
            return builder.ToString();
        }

        private string GetGroupCount(TMap map, TSite site, TAllocation allocation)
        {
            List<TGroup> list = null;
            if (allocation.SiteGroups.ContainsKey(site.Key))
            {
                list = allocation.SiteGroups[site.Key];
            }

            if (list == null || list.Count == 0)
            {
                return "";
            }

            int total = 0;
            foreach (var group in list)
            {
                int cnt = GetIntVal(group.ECnt);
                total += cnt;
            }
            return " (" + total + ")";
        }

        private string GetGroupSelectCtrl(TMap map, TSite site, TAllocation allocation)
        {
            if (map.IsEdit)
            {
                StringBuilder builder = new StringBuilder();
                builder.Append("<select class='form-control ctrl_program' data-sid='" + site.Id + "' data-did='" + allocation.Key + "' onchange='Program_Change(this)'>");
                builder.Append("<option value=''>請選擇</option>");
                builder.Append("</select>");
                return builder.ToString();
            }
            else
            {
                return "";
            }
        }

        /// <summary>
        /// 轉換場地分配資訊
        /// </summary>
        private TMap MapAllocation(TConfig cfg
            , Item itmAllocations
            , Item itmSites
            , bool isEdit = false
            , bool isUpload = false
            , bool linkWeight = false
            , bool noMenu = false)
        {
            int site_count = itmSites.getItemCount();

            Item itmFirstSite = null;

            if (site_count <= 0)
            {
                itmFirstSite = cfg.inn.newItem();
            }
            else
            {
                itmFirstSite = itmSites.getItemByIndex(0);
            }

            TMap map = new TMap
            {
                NoMenu = noMenu,
                IsEdit = isEdit,
                IsUpload = isUpload,
                IsLinkWeight = linkWeight,
                Rows = GetIntVal(itmFirstSite.getProperty("in_rows", "")),
                Cols = GetIntVal(itmFirstSite.getProperty("in_cols", "")),
                Sites = new Dictionary<string, TSite>(),
                Allocations = new Dictionary<string, TAllocation>(),
            };

            for (int i = 0; i < site_count; i++)
            {
                Item itmSite = itmSites.getItemByIndex(i);
                string id = itmSite.getProperty("id", "");
                string in_code = itmSite.getProperty("in_code", "");
                string in_code_en = itmSite.getProperty("in_code_en", "");
                string en_site = "場地 " + in_code_en;
                string in_name = itmSite.getProperty("in_name", "");
                string in_row_index = itmSite.getProperty("in_row_index", "");
                string in_col_index = itmSite.getProperty("in_col_index", "");
                string key = in_row_index + "_" + in_col_index;

                if (!map.Sites.ContainsKey(key))
                {
                    map.Sites.Add(key, new TSite
                    {
                        Key = key,
                        Id = id,
                        Code = in_code,
                        Name = en_site,
                    });
                }
            }

            int allocate_count = itmAllocations.getItemCount();

            for (int i = 0; i < allocate_count; i++)
            {
                Item itmAllocation = itmAllocations.getItemByIndex(i);
                string id = itmAllocation.getProperty("id", "");

                string in_type = itmAllocation.getProperty("in_type", "");
                string in_date_key = itmAllocation.getProperty("in_date_key", "");
                string in_date = GetDateTimeVal(itmAllocation.getProperty("in_date", ""), "yyy-MM-dd", 8);
                string chinese_date = GetDateTimeVal(itmAllocation.getProperty("in_date", ""), "chinese", 8);

                //分類用
                string in_l1 = itmAllocation.getProperty("in_l1", "");
                string in_l2 = itmAllocation.getProperty("in_l2", "");

                //場地分配
                string sid = itmAllocation.getProperty("sid", "");
                string in_row_index = itmAllocation.getProperty("in_row_index", "");
                string in_col_index = itmAllocation.getProperty("in_col_index", "");
                string skey = in_row_index + "_" + in_col_index;

                //組別分配
                string pid = itmAllocation.getProperty("pid", "");
                string pl1 = itmAllocation.getProperty("pl1", "");
                string pl2 = itmAllocation.getProperty("pl2", "");
                string pl3 = itmAllocation.getProperty("pl3", "");
                string pname = itmAllocation.getProperty("pname", "");
                string pname2 = itmAllocation.getProperty("pname2", "");
                string pname3 = itmAllocation.getProperty("pname3", "");
                string pcnt = itmAllocation.getProperty("pcnt", "");
                string ecnt = itmAllocation.getProperty("ecnt", "");

                TAllocation entity = null;
                if (map.Allocations.ContainsKey(in_date_key))
                {
                    entity = map.Allocations[in_date_key];
                }
                else
                {
                    entity = new TAllocation
                    {
                        Key = in_date_key,
                        DisplayDate = in_date,
                        ChineseDate = chinese_date,
                        Categories = new List<TCategory>(),
                        SiteGroups = new Dictionary<string, List<TGroup>>(),
                    };
                    map.Allocations.Add(in_date_key, entity);
                }

                if (in_type == "category")
                {
                    entity.Categories.Add(new TCategory
                    {
                        Id = id,
                        InL1 = in_l1,
                        InL2 = in_l2,
                        Label = in_l1 + "-" + in_l2,
                    }); ;
                }
                else
                {
                    List<TGroup> groups = null;
                    if (entity.SiteGroups.ContainsKey(skey))
                    {
                        groups = entity.SiteGroups[skey];
                    }
                    else
                    {
                        groups = new List<TGroup>();
                        entity.SiteGroups.Add(skey, groups);
                    }

                    groups.Add(new TGroup
                    {
                        Id = id,
                        SiteId = sid,
                        ProgramId = pid,
                        DateKey = in_date_key,
                        Label = pname2,
                        PCnt = pcnt,
                        ECnt = ecnt,
                        LabelCnt = pname2 + " (" + ecnt + ")",
                        in_l1 = pl1,
                        in_l2 = pl2,
                        in_l3 = pl3,
                    });
                }
            }

            return map;
        }

        /// <summary>
        /// 場地分配資料模型
        /// </summary>
        private class TMap
        {
            public bool NoMenu { get; set; }

            /// <summary>
            /// 是否為編輯狀態
            /// </summary>
            public bool IsEdit { get; set; }

            /// <summary>
            /// 是否為上傳圖片狀態
            /// </summary>
            public bool IsUpload { get; set; }

            /// <summary>
            /// 是否連結至組別過磅
            /// </summary>
            public bool IsLinkWeight { get; set; }

            /// <summary>
            /// 列數
            /// </summary>
            public int Rows { get; set; }

            /// <summary>
            /// 欄數
            /// </summary>
            public int Cols { get; set; }

            /// <summary>
            /// 場地資料
            /// </summary>
            public Dictionary<string, TSite> Sites { get; set; }

            /// <summary>
            /// 日期資料
            /// </summary>
            public Dictionary<string, TAllocation> Allocations { get; set; }
        }

        private class TSite
        {
            /// <summary>
            /// 鍵
            /// </summary>
            public string Key { get; set; }

            /// <summary>
            /// 系統編號
            /// </summary>
            public string Id { get; set; }

            /// <summary>
            /// 場地代號
            /// </summary>
            public string Code { get; set; }

            /// <summary>
            /// 場地名稱
            /// </summary>
            public string Name { get; set; }

        }

        private class TAllocation
        {
            /// <summary>
            /// 鍵
            /// </summary>
            public string Key { get; set; }

            /// <summary>
            /// 日期呈現 ex: 2020-11-16
            /// </summary>
            public string DisplayDate { get; set; }

            /// <summary>
            /// 日期呈現 ex: 109年11月20日(星期五)
            /// </summary>
            public string ChineseDate { get; set; }

            /// <summary>
            /// 當日比賽組別
            /// </summary>
            public List<TCategory> Categories { get; set; }

            /// <summary>
            /// 當日比賽場地組別
            /// </summary>
            public Dictionary<string, List<TGroup>> SiteGroups { get; set; }
        }

        private class TCategory
        {
            public string Id { get; set; }
            public string InL1 { get; set; }
            public string InL2 { get; set; }
            public string Label { get; set; }
        }

        private class TGroup
        {
            /// <summary>
            /// Allocation Id
            /// </summary>
            public string Id { get; set; }

            /// <summary>
            /// Site Id
            /// </summary>
            public string SiteId { get; set; }

            /// <summary>
            /// Program Id
            /// </summary>
            public string ProgramId { get; set; }

            /// <summary>
            /// Date Key
            /// </summary>
            public string DateKey { get; set; }

            /// <summary>
            /// 組別-量級
            /// </summary>
            public string Label { get; set; }

            /// <summary>
            /// 組別-人數
            /// </summary>
            public string PCnt { get; set; }

            /// <summary>
            /// 組別-場次數
            /// </summary>
            public string ECnt { get; set; }

            /// <summary>
            /// 組別-量級
            /// </summary>
            public string LabelCnt { get; set; }

            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }
        }

        //附加賽事資訊
        private void AppendMeeting(Item itmMeeting, Item itmReturn)
        {
            itmReturn.setProperty("in_title", itmMeeting.getProperty("in_title", ""));
            itmReturn.setProperty("in_battle_type", itmMeeting.getProperty("in_battle_type", ""));
            itmReturn.setProperty("in_surface_code", itmMeeting.getProperty("in_surface_code", ""));
            itmReturn.setProperty("banner_file", itmMeeting.getProperty("in_banner_photo", ""));
            itmReturn.setProperty("banner_file2", itmMeeting.getProperty("in_banner_photo2", ""));
        }

        //取得賽事資訊
        private Item GetMeeting(TConfig cfg)
        {
            string aml = @"
                <AML>
                    <Item type='In_Meeting' action='get' id='{#meeting_id}' select='in_title,in_battle_type,in_surface_code,in_banner_photo,in_banner_photo2'>
                    </Item>
                </AML>
            ".Replace("{#meeting_id}", cfg.meeting_id);

            return cfg.inn.applyAML(aml);
        }

        //取得分配場地資訊
        private Item GetMeetingAllocations(TConfig cfg)
        {
            string sql = @"
                SELECT
                    t1.*
			        , t2.id AS 'sid'
			        , t2.in_row_index
			        , t2.in_col_index
			        , t3.id    AS 'pid'
			        , t3.in_l1 AS 'pl1'
			        , t3.in_l2 AS 'pl2'
			        , t3.in_l3 AS 'pl3'
			        , t3.in_name AS 'pname'
			        , t3.in_name2 AS 'pname2'
			        , t3.in_name3 AS 'pname3'
			        , t3.in_team_count AS 'pcnt'
			        , t3.in_event_count AS 'ecnt'
                FROM
                    IN_MEETING_ALLOCATION t1 WITH(NOLOCK)
		        LEFT OUTER JOIN
			        IN_MEETING_SITE t2 WITH(NOLOCK)
			        ON t2.in_meeting = t1.in_meeting
			        AND t2.id = t1.in_site
		        LEFT OUTER JOIN
			        IN_MEETING_PROGRAM t3 WITH(NOLOCK)
			        ON t3.in_meeting = t1.in_meeting
			        AND t3.id = t1.in_program
                WHERE
                    t1.in_meeting = '{#meeting_id}'
                ORDER BY
                    t1.in_date
                    , t1.created_on
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            return cfg.inn.applySQL(sql);
        }

        private Dictionary<string, Item> GetMeetingSiteMap(TConfig cfg)
        {
            var map = new Dictionary<string, Item>();
            var items = GetMeetingSites(cfg);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var code = item.getProperty("in_code", "");
                if (map.ContainsKey(code)) continue;
                map.Add(code, item);
            }
            return map;
        }

        //取得賽事場地資訊
        private Item GetMeetingSites(TConfig cfg)
        {
            string sql = @"SELECT * FROM IN_MEETING_SITE WITH(NOLOCK) WHERE in_meeting = '" + cfg.meeting_id + "'";
            return cfg.inn.applySQL(sql);
        }

        private string GetJsonValue(Newtonsoft.Json.Linq.JObject obj, string key)
        {
            Newtonsoft.Json.Linq.JToken value;
            if (obj.TryGetValue(key, out value))
            {
                return value.ToString();
            }
            return "";
        }

        private int GetIntVal(string value, int def = 0)
        {
            int result = def;
            int.TryParse(value, out result);
            return result;
        }

        private DateTime GetDtmeVal(string value)
        {
            DateTime result = DateTime.MinValue;
            DateTime.TryParse(value, out result);
            return result;
        }

        private string GetDateTimeVal(string value, string format, int add_hours = 0)
        {
            DateTime result = DateTime.MinValue;
            DateTime.TryParse(value, out result);
            if (result != DateTime.MinValue)
            {
                if (format == "chinese")
                {
                    return ChineseDateTime(result.AddHours(add_hours));
                }
                else
                {
                    return result.AddHours(add_hours).ToString(format);
                }
            }
            else
            {
                return "";
            }
        }

        /// <summary>
        /// 轉換為民國年月日(星期)
        /// </summary>
        private string ChineseDateTime(DateTime value)
        {
            var y = value.Year - 1911;
            var m = value.Month.ToString().PadLeft(2, '0');
            var d = value.Day.ToString().PadLeft(2, '0');
            var w = "";

            switch (value.DayOfWeek)
            {
                case DayOfWeek.Monday:
                    w = "星期一";
                    break;
                case DayOfWeek.Tuesday:
                    w = "星期二";
                    break;
                case DayOfWeek.Wednesday:
                    w = "星期三";
                    break;
                case DayOfWeek.Thursday:
                    w = "星期四";
                    break;
                case DayOfWeek.Friday:
                    w = "星期五";
                    break;
                case DayOfWeek.Saturday:
                    w = "星期六";
                    break;
                case DayOfWeek.Sunday:
                    w = "星期日";
                    break;
                default:
                    break;
            }

            return y + "年" + m + "月" + d + "日" + "（" + w + "）";
        }
    }
}