﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Wrestling.Common
{
    public class In_Meeting_Medal : Item
    {
        public In_Meeting_Medal(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 
                    獎牌戰
                輸出: 
                    docx
                重點: 
                    - 需 Xceed.Document.NET.dll
                    - 需 Xceed.Words.NET.dll
                    - 請於 method-config.xml 引用 DLL
                日期: 
                    2024-04-03: 角力版本 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]In_Meeting_Medal";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                meeting_id = itmR.getProperty("meeting_id", ""),
                in_date = itmR.getProperty("in_date", ""),
                in_item = itmR.getProperty("in_item", ""),
                scene = itmR.getProperty("scene", ""),
                export_type = itmR.getProperty("export_type", ""),
                need_twofight = itmR.getProperty("need_twofight", ""),
                default_site_nav = "",
            };

            string sql = "SELECT in_title,in_language, in_battle_type FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'";
            cfg.itmMeeting = cfg.inn.applySQL(sql);
            cfg.in_title = cfg.itmMeeting.getProperty("in_title", "");
            cfg.in_language = cfg.itmMeeting.getProperty("in_language", "");

            switch (cfg.in_item)
            {
                default:
                    cfg.AppendMatchAction = AppendMatch;
                    cfg.GetF1OrgDisplayFunc = GetOrgDisplay;
                    cfg.GetF2OrgDisplayFunc = GetOrgDisplay;
                    break;
            }

            if (cfg.scene == "")
            {
                Page(cfg, itmR);
                if (cfg.in_item != "")
                {
                    Table(cfg, itmR);
                }
            }
            else if (cfg.scene == "table" && cfg.in_item != "")
            {
                Table(cfg, itmR);
            }
            else if (cfg.scene == "modal")
            {
                Modal(cfg, itmR);
            }
            else if (cfg.scene == "edit")
            {
                EditStaff(cfg, itmR);
            }
            else if (cfg.scene == "edit_site")
            {
                EditSite(cfg, itmR);
            }
            else if (cfg.scene == "edit_one_staff")
            {
                EditOneStaff(cfg, itmR);
            }
            else if (cfg.scene == "pdf")
            {

                //下載獎牌戰
                if (cfg.in_language == "en")
                {
                    //國際板
                    itmR = itmR.apply("In_MeetingReport_IJF_contest");
                }
                else
                {
                    DownloadPDF(cfg, itmR);
                }

            }
            return itmR;
        }

        #region PDF

        private void DownloadPDF(TConfig cfg, Item itmReturn)
        {
            string sql = "SELECT in_title,in_language, in_battle_type FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'";
            cfg.itmMeeting = cfg.inn.applySQL(sql);
            cfg.in_title = cfg.itmMeeting.getProperty("in_title", "");
            cfg.in_language = cfg.itmMeeting.getProperty("in_language", "");

            TSheet sheetCfg = new TSheet
            {
                WsRow = 4,
                WsCol = 0,
                Heads = new string[] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" },
                Font = "標楷體",
                SheetCount = 1,
                Sheets = new List<string>()
            };

            List<Item> sites = GetSiteMenu(cfg);

            List<TTab> tabs = new List<TTab>();

            for (int i = 0; i < sites.Count; i++)
            {
                Item site = sites[i];
                string no = (i + 1).ToString();
                string id = "site_tab_" + no;
                string site_id = site.getProperty("id", "");
                string site_name = site.getProperty("in_name", "");
                string active = i == 0 ? "active" : "";

                tabs.Add(new TTab
                {
                    id = id,
                    active = active,
                    title = site_name,
                    in_site = site_id,
                    GetListFunc = GetSiteEvents,
                });
            }

            foreach (var tab in tabs)
            {
                FixTab(cfg, tab);
            }

            int total_count = 0;
            foreach (var tab in tabs)
            {
                FixTab(cfg, tab);
                total_count += tab.count;
            }

            if (total_count == 0)
            {
                throw new Exception("無場次資料");
            }

            cfg.day = GetDayInfo(cfg);
            cfg.xls = GetSpotCheckPDFInfo(cfg);

            Spire.Xls.Workbook workbook = new Spire.Xls.Workbook();
            workbook.LoadFromFile(cfg.xls.source);
            Spire.Xls.Worksheet sheetTemplate = workbook.Worksheets[0];

            foreach (var tab in tabs)
            {
                if (tab.count > 0)
                {
                    AppendSheets(workbook, sheetTemplate, cfg, sheetCfg, tab);
                }
            }

            //移除樣板 sheet
            sheetTemplate.Remove();
            workbook.Worksheets[0].Activate();

            switch (cfg.export_type)
            {
                case "PDF":
                    workbook.SaveToFile(cfg.xls.file.Replace("xlsx", "pdf"), Spire.Xls.FileFormat.PDF);
                    itmReturn.setProperty("xls_name", cfg.xls.url.Replace("xlsx", "pdf"));
                    break;

                default:
                    workbook.SaveToFile(cfg.xls.file, Spire.Xls.ExcelVersion.Version2010);
                    itmReturn.setProperty("xls_name", cfg.xls.url);
                    break;
            }
        }

        private void AppendSheets(
            Spire.Xls.Workbook workbook
            , Spire.Xls.Worksheet sheetTemplate
            , TConfig cfg
            , TSheet sheetCfg
            , TTab tab)
        {
            Spire.Xls.Worksheet sheet = workbook.CreateEmptySheet();
            sheet.CopyFrom(sheetTemplate);
            sheet.Name = tab.title;

            //sheet.Range["A1"].Style.Font.FontName = sheetCfg.Font;

            sheet.Range["A1"].Text = cfg.in_title + " Day " + cfg.day.day_no;
            sheet.Range["A2"].Text = tab.title + " (" + GetEventTitle(cfg.in_item) + ")";
            sheet.Range["A3"].Text = System.DateTime.Now.ToString("yyyy.MM.dd.");

            int wsRow = sheetCfg.WsRow;
            int wsCol = sheetCfg.WsCol;

            int count = tab.items.Count;
            for (int i = 0; i < count; i++)
            {
                var match = tab.items[i];

                SetMText(sheet, sheetCfg, "A" + wsRow + ":" + "D" + wsRow, match.program_name + GetEvtNote(cfg, match), e: HAlign.Left);
                SetCText(sheet, sheetCfg, "E" + wsRow + ":" + "E" + wsRow, "主審", e: HAlign.Center);
                SetCText(sheet, sheetCfg, "F" + wsRow + ":" + "F" + wsRow, "副審");
                SetMText(sheet, sheetCfg, "G" + wsRow + ":" + "G" + wsRow, "主任裁判");
                wsRow++;

                SetMText(sheet, sheetCfg, "A" + wsRow + ":" + "A" + (wsRow + 1), match.in_tree_no.ToString());
                SetCText(sheet, sheetCfg, "B" + wsRow + ":" + "B" + wsRow, match.Foot1.color);
                SetCText(sheet, sheetCfg, "C" + wsRow + ":" + "C" + wsRow, match.Foot1.org_display);
                SetCText(sheet, sheetCfg, "D" + wsRow + ":" + "D" + wsRow, match.Foot1.in_name + match.Foot1.in_weight_message);
                SetMText(sheet, sheetCfg, "E" + wsRow + ":" + "E" + (wsRow + 1), match.in_staff_1);
                SetMText(sheet, sheetCfg, "F" + wsRow + ":" + "F" + (wsRow + 1), match.in_staff_2);
                SetMText(sheet, sheetCfg, "G" + wsRow + ":" + "G" + (wsRow + 1), match.in_staff_3);
                //SetMText(sheet, sheetCfg, "H" + wsRow + ":" + "H" + (wsRow + 1), match.in_staff_4);
                sheet.SetRowHeight(wsRow, 21.6);
                wsRow++;

                SetCText(sheet, sheetCfg, "B" + wsRow + ":" + "B" + wsRow, match.Foot2.color);
                SetCText(sheet, sheetCfg, "C" + wsRow + ":" + "C" + wsRow, match.Foot2.org_display);
                SetCText(sheet, sheetCfg, "D" + wsRow + ":" + "D" + wsRow, match.Foot2.in_name + match.Foot2.in_weight_message);
                sheet.SetRowHeight(wsRow, 21.6);
                wsRow++;
            }

            string ps = "A" + sheetCfg.WsRow;
            string pe = "G" + (wsRow - 1);
            SetRangeBorder(sheet, ps + ":" + pe);
        }

        //設定格線
        private void SetRangeBorder(Spire.Xls.Worksheet sheet, string pos)
        {
            var range = sheet.Range[pos];
            range.BorderInside(Spire.Xls.LineStyleType.Thin, System.Drawing.Color.Black);
            range.BorderAround(Spire.Xls.LineStyleType.Thin, System.Drawing.Color.Black);
        }

        private enum HAlign
        {
            Left = 1,
            Center = 2,
            Right = 3
        }

        private void SetMText(Spire.Xls.Worksheet sheet, TSheet sheetCfg, string pos, string text, HAlign e = HAlign.Center)
        {
            Spire.Xls.CellRange range = sheet.Range[pos];
            range.Merge();
            range.Text = text;
            range.Style.Font.FontName = sheetCfg.Font;

            switch (e)
            {
                case HAlign.Left:
                    range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Left;
                    break;

                case HAlign.Right:
                    range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Right;
                    break;

                case HAlign.Center:
                    range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                    break;
            }

            //range.Style.Borders[Spire.Xls.BordersLineType.EdgeTop].LineStyle = Spire.Xls.LineStyleType.Thin;
            //range.Style.Borders[Spire.Xls.BordersLineType.EdgeRight].LineStyle = Spire.Xls.LineStyleType.Thin;
            //range.Style.Borders[Spire.Xls.BordersLineType.EdgeBottom].LineStyle = Spire.Xls.LineStyleType.Thin;
            //range.Style.Borders[Spire.Xls.BordersLineType.EdgeLeft].LineStyle = Spire.Xls.LineStyleType.Thin;
        }

        private void SetCText(Spire.Xls.Worksheet sheet, TSheet sheetCfg, string pos, string text, HAlign e = HAlign.Center)
        {
            Spire.Xls.CellRange range = sheet.Range[pos];
            range.Text = text;
            range.Style.Font.FontName = sheetCfg.Font;
            switch (e)
            {
                case HAlign.Left:
                    range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Left;
                    break;

                case HAlign.Right:
                    range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Right;
                    break;

                case HAlign.Center:
                    range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                    break;
            }

            range.Style.ShrinkToFit = true;
            //range.Style.Borders[Spire.Xls.BordersLineType.EdgeTop].LineStyle = Spire.Xls.LineStyleType.Thin;
            //range.Style.Borders[Spire.Xls.BordersLineType.EdgeRight].LineStyle = Spire.Xls.LineStyleType.Thin;
            //range.Style.Borders[Spire.Xls.BordersLineType.EdgeBottom].LineStyle = Spire.Xls.LineStyleType.Thin;
            //range.Style.Borders[Spire.Xls.BordersLineType.EdgeLeft].LineStyle = Spire.Xls.LineStyleType.Thin;
        }

        private TXls GetSpotCheckPDFInfo(TConfig cfg)
        {
            Item itmPath = GetExcelPath(cfg, "medal_list_path");

            string export_path = itmPath.getProperty("export_path", "").TrimEnd('\\');
            string ext_name = ".xlsx";

            string xls_name = cfg.in_title + " - " + cfg.in_date + "_" + DateTime.Now.ToString("MMdd_HHmmss");

            return new TXls
            {
                file = export_path + "\\" + xls_name + ext_name,
                url = xls_name + ext_name,
                source = itmPath.getProperty("template_path", ""),
            };
        }

        //設定物件與資料列
        private void SetItemCell(Spire.Xls.Worksheet sheet, TConfig cfg, TSheet sheetCfg, int wsRow, int wsCol, Item item, List<TField> fields)
        {
            for (int i = 0; i < fields.Count; i++)
            {
                var field = fields[i];
                var value = "";
                var position = sheetCfg.Heads[wsCol + i] + wsRow;

                if (field.getVal != null)
                {
                    value = field.getVal(cfg, field, item);
                }
                else if (!string.IsNullOrWhiteSpace(field.property))
                {
                    value = item.getProperty(field.property, "");
                }

                Spire.Xls.CellRange range = sheet.Range[position];
                SetBodyCell(range, sheetCfg, value, field.format);
            }
        }

        //設定資料列
        private void SetBodyCell(Spire.Xls.CellRange range, TSheet cfg, string value = "", string format = "")
        {
            switch (format)
            {
                case "center":
                    range.Text = value;
                    range.Style.Font.FontName = cfg.Font;
                    break;

                case "yyyy/MM/dd":
                case "yyyy/MM/dd HH:mm":
                    range.Text = GetDateTimeValue(value, format);
                    range.Style.Font.FontName = cfg.Font;
                    break;

                case "$ #,##0":
                    range.Text = value;
                    range.Style.Font.FontName = cfg.Font;
                    break;

                case "text":
                    range.Text = value;
                    range.Style.Font.FontName = cfg.Font;
                    break;

                default:
                    range.Text = value;
                    range.Style.Font.FontName = cfg.Font;
                    break;
            }

            //range.Style.Borders[Spire.Xls.BordersLineType.EdgeTop].LineStyle = Spire.Xls.LineStyleType.Thin;
            //range.Style.Borders[Spire.Xls.BordersLineType.EdgeRight].LineStyle = Spire.Xls.LineStyleType.Thin;
            //range.Style.Borders[Spire.Xls.BordersLineType.EdgeBottom].LineStyle = Spire.Xls.LineStyleType.Thin;
            //range.Style.Borders[Spire.Xls.BordersLineType.EdgeLeft].LineStyle = Spire.Xls.LineStyleType.Thin;
        }

        private Item GetExcelPath(TConfig cfg, string in_name)
        {
            Item itmResult = cfg.inn.newItem();

            //Excel 匯出路徑 (from 原創.設定參數)
            string sql = @"
                SELECT 
                    t2.in_name AS 'variable'
                    , t1.in_name
                    , t1.in_value 
                FROM 
                    In_Variable_Detail t1 WITH(NOLOCK)
                INNER JOIN 
                    In_Variable t2 WITH(NOLOCK) 
                    ON t2.id = t1.source_id 
                WHERE 
                    t2.in_name = N'meeting_excel'  
                    AND t1.in_name IN (N'export_path', N'{#in_name}')
            ";

            sql = sql.Replace("{#in_name}", in_name);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item items = cfg.inn.applySQL(sql);

            if (items.isError())
            {
                throw new Exception("未設定匯出路徑 _# " + in_name);
            }

            for (int i = 0; i < items.getItemCount(); i++)
            {
                Item item = items.getItemByIndex(i);
                string xls_name = item.getProperty("in_name", "");
                string xls_value = item.getProperty("in_value", "");

                if (xls_name == "export_path")
                {
                    itmResult.setProperty("export_path", xls_value);
                }
                else
                {
                    itmResult.setProperty("template_path", xls_value);
                }
            }

            return itmResult;
        }
        #endregion PDF

        private void EditOneStaff(TConfig cfg, Item itmReturn)
        {
            string meeting_id = itmReturn.getProperty("meeting_id", "");
            string program_id = itmReturn.getProperty("program_id", "");
            string tree_id = itmReturn.getProperty("tree_id", "");
            string property = itmReturn.getProperty("property", "");
            string staff = itmReturn.getProperty("staff", "");

            if (program_id == "" || tree_id == "" || property == "")
            {
                throw new Exception("參數錯誤");
            }

            Item itmEvent = cfg.inn.applySQL("SELECT id FROM IN_MEETING_PEVENT WITH(NOLOCK) WHERE source_id = '" + program_id + "' AND in_tree_id = '" + tree_id + "'");
            if (itmEvent.isError() || itmEvent.getResult() == "")
            {
                throw new Exception("查無場次資料");
            }

            string event_id = itmEvent.getProperty("id", "");

            string sql = "UPDATE IN_MEETING_PEVENT SET {#property} = N'{#staff}' WHERE id = '{#event_id}'";

            sql = sql.Replace("{#event_id}", event_id)
                .Replace("{#property}", property)
                .Replace("{#staff}", staff);

            Item itmResult = cfg.inn.applySQL(sql);

            if (itmResult.isError())
            {
                throw new Exception("工作人員變更失敗");
            }
        }

        private void EditSite(TConfig cfg, Item itmReturn)
        {
            string meeting_id = itmReturn.getProperty("meeting_id", "");
            string program_id = itmReturn.getProperty("program_id", "");
            string tree_id = itmReturn.getProperty("tree_id", "");
            string site_id = itmReturn.getProperty("site_id", "");
            if (program_id == "" || tree_id == "" || site_id == "")
            {
                throw new Exception("參數錯誤");
            }

            Item itmSite = cfg.inn.applySQL("SELECT id, in_code, in_code_en FROM IN_MEETING_SITE WITH(NOLOCK) WHERE id = '" + site_id + "'");
            if (itmSite.isError() || itmSite.getResult() == "")
            {
                throw new Exception("查無場地資料");
            }

            Item itmEvent = cfg.inn.applySQL("SELECT id, in_type, in_tree_no FROM IN_MEETING_PEVENT WITH(NOLOCK) WHERE source_id = '" + program_id + "' AND in_tree_id = '" + tree_id + "'");
            if (itmEvent.isError() || itmEvent.getResult() == "")
            {
                throw new Exception("查無場次資料");
            }

            string in_code = itmSite.getProperty("in_code", "");
            string in_code_en = itmSite.getProperty("in_code_en", "");

            string event_id = itmEvent.getProperty("id", "");
            string in_type = itmEvent.getProperty("in_type", "");

            string sql = "UPDATE IN_MEETING_PEVENT SET in_site_code = '{#in_code}', in_site = '{#site_id}', in_show_site = '{#in_code}' WHERE id = '{#event_id}'";

            sql = sql.Replace("{#event_id}", event_id)
                .Replace("{#in_code}", in_code)
                .Replace("{#site_id}", site_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item itmResult = cfg.inn.applySQL(sql);

            if (itmResult.isError())
            {
                throw new Exception("場地變更失敗");
            }

            if (in_type == "p")
            {
                //變更子場地
                sql = @"
                    UPDATE t1 SET
                        t1.in_site = t2.in_site
                        , t1.in_tree_no = t2.in_tree_no * 100 + t1.in_sub_id
                    FROM 
                        IN_MEETING_PEVENT t1 WITH(NOLOCK) 
                    INNER JOIN
                        IN_MEETING_PEVENT t2 WITH(NOLOCK) 
                        ON t2.id = t1.in_parent
                    WHERE
                        t1.in_parent = '{#event_id}'
                ";

                sql = sql.Replace("{#event_id}", event_id);

                //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

                itmResult = cfg.inn.applySQL(sql);
            }
        }

        private void EditStaff(TConfig cfg, Item itmReturn)
        {
            string event_id = itmReturn.getProperty("event_id", "");
            string in_staff_1 = itmReturn.getProperty("in_staff_1", "");
            string in_staff_2 = itmReturn.getProperty("in_staff_2", "");
            string in_staff_3 = itmReturn.getProperty("in_staff_3", "");
            string in_staff_4 = itmReturn.getProperty("in_staff_4", "");

            string sql = "UPDATE IN_MEETING_PEVENT SET"
                + "  in_staff_1 = N'" + in_staff_1 + "'"
                + ", in_staff_2 = N'" + in_staff_2 + "'"
                + ", in_staff_3 = N'" + in_staff_3 + "'"
                + ", in_staff_4 = N'" + in_staff_4 + "'"
                + " WHERE id = '" + event_id + "'";

            Item itmSQL = cfg.inn.applySQL(sql);

            if (itmSQL.isError())
            {
                throw new Exception("儲存失敗");
            }

            ResetDetail(cfg, itmReturn);

            ResetSubSite(cfg, event_id);
        }

        private void ResetDetail(TConfig cfg, Item itmReturn)
        {
            string event_id = itmReturn.getProperty("event_id", "");

            string player1_muid = itmReturn.getProperty("player1_muid", "");
            string player2_muid = itmReturn.getProperty("player2_muid", "");

            Item itmFoot1 = GetMUser(cfg, player1_muid);
            itmFoot1.setProperty("event_id", event_id);
            itmFoot1.setProperty("in_sign_foot", "1");

            Item itmFoot2 = GetMUser(cfg, player2_muid);
            itmFoot2.setProperty("event_id", event_id);
            itmFoot2.setProperty("in_sign_foot", "2");

            itmFoot1.setProperty("in_target_no", itmFoot2.getProperty("in_sign_no", ""));
            itmFoot2.setProperty("in_target_no", itmFoot1.getProperty("in_sign_no", ""));

            UpdDetail(cfg, itmFoot1);
            UpdDetail(cfg, itmFoot2);
        }

        private void UpdDetail(TConfig cfg, Item item)
        {
            string event_id = item.getProperty("event_id", "");
            string in_sign_foot = item.getProperty("in_sign_foot", "");
            string in_short_org = item.getProperty("in_short_org", "");
            string in_team = item.getProperty("in_team", "");
            string in_name = item.getProperty("in_name", "");
            string in_sno = item.getProperty("in_sno", "");

            string in_sign_no = item.getProperty("in_sign_no", "");
            if (in_sign_no == "0") in_sign_no = "";

            string in_target_no = item.getProperty("in_target_no", "");
            if (in_target_no == "0") in_target_no = "";


            string sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET"
                + " in_sign_no = '" + in_sign_no + "'"
                + ", in_target_no = '" + in_target_no + "'"
                + ", in_player_org = N'" + in_short_org + "'"
                + ", in_player_team = N'" + in_team + "'"
                + ", in_player_name = N'" + in_name + "'"
                + ", in_player_sno = N'" + in_sno + "'"
                + " WHERE source_id = '" + event_id + "'"
                + " AND in_sign_foot = '" + in_sign_foot + "'";

            Item itmUpdResult = cfg.inn.applySQL(sql);

            if (itmUpdResult.isError())
            {
                throw new Exception("執行失敗");
            }
        }

        private Item GetMUser(TConfig cfg, string muid)
        {
            Item items = GetMUsers(cfg, "", "", muid);
            if (items.isError() || items.getResult() == "")
            {
                return cfg.inn.newItem();
            }
            else
            {
                return items.getItemByIndex(0);
            }
        }

        //子場次全部重編
        private void ResetSubSite(TConfig cfg, string event_id)
        {
            string sql = "";
            Item itmSQL = null;

            sql = @"
                UPDATE t1 SET
                    t1.in_site = t2.in_site
                    , t1.in_tree_no = t2.in_tree_no * 100 + t1.in_sub_id
                FROM 
                    IN_MEETING_PEVENT t1 WITH(NOLOCK) 
                INNER JOIN
                    IN_MEETING_PEVENT t2 WITH(NOLOCK) 
                    ON t2.id = t1.in_parent
                WHERE
                    t1.in_parent = '{#event_id}'
            ";

            sql = sql.Replace("{#event_id}", event_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            itmSQL = cfg.inn.applySQL(sql);
        }

        private void Modal(TConfig cfg, Item itmReturn)
        {
            string event_id = itmReturn.getProperty("event_id", "");
            Item itmEvent = cfg.inn.applySQL("SELECT * FROM IN_MEETING_PEVENT WITH(NOLOCK) WHERE id = '" + event_id + "'");

            cfg.meeting_id = itmEvent.getProperty("in_meeting", "");

            //場地選單
            var sites = GetSiteMenu(cfg);
            AppendItems(cfg, sites, "modal_site", "id", "in_code_en", "", "", itmReturn);

            var staffs = GetStaffMenu(cfg);
            AppendItems(cfg, staffs, "modal_staff", "value", "label", "", "job", itmReturn);

            //附加資料
            List<TMatch> list = GetEvent(cfg, event_id);
            if (list == null || list.Count == 0) return;

            var match = list.FirstOrDefault();
            var item = match.Value;
            var player1 = match.Foot1.Value;
            var player2 = match.Foot2.Value;

            itmReturn.setProperty("program_name", item.getProperty("program_name", ""));
            itmReturn.setProperty("in_date_key", item.getProperty("in_date_key", ""));
            itmReturn.setProperty("site_id", item.getProperty("site_id", ""));
            itmReturn.setProperty("in_tree_id", item.getProperty("in_tree_id", ""));
            itmReturn.setProperty("in_tree_no", item.getProperty("in_tree_no", ""));
            itmReturn.setProperty("in_staff_1", item.getProperty("in_staff_1", ""));
            itmReturn.setProperty("in_staff_2", item.getProperty("in_staff_2", ""));
            itmReturn.setProperty("in_staff_3", item.getProperty("in_staff_3", ""));
            itmReturn.setProperty("in_staff_4", item.getProperty("in_staff_4", ""));

            itmReturn.setProperty("site_id", match.Value.getProperty("site_id", ""));

            var meeting_id = item.getProperty("meeting_id", "");
            var program_id = item.getProperty("program_id", "");
            var itmMUsers = GetMUsers(cfg, meeting_id, program_id, "");

            string p1 = player1.getProperty("map_short_org", "") + "-" + player1.getProperty("in_name", "");
            string p2 = player2.getProperty("map_short_org", "") + "-" + player2.getProperty("in_name", "");

            itmReturn.setProperty("player_menu1", GetMUserMenu(itmMUsers, "player_menu1", p1));
            itmReturn.setProperty("player_menu2", GetMUserMenu(itmMUsers, "player_menu2", p2));
        }

        private string GetMUserMenu(Item items, string id, string player)
        {
            StringBuilder builder = new StringBuilder();

            builder.Append("<select id='" + id + "' class='form-control' data-player='" + player + "'>");
            builder.Append("<option value='' selected>請選擇</option>");
            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string muid = item.getProperty("muid", "");
                string in_short_org = item.getProperty("in_short_org", "");
                string in_name = item.getProperty("in_name", "");

                string label = in_short_org + "-" + in_name;
                string value = label;

                builder.Append("<option data-muid='" + muid + "'"
                        + " value='" + value + "' >" + label + "</option>");
            }

            builder.Append("</select>");

            return builder.ToString();
        }

        private Item GetMUsers(TConfig cfg, string meeting_id, string program_id, string muid)
        {
            string condition = muid == ""
                ? "t1.source_id = '" + meeting_id + "' AND t2.id = '" + program_id + "'"
                : "t1.id = '" + muid + "'";

            string sql = @"
                SELECT 
	                t1.id AS 'muid'
	                , t1.in_current_org
	                , t1.in_short_org
	                , t1.in_sno
	                , t1.in_name
	                , t3.in_judo_no
	                , t3.in_sign_no
                FROM
	                IN_MEETING_USER t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PROGRAM t2 WITH(NOLOCK)
	                ON t2.in_meeting = t1.source_id 
					AND t2.in_l1 = t1.in_l1
	                AND t2.in_l2 = t1.in_l2
	                AND t2.in_l3 = t1.in_l3
                LEFT OUTER JOIN
	                VU_MEETING_PTEAM t3
	                ON t3.source_id = t2.id
	                AND t3.in_index = t1.in_index
	                AND t3.in_creator_sno = t1.in_creator_sno
                WHERE
	                {#condition}
                ORDER BY
	                t1.in_short_org
	                , t1.in_team
	                , t1.in_sno
            ";

            sql = sql.Replace("{#condition}", condition);

            return cfg.inn.applySQL(sql);
        }

        #region Table

        private void Table(TConfig cfg, Item itmReturn)
        {
            List<Item> sites = GetSiteMenu(cfg);
            cfg.lstSites = sites;
            cfg.lstStaff = GetStaffMenu2(cfg);

            List<TTab> tabs = new List<TTab>();

            for (int i = 0; i < sites.Count; i++)
            {
                Item site = sites[i];
                string no = (i + 1).ToString();
                string id = "site_tab_" + no;
                string site_id = site.getProperty("id", "");
                string site_name = site.getProperty("in_code_en", "");
                string active = i == 0 ? "active" : "";

                tabs.Add(new TTab
                {
                    id = id,
                    active = active,
                    title = site_name,
                    in_site = site_id,
                    GetListFunc = GetSiteEvents,
                });
            }

            foreach (var tab in tabs)
            {
                FixTab(cfg, tab);
            }

            StringBuilder builder = new StringBuilder();

            builder.Append("<ul class='nav nav-pills' style='justify-content: start ;' id='pills-tab' role='tablist'>");
            AppendTabHeads(cfg, tabs, builder);
            builder.Append("</ul>");

            builder.Append("<div class='tab-content' id='pills-tabContent' style='margin-top: -9px'>");
            AppendTabContents(cfg, tabs, builder);
            builder.Append("</div>");

            itmReturn.setProperty("inn_tabs", builder.ToString());
        }

        private void AppendTabHeads(TConfig cfg, List<TTab> tabs, StringBuilder builder)
        {
            foreach (var tab in tabs)
            {
                if (tab.is_error) continue;

                builder.Append("<li id='" + tab.li_id + "' class='nav-item " + tab.active + "'>");
                builder.Append("<a id='" + tab.a_id + "' href='#" + tab.content_id + "' class='nav-link' data-toggle='pill' role='tab' aria-controls='pills-home' aria-selected='true' onclick='StoreTab_Click(this)'>");
                builder.Append(tab.title + "(" + tab.count + ")");
                builder.Append("</a>");
                builder.Append("</li>");
            }
        }

        private void AppendTabContents(TConfig cfg, List<TTab> tabs, StringBuilder builder)
        {
            foreach (var tab in tabs)
            {
                if (tab.is_error) continue;

                builder.Append("<div class='tab-pane fade in " + tab.active + "' id='" + tab.content_id + "' role='tabpanel'>");
                builder.Append("    <div class='showsheet_ clearfix'>");
                builder.Append("        <div id='" + tab.folder_id + "'>");
                builder.Append("            <div class='float-btn clearfix'>");
                builder.Append("            </div>");

                AppendTable(cfg, tab, builder);

                builder.Append("        </div>");

                builder.Append("    </div>");
                builder.Append("</div>");
            }
        }

        private void AppendTable(TConfig cfg, TTab tab, StringBuilder builder)
        {
            var table = new TTable
            {
                id = tab.table_id,
                title = "",
                count = tab.items.Count,
            };

            var list = tab.items;

            StringBuilder body = new StringBuilder();
            int count = list.Count;
            body.Append("<tbody>");
            for (int i = 0; i < count; i++)
            {
                var match = list[i];
                cfg.AppendMatchAction(cfg, tab, match, body);
            }
            body.Append("</tbody>");

            //builder.Append("<h4 class='box-header with-border'>" + table.title + " (" + table.count + ")</h4>");
            builder.Append("<table id='" + table.id + "' class='table table-bordered table-rwd rwd'>");
            builder.Append(body);
            builder.Append("</table>");
            //builder.Append("<script>");
            //builder.Append("    $('#" + table.id + "').bootstrapTable({});");
            //builder.Append("    if($(window).width() <= 768) { $('#" + table.id + "').bootstrapTable('toggleView'); }");
            //builder.Append("</script>");
        }

        private void AppendMatch(TConfig cfg, TTab tab, TMatch match, StringBuilder body)
        {
            string td_css0 = "style='border: 1px solid black; vertical-align: middle; font-weight: bold; ' ";
            string td_css1 = "style='border: 1px solid black; vertical-align: middle; ' ";
            string td_css2 = "style='border: 1px solid black; width: 120px; vertical-align: middle; '";
            string td_css3 = "style='border: 1px solid black; width: 90px; vertical-align: middle; '";

            body.Append("  <tr>");
            body.Append("    <td class='text-left form-inline'   colspan='4' " + td_css0 + ">" + ProgramLink(cfg, match) + "</td>");
            body.Append("    <td class='text-center' " + td_css2 + ">主審</td>");
            body.Append("    <td class='text-center' " + td_css2 + ">副審</td>");
            body.Append("    <td class='text-center' " + td_css2 + ">主任裁判</td>");
            body.Append("    <td class='text-center col-md-1' " + td_css3 + ">功能</td>");
            body.Append("  </tr>");
            body.Append("  <tr>");
            body.Append("    <td class='text-center' rowspan='2' " + td_css2 + ">" + TreeNo(cfg, match) + "</td>");
            body.Append("    <td class='text-center' " + td_css1 + ">" + match.Foot1.color + "</td>");
            body.Append("    <td class='text-center' " + td_css1 + ">" + match.Foot1.org_display + "</td>");
            body.Append("    <td class='text-center' " + td_css1 + ">" + match.Foot1.in_name + match.Foot1.in_weight_message + "</td>");
            body.Append("    <td class='text-center' rowspan='2' " + td_css2 + ">" + StaffMenu(cfg, match, "主審", match.in_staff_1, "in_staff_1") + "</td>");
            body.Append("    <td class='text-center' rowspan='2' " + td_css2 + ">" + StaffMenu(cfg, match, "副審", match.in_staff_2, "in_staff_2") + "</td>");
            body.Append("    <td class='text-center' rowspan='2' " + td_css2 + ">" + StaffMenu(cfg, match, "主任裁判", match.in_staff_3, "in_staff_3") + "</td>");
            body.Append("    <td class='text-center' rowspan='2' " + td_css3 + ">" + StaffSetting(cfg, match) + "</td>");
            body.Append("  </tr>");
            body.Append("  <tr>");
            body.Append("    <td class='text-center' " + td_css2 + ">" + match.Foot2.color + "</td>");
            body.Append("    <td class='text-center' " + td_css2 + ">" + match.Foot2.org_display + "</td>");
            body.Append("    <td class='text-center' " + td_css2 + ">" + match.Foot2.in_name + match.Foot2.in_weight_message + "</td>");
            body.Append("  </tr>");
        }

        private string GetEvtNote(TConfig cfg, TMatch match)
        {
            string result = "";

            switch (match.in_tree_name)
            {
                case "main":
                    result = " 金牌";
                    break;

                case "repechage":
                    result = " 銅牌";
                    break;

                case "rank56":
                    result = " 五六名";
                    break;
            }

            return result;
        }

        private string ProgramLink(TConfig cfg, TMatch match)
        {
            string result = "";

            string href = "c.aspx"
                + "?page=In_Competition_Preview.html"
                + "&method=in_meeting_program_preview"
                + "&meeting_id=" + cfg.meeting_id
                + "&program_id=" + match.program_id;

            result = "<a target='_blank' href='" + href + "'>"
                + match.program_name + GetEvtNote(cfg, match)
                + "</a>";

            if (cfg.lstSites != null && cfg.lstSites.Count > 0)
            {
                result += GetSiteMenu(cfg, match);
            }

            return result;
        }

        private string GetSiteMenu(TConfig cfg, TMatch match)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(" <select class='form-control inn_edit_ctrl'");
            builder.Append(" onchange='EventSite_Change(this)'");
            builder.Append(" data-pid='" + match.program_id + "'");
            builder.Append(" data-rid='" + match.in_tree_id + "'>");
            builder.Append("  <option value=''>變更場地</option>");

            int count = cfg.lstSites.Count;
            for (int i = 0; i < count; i++)
            {
                Item item = cfg.lstSites[i];
                string id = item.getProperty("id", "");
                string in_name = item.getProperty("in_code_en", "");
                builder.Append("  <option value='" + id + "'>" + in_name + "</option>");
            }

            builder.Append("</select>");

            return builder.ToString();
        }

        private string TreeNo(TConfig cfg, TMatch match)
        {
            //return match.in_tree_no + match.we;

            return "<label style='color: red'>" + match.in_tree_no + match.we + "</label>"
                + "<input type='text' value='" + match.in_tree_no + "' class='event-input inn_edit_ctrl'"
                + " data-pid='" + match.program_id + "'"
                + " data-rid='" + match.in_tree_id + "'"
                + " data-old='" + match.in_tree_no + "'"
                + " onkeyup='Event_KeyUp(this)'"
                + " autocomplete='off'"
                + " >";
        }

        private string StaffMenu(TConfig cfg, TMatch match, string job, string name, string property)
        {
            if (cfg.lstStaff == null || cfg.lstStaff.Count == 0)
            {
                return name;
            }

            StringBuilder builder = new StringBuilder();
            if (!string.IsNullOrWhiteSpace(name))
            {
                builder.Append(name);
                builder.Append("<br>");
            }
            builder.Append(" <select class='form-control inn_staff inn_edit_ctrl' style='padding-left: 3px;'");
            builder.Append(" onchange='EventStaff_Change(this)'");
            builder.Append(" data-pid='" + match.program_id + "'");
            builder.Append(" data-rid='" + match.in_tree_id + "'");
            builder.Append(" data-job='" + job + "'");
            builder.Append(" data-pro='" + property + "'");
            builder.Append(" data-dfv='" + name + "'>");
            builder.Append("  <option value=''>----</option>");

            int count = cfg.lstStaff.Count;
            for (int i = 0; i < count; i++)
            {
                Item item = cfg.lstStaff[i];
                string in_no = item.getProperty("in_no", "");
                string in_name = item.getProperty("in_name", "");
                string in_city = item.getProperty("in_city", "");
                string in_job = item.getProperty("in_job", "");

                if (in_no != "") in_no = in_no + ". ";
                if (in_city != "") in_city = "-" + in_city;

                string text = in_no + in_name + in_city;

                builder.Append("  <option data-job='" + in_job + "' value='" + in_name + "'>" + text + "</option>");
            }

            builder.Append("</select>");

            return builder.ToString();
        }

        private string StaffSetting(TConfig cfg, TMatch match)
        {
            return "<button class='btn btn-success' data-eid='" + match.event_id + "' onclick='Staff_Click(this)'>"
                + "設定"
                + "</button>";
        }

        private void FixTab(TConfig cfg, TTab tab)
        {
            tab.li_id = "li_" + tab.id;
            tab.a_id = "a_" + tab.id;
            tab.content_id = "box_" + tab.id;
            tab.folder_id = "folder_" + tab.id;
            tab.toggle_id = "toggle_" + tab.id;
            tab.table_id = "table_" + tab.id;
            tab.items = tab.GetListFunc(cfg, tab);

            tab.is_error = tab.items.Count == 0;
            tab.count = tab.items.Count;
        }

        #endregion Table

        private void Page(TConfig cfg, Item itmReturn)
        {
            string sql = "SELECT in_title, in_battle_type, in_language FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'";
            cfg.itmMeeting = cfg.inn.applySQL(sql);
            cfg.in_title = cfg.itmMeeting.getProperty("in_title", "");
            cfg.in_battle_type = cfg.itmMeeting.getProperty("in_battle_type", "");
            itmReturn.setProperty("in_title", cfg.in_title);
            itmReturn.setProperty("in_language", cfg.itmMeeting.getProperty("in_language", ""));

            //日期選單
            List<Item> days = GetDateMenu(cfg);
            AppendItems(cfg, days, "inn_date", "in_date_key", "in_date_key", "比賽日期", "", itmReturn);

            if (cfg.in_date == "")
            {
                cfg.in_date = days[0].getProperty("in_date_key", "");
                itmReturn.setProperty("in_date", cfg.in_date);
            }

            AppendJudoTopFourMenu(cfg, itmReturn);
        }

        private void AppendChallengeMenu(TConfig cfg, Item itmReturn)
        {
            string in_type = "inn_item";

            List<Item> list = new List<Item>();
            AddItem(cfg, list, in_type, "CA01", GetEventTitle("CA01"));
            AddItem(cfg, list, in_type, "CA02", GetEventTitle("CA02"));
            AddItem(cfg, list, in_type, "CB01", GetEventTitle("CB01"));
            AddItem(cfg, list, in_type, "KA01", GetEventTitle("KA01"));
            AddItem(cfg, list, in_type, "KB01", GetEventTitle("KB01"));
            AppendItems(cfg, list, in_type, "value", "label", "", "", itmReturn);
        }

        private void AppendJudoTopFourMenu(TConfig cfg, Item itmReturn)
        {
            string in_type = "inn_item";

            List<Item> list = new List<Item>();
            AddItem(cfg, list, in_type, "MAIN", GetEventTitle("MAIN"));
            AddItem(cfg, list, in_type, "RPC", GetEventTitle("RPC"));
            AddItem(cfg, list, in_type, "RPC-COPPER", GetEventTitle("RPC-COPPER"));
            AddItem(cfg, list, in_type, "GOLD", GetEventTitle("GOLD"));
            AddItem(cfg, list, in_type, "COPPER-GOLD", GetEventTitle("COPPER-GOLD"));
            AppendItems(cfg, list, in_type, "value", "label", "", "", itmReturn);
        }

        private string GetEventTitle(string value)
        {
            switch (value)
            {
                //case "CA01": return "第 3 名挑戰第 2 名";
                //case "CA02": return "第 2 名挑戰第 1 名";
                //case "CB01": return "第 2 名挑戰第 1 名挑戰成功加賽場";

                //case "KA01": return "盟主戰";
                //case "KB01": return "盟主戰加賽";

                //case "MAIN": return "勝部場次";
                //case "RPC": return "敗部場次";
                case "RPC-COPPER": return "銅牌戰";
                case "GOLD": return "金牌戰";
                case "COPPER-GOLD": return "金牌與銅牌";

                default: return "";
            }
        }

        //日期選單
        private List<Item> GetDateMenu(TConfig cfg)
        {
            string sql = @"
                SELECT 
	                DISTINCT t1.in_date_key
                FROM 
	                IN_MEETING_PEVENT t1 WITH(NOLOCK) 
                WHERE 
	                t1.in_meeting = '{#meeting_id}'
	                AND ISNULL(t1.in_date_key, '') <> ''
                ORDER BY 
	                t1.in_date_key
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return MapList(cfg.inn.applySQL(sql));
        }

        //場地選單
        private List<Item> GetSiteMenu(TConfig cfg)
        {
            string sql = @"
                SELECT 
	                DISTINCT t2.id
                    , t2.in_code
                    , t2.in_name
                    , t2.in_code_en
                FROM 
	                IN_MEETING_PEVENT t1 WITH(NOLOCK) 
                INNER JOIN
                    IN_MEETING_SITE t2 WITH(NOLOCK)
                    ON t2.id = t1.in_site
                WHERE 
	                t1.in_meeting = '{#meeting_id}'
	                AND ISNULL(t1.in_site, '') <> ''
                ORDER BY 
	                t2.in_code
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return MapList(cfg.inn.applySQL(sql));
        }

        //工作人員選單
        private List<Item> GetStaffMenu(TConfig cfg)
        {
            string sql = @"
                SELECT
				    IIF(ISNULL(t1.in_no, 0) = 0, '', CAST(t1.in_no AS VARCHAR) + '. ') 
	                + '【' + t1.in_job + '】'
	                + t1.in_name
	                + IIF(ISNULL(t1.in_city, '') = '', '', '-' + t1.in_city) 
	                  AS 'label'
	                , t1.in_name AS 'value'
	                , t1.in_job  AS 'job'
                FROM
	                IN_MEETING_STAFF t1 WITH(NOLOCK)
                INNER JOIN
                (
	                SELECT 
		                t12.label_zt
		                , t12.value
		                , t12.sort_order
	                FROM
		                [LIST] t11 WITH(NOLOCK)
	                INNER JOIN
		                [VALUE] t12 WITH(NOLOCK)
		                ON t12.source_id = t11.id
	                WHERE
		                t11.name = 'In_Meeting_JobTitle'
                ) t2 ON t2.label_zt = t1.in_job
                WHERE
	                t1.source_id = '{#meeting_id}'
                ORDER BY 
	                t2.sort_order
	                , t1.in_no
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return MapList(cfg.inn.applySQL(sql));
        }

        //工作人員選單
        private List<Item> GetStaffMenu2(TConfig cfg)
        {
            string sql = @"
                SELECT
	                t1.in_no
	                , t1.in_name
	                , t1.in_city
	                , t1.in_job
                FROM
	                IN_MEETING_STAFF t1 WITH(NOLOCK)
                LEFT OUTER JOIN
	                VU_Mt_Staff t2 
	                ON t2.label_zt = t1.in_job
                WHERE
	                t1.source_id = '{#meeting_id}'
                ORDER BY 
	                t2.sort_order
	                , t1.in_no
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return MapList(cfg.inn.applySQL(sql));
        }

        private List<TMatch> GetSiteEvents(TConfig cfg, TTab tab)
        {
            string tree_condition = "";

            switch (cfg.in_item)
            {
                case "RPC-COPPER"://銅牌戰
                    tree_condition = "AND t2.in_fight_id IN ('R004-01', 'R004-02')";
                    break;

                case "GOLD"://金牌戰
                    tree_condition = "AND t2.in_fight_id IN ('M002-01')";
                    break;

                case "COPPER-GOLD"://金銅牌
                    tree_condition = "AND t2.in_fight_id IN ('M002-01', 'R004-01', 'R004-02')";
                    break;
            }

            string sql = @"
                SELECT
	                t1.id         AS 'program_id'
	                , t1.in_name3 AS 'program_name'
	                , t2.id       AS 'event_id'
	                , t2.in_tree_name
	                , t2.in_fight_id
	                , t2.in_tree_id
	                , t2.in_tree_no
	                , t2.in_round_code
	                , t2.in_staff_1
	                , t2.in_staff_2
	                , t2.in_staff_3
	                , t2.in_staff_4
	                , t2.in_course
	                , t2.in_we
	                , t3.in_sign_foot
	                , t3.in_sign_no
	                , t11.id AS 'team_id'
	                , ISNULL(t11.map_short_org, t3.in_player_org) AS 'map_short_org'
	                , ISNULL(t11.in_team, t3.in_player_team)      AS 'in_team'
	                , ISNULL(t11.in_name, t3.in_player_name)      AS 'in_name'
	                , ISNULL(t11.in_sno, t3.in_player_sno)        AS 'in_sno'
	                , t11.in_weight_message
	                , t11.in_section_no
	                , t11.in_final_rank
	                , t21.id        AS 'site_id'
	                , t21.in_name   AS 'site_name'
	                , t21.in_code   AS 'site_code'
                FROM
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PEVENT t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t3 WITH(NOLOCK)
	                ON t3.source_id = t2.id
                LEFT OUTER JOIN
	                VU_MEETING_PTEAM  t11 WITH(NOLOCK)
	                ON t11.source_id = t1.id
	                AND t11.in_sign_no = t3.in_sign_no
                INNER JOIN
				    IN_MEETING_SITE AS t21 WITH(NOLOCK)
				    ON t21.id =  t2.in_site
                WHERE
	                t2.in_meeting = '{#meeting_id}'
	                AND t2.in_date_key = '{#in_date}'
	                AND t2.in_site = '{#in_site}'
	                AND ISNULL(t2.in_tree_no, 0) > 0
	                {#tree_condition}
            ";

            List<TMid> source_list = new List<TMid>();

            string sql3 = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_date}", cfg.in_date)
                .Replace("{#in_site}", tab.in_site)
                .Replace("{#tree_condition}", tree_condition);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql3);

            Item items3 = cfg.inn.applySQL(sql3);
            AppendMidItems(source_list, items3);


            var sorted_list = source_list
                .OrderBy(x => x.tree_no)
                .ThenBy(x => x.event_id)
                .ThenBy(x => x.in_sign_foot);

            var item_list = sorted_list.Select(x => x.item).ToList();

            return MapTeamList(cfg, item_list);
        }

        private void AppendMidItems(List<TMid> list, Item items)
        {
            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string event_id = item.getProperty("event_id", "");
                string in_tree_no = item.getProperty("in_tree_no", "0");
                string in_sign_foot = item.getProperty("in_sign_foot", "0");

                int tree_no = GetInt32(in_tree_no);

                list.Add(new TMid
                {
                    event_id = event_id,
                    tree_no = tree_no,
                    in_sign_foot = in_sign_foot,
                    item = item,
                });
            }
        }

        private class TMid
        {
            public int tree_no { get; set; }
            public string event_id { get; set; }
            public string in_sign_foot { get; set; }
            public Item item { get; set; }
        }

        private List<TMatch> GetEvent(TConfig cfg, string event_id)
        {
            string sql = @"
                SELECT
                    t1.in_meeting   AS 'meeting_id'
	                , t1.id         AS 'program_id'
	                , t1.in_name3   AS 'program_name'
	                , t2.id         AS 'event_id'
	                , t2.in_tree_name
	                , t2.in_tree_id
	                , t2.in_tree_no
	                , t2.in_date_key
	                , t2.in_round_code
	                , t2.in_staff_1
	                , t2.in_staff_2
	                , t2.in_staff_3
	                , t2.in_staff_4
	                , t3.in_sign_foot
	                , t3.in_sign_no
	                , t11.id AS 'team_id'
	                , ISNULL(t11.map_short_org, t3.in_player_org) AS 'map_short_org'
	                , ISNULL(t11.in_team, t3.in_player_team) AS 'in_team'
	                , ISNULL(t11.in_name, t3.in_player_name) AS 'in_name'
	                , ISNULL(t11.in_sno, t3.in_player_sno) AS 'in_sno'
	                , t11.in_section_no
	                , t11.in_final_rank
	                , t21.id        AS 'site_id'
	                , t21.in_name   AS 'site_name'
	                , t21.in_code   AS 'site_code'
                FROM
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PEVENT t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t3 WITH(NOLOCK)
	                ON t3.source_id = t2.id
                LEFT OUTER JOIN
	                VU_MEETING_PTEAM  t11 WITH(NOLOCK)
	                ON t11.source_id = t1.id
	                AND t11.in_sign_no = t3.in_sign_no
                INNER JOIN
				    IN_MEETING_SITE AS t21 WITH(NOLOCK)
				    ON  t21.id =  t2.in_site
                WHERE
	                t2.id = '{#event_id}'
                ORDER BY
                    t2.in_tree_no
                    , t3.in_sign_foot
            ";

            sql = sql.Replace("{#event_id}", event_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            List<Item> item_list = new List<Item>();
            Item items = cfg.inn.applySQL(sql);
            for (int i = 0; i < items.getItemCount(); i++)
            {
                item_list.Add(items.getItemByIndex(i));
            }

            return MapTeamList(cfg, item_list);
        }

        //取得日期資料
        private TDay GetDayInfo(TConfig cfg)
        {
            string sql = @"
                SELECT t11.* FROM
                (
	                SELECT in_date_key, ROW_NUMBER() OVER (ORDER BY in_date_key) AS 'day_no' FROM
	                (
		                SELECT DISTINCT in_date_key FROM IN_MEETING_PEVENT WITH(NOLOCK) 
		                WHERE in_meeting = '{#meeting_id}'
		
	                ) t1
                ) t11
                WHERE t11.in_date_key = '{#in_date_key}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_date_key}", cfg.in_date);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item itmDay = cfg.inn.applySQL(sql);

            if (itmDay.isError() || itmDay.getResult() == "")
            {
                throw new Exception("查無比賽日期資料");
            }

            TDay day = new TDay
            {
                itmDay = itmDay,
                day_no = itmDay.getProperty("day_no", ""),
                tw_day = TwDay(cfg.in_date),
                spot_check_time = "pm 08:00～08:45",
            };

            //110年4月20日（二） pm 08:00~08:45
            day.contents = day.tw_day + " " + day.spot_check_time;

            return day;
        }

        private List<TMatch> MapTeamList(TConfig cfg, List<Item> item_list)
        {
            int count = item_list.Count;

            List<TMatch> list = new List<TMatch>();

            for (int i = 0; i < count; i++)
            {
                Item item = item_list[i];
                string event_id = item.getProperty("event_id", "");
                string in_sign_foot = item.getProperty("in_sign_foot", "");

                TMatch match = list.Find(x => x.event_id == event_id);

                if (match == null)
                {
                    match = new TMatch
                    {
                        program_id = item.getProperty("program_id", ""),
                        program_name = item.getProperty("program_name", ""),
                        event_id = item.getProperty("event_id", ""),
                        in_tree_name = item.getProperty("in_tree_name", ""),
                        in_tree_id = item.getProperty("in_tree_id", ""),
                        in_tree_no = item.getProperty("in_tree_no", ""),
                        in_round_code = item.getProperty("in_round_code", ""),

                        in_staff_1 = item.getProperty("in_staff_1", " "),
                        in_staff_2 = item.getProperty("in_staff_2", " "),
                        in_staff_3 = item.getProperty("in_staff_3", " "),
                        in_staff_4 = item.getProperty("in_staff_4", " "),

                        Foot1 = new TTeam { color = "White" },
                        Foot2 = new TTeam { color = "Blue" },
                        Value = item,

                        we = " " + item.getProperty("in_we", " "),
                    };


                    list.Add(match);
                }

                if (in_sign_foot == "1")
                {
                    match.Foot1.Value = item;
                    match.Foot1.org_display = cfg.GetF1OrgDisplayFunc(cfg, item);
                }
                else if (in_sign_foot == "2")
                {
                    match.Foot2.Value = item;
                    match.Foot2.org_display = cfg.GetF2OrgDisplayFunc(cfg, item);
                }
            }

            foreach (var match in list)
            {
                FixMatch(cfg, match.Foot1);
                FixMatch(cfg, match.Foot2);
            }
            return list;
        }

        private string GetOrgDisplay(TConfig cfg, Item item)
        {
            return item.getProperty("map_short_org", "");
        }

        private string GetOrgDisplay_CA01_F1(TConfig cfg, Item item)
        {
            //return "第３名 " + item.getProperty("map_short_org", "");
            return item.getProperty("map_short_org", "");
        }

        private string GetOrgDisplay_CA01_F2(TConfig cfg, Item item)
        {
            //return "第２名 " + item.getProperty("map_short_org", "");
            return item.getProperty("map_short_org", "");
        }

        private string GetOrgDisplay_CA02_F1(TConfig cfg, Item item)
        {
            //return "第２名 " + item.getProperty("map_short_org", "");
            return item.getProperty("map_short_org", "");
        }

        private string GetOrgDisplay_CA02_F2(TConfig cfg, Item item)
        {
            //return "第１名 " + item.getProperty("map_short_org", "");
            return item.getProperty("map_short_org", "");
        }

        private string GetOrgDisplay_CB01_F1(TConfig cfg, Item item)
        {
            return item.getProperty("map_short_org", "");
        }

        private string GetOrgDisplay_CB01_F2(TConfig cfg, Item item)
        {
            return item.getProperty("map_short_org", "");
        }

        private string GetOrgDisplay_King(TConfig cfg, Item item)
        {
            return item.getProperty("map_short_org", "");
        }

        private void FixMatch(TConfig cfg, TTeam team)
        {
            if (team.Value == null)
            {
                team.Value = cfg.inn.newItem();
            }

            team.in_name = team.Value.getProperty("in_name", "");
            team.in_weight_message = team.Value.getProperty("in_weight_message", "");
            team.in_sign_foot = team.Value.getProperty("in_sign_foot", "");
            team.in_sign_no = team.Value.getProperty("in_sign_no", "");
            team.in_section_no = team.Value.getProperty("in_section_no", "");
            team.in_status = team.Value.getProperty("in_status", "");
            team.in_points = team.Value.getProperty("in_points", "0");
            team.in_correct_count = team.Value.getProperty("in_correct_count", "0");

            if (team.org_display == null) team.org_display = "";
        }

        private List<Item> MapList(Item items)
        {
            int count = items.getItemCount();
            List<Item> list = new List<Item>();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                list.Add(item);
            }
            return list;
        }

        private void AddItem(TConfig cfg, List<Item> list, string type, string value, string label)
        {
            if (value == "") return;
            if (label == "") return;
            Item item = cfg.inn.newItem();
            item.setType(type);
            item.setProperty("value", value);
            item.setProperty("label", label);
            list.Add(item);
        }

        private void AppendItems(
            TConfig cfg
            , List<Item> list
            , string type_name
            , string val_property
            , string lbl_property
            , string title
            , string ext_property
            , Item itmReturn)
        {

            Item itmEmpty = cfg.inn.newItem();
            itmEmpty.setType(type_name);
            itmEmpty.setProperty("label", "請選擇" + title);
            itmEmpty.setProperty("value", "");
            itmReturn.addRelationship(itmEmpty);

            bool need_ext = ext_property != "";

            int count = list.Count;
            for (int i = 0; i < count; i++)
            {
                Item item = list[i];
                item.setType(type_name);
                item.setProperty("value", item.getProperty(val_property, ""));
                item.setProperty("label", item.getProperty(lbl_property, ""));

                if (need_ext)
                {
                    item.setProperty(ext_property, item.getProperty(ext_property, ""));
                }

                itmReturn.addRelationship(item);
            }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }

            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }

            public string meeting_id { get; set; }
            public string in_date { get; set; }
            public string in_item { get; set; }
            public string scene { get; set; }
            public string default_site_nav { get; set; }
            public string export_type { get; set; }
            public string need_twofight { get; set; }

            public Item itmMeeting { get; set; }
            public string in_title { get; set; }
            public string in_battle_type { get; set; }
            public string in_language { get; set; }

            public Action<TConfig, TTab, TMatch, StringBuilder> AppendMatchAction { get; set; }
            public Func<TConfig, Item, string> GetF1OrgDisplayFunc { get; set; }
            public Func<TConfig, Item, string> GetF2OrgDisplayFunc { get; set; }

            public TDay day { get; set; }
            public TXls xls { get; set; }

            public List<Item> lstSites { get; set; }
            public List<Item> lstStaff { get; set; }
        }

        private class TTab
        {
            public string id { get; set; }
            public string active { get; set; }
            public string title { get; set; }
            public string groups { get; set; }

            public string li_id { get; set; }
            public string a_id { get; set; }
            public string content_id { get; set; }
            public string folder_id { get; set; }
            public string toggle_id { get; set; }
            public string table_id { get; set; }

            public List<TMatch> items { get; set; }
            public bool is_error { get; set; }
            public int count { get; set; }

            public string in_site { get; set; }

            public Func<TConfig, TTab, List<TMatch>> GetListFunc { get; set; }
        }

        private class TTable
        {
            public string id { get; set; }
            public string title { get; set; }
            public int count { get; set; }
        }

        private class TMatch
        {
            public string program_id { get; set; }
            public string program_name { get; set; }
            public string event_id { get; set; }
            public string in_tree_name { get; set; }
            public string in_tree_id { get; set; }
            public string in_tree_no { get; set; }
            public string in_round_code { get; set; }

            public string in_staff_1 { get; set; }
            public string in_staff_2 { get; set; }
            public string in_staff_3 { get; set; }
            public string in_staff_4 { get; set; }

            public string we { get; set; }

            public Item Value { get; set; }
            public TTeam Foot1 { get; set; }
            public TTeam Foot2 { get; set; }
        }

        private class TTeam
        {
            public string color { get; set; }
            public string in_name { get; set; }
            public string in_weight_message { get; set; }
            public string in_sign_foot { get; set; }
            public string in_sign_no { get; set; }
            public string in_section_no { get; set; }
            public string in_status { get; set; }
            public string in_points { get; set; }
            public string in_correct_count { get; set; }

            public string org_display { get; set; }

            public Item Value { get; set; }
        }


        private class TField
        {
            public string title { get; set; }
            public string property { get; set; }
            public string hcss { get; set; }
            public string css { get; set; }
            public string format { get; set; }
            public string defval { get; set; }
            public Func<TConfig, TField, Item, string> getVal { get; set; }
        }

        private class TXls
        {
            /// <summary>
            /// 樣板完整路徑
            /// </summary>
            public string source { get; set; }
            /// <summary>
            /// 檔案名稱
            /// </summary>
            public string file { get; set; }
            /// <summary>
            /// 檔案相對路徑
            /// </summary>
            public string url { get; set; }
        }

        private class TSheet
        {
            public int WsRow { get; set; }
            public int WsCol { get; set; }
            public string[] Heads { get; set; }
            public string Font { get; set; }
            public int SheetCount { get; set; }
            public List<string> Sheets { get; set; }
        }

        private class TDay
        {
            public Item itmDay { get; set; }
            public string day_no { get; set; }
            public string tw_day { get; set; }
            public string spot_check_time { get; set; }
            public string contents { get; set; }
        }

        private string TwDay(string value)
        {
            if (value == "") return "";
            DateTime dt = Convert.ToDateTime(value);

            string day = (dt.Year - 1911) + "年"
                + dt.Month.ToString().PadLeft(2, '0') + "月"
                + dt.Day.ToString().PadLeft(2, '0') + "日";

            switch (dt.DayOfWeek)
            {
                case DayOfWeek.Sunday: return day + "（日）";
                case DayOfWeek.Monday: return day + "（一）";
                case DayOfWeek.Tuesday: return day + "（二）";
                case DayOfWeek.Wednesday: return day + "（三）";
                case DayOfWeek.Thursday: return day + "（四）";
                case DayOfWeek.Friday: return day + "（五）";
                case DayOfWeek.Saturday: return day + "（六）";
                default: return "";
            }
        }

        private string GetDateTimeValue(string value, string format)
        {
            if (value == "") return "";
            DateTime dt = Convert.ToDateTime(value);
            return dt.ToString(format);
        }

        private int GetInt32(string value)
        {
            if (value == "" || value == "0") return 0;
            int result = 0;
            int.TryParse(value, out result);
            return result;
        }
    }
}