﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Wrestling.Common
{
    public class in_meeting_program_score_fix : Item
    {
        public in_meeting_program_score_fix(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
    目的: 修正籤腳
    日誌: 
        - 2024-03-07: 創建 (lina)
*/

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "in_meeting_program_score_fix";

            var itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,

                meeting_id = itmR.getProperty("meeting_id", ""),
                program_id = itmR.getProperty("program_id", ""),
                event_id = itmR.getProperty("event_id", ""),
                scene = itmR.getProperty("scene", ""),
            };

            switch (cfg.scene)
            {
                case "auto_win":
                    AutoWin(cfg, itmR);
                    break;

                case "change_player":
                    ChangePlayer(cfg, itmR);
                    break;

                case "cancel_event":
                    CancelEvent(cfg);
                    break;

                case "rollback_event":
                    RollbackEvent(cfg);
                    break;

                case "scan":
                    ScanErrorByPass(cfg);
                    break;

                case "fix":
                    RefreshBypassPrograms(cfg);
                    break;
            }

            return itmR;
        }

        //自動勝出 for Report
        private void AutoWin(TConfig cfg, Item itmReturn)
        {
            var value = itmReturn.getProperty("value", "");
            var rows = Newtonsoft.Json.JsonConvert.DeserializeObject<List<TEvt>>(value);
            if (rows == null || rows.Count == 0) throw new Exception("無場次資料");

            var itmMeeting = cfg.inn.applySQL("SELECT * FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            if (itmMeeting.isError() || itmMeeting.getResult() == "") throw new Exception("無活動資料");

            var itmProgram = cfg.inn.applySQL("SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + cfg.program_id + "'");
            if (itmProgram.isError() || itmProgram.getResult() == "") throw new Exception("無組別資料");

            var teamCount = GetInt(itmProgram.getProperty("in_team_count", "0"));

            var itmDrawSheet = cfg.inn.applySQL("SELECT * FROM IN_MEETING_DRAWSHEET WITH(NOLOCK) WHERE in_team_count = '" + teamCount + "'");
            if (itmDrawSheet.isError() || itmDrawSheet.getResult() == "") throw new Exception("無籤表設定資料");

            var source_id = itmDrawSheet.getProperty("id", "");
            for (var i = 0; i < rows.Count; i++)
            {
                var row = rows[i];
                var treeNo = GetInt(row.in_tree_no);
                var sortOrder = treeNo * 100;
                if (treeNo <= 0) continue;

                Item itmNew = cfg.inn.newItem("In_Meeting_DrawSheet_Event", "merge");
                itmNew.setAttribute("where", "source_id='" + source_id + "' AND in_fight_id = '" + row.in_fight_id + "'");
                itmNew.setProperty("source_id", source_id);
                itmNew.setProperty("sort_order", sortOrder.ToString());
                itmNew.setProperty("in_fight_id", row.in_fight_id);
                itmNew.setProperty("in_word", GetTreeNoWord(treeNo));
                itmNew.apply();
            }

            for (var i = 0; i < rows.Count; i++)
            {
                var row = rows[i];
                var foot = GetEventFoot(cfg, row.event_id);
                if (foot == null || foot.detail_id == "") continue;

                var itmData = cfg.inn.newItem();
                itmData.setType("In_Meeting_Events");
                itmData.setProperty("meeting_id", cfg.meeting_id);
                itmData.setProperty("program_id", cfg.program_id);
                itmData.setProperty("event_id", row.event_id);
                itmData.setProperty("detail_id", foot.detail_id);
                itmData.setProperty("points_type", "HWIN");
                itmData.setProperty("points_text", "勝出");
                itmData.setProperty("mode", "score");
                itmData.setProperty("target_detail_id", foot.target_detail_id);
                itmData.apply("in_meeting_program_score");

                System.Threading.Thread.Sleep(100);
            }
        }

        private TRowDetail GetEventFoot(TConfig cfg, string event_id)
        {
            var sql = "SELECT id, in_sign_foot, in_sign_no FROM IN_MEETING_PEVENT_DETAIL WITH(NOLOCK) WHERE source_id = '" + event_id + "' ORDER BY in_sign_foot";
            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            if (count != 2) return null;

            var result = new TRowDetail { detail_id = "", target_detail_id = "" };

            var itmF1 = items.getItemByIndex(0);
            var itmF2 = items.getItemByIndex(1);

            var f1_id = itmF1.getProperty("id", "");
            var f1_no = itmF1.getProperty("in_sign_no", "");
            var f2_id = itmF2.getProperty("id", "");
            var f2_no = itmF2.getProperty("in_sign_no", "");

            var f1_setted = f1_no != "" && f1_no != "0";
            var f2_setted = f2_no != "" && f2_no != "0";

            if (f1_setted)
            {
                result.detail_id = f1_id;
                result.target_detail_id = f2_id;
            }
            else if (f2_setted)
            {
                result.detail_id = f2_id;
                result.target_detail_id = f2_id;
            }

            return result;
        }

        private class TRowDetail
        {
            public string detail_id { get; set; }
            public string target_detail_id { get; set; }
        }

        private string GetTreeNoWord(int treeNo)
        {
            switch (treeNo)
            {
                case 1: return "一";
                case 2: return "二";
                case 3: return "三";
                case 4: return "四";
                case 5: return "五";
                case 6: return "六";
                case 7: return "七";
                case 8: return "八";
                case 9: return "九";
                case 10: return "十";

                case 11: return "十一";
                case 12: return "十二";
                case 13: return "十三";
                case 14: return "十四";
                case 15: return "十五";
                case 16: return "十六";
                case 17: return "十七";
                case 18: return "十八";
                case 19: return "十九";
                case 20: return "二十";

                case 21: return "廿一";
                case 22: return "廿二";
                case 23: return "廿三";
                case 24: return "廿四";
                case 25: return "廿五";
                case 26: return "廿六";
                case 27: return "廿七";
                case 28: return "廿八";
                case 29: return "廿九";
                case 30: return "三十";

                case 31: return "卅一";
                case 32: return "卅二";
                case 33: return "卅三";
                case 34: return "卅四";
                case 35: return "卅五";
                case 36: return "卅六";
                case 37: return "卅七";
                case 38: return "卅八";
                case 39: return "卅九";
                case 40: return "四十";

                case 41: return "卌一";
                case 42: return "卌二";
                case 43: return "卌三";
                case 44: return "卌四";
                case 45: return "卌五";
                case 46: return "卌六";
                case 47: return "卌七";
                case 48: return "卌八";
                case 49: return "卌九";
                case 50: return "五十";

                case 51: return "五一";
                case 52: return "五二";
                case 53: return "五三";
                case 54: return "五四";
                case 55: return "五五";
                case 56: return "五六";
                case 57: return "五七";
                case 58: return "五八";
                case 59: return "五九";
                case 60: return "六十";

                case 61: return "六一";
                case 62: return "六二";
                case 63: return "六三";
                case 64: return "六四";

                default: return treeNo.ToString();
            }
        }

        private void ChangePlayer(TConfig cfg, Item itmReturn)
        {
            var detail_id = itmReturn.getProperty("detail_id", "");
            var in_sign_no = itmReturn.getProperty("in_sign_no", "");
            var is_clear = in_sign_no == "clear" || in_sign_no == "" || in_sign_no == "0";

            var value = "'" + in_sign_no + "'";
            if (is_clear) value = "NULL";

            var sql1 = @"
                UPDATE IN_MEETING_PEVENT_DETAIL SET 
	                in_sign_no = {#value}
                WHERE
	                id = '{#detail_id}'
            ";

            sql1 = sql1.Replace("{#event_id}", cfg.event_id)
                .Replace("{#detail_id}", detail_id)
                .Replace("{#value}", value);

            cfg.inn.applySQL(sql1);


            var sql2 = @"
                UPDATE IN_MEETING_PEVENT_DETAIL SET 
	                in_target_no = {#value}
                WHERE
	                source_id = '{#event_id}'
                    AND id <> '{#detail_id}'
            ";

            sql2 = sql2.Replace("{#event_id}", cfg.event_id)
                .Replace("{#detail_id}", detail_id)
                .Replace("{#value}", value);

            cfg.inn.applySQL(sql1);
        }

        private void CancelEvent(TConfig cfg)
        {
            string sql = "";

            sql = "UPDATE IN_MEETING_PEVENT SET in_win_status = 'cancel' WHERE id = '" + cfg.event_id + "'";
            cfg.inn.applySQL(sql);

            sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET in_sign_status = N'cancel' WHERE source_id = '" + cfg.event_id + "'";
            cfg.inn.applySQL(sql);
        }

        private void RollbackEvent(TConfig cfg)
        {
            var row = GetTargetEvent(cfg, cfg.event_id);
            RollbackCurrent(cfg, row.event_id);
            RollbackNext(cfg, row.in_next_win, row.in_next_foot_win);
        }

        private void ScanErrorByPass(TConfig cfg)
        {
            var itmPrograms = cfg.inn.applySQL("SELECT id, in_name FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE in_meeting = '" + cfg.meeting_id + "' ORDER BY in_sort_order");
            var count = itmPrograms.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var itmProgram = itmPrograms.getItemByIndex(i);
                ScanBypassEvents(cfg, itmProgram);
            }
        }

        //檢查需要修正
        private void ScanBypassEvents(TConfig cfg, Item itmProgram)
        {
            var program_id = itmProgram.getProperty("id", "");
            var program_name = itmProgram.getProperty("in_name", "");

            var items = GetDetailItems(cfg, program_id);
            var rows = MapRows(cfg, items);
            if (rows.Count == 0) throw new Exception("查無場次資料");

            var builder = new StringBuilder();
            for (var i = 0; i < rows.Count; i++)
            {
                var row = rows[i];

                if (row.isBypassStatus && row.hasTwoPlayers)
                {
                    AddLogMessage(builder, row, program_name, "Rollback");
                }
                else if (row.noWinner && row.needWinner)
                {
                    AddLogMessage(builder, row, program_name, "Bypass");
                }
            }

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, builder.ToString());
        }

        //執行修正
        private void RefreshBypassEvents(TConfig cfg, Item itmProgram)
        {
            var program_id = itmProgram.getProperty("id", "");
            var program_name = itmProgram.getProperty("in_name", "");

            var items = GetDetailItems(cfg, program_id);
            var rows = MapRows(cfg, items);
            if (rows.Count == 0) throw new Exception("查無場次資料");

            var builder = new StringBuilder();
            for (var i = 0; i < rows.Count; i++)
            {
                var row = rows[i];

                if (row.isBypassStatus && row.hasTwoPlayers)
                {
                    AddLogMessage(builder, row, program_name, "Rollback");
                    RollbackCurrent(cfg, row.event_id);
                    RollbackNext(cfg, row.in_next_win, row.in_next_foot_win);
                }
                else if (row.noWinner && row.needWinner)
                {
                    AddLogMessage(builder, row, program_name, "Bypass");
                    WinCurrent(cfg, row.event_id, row.in_tree_no, row.win_no, row.win_foot);
                    LinkNext(cfg, row.in_next_win, row.in_next_foot_win, row.win_no);
                }
            }

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, builder.ToString());
        }

        private void AddLogMessage(StringBuilder builder, TEvt row, string program_name, string message)
        {
            builder.AppendLine("[" + program_name + "]"
                + row.in_tree_id + " (" + row.in_tree_no + ")"
                + ": 需要 " + message + " (" + row.event_id + ")");
        }

        private void RefreshBypassPrograms(TConfig cfg)
        {
            var itmPrograms = GetProgramItems(cfg);
            var count = itmPrograms.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var itmProgram = itmPrograms.getItemByIndex(i);
                RefreshBypassEvents(cfg, itmProgram);
            }
        }

        private Item GetProgramItems(TConfig cfg)
        {
            var cond = cfg.program_id == ""
                ? ""
                : "AND t1.id = '" + cfg.program_id + "'";

            var sql = @"
                SELECT
	                t1.in_meeting
	                , t1.id
	                , t1.in_name
	                , t1.in_l1
	                , t1.in_l2
	                , t1.in_l3
                FROM
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
                (
	                SELECT DISTINCT in_meeting, in_l1, in_l2, in_l3 FROM IN_MEETING_USER_IMPORTNEW WITH(NOLOCK)
                ) t2
	                ON t2.in_meeting = t1.in_meeting
	                AND t2.in_l1 = t1.in_l1
	                AND t2.in_l2 = t1.in_l2
	                AND t2.in_l3 = t1.in_l3
                WHERE
	                t1.in_meeting = '{#meeting_id}'
                    {#cond}
                ORDER BY
	                t1.in_sort_order
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#cond}", cond);

            return cfg.inn.applySQL(sql);
        }

        private void LinkNext(TConfig cfg, string event_id, string in_sign_foot, string in_sign_no)
        {
            var in_target_foot = in_sign_foot == "1" ? "2" : "1";

            var sql1 = @"
                UPDATE IN_MEETING_PEVENT_DETAIL SET 
	                in_sign_no = '{#in_sign_no}'
                WHERE
	                source_id = '{#event_id}'
                    AND in_sign_foot = {#in_sign_foot}
            ";

            sql1 = sql1.Replace("{#event_id}", event_id)
                .Replace("{#in_sign_foot}", in_sign_foot)
                .Replace("{#in_sign_no}", in_sign_no);

            cfg.inn.applySQL(sql1);


            var sql2 = @"
                UPDATE IN_MEETING_PEVENT_DETAIL SET 
	                in_target_no = '{#in_sign_no}'
                WHERE
	                source_id = '{#event_id}'
                    AND in_sign_foot = {#in_target_foot}
            ";

            sql2 = sql2.Replace("{#event_id}", event_id)
                .Replace("{#in_target_foot}", in_target_foot)
                .Replace("{#in_sign_no}", in_sign_no);

            cfg.inn.applySQL(sql2);
        }

        private void WinCurrent(TConfig cfg, string event_id, string in_tree_no, string in_sign_no, string in_sign_foot)
        {
            var in_target_foot = in_sign_foot == "1" ? "2" : "1";
            var new_tree_no = in_tree_no == "9999" ? "NULL" : "'" + in_tree_no + "'";

            var sql1 = @"
                UPDATE IN_MEETING_PEVENT SET 
	                in_bypass_foot = '1'
	                , in_bypass_status = '0'
	                , in_win_status = 'bypass'
	                , in_win_sign_no = '{#in_sign_no}'
	                , in_win_time = GETUTCDATE()
	                , in_win_creator = 'lwu001'
	                , in_win_creator_sno = 'lwu001'
	                , in_tree_no = {#new_tree_no}
                WHERE
	                id = '{#event_id}'
            ";

            sql1 = sql1.Replace("{#event_id}", event_id)
                .Replace("{#in_sign_no}", in_sign_no)
                .Replace("{#new_tree_no}", new_tree_no);

            cfg.inn.applySQL(sql1);


            var sql2 = @"
                UPDATE IN_MEETING_PEVENT_DETAIL SET 
	                in_status = '1'
	                , in_sign_bypass = '0'
                WHERE
	                source_id = '{#event_id}'
                    AND in_sign_foot = {#in_sign_foot}
            ";

            sql2 = sql2.Replace("{#event_id}", event_id)
                .Replace("{#in_sign_foot}", in_sign_foot);

            cfg.inn.applySQL(sql2);


            var sql3 = @"
                UPDATE IN_MEETING_PEVENT_DETAIL SET 
	                in_status = '0'
	                , in_sign_bypass = '1'
                WHERE
	                source_id = '{#event_id}'
                    AND in_sign_foot = {#in_target_foot}
            ";

            sql3 = sql3.Replace("{#event_id}", event_id)
                .Replace("{#in_target_foot}", in_target_foot);

            cfg.inn.applySQL(sql3);
        }

        private void RollbackNext(TConfig cfg, string event_id, string in_sign_foot)
        {
            var in_target_foot = in_sign_foot == "1" ? "2" : "1";

            var sql1 = @"
                UPDATE IN_MEETING_PEVENT_DETAIL SET 
	                in_sign_no = NULL
                WHERE
	                source_id = '{#event_id}'
                    AND in_sign_foot = {#in_sign_foot}
            ";

            sql1 = sql1.Replace("{#event_id}", event_id)
                .Replace("{#in_sign_foot}", in_sign_foot);

            cfg.inn.applySQL(sql1);


            var sql2 = @"
                UPDATE IN_MEETING_PEVENT_DETAIL SET 
	                in_target_no = NULL
                WHERE
	                source_id = '{#event_id}'
                    AND in_sign_foot = {#in_target_foot}
            ";

            sql2 = sql2.Replace("{#event_id}", event_id)
                .Replace("{#in_target_foot}", in_target_foot);

            cfg.inn.applySQL(sql2);
        }

        private void RollbackCurrent(TConfig cfg, string event_id)
        {
            var sql1 = @"
                UPDATE IN_MEETING_PEVENT SET 
	                in_win_status = NULL
	                , in_win_sign_no = NULL
	                , in_win_time = NULL
	                , in_win_creator = NULL
	                , in_win_creator_sno = NULL
                WHERE
	                id = '{#event_id}'
            ";

            sql1 = sql1.Replace("{#event_id}", event_id);

            cfg.inn.applySQL(sql1);


            var sql2 = @"
                UPDATE IN_MEETING_PEVENT_DETAIL SET 
	                in_status = NULL
	                , in_sign_bypass = '0'
	                , in_sign_status = NULL
	                , in_points = NULL
	                , in_points_type = NULL
	                , in_points_text = NULL
	                , in_correct_count = NULL
	                , in_player_name = NULL
                WHERE
	                source_id = '{#event_id}'
            ";

            sql2 = sql2.Replace("{#event_id}", event_id);

            cfg.inn.applySQL(sql2);
        }

        private TEvt GetTargetEvent(TConfig cfg, string event_id)
        {
            var sql = @"
                SELECT TOP 1
	                t1.id AS 'event_id'
	                , t1.in_tree_name
	                , t1.in_round
	                , t1.in_tree_no
	                , t1.in_tree_id
	                , t1.in_fight_id
	                , t1.in_bypass_foot
	                , t1.in_bypass_status
	                , t1.in_win_status
	                , t1.in_win_sign_no
	                , t1.in_next_win
	                , t1.in_next_foot_win
	                , t2.id					AS 'detail_id'
	                , t2.in_status
	                , t2.in_sign_no			AS 'f1_no'
	                , t11.in_current_org	AS 'f1_org'
	                , t11.in_name			AS 'f1_name'
	                , t2.in_target_no		AS 'f2_no'
	                , t12.in_current_org	AS 'f2_org'
	                , t12.in_name			AS 'f2_name'
                FROM
	                IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                LEFT OUTER JOIN
	                IN_MEETING_PTEAM t11 WITH(NOLOCK)
	                ON t11.source_id = t1.source_id
	                AND t11.in_sign_no = t2.in_sign_no
                LEFT OUTER JOIN
	                IN_MEETING_PTEAM t12 WITH(NOLOCK)
	                ON t12.source_id = t1.source_id
	                AND t12.in_sign_no = t2.in_target_no
                WHERE
	                t1.id = '{#event_id}'
            ";

            sql = sql.Replace("{#event_id}", event_id);

            var itmData = cfg.inn.applySQL(sql);

            if (itmData.isError() || itmData.getResult() == "")
            {
                throw new Exception("查無資料");
            }

            return MapEvt(cfg, itmData);
        }

        private Item GetDetailItems(TConfig cfg, string program_id)
        {
            var sql = @"
                SELECT
	                t1.id AS 'event_id'
	                , t1.in_tree_name
	                , t1.in_round
	                , t1.in_tree_no
	                , t1.in_tree_id
	                , t1.in_fight_id
	                , t1.in_bypass_foot
	                , t1.in_bypass_status
	                , t1.in_win_status
	                , t1.in_win_sign_no
	                , t1.in_next_win
	                , t1.in_next_foot_win
	                , t2.id					AS 'detail_id'
	                , t2.in_status
	                , t2.in_sign_no			AS 'f1_no'
	                , t11.in_current_org	AS 'f1_org'
	                , t11.in_name			AS 'f1_name'
	                , t2.in_target_no		AS 'f2_no'
	                , t12.in_current_org	AS 'f2_org'
	                , t12.in_name			AS 'f2_name'
                FROM
	                IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                LEFT OUTER JOIN
	                IN_MEETING_PTEAM t11 WITH(NOLOCK)
	                ON t11.source_id = t1.source_id
	                AND t11.in_sign_no = t2.in_sign_no
                LEFT OUTER JOIN
	                IN_MEETING_PTEAM t12 WITH(NOLOCK)
	                ON t12.source_id = t1.source_id
	                AND t12.in_sign_no = t2.in_target_no
                WHERE
	                t1.source_id = '{#program_id}'
	                AND t1.in_round = 1
	                AND t1.in_tree_id NOT IN ('rank57a', 'rank34')
	                AND t2.in_sign_foot = 1
                ORDER BY
	                t1.in_round DESC
	                , t1.in_sign_no DESC
	                , t2.in_sign_foot DESC
            ";

            sql = sql.Replace("{#program_id}", program_id);

            return cfg.inn.applySQL(sql);
        }

        private List<TEvt> MapRows(TConfig cfg, Item items)
        {
            var rows = new List<TEvt>();
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var row = MapEvt(cfg, item);
                rows.Add(row);
            }
            return rows;
        }

        private TEvt MapEvt(TConfig cfg, Item item)
        {
            var row = new TEvt
            {
                event_id = item.getProperty("event_id", ""),
                in_tree_id = item.getProperty("in_tree_id", ""),
                in_tree_no = item.getProperty("in_tree_no", ""),
                in_win_status = item.getProperty("in_win_status", ""),
                in_win_sign_no = item.getProperty("in_win_sign_no", ""),
                in_next_win = item.getProperty("in_next_win", ""),
                in_next_foot_win = item.getProperty("in_next_foot_win", ""),

                f1_no = item.getProperty("f1_no", ""),
                f1_org = item.getProperty("f1_org", ""),
                f1_name = item.getProperty("f1_name", ""),
                f1_win = false,

                f2_no = item.getProperty("f2_no", ""),
                f2_org = item.getProperty("f2_org", ""),
                f2_name = item.getProperty("f2_name", ""),
                f2_win = false,

                hasTwoPlayers = false,
                onlyOnePlayer = false,
                isBypassStatus = false,
                noWinner = false,
                needWinner = false,

                win_no = "",
                win_foot = "",
            };

            if (row.in_win_status == "")
            {
                row.noWinner = true;
            }
            else if (row.in_win_status == "bypass")
            {
                row.isBypassStatus = true;
            }

            if (row.f1_name != "")
            {
                if (row.f2_name != "")
                {
                    row.hasTwoPlayers = true;
                }
                else
                {
                    row.needWinner = true;
                    row.onlyOnePlayer = true;
                    row.f1_win = true;
                    row.win_no = row.f1_no;
                    row.win_foot = "1";
                }
            }
            else if (row.f2_name != "")
            {
                if (row.f1_name != "")
                {
                    row.hasTwoPlayers = true;
                }
                else
                {
                    row.needWinner = true;
                    row.onlyOnePlayer = true;
                    row.f2_win = true;
                    row.win_no = row.f2_no;
                    row.win_foot = "2";
                }
            }
            else
            {
                //異常，本場次無選手
            }

            if (row.needWinner && row.in_win_sign_no != row.win_no)
            {
                row.wrongWinner = true;
            }

            return row;
        }

        private class TEvt
        {
            public string event_id { get; set; }
            public string in_fight_id { get; set; }
            public string in_tree_id { get; set; }
            public string in_tree_no { get; set; }
            public string in_win_status { get; set; }
            public string in_win_sign_no { get; set; }

            public string in_next_win { get; set; }
            public string in_next_foot_win { get; set; }

            public string f1_no { get; set; }
            public string f1_org { get; set; }
            public string f1_name { get; set; }
            public bool f1_win { get; set; }

            public string f2_no { get; set; }
            public string f2_org { get; set; }
            public string f2_name { get; set; }
            public bool f2_win { get; set; }

            public bool onlyOnePlayer { get; set; }
            public bool hasTwoPlayers { get; set; }
            public bool isBypassStatus { get; set; }
            public bool noWinner { get; set; }
            public bool needWinner { get; set; }
            public bool wrongWinner { get; set; }

            public string win_no { get; set; }
            public string win_foot { get; set; }
        }

        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }

            public string meeting_id { get; set; }
            public string program_id { get; set; }
            public string event_id { get; set; }
            public string scene { get; set; }
        }

        private int GetInt(string value, int defV = 0)
        {
            if (value == "") return 0;
            int result = defV;
            if (int.TryParse(value, out result))
            {
                return result;
            }
            return defV;
        }
    }
}