﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Wrestling.Common
{
    public class in_meeting_draw_run : Item
    {
        public in_meeting_draw_run(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 亂數抽籤
                輸入: meeting_id
                輸出: 
                    亂數籤表
                日期: 
                    2024/04/26: 角力同隊分面 (lina)
                    2024/03/20: 角力99號籤 (lina)
                    2022/10/13: 種子籤14同邊(參數化) (lina)
                    2022/08/01: 抽籤模組改為參數化
                    2021/09/10: 改為 Aras InnerXml 抽籤 (lina)
                    2020/07/30: 全部抽籤一直有問題，乾脆改迴圈+組別抽籤  (lina)
                    2020/07/29: 改為同單位分面 須配合 dll (lina)
                    2020/07/21: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]in_meeting_draw_run";

            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "input dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),

                meeting_id = itmR.getProperty("meeting_id", "").Trim(),
                program_id = itmR.getProperty("program_id", "").Trim(),
                in_index = itmR.getProperty("in_index", "").Trim(),

                draw_type = itmR.getProperty("draw_type", "").Trim(),
                draw_integral = itmR.getProperty("draw_integral", "").Trim(),
                draw_99 = itmR.getProperty("draw_99", "").Trim(),
                draw_sameteam = itmR.getProperty("draw_sameteam", "").Trim(),
                SameTeam_Separate = false,
                new_sign_no = ""
            };

            if (cfg.draw_integral == "") cfg.draw_integral = "N";
            if (cfg.draw_99 == "") cfg.draw_99 = "N";
            if (cfg.draw_sameteam == "Y") cfg.SameTeam_Separate = true;

            if (cfg.meeting_id == "")
            {
                throw new Exception("賽事 id 不得為空白");
            }
            if (cfg.draw_type == "")
            {
                throw new Exception("請選擇 抽籤方式");
            }

            if ((cfg.draw_type == "1" || cfg.draw_type == "2") && cfg.program_id == "")
            {
                throw new Exception("請選擇組別");
            }

            if (cfg.draw_type == "1" && cfg.in_index == "")
            {
                throw new Exception("請輸入選手編號");
            }

            Item itmDrawMode = cfg.inn.applySQL("SELECT in_value FROM IN_VARIABLE WITH(NOLOCK) WHERE in_name = 'draw_mode'");
            cfg.draw_mode = itmDrawMode.getProperty("in_value", "").ToLower();
            string aml = @"
                <AML>
                    <Item type='In_Meeting' action='get' id='{#meeting_id}' select='keyed_name,in_robin_player,in_draw_status,in_draw_file'>
                    </Item>
                </AML>
            ".Replace("{#meeting_id}", cfg.meeting_id);

            cfg.itmMeeting = inn.applyAML(aml);

            if (cfg.itmMeeting.isError())
            {
                throw new Exception("取得賽事資料發生錯誤");
            }


            Item itmSeed14 = cfg.inn.applySQL("SELECT id, in_value FROM IN_MEETING_VARIABLE WITH(NOLOCK) WHERE source_id = '" + cfg.meeting_id + "' AND in_key = 'seed_14_same_face'");
            if (itmSeed14.isError() || itmSeed14.getResult() == "")
            {
                cfg.seed_14_same_face = "";
            }
            else
            {
                cfg.seed_14_same_face = itmSeed14.getProperty("in_value", "").ToLower();
            }

            //已抽籤的組別不重抽
            Item itmFreezeDrawNo = cfg.inn.applySQL("SELECT id, in_value FROM IN_MEETING_VARIABLE WITH(NOLOCK) WHERE source_id = '" + cfg.meeting_id + "' AND in_key = 'freeze_has_draw_no'");
            if (itmFreezeDrawNo.isError() || itmFreezeDrawNo.getResult() == "")
            {
                cfg.freeze_has_draw_no = "";
            }
            else
            {
                cfg.freeze_has_draw_no = itmFreezeDrawNo.getProperty("in_value", "").ToLower();
            }

            cfg.isFreeze = cfg.freeze_has_draw_no == "1";


            cfg.in_robin_player = GetIntVal(cfg.itmMeeting.getProperty("in_robin_player", "0"));

            if (cfg.draw_type == "3")
            {
                //全部抽籤
                //lina: 由於全部抽籤一直發生問題: 不回傳、轉型失敗等等，所以改跑回圈

                Item itmPrograms = GetMeetingPrograms(cfg);
                int count = itmPrograms.getItemCount();
                for (int i = 0; i < count; i++)
                {
                    TProgram program = MapProgram(itmPrograms.getItemByIndex(i));

                    //已抽籤的組別不重抽
                    var hasDrawed = program.in_sign_time != "";
                    if (cfg.isFreeze && hasDrawed) continue;

                    RunStraw(cfg, program, "2");
                }
            }
            else
            {
                TProgram program = MapProgram(GetMeetingProgram(cfg));
                //已抽籤的組別不重抽
                var hasDrawed = program.in_sign_time != "";
                var disabledDraw = cfg.isFreeze && hasDrawed;
                if (!disabledDraw)
                {
                    RunStraw(cfg, program, cfg.draw_type);
                }
            }

            //個人抽籤結果
            if (cfg.new_sign_no != "")
            {
                itmR.setProperty("in_draw_no", cfg.new_sign_no);
            }


            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

            return itmR;
        }

        //抽籤
        private void RunStraw(TConfig cfg, TProgram program, string draw_type)
        {
            //該組只有1人抽籤時，Draw.dll 會抽不籤號，直接給號
            if (program.team_count == 1)
            {
                UpdateOnlyOne(cfg, program);
                return;
            }

            Item itmPlayers = GetProgramTeams(cfg);

            var draw = new InnSport.Core.Models.Business.TConfig();

            draw.SportType = cfg.draw_mode == "judo"
                ? InnSport.Core.Enums.SportEnum.Judo
                : InnSport.Core.Enums.SportEnum.Taekwondo;

            draw.ArasXml = itmPlayers.dom.InnerXml;
            draw.DrawType = draw_type;
            draw.Integral = cfg.draw_integral;
            draw.Draw99 = cfg.draw_99;
            //draw.Draw99MaxNumber = "9999";
            //draw.Draw99Dscending = "Y";
            draw.SameTeamSeparate = cfg.SameTeam_Separate;
            draw.RobinPlayerCount = cfg.in_robin_player;
            draw.Seed14SameFace = !program.in_battle_type.Contains("RoundRobin") && cfg.seed_14_same_face == "1";

            if (program.in_l1 == "格式組")
            {
                draw.SameTeamSeparate = false;
                draw.RobinPlayerCount = itmPlayers.getItemCount();
            }

            if (draw_type == "1")
            {
                draw.SectName = program.in_name3;
                draw.PlayerNo = cfg.in_index;
                draw.ResetDraw = false;
            }
            else if (draw_type == "2")
            {
                draw.SectName = program.in_name3;
                draw.ResetDraw = true;
            }

            if (cfg.SameTeam_Separate)
            {
                draw.BoxList = GenerateBoxList(cfg, program);
            }

            string xml = "";
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "[新版]draw run.before");
            try
            {
                xml = draw.ExecuteXml();
            }
            catch (Exception ex)
            {
                throw new Exception("網路忙線中，請稍後再試: " + ex.Message);
            }
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "[新版]draw run.after _# xml: " + xml);

            if (xml == null || xml == "")
            {
                throw new Exception("網路忙線中，請稍後再試...");
            }

            Item itmDraws = cfg.inn.newItem();
            itmDraws.loadAML(xml);

            if (itmDraws.isError() || itmDraws.getItemCount() == 0)
            {
                throw new Exception("網路忙線中，請稍後再試...");
            }

            AfterStraw(cfg, program, draw_type, MapDraws(itmDraws), draw);
        }

        private List<InnSport.Core.Models.Business.TBox> GenerateBoxList(TConfig cfg, TProgram program)
        {
            var rows = new List<InnSport.Core.Models.Business.TBox>();
            var items = GetDrawSignNoItems(cfg, program.in_team_count);
            if (items.isError() || items.getResult() == "") throw new Exception("籤號設定有問題");

            var count = items.getItemCount(); 
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var available = GetIntVal(item.getProperty("in_available", "0"));
                var n1 = GetIntVal(item.getProperty("in_f1_no", "0"));
                var n2 = GetIntVal(item.getProperty("in_f2_no", "0"));

                var bx = new InnSport.Core.Models.Business.TBox
                {
                    Balls = new List<InnSport.Core.Models.Business.TBall>(),
                    Available = available
                };

                if (available == 1)
                {
                    var no = n1 > 0 ? n1 : n2;
                    bx.Balls.Add(new InnSport.Core.Models.Business.TBall { BallNo = no, CanCatch = true, IsByPass = false });
                }
                else if (available == 2)
                {
                    bx.Balls.Add(new InnSport.Core.Models.Business.TBall { BallNo = n1, CanCatch = true, IsByPass = false });
                    bx.Balls.Add(new InnSport.Core.Models.Business.TBall { BallNo = n2, CanCatch = true, IsByPass = false });
                }
                rows.Add(bx);
            }

            return rows;
        }

        private Item GetDrawSignNoItems(TConfig cfg, string in_team_count)
        {
            var sql = @"
                SELECT DISTINCT 
	                in_fight_id
	                , in_f1_no
	                , in_f2_no
	                , in_available
                FROM
	                IN_MEETING_DRAWSIGNNO WITH(NOLOCK)
                WHERE
	                in_mode = 'knockout'
	                AND in_team_count = {#in_team_count}
	                AND ISNULL(in_available, 0) > 0
                ORDER BY
	                in_fight_id
            ";

            sql = sql.Replace("{#in_team_count}", in_team_count);

            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// 將被施術者更新至對手位置
        /// </summary>
        private void UpdateKataPEventDetail(TConfig cfg, TProgram program)
        {
            string sql = @"
                UPDATE t4 SET
                	t4.in_player_name = t1.in_exe_a1
                	, t4.in_player_org = REPLACE(t1.in_exe_a4, t1.in_stuff_b1, '')
                FROM
                	IN_MEETING_USER t1 WITH(NOLOCK)
                INNER JOIN
                	IN_MEETING_PROGRAM t2 WITH(NOLOCK)
                	ON t2.in_meeting = t1.source_id
                	AND t2.in_l1 = t1.in_l1
                	AND t2.in_l2 = t1.in_l2
                	AND t2.in_l3 = t1.in_l3
                INNER JOIN
                (
                	SELECT
                		t11.id AS 'pg_id'
                		, t12.id      AS 'evt_id'
                		, t13.in_sign_no
                		, t14.id      AS 'dtl_id'
                	FROM 
                		IN_MEETING_PROGRAM t11 WITH(NOLOCK)
                	INNER JOIN
                		IN_MEETING_PEVENT t12 WITH(NOLOCK)
                		ON t12.source_id = t11.id
                	INNER JOIN
                		IN_MEETING_PEVENT_DETAIL t13 WITH(NOLOCK)
                		ON t13.source_id = t12.id
                		AND t13.in_sign_foot = 1
                	INNER JOIN
                		IN_MEETING_PEVENT_DETAIL t14 WITH(NOLOCK)
                		ON t14.source_id = t12.id
                		AND t14.in_sign_foot = 2
                	WHERE
                		t11.in_meeting = '{#meeting_id}'
                		AND t11.id = '{#program_id}'
                		AND t11.in_l1 = N'格式組'
                ) t3
                	ON t3.pg_id = t2.id
                	AND t3.in_sign_no = t1.in_sign_no
                INNER JOIN
                	IN_MEETING_PEVENT_DETAIL t4 WITH(NOLOCK)
                	ON t4.id = t3.dtl_id
                WHERE
                	t1.source_id = '{#meeting_id}'
                	AND t2.id = '{#program_id}'
                	AND t1.in_l1 = N'格式組'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#program_id}", program.id);

            cfg.inn.applySQL(sql);

        }

        /// <summary>
        /// 只有一個人
        /// </summary>
        private void UpdateOnlyOne(TConfig cfg, TProgram program)
        {
            string in_sign_time = System.DateTime.Now.AddHours(-8).ToString("yyyy-MM-ddTHH:mm:ss");

            string sql = @"
                UPDATE
                    IN_MEETING_PTEAM
                SET
                    in_sign_no = '1'
                    , in_sign_time = '{#in_sign_time}'
                    , in_section_no = '1'
                    , in_judo_no = '1'
                    , in_show_no = '1'
                WHERE
                    source_id = '{#program_id}'
            ";

            sql = sql.Replace("{#program_id}", program.id)
                .Replace("{#in_sign_time}", in_sign_time);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item itmSQL = cfg.inn.applySQL(sql);

            //更新組別抽籤時間
            UpdateProgram(cfg, program, in_sign_time);

            //更新與會者籤號
            FixMUserSignNo(cfg, program);
        }

        /// <summary>
        /// 抽籤成功之後
        /// </summary>
        private void AfterStraw(TConfig cfg, TProgram program, string draw_type, List<TDraw> list, InnSport.Core.Models.Business.TConfig draw)
        {
            List<string> errors = new List<string>();

            string in_sign_time = System.DateTime.Now.AddHours(-8).ToString("yyyy-MM-ddTHH:mm:ss");
            int count = list.Count;

            for (int i = 0; i < count; i++)
            {
                var entity = list[i];

                //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, entity.in_index + " " + entity.in_name + " " + entity.in_draw_no);

                if (entity.in_index == "")
                {
                    string error = "組別: " + entity.in_section_name + ", [選手編號]為空值";
                    cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, error);
                    throw new Exception("抽籤結果發生錯誤，" + error);
                }

                if (entity.in_draw_no == "")
                {
                    string error = "組別: " + entity.in_section_name + ", [籤號]為空值，選手編號: " + entity.in_index;
                    cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, error);
                    throw new Exception("抽籤結果發生錯誤，" + error);
                }

                //轉換籤號(抽回來是跆拳道籤號)
                MapNo(cfg, program, entity, draw);

                //更新隊伍籤號
                bool exe_result = UpdatePTeam(cfg, program, entity, in_sign_time);

                if (!exe_result)
                {
                    errors.Add(entity.in_index + " " + entity.in_name);
                }
            }

            if (errors.Count > 0)
            {
                throw new Exception("更新籤號發生錯誤 \n" + string.Join("\n", errors));
            }

            //更新隊伍 ShowNo
            FixTeamShowNo(cfg, program);

            //更新組別抽籤時間
            UpdateProgram(cfg, program, in_sign_time);

            //更新與會者籤號
            FixMUserSignNo(cfg, program);

            if (draw_type == "1")
            {
                if (count > 0)
                {
                    cfg.new_sign_no = list[0].in_draw_no;
                }
                //若該組別所有人員已抽完籤，重整場次編號
            }
        }

        /// <summary>
        /// 轉換籤號
        /// </summary>
        private void MapNo(TConfig cfg, TProgram program, TDraw entity, InnSport.Core.Models.Business.TConfig draw)
        {
            if (program.is_round_robin)
            {
                entity.in_tkd_no = entity.in_draw_no;
                entity.in_judo_no = entity.in_draw_no;
                entity.in_section_no = entity.in_draw_no;
            }
            else
            {
                if (draw.SportType == InnSport.Core.Enums.SportEnum.Judo)
                {
                    entity.in_judo_no = entity.in_draw_no;
                    entity.in_tkd_no = entity.in_map_no;
                    entity.in_section_no = entity.in_judo_no;
                }
                else
                {
                    entity.in_tkd_no = entity.in_draw_no;
                    entity.in_section_no = entity.in_draw_no;
                    entity.in_judo_no = entity.in_map_no;
                }
            }
        }

        /// <summary>
        /// 更新與會者籤號
        /// </summary>
        private void FixMUserSignNo(TConfig cfg, TProgram program)
        {
            string sql = @"
                UPDATE t1 SET
        	        t1.in_sign_no = t2.in_sign_no
        	        , t1.in_section_no = t2.in_section_no
        	        , t1.in_draw_no = t2.in_lottery_99
                FROM
        	        IN_MEETING_USER t1 WITH(NOLOCK)
                INNER JOIN
        	        IN_MEETING_PTEAM t2 WITH(NOLOCK)
        	        ON t2.in_meeting = t1.source_id
        	        AND t2.in_team_key = ISNULL(t1.in_l1, '') 
        		        + '-' + ISNULL(t1.in_l2, '') 
        		        + '-' + ISNULL(t1.in_l3, '') 
        		        + '-' + ISNULL(t1.in_index, '')
        		        + '-' + ISNULL(t1.in_creator_sno, '')
                WHERE
        	        t1.source_id = '{#meeting_id}'
        	        AND t2.source_id = '{#program_id}'
        	        AND ISNULL(t2.in_sign_no, '') <> ''
                    AND ISNULL(t2.in_not_draw, 0) = 0
            ";

            sql = sql.Replace("{#meeting_id}", program.in_meeting)
                .Replace("{#program_id}", program.id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item itmSQL = cfg.inn.applySQL(sql);

            if (itmSQL.isError())
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "更新與會者籤號與量級序號發生錯誤 _# sql: " + sql);
            }
        }

        /// <summary>
        /// 更新隊伍 ShowNo
        /// </summary>
        private void FixTeamShowNo(TConfig cfg, TProgram program)
        {
            string sql = @"
                UPDATE t1 SET
                    t1.in_show_no = t2.rno
                FROM
                    IN_MEETING_PTEAM t1
                INNER JOIN
                (
                    SELECT
                        id
                        , ROW_NUMBER() OVER(PARTITION BY source_id ORDER BY in_stuff_b1, in_team, in_sno) AS 'rno'
                    FROM
                        IN_MEETING_PTEAM WITH(NOLOCK)
                    WHERE
                        source_id = '{#program_id}'
                        AND ISNULL(in_type, '') in ('', 'p')
                        AND ISNULL(in_not_draw, 0) = 0
                ) t2
                    ON t2.id = t1.id
                WHERE
                    t1.source_id = '{#program_id}'
            ";

            sql = sql.Replace("{#program_id}", program.id);

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item itmSQL = cfg.inn.applySQL(sql);

            if (itmSQL.isError())
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "更新隊伍 ShowNo _# sql: " + sql);
            }

            sql = "UPDATE IN_MEETING_PTEAM SET"
                + " in_show_no = NULL"
                + " , in_section_no = NULL"
                + " , in_sign_no = NULL"
                + " , in_judo_no = NULL"
                + " WHERE source_id = '" + program.id + "'"
                + " AND ISNULL(in_not_draw, 0) = 1";

            itmSQL = cfg.inn.applySQL(sql);
            if (itmSQL.isError())
            {
                throw new Exception("ERROR");
            }

        }

        //取得賽事組別
        private Item GetMeetingProgram(TConfig cfg)
        {
            string sql = @"
                SELECT
                    *
                FROM
                    IN_MEETING_PROGRAM WITH(NOLOCK)
                WHERE
                    id = '{#program_id}'
            ";

            sql = sql.Replace("{#program_id}", cfg.program_id);

            Item item = cfg.inn.applySQL(sql);

            if (item.isError() || item.getItemCount() <= 0)
            {
                throw new Exception("取得賽事組別發生錯誤");
            }

            return item;
        }

        //取得賽事組別
        private Item GetMeetingPrograms(TConfig cfg)
        {
            string sql = @"
                SELECT
                    *
                FROM
                    IN_MEETING_PROGRAM WITH(NOLOCK)
                WHERE
                    in_meeting = '{#meeting_id}'
                ORDER BY
                    in_l1_sort,
                    in_l2_sort,
                    in_l3_sort   
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            Item items = cfg.inn.applySQL(sql);

            if (items.isError() || items.getItemCount() <= 0)
            {
                throw new Exception("取得賽事組別發生錯誤");
            }

            return items;
        }

        //更新組別抽籤時間
        private bool UpdateProgram(TConfig cfg, TProgram program, string in_sign_time)
        {
            string sql = "";

            sql = @"
                UPDATE
                    IN_MEETING_PROGRAM
                SET
                    in_sign_time = '{#in_sign_time}'
                WHERE
                    id = '{#program_id}'
            ";

            sql = sql.Replace("{#program_id}", program.id)
                .Replace("{#in_sign_time}", in_sign_time);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item itmSQL = cfg.inn.applySQL(sql);

            if (itmSQL.isError())
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);
                return false;
            }
            else
            {
                return true;
            }
        }

        //更新籤號
        private bool UpdatePTeam(TConfig cfg, TProgram program, TDraw draw, string in_sign_time)
        {
            string sql = "";

            sql = @"
                UPDATE
                    IN_MEETING_PTEAM
                SET
                    in_sign_no = '{#in_sign_no}'
                    , in_sign_time = '{#in_sign_time}'
                    , in_section_no = '{#in_section_no}'
                    , in_judo_no = '{#in_judo_no}'
                    , in_lottery_99 = '{#in_lottery_99}'
                WHERE
                    source_id = '{#program_id}'
                    AND in_team_index = '{#in_team_index}'
            ";

            sql = sql.Replace("{#program_id}", program.id)
                .Replace("{#in_team_index}", draw.in_index)
                .Replace("{#in_sign_no}", draw.in_tkd_no)
                .Replace("{#in_judo_no}", draw.in_judo_no)
                .Replace("{#in_section_no}", draw.in_section_no)
                .Replace("{#in_lottery_99}", draw.in_lottery_99)
                .Replace("{#in_sign_time}", in_sign_time);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item itmSQL = cfg.inn.applySQL(sql);

            if (itmSQL.isError())
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);
                return false;
            }
            else
            {
                return true;
            }
        }

        //取得隊伍資料
        private Item GetProgramTeams(TConfig cfg)
        {
            string sql = @"
                SELECT 
                	ROW_NUMBER()　OVER (ORDER BY t2.in_team_index) AS 'id'
                	, t1.in_name3　					AS 'in_section_name'
                	, t2.map_org_name				AS 'in_current_org'
                	, t2.in_team_index				AS 'in_index'
                	, t2.in_name					AS 'in_name'
                	, ISNULL(t2.in_sign_no, 0)		AS 'in_draw_no'
                	, ISNULL(t2.in_points, 0)		AS 'in_points'
                	, 0								AS 'in_sort_order'
                FROM 
                	IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
                	VU_MEETING_PTEAM  t2 WITH(NOLOCK)
                	ON t2.source_id = t1.id
                WHERE
                	t1.id = '{#program_id}'
                    AND ISNULL(t2.in_not_draw, 0) = 0
                    AND ISNULL(t2.in_type, '') <> 's'
            ";

            sql = sql.Replace("{#program_id}", cfg.program_id);
            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql :" + sql);

            Item itmPlayers = cfg.inn.applySQL(sql);

            if (itmPlayers.isError() || itmPlayers.getResult() == "")
            {
                throw new Exception("查無選手資料");
            }

            return itmPlayers;
        }

        //轉換組別量級
        private TProgram MapProgram(Item itmProgram)
        {
            var result = new TProgram
            {
                id = itmProgram.getProperty("id", ""),
                in_meeting = itmProgram.getProperty("in_meeting", ""),
                in_name = itmProgram.getProperty("in_name", ""),
                in_name2 = itmProgram.getProperty("in_name2", ""),
                in_name3 = itmProgram.getProperty("in_name3", ""),
                in_battle_type = itmProgram.getProperty("in_battle_type", ""),
                in_team_count = itmProgram.getProperty("in_team_count", ""),
                in_round_code = itmProgram.getProperty("in_round_code", ""),
                in_l1 = itmProgram.getProperty("in_l1", ""),
                in_sign_time = itmProgram.getProperty("in_sign_time", ""),
            };

            result.team_count = GetIntVal(result.in_team_count);
            result.round_code = GetIntVal(result.in_round_code);

            result.is_round_robin = result.in_battle_type.Contains("RoundRobin");

            return result;
        }


        //轉換抽籤結果
        private List<TDraw> MapDraws(Item itmDraws)
        {
            List<TDraw> list = new List<TDraw>();

            int count = itmDraws.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item itmDraw = itmDraws.getItemByIndex(i);

                TDraw entity = new TDraw
                {
                    in_index = itmDraw.getProperty("in_index", ""),
                    in_current_org = itmDraw.getProperty("in_current_org", ""),
                    in_section_name = itmDraw.getProperty("in_section_name", ""),
                    in_name = itmDraw.getProperty("in_name", ""),
                    in_draw_no = itmDraw.getProperty("in_draw_no", ""),
                    in_map_no = itmDraw.getProperty("in_map_no", ""),
                    in_lottery_99 = itmDraw.getProperty("in_lottery_99", ""),
                    //in_points = itmDraw.getProperty("in_points", ""),
                };

                list.Add(entity);
            }

            return list;
        }

        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string draw_mode { get; set; }
            public string seed_14_same_face { get; set; }
            public string freeze_has_draw_no { get; set; }
            public bool isFreeze { get; set; }

            /// <summary>
            /// 賽事 id
            /// </summary>
            public string meeting_id { get; set; }

            /// <summary>
            /// 循環賽人數
            /// </summary>
            public int in_robin_player { get; set; }

            /// <summary>
            /// 組別 id
            /// </summary>
            public string program_id { get; set; }

            /// <summary>
            /// 1: 個人抽籤、2: 整組抽籤、3: 全部抽籤
            /// </summary>
            public string draw_type { get; set; }

            /// <summary>
            /// 選手編號 in_player_no (in_team_index)
            /// </summary>
            public string in_index { get; set; }

            /// <summary>
            /// 種子籤
            /// </summary>
            public string draw_integral { get; set; }

            /// <summary>
            /// 99號抽籤
            /// </summary>
            public string draw_99 { get; set; }

            /// <summary>
            /// 同隊分面
            /// </summary>
            public string draw_sameteam { get; set; }

            /// <summary>
            /// 同隊分面
            /// </summary>
            public bool SameTeam_Separate { get; set; }

            /// <summary>
            /// 賽事資料
            /// </summary>
            public Item itmMeeting { get; set; }

            /// <summary>
            /// 個人抽籤的抽號結果
            /// </summary>
            public string new_sign_no { get; set; }
        }

        private class TProgram
        {
            /// <summary>
            /// 組別 id
            /// </summary>
            public string id { get; set; }
            /// <summary>
            /// 賽事 id
            /// </summary>
            public string in_meeting { get; set; }
            /// <summary>
            /// 完整組名
            /// </summary>
            public string in_name { get; set; }
            /// <summary>
            /// 簡易組名
            /// </summary>
            public string in_name2 { get; set; }
            /// <summary>
            /// 含量級組名
            /// </summary>
            public string in_name3 { get; set; }
            /// <summary>
            /// 賽制
            /// </summary>
            public string in_battle_type { get; set; }
            /// <summary>
            /// 隊伍數量
            /// </summary>
            public string in_team_count { get; set; }
            /// <summary>
            /// 隊伍籤表
            /// </summary>
            public string in_round_code { get; set; }
            /// <summary>
            /// 競賽項目
            /// </summary>
            public string in_l1 { get; set; }
            /// <summary>
            /// 抽籤時間
            /// </summary>
            public string in_sign_time { get; set; }

            public int team_count { get; set; }

            public int round_code { get; set; }

            public Item Value { get; set; }

            public bool is_round_robin { get; set; }
        }

        private class TDraw
        {
            /// <summary>
            /// 選手編號(in_team_index)
            /// </summary>
            public string in_index { get; set; }
            /// <summary>
            /// 單位
            /// </summary>
            public string in_current_org { get; set; }
            /// <summary>
            /// 組別
            /// </summary>
            public string in_section_name { get; set; }
            /// <summary>
            /// 姓名
            /// </summary>
            public string in_name { get; set; }
            /// <summary>
            /// 籤號結果
            /// </summary>
            public string in_draw_no { get; set; }
            /// <summary>
            /// 映射籤號
            /// </summary>
            public string in_map_no { get; set; }

            /// <summary>
            /// 跆拳道籤號
            /// </summary>
            public string in_tkd_no { get; set; }
            /// <summary>
            /// 柔道籤號
            /// </summary>
            public string in_judo_no { get; set; }
            /// <summary>
            /// 量級序號(由跆拳道對戰表排序而成)
            /// </summary>
            public string in_section_no { get; set; }
            /// <summary>
            /// 99號籤
            /// </summary>
            public string in_lottery_99 { get; set; }
        }

        private int GetIntVal(string value, int def = 0)
        {
            int result = def;
            int.TryParse(value, out result);
            return result;
        }
    }
}