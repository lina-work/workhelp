﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;
using WorkHelp.ArasDesk.Samples.SqlClient;

namespace WorkHelp.ArasDesk.Methods.Wrestling.Common
{
    public class in_meeting_rank_sumpage_111nmssport : Item
    {
        public in_meeting_rank_sumpage_111nmssport(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 名次統計(111全中運版)
                位置: In_Competition_Bulletin_N1.html
                日期: 
                    - 2024-09-24 超輕量級 (Lina)
                    - 2021-10-09 修改 (Lina)
                    - 2020-11-17 創建 (Alan)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]in_meeting_rank_sumpage_111nmssport";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, this.dom.InnerXml);

            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,

                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,

                meeting_id = itmR.getProperty("meeting_id", ""),
                in_date = itmR.getProperty("in_date", ""),
                program_id = itmR.getProperty("program_id", ""),
                in_gender = itmR.getProperty("in_gender", ""),
                scene = itmR.getProperty("scene", ""),
                CharSet = GetCharSet(),
            };

            cfg.itmMeeting = cfg.inn.applySQL("SELECT in_title, in_group_score_mode FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            cfg.mt_title = cfg.itmMeeting.getProperty("in_title", "");

            var in_group_score_mode = cfg.itmMeeting.getProperty("in_group_score_mode", "");
            if (in_group_score_mode == "nhsg")
            {   //全中運模式
                cfg.Rank1Points = 4;
                cfg.Rank2Points = 2;
                cfg.Rank3Points = 1;
                cfg.Rank5Points = 0;
                cfg.Rank7Points = 0;
                cfg.NeedDivide = true;//需以人數均分
            }
            else
            {   //協會模式
                cfg.Rank1Points = 5;
                cfg.Rank2Points = 2;
                cfg.Rank3Points = 1;
                cfg.Rank5Points = 0;
                cfg.Rank7Points = 0;
                cfg.NeedDivide = false; //不需以人數均分
            }

            if (cfg.in_gender == "")
            {
                cfg.show_a = true;
                cfg.show_m = true;
                cfg.show_w = true;
                cfg.show_x = true;
            }

            switch (cfg.in_gender)
            {
                case "混合":
                    cfg.show_x = true;
                    break;
                case "男":
                    cfg.show_m = true;
                    break;
                case "女":
                    cfg.show_w = true;
                    break;
            }

            if (cfg.mt_title.Contains("金德盃"))
            {
                cfg.has_mix = true;
            }

            if (!cfg.has_mix) cfg.show_x = false;

            AppendSectPointsMap(cfg);

            switch (cfg.scene)
            {
                case "sum_page":
                    cfg.IsSumReport = true;
                    SumFunc(cfg, itmR, SumPage);
                    break;

                case "sum_xlsx":
                    cfg.IsSumReport = true;
                    SumFunc(cfg, itmR, SumXlsx);
                    break;

                case "rank_xlsx":
                    cfg.IsSumReport = false;
                    ExportRankXlsx(cfg, itmR);
                    break;
            }

            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

            return itmR;
        }

        #region 名次統計

        private void SumFunc(TConfig cfg, Item itmReturn, Action<TConfig, List<TMedal>, List<TGroup>, Item> action)
        {
            var medals = new List<TMedal>();
            medals.Add(new TMedal { rank = 1, label = "冠軍", ranks = new int[] { 1 }, prop = "rank01_count" });
            medals.Add(new TMedal { rank = 2, label = "亞軍", ranks = new int[] { 2 }, prop = "rank02_count" });
            medals.Add(new TMedal { rank = 3, label = "季軍", ranks = new int[] { 3, 4 }, prop = "rank03_count" });
            medals.Add(new TMedal { rank = 5, label = "第五名", ranks = new int[] { 5, 6 }, prop = "rank05_count" });
            medals.Add(new TMedal { rank = 7, label = "第七名", ranks = new int[] { 7, 8 }, prop = "rank07_count" });

            var items = GetScores(cfg);
            var groups = MapGroup(cfg, items);
            action(cfg, medals, groups, itmReturn);
        }

        private void SumXlsx(TConfig cfg, List<TMedal> medals, List<TGroup> groups, Item itmReturn)
        {
            var exp = ExportInfo(cfg, "");

            //試算表
            var workbook = new Spire.Xls.Workbook();

            foreach (var gp in groups)
            {
                var orgs = MapOrgList(cfg, medals, gp);
                SumXlsxSheet(cfg, workbook, medals, gp, orgs);
            }

            workbook.Worksheets[0].Remove();
            workbook.Worksheets[0].Remove();
            workbook.Worksheets[0].Remove();

            string ext_name = ".xlsx";
            string xls_name = cfg.mt_title + "_名次統計_" + DateTime.Now.ToString("MMdd_HHmmss");
            string xls_file = exp.export_Path + "\\" + xls_name + ext_name;
            string xls_url = xls_name + ext_name;

            workbook.SaveToFile(xls_file, Spire.Xls.ExcelVersion.Version2013);

            itmReturn.setProperty("xls_name", xls_url);
        }

        private void SumXlsxSheet(TConfig cfg, Spire.Xls.Workbook workbook, List<TMedal> medals, TGroup gp, List<TOrg> orgs)
        {
            Spire.Xls.Worksheet sheet = workbook.CreateEmptySheet();
            sheet.Name = gp.sheetName;

            int ci = 1;

            List<TField> fields = new List<TField>();

            fields.Add(new TField { ci = ci, title = "序號", property = "no", css = TCss.Center, width = 8 });
            ci++;
            fields.Add(new TField { ci = ci, title = "單位", property = "map_short_org", css = TCss.None, width = 18 });
            ci++;

            if (cfg.show_x)
            {
                foreach (var medal in medals)
                {
                    fields.Add(new TField { ci = ci, title = "混合" + Environment.NewLine + medal.label, property = "x_" + medal.prop, css = TCss.Double, width = 6 });
                    ci++;
                }
                fields.Add(new TField { ci = ci, title = "混合" + Environment.NewLine + "總分", property = "x_total", css = TCss.Double, width = 5 });
                ci++;
            }

            foreach (var medal in medals)
            {
                fields.Add(new TField { ci = ci, title = "男子" + Environment.NewLine + medal.label, property = "m_" + medal.prop, css = TCss.Double, width = 6 });
                ci++;
            }
            fields.Add(new TField { ci = ci, title = "男子" + Environment.NewLine + "總分", property = "m_total", css = TCss.Double, width = 5 });
            ci++;

            foreach (var medal in medals)
            {
                fields.Add(new TField { ci = ci, title = "女子" + Environment.NewLine + medal.label, property = "w_" + medal.prop, css = TCss.Double, width = 6 });
                ci++;
            }
            fields.Add(new TField { ci = ci, title = "女子" + Environment.NewLine + "總分", property = "w_total", css = TCss.Double, width = 5 });
            ci++;

            fields.Add(new TField { ci = ci, title = "總積分", property = "total", css = TCss.Double, width = 5 });

            MapCharSet(cfg, fields);

            var fs = fields.First();
            var fe = fields.Last();

            var mt_mr = sheet.Range[fs.cs + "1" + ":" + fe.cs + "1"];
            mt_mr.Merge();
            mt_mr.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
            mt_mr.Text = cfg.mt_title;
            mt_mr.Style.Font.Size = 16;
            mt_mr.RowHeight = 20;

            var rpt_mr = sheet.Range[fs.cs + "2" + ":" + fe.cs + "2"];
            rpt_mr.Merge();
            rpt_mr.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
            rpt_mr.Text = "名次統計";
            rpt_mr.Style.Font.Size = 16;
            rpt_mr.RowHeight = 20;

            int mnRow = 3;
            int wsRow = 3;

            //標題列
            SetHeadRow(sheet, wsRow, fields);
            wsRow++;

            //凍結窗格
            sheet.FreezePanes(wsRow, fields.Last().ci + 1);

            //資料容器
            Item item = cfg.inn.newItem();
            int no = 0;

            foreach (var org in orgs)
            {
                no++;

                item.setProperty("no", no.ToString());
                item.setProperty("map_short_org", org.map_short_org);

                if (cfg.show_x)
                {
                    item.setProperty("x_rank01_count", org.x_box.rank01_count.ToString());
                    item.setProperty("x_rank02_count", org.x_box.rank02_count.ToString());
                    item.setProperty("x_rank03_count", org.x_box.rank03_count.ToString());
                    item.setProperty("x_rank05_count", org.x_box.rank05_count.ToString());
                    item.setProperty("x_rank06_count", org.x_box.rank07_count.ToString());
                    item.setProperty("x_total", org.x_box.total_points.ToString());
                }

                item.setProperty("m_rank01_count", org.m_box.rank01_count.ToString());
                item.setProperty("m_rank02_count", org.m_box.rank02_count.ToString());
                item.setProperty("m_rank03_count", org.m_box.rank03_count.ToString());
                item.setProperty("m_rank05_count", org.m_box.rank05_count.ToString());
                item.setProperty("m_rank06_count", org.m_box.rank07_count.ToString());
                item.setProperty("m_total", org.m_box.total_points.ToString());

                item.setProperty("w_rank01_count", org.w_box.rank01_count.ToString());
                item.setProperty("w_rank02_count", org.w_box.rank02_count.ToString());
                item.setProperty("w_rank03_count", org.w_box.rank03_count.ToString());
                item.setProperty("w_rank05_count", org.w_box.rank05_count.ToString());
                item.setProperty("w_rank06_count", org.w_box.rank07_count.ToString());
                item.setProperty("w_total", org.w_box.total_points.ToString());

                item.setProperty("total", org.o_box.total_points.ToString());

                SetItemCell(cfg, sheet, wsRow, item, fields);

                wsRow++;
            }

            //設定格線
            SetRangeBorder(sheet, fields, mnRow, wsRow);

            //設定欄寬
            SetColumnWidth(sheet, fields);
        }

        private void SumPage(TConfig cfg, List<TMedal> medals, List<TGroup> groups, Item itmReturn)
        {
            var builder = new StringBuilder();
            AppendTabHead(cfg, medals, groups, builder);
            AppendTabBody(cfg, medals, groups, builder);
            itmReturn.setProperty("inn_tab", builder.ToString());
            itmReturn.setProperty("in_title", cfg.mt_title);
        }

        private void AppendTabHead(TConfig cfg, List<TMedal> medals, List<TGroup> groups, StringBuilder builder)
        {
            builder.Append("<ul class='nav nav-pills' style='justify-content: start;' id='pills-tab' role='tablist'>");
            foreach (var gp in groups)
            {
                string active = gp.Id == "1" ? " active" : "";
                builder.Append("<li class='nav-item" + active + "'>");
                builder.Append("  <a class='nav-link' data-toggle='pill' id='" + gp.Tab_Id + "'");
                builder.Append("     role='tab' aria-controls='pills-home' href='#" + gp.Div_Id + "' onclick='StoreTab_Click(this)'>");
                builder.Append("  " + gp.sheetName);
                builder.Append("  </a>");
                builder.Append("</li>");
            }
            builder.Append("</ul>");
        }

        private void AppendTabBody(TConfig cfg, List<TMedal> medals, List<TGroup> groups, StringBuilder builder)
        {
            builder.Append("<div class='tab-content page-tab-content' id='pills-tabContent'>");

            foreach (var gp in groups)
            {
                var orgs = MapOrgList(cfg, medals, gp);

                string active = gp.Id == "1" ? " active in" : "";

                builder.Append("<div id='" + gp.Div_Id + "' class='tab-pane " + active + "' role='tabpanel'>");
                builder.Append("  <div class='box-body row container-row'>");
                builder.Append("    <div class='box box-fix'>");
                builder.Append("      <div class='box-body'>");

                AppendTable(cfg, medals, gp, orgs, builder);


                builder.Append("      </div>");
                builder.Append("    </div>");
                builder.Append("  </div>");
                builder.Append("</div>");
            }
            builder.Append("</div>");
        }

        private void AppendTable(TConfig cfg, List<TMedal> medals, TGroup gp, List<TOrg> orgs, StringBuilder builder)
        {
            var map = GetOrgGroupRankList(cfg, gp);
            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            head.Append("<thead>");
            head.Append("<tr>");
            head.Append("<th class='text-center'>&nbsp;</th>");
            head.Append("<th class='text-center'>單位</th>");

            if (cfg.show_m)
            {
                for (int i = 0; i < medals.Count; i++)
                {
                    var medal = medals[i];
                    head.Append("<th class='text-center'>" + "男子<br>" + medal.label + "</th>");
                }
                head.Append("<th class='text-center'>男子<br>總分</th>");
            }

            if (cfg.show_w)
            {
                for (int i = 0; i < medals.Count; i++)
                {
                    var medal = medals[i];
                    head.Append("<th class='text-center'>" + "女子<br>" + medal.label + "</th>");
                }
                head.Append("<th class='text-center'>女子<br>總分</th>");
            }

            if (cfg.show_x)
            {
                for (int i = 0; i < medals.Count; i++)
                {
                    var medal = medals[i];
                    head.Append("<th class='text-center'>" + "混合<br>" + medal.label + "</th>");
                }
                head.Append("<th class='text-center'>混合<br>總分</th>");
            }

            if (cfg.show_a)
            {
                head.Append("<th class='text-center'>總積分</th>");
            }

            head.Append("<th class='text-center'>獎狀</th>");

            head.Append("</tr>");
            head.Append("</thead>");

            body.Append("<tbody>");

            for (int i = 0; i < orgs.Count; i++)
            {
                var org = orgs[i];

                var tr_id = gp.Table_Id + "_tr_" + (i + 1);

                body.Append("<tr>");

                var no_wait = org.Teams.Count <= 0;
                if (no_wait)
                {
                    body.Append("<td class='text-center'> &nbsp; </td>");
                }
                else
                {
                    body.Append("<td class='text-center'> <span class='event-btn' onclick='ToggleWaitRow_Click(this)' data-id='" + tr_id + "' data-state='hide'> &nbsp; <i class='fa fa-plus '></i> &nbsp; </span> </td>");
                }

                body.Append("<td class='text-center'>" + org.map_short_org + "</td>");

                if (cfg.show_m)
                {
                    var m_medals = org.medals.FindAll(x => x.gcode == 1);
                    for (int j = 0; j < m_medals.Count; j++)
                    {
                        var medal = m_medals[j];
                        body.Append("<td class='text-cente boy_group'>" + medal.count + "</td>");
                    }
                    body.Append("<td class='text-center gold_group'>" + org.m_box.total_points.ToString().Replace(".0", "") + "</td>");
                }

                if (cfg.show_w)
                {
                    var w_medals = org.medals.FindAll(x => x.gcode == 2);
                    for (int j = 0; j < w_medals.Count; j++)
                    {
                        var medal = w_medals[j];
                        body.Append("<td class='text-center girl_group'>" + medal.count + "</td>");
                    }
                    body.Append("<td class='text-center gold_group'>" + org.w_box.total_points.ToString().Replace(".0", "") + "</td>");
                }

                if (cfg.show_x)
                {
                    var x_medals = org.medals.FindAll(x => x.gcode == 3);
                    for (int j = 0; j < x_medals.Count; j++)
                    {
                        var medal = x_medals[j];
                        body.Append("<td class='text-center mix_group'>" + medal.count + "</td>");
                    }
                    body.Append("<td class='text-center gold_group'>" + org.x_box.total_points.ToString().Replace(".0", "") + "</td>");
                }

                if (cfg.show_a)
                {
                    body.Append("<td class='text-center'>" + org.o_box.total_points + "</td>");
                }

                body.Append("<td class='text-center'>"
                    + "<button class='btn btn-sm btn-primary' onclick='DownloadMedalWordClick(this)'"
                    + "   data-l1='" + gp.in_l1 + "' data-l2='" + gp.in_l2 + "'>"
                    + "下載"
                    + "</button>"
                    + "</td>");

                body.Append("</tr>");

                var colspan = 0;
                if (cfg.show_m) colspan += 7;
                if (cfg.show_w) colspan += 7;
                if (cfg.show_x) colspan += 7;


                if (!no_wait)
                {
                    body.Append("<tr id='" + tr_id + "' style='display: none'>");
                    body.Append("<td class='text-center' style='vertical-align: middle'> &nbsp; </td>");
                    body.Append("<td class='text-center' colspan='" + colspan + "'>");
                    body.Append(AppendRanks(cfg, gp, org));
                    body.Append("</td>");

                    body.Append("</tr>");
                }
            }
            body.Append("</tbody>");

            builder.Append("<table id='" + gp.Table_Id + "' class='table table-hover table-bordered table-rwd rwd rwdtable match-table'>");
            builder.Append(head);
            builder.Append(body);
            builder.Append("</table>");
        }

        private StringBuilder AppendRanks(TConfig cfg, TGroup gp, TOrg org)
        {
            StringBuilder builder = new StringBuilder();

            builder.Append("<table class='table table-bordered table-rwd rwd'>");
            builder.Append("  <tbody>");

            var teams = org.Teams
                .OrderBy(x => x.in_gender)
                .ThenBy(x => x.rank).ToList();

            var no = 1;
            for (int i = 0; i < teams.Count; i++)
            {
                var team = teams[i];

                builder.Append("<tr>");
                builder.Append("  <td>" + no + "</td>");
                builder.Append("  <td>" + ProgramName(cfg, team) + "</td>");
                builder.Append("  <td>" + team.player_name + "</td>");
                builder.Append("  <td>名次：" + team.rank + "</td>");
                builder.Append("  <td>積分：" + team.points + "</td>");
                builder.Append("</tr>");

                no++;
            }

            builder.Append("  </tbody>");
            builder.Append("</table>");

            return builder;
        }

        private string ProgramName(TConfig cfg, TTeam team)
        {
            return "<a target='_blank' href='c.aspx?page=In_Competition_Preview.html&method=In_Get_NoAnswer2"
                + "&meeting_id=" + cfg.meeting_id
                + "&program_id=" + team.pg_id + "'>"
                + team.pg_name
                + "</a>";
        }

        private List<TOrg> MapOrgList(TConfig cfg, List<TMedal> medals, TGroup gp)
        {
            List<TOrg> list = new List<TOrg>();

            var players = gp.Values;

            for (int i = 0; i < players.Count; i++)
            {
                var player = players[i];
                var item = player.value;
                string program_short_name = item.getProperty("program_short_name", "");
                string in_stuff_b1 = item.getProperty("in_stuff_b1", "");
                string map_short_org = item.getProperty("map_short_org", "");

                int gcode = 0;
                if (program_short_name.Contains("混"))
                {
                    gcode = 3;
                }
                else if (program_short_name.Contains("男"))
                {
                    gcode = 1;
                }
                else if (program_short_name.Contains("女"))
                {
                    gcode = 2;
                }

                var org = list.Find(x => x.map_short_org == map_short_org);

                if (org == null)
                {
                    org = new TOrg
                    {
                        in_stuff_b1 = in_stuff_b1,
                        map_short_org = map_short_org,
                        medals = new List<TMedal>(),
                        Teams = new List<TTeam>(),
                        o_box = new TRankBox(),
                        m_box = new TRankBox(),
                        w_box = new TRankBox(),
                        x_box = new TRankBox(),
                    };

                    for (int g = 1; g <= 3; g++)
                    {
                        for (int j = 0; j < medals.Count; j++)
                        {
                            var medal = medals[j];
                            org.medals.Add(new TMedal
                            {
                                gcode = g,
                                rank = medal.rank,
                                ranks = medal.ranks,
                                label = medal.label,
                                count = 0,
                                items = new List<Item>(),
                            });
                        }
                    }
                    list.Add(org);
                }

                //找到對應性別的獎牌統計
                var rank_medal = org.medals.Find(x => x.gcode == gcode && x.ranks.Contains(player.rank));
                if (rank_medal == null) throw new Exception("異常");

                rank_medal.count++;
                rank_medal.items.Add(item);

                org.Teams.Add(MapTeam(player));
            }

            for (int i = 0; i < list.Count; i++)
            {
                var org = list[i];

                for (int j = 0; j < org.medals.Count; j++)
                {
                    var medal = org.medals[j];

                    switch (medal.rank)
                    {
                        case 1:
                            org.o_box.rank01_count += medal.count;
                            if (medal.gcode == 1) org.m_box.rank01_count = medal.count;
                            if (medal.gcode == 2) org.w_box.rank01_count = medal.count;
                            if (medal.gcode == 3) org.x_box.rank01_count = medal.count;
                            break;
                        case 2:
                            org.o_box.rank02_count += medal.count;
                            if (medal.gcode == 1) org.m_box.rank02_count = medal.count;
                            if (medal.gcode == 2) org.w_box.rank02_count = medal.count;
                            if (medal.gcode == 3) org.x_box.rank02_count = medal.count;
                            break;
                        case 3:
                            org.o_box.rank03_count += medal.count;
                            if (medal.gcode == 1) org.m_box.rank03_count = medal.count;
                            if (medal.gcode == 2) org.w_box.rank03_count = medal.count;
                            if (medal.gcode == 3) org.x_box.rank03_count = medal.count;
                            break;
                        case 5:
                            org.o_box.rank05_count += medal.count;
                            if (medal.gcode == 1) org.m_box.rank05_count = medal.count;
                            if (medal.gcode == 2) org.w_box.rank05_count = medal.count;
                            if (medal.gcode == 3) org.x_box.rank05_count = medal.count;
                            break;
                        case 7:
                            org.o_box.rank07_count += medal.count;
                            if (medal.gcode == 1) org.m_box.rank07_count = medal.count;
                            if (medal.gcode == 2) org.w_box.rank07_count = medal.count;
                            if (medal.gcode == 3) org.x_box.rank07_count = medal.count;
                            break;
                    }

                    org.o_box.total_count += medal.count;
                    if (medal.gcode == 1) org.m_box.total_count += medal.count;
                    if (medal.gcode == 2) org.w_box.total_count += medal.count;
                    if (medal.gcode == 3) org.x_box.total_count += medal.count;
                }
            }

            foreach (var org in list)
            {
                org.o_box.total_points = 0;
                org.m_box.total_points = 0;
                org.w_box.total_points = 0;
                org.x_box.total_points = 0;
                foreach (var team in org.Teams)
                {
                    org.o_box.total_points += team.points;
                    if (team.in_gender == "M") org.m_box.total_points += team.points;
                    if (team.in_gender == "W") org.w_box.total_points += team.points;
                    if (team.in_gender == "X") org.x_box.total_points += team.points;
                }
            }

            return list.OrderByDescending(x => x.o_box.total_points)
                .ThenByDescending(x => x.o_box.rank01_count)
                .ThenByDescending(x => x.o_box.rank02_count)
                .ThenByDescending(x => x.o_box.rank03_count)
                .ThenByDescending(x => x.o_box.rank05_count)
                .ThenByDescending(x => x.o_box.rank07_count)
                .ToList();
        }

        private TTeam MapTeam(TPlayer player)
        {
            var item = player.value;
            string pg_id = item.getProperty("program_id", "");
            string pg_name = item.getProperty("program_name2", "");
            string in_team = item.getProperty("in_team", "");
            string in_names = item.getProperty("in_names", "");
            string in_show_rank = item.getProperty("in_show_rank", "");

            if (in_team != "") in_names = "(" + in_team + "隊)" + in_names;

            TTeam result = new TTeam
            {
                rank = player.rank,
                pg_id = pg_id,
                pg_name = pg_name,
                player_name = in_names,
                points = player.points,
            };

            if (pg_name.Contains("混合"))
            {
                result.in_gender = "X";
            }
            else if (pg_name.Contains("男"))
            {
                result.in_gender = "M";
            }
            else if (pg_name.Contains("女"))
            {
                result.in_gender = "W";
            }

            return result;
        }

        private Item GetScores(TConfig cfg)
        {
            string property = "in_l1";
            if (cfg.has_mix)
            {
                property = "in_l2";
            }

            string gender_condition = "";
            if (cfg.in_gender != "")
            {
                if (cfg.in_gender == "混合")
                {
                    gender_condition = "AND t1." + property + " LIKE N'%" + cfg.in_gender + "%'";
                }
                else
                {
                    gender_condition = "AND t1." + property + " LIKE N'%" + cfg.in_gender + "%' AND t1." + property + " NOT LIKE '%混合%'";
                }
            }

            string sql = @"
                SELECT
	                t2.in_stuff_b1
	                , t2.in_current_org
	                , t2.map_short_org
	                , t2.in_team
	                , t2.in_names
	                , t2.in_final_rank 
	                , t2.in_show_rank 
	                , t1.id               AS 'program_id'
	                , t1.in_name2         AS 'program_name2'
	                , t1.in_short_name    AS 'program_short_name'
	                , t1.in_l1
	                , t1.in_l2
	                , t1.in_l3
                FROM 
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
	                VU_MEETING_PTEAM t2
	                ON t2.source_id = t1.id
	            LEFT OUTER JOIN
	                IN_MEETING_PEVENT t3 WITH(NOLOCK)
	                ON t3.id = t2.in_event
                WHERE 
	                t1.in_meeting = '{#meeting_id}'
	                AND ISNULL(t2.in_final_rank, 0) > 0
	                {#gender_condition}
                ORDER BY
	                t1.in_sort_order
	                , t2.in_final_rank
	                , t3.in_tree_id
	                , t2.in_foot
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#gender_condition}", gender_condition);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        private class TOrg
        {
            public string in_stuff_b1 { get; set; }
            public string map_short_org { get; set; }
            public List<TMedal> medals { get; set; }

            public TRankBox o_box { get; set; }
            public TRankBox m_box { get; set; }
            public TRankBox w_box { get; set; }
            public TRankBox x_box { get; set; }

            public List<TTeam> Teams { get; set; }
        }

        private class TMedal
        {
            public int gcode { get; set; }
            public int rank { get; set; }
            public int[] ranks { get; set; }
            public string label { get; set; }
            public string prop { get; set; }
            public int count { get; set; }
            public List<Item> items { get; set; }
        }

        private class TRankBox
        {
            public int rank01_count { get; set; }
            public int rank02_count { get; set; }
            public int rank03_count { get; set; }
            public int rank05_count { get; set; }
            public int rank07_count { get; set; }
            public int total_count { get; set; }
            public decimal total_points { get; set; }
        }

        private class TTeam
        {
            public int rank { get; set; }
            public string pg_id { get; set; }
            public string pg_name { get; set; }
            public string player_name { get; set; }
            public string in_gender { get; set; }
            public decimal points { get; set; }
        }

        #endregion 名次統計

        #region 名次清單

        private void ExportRankXlsx(TConfig cfg, Item itmReturn)
        {
            Item items = GetRankList(cfg);

            if (items.getResult() == "")
            {
                throw new Exception("該組尚無資料再請確認!");
            }

            var exp = ExportInfo(cfg, "rank_path");

            var groups = MapGroup(cfg, items);
            //試算表
            Spire.Xls.Workbook workbook = new Spire.Xls.Workbook();

            foreach (var gp in groups)
            {
                AppendRankTable1233(cfg, workbook, gp);
                AppendRankTable(cfg, workbook, gp);
                if (cfg.show_x)
                {
                    AppendRankTable(cfg, workbook, gp, "混合");
                }
                AppendRankTable(cfg, workbook, gp, "男");
                AppendRankTable(cfg, workbook, gp, "女");
            }

            workbook.Worksheets[0].Remove();
            workbook.Worksheets[0].Remove();
            workbook.Worksheets[0].Remove();

            string ext_name = ".xlsx";
            string xls_name = cfg.mt_title + "_名次總表_" + DateTime.Now.ToString("MMdd_HHmmss");

            if (cfg.in_date != "")
            {
                xls_name = cfg.itmMeeting.getProperty("in_title", "")
                    + "_" + cfg.in_date.Replace("-", "")
                    + "_名次總表_" + DateTime.Now.ToString("MMdd_HHmmss");
            }

            string xls_file = exp.export_Path + "\\" + xls_name + ext_name;
            string xls_url = xls_name + ext_name;

            workbook.SaveToFile(xls_file, Spire.Xls.ExcelVersion.Version2013);

            itmReturn.setProperty("xls_name", xls_name + ".xlsx");
        }

        private void AppendRankTable1233(TConfig cfg, Spire.Xls.Workbook workbook, TGroup gp)
        {
            Spire.Xls.Worksheet sheet = workbook.CreateEmptySheet();
            sheet.Name = gp.sheetName + "1233(A3)";
            sheet.PageSetup.PaperSize = Spire.Xls.PaperSizeType.PaperA3;

            //頁面配置-邊界
            sheet.PageSetup.TopMargin = 0.5;
            // sheet.PageSetup.BottomMargin = 0.5;
            // sheet.PageSetup.LeftMargin = 0.5;
            // sheet.PageSetup.RightMargin = 0.5;

            //列印標題
            sheet.PageSetup.PrintTitleRows = "$1:$2";

            var mt_mr = sheet.Range["A1:G1"];
            mt_mr.Merge();
            mt_mr.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
            mt_mr.Text = cfg.mt_title;
            mt_mr.Style.Font.Size = 16;
            mt_mr.Style.Font.IsBold = true;
            mt_mr.RowHeight = 20;

            var rpt_mr = sheet.Range["A2:G2"];
            rpt_mr.Merge();
            rpt_mr.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
            rpt_mr.Text = "名次清單";
            rpt_mr.Style.Font.Size = 16;
            rpt_mr.RowHeight = 20;

            //混合
            if (cfg.show_x)
            {
                List<TField> fields_x = new List<TField>();
                fields_x.Add(new TField { ci = 1, cs = "A", title = "名次", getValue = RankName, css = TCss.Center, width = 10 });
                fields_x.Add(new TField { ci = 2, cs = "B", title = "單位", getValue = OrgShortName, css = TCss.Center, width = 28 });
                fields_x.Add(new TField { ci = 3, cs = "C", title = "姓名/隊別", getValue = PlayerName, css = TCss.Center, width = 20 });

                var programs_x = gp.programs.FindAll(x => x.name.Contains("混合"));
                AppendHalfSheet1233(cfg, sheet, fields_x, programs_x, 14);
            }

            //男子
            List<TField> fields_m = new List<TField>();
            fields_m.Add(new TField { ci = 1, cs = "A", title = "名次", getValue = RankName, css = TCss.Center, width = 10 });
            fields_m.Add(new TField { ci = 2, cs = "B", title = "單位", getValue = OrgShortName, css = TCss.Center, width = 28 });
            fields_m.Add(new TField { ci = 3, cs = "C", title = "姓名/隊別", getValue = PlayerName, css = TCss.Center, width = 20 });

            var programs_m = gp.programs.FindAll(x => x.name.Contains("男") && !x.name.Contains("混合"));
            AppendHalfSheet1233(cfg, sheet, fields_m, programs_m, 14);


            //女子
            List<TField> fields_w = new List<TField>();
            fields_w.Add(new TField { ci = 5, cs = "E", title = "名次", getValue = RankName, css = TCss.Center, width = 10 });
            fields_w.Add(new TField { ci = 6, cs = "F", title = "單位", getValue = OrgShortName, css = TCss.Center, width = 28 });
            fields_w.Add(new TField { ci = 7, cs = "G", title = "姓名/隊別", getValue = PlayerName, css = TCss.Center, width = 20 });

            var programs_w = gp.programs.FindAll(x => x.name.Contains("女") && !x.name.Contains("混合"));
            AppendHalfSheet1233(cfg, sheet, fields_w, programs_w, 14);
        }

        private void AppendRankTable(TConfig cfg, Spire.Xls.Workbook workbook, TGroup gp)
        {
            Spire.Xls.Worksheet sheet = workbook.CreateEmptySheet();
            sheet.Name = gp.sheetName + "總表(A3)";
            sheet.PageSetup.PaperSize = Spire.Xls.PaperSizeType.PaperA3;

            //頁面配置-邊界
            sheet.PageSetup.TopMargin = 0.5;
            // sheet.PageSetup.BottomMargin = 0.5;
            // sheet.PageSetup.LeftMargin = 0.5;
            // sheet.PageSetup.RightMargin = 0.5;

            //列印標題
            sheet.PageSetup.PrintTitleRows = "$1:$2";

            var mt_mr = sheet.Range["A1:G1"];
            mt_mr.Merge();
            mt_mr.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
            mt_mr.Text = cfg.mt_title;
            mt_mr.Style.Font.Size = 16;
            mt_mr.Style.Font.IsBold = true;
            mt_mr.RowHeight = 20;

            var rpt_mr = sheet.Range["A2:G2"];
            rpt_mr.Merge();
            rpt_mr.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
            rpt_mr.Text = "名次清單";
            rpt_mr.Style.Font.Size = 16;
            rpt_mr.RowHeight = 20;

            //混合
            if (cfg.show_x)
            {
                List<TField> fields_x = new List<TField>();
                fields_x.Add(new TField { ci = 1, cs = "A", title = "名次", getValue = RankName, css = TCss.Center, width = 10 });
                fields_x.Add(new TField { ci = 2, cs = "B", title = "單位", getValue = OrgShortName, css = TCss.Center, width = 28 });
                fields_x.Add(new TField { ci = 3, cs = "C", title = "姓名/隊別", getValue = PlayerName, css = TCss.Center, width = 20 });

                var programs_x = gp.programs.FindAll(x => x.name.Contains("混合"));
                AppendHalfSheet(cfg, sheet, fields_x, programs_x, 14);
            }

            //男子
            List<TField> fields_m = new List<TField>();
            fields_m.Add(new TField { ci = 1, cs = "A", title = "名次", getValue = RankName, css = TCss.Center, width = 10 });
            fields_m.Add(new TField { ci = 2, cs = "B", title = "單位", getValue = OrgShortName, css = TCss.Center, width = 28 });
            fields_m.Add(new TField { ci = 3, cs = "C", title = "姓名/隊別", getValue = PlayerName, css = TCss.Center, width = 20 });

            var programs_m = gp.programs.FindAll(x => x.name.Contains("男") && !x.name.Contains("混合"));
            AppendHalfSheet(cfg, sheet, fields_m, programs_m, 14);


            //女子
            List<TField> fields_w = new List<TField>();
            fields_w.Add(new TField { ci = 5, cs = "E", title = "名次", getValue = RankName, css = TCss.Center, width = 10 });
            fields_w.Add(new TField { ci = 6, cs = "F", title = "單位", getValue = OrgShortName, css = TCss.Center, width = 28 });
            fields_w.Add(new TField { ci = 7, cs = "G", title = "姓名/隊別", getValue = PlayerName, css = TCss.Center, width = 20 });

            var programs_w = gp.programs.FindAll(x => x.name.Contains("女") && !x.name.Contains("混合"));
            AppendHalfSheet(cfg, sheet, fields_w, programs_w, 14);
        }

        private void AppendRankTable(TConfig cfg, Spire.Xls.Workbook workbook, TGroup gp, string in_gender)
        {
            Spire.Xls.Worksheet sheet = workbook.CreateEmptySheet();
            sheet.Name = gp.sheetName + in_gender;

            //將所有欄放入單一頁面
            sheet.PageSetup.FitToPagesWide = 1;
            sheet.PageSetup.FitToPagesTall = 0;
            //頁面配置-邊界
            sheet.PageSetup.TopMargin = 0.5;
            // sheet.PageSetup.BottomMargin = 0.5;
            // sheet.PageSetup.LeftMargin = 0.5;
            // sheet.PageSetup.RightMargin = 0.5;

            //列印標題
            sheet.PageSetup.PrintTitleRows = "$1:$2";

            var mt_mr = sheet.Range["A1:C1"];
            mt_mr.Merge();
            mt_mr.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
            mt_mr.Text = cfg.mt_title;
            mt_mr.Style.Font.Size = 16;
            mt_mr.Style.Font.IsBold = true;
            mt_mr.RowHeight = 20;

            var rpt_mr = sheet.Range["A2:C2"];
            rpt_mr.Merge();
            rpt_mr.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
            rpt_mr.Text = "名次清單";
            rpt_mr.Style.Font.Size = 16;
            rpt_mr.RowHeight = 20;

            List<TField> fields_m = new List<TField>();
            fields_m.Add(new TField { ci = 1, cs = "A", title = "名次", getValue = RankName, css = TCss.Center, width = 12 });
            fields_m.Add(new TField { ci = 2, cs = "B", title = "單位", getValue = OrgShortName, css = TCss.Center, width = 50 });
            fields_m.Add(new TField { ci = 3, cs = "C", title = "姓名/隊別", getValue = PlayerName, css = TCss.Center, width = 24 });

            var programs_m = gp.programs.FindAll(x => x.name.Contains(in_gender));
            AppendHalfSheet(cfg, sheet, fields_m, programs_m, fsize: 16, need_empty_row: false);
        }

        private void AppendHalfSheet1233(TConfig cfg, Spire.Xls.Worksheet sheet, List<TField> fields, List<TProgram> programs, int fsize = 0, bool need_empty_row = true)
        {
            int mnRow = 4;
            int wsRow = 4;

            //資料容器
            Item item = cfg.inn.newItem();

            var fs = fields.First();
            var fe = fields.Last();

            foreach (var program in programs)
            {
                var teams = program.players;

                var pg_mr = sheet.Range[fs.cs + wsRow + ":" + fe.cs + wsRow];
                pg_mr.Merge();
                pg_mr.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                pg_mr.Text = program.name + " (" + program.rank_count + ")";
                pg_mr.Style.Font.Size = 12;
                pg_mr.RowHeight = 16;

                if (fsize > 0) pg_mr.Style.Font.Size = fsize;
                if (fsize >= 16) pg_mr.RowHeight = 24;

                wsRow++;

                //標題列
                SetHeadRow(sheet, wsRow, fields, 16, need_color: false);
                wsRow++;

                var rank_teams = new List<Item>();
                foreach (var team in teams)
                {
                    var itemA = team.value;
                    string in_show_rank = itemA.getProperty("in_show_rank", "0");
                    switch (in_show_rank)
                    {
                        case "1":
                        case "2":
                        case "3":
                        case "4":
                            rank_teams.Add(itemA);
                            break;
                    }
                }

                //維持八列
                var max_row = 4;
                var max_idx = rank_teams.Count - 1;

                for (int i = 0; i < max_row; i++)
                {
                    if (i <= max_idx)
                    {
                        var team = rank_teams[i];
                        SetItemCell(cfg, sheet, wsRow, team, fields, fsize);
                        wsRow++;
                    }
                }

                var rs = wsRow - max_row - 2;
                var re = wsRow - 1;
                SetRangeBorder(sheet, fs.cs + rs + ":" + fe.cs + re);

                wsRow++;
            }

            ////設定格線
            //SetRangeBorder(sheet, fields, mnRow, wsRow);

            //設定欄寬
            SetColumnWidth(sheet, fields);
        }

        private void AppendHalfSheet(TConfig cfg, Spire.Xls.Worksheet sheet, List<TField> fields, List<TProgram> programs, int fsize = 0, bool need_empty_row = true)
        {
            int mnRow = 4;
            int wsRow = 4;

            //資料容器
            Item item = cfg.inn.newItem();

            var fs = fields.First();
            var fe = fields.Last();

            foreach (var program in programs)
            {
                var players = program.players;

                var pg_mr = sheet.Range[fs.cs + wsRow + ":" + fe.cs + wsRow];
                pg_mr.Merge();
                pg_mr.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                pg_mr.Text = program.name + " (" + program.rank_count + ")";
                pg_mr.Style.Font.Size = 12;
                pg_mr.RowHeight = 16;

                if (fsize > 0) pg_mr.Style.Font.Size = fsize;
                if (fsize >= 16) pg_mr.RowHeight = 20;

                wsRow++;

                //標題列
                SetHeadRow(sheet, wsRow, fields, fsize, need_color: false);
                wsRow++;

                //foreach (var team in teams)
                //{
                //    SetItemCell(cfg, sheet, wsRow, team, fields);
                //    wsRow++;
                //}

                //維持八列
                var max_row = 8;
                var max_idx = players.Count - 1;

                if (!need_empty_row)
                {
                    //不維持
                    max_row = players.Count;
                }

                for (int i = 0; i < max_row; i++)
                {
                    if (i <= max_idx)
                    {
                        var team = players[i];
                        var itemB = team.value;
                        SetItemCell(cfg, sheet, wsRow, itemB, fields, fsize);
                    }
                    wsRow++;
                }

                var rs = wsRow - max_row - 2;
                var re = wsRow - 1;
                SetRangeBorder(sheet, fs.cs + rs + ":" + fe.cs + re);

                wsRow++;
            }

            ////設定格線
            //SetRangeBorder(sheet, fields, mnRow, wsRow);

            //設定欄寬
            SetColumnWidth(sheet, fields);
        }

        private string RankName(TConfig cfg, TField field, Item item)
        {
            //string[] rank_arr = new string[] { "", "一", "二", "三", "四", "五", "五", "七", "七", "" };
            return item.getProperty("in_show_rank", "");
        }

        private string OrgLongName(TConfig cfg, TField field, Item item)
        {
            string map_short_org = item.getProperty("map_short_org", "");
            string in_current_org = item.getProperty("in_current_org", "");
            string in_stuff_b1 = item.getProperty("in_stuff_b1", "");
            if (in_current_org == "") in_current_org = map_short_org;

            if (in_stuff_b1 == "")
            {
                return in_current_org;
            }
            else
            {
                return in_current_org.Replace(in_stuff_b1, "");
            }
        }

        private string OrgShortName(TConfig cfg, TField field, Item item)
        {
            string map_short_org = item.getProperty("map_short_org", "");
            string in_current_org = item.getProperty("in_current_org", "");
            string in_stuff_b1 = item.getProperty("in_stuff_b1", "");
            return map_short_org;
        }


        private string PlayerName(TConfig cfg, TField field, Item item)
        {
            return item.getProperty("in_name", "");
        }

        private List<TGroup> MapGroup(TConfig cfg, Item items)
        {
            var result = new List<TGroup>();
            var programs = MapProgram(cfg, items);

            foreach (var program in programs)
            {
                program.sheetName = GetSheetName(cfg, program);
                var gp = result.Find(x => x.sheetName == program.sheetName);
                if (gp == null)
                {
                    gp = new TGroup
                    {
                        Id = (result.Count + 1).ToString(),
                        sheetName = program.sheetName,
                        programs = new List<TProgram>(),
                        Values = new List<TPlayer>(),
                        in_l1 = program.in_l1,
                        in_l2 = program.in_l2,
                    };
                    gp.Tab_Id = "tab_a_" + gp.Id;
                    gp.Div_Id = "tab_div_" + gp.Id;
                    gp.Table_Id = "tab_div_tb_" + gp.Id;
                    result.Add(gp);
                }
                gp.programs.Add(program);
                gp.Values.AddRange(program.players);
            }
            return result;
        }

        private List<TProgram> MapProgram(TConfig cfg, Item items)
        {
            var programs = new List<TProgram>();
            var count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var id = item.getProperty("program_id", "");
                var program = programs.Find(x => x.id == id);
                if (program == null)
                {
                    program = new TProgram
                    {
                        id = id,
                        name = item.getProperty("program_name2", ""),
                        short_name = item.getProperty("program_short_name", ""),
                        in_l1 = item.getProperty("in_l1", ""),
                        in_l2 = item.getProperty("in_l2", ""),
                        in_l3 = item.getProperty("in_l3", ""),
                        rank_count = item.getProperty("program_rank_count", "0"),
                        players = new List<TPlayer>(),
                    };
                    programs.Add(program);
                }

                var player = new TPlayer
                {
                    rank = GetIntVal(item.getProperty("in_final_rank", "0")),
                    value = item,
                };
                player.points = cfg.FindRankPoints(id, player.rank);
                program.players.Add(player);
            }

            return programs;
        }

        private string GetSheetName(TConfig cfg, TProgram pg)
        {
            var value = pg.short_name;
            if (pg.in_l1 == "個人組")
            {
                if (value.StartsWith("小")) return "國小組-自由式";
                if (value.StartsWith("國")) return "國中組-自由式";
                if (value.StartsWith("高")) return "高中組-自由式";
                if (value.StartsWith("大")) return "大專社會-自由式";
                if (value.StartsWith("社")) return "社會-自由式";
                if (value.StartsWith("公")) return "公開組-自由式";
                if (value.StartsWith("一")) return "一般組-自由式";
            }
            else if (pg.in_l2.Contains("希羅"))
            {
                if (value.StartsWith("小")) return "國小組-希羅式";
                if (value.StartsWith("國")) return "國中組-希羅式";
                if (value.StartsWith("高")) return "高中組-希羅式";
                if (value.StartsWith("大")) return "大專社會-希羅式";
                if (value.StartsWith("社")) return "社會-希羅式";
                if (value.StartsWith("公")) return "公開組-希羅式";
                if (value.StartsWith("一")) return "一般組-希羅式";
            }
            else if (pg.in_l2.Contains("自由"))
            {
                if (value.StartsWith("小")) return "國小組-自由式";
                if (value.StartsWith("國")) return "國中組-自由式";
                if (value.StartsWith("高")) return "高中組-自由式";
                if (value.StartsWith("大")) return "大專社會-自由式";
                if (value.StartsWith("社")) return "社會-自由式";
                if (value.StartsWith("公")) return "公開組-自由式";
                if (value.StartsWith("一")) return "一般組-自由式";
            }
            else
            {
                switch (value)
                {
                    case "大社男二": return "大專社會組-自由式";
                    case "大社男三": return "大專社會組-自由式";
                    case "大社男四": return "大專社會組-自由式";
                    case "大社男五": return "大專社會組-自由式";
                    case "大社男六": return "大專社會組-自由式";
                    case "大社男七": return "大專社會組-自由式";
                    case "大社男八": return "大專社會組-自由式";
                    case "大社男九": return "大專社會組-自由式";
                    case "大社男十": return "大專社會組-自由式";
                    case "大社女一": return "大專社會組-自由式";
                    case "大社女二": return "大專社會組-自由式";
                    case "大社女三": return "大專社會組-自由式";
                    case "大社女四": return "大專社會組-自由式";
                    case "大社女五": return "大專社會組-自由式";
                    case "大社女六": return "大專社會組-自由式";
                    case "大社女七": return "大專社會組-自由式";
                    case "大社女八": return "大專社會組-自由式";
                    case "大社女十": return "大專社會組-自由式";
                    case "高男三": return "高中組-自由式";
                    case "高男四": return "高中組-自由式";
                    case "高男六": return "高中組-自由式";
                    case "高男八": return "高中組-自由式";
                    case "高男十": return "高中組-自由式";
                    case "高女一": return "高中組-自由式";
                    case "高女二": return "高中組-自由式";
                    case "高女三": return "高中組-自由式";
                    case "高女四": return "高中組-自由式";
                    case "高女五": return "高中組-自由式";
                    case "高女七": return "高中組-自由式";
                    case "高女八": return "高中組-自由式";
                    case "國男一": return "國中組-自由式";
                    case "國男二": return "國中組-自由式";
                    case "國男三": return "國中組-自由式";
                    case "國男四": return "國中組-自由式";
                    case "國男五": return "國中組-自由式";
                    case "國男六": return "國中組-自由式";
                    case "國男七": return "國中組-自由式";
                    case "國男八": return "國中組-自由式";
                    case "國男九": return "國中組-自由式";
                    case "國男十": return "國中組-自由式";
                    case "國女一": return "國中組-自由式";
                    case "國女三": return "國中組-自由式";
                    case "國女四": return "國中組-自由式";
                    case "國女五": return "國中組-自由式";
                    case "國女六": return "國中組-自由式";
                    case "國女七": return "國中組-自由式";
                    case "國女八": return "國中組-自由式";
                    case "國女九": return "國中組-自由式";
                    case "國女十": return "國中組-自由式";
                    case "小男一": return "國小組-自由式";
                    case "小男二": return "國小組-自由式";
                    case "小男三": return "國小組-自由式";
                    case "小男四": return "國小組-自由式";
                    case "小男五": return "國小組-自由式";
                    case "小男六": return "國小組-自由式";
                    case "小男七": return "國小組-自由式";
                    case "小男八": return "國小組-自由式";
                    case "小男九": return "國小組-自由式";
                    case "小男十": return "國小組-自由式";
                    case "小男十一": return "國小組-自由式";
                    case "小女一": return "國小組-自由式";
                    case "小女二": return "國小組-自由式";
                    case "小女三": return "國小組-自由式";
                    case "小女四": return "國小組-自由式";
                    case "小女五": return "國小組-自由式";
                    case "小女六": return "國小組-自由式";
                    case "小女七": return "國小組-自由式";
                    case "小女八": return "國小組-自由式";
                    case "小女九": return "國小組-自由式";
                    case "小女十": return "國小組-自由式";
                    case "小女十一": return "國小組-自由式";
                }
                if (value.Contains("小A")) return "國小高年級組";
                if (value.Contains("小B")) return "國小中年級組";
                if (value.Contains("小C")) return "國小低年級組";
                if (value.Contains("小高")) return "國小高年級組";
                if (value.Contains("小中")) return "國小中年級組";
                if (value.Contains("小低")) return "國小低年級組";
            }
            return "名次總表";
        }

        private Item GetRankList(TConfig cfg)
        {
            string condition = "";

            if (cfg.program_id != "")
            {
                condition = "AND t1.source_id = '" + cfg.program_id + "'";
            }
            else if (cfg.in_date != "")
            {
                condition = "AND t3.in_fight_day = '" + cfg.in_date + "'";
            }

            string sql = @"
                SELECT
                    t1.in_current_org
                    , t1.in_name
                    , t1.in_sno
                    , t1.in_final_rank
                    , t1.in_show_rank
                    , t1.in_sign_no
                    , t1.in_section_no
                    , t1.map_short_org
                    , t1.map_org_name
                    , t1.in_stuff_b1
                    , t3.id             AS 'program_id'
                    , t3.in_name        AS 'program_name'
                    , t3.in_name2       AS 'program_name2'
                    , t3.in_name3       AS 'program_name3'
                    , t3.in_display     AS 'program_display'
                    , t3.in_short_name  AS 'program_short_name'
                    , t3.in_sort_order  AS 'program_sort'
                    , t3.in_rank_count  AS 'program_rank_count'
                FROM 
                    VU_MEETING_PTEAM t1
                LEFT OUTER JOIN
                    IN_MEETING_PEVENT t2 WITH(NOLOCK)
                    ON t2.id = t1.in_event
                INNER JOIN
                    IN_MEETING_PROGRAM t3 WITH(NOLOCK)
                    ON t3.id = t1.source_id
                WHERE
                    t1.in_meeting = '{#meeting_id}'
                    {#condition}
	                AND ISNULL(t1.in_final_rank, 0) > 0
                ORDER BY
                    t3.in_sort_order
                    , t1.in_final_rank
                    , t2.in_tree_id
                    , t1.in_foot
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                      .Replace("{#condition}", condition);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        private class TGroup
        {
            public string sheetName { get; set; }
            public List<TProgram> programs { get; set; }
            public List<TPlayer> Values { get; set; }

            public string Id { get; set; }
            public string Tab_Id { get; set; }
            public string Div_Id { get; set; }
            public string Table_Id { get; set; }
            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
        }

        private class TProgram
        {
            public string id { get; set; }
            public string name { get; set; }
            public string short_name { get; set; }
            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }
            public string rank_count { get; set; }
            public List<TPlayer> players { get; set; }
            public string sheetName { get; set; }
        }

        private class TPlayer
        {
            public int rank { get; set; }
            public decimal points { get; set; }
            public Item value { get; set; }
        }

        #endregion 名次清單

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }

            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }

            public string meeting_id { get; set; }
            public string in_date { get; set; }
            public string program_id { get; set; }
            public string in_gender { get; set; }
            public string scene { get; set; }

            public bool has_mix { get; set; }

            public bool show_a { get; set; }
            public bool show_m { get; set; }
            public bool show_w { get; set; }
            public bool show_x { get; set; }

            public int Rank1Points { get; set; }
            public int Rank2Points { get; set; }
            public int Rank3Points { get; set; }
            public int Rank5Points { get; set; }
            public int Rank7Points { get; set; }
            public bool NeedDivide { get; set; }

            public Item itmMeeting { get; set; }
            public string mt_title { get; set; }

            public string[] CharSet { get; set; }
            public bool IsSumReport { get; set; }

            public Dictionary<string, TSect> sectPointsMap { get; set; }

            public decimal FindRankPoints(string program_id, int rank)
            {
                if (sectPointsMap == null || sectPointsMap.Count == 0) return 0;

                var map = sectPointsMap;
                if (!map.ContainsKey(program_id)) return 0;

                var sect = map[program_id];
                switch (rank)
                {
                    case 1: return sect.r1.points;
                    case 2: return sect.r2.points;
                    case 3: return sect.r3.points;
                    case 5: return sect.r5.points;
                    case 7: return sect.r7.points;
                    default: return 0;
                }
            }
        }

        private void AppendSectPointsMap(TConfig cfg)
        {
            var map = new Dictionary<string, TSect>();

            var sql = @"
                SELECT 
	                source_id
	                , in_current_org
	                , in_name
	                , in_final_rank
                FROM
	                IN_MEETING_PTEAM WITH(NOLOCK)
                WHERE
	                in_meeting = '{#meeting_id}'
	                AND ISNULL(in_final_rank, 0) > 0
	                AND ISNULL(in_type, '') IN ('', 'p')
                ORDER BY
	                source_id
	                , in_final_rank
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var source_id = item.getProperty("source_id", "");
                var final_rank = GetIntVal(item.getProperty("in_final_rank", "0"));
                var sect = default(TSect);
                if (!map.ContainsKey(source_id))
                {
                    sect = new TSect
                    {
                        r1 = new TSectRank { sum = cfg.Rank1Points, rank = 1, count = 0, points = 0 },
                        r2 = new TSectRank { sum = cfg.Rank2Points, rank = 2, count = 0, points = 0 },
                        r3 = new TSectRank { sum = cfg.Rank3Points, rank = 3, count = 0, points = 0 },
                        r5 = new TSectRank { sum = cfg.Rank5Points, rank = 5, count = 0, points = 0 },
                        r7 = new TSectRank { sum = cfg.Rank7Points, rank = 7, count = 0, points = 0 },
                    };
                    map.Add(source_id, sect);
                }
                else
                {
                    sect = map[source_id];
                }

                switch (final_rank)
                {
                    case 1:
                        sect.r1.count++;
                        if (cfg.NeedDivide)
                        {
                            if (sect.r1.count == 1)
                            {
                                sect.r1.points = 4;
                            }
                            else if (sect.r1.count == 2)
                            {
                                sect.r1.points = 2;
                            }
                        }
                        else
                        {
                            sect.r1.points = cfg.Rank1Points;
                        }
                        break;

                    case 2:
                        sect.r2.count++;
                        if (cfg.NeedDivide)
                        {
                            if (sect.r2.count == 1)
                            {
                                sect.r2.points = 2;
                            }
                            else if (sect.r2.count == 2)
                            {
                                sect.r2.points = 1;
                            }
                        }
                        else
                        {
                            sect.r2.points = cfg.Rank2Points;
                        }
                        break;

                    case 3:
                        sect.r3.count++;
                        if (cfg.NeedDivide)
                        {
                            if (sect.r3.count == 1)
                            {
                                sect.r3.points = 1;
                            }
                            else if (sect.r3.count == 2)
                            {
                                sect.r3.points = 0.5m;
                            }
                        }
                        else
                        {
                            sect.r3.points = cfg.Rank3Points;
                        }
                        break;

                    case 5:
                        sect.r5.count++;
                        sect.r5.points = 0;
                        break;

                    case 7:
                        sect.r7.count++;
                        sect.r7.points = 0;
                        break;
                }
            }

            cfg.sectPointsMap = map;
        }

        private class TSect
        {
            public string program_id { get; set; }
            public TSectRank r1 { get; set; }
            public TSectRank r2 { get; set; }
            public TSectRank r3 { get; set; }
            public TSectRank r5 { get; set; }
            public TSectRank r7 { get; set; }
        }

        private class TSectRank
        {
            public int rank { get; set; }
            public int count { get; set; }
            public decimal sum { get; set; }
            public decimal points { get; set; }
        }

        #region Xlsx

        private class TExport
        {
            public string template_Path { get; set; }
            public string export_Path { get; set; }
        }

        private class TField
        {
            public string title { get; set; }
            public string property { get; set; }
            public TCss css { get; set; }
            public int ci { get; set; }
            public string cs { get; set; }
            public int width { get; set; }
            public Func<TConfig, TField, Item, string> getValue { get; set; }
            public bool is_merge { get; set; }
        }

        private enum TCss
        {
            None = 0,
            Center = 10,
            Text = 100,
            Double = 200,
        }

        private string[] GetCharSet()
        {
            return new string[] { "", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
        }

        private TExport ExportInfo(TConfig cfg, string in_name)
        {
            var itmXls = cfg.inn.applyMethod("In_Meeting_Excel_Path", "<in_name>" + in_name + "</in_name>");
            var Export_Path = itmXls.getProperty("export_path", "");
            var Template_Path = itmXls.getProperty("template_path", "");
            var result = new TExport
            {
                template_Path = Template_Path,
                export_Path = Export_Path,
            };
            return result;
        }

        //設定資料列
        private void SetItemCell(TConfig cfg, Spire.Xls.Worksheet sheet, int wsRow, Item item, List<TField> fields, int fsize = 0)
        {
            foreach (var field in fields)
            {
                var cr = field.cs + wsRow;
                var va = " ";
                if (field.getValue != null)
                {
                    va = field.getValue(cfg, field, item);
                }
                else if (field.property != "")
                {
                    va = item.getProperty(field.property, "");
                }
                SetCell(sheet, cr, va, field.css, fsize);
            }
        }

        //設定資料格
        private void SetCell(Spire.Xls.Worksheet sheet, string cr, string value, TCss css, int fsize = 0)
        {
            var range = sheet.Range[cr];
            if (fsize > 0) range.Style.Font.Size = fsize;
            if (fsize >= 16) range.RowHeight = 20;
            switch (css)
            {
                case TCss.None:
                    range.Text = value;
                    break;
                case TCss.Center:
                    range.Text = value;
                    range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                    break;
                case TCss.Text:
                    range.Text = "'" + value;
                    range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                    break;
                case TCss.Double:
                    var v = GetDblVal(value);
                    if (v > 0)
                    {
                        range.NumberValue = v;
                        range.NumberFormat = "0";
                        range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Right;
                        range.Style.Font.Color = System.Drawing.Color.Red;
                    }
                    break;
            }
        }

        //設定標題列
        private void SetHead(Spire.Xls.Worksheet sheet, string cr, string value, int fsize = 0, bool need_color = true)
        {
            var range = sheet.Range[cr];
            range.Text = value;
            range.Style.Font.IsBold = true;

            if (fsize > 0) range.Style.Font.Size = fsize;
            if (fsize >= 16) range.RowHeight = 20;

            if (need_color)
            {
                range.Style.Font.Color = System.Drawing.Color.White;
                range.Style.Color = System.Drawing.Color.FromArgb(41, 92, 144);//#295C90
            }

            range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
            range.Style.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;
        }

        //設定格線
        private void SetRangeBorder(Spire.Xls.Worksheet sheet, string pos)
        {
            var range = sheet.Range[pos];
            range.BorderInside(Spire.Xls.LineStyleType.Thin, System.Drawing.Color.Black);
            range.BorderAround(Spire.Xls.LineStyleType.Thin, System.Drawing.Color.Black);
        }

        //設定格線
        private void SetRangeBorder(Spire.Xls.Worksheet sheet, List<TField> fields, int mnRow, int wsRow)
        {
            var tbl_ps = fields.First().cs + mnRow;
            var tbl_pe = fields.Last().cs + (wsRow - 1);
            SetRangeBorder(sheet, tbl_ps + ":" + tbl_pe);
        }

        private void MapCharSet(TConfig cfg, List<TField> fields)
        {
            foreach (var field in fields)
            {
                field.cs = cfg.CharSet[field.ci];
            }
        }

        private void SetHeadRow(Spire.Xls.Worksheet sheet, int wsRow, List<TField> fields, int fsize = 0, bool need_color = true)
        {
            foreach (var field in fields)
            {
                string cr = field.cs + wsRow;
                SetHead(sheet, cr, field.title, fsize, need_color);
            }
        }

        private void SetColumnWidth(Spire.Xls.Worksheet sheet, List<TField> fields)
        {
            foreach (var field in fields)
            {
                sheet.Columns[field.ci - 1].ColumnWidth = field.width;
            }
        }

        #endregion Xlsx

        private int GetIntVal(string value, int def = 0)
        {
            int result = def;
            int.TryParse(value, out result);
            return result;
        }

        private double GetDblVal(string value, int def = 0)
        {
            double result = def;
            double.TryParse(value, out result);
            return result;
        }

        private string FindOrgGroupRank(TConfig cfg, Dictionary<string, string> map, string org, int defaultRank)
        {
            if (!map.ContainsKey(org)) return defaultRank.ToString();
            return map[org];
        }

        private Dictionary<string, string> GetOrgGroupRankList(TConfig cfg, TGroup gp)
        {
            var map = new Dictionary<string, string>();
            var in_group = gp.in_l1 + gp.in_l2;

            var sql = @"
                SELECT
                    in_org
                    , in_rank
                FROM
	                IN_ORG_SCORE WITH(NOLOCK)
                WHERE
	                in_meeting = '{#meeting_id}'
	                AND in_group = '{#in_group}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_group}", in_group);

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var key = item.getProperty("in_org", "");
                var in_rank = item.getProperty("in_rank", "");
                if (in_rank == "0") in_rank = "";
                if (!map.ContainsKey(key))
                {
                    map.Add(key, in_rank);
                }
            }
            return map;
        }
    }
}