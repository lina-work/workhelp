﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Wrestling.Common
{
    public class In_Local_Sync : Item
    {
        public In_Local_Sync(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 現場系統整合(成績同步)
                輸入: meeting_id
                日期: 
                    - 2023-04-06: Wrestling 分組交叉資料處理 (lina)
                    - 2023-03-13: Wrestling 版本 (lina)
                    - 2022-08-10: TKD 版本 (lina)
                    - 2021-09-28: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]In_Local_Sync";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "input dom: " + itmR.dom.InnerXml);

            //return itmR;

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,

                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
            };

            //即時成績(R1+R2)與新同步資料
            Item itmSchedules = GetLocalSchedules(cfg);
            RefreshScore(cfg, itmSchedules);

            //成績修正
            Item itmChanges = GetChangeScores(cfg);
            RefreshScore(cfg, itmChanges);

            CCO.Utilities.WriteDebug(strMethodName, "out");

            return itmR;
        }

        private void RefreshScore(TConfig cfg, Item itmSchedules)
        {
            int count = itmSchedules.getItemCount();
            if (count <= 0) return;

            for (int i = 0; i < count; i++)
            {
                var itmSchedule = itmSchedules.getItemByIndex(i);
                var row = MapRow(cfg, itmSchedule);

                if (row.meeting_id == "")
                {
                    UpdateSchedule(cfg, row, "2");
                    continue;
                }

                var itmDetails = GetEventDetail(cfg, row);
                if (itmDetails.isError() || itmDetails.getResult() == "" || itmDetails.getItemCount() <= 0)
                {
                    //明細無資料
                    //UpdateSchedule(cfg, row, "3");
                    continue;
                }

                var evt = MapEvent(cfg, itmDetails);
                var is_f1_win = row.win == "R";
                var is_f2_win = row.win == "B";

                RunWin(cfg, row, evt, is_f1_win, is_f2_win);
            }
        }

        private void RunWin(TConfig cfg, TRow row, TEvent evt, bool is_f1_win, bool is_f2_win)
        {
            if (is_f1_win)
            {
                //籤腳 1 勝出
                row.item.setProperty("red_win", "勝出");
                row.item.setProperty("blue_win", "");
            }

            if (is_f2_win)
            {
                //籤腳 2 勝出
                row.item.setProperty("red_win", "");
                row.item.setProperty("blue_win", "勝出");
            }

            var f1psList = new List<TPS>();
            f1psList.Add(new TPS { col = "in_sign_action", src = "Red_Win" });
            //f1psList.Add(new TPS { col = "in_check", src = "Red_Check" });
            f1psList.Add(new TPS { col = "in_points", src = "RedIntegral" });
            f1psList.Add(new TPS { col = "in_score", src = "RedScore" });
            f1psList.Add(new TPS { col = "in_score1", src = "RedC1" });
            f1psList.Add(new TPS { col = "in_score2", src = "RedC2" });
            f1psList.Add(new TPS { col = "in_word1", src = "Red1" });
            f1psList.Add(new TPS { col = "in_word2", src = "Red2" });
            f1psList.Add(new TPS { col = "in_word4", src = "Red3" });
            f1psList.Add(new TPS { col = "in_word5", src = "Red4" });

            var f2psList = new List<TPS>();
            f2psList.Add(new TPS { col = "in_sign_action", src = "Blue_Win" });
            //f2psList.Add(new TPS { col = "in_check", src = "Blue_Check" });
            f2psList.Add(new TPS { col = "in_points", src = "BlueIntegral" });
            f2psList.Add(new TPS { col = "in_score", src = "BlueScore" });
            f2psList.Add(new TPS { col = "in_score1", src = "BlueC1" });
            f2psList.Add(new TPS { col = "in_score2", src = "BlueC2" });
            f2psList.Add(new TPS { col = "in_word1", src = "Blue1" });
            f2psList.Add(new TPS { col = "in_word2", src = "Blue2" });
            f2psList.Add(new TPS { col = "in_word4", src = "Blue3" });
            f2psList.Add(new TPS { col = "in_word5", src = "Blue4" });

            //更新明細: 籤腳1
            UpdateDetail(cfg, evt.Foot1, row, f1psList);
            //更新明細: 籤腳2
            UpdateDetail(cfg, evt.Foot2, row, f2psList);
            //更新場次
            UpdateEvent(cfg, evt, row);

            //遞迴勝出
            if (is_f1_win)
            {
                ApplySync(cfg, evt.Value, evt.Foot1, evt.Foot2, row);

                //更新現場 Schedule
                UpdateSchedule(cfg, row, "1");
            }
            else if (is_f2_win)
            {
                ApplySync(cfg, evt.Value, evt.Foot2, evt.Foot1, row);

                //更新現場 Schedule
                UpdateSchedule(cfg, row, "1");
            }
        }

        /// <summary>
        /// 更新現場 Schedule
        /// </summary>
        private void UpdateSchedule(TConfig cfg, TRow row, string sync_flag)
        {
            var keys = new List<string>();
            keys.Add(row.item.getProperty("redscore", ""));
            keys.Add(row.item.getProperty("redintegral", ""));
            //keys.Add(row.item.getProperty("redc1", ""));
            //keys.Add(row.item.getProperty("redc2", ""));
            keys.Add(row.item.getProperty("bluescore", ""));
            keys.Add(row.item.getProperty("blueintegral", ""));
            //keys.Add(row.item.getProperty("bluec1", ""));
            //keys.Add(row.item.getProperty("bluec2", ""));
            keys.Add(row.item.getProperty("win", ""));

            var upd_result = string.Join("-", keys);

            var sql = "UPDATE In_Local_Schedule SET"
                + "  SyncFlag = '" + sync_flag + "'"
                + ", UpdResult = '" + upd_result + "'"
                + " WHERE ENumber = '" + row.enumber + "'";

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            cfg.inn.applySQL(sql);
        }

        private void ApplySync(TConfig cfg, Item itmEvent, Item itmWinner, Item itmLoser, TRow row)
        {
            Item itmData = cfg.inn.newItem();
            itmData.setType("In_Meeting_PEvent");
            itmData.setProperty("meeting_id", row.meeting_id);
            itmData.setProperty("program_id", itmEvent.getProperty("program_id", ""));
            itmData.setProperty("event_id", itmEvent.getProperty("event_id", ""));
            itmData.setProperty("detail_id", itmWinner.getProperty("detail_id", ""));
            itmData.setProperty("target_detail_id", itmLoser.getProperty("detail_id", ""));
            itmData.setProperty("mode", "sync");

            itmData.apply("in_meeting_program_score");
        }

        private void UpdateDetail(TConfig cfg, Item itmDetail, TRow row, List<TPS> pslist)
        {
            var item = row.item;
            var cmds = new List<string>();
            foreach (var ps in pslist)
            {
                cmds.Add("[" + ps.col + "] = N'" + item.getProperty(ps.src.ToLower()) + "'");
            }

            var detail_id = itmDetail.getProperty("detail_id", "");

            var sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET "
                + string.Join(", ", cmds)
                + " WHERE id = '" + detail_id + "'";

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            cfg.inn.applySQL(sql);
        }

        private void UpdateEvent(TConfig cfg, TEvent evt, TRow row)
        {
            var sql = "UPDATE IN_MEETING_PEVENT SET"
                + "   in_win_local = N'" + row.win + "'"
                + ", in_win_local_time = '" + row.timw + "'"
                + " WHERE id = '" + evt.Id + "'";

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            cfg.inn.applySQL(sql);
        }

        private TEvent MapEvent(TConfig cfg, Item items)
        {
            var itmEvent = items.getItemByIndex(0);

            var evt = new TEvent
            {
                Id = itmEvent.getProperty("event_id", ""),
                Value = itmEvent,
            };

            var count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_sign_foot = item.getProperty("in_sign_foot", "");
                if (in_sign_foot == "1")
                {
                    evt.Foot1 = item;
                }
                else
                {
                    evt.Foot2 = item;
                }
            }

            if (evt.Foot1 == null) evt.Foot1 = cfg.inn.newItem();
            if (evt.Foot2 == null) evt.Foot2 = cfg.inn.newItem();

            return evt;
        }

        private Item GetLocalSchedules(TConfig cfg)
        {
            string sql = @"
                SELECT 
                    t1.*
					, t4.id				AS 'meeting_id'
					, t3.id				AS 'program_id'
					, t2.id				AS 'event_id'
					, t3.in_l1
					, t3.in_l2
					, t3.in_l3
					, t3.in_fight_day
					, t2.in_tree_no
					, t2.in_fight_id
                FROM 
                    IN_LOCAL_SCHEDULE t1 WITH(NOLOCK) 
                INNER JOIN 
                    IN_MEETING_PEVENT t2 WITH(NOLOCK) 
                    ON t2.item_number = t1.ENumber
                INNER JOIN 
                    IN_MEETING_PROGRAM t3 WITH(NOLOCK) 
                    ON t3.id = t2.source_id
				INNER JOIN
					IN_MEETING t4 WITH(NOLOCK)
					ON t4.id = t3.in_meeting
                WHERE
                    ISNULL(t1.SyncFlag, '') = ''
                    AND ISNULL(t1.Win, '') <> ''
                ORDER BY
                    t1.No
            ";

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        private Item GetChangeScores(TConfig cfg)
        {
            string sql = @"
                SELECT 
                    t1.*
					, t4.id				AS 'meeting_id'
					, t3.id				AS 'program_id'
					, t2.id				AS 'event_id'
					, t3.in_l1
					, t3.in_l2
					, t3.in_l3
					, t3.in_fight_day
					, t2.in_tree_no
					, t2.in_fight_id
                FROM 
                    IN_LOCAL_SCHEDULE t1 WITH(NOLOCK) 
                INNER JOIN 
                    IN_MEETING_PEVENT t2 WITH(NOLOCK) 
                    ON t2.item_number = t1.ENumber
                INNER JOIN 
                    IN_MEETING_PROGRAM t3 WITH(NOLOCK) 
                    ON t3.id = t2.source_id
				INNER JOIN
					IN_MEETING t4 WITH(NOLOCK)
					ON t4.id = t3.in_meeting
                WHERE
                    ISNULL(t1.SyncFlag, '') = 1
                    AND ISNULL(t1.Win, '') <> ''
                    AND (
                                ISNULL(t1.RedScore, '') 
                        + '-' + ISNULL(t1.RedIntegral, '') 
                        + '-' + ISNULL(t1.BlueScore, '') 
                        + '-' + ISNULL(t1.BlueIntegral, '') 
                        + '-' + ISNULL(t1.Win, '')
                        <> 
                        ISNULL(t1.UpdResult, '')
                    )
            ";

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        private Item GetEventDetail(TConfig cfg, TRow row)
        {
            string sql = @"
                SELECT 
                    t1.source_id    AS 'program_id'
                    , t1.id         AS 'event_id'
                    , t2.id         AS 'detail_id'
                    , t2.in_sign_foot
                    , t2.in_sign_no
                FROM 
                    IN_MEETING_PEVENT t1 WITH(NOLOCK) 
                INNER JOIN 
                    IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK)
                    ON t2.source_id = t1.id
                INNER JOIN 
                    IN_MEETING_PROGRAM t3 WITH(NOLOCK)
                    ON t3.id = t1.source_id
                WHERE
                    t1.id = '{#event_id}'
                ORDER BY
                    t2.in_sign_foot
            ";

            sql = sql.Replace("{#event_id}", row.event_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }

            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
        }

        private class TEvent
        {
            /// <summary>
            /// 編號
            /// </summary>
            public string Id { get; set; }
            public Item Value { get; set; }
            public Item Foot1 { get; set; }
            public Item Foot2 { get; set; }
        }

        private class TPS
        {
            public string col { get; set; }
            public string src { get; set; }
        }

        private class TFixEvt
        {
            public string fight_id { get; set; }
            public bool need_link_player { get; set; }
        }

        private class TRow
        {
            public string meeting_id { get; set; }
            public string program_id { get; set; }
            public string event_id { get; set; }
            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }
            public string in_fight_day { get; set; }
            public string in_fight_id { get; set; }
            public string in_tree_no { get; set; }
            public string timw { get; set; }
            public string win { get; set; }
            public string enumber { get; set; }

            public string redscore { get; set; }
            public string redintegral { get; set; }
            public string redc1 { get; set; }
            public string redc2 { get; set; }
            public string red1 { get; set; }
            public string red2 { get; set; }
            public string red4 { get; set; }
            public string red5 { get; set; }

            public string bluescore { get; set; }
            public string blueintegral { get; set; }
            public string bluec1 { get; set; }
            public string bluec2 { get; set; }
            public string blue1 { get; set; }
            public string blue2 { get; set; }
            public string blue4 { get; set; }
            public string blue5 { get; set; }

            public Item item { get; set; }
        }

        private class TPlayer
        {
            public string no { get; set; }
            public string org { get; set; }
            public string name { get; set; }
        }

        private TRow MapRow(TConfig cfg, Item item)
        {
            var result = new TRow
            {
                meeting_id = item.getProperty("meeting_id", ""),
                program_id = item.getProperty("program_id", ""),
                event_id = item.getProperty("event_id", ""),
                in_l1 = item.getProperty("in_l1", ""),
                in_l2 = item.getProperty("in_l2", ""),
                in_l3 = item.getProperty("in_l3", ""),
                in_fight_day = item.getProperty("in_fight_day", ""),
                in_fight_id = item.getProperty("in_fight_id", ""),
                in_tree_no = item.getProperty("in_tree_no", ""),
                timw = item.getProperty("timw", ""),
                win = item.getProperty("win", ""),
                enumber = item.getProperty("enumber", ""),

                redscore = item.getProperty("redscore", ""),
                redintegral = item.getProperty("redintegral", ""),
                redc1 = item.getProperty("redc1", ""),
                redc2 = item.getProperty("redc2", ""),
                red1 = item.getProperty("red1", ""),
                red2 = item.getProperty("red2", ""),
                red4 = item.getProperty("red4", ""),
                red5 = item.getProperty("red5", ""),

                bluescore = item.getProperty("bluescore", ""),
                blueintegral = item.getProperty("blueintegral", ""),
                bluec1 = item.getProperty("bluec1", ""),
                bluec2 = item.getProperty("bluec2", ""),
                blue1 = item.getProperty("blue1", ""),
                blue2 = item.getProperty("blue2", ""),
                blue4 = item.getProperty("blue4", ""),
                blue5 = item.getProperty("blue5", ""),

                item = item,
            };

            return result;
        }

        
    }
}