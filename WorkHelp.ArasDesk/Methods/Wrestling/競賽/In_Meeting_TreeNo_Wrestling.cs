﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;
using WorkHelp.ArasDesk.Samples.SqlClient;
using System.Data;
using System.Security.Cryptography;
using System.Web.Management;
using System.Web.Caching;

namespace WorkHelp.ArasDesk.Methods.Wrestling.Common
{
    public class In_Meeting_TreeNo_Wrestling : Item
    {
        public In_Meeting_TreeNo_Wrestling(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
               目的: 角力場次編號
               日誌: 
                   - 2024-03-22: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "In_Meeting_TreeNo_Wrestling";

            var itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                meeting_id = itmR.getProperty("meeting_id", "").Trim(),
                in_fight_day = itmR.getProperty("in_fight_day", "").Trim(),
                in_site_code = itmR.getProperty("in_site_code", "").Trim(),
                in_group_tag = itmR.getProperty("in_group_tag", "").Trim(),
                in_event_span = itmR.getProperty("in_event_span", "").Trim(),
                no_rank12 = itmR.getProperty("in_rank12", "") == "N",
                scene = itmR.getProperty("scene", ""),
            };

            if (cfg.in_event_span == "" || cfg.in_event_span == "0")
            {
                cfg.in_event_span = "4";
            }

            cfg.eventSpan = GetInt(cfg.in_event_span);

            cfg.itmMeeting = cfg.inn.applySQL("SELECT id, in_battle_mode, in_battle_repechage FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            cfg.in_battle_mode = cfg.itmMeeting.getProperty("in_battle_mode", "");
            cfg.in_battle_repechage = cfg.itmMeeting.getProperty("in_battle_repechage", "");

            cfg.allocate_which_first = GetMeetingVariable(cfg, "allocate_which_first");
            cfg.need_rpc_number = GetMeetingVariable(cfg, "need_rpc_number");

            cfg.teamCountMap = GenerateTeamCountMap(cfg);

            switch (cfg.scene)
            {
                case "clear_day_site":
                    ClearDaySiteEventNumber(cfg, itmR);
                    break;

                case "clear":
                    ClearEventNumber(cfg, itmR);
                    ResetForTopTwo(cfg, itmR);
                    ResetOriginTreeNoB(cfg, itmR);
                    break;

                case "run":
                    ResetEventNumber(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void ClearDaySiteEventNumber(TConfig cfg, Item itmReturn)
        {
            var programs = MeetingProgramItems(cfg);
            for (var i = 0; i < programs.Count; i++)
            {
                var program = programs[i];
                ClearTreeNo(cfg, program);
            }
        }

        private void ResetEventNumber(TConfig cfg, Item itmReturn)
        {
            if (cfg.in_battle_mode == "uww")
            {
                if (cfg.need_rpc_number == "Y")
                {
                    ResetOriginTreeNoA(cfg, itmReturn);
                    //ResetEventNumberA(cfg, itmReturn);
                    ResetEventNumberC(cfg, itmReturn);
                }
                else if (cfg.need_rpc_number == "NO_RPC")
                {
                    ResetOriginTreeNoB(cfg, itmReturn);
                    ResetEventNumberD(cfg, itmReturn);
                }
                else
                {
                    ResetEventNumberB(cfg, itmReturn);
                }
            }
            else
            {
                ResetEventNumberB(cfg, itmReturn);
                //ResetForTopTwo(cfg, itmReturn);
            }
        }

        private void ResetOriginTreeNoB(TConfig cfg, Item itmReturn)
        {
            var itmPrograms = GetProgramItems(cfg);
            var count = itmPrograms.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var itmProgram = itmPrograms.getItemByIndex(i);
                var source_id = itmProgram.getProperty("id", "");
                var in_team_count = itmProgram.getProperty("in_team_count", "");
                ResetOriginTreeNoB1(cfg, source_id, in_team_count);
                ResetOriginTreeNoB2(cfg, source_id, in_team_count);
            }
        }

        private void ResetOriginTreeNoB1(TConfig cfg, string source_id, string in_team_count)
        {
            var sql = @"
                UPDATE t1 SET
	                t1.in_tree_no = NULL
	                , t1.in_tree_sno = NULL
                FROM
	                IN_MEETING_PEVENT t1 WITH(NOLOCK)
                LEFT OUTER JOIN
	                IN_MEETING_DRAWSHEET t2 WITH(NOLOCK)
	                ON t2.in_mode = 'uww' 
	                AND t2.in_team_count = {#in_team_count}
                LEFT OUTER JOIN
	                IN_MEETING_DRAWSHEET_EVENT t3 WITH(NOLOCK)
	                ON t3.source_id = t2.id
	                AND t3.in_fight_id = t1.in_fight_id
                WHERE
	                t1.source_id = '{#source_id}'
	                AND t3.sort_order IS NULL
            ";

            sql = sql.Replace("{#source_id}", source_id)
                .Replace("{#in_team_count}", in_team_count);

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            cfg.inn.applySQL(sql);
        }

        private void ResetOriginTreeNoB2(TConfig cfg, string source_id, string in_team_count)
        {
            var sql = @"
                UPDATE t101 SET
	                t101.in_tree_sno = t102.rn
                FROM
	                IN_MEETING_PEVENT t101 
                INNER JOIN
                (
	                SELECT
		                t1.id
		                , t1.in_fight_id
		                , t3.sort_order
		                , ROW_NUMBER() OVER (ORDER BY t3.sort_order) AS 'rn'
	                FROM
		                IN_MEETING_PEVENT t1 WITH(NOLOCK)
	                LEFT OUTER JOIN
		                IN_MEETING_DRAWSHEET t2 WITH(NOLOCK)
		                ON t2.in_mode = 'uww' 
		                AND t2.in_team_count = {#in_team_count}
	                LEFT OUTER JOIN
		                IN_MEETING_DRAWSHEET_EVENT t3 WITH(NOLOCK)
		                ON t3.source_id = t2.id
		                AND t3.in_fight_id = t1.in_fight_id
	                WHERE
		                t1.source_id = '{#source_id}'
		                AND t3.sort_order IS NOT NULL
                ) t102
                ON t102.id = t101.id
            ";

            sql = sql.Replace("{#source_id}", source_id)
                .Replace("{#in_team_count}", in_team_count);

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            cfg.inn.applySQL(sql);
        }

        private void ResetOriginTreeNoA(TConfig cfg, Item itmReturn)
        {
            var itmPrograms = GetProgramItems(cfg);
            var count = itmPrograms.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var itmProgram = itmPrograms.getItemByIndex(i);
                var source_id = itmProgram.getProperty("id", "");
                var in_team_count = itmProgram.getProperty("in_team_count", "");
                ResetOriginTreeNoA1(cfg, source_id, in_team_count);
                ResetOriginTreeNoA2(cfg, source_id, in_team_count);
            }
        }

        private void ResetOriginTreeNoA1(TConfig cfg, string source_id, string in_team_count)
        {
            var sql = @"
                UPDATE t1 SET
	                t1.in_tree_no = NULL
	                , t1.in_tree_sno = NULL
                FROM
	                IN_MEETING_PEVENT t1 WITH(NOLOCK)
                LEFT OUTER JOIN
	                IN_MEETING_DRAWSHEET t2 WITH(NOLOCK)
	                ON t2.in_mode = 'uww' 
	                AND t2.in_team_count = {#in_team_count}
                LEFT OUTER JOIN
	                IN_MEETING_DRAWSHEET_EVENT t3 WITH(NOLOCK)
	                ON t3.source_id = t2.id
	                AND t3.in_fight_id = t1.in_fight_id
                WHERE
	                t1.source_id = '{#source_id}'
	                AND t3.sort_order IS NULL
            ";

            sql = sql.Replace("{#source_id}", source_id)
                .Replace("{#in_team_count}", in_team_count);

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            cfg.inn.applySQL(sql);
        }

        private void ResetOriginTreeNoA2(TConfig cfg, string source_id, string in_team_count)
        {
            var sql = @"
                UPDATE t101 SET
	                t101.in_tree_no = t102.rn
	                , t101.in_tree_sno = t102.rn
                FROM
	                IN_MEETING_PEVENT t101 
                INNER JOIN
                (
	                SELECT
		                t1.id
		                , t1.in_fight_id
		                , t3.sort_order
		                , ROW_NUMBER() OVER (ORDER BY t3.sort_order) AS 'rn'
	                FROM
		                IN_MEETING_PEVENT t1 WITH(NOLOCK)
	                LEFT OUTER JOIN
		                IN_MEETING_DRAWSHEET t2 WITH(NOLOCK)
		                ON t2.in_mode = 'uww' 
		                AND t2.in_team_count = {#in_team_count}
	                LEFT OUTER JOIN
		                IN_MEETING_DRAWSHEET_EVENT t3 WITH(NOLOCK)
		                ON t3.source_id = t2.id
		                AND t3.in_fight_id = t1.in_fight_id
	                WHERE
		                t1.source_id = '{#source_id}'
		                AND t3.sort_order IS NOT NULL
                ) t102
                ON t102.id = t101.id
            ";

            sql = sql.Replace("{#source_id}", source_id)
                .Replace("{#in_team_count}", in_team_count);

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            cfg.inn.applySQL(sql);
        }

        private Item GetProgramItems(TConfig cfg)
        {
            var sql = @"
                SELECT
	                id
	                , in_name
	                , in_battle_mode
	                , in_battle_type
	                , in_team_count
                FROM
	                IN_MEETING_PROGRAM WITH(NOLOCK)
                WHERE
	                in_meeting = '{#meeting_id}'
	                AND in_fight_day = '{#in_fight_day}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_fight_day}", cfg.in_fight_day);

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        private void ResetEventNumberA(TConfig cfg, Item itmReturn)
        {
            var itmSites = GetMeetingSiteItems(cfg);
            var count = itmSites.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var itmSite = itmSites.getItemByIndex(i);
                var in_code = itmSite.getProperty("in_code", "");

                var itmSects = GetGroupSectEventItems(cfg, cfg.in_fight_day, in_code);
                var pkg = ProgramEventGroupListA(cfg, itmSects);

                if (pkg.total == 0) continue;
                if (pkg.sorted.Count == 0) continue;

                ResetTreeNo(cfg, itmSite, pkg);
            }
        }

        private void ResetEventNumberB(TConfig cfg, Item itmReturn)
        {
            var itmSites = GetMeetingSiteItems(cfg);
            var count = itmSites.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var itmSite = itmSites.getItemByIndex(i);
                var in_code = itmSite.getProperty("in_code", "");

                var itmSects = GetGroupSectEventItems(cfg, cfg.in_fight_day, in_code);
                var pkg = ProgramEventGroupListB(cfg, itmSects);

                if (pkg.total == 0) continue;
                if (pkg.sorted.Count == 0) continue;

                ResetTreeNo(cfg, itmSite, pkg);
            }
        }

        private void ResetTreeNo(TConfig cfg, Item itmSite, TSectPackage pkg)
        {
            var catched = new List<TEvt>();
            while (catched.Count < pkg.total && pkg.sorted.Count > 0)
            {
                var newTreeNo = catched.Count + 1;

                var sect = pkg.sorted[0];
                if (sect.keys.Count == 0)
                {
                    pkg.sorted.Remove(sect);
                    continue;
                }

                var key = sect.keys[0];
                var spanR = GetSectRoundSpan(cfg, sect.teamCount, key);
                var spanC = catched.Count + 1 - sect.currentMaxNumber;
                if (sect.currentMaxNumber > 0 && spanC < spanR)
                {
                    AppendEventSpans(cfg, catched, pkg, sect.Next, needSpan: spanR);
                }


                //一次抓一整輪
                var round = sect.rounds[key];
                NeedRoundEventSort(cfg, round, newTreeNo);

                while (round.nodes.Count > 0)
                {
                    var idx = 0;

                    var ball = round.nodes[idx];
                    ball.stat = "MAIN";
                    catched.Add(ball);

                    round.nodes.RemoveAt(idx);
                    round.end_tree_no = catched.Count + 1;

                    sect.currentRoundKey = key;
                    sect.currentMaxNumber = catched.Count + 1;
                }

                sect.rounds.Remove(key);
                sect.keys.Remove(key);
                if (sect.keys.Count == 0)
                {
                    pkg.sorted.Remove(sect);
                }
                else
                {
                    AppendEventSpans(cfg, catched, pkg, sect.Next, needSpan: spanR);
                }
            }

            LogTreeNo(cfg, itmSite, catched);
            SaveTreeNo(cfg, itmSite, catched);
        }

        //增加場次間隔
        private void AppendEventSpans(TConfig cfg, List<TEvt> catched, TSectPackage pkg, TSect sect, int needSpan = 0)
        {
            if (sect == null) return;
            if (needSpan == 0) return;

            if (sect.keys.Count == 0)
            {
                AppendEventSpans(cfg, catched, pkg, sect.Next, needSpan: needSpan);
                return;
            }

            var newTreeNo = catched.Count + 1;

            //一次抓一整輪
            var key = sect.keys[0];
            var round = sect.rounds[key];
            var total = round.nodes.Count;
            var spanR = GetSectRoundSpan(cfg, sect.teamCount, key);
            var spanC = catched.Count + 1 - sect.currentMaxNumber;
            if (sect.currentMaxNumber > 0 && spanC < spanR)
            {
                AppendEventSpans(cfg, catched, pkg, sect.Next, needSpan: needSpan);
                return;
            }

            NeedRoundEventSort(cfg, round, newTreeNo);
            while (round.nodes.Count > 0)
            {
                var idx = 0;

                var ball = round.nodes[idx];
                ball.stat = "SPAN";
                catched.Add(ball);

                round.nodes.RemoveAt(idx);
                round.end_tree_no = catched.Count + 1;

                sect.currentRoundKey = key;
                sect.currentMaxNumber = catched.Count + 1;
            }

            sect.rounds.Remove(key);
            sect.keys.Remove(key);
            if (sect.keys.Count == 0)
            {
                pkg.sorted.Remove(sect);
            }

            var newSpan = needSpan - total;
            if (total < needSpan)
            {
                AppendEventSpans(cfg, catched, pkg, sect.Next, needSpan: newSpan);
            }
        }

        private void NeedRoundEventSort(TConfig cfg, TRound current, int newTreeNo)
        {
            if (current.Last == null) return;
            if (current.nodes.Count == 0) return;

            var last = current.Last;
            var span = newTreeNo - last.end_tree_no;

            if (span < 5)
            {
                current.nodes = current.nodes.OrderByDescending(x => x.weight)
                       .ThenBy(x => x.serial)
                       .ToList();
            }
        }

        private void SaveTreeNo(TConfig cfg, Item itmSite, List<TEvt> catched)
        {
            for (var i = 0; i < catched.Count; i++)
            {
                var ball = catched[i];
                var newTreeNo = i + 1;
                var sql = "UPDATE IN_MEETING_PEVENT SET"
                    + "  in_tree_no = " + newTreeNo
                    + ", in_show_serial = " + newTreeNo
                    + " WHERE id = '" + ball.id + "'";
                cfg.inn.applySQL(sql);
            }
        }

        private void LogTreeNo(TConfig cfg, Item itmSite, List<TEvt> catched)
        {
            var in_code_en = itmSite.getProperty("in_code_en", "");

            var contents = new StringBuilder();
            contents.AppendLine(cfg.in_fight_day + " 場地 " + in_code_en);
            for (var i = 0; i < catched.Count; i++)
            {
                var ball = catched[i];
                var c1 = ball.pg_name2 + "(" + ball.pg_team_count.PadLeft(2, '0') + "人, " + ball.pg_mat2 + ")";
                var c2 = ball.in_fight_id + ": " + ball.in_tree_no + " vs " + (i + 1) + "\t" + ball.stat;

                contents.AppendLine(c1 + "\t" + c2);
            }
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, contents.ToString());
        }

        private int GetSectRoundSpan(TConfig cfg, int teamCount, string roundKey)
        {
            var def = cfg.eventSpan;
            switch (roundKey)
            {
                case "M064":
                    return teamCount > 68 ? 0 : def;
                case "M032":
                    return teamCount > 36 ? 0 : def;
                case "M016":
                    return teamCount > 20 ? 0 : def;
                case "M008":
                    return teamCount > 12 ? 0 : def;
                default:
                    return def;
            }
        }

        private TSectPackage ProgramEventGroupListA(TConfig cfg, Item items)
        {
            var pkg = new TSectPackage();
            var sects = new List<TSect>();

            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var program_id = item.getProperty("program_id", "");
                var sect = sects.Find(x => x.program_id == program_id);
                if (sect == null)
                {
                    sect = NewSect(cfg, item);
                    sect.groupSort = sect.isRobin5 ? 1 : 2;
                    sect.serial = sects.Count + 1;
                    sects.Add(sect);
                }

                var evt = NewEvt(cfg, sect, item, i);
                //重設場次權重
                ResetEventWeight(cfg, sect, evt);
                //分輪
                AppednRoundEventA(cfg, pkg, sect, evt);
            }

            LinkRoundEvents(cfg, sects);

            //分組場次依特殊條件排序
            SortedGroupSects(cfg, pkg, sects);

            return pkg;
        }

        private TSectPackage ProgramEventGroupListB(TConfig cfg, Item items)
        {
            var pkg = new TSectPackage();
            var sects = new List<TSect>();

            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var program_id = item.getProperty("program_id", "");
                var sect = sects.Find(x => x.program_id == program_id);
                if (sect == null)
                {
                    sect = NewSect(cfg, item);
                    sect.groupSort = sect.isRobin5 ? 1 : 2;
                    sect.serial = sects.Count + 1;
                    sects.Add(sect);
                }

                var evt = NewEvt(cfg, sect, item, i);
                //重設場次權重
                ResetEventWeight(cfg, sect, evt);
                //分輪
                AppednRoundEventB(cfg, pkg, sect, evt);
            }

            LinkRoundEvents(cfg, sects);

            //分組場次依特殊條件排序
            SortedGroupSects(cfg, pkg, sects);

            return pkg;
        }

        private void LinkRoundEvents(TConfig cfg, List<TSect> sects)
        {
            for (var i = 0; i < sects.Count; i++)
            {
                var sect = sects[i];
                if (sect.keys.Count == 0) continue;

                var bgn = 0;
                var end = sect.keys.Count - 1;
                for (var j = 0; j < sect.keys.Count; j++)
                {
                    var key = sect.keys[j];
                    var current = sect.rounds[key];
                    if (j > bgn) current.Last = sect.rounds[sect.keys[j - 1]];
                    if (j < end) current.Next = sect.rounds[sect.keys[j + 1]];
                }
            }
        }

        private void SortedGroupSects(TConfig cfg, TSectPackage pkg, List<TSect> sects)
        {
            var sorted = sects.OrderBy(x => x.groupSort)
                .ThenBy(x => x.serial);

            pkg.sects = sects;
            pkg.sorted = sorted.ToList();

            if (pkg.sorted.Count <= 1) return;

            var bgn = 0;
            var end = pkg.sorted.Count - 1;
            for (var i = 0; i < pkg.sorted.Count; i++)
            {
                var current = pkg.sorted[i];
                if (i > bgn) current.Last = pkg.sorted[i - 1];
                if (i < end) current.Next = pkg.sorted[i + 1];
            }
        }

        //分輪
        private void AppednRoundEventA(TConfig cfg, TSectPackage pkg, TSect program, TEvt evt)
        {
            if (!evt.needNumber) return;

            var arr = evt.in_fight_id.Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);
            var w1 = arr != null && arr.Length > 0 ? arr[0] : "";

            var key = w1;
            var c = w1.Substring(0, 2);

            //if (c == "R0")
            //{
            //    evt.needNumber = false;
            //    return;
            //}

            if (cfg.no_rank12 && key == "M002")
            {
                evt.needNumber = false;
                return;
            }

            //if (key == "R004")
            //{
            //    evt.needNumber = false;
            //    return;
            //}

            switch (key)
            {
                case "RBN3":
                case "RBN4":
                case "RBN5":
                case "CRS6":
                case "CRS7":
                    key += "R" + evt.round;
                    break;
            }

            if (program.isCross && key == "M004")
            {
                key = TKey.CM004;
            }

            var round = default(TRound);
            if (program.rounds.ContainsKey(key))
            {
                round = program.rounds[key];
            }
            else
            {
                program.keys.Add(key);

                round = new TRound
                {
                    key = key,
                    end_tree_no = int.MinValue,
                    nodes = new List<TEvt>()
                };

                program.rounds.Add(key, round);
            }

            round.nodes.Add(evt);

            pkg.total++;
        }

        //分輪
        private void AppednRoundEventB(TConfig cfg, TSectPackage pkg, TSect program, TEvt evt)
        {
            if (!evt.needNumber) return;

            var arr = evt.in_fight_id.Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);
            var w1 = arr != null && arr.Length > 0 ? arr[0] : "";

            var key = w1;
            var c = w1.Substring(0, 2);

            if (c == "R0")
            {
                evt.needNumber = false;
                return;
            }

            if (cfg.no_rank12 && key == "M002")
            {
                evt.needNumber = false;
                return;
            }

            if (key == "R004")
            {
                evt.needNumber = false;
                return;
            }

            switch (key)
            {
                case "RBN3":
                case "RBN4":
                case "RBN5":
                case "CRS6":
                case "CRS7":
                    key += "R" + evt.round;
                    break;
            }

            if (program.isCross && key == "M004")
            {
                key = TKey.CM004;
            }

            var round = default(TRound);
            if (program.rounds.ContainsKey(key))
            {
                round = program.rounds[key];
            }
            else
            {
                program.keys.Add(key);

                round = new TRound
                {
                    key = key,
                    end_tree_no = int.MinValue,
                    nodes = new List<TEvt>()
                };

                program.rounds.Add(key, round);
            }

            round.nodes.Add(evt);

            pkg.total++;
        }

        //重設場次權重
        private void ResetEventWeight(TConfig cfg, TSect program, TEvt evt)
        {
            var setting = program.map.ContainsKey(evt.in_fight_id)
                ? program.map[evt.in_fight_id]
                : default(TDraw);

            if (evt.round == 1 && setting == null)
            {
                evt.weight = 0;
                evt.needNumber = false;
                return;
            }

            if (setting == null)
            {
                evt.weight = 1;//等待兩個籤腳籤號
                evt.needNumber = true;
                return;
            }

            if (setting.isByPass)
            {
                evt.weight = 0;
                evt.needNumber = false;//輪空不排
                return;
            }

            if (setting.available == 2)
            {
                evt.weight = 4;//有兩個籤腳籤號
                evt.needNumber = true;
            }
            else if (setting.available == 1)
            {
                evt.weight = 2;//等待一個籤腳籤號
                evt.needNumber = true;
            }
            else if (setting.available == 0)
            {
                evt.weight = 1;//等待兩個籤腳籤號
                evt.needNumber = true;
            }
        }

        private TEvt NewEvt(TConfig cfg, TSect sect, Item item, int idx)
        {
            var x = new TEvt
            {
                id = item.getProperty("event_id", ""),
                pg_name = item.getProperty("in_name", ""),
                pg_name2 = item.getProperty("in_name2", ""),
                pg_mat2 = item.getProperty("in_site_mat2", ""),
                pg_team_count = item.getProperty("in_team_count", ""),
                in_tree_name = item.getProperty("in_tree_name", ""),
                in_tree_id = item.getProperty("in_tree_id", ""),
                in_tree_no = item.getProperty("in_tree_no", ""),
                in_fight_id = item.getProperty("in_fight_id", ""),
                round = GetInt(item.getProperty("in_round", "0")),
                stat = "",
                serial = idx + 1,
            };

            if (sect.isCross6)
            {
                switch (x.in_fight_id)
                {
                    case "M004-01":
                    case "M004-02":
                        x.round = 4;
                        break;
                    case "M002-01":
                        x.round = 5;
                        break;
                }
            }
            else if (sect.isCross7)
            {
                switch (x.in_fight_id)
                {
                    case "RNK56-01":
                        x.round = 4;
                        break;
                    case "M004-01":
                    case "M004-02":
                        x.round = 5;
                        break;
                    case "M002-01":
                        x.round = 6;
                        break;
                }
            }
            if (x.in_fight_id == "RNK78-01")
            {
                x.round = 7;
            }
            return x;
        }

        private TSect NewSect(TConfig cfg, Item item)
        {
            var result = new TSect
            {
                program_id = item.getProperty("program_id", ""),
                in_name = item.getProperty("in_name", ""),
                in_name2 = item.getProperty("in_name2", ""),
                in_battle_mode = item.getProperty("in_battle_mode", ""),
                in_battle_type = item.getProperty("in_battle_type", ""),
                in_team_count = item.getProperty("in_team_count", "0"),
                site_id = item.getProperty("in_site", ""),
                site_code = item.getProperty("in_site_code", ""),
                keys = new List<string>(),
                rounds = new Dictionary<string, TRound>(),
                currentRoundKey = "",
                currentMaxNumber = int.MinValue,
            };

            result.teamCount = GetInt(result.in_team_count);
            result.isRobin5 = result.teamCount == 5 && result.in_battle_type == "SingleRoundRobin";
            result.isCross6 = result.in_battle_type == "SixPlayers";
            result.isCross7 = result.in_battle_type == "SevenPlayers";
            if (result.isCross6 || result.isCross7) result.isCross = true;
            result.map = GetDrawSetting(cfg, result);

            return result;
        }

        private Dictionary<string, TDraw> GetDrawSetting(TConfig cfg, TSect program)
        {
            var sql = @"
                SELECT 
	                in_fight_id
	                , in_sign_foot
	                , in_sign_no
	                , in_bypass_status
	                , in_available
                FROM 
	                IN_MEETING_DRAWSIGNNO WITH(NOLOCK) 
                WHERE 
	                in_mode = '{#in_mode}' 
	                AND in_team_count = {#in_team_count}
                ORDER BY
	                in_fight_id DESC
	                , in_sign_foot
            ";

            sql = sql.Replace("{#in_mode}", program.in_battle_mode)
                .Replace("{#in_team_count}", program.in_team_count);

            var map = new Dictionary<string, TDraw>();
            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var in_fight_id = item.getProperty("in_fight_id", "");
                var in_sign_foot = item.getProperty("in_sign_foot", "");
                var in_sign_no = item.getProperty("in_sign_no", "");
                var in_bypass_status = item.getProperty("in_bypass_status", "");
                var in_available = item.getProperty("in_available", "");
                var row = default(TDraw);
                if (map.ContainsKey(in_fight_id))
                {
                    row = map[in_fight_id];
                }
                else
                {
                    row = new TDraw
                    {
                        in_fight_id = in_fight_id,
                        available = GetInt(in_available)
                    };
                    map.Add(in_fight_id, row);
                }

                if (in_bypass_status == "1")
                {
                    row.isByPass = true;
                }

                if (in_sign_foot == "1")
                {
                    row.in_f1_no = in_sign_no;
                }
                else if (in_sign_foot == "2")
                {
                    row.in_f2_no = in_sign_no;
                }
            }
            return map;
        }

        private class TDraw
        {
            public string in_fight_id { get; set; }
            public string in_f1_no { get; set; }
            public string in_f2_no { get; set; }
            public bool isByPass { get; set; }
            public int available { get; set; }
        }

        private class TKey
        {
            public const string M128 = "M128";
            public static string M064 = "M064";
            public static string M032 = "M032";
            public static string M016 = "M016";
            public static string M008 = "M008";
            public static string M004 = "M004";
            public static string M002 = "M002";

            public static string RBN3R1 = "RBN3R1";
            public static string RBN3R2 = "RBN3R2";
            public static string RBN3R3 = "RBN3R3";

            public static string RBN4R1 = "RBN4R1";
            public static string RBN4R2 = "RBN4R2";
            public static string RBN4R3 = "RBN4R3";

            public static string RBN5R1 = "RBN5R1";
            public static string RBN5R2 = "RBN5R2";
            public static string RBN5R3 = "RBN5R3";
            public static string RBN5R4 = "RBN5R4";
            public static string RBN5R5 = "RBN5R5";

            public static string CRS6R1 = "CRS6R1";
            public static string CRS6R2 = "CRS6R2";
            public static string CRS6R3 = "CRS6R3";

            public static string CRS7R1 = "CRS7R1";
            public static string CRS7R2 = "CRS7R2";
            public static string CRS7R3 = "CRS7R3";

            public static string CM004 = "CM004";

            public static string RK005 = "RK005";
            public static string RK007 = "RK007";
        }

        private class TSectPackage
        {
            public int total { get; set; }
            public List<TSect> sects { get; set; }
            public List<TSect> sorted { get; set; }
        }

        private class TSect
        {
            public string program_id { get; set; }
            public string site_id { get; set; }
            public string site_code { get; set; }

            public string in_name { get; set; }
            public string in_name2 { get; set; }
            public string in_battle_mode { get; set; }
            public string in_battle_type { get; set; }
            public string in_team_count { get; set; }
            public int teamCount { get; set; }

            public int groupSort { get; set; }
            public int serial { get; set; }

            public bool isRobin5 { get; set; }
            public bool isCross { get; set; }
            public bool isCross6 { get; set; }
            public bool isCross7 { get; set; }

            public Dictionary<string, TDraw> map { get; set; }

            public List<string> keys { get; set; }
            public Dictionary<string, TRound> rounds { get; set; }

            public TSect Next { get; set; }
            public TSect Last { get; set; }
            public string currentRoundKey { get; set; }
            public int currentMaxNumber { get; set; }
        }

        private class TRound
        {
            public string key { get; set; }
            public int end_tree_no { get; set; }
            public List<TEvt> nodes { get; set; }
            public TRound Last { get; set; }
            public TRound Next { get; set; }
        }

        private class TEvt
        {
            public string id { get; set; }
            public string pg_name { get; set; }
            public string pg_name2 { get; set; }
            public string pg_mat2 { get; set; }
            public string pg_team_count { get; set; }
            public string in_tree_name { get; set; }
            public string in_tree_id { get; set; }
            public string in_tree_no { get; set; }
            public string in_fight_id { get; set; }
            public int round { get; set; }
            public int weight { get; set; }
            public int serial { get; set; }
            public bool needNumber { get; set; }
            public string stat { get; set; }
        }

        private void ResetEventNumberC(TConfig cfg, Item itmReturn)
        {
            //修正組別場地場次序
            FixProgramMatNo(cfg);

            var programs = MeetingProgramItems(cfg);
            for (var i = 0; i < programs.Count; i++)
            {
                var program = programs[i];
                ClearTreeNo(cfg, program);
                if (program.reference != null && program.reference.cancelEvents != null)
                {
                    CancelEvents(cfg, program, program.reference.cancelEvents);
                }
                if (program.team_count == 9)
                {
                    ClearRank8(cfg, program);
                }
            }

            var sites = new Dictionary<int, List<Item>>();
            AppendDaySiteEvetsC(cfg, sites, programs);
            foreach (var kv in sites)
            {
                var list = kv.Value;
                var no = 1;
                for (var i = 0; i < list.Count; i++)
                {
                    var item = list[i];
                    UpdateTreeNo(cfg, item, no);
                    no++;
                }
            }
        }

        private void ResetEventNumberD(TConfig cfg, Item itmReturn)
        {
            //修正組別場地場次序
            FixProgramMatNo(cfg);

            var programs = MeetingProgramItems(cfg);
            for (var i = 0; i < programs.Count; i++)
            {
                var program = programs[i];
                ClearTreeNo(cfg, program);
                if (program.reference != null && program.reference.cancelEvents != null)
                {
                    CancelEvents(cfg, program, program.reference.cancelEvents);
                }
                if (program.team_count == 9)
                {
                    ClearRank8(cfg, program);
                }
            }

            var sites = new Dictionary<int, List<Item>>();
            AppendDaySiteEvetsD(cfg, sites, programs);
            foreach (var kv in sites)
            {
                var list = kv.Value;
                var no = 1;
                for (var i = 0; i < list.Count; i++)
                {
                    var item = list[i];
                    UpdateTreeNo(cfg, item, no);
                    no++;
                }
            }
        }

        private void ResetForTopTwo(TConfig cfg, Item itmReturn)
        {
            var sql = @"
                UPDATE t2 SET
                	t2.in_tree_rank = 'rank23'
                	, t2.in_tree_rank_ns = '0,3'
                	, t2.in_tree_rank_nss = '0,3'
                FROM 
                	IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
                	IN_MEETING_PEVENT t2 WITH(NOLOCK)
                	ON t2.source_id = t1.id
                WHERE
                	t1.in_meeting = '{#meeting_id}'
                	AND t1.in_fight_day = '{#in_fight_day}'
                	AND t1.in_team_count >= 6
                	AND t2.in_fight_id IN ('M004-01', 'M004-02')
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_fight_day}", cfg.in_fight_day);

            cfg.inn.applySQL(sql);
        }

        private void ClearEventNumber(TConfig cfg, Item itmReturn)
        {
            //修正組別場地場次序
            FixProgramMatNo(cfg);

            var programs = MeetingProgramItems(cfg);
            for (var i = 0; i < programs.Count; i++)
            {
                var program = programs[i];

                UpdateEventCount(cfg, program);

                ClearTreeNo(cfg, program);

                if (program.reference != null && program.reference.cancelEvents != null)
                {
                    CancelEvents(cfg, program, program.reference.cancelEvents);
                }

                if (program.team_count == 9)
                {
                    ClearRank8(cfg, program);
                }
            }

        }

        //修正組別場地場次序
        private void FixProgramMatNo(TConfig cfg)
        {
            string sql = @"
                UPDATE t1 SET
                	t1.in_site_mat = in_code_en + '-' + CAST(rn AS VARCHAR)
                	, t1.in_site_mat2 = in_code_en + '-' + RIGHT(REPLICATE('0', 2) + CAST(rn as VARCHAR), 2)
                FROM
                	IN_MEETING_PROGRAM t1
                INNER JOIN
                (
                	SELECT
                		t11.in_program
                		, t12.in_code_en
                		, ROW_NUMBER() OVER (PARTITION BY t12.in_code ORDER BY t11.created_on) AS 'rn'
                	FROM
                		IN_MEETING_ALLOCATION t11 WITH(NOLOCK)
                	INNER JOIN
                		IN_MEETING_SITE t12 WITH(NOLOCK)
                		ON t12.id = t11.in_site
                	WHERE
                		t11.in_meeting = '{#meeting_id}'
                		AND t11.in_date_key = '{#in_date_key}'
                ) t2 ON t2.in_program = t1.id
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_date_key}", cfg.in_fight_day);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            cfg.inn.applySQL(sql);
        }

        private void UpdateTreeNo(TConfig cfg, Item item, int no)
        {
            var event_id = item.getProperty("id", "");

            var sql = "UPDATE IN_MEETING_PEVENT SET"
                + "  in_tree_no = " + no
                + ", in_show_serial = " + no
                + ", in_show_no = " + no
                + " WHERE id = '" + event_id + "'";

            cfg.inn.applySQL(sql);
        }

        private void AppendDaySiteEvetsC(TConfig cfg, Dictionary<int, List<Item>> sites, List<TProgram> programs)
        {
            AppendDaySiteEvents1(cfg, sites, programs, "M128");
            AppendDaySiteEvents1(cfg, sites, programs, "M064");
            AppendDaySiteEvents1(cfg, sites, programs, "M032");
            AppendDaySiteEvents1(cfg, sites, programs, "M016");

            AppendDaySiteEvents1(cfg, sites, programs, "RBN5-01");//(循) 1 / 5
            AppendDaySiteEvents1(cfg, sites, programs, "RBN5-02");
            AppendDaySiteEvents1(cfg, sites, programs, "M008");
            AppendDaySiteEvents1(cfg, sites, programs, "CRS7-01");
            AppendDaySiteEvents1(cfg, sites, programs, "CRS7-02");
            AppendDaySiteEvents1(cfg, sites, programs, "CRS7-07");
            AppendDaySiteEvents1(cfg, sites, programs, "CRS6-01");
            AppendDaySiteEvents1(cfg, sites, programs, "CRS6-04");
            AppendDaySiteEvents1(cfg, sites, programs, "RBN5-03");//(循) 2 / 5
            AppendDaySiteEvents1(cfg, sites, programs, "RBN5-04");

            AppendDaySiteEvents1(cfg, sites, programs, "M004");//準決賽-打完出敗部
            AppendDaySiteEvents1(cfg, sites, programs, "CRS7-03");
            AppendDaySiteEvents1(cfg, sites, programs, "CRS7-04");
            AppendDaySiteEvents1(cfg, sites, programs, "CRS7-08");
            AppendDaySiteEvents1(cfg, sites, programs, "CRS6-02");
            AppendDaySiteEvents1(cfg, sites, programs, "CRS6-05");
            AppendDaySiteEvents1(cfg, sites, programs, "RBN5-05");//(循) 3 / 5
            AppendDaySiteEvents1(cfg, sites, programs, "RBN5-06");
            AppendDaySiteEvents1(cfg, sites, programs, "RBN4-01");
            AppendDaySiteEvents1(cfg, sites, programs, "RBN4-02");
            AppendDaySiteEvents1(cfg, sites, programs, "RBN3-01");

            AppendDaySiteEvents1(cfg, sites, programs, "R064");
            AppendDaySiteEvents1(cfg, sites, programs, "R032");
            AppendDaySiteEvents1(cfg, sites, programs, "R016");
            AppendDaySiteEvents1(cfg, sites, programs, "RBN5-07");//(循) 4 / 5
            AppendDaySiteEvents1(cfg, sites, programs, "RBN5-08");
            AppendDaySiteEvents1(cfg, sites, programs, "RBN4-03");
            AppendDaySiteEvents1(cfg, sites, programs, "RBN4-04");
            AppendDaySiteEvents1(cfg, sites, programs, "RBN3-02");

            AppendDaySiteEvents1(cfg, sites, programs, "R008");
            AppendDaySiteEvents1(cfg, sites, programs, "CRS7-05");
            AppendDaySiteEvents1(cfg, sites, programs, "CRS7-06");
            AppendDaySiteEvents1(cfg, sites, programs, "CRS7-09");
            AppendDaySiteEvents1(cfg, sites, programs, "CRS6-03");
            AppendDaySiteEvents1(cfg, sites, programs, "CRS6-06");
            AppendDaySiteEvents1(cfg, sites, programs, "RBN5-09");//(循) 5 / 5
            AppendDaySiteEvents1(cfg, sites, programs, "RBN5-10");
            AppendDaySiteEvents1(cfg, sites, programs, "RBN4-05");
            AppendDaySiteEvents1(cfg, sites, programs, "RBN4-06");
            AppendDaySiteEvents1(cfg, sites, programs, "RBN3-03");

            // //九取七
            // AppendDaySiteEvents3(cfg, sites, programs, "RNK78-01", 9);
            // //七取五，分組循環賽打完，打五六名
            // AppendDaySiteEvents3(cfg, sites, programs, "RNK56-01", 7);

            AppendDaySiteEvents2(cfg, sites, programs, "M004");
            AppendDaySiteEvents1(cfg, sites, programs, "R004");
            AppendDaySiteEvents1(cfg, sites, programs, "M002");
        }

        private void AppendDaySiteEvetsD(TConfig cfg, Dictionary<int, List<Item>> sites, List<TProgram> programs)
        {
            AppendDaySiteEvents1(cfg, sites, programs, "M128");
            AppendDaySiteEvents1(cfg, sites, programs, "M064");
            AppendDaySiteEvents1(cfg, sites, programs, "M032");
            AppendDaySiteEvents1(cfg, sites, programs, "M016");

            AppendDaySiteEvents1(cfg, sites, programs, "RBN5-01");//(循) 1 / 5
            AppendDaySiteEvents1(cfg, sites, programs, "RBN5-02");

            AppendDaySiteEvents1(cfg, sites, programs, "M008");
            AppendDaySiteEvents1(cfg, sites, programs, "CRS7-01");
            AppendDaySiteEvents1(cfg, sites, programs, "CRS7-02");
            AppendDaySiteEvents1(cfg, sites, programs, "CRS7-07");
            AppendDaySiteEvents1(cfg, sites, programs, "CRS6-01");
            AppendDaySiteEvents1(cfg, sites, programs, "CRS6-04");
            AppendDaySiteEvents1(cfg, sites, programs, "RBN5-03");//(循) 2 / 5
            AppendDaySiteEvents1(cfg, sites, programs, "RBN5-04");

            AppendDaySiteEvents1(cfg, sites, programs, "M004");//準決賽-打完出敗部
            AppendDaySiteEvents1(cfg, sites, programs, "CRS7-03");
            AppendDaySiteEvents1(cfg, sites, programs, "CRS7-04");
            AppendDaySiteEvents1(cfg, sites, programs, "CRS7-08");
            AppendDaySiteEvents1(cfg, sites, programs, "CRS6-02");
            AppendDaySiteEvents1(cfg, sites, programs, "CRS6-05");
            AppendDaySiteEvents1(cfg, sites, programs, "RBN5-05");//(循) 3 / 5
            AppendDaySiteEvents1(cfg, sites, programs, "RBN5-06");
            AppendDaySiteEvents1(cfg, sites, programs, "RBN4-01");
            AppendDaySiteEvents1(cfg, sites, programs, "RBN4-02");
            AppendDaySiteEvents1(cfg, sites, programs, "RBN3-01");

            // AppendDaySiteEvents1(cfg, sites, programs, "R064");
            // AppendDaySiteEvents1(cfg, sites, programs, "R032");
            // AppendDaySiteEvents1(cfg, sites, programs, "R016");
            AppendDaySiteEvents1(cfg, sites, programs, "RBN5-07");//(循) 4 / 5
            AppendDaySiteEvents1(cfg, sites, programs, "RBN5-08");
            AppendDaySiteEvents1(cfg, sites, programs, "RBN4-03");
            AppendDaySiteEvents1(cfg, sites, programs, "RBN4-04");
            AppendDaySiteEvents1(cfg, sites, programs, "RBN3-02");

            //AppendDaySiteEvents1(cfg, sites, programs, "R008");
            AppendDaySiteEvents1(cfg, sites, programs, "CRS7-05");
            AppendDaySiteEvents1(cfg, sites, programs, "CRS7-06");
            AppendDaySiteEvents1(cfg, sites, programs, "CRS7-09");
            AppendDaySiteEvents1(cfg, sites, programs, "CRS6-03");
            AppendDaySiteEvents1(cfg, sites, programs, "CRS6-06");
            AppendDaySiteEvents1(cfg, sites, programs, "RBN5-09");//(循) 5 / 5
            AppendDaySiteEvents1(cfg, sites, programs, "RBN5-10");
            AppendDaySiteEvents1(cfg, sites, programs, "RBN4-05");
            AppendDaySiteEvents1(cfg, sites, programs, "RBN4-06");
            AppendDaySiteEvents1(cfg, sites, programs, "RBN3-03");

            // //九取七
            // AppendDaySiteEvents3(cfg, sites, programs, "RNK78-01", 9);
            // //七取五，分組循環賽打完，打五六名
            // AppendDaySiteEvents3(cfg, sites, programs, "RNK56-01", 7);

            //AppendDaySiteEvents2(cfg, sites, programs, "M004");
            //AppendDaySiteEvents1(cfg, sites, programs, "R004");
            //AppendDaySiteEvents1(cfg, sites, programs, "M002");
        }

        private void AppendDaySiteEvents1(TConfig cfg, Dictionary<int, List<Item>> sites, List<TProgram> programs, string prefix)
        {
            for (var i = 0; i < programs.Count; i++)
            {
                var program = programs[i];
                if (prefix == "M004" && program.team_count <= 7) continue;

                var itmEvts = GetEventItems(cfg, program, prefix);
                var evt_count = itmEvts.getItemCount();

                for (var j = 0; j < evt_count; j++)
                {
                    var itmEvt = itmEvts.getItemByIndex(j);
                    if (sites.ContainsKey(program.site_code))
                    {
                        sites[program.site_code].Add(itmEvt);
                    }
                    else
                    {
                        var list = new List<Item> { itmEvt };
                        sites.Add(program.site_code, list);
                    }
                }
            }
        }

        private void AppendDaySiteEvents2(TConfig cfg, Dictionary<int, List<Item>> sites, List<TProgram> programs, string prefix)
        {
            for (var i = 0; i < programs.Count; i++)
            {
                var program = programs[i];

                var itmEvts = GetEventItems(cfg, program, prefix);
                var evt_count = itmEvts.getItemCount();
                if (prefix == "M004" && program.team_count >= 8) continue;

                for (var j = 0; j < evt_count; j++)
                {
                    var itmEvt = itmEvts.getItemByIndex(j);
                    if (sites.ContainsKey(program.site_code))
                    {
                        sites[program.site_code].Add(itmEvt);
                    }
                    else
                    {
                        var list = new List<Item> { itmEvt };
                        sites.Add(program.site_code, list);
                    }
                }
            }
        }

        private void AppendDaySiteEvents3(TConfig cfg, Dictionary<int, List<Item>> sites, List<TProgram> programs, string in_fight_id, int team_count)
        {
            for (var i = 0; i < programs.Count; i++)
            {
                var program = programs[i];

                var itmEvts = GetEventItems(cfg, program, in_fight_id);
                var evt_count = itmEvts.getItemCount();
                if (program.team_count != team_count) continue;

                for (var j = 0; j < evt_count; j++)
                {
                    var itmEvt = itmEvts.getItemByIndex(j);
                    if (sites.ContainsKey(program.site_code))
                    {
                        sites[program.site_code].Add(itmEvt);
                    }
                    else
                    {
                        var list = new List<Item> { itmEvt };
                        sites.Add(program.site_code, list);
                    }
                }
            }
        }

        private Item GetEventItems(TConfig cfg, TProgram program, string prefix)
        {
            var sql = @"
                SELECT 
                    id 
                FROM 
                    IN_MEETING_PEVENT WITH(NOLOCK) 
                WHERE 
                    source_id = '{#program_id}' 
                    AND ISNULL(in_bypass_status, 0) <> 1
                    AND ISNULL(in_win_status, '') <> 'cancel'
                    AND in_fight_id LIKE '{#prefix}%'
                ORDER BY
                    in_fight_id
            ";

            sql = sql.Replace("{#program_id}", program.program_id)
                .Replace("{#prefix}", prefix);

            return cfg.inn.applySQL(sql);
        }


        private void CancelEvents(TConfig cfg, TProgram program, List<string> evts)
        {
            if (evts.Count == 0) return;

            var values = string.Join(", ", evts.Select(x => "'" + x + "'"));

            var sql = "UPDATE IN_MEETING_PEVENT SET"
                + "  in_win_status = 'cancel'"
                + " WHERE source_id = '" + program.program_id + "'"
                + " AND in_fight_id IN (" + values + ")";

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            cfg.inn.applySQL(sql);
        }

        private void ClearTreeNo(TConfig cfg, TSect program)
        {
            var sql = "UPDATE IN_MEETING_PEVENT SET"
                + "  in_tree_no = NULL"
                + ", in_site = '" + program.site_id + "'"
                + ", in_site_code = " + program.site_code
                + ", in_show_site = " + program.site_code
                + ", in_show_serial = NULL"
                + ", in_site_move = NULL"
                + ", in_site_time = NULL"
                + " WHERE source_id = '" + program.program_id + "'";
            cfg.inn.applySQL(sql);


            var sql2 = "UPDATE IN_MEETING_PEVENT SET"
                + "  in_win_status = NULL"
                + " WHERE source_id = '" + program.program_id + "'"
                + " AND ISNULL(in_win_status, '') = 'cancel'";
            cfg.inn.applySQL(sql2);
        }
        private void ClearTreeNo(TConfig cfg, TProgram program)
        {
            var sql = "UPDATE IN_MEETING_PEVENT SET"
                + "  in_tree_no = NULL"
                + ", in_site = '" + program.site_id + "'"
                + ", in_site_code = " + program.site_code
                + ", in_show_site = " + program.site_code
                + ", in_show_serial = NULL"
                + ", in_site_move = NULL"
                + ", in_site_time = NULL"
                + " WHERE source_id = '" + program.program_id + "'";
            cfg.inn.applySQL(sql);

            var sql2 = "UPDATE IN_MEETING_PEVENT SET"
                + "  in_win_status = NULL"
                + " WHERE source_id = '" + program.program_id + "'"
                + " AND ISNULL(in_win_status, '') = 'cancel'";
            cfg.inn.applySQL(sql2);
        }

        private void UpdateEventCount(TConfig cfg, TProgram program)
        {
            var sql = "UPDATE IN_MEETING_PROGRAM SET in_event_count = " + program.event_count + " WHERE id = '" + program.program_id + "'";
            cfg.inn.applySQL(sql);
        }

        private void ClearRank8(TConfig cfg, TProgram row)
        {
            var sql = "UPDATE IN_MEETING_PEVENT SET in_tree_rank_ns = '7,0', in_tree_rank_nss = '7,0' WHERE source_id = '" + row.program_id + "' AND in_fight_id = 'RNK78-01'";
            cfg.inn.applySQL(sql);
        }

        private List<TProgram> MeetingProgramItems(TConfig cfg)
        {
            var siteCond = "";
            if (cfg.in_site_code != "")
            {
                siteCond = "AND t3.in_site_code = " + cfg.in_site_code;
                if (cfg.in_group_tag != "")
                {
                    siteCond += " AND ISNULL(t3.in_group_tag, '') = '" + cfg.in_group_tag + "'";
                }
            }

            var orderBy = "t2.in_code, t3.in_site_mat2";
            if (cfg.allocate_which_first == "more_first")
            {
                orderBy = "t2.in_code, t3.in_team_count DESC, t3.in_site_mat2";
            }

            var sql = @"
                SELECT 
	                t1.in_date_key
	                , t2.id			AS 'site_id'
	                , t2.in_code	AS 'site_code'
	                , t2.in_name	AS 'site_name'
	                , t3.*
                FROM
	                IN_MEETING_ALLOCATION t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_SITE t2 WITH(NOLOCK)
	                ON t2.in_meeting = t1.in_meeting
	                AND t2.id = t1.in_site
                INNER JOIN
	                IN_MEETING_PROGRAM t3 WITH(NOLOCK)
	                ON t3.in_meeting = t1.in_meeting
	                AND t3.id = t1.in_program
                WHERE
	                t1.in_meeting = '{#meeting_id}'
	                AND t1.in_date_key = '{#in_fight_day}'
	                AND t3.in_team_count > 1
	                {#siteCond}
                ORDER BY
	                {#orderBy}
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_fight_day}", cfg.in_fight_day)
                .Replace("{#siteCond}", siteCond)
                .Replace("{#orderBy}", orderBy);

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            if (items.isError() || items.getResult() == "")
            {
                throw new Exception("查無組別資訊");
            }

            var list = new List<TProgram>();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var p = new TProgram
                {
                    program_id = item.getProperty("id", ""),
                    site_id = item.getProperty("site_id", ""),
                    site_code = GetInt(item.getProperty("site_code", "0")),
                    in_team_count = item.getProperty("in_team_count", "0"),
                };

                p.team_count = GetInt(p.in_team_count);
                if (!cfg.teamCountMap.ContainsKey(p.team_count))
                {
                    throw new Exception("[" + p.team_count + "]未設定場次數");
                }

                p.setting = cfg.teamCountMap[p.team_count];
                if (p.setting.cancelReference > 0)
                {
                    p.reference = cfg.teamCountMap[p.setting.cancelReference];
                }

                p.event_count = p.setting.eventCount;

                list.Add(p);
            }
            return list;
        }

        private string GetMeetingVariable(TConfig cfg, string in_key)
        {
            var sql = "SELECT id, in_value FROM IN_MEETING_VARIABLE WITH(NOLOCK)"
                + " WHERE source_id = '" + cfg.meeting_id + "'"
                + " AND in_key = '" + in_key + "'";

            var itmVariable = cfg.inn.applySQL(sql);

            if (itmVariable.isError() || itmVariable.getResult() == "")
            {
                return "";
            }
            else
            {
                return itmVariable.getProperty("in_value", "");
            }
        }

        private Dictionary<int, TCountSetting> GenerateTeamCountMap(TConfig cfg)
        {
            var map = new Dictionary<int, TCountSetting>();

            var sql = @"
                SELECT
                    id
	                , in_mode
	                , in_team_count
	                , in_event_count
	                , in_cancel_reference
                FROM
	                IN_MEETING_DRAWSHEET WITH(NOLOCK)
	            WHERE
	                in_mode = '{#in_mode}'
                ORDER BY
	                in_team_count
            ";

            sql = sql.Replace("{#in_mode}", cfg.in_battle_mode);

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            var ns = new Dictionary<int, int>();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var teamCount = GetInt(item.getProperty("in_team_count", "0"));
                if (map.ContainsKey(teamCount)) throw new Exception("in_team_count 重複: GenerateTeamCountMap");

                var row = new TCountSetting
                {
                    id = item.getProperty("id", ""),
                    teamCount = teamCount,
                    eventCount = GetInt(item.getProperty("in_event_count", "0")),
                    cancelReference = GetInt(item.getProperty("in_cancel_reference", "0")),
                    cancelEvents = new List<string>(),
                };

                map.Add(teamCount, row);

                if (row.cancelReference > 0 && !ns.ContainsKey(row.cancelReference))
                {
                    ns.Add(row.cancelReference, row.cancelReference);
                }
            }

            foreach (var kv in ns)
            {
                var teamCount = kv.Value;
                if (!map.ContainsKey(teamCount)) throw new Exception("in_team_count 異常: GenerateTeamCountMap #2");
                var row = map[teamCount];
                AppendCancelEvent(cfg, row);
            }

            return map;
        }

        private void AppendCancelEvent(TConfig cfg, TCountSetting row)
        {
            var sql = @"
                SELECT
	                in_fight_id
                FROM
	                IN_MEETING_DRAWSHEET_CANCEL WITH(NOLOCK)
                WHERE
	                source_id = '{#source_id}'
            ";

            sql = sql.Replace("{#source_id}", row.id);

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var in_fight_id = item.getProperty("in_fight_id", "");
                row.cancelEvents.Add(in_fight_id);
            }
        }

        private Item GetGroupSectEventItems(TConfig cfg, string in_fight_day, string in_site_code)
        {
            var sql = @"
                SELECT
	                t1.in_battle_mode
	                , t2.id AS 'program_id'
	                , t3.id AS 'event_id'
	                , t2.in_name
	                , t2.in_name2
	                , t2.in_l1
	                , t2.in_l2
	                , t2.in_l3
	                , t2.in_site_mat2
	                , t2.in_team_count
	                , t2.in_battle_type
	                , t3.in_tree_name
	                , t3.in_tree_id
	                , t3.in_tree_no
	                , t3.in_fight_id
	                , t3.in_tree_sno
	                , t3.in_round
                FROM
	                IN_MEETING t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PROGRAM t2 WITH(NOLOCK)
	                ON t2.in_meeting = t1.id
                INNER JOIN
	                IN_MEETING_PEVENT t3 WITH(NOLOCK)
	                ON t3.source_id = t2.id
                INNER JOIN
	                IN_MEETING_SITE t4 WITH(NOLOCK)
	                ON t4.id = t2.in_site
                WHERE
	                t2.in_meeting = '{#meeting_id}'
	                AND t2.in_fight_day = '{#in_fight_day}'
	                AND t2.in_site_code = '{#in_site_code}'
	                AND t3.in_fight_id NOT IN ('RNK34-01', 'RNK56-01', 'RNK78-01')
                ORDER BY
	                t2.in_site_mat2
	                , t2.id
	                , t3.in_tree_id
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_fight_day}", in_fight_day)
                .Replace("{#in_site_code}", in_site_code);

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        private Item GetMeetingSiteItems(TConfig cfg)
        {
            var sql = "SELECT * FROM IN_MEETING_SITE WITH(NOLOCK) WHERE in_meeting = '" + cfg.meeting_id + "' ORDER BY in_code";
            return cfg.inn.applySQL(sql);
        }

        private class TCountSetting
        {
            public string id { get; set; }
            public int teamCount { get; set; }
            public int eventCount { get; set; }
            public int cancelReference { get; set; }
            public List<string> cancelEvents { get; set; }
        }

        private class TProgram
        {
            public string program_id { get; set; }
            public string in_team_count { get; set; }
            public string site_id { get; set; }
            public int site_code { get; set; }
            public int team_count { get; set; }
            public int event_count { get; set; }
            public TCountSetting setting { get; set; }
            public TCountSetting reference { get; set; }
        }

        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string meeting_id { get; set; }
            public string in_fight_day { get; set; }
            public string in_site_code { get; set; }
            public string in_group_tag { get; set; }
            public string in_event_span { get; set; }
            public bool no_rank12 { get; set; }
            public string scene { get; set; }

            public Item itmMeeting { get; set; }
            public string in_battle_mode { get; set; }
            public string in_battle_repechage { get; set; }

            public string allocate_which_first { get; set; }
            public string need_rpc_number { get; set; }
            public Dictionary<int, TCountSetting> teamCountMap { get; set; }
            public int eventSpan { get; set; }
        }

        private int GetInt(string value, int def = 0)
        {
            int result;
            int.TryParse(value, out result);
            return result;
        }
    }
}