﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Wrestling.Common
{
    public class In_Meeting_Program_ShortName : Item
    {
        public In_Meeting_Program_ShortName(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            var itmR = this;
            string in_l1 = itmR.getProperty("in_l1", "").Trim();
            string in_l2 = itmR.getProperty("in_l2", "").Trim();
            string in_l3 = itmR.getProperty("in_l3", "").Trim();
            
            if (in_l2 == "希羅式" || in_l2 == "自由式")
            {
                Mode1(in_l1, in_l2, in_l3, itmR);
            }
            else
            {
                Mode2(in_l1, in_l2, in_l3, itmR);
            }

            return itmR;
        }

        private void Mode2(string in_l1, string in_l2, string in_l3, Item itmReturn)
        {
            //高中部男生組希羅式第八級：82.1 公斤以上至 87.0 公斤以下

            string in_section_name = itmReturn.getProperty("in_section_name", "");

            string age = GetAgeStr(in_l2);
            string gender = GetGenderStr(in_l2);
            string typeName = GetTypeStr(in_l2);
            string lv = GetLevelNumStr(in_l3);

            string result = age + gender + typeName + lv;

            this.setProperty("result", result);
            this.setProperty("weight", GetWeightStr2(in_l3));
        }

        private void Mode1(string in_l1, string in_l2, string in_l3, Item itmReturn)
        {
            //高中部男生組希羅式第八級：82.1 公斤以上至 87.0 公斤以下
          
            string in_section_name = itmReturn.getProperty("in_section_name", "");

            string age = GetAgeStr(in_l1);
            string gender = GetGenderStr(in_l1);
            string typeName = GetTypeStr(in_l2);
            string lv = GetLevelNumStr(in_l3);

            string result = age + gender + typeName + lv;

            this.setProperty("result", result);
            this.setProperty("weight", GetWeightStr1(in_section_name));
        }

        private string GetTypeStr(string value)
        {
            switch (value)
            {
                case "希羅": return "希羅";
                case "希羅式": return "希羅";

                case "自由": return "自由";
                case "自由式": return "自由";

                default: return "";
            }
        }

        private string GetAgeStr(string value)
        {
            switch (value)
            {
                case "公開男生組": return "公";
                case "公開男子組": return "公";
                case "公開女生組": return "公";
                case "公開女子組": return "公";

                case "一般男生組": return "一";
                case "一般男子組": return "一";
                case "一般女生組": return "一";
                case "一般女子組": return "一";

                case "國中部男生組": return "國";
                case "國中男子組": return "國";
                case "國男組": return "國";
                case "國男": return "國";

                case "國中部女生組": return "國";
                case "國中女子組": return "國";
                case "國女組": return "國";
                case "國女": return "國";

                case "高中部男生組": return "高";
                case "高中男子組": return "高";
                case "高男組": return "高";
                case "高男": return "高";

                case "高中部女生組": return "高";
                case "高中女子組": return "高";
                case "高女組": return "高";
                case "高女": return "高";

                case "國小高年級男生組": return "小高";
                case "國小高年級女生組": return "小高";
                case "國小高年級男女混合組": return "小高";

                case "國小中年級男生組": return "小中";
                case "國小中年級女生組": return "小中";
                case "國小中年級男女混合組": return "小中";

                case "國小低年級男生組": return "小低";
                case "國小低年級女生組": return "小低";
                case "國小低年級男女混合組": return "小低";
            }

            if (value.Contains("社會")) return "社";
            else if (value.Contains("一般")) return "一";
            else if (value.Contains("公開")) return "公";
            else if (value.Contains("大專")) return "大";
            else if (value.Contains("高中")) return "高";
            else if (value.Contains("國中")) return "國";
            else if (value.Contains("國小")) return "小";
            else if (value.Contains("成人")) return "成";
            else if (value.Contains("青年")) return "青";
            else if (value.Contains("青少年")) return "青少";
            else if (value.Contains("少年")) return "少";
            return "";
        }

        private string GetGenderStr(string value)
        {
            if (value.Contains("男")) return "男";
            else if (value.Contains("女")) return "女";
            else if (value.Contains("男生")) return "男";
            else if (value.Contains("男子")) return "男";
            else if (value.Contains("女生")) return "女";
            else if (value.Contains("女子")) return "女";
            else if (value.Contains("混合")) return "混";
            return "";
        }

        private string GetLevelNumStr(string value)
        {
            if (value.Contains("第一級")) return "一";
            else if (value.Contains("第二級")) return "二";
            else if (value.Contains("第三級")) return "三";
            else if (value.Contains("第四級")) return "四";
            else if (value.Contains("第五級")) return "五";
            else if (value.Contains("第六級")) return "六";
            else if (value.Contains("第七級")) return "七";
            else if (value.Contains("第八級")) return "八";
            else if (value.Contains("第九級")) return "九";
            else if (value.Contains("第十級")) return "十";
            else if (value.Contains("第十一級")) return "十一";
            else if (value.Contains("第十二級")) return "十二";
            else if (value.Contains("第十三級")) return "十三";
            else if (value.Contains("第十四級")) return "十四";
            else if (value.Contains("第十五級")) return "十五";
            return "";
        }

        private string GetWeightStr1(string in_l3)
        {
            //希羅式第一級：41.1 公斤以上至 45.0 公斤以下
            //希羅式第一級：55.0 公斤以下

            var value = in_l3;
            if (!value.EndsWith("級") && !value.Contains("級："))
            {
                value = value.Replace("級", "級：");
            }

            //高中部男生組希羅式第七級：77.1 公斤以上至 82.0 公斤以下
            var arr = value.Split(new char[] { '：' }, StringSplitOptions.RemoveEmptyEntries);
            if (arr == null || arr.Length < 2) return "";

            var weight = "";
            var wstr = arr[1];
            var opt = "-";
            if (wstr.Contains("以上至"))
            {
                //77.1 公斤以上至 82.0 公斤以下
                var arr2 = wstr.Split(new string[] { "以上至" }, StringSplitOptions.RemoveEmptyEntries);
                weight = arr2[1].Replace(".0", "").Replace("公斤以下", "");

            }
            else if (wstr.Contains("公斤以下"))
            {
                //55.0 公斤以下
                weight = wstr.Replace(".0", "").Replace("公斤以下", "");
            }
            else
            {
                //55.0 公斤以上
                opt = "+";
                weight = wstr.Replace(".0", "").Replace("公斤以上", "");
            }

            weight = opt + weight.Trim() + "kg";
            return weight;
        }

        private string GetWeightStr2(string in_l3)
        {
            //第三級：25kg 以下
            var value = in_l3.ToLower();
            if (!value.EndsWith("級") && !value.Contains("級："))
            {
                value = value.Replace("級", "級：");
            }
            value = value.Replace("kg ", "公斤");

            var arr = value.Split(new char[] { '：' }, StringSplitOptions.RemoveEmptyEntries);
            if (arr == null || arr.Length < 2) return "";

            var weight = "";
            var wstr = arr[1];//25公斤以下 OR 30.1公斤以上
            var opt = "-";

            if (wstr.Contains("以上至"))
            {
                //77.1 公斤以上至 82.0 公斤以下
                var arr2 = wstr.Split(new string[] { "以上至" }, StringSplitOptions.RemoveEmptyEntries);
                weight = arr2[1].Replace(".0", "").Replace("公斤以下", "");

            }
            else if (wstr.Contains("公斤以下"))
            {
                //55.0 公斤以下
                weight = wstr.Replace(".0", "").Replace("公斤以下", "");
            }
            else
            {
                //55.0 公斤以上
                opt = "+";
                weight = wstr.Replace(".0", "").Replace(".1", "").Replace("公斤以上", "");
            }

            weight = opt + weight.Trim() + "kg";
            return weight;
        }
    }
}