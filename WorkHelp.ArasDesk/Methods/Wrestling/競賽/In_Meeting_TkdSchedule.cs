﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Wrestling.Common
{
    public class In_Meeting_TkdSchedule : Item
    {
        public In_Meeting_TkdSchedule(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 場次編號匯入
                日誌: 
                    - 2022-10-03: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "In_Meeting_TkdSchedule";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                strIdentityId = inn.getUserAliases(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                scene = itmR.getProperty("scene", ""),
                need_clear_rpc = itmR.getProperty("need_clear_rpc", ""),
            };

            cfg.scene = "xls_import";

            switch (cfg.scene)
            {
                case "xls_import":
                    XlsImport(cfg, itmR);
                    break;

                case "import":
                    Import(cfg, itmR);
                    break;

                case "match_tree_no": //匹配場次編號
                    MatchTreeNo(cfg, itmR);
                    break;

                case "reset_local_schedule":
                    UpdateInLocalScheduleAgain(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void XlsImport(TConfig cfg, Item itmReturn)
        {
            var list = new List<TRow>();
            var sql = @"SELECT * FROM AAA_Schedule_20230425 WITH(NOLOCK) ORDER BY no";
            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();

        }

        private string ClearTrim(string v)
        {
            
        }

        private List<TRow> ListFromXlsImport(TConfig cfg, Item itmReturn)
        {
            var list = new List<TRow>();
            var sql = @"SELECT * FROM AAA_Schedule_20230425 WITH(NOLOCK) ORDER BY no";
            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var row = new TRow();
                row.c00 = ClearTrim(item.getProperty("no", ""));
                'no': rows.length + 1,
				'c00': ClearTrim(src_row["No"]),
				'c01': ClearTrim(src_row["Event_Num"]),
				'c02': ClearTrim(src_row["Site"]),
				'c03': ClearTrim(src_row["EGroup"]),
				'c04': ClearTrim(src_row["ESystem"]),//場次類型代號
				'c05': ClearTrim(src_row["Code"]),
				'c06': ClearTrim(src_row["Item"]),
				'c07': ClearTrim(src_row["EGrade"]),
				'c08': ClearTrim(src_row["Weight"]),
				'c09': ClearTrim(src_row["Red"]),
				'c10': ClearTrim(src_row["Red_Aplseq"]),
				'c11': ClearTrim(src_row["Red_Dptname"]),
				'c12': ClearTrim(src_row["Blue"]),
				'c13': ClearTrim(src_row["Blue_Aplseq"]),
				'c14': ClearTrim(src_row["Blue_Dptname"]),
				'c15': ClearTrim(src_row["Weight_No"]),
				'c16': ClearTrim(src_row["ERound"]),
				'c17': ClearTrim(src_row["Next_Site"]),
				'c18': ClearTrim(src_row["Next_Round"]),
				'c19': ClearTrim(src_row["Next_BR"]),
				'c20': ClearTrim(src_row["Lose_Site"]),
				'c21': ClearTrim(src_row["Lose_Round"]),
				'c22': ClearTrim(src_row["Lose_BR"]),
				'c31': ClearTrim(src_row["ERound_F"]),
				'c32': ClearTrim(src_row["Next_Site_F"]),
				'c33': ClearTrim(src_row["Next_Round_F"]),
				'c34': ClearTrim(src_row["Next_BR_F"]),
				'c35': ClearTrim(src_row["Lose_Site_F"]),
				'c36': ClearTrim(src_row["Lose_Round_F"]),
				'c37': ClearTrim(src_row["Lose_BR_F"]),
                
            }
            return list;
        }

        private void UpdateInLocalScheduleAgain(TConfig cfg, Item itmReturn)
        {
            var code = "";
            var sql = "SELECT id, item_number FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'";
            var itmMeeting = cfg.inn.applySQL(sql);
            var item_number = itmMeeting.getProperty("item_number", "");

            code = "128";
            RunUpdateInLocalSchedule(cfg, item_number, code);
            RunLocalSync(cfg);
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "FINISHED: " + code);


            code = "64";
            RunUpdateInLocalSchedule(cfg, item_number, code);
            RunLocalSync(cfg);
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "FINISHED: " + code);


            code = "32";
            RunUpdateInLocalSchedule(cfg, item_number, code);
            RunLocalSync(cfg);
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "FINISHED: " + code);


            code = "16";
            RunUpdateInLocalSchedule(cfg, item_number, code);
            RunLocalSync(cfg);
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "FINISHED: " + code);


            code = "8";
            RunUpdateInLocalSchedule(cfg, item_number, code);
            RunLocalSync(cfg);
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "FINISHED: " + code);


            code = "4";
            RunUpdateInLocalSchedule(cfg, item_number, code);
            RunLocalSync(cfg);
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "FINISHED: " + code);


            code = "2";
            RunUpdateInLocalSchedule(cfg, item_number, code);
            RunLocalSync(cfg);
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "FINISHED: " + code);


            code = "34";
            RunUpdateInLocalSchedule(cfg, item_number, code);
            RunLocalSync(cfg);
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "FINISHED: " + code);


            code = "BC04";
            RunUpdateInLocalSchedule(cfg, item_number, code);
            RunLocalSync(cfg);
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "FINISHED: " + code);

            code = "BC03";
            RunUpdateInLocalSchedule(cfg, item_number, code);
            RunLocalSync(cfg);
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "FINISHED: " + code);


            code = "BC02";
            RunUpdateInLocalSchedule(cfg, item_number, code);
            RunLocalSync(cfg);
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "FINISHED: " + code);


            code = "BC01";
            RunUpdateInLocalSchedule(cfg, item_number, code);
            RunLocalSync(cfg);
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "FINISHED: " + code);

            code = "BC3";
            RunUpdateInLocalSchedule(cfg, item_number, code);
            RunLocalSync(cfg);
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "FINISHED: " + code);


            code = "1";
            RunUpdateInLocalSchedule(cfg, item_number, code);
            RunLocalSync(cfg);
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "FINISHED: " + code);


            code = "BC2";
            RunUpdateInLocalSchedule(cfg, item_number, code);
            RunLocalSync(cfg);
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "FINISHED: " + code);

            code = "BC1";
            RunUpdateInLocalSchedule(cfg, item_number, code);
            RunLocalSync(cfg);
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "FINISHED: " + code);

        }

        private void RunUpdateInLocalSchedule(TConfig cfg, string item_number, string esystem)
        {
            var sql = "UPDATE In_Local_Schedule SET syncflag = NULL WHERE mnumber = '" + item_number + "' AND esystem = '" + esystem + "'";
            cfg.inn.applySQL(sql);
        }

        private void RunLocalSync(TConfig cfg)
        {
            Item itmData = cfg.inn.newItem();
            itmData.setType("In_Meeting_PEvent");
            itmData.apply("In_Local_Sync");
        }

        private void MatchTreeNo(TConfig cfg, Item itmReturn)
        {
            // //將場次分配狀態清空
            // ClearAllocateStatus(cfg);

            var itmImports = GetImportItems(cfg);
            var count = itmImports.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var itmImport = itmImports.getItemByIndex(i);
                var row = MapRow(cfg, itmImport);
                RunMapTreeNo(cfg, row);
            }

            // //清空未取得場次號的資料
            // ClearNoMatchTreeNo(cfg);

            //清除敗部場次編號
            if (cfg.need_clear_rpc == "1")
            {
                ClearRepechageTreeNos(cfg, itmReturn);
            }

            //清空人數過少場次
            ClearProgramTreeNo(cfg);
        }

        //清除敗部場次編號
        private void ClearRepechageTreeNos(TConfig cfg, Item itmReturn)
        {
            //將一併清除以下場次編號: 勝部決賽、敗部、分組交叉單淘、七人五六名戰
            var sql = @"
                SELECT DISTINCT
	                t1.c05
	                , t2.id AS 'program_id'
	                , t2.in_name
	                , t2.in_battle_type
                FROM
	                IN_MEETING_TKDSCHEDULE t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PROGRAM t2 WITH(NOLOCK)
	                ON t2.in_meeting = t1.in_meeting
	                AND t2.in_program_no = t1.c05
            ";

            // var sql = @"
            //     SELECT DISTINCT
            //     t1.in_program_no
            //     , t2.id AS 'program_id'
            //     , t2.in_name
            //     , t2.in_battle_type
            //     FROM
            //     IN_MEETING_USER_IMPORTNEW t1 WITH(NOLOCK)
            //     INNER JOIN
            //     IN_MEETING_PROGRAM t2 WITH(NOLOCK)
            //     ON t2.in_meeting = t1.in_meeting
            //     AND t2.in_program_no = t1.in_program_no
            // ";

            var itmPrograms = cfg.inn.applySQL(sql);

            var count = itmPrograms.getItemCount();

            for (var i = 0; i < count; i++)
            {
                var itmProgram = itmPrograms.getItemByIndex(i);
                var program_id = itmProgram.getProperty("program_id", "");
                var in_battle_type = itmProgram.getProperty("in_battle_type", "");

                switch (in_battle_type)
                {
                    case "SingleRoundRobin": //五人以下循環賽制
                        break;

                    case "SixPlayers": //六人分組交叉賽制
                        ClearTreeNoForCross(cfg, program_id);
                        break;

                    case "SevenPlayers": //七人分組交叉賽制
                        ClearTreeNoForCross(cfg, program_id);
                        break;

                    case "WrestlingTopTwo": //八人以上二柱復活賽
                        //清空[決賽]場次編號
                        ClearTreeNoForFinal12(cfg, program_id);
                        ClearTreeNoForRepechage(cfg, program_id);
                        break;
                }
            }
        }

        //清空[決賽]場次編號
        private void ClearTreeNoForFinal12(TConfig cfg, string program_id)
        {
            var sql = @"
                UPDATE t1 SET
	                t1.in_tree_no = NULL
                FROM
	                IN_MEETING_PEVENT t1 WITH(NOLOCK)
                WHERE
	                t1.source_id = '{#program_id}'
	                AND t1.in_tree_name = 'main'
	                AND t1.in_round_code = '2'
            ";

            sql = sql.Replace("{#program_id}", program_id);

            cfg.inn.applySQL(sql);
        }

        //清空[敗部]場次編號
        private void ClearTreeNoForRepechage(TConfig cfg, string program_id)
        {
            var sql = @"
                UPDATE t1 SET
	                t1.in_tree_no = NULL
                FROM
	                IN_MEETING_PEVENT t1 WITH(NOLOCK)
                WHERE
	                t1.source_id = '{#program_id}'
	                AND t1.in_tree_name = 'repechage'
            ";

            sql = sql.Replace("{#program_id}", program_id);

            cfg.inn.applySQL(sql);
        }

        //清空[分組交叉]決賽場次編號
        private void ClearTreeNoForCross(TConfig cfg, string program_id)
        {
            var sql = @"
                UPDATE t1 SET
	                t1.in_tree_no = NULL
                FROM
	                IN_MEETING_PEVENT t1 WITH(NOLOCK)
                WHERE
	                t1.source_id = '{#program_id}'
	                AND t1.in_tree_name = 'cross'
	                AND in_sub_id IN (5)
            ";

            sql = sql.Replace("{#program_id}", program_id);

            cfg.inn.applySQL(sql);
        }

        private void ClearProgramTreeNo(TConfig cfg)
        {
            string sql = @"
                UPDATE t2 SET
                	t2.in_tree_no = NULL
                FROM
                	IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
                	IN_MEETING_PEVENT t2 WITH(NOLOCK)
                	ON t2.source_id = t1.id
                WHERE
                	t1.in_meeting = '{#meeting_id}'
                	AND ISNULL(t1.in_team_count, 0) < 2
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            cfg.inn.applySQL(sql);
        }

        private void ClearNoMatchTreeNo(TConfig cfg)
        {
            var sql = @"
                SELECT 
	                t1.in_l1
	                , t1.in_l2
	                , t1.in_l3
	                , t1.in_program_no
	                , t2.in_tree_name
	                , t2.in_tree_id
	                , t2.in_tree_no
	                , t2.in_fight_id
                FROM 
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PEVENT t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                WHERE 
	                t1.in_meeting = '{#meeting_id}'
	                AND ISNULL(t2.in_tree_no, 0) > 0
	                AND t2.in_site_allocate = 0
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            if (count <= 0) return;

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, items.dom.InnerXml);

            sql = @"
	            UPDATE t2 SET 
					t2.in_tree_no = NULL
					, t2.in_site_allocate = '1'
                FROM 
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PEVENT t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                WHERE 
	                t1.in_meeting = '{#meeting_id}'
	                AND ISNULL(t2.in_tree_no, 0) > 0
	                AND t2.in_site_allocate = 0
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            cfg.inn.applySQL(sql);
        }

        private void RunMapTreeNo(TConfig cfg, TRow row)
        {
            var mat = RunMatch(cfg, row);
            if (mat.q == QueryType.Player)
            {
                var itmEvent1 = QueryMatchEventByPlayer(cfg, row);
                if (itmEvent1 == null)
                {
                    LogErr(cfg, "EVT", row.c05, row.c16, "", "player");
                }
                else
                {
                    UpdateEvent(cfg, row, itmEvent1);
                    CheckNext(cfg, row, itmEvent1);
                }
            }
            else if (mat.q == QueryType.FightId)
            {
                var itmEvent2 = QueryMatchEventByFightId(cfg, row, mat.in_fight_id);
                if (itmEvent2 == null)
                {
                    LogErr(cfg, "EVT", row.c05, row.c16, mat.in_fight_id, "fight_id");
                }
                else
                {
                    UpdateEvent(cfg, row, itmEvent2);
                }
            }
        }

        private void CheckNext(TConfig cfg, TRow row, Item itmEvent)
        {
            var next_evt_id_w = itmEvent.getProperty("in_next_win", "");
            var next_evt_id_l = itmEvent.getProperty("in_next_lose", "");

            if (row.c18 != "")
            {
                //勝者有下一場
                var itmNextSchW = GetImportItemFromERound(cfg, row.c05, row.c18);
                if (itmNextSchW.isError() || itmNextSchW.getResult() == "")
                {
                    LogErr(cfg, "SCH", row.c05, row.c18, "", "WIN");
                }
                else
                {
                    var itmNextEvtW = QueryMatchEventByEvtId(cfg, row, next_evt_id_w);
                    if (itmNextEvtW == null)
                    {
                        LogErr(cfg, "EVT", row.c05, row.c18, next_evt_id_w, "WIN");
                    }
                    else
                    {
                        var next_row_w = MapRow(cfg, itmNextSchW);
                        UpdateEvent(cfg, next_row_w, itmNextEvtW);
                        CheckNext(cfg, next_row_w, itmNextEvtW);
                    }
                }
            }

            if (row.c21 != "")
            {
                //敗者有下一場
                var itmNextSchL = GetImportItemFromERound(cfg, row.c05, row.c21);
                if (itmNextSchL.isError() || itmNextSchL.getResult() == "")
                {
                    LogErr(cfg, "SCH", row.c05, row.c21, "", "LOSE");
                }
                else
                {
                    var itmNextEvtL = QueryMatchEventByEvtId(cfg, row, next_evt_id_l);
                    if (itmNextEvtL == null)
                    {
                        LogErr(cfg, "EVT", row.c05, row.c21, next_evt_id_l, "LOSE");
                    }
                    else
                    {
                        var next_row_l = MapRow(cfg, itmNextSchL);
                        UpdateEvent(cfg, next_row_l, itmNextEvtL);
                        CheckNext(cfg, next_row_l, itmNextEvtL);
                    }
                }
            }
        }

        private void LogErr(TConfig cfg, string type, string in_program_no, string in_tree_no, string evt_id, string note)
        {
            var message = "查無[" + type + "]: " + in_program_no + " | " + in_tree_no + " | " + evt_id + " | " + note;
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName + "_Error", message);
        }

        private Item GetImportItemFromERound(TConfig cfg, string in_program_no, string next_tree_no)
        {
            string sql = @"
                SELECT TOP 1
	                t1.*
	                , t2.id AS 'site_id'
	                , t2.in_code AS 'site_code'
                FROM 
	                IN_MEETING_TKDSCHEDULE t1 WITH(NOLOCK)
                LEFT OUTER JOIN
	                IN_MEETING_SITE t2 WITH(NOLOCK)
	                ON t2.in_meeting = '{#meeting_id}'
	                AND t2.in_code = t1.c02
                WHERE
	                t1.c05 = '{#in_program_no}'
	                AND t1.C16 = '{#next_tree_no}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_program_no}", in_program_no)
                .Replace("{#next_tree_no}", next_tree_no);

            return cfg.inn.applySQL(sql);
        }

        private void UpdateEvent(TConfig cfg, TRow row, Item itmEvt)
        {
            var evt_id = itmEvt.getProperty("id", "");

            var in_tree_no = row.c16;
            var site_id = row.Value.getProperty("site_id", "");
            var site_code = row.Value.getProperty("site_code", "");

            string sql_upd = "UPDATE IN_MEETING_PEVENT SET"
                + "  in_site_no = '" + in_tree_no + "'"
                + ", in_site_code = '" + site_code + "'"
                + ", in_site_id = '" + in_tree_no + "'"
                + ", in_tree_no = '" + in_tree_no + "'"
                + ", in_site = '" + site_id + "'"
                + ", in_import_no = '" + row.c00 + "'"
                + ", in_site_allocate = '1'"
                + " WHERE id = '" + evt_id + "'";

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql_upd);

            cfg.inn.applySQL(sql_upd);
        }

        private Item QueryMatchEventByEvtId(TConfig cfg, TRow row, string evt_id)
        {
            string sql = @"
                SELECT TOP 1
                    t2.*
                FROM 
					IN_MEETING_PROGRAM t1 WITH(NOLOCK)
				INNER JOIN
	                IN_MEETING_PEVENT t2 WITH(NOLOCK)
					ON t2.source_id = t1.id
                WHERE 
	                t2.id = '{#id}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_program_no}", row.c05)
                .Replace("{#id}", evt_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            Item item = cfg.inn.applySQL(sql);

            if (item.isError() || item.getResult() == "")
            {
                item = null;
            }

            return item;
        }

        private Item QueryMatchEventByFightId(TConfig cfg, TRow row, string in_fight_id)
        {
            string sql = @"
                SELECT TOP 1 
                    t2.*
                FROM 
					IN_MEETING_PROGRAM t1 WITH(NOLOCK)
				INNER JOIN
	                IN_MEETING_PEVENT t2 WITH(NOLOCK)
					ON t2.source_id = t1.id
                WHERE 
	                t1.in_meeting = '{#meeting_id}'
	                AND t1.in_program_no = '{#in_program_no}'
					AND t2.in_fight_id = '{#in_fight_id}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_program_no}", row.c05)
                .Replace("{#in_fight_id}", in_fight_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            Item item = cfg.inn.applySQL(sql);

            if (item.isError() || item.getResult() == "")
            {
                item = null;
            }

            return item;
        }

        private Item QueryMatchEventByPlayer(TConfig cfg, TRow row)
        {
            string sql = @"
                SELECT TOP 1 
                    t2.*
                FROM 
					IN_MEETING_PROGRAM t1 WITH(NOLOCK)
				INNER JOIN
	                IN_MEETING_PEVENT t2 WITH(NOLOCK)
					ON t2.source_id = t1.id
				INNER JOIN
					IN_MEETING_PEVENT_DETAIL t11 WITH(NOLOCK)
					ON t11.source_id = t2.id
					AND t11.in_sign_foot = 1
				INNER JOIN
					IN_MEETING_PEVENT_DETAIL t12 WITH(NOLOCK)
					ON t12.source_id = t2.id
					AND t12.in_sign_foot = 2
				INNER JOIN
					VU_MEETING_PTEAM t21 WITH(NOLOCK)
					ON t21.source_id = t1.id
					AND t21.in_sign_no = t11.in_sign_no
				INNER JOIN
					VU_MEETING_PTEAM t22 WITH(NOLOCK)
					ON t22.source_id = t1.id
					AND t22.in_sign_no = t12.in_sign_no
                WHERE 
	                t1.in_meeting = '{#meeting_id}'
	                AND t1.in_program_no = '{#in_program_no}'
					AND t21.in_team_index = '{#c10}'
					AND t22.in_team_index = '{#c13}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_program_no}", row.c05)
                .Replace("{#c10}", row.c10)
                .Replace("{#c13}", row.c13);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            Item item = cfg.inn.applySQL(sql);

            if (item.isError() || item.getResult() == "")
            {
                item = null;
            }

            return item;
        }

        private void ClearAllocateStatus(TConfig cfg)
        {
            string sql = @"
                UPDATE t1 SET 
	                t1.in_site_allocate = 0
	                , t1.in_import_no = ''
                FROM
	                IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PROGRAM t2 WITH(NOLOCK)
	                ON t2.id = t1.source_id
                WHERE 
	                t2.in_meeting = '{#meeting_id}'
	                AND t2.in_program_no IN (SELECT DISTINCT c05 FROM IN_MEETING_TKDSCHEDULE WITH(NOLOCK))
	                AND ISNULL(t1.in_tree_no, 0) > 0
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            cfg.inn.applySQL(sql);
        }

        private Dictionary<string, Item> MapPrograms(TConfig cfg, Item items)
        {
            var result = new Dictionary<string, Item>();
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var in_program_no = item.getProperty("in_program_no", "");
                if (!result.ContainsKey(in_program_no))
                {
                    result.Add(in_program_no, item);
                }
            }
            return result;
        }

        private Item GetProgramItems(TConfig cfg)
        {
            string sql = @"
                SELECT
	                *
                FROM
	                IN_MEETING_PROGRAM WITH(NOLOCK)
                WHERE
	                in_meeting = '{#meeting_id}'
	                AND in_program_no IN (SELECT DISTINCT c05 FROM IN_MEETING_TKDSCHEDULE WITH(NOLOCK))
                ORDER BY
	                in_program_no
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            return cfg.inn.applySQL(sql);
        }

        private List<TSite> GetSiteList(TConfig cfg, Item items)
        {
            var result = new List<TSite>();
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var c02 = item.getProperty("c02", "");
                var c16 = item.getProperty("c16", "");

                var site = result.Find(x => x.code == c02);
                if (site == null)
                {
                    site = new TSite
                    {
                        code = c02,
                        rows = new List<TRow>(),
                    };
                    result.Add(site);
                }

                var row = site.rows.Find(x => x.tno == c16);
                if (row == null)
                {
                    row = MapRow(cfg, item);
                    site.rows.Add(row);
                }
            }
            return result;
        }

        private Item GetImportItems(TConfig cfg)
        {
            string sql = @"
                SELECT 
	                t1.*
					, t2.id      AS 'site_id'
					, t2.in_code AS 'site_code'
                FROM 
	                IN_MEETING_TKDSCHEDULE t1 WITH(NOLOCK)
				LEFT OUTER JOIN
					IN_MEETING_SITE t2 WITH(NOLOCK)
					ON t2.in_meeting = '{#meeting_id}'
					AND t2.in_code = t1.c02
                ORDER BY 
	                c00
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        private void Import(TConfig cfg, Item itmReturn)
        {
            var value = itmReturn.getProperty("value", "");
            if (value == "") throw new Exception("無匯入資料");

            var list = MapList(cfg, value);
            if (list == null || list.Count == 0) throw new Exception("無匯入資料");

            RunImport(cfg, list);
        }
        
        private void RunImport(TConfig cfg, List<TRow> list)
        {
            cfg.inn.applySQL("DELETE FROM In_Meeting_TkdSchedule");

            for (var i = 0; i < list.Count; i++)
            {
                var row = list[i];
                Item itmNew = cfg.inn.newItem("In_Meeting_TkdSchedule", "add");
                itmNew.setProperty("in_meeting", cfg.meeting_id);
                itmNew.setProperty("c00", ClearVal(row.c00));
                itmNew.setProperty("c01", ClearVal(row.c01));
                itmNew.setProperty("c02", ClearVal(row.c02));
                itmNew.setProperty("c03", ClearVal(row.c03));
                itmNew.setProperty("c04", ClearVal(row.c04));
                itmNew.setProperty("c05", ClearVal(row.c05));
                itmNew.setProperty("c06", ClearVal(row.c06));
                itmNew.setProperty("c07", ClearVal(row.c07));
                itmNew.setProperty("c08", ClearVal(row.c08));
                itmNew.setProperty("c09", ClearVal(row.c09));
                itmNew.setProperty("c10", ClearVal(row.c10));
                itmNew.setProperty("c11", ClearVal(row.c11));
                itmNew.setProperty("c12", ClearVal(row.c12));
                itmNew.setProperty("c13", ClearVal(row.c13));
                itmNew.setProperty("c14", ClearVal(row.c14));
                itmNew.setProperty("c15", ClearVal(row.c15));
                itmNew.setProperty("c16", ClearVal(row.c16));
                itmNew.setProperty("c17", ClearVal(row.c17));
                itmNew.setProperty("c18", ClearVal(row.c18));
                itmNew.setProperty("c19", ClearVal(row.c19));
                itmNew.setProperty("c20", ClearVal(row.c20));
                itmNew.setProperty("c21", ClearVal(row.c21));
                itmNew.setProperty("c22", ClearVal(row.c22));

                itmNew.setProperty("c31", ClearVal(row.c31));
                itmNew.setProperty("c32", ClearVal(row.c32));
                itmNew.setProperty("c33", ClearVal(row.c33));
                itmNew.setProperty("c34", ClearVal(row.c34));
                itmNew.setProperty("c35", ClearVal(row.c35));
                itmNew.setProperty("c36", ClearVal(row.c36));
                itmNew.setProperty("c37", ClearVal(row.c37));

                itmNew.setProperty("in_l1", ClearVal(row.c06));
                itmNew.setProperty("in_l2", ClearVal(row.c07));
                itmNew.setProperty("in_l3", ClearVal(row.c08));

                itmNew.setProperty("in_section_name", ClearVal(row.c03));
                itmNew.setProperty("in_program_no", ClearVal(row.c05));

                itmNew = itmNew.apply();
            }
        }

        private List<TRow> MapList(TConfig cfg, string value)
        {
            try
            {
                var list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<TRow>>(value);
                return list;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private TRow MapRow(TConfig cfg, Item item)
        {
            var result = new TRow
            {
                c00 = item.getProperty("c00", ""),
                c01 = item.getProperty("c01", ""),
                c02 = item.getProperty("c02", ""),
                c03 = item.getProperty("c03", ""),
                c04 = item.getProperty("c04", ""),
                c05 = item.getProperty("c05", ""),
                c06 = item.getProperty("c06", ""),
                c07 = item.getProperty("c07", ""),
                c08 = item.getProperty("c08", ""),
                c09 = item.getProperty("c09", ""),
                c10 = item.getProperty("c10", ""),
                c11 = item.getProperty("c11", ""),
                c12 = item.getProperty("c12", ""),
                c13 = item.getProperty("c13", ""),
                c14 = item.getProperty("c14", ""),
                c15 = item.getProperty("c15", ""),
                c16 = item.getProperty("c16", ""),
                c17 = item.getProperty("c17", ""),
                c18 = item.getProperty("c18", ""),
                c19 = item.getProperty("c19", ""),
                c20 = item.getProperty("c20", ""),
                c21 = item.getProperty("c21", ""),
                c22 = item.getProperty("c22", ""),
                c31 = item.getProperty("c31", ""),
                c32 = item.getProperty("c32", ""),
                c33 = item.getProperty("c33", ""),
                c34 = item.getProperty("c34", ""),
                c35 = item.getProperty("c35", ""),
                c36 = item.getProperty("c36", ""),
                c37 = item.getProperty("c37", ""),
            };

            result.tno = item.getProperty("c16", "");
            result.Value = item;

            return result;
        }

        private TMatch RunMatch(TConfig cfg, TRow row)
        {
            var result = new TMatch { e = TreeType.NONE, q = QueryType.NONE, in_tree_name = "", in_tree_id = "", in_round_code = "", in_fight_id = "" };
            //ESystem 代號	備註	
            //   1: 冠亞軍
            //   3: 輸的第3
            //   35: 贏的第3、輸的第5
            //   4R: 贏的第3
            //   57: 贏的第5、輸的第7
            //   7: 輸的第7
            //   7R: 贏的第7
            //   56: 56名
            //   78: 78名
            //   99A: A組循環賽    6、7人
            //   99B: B組循環賽    6、7人
            //   99: 循環賽   5人以下

            var sub = row.c31.Replace(row.c05, "");

            switch (row.c04)
            {
                case "64":
                    result.q = QueryType.Player;
                    result.e = TreeType.MAIN;
                    result.in_tree_name = "main";
                    result.in_round_code = "32";
                    break;
                case "32":
                    result.q = QueryType.Player;
                    result.e = TreeType.MAIN;
                    result.in_tree_name = "main";
                    result.in_round_code = "32";
                    break;
                case "16":
                    result.q = QueryType.Player;
                    result.e = TreeType.MAIN;
                    result.in_tree_name = "main";
                    result.in_round_code = "16";
                    break;
                case "8":
                    result.q = QueryType.Player;
                    result.e = TreeType.MAIN;
                    result.in_tree_name = "main";
                    result.in_round_code = "8";
                    break;
                case "4":
                    result.q = QueryType.Player;
                    result.e = TreeType.MAIN;
                    result.in_tree_name = "main";
                    result.in_round_code = "4";
                    break;
                case "1":
                    result.q = QueryType.FightId;
                    result.e = TreeType.MAIN;
                    result.in_tree_name = "main";
                    result.in_round_code = "2";
                    result.in_fight_id = "M002-01";
                    break;

                case "7"://輸的第7
                    result.q = QueryType.FightId;
                    result.e = TreeType.RPC;
                    result.in_tree_name = "repechage";
                    switch (sub)
                    {
                        case "93": result.in_fight_id = "R008-01"; break;
                        case "94": result.in_fight_id = "R008-02"; break;
                        default: break;
                    }
                    break;

                case "35":
                    result.q = QueryType.FightId;
                    result.e = TreeType.RPC;
                    result.in_tree_name = "repechage";
                    switch (sub)
                    {
                        case "95": result.in_fight_id = "R004-01"; break;
                        case "96": result.in_fight_id = "R004-02"; break;
                        default: break;
                    }
                    break;

                case "57":
                    result.q = QueryType.FightId;
                    result.e = TreeType.RANK57;
                    result.in_fight_id = "RNK56-01";
                    break;

                case "3":
                    result.q = QueryType.FightId;
                    result.e = TreeType.RANK23;
                    if (sub == "11")
                    {
                        result.in_fight_id = "M004-01";
                    }
                    else if (sub == "12")
                    {
                        result.in_fight_id = "M004-02";
                    }
                    break;

                case "99":
                    result.q = QueryType.Player;
                    result.e = TreeType.ROBIN;
                    result.in_tree_name = "robin";
                    break;

                case "99A":
                    result.q = QueryType.Player;
                    result.e = TreeType.CROSS;
                    result.in_tree_name = "cross";
                    break;

                case "99B":
                    result.q = QueryType.Player;
                    result.e = TreeType.CROSS;
                    result.in_tree_name = "cross";
                    break;

                default:
                    break;
            }

            return result;
        }

        private enum QueryType
        {
            NONE = 0,
            FightId = 100,
            Player = 200,
        }

        private enum TreeType
        {
            NONE = 0,
            MAIN = 100,
            RPC = 200,
            ROBIN = 300,
            CROSS = 400,
            RANK23 = 3500,
            RANK35 = 3500,
            RANK57 = 5700,
            RANK78 = 7800,
        }

        private class TMatch
        {
            public string in_tree_name { get; set; }
            public string in_tree_id { get; set; }
            public string in_fight_id { get; set; }
            public string in_round_code { get; set; }
            public TreeType e { get; set; }
            public QueryType q { get; set; }
        }


        private class TSite
        {
            public string code { get; set; }
            public List<TRow> rows { get; set; }
        }

        private class TRow
        {
            public string c00 { get; set; }
            public string c01 { get; set; }
            public string c02 { get; set; }
            public string c03 { get; set; }
            public string c04 { get; set; }
            public string c05 { get; set; }
            public string c06 { get; set; }
            public string c07 { get; set; }
            public string c08 { get; set; }
            public string c09 { get; set; }
            public string c10 { get; set; }
            public string c11 { get; set; }
            public string c12 { get; set; }
            public string c13 { get; set; }
            public string c14 { get; set; }
            public string c15 { get; set; }
            public string c16 { get; set; }
            public string c17 { get; set; }
            public string c18 { get; set; }
            public string c19 { get; set; }
            public string c20 { get; set; }
            public string c21 { get; set; }
            public string c22 { get; set; }

            public string c31 { get; set; }
            public string c32 { get; set; }
            public string c33 { get; set; }
            public string c34 { get; set; }
            public string c35 { get; set; }
            public string c36 { get; set; }
            public string c37 { get; set; }

            public Item Value { get; set; }

            public string tno { get; set; }
            public string evt_id { get; set; }
            public string in_site_no { get; set; }
            public string in_site_code { get; set; }
            public string in_site_id { get; set; }
            public string in_site { get; set; }

        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }

            public string meeting_id { get; set; }
            public string scene { get; set; }
            public string need_clear_rpc { get; set; }

            public Item itmMeeting { get; set; }
        }

        private string ClearVal(string value)
        {
            if (value == null) return "";
            return value.Replace("\t", "").Trim();
        }

        private int GetInt(string value, int def = 0)
        {
            int result = def;
            int.TryParse(value, out result);
            return result;
        }
    }
}