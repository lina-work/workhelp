﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;
using Newtonsoft.Json.Linq;

namespace WorkHelp.ArasDesk.Methods.PLMCTA.Common
{
    public class In_Meeting_DrawSheets_Fix : Item
    {
        public In_Meeting_DrawSheets_Fix(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "In_Meeting_DrawSheets_Fix";

            var itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                strIdentityId = inn.getUserAliases(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                scene = itmR.getProperty("scene", ""),
            };

            Run(cfg, itmR);

            return itmR;
        }

        private void Run(TConfig cfg, Item itmReturn)
        {
            var mn = 1;
            var mx = 128;
            for (var i = mn; i <= mx; i++) 
            {
                var teamCount = i;
                var arr = InnSport.Core.Utilities.TUtility.Cardinarity(teamCount);
                var round_count = arr[0];
                var sys_count = arr[1];

                var items = GetMeetingDrawEventsByKnockout(cfg, sys_count);
                AppendDrawSheetEvents(cfg, teamCount, items);
            }
        }

        private void AppendDrawSheetEvents(TConfig cfg, int teamCount, Item items)
        {
            var itmParent = GetDrawSheetItem(cfg, teamCount);
            var source_id = itmParent.getProperty("id", "");

            var count = items.getItemCount();
            for (var i = 0; i< count; i++)
            {
                var item = items.getItemByIndex(i);
                var in_fight_id = item.getProperty("in_fight_id", "");

                var itmNew = cfg.inn.newItem("In_Meeting_DrawSheet_Event", "merge");
                itmNew.setAttribute("where", "source_id = '" + source_id + "' AND in_fight_id = '" + in_fight_id + "'");
                itmNew.setProperty("source_id", source_id);
                itmNew.setProperty("in_fight_id", in_fight_id);
                itmNew.setProperty("in_word", TreeNoWord(i + 1));
                itmNew.apply();
            }
        }

        private string TreeNoWord(int no)
        {
            switch(no)
            {
                case 1: return "一";
                case 2: return "二";
                case 3: return "三";
                case 4: return "四";
                case 5: return "五";
                case 6: return "六";
                case 7: return "七";
                case 8: return "八";
                case 9: return "九";

                case 10: return "十";
                case 11: return "十一";
                case 12: return "十二";
                case 13: return "十三";
                case 14: return "十四";
                case 15: return "十五";
                case 16: return "十六";
                case 17: return "十七";
                case 18: return "十八";
                case 19: return "十九";

                case 20: return "二十";
                case 21: return "二一";
                case 22: return "二二";
                case 23: return "二三";
                case 24: return "二四";
                case 25: return "二五";
                case 26: return "二六";
                case 27: return "二七";
                case 28: return "二八";
                case 29: return "二九";

                case 30: return "三十";
                case 31: return "三一";
                case 32: return "三二";
                case 33: return "三三";
                case 34: return "三四";
                case 35: return "三五";
                case 36: return "三六";
                case 37: return "三七";
                case 38: return "三八";
                case 39: return "三九";

                case 40: return "四十";
                case 41: return "四一";
                case 42: return "四二";
                case 43: return "四三";
                case 44: return "四四";
                case 45: return "四五";
                case 46: return "四六";
                case 47: return "四七";
                case 48: return "四八";
                case 49: return "四九";

                case 50: return "五十";
                case 51: return "五一";
                case 52: return "五二";
                case 53: return "五三";
                case 54: return "五四";
                case 55: return "五五";
                case 56: return "五六";
                case 57: return "五七";
                case 58: return "五八";
                case 59: return "五九";

                case 60: return "六十";
                case 61: return "六一";
                case 62: return "六二";
                case 63: return "六三";
                case 64: return "六四";
                case 65: return "六五";
                case 66: return "六六";
                case 67: return "六七";
                case 68: return "六八";
                case 69: return "六九";

                case 70: return "七十";
                case 71: return "七一";
                case 72: return "七二";
                case 73: return "七三";
                case 74: return "七四";
                case 75: return "七五";
                case 76: return "七六";
                case 77: return "七七";
                case 78: return "七八";
                case 79: return "七九";

                case 80: return "八十";
                case 81: return "八一";
                case 82: return "八二";
                case 83: return "八三";
                case 84: return "八四";
                case 85: return "八五";
                case 86: return "八六";
                case 87: return "八七";
                case 88: return "八八";
                case 89: return "八九";

                case 90: return "九十";
                case 91: return "九一";
                case 92: return "九二";
                case 93: return "九三";
                case 94: return "九四";
                case 95: return "九五";
                case 96: return "九六";
                case 97: return "九七";
                case 98: return "九八";
                case 99: return "九九";

                default: return no.ToString();
            }
        }

        private Item GetDrawSheetItem(TConfig cfg, int teamCount)
        {
            var sql = @"
                SELECT
	                id
                FROM
	                IN_MEETING_DRAWSHEET WITH(NOLOCK)
                WHERE
	                in_mode = 'knockout'
	                AND in_team_count = {#teamCount}
            ";

            sql = sql.Replace("{#teamCount}", teamCount.ToString());

            return cfg.inn.applySQL(sql);
        }

        private Item GetMeetingDrawEventsByKnockout(TConfig cfg, int sys_count)
        {
            string sql = @"
                SELECT 
	                * 
                FROM 
	                IN_MEETING_DRAWEVENT WITH(NOLOCK) 
                WHERE 
	                in_team_count <= {#sys_count}
                    AND in_group IN ('main')
                ORDER BY
	                in_tree_no
            ";

            sql = sql.Replace("{#sys_count}", sys_count.ToString());

            return cfg.inn.applySQL(sql);
        }

        private int GetInt(string value)
        {
            if (value == "" || value == "0") return 0;
            int result = 0;
            if (int.TryParse(value, out result))
            {
                return result;
            }
            return 0;
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }

            public string meeting_id { get; set; }
            public string scene { get; set; }
        }

    }
}
