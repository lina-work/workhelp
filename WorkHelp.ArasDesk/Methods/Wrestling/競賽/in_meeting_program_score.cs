﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;
using Xceed.Document.NET;
using Aras.Server.Security;
using System.Web.UI;

namespace WorkHelp.ArasDesk.Methods.Wrestling.Common
{
    public class in_meeting_program_score : Item
    {
        public in_meeting_program_score(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
    目的: 登錄成績
    日期: 
        - 2023-04-23: 敗部 Round 1 輪空判斷 (lina)
        - 2023-03-06: 調整為角力版本 (lina)
*/

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]in_meeting_program_score";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, this.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,

                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                mode = itmR.getProperty("mode", ""),
            };

            if (cfg.meeting_id == "")
            {
                itmR.setProperty("error_message", "賽事 id 不得為空白");
                return itmR;
            }

            //取得登入者資訊
            string sql = "SELECT id, in_name, in_sno FROM IN_RESUME WITH(NOLOCK) WHERE in_user_id = '" + cfg.strUserId + "'";
            cfg.itmLogin = inn.applySQL(sql);
            if (cfg.itmLogin.isError() || cfg.itmLogin.getItemCount() != 1)
            {
                throw new Exception("登入者資料異常");
            }

            switch (cfg.mode)
            {
                case "upgrade": //自動勝出
                    AutoUpgrade(cfg, itmR);
                    break;

                case "rollcall": //檢錄
                    RollCall(cfg, itmR);
                    break;

                case "score": //登錄成績
                    EnterScore(cfg, itmR);
                    break;

                case "sync": //現場同步
                    EnterSync(cfg, itmR);
                    break;

                case "fight": //籤腳非輪空
                    Fight(cfg, itmR);
                    break;

                case "bypass": //籤腳輪空
                    ByPass(cfg, itmR);
                    break;

                case "clear": //籤腳清空
                    ClearFoot(cfg, itmR);
                    break;

                case "dq_upgrade": //勝部自動勝出
                    AutoUpgradeMainDQ(cfg, itmR);
                    break;

                case "rpc_dq_upgrade": //敗部自動勝出
                    AutoUpgradeRpcDQ(cfg, itmR);
                    break;

                default:
                    break;
            }

            return itmR;
        }

        /// <summary>
        /// 籤腳清空
        /// </summary>
        private void ClearFoot(TConfig cfg, Item itmReturn)
        {
            string sql = "";
            Item itmSQL = null;

            string meeting_id = itmReturn.getProperty("meeting_id", "");
            string program_id = itmReturn.getProperty("program_id", "");
            string tree_id = itmReturn.getProperty("tree_id", "");
            string in_sign_foot = itmReturn.getProperty("in_sign_foot", "");
            string target_sign_foot = in_sign_foot == "1" ? "2" : "1";

            sql = "SELECT * FROM IN_MEETING_PEVENT WITH(NOLOCK) WHERE source_id = '" + program_id + "' AND in_tree_id = '" + tree_id + "'";
            Item itmEvent = cfg.inn.applySQL(sql);
            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "event count: " + itmEvent.getItemCount().ToString());

            if (itmEvent.isError() || itmEvent.getItemCount() != 1)
            {
                throw new Exception("場次資料錯誤");
            }

            string event_id = itmEvent.getProperty("id", "");

            sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET in_sign_no = NULL WHERE source_id = '" + event_id + "' AND in_sign_foot = '" + in_sign_foot + "'";
            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);
            itmSQL = cfg.inn.applySQL(sql);

            sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET in_target_no = NULL WHERE source_id = '" + event_id + "' AND in_sign_foot = '" + target_sign_foot + "'";
            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);
            itmSQL = cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// 籤腳輪空
        /// </summary>
        private void ByPass(TConfig cfg, Item itmReturn)
        {
            string sql = "";
            Item itmSQL = null;

            string meeting_id = itmReturn.getProperty("meeting_id", "");
            string program_id = itmReturn.getProperty("program_id", "");
            string tree_id = itmReturn.getProperty("tree_id", "");
            string in_sign_foot = itmReturn.getProperty("in_sign_foot", "");

            sql = "SELECT * FROM IN_MEETING_PEVENT WITH(NOLOCK) WHERE source_id = '" + program_id + "' AND in_tree_id = '" + tree_id + "'";
            Item itmEvent = cfg.inn.applySQL(sql);
            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "event count: " + itmEvent.getItemCount().ToString());

            if (itmEvent.isError() || itmEvent.getItemCount() != 1)
            {
                throw new Exception("場次資料錯誤");
            }

            string event_id = itmEvent.getProperty("id", "");
            string in_next_win = itmEvent.getProperty("in_next_win", "");
            string in_next_foot_win = itmEvent.getProperty("in_next_foot_win", "");
            string in_next_target_foot = in_next_foot_win == "1" ? "2" : "1";

            sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET in_sign_bypass = '1', in_status = '' WHERE source_id = '" + event_id + "' AND in_sign_foot = '" + in_sign_foot + "'";
            itmSQL = cfg.inn.applySQL(sql);


            sql = "SELECT * FROM IN_MEETING_PEVENT_DETAIL WITH(NOLOCK) WHERE source_id = '" + event_id + "' AND in_sign_foot = '1'";
            Item itmDetail1 = cfg.inn.applySQL(sql);

            if (itmDetail1.isError() || itmDetail1.getItemCount() != 1)
            {
                throw new Exception("場次對戰資料錯誤(1)");
            }


            sql = "SELECT * FROM IN_MEETING_PEVENT_DETAIL WITH(NOLOCK) WHERE source_id = '" + event_id + "' AND in_sign_foot = '2'";
            Item itmDetail2 = cfg.inn.applySQL(sql);

            if (itmDetail2.isError() || itmDetail2.getItemCount() != 1)
            {
                throw new Exception("場次對戰資料錯誤(2)");
            }

            string sign_bypass1 = itmDetail1.getProperty("in_sign_bypass", "");
            string sign_bypass2 = itmDetail2.getProperty("in_sign_bypass", "");

            string in_bypass_foot = "";
            string in_bypass_status = "1"; //整數，0 = 不排, 1 = 無輪空, 2 = 單腳輪空(上場次), 8 兩腳皆輪空(上場次)

            if (sign_bypass1 == "1" && sign_bypass2 == "1")
            {
                in_bypass_foot = "";
                in_bypass_status = "0";
            }
            else if (sign_bypass1 == "1")
            {
                in_bypass_foot = "1";
                in_bypass_status = "0";
            }
            else if (sign_bypass2 == "1")
            {
                in_bypass_foot = "2";
                in_bypass_status = "0";
            }
            else
            {
                in_bypass_status = "1";
            }

            sql = "UPDATE IN_MEETING_PEVENT SET"
                + " in_bypass_foot = '" + in_bypass_foot + "'"
                + " , in_bypass_status = '" + in_bypass_status + "'"
                + " WHERE id = '" + event_id + "'";
            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);
            itmSQL = cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// 非輪空
        /// </summary>
        private void Fight(TConfig cfg, Item itmReturn)
        {
            string sql = "";
            Item itmSQL = null;

            string meeting_id = itmReturn.getProperty("meeting_id", "");
            string program_id = itmReturn.getProperty("program_id", "");
            string tree_id = itmReturn.getProperty("tree_id", "");
            string in_sign_foot = itmReturn.getProperty("in_sign_foot", "");

            sql = "SELECT * FROM IN_MEETING_PEVENT WITH(NOLOCK) WHERE source_id = '" + program_id + "' AND in_tree_id = '" + tree_id + "'";
            Item itmEvent = cfg.inn.applySQL(sql);
            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "event count: " + itmEvent.getItemCount().ToString());

            if (itmEvent.isError() || itmEvent.getItemCount() != 1)
            {
                throw new Exception("場次資料錯誤");
            }

            string event_id = itmEvent.getProperty("id", "");
            string in_next_win = itmEvent.getProperty("in_next_win", "");
            string in_next_foot_win = itmEvent.getProperty("in_next_foot_win", "");
            string in_next_target_foot = in_next_foot_win == "1" ? "2" : "1";

            sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET in_sign_bypass = '0', in_status = NULL WHERE source_id = '" + event_id + "' AND in_sign_foot = '" + in_sign_foot + "'";
            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);
            itmSQL = cfg.inn.applySQL(sql);

            sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET in_sign_no = '' WHERE source_id = '" + in_next_win + "' AND in_sign_foot = '" + in_next_foot_win + "'";
            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);
            itmSQL = cfg.inn.applySQL(sql);

            sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET in_target_no = '' WHERE source_id = '" + in_next_win + "' AND in_sign_foot = '" + in_next_target_foot + "'";
            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);
            itmSQL = cfg.inn.applySQL(sql);

            sql = "UPDATE IN_MEETING_PEVENT SET"
                + " in_bypass_foot = NULL"
                + " , in_bypass_status = '1'"
                + " , in_win_status = NULL"
                + " , in_win_sign_no = NULL"
                + " , in_win_time = NULL"
                + " , in_win_creator = NULL"
                + " , in_win_creator_sno = NULL"
                + " WHERE id = '" + event_id + "'";
            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);
            itmSQL = cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// 登錄成績(積分模式)
        /// </summary>
        private void EnterScore(TConfig cfg, Item itmReturn)
        {
            TScore entity = new TScore
            {
                meeting_id = itmReturn.getProperty("meeting_id", ""),
                program_id = itmReturn.getProperty("program_id", ""),
                event_id = itmReturn.getProperty("event_id", ""),
                detail_id = itmReturn.getProperty("detail_id", ""),
                points_type = itmReturn.getProperty("points_type", ""),
                points_text = itmReturn.getProperty("points_text", ""),
                team_name = itmReturn.getProperty("team_name", ""),
                target_detail_id = itmReturn.getProperty("target_detail_id", ""),
                equipment_sno = itmReturn.getProperty("equipment_sno", ""),
                status_enum = StatusEnum.None,
                in_note = "",
            };

            entity.Value = GetDetail(cfg, entity.detail_id);
            entity.TargetValue = GetDetail(cfg, entity.target_detail_id);

            //設定場次資訊
            SetEventInfo(entity);
            //設定積分資訊
            SetPointsInfo(entity);

            // //更新場次備註
            // cfg.inn.applySQL("UPDATE IN_MEETING_PEVENT SET in_note = '" + entity.in_note + "' WHERE id = '" + entity.event_id + "'");

            EnterScore(cfg, entity, itmReturn);
        }

        /// <summary>
        /// 登錄成績(現場同步模式)
        /// </summary>
        private void EnterSync(TConfig cfg, Item itmReturn)
        {
            TScore entity = new TScore
            {
                meeting_id = itmReturn.getProperty("meeting_id", ""),
                program_id = itmReturn.getProperty("program_id", ""),
                event_id = itmReturn.getProperty("event_id", ""),
                detail_id = itmReturn.getProperty("detail_id", ""),
                target_detail_id = itmReturn.getProperty("target_detail_id", ""),
            };

            List<string> err = new List<string>();
            if (entity.meeting_id == "") err.Add("賽事 id 不得為空白");
            if (entity.program_id == "") err.Add("組別 id 不得為空白");
            if (entity.event_id == "") err.Add("場次 id 不得為空白");
            if (entity.detail_id == "") err.Add("對戰 id 不得為空白");
            if (entity.target_detail_id == "") err.Add("對戰 id 不得為空白");

            entity.Value = GetDetail(cfg, entity.detail_id);
            entity.TargetValue = GetDetail(cfg, entity.target_detail_id);

            //設定場次資訊
            SetEventInfo(entity);

            //登錄成績之後
            AfterScore(cfg, entity);
        }

        /// <summary>
        /// 設定場次資訊
        /// </summary>
        private void SetEventInfo(TScore entity)
        {
            Item source = entity.Value;
            entity.parent_program = source.getProperty("parent_program", "");
            entity.parent_battle_type = source.getProperty("parent_battle_type", "");
            entity.parent_group_rank = source.getProperty("parent_group_rank", "");
            entity.parent_tiebreakere = source.getProperty("parent_tiebreakere", "");

            entity.pg_battle_type = source.getProperty("program_battle_type", "");
            entity.pg_rank_type = source.getProperty("program_rank_type", "");
            entity.pg_team_count = source.getProperty("program_team_count", "0");
            entity.pg_fight_time = source.getProperty("program_fight_time", "0");

            entity.team_count = GetIntVal(entity.pg_team_count);
            entity.fight_time = entity.pg_fight_time;

            entity.BattleMap = GetBattleMap(entity.pg_battle_type, entity.team_count);

            entity.in_tree_name = source.getProperty("in_tree_name", "");
            entity.in_tree_id = source.getProperty("in_tree_id", "");
            entity.in_round_code = source.getProperty("in_round_code", "0");
            entity.round_code = GetIntVal(entity.in_round_code);

            entity.in_fight_id = source.getProperty("in_fight_id", "");
            entity.in_sub_sect = source.getProperty("in_sub_sect", "");
            entity.in_sign_no = source.getProperty("in_sign_no", "");
            entity.old_points = GetIntVal(source.getProperty("in_points", "0"));

            switch (entity.in_tree_name)
            {
                case "main":
                    entity.is_main = true;
                    break;

                case "repechage":
                    entity.is_rpc = true;
                    break;

                case "robin":
                    entity.is_robin = true;
                    entity.needSummary = true;
                    break;

                case "cross":
                    entity.is_cross = true;
                    entity.needSummary = true;
                    break;
            }

            if (entity.is_main)
            {
                entity.is_main_eight = entity.round_code == 8; //八強賽
                entity.is_main_semi = entity.round_code == 4; //準決賽
                entity.is_main_final = entity.round_code == 2; //決賽
            }

            if (entity.is_rpc)
            {
                entity.is_rpc_final = entity.round_code == 2; //四柱復活賽-決賽
            }
        }

        /// <summary>
        /// 設定積分資訊
        /// </summary>
        private void SetPointsInfo(TScore entity)
        {
            switch (entity.points_type)
            {
                case "HWIN": //勝出
                    entity.status_enum = StatusEnum.Win;
                    entity.in_points_type = entity.points_type;
                    entity.status = "1";

                    if (entity.old_points == 0)
                    {
                        entity.in_points_text = "直接勝出";
                        entity.in_points = "0";
                        entity.in_note = "1";
                    }
                    else if (entity.old_points == 1)
                    {
                        //半勝勝出
                        entity.in_points_text = "一個半勝勝出";
                        entity.in_points = "1";
                        entity.in_note = "2";
                    }
                    else
                    {
                        entity.in_points_text = "其他勝出";
                        entity.in_points = entity.old_points.ToString();
                        entity.in_note = "9";
                    }
                    break;

                case "10": //全勝
                    entity.status_enum = StatusEnum.Win;
                    entity.in_points_type = entity.points_type;
                    entity.status = "1";

                    if (entity.old_points == 0)
                    {
                        entity.in_points_text = "全勝";
                        entity.in_points = "10";
                        entity.in_note = "11";
                    }
                    else if (entity.old_points == 1)
                    {
                        entity.in_points_text = "半勝+全勝";
                        entity.in_points = "11";
                        entity.in_note = "12";
                    }
                    else
                    {
                        entity.in_points_text = "其他勝出";
                        entity.in_points = entity.old_points.ToString();
                        entity.in_note = "19";
                    }
                    break;

                case "Initial": //歸零
                    entity.status_enum = StatusEnum.Initial;
                    entity.status = "";
                    entity.in_points = "0";
                    entity.in_points_text = entity.points_text;
                    entity.in_points_type = entity.points_type;
                    entity.in_note = "";
                    break;

                case "DQ": //失格
                    entity.in_points = entity.old_points.ToString();
                    entity.status_enum = StatusEnum.DQ;
                    entity.status = "0";
                    entity.in_points_text = entity.points_text;
                    entity.in_points_type = entity.points_type;
                    entity.in_note = "91";

                    string target_points_type = entity.TargetValue.getProperty("in_points_type", "");
                    if (target_points_type == "DQ")
                    {
                        //雙方DQ
                        entity.in_note = "92";
                    }
                    break;
            }
        }

        /// <summary>
        /// 登錄成績(積分模式)
        /// </summary>
        private void EnterScore(TConfig cfg, TScore entity, Item itmReturn)
        {
            string sql = "";

            if (entity.status_enum == StatusEnum.DQ)
            {
                //更新隊伍資料
                sql = "UPDATE IN_MEETING_PTEAM SET"
                    + " in_check_result = '0'"
                    + " , in_check_status = N'失格'"
                    + " WHERE source_id = '" + entity.program_id + "'"
                    + " AND in_sign_no = '" + entity.in_sign_no + "'";
                cfg.inn.applySQL(sql);

                //我方失格
                sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET"
                    + " in_sign_action = N'失格'"
                    + " , in_points = N'" + entity.in_points + "'"
                    + " , in_points_type = N'" + entity.in_points_type + "'"
                    + " , in_points_text = N'" + entity.in_points_text + "'"
                    + " WHERE id = '" + entity.detail_id + "'";
                cfg.inn.applySQL(sql);

                //對手不戰而勝
                sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET"
                    + " in_sign_action = N''"
                    + " WHERE id = '" + entity.target_detail_id + "'";
                cfg.inn.applySQL(sql);

                //登錄成績後
                AfterScore(cfg, entity);

                itmReturn.setProperty("is_go_next", "1");
            }
            else if (entity.status_enum == StatusEnum.Win)
            {
                //我方勝出
                sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET"
                    + " in_sign_action = N'勝出'"
                    + " , in_points = N'" + entity.in_points + "'"
                    + " , in_points_type = N'" + entity.in_points_type + "'"
                    + " , in_points_text = N'" + entity.in_points_text + "'"
                    + " WHERE id = '" + entity.detail_id + "'";
                cfg.inn.applySQL(sql);

                //對手戰敗
                sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET"
                    + " in_sign_action = N''"
                    + " WHERE id = '" + entity.target_detail_id + "'";
                cfg.inn.applySQL(sql);

                //登錄成績後
                AfterScore(cfg, entity);

                itmReturn.setProperty("is_go_next", "1");
            }
            else if (entity.points_type == "CHK")
            {
                //0129檢錄
                string chkInfo = "";
                chkInfo += "<in_equipment_sno>" + entity.equipment_sno + "</in_equipment_sno>";
                chkInfo += "<meeting_id>" + entity.meeting_id + "</meeting_id>";
                chkInfo += "<data_mode>entrance</data_mode>";
                chkInfo += "<rollcall>1</rollcall>";
                cfg.inn.applyMethod("In_Meeting_GetCheckIdByEqNo", chkInfo);

                AfterScore(cfg, entity);
            }
            else
            {
                //只更新我方狀態
                sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET"
                    + " in_sign_action = N''"
                    + " , in_points = N'" + entity.in_points + "'"
                    + " , in_points_type = N'" + entity.in_points_type + "'"
                    + " , in_points_text = N'" + entity.in_points_text + "'"
                    + " WHERE id = '" + entity.detail_id + "'";
                cfg.inn.applySQL(sql);

                //我方歸零，並且我方為勝出狀態
                if (entity.status_enum == StatusEnum.Initial && entity.Value.getProperty("in_status", "") == "1")
                {
                    AfterScore(cfg, entity);
                }
            }
        }

        /// <summary>
        /// 登錄成績(積分模式)
        /// </summary>
        private void AfterScore(TConfig cfg, TScore entity)
        {
            //刷新成績(遞迴)
            ScoreRecursive(cfg, entity.event_id);

            //二柱復活
            LinkForRepechagePlayer(cfg, entity);

            //敗部改為動態設定
            LinkForRepechageWinner(cfg, entity);

            //總結成績
            //SummaryResults(cfg, entity);
        }

        //敗部改為動態設定
        private void LinkForRepechageWinner(TConfig cfg, TScore entity)
        {
            var bmap = entity.BattleMap;
            if (bmap.type != BattleEnum.WrestlingTopTwo) return;

            var items = GetActionItemsByFightId(cfg, entity.program_id, entity.pg_team_count, entity.in_fight_id);
            if (items.isError() || items.getResult() == "") return;

            var count = items.getItemCount();
            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "Link Event: " + entity.in_fight_id + ", count: " + count);

            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var evt_id = item.getProperty("evt_id", "");

                var next_w_evt_id = item.getProperty("next_w_evt_id", "");
                var next_w_evt_ft = item.getProperty("in_target_sign_foot", "");

                var next_l_evt_id = item.getProperty("next_l_evt_id", "");
                var next_l_evt_ft = item.getProperty("in_lose_sign_foot", "");

                var itmMap = GetEventSignNos(cfg, evt_id);
                var w_sign_no = itmMap.getProperty("w_sign_no", "");
                var l_sign_no = itmMap.getProperty("l_sign_no", "");

                UpdEventDetail(cfg, next_w_evt_id, next_w_evt_ft, w_sign_no);
                UpdEventDetail(cfg, next_l_evt_id, next_l_evt_ft, l_sign_no);
            }
        }

        private void UpdEventDetail(TConfig cfg, string eid, string foot, string sign_no)
        {
            if (eid == "" || foot == "") return;

            var tgt_foot = foot == "1" ? "2" : "1";

            var sql1 = "UPDATE IN_MEETING_PEVENT_DETAIL SET"
                + "  in_sign_no = '" + sign_no + "'"
                + ", in_sign_status = ''"
                + ", in_player_name = ''"
                + " WHERE source_id = '" + eid + "'"
                + " AND in_sign_foot = '" + foot + "'"
                ;

            cfg.inn.applySQL(sql1);

            var sql2 = "UPDATE IN_MEETING_PEVENT_DETAIL SET"
                + "  in_target_no = '" + sign_no + "'"
                + ", in_target_status = ''"
                + " WHERE source_id = '" + eid + "'"
                + " AND in_sign_foot = '" + tgt_foot + "'"
                ;

            cfg.inn.applySQL(sql2);
        }

        private Item GetEventSignNos(TConfig cfg, string evt_id)
        {
            var result = cfg.inn.newItem();

            var sql = @"
                SELECT 
	                in_sign_foot
	                , in_sign_no
	                , in_status 
                FROM 
	                IN_MEETING_PEVENT_DETAIL WITH(NOLOCK)
                WHERE 
	                source_id = '{#evt_id}'
                ORDER BY 
	                in_sign_foot
            ";

            sql = sql.Replace("{#evt_id}", evt_id);

            var items = cfg.inn.applySQL(sql);

            var count = items.getItemCount();

            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var in_sign_no = item.getProperty("in_sign_no", "");
                var in_status = item.getProperty("in_status", "");
                if (in_status == "1")
                {
                    result.setProperty("w_sign_no", in_sign_no);
                }
                else if (in_status == "0")
                {
                    result.setProperty("l_sign_no", in_sign_no);
                }
            }

            return result;
        }

        //二柱復活
        private void LinkForRepechagePlayer(TConfig cfg, TScore entity)
        {
            var bmap = entity.BattleMap;
            if (bmap.type != BattleEnum.WrestlingTopTwo) return;

            var items = GetActionItemsByTeamCnt(cfg, entity.program_id, entity.pg_team_count, entity.in_fight_id);
            if (items.isError() || items.getResult() == "") return;

            var count = items.getItemCount();
            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "Link Event: " + entity.in_fight_id + ", count: " + count);

            var map = new Dictionary<string, List<string>>();

            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var in_target_fight_id = item.getProperty("in_target_fight_id", "");
                var in_target_sign_foot = item.getProperty("in_target_sign_foot", "");
                var evt_win_no = item.getProperty("evt_win_no", "");
                var act_round = item.getProperty("act_round", "");

                //下一場
                var next_evt_id = item.getProperty("next_evt_id", "");
                var next_cancel = item.getProperty("next_cancel", "");

                //對手
                var itmFighter = GetCompetitorItem(cfg, entity.program_id, evt_win_no, act_round);
                if (itmFighter.isError() || itmFighter.getResult() == "")
                {
                    itmFighter = cfg.inn.newItem();
                }

                if (next_cancel == "1")
                {
                    return;
                }

                if (!map.ContainsKey(in_target_fight_id))
                {
                    map.Add(in_target_fight_id, new List<string> { in_target_sign_foot });
                }
                else
                {
                    map[in_target_fight_id].Add(in_target_sign_foot);
                }

                var row = new TRpcDetail
                {
                    team_id = itmFighter.getProperty("team_id", ""),
                    sign_no = itmFighter.getProperty("in_sign_no", ""),
                    sign_status = itmFighter.getProperty("in_sign_status", ""),
                    weight_message = itmFighter.getProperty("in_weight_message", ""),
                };

                if (row.sign_status != "cancel")
                {
                    row.sign_status = "";
                }

                row.sign_bypass = row.team_id == "" ? "1" : "0";
                row.next_sign_foot = in_target_sign_foot;
                row.next_target_foot = row.next_sign_foot == "1" ? "2" : "1";
                row.next_event_id = next_evt_id;

                if (row.weight_message == "")
                {
                    UpdateJudoTopFourDetailOK(cfg, row);
                }
                else
                {
                    UpdateJudoTopFourDetailDQ(cfg, row);
                }
            }

            //檢查敗部是否輪空
            CheckRepechageBypassEvent(cfg, entity.program_id, map);
        }

        //檢查敗部是否輪空
        private void CheckRepechageBypassEvent(TConfig cfg, string program_id, Dictionary<string, List<string>> map)
        {
            foreach (var kv in map)
            {
                var key = kv.Key;
                var list = kv.Value;
                if (list.Count != 2)
                {
                    continue;
                }

                var itmEDetail = GetRepechageBypassEventDetailItem(cfg, program_id, key);
                if (itmEDetail.isError() || itmEDetail.getResult() == "")
                {
                    continue;
                }

                var foot1_sign_no = itmEDetail.getProperty("foot1_sign_no", "0");
                var foot2_sign_no = itmEDetail.getProperty("foot2_sign_no", "0");
                if (foot1_sign_no == "") foot1_sign_no = "0";
                if (foot2_sign_no == "") foot2_sign_no = "0";

                var w_sign_no = "";
                if (foot1_sign_no != "0" && foot2_sign_no == "0")
                {
                    //籤腳1 勝出
                    w_sign_no = foot1_sign_no;
                }
                else if (foot1_sign_no == "0" && foot2_sign_no != "0")
                {
                    //籤腳2 勝出
                    w_sign_no = foot2_sign_no;
                }
                if (w_sign_no == "")
                {
                    continue;
                }

                var itmNext = GetRepechageBypassLinkEventItem(cfg, program_id, key);
                if (itmNext.isError() || itmNext.getResult() == "")
                {
                    continue;
                }

                var next_eid = itmNext.getProperty("id", "");
                var my_foot = itmNext.getProperty("in_target_sign_foot", "");
                var op_foot = my_foot == "1" ? "2" : "1";

                LinkDetail(cfg, new TDetailEdit
                {
                    sign_no = w_sign_no,
                    event_id = next_eid,
                    sign_foot = my_foot,
                    target_foot = op_foot,
                });
            }
        }

        private Item GetRepechageBypassLinkEventItem(TConfig cfg, string program_id, string in_fight_id)
        {
            var sql = @"
                SELECT 
	                t1.in_target_fight_id
	                , t1.in_target_sign_foot
	                , t2.id 
                FROM 
	                IN_MEETING_DRAWACTION t1 WITH(NOLOCK) 
                INNER JOIN
	                IN_MEETING_PEVENT t2 WITH(NOLOCK)
	                ON t2.source_id = '{#program_id}'
	                AND t2.in_fight_id = t1.in_target_fight_id
                WHERE 
	                t1.in_target_type = 'event' 
	                AND t1.in_fight_id = '{#in_fight_id}'
            ";

            sql = sql.Replace("{#program_id}", program_id)
                .Replace("{#in_fight_id}", in_fight_id);

            return cfg.inn.applySQL(sql);

        }

        private Item GetRepechageBypassEventDetailItem(TConfig cfg, string program_id, string in_fight_id)
        {
            var sql = @"
                SELECT
	                t1.id
	                , t1.in_tree_id
	                , t1.in_tree_no
	                , t1.in_fight_id
	                , t1.in_win_status
	                , t1.in_win_time
	                , t1.in_next_win
	                , t1.in_next_foot_win
	                , t11.id         AS 'foot1_did'
	                , t11.in_sign_no AS 'foot1_sign_no'
	                , t12.id		 AS 'foot2_did'
	                , t12.in_sign_no AS 'foot2_sign_no'
                FROM
	                IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t11 WITH(NOLOCK)
	                ON t11.source_id = t1.id
	                AND t11.in_sign_foot = 1
                INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t12 WITH(NOLOCK)
	                ON t12.source_id = t1.id
	                AND t12.in_sign_foot = 2
                WHERE
	                t1.source_id = '{#program_id}'
	                AND t1.in_fight_id = '{#in_fight_id}'
            ";

            sql = sql.Replace("{#program_id}", program_id)
                .Replace("{#in_fight_id}", in_fight_id);

            return cfg.inn.applySQL(sql);
        }

        private Item GetActionItemsByTeamCnt(TConfig cfg, string program_id, string in_team_count, string in_fight_id)
        {
            string sql = @"
                SELECT
	                t1.in_player_type
	                , t1.in_target_type
	                , t1.in_target_fight_id
	                , t1.in_target_sign_foot
	                , t1.in_round        AS 'act_round'
	                , t11.id             AS 'evt_id'
	                , t11.in_win_sign_no AS 'evt_win_no'
	                , t12.id             AS 'next_evt_id'
	                , t12.in_cancel      AS 'next_cancel'
                FROM 
	                In_Meeting_DrawAction t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PEVENT t11 WITH(NOLOCK)
	                ON t11.source_id = '{#program_id}'
	                AND t11.in_fight_id = t1.in_fight_id
                INNER JOIN
	                IN_MEETING_PEVENT t12 WITH(NOLOCK)
	                ON t12.source_id = '{#program_id}'
	                AND t12.in_fight_id = t1.in_target_fight_id
                WHERE
	                ISNULL(t1.in_team_count, 0) = {#in_team_count}
	                AND t1.in_fight_id = '{#in_fight_id}'
                ORDER BY
	                t1.in_round
            ";

            sql = sql.Replace("{#program_id}", program_id)
                .Replace("{#in_team_count}", MapActionTeamCount(in_team_count).ToString())
                .Replace("{#in_fight_id}", in_fight_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }


        private Item GetActionItemsByFightId(TConfig cfg, string program_id, string in_team_count, string in_fight_id)
        {
            string sql = @"
                SELECT
	                t1.in_fight_id
	                , t1.in_player_type
	                , t1.in_target_type
	                , t1.in_target_fight_id
	                , t1.in_target_sign_foot
	                , t1.in_lose_fight_id
	                , t1.in_lose_sign_foot
	                , t1.in_round        AS 'act_round'
	                , t11.id             AS 'evt_id'
	                , t11.in_win_sign_no AS 'evt_win_sign_no'
	                , t21.id             AS 'next_w_evt_id'
	                , t21.in_cancel      AS 'next_w_evt_cancel'
	                , t22.id             AS 'next_l_evt_id'
	                , t21.in_cancel      AS 'next_l_evt_cancel'
                FROM 
	                In_Meeting_DrawAction t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PEVENT t11 WITH(NOLOCK)
	                ON t11.source_id = '{#program_id}'
	                AND t11.in_fight_id = t1.in_fight_id
                LEFT OUTER JOIN
	                IN_MEETING_PEVENT t21 WITH(NOLOCK)
	                ON t21.source_id = '{#program_id}'
	                AND t21.in_fight_id = t1.in_target_fight_id
                LEFT OUTER JOIN
	                IN_MEETING_PEVENT t22 WITH(NOLOCK)
	                ON t22.source_id = '{#program_id}'
	                AND t22.in_fight_id = t1.in_lose_fight_id
                WHERE
	               t1.in_target_type = 'event'
				   AND t1.in_fight_id = '{#in_fight_id}'
            ";

            sql = sql.Replace("{#program_id}", program_id)
                .Replace("{#in_fight_id}", in_fight_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        private int MapActionTeamCount(string in_team_count)
        {
            var cnt = GetIntVal(in_team_count);
            if (cnt <= 8) return 8;
            else if (cnt <= 9) return 9;
            else if (cnt <= 16) return 16;
            else if (cnt <= 17) return 17;
            else if (cnt <= 32) return 32;
            else if (cnt <= 33) return 33;
            else return 64;
        }

        /// <summary>
        /// 取得準決賽之前的對手清單
        /// </summary>
        private Item GetCompetitorItem(TConfig cfg, string program_id, string in_sign_no, string in_round)
        {
            string sql = @"
                SELECT
                    t1.id                   AS 'event_id'
                    , t1.in_round
                    , t1.in_round_code
                    , t1.in_round_id
                    , t1.in_win_status
                    , t1.in_type
                    , t2.id                 AS 'detail_id'
                    , t2.in_sign_foot
                    , t2.in_sign_no
                    , t2.in_sign_status
                    , t3.id                 AS 'team_id'
                    , t3.in_current_org
                    , t3.in_short_org
                    , t3.in_name
                    , t3.in_sno
                    , t3.in_section_no
                    , t3.in_weight_message
                FROM
                    IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
                    IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK)
                    ON t2.source_id = t1.id
                LEFT OUTER JOIN
                    IN_MEETING_PTEAM t3 WITH(NOLOCK)
                    ON t3.source_id = t1.source_id
                    AND t3.in_sign_no = t2.in_sign_no
                WHERE
                    t1.source_id = '{#program_id}'
                    AND t1.in_tree_name = 'main'
                    AND t1.in_round = '{#in_round}'
                    AND t2.in_target_no = '{#in_target_no}'
                ORDER BY
                    t1.in_round
            ";

            sql = sql.Replace("{#program_id}", program_id)
                .Replace("{#in_round}", in_round)
                .Replace("{#in_target_no}", in_sign_no);

            return cfg.inn.applySQL(sql);
        }

        #region 循環賽


        #region 循環賽結算成績


        /// <summary>
        /// 總結成績
        /// </summary>
        private void SummaryResults(TConfig cfg, TScore entity)
        {
            if (!entity.needSummary) return;

            var evts = GetRobinEvents(cfg, entity);
            var teams = GetRobinTeams(cfg, entity);
            var robin = MapRobin(cfg, entity, evts, teams);

            var isFinished = robin.event_count == robin.finished_count;
            if (!isFinished) return;


            var mx = 3;
            switch (teams.Count)
            {
                case 1: mx = 0; break;
                case 2: mx = 1; break;
                case 3: mx = 1; break;
                case 4: mx = 2; break;
                case 5: mx = 3; break;
                case 6: mx = 2; break;
                case 7: mx = 3; break;
            }

            //清除所有分組名次
            ClearRobinRanks(cfg, entity.program_id, entity.in_sub_sect, entity.is_robin);

            //已盡數完賽，只取前三
            for (int i = 0; i < robin.players.Count; i++)
            {
                var current = robin.players[i];
                current.rank = i + 1;
                current.in_robin_rank = entity.in_sub_sect + current.rank;

                if (current.rank > mx) break;

                if (entity.is_robin)
                {
                    SetRobinRank(cfg
                        , entity.program_id
                        , teams.Count.ToString()
                        , current.in_sign_no
                        , current.in_robin_rank);
                }
                else
                {
                    SetCrossRobinRank(cfg
                        , entity.program_id
                        , teams.Count.ToString()
                        , current.in_sign_no
                        , current.in_robin_rank);
                }
            }

        }

        //清除所有分組名次
        private void ClearRobinRanks(TConfig cfg, string program_id, string in_sub_sect, bool isRobin)
        {
            var sql = "UPDATE [IN_MEETING_PTEAM] SET"
                + " in_robin_rank = NULL"
                + " WHERE source_id = '" + program_id + "'"
                + " AND in_robin_rank LIKE '" + in_sub_sect + "%'";

            if (isRobin)
            {
                sql = "UPDATE [IN_MEETING_PTEAM] SET"
                    + "  in_robin_rank = NULL"
                    + ", in_final_rank = NULL"
                    + ", in_show_rank = NULL"
                    + " WHERE source_id = '" + program_id + "'";
            }

            cfg.inn.applySQL(sql);
        }

        private void SetRobinRank(TConfig cfg, string program_id, string in_team_count, string in_sign_no, string in_robin_rank)
        {
            var sql = "";

            //選手打上分組名次
            sql = "UPDATE [IN_MEETING_PTEAM] SET"
                + "  in_robin_rank = '" + in_robin_rank + "'"
                + ", in_final_rank = '" + in_robin_rank + "'"
                + ", in_show_rank = '" + in_robin_rank + "'"
                + " WHERE source_id = '" + program_id + "'"
                + " AND in_sign_no = '" + in_sign_no + "'";

            cfg.inn.applySQL(sql);
        }

        private void SetCrossRobinRank(TConfig cfg, string program_id, string in_team_count, string in_sign_no, string in_robin_rank)
        {
            var sql = "";

            //選手打上分組名次
            sql = "UPDATE [IN_MEETING_PTEAM] SET"
                + " in_robin_rank = '" + in_robin_rank + "'"
                + " WHERE source_id = '" + program_id + "'"
                + " AND in_sign_no = '" + in_sign_no + "'";

            cfg.inn.applySQL(sql);

            sql = @"
                SELECT TOP 1
                	in_fight_id
                	, in_f1_link
                	, in_f2_link 
                FROM 
                	IN_MEETING_DRAWEVENT WITH(NOLOCK)
                WHERE 
                	in_tree_name = 'cross' 
                	AND in_team_count = {#in_team_count}
                	AND (in_f1_link = '{#in_robin_rank}' OR in_f2_link = '{#in_robin_rank}')
            ";

            sql = sql.Replace("{#in_team_count}", in_team_count)
                .Replace("{#in_robin_rank}", in_robin_rank);

            var itmData = cfg.inn.applySQL(sql);
            if (itmData.isError() || itmData.getResult() == "")
            {
                return;
            }

            var in_fight_id = itmData.getProperty("in_fight_id", "");
            var in_f1_link = itmData.getProperty("in_f1_link", "");
            var foot1 = in_f1_link == in_robin_rank ? "1" : "2";
            var foot2 = foot1 == "1" ? "2" : "1";

            var itmEvt = cfg.inn.applySQL("SELECT id FROM IN_MEETING_PEVENT WITH(NOLOCK) WHERE source_id = '" + program_id + "' AND in_fight_id = '" + in_fight_id + "'");
            var event_id = itmEvt.getProperty("id", "");

            //將分組籤號選手設定到場次腳位
            var sql1 = "UPDATE IN_MEETING_PEVENT_DETAIL SET in_sign_no = '" + in_sign_no + "' WHERE source_id = '" + event_id + "' AND in_sign_foot = " + foot1;
            var sql2 = "UPDATE IN_MEETING_PEVENT_DETAIL SET in_target_no = '" + in_sign_no + "' WHERE source_id = '" + event_id + "' AND in_sign_foot = " + foot2;

            cfg.inn.applySQL(sql1);
            cfg.inn.applySQL(sql2);
        }

        private TRobin MapRobin(TConfig cfg, TScore entity, List<TEvent> evts, List<TPlayer> teams)
        {
            var finished_count = 0;
            var players = new List<TRobinPlayer>();

            for (int i = 0; i < evts.Count; i++)
            {
                var evt = evts[i];
                if (evt.f1 == null) evt.f1 = new TDetail { value = cfg.inn.newItem() };
                if (evt.f2 == null) evt.f2 = new TDetail { value = cfg.inn.newItem() };

                var itmEvent = evt.value;
                var itmFoot1 = evt.f1.value;
                var itmFoot2 = evt.f2.value;
                var f1_sign_no = itmFoot1.getProperty("in_sign_no", "");
                var f2_sign_no = itmFoot2.getProperty("in_sign_no", "");

                AppendRobinPlayer(players, f1_sign_no, teams);
                AppendRobinPlayer(players, f2_sign_no, teams);

                string in_win_sign_no = itmEvent.getProperty("in_win_sign_no", "");
                if (in_win_sign_no == "" || in_win_sign_no == "0")
                {
                    continue;
                }

                finished_count++;


                var winner = players.Find(x => x.in_sign_no == in_win_sign_no);
                if (winner == null)
                {
                    cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "循環賽資料錯誤");
                    continue;
                }

                var itmTarget = default(Item);
                if (in_win_sign_no == f1_sign_no)
                {
                    itmTarget = itmFoot1;
                }
                else if (in_win_sign_no == f2_sign_no)
                {
                    itmTarget = itmFoot2;
                }
                else
                {
                    cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "三戰兩勝 勝出者籤號異常" + entity.program_id);
                    continue;
                }

                string in_win_local_time = itmTarget.getProperty("in_win_local_time", "").Trim();
                string in_weight_value = itmTarget.getProperty("in_weight_value", "0");
                int in_points = GetIntVal(itmTarget.getProperty("in_points", "0"));
                int in_score5 = GetIntVal(itmTarget.getProperty("in_score5", "0"));
                int in_score4 = GetIntVal(itmTarget.getProperty("in_score4", "0"));
                int in_score2 = GetIntVal(itmTarget.getProperty("in_score2", "0"));
                int in_score1 = GetIntVal(itmTarget.getProperty("in_score1", "0"));
                int in_score = GetIntVal(itmTarget.getProperty("in_score", "0"));

                winner.in_weight_value = GetDcmVal(in_weight_value);
                winner.win_count++;
                winner.win_points += in_points;
                if (in_points == 5) winner.win_score5_count++; //FALL壓倒勝
                if (in_points == 4) winner.win_score4_count++; //大差分優勝
                if (in_points == 2) winner.win_score2_count++;
                if (in_points == 1) winner.win_score1_count++;
                winner.win_score += in_score;
            }

            var orderList = players
                .OrderByDescending(x => x.win_count) //勝場數
                .ThenByDescending(x => x.win_points) //積分
                .ThenByDescending(x => x.win_score5_count) //FALL壓倒勝
                .ThenByDescending(x => x.win_score4_count) //大差分優勝
                .ThenByDescending(x => x.win_score2_count) //
                .ThenByDescending(x => x.win_score1_count) //
                .ThenByDescending(x => x.win_score) //
                .ThenBy(x => x.win_correct) //
                .ThenBy(x => x.win_record) //
                .ThenBy(x => x.lottery) //
                .ToList();

            TRobin result = new TRobin
            {
                event_count = evts.Count,
                finished_count = finished_count,
                players = orderList,
            };

            return result;
        }

        private void AppendRobinPlayer(List<TRobinPlayer> players, string in_sign_no, List<TPlayer> teams)
        {
            if (in_sign_no == "") return;
            if (in_sign_no == "0") return;

            var player = players.Find(x => x.in_sign_no == in_sign_no);

            if (player == null)
            {
                player = new TRobinPlayer
                {
                    in_sign_no = in_sign_no,
                    win_count = 0,
                    win_points = 0,
                    win_score5_count = 0,
                    win_score4_count = 0,
                    win_score2_count = 0,
                    win_score1_count = 0,
                    win_score = 0,
                    win_correct = 0,
                    win_record = 0,
                    lottery = 0,
                };

                player.row = teams.Find(x => x.in_sign_no == in_sign_no);
                if (player.row != null)
                {
                    player.lottery = GetIntVal(player.row.value.getProperty("in_lottery_99", "0"));
                }
                players.Add(player);
            }
        }

        private int SumTime(TScore entity, string value)
        {
            if (entity.fight_time == "" || value == "") return 0;

            var timeArr = entity.fight_time.Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
            int m = GetIntVal(timeArr[0]);
            int s = GetIntVal(timeArr[1]);
            int fight_seconds = m * 60 + s; //比賽總秒數
            int sands = 0;

            if (value.Contains("GS"))
            {
                string tm = value.Trim().Replace("GS", "").Trim();//GS3:10
                string[] arr = tm.Split(':');
                if (arr != null && arr.Length == 2)
                {
                    sands = GetIntVal(arr[0]) * 60 + GetIntVal(arr[1]);
                }
            }
            else
            {
                string[] arr = value.Trim().Split(':');
                if (arr != null && arr.Length == 2)
                {
                    sands = GetIntVal(arr[0]) * 60 + GetIntVal(arr[1]);
                    sands *= -1;
                }
            }

            int total_seconds = fight_seconds + sands;

            if (total_seconds < 0) total_seconds = 0;

            return total_seconds;
        }

        private class TPlayer
        {
            public string in_sign_no { get; set; }
            public Item value { get; set; }
        }

        private List<TPlayer> GetRobinTeams(TConfig cfg, TScore entity)
        {
            var rows = new List<TPlayer>();

            var sql = @"
                SELECT 
	                t1.id
	                , t1.in_name
	                , t1.in_current_org
	                , t1.in_index
	                , t1.in_sign_no
	                , t1.in_lottery_99
                FROM 
	                IN_MEETING_PTEAM t1 WITH(NOLOCK) 
                WHERE 
	                t1.source_id = '{#program_id}'
            ";

            sql = sql.Replace("{#program_id}", entity.program_id);

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_sign_no = item.getProperty("in_sign_no", "");
                var row = new TPlayer
                {
                    in_sign_no = in_sign_no,
                    value = item
                };
                rows.Add(row);
            }
            return rows;
        }

        private List<TEvent> GetRobinEvents(TConfig cfg, TScore entity)
        {
            string sql = @"
                SELECT 
	                t1.in_tree_id
	                , t1.in_fight_id
	                , t1.in_tree_no
	                , t2.in_sign_foot
	                , t2.in_sign_no
	                , t1.in_win_sign_no
	                , t1.in_win_local_time
	                , t2.in_points
	                , t2.in_score
	                , t2.in_score1
	                , t2.in_score2
	                , t2.in_score4
	                , t2.in_score5
	                , t2.in_score_c1
	                , t2.in_score_c2
					, t3.in_weight_value
                FROM 
	                IN_MEETING_PEVENT t1 WITH(NOLOCK) 
                INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
				LEFT OUTER JOIN
					IN_MEETING_PTEAM t3
					ON t3.source_id = t1.source_id
					AND t3.in_sign_no = t2.in_sign_no
                WHERE 
	                t1.source_id = '{#program_id}' 
					AND ISNULL(t1.in_sub_sect, '') = '{#in_sub_sect}'
                ORDER BY
	                t1.in_tree_no
	                , t2.in_sign_foot
            ";

            sql = sql.Replace("{#program_id}", entity.program_id)
                .Replace("{#in_sub_sect}", entity.in_sub_sect);

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            var evts = new List<TEvent>();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_fight_id = item.getProperty("in_fight_id", "");
                string in_tree_id = item.getProperty("in_tree_id", "");
                string in_sign_foot = item.getProperty("in_sign_foot", "");

                var evt = evts.Find(x => x.in_fight_id == in_fight_id);
                if (evt == null)
                {
                    evt = new TEvent
                    {
                        in_fight_id = in_fight_id,
                        in_tree_id = in_tree_id,
                        value = item,
                    };
                    evts.Add(evt);
                }

                if (in_sign_foot == "1")
                {
                    evt.f1 = new TDetail { value = item };
                }
                else if (in_sign_foot == "2")
                {
                    evt.f2 = new TDetail { value = item };
                }
            }

            return evts;
        }

        private class TRobin
        {
            public int event_count { get; set; }
            public int finished_count { get; set; }
            public List<TRobinPlayer> players { get; set; }
        }

        private class TRobinPlayer
        {
            public string in_sign_no { get; set; }
            public decimal in_weight_value { get; set; }
            public int rank { get; set; }
            public string in_robin_rank { get; set; }

            /// <summary>
            /// 勝場數
            /// </summary>
            public int win_count { get; set; }
            /// <summary>
            /// 積分數
            /// </summary>
            public int win_points { get; set; }
            /// <summary>
            /// 壓制 Fall 勝場數
            /// </summary>
            public int win_score5_count { get; set; }
            /// <summary>
            /// 積分有 4:0、4:1
            /// </summary>
            public int win_score4_count { get; set; }
            /// <summary>
            /// 積分有 2 分
            /// </summary>
            public int win_score2_count { get; set; }
            /// <summary>
            /// 積分有 2 分
            /// </summary>
            public int win_score1_count { get; set; }
            /// <summary>
            /// 技術得分
            /// </summary>
            public int win_score { get; set; }
            /// <summary>
            /// 技術失分
            /// </summary>
            public int win_correct { get; set; }
            /// <summary>
            /// 對戰成績
            /// </summary>
            public int win_record { get; set; }
            /// <summary>
            /// 99號籤
            /// </summary>
            public int lottery { get; set; }

            public TPlayer row { get; set; }
        }

        #endregion 循環賽結算成績

        /// <summary>
        /// 自動生成加賽場次
        /// </summary>
        private void AutoGenerateTiebreaker(TConfig cfg, TScore entity)
        {
            Item itmPost = cfg.inn.newItem();
            itmPost.setType("In_Meeting_Program");
            itmPost.setProperty("meeting_id", entity.meeting_id);
            itmPost.setProperty("program_id", entity.parent_program);
            itmPost.setProperty("mode", "tiebreaker");

            Item itmResult = itmPost.apply("in_meeting_program_group");

            if (itmResult.isError())
            {
                throw new Exception("自動生成加賽場次發生錯誤");
            }
        }

        #endregion 循環賽

        /// <summary>
        /// 自動勝出
        /// </summary>
        private void AutoUpgrade(TConfig cfg, Item itmReturn)
        {
            string program_id = itmReturn.getProperty("program_id", "");

            List<string> err = new List<string>();
            if (program_id == "") err.Add("組別 id 不得為空白");

            if (err.Count > 0)
            {
                itmReturn.setProperty("error_message", string.Join(" <br> ", err));
                return;
            }

            Item itmProgram = GetProgram(cfg, program_id);
            if (itmProgram.isError())
            {
                itmReturn.setProperty("error_message", "取得賽事組別發生錯誤");
                return;
            }

            //輪空，自動晉級
            Item items = GetByPassEventsMain(cfg, program_id);

            RunAutoUpgrade(cfg, items);
        }

        /// <summary>
        /// 敗部自動勝出 (DQ)
        /// </summary>
        private void AutoUpgradeRpcDQ(TConfig cfg, Item itmReturn)
        {
            string program_id = itmReturn.getProperty("program_id", "");

            //復活賽自動晉級
            Item items1 = GetByPassEventsRpc(cfg, program_id, "1");
            RunAutoUpgrade(cfg, items1);

            Item items2 = GetByPassEventsRpc(cfg, program_id, "2");
            RunAutoUpgrade(cfg, items2);
        }

        /// <summary>
        /// 自動勝出 (DQ)
        /// </summary>
        private void AutoUpgradeMainDQ(TConfig cfg, Item itmReturn)
        {
            string program_id = itmReturn.getProperty("program_id", "");

            List<string> err = new List<string>();
            if (program_id == "") err.Add("組別 id 不得為空白");

            if (err.Count > 0)
            {
                itmReturn.setProperty("error_message", string.Join(" <br> ", err));
                return;
            }

            Item itmProgram = GetProgram(cfg, program_id);
            if (itmProgram.isError())
            {
                itmReturn.setProperty("error_message", "取得賽事組別發生錯誤");
                return;
            }

            //勝部未輪空但 DQ
            Item items = GetMainDQEvents(cfg, program_id);
            //DQ，自動晉級
            RunAutoUpgrade(cfg, items);
        }

        /// <summary>
        /// 檢錄(報到)
        /// </summary>
        private void RollCall(TConfig cfg, Item itmReturn)
        {
            string sql = "";

            string meeting_id = itmReturn.getProperty("meeting_id", "");
            string in_l1 = itmReturn.getProperty("in_l1", "");
            string in_l2 = itmReturn.getProperty("in_l2", "");
            string in_l3 = itmReturn.getProperty("in_l3", "");
            string in_sno = itmReturn.getProperty("in_sno", "");
            string in_sign_no = itmReturn.getProperty("in_sign_no", "");

            //檢錄類型 rollcall: 報到, weight: 過磅, spot: 抽磅
            string w_type = itmReturn.getProperty("w_type", "");
            //檢錄結果 1: 通過, 0: 不通過
            string w_check = itmReturn.getProperty("w_check", "");
            //過磅狀態 on: 過磅合格、dq: DQ、off: 未到、leave: 請假
            string w_status = itmReturn.getProperty("w_status", "");
            //過磅體重
            string w_value = itmReturn.getProperty("w_value", "");
            //檢錄狀態
            string c_status = w_check == "1" ? "通過" : "DQ";

            string sno_condition = "AND t1.in_sno = N'" + in_sno + "'";
            if (in_sign_no != "")
            {
                sno_condition = "AND t1.in_sign_no = N'" + in_sign_no + "'";
            }

            //參賽組別隊伍(暫不考慮團體賽)
            sql = @"
                SELECT 
                    t1.*
                FROM 
                    IN_MEETING_PTEAM t1 WITH(NOLOCK) 
                INNER JOIN
                    IN_MEETING_PROGRAM t2 WITH(NOLOCK)
                    ON t2.id = t1.source_id
                WHERE 
                    t2.in_meeting = '{#meeting_id}' 
                    AND t2.in_l1 = N'{#in_l1}'
                    AND t2.in_l2 = N'{#in_l2}'
                    AND t2.in_l3 = N'{#in_l3}'
                    {#sno_condition}
            ";

            sql = sql.Replace("{#meeting_id}", meeting_id)
                .Replace("{#in_l1}", in_l1)
                .Replace("{#in_l2}", in_l2)
                .Replace("{#in_l3}", in_l3)
                .Replace("{#sno_condition}", sno_condition);

            Item itmTeams = cfg.inn.applySQL(sql);

            int count = itmTeams.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item itmTeam = itmTeams.getItemByIndex(i);

                //alan 調整
                string program_id = itmTeam.getProperty("source_id", "");
                string team_id = itmTeam.getProperty("id", "");
                string in_weight_result = itmTeam.getProperty("in_weight_result", "");

                string new_weight_message = "";
                string old_weight_message = itmTeam.getProperty("in_weight_message", "");

                if (w_type == "weight")
                {
                    if (w_check == "0")
                    {
                        new_weight_message = "(DQ)";
                    }
                    else if (w_check == "1")
                    {
                        new_weight_message = "";
                    }
                }

                string sql_update = "";

                if (w_type == "")
                {
                    sql_update = "UPDATE IN_MEETING_PTEAM SET in_check_result = N'" + w_check + "', in_check_status = N'" + c_status + "' WHERE id = '" + team_id + "'";
                }
                else if (w_type == "rollcall")
                {
                    if (in_weight_result == "0")
                    {
                        //過磅不通過，不更改檢核結果
                        sql_update = "UPDATE IN_MEETING_PTEAM SET"
                            + " in_rollcall_result = N'" + w_check + "'"
                            + " WHERE id = '" + team_id + "'";
                    }
                    else
                    {
                        sql_update = "UPDATE IN_MEETING_PTEAM SET"
                            + " in_rollcall_result = N'" + w_check + "'"
                            + ", in_check_result = N'" + w_check + "'"
                            + ", in_check_status = N'" + c_status + "'"
                            + " WHERE id = '" + team_id + "'";
                    }
                }
                else if (w_type == "weight")
                {
                    if (w_value == "")
                    {
                        //無輸入體重
                        sql_update = "UPDATE IN_MEETING_PTEAM SET"
                            + " in_weight_result = N'" + w_check + "'"
                            + ", in_check_result = N'" + w_check + "'"
                            + ", in_check_status = N'" + c_status + "'"
                            + " WHERE id = '" + team_id + "'";
                    }
                    else
                    {
                        sql_update = "UPDATE IN_MEETING_PTEAM SET"
                            + " in_weight_result = N'" + w_check + "'"
                            + ", in_weight_value = N'" + w_value + "'"
                            + ", in_check_result = N'" + w_check + "'"
                            + ", in_check_status = N'" + c_status + "'"
                            + ", in_weight_message = N'" + new_weight_message + "'"
                            + " WHERE id = '" + team_id + "'";
                    }
                }
                else if (w_type == "spot")
                {
                    if (w_check == "1") w_check = "";

                    sql_update = "UPDATE IN_MEETING_PTEAM SET"
                        + "  in_check_result = N'" + w_check + "'"
                        + ", in_check_status = N'spot'"
                        + " WHERE id = '" + team_id + "'";
                }

                if (sql_update != "")
                {
                    //CCO.Utilities.WriteDebug(strMethodName, sql_update);

                    Item itmUpdate = cfg.inn.applySQL(sql_update);

                    if (itmUpdate.isError())
                    {
                        throw new Exception("對戰表更新失敗");
                    }

                    Item itmDetail = GetEventDetail(cfg, itmTeam);

                    if (itmDetail.isError() || itmDetail.getResult() == "")
                    {
                        continue;
                    }

                    string event_id = itmDetail.getProperty("event_id", "");

                    //刷新成績(遞迴)
                    ScoreRecursive(cfg, event_id);

                    itmReturn.setProperty("program_id", program_id);
                }
            }
        }

        /// <summary>
        /// 刷新成績(遞迴)
        /// </summary>
        private void ScoreRecursive(TConfig cfg, string event_id)
        {
            if (event_id == "")
            {
                return;
            }

            Item itmDetails = GetDetails(cfg, event_id);

            TEvent evt = MapEventFromTeam(itmDetails);

            if (evt == null || evt.is_error)
            {
                throw new Exception("對戰表更新失敗");
            }

            Item itmEvent = evt.value;
            Item itmWinner = evt.winner;
            Item itmLoser = evt.loser;

            string next_event_id = itmEvent.getProperty("in_next_win", "");

            if (evt.need_commit)
            {
                if (itmWinner == null || itmLoser == null)
                {
                    return;
                }

                itmWinner.setProperty("in_next_event_id", itmEvent.getProperty("in_next_win", ""));
                itmWinner.setProperty("in_next_sign_foot", itmEvent.getProperty("in_next_foot_win", ""));

                itmLoser.setProperty("in_next_event_id", itmEvent.getProperty("in_next_lose", ""));
                itmLoser.setProperty("in_next_sign_foot", itmEvent.getProperty("in_next_foot_lose", ""));

                //更新場次勝敗
                WinEvent(cfg, evt, itmWinner, itmLoser);
                //將籤號更新至下個場次 (W)
                LinkNextEvent(cfg, itmEvent, itmWinner);
                //將籤號更新至下個場次 (L)
                LinkNextEvent(cfg, itmEvent, itmLoser);
                //更新名次
                UpdateRank(cfg, itmEvent, itmWinner, itmLoser);
                // //更新復活表
                // UpdateRepechage(cfg, itmEvent, itmWinner, itmLoser);
                //繼續往下走
                ScoreRecursive(cfg, next_event_id);
            }
            else if (evt.need_rollback)
            {
                //回滾勝敗
                RollbackEvent(cfg, event_id);
                //回滾下個場次
                RollbackNextEvent(cfg, itmEvent);
                //回滾名次
                RollbackRanks(cfg, itmEvent);
                //繼續往下走
                ScoreRecursive(cfg, next_event_id);
            }
            else if (evt.need_cancel)
            {
                //取消場次
                CancelEvent(cfg, event_id);
                //取消下個場次的對手
                CancelNextEvent(cfg, itmEvent);
                //繼續往下走
                ScoreRecursive(cfg, next_event_id);
            }
            else if (evt.need_next)
            {
                //繼續往下走
                ScoreRecursive(cfg, next_event_id);
            }
        }

        /// <summary>
        /// 更新場次勝敗
        /// </summary>
        private void WinEvent(TConfig cfg, TEvent evt, Item itmWinner, Item itmLoser)
        {
            string sql = "";
            Item itmSQL = null;

            string login_name = cfg.itmLogin.getProperty("in_name");
            string login_sno = cfg.itmLogin.getProperty("in_sno");

            string w_detail_id = itmWinner.getProperty("id", "");
            string l_detail_id = itmLoser.getProperty("id", "");

            string w_sign_no = itmWinner.getProperty("in_sign_no", "");

            string old_win_time = itmWinner.getProperty("in_win_time", "");
            string new_win_time = old_win_time == ""
                ? DateTime.Now.AddHours(-8).ToString("yyyy-MM-ddTHH:mm:ss")
                : Convert.ToDateTime(old_win_time).ToString("yyyy-MM-ddTHH:mm:ss");

            //更新場次勝敗狀態
            sql = "UPDATE IN_MEETING_PEVENT SET"
                + " in_win_status = N'" + evt.new_win_status + "'"
                + ", in_win_sign_no = '" + w_sign_no + "'"
                + ", in_win_time = '" + new_win_time + "'"
                + ", in_win_creator = N'" + login_name + "'"
                + ", in_win_creator_sno = N'" + login_sno + "'"
                + " WHERE id = '" + evt.id + "' ";
            itmSQL = cfg.inn.applySQL(sql);

            //更新為敗
            sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET in_status = '0' WHERE id = '" + l_detail_id + "' ";
            itmSQL = cfg.inn.applySQL(sql);

            //更新為勝
            sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET in_status = '1' WHERE id = '" + w_detail_id + "' ";
            itmSQL = cfg.inn.applySQL(sql);

            if (evt.in_type != "s")
            {
                //將未決的子場註取消，並註記主場決出時間
                sql = "UPDATE IN_MEETING_PEVENT SET in_win_status = 'cancel', in_win_time = '" + new_win_time + "' WHERE in_parent = '" + evt.id + "' AND in_win_time IS NULL";
                itmSQL = cfg.inn.applySQL(sql);
            }
        }

        /// <summary>
        /// 回滾勝敗
        /// </summary>
        private void RollbackEvent(TConfig cfg, string event_id)
        {
            string sql = "";
            Item itmSQL = null;

            //清空場次勝敗狀態
            sql = "UPDATE IN_MEETING_PEVENT SET in_win_status = N''"
                + ", in_win_sign_no = ''"
                + ", in_win_time = NULL"
                + ", in_win_creator = N''"
                + ", in_win_creator_sno = N''"
                + " WHERE id = '" + event_id + "' ";
            itmSQL = cfg.inn.applySQL(sql);

            //清空勝敗
            sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET in_status = '', in_sign_status = N'', in_target_status = N'' WHERE source_id = '" + event_id + "' ";
            itmSQL = cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// 取消場次
        /// </summary>
        private void CancelEvent(TConfig cfg, string event_id)
        {
            string sql = "";
            Item itmSQL = null;

            //清空場次勝敗狀態
            sql = "UPDATE IN_MEETING_PEVENT SET in_win_status = N'cancel', in_win_sign_no = '' WHERE id = '" + event_id + "' ";
            itmSQL = cfg.inn.applySQL(sql);

            //更新為敗
            sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET in_status = '0' WHERE source_id = '" + event_id + "' ";
            itmSQL = cfg.inn.applySQL(sql);
        }

        //取得子場次
        private Item GetSubEventDetails(TConfig cfg, string eid, string foot)
        {
            string sql = @"
                SELECT 
	                t1.in_tree_id
	                , t2.*
                FROM 
	                IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                WHERE 
	                t1.in_parent = '{#eid}'
	                AND t1.in_sub_id <> 7
	                AND t2.in_sign_foot = '{#foot}'
                ORDER BY 
	                t1.in_sub_id
            ";

            sql = sql.Replace("{#eid}", eid)
                .Replace("{#foot}", foot);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// 將籤號更新至下個場次
        /// </summary>
        private void LinkNextEvent(TConfig cfg, Item itmEvent, Item itmDetail)
        {
            string in_fight_id = itmEvent.getProperty("in_fight_id", "");
            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, in_fight_id);

            string in_sign_no = itmDetail.getProperty("in_sign_no", "");
            //下個場次 id
            string next_event_id = itmDetail.getProperty("in_next_event_id", "");
            //下個場次腳位
            string next_sign_foot = itmDetail.getProperty("in_next_sign_foot", "");
            //下個場次腳位 (對手那一筆)
            string next_target_foot = next_sign_foot == "1" ? "2" : "1";

            if (next_event_id == "") return;

            TDetailEdit detail = new TDetailEdit
            {
                sign_no = in_sign_no,
                event_id = next_event_id,
                sign_foot = next_sign_foot,
                target_foot = next_target_foot,
            };

            //將籤號更新至下個場次
            LinkDetail(cfg, detail);
        }

        /// <summary>
        /// 將籤號更新至下個場次
        /// </summary>
        private void LinkDetail(TConfig cfg, TDetailEdit detail)
        {
            string sql = "";

            Item itmSQL = null;

            sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET"
                + " in_sign_no = '" + detail.sign_no + "'"
                + ", in_sign_status = ''"
                + " WHERE source_id = '" + detail.event_id + "'"
                + " AND in_sign_foot = '" + detail.sign_foot + "'";

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            cfg.inn.applySQL(sql);


            sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET"
                + " in_target_no = '" + detail.sign_no + "'"
                + ", in_target_status = ''"
                + " WHERE source_id = '" + detail.event_id + "'"
                + " AND in_sign_foot = '" + detail.target_foot + "'";

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// 回滾下個場次
        /// </summary>
        private void RollbackNextEvent(TConfig cfg, Item itmEvent)
        {
            string sql = "";
            Item itmSQL = null;

            //下個場次 id
            string next_event_id = itmEvent.getProperty("in_next_win", "");

            //下個場次腳位
            string next_sign_foot = itmEvent.getProperty("in_next_foot_win", "");

            //下個場次腳位 (對手那一筆)
            string next_target_foot = next_sign_foot == "1" ? "2" : "1";

            if (next_event_id == "")
            {
                return;
            }

            sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET in_sign_no = '', in_sign_status = '', in_player_name = '' WHERE source_id = '" + next_event_id + "' AND in_sign_foot = '" + next_sign_foot + "'";
            itmSQL = cfg.inn.applySQL(sql);

            sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET in_target_no = '', in_target_status = '' WHERE source_id = '" + next_event_id + "' AND in_sign_foot = '" + next_target_foot + "'";
            itmSQL = cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// 取消下個場次的對手
        /// </summary>
        private void CancelNextEvent(TConfig cfg, Item itmEvent)
        {
            string sql = "";
            Item itmSQL = null;

            //下個場次 id
            string next_event_id = itmEvent.getProperty("in_next_win", "");

            //下個場次腳位
            string next_sign_foot = itmEvent.getProperty("in_next_foot_win", "");

            //下個場次腳位 (對手那一筆)
            string next_target_foot = next_sign_foot == "1" ? "2" : "1";

            if (next_event_id == "")
            {
                return;
            }

            sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET in_sign_no = '', in_sign_status = N'cancel', in_player_name = N'DQ' WHERE source_id = '" + next_event_id + "' AND in_sign_foot = '" + next_sign_foot + "'";
            itmSQL = cfg.inn.applySQL(sql);

            sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET in_target_no = '', in_target_status = N'cancel' WHERE source_id = '" + next_event_id + "' AND in_sign_foot = '" + next_target_foot + "'";
            itmSQL = cfg.inn.applySQL(sql);
        }

        private class TRpcDetail
        {
            public string team_id { get; set; }
            public string sign_no { get; set; }
            public string sign_status { get; set; }
            public string weight_message { get; set; }
            public string sign_bypass { get; set; }

            public string next_event_id { get; set; }
            public string next_tree_id { get; set; }
            public string next_sign_foot { get; set; }
            public string next_target_foot { get; set; }
        }

        /// <summary>
        /// 更新對戰明細
        /// </summary>
        private void UpdateJudoTopFourDetailRun(TConfig cfg, string program_id, int team_count, TLink link, Item itmFighter)
        {
            Item itmRpcEvent = GetOneEvent(cfg, program_id, link.id);
            if (itmRpcEvent.isError() || itmRpcEvent.getResult() == "")
            {
                return;
            }

            string evt_type = itmRpcEvent.getProperty("in_type", "");
            string evt_cancel = itmRpcEvent.getProperty("in_cancel", "");
            if (evt_cancel == "1")
            {
                return;
            }

            string sql = "";
            Item itmSQL = null;

            var row = new TRpcDetail
            {
                team_id = itmFighter.getProperty("team_id", ""),
                sign_no = itmFighter.getProperty("in_sign_no", ""),
                sign_status = itmFighter.getProperty("in_sign_status", ""),
                weight_message = itmFighter.getProperty("in_weight_message", ""),
            };

            if (row.sign_status != "cancel")
            {
                row.sign_status = "";
            }

            row.sign_bypass = row.team_id == "" ? "1" : "0";
            row.next_sign_foot = link.foot;
            row.next_target_foot = row.next_sign_foot == "1" ? "2" : "1";
            row.next_event_id = itmRpcEvent.getProperty("id", "");

            if (row.weight_message == "")
            {
                UpdateJudoTopFourDetailOK(cfg, row);
            }
            else
            {
                UpdateJudoTopFourDetailDQ(cfg, row);
            }
        }

        /// <summary>
        /// 更新對戰明細
        /// </summary>
        private void UpdateJudoTopFourDetailOK(TConfig cfg, TRpcDetail row)
        {
            string sql = "";

            sql = @"
                UPDATE
                    IN_MEETING_PEVENT_DETAIL 
                SET
                    in_sign_no = '{#sign_no}'
                    , in_sign_bypass = '{#sign_bypass}'
                    , in_sign_status = '{#sign_status}'
                    , in_player_org = ''
                    , in_player_name = ''
                WHERE
                    source_id = '{#event_id}' 
                    AND in_sign_foot = '{#sign_foot}'
            ";

            sql = sql.Replace("{#event_id}", row.next_event_id)
                .Replace("{#sign_foot}", row.next_sign_foot)
                .Replace("{#sign_no}", row.sign_no)
                .Replace("{#sign_bypass}", row.sign_bypass)
                .Replace("{#sign_status}", row.sign_status);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            cfg.inn.applySQL(sql);

            sql = @"
                UPDATE
                    IN_MEETING_PEVENT_DETAIL 
                SET
                    in_target_no = '{#sign_no}'
                    , in_target_bypass = '{#sign_bypass}'
                    , in_target_status = '{#sign_status}'
                WHERE
                    source_id = '{#event_id}' 
                    AND in_sign_foot = '{#sign_foot}'
            ";

            sql = sql.Replace("{#event_id}", row.next_event_id)
                .Replace("{#sign_foot}", row.next_target_foot)
                .Replace("{#sign_no}", row.sign_no)
                .Replace("{#sign_bypass}", row.sign_bypass)
                .Replace("{#sign_status}", row.sign_status);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// 更新對戰明細
        /// </summary>
        private void UpdateJudoTopFourDetailDQ(TConfig cfg, TRpcDetail row)
        {
            string sql = "";

            sql = @"
                UPDATE
                    IN_MEETING_PEVENT_DETAIL 
                SET
                    in_sign_no = '-'
                    , in_sign_bypass = '{#sign_bypass}'
                    , in_sign_status = '{#sign_status}'
                    , in_player_org = N''
                    , in_player_name = N'DQ'
                WHERE
                    source_id = '{#event_id}' 
                    AND in_sign_foot = '{#sign_foot}'
            ";

            sql = sql.Replace("{#event_id}", row.next_event_id)
                .Replace("{#sign_foot}", row.next_sign_foot)
                .Replace("{#sign_no}", row.sign_no)
                .Replace("{#sign_bypass}", row.sign_bypass)
                .Replace("{#sign_status}", row.sign_status);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            cfg.inn.applySQL(sql);

            sql = @"
                UPDATE
                    IN_MEETING_PEVENT_DETAIL 
                SET
                    in_target_no = ''
                    , in_target_bypass = '{#sign_bypass}'
                    , in_target_status = '{#sign_status}'
                WHERE
                    source_id = '{#event_id}' 
                    AND in_sign_foot = '{#sign_foot}'
            ";

            sql = sql.Replace("{#event_id}", row.next_event_id)
                .Replace("{#sign_foot}", row.next_target_foot)
                .Replace("{#sign_no}", row.sign_no)
                .Replace("{#sign_bypass}", row.sign_bypass)
                .Replace("{#sign_status}", row.sign_status);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            cfg.inn.applySQL(sql);
        }

        private string GetColumnCode(string in_tree_id, string in_sign_foot)
        {
            if (in_tree_id.EndsWith("1"))
            {
                if (in_sign_foot == "1")
                {
                    return "1";
                }
                else
                {
                    return "2";
                }
            }
            else
            {
                if (in_sign_foot == "1")
                {
                    return "3";
                }
                else
                {
                    return "4";
                }
            }
        }

        private string GetColumnCode2(string in_tree_id, string in_sign_foot)
        {
            var code = in_tree_id[in_tree_id.Length - 1].ToString();
            switch (code)
            {
                case "1": return "1";
                case "2": return "2";
                case "3": return "3";
                case "4": return "4";
                default: return "";
            }
        }
        #region GetRpcTreeId

        private TLink GetRpcTreeId8(Item itmEvent, Item itmDetail, Item itmFighter, string related_column_code, int idx)
        {
            List<TLink> links = new List<TLink>();
            switch (related_column_code)
            {
                case "1":
                    links.Add(new TLink { id = "R101", foot = "1" });
                    break;

                case "2":
                    links.Add(new TLink { id = "R101", foot = "2" });
                    break;

                case "3":
                    links.Add(new TLink { id = "R102", foot = "1" });
                    break;

                case "4":
                    links.Add(new TLink { id = "R102", foot = "2" });
                    break;

                default:
                    break;
            }

            if (links.Count > idx)
            {
                return links[idx];
            }
            else
            {
                return new TLink { id = "", foot = "" };
            }
        }

        private TLink GetRpcTreeId16(Item itmEvent, Item itmDetail, Item itmFighters, string related_column_code, int idx)
        {
            List<TLink> links = new List<TLink>();

            switch (related_column_code)
            {
                case "1":
                    links.Add(new TLink { id = "R101", foot = "1" });
                    links.Add(new TLink { id = "R101", foot = "2" });
                    break;

                case "2":
                    links.Add(new TLink { id = "R102", foot = "1" });
                    links.Add(new TLink { id = "R102", foot = "2" });
                    break;

                case "3":
                    links.Add(new TLink { id = "R103", foot = "1" });
                    links.Add(new TLink { id = "R103", foot = "2" });
                    break;

                case "4":
                    links.Add(new TLink { id = "R104", foot = "1" });
                    links.Add(new TLink { id = "R104", foot = "2" });
                    break;

                default:
                    break;
            }

            return links.Count > idx ? links[idx] : new TLink { id = "", foot = "" };
        }

        private TLink GetRpcTreeId32(Item itmEvent, Item itmDetail, Item itmFighters, string related_column_code, int idx)
        {
            List<TLink> links = new List<TLink>();

            switch (related_column_code)
            {
                case "1":
                    links.Add(new TLink { id = "R101", foot = "1" });
                    links.Add(new TLink { id = "R101", foot = "2" });
                    links.Add(new TLink { id = "R201", foot = "2" });
                    break;

                case "2":
                    links.Add(new TLink { id = "R102", foot = "1" });
                    links.Add(new TLink { id = "R102", foot = "2" });
                    links.Add(new TLink { id = "R202", foot = "2" });
                    break;

                case "3":
                    links.Add(new TLink { id = "R103", foot = "1" });
                    links.Add(new TLink { id = "R103", foot = "2" });
                    links.Add(new TLink { id = "R203", foot = "2" });
                    break;

                case "4":
                    links.Add(new TLink { id = "R104", foot = "1" });
                    links.Add(new TLink { id = "R104", foot = "2" });
                    links.Add(new TLink { id = "R204", foot = "2" });
                    break;

                default:
                    break;
            }

            return links.Count > idx ? links[idx] : new TLink { id = "", foot = "" };
        }

        private TLink GetRpcTreeId64(Item itmEvent, Item itmDetail, Item itmFighters, string related_column_code, int idx)
        {
            List<TLink> links = new List<TLink>();

            switch (related_column_code)
            {
                case "1":
                    links.Add(new TLink { id = "R101", foot = "1" });
                    links.Add(new TLink { id = "R101", foot = "2" });
                    links.Add(new TLink { id = "R201", foot = "2" });
                    links.Add(new TLink { id = "R301", foot = "2" });
                    break;

                case "2":
                    links.Add(new TLink { id = "R102", foot = "1" });
                    links.Add(new TLink { id = "R102", foot = "2" });
                    links.Add(new TLink { id = "R202", foot = "2" });
                    links.Add(new TLink { id = "R302", foot = "2" });
                    break;

                case "3":
                    links.Add(new TLink { id = "R103", foot = "1" });
                    links.Add(new TLink { id = "R103", foot = "2" });
                    links.Add(new TLink { id = "R203", foot = "2" });
                    links.Add(new TLink { id = "R303", foot = "2" });
                    break;

                case "4":
                    links.Add(new TLink { id = "R104", foot = "1" });
                    links.Add(new TLink { id = "R104", foot = "2" });
                    links.Add(new TLink { id = "R204", foot = "2" });
                    links.Add(new TLink { id = "R304", foot = "2" });
                    break;

                default:
                    break;
            }

            return links.Count > idx ? links[idx] : new TLink { id = "", foot = "" };
        }

        private TLink GetRpcTreeId128(Item itmEvent, Item itmDetail, Item itmFighters, string related_column_code, int idx)
        {
            List<TLink> links = new List<TLink>();

            switch (related_column_code)
            {
                case "1":
                    links.Add(new TLink { id = "R101", foot = "1" });
                    links.Add(new TLink { id = "R101", foot = "2" });
                    links.Add(new TLink { id = "R201", foot = "2" });
                    links.Add(new TLink { id = "R301", foot = "2" });
                    links.Add(new TLink { id = "R401", foot = "2" });
                    break;

                case "2":
                    links.Add(new TLink { id = "R102", foot = "1" });
                    links.Add(new TLink { id = "R102", foot = "2" });
                    links.Add(new TLink { id = "R202", foot = "2" });
                    links.Add(new TLink { id = "R302", foot = "2" });
                    links.Add(new TLink { id = "R402", foot = "2" });
                    break;

                case "3":
                    links.Add(new TLink { id = "R103", foot = "1" });
                    links.Add(new TLink { id = "R103", foot = "2" });
                    links.Add(new TLink { id = "R203", foot = "2" });
                    links.Add(new TLink { id = "R303", foot = "2" });
                    links.Add(new TLink { id = "R403", foot = "2" });
                    break;

                case "4":
                    links.Add(new TLink { id = "R104", foot = "1" });
                    links.Add(new TLink { id = "R104", foot = "2" });
                    links.Add(new TLink { id = "R204", foot = "2" });
                    links.Add(new TLink { id = "R304", foot = "2" });
                    links.Add(new TLink { id = "R404", foot = "2" });
                    break;

                default:
                    break;
            }

            return links.Count > idx ? links[idx] : new TLink { id = "", foot = "" };
        }

        #endregion GetRpcTreeId

        /// <summary>
        /// 轉換資料
        /// </summary>
        private TEvent MapEventFromTeam(Item items)
        {
            Dictionary<string, TEvent> match_map = new Dictionary<string, TEvent>();

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                TEvent evt = AddAndGetEvent(match_map, item);
                TDetail foot = GetDetailEntity(evt, item);

                if (foot.in_sign_foot == "1")
                {
                    evt.f1 = foot;
                }
                else
                {
                    evt.f2 = foot;
                }
            }

            foreach (var match in match_map)
            {
                TEvent evt = match.Value;

                if (evt.f1 == null || evt.f2 == null)
                {
                    evt.is_error = true;
                    continue;
                }

                BindStatus(evt);

                switch (evt.new_win_status)
                {
                    case "cancel":
                        evt.need_cancel = true;
                        break;

                    case "rollcall": //檢錄勝出
                    case "bypass":   //輪空勝出
                    case "fight":    //對戰勝出
                    case "nofight":  //不戰而勝
                    case "force":    //強制勝出
                        evt.need_commit = true;
                        break;

                    // //有勝出者，非該場次已勝出者資料 or 該場次無勝出者資料
                    // if (evt.new_win_sign_no != evt.in_win_sign_no || evt.in_win_status == "")
                    // {
                    //     evt.need_commit = true;
                    // }
                    // else
                    // {
                    //     evt.need_next = true;
                    // }
                    //break;

                    default:
                        //無勝出者，該場次卻已有勝出者資料
                        if (evt.in_win_status != "")
                        {
                            evt.need_rollback = true;
                        }
                        break;
                }
            }

            return match_map.Count > 0 ? match_map.First().Value : null;
        }

        /// <summary>
        /// 繫結狀態處理
        /// </summary>
        private void BindStatus(TEvent evt)
        {
            TDetail f1 = evt.f1;
            TDetail f2 = evt.f2;

            evt.new_win_status = "";
            evt.all_weighted = f1.has_weighted && f2.has_weighted;
            evt.all_player = f1.is_player && f2.is_player;
            evt.all_cancel = f1.is_cancel && f2.is_cancel;

            if (!evt.all_cancel)
            {
                evt.all_cancel = f1.is_not_good && f2.is_not_good;
            }

            if (!evt.all_cancel)
            {
                evt.all_cancel = f1.is_bypass && f2.is_bypass;
            }

            bool f1_win = false;
            bool f2_win = false;
            bool vu_win = false;

            if (evt.all_cancel)
            {
                //雙方都取消
                evt.new_win_status = "cancel";
            }
            else if (f1.is_bypass && f2.is_cancel)
            {
                //一方輪空，一方取消
                evt.new_win_status = "cancel";
            }
            else if (f2.is_bypass && f1.is_cancel)
            {
                //一方輪空，一方取消
                evt.new_win_status = "cancel";
            }
            else if (f1.has_weighted_passed && f2.is_bypass)
            {
                //一方過磅合格，一方輪空
                evt.new_win_status = "bypass";
                f1_win = true;
            }
            else if (f2.has_weighted_passed && f1.is_bypass)
            {
                //一方過磅合格，一方輪空
                evt.new_win_status = "bypass";
                f2_win = true;
            }
            else if (f1.has_weighted_passed && f2.is_cancel)
            {
                vu_win = true;
            }
            else if (f2.has_weighted_passed && f1.is_cancel)
            {
                vu_win = true;
            }
            else if (f1.has_signed)
            {
                //籤腳 1 已簽出勝負
                vu_win = true;
            }
            else if (f2.has_signed)
            {
                //籤腳 2 已簽出勝負
                vu_win = true;
            }
            else if (evt.all_weighted)
            {
                //雙方皆已過磅，DQ者輸(取消自動勝出判定)
                vu_win = true;
            }
            else if (f1.has_weighted && f2.is_bypass)
            {
                if (f1.has_weighted_passed)
                {
                    //籤腳 1 輪空勝出
                    evt.new_win_status = "bypass";
                    f1_win = true;
                }
                else
                {
                    //一方檢核不通過，一方輪空
                    evt.new_win_status = "cancel";
                }
            }
            else if (f2.has_weighted && f1.is_bypass)
            {
                if (f2.has_weighted_passed)
                {
                    //籤腳 2 勝出
                    evt.new_win_status = "bypass";
                    f2_win = true;
                }
                else
                {
                    //一方檢核不通過，一方輪空
                    evt.new_win_status = "cancel";
                }
            }
            else
            {
                //雙方都未檢錄，一方有籤號，一方輪空
                if (f1.assigned_no && f2.is_bypass)
                {
                    //籤腳 1 輪空勝出
                    evt.new_win_status = "bypass";
                    f1_win = true;
                }
                else if (f2.assigned_no && f1.is_bypass)
                {
                    //籤腳 2 輪空勝出
                    evt.new_win_status = "bypass";
                    f2_win = true;
                }
                else if (f1.assigned_no && f2.is_cancel)
                {
                    vu_win = true;
                }
                else if (f2.assigned_no && f1.is_cancel)
                {
                    vu_win = true;
                }
            }

            if (vu_win)
            {
                if (f1.is_win)
                {
                    evt.new_win_status = "fight";
                    f1_win = true;
                }
                else if (f1.is_lose)
                {
                    evt.new_win_status = "fight";
                    f2_win = true;
                }
                else if (f2.is_win)
                {
                    evt.new_win_status = "fight";
                    f2_win = true;
                }
                else if (f2.is_lose)
                {
                    evt.new_win_status = "fight";
                    f1_win = true;
                }
            }

            //籤腳 1 勝出
            if (f1_win)
            {
                evt.new_win_sign_no = f1.in_sign_no;
                evt.winner = f1.value;
                evt.loser = f2.value;
            }
            //籤腳 2 勝出
            else if (f2_win)
            {
                evt.new_win_sign_no = f2.in_sign_no;
                evt.winner = f2.value;
                evt.loser = f1.value;
            }
        }


        /// <summary>
        /// 取得並附加場次資料
        /// </summary>
        private TEvent AddAndGetEvent(Dictionary<string, TEvent> dictionary, Item item)
        {
            TEvent entity = null;

            string event_id = item.getProperty("event_id", "");

            if (dictionary.ContainsKey(event_id))
            {
                entity = dictionary[event_id];
            }
            else
            {
                entity = new TEvent
                {
                    id = event_id,
                    in_tree_name = item.getProperty("in_tree_name", ""),
                    in_tree_id = item.getProperty("in_tree_id", ""),
                    in_tree_no = item.getProperty("in_tree_no", ""),
                    in_round = item.getProperty("in_round", ""),
                    in_bypass_foot = item.getProperty("in_bypass_foot", ""),
                    in_win_status = item.getProperty("in_win_status", ""),
                    in_win_sign_no = item.getProperty("in_win_sign_no", ""),
                    in_type = item.getProperty("in_type", ""),
                    value = item,
                };
                dictionary.Add(event_id, entity);
            }
            return entity;
        }

        /// <summary>
        /// 取得明細資料
        /// </summary>
        private TDetail GetDetailEntity(TEvent evt, Item item)
        {
            TDetail entity = new TDetail
            {
                in_sign_foot = item.getProperty("in_sign_foot", ""),
                in_sign_no = item.getProperty("in_sign_no", ""),
                in_sign_bypass = item.getProperty("in_sign_bypass", ""),
                in_sign_status = item.getProperty("in_sign_status", ""),
                in_sign_action = item.getProperty("in_sign_action", ""),

                in_target_status = item.getProperty("in_target_status", ""),

                in_status = item.getProperty("in_status", ""),

                in_check_result = item.getProperty("in_check_result", ""),
                in_weight_message = item.getProperty("in_weight_message", ""),

                in_player_org = item.getProperty("in_player_org", ""),
                in_player_name = item.getProperty("in_player_name", ""),

                value = item,
            };

            //是否已配置籤號
            entity.assigned_no = entity.in_sign_no != "" && entity.in_sign_no != "0";

            //是否輪空
            entity.is_bypass = evt.in_bypass_foot == entity.in_sign_foot;
            if (entity.in_sign_bypass == "1")
            {
                entity.is_bypass = true;
            }
            else if (evt.in_tree_no == "" && !entity.assigned_no && entity.in_player_name == "")
            {
                //lina  角力的決賽、敗部、分組交叉決賽都沒有場次編號
                //無場次編號，無籤號，亦無配置選手姓名
                //entity.is_bypass = true;
            }
            else if (entity.in_player_name == "消失")
            {
                entity.is_bypass = true;
            }

            if (entity.in_check_result == "")
            {
                //未過磅
                entity.has_weighted = false;
            }
            else
            {
                //已過磅
                entity.has_weighted = true;
                if (entity.in_check_result == "1")
                {
                    //過磅合格
                    entity.has_weighted_passed = true;
                }
                else
                {
                    //過磅不合格
                    entity.has_weighted_passed = false;
                }
            }

            entity.is_player = !entity.is_bypass;
            entity.is_cancel = entity.in_sign_status == "cancel";

            // if (entity.in_weight_message != "")
            // {
            //     //DQ、請假、未到、停賽 視為我方取消
            //     entity.is_cancel = true;
            // }

            if (entity.in_weight_message != "")
            {
                //DQ、請假、未到、停賽 視為我方取消
                entity.is_not_good = true;
            }

            entity.has_signed = entity.in_sign_action != "";
            entity.is_win = entity.in_sign_action == "勝出";
            entity.is_lose = entity.in_sign_action == "失格";

            return entity;
        }

        /// <summary>
        /// 取得名次資訊
        /// </summary>
        private TRank GetRankInfo(string in_tree_rank, string in_tree_rank_ns, string in_tree_rank_nss)
        {
            bool need_update_rank = false;
            string[] ranks = null;
            string[] show_ranks_ = null;

            if (in_tree_rank.Contains("rank"))
            {
                if (in_tree_rank_ns == "")
                {
                    throw new Exception("決定名次的場次未設定名次資料");
                }

                need_update_rank = true;
                ranks = GetRankArray(in_tree_rank_ns, "0,0");
                show_ranks_ = GetRankArray(in_tree_rank_nss, "0,0");
            }
            else
            {
                ranks = new string[] { "", "" };
                show_ranks_ = new string[] { "", "" };
            }

            TRank result = new TRank
            {
                w_frank = ranks[0],
                l_frank = ranks[1],
                w_srank = show_ranks_[0],
                l_srank = show_ranks_[1],
                need_update = need_update_rank,
            };

            return result;
        }


        /// <summary>
        /// 取得組別資訊
        /// </summary>
        private Item GetProgram(TConfig cfg, string program_id)
        {
            string sql = @"
                SELECT 
                    t1.*
                    , t2.in_battle_repechage AS 'mt_battle_repechage'
                FROM 
                    IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
                    IN_MEETING t2 WITH(NOLOCK)
                    ON t2.id = t1.in_meeting
                WHERE 
                    t1.id = '{#program_id}'
            ";

            sql = sql.Replace("{#program_id}", program_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// 取得賽事組別場次明細
        /// </summary>
        private Item GetEventDetail(TConfig cfg, Item itmTeam)
        {
            string sql = @"
                SELECT
                    TOP 1
                    t2.id AS 'event_id'
                    , t1.*
                FROM
                    IN_MEETING_PEVENT_DETAIL t1 WITH(NOLOCK)
                INNER JOIN
                    IN_MEETING_PEVENT t2 WITH(NOLOCK)
                    ON t2.id = t1.source_id
                WHERE
                    t2.source_id = '{#program_id}'
                    AND t2.in_tree_name = N'main'
                    AND t2.in_round = 1
                    AND t1.in_sign_no = '{#in_sign_no}'
            ";

            string program_id = itmTeam.getProperty("source_id", "");
            string in_sign_no = itmTeam.getProperty("in_sign_no", "");

            sql = sql.Replace("{#program_id}", program_id)
                .Replace("{#in_sign_no}", in_sign_no);

            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// 取得場次
        /// </summary>
        private Item GetOneEvent(TConfig cfg, string program_id, string in_tree_id)
        {
            string sql = @"
                SELECT
                    TOP 1 t1.*
                FROM
                    IN_MEETING_PEVENT t1 WITH(NOLOCK)
                WHERE
                    t1.source_id = '{#program_id}'
                    AND t1.in_tree_id = N'{#in_tree_id}'
            ";

            sql = sql.Replace("{#program_id}", program_id)
                .Replace("{#in_tree_id}", in_tree_id);

            return cfg.inn.applySQL(sql);
        }

        //取得賽事組別場次明細
        private Item GetDetails(TConfig cfg, string event_id)
        {
            string sql = @"
                SELECT
                    t1.source_id AS 'program_id'
                    , t1.id      AS 'event_id'
                    , t1.in_tree_id
                    , t1.in_tree_no
                    , t1.in_tree_name
                    , t1.in_tree_rank
                    , t1.in_tree_rank_ns
                    , t1.in_tree_rank_nss
                    , t1.in_round
                    , t1.in_round_code
                    , t1.in_bypass_foot
                    , t1.in_win_time
                    , t1.in_win_status
                    , t1.in_win_sign_no
                    , t1.in_next_win
                    , t1.in_next_foot_win
                    , t1.in_next_lose
                    , t1.in_next_foot_lose
                    , t1.in_type
                    , t1.in_fight_id
                    , t2.id
                    , t2.in_sign_foot
                    , t2.in_sign_no
                    , t2.in_sign_bypass
                    , t2.in_sign_status
                    , t2.in_sign_action
                    , t2.in_points
                    , t2.in_points_type
                    , t2.in_points_text
                    , t2.in_correct_count
                    , t2.in_status
                    , t2.in_player_org
                    , t2.in_player_name
                    , t3.id AS 'team_id'
                    , t3.in_check_result
                    , t3.in_check_status
                    , t3.in_weight_message
                FROM 
                    IN_MEETING_PEVENT t1 WITH(NOLOCK) 
                INNER JOIN 
                    IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK) 
                    ON t2.source_id = t1.id 
                LEFT OUTER JOIN
                    IN_MEETING_PTEAM t3 WITH(NOLOCK)
                    ON t3.source_id = t1.source_id
                    AND t3.in_sign_no = t2.in_sign_no
                WHERE 
                    t1.id = '{#event_id}' 
                ORDER BY
                    t2.in_sign_foot
            ";

            sql = sql.Replace("{#event_id}", event_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// 取得自動晉級場次(主線第一回合)
        /// </summary>
        private Item GetByPassEventsMain(TConfig cfg, string program_id)
        {
            string sql = @"
                SELECT
                    t1.id AS 'event_id'
                    , t1.in_round_code
                    , t1.in_bypass_foot
                FROM
                    IN_MEETING_PEVENT t1 WITH(NOLOCK)
        
                WHERE
                    t1.source_id = '{#program_id}'
                    AND t1.in_tree_name = 'main'
                    AND t1.in_round = '1'
                    AND ISNULL(t1.in_bypass_foot, '') <> ''
                ORDER BY
                    t1.in_round_id
                ";

            sql = sql.Replace("{#program_id}", program_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        //勝部未輪空但 DQ
        private Item GetMainDQEvents(TConfig cfg, string program_id)
        {
            string sql = @"
                SELECT 
                    t1.id AS 'event_id'
                    , t1.in_round_code
                    , t1.in_tree_id
                FROM
                	IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
                	IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK)
                	ON t2.source_id = t1.id
                INNER JOIN
                	VU_MEETING_PTEAM t3 WITH(NOLOCK)
                	ON t3.source_id = t1.source_id
                	AND t3.in_sign_no = t2.in_sign_no
                WHERE
                	t1.source_id = '{#program_id}'
                	AND t1.in_tree_name = N'main'
                	AND ISNULL(t1.in_win_status, '') = ''
                	AND ISNULL(t3.in_check_result, '') = '0'
                	AND ISNULL(t3.in_weight_message, '') <> ''
                ORDER BY
                	t1.in_tree_id
                	, t2.in_sign_foot
            ";

            sql = sql.Replace("{#program_id}", program_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// 取得自動晉級場次(復活表第一回合)
        /// </summary>
        private Item GetByPassEventsRpc(TConfig cfg, string program_id, string in_round)
        {
            string sql = @"
                SELECT
                    t1.id AS 'event_id'
                    , t1.in_round_code
                FROM
                    IN_MEETING_PEVENT t1 WITH(NOLOCK)
                WHERE
                    t1.source_id = '{#program_id}'
                    AND t1.in_tree_name = 'repechage'
                    AND t1.in_round = '{#in_round}'
                ORDER BY
                    t1.in_round_id
                ";

            sql = sql.Replace("{#program_id}", program_id)
                .Replace("{#in_round}", in_round);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// 取得準決賽之前的對手清單
        /// </summary>
        private Item GetFightersBeforeSemiFinal(TConfig cfg, string program_id, string in_sign_no)
        {
            string sql = @"
                SELECT
                    t1.id                   AS 'event_id'
                    , t1.in_round
                    , t1.in_round_code
                    , t1.in_round_id
                    , t1.in_win_status
                    , t1.in_type
                    , t2.id                 AS 'detail_id'
                    , t2.in_sign_foot
                    , t2.in_sign_no
                    , t2.in_sign_status
                    , t3.id                 AS 'team_id'
                    , t3.in_current_org
                    , t3.in_short_org
                    , t3.in_name
                    , t3.in_sno
                    , t3.in_section_no
                    , t3.in_weight_message
                FROM
                    IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
                    IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK)
                    ON t2.source_id = t1.id
                LEFT OUTER JOIN
                    IN_MEETING_PTEAM t3 WITH(NOLOCK)
                    ON t3.source_id = t1.source_id
                    AND t3.in_sign_no = t2.in_sign_no
                WHERE
                    t1.source_id = '{#program_id}'
                    AND t1.in_tree_name = N'main'
                    AND t1.in_round_code > 4
                    AND t2.in_target_no = '{#in_target_no}'
                ORDER BY
                    t1.in_round
            ";

            sql = sql.Replace("{#program_id}", program_id)
                .Replace("{#in_target_no}", in_sign_no);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// 取得 Detail
        /// </summary>
        private Item GetDetail(TConfig cfg, string detail_id)
        {
            string sql = @"
                SELECT
                    t1.*
                    , t2.id             AS 'event_id'
                    , t2.in_tree_name
                    , t2.in_tree_id
                    , t2.in_tree_no
                    , t2.in_round_code
                    , t2.in_round_id
                    , t2.in_type
                    , t2.in_fight_id
                    , t2.in_sub_sect
                    , t3.in_meeting     AS 'meeting_id'
                    , t3.id             AS 'program_id'
                    , t3.in_battle_type AS 'program_battle_type'
                    , t3.in_rank_type   AS 'program_rank_type'
                    , t3.in_team_count  AS 'program_team_count'
                    , t3.in_fight_time  AS 'program_fight_time'
                    , t4.id             AS 'parent_program'
                    , t4.in_battle_type AS 'parent_battle_type'
                    , t4.in_group_rank  AS 'parent_group_rank'
                    , t4.in_tiebreaker  AS 'parent_tiebreakere'
                FROM
                    IN_MEETING_PEVENT_DETAIL t1 WITH(NOLOCK)
                INNER JOIN
                    IN_MEETING_PEVENT t2 WITH(NOLOCK)
                    ON t2.id = t1.source_id
                INNER JOIN
                    IN_MEETING_PROGRAM t3 WITH(NOLOCK)
                    ON t3.id = t2.source_id
                LEFT OUTER JOIN
                    IN_MEETING_PROGRAM t4 WITH(NOLOCK)
                    ON t4.id = t3.in_program
                WHERE
                    t1.id = '{#detail_id}'
            ";

            sql = sql.Replace("{#detail_id}", detail_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// 取得未進行的場次數量
        /// </summary>
        private Item GetEventCount(TConfig cfg, string program_id, bool is_parent = false)
        {
            string program_condition = is_parent ? "t2.in_program = '{#program_id}'" : "t2.id = '{#program_id}'";
            program_condition = program_condition.Replace("{#program_id}", program_id);

            string sql = @"
                SELECT
                    count(t1.id) AS 'event_count'
                FROM 
                    IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
                    IN_MEETING_PROGRAM t2 WITH(NOLOCK)
                    ON t2.id = t1.source_id
                WHERE
                    {#program_condition}
                    AND ISNULL(t1.in_win_status, '') = ''
            ";

            sql = sql.Replace("{#program_condition}", program_condition);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        //取得賽事組別場次
        private Item GetEventPlayers(TConfig cfg, string program_id)
        {
            string sql = @"
                SELECT
                    t1.id AS 'event_id'
                    , t1.in_tree_name
                    , t1.in_tree_id
                    , t1.in_tree_no
                    , t1.in_round
                    , t1.in_round_id
                    , t1.in_round_code
                    , t1.in_site_code
                    , t1.in_site_no
                    , t1.in_site_id
                    , t1.in_site_allocate
                    , t1.in_next_win
                    , t1.in_next_foot_win
                    , t1.in_next_lose
                    , t1.in_next_foot_lose
                    , t1.in_detail_ns
                    , t1.in_win_sign_no
                    , t2.id AS 'detail_id'
                    , t2.in_sign_foot
                    , t2.in_sign_no
                    , t2.in_sign_bypass
                    , t2.in_target_no
                    , t2.in_status
                    , t2.in_points
                    , t2.in_points_type
                    , t3.id AS 'team_id'
                    , t3.in_current_org
                    , t3.in_short_org
                    , t3.map_short_org
                    , t3.in_name
                    , t3.in_names
                    , t3.in_sno
                    , t3.in_section_no
                    , t3.in_org_teams
                    , t3.in_seeds
                    , t3.in_check_result
                FROM 
                    IN_MEETING_PEVENT t1 WITH(NOLOCK)
                LEFT OUTER JOIN
                    IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK)
                    ON t2.source_id = t1.id
                LEFT OUTER JOIN
                    VU_MEETING_PTEAM t3 WITH(NOLOCK)
                    ON t3.source_id = t1.source_id
                    AND ISNULL(t3.in_sign_no, '') <> ''
                    AND t3.in_sign_no = t2.in_sign_no
                WHERE
                    t1.source_id = '{#program_id}'
                ORDER BY
                    t1.in_tree_name
                    , t1.in_round
                    , t1.in_round_id
                    , t2.in_sign_foot
            ";

            sql = sql.Replace("{#program_id}", program_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        private class TStatistics
        {
            /// <summary>
            /// 籤號
            /// </summary>
            public string in_sign_no { get; set; }

            /// <summary>
            /// 總積分
            /// </summary>
            public int total_points { get; set; }

            /// <summary>
            /// 總勝場
            /// </summary>
            public int total_wins { get; set; }

            /// <summary>
            /// 排名
            /// </summary>
            public int rank { get; set; }
        }

        /// <summary>
        /// 統計隊伍積分與勝場
        /// </summary>
        private Dictionary<string, TStatistics> MapTeam(Item itmEvents)
        {
            Dictionary<string, TStatistics> map = new Dictionary<string, TStatistics>();
            int count = itmEvents.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item itmEvent = itmEvents.getItemByIndex(i);
                string in_sign_no = itmEvent.getProperty("in_sign_no", "");
                string in_status = itmEvent.getProperty("in_status", "");
                string in_points = itmEvent.getProperty("in_points", "");
                int points = GetIntVal(in_points);

                TStatistics entity = null;
                if (map.ContainsKey(in_sign_no))
                {
                    entity = map[in_sign_no];
                }
                else
                {
                    entity = new TStatistics
                    {
                        in_sign_no = in_sign_no,
                        total_points = 0,
                        total_wins = 0,
                    };
                    map.Add(in_sign_no, entity);
                }

                entity.total_points += points;

                if (in_status == "1")
                {
                    entity.total_wins++;
                }

            }
            return map;
        }

        private void RunAutoUpgrade(TConfig cfg, Item items)
        {
            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string event_id = item.getProperty("event_id", "");

                //刷新成績(遞迴)
                ScoreRecursive(cfg, event_id);
            }
        }

        #region 更新名次

        /// <summary>
        /// 更新名次
        /// </summary>
        private void UpdateRank(TConfig cfg, Item itmEvent, Item itmWinner, Item itmLoser)
        {
            string event_id = itmEvent.getProperty("event_id", "");
            string program_id = itmEvent.getProperty("program_id", "");
            string in_tree_rank = itmEvent.getProperty("in_tree_rank", "");
            string in_tree_rank_ns = itmEvent.getProperty("in_tree_rank_ns", "");
            string in_tree_rank_nss = itmEvent.getProperty("in_tree_rank_nss", "");

            TRank rank = GetRankInfo(in_tree_rank, in_tree_rank_ns, in_tree_rank_nss);
            if (!rank.need_update) return;

            string sql = @"
                SELECT 
	                t1.in_name
					, t1.in_battle_type
	                , t1.in_team_count
                    , t2.id AS 'event_id'
	                , t2.in_next_win
	                , t2.in_next_foot_win
	                , t2.in_next_lose
	                , t2.in_next_foot_lose 
	                , t2.in_fight_id 
	                , t3.in_status
	                , t3.in_sign_foot
	                , t3.in_sign_no
                FROM 
                    IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PEVENT t2 WITH(NOLOCK)
                    ON t2.source_id = t1.id
                INNER JOIN 
	                IN_MEETING_PEVENT_DETAIL t3 WITH(NOLOCK) 
	                ON t3.source_id = t2.id
                WHERE 
	                t2.id = '{#event_id}'
	                AND t2.in_win_time IS NOT NULL
                ORDER BY 
	                t2.in_tree_id
                    , t3.in_status
            ";

            sql = sql.Replace("{#program_id}", program_id)
                .Replace("{#event_id}", event_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            if (count <= 0) return;

            for (int i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var in_battle_type = item.getProperty("in_battle_type", "");
                var in_status = item.getProperty("in_status", "");
                var in_sign_no = item.getProperty("in_sign_no", "");
                if (in_sign_no == "" || in_sign_no == "0") continue;

                if (in_status == "1")
                {
                    item.setProperty("in_final_rank", rank.w_frank);
                    item.setProperty("in_show_rank", rank.w_srank);
                    item.setProperty("in_next_event_id", item.getProperty("in_next_win", ""));
                    item.setProperty("in_next_sign_foot", item.getProperty("in_next_foot_win", ""));
                }
                else
                {
                    item.setProperty("in_final_rank", rank.l_frank);
                    item.setProperty("in_show_rank", rank.l_srank);
                    item.setProperty("in_next_event_id", item.getProperty("in_next_lose", ""));
                    item.setProperty("in_next_sign_foot", item.getProperty("in_next_foot_lose", ""));
                }

                UpdateRanks(cfg, program_id, in_battle_type, item);
            }

            //Item itmFirst = items.getItemByIndex(0);
            //string in_team_count = itmFirst.getProperty("in_team_count", "0");

            //if (in_team_count == "9" && in_tree_rank == "rank78")
            //{
            //    //清空第八名 (9取7)
            //    ClearRanks(cfg, program_id, "8");
            //}

            //if (in_team_count == "7" && in_tree_rank == "rank56")
            //{
            //    //清空第六名 (7取5)
            //    ClearRanks(cfg, program_id, "6");
            //}
        }

        /// <summary>
        /// 執行更新名次
        /// </summary>
        private void UpdateRanks(TConfig cfg, string program_id, string in_battle_type, Item itmDetail)
        {
            var x = MapScore(cfg, itmDetail, program_id);

            string sql = "UPDATE IN_MEETING_PTEAM SET "
                + "  in_final_rank = '{#final_rank}'"
                + ", in_show_rank = '{#show_rank}'"
                + " WHERE source_id = '{#pid}'"
                + " AND in_sign_no = '{#sign_no}'"
                + " AND ISNULL(in_weight_message, '') = ''";

            if (x.next_evt != "")
            {
                sql = "UPDATE IN_MEETING_PTEAM SET "
                    + " in_final_rank = '{#final_rank}'"
                    + ", in_show_rank = '{#show_rank}'"
                    + ", in_event = '{#event}'"
                    + ", in_foot = '{#foot}'"
                    + " WHERE source_id = '{#pid}'"
                    + " AND in_sign_no = '{#sign_no}'"
                + " AND ISNULL(in_weight_message, '') = ''";
            }
            else if (in_battle_type == "TopTwo" && x.frank == "3")
            {
                sql = FixRank3TopTwo(cfg, x, sql);
            }
            else if (in_battle_type == "WrestlingTopTwo" && x.frank == "3")
            {
                sql = FixRank35UwwTopTwo(cfg, x, sql);
            }
            else if (in_battle_type == "WrestlingTopTwo" && x.frank == "5")
            {
                sql = FixRank35UwwTopTwo(cfg, x, sql);
            }

            sql = sql.Replace("{#pid}", x.pid)
                .Replace("{#sign_no}", x.no)
                .Replace("{#final_rank}", x.frank)
                .Replace("{#show_rank}", x.srank)
                .Replace("{#event}", x.next_evt)
                .Replace("{#foot}", x.next_foot);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "UpdateRanks _# sql: " + sql);

            cfg.inn.applySQL(sql);
        }

        private string FixRank3TopTwo(TConfig cfg, TSCR x, string defaultSql)
        {
            var b1 = x.fid == "M004-01";
            var b2 = x.fid == "M004-02";

            x.next_foot = "";
            if (b1) x.next_foot = "1";
            if (b2) x.next_foot = "2";

            if (b1 || b2)
            {
                return "UPDATE IN_MEETING_PTEAM SET "
                    + " in_final_rank = '{#final_rank}'"
                    + ", in_show_rank = '{#show_rank}'"
                    + ", in_event = NULL"
                    + ", in_foot = '{#foot}'"
                    + " WHERE source_id = '{#pid}'"
                    + " AND in_sign_no = '{#sign_no}'"
                + " AND ISNULL(in_weight_message, '') = ''";
            }
            else
            {
                return defaultSql;
            }
        }

        private string FixRank35UwwTopTwo(TConfig cfg, TSCR x, string defaultSql)
        {
            var b1 = x.fid == "R004-01";
            var b2 = x.fid == "R004-02";

            x.next_foot = "";
            if (b1) x.next_foot = "1";
            if (b2) x.next_foot = "2";

            if (b1 || b2)
            {
                return "UPDATE IN_MEETING_PTEAM SET "
                    + " in_final_rank = '{#final_rank}'"
                    + ", in_show_rank = '{#show_rank}'"
                    + ", in_event = NULL"
                    + ", in_foot = '{#foot}'"
                    + " WHERE source_id = '{#pid}'"
                    + " AND in_sign_no = '{#sign_no}'"
                + " AND ISNULL(in_weight_message, '') = ''";
            }
            else
            {
                return defaultSql;
            }
        }

        /// <summary>
        /// 回滾名次
        /// </summary>
        private void RollbackRanks(TConfig cfg, Item itmEvent)
        {
            // string in_fight_id = itmEvent.getProperty("in_fight_id", "");
            // //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "RollbackRanks: " + in_fight_id);

            // string program_id = itmEvent.getProperty("program_id", "");
            // string in_tree_rank = itmEvent.getProperty("in_tree_rank", "");
            // string in_tree_rank_ns = itmEvent.getProperty("in_tree_rank_ns", "");
            // string in_tree_rank_nss = itmEvent.getProperty("in_tree_rank_nss", "");

            // TRank rank = GetRankInfo(in_tree_rank, in_tree_rank_ns, in_tree_rank_nss);

            // if (rank.need_update)
            // {
            //     var sql = "UPDATE IN_MEETING_PTEAM SET in_final_rank = NULL, in_show_rank = NULL"
            //         + " WHERE source_id = '" + program_id + "'"
            //         + " AND in_final_rank IN(" + rank.w_frank + ", " + rank.l_frank + ")";

            //     cfg.inn.applySQL(sql);
            // }
        }

        #endregion 更新名次

        #region 資料結構

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }

            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }

            public string meeting_id { get; set; }
            public string mode { get; set; }

            public Item itmLogin { get; set; }
        }

        /// <summary>
        /// 對戰場次資料模型
        /// </summary>
        private class TEvent
        {
            /// <summary>
            /// id
            /// </summary>
            public string id { get; set; }

            /// <summary>
            /// 樹圖名稱
            /// </summary>
            public string in_tree_name { get; set; }

            /// <summary>
            /// 樹圖序號
            /// </summary>
            public string in_tree_id { get; set; }

            /// <summary>
            /// 對戰序號
            /// </summary>
            public string in_fight_id { get; set; }

            /// <summary>
            /// 樹圖序號
            /// </summary>
            public string in_tree_no { get; set; }

            /// <summary>
            /// 回合
            /// </summary>
            public string in_round { get; set; }

            /// <summary>
            /// 輪空籤腳
            /// </summary>
            public string in_bypass_foot { get; set; }

            /// <summary>
            /// 勝出狀態 (bypass: 輪空勝出、rollcall: 檢錄勝出、fight: 對戰勝出、nofight: 不戰而勝、force: 強制勝出、cancel: 取消)
            /// </summary>
            public string in_win_status { get; set; }

            /// <summary>
            /// 勝出籤號
            /// </summary>
            public string in_win_sign_no { get; set; }

            /// <summary>
            /// 場次類型(p: 父場次、s: 子場次)
            /// </summary>
            public string in_type { get; set; }

            /// <summary>
            /// 籤腳 1 資料
            /// </summary>
            public TDetail f1 { get; set; }

            /// <summary>
            /// 籤腳 2 資料
            /// </summary>
            public TDetail f2 { get; set; }

            /// <summary>
            /// 雙方都是真人
            /// </summary>
            public bool all_player { get; set; }

            /// <summary>
            /// 雙方都已過磅
            /// </summary>
            public bool all_weighted { get; set; }

            /// <summary>
            /// 雙方取消
            /// </summary>
            public bool all_cancel { get; set; }

            /// <summary>
            /// 當前勝出狀態 (bypass: 輪空勝出、rollcall: 檢錄勝出、fight: 對戰勝出、nofight: 不戰而勝、force: 強制勝出、cancel: 取消)
            /// </summary>
            public string new_win_status { get; set; }

            /// <summary>
            /// 當前勝出籤號
            /// </summary>
            public string new_win_sign_no { get; set; }

            /// <summary>
            /// 需提交成績
            /// </summary>
            public bool need_cancel { get; set; }

            /// <summary>
            /// 需返還
            /// </summary>
            public bool need_rollback { get; set; }

            /// <summary>
            /// 需提交成績
            /// </summary>
            public bool need_commit { get; set; }

            /// <summary>
            /// 需進行下一場
            /// </summary>
            public bool need_next { get; set; }

            /// <summary>
            /// 異常
            /// </summary>
            public bool is_error { get; set; }

            /// <summary>
            /// event 資料
            /// </summary>
            public Item value { get; set; }

            /// <summary>
            /// 勝者
            /// </summary>
            public Item winner { get; set; }

            /// <summary>
            /// 敗者
            /// </summary>
            public Item loser { get; set; }
        }

        /// <summary>
        /// 對戰場次明細資料模型
        /// </summary>
        private class TDetail
        {
            /// <summary>
            /// 我方籤腳
            /// </summary>
            public string in_sign_foot { get; set; }

            /// <summary>
            /// 我方籤號
            /// </summary>
            public string in_sign_no { get; set; }

            /// <summary>
            /// 我方輪空
            /// </summary>
            public string in_sign_bypass { get; set; }

            /// <summary>
            /// 我方狀態
            /// </summary>
            public string in_sign_status { get; set; }

            /// <summary>
            /// 我方行為
            /// </summary>
            public string in_sign_action { get; set; }

            /// <summary>
            /// 檢錄結果
            /// </summary>
            public string in_check_result { get; set; }

            /// <summary>
            /// 過磅訊息
            /// </summary>
            public string in_weight_message { get; set; }

            /// <summary>
            /// 對手狀態
            /// </summary>
            public string in_target_status { get; set; }

            /// <summary>
            /// 勝敗狀態
            /// </summary>
            public string in_status { get; set; }

            /// <summary>
            /// 選手單位
            /// </summary>
            public string in_player_org { get; set; }

            /// <summary>
            /// 選手姓名
            /// </summary>
            public string in_player_name { get; set; }


            /// <summary>
            /// 原始資料
            /// </summary>
            public Item value { get; set; }


            /// <summary>
            /// 是否已配置籤號
            /// </summary>
            public bool assigned_no { get; set; }

            /// <summary>
            /// 這是輪空的籤
            /// </summary>
            public bool is_bypass { get; set; }

            /// <summary>
            /// 是否已過磅
            /// </summary>
            public bool has_weighted { get; set; }

            /// <summary>
            /// 過磅合格
            /// </summary>
            public bool has_weighted_passed { get; set; }

            /// <summary>
            /// 真人選手
            /// </summary>
            public bool is_player { get; set; }

            /// <summary>
            /// 對手已取消
            /// </summary>
            public bool is_cancel { get; set; }

            /// <summary>
            /// 對手已取消
            /// </summary>
            public bool is_not_good { get; set; }

            /// <summary>
            /// 已簽出勝負
            /// </summary>
            public bool has_signed { get; set; }

            /// <summary>
            /// 我方勝出
            /// </summary>
            public bool is_win { get; set; }

            /// <summary>
            /// 我方敗出
            /// </summary>
            public bool is_lose { get; set; }
        }

        private class TDetailEdit
        {
            /// <summary>
            /// 籤號
            /// </summary>
            public string sign_no { get; set; }

            /// <summary>
            /// 當前名次
            /// </summary>
            public string rank { get; set; }

            /// <summary>
            /// 場次編號
            /// </summary>
            public string event_id { get; set; }

            /// <summary>
            /// 腳位
            /// </summary>
            public string sign_foot { get; set; }

            /// <summary>
            /// 對位
            /// </summary>
            public string target_foot { get; set; }

        }

        private class TRank
        {
            public string w_frank { get; set; }
            public string w_srank { get; set; }

            public string l_frank { get; set; }
            public string l_srank { get; set; }

            public bool need_update { get; set; }
        }

        private class TLink
        {
            public string id { get; set; }
            public string foot { get; set; }
        }

        /// <summary>
        /// 登錄成績資料模型
        /// </summary>
        private class TScore
        {
            //外部輸入欄位

            /// <summary>
            /// 賽事 id
            /// </summary>
            public string meeting_id { get; set; }

            /// <summary>
            /// 組別 id
            /// </summary>
            public string program_id { get; set; }

            /// <summary>
            /// 場次 id
            /// </summary>
            public string event_id { get; set; }

            /// <summary>
            /// 我方 id
            /// </summary>
            public string detail_id { get; set; }

            /// <summary>
            /// 積分類型(外部輸入)
            /// </summary>
            public string points_type { get; set; }

            /// <summary>
            /// 積分內文(外部輸入)
            /// </summary>
            public string points_text { get; set; }

            /// <summary>
            /// 對方 id
            /// </summary>
            public string target_detail_id { get; set; }

            /// <summary>
            /// 檢錄sno
            /// </summary>
            public string equipment_sno { get; set; }

            /// <summary>
            /// 隊伍 or 姓名
            /// </summary>
            public string team_name { get; set; }

            //存取資料

            /// <summary>
            /// 資料來源
            /// </summary>
            public Item Value { get; set; }

            /// <summary>
            /// 資料來源(對手)
            /// </summary>
            public Item TargetValue { get; set; }


            // 主組別

            /// <summary>
            /// 主組別 id
            /// </summary>
            public string parent_program { get; set; }

            /// <summary>
            /// 主組別 - 賽制類型
            /// </summary>
            public string parent_battle_type { get; set; }

            /// <summary>
            /// 主組別 - 取名次類型
            /// </summary>
            public string parent_group_rank { get; set; }

            /// <summary>
            /// 主組別 - 加賽類型
            /// </summary>
            public string parent_tiebreakere { get; set; }


            //該組別

            /// <summary>
            /// 賽制類型
            /// </summary>
            public string pg_battle_type { get; set; }

            /// <summary>
            /// 組別人數/隊數
            /// </summary>
            public string pg_team_count { get; set; }

            /// <summary>
            /// 比賽時間
            /// </summary>
            public string pg_fight_time { get; set; }

            /// <summary>
            /// 名次類型
            /// </summary>
            public string pg_rank_type { get; set; }

            /// <summary>
            /// 組別人數/隊數
            /// </summary>
            public int team_count { get; set; }

            /// <summary>
            /// 比賽時間
            /// </summary>
            public string fight_time { get; set; }

            /// <summary>
            /// 賽制資料
            /// </summary>
            public TBattleMap BattleMap { get; set; }

            /// <summary>
            /// 樹圖名稱
            /// </summary>
            public string in_tree_name { get; set; }

            /// <summary>
            /// 樹圖 id
            /// </summary>
            public string in_tree_id { get; set; }

            /// <summary>
            /// 對打 id
            /// </summary>
            public string in_fight_id { get; set; }

            /// <summary>
            /// 回合編號
            /// </summary>
            public string in_round_code { get; set; }

            /// <summary>
            /// 籤號
            /// </summary>
            public string in_sign_no { get; set; }

            /// <summary>
            /// 回合編號
            /// </summary>
            public int round_code { get; set; }

            // 更新欄位



            /// <summary>
            /// 我方積分(數值)
            /// </summary>
            public int old_points { get; set; }

            /// <summary>
            /// 我方積分
            /// </summary>
            public string in_points { get; set; }

            /// <summary>
            /// 積分類型代碼
            /// </summary>
            public string in_points_type { get; set; }

            /// <summary>
            /// 積分類型
            /// </summary>
            public string in_points_text { get; set; }

            /// <summary>
            /// 勝敗狀態(1: 勝出、0: 敗)
            /// </summary>
            public string status { get; set; }

            /// <summary>
            /// 備註
            /// </summary>
            public string in_note { get; set; }

            //判斷欄位

            /// <summary>
            /// 狀態
            /// </summary>
            public StatusEnum status_enum { get; set; }

            /// <summary>
            /// 是否為勝部
            /// </summary>
            public bool is_main { get; set; }

            /// <summary>
            /// 是否為敗部
            /// </summary>
            public bool is_rpc { get; set; }

            /// <summary>
            /// 是否為循環賽
            /// </summary>
            public bool is_robin { get; set; }

            /// <summary>
            /// 是否為分組交叉循環賽
            /// </summary>
            public bool is_cross { get; set; }

            /// <summary>
            /// 是否八強賽(四柱復活賽連結)
            /// </summary>
            public bool is_main_eight { get; set; }

            /// <summary>
            /// 是否準決賽(四柱復活賽連結)
            /// </summary>
            public bool is_main_semi { get; set; }

            /// <summary>
            /// 是否決賽(挑戰賽連結)
            /// </summary>
            public bool is_main_final { get; set; }

            /// <summary>
            /// 是否四柱復活賽決賽(挑戰賽連結)
            /// </summary>
            public bool is_rpc_final { get; set; }

            /// <summary>
            /// 分組
            /// </summary>
            public string in_sub_sect { get; set; }

            /// <summary>
            /// 是否需要總結成績
            /// </summary>
            public bool needSummary { get; set; }
        }

        /// <summary>
        /// 取得賽制類型
        /// </summary>
        private TBattleMap GetBattleMap(string value, int tcount)
        {
            switch (value)
            {
                case "SingleRoundRobin": return new TBattleMap { type = BattleEnum.SingleRoundRobin, isRobin = true };
                case "SixPlayers": return new TBattleMap { type = BattleEnum.SixPlayers, isRobin = true };
                case "SevenPlayers": return new TBattleMap { type = BattleEnum.SevenPlayers, isRobin = true };
                case "WrestlingTopTwo": return new TBattleMap { type = BattleEnum.WrestlingTopTwo };
                default: return new TBattleMap { type = BattleEnum.None };
            }
        }

        private class TBattleMap
        {
            /// <summary>
            /// 賽制類型
            /// </summary>
            public BattleEnum type { get; set; }

            /// <summary>
            /// 是否為四柱復活賽
            /// </summary>
            public bool isRepechage { get; set; }

            /// <summary>
            /// 是否為循環賽
            /// </summary>
            public bool isRobin { get; set; }

            /// <summary>
            /// 是否為挑戰賽
            /// </summary>
            public bool isChallenge { get; set; }

            /// <summary>
            /// 是否八強賽(四柱復活挑戰賽-八強進復活)
            /// </summary>
            public bool isEight { get; set; }

            /// <summary>
            ///三戰兩勝
            /// </summary>
            public bool isTwoFight { get; set; }
        }

        /// <summary>
        /// 賽制類型
        /// </summary>
        private enum BattleEnum
        {
            /// <summary>
            /// 未設定
            /// </summary>
            None = 0,
            /// <summary>
            /// 五人以下循環賽制
            /// </summary>
            SingleRoundRobin = 500,
            /// <summary>
            /// 六人分組交叉賽制
            /// </summary>
            SixPlayers = 600,
            /// <summary>
            /// 七人分組交叉賽制
            /// </summary>
            SevenPlayers = 700,
            /// <summary>
            /// 八人以上二柱復活賽
            /// </summary>
            WrestlingTopTwo = 800,
        }

        private enum StatusEnum
        {
            /// <summary>
            /// 未設定
            /// </summary>
            None = 0,
            /// <summary>
            /// 歸零
            /// </summary>
            Initial = 1,
            /// <summary>
            /// 失格
            /// </summary>
            DQ = 2,
            /// <summary>
            /// 指導滿次數而落敗
            /// </summary>
            Correct = 3,
            /// <summary>
            /// 勝出
            /// </summary>
            Win = 100,
        }

        private TSCR MapScore(TConfig cfg, Item itmDetail, string program_id)
        {
            return new TSCR
            {
                pid = program_id,
                fid = itmDetail.getProperty("in_fight_id", ""),
                no = itmDetail.getProperty("in_sign_no", ""),
                frank = itmDetail.getProperty("in_final_rank", ""),
                srank = itmDetail.getProperty("in_show_rank", ""),
                next_evt = itmDetail.getProperty("in_next_event_id", ""),
                next_foot = itmDetail.getProperty("in_next_sign_foot", ""),
            };
        }

        private class TSCR
        {
            public string pid { get; set; }
            public string fid { get; set; }
            public string no { get; set; }
            public string frank { get; set; }
            public string srank { get; set; }
            public string next_evt { get; set; }
            public string next_foot { get; set; }
        }

        #endregion 資料結構

        /// <summary>
        /// 取得名次陣列
        /// </summary>
        private string[] GetRankArray(string value, string ranks)
        {
            return string.IsNullOrEmpty(value)
                ? ranks.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                : value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
        }

        private decimal GetDcmVal(string value)
        {
            if (value == "") return 0;

            decimal result = 0;
            decimal.TryParse(value, out result);
            return result;
        }

        private int GetIntVal(string value, int def = 0)
        {
            int result = def;
            int.TryParse(value, out result);
            return result;
        } 
    }
}