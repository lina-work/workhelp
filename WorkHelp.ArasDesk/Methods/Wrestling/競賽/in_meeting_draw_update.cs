﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Wrestling.Common
{
    public class in_meeting_draw_update : Item
    {
        public in_meeting_draw_update(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
            目的: 更新籤號相關數值
            輸入: 
                - meeting_id
                - in_team_index
                - exe_type
                    - in_seeds 種子籤
                    - number 跆拳道籤號
                    - section 量級序號
                    - in_seeds 種子籤
            日期: 
                2020-07-28: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "in_meeting_draw_update";

            var itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,

                meeting_id = itmR.getProperty("meeting_id", "").Trim(),
                program_id = itmR.getProperty("program_id", "").Trim(),
                team_id = itmR.getProperty("team_id", "").Trim(),
                exe_type = itmR.getProperty("exe_type", ""),
            };


            if (cfg.program_id == "" || cfg.team_id == "")
            {
                throw new Exception("參數錯誤");
            }

            switch (cfg.exe_type)
            {
                case "seed":
                    //更新種子籤
                    UpdateSeed(cfg, itmR);
                    break;

                case "tkd_no":
                    //更新籤號
                    UpdateTkdNo(cfg, itmR);
                    break;

                case "section_no":
                    //更新量級
                    UpdateSectionNo(cfg, itmR);
                    break;

                case "judo_no":
                    //用[柔道籤號]更新[跆拳道籤號]，回傳新跆拳道籤號
                    UpdateJudoNo(cfg, itmR);
                    break;

                case "lottery_99":
                    UpdateLottery99(cfg, itmR);
                    break;

                default:
                    break;
            }

            return itmR;
        }

        private void UpdateLottery99(TConfig cfg, Item itmReturn)
        {
            var sql = "";
            var in_lottery_99 = itmReturn.getProperty("in_lottery_99", "");

            sql = @"
                UPDATE 
                    IN_MEETING_PTEAM 
                SET 
                    in_lottery_99 = '{#in_lottery_99}'
                WHERE
                    source_id = '{#program_id}'
                    AND id = '{#team_id}'
            ";

            sql = sql.Replace("{#program_id}", cfg.program_id)
                .Replace("{#team_id}", cfg.team_id)
                .Replace("{#in_judo_no}", in_lottery_99);

            cfg.inn.applySQL(sql);


            sql = @"
                UPDATE t1 SET
        	        t1.in_draw_no = t2.in_lottery_99
                FROM
        	        IN_MEETING_USER t1 WITH(NOLOCK)
                INNER JOIN
        	        IN_MEETING_PTEAM t2 WITH(NOLOCK)
        	        ON t2.in_meeting = t1.source_id
        	        AND t2.in_team_key = ISNULL(t1.in_l1, '') 
        		        + '-' + ISNULL(t1.in_l2, '') 
        		        + '-' + ISNULL(t1.in_l3, '') 
        		        + '-' + ISNULL(t1.in_index, '')
        		        + '-' + ISNULL(t1.in_creator_sno, '')
                WHERE
        	        t2.source_id = '{#program_id}'
        	        AND t2.id = '{#team_id}'
            ";

            sql = sql.Replace("{#program_id}", cfg.program_id)
                .Replace("{#team_id}", cfg.team_id);

            cfg.inn.applySQL(sql);
        }
        #region 跆拳道籤號

        //用[跆拳道籤號]更新[柔道籤號]，回傳新柔道籤號
        private void UpdateTkdNo(TConfig cfg, Item itmReturn)
        {
            //抽籤演算法
            Item itmDrawMode = cfg.inn.applySQL("SELECT in_value FROM IN_VARIABLE WITH(NOLOCK) WHERE in_name = 'draw_mode'");
            string draw_mode = itmDrawMode.getProperty("in_value", "").ToLower();
            bool is_tkd_mode = draw_mode == "tkd";

            string sql = "";

            string program_id = itmReturn.getProperty("program_id", "").Trim();
            string team_id = itmReturn.getProperty("team_id", "").Trim();
            string in_sign_no = itmReturn.getProperty("in_sign_no", "").Trim();

            sql = "SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + program_id + "'";
            Item itmProgram = cfg.inn.applySQL(sql);
            string in_battle_type = itmProgram.getProperty("in_battle_type", "");
            string in_team_count = itmProgram.getProperty("in_team_count", "");
            string in_round_code = itmProgram.getProperty("in_round_code", "");

            Item itmData = cfg.inn.newItem();
            itmData.setType("In_Meeting_PTeam");
            itmData.setProperty("in_battle_type", in_battle_type);
            itmData.setProperty("in_team_count", in_team_count);
            itmData.setProperty("in_round_code", in_round_code);
            itmData.setProperty("is_tkd_to_judo", "1");
            itmData.setProperty("value", in_sign_no);
            Item itmResult = itmData.apply("In_Judo_SignNo");

            string in_judo_no = itmResult.getProperty("map_no", "");
            string in_section_no = is_tkd_mode ? in_sign_no : in_judo_no;

            sql = @"
                UPDATE 
                    IN_MEETING_PTEAM 
                SET 
                    in_judo_no = '{#in_judo_no}'
                    , in_sign_no = '{#in_sign_no}'
                    , in_section_no = '{#in_section_no}'
                WHERE
                    source_id = '{#program_id}'
                    AND id = '{#team_id}'
            ";

            sql = sql.Replace("{#program_id}", program_id)
                .Replace("{#team_id}", team_id)
                .Replace("{#in_judo_no}", in_judo_no)
                .Replace("{#in_sign_no}", in_sign_no)
                .Replace("{#in_section_no}", in_section_no);


            Item itmSQL = cfg.inn.applySQL(sql);

            if (itmSQL.isError())
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "TKD籤號更新失敗: " + sql);
                throw new Exception("TKD籤號更新失敗");
            }

            itmReturn.setProperty("new_judo_no", in_judo_no);
        }

        #endregion 跆拳道籤號

        #region 柔道籤號

        //用[柔道籤號]更新[跆拳道籤號]，回傳新跆拳道籤號
        private void UpdateJudoNo(TConfig cfg, Item itmReturn)
        {
            string sql = "";

            string program_id = itmReturn.getProperty("program_id", "").Trim();
            string team_id = itmReturn.getProperty("team_id", "").Trim();
            string in_judo_no = itmReturn.getProperty("in_judo_no", "").Trim();

            sql = "SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + program_id + "'";
            Item itmProgram = cfg.inn.applySQL(sql);
            string in_battle_type = itmProgram.getProperty("in_battle_type", "");
            string in_team_count = itmProgram.getProperty("in_team_count", "");
            string in_round_code = itmProgram.getProperty("in_round_code", "");

            Item itmData = cfg.inn.newItem();
            itmData.setType("In_Meeting_PTeam");
            itmData.setProperty("in_battle_type", in_battle_type);
            itmData.setProperty("in_team_count", in_team_count);
            itmData.setProperty("in_round_code", in_round_code);
            itmData.setProperty("is_tkd_to_judo", "0");
            itmData.setProperty("value", in_judo_no);
            Item itmResult = itmData.apply("In_Judo_SignNo");

            string in_sign_no = itmResult.getProperty("map_no", "");

            sql = @"
                UPDATE 
                    IN_MEETING_PTEAM 
                SET 
                    in_judo_no = '{#in_judo_no}'
                    , in_sign_no = '{#in_sign_no}'
                    , in_section_no = '{#in_judo_no}'
                WHERE
                    source_id = '{#program_id}'
                    AND id = '{#team_id}'
            ";

            sql = sql.Replace("{#program_id}", program_id)
                .Replace("{#team_id}", team_id)
                .Replace("{#in_judo_no}", in_judo_no)
                .Replace("{#in_sign_no}", in_sign_no);

            Item itmSQL = cfg.inn.applySQL(sql);

            if (itmSQL.isError())
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "柔道籤號更新失敗: " + sql);
                throw new Exception("柔道籤號更新失敗");
            }

            itmReturn.setProperty("new_sign_no", in_sign_no);
        }

        #endregion 柔道籤號

        //更新種子籤
        private void UpdateSeed(TConfig cfg, Item itmReturn)
        {
            string program_id = itmReturn.getProperty("program_id", "").Trim();
            string team_id = itmReturn.getProperty("team_id", "").Trim();
            string in_seeds = itmReturn.getProperty("in_seeds", "").Trim();

            string in_points = ""; // 積分，由種子籤倒扣

            int seed_lottery = 0;
            int base_lottery = 10000;
            if (in_seeds == "")
            {
                // 無值
            }
            else if (Int32.TryParse(in_seeds, out seed_lottery))
            {
                in_points = (base_lottery - seed_lottery).ToString();
            }
            else
            {
                throw new Exception("種子籤必須為整數");
            }

            string sql = @"
                UPDATE 
                    IN_MEETING_PTEAM 
                SET 
                    in_seeds = '{#in_seeds}'
                    , in_points = '{#in_points}'
                WHERE
                    source_id = '{#program_id}'
                    AND id = '{#team_id}'
            ";

            sql = sql.Replace("{#program_id}", program_id)
                .Replace("{#team_id}", team_id)
                .Replace("{#in_seeds}", in_seeds)
                .Replace("{#in_points}", in_points);

            Item itmSQL = cfg.inn.applySQL(sql);

            if (itmSQL.isError())
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "種子籤更新失敗: " + sql);
                throw new Exception("種子籤更新失敗");
            }

            Item itmProgram = cfg.inn.applySQL("SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + program_id + "'");
            if (itmProgram.isError() || itmProgram.getResult() == "")
            {
                return;
            }

            Item itmPTeam = cfg.inn.applySQL("SELECT * FROM IN_MEETING_PTEAM WITH(NOLOCK) WHERE id = '" + team_id + "'");
            if (itmPTeam.isError() || itmPTeam.getResult() == "")
            {
                return;
            }

            string meeting_id = itmProgram.getProperty("in_meeting", "");
            string in_l1 = itmProgram.getProperty("in_l1", "");
            string in_l2 = itmProgram.getProperty("in_l2", "");
            string in_l3 = itmProgram.getProperty("in_l3", "");
            string in_index = itmPTeam.getProperty("in_index", "");
            string in_creator_sno = itmPTeam.getProperty("in_creator_sno", "");

            string sql_upd = "UPDATE IN_MEETING_USER SET"
                + " in_seed_lottery = '" + in_seeds + "'"
                + " WHERE source_id = '" + meeting_id + "'"
                + " AND in_l1 = N'" + in_l1 + "'"
                + " AND in_l2 = N'" + in_l2 + "'"
                + " AND in_l3 = N'" + in_l3 + "'"
                + " AND in_index = N'" + in_index + "'"
                + " AND in_creator_sno = N'" + in_creator_sno + "'"
                + "";

            Item itmUpd = cfg.inn.applySQL(sql_upd);
        }

        //更新量級序號
        private void UpdateSectionNo(TConfig cfg, Item itmReturn)
        {
            string program_id = itmReturn.getProperty("program_id", "").Trim();
            string team_id = itmReturn.getProperty("team_id", "").Trim();
            string in_section_no = itmReturn.getProperty("in_section_no", "").Trim();

            string sql = @"
                UPDATE 
                    IN_MEETING_PTEAM 
                SET 
                    in_section_no = '{#in_section_no}'
                WHERE
                    source_id = '{#program_id}'
                    AND id = '{#team_id}'
            ";

            sql = sql.Replace("{#program_id}", program_id)
                .Replace("{#team_id}", team_id)
                .Replace("{#in_section_no}", in_section_no);

            Item itmSQL = cfg.inn.applySQL(sql);

            if (itmSQL.isError())
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "量級序號更新失敗: " + sql);
                throw new Exception("量級序號更新失敗");
            }

            Item itmProgram = cfg.inn.applySQL("SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + program_id + "'");

            if (!itmProgram.isError() && itmProgram.getItemCount() == 1)
            {
                FixMUserSignNo(cfg, itmProgram);
            }
        }

        private void FixMUserSignNo(TConfig cfg, Item itmProgram)
        {
            string sql = "";
            Item itmSQL = null;

            string meeting_id = itmProgram.getProperty("in_meeting", "");
            string program_id = itmProgram.getProperty("id", "");

            sql = @"
                UPDATE t1 SET
	                t1.in_sign_no = t2.in_sign_no
	                , t1.in_section_no = t2.in_section_no
                FROM
	                IN_MEETING_USER t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PTEAM t2 WITH(NOLOCK)
	                ON t2.in_meeting = t1.source_id
	                AND t2.in_team_key = ISNULL(t1.in_l1, '') 
		                + '-' + ISNULL(t1.in_l2, '') 
		                + '-' + ISNULL(t1.in_l3, '') 
		                + '-' + ISNULL(t1.in_index, '')
		                + '-' + ISNULL(t1.in_creator_sno, '')
                WHERE
	                t1.source_id = '{#meeting_id}'
	                AND t2.source_id = '{#program_id}'
	                AND ISNULL(t2.in_sign_no, '') <> ''
            ";

            sql = sql.Replace("{#meeting_id}", meeting_id)
                .Replace("{#program_id}", program_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            itmSQL = cfg.inn.applySQL(sql);

            if (itmSQL.isError())
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "更新與會者籤號與量級序號發生錯誤 _# sql: " + sql);
            }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string meeting_id { get; set; }
            public string program_id { get; set; }
            public string team_id { get; set; }
            public string exe_type { get; set; }
        }
    }
}