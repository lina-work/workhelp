﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Wrestling.Common
{
    public class In_Local_SyncRanks : Item
    {
        public In_Local_SyncRanks(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 現場系統整合(成績同步)
                輸入: meeting_id
                日期: 
                    2021-09-28: 創建 (lina)
             */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]In_Local_SyncRanks";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "input dom: " + itmR.dom.InnerXml);
            CCO.Utilities.WriteDebug(strMethodName, "in");

            //return itmR;

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,

                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
            };

            //新同步名次
            Item itmRanks = GetLocalRanks(cfg);
            RefreshRanks(cfg, itmRanks);

            ////名次修正
            //Item itmChanges = GetChangeScores(cfg);
            //RefreshScore(cfg, itmChanges);

            return itmR;
        }

        private void RefreshRanks(TConfig cfg, Item items)
        {
            if (items.isError() || items.getResult() == "" || items.getItemCount() <= 0)
            {
                return;
            }

            var map = new Dictionary<string, string>();

            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var row = MapRow(cfg, item);

                if (row.Event_Num == "" || row.mt_id == "" || row.pg_id == "" || row.tm_id == "")
                {
                    //更新現場名次檔
                    UpdateLocalRank(cfg, row, "2");
                    continue;
                }

                //更新系統名次
                UpdatePlayer(cfg, row);

                switch (row.pg_battle_type)
                {
                    case "SixPlayers"://六人分組交叉賽制
                    case "SevenPlayers"://七人分組交叉賽制
                        if (!map.ContainsKey(row.pg_program_no))
                        {
                            map.Add(row.pg_program_no, row.pg_id);
                        }
                        break;
                }

                //更新現場名次檔
                UpdateLocalRank(cfg, row, "1");
            }

            foreach (var kv in map)
            {
                ResetCrossPlayers(cfg, kv.Value);
            }
        }

        //分組交叉刷新
        private void ResetCrossPlayers(TConfig cfg, string program_id)
        {
            var items = GetCrossPlayerItems(cfg, program_id);
            if (items.isError() || items.getResult() == "") return;

            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var row = GetCrossEventFoot(cfg, item);
                if (row.fid == "") continue;

                var itmDetail = GetCrossEventDetail(cfg, program_id, row);
                if (itmDetail.isError() || itmDetail.getResult() == "") continue;

                var ft_sign_no = itmDetail.getProperty("in_sign_no", "");
                if (row.sign_no == ft_sign_no) continue;

                if (ft_sign_no == "" || ft_sign_no == "0")
                {
                    UpdCrossEventFootSignNo(cfg, itmDetail, row.sign_no);
                }
            }
        }

        private void UpdCrossEventFootSignNo(TConfig cfg, Item itmDetail, string in_sign_no)
        {
            var evt_id = itmDetail.getProperty("evt_id", "");
            var in_sign_foot = itmDetail.getProperty("in_sign_foot", "");
            var tg_sign_foot = in_sign_foot == "1" ? "2" : "1";

            var sql_upd1 = "UPDATE IN_MEETING_PEVENT_DETAIL SET"
                + "  in_sign_no = '" + in_sign_no + "'"
                + ", in_sign_status = ''"
                + ", in_player_name = ''"
                + " WHERE source_id = '" + evt_id + "'"
                + " AND in_sign_foot = '" + in_sign_foot + "'";

            cfg.inn.applySQL(sql_upd1);


            var sql_upd2 = "UPDATE IN_MEETING_PEVENT_DETAIL SET"
                + "  in_target_no = '" + in_sign_no + "'"
                + ", in_target_status = ''"
                + " WHERE source_id = '" + evt_id + "'"
                + " AND in_sign_foot = '" + tg_sign_foot + "'";

            cfg.inn.applySQL(sql_upd2);
        }

        private TEvtFoot GetCrossEventFoot(TConfig cfg, Item item)
        {
            var result = new TEvtFoot { fid = "", foot = "" };

            var in_sub_id = item.getProperty("in_sub_id", "");
            var in_sign_no = item.getProperty("in_sign_no", "");
            var in_final_rank = item.getProperty("in_final_rank", "");

            result.sign_no = in_sign_no;

            if (in_sub_id == "1")
            {
                //A組
                switch (in_final_rank)
                {
                    case "1":
                        result.fid = "M004-01";
                        result.foot = "1";
                        break;
                    case "2":
                        result.fid = "M004-02";
                        result.foot = "2";
                        break;
                    case "3":
                        result.fid = "RNK56-01";
                        result.foot = "1";
                        break;
                }
            }
            else if (in_sub_id == "2")
            {
                //B組
                switch (in_final_rank)
                {
                    case "1":
                        result.fid = "M004-02";
                        result.foot = "1";
                        break;
                    case "2":
                        result.fid = "M004-01";
                        result.foot = "2";
                        break;
                    case "3":
                        result.fid = "RNK56-01";
                        result.foot = "2";
                        break;
                }
            }
            return result;
        }

        private Item GetCrossEventDetail(TConfig cfg, string program_id, TEvtFoot row)
        {
            string sql = @"
                SELECT 
	                t1.id   AS 'evt_id'
	                , t2.id AS 'dtl_id'
	                , t2.in_sign_foot
	                , t2.in_sign_no
                FROM
	                IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                WHERE
	                t1.source_id = '{#program_id}'
	                AND t1.in_fight_id = '{#in_fight_id}'
	                AND t2.in_sign_foot = '{#in_sign_foot}'
            ";

            sql = sql.Replace("{#program_id}", program_id)
                .Replace("{#in_fight_id}", row.fid)
                .Replace("{#in_sign_foot}", row.foot);

            return cfg.inn.applySQL(sql);
        }

        private Item GetCrossPlayerItems(TConfig cfg, string program_id)
        {
            string sql = @"
                SELECT
                    t1.in_sub_id
                    , t1.in_sub_sect
                    , t1.in_team_index
                    , t1.in_name
                    , t1.in_current_org
                    , t1.in_short_org
                    , t1.in_sign_no
                    , t1.in_final_rank
                FROM
                    IN_MEETING_PTEAM t1 WITH(NOLOCK)
                WHERE
                    t1.source_id = '{#program_id}'
                    AND ISNULL(t1.in_final_rank, 0) > 0
                ORDER BY
                    t1.in_sub_id
                    , t1.in_final_rank
            ";

            sql = sql.Replace("{#program_id}", program_id);

            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// 更新現場名次檔
        /// </summary>
        private void UpdateLocalRank(TConfig cfg, TRow row, string sync_flag)
        {
            string sql = "UPDATE In_Local_Rank SET"
                + "  SyncFlag = '" + sync_flag + "'"
                + ", UpdResult = '" + row.Rank + "'"
                + " WHERE no = '" + row.No + "'";

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            cfg.inn.applySQL(sql);
        }

        private void UpdatePlayer(TConfig cfg, TRow row)
        {
            string sql = "UPDATE IN_MEETING_PTEAM SET"
                + "  in_final_rank = '" + row.Rank + "'"
                + ", in_show_rank = '" + row.Rank + "'"
                + " WHERE id = '" + row.tm_id + "'";

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item itmSQL = cfg.inn.applySQL(sql);

            if (itmSQL.isError())
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "更新 IN_MEETING_PTEAM 失敗: " + sql);
            }
        }


        private TRow MapRow(TConfig cfg, Item itmSchedule)
        {
            TRow result = new TRow
            {
                No = itmSchedule.getProperty("no", ""),
                Event_Num = itmSchedule.getProperty("event_num", ""),
                SDte = itmSchedule.getProperty("sdte", ""),
                EGroup = itmSchedule.getProperty("egroup", ""),
                EGrade = itmSchedule.getProperty("egrade", ""),
                Item = itmSchedule.getProperty("item", ""),
                EWeight = itmSchedule.getProperty("eweight", ""),
                Aplseq = itmSchedule.getProperty("aplseq", ""),
                Dptname = itmSchedule.getProperty("dptname", ""),
                Ptname = itmSchedule.getProperty("ptname", ""),
                Rank = itmSchedule.getProperty("rank", ""),

                mt_id = itmSchedule.getProperty("mt_id", ""),
                pg_id = itmSchedule.getProperty("pg_id", ""),
                pg_team_count = itmSchedule.getProperty("pg_team_count", ""),
                pg_battle_type = itmSchedule.getProperty("pg_battle_type", ""),
                pg_program_no = itmSchedule.getProperty("pg_program_no", ""),

                tm_id = itmSchedule.getProperty("tm_id", ""),
                in_name = itmSchedule.getProperty("in_name", ""),
                in_current_org = itmSchedule.getProperty("in_current_org", ""),
                in_final_rank = itmSchedule.getProperty("in_final_rank", ""),
            };

            return result;
        }

        private Item GetLocalRanks(TConfig cfg)
        {
            string sql = @"
                SELECT
	                t1.*
	                , t2.id             AS 'mt_id'
	                , t2.in_title       AS 'mt_title'
	                , t3.id             AS 'pg_id'
	                , t3.in_team_count  AS 'pg_team_count'
	                , t3.in_battle_type AS 'pg_battle_type'
	                , t3.in_program_no  AS 'pg_program_no'
	                , t4.id             AS 'tm_id'
	                , t4.in_final_rank
                FROM
	                In_Local_Rank t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING t2 WITH(NOLOCK)
	                ON t2.item_number = t1.MNumber
                INNER JOIN
	                IN_MEETING_PROGRAM t3 WITH(NOLOCK)
	                ON t3.in_meeting = t2.id
	                AND t3.in_l1 = t1.Item
	                AND t3.in_l2 = t1.EGrade
	                AND t3.in_l3 = t1.EWeight
                LEFT OUTER JOIN
	                IN_MEETING_PTEAM t4 WITH(NOLOCK)
	                ON t4.source_id = t3.id
	                AND t4.in_team_index = t1.Aplseq
                WHERE
	                ISNULL(t1.SyncFlag, '') = ''
            ";

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        private Item GetChangeRanks(TConfig cfg)
        {
            string sql = @"
                SELECT 
	                t1.*
	                , t2.id AS 'MId'
                FROM 
	                In_Local_Schedule t1 WITH(NOLOCK) 
                INNER JOIN 
	                IN_MEETING t2 WITH(NOLOCK) 
	                ON t2.item_number = t1.MNumber 
				WHERE
                    ISNULL(t1.SyncFlag, '') = 1
					AND (
                        ISNULL(t1.WhiteI, '') 
                        + '-' + ISNULL(t1.WhiteW, '') 
                        + '-' + ISNULL(t1.WhiteS, '') 
                        + '-' + ISNULL(t1.BlueI, '') 
                        + '-' + ISNULL(t1.BlueW, '') 
                        + '-' + ISNULL(t1.BlueS, '') 
                        + '-' + ISNULL(t1.Win, '')
                        <> 
                        ISNULL(t1.UpdResult, '')
                    )
            ";

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }

            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
        }

        private class TRow
        {
            /// <summary>
            /// 編號
            /// </summary>
            public string No { get; set; }

            /// <summary>
            /// 活動代碼
            /// </summary>
            public string Event_Num { get; set; }

            /// <summary>
            /// 比賽日期
            /// </summary>
            public string SDte { get; set; }

            /// <summary>
            /// 級組
            /// </summary>
            public string EGroup { get; set; }

            /// <summary>
            /// 組別
            /// </summary>
            public string EGrade { get; set; }

            /// <summary>
            /// 項目
            /// </summary>
            public string Item { get; set; }

            /// <summary>
            /// 量級
            /// </summary>
            public string EWeight { get; set; }

            /// <summary>
            /// 選手編號
            /// </summary>
            public string Aplseq { get; set; }

            /// <summary>
            /// 選手單位
            /// </summary>
            public string Dptname { get; set; }

            /// <summary>
            /// 選手姓名
            /// </summary>
            public string Ptname { get; set; }

            /// <summary>
            /// 名次
            /// </summary>
            public string Rank { get; set; }

            /// <summary>
            /// 活動 id
            /// </summary>
            public string mt_id { get; set; }

            /// <summary>
            /// 組別 id
            /// </summary>
            public string pg_id { get; set; }

            /// <summary>
            /// 參數人數
            /// </summary>
            public string pg_team_count { get; set; }

            /// <summary>
            /// 賽制
            /// </summary>
            public string pg_battle_type { get; set; }

            /// <summary>
            /// 組別代號
            /// </summary>
            public string pg_program_no { get; set; }

            /// <summary>
            /// 隊伍 id
            /// </summary>
            public string tm_id { get; set; }

            /// <summary>
            /// 競賽系統 姓名
            /// </summary>
            public string in_name { get; set; }

            /// <summary>
            /// 競賽系統 所屬單位
            /// </summary>
            public string in_current_org { get; set; }

            /// <summary>
            /// 競賽系統 名次
            /// </summary>
            public string in_final_rank { get; set; }
        }

        private class TEvtFoot
        {
            public string sign_no { get; set; }
            public string fid { get; set; }
            public string foot { get; set; }
        }
    }
}