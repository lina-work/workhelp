﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Wrestling.Common
{
    public class In_Meeting_User_ImportNew : Item
    {
        public In_Meeting_User_ImportNew(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 新版與會者匯入
                日誌: 
                    - 2024-03-20: 組別改到 in_l1 = 性別判斷欄位 (lina)
                    - 2023-03-09: 與會者改為可分開匯入 (lina)
                    - 2022-10-03: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "In_Meeting_User_ImportNew";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,

                meeting_id = itmR.getProperty("meeting_id", ""),
                need_rebuild_meeting = itmR.getProperty("need_rebuild_meeting", ""),
                in_battle_mode = itmR.getProperty("in_battle_mode", ""),
                in_battle_repechage = itmR.getProperty("in_battle_repechage", ""),
                scene = itmR.getProperty("scene", ""),
                import_all_muser = true,
            };

            switch (cfg.scene)
            {
                case "import":
                    CacheStep1(cfg, itmR);
                    Import(cfg, itmR);
                    MatchSiteCode(cfg, itmR);
                    break;

                case "rebuild":
                    Rebuild(cfg, itmR);
                    break;

                case "fight":
                    Fight(cfg, itmR);
                    break;

                case "day_menu":
                    DayMenu(cfg, itmR);
                    break;

                case "fix_rank": //修正準決賽名次
                    FixOther(cfg, itmR);
                    CacheStep2(cfg, itmR);
                    break;

                case "refresh_rank":
                    RefreshRank(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void RefreshRank(TConfig cfg, Item itmReturn)
        {
            ClearProgramRranks(cfg);

            var items = GetProgramRrankItems(cfg);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var team_id = item.getProperty("team_id", "");
                var org = item.getProperty("dptname", "").Trim();
                var player = item.getProperty("ptname", "").Trim();
                var rank = item.getProperty("rank", "");

                if (org == "" || player == "")
                {
                    continue;
                }

                if (team_id == "")
                {
                    team_id = GetRankTeamId(cfg, item);
                }

                if (team_id == "")
                {
                    throw new Exception(org + " " + player + " 系統查無資料");
                }

                var sql_upd = "UPDATE IN_MEETING_PTEAM SET"
                    + "  in_final_rank = '" + rank + "'"
                    + ", in_show_rank = '" + rank + "'"
                    + " WHERE id = '" + team_id + "'";

                cfg.inn.applySQL(sql_upd);
            }
        }

        private string GetRankTeamId(TConfig cfg, Item item)
        {
            var program_id = item.getProperty("program_id", "");
            var org = item.getProperty("dptname", "");
            var player = item.getProperty("ptname", "");

            var sql = @"
                SELECT TOP 1
	                id
                FROM
	                IN_MEETING_PTEAM WITH(NOLOCK)
                WHERE
	                source_id = '{#program_id}'
	                AND in_current_org = N'{#org}'
	                AND in_name = N'{#player}'
            ";

            sql = sql.Replace("{#program_id}", program_id)
                .Replace("{#org}", org)
                .Replace("{#player}", player);

            var itmData = cfg.inn.applySQL(sql);
            if (!itmData.isError() && itmData.getResult() != "")
            {
                return itmData.getProperty("id", "");
            }
            else
            {
                return "";
            }
        }

        private Item GetProgramRrankItems(TConfig cfg)
        {
            var sql = @"
	            SELECT  
		            t3.id               AS 'program_id'
		            , t3.in_name
		            , t3.in_program_no
		            , t1.Aplseq
		            , t1.Dptname
		            , t1.Ptname
		            , t1.Rank
		            , t4.id             AS 'team_id'
	            FROM 
		            In_Local_Rank t1 WITH(NOLOCK)
	            INNER JOIN
		            IN_MEETING t2 WITH(NOLOCK)
		            ON t2.item_number = t1.MNumber
	            INNER JOIN
		            IN_MEETING_PROGRAM t3 WITH(NOLOCK)
		            ON t3.in_meeting = t2.id
		            AND t3.in_l1 = t1.Item
		            AND t3.in_l2 = t1.EGrade
		            AND t3.in_l3 = t1.EWeight
	            LEFT OUTER JOIN
		            IN_MEETING_PTEAM t4 WITH(NOLOCK)
		            ON t4.source_id = t3.id
		            AND t4.in_team_index = t1.Aplseq
	            WHERE
		            t2.id = '{#meeting_id}'
	            ORDER BY
		            t3.in_sort_order
		            , t1.rank
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            return cfg.inn.applySQL(sql);
        }

        private void ClearProgramRranks(TConfig cfg)
        {
            var sql = @"
                UPDATE IN_MEETING_PTEAM SET
	                  in_final_rank = NULL
	                , in_show_rank = NULL
                WHERE
	                id IN
                (
	                SELECT DISTINCT 
		                t3.id
	                FROM 
		                IN_LOCAL_RANK t1 WITH(NOLOCK)
	                INNER JOIN
		                IN_MEETING t2 WITH(NOLOCK)
		                ON t2.item_number = t1.MNumber
	                INNER JOIN
		                IN_MEETING_PROGRAM t3 WITH(NOLOCK)
		                ON t3.in_meeting = t2.id
	                WHERE
		                t2.id = '{#meeting_id}'
                )
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            cfg.inn.applySQL(sql);
        }

        private void CacheStep1(TConfig cfg, Item itmReturn)
        {
            if (cfg.need_rebuild_meeting == "1") return;

            var sql = "";

            sql = "UPDATE IN_MEETING_ALLOCATION SET in_meeting = NULL WHERE in_meeting = '" + cfg.meeting_id + "'";
            cfg.inn.applySQL(sql);

            sql = "UPDATE IN_MEETING_PROGRAM SET in_meeting = NULL WHERE in_meeting = '" + cfg.meeting_id + "'";
            cfg.inn.applySQL(sql);

            sql = "UPDATE IN_MEETING_PTEAM SET in_meeting = NULL WHERE in_meeting = '" + cfg.meeting_id + "'";
            cfg.inn.applySQL(sql);

            sql = "UPDATE IN_MEETING_PEVENT SET in_meeting = NULL WHERE in_meeting = '" + cfg.meeting_id + "'";
            cfg.inn.applySQL(sql);

            sql = "UPDATE IN_MEETING_USER SET source_id = NULL WHERE source_id = '" + cfg.meeting_id + "'";
            cfg.inn.applySQL(sql);
        }

        private void CacheStep2(TConfig cfg, Item itmReturn)
        {
            var sql = "";

            //檢查是否有緩存資料
            sql = "SELECT TOP 1 id FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE in_meeting IS NULL";
            var itmData = cfg.inn.applySQL(sql);
            if (itmData.isError() || itmData.getResult() == "") return;

            sql = "UPDATE IN_MEETING_ALLOCATION SET in_meeting = '" + cfg.meeting_id + "' WHERE in_meeting IS NULL";
            cfg.inn.applySQL(sql);

            sql = "UPDATE IN_MEETING_PROGRAM SET in_meeting = '" + cfg.meeting_id + "' WHERE in_meeting IS NULL";
            cfg.inn.applySQL(sql);

            sql = "UPDATE IN_MEETING_PTEAM SET in_meeting = '" + cfg.meeting_id + "' WHERE in_meeting IS NULL";
            cfg.inn.applySQL(sql);

            sql = "UPDATE IN_MEETING_PEVENT SET in_meeting = '" + cfg.meeting_id + "' WHERE in_meeting IS NULL";
            cfg.inn.applySQL(sql);

            sql = "UPDATE IN_MEETING_USER SET source_id = '" + cfg.meeting_id + "' WHERE source_id IS NULL";
            cfg.inn.applySQL(sql);

            sql = @"
                UPDATE t1 SET
                	t1.IN_SORT_ORDER = rn * 100
                FROM
                	IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
                (
                	SELECT
                		id
                		, ROW_NUMBER() OVER (ORDER BY in_l1, in_l2, in_sort_order) AS 'rn' 
                	FROM
                		IN_MEETING_PROGRAM WITH(NOLOCK)
                	WHERE
                		in_meeting = '{#meeting_id}'
                ) t2 ON t2.id = t1.id
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);
            cfg.inn.applySQL(sql);
        }

        private void DayMenu(TConfig cfg, Item itmReturn)
        {
            string sql = @"
                SELECT DISTINCT
	                in_day AS 'in_day'
                FROM
	                IN_MEETING_USER_IMPORTNEW WITH(NOLOCK)
                WHERE
	                in_meeting = '{#meeting_id}'
                ORDER BY
	                in_day
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql + ": " + count.ToString());

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_day = item.getProperty("in_day", "");
                string in_fight_day = GetFightDay(cfg, in_day);
                item.setProperty("in_date", in_fight_day);
                item.setType("inn_day");
                itmReturn.addRelationship(item);
            }
        }

        private void Fight(TConfig cfg, Item itmReturn)
        {
            if (cfg.import_all_muser)
            {
                Fight1(cfg, itmReturn);
            }
            else
            {
                Fight2(cfg, itmReturn);
            }

        }
        private void Fight1(TConfig cfg, Item itmReturn)
        {
            //賽會參數設定
            RebuildMtVariable(cfg, itmReturn);
            //過磅狀態設定
            RebuildMtWeight(cfg, itmReturn);
            //清除賽程組別資料
            ClearAllPrograms1(cfg, itmReturn);

            var itmDays = GetFightDayItems(cfg, itmReturn);
            var count = itmDays.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var itmDay = itmDays.getItemByIndex(i);
                var in_day = itmDay.getProperty("in_day", "");
                var in_fight_day = GetFightDay(cfg, in_day);
                //建立賽程組別資料
                CreatePrograms1(cfg, in_fight_day);
                //更新隊伍資料
                UpdateTeamData1(cfg, in_fight_day);
            }

            //建立場地分配
            CreateAllocation(cfg, itmReturn);
            //建立競賽連結
            CreateQrCode(cfg, itmReturn);
            //清除本地同步資料
            RemoveLocalSchedules(cfg, itmReturn);
        }

        private void RemoveLocalSchedules(TConfig cfg, Item itmReturn)
        {
            var sql = "SELECT id, item_number FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'";
            var itmMeeting = cfg.inn.applySQL(sql);
            var item_number = itmMeeting.getProperty("item_number", "");

            sql = "DELETE FROM In_Local_Schedule WHERE MNumber = '" + item_number + "'";
            cfg.inn.applySQL(sql);
        }

        private void Fight2(TConfig cfg, Item itmReturn)
        {
            //賽會參數設定
            RebuildMtVariable(cfg, itmReturn);
            //過磅狀態設定
            RebuildMtWeight(cfg, itmReturn);
            //清除賽程組別資料
            ClearAllPrograms2(cfg, itmReturn);

            var itmDays = GetFightDayItems(cfg, itmReturn);
            var count = itmDays.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var itmDay = itmDays.getItemByIndex(i);
                var in_day = itmDay.getProperty("in_day", "");
                var in_fight_day = GetFightDay(cfg, in_day);
                //建立賽程組別資料
                CreatePrograms2(cfg, in_fight_day);
                //更新隊伍資料
                UpdateTeamData2(cfg, in_fight_day);
            }

            //建立場地分配
            CreateAllocation(cfg, itmReturn);
            //建立競賽連結
            CreateQrCode(cfg, itmReturn);
        }

        private Item GetFightDayItems(TConfig cfg, Item itmReturn)
        {
            string sql = @"
                SELECT
	                DISTINCT in_day
                FROM
	                IN_MEETING_USER_IMPORTNEW t1 WITH(NOLOCK)
                ORDER BY
	                in_day
            ";
            return cfg.inn.applySQL(sql);
        }

        //修正其他
        private void FixOther(TConfig cfg, Item itmReturn)
        {
            FixSectCode(cfg, itmReturn);

            //修正第七名
            FixRepecahgeRank57(cfg, itmReturn);

            //將人數少於等於 1 者關閉
            ClosePrograms(cfg, itmReturn);

            //將組別代碼做排序
            CloneProgramNoToShortName(cfg, itmReturn);

            //修正第三階排序
            FixLevel3SortOrder(cfg, itmReturn);

            //FixSemiFinalRank(cfg, itmReturn);
        }

        //修正第三階排序
        private void FixLevel3SortOrder(TConfig cfg, Item itmReturn)
        {
            var sql = @"
                UPDATE t11 SET
                	t11.sort_order = t12.in_sort_order
                FROM
                	IN_SURVEY_OPTION t11
                INNER JOIN
                (
                	SELECT
                		t2.opt_id
                		, t2.in_value
                		, t1.in_sort_order
                	FROM
                		IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                	INNER JOIN
                		VU_MEETING_SVY_L3 t2
                		ON t2.source_id = t1.in_meeting
                		AND t2.in_grand_filter = t1.in_l1
                		AND t2.in_filter = t1.in_l2
                		AND t2.in_value = t1.in_l3
                	WHERE
                		t1.in_meeting = '{#meeting_id}'
                ) t12 ON t12.opt_id = t11.id
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            cfg.inn.applySQL(sql);
        }

        //將組別代碼做排序
        private void CloneProgramNoToShortName(TConfig cfg, Item itmReturn)
        {
            // var sql1 = "UPDATE IN_MEETING_PROGRAM SET in_short_name = in_program_no WHERE in_meeting = '{#meeting_id}'";
            // sql1 = sql1.Replace("{#meeting_id}", cfg.meeting_id);
            // cfg.inn.applySQL(sql1);

            var sql2 = @"
                UPDATE t1 SET
                	t1.in_sort_order = t2.rn * 100
                FROM
                    IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
                (
                	SELECT id, ROW_NUMBER() OVER (ORDER BY in_program_no) AS 'rn' FROM IN_MEETING_PROGRAM WITH(NOLOCK)
                ) t2 ON t2.id = t1.id
                WHERE
                	in_meeting = '{#meeting_id}'
            ";
            sql2 = sql2.Replace("{#meeting_id}", cfg.meeting_id);
            cfg.inn.applySQL(sql2);

            var sqls = new List<string>();

            sqls.Add("UPDATE IN_MEETING_PROGRAM SET in_n1 = '公開男生組' WHERE in_meeting = '" + cfg.meeting_id + "' AND in_l1 = '公開男生組'");
            sqls.Add("UPDATE IN_MEETING_PROGRAM SET in_n1 = '公開女生組' WHERE in_meeting = '" + cfg.meeting_id + "' AND in_l1 = '公開女生組'");
            sqls.Add("UPDATE IN_MEETING_PROGRAM SET in_n1 = '一般男生組' WHERE in_meeting = '" + cfg.meeting_id + "' AND in_l1 = '一般男生組'");
            sqls.Add("UPDATE IN_MEETING_PROGRAM SET in_n1 = '一般女生組' WHERE in_meeting = '" + cfg.meeting_id + "' AND in_l1 = '一般女生組'");

            sqls.Add("UPDATE IN_MEETING_PROGRAM SET in_n1 = '國中部男生組' WHERE in_meeting = '" + cfg.meeting_id + "' AND in_l1 = '國男組'");
            sqls.Add("UPDATE IN_MEETING_PROGRAM SET in_n1 = '國中部女生組' WHERE in_meeting = '" + cfg.meeting_id + "' AND in_l1 = '國女組'");
            sqls.Add("UPDATE IN_MEETING_PROGRAM SET in_n1 = '高中部男生組' WHERE in_meeting = '" + cfg.meeting_id + "' AND in_l1 = '高男組'");
            sqls.Add("UPDATE IN_MEETING_PROGRAM SET in_n1 = '高中部女生組' WHERE in_meeting = '" + cfg.meeting_id + "' AND in_l1 = '高女組'");

            sqls.Add("UPDATE IN_MEETING_PROGRAM SET in_n2 = in_l2 WHERE in_meeting = '" + cfg.meeting_id + "'");

            sqls.Add("UPDATE IN_MEETING_PROGRAM SET in_n3 = '第一級' WHERE in_meeting = '" + cfg.meeting_id + "' AND in_l3 LIKE '%第一級%'");
            sqls.Add("UPDATE IN_MEETING_PROGRAM SET in_n3 = '第二級' WHERE in_meeting = '" + cfg.meeting_id + "' AND in_l3 LIKE '%第二級%'");
            sqls.Add("UPDATE IN_MEETING_PROGRAM SET in_n3 = '第三級' WHERE in_meeting = '" + cfg.meeting_id + "' AND in_l3 LIKE '%第三級%'");
            sqls.Add("UPDATE IN_MEETING_PROGRAM SET in_n3 = '第四級' WHERE in_meeting = '" + cfg.meeting_id + "' AND in_l3 LIKE '%第四級%'");
            sqls.Add("UPDATE IN_MEETING_PROGRAM SET in_n3 = '第五級' WHERE in_meeting = '" + cfg.meeting_id + "' AND in_l3 LIKE '%第五級%'");
            sqls.Add("UPDATE IN_MEETING_PROGRAM SET in_n3 = '第六級' WHERE in_meeting = '" + cfg.meeting_id + "' AND in_l3 LIKE '%第六級%'");
            sqls.Add("UPDATE IN_MEETING_PROGRAM SET in_n3 = '第七級' WHERE in_meeting = '" + cfg.meeting_id + "' AND in_l3 LIKE '%第七級%'");
            sqls.Add("UPDATE IN_MEETING_PROGRAM SET in_n3 = '第八級' WHERE in_meeting = '" + cfg.meeting_id + "' AND in_l3 LIKE '%第八級%'");
            sqls.Add("UPDATE IN_MEETING_PROGRAM SET in_n3 = '第九級' WHERE in_meeting = '" + cfg.meeting_id + "' AND in_l3 LIKE '%第九級%'");
            sqls.Add("UPDATE IN_MEETING_PROGRAM SET in_n3 = '第十級' WHERE in_meeting = '" + cfg.meeting_id + "' AND in_l3 LIKE '%第十級%'");
            sqls.Add("UPDATE IN_MEETING_PROGRAM SET in_n3 = '第十一級' WHERE in_meeting = '" + cfg.meeting_id + "' AND in_l3 LIKE '%第十一級%'");
            sqls.Add("UPDATE IN_MEETING_PROGRAM SET in_n3 = '第十二級' WHERE in_meeting = '" + cfg.meeting_id + "' AND in_l3 LIKE '%第十二級%'");
            sqls.Add("UPDATE IN_MEETING_PROGRAM SET in_n3 = '第十三級' WHERE in_meeting = '" + cfg.meeting_id + "' AND in_l3 LIKE '%第十三級%'");
            sqls.Add("UPDATE IN_MEETING_PROGRAM SET in_n3 = '第十四級' WHERE in_meeting = '" + cfg.meeting_id + "' AND in_l3 LIKE '%第十四級%'");
            sqls.Add("UPDATE IN_MEETING_PROGRAM SET in_n3 = '第十五級' WHERE in_meeting = '" + cfg.meeting_id + "' AND in_l3 LIKE '%第十五級%'");

            sqls.Add("UPDATE IN_MEETING_PROGRAM SET in_n4 = in_l1 + in_l2 + in_l3 WHERE in_meeting = '" + cfg.meeting_id + "'");

            cfg.inn.applySQL(string.Join("; ", sqls));
        }

        //修正第七名
        private void FixRepecahgeRank57(TConfig cfg, Item itmReturn)
        {
            var sql = @"
                UPDATE t11 SET
                	 t11.in_tree_rank = 'rank57'
                   , t11.in_tree_rank_ns = '0,7' 
                   , t11.in_tree_rank_nss = '0,7' 
                FROM
                	IN_MEETING_PEVENT t11 WITH(NOLOCK)
                INNER JOIN
                (
                	SELECT DISTINCT
                		t1.in_program_no
                		, t2.id
                		, t2.in_team_count
                	FROM 
                		IN_MEETING_USER_IMPORTNEW t1 WITH(NOLOCK)
                	INNER JOIN
                		IN_MEETING_PROGRAM t2 WITH(NOLOCK)
                		ON t2.in_meeting = t1.in_meeting
                		AND t2.in_program_no = t1.in_program_no
                ) t12 ON t12.id = t11.source_id
                WHERE
                    t11.in_meeting = '{#meeting_id}'
                	AND t12.in_team_count >= 8
                	AND t11.in_fight_id IN ('R008-01', 'R008-02') 
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            cfg.inn.applySQL(sql);
        }

        //將人數少於等於 1 者關閉
        private void ClosePrograms(TConfig cfg, Item itmReturn)
        {
            string sql = @"
                UPDATE t1 SET
                	t1.in_team_count = 0
                FROM
                	IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                WHERE
                	t1.in_meeting = '{#meeting_id}'
                	AND ISNULL(t1.in_team_count, 0) <= 1
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            cfg.inn.applySQL(sql);
        }

        //修正組別代碼
        private void FixSectCode(TConfig cfg, Item itmReturn)
        {
            string sql = @"
                UPDATE t1 SET
                	t1.in_program_no = t2.in_program_no
                FROM
                	IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
                (
                	SELECT DISTINCT
						in_meeting
						, in_program_no
						, in_l1, in_l2
						, in_l3 
					FROM 
						In_Meeting_User_ImportNew WITH(NOLOCK)
                ) t2 ON t2.in_meeting = t1.in_meeting
                    AND t2.in_l1 = t1.in_l1
                    AND t2.in_l2 = t1.in_l2
                    AND t2.in_l3 = t1.in_l3
                WHERE
                	t1.in_meeting = '{#meeting_id}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            cfg.inn.applySQL(sql);
        }

        // //修正準決賽名次
        // private void FixSemiFinalRank(TConfig cfg, Item itmReturn)
        // {
        //     string sql = @"
        //         UPDATE IN_MEETING_PEVENT SET
        //             in_tree_rank = 'rank23'
        //             , in_tree_rank_ns = '2,3'
        //             , in_tree_rank_nss = '2,3'
        //         where in_meeting = '{#meeting_id}'
        //         AND in_tree_name = 'main'
        //         AND in_round_code = 4
        //         AND ISNULL(in_robin_key, '') = ''
        //     ";

        //     sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

        //     cfg.inn.applySQL(sql);
        // }

        //建立競賽連結
        private void CreateQrCode(TConfig cfg, Item itmReturn)
        {
            Item itmData = cfg.inn.newItem("In_Meeting");
            itmData.setProperty("meeting_id", cfg.meeting_id);
            itmData.apply("in_qrcode_create");
        }

        //建立場地分配
        private void CreateAllocation(TConfig cfg, Item itmReturn)
        {
            //建立場地分配
            var sql = "SELECT * FROM IN_MEETING_SITE WITH(NOLOCK) WHERE in_meeting = '{#meeting_id}' AND in_code = 1";
            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);
            var itmSite = cfg.inn.applySQL(sql);
            if (itmSite.isError() || itmSite.getResult() == "")
            {
                return;
            }

            //清除舊場地分配
            cfg.inn.applySQL("DELETE FROM IN_MEETING_ALLOCATION WHERE in_meeting = '" + cfg.meeting_id + "'");

            //建立主分類
            MergeAllocateCategory(cfg);

            //建立場地分配
            MergeAllocateFight(cfg, itmSite);
        }

        private void MergeAllocateFight(TConfig cfg, Item itmSiteSrc)
        {
            List<string> pgs = new List<string>();

            string sql = @"
                SELECT DISTINCT
					in_program_no
					, in_l1
					, in_l2
					, in_l3
					, in_fight_day
                FROM 
                    In_Meeting_User WITH(NOLOCK) 
                WHERE 
                    source_id = '{#meeting_id}'
                ORDER BY
					in_program_no
					, in_l1
					, in_l2
					, in_l3
					, in_fight_day
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_l1 = item.getProperty("in_l1", "");
                string in_l2 = item.getProperty("in_l2", "");
                string in_l3 = item.getProperty("in_l3", "");
                string in_fight_day = item.getProperty("in_fight_day", "");

                string sql_qry1 = "SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK)"
                    + " WHERE in_meeting = '" + cfg.meeting_id + "'"
                    + " AND in_l1 = N'" + in_l1 + "'"
                    + " AND in_l2 = N'" + in_l2 + "'"
                    + " AND in_l3 = N'" + in_l3 + "'";

                Item itmProgram = cfg.inn.applySQL(sql_qry1);
                if (itmProgram.isError() || itmProgram.getResult() == "")
                {
                    continue;
                }

                string sql_qry2 = @"SELECT TOP 1 t2.id FROM IN_MEETING_USER_IMPORTNEW t1 WITH(NOLOCK)"
                    + " LEFT OUTER JOIN IN_MEETING_SITE t2 WITH(NOLOCK)"
                    + " ON t2.in_meeting = t1.in_meeting"
                    + " AND CAST(t2.in_code AS VARCHAR) = t1.in_site_code"
                    + " WHERE t1.in_meeting = '" + cfg.meeting_id + "'"
                    + " AND t1.in_l1 = N'" + in_l1 + "'"
                    + " AND t1.in_l2 = N'" + in_l2 + "'"
                    + " AND t1.in_l3 = N'" + in_l3 + "'";

                Item itmSite = cfg.inn.applySQL(sql_qry2);
                if (itmSite.isError() || itmSite.getResult() == "")
                {
                    itmSite = itmSiteSrc;
                }


                if (in_fight_day == "")
                {
                    continue;
                }


                string site_id = itmSite.getProperty("id", "");
                string program_id = itmProgram.getProperty("id", "");

                //補刀 (比賽日期)
                cfg.inn.applySQL("UPDATE IN_MEETING_PROGRAM SET in_fight_day = '" + in_fight_day + "' WHERE id = '" + program_id + "'");

                Item itmNew = cfg.inn.newItem("In_Meeting_Allocation", "add");
                itmNew.setProperty("in_meeting", cfg.meeting_id);
                itmNew.setProperty("in_date", in_fight_day);
                itmNew.setProperty("in_date_key", in_fight_day);
                itmNew.setProperty("in_site", site_id);
                itmNew.setProperty("in_program", program_id);
                itmNew.setProperty("in_type", "fight");
                itmNew.setProperty("in_l1", "");
                itmNew.setProperty("in_l2", "");
                itmNew = itmNew.apply();

                pgs.Add(program_id);
            }

            DateTime dt = DateTime.Now.AddDays(-1).Date;

            foreach (var pg in pgs)
            {
                string sql_upd = "UPDATE IN_MEETING_ALLOCATION SET created_on = '" + dt.ToString("yyyy-MM-dd HH:mm:00") + "' WHERE in_meeting = '" + cfg.meeting_id + "' AND in_program = '" + pg + "'";
                cfg.inn.applySQL(sql_upd);
                dt = dt.AddMinutes(10);
            }
        }

        private void MergeAllocateCategory(TConfig cfg)
        {
            string sql = @"
                SELECT DISTINCT
					in_program_no
					, in_l1
					, in_l2
					, in_fight_day
                FROM 
                    In_Meeting_User WITH(NOLOCK) 
                WHERE 
                    source_id = '{#meeting_id}'
                ORDER BY
					in_program_no
					, in_l1
					, in_l2
					, in_fight_day
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_l1 = item.getProperty("in_l1", "");
                string in_l2 = item.getProperty("in_l2", "");
                string in_fight_day = item.getProperty("in_fight_day", "");

                if (in_fight_day == "")
                {
                    continue;
                }

                Item itmNew = cfg.inn.newItem("In_Meeting_Allocation", "add");
                itmNew.setProperty("in_meeting", cfg.meeting_id);
                itmNew.setProperty("in_date", in_fight_day);
                itmNew.setProperty("in_date_key", in_fight_day);
                itmNew.setProperty("in_type", "category");
                itmNew.setProperty("in_l1", in_l1);
                itmNew.setProperty("in_l2", in_l2);
                itmNew = itmNew.apply();
            }
        }

        private void UpdateTeamData1(TConfig cfg, string in_fight_day)
        {
            var sql = @"
                UPDATE t1 SET
                	t1.in_sign_no = t11.in_draw_no
                	, t1.in_section_no = t11.in_draw_no
                	, t1.in_team_index = t11.in_index
                FROM
                	IN_MEETING_PTEAM t1 WITH(NOLOCK)
                INNER JOIN
                	IN_MEETING_PROGRAM t2 WITH(NOLOCK)
                	ON t2.id = t1.source_id
                INNER JOIN
                	IN_MEETING_USER t11 WITH(NOLOCK)
                	ON t11.source_id = t2.in_meeting
                	AND t11.in_l1 = t2.in_l1
                	AND t11.in_l2 = t2.in_l2
                	AND t11.in_l3 = t2.in_l3
                	AND t11.in_sno = t1.in_sno
                WHERE
                	t11.source_id = '{#meeting_id}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_fight_day}", in_fight_day);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            cfg.inn.applySQL(sql);
        }

        private void UpdateTeamData2(TConfig cfg, string in_fight_day)
        {
            var sql = @"
                UPDATE t1 SET
                	t1.in_sign_no = t11.in_draw_no
                	, t1.in_section_no = t11.in_draw_no
                	, t1.in_team_index = t11.in_index
                FROM
                	IN_MEETING_PTEAM t1 WITH(NOLOCK)
                INNER JOIN
                	IN_MEETING_PROGRAM t2 WITH(NOLOCK)
                	ON t2.id = t1.source_id
                INNER JOIN
                	IN_MEETING_USER t11 WITH(NOLOCK)
                	ON t11.source_id = t2.in_meeting
                	AND t11.in_l1 = t2.in_l1
                	AND t11.in_l2 = t2.in_l2
                	AND t11.in_l3 = t2.in_l3
                	AND t11.in_sno = t1.in_sno
                WHERE
                	t11.source_id = '{#meeting_id}'
                	AND t11.in_fight_day = '{#in_fight_day}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_fight_day}", in_fight_day);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            cfg.inn.applySQL(sql);
        }

        private void CreatePrograms1(TConfig cfg, string in_fight_day)
        {
            Item itmData = cfg.inn.newItem("In_Meeting");
            itmData.setProperty("meeting_id", cfg.meeting_id);
            itmData.setProperty("in_fight_day", in_fight_day);
            itmData.setProperty("battle_type", "WrestlingTopTwo");
            itmData.setProperty("battle_repechage", "");
            itmData.setProperty("rank_type", "SameRank");
            itmData.setProperty("surface_code", "32");
            itmData.setProperty("robin_player", "0");
            itmData.setProperty("sub_event", "0");
            itmData.setProperty("mode", "save");
            itmData.apply("In_Meeting_Program");
        }

        private void CreatePrograms2(TConfig cfg, string in_fight_day)
        {
            Item itmData = cfg.inn.newItem("In_Meeting");
            itmData.setProperty("meeting_id", cfg.meeting_id);
            itmData.setProperty("in_fight_day", in_fight_day);
            itmData.setProperty("battle_type", "WrestlingTopTwo");
            itmData.setProperty("battle_repechage", "");
            itmData.setProperty("rank_type", "SameRank");
            itmData.setProperty("surface_code", "32");
            itmData.setProperty("robin_player", "0");
            itmData.setProperty("sub_event", "0");
            itmData.setProperty("mode", "save_by_day");
            itmData.apply("In_Meeting_Program");
        }

        //清除所有比賽資料
        private void ClearAllPrograms1(TConfig cfg, Item itmReturn)
        {
            var sql = "";
            var itmSQL = default(Item);

            sql = @"
                SELECT
	                DISTINCT t1.id
                FROM
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                WHERE
	                t1.in_meeting = '{#meeting_id}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            var itmPrograms = cfg.inn.applySQL(sql);
            int count = itmPrograms.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item itmProgram = itmPrograms.getItemByIndex(i);
                string program_id = itmProgram.getProperty("id", "");

                //刪除隊伍
                sql = "DELETE FROM IN_MEETING_PTEAM WHERE source_id = '" + program_id + "'";
                cfg.inn.applySQL(sql);

                //刪除明細
                sql = @"
                    DELETE FROM 
                        IN_MEETING_PEVENT_DETAIL 
                    WHERE 
                        source_id IN
                        (
                            SELECT id FROM IN_MEETING_PEVENT WITH(NOLOCK) WHERE source_id = '{#program_id}'
                        )
                ";
                sql = sql.Replace("{#program_id}", program_id);
                cfg.inn.applySQL(sql);

                //刪除場次
                sql = @"DELETE FROM IN_MEETING_PEVENT WHERE source_id = '{#program_id}'";
                sql = sql.Replace("{#program_id}", program_id);
                cfg.inn.applySQL(sql);
            }

            //刪除無歸屬隊伍
            sql = "DELETE FROM IN_MEETING_PTEAM WHERE in_meeting = '" + cfg.meeting_id + "'";
            cfg.inn.applySQL(sql);

            //移除場地分配
            sql = "UPDATE t1 SET t1.in_program_name = t2.in_name"
                 + " FROM IN_MEETING_ALLOCATION t1 INNER JOIN IN_MEETING_PROGRAM t2 ON t2.id = t1.in_program"
                 + " WHERE t1.in_meeting = '" + cfg.meeting_id + "'";
            cfg.inn.applySQL(sql);

            //移除場地分配
            sql = "UPDATE t1 SET t1.in_program = NULL"
                 + " FROM IN_MEETING_ALLOCATION t1 INNER JOIN IN_MEETING_PROGRAM t2 ON t2.id = t1.in_program"
                 + " WHERE t1.in_meeting = '" + cfg.meeting_id + "'"
                 + " AND ISNULL(t1.in_program_name, '') <> ''";
            cfg.inn.applySQL(sql);

            //移除團體量級
            sql = "UPDATE t1 SET t1.in_program = NULL"
                 + " FROM IN_MEETING_PSECT t1 INNER JOIN IN_MEETING_PROGRAM t2 ON t2.id = t1.in_program"
                 + " WHERE t1.in_meeting = '" + cfg.meeting_id + "'"
                 + " AND ISNULL(t1.in_program_name, '') <> ''";
            cfg.inn.applySQL(sql);

            //刪除組別
            sql = "DELETE FROM IN_MEETING_PROGRAM WHERE in_meeting = '" + cfg.meeting_id + "'";
            cfg.inn.applySQL(sql);

            // //刪除場地
            // sql = "DELETE FROM IN_MEETING_SITE WHERE in_meeting = '" + meeting_id + "'";
            // itmSQL = inn.applySQL(sql);

            sql = "UPDATE IN_MEETING SET in_draw_status = NULL, in_draw_file = NULL, in_site_mode = '' WHERE id = '" + cfg.meeting_id + "'";

            cfg.inn.applySQL(sql);
        }

        //清除所有比賽資料
        private void ClearAllPrograms2(TConfig cfg, Item itmReturn)
        {
            var sql = "";
            var itmSQL = default(Item);

            sql = @"
                SELECT
	                DISTINCT t1.id
                FROM
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_USER_IMPORTNEW t2 WITH(NOLOCK)
	                ON t2.in_meeting = t1.in_meeting
	                AND t2.in_l1 = t1.in_l1
	                AND t2.in_l2 = t1.in_l2
	                AND t2.in_l3 = t1.in_l3
                WHERE
	                t1.in_meeting = '{#meeting_id}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            var itmPrograms = cfg.inn.applySQL(sql);
            int count = itmPrograms.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item itmProgram = itmPrograms.getItemByIndex(i);
                string program_id = itmProgram.getProperty("id", "");

                //刪除隊伍
                sql = "DELETE FROM IN_MEETING_PTEAM WHERE source_id = '" + program_id + "'";
                cfg.inn.applySQL(sql);


                //刪除明細
                sql = @"
                    DELETE FROM 
                        IN_MEETING_PEVENT_DETAIL 
                    WHERE 
                        source_id IN
                        (
                            SELECT id FROM IN_MEETING_PEVENT WITH(NOLOCK) WHERE source_id = '{#program_id}'
                        )
                ";
                sql = sql.Replace("{#program_id}", program_id);
                cfg.inn.applySQL(sql);

                //刪除場次
                sql = @"DELETE FROM IN_MEETING_PEVENT WHERE source_id = '{#program_id}'";
                sql = sql.Replace("{#program_id}", program_id);
                cfg.inn.applySQL(sql);

            }

            //刪除無歸屬隊伍
            sql = "DELETE FROM IN_MEETING_PTEAM WHERE in_meeting = '" + cfg.meeting_id + "' AND ISNULL(source_id, '') = ''";
            itmSQL = cfg.inn.applySQL(sql);

            //移除場地分配
            sql = "UPDATE t1 SET t1.in_program_name = t2.in_name"
                 + " FROM IN_MEETING_ALLOCATION t1 INNER JOIN IN_MEETING_PROGRAM t2 ON t2.id = t1.in_program"
                 + " WHERE t1.in_meeting = '" + cfg.meeting_id + "'";
            itmSQL = cfg.inn.applySQL(sql);

            //移除場地分配
            sql = "UPDATE t1 SET t1.in_program = NULL"
                 + " FROM IN_MEETING_ALLOCATION t1 INNER JOIN IN_MEETING_PROGRAM t2 ON t2.id = t1.in_program"
                 + " WHERE t1.in_meeting = '" + cfg.meeting_id + "'"
                 + " AND ISNULL(t1.in_program_name, '') <> ''";
            itmSQL = cfg.inn.applySQL(sql);

            //移除團體量級
            sql = "UPDATE t1 SET t1.in_program = NULL"
                 + " FROM IN_MEETING_PSECT t1 INNER JOIN IN_MEETING_PROGRAM t2 ON t2.id = t1.in_program"
                 + " WHERE t1.in_meeting = '" + cfg.meeting_id + "'"
                 + " AND ISNULL(t1.in_program_name, '') <> ''";
            cfg.inn.applySQL(sql);

        }

        //賽會參數設定
        private void RebuildMtVariable(TConfig cfg, Item itmReturn)
        {
            string need_rank57 = itmReturn.getProperty("need_rank57", "1");

            cfg.inn.applySQL("DELETE FROM In_Meeting_Variable WHERE source_id = '" + cfg.meeting_id + "'");

            RebuildMtVariable(cfg, "fight_site", "臺北和平籃球館", "100");
            RebuildMtVariable(cfg, "allocate_mode", "cycle", "200");
            RebuildMtVariable(cfg, "fight_robin_player", "5", "300");
            RebuildMtVariable(cfg, "line_color", "red", "400");
            RebuildMtVariable(cfg, "need_rank78", "0", "2100");
            RebuildMtVariable(cfg, "need_rank56", "0", "2200");
            RebuildMtVariable(cfg, "need_rank57", need_rank57, "2300");
            RebuildMtVariable(cfg, "freeze_has_draw_no", "1", "3100");
            RebuildMtVariable(cfg, "draw_report_type", "PDF", "3200");
            RebuildMtVariable(cfg, "draw_event_number", "all", "3300");
        }

        private void RebuildMtVariable(TConfig cfg, string in_key, string in_value, string sort_order)
        {
            Item itmNew = cfg.inn.newItem("In_Meeting_Variable", "add");
            itmNew.setProperty("source_id", cfg.meeting_id);
            itmNew.setProperty("in_key", in_key);
            itmNew.setProperty("in_value", in_value);
            itmNew.setProperty("sort_order", sort_order);
            itmNew.apply();
        }

        //過磅狀態設定
        private void RebuildMtWeight(TConfig cfg, Item itmReturn)
        {
            cfg.inn.applySQL("DELETE FROM In_Meeting_PWeight WHERE in_meeting = '" + cfg.meeting_id + "'");
            RebuildMtWeight(cfg, "off", "0");
            RebuildMtWeight(cfg, "leave", "1");
            RebuildMtWeight(cfg, "dq", "0");
        }

        private void RebuildMtWeight(TConfig cfg, string in_status, string in_is_remove)
        {
            Item itmNew = cfg.inn.newItem("In_Meeting_PWeight", "add");
            itmNew.setProperty("in_meeting", cfg.meeting_id);
            itmNew.setProperty("in_status", in_status);
            itmNew.setProperty("in_is_remove", in_is_remove);
            itmNew.apply();
        }

        private void Rebuild(TConfig cfg, Item itmReturn)
        {
            string aml = "<AML><Item type='In_Meeting' action='get' id='" + cfg.meeting_id + "' select='*'/></AML>";
            cfg.itmMeeting = cfg.inn.applyAML(aml);

            if (cfg.import_all_muser)
            {
                MergeMtMuser1(cfg);
            }
            else
            {
                MergeMtMuser2(cfg);
            }

            var svy_model = GetMtSvy(cfg);
            RebuildInL1(cfg, svy_model.l1_id);
            RebuildInL2(cfg, svy_model.l2_id);
            RebuildInL3(cfg, svy_model.l3_id);
        }

        private void MergeMtMuser1(TConfig cfg)
        {
            //刪除舊與會者資料
            string sql_del = "DELETE FROM IN_MEETING_USER WHERE source_id = '{#meeting_id}'";
            sql_del = sql_del.Replace("{#meeting_id}", cfg.meeting_id);
            cfg.inn.applySQL(sql_del);

            string sql = "SELECT * FROM In_Meeting_User_ImportNew WITH(NOLOCK)"
                + " WHERE in_meeting = '" + cfg.meeting_id + "' ORDER BY in_player_no";

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                //與會者資訊
                Item applicant = NewMUser(cfg, item);
                //建立與會者
                applicant.apply("add");
            }

            //重算正取人數
            cfg.itmMeeting.apply("In_Update_MeetingOnMUEdit");

            FixCityNo(cfg);
        }

        private void FixCityNo(TConfig cfg)
        {
            var rows = new List<TCityRow>();
            rows.Add(new TCityRow { name = "基隆市", code = "CT001" });
            rows.Add(new TCityRow { name = "臺北市", code = "CT002" });
            rows.Add(new TCityRow { name = "新北市", code = "CT003" });
            rows.Add(new TCityRow { name = "桃園市", code = "CT004" });
            rows.Add(new TCityRow { name = "新竹縣", code = "CT005" });
            rows.Add(new TCityRow { name = "新竹市", code = "CT006" });
            rows.Add(new TCityRow { name = "苗栗縣", code = "CT007" });
            rows.Add(new TCityRow { name = "臺中市", code = "CT008" });
            rows.Add(new TCityRow { name = "彰化縣", code = "CT009" });
            rows.Add(new TCityRow { name = "南投縣", code = "CT010" });
            rows.Add(new TCityRow { name = "雲林縣", code = "CT011" });
            rows.Add(new TCityRow { name = "嘉義縣", code = "CT012" });
            rows.Add(new TCityRow { name = "嘉義市", code = "CT013" });
            rows.Add(new TCityRow { name = "臺南市", code = "CT014" });
            rows.Add(new TCityRow { name = "高雄市", code = "CT015" });
            rows.Add(new TCityRow { name = "屏東縣", code = "CT016" });
            rows.Add(new TCityRow { name = "宜蘭縣", code = "CT017" });
            rows.Add(new TCityRow { name = "花蓮縣", code = "CT018" });
            rows.Add(new TCityRow { name = "臺東縣", code = "CT019" });
            rows.Add(new TCityRow { name = "澎湖縣", code = "CT020" });
            rows.Add(new TCityRow { name = "金門縣", code = "CT021" });
            rows.Add(new TCityRow { name = "連江縣", code = "CT022" });
            foreach (var row in rows)
            {
                FixCityNoByOne(cfg, row);
            }
        }

        private void FixCityNoByOne(TConfig cfg, TCityRow row)
        {
            var sql1 = "UPDATE IN_MEETING_USER  SET in_city_no = '" + row.code + "' WHERE  source_id = '" + cfg.meeting_id + "' AND in_current_org LIKE N'" + row.name + "%'";
            var sql2 = "UPDATE IN_MEETING_PTEAM SET in_city_no = '" + row.code + "' WHERE in_meeting = '" + cfg.meeting_id + "' AND in_current_org LIKE N'" + row.name + "%'";
            cfg.inn.applySQL(sql1);
            cfg.inn.applySQL(sql2);
        }

        private class TCityRow
        {
            public string name { get; set; }
            public string code { get; set; }
        }

        private void MergeMtMuser2(TConfig cfg)
        {
            //刪除舊與會者資料
            string sql_del = @"
                DELETE 
                    t1
                FROM
	                IN_MEETING_USER t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_USER_IMPORTNEW t2 WITH(NOLOCK)
	                ON t2.in_meeting = t1.source_id
	                AND t2.in_l1 = t1.in_l1
	                AND t2.in_l2 = t1.in_l2
	                AND t2.in_l3 = t1.in_l3
                WHERE
	                t1.source_id = '{#meeting_id}'
            ";

            sql_del = sql_del.Replace("{#meeting_id}", cfg.meeting_id);

            cfg.inn.applySQL(sql_del);

            string sql = "SELECT * FROM In_Meeting_User_ImportNew WITH(NOLOCK)"
                + " WHERE in_meeting = '" + cfg.meeting_id + "' ORDER BY in_player_no";

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                //與會者資訊
                Item applicant = NewMUser(cfg, item);
                //建立與會者
                applicant.apply("add");
            }

            //重算正取人數
            cfg.itmMeeting.apply("In_Update_MeetingOnMUEdit");
        }

        private Item NewMUser(TConfig cfg, Item item)
        {
            var applicant = cfg.inn.newItem("In_Meeting_User");

            applicant.setProperty("source_id", cfg.meeting_id);

            //所屬單位
            applicant.setProperty("in_current_org", item.getProperty("in_current_org", ""));

            //單位簡稱
            applicant.setProperty("in_short_org", item.getProperty("in_current_org", ""));

            //姓名
            applicant.setProperty("in_name", item.getProperty("in_name", ""));

            //身分證號
            //var in_sno = item.getProperty("in_sno", "");
            var in_sno = "";
            if (in_sno == "") in_sno = item.getProperty("in_player_no", "");
            applicant.setProperty("in_sno", in_sno);

            //性別
            var in_gender = item.getProperty("in_gender", "");
            if (in_gender == "") in_gender = GetGender(cfg, item);
            applicant.setProperty("in_gender", in_gender);

            //西元生日
            var in_birth = item.getProperty("in_birth", "");
            if (in_birth == "")
            {
                in_birth = "1990-01-01";
            }
            else
            {
                in_birth = GetDtm(in_birth).ToString("yyyy-MM-dd");
            }

            applicant.setProperty("in_birth", in_birth);

            //電子信箱
            applicant.setProperty("in_email", "na@na.n");

            //手機號碼
            applicant.setProperty("in_tel", "");

            //協助報名者姓名
            applicant.setProperty("in_creator", "lwu001");

            //協助報名者身分號
            applicant.setProperty("in_creator_sno", "lwu001");

            //所屬群組
            applicant.setProperty("in_group", "原創"); //F103277376測試者


            //預設身分均為選手
            applicant.setProperty("in_role", "player");

            //正取
            applicant.setProperty("in_note_state", "official");

            //組別
            applicant.setProperty("in_gameunit", item.getProperty("in_section_name", ""));

            //組名
            applicant.setProperty("in_section_name", item.getProperty("in_section_name", ""));

            //組別編號
            applicant.setProperty("in_program_no", item.getProperty("in_program_no", ""));

            //捐款金額
            applicant.setProperty("in_expense", "0");

            //序號
            Item itmIndex = MaxIndexItem(cfg, applicant);
            applicant.setProperty("in_index", item.getProperty("in_player_no", ""));

            //競賽項目
            applicant.setProperty("in_l1", item.getProperty("in_l1", ""));

            //競賽組別
            applicant.setProperty("in_l2", item.getProperty("in_l2", ""));

            //競賽分級
            applicant.setProperty("in_l3", item.getProperty("in_l3", ""));

            //非實名制
            applicant.setProperty("in_mail", item.getProperty("in_player_no", ""));

            //報名時間
            applicant.setProperty("in_regdate", System.DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss"));

            //繳費單號
            applicant.setProperty("in_paynumber", "");

            //籤號
            applicant.setProperty("in_draw_no", item.getProperty("in_draw_no", ""));

            //比賽日期
            applicant.setProperty("in_fight_day", GetFightDay(cfg, item.getProperty("in_day", "")));

            //縣市代碼
            var in_city_no = item.getProperty("in_city_no", "");
            applicant.setProperty("in_city_no", in_city_no);

            return applicant;
        }

        private string GetGender(TConfig cfg, Item item)
        {
            string in_l1 = item.getProperty("in_l1", "");
            return GetGender(cfg, in_l1);

            //string in_l2 = item.getProperty("in_l2", "");
            //return GetGender(cfg, in_l2);
        }

        private string GetGender(TConfig cfg, string value)
        {
            if (value.Contains("男")) return "男";
            if (value.Contains("女")) return "女";
            return "";
        }

        //取得該組當前最大序號
        private Item MaxIndexItem(TConfig cfg, Item applicant)
        {
            string sql = "";

            string in_l1 = applicant.getProperty("in_l1", "");
            string in_l2 = applicant.getProperty("in_l2", "");
            string in_l3 = applicant.getProperty("in_l3", "");
            string in_l4 = applicant.getProperty("in_l4", "");

            sql = "SELECT MAX(in_index) AS 'max_idx' FROM In_Meeting_User WITH(NOLOCK) WHERE source_id = '" + cfg.meeting_id + "'"
                + " AND in_l1 = N'" + in_l1 + "'";

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql_index: " + sql);

            Item itmResult = cfg.inn.applySQL(sql);

            if (itmResult.isError() || itmResult.getResult() == "")
            {
                itmResult = cfg.inn.newItem();
                itmResult.setProperty("new_idx", "00001");
            }
            else
            {
                var max_idx = itmResult.getProperty("max_idx", "1");
                var new_idx = GetInt(max_idx) + 1;
                applicant.setProperty("new_idx", new_idx.ToString("00000"));

            }

            return itmResult;
        }

        private void RebuildInL1(TConfig cfg, string id)
        {
            cfg.inn.applySQL("DELETE FROM IN_SURVEY_OPTION WHERE source_id = '" + id + "'");

            string sql = @"
                SELECT DISTINCT
                    in_l1
                FROM 
                    IN_MEETING_USER WITH(NOLOCK) 
                WHERE 
                    source_id = '{#meeting_id}'
                ORDER BY
                    in_l1
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();

            string in_selectoption = "";

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_l1 = item.getProperty("in_l1", "");

                Item itmNew = cfg.inn.newItem("In_Survey_Option", "merge");
                itmNew.setAttribute("where", "source_id='" + id + "' AND in_value=N'" + in_l1 + "'");
                itmNew.setProperty("source_id", id);
                itmNew.setProperty("in_label", in_l1);
                itmNew.setProperty("in_value", in_l1);
                itmNew.setProperty("sort_order", GetL1SortOrder(in_l1).ToString());
                itmNew = itmNew.apply();

                in_selectoption += "@" + in_l1;
            }

            cfg.inn.applySQL("UPDATE IN_SURVEY SET in_selectoption = N'" + in_selectoption + "' WHERE id = '" + id + "'");
        }

        private int GetL1SortOrder(string in_l1)
        {
            switch (in_l1)
            {
                case "國男組": return 1000;
                case "國中男生組": return 1000;
                case "國中男子組": return 1000;
                case "國中組男生部": return 1000;

                case "國女組": return 2000;
                case "國中女生組": return 2000;
                case "國中女子組": return 2000;
                case "國中組女生部": return 2000;

                case "高男組": return 3000;
                case "高中男生組": return 3000;
                case "高中男子組": return 3000;
                case "高中組男生部": return 3000;

                case "高女組": return 4000;
                case "高中女生組": return 4000;
                case "高中女子組": return 4000;
                case "高中組女生部": return 4000;

                case "公開男生組": return 5000;
                case "公開男子組": return 5000;

                case "公開女生組": return 6000;
                case "公開女子組": return 6000;

                case "一般男生組": return 7000;
                case "一般男子組": return 7000;

                case "一般女生組": return 8000;
                case "一般女子組": return 8000;

                default: return 0;
            }
        }

        private void RebuildInL2(TConfig cfg, string id)
        {
            cfg.inn.applySQL("DELETE FROM IN_SURVEY_OPTION WHERE source_id = '" + id + "'");

            string sql = @"
                SELECT DISTINCT
                    in_l1
                    , in_l2
                FROM 
                    IN_MEETING_USER WITH(NOLOCK) 
                WHERE 
                    source_id = '{#meeting_id}'
                ORDER BY
                    in_l1
                    , in_l2
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_l1 = item.getProperty("in_l1", "");
                string in_l2 = item.getProperty("in_l2", "");
                int sort_order = (i + 1) * 100;

                Item itmNew = cfg.inn.newItem("In_Survey_Option", "merge");
                itmNew.setAttribute("where", "source_id='" + id + "' AND in_filter = N'" + in_l1 + "' AND in_value = N'" + in_l2 + "'");
                itmNew.setProperty("source_id", id);
                itmNew.setProperty("in_filter", in_l1);
                itmNew.setProperty("in_label", in_l2);
                itmNew.setProperty("in_value", in_l2);
                itmNew = itmNew.apply();
            }
        }

        private void RebuildInL3(TConfig cfg, string id)
        {
            cfg.inn.applySQL("DELETE FROM IN_SURVEY_OPTION WHERE source_id = '" + id + "'");

            string sql = @"
                SELECT DISTINCT
                    in_program_no
                    , in_l1
                    , in_l2
                    , in_l3
                    , in_section_name
                FROM 
                    IN_MEETING_USER WITH(NOLOCK) 
                WHERE 
                    source_id = '{#meeting_id}'
                ORDER BY
                    in_program_no
                    , in_l1
                    , in_l2
                    , in_l3
                    , in_section_name
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);
            Item items = cfg.inn.applySQL(sql);
            int count = items.getItemCount();

            int weight_idx = 0;

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_l1 = item.getProperty("in_l1", "");
                string in_l2 = item.getProperty("in_l2", "");
                string in_l3 = item.getProperty("in_l3", "");
                string in_section_name = item.getProperty("in_section_name", "");

                var itmShortName = cfg.inn.applyMethod("In_Meeting_Program_ShortName"
                    , "<in_l1>" + in_l1 + "</in_l1>"
                    + "<in_l2>" + in_l2 + "</in_l2>"
                    + "<in_l3>" + in_l3 + "</in_l3>"
                    + "<in_section_name>" + in_section_name + "</in_section_name>"
                    );

                var in_n1 = itmShortName.getProperty("result", "");
                var in_weight = itmShortName.getProperty("weight", "");

                Item itmNew = cfg.inn.newItem("In_Survey_Option", "merge");
                itmNew.setAttribute("where", "source_id='" + id + "' AND in_grand_filter = N'" + in_l1 + "' AND in_filter = N'" + in_l2 + "' AND in_value = N'" + in_l3 + "'");
                itmNew.setProperty("source_id", id);
                itmNew.setProperty("in_grand_filter", in_l1);
                itmNew.setProperty("in_filter", in_l2);
                itmNew.setProperty("in_label", in_l3);
                itmNew.setProperty("in_value", in_l3);
                itmNew.setProperty("in_weight", in_weight);
                itmNew.setProperty("in_n1", in_n1);
                itmNew = itmNew.apply();

                if (in_l3.Contains("以上"))
                {
                    weight_idx = 0;
                }
                else
                {
                    weight_idx++;
                }
            }
        }

        private TMtSvy GetMtSvy(TConfig cfg)
        {
            TMtSvy result = new TMtSvy { l1_id = "", l2_id = "", l3_id = "" };

            string sql = @"
                SELECT
	                t2.id
	                , t2.in_property
	                , t2.in_questions
                FROM
	                IN_MEETING_SURVEYS t1 WITH(NOLOCK)
                INNER JOIN
	                IN_SURVEY t2 WITH(NOLOCK)
	                ON t2.id = t1.related_id
                WHERE
	                t1.source_id = '{#meeting_id}'
	                AND t2.in_property IN ('in_l1', 'in_l2', 'in_l3')
                ORDER BY
	                t2.in_property 
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string id = item.getProperty("id", "");
                string in_property = item.getProperty("in_property", "");
                switch (in_property)
                {
                    case "in_l1": result.l1_id = id; break;
                    case "in_l2": result.l2_id = id; break;
                    case "in_l3": result.l3_id = id; break;
                }
            }
            return result;
        }

        //匯入與會者
        private void Import(TConfig cfg, Item itmReturn)
        {
            var sql = "UPDATE IN_MEETING SET"
                + "  in_battle_mode = '" + cfg.in_battle_mode + "'"
                + ", in_battle_repechage = '" + cfg.in_battle_repechage + "'"
                + " WHERE id = '" + cfg.meeting_id + "'";
            cfg.inn.applySQL(sql);

            //重建場地資料
            RebuildMeetingSites(cfg, itmReturn.getProperty("site_count", "0"));

            var value = itmReturn.getProperty("value", "");
            if (value == "") throw new Exception("無匯入資料");

            var list = MapList(cfg, value);
            if (list == null || list.Count == 0) throw new Exception("無匯入資料");

            //刪除資料
            cfg.inn.applySQL("DELETE FROM In_Meeting_User_ImportNew");

            for (var i = 0; i < list.Count; i++)
            {
                var row = list[i];
                if (row.c12 == null) row.c12 = "";
                if (row.c13 == null) row.c13 = "";
                if (row.c14 == null) row.c14 = "";
                if (row.c15 == null) row.c15 = "";
                if (row.c16 == null) row.c16 = "";
                if (row.c17 == null) row.c17 = "";
                if (row.c18 == null) row.c18 = "";

                Item itmNew = cfg.inn.newItem("In_Meeting_User_ImportNew", "add");
                itmNew.setProperty("in_no", row.c00);
                itmNew.setProperty("in_meeting", cfg.meeting_id);
                itmNew.setProperty("in_l1", row.c06);
                itmNew.setProperty("in_l2", row.c04);
                itmNew.setProperty("in_l3", row.c05);
                itmNew.setProperty("in_section_name", row.c03);
                itmNew.setProperty("in_program_no", row.c02);

                itmNew.setProperty("in_player_no", row.c07);
                itmNew.setProperty("in_current_org", row.c08);
                itmNew.setProperty("in_name", row.c09);
                itmNew.setProperty("in_draw_no", row.c10);
                itmNew.setProperty("in_day", row.c11);

                itmNew.setProperty("in_gender", row.c14);
                itmNew.setProperty("in_sno", row.c15);
                itmNew.setProperty("in_birth", row.c16.Replace("/", "-"));
                itmNew.setProperty("in_city_code", row.c17);
                itmNew.setProperty("in_city", row.c18);
                itmNew.apply();
            }
        }

        private void MatchSiteCode(TConfig cfg, Item itmReturn)
        {
            var value = itmReturn.getProperty("sites", "");
            if (value == "") return;

            var list = MapList(cfg, value);
            if (list == null || list.Count == 0) return;
            for (var i = 0; i < list.Count; i++)
            {
                var row = list[i];
                var sql = "UPDATE In_Meeting_User_ImportNew SET in_site_code = '" + row.c13 + "' WHERE in_program_no = '" + row.c02 + "'";
                cfg.inn.applySQL(sql);
            }
        }

        //重建場地資料
        private void RebuildMeetingSites(TConfig cfg, string value)
        {
            if (cfg.need_rebuild_meeting != "1") return;

            var max = GetInt(value);
            if (max <= 0) throw new Exception("場地數量錯誤");

            //清除所有場地資料
            Item itmData = cfg.inn.newItem();
            itmData.setType("In_Meeting");
            itmData.setProperty("meeting_id", cfg.meeting_id);
            itmData.setProperty("mode", "remove");
            itmData.apply("in_meeting_site");

            for (var i = 1; i <= max; i++)
            {
                var code = i.ToString();
                var row = SiteCodeRow(i);

                var itmNew = cfg.inn.newItem("In_Meeting_Site", "add");
                itmNew.setProperty("in_meeting", cfg.meeting_id);
                itmNew.setProperty("in_code", code);
                itmNew.setProperty("in_code_en", row.w1);
                itmNew.setProperty("in_name", "第" + row.w2 + "場地");
                itmNew.setProperty("in_rows", "1");
                itmNew.setProperty("in_cols", value);
                itmNew.setProperty("in_row_index", "0");
                itmNew.setProperty("in_col_index", (i - 1).ToString());
                itmNew.apply();
            }
        }

        private TSP SiteCodeRow(int code)
        {
            switch (code)
            {
                case 1: return new TSP { w1 = "A", w2 = "一" };
                case 2: return new TSP { w1 = "B", w2 = "二" };
                case 3: return new TSP { w1 = "C", w2 = "三" };
                case 4: return new TSP { w1 = "D", w2 = "四" };
                case 5: return new TSP { w1 = "E", w2 = "五" };
                case 6: return new TSP { w1 = "F", w2 = "六" };
                case 7: return new TSP { w1 = "G", w2 = "七" };
                case 8: return new TSP { w1 = "H", w2 = "八" };
                case 9: return new TSP { w1 = "I", w2 = "九" };
                case 10: return new TSP { w1 = "J", w2 = "十" };
                case 11: return new TSP { w1 = "K", w2 = "十一" };
                case 12: return new TSP { w1 = "L", w2 = "十二" };
                default: return new TSP { w1 = "ERR", w2 = "ERR" };
            }
        }

        private class TSP
        {
            public string w1 { get; set; }
            public string w2 { get; set; }
        }

        private List<TRow> MapList(TConfig cfg, string value)
        {
            try
            {
                var list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<TRow>>(value);
                return list;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private class TRow
        {
            public string c00 { get; set; }
            public string c01 { get; set; }
            public string c02 { get; set; }
            public string c03 { get; set; }
            public string c04 { get; set; }
            public string c05 { get; set; }
            public string c06 { get; set; }
            public string c07 { get; set; }
            public string c08 { get; set; }
            public string c09 { get; set; }
            public string c10 { get; set; }
            public string c11 { get; set; }
            public string c12 { get; set; }
            public string c13 { get; set; }

            public string c14 { get; set; }
            public string c15 { get; set; }
            public string c16 { get; set; }
            public string c17 { get; set; }
            public string c18 { get; set; }
        }

        private class TMtSvy
        {
            public string l1_id { get; set; }
            public string l2_id { get; set; }
            public string l3_id { get; set; }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }

            public string meeting_id { get; set; }
            public string need_rebuild_meeting { get; set; }
            public string in_battle_mode { get; set; }
            public string in_battle_repechage { get; set; }
            public string scene { get; set; }
            public bool import_all_muser { get; set; }

            public Item itmMeeting { get; set; }
        }

        private string GetFightDay(TConfig cfg, string value)
        {
            string code = value;
            if (code.Length <= 4)
            {
                code = DateTime.Now.Year + code.PadLeft(4, '0');
            }
            if (code.Length != 8)
            {
                return "";
            }

            string y = code.Substring(0, 4);
            string m = code.Substring(4, 2);
            string d = code.Substring(6, 2);
            return y + "-" + m + "-" + d;
        }

        private int GetInt(string value, int def = 0)
        {
            int result = def;
            int.TryParse(value, out result);
            return result;
        }

        private DateTime GetDtm(string value)
        {
            if (value == "") return DateTime.MinValue;
            var result = DateTime.MinValue;
            if (DateTime.TryParse(value, out result))
            {
                return result;
            }
            return DateTime.MinValue;
        }
    }
}