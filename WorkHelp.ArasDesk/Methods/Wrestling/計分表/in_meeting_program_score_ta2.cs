﻿using Aras.IOM;
using System;
using System.Collections.Generic;
using Xceed.Document.NET;

namespace WorkHelp.ArasDesk.Methods.Wresling.Fight
{
    public class in_meeting_program_score_ta2 : Item
    {
        public in_meeting_program_score_ta2(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 角力 TA 表
                日誌: 
                    - 2024-08-27: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "in_meeting_program_score_ta";

            var itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                meeting_id = itmR.getProperty("meeting_id", ""),
                program_id = itmR.getProperty("program_id", ""),
                event_id = itmR.getProperty("event_id", ""),
                site_code = itmR.getProperty("site_code", ""),
                min_number = itmR.getProperty("min_number", ""),
                max_number = itmR.getProperty("max_number", ""),
                export_type = itmR.getProperty("export_type", ""),
                scene = itmR.getProperty("scene", ""),
            };

            switch (cfg.scene)
            {
                case "download":
                    Download(cfg, itmR);
                    break;
                case "download2":
                    Download2(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void Download2(TConfig cfg, Item itmReturn)
        {
            var in_fight_id = itmReturn.getProperty("in_fight_id", "");
            var sql = "SELECT id FROM IN_MEETING_PEVENT WITH(NOLOCK) WHERE source_id = '" + cfg.program_id + "' AND in_fight_id = '" + in_fight_id + "'";

            var itmEvent = cfg.inn.applySQL(sql);
            if (itmEvent.isError() || itmEvent.getResult() == "") throw new Exception("查無場次資料");

            var event_id = itmEvent.getProperty("id", "");
            cfg.event_id = event_id;
            itmReturn.setProperty("event_id", event_id);

            Download(cfg, itmReturn);
        }

        private void Download(TConfig cfg, Item itmReturn)
        {
            var reports = MapRows(cfg);

            //lina: 因為 TA 上有放活動的圖片，所以每換活動要換 Word 樣板
            cfg.itmMeeting = cfg.inn.applySQL("SELECT in_title, item_number FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            cfg.mt_title = cfg.itmMeeting.getProperty("in_title", "");
            cfg.mt_number = cfg.itmMeeting.getProperty("item_number", "");

            var xls_parm_name = "fight_ta_excel";
            cfg.itmXlsx = cfg.inn.applyMethod("In_Variable_File", "<in_name>" + xls_parm_name + "</in_name>");
            cfg.template_path = cfg.itmXlsx.getProperty("template_path", "");
            cfg.export_path = cfg.itmXlsx.getProperty("export_path", "");

            var itmSitePath = cfg.inn.applySQL("SELECT TOP 1 in_value FROM IN_VARIABLE WITH(NOLOCK) WHERE in_name = 'site_path'");
            var site_path = itmSitePath.getProperty("in_value", "");
            if (site_path != "") site_path = site_path.Trim('\\');

            cfg.meeting_full_file = site_path + @"\pages\excelIO\images\"+ cfg.mt_number + ".png";
            cfg.manager_full_file = site_path + @"\pages\excelIO\images\logo-with-word-1.png";
            if(System.IO.File.Exists(cfg.meeting_full_file))
            {
                cfg.has_meeting_picture = true;
            } 
            else if (System.IO.File.Exists(cfg.manager_full_file))
            {
                cfg.has_manager_picture = true;
            }

            var book = new Spire.Xls.Workbook();
            book.LoadFromFile(cfg.template_path);

            var sheetTemplate = book.Worksheets[0];
            for (var i = 0; i < reports.Count; i++)
            {
                var report = reports[i];
                SetReportData(cfg, book, sheetTemplate, report);
            }

            if (book.Worksheets.Count > 1)
            {
                sheetTemplate.Remove();
                book.Worksheets[0].Activate();
            }

            var rpt = reports.Count == 0 
                ? new TRpt { day = "", siteName = "", number = "" }
                : reports[0];

            //匯出檔名
            var xlsName = cfg.mt_title
                + "_角力計分表"
                + "_" + rpt.day
                + "_" + rpt.siteName
                + "_" + rpt.number
                + "(" + System.DateTime.Now.ToString("HHmmss") + ")";

            xlsName = xlsName.Replace("+", "")
                            .Replace("-", "")
                            .Replace(".", "")
                            .Replace(" ", "");

            string extName = cfg.export_type != "" && cfg.export_type.ToLower() == "pdf" ? ".pdf" : ".xlsx";
            string xls_file = cfg.export_path + xlsName + extName;
            string xls_name = xlsName + extName;

            if (!System.IO.Directory.Exists(cfg.export_path))
            {
                System.IO.Directory.CreateDirectory(cfg.export_path);
            }

            //儲存檔案          
            if (extName == ".pdf")
            {
                book.SaveToFile(xls_file, Spire.Xls.FileFormat.PDF);
            }
            else
            {
                book.SaveToFile(xls_file, Spire.Xls.ExcelVersion.Version2013);
            }

            //下載路徑
            itmReturn.setProperty("xls_name", xls_name);
        }

        private void SetReportData(TConfig cfg, Spire.Xls.Workbook book, Spire.Xls.Worksheet sheetTemplate, TRpt report)
        {
            Spire.Xls.Worksheet sheet = book.CreateEmptySheet();
            sheet.CopyFrom(sheetTemplate);
            sheet.Name = report.siteName + report.number;

            // sheet.PageSetup.TopMargin = 0.1;
            // sheet.PageSetup.LeftMargin = 0.1;
            // sheet.PageSetup.RightMargin = 0.1;
            // sheet.PageSetup.BottomMargin = 0.1;

            var f1 = report.foot1;
            var f2 = report.foot2;

            SetCell(cfg, sheet, "A6", report.siteName);
            SetCell(cfg, sheet, "A2", report.number);
            SetCell(cfg, sheet, "Q9", report.section);

            SetCell(cfg, sheet, "C15", f1.name);
            SetCell(cfg, sheet, "Q15", f1.org);
            SetCell(cfg, sheet, "AK15", f1.playerNo);

            SetCell(cfg, sheet, "AY15", f2.name);
            SetCell(cfg, sheet, "BM15", f2.org);
            SetCell(cfg, sheet, "CG15", f2.playerNo);

            if (cfg.has_meeting_picture)
            {
                var picture = sheet.Pictures.Add(2, 28, cfg.meeting_full_file);
                picture.Width = 280;
                picture.Height = 100;
                picture.LeftColumnOffset = 0;
                picture.TopRowOffset = 0;
            }
            else if (cfg.has_manager_picture)
            {
                var picture = sheet.Pictures.Add(1, 28, cfg.manager_full_file);
                picture.Width = 280;
                picture.Height = 57;
                picture.LeftColumnOffset = 0;
                picture.TopRowOffset = 0;
                SetCell(cfg, sheet, "AC5", cfg.mt_title);
            }

            //適合頁面
            book.ConverterSetting.SheetFitToPage = true;
        }

        private void SetCell(TConfig cfg, Spire.Xls.Worksheet sheet, string pos, string value)
        {
            sheet.Range[pos].Text = value;
        }

        private List<TRpt> MapRows(TConfig cfg)
        {
            var reports = new List<TRpt>();

            var items = GetMeetingEventFoots(cfg);
            var count = items.getItemCount();

            for (var i = 0; i < count; i = i + 2)
            {
                var itmF1 = items.getItemByIndex(i);
                var itmF2 = items.getItemByIndex(i + 1);

                var report = new TRpt
                {
                    day = itmF1.getProperty("in_fight_day", "").Replace("-", ""),
                    siteCode = itmF1.getProperty("site_code", ""),
                    number = itmF1.getProperty("in_tree_no", ""),
                    staff1 = itmF1.getProperty("in_staff_1", ""),
                    staff2 = itmF1.getProperty("in_staff_2", ""),
                    staff3 = itmF1.getProperty("in_staff_3", ""),
                    staff4 = itmF1.getProperty("in_staff_4", ""),
                    section = GetSectionName(itmF1),
                    foot1 = GetPlayer(itmF1),
                    foot2 = GetPlayer(itmF2),
                };

                report.siteName = GetSiteEnName(report.siteCode);
                reports.Add(report);
            }

            return reports;
        }

        private TRptPlayer GetPlayer(Item item)
        {
            return new TRptPlayer
            {
                foot = item.getProperty("in_sign_foot", ""),
                org = item.getProperty("in_short_org", ""),
                name = item.getProperty("in_name", ""),
                drawNo = item.getProperty("in_sign_no", ""),
                playerNo = item.getProperty("in_player_no", ""),
                team = item.getProperty("in_team", ""),
            };
        }

        private string GetSectionName(Item item)
        {
            var in_l1 = item.getProperty("in_l1", "");
            var in_l2 = item.getProperty("in_l2", "");
            var in_l3 = item.getProperty("in_l3", "");
            var pg_name3 = item.getProperty("pg_name3", "");
            var in_course = item.getProperty("in_course", "");

            if (in_l1 == "個人組")
            {
                return pg_name3.Replace("個人組", "").Replace("-", "") + " " + in_course;
            }
            if (in_l1.Length > 3)
            {
                return pg_name3.Replace("-", "") + " " + in_course;
            }
            return pg_name3.Replace("-", " ") + " " + in_course;
        }

        private string GetRankName(Item item)
        {
            var team_count = GetInt(item.getProperty("in_team_count", "0"));
            if (team_count <= 5) return "循環賽";
            if (team_count == 6) return GetRankNamePlayer6(item);
            if (team_count == 7) return GetRankNamePlayer7(item);
            return GetRankNamePlayer8(item);
        }

        private string GetRankNamePlayer6(Item item)
        {
            var in_fight_id = item.getProperty("in_fight_id", "");

            switch (in_fight_id)
            {
                case "CRS6-01":
                case "CRS6-02":
                case "CRS6-03":
                    return "循環賽";

                case "CRS6-04":
                case "CRS6-05":
                case "CRS6-06":
                    return "循環賽";

                case "M004-01": return "銅牌戰";//"準決賽";
                case "M004-02": return "銅牌戰";//"準決賽";
                case "M002-01": return "金牌戰";//"決賽";
                case "RNK34-01": return "三四名";
                default: return "";
            }
        }

        private string GetRankNamePlayer7(Item item)
        {
            var in_fight_id = item.getProperty("in_fight_id", "");

            switch (in_fight_id)
            {
                case "CRS7-01":
                case "CRS7-02":
                case "CRS7-03":
                case "CRS7-04":
                case "CRS7-05":
                case "CRS7-06":
                    return "循環賽";

                case "CRS7-07":
                case "CRS7-08":
                case "CRS7-09":
                    return "循環賽";

                case "M004-01": return "銅牌戰";//"準決賽";
                case "M004-02": return "銅牌戰";//"準決賽";
                case "M002-01": return "金牌戰";//"決賽";
                case "RNK34-01": return "三四名";
                case "RNK56-01": return "五六名";
                default: return "";
            }
        }

        private string GetRankNamePlayer8(Item item)
        {
            var in_fight_id = item.getProperty("in_fight_id", "");
            var in_tree_rank = item.getProperty("in_tree_rank", "");

            switch (in_fight_id)
            {
                case "M004-01": return "準決賽";
                case "M004-02": return "準決賽";
                case "M002-01": return "金牌戰";
                case "RNK34-01": return "三四名";
                case "RNK56-01": return "五六名";
                case "RNK78-01": return "七八名";
            }

            switch (in_tree_rank)
            {
                case "rank35": return "銅牌戰";
            }

            var c = in_fight_id[0];
            if (c == 'M') return "預賽";
            if (c == 'R') return "複賽";

            return "";
        }

        private string GetSiteEnName(string code)
        {
            switch (code)
            {
                case "1": return "A";
                case "2": return "B";
                case "3": return "C";
                case "4": return "D";
                case "5": return "E";
                case "6": return "F";
                case "7": return "G";
                case "8": return "H";
                default: return "A";
            }
        }

        private Item GetMeetingEventFoots(TConfig cfg)
        {
            if (cfg.site_code != "" && cfg.min_number != "" && cfg.max_number != "")
            {
                return GetMeetingRangeEventFoots(cfg);
            }
            else
            {
                return GetMeetingOneEventFoots(cfg);
            }
        }
        private Item GetMeetingRangeEventFoots(TConfig cfg)
        {
            var sql = @"
                SELECT 
                    t1.in_name3 AS 'pg_name3'
	                , t1.in_l1
	                , t1.in_l2
	                , t1.in_l3
	                , t1.in_fight_site
	                , t1.in_fight_day
	                , t1.in_fight_time
	                , t1.in_short_name
	                , t1.in_program_no
	                , t1.in_weight
	                , t1.in_team_count
	                , t2.in_tree_name
	                , t2.in_fight_id
	                , t2.in_tree_no
	                , t2.in_tree_rank
	                , t2.in_win_status
	                , t2.in_win_sign_no
	                , t2.in_course
	                , t2.in_we
	                , t2.in_staff_1
	                , t2.in_staff_2
	                , t2.in_staff_3
	                , t2.in_staff_4
	                , t3.in_sign_foot
	                , t3.in_sign_no
	                , t4.in_code AS 'site_code'
	                , t4.in_name AS 'site_name'
	                , t11.in_current_org
	                , t11.in_short_org
	                , t11.in_name
	                , t11.in_names
	                , t11.in_team
	                , t11.in_index
	                , t11.in_player_no
	                , t11.map_short_org
                FROM 
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PEVENT t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t3 WITH(NOLOCK)
	                ON t3.source_id = t2.id
                INNER JOIN
	                IN_MEETING_SITE t4 WITH(NOLOCK)
	                ON t4.id = t2.in_site
                LEFT OUTER JOIN
	                VU_MEETING_PTEAM t11 WITH(NOLOCK)
	                ON t11.source_id = t2.source_id
	                AND ISNULL(t11.in_sign_no, '') NOT IN ('', '0')
	                AND t11.in_sign_no = t3.in_sign_no
                WHERE
	                t2.in_site_code = '{#site_code}'
                    AND t2.in_tree_no >= {min}
                    AND t2.in_tree_no <= {max}
                ORDER BY
                    t2.in_tree_no
                    , t2.id
	                , t3.in_sign_foot
            ";

            sql = sql.Replace("{#site_code}", cfg.site_code)
                .Replace("{#min}", cfg.min_number)
                .Replace("{#max}", cfg.max_number); 

            return cfg.inn.applySQL(sql);
        }

        private Item GetMeetingOneEventFoots(TConfig cfg)
        {
            var sql = @"
                SELECT 
                    t1.in_name3 AS 'pg_name3'
	                , t1.in_l1
	                , t1.in_l2
	                , t1.in_l3
	                , t1.in_fight_site
	                , t1.in_fight_day
	                , t1.in_fight_time
	                , t1.in_short_name
	                , t1.in_program_no
	                , t1.in_weight
	                , t1.in_team_count
	                , t2.in_tree_name
	                , t2.in_fight_id
	                , t2.in_tree_no
	                , t2.in_tree_rank
	                , t2.in_win_status
	                , t2.in_win_sign_no
	                , t2.in_course
	                , t2.in_we
	                , t2.in_staff_1
	                , t2.in_staff_2
	                , t2.in_staff_3
	                , t2.in_staff_4
	                , t3.in_sign_foot
	                , t3.in_sign_no
	                , t4.in_code AS 'site_code'
	                , t4.in_name AS 'site_name'
	                , t11.in_current_org
	                , t11.in_short_org
	                , t11.in_name
	                , t11.in_names
	                , t11.in_team
	                , t11.in_index
	                , t11.in_player_no
	                , t11.map_short_org
                FROM 
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PEVENT t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t3 WITH(NOLOCK)
	                ON t3.source_id = t2.id
                INNER JOIN
	                IN_MEETING_SITE t4 WITH(NOLOCK)
	                ON t4.id = t2.in_site
                LEFT OUTER JOIN
	                VU_MEETING_PTEAM t11 WITH(NOLOCK)
	                ON t11.source_id = t2.source_id
	                AND ISNULL(t11.in_sign_no, '') NOT IN ('', '0')
	                AND t11.in_sign_no = t3.in_sign_no
                WHERE
	                t2.id = '{#event_id}'
                ORDER BY
	                t3.in_sign_foot
            ";

            sql = sql.Replace("{#event_id}", cfg.event_id);

            return cfg.inn.applySQL(sql);
        }

        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string meeting_id { get; set; }
            public string program_id { get; set; }
            public string event_id { get; set; }
            public string export_type { get; set; }
            public string scene { get; set; }
            public Item itmMeeting { get; set; }
            public Item itmProgram { get; set; }
            public Item itmXlsx { get; set; }
            public string mt_title { get; set; }
            public string mt_number { get; set; }
            public string template_path { get; set; }
            public string export_path { get; set; }
            
            public string meeting_full_file { get; set; }
            public string manager_full_file { get; set; }
            public bool has_meeting_picture { get; set; }
            public bool has_manager_picture { get; set; }

            public Dictionary<string, string> map { get; set; }

            public string site_code { get; set; }
            public string min_number { get; set; }
            public string max_number { get; set; }
        }


        private class TRpt
        {
            public string day { get; set; }
            public string siteCode { get; set; }
            public string siteName { get; set; }
            public string number { get; set; }
            public string section { get; set; }
            public string staff1 { get; set; }
            public string staff2 { get; set; }
            public string staff3 { get; set; }
            public string staff4 { get; set; }
            public TRptPlayer foot1 { get; set; }
            public TRptPlayer foot2 { get; set; }
        }

        private class TRptPlayer
        {
            public string org { get; set; }
            public string name { get; set; }
            public string foot { get; set; }
            public string drawNo { get; set; }
            public string playerNo { get; set; }
            public string team { get; set; }
        }

        private int GetInt(string value, int def = 0)
        {
            int result = def;
            int.TryParse(value, out result);
            return result;
        }
    }
}