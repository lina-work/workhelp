﻿using Aras.IOM;
using System;
using System.Linq;
using System.Text;

namespace WorkHelp.ArasDesk.Methods.PLMCTA.Common
{
    public class in_meeting_program_score_ta : Item
    {
        public in_meeting_program_score_ta(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 角力 TA 表
                日誌: 
                    - 2024-03-21: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "in_meeting_program_score_ta";

            var itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                meeting_id = itmR.getProperty("meeting_id", ""),
                program_id = itmR.getProperty("program_id", ""),
                event_id = itmR.getProperty("event_id", ""),
                scene = itmR.getProperty("scene", ""),
            };

            switch (cfg.scene)
            {
                case "download":
                    Download(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void Download(TConfig cfg, Item itmReturn)
        {
            var report = MapRow(cfg);

            //lina: 因為 TA 上有放活動的圖片，所以每換活動要換 Word 樣板
            cfg.itmMeeting = cfg.inn.applySQL("SELECT in_title FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            cfg.mt_title = cfg.itmMeeting.getProperty("in_title", "");

            var xls_parm_name = "fight_ta_" + report.siteName.ToLower();
            cfg.itmXlsx = cfg.inn.applyMethod("In_Variable_File", "<in_name>" + xls_parm_name + "</in_name>");
            cfg.template_path = cfg.itmXlsx.getProperty("template_path", "");
            cfg.export_path = cfg.itmXlsx.getProperty("export_path", "");

            //載入模板
            var doc = Xceed.Words.NET.DocX.Load(cfg.template_path);

            //填入資料
            SetReportData(cfg, doc, report);

            //匯出檔名
            var xlsName = cfg.mt_title
                + "_角力計分表"
                + "_" + report.day
                + "_" + report.siteName
                + "_" + report.number
                + "(" + System.DateTime.Now.ToString("HHmmss") + ")";

            xlsName = xlsName.Replace("+", "")
                            .Replace("-", "")
                            .Replace(".", "")
                            .Replace(" ", "");

            string extName = ".docx";
            string xls_file = cfg.export_path + xlsName + extName;
            string xls_name = xlsName + extName;

            if (!System.IO.Directory.Exists(cfg.export_path)) System.IO.Directory.CreateDirectory(cfg.export_path);
      
            //儲存檔案
            doc.SaveAs(xls_file);

            //下載路徑
            itmReturn.setProperty("xls_name", xls_name);
        }

        private void SetReportData(TConfig cfg, Xceed.Words.NET.DocX doc, TRpt report)
        {
            //以下 Cell 位置從分析 Word 得出
            //場次編號
            var cellNumber = doc.Tables[0].Rows[0].Cells[0].Paragraphs.First();

            //式別/量級/賽別
            var cellSect = doc.Tables[4].Rows[0].Cells[0].Paragraphs.First();

            //紅方
            var cellF1Name = doc.Tables[8].Rows[2].Cells[0].Paragraphs.First();
            var cellF1Org = doc.Tables[8].Rows[2].Cells[1].Paragraphs.First();
            var cellF1Number = doc.Tables[8].Rows[2].Cells[2].Paragraphs.First();
            //藍方
            var cellF2Name = doc.Tables[8].Rows[2].Cells[4].Paragraphs.First();
            var cellF2Org = doc.Tables[8].Rows[2].Cells[5].Paragraphs.First();
            var cellF2Number = doc.Tables[8].Rows[2].Cells[6].Paragraphs.First();

            var fontName = "標楷體";
            cellNumber.Append(report.number).Font(fontName);
            cellSect.Append(report.section).Font(fontName);

            cellF1Name.Append(report.foot1.name).Font(fontName);
            cellF1Org.Append(report.foot1.org).Font(fontName);
            cellF1Number.Append(report.foot1.playerNo).Font(fontName);

            cellF2Name.Append(report.foot2.name).Font(fontName);
            cellF2Org.Append(report.foot2.org).Font(fontName);
            cellF2Number.Append(report.foot2.playerNo).Font(fontName);
        }

        static void LoadDocPosition(TConfig cfg, Xceed.Words.NET.DocX doc)
        {
            var content = new StringBuilder();

            var tableCount = doc.Tables.Count;
            for (var i = 0; i < tableCount; i++)
            {
                var table = doc.Tables[i];
                var rows = table.Rows;
                for (var j = 0; j < rows.Count; j++)
                {
                    var row = rows[j];
                    var cells = row.Cells;
                    for (var k = 0; k < cells.Count; k++)
                    {
                        var cell = cells[k];
                        foreach (var p in cell.Paragraphs)
                        {
                            string text = p.Text;
                            if (string.IsNullOrWhiteSpace(text)) continue;
                            content.AppendLine("Table[" + i + "] Rows[" + j + "] Cells[" + k + "] = " + text);
                        }
                    }
                }
            }

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, content.ToString());
        }

        private TRpt MapRow(TConfig cfg)
        {
            var items = GetMeetingEventFoots(cfg);
            var count = items.getItemCount();
            if (items.isError() || items.getResult() == "" || count != 2)
            {
                throw new Exception("場次明細資料錯誤");
            }

            var itmF1 = items.getItemByIndex(0);
            var itmF2 = items.getItemByIndex(1);

            var report = new TRpt
            {
                day = itmF1.getProperty("in_fight_day", "").Replace("-", ""),
                siteCode = itmF1.getProperty("site_code", ""),
                number = itmF1.getProperty("in_tree_no", ""),
                section = GetSectionName(itmF1),
                foot1 = GetPlayer(itmF1),
                foot2 = GetPlayer(itmF2),
            };

            report.siteName = GetSiteEnName(report.siteCode);

            return report;
        }

        private TRptPlayer GetPlayer(Item item)
        {
            return new TRptPlayer
            {
                foot = item.getProperty("in_sign_foot", ""),
                org = item.getProperty("in_short_org", ""),
                name = item.getProperty("in_name", ""),
                drawNo = item.getProperty("in_sign_no", ""),
                playerNo = item.getProperty("in_index", ""),
                team = item.getProperty("in_team", ""),
            };
        }

        private string GetSectionName(Item item)
        {
            var in_l1 = item.getProperty("in_l1", "");
            var in_l2 = item.getProperty("in_l2", "");
            var in_l3 = item.getProperty("in_l3", "");
            var rank = GetRankName(item);

            var arr = in_l3.Split(new char[] { '：' }, StringSplitOptions.RemoveEmptyEntries);

            var in_wn = arr[0];
            var in_wv = arr[1].Replace(" ", "");
            var arr2 = in_wv.Split(new char[] { '至' }, StringSplitOptions.RemoveEmptyEntries);
            var weight = arr2.Length == 1 ? arr2[0] : arr2[1];

            //式別/量級/賽別
            return in_l1 + " " + in_l2 + " " + weight + " " + rank;
        }

        private string GetRankName(Item item)
        {
            var team_count = GetInt(item.getProperty("in_team_count", "0"));
            if (team_count <= 5) return "循環賽";
            if (team_count == 6) return GetRankNamePlayer6(item);
            if (team_count == 7) return GetRankNamePlayer7(item);
            return GetRankNamePlayer8(item);
        }

        private string GetRankNamePlayer6(Item item)
        {
            var in_fight_id = item.getProperty("in_fight_id", "");

            switch (in_fight_id)
            {
                case "CRS6-01": return "Group A";
                case "CRS6-02": return "Group A";
                case "CRS6-03": return "Group A";
                case "CRS6-04": return "Group B";
                case "CRS6-05": return "Group B";
                case "CRS6-06": return "Group B";

                case "M004-01": return "準決賽";
                case "M004-02": return "準決賽";
                case "M002-01": return "決賽";
                case "RNK34-01": return "三四名";
                default: return "";
            }
        }

        private string GetRankNamePlayer7(Item item)
        {
            var in_fight_id = item.getProperty("in_fight_id", "");

            switch (in_fight_id)
            {
                case "CRS7-01": return "Group A";
                case "CRS7-02": return "Group A";
                case "CRS7-03": return "Group A";
                case "CRS7-04": return "Group A";
                case "CRS7-05": return "Group A";
                case "CRS7-06": return "Group A";

                case "CRS7-07": return "Group B";
                case "CRS7-08": return "Group B";
                case "CRS7-09": return "Group B";

                case "M004-01": return "準決賽";
                case "M004-02": return "準決賽";
                case "M002-01": return "決賽";
                case "RNK34-01": return "三四名";
                case "RNK56-01": return "五六名";
                default: return "";
            }
        }

        private string GetRankNamePlayer8(Item item)
        {
            var in_fight_id = item.getProperty("in_fight_id", "");
            var in_tree_rank = item.getProperty("in_tree_rank", "");

            switch (in_fight_id)
            {
                case "M004-01": return "準決賽";
                case "M004-02": return "準決賽";
                case "M002-01": return "決賽";
                case "RNK34-01": return "三四名";
                case "RNK56-01": return "五六名";
                case "RNK78-01": return "七八名";
            }

            switch (in_tree_rank)
            {
                case "rank35": return "三五名";
            }

            var c = in_fight_id[0];
            if (c == 'M') return "預賽";
            if (c == 'R') return "複賽";

            return "";
        }

        private string GetSiteEnName(string code)
        {
            switch (code)
            {
                case "1": return "A";
                case "2": return "B";
                case "3": return "C";
                case "4": return "D";
                case "5": return "E";
                case "6": return "F";
                case "7": return "G";
                case "8": return "H";
                default: return "A";
            }
        }

        private Item GetMeetingEventFoots(TConfig cfg)
        {
            var sql = @"
                SELECT 
	                t1.in_l1
	                , t1.in_l2
	                , t1.in_l3
	                , t1.in_fight_site
	                , t1.in_fight_day
	                , t1.in_fight_time
	                , t1.in_short_name
	                , t1.in_program_no
	                , t1.in_weight
	                , t1.in_team_count
	                , t2.in_tree_name
	                , t2.in_fight_id
	                , t2.in_tree_no
	                , t2.in_tree_rank
	                , t2.in_win_status
	                , t2.in_win_sign_no
	                , t3.in_sign_foot
	                , t3.in_sign_no
	                , t4.in_code AS 'site_code'
	                , t4.in_name AS 'site_name'
	                , t11.in_current_org
	                , t11.in_short_org
	                , t11.in_name
	                , t11.in_names
	                , t11.in_team
	                , t11.in_index
                FROM 
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PEVENT t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t3 WITH(NOLOCK)
	                ON t3.source_id = t2.id
                INNER JOIN
	                IN_MEETING_SITE t4 WITH(NOLOCK)
	                ON t4.id = t2.in_site
                LEFT OUTER JOIN
	                IN_MEETING_PTEAM t11 WITH(NOLOCK)
	                ON t11.source_id = t2.source_id
	                AND t11.in_sign_no = t3.in_sign_no
                WHERE
	                t2.id = '{#event_id}'
                ORDER BY
	                t3.in_sign_foot
            ";

            sql = sql.Replace("{#event_id}", cfg.event_id);

            return cfg.inn.applySQL(sql);
        }

        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string meeting_id { get; set; }
            public string program_id { get; set; }
            public string event_id { get; set; }
            public string scene { get; set; }
            public Item itmMeeting { get; set; }
            public Item itmProgram { get; set; }
            public Item itmXlsx { get; set; }
            public string mt_title { get; set; }
            public string template_path { get; set; }
            public string export_path { get; set; }
        }

        private class TRpt
        {
            public string day { get; set; }
            public string siteCode { get; set; }
            public string siteName { get; set; }
            public string number { get; set; }
            public string section { get; set; }
            public TRptPlayer foot1 { get; set; }
            public TRptPlayer foot2 { get; set; }
        }

        private class TRptPlayer
        {
            public string org { get; set; }
            public string name { get; set; }
            public string foot { get; set; }
            public string drawNo { get; set; }
            public string playerNo { get; set; }
            public string team { get; set; }
        }

        private int GetInt(string value, int def = 0)
        {
            int result = def;
            int.TryParse(value, out result);
            return result;
        }
    }
}