﻿using Aras.IOM;
using System;
using System.Collections.Generic;
using System.Linq;

namespace WorkHelp.ArasDesk.Methods.Wrestling.報表
{
    public class in_meeting_draw_xls3 : Item
    {
        public in_meeting_draw_xls3(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 隊職員名冊
                輸入: meeting_id
                輸出: 隊職員名冊
                人員: lina 創建 (2024-07-15)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]in_meeting_draw_xls3";

            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "input dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                meeting_id = itmR.getProperty("meeting_id", ""),
                FontName = "標楷體",
            };

            if (cfg.meeting_id == "")
            {
                throw new Exception("賽事 id 不得為空白");
            }

            cfg.itmMeeting = cfg.inn.applySQL("SELECT in_title FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            cfg.mt_title = cfg.itmMeeting.getProperty("in_title", "");

            var exp = ExportInfo(cfg, "weight_path");

            //取得與會者資料
            var itmMUsers = GetMeetingUsers(cfg);
            var musers = MapMUserList(cfg, itmMUsers);
            var orgSource = MapOrgList(cfg, musers);
            var orgs = orgSource.OrderBy(x => x.in_stuff_b1).ToList();

            //試算表
            var workbook = new Spire.Xls.Workbook();
            //選手名單
            AppendPlayersSheet(cfg, workbook, musers);
            //公告名單
            AppendBulletinSheet(cfg, workbook, musers);
            //保險代表人
            AppendInsuranceSheet(cfg, workbook, musers);
            //隊職員名冊
            AppendOrgPersonsSheet(cfg, workbook, orgs);
            //簽到表
            AppendOrgSignatureSheet(cfg, workbook, orgs);

            workbook.Worksheets[0].Remove();
            workbook.Worksheets[0].Remove();
            workbook.Worksheets[0].Remove();

            string ext_name = ".xlsx";
            string xls_name = cfg.mt_title + "_選手與隊職員_" + DateTime.Now.ToString("MMdd_HHmmss");
            string xls_file = exp.export_Path + "\\" + xls_name + ext_name;
            string xls_url = xls_name + ext_name;

            workbook.SaveToFile(xls_file, Spire.Xls.ExcelVersion.Version2013);

            itmR.setProperty("xls_name", xls_url);

            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

            return itmR;
        }

        private void AppendOrgSignatureSheet(TConfig cfg, Spire.Xls.Workbook workbook, List<TOrg> orgs)
        {
            cfg.FontName = "標楷體";

            var sheet = workbook.CreateEmptySheet();
            sheet.Name = "簽到表";

            sheet.PageSetup.TopMargin = 0.2;
            sheet.PageSetup.LeftMargin = 0.2;
            sheet.PageSetup.RightMargin = 0.2;
            sheet.PageSetup.BottomMargin = 0.2;

            var titlePos = "A1:D1";
            var titleTxt = cfg.mt_title + "簽到表";
            SetTitleCell(cfg, sheet, titlePos, titleTxt, 24, 30, false, true, HA.C);

            SetStrCell(cfg, sheet, "A2", "編號", 18, 0, false, false, HA.C);
            SetStrCell(cfg, sheet, "B2", "單位", 18, 0, false, false, HA.C);
            SetStrCell(cfg, sheet, "C2", "簽到", 18, 0, false, false, HA.C);
            SetStrCell(cfg, sheet, "D2", "備註" + Environment.NewLine + "(收據編號)", 12, 0, true, false, HA.C);

            var ri = 3;
            var no = 1;
            for (var i = 0; i < orgs.Count; i++)
            {
                var x = orgs[i];

                SetStrCell(cfg, sheet, "A" + ri, "【" + no + "】", 12, 36, false, false, HA.C);
                SetStrCell(cfg, sheet, "B" + ri, x.in_short_org, 18, 36, false, false, HA.L);
                ri++;
                no++;
            }
            sheet.Columns[0].ColumnWidth = 9;
            sheet.Columns[1].ColumnWidth = 42;
            sheet.Columns[2].ColumnWidth = 30;
            sheet.Columns[3].ColumnWidth = 15;

            SetRangeBorder(sheet, "A2" + ":" + "D" + (ri - 1));

            sheet.PageSetup.PrintTitleRows = "$1:$2";
            sheet.PageSetup.CenterHorizontally = true;
        }

        private void AppendBulletinSheet(TConfig cfg, Spire.Xls.Workbook workbook, List<TPerson> musers)
        {
            cfg.FontName = "新細明體";

            var sorted = musers.FindAll(x => x.isPlayer)
                .OrderBy(x => x.in_stuff_b1).ThenBy(x => x.program_sort).ThenBy(x => x.in_name).ToList();

            var sheet = workbook.CreateEmptySheet();
            sheet.Name = "公告名單";

            sheet.PageSetup.TopMargin = 0.2;
            sheet.PageSetup.LeftMargin = 0.2;
            sheet.PageSetup.RightMargin = 0.2;
            sheet.PageSetup.BottomMargin = 0.2;

            var titlePos = "A1:C1";
            var titleTxt = cfg.mt_title + "確認名單";
            SetTitleCell(cfg, sheet, titlePos, titleTxt, 18, 30, false, true, HA.C);

            SetStrCell(cfg, sheet, "A2", "姓名", 11, 0, false, false, HA.C);
            SetStrCell(cfg, sheet, "B2", "單位", 11, 0, false, false, HA.C);
            SetStrCell(cfg, sheet, "C2", "量級", 11, 0, false, false, HA.C);

            var ri = 3;
            var lastOrg = "";
            for (var i = 0; i < sorted.Count; i++)
            {
                var x = sorted[i];
                var name = x.in_name[0] + "O" + x.in_name.Substring(2, x.in_name.Length - 2);

                if (i > 0 && lastOrg != x.map_org_name)
                {
                    lastOrg = x.map_org_name;
                    ri++;
                }

                SetStrCell(cfg, sheet, "A" + ri, name, 11, 0, false, false, HA.L);
                SetStrCell(cfg, sheet, "B" + ri, x.map_org_name, 11, 0, false, false, HA.L);
                SetStrCell(cfg, sheet, "C" + ri, x.program_name, 11, 0, false, false, HA.L);

                ri++;
            }
            sheet.Columns[0].ColumnWidth = 12;
            sheet.Columns[1].ColumnWidth = 28;
            sheet.Columns[2].ColumnWidth = 57;

            SetRangeBorder(sheet, "A2" + ":" + "C" + (ri - 1));

            sheet.PageSetup.PrintTitleRows = "$1:$2";
            sheet.PageSetup.CenterHorizontally = true;
        }

        private void AppendPlayersSheet(TConfig cfg, Spire.Xls.Workbook workbook, List<TPerson> musers)
        {
            cfg.FontName = "新細明體";

            var sorted = musers.FindAll(x => x.isPlayer)
                .OrderBy(x => x.reg_birth).ThenBy(x => x.in_stuff_b1).ThenBy(x => x.in_name).ToList();

            var sheet = workbook.CreateEmptySheet();
            sheet.Name = "選手名單";

            sheet.PageSetup.TopMargin = 0.4;
            sheet.PageSetup.LeftMargin = 0.6;
            sheet.PageSetup.RightMargin = 0.6;
            sheet.PageSetup.BottomMargin = 0.6;

            SetStrCell(cfg, sheet, "A1", "姓名", 11, 0, true, false, HA.C);
            SetStrCell(cfg, sheet, "B1", "身分證字號", 11, 0, true, false, HA.C);
            SetStrCell(cfg, sheet, "C1", "性別", 11, 0, true, false, HA.C);
            SetStrCell(cfg, sheet, "D1", "生日", 11, 0, true, false, HA.C);
            SetStrCell(cfg, sheet, "E1", "單位", 11, 0, true, false, HA.C);
            SetStrCell(cfg, sheet, "F1", "賽事", 11, 0, true, false, HA.C);
            SetStrCell(cfg, sheet, "G1", "組別", 11, 0, true, false, HA.C);
            SetStrCell(cfg, sheet, "H1", "型式", 11, 0, true, false, HA.C);
            SetStrCell(cfg, sheet, "I1", "量級", 11, 0, true, false, HA.C);
            SetStrCell(cfg, sheet, "J1", "籤號", 11, 0, true, false, HA.C);

            var ri = 2;
            for (var i = 0; i < sorted.Count; i++)
            {
                var x = sorted[i];
                if (!x.isPlayer) continue;
                var wsBirth = x.reg_birth.ToString("yyyy年MM月dd日");
                var twBirth = (x.reg_birth.Year - 1911) + "年" + x.reg_birth.ToString("MM月dd日");
                SetStrCell(cfg, sheet, "A" + ri, x.in_name, 11, 0, false, false, HA.L);
                SetStrCell(cfg, sheet, "B" + ri, x.in_sno.ToUpper(), 11, 0, false, false, HA.L);
                SetStrCell(cfg, sheet, "C" + ri, x.in_gender, 11, 0, false, false, HA.L);
                SetStrCell(cfg, sheet, "D" + ri, twBirth, 11, 0, false, false, HA.L);
                SetStrCell(cfg, sheet, "E" + ri, x.map_org_name, 11, 0, false, false, HA.L);
                SetStrCell(cfg, sheet, "F" + ri, cfg.mt_title, 11, 0, false, false, HA.L);
                SetStrCell(cfg, sheet, "G" + ri, x.in_l1, 11, 0, false, false, HA.L);
                SetStrCell(cfg, sheet, "H" + ri, x.in_l2, 11, 0, false, false, HA.L);
                SetStrCell(cfg, sheet, "I" + ri, x.program_name, 11, 0, false, false, HA.L);
                ri++;
            }
            for (var i = 0; i < 8; i++)
            {
                sheet.Columns[i].AutoFitColumns();
            }
        }

        private void AppendInsuranceSheet(TConfig cfg, Spire.Xls.Workbook workbook, List<TPerson> musers)
        {
            cfg.FontName = "新細明體";

            var sorted = musers.FindAll(x => x.isInsurance)
                .OrderBy(x => x.in_stuff_b1).ToList();

            var sheet = workbook.CreateEmptySheet();
            sheet.Name = "保險代表人";

            sheet.PageSetup.TopMargin = 0.4;
            sheet.PageSetup.LeftMargin = 0.6;
            sheet.PageSetup.RightMargin = 0.6;
            sheet.PageSetup.BottomMargin = 0.6;

            SetStrCell(cfg, sheet, "A1", "姓名", 11, 0, true, false, HA.C);
            SetStrCell(cfg, sheet, "B1", "身分證字號", 11, 0, true, false, HA.C);
            SetStrCell(cfg, sheet, "C1", "性別", 11, 0, true, false, HA.C);
            SetStrCell(cfg, sheet, "D1", "生日", 11, 0, true, false, HA.C);
            SetStrCell(cfg, sheet, "E1", "單位", 11, 0, true, false, HA.C);
            SetStrCell(cfg, sheet, "F1", "賽事", 11, 0, true, false, HA.C);
            SetStrCell(cfg, sheet, "G1", "組別", 11, 0, true, false, HA.C);
            SetStrCell(cfg, sheet, "H1", "選手家長", 11, 0, true, false, HA.C);
            SetStrCell(cfg, sheet, "I1", "與選手關係", 11, 0, true, false, HA.C);

            var ri = 2;
            for (var i = 0; i < sorted.Count; i++)
            {
                var x = sorted[i];
                SetStrCell(cfg, sheet, "A" + ri, x.in_name, 11, 0, false, false, HA.L);
                SetStrCell(cfg, sheet, "B" + ri, x.in_sno.ToUpper(), 11, 0, false, false, HA.L);
                SetStrCell(cfg, sheet, "C" + ri, x.in_gender, 11, 0, false, false, HA.L);
                SetStrCell(cfg, sheet, "D" + ri, x.twBirth, 11, 0, false, false, HA.L);
                SetStrCell(cfg, sheet, "E" + ri, x.map_org_name, 11, 0, false, false, HA.L);
                SetStrCell(cfg, sheet, "F" + ri, cfg.mt_title, 11, 0, false, false, HA.L);
                SetStrCell(cfg, sheet, "G" + ri, x.in_l1, 11, 0, false, false, HA.L);
                SetStrCell(cfg, sheet, "H" + ri, x.in_emrg_contact1, 11, 0, false, false, HA.L);
                SetStrCell(cfg, sheet, "I" + ri, x.in_emrg_relation1, 11, 0, false, false, HA.L);
                ri++;
            }
            for (var i = 0; i < 9; i++)
            {
                sheet.Columns[i].AutoFitColumns();
            }
        }

        private void AppendOrgPersonsSheet(TConfig cfg, Spire.Xls.Workbook workbook, List<TOrg> orgs)
        {
            cfg.FontName = "標楷體";
            var sheet = workbook.CreateEmptySheet();
            sheet.Name = "隊職員名冊";

            sheet.PageSetup.TopMargin = 0.4;
            sheet.PageSetup.LeftMargin = 0.6;
            sheet.PageSetup.RightMargin = 0.6;
            sheet.PageSetup.BottomMargin = 0.6;

            var titleSize = 16;
            if (cfg.mt_title.Length >= 32) titleSize = 15;
            var titlePos = "A1:I3";
            var titleTxt = cfg.mt_title + Environment.NewLine + "隊職員名冊";
            SetStrCell(cfg, sheet, titlePos, titleTxt, titleSize, 20, false, true, HA.C);

            var rpt = new TReport { ridx = 4 };
            var no = 1;
            for (var i = 0; i < orgs.Count; i++)
            {
                var org = orgs[i];
                var staffCount = org.leaders.Count + org.managers.Count + org.coaches_m.Count + org.coaches_w.Count + org.coaches_n.Count;

                var orgPos1 = "A" + rpt.ridx;
                var orgTxt1 = "【" + no + "】";
                SetStrCell(cfg, sheet, orgPos1, orgTxt1, 14, 20, false, false, HA.C);

                var orgPos2 = "B" + rpt.ridx + ":" + "I" + rpt.ridx;
                var orgTxt2 = org.in_short_org;
                SetStrCell(cfg, sheet, orgPos2, orgTxt2, 14, 20, false, true, HA.L);
                rpt.ridx++;
                rpt.ridx++;

                if (staffCount > 0)
                {
                    AppendStaffBlock(cfg, sheet, rpt, org);
                    rpt.ridx++;
                    rpt.ridx++;
                }

                AppendPlayerBlock(cfg, sheet, rpt, org);
                rpt.ridx++;

                no++;
            }

            sheet.Columns[8].AutoFitColumns();
        }

        private void AppendPlayerBlock(TConfig cfg, Spire.Xls.Worksheet sheet, TReport rpt, TOrg org)
        {
            var titlePos = "A" + rpt.ridx + ":" + "B" + rpt.ridx;
            var titleTxt = "選手:";
            SetStrCell(cfg, sheet, titlePos, titleTxt, 14, 20, false, true, HA.C);
            rpt.ridx++;

            var groups = GetPlayerGroupMap(org.playersB);
            foreach (var kv in groups)
            {
                var in_l1 = kv.Key;
                var players = kv.Value;

                var groupPos = "A" + rpt.ridx + ":" + "C" + rpt.ridx;
                //var groupTxt = in_l1 + " " + "自由式";
                var groupTxt = in_l1;
                SetStrCell(cfg, sheet, groupPos, groupTxt, 14, 20, false, true, HA.L);

                var cn = 6;
                var rc = players.Count % cn == 0 ? players.Count / cn : (players.Count / cn) + 1;
                var mx = players.Count - 1;
                var arr = new string[] { "D", "E", "F", "G", "H", "I" };
                for (var i = 0; i < rc; i++)
                {
                    for (var j = 0; j < cn; j++)
                    {
                        var idx = i * 6 + j;
                        if (idx > mx) continue;

                        var player = players[idx];
                        var playerPos = arr[j] + rpt.ridx;
                        var playerTxt = player.in_name;
                        SetNameCell(cfg, sheet, playerPos, playerTxt, 14, 20, false, false, HA.L);
                    }
                    rpt.ridx++;
                }
            }
        }

        private void AppendStaffBlock(TConfig cfg, Spire.Xls.Worksheet sheet, TReport rpt, TOrg org)
        {
            var has2Rows = false;
            if (org.leaders.Count > 0)
            {
                var staffPos1 = "A" + rpt.ridx;
                var staffTxt1 = "領隊:";
                SetStrCell(cfg, sheet, staffPos1, staffTxt1, 14, 0, false, false, HA.L);

                var staffPos2 = "B" + rpt.ridx;
                var staffTxt2 = org.leaders[0].in_name;
                SetNameCell(cfg, sheet, staffPos2, staffTxt2, 14, 0, false, false, HA.L);

                if (org.leaders.Count > 1)
                {
                    var staffPos3 = "B" + (rpt.ridx + 1);
                    var staffTxt3 = org.leaders[1].in_name;
                    SetNameCell(cfg, sheet, staffPos3, staffTxt3, 14, 0, false, false, HA.L);
                    has2Rows = true;
                }
            }

            if (org.managers.Count > 0)
            {
                var staffPos1 = "H" + rpt.ridx;
                var staffTxt1 = "管理:";
                SetStrCell(cfg, sheet, staffPos1, staffTxt1, 14, 0, false, false, HA.L);

                var staffPos2 = "I" + rpt.ridx;
                var staffTxt2 = org.managers[0].in_name;
                SetNameCell(cfg, sheet, staffPos2, staffTxt2, 14, 0, false, false, HA.L);

                if (org.leaders.Count > 1)
                {
                    var staffPos3 = "I" + (rpt.ridx + 1);
                    var staffTxt3 = org.managers[1].in_name;
                    SetNameCell(cfg, sheet, staffPos3, staffTxt3, 14, 0, false, false, HA.L);
                    has2Rows = true;
                }
            }

            if (org.coaches_m.Count > 0 && org.coaches_w.Count > 0)
            {
                var staffPos1 = "D" + rpt.ridx;
                var staffTxt1 = "男子教練:";
                SetStrCell(cfg, sheet, staffPos1, staffTxt1, 14, 0, false, false, HA.R);

                var staffPos2 = "E" + rpt.ridx;
                var staffTxt2 = org.coaches_m[0].in_name;
                SetNameCell(cfg, sheet, staffPos2, staffTxt2, 14, 0, false, false, HA.L);

                if (org.coaches_m.Count > 1)
                {
                    var staffPos3 = "F" + rpt.ridx;
                    var staffTxt3 = org.coaches_m[1].in_name;
                    SetNameCell(cfg, sheet, staffPos3, staffTxt3, 14, 0, false, false, HA.L);
                }

                var staffPosW1 = "D" + (rpt.ridx + 1);
                var staffTxtW1 = "女子教練:";
                SetStrCell(cfg, sheet, staffPosW1, staffTxtW1, 14, 0, false, false, HA.R);

                var staffPosW2 = "E" + (rpt.ridx + 1);
                var staffTxtW2 = org.coaches_w[0].in_name;
                SetNameCell(cfg, sheet, staffPosW2, staffTxtW2, 14, 0, false, false, HA.L);

                if (org.coaches_w.Count > 1)
                {
                    var staffPosW3 = "F" + (rpt.ridx + 1);
                    var staffTxtW3 = org.coaches_w[1].in_name;
                    SetNameCell(cfg, sheet, staffPosW3, staffTxtW3, 14, 0, false, false, HA.L);
                }
                has2Rows = true;
            }
            else if (org.coaches_m.Count > 0)
            {
                var staffPos1 = "D" + rpt.ridx;
                var staffTxt1 = "男子教練:";
                SetStrCell(cfg, sheet, staffPos1, staffTxt1, 14, 0, false, false, HA.R);

                var staffPos2 = "E" + rpt.ridx;
                var staffTxt2 = org.coaches_m[0].in_name;
                SetNameCell(cfg, sheet, staffPos2, staffTxt2, 14, 0, false, false, HA.L);

                if (org.coaches_m.Count > 1)
                {
                    var staffPos3 = "F" + rpt.ridx;
                    var staffTxt3 = org.coaches_m[1].in_name;
                    SetNameCell(cfg, sheet, staffPos3, staffTxt3, 14, 0, false, false, HA.L);
                }
            }
            else if (org.coaches_w.Count > 0)
            {
                var staffPos1 = "D" + rpt.ridx;
                var staffTxt1 = "女子教練:";
                SetStrCell(cfg, sheet, staffPos1, staffTxt1, 14, 0, false, false, HA.R);

                var staffPos2 = "E" + rpt.ridx;
                var staffTxt2 = org.coaches_w[0].in_name;
                SetNameCell(cfg, sheet, staffPos2, staffTxt2, 14, 0, false, false, HA.L);

                if (org.coaches_w.Count > 1)
                {
                    var staffPos3 = "F" + rpt.ridx;
                    var staffTxt3 = org.coaches_w[1].in_name;
                    SetNameCell(cfg, sheet, staffPos3, staffTxt3, 14, 0, false, false, HA.L);
                }
            }
            else if (org.coaches_n.Count > 0)
            {
                var staffPos1 = "D" + rpt.ridx;
                var staffTxt1 = "教練:";
                SetStrCell(cfg, sheet, staffPos1, staffTxt1, 14, 0, false, false, HA.R);

                var staffPos2 = "E" + rpt.ridx;
                var staffTxt2 = org.coaches_n[0].in_name;
                SetNameCell(cfg, sheet, staffPos2, staffTxt2, 14, 0, false, false, HA.L);

                if (org.coaches_n.Count > 1)
                {
                    var staffPos3 = "F" + rpt.ridx;
                    var staffTxt3 = org.coaches_n[1].in_name;
                    SetNameCell(cfg, sheet, staffPos3, staffTxt3, 14, 0, false, false, HA.L);
                }
            }
            if (has2Rows) rpt.ridx++;
        }

        private void SetNameCell(TConfig cfg, Spire.Xls.Worksheet sheet
            , string pos
            , string text
            , int size
            , int height = 0
            , bool needBold = false
            , bool needMerge = false
            , HA ha = HA.C)
        {
            var range = sheet.Range[pos];
            if (needMerge) range.Merge();
            range.Text = text;
            var newSize = size;
            if (text.Length == 4)
            {
                newSize = 12;
            }
            else if (text.Length >= 5)
            {
                newSize = 9;
            }
            SetUtlCell(cfg, range, newSize, height, needBold, ha);
        }

        private void SetTitleCell(TConfig cfg, Spire.Xls.Worksheet sheet
            , string pos
            , string text
            , int size
            , int height = 0
            , bool needBold = false
            , bool needMerge = false
            , HA ha = HA.C)
        {
            var range = sheet.Range[pos];
            if (needMerge) range.Merge();
            range.Text = text;
            SetUtlCell(cfg, range, size, height, needBold, ha);
            range.Style.ShrinkToFit = true;
            range.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;
        }

        private void SetStrCell(TConfig cfg, Spire.Xls.Worksheet sheet
            , string pos
            , string text
            , int size
            , int height = 0
            , bool needBold = false
            , bool needMerge = false
            , HA ha = HA.C)
        {
            var range = sheet.Range[pos];
            if (needMerge) range.Merge();
            range.Text = text;
            SetUtlCell(cfg, range, size, height, needBold, ha);
        }

        private void SetIntCell(TConfig cfg, Spire.Xls.Worksheet sheet
            , string pos
            , int text
            , int size
            , int height = 0
            , bool needBold = false
            , bool needMerge = false
            , HA ha = HA.C)
        {
            var range = sheet.Range[pos];
            if (needMerge) range.Merge();
            range.NumberValue = text;
            range.NumberFormat = "0";
            SetUtlCell(cfg, range, size, height, needBold, ha);
        }

        private void SetUtlCell(TConfig cfg, Spire.Xls.CellRange range
            , int size
            , int height
            , bool needBold
            , HA ha = HA.C)
        {
            range.Style.Font.FontName = cfg.FontName;
            range.Style.Font.Size = size;

            if (needBold) range.Style.Font.IsBold = true;
            if (height > 0) range.RowHeight = height;

            range.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;

            switch (ha)
            {
                case HA.C:
                    range.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                    break;
                case HA.L:
                    range.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Left;
                    break;
                case HA.R:
                    range.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Right;
                    break;
            }
        }

        private enum HA
        {
            L = 100,
            C = 200,
            R = 300
        }

        //設定格線
        private void SetRangeBorder(Spire.Xls.Worksheet sheet, string pos)
        {
            var range = sheet.Range[pos];
            range.BorderInside(Spire.Xls.LineStyleType.Thin, System.Drawing.Color.Black);
            range.BorderAround(Spire.Xls.LineStyleType.Thin, System.Drawing.Color.Black);
        }

        /// <summary>
        /// 取得與會者資料
        /// </summary>
        private Item GetMeetingUsers(TConfig cfg)
        {
            string sql = @"
                SELECT
                    t1.in_l1
                    , t1.in_l2
                    , t1.in_l3
                    , t1.in_index
                    , ISNULL(t1.in_l1, '') + '-' + ISNULL(t1.in_l2, '') + '-' + ISNULL(t1.in_l3, '') + '-' + ISNULL(t1.in_index, '') + '-' + ISNULL(t1.in_creator_sno, '') AS 'in_team_key'
                    , t1.in_l1 + '-' + t1.in_l3 AS 'program_name'
                    , t1.in_current_org
                    , t1.in_team
                    , t1.in_name
                    , t1.in_sno
                    , t1.in_gender
                    , t1.in_birth
                    , t1.in_creator
                    , t1.in_creator_sno
                    , t2.in_tel AS 'in_creator_tel'
                    , t1.in_expense
                    , t1.in_paynumber
	                , CONVERT(VARCHAR, DATEADD(HOUR, 8, t1.in_birth), 111) AS 'reg_birth'
                    , t1.in_emrg_contact1
                    , t1.in_emrg_relation1
					, t1.in_stuff_b1
					, t2.in_email AS 'creator_email'
					, t2.in_tel   AS 'creator_tel'
                    , t3.pay_bool
                    , t3.in_pay_amount_exp
                    , t3.in_pay_amount_real
					, t4.in_type
                    , t4.in_short_org AS 'map_org_name'
                    , t12.sort_order   AS 'program_group_sort'
                    , t13.sort_order   AS 'program_sort'
                FROM 
                    IN_MEETING_USER t1 WITH(NOLOCK)
                LEFT OUTER JOIN
                    IN_RESUME t2 WITH(NOLOCK)
                    ON t2.in_sno = t1.in_creator_sno
                LEFT OUTER JOIN
                    IN_MEETING_PAY t3 WITH(NOLOCK)
                    ON t3.in_meeting = t1.source_id 
                    AND t3.item_number = t1.in_paynumber
                LEFT OUTER JOIN
                    IN_ORG_MAP t4 WITH(NOLOCK)
                    ON t4.in_stuff_b1 = t1.in_stuff_b1 
				LEFT OUTER JOIN
					VU_MEETING_SVY_L1 t11
					ON t11.source_id = t1.source_id
					AND t11.in_value = t1.in_l1
				LEFT OUTER JOIN
					VU_MEETING_SVY_L2 t12
					ON t12.source_id = t1.source_id
					AND t12.in_filter = t1.in_l1
					AND t12.in_value = t1.in_l2
				LEFT OUTER JOIN
					VU_MEETING_SVY_L3 t13
					ON t13.source_id = t1.source_id
					AND t13.in_grand_filter = t1.in_l1
					AND t13.in_filter = t1.in_l2
					AND t13.in_value = t1.in_l3
                WHERE 
                    t1.source_id = '{#meeting_id}'
                ORDER BY 
                    t11.sort_order
                    , t12.sort_order
                    , t13.sort_order
					, t1.in_stuff_b1
					, t1.in_sno
					, t1.in_name
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            var items = cfg.inn.applySQL(sql);

            if (items.isError() || items.getItemCount() <= 0)
            {
                throw new Exception("查無與會者資料");
            }

            return items;
        }

        private TExport ExportInfo(TConfig cfg, string in_name)
        {
            Item itmXls = cfg.inn.applyMethod("In_Meeting_Excel_Path", "<in_name>" + in_name + "</in_name>");
            string Export_Path = itmXls.getProperty("export_path", "");
            string Template_Path = itmXls.getProperty("template_path", "");

            TExport result = new TExport
            {
                template_Path = Template_Path,
                export_Path = Export_Path,
            };

            return result;
        }

        private List<TOrg> MapOrgList(TConfig cfg, List<TPerson> rows)
        {
            var orgs = new List<TOrg>();
            for (int i = 0; i < rows.Count; i++)
            {
                var p = rows[i];
                var org = orgs.Find(x => x.in_stuff_b1 == p.in_stuff_b1);
                if (org == null)
                {
                    org = new TOrg
                    {
                        in_type = p.in_type,
                        in_stuff_b1 = p.in_stuff_b1,
                        in_current_org = p.in_current_org,
                        in_short_org = p.map_org_name,
                        in_name = p.creator_name,
                        in_email = p.creator_email,
                        in_tel = p.creator_tel,
                        leaders = new List<TPerson>(),
                        managers = new List<TPerson>(),
                        coaches_m = new List<TPerson>(),
                        coaches_w = new List<TPerson>(),
                        coaches_n = new List<TPerson>(),
                        insurances = new List<TPerson>(),
                        playersA = new List<TPerson>(),
                        playersB = new List<TPerson>(),
                    };
                    orgs.Add(org);
                }
                AnalysisMUser(cfg, org, p);
            }
            return orgs;
        }

        private void AnalysisMUser(TConfig cfg, TOrg org, TPerson p)
        {
            if (p.isPlayer)
            {
                org.playersA.Add(p);
                var old = org.playersB.Find(x => x.groupKey == p.groupKey);
                if (old == null)
                {
                    old = p;
                    org.playersB.Add(p);
                }
                old.weights.Add(new TWeight
                {
                    in_l1 = p.in_l1,
                    in_l2 = p.in_l2,
                    in_l3 = p.in_l3,
                });
            }
            else if (p.isInsurance)
            {
                org.insurances.Add(p);
            }
            else if (p.isLeader)
            {
                org.hasStaff = true;
                org.leaders.Add(p);
            }
            else if (p.isManager)
            {
                org.hasStaff = true;
                org.managers.Add(p);
            }
            else if (p.isCoachM)
            {
                org.hasStaff = true;
                org.coaches_m.Add(p);
            }
            else if (p.isCoachW)
            {
                org.hasStaff = true;
                org.coaches_w.Add(p);
            }
            else if (p.isCoachN)
            {
                org.hasStaff = true;
                org.coaches_n.Add(p);
            }
        }

        private List<TPerson> MapMUserList(TConfig cfg, Item items)
        {
            var list = new List<TPerson>();
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                list.Add(MapMUser(cfg, item));
            }
            return list;
        }

        private TPerson MapMUser(TConfig cfg, Item item)
        {
            var x = new TPerson
            {
                item = item,
                in_type = item.getProperty("in_type", ""),
                in_stuff_b1 = item.getProperty("in_stuff_b1", ""),
                in_current_org = item.getProperty("in_current_org", ""),
                map_org_name = item.getProperty("map_org_name", ""),
                in_l1 = item.getProperty("in_l1", ""),
                in_l2 = item.getProperty("in_l2", ""),
                in_l3 = item.getProperty("in_l3", ""),
                in_name = item.getProperty("in_name", ""),
                in_sno = item.getProperty("in_sno", "").ToUpper(),
                reg_birth = GetDtm(item.getProperty("reg_birth", "")),
                in_gender = item.getProperty("in_gender", ""),
                in_emrg_contact1 = item.getProperty("in_emrg_contact1", ""),
                in_emrg_relation1 = item.getProperty("in_emrg_relation1", ""),
                creator_name = item.getProperty("in_creator", ""),
                creator_email = item.getProperty("creator_email", ""),
                creator_tel = item.getProperty("creator_tel", ""),
                program_name = item.getProperty("program_name", "").Replace("自由式-", ""),
                program_sort = GetInt(item.getProperty("program_sort", "0")),
                weights = new List<TWeight>(),
            };

            if (x.in_l2.Contains("自由式")) x.in_l2 = "自由式";
            else if (x.in_l2.Contains("希羅式")) x.in_l2 = "希羅式";
            x.program_name = x.in_l1 + "-" + x.in_l2 + "-" + x.in_l3;

            if (x.reg_birth != DateTime.MinValue)
            {
                x.wsBirth = x.reg_birth.ToString("yyyy年MM月dd日");
                x.twBirth = (x.reg_birth.Year - 1911) + "年" + x.reg_birth.ToString("MM月dd日");
            }

            switch (x.in_l1)
            {
                case "隊職員":
                    x.isStaff = true;
                    AnalysisStaff(cfg, x);
                    x.groupName = x.in_l1;
                    x.sheetName = x.in_l1;
                    break;
                case "保險代表人":
                    x.isInsurance = true;
                    x.groupName = x.in_l1;
                    x.sheetName = x.in_l1;
                    break;
                default:
                    x.isPlayer = true;
                    AnalysisPlayer(cfg, x);
                    break;
            }
            x.groupKey = x.groupName + "-" + x.in_sno;
            x.item.setProperty("groupName", x.groupName);
            return x;
        }

        private void AnalysisPlayer(TConfig cfg, TPerson x)
        {
            if (x.in_l1.Contains("大專"))
            {
                x.groupName = "大專社會組";
            }
            else if (x.in_l1.Contains("高中"))
            {
                x.groupName = "高中組";
            }
            else if (x.in_l1.Contains("國中"))
            {
                x.groupName = "國中組";
            }
            else if (x.in_l1.Contains("國小"))
            {
                x.groupName = "國小組";
            }
            else
            {
                x.groupName = x.in_l1;
            }
            x.sheetName = x.groupName + "籤表";

        }
        private void AnalysisStaff(TConfig cfg, TPerson x)
        {
            switch (x.in_l2)
            {
                case "領隊":
                    x.isLeader = true;
                    break;
                case "管理":
                    x.isManager = true;
                    break;
                case "男教練":
                case "男子隊教練":
                    x.isCoachM = true;
                    break;
                case "女教練":
                case "女子隊教練":
                    x.isCoachW = true;
                    break;
                case "教練":
                    x.isCoachN = true;
                    break;
            }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string meeting_id { get; set; }
            public Item itmMeeting { get; set; }
            public string mt_title { get; set; }
            public string FontName { get; set; }
        }

        private class TExport
        {
            public string template_Path { get; set; }
            public string export_Path { get; set; }
        }

        private class TOrg
        {
            public string in_type { get; set; }
            public string in_stuff_b1 { get; set; }
            public string in_current_org { get; set; }
            public string in_short_org { get; set; }
            public string in_name { get; set; }
            public string in_email { get; set; }
            public string in_tel { get; set; }
            public List<TPerson> leaders { get; set; }
            public List<TPerson> managers { get; set; }
            public List<TPerson> coaches_m { get; set; }
            public List<TPerson> coaches_w { get; set; }
            public List<TPerson> coaches_n { get; set; }
            public List<TPerson> insurances { get; set; }
            public List<TPerson> playersA { get; set; }
            public List<TPerson> playersB { get; set; }
            public bool hasStaff { get; set; }
        }

        private class TPerson
        {
            public bool isStaff { get; set; }
            public bool isLeader { get; set; }
            public bool isManager { get; set; }
            public bool isCoachM { get; set; }
            public bool isCoachW { get; set; }
            public bool isCoachN { get; set; }
            public bool isPlayer { get; set; }
            public bool isInsurance { get; set; }
            public string groupName { get; set; }
            public string sheetName { get; set; }

            public string in_type { get; set; }
            public string in_stuff_b1 { get; set; }
            public string in_current_org { get; set; }
            public string map_org_name { get; set; }
            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }
            public string program_name { get; set; }
            public string in_name { get; set; }
            public string in_sno { get; set; }
            public string in_gender { get; set; }
            public string in_emrg_contact1 { get; set; }
            public string in_emrg_relation1 { get; set; }
            public string creator_name { get; set; }
            public string creator_email { get; set; }
            public string creator_tel { get; set; }
            public DateTime reg_birth { get; set; }
            public Item item { get; set; }

            public string wsBirth { get; set; }
            public string twBirth { get; set; }
            public int program_sort { get; set; }
            public string groupKey { get; set; }

            public List<TWeight> weights { get; set; }
        }

        private class TWeight
        {
            public string program_id { get; set; }
            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }
        }

        private class TReport
        {
            public int ridx { get; set; }
        }

        private Dictionary<string, List<TPerson>> GetPlayerGroupMap(List<TPerson> rows)
        {
            var map = new Dictionary<string, List<TPerson>>();
            for (var i = 0; i < rows.Count; i++)
            {
                var x = rows[i];
                var group = default(List<TPerson>);
                if (map.ContainsKey(x.in_l1))
                {
                    group = map[x.in_l1];
                }
                else
                {
                    group = new List<TPerson>();
                    map.Add(x.in_l1, group);
                }
                group.Add(x);
            }
            return map;
        }

        private string FixSectName(string value)
        {
            var resule = "";
            var arr = value.Split(new char[] { '：' }, StringSplitOptions.RemoveEmptyEntries);
            var w1 = arr.Length > 0 ? arr[0] : "";
            var w2 = arr.Length > 1 ? arr[1] : "";

            if (w2.Contains("以上至"))
            {
                w2 = w2.Replace(".1公斤以上至", "~");
                w2 = w2.Replace(".0公斤以下", "");
                var arr2 = w2.Split(new char[] { '~' }, StringSplitOptions.RemoveEmptyEntries);
                var weight = arr2.Length > 1 ? arr2[1] : "";
                resule = w1.Replace("-", "-自由式 : ") + "(-" + weight + "公斤)";
            }
            else
            {
                w2 = "~" + w2.Replace(".0公斤以下", "");
                resule = w1.Replace("-", "-自由式 : ") + "(-" + w2 + "公斤)";
            }
            return resule;
        }

        private int GetInt(string value, int def = 0)
        {
            if (value == "") return 0;
            int result = def;
            int.TryParse(value, out result);
            return result;
        }

        private DateTime GetDtm(string value)
        {
            if (value == "") return DateTime.MinValue;
            DateTime dt;
            if (DateTime.TryParse(value, out dt)) return dt;
            return DateTime.MinValue;
        }
    }
}