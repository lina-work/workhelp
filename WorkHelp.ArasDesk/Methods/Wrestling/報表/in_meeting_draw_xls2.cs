﻿using Aras.IOM;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace WorkHelp.ArasDesk.Methods.Wrestling.報表
{
    public class in_meeting_draw_xls2 : Item
    {
        public in_meeting_draw_xls2(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
    目的: 分組分項編配表
    輸入: meeting_id
    輸出: 分組分項編配表
    人員: lina 創建 (2024-07-15)
*/

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]in_meeting_draw_xls2";

            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "input dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                meeting_id = itmR.getProperty("meeting_id", ""),
                CharSet = GetCharSet(),
                FontName = "標楷體",
            };

            if (cfg.meeting_id == "")
            {
                throw new Exception("賽事 id 不得為空白");
            }

            cfg.itmMeeting = cfg.inn.applySQL("SELECT in_title FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            cfg.mt_title = cfg.itmMeeting.getProperty("in_title", "");

            var exp = ExportInfo(cfg, "weight_path");

            //取得與會者資料
            var itmMUsers = GetMeetingUsers(cfg);
            var musers = MapMUserList(cfg, itmMUsers);
            var reports = MapReports(cfg, musers);
            var sects = MergeReportSects(cfg, reports);

            //試算表
            var workbook = new Spire.Xls.Workbook();

            //組別摘要
            AppendSectsSheet(cfg, workbook, reports);
            //預估時長
            AppendTimeSheet(cfg, workbook, sects);
            //每日時長
            AppendDaySheet(cfg, workbook, sects);

            //分組籤表與報名名單
            foreach (var report in reports)
            {
                switch (report.key)
                {
                    default:
                        AppendSingleSheet(cfg, workbook, report);
                        break;
                }
            }

            workbook.Worksheets[0].Remove();
            workbook.Worksheets[0].Remove();
            workbook.Worksheets[0].Remove();

            string ext_name = ".xlsx";
            string xls_name = cfg.mt_title + "_分組分項編配表_" + DateTime.Now.ToString("MMdd_HHmmss");
            string xls_file = exp.export_Path + "\\" + xls_name + ext_name;
            string xls_url = xls_name + ext_name;

            workbook.SaveToFile(xls_file, Spire.Xls.ExcelVersion.Version2013);

            itmR.setProperty("xls_name", xls_url);

            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

            return itmR;
        }

        private void AppendDaySheet(TConfig cfg, Spire.Xls.Workbook workbook, List<TSect> sects)
        {
            var days = MapDayList(cfg, sects);

            var sheet = workbook.CreateEmptySheet();
            sheet.Name = "每日時長";

            sheet.PageSetup.TopMargin = 0.6;
            sheet.PageSetup.LeftMargin = 0.5;
            sheet.PageSetup.RightMargin = 0.5;
            sheet.PageSetup.BottomMargin = 0.6;

            List<TField> fields = new List<TField>();
            fields.Add(new TField { title = "比賽日期", property = "fight_day", css = TCss.Center, width = 12 });
            fields.Add(new TField { title = "總分鐘", property = "day_minutes", css = TCss.Center, width = 12 });
            fields.Add(new TField { title = "時分", property = "day_hhmm", css = TCss.Center, width = 12 });
            fields.Add(new TField { title = "場地數", property = "site_count", css = TCss.Center, width = 12 });
            fields.Add(new TField { title = "各場地平均", property = "day_site_hhmm", css = TCss.Center, width = 27 });
            fields.Add(new TField { title = "平均分鐘", property = "day_site_minutes", css = TCss.Center, width = 12 });

            MapCharSetAndIndex(cfg, fields);

            int mnRow = 1;
            int wsRow = 1;

            //標題列
            SetHeadRow(cfg, sheet, wsRow, fields);
            wsRow++;

            var sorted = days.OrderBy(x => x.fight_day).ToList();
            var item = cfg.inn.newItem();
            for (var i = 0; i < sorted.Count; i++)
            {
                var x = sorted[i];

                item.setProperty("fight_day", x.fight_day);
                item.setProperty("day_minutes", x.day_minutes.ToString());
                item.setProperty("day_hhmm", x.show_hhmm);
                item.setProperty("site_count", x.site_count.ToString());
                item.setProperty("day_site_hhmm", x.show_site_hhmm);
                item.setProperty("day_site_minutes", x.show_site_minutes);

                SetItemCell(cfg, sheet, wsRow, item, fields);
                wsRow++;
            }

            //設定格線
            SetRangeBorder(sheet, fields, mnRow, wsRow);
            //設定欄寬
            SetColumnWidth(sheet, fields);
        }

        private void AppendTimeSheet(TConfig cfg, Spire.Xls.Workbook workbook, List<TSect> sects)
        {
            var sheet = workbook.CreateEmptySheet();
            sheet.Name = "預估時長";

            sheet.PageSetup.TopMargin = 0.6;
            sheet.PageSetup.LeftMargin = 0.5;
            sheet.PageSetup.RightMargin = 0.5;
            sheet.PageSetup.BottomMargin = 0.6;

            List<TField> fields = new List<TField>();
            fields.Add(new TField { title = "比賽日期", property = "fight_day", css = TCss.None, width = 12 });
            fields.Add(new TField { title = "量級", property = "sect_w", css = TCss.None, width = 36 });
            fields.Add(new TField { title = "人數", property = "count", css = TCss.Number, width = 7 });
            fields.Add(new TField { title = "場次", property = "event_count", css = TCss.Number, width = 7 });
            fields.Add(new TField { title = "每回合", property = "fight_time", css = TCss.Center, width = 8 });
            fields.Add(new TField { title = "回合數", property = "fight_round", css = TCss.Center, width = 8 });
            fields.Add(new TField { title = "分鐘時長", property = "event_minutes", css = TCss.Double, width = 12 });

            MapCharSetAndIndex(cfg, fields);

            var sorted = sects.OrderBy(x => x.in_fight_day).ThenBy(x => x.sort).ToList();

            int mnRow = 1;
            int wsRow = 1;

            var item = cfg.inn.newItem();
            var lastDay = "";
            for (var i = 0; i < sorted.Count; i++)
            {
                var sect = sorted[i];
                if (lastDay != sect.in_fight_day)
                {
                    lastDay = sect.in_fight_day;
                    //標題列
                    SetHeadRow(cfg, sheet, wsRow, fields);
                    wsRow++;
                }
                item.setProperty("fight_day", sect.in_fight_day);
                item.setProperty("sect_w", sect.display);
                item.setProperty("count", sect.persons.Count.ToString());
                item.setProperty("event_count", sect.event_count.ToString());
                item.setProperty("fight_time", sect.in_fight_time);
                item.setProperty("fight_round", sect.in_fight_round);
                item.setProperty("event_minutes", sect.event_minutes);

                SetItemCell(cfg, sheet, wsRow, item, fields);
                wsRow++;
            }

            //設定格線
            SetRangeBorder(sheet, fields, mnRow, wsRow);
            //設定欄寬
            SetColumnWidth(sheet, fields);
            //凍結窗格
            //sheet.FreezePanes(1, 2);
        }

        private void AppendSectsSheet(TConfig cfg, Spire.Xls.Workbook workbook, List<TReport> reports)
        {
            var sheet = workbook.CreateEmptySheet();
            sheet.Name = "組別摘要";

            sheet.PageSetup.TopMargin = 0.6;
            sheet.PageSetup.LeftMargin = 0.5;
            sheet.PageSetup.RightMargin = 0.5;
            sheet.PageSetup.BottomMargin = 0.6;

            List<TField> fields = new List<TField>();
            fields.Add(new TField { title = "序號", property = "no", css = TCss.Center, width = 4 });
            fields.Add(new TField { title = "項目", property = "in_l1", css = TCss.None, width = 14 });
            fields.Add(new TField { title = "組別", property = "sect_g", css = TCss.None, width = 18 });
            fields.Add(new TField { title = "量級", property = "sect_w", css = TCss.None, width = 36 });
            fields.Add(new TField { title = "人數", property = "count", css = TCss.Number, width = 8 });
            fields.Add(new TField { title = "場次", property = "event_count", css = TCss.Number, width = 8 });

            MapCharSetAndIndex(cfg, fields);

            int mnRow = 1;
            int wsRow = 1;

            //標題列
            SetHeadRow(cfg, sheet, wsRow, fields);
            wsRow++;

            var item = cfg.inn.newItem();
            var no = 1;
            for (var i = 0; i < reports.Count; i++)
            {
                var report = reports[i];
                for (var j = 0; j < report.blocks.Count; j++)
                {
                    var block = report.blocks[j];
                    for (var k = 0; k < block.sects.Count; k++)
                    {
                        var sect = block.sects[k];
                        var arr = sect.key.Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);

                        item.setProperty("no", no.ToString());
                        item.setProperty("in_l1", report.key.Replace("籤表", ""));
                        item.setProperty("sect_g", arr[0]);
                        item.setProperty("sect_w", sect.display);
                        item.setProperty("count", sect.persons.Count.ToString());
                        item.setProperty("event_count", sect.event_count.ToString());

                        SetItemCell(cfg, sheet, wsRow, item, fields);
                        no++;
                        wsRow++;
                    }
                }
            }

            //設定格線
            SetRangeBorder(sheet, fields, mnRow, wsRow);
            //設定欄寬
            SetColumnWidth(sheet, fields);
            //凍結窗格
            //sheet.FreezePanes(1, 2);
        }

        private void AppendSingleSheet(TConfig cfg, Spire.Xls.Workbook workbook, TReport report)
        {
            report.ridx = 1;
            report.groupName = report.key.Replace("組", "");
            report.genderM = report.key.Contains("國小") ? "男生組" : "男子組";
            report.genderW = report.key.Contains("國小") ? "女生組" : "女子組";


            var sheet = workbook.CreateEmptySheet();
            sheet.Name = report.groupName;

            //sheet.PageSetup.FitToPagesWide = 1;
            //sheet.PageSetup.FitToPagesTall = 1;
            sheet.PageSetup.TopMargin = 0.6;
            sheet.PageSetup.LeftMargin = 0.6;
            sheet.PageSetup.RightMargin = 0.6;
            sheet.PageSetup.BottomMargin = 0.6;

            for (var i = 0; i < report.blocks.Count; i++)
            {
                var block = report.blocks[i];
                var m_sects = block.sects.FindAll(x => x.key.Contains("男"));
                var w_sects = block.sects.FindAll(x => x.key.Contains("女"));

                AppendOneSectBlock(cfg, sheet, report, block, m_sects, report.genderM);
                AppendOneSectBlock(cfg, sheet, report, block, w_sects, report.genderW);
            }

            sheet.Columns[8].AutoFitColumns();
            sheet.Columns[9].AutoFitColumns();
        }

        private void AppendOneSectBlock(TConfig cfg, Spire.Xls.Worksheet sheet, TReport rpt, TBlock block, List<TSect> sects, string gender)
        {
            if (sects == null || sects.Count == 0) return;

            var titlePos = "A" + rpt.ridx + ":J" + rpt.ridx;
            var titleTxt = block.key;
            SetStrCell(cfg, sheet, titlePos, titleTxt, 14, 30, true, true, HA.C);
            rpt.ridx++;
            rpt.ridx++;

            for (var i = 0; i < sects.Count; i++)
            {
                var sect = sects[i];
                var sectPos = "B" + rpt.ridx;
                var sectTxt = sect.display;
                SetStrCell(cfg, sheet, sectPos, sectTxt, 12, 0, true, false, HA.L);

                var cntPos1 = "H" + rpt.ridx;
                var cntTxt1 = "(共";
                SetStrCell(cfg, sheet, cntPos1, cntTxt1, 12, 0, false, false, HA.R);

                var cntPos2 = "I" + rpt.ridx;
                var cntTxt2 = sect.persons.Count;
                SetIntCell(cfg, sheet, cntPos2, cntTxt2, 12, 0, false, false, HA.C);

                var cntPos3 = "J" + rpt.ridx;
                var cntTxt3 = "人)";
                SetStrCell(cfg, sheet, cntPos3, cntTxt3, 12, 0, false, false, HA.L);
                rpt.ridx++;
                rpt.ridx++;

                var rcnt = sect.persons.Count % 2 == 0 ? sect.persons.Count / 2 : (sect.persons.Count / 2) + 1;
                var rsidx = rpt.ridx;
                var mx = sect.persons.Count;
                var cidx = 0;
                while (cidx < rcnt)
                {
                    var p = sect.persons[cidx];
                    var playerPos = "B" + rpt.ridx;
                    var playerTxt = p.in_name;
                    SetStrCell(cfg, sheet, playerPos, playerTxt, 12, 0, false, false, HA.L);

                    var orgPos = "C" + rpt.ridx;
                    var orgTxt = p.map_org_name;
                    SetStrCell(cfg, sheet, orgPos, orgTxt, 12, 0, false, false, HA.L);

                    cidx++;
                    rpt.ridx++;
                }
                while (cidx < mx)
                {
                    var p = sect.persons[cidx];
                    var playerPos = "F" + rsidx;
                    var playerTxt = p.in_name;
                    SetStrCell(cfg, sheet, playerPos, playerTxt, 12, 0, false, false, HA.L);

                    var orgPos = "G" + rsidx;
                    var orgTxt = p.map_org_name;
                    SetStrCell(cfg, sheet, orgPos, orgTxt, 12, 0, false, false, HA.L);

                    cidx++;
                    rsidx++;
                }
                rpt.ridx++;
            }
            rpt.ridx++;
        }

        private void SetStrCell(TConfig cfg, Spire.Xls.Worksheet sheet
            , string pos
            , string text
            , int size
            , int height = 0
            , bool needBold = false
            , bool needMerge = false
            , HA ha = HA.C)
        {
            var range = sheet.Range[pos];
            if (needMerge) range.Merge();
            range.Text = text;
            SetUtlCell(cfg, range, size, height, needBold, ha);
        }

        private void SetIntCell(TConfig cfg, Spire.Xls.Worksheet sheet
            , string pos
            , int text
            , int size
            , int height = 0
            , bool needBold = false
            , bool needMerge = false
            , HA ha = HA.C)
        {
            var range = sheet.Range[pos];
            if (needMerge) range.Merge();
            range.NumberValue = text;
            range.NumberFormat = "0";
            SetUtlCell(cfg, range, size, height, needBold, ha);
        }

        private void SetUtlCell(TConfig cfg, Spire.Xls.CellRange range
            , int size
            , int height
            , bool needBold
            , HA ha = HA.C)
        {
            range.Style.Font.FontName = cfg.FontName;
            range.Style.Font.Size = size;

            if (needBold) range.Style.Font.IsBold = true;
            if (height > 0) range.RowHeight = height;

            range.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;

            switch (ha)
            {
                case HA.C:
                    range.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                    break;
                case HA.L:
                    range.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Left;
                    break;
                case HA.R:
                    range.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Right;
                    break;
            }
        }

        private enum HA
        {
            L = 100,
            C = 200,
            R = 300
        }

        /// <summary>
        /// 取得與會者資料
        /// </summary>
        private Item GetMeetingUsers(TConfig cfg)
        {
            string sql = @"
                SELECT
                    t1.in_l1
                    , t1.in_l2
                    , t1.in_l3
                    , t1.in_index
                    , ISNULL(t1.in_l1, '') + '-' + ISNULL(t1.in_l2, '') + '-' + ISNULL(t1.in_l3, '') + '-' + ISNULL(t1.in_index, '') + '-' + ISNULL(t1.in_creator_sno, '') AS 'in_team_key'
                    , t1.in_l1 + '-' + t1.in_l3 AS 'program_name'
                    , t1.in_current_org
                    , t1.in_team
                    , t1.in_name
                    , t1.in_sno
                    , t1.in_gender
                    , t1.in_birth
                    , t1.in_creator
                    , t1.in_creator_sno
                    , t2.in_tel AS 'in_creator_tel'
                    , t1.in_expense
                    , t1.in_paynumber
                    , t1.in_exe_a1
                    , t1.in_exe_a2
                    , t1.in_exe_a3
                    , t1.in_exe_a4
					, t1.in_stuff_b1
					, t2.in_email AS 'creator_email'
					, t2.in_tel   AS 'creator_tel'
                    , t3.pay_bool
                    , t3.in_pay_amount_exp
                    , t3.in_pay_amount_real
					, t4.in_type
                    , t4.in_short_org AS 'map_org_name'
                    , t21.in_team_count
                    , t21.in_event_count
                    , t21.in_fight_day
                    , t21.in_fight_time
                    , t21.in_fight_round
                FROM 
                    IN_MEETING_USER t1 WITH(NOLOCK)
                LEFT OUTER JOIN
                    IN_RESUME t2 WITH(NOLOCK)
                    ON t2.in_sno = t1.in_creator_sno
                LEFT OUTER JOIN
                    IN_MEETING_PAY t3 WITH(NOLOCK)
                    ON t3.in_meeting = t1.source_id 
                    AND t3.item_number = t1.in_paynumber
                LEFT OUTER JOIN
                    IN_ORG_MAP t4 WITH(NOLOCK)
                    ON t4.in_stuff_b1 = t1.in_stuff_b1 
				LEFT OUTER JOIN
					VU_MEETING_SVY_L1 t11
					ON t11.source_id = t1.source_id
					AND t11.in_value = t1.in_l1
				LEFT OUTER JOIN
					VU_MEETING_SVY_L2 t12
					ON t12.source_id = t1.source_id
					AND t12.in_filter = t1.in_l1
					AND t12.in_value = t1.in_l2
				LEFT OUTER JOIN
					VU_MEETING_SVY_L3 t13
					ON t13.source_id = t1.source_id
					AND t13.in_grand_filter = t1.in_l1
					AND t13.in_filter = t1.in_l2
					AND t13.in_value = t1.in_l3
				LEFT OUTER JOIN
				    IN_MEETING_PROGRAM t21 WITH(NOLOCK)
				    ON t21.in_meeting = t1.source_id
				    AND t21.in_l1 = t1.in_l1
				    AND t21.in_l2 = t1.in_l2
				    AND t21.in_l3 = t1.in_l3
                WHERE 
                    t1.source_id = '{#meeting_id}'
                    AND t1.in_l1 NOT IN (N'隊職員', N'保險代表人')
                ORDER BY 
                    t11.sort_order
                    , t12.sort_order
                    , t13.sort_order
					, t1.in_stuff_b1
					, t1.in_name
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item items = cfg.inn.applySQL(sql);

            if (items.isError() || items.getItemCount() <= 0)
            {
                throw new Exception("查無與會者資料");
            }

            return items;
        }

        private List<TSect> MergeReportSects(TConfig cfg, List<TReport> reports)
        {
            var sects = new List<TSect>();
            for (var i = 0; i < reports.Count; i++)
            {
                var report = reports[i];
                for (var j = 0; j < report.blocks.Count; j++)
                {
                    var block = report.blocks[j];
                    for (var k = 0; k < block.sects.Count; k++)
                    {
                        var sect = block.sects[k];

                        var event_count = GetIntVal(sect.in_event_count);
                        if (event_count <= 0) event_count = sect.persons.Count - 1;
                        if (event_count <= 0) event_count = 0;
                        sect.event_count = event_count;

                        if (sect.persons.Count <= 1) continue;
                        sects.Add(sect);
                    }
                }
            }
            return sects;
        }

        private List<TReport> MapReports(TConfig cfg, List<TPerson> rows)
        {
            var sort = 100;
            var reports = new List<TReport>();
            for (var i = 0; i < rows.Count; i++)
            {
                var p = rows[i];
                var report = reports.Find(x => x.key == p.groupName);
                if (report == null)
                {
                    report = new TReport { key = p.groupName, name = p.sheetName, blocks = new List<TBlock>() };
                    reports.Add(report);
                }

                var block = report.blocks.Find(x => x.key == p.in_l1 + "-" + p.in_l2);
                if (block == null)
                {
                    block = new TBlock
                    {
                        key = p.in_l1 + "-" + p.in_l2,
                        name = p.in_l1 + "-" + p.in_l2,
                        sects = new List<TSect>(),
                    };
                    report.blocks.Add(block);
                }

                var sect = block.sects.Find(x => x.key == p.program_name);
                if (sect == null)
                {
                    sect = new TSect
                    {
                        key = p.program_name,
                        persons = new List<TPerson>(),
                        in_team_count = p.in_team_count,
                        in_event_count = p.in_event_count,
                        in_fight_day = p.in_fight_day,
                        in_fight_time = p.in_fight_time,
                        in_fight_round = p.in_fight_round,
                        sort = sort,
                    };
                    sect.display = FixSectName(sect.key);
                    sect.fight_seconds = GetFightSeconds(sect);
                    sect.event_seconds = sect.fight_seconds * GetIntVal(sect.in_event_count);
                    sect.event_minutes = GetFightMinutes(sect.event_seconds);
                    block.sects.Add(sect);
                    sort += 100;
                }
                sect.persons.Add(p);
            }
            return reports;
        }

        private class TReport
        {
            public string key { get; set; }
            public string name { get; set; }
            public int ridx { get; set; }
            public string groupName { get; set; }
            public string genderM { get; set; }
            public string genderW { get; set; }

            public List<TBlock> blocks { get; set; }
        }

        private class TBlock
        {
            public string key { get; set; }
            public string name { get; set; }
            public List<TSect> sects { get; set; }
        }

        private class TSect
        {
            public string key { get; set; }
            public string display { get; set; }
            public string in_team_count { get; set; }
            public string in_event_count { get; set; }
            public string in_fight_day { get; set; }
            public string in_fight_time { get; set; }
            public string in_fight_round { get; set; }
            public int fight_seconds { get; set; }
            public int event_seconds { get; set; }
            public string event_minutes { get; set; }
            public List<TPerson> persons { get; set; }
            public int event_count { get; set; }
            public int sort { get; set; }
        }

        /// <summary>
        /// 用身分證號推算性別
        /// </summary>
        private string GetGenderBySno(string value)
        {
            if (value == "" || value.Length < 2)
            {
                return "";
            }
            if (value[1] == '1') return "男";
            if (value[1] == '2') return "女";
            return "";
        }

        private TExport ExportInfo(TConfig cfg, string in_name)
        {
            Item itmXls = cfg.inn.applyMethod("In_Meeting_Excel_Path", "<in_name>" + in_name + "</in_name>");
            string Export_Path = itmXls.getProperty("export_path", "");
            string Template_Path = itmXls.getProperty("template_path", "");

            TExport result = new TExport
            {
                template_Path = Template_Path,
                export_Path = Export_Path,
            };

            return result;
        }

        //設定資料列
        private void SetItemCell(TConfig cfg, Spire.Xls.Worksheet sheet, int wsRow, Item item, List<TField> fields)
        {
            foreach (var field in fields)
            {
                var cr = field.cs + wsRow;
                var va = "";

                if (field.getValue != null)
                {
                    va = field.getValue(cfg, field, item);
                }
                else if (field.property != "")
                {
                    va = item.getProperty(field.property, "");
                }

                SetCell(cfg, sheet, cr, va, field.css);
            }
        }

        //設定資料格
        private void SetCell(TConfig cfg, Spire.Xls.Worksheet sheet, string cr, string value, TCss css)
        {
            var range = sheet.Range[cr];
            range.Style.Font.FontName = cfg.FontName;
            range.Style.Font.Size = 12;

            switch (css)
            {
                case TCss.None:
                    range.Text = value;
                    break;
                case TCss.Center:
                    range.Text = value;
                    range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                    break;
                case TCss.Text:
                    range.Text = "'" + value;
                    range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                    break;
                case TCss.Number:
                    range.NumberValue = GetIntVal(value);
                    range.NumberFormat = "0";
                    range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Right;
                    break;
                case TCss.Money:
                    range.NumberValue = GetIntVal(value);
                    range.NumberFormat = "$ #,##0";
                    break;
                case TCss.Double:
                    range.NumberValue = GetDbl(value);
                    range.NumberFormat = "0";
                    range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Right;
                    break;
                case TCss.Day:
                    range.Text = GetDateTimeValue(value, "yyyy/MM/dd");
                    range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                    break;
            }
        }

        private void SetMoney(Spire.Xls.Worksheet sheet, string cr, double value)
        {
            var range = sheet.Range[cr];
            range.NumberValue = value;
            range.NumberFormat = "$ #,##0";
        }

        //設定標題列
        private void SetHead(TConfig cfg, Spire.Xls.Worksheet sheet, string cr, string value)
        {
            var range = sheet.Range[cr];
            range.Style.Font.FontName = cfg.FontName;
            range.Text = value;
            range.Style.Font.Size = 12;
            range.Style.Font.IsBold = true;
            range.Style.Font.Color = System.Drawing.Color.White;
            range.Style.Color = System.Drawing.Color.FromArgb(41, 92, 144);//#295C90
            range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
        }

        //設定格線
        private void SetRangeBorder(Spire.Xls.Worksheet sheet, string pos)
        {
            var range = sheet.Range[pos];
            range.BorderInside(Spire.Xls.LineStyleType.Thin, System.Drawing.Color.Black);
            range.BorderAround(Spire.Xls.LineStyleType.Thin, System.Drawing.Color.Black);
        }

        //設定格線
        private void SetRangeBorder(Spire.Xls.Worksheet sheet, List<TField> fields, int mnRow, int wsRow)
        {
            var tbl_ps = fields.First().cs + mnRow;
            var tbl_pe = fields.Last().cs + (wsRow - 1);
            SetRangeBorder(sheet, tbl_ps + ":" + tbl_pe);
        }

        private void MapCharSet(TConfig cfg, List<TField> fields)
        {
            foreach (var field in fields)
            {
                field.cs = cfg.CharSet[field.ci];
            }
        }

        private void MapCharSetAndIndex(TConfig cfg, List<TField> fields)
        {
            for (int i = 0; i < fields.Count; i++)
            {
                var field = fields[i];
                field.ci = i + 1;
                field.cs = cfg.CharSet[field.ci];
            }
        }

        private void SetHeadRow(TConfig cfg, Spire.Xls.Worksheet sheet, int wsRow, List<TField> fields)
        {
            foreach (var field in fields)
            {
                string cr = field.cs + wsRow;
                SetHead(cfg, sheet, cr, field.title);
            }
        }

        private void AutoFit(Spire.Xls.Worksheet sheet, List<TField> fields)
        {
            foreach (var field in fields)
            {
                sheet.Columns[field.ci - 1].AutoFitColumns();
            }
        }

        private void SetColumnWidth(Spire.Xls.Worksheet sheet, List<TField> fields)
        {
            foreach (var field in fields)
            {
                sheet.Columns[field.ci - 1].ColumnWidth = field.width;
            }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string meeting_id { get; set; }
            public Item itmMeeting { get; set; }
            public string mt_title { get; set; }
            public string FontName { get; set; }
            public string[] CharSet { get; set; }
        }

        private class TExport
        {
            public string template_Path { get; set; }
            public string export_Path { get; set; }
        }

        private class TField
        {
            public string title { get; set; }
            public string property { get; set; }
            public TCss css { get; set; }
            public int ci { get; set; }
            public string cs { get; set; }
            public int width { get; set; }
            public Func<TConfig, TField, Item, string> getValue { get; set; }

            public bool is_merge { get; set; }
        }

        private List<TOrg> MapOrgList(TConfig cfg, List<TPerson> rows)
        {
            var orgs = new List<TOrg>();
            for (int i = 0; i < rows.Count; i++)
            {
                var p = rows[i];
                var org = orgs.Find(x => x.in_stuff_b1 == p.in_stuff_b1);
                if (org == null)
                {
                    org = new TOrg
                    {
                        in_type = p.in_type,
                        in_stuff_b1 = p.in_stuff_b1,
                        in_current_org = p.in_current_org,
                        in_name = p.creator_name,
                        in_email = p.creator_email,
                        in_tel = p.creator_tel,
                        leaders = new List<TPerson>(),
                        managers = new List<TPerson>(),
                        coaches_m = new List<TPerson>(),
                        coaches_w = new List<TPerson>(),
                        insurances = new List<TPerson>(),
                        players = new List<TPerson>(),
                    };
                    orgs.Add(org);
                }
                AnalysisMUser(cfg, org, p);
            }
            return orgs;
        }

        private void AnalysisMUser(TConfig cfg, TOrg org, TPerson p)
        {
            if (p.isPlayer)
            {
                org.players.Add(p);
            }
            else if (p.isInsurance)
            {
                org.insurances.Add(p);
            }
            else if (p.isLeader)
            {
                org.leaders.Add(p);
            }
            else if (p.isManager)
            {
                org.managers.Add(p);
            }
            else if (p.isCoachM)
            {
                org.coaches_m.Add(p);
            }
            else if (p.isCoachW)
            {
                org.coaches_w.Add(p);
            }
        }

        private class TOrg
        {
            public string in_type { get; set; }
            public string in_stuff_b1 { get; set; }
            public string in_current_org { get; set; }
            public string map_short_org { get; set; }
            public string in_name { get; set; }
            public string in_email { get; set; }
            public string in_tel { get; set; }
            public List<TPerson> leaders { get; set; }
            public List<TPerson> managers { get; set; }
            public List<TPerson> coaches_m { get; set; }
            public List<TPerson> coaches_w { get; set; }
            public List<TPerson> insurances { get; set; }
            public List<TPerson> players { get; set; }
        }

        private List<TPerson> MapMUserList(TConfig cfg, Item items)
        {
            var list = new List<TPerson>();
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                list.Add(MapMUser(cfg, item));
            }
            return list;
        }

        private TPerson MapMUser(TConfig cfg, Item item)
        {
            var x = new TPerson
            {
                item = item,
                in_type = item.getProperty("in_type", ""),
                in_stuff_b1 = item.getProperty("in_stuff_b1", ""),
                in_current_org = item.getProperty("in_current_org", ""),
                map_org_name = item.getProperty("map_org_name", ""),
                in_l1 = item.getProperty("in_l1", ""),
                in_l2 = item.getProperty("in_l2", ""),
                in_l3 = item.getProperty("in_l3", ""),
                program_name = item.getProperty("program_name", ""),
                in_name = item.getProperty("in_name", ""),
                in_gender = item.getProperty("in_gender", ""),
                creator_name = item.getProperty("in_creator", ""),
                creator_email = item.getProperty("creator_email", ""),
                creator_tel = item.getProperty("creator_tel", ""),
                in_team_count = item.getProperty("in_team_count", "0"),
                in_event_count = item.getProperty("in_event_count", "0"),
                in_fight_day = item.getProperty("in_fight_day", ""),
                in_fight_time = item.getProperty("in_fight_time", ""),
                in_fight_round = item.getProperty("in_fight_round", "0"),
            };

            if (x.in_l2.Contains("自由式")) x.in_l2 = "自由式";
            else if (x.in_l2.Contains("希羅式")) x.in_l2 = "希羅式";
            x.program_name = x.in_l1 + "-" + x.in_l2 + "-" + x.in_l3;

            switch (x.in_l1)
            {
                case "隊職員":
                    x.isStaff = true;
                    AnalysisStaff(cfg, x);
                    x.groupName = x.in_l1;
                    x.sheetName = x.in_l1;
                    break;
                case "保險代表人":
                    x.isInsurance = true;
                    x.groupName = x.in_l1;
                    x.sheetName = x.in_l1;
                    break;
                default:
                    x.isPlayer = true;
                    AnalysisPlayer(cfg, x);
                    break;
            }
            x.item.setProperty("groupName", x.groupName);
            return x;
        }

        private void AnalysisPlayer(TConfig cfg, TPerson x)
        {
            if (x.in_l1.Contains("大專"))
            {
                x.groupName = "大專社會組";
            }
            else if (x.in_l1.Contains("高中"))
            {
                x.groupName = "高中組";
            }
            else if (x.in_l1.Contains("國中"))
            {
                x.groupName = "國中組";
            }
            else if (x.in_l1.Contains("國小"))
            {
                x.groupName = "國小組";
            }
            else
            {
                x.groupName = x.in_l1;
            }
            x.sheetName = x.groupName + "籤表";
        }
        private void AnalysisStaff(TConfig cfg, TPerson x)
        {
            switch (x.in_l2)
            {
                case "領隊":
                    x.isLeader = true;
                    break;
                case "管理":
                    x.isManager = true;
                    break;
                case "男教練":
                case "男子隊教練":
                    x.isCoachM = true;
                    break;
                case "女教練":
                case "女子隊教練":
                    x.isCoachW = true;
                    break;
            }
        }

        private class TPerson
        {
            public bool isStaff { get; set; }
            public bool isLeader { get; set; }
            public bool isManager { get; set; }
            public bool isCoachM { get; set; }
            public bool isCoachW { get; set; }
            public bool isPlayer { get; set; }
            public bool isInsurance { get; set; }
            public string groupName { get; set; }
            public string sheetName { get; set; }

            public string in_type { get; set; }
            public string in_stuff_b1 { get; set; }
            public string in_current_org { get; set; }
            public string map_org_name { get; set; }
            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }
            public string program_name { get; set; }
            public string in_name { get; set; }
            public string in_gender { get; set; }
            public string creator_name { get; set; }
            public string creator_email { get; set; }
            public string creator_tel { get; set; }
            public string in_team_count { get; set; }
            public string in_event_count { get; set; }
            public string in_fight_day { get; set; }
            public string in_fight_time { get; set; }
            public string in_fight_round { get; set; }
            public Item item { get; set; }

        }

        private enum TCss
        {
            None = 0,
            Center = 10,
            Text = 100,
            Number = 200,
            Money = 300,
            Day = 400,
            Double = 500
        }

        private string[] GetCharSet()
        {
            return new string[] { "", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
        }

        private int GetIntVal(string value, int def = 0)
        {
            if (value == "") return 0;
            int result = def;
            int.TryParse(value, out result);
            return result;
        }

        private double GetDbl(string value, double defV = 0)
        {
            if (value == "") return 0;
            double result = defV;
            if (double.TryParse(value, out result)) return result;
            return defV;
        }

        //轉換日期
        private string GetDateTimeValue(string value, string format, bool add8Hours = true)
        {
            if (value == "") return "";
            DateTime dt = Convert.ToDateTime(value);
            if (add8Hours) dt = dt.AddHours(8);
            return dt.ToString(format);
        }

        private string FixSectName(string value)
        {
            var result = "";
            var arr = value.Split(new char[] { '：' }, StringSplitOptions.RemoveEmptyEntries);
            var w1 = arr.Length > 0 ? arr[0] : "";
            var w2 = arr.Length > 1 ? arr[1] : "";

            if (w2.Contains("以上至"))
            {
                w2 = w2.Replace(".1公斤以上至", "~");
                w2 = w2.Replace(".0公斤以下", "");
                var arr2 = w2.Split(new char[] { '~' }, StringSplitOptions.RemoveEmptyEntries);
                var weight = arr2.Length > 1 ? arr2[1] : "";
                var w31 = w1.Replace("-自由式-", "-自由式 : ").Replace("-希羅式-", "-希羅式 : ");
                result = w31 + "(-" + weight + "公斤)";
            }
            else if (w2.Contains("公斤以上"))
            {
                w2 = w2.Replace(".1 公斤以上", "");
                var w32 = w1.Replace("-自由式-", "-自由式 : ").Replace("-希羅式-", "-希羅式 : ");
                result = w32 + "(+" + w2 + ".1公斤)";
            }
            else if (value.Contains("超輕量級"))
            {
                w2 = w2.Replace(".0公斤以下", "");
                var w33 = w1.Replace("-自由式-", "-自由式 : ").Replace("-希羅式-", "-希羅式 : ");
                result = w33 + "(-" + w2 + "公斤)";
                result = result.Replace(" 公斤以下公斤", "kg 以下公斤");
            }
            else
            {
                w2 = w2.Replace(".0公斤以下", "");
                var w33 = w1.Replace("-自由式-", "-自由式 : ").Replace("-希羅式-", "-希羅式 : ");
                result = w33 + "(-" + w2 + "公斤)";
            }
            result = result.Replace("kg 以上公斤", "公斤以上");
            result = result.Replace("kg 以下公斤", "公斤以下");
            result = result.Replace("公斤以下公斤", "公斤以下");
            result = result.Replace("(-公斤)", "");

            if (result.Contains("以上")) result = result.Replace("(-", "(+");

            return result;
        }

        private class TDay
        {
            public string fight_day { get; set; }
            public int site_count { get; set; }
            public double day_minutes { get; set; }

            public string show_hhmm { get; set; }
            public string show_site_hhmm { get; set; }
            public string show_site_minutes { get; set; }
        }

        private List<TDay> MapDayList(TConfig cfg, List<TSect> sects)
        {
            var itmSiteCount = cfg.inn.applySQL("SELECT COUNT(*) AS 'cnt' FROM IN_MEETING_SITE WITH(NOLOCK) WHERE in_meeting = '" + cfg.meeting_id + "'");
            var site_count = itmSiteCount.getResult() == "" ? 0 : GetIntVal(itmSiteCount.getProperty("cnt", "0"));

            var map = new Dictionary<string, TDay>();
            for (var i = 0; i < sects.Count; i++)
            {
                var sect = sects[i];
                var minutes = GetDbl(sect.event_minutes);

                var key = sect.in_fight_day;
                if (!map.ContainsKey(key))
                {
                    map.Add(key, new TDay { fight_day = key, site_count = site_count, day_minutes = 0 });
                }
                var x = map[key];
                x.day_minutes += minutes;
            }
            var days = map.Values.ToList();
            for (var i = 0; i < days.Count; i++)
            {
                var x = days[i];
                var dv = (int)x.day_minutes;
                var dm = dv % 60;
                var dh = (dv - dm) / 60;

                var sv = dv / x.site_count;
                var sm = sv % 60;
                var sh = (sv - sm) / 60;
                x.show_hhmm = dh.ToString().PadLeft(2, '0') + ":" + dm.ToString().PadLeft(2, '0');
                x.show_site_hhmm = "約 " + sh + " 個小時又 " + sm + " 分鐘";
                x.show_site_minutes = sv.ToString();
            }
            return days;
        }

        private string GetFightMinutes(int seconds)
        {
            var d = seconds % 60;
            if (d > 0)
            {
                var m = (seconds - d) / 60;
                return m + ".5";
            }
            else
            {
                return (seconds / 60).ToString();
            }
        }

        private int GetFightSeconds(TSect sect)
        {
            if (sect.in_fight_time == "") return 0;
            if (sect.in_fight_round == "" || sect.in_fight_round == "0") return 0;

            var arr = sect.in_fight_time.Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
            var m = arr != null && arr.Length > 0 ? GetIntVal(arr[0]) : 0;
            var s = arr != null && arr.Length > 1 ? GetIntVal(arr[1]) : 0;
            var r = GetIntVal(sect.in_fight_round);

            var seconds = (m * 60 + s) * r;
            return seconds;
        }
    }
}