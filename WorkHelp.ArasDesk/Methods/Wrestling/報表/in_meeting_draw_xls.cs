﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;
using InnSport.Core.Models.Business;
using System.Web.WebSockets;
using System.Net.NetworkInformation;

namespace WorkHelp.ArasDesk.Methods.Wrestling.報表
{
    public class in_meeting_draw_xls : Item
    {
        public in_meeting_draw_xls(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 新籤表 (含亂數抽籤結果)
                輸入: meeting_id
                輸出: 
                    1. 報名總表
                    2. 參賽各組統計
                    3. 競賽項目
                        個人組籤表
                人員: lina
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]in_meeting_draw_xls";

            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "input dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                meeting_id = itmR.getProperty("meeting_id", ""),
                CharSet = GetCharSet(),
            };

            if (cfg.meeting_id == "")
            {
                throw new Exception("賽事 id 不得為空白");
            }

            cfg.itmMeeting = cfg.inn.applySQL("SELECT in_title FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            cfg.mt_title = cfg.itmMeeting.getProperty("in_title", "");

            var exp = ExportInfo(cfg, "weight_path");

            //取得與會者資料
            var itmMUsers = GetMeetingUsers(cfg);
            var musers = MapMUserList(cfg, itmMUsers);
            var orgSource = MapOrgList(cfg, musers);
            var orgs = orgSource.OrderBy(x => x.in_stuff_b1).ToList();
            var reports = MapReports(cfg, musers);

            //試算表
            var workbook = new Spire.Xls.Workbook();

            //組別摘要
            AppendSectsSheet(cfg, workbook, reports);
            //單位摘要
            AppendOrgsSheet(cfg, workbook, orgs);
            //單位明細
            AppendOrgDetailSheet(cfg, workbook, orgs);

            //分組籤表與報名名單
            foreach (var report in reports)
            {
                switch (report.key)
                {
                    case "隊職員":
                    case "保險代表人":
                        AppendStaffSheet(cfg, workbook, report);
                        break;
                    default:
                        AppendSingleSheet(cfg, workbook, report);
                        break;
                }
            }

            workbook.Worksheets[0].Remove();
            workbook.Worksheets[0].Remove();
            workbook.Worksheets[0].Remove();

            string ext_name = ".xlsx";
            string xls_name = cfg.mt_title + "_籤表_" + DateTime.Now.ToString("MMdd_HHmmss");
            string xls_file = exp.export_Path + "\\" + xls_name + ext_name;
            string xls_url = xls_name + ext_name;

            workbook.SaveToFile(xls_file, Spire.Xls.ExcelVersion.Version2013);

            itmR.setProperty("xls_name", xls_url);

            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

            return itmR;
        }

        private void AppendSectsSheet(TConfig cfg, Spire.Xls.Workbook workbook, List<TReport> reports)
        {
            var sheet = workbook.CreateEmptySheet();
            sheet.Name = "組別摘要";

            List<TField> fields = new List<TField>();
            fields.Add(new TField { title = "序號", property = "no", css = TCss.Center, width = 5 });
            fields.Add(new TField { title = "項目", property = "in_l1", css = TCss.None, width = 15 });
            fields.Add(new TField { title = "組別", property = "sect_g", css = TCss.None, width = 20 });
            fields.Add(new TField { title = "量級", property = "sect_w", css = TCss.None, width = 40 });
            fields.Add(new TField { title = "人數", property = "count", css = TCss.Number, width = 10 });

            MapCharSetAndIndex(cfg, fields);

            int mnRow = 1;
            int wsRow = 1;

            //標題列
            SetHeadRow(sheet, wsRow, fields);
            wsRow++;

            var item = cfg.inn.newItem();
            var no = 1;
            for (var i = 0; i < reports.Count; i++)
            {
                var report = reports[i];
                for (var j = 0; j < report.sects.Count; j++)
                {
                    var sect = report.sects[j];
                    var arr = sect.key.Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);

                    item.setProperty("no", no.ToString());
                    item.setProperty("in_l1", report.key.Replace("籤表", ""));
                    item.setProperty("sect_g", arr[0]);
                    item.setProperty("sect_w", arr.Length > 1 ? arr[1] : "");
                    item.setProperty("count", sect.persons.Count.ToString());

                    SetItemCell(cfg, sheet, wsRow, item, fields);
                    no++;
                    wsRow++;
                }
            }

            //設定格線
            SetRangeBorder(sheet, fields, mnRow, wsRow);
            //設定欄寬
            SetColumnWidth(sheet, fields);
            //凍結窗格
            //sheet.FreezePanes(1, 2);
        }

        private void AppendOrgsSheet(TConfig cfg, Spire.Xls.Workbook workbook, List<TOrg> orgs)
        {
            var sheet = workbook.CreateEmptySheet();
            sheet.Name = "單位摘要";

            List<TField> fields = new List<TField>();
            fields.Add(new TField { title = "序號", property = "no", css = TCss.Center, width = 5 });
            fields.Add(new TField { title = "單位分類", property = "in_type", css = TCss.None, width = 15 });
            fields.Add(new TField { title = "單位", property = "in_current_org", css = TCss.None, width = 32 });
            fields.Add(new TField { title = "姓名", property = "creator_name", css = TCss.None, width = 20 });
            fields.Add(new TField { title = "電子信箱", property = "creator_email", css = TCss.None, width = 20 });
            fields.Add(new TField { title = "手機號碼", property = "creator_tel", css = TCss.None, width = 12 });
            fields.Add(new TField { title = "領隊", property = "c1", css = TCss.Number, width = 10 });
            fields.Add(new TField { title = "管理", property = "c2", css = TCss.Number, width = 10 });
            fields.Add(new TField { title = "男子隊教練", property = "c3", css = TCss.Number, width = 10 });
            fields.Add(new TField { title = "女子隊教練", property = "c4", css = TCss.Number, width = 10 });
            fields.Add(new TField { title = "保險代理人", property = "c5", css = TCss.Number, width = 10 });
            fields.Add(new TField { title = "選手", property = "c6", css = TCss.Number, width = 10 });

            MapCharSetAndIndex(cfg, fields);

            int mnRow = 1;
            int wsRow = 1;

            //標題列
            SetHeadRow(sheet, wsRow, fields);
            wsRow++;

            var item = cfg.inn.newItem();
            var no = 1;
            for (var i = 0; i < orgs.Count; i++)
            {
                var org = orgs[i];
                item.setProperty("no", no.ToString());
                item.setProperty("in_type", org.in_type);
                item.setProperty("in_current_org", org.in_current_org);
                item.setProperty("creator_name", org.in_name);
                item.setProperty("creator_email", org.in_email);
                item.setProperty("creator_tel", org.in_tel);
                item.setProperty("c1", org.leaders.Count.ToString());
                item.setProperty("c2", org.managers.Count.ToString());
                item.setProperty("c3", org.coaches_m.Count.ToString());
                item.setProperty("c4", org.coaches_w.Count.ToString());
                item.setProperty("c5", org.insurances.Count.ToString());
                item.setProperty("c6", org.players.Count.ToString());

                SetItemCell(cfg, sheet, wsRow, item, fields);
                no++;
                wsRow++;
            }

            //設定格線
            SetRangeBorder(sheet, fields, mnRow, wsRow);
            //設定欄寬
            SetColumnWidth(sheet, fields);
            //凍結窗格
            //sheet.FreezePanes(1, 2);
        }

        private void AppendOrgDetailSheet(TConfig cfg, Spire.Xls.Workbook workbook, List<TOrg> orgs)
        {
            var sheet = workbook.CreateEmptySheet();
            sheet.Name = "單位清單";

            List<TField> fields = new List<TField>();
            fields.Add(new TField { title = "單位分類", property = "in_type", css = TCss.None, width = 15 });
            fields.Add(new TField { title = "單位", property = "in_current_org", css = TCss.None, width = 30 });
            fields.Add(new TField { title = "群組", property = "groupName", css = TCss.None, width = 10 });
            fields.Add(new TField { title = "序號", property = "no", css = TCss.Center, width = 5 });
            fields.Add(new TField { title = "姓名", property = "in_name", css = TCss.None, width = 15 });
            fields.Add(new TField { title = "組別", property = "in_l1", css = TCss.None, width = 18 });
            fields.Add(new TField { title = "量級", property = "in_l3", css = TCss.None, width = 40 });

            MapCharSetAndIndex(cfg, fields);

            int mnRow = 1;
            int wsRow = 1;
            for (var i = 0; i < orgs.Count; i++)
            {
                //標題列
                SetHeadRow(sheet, wsRow, fields);
                wsRow++;

                var org = orgs[i];
                AppendPersonRows(cfg, sheet, fields, wsRow, org, org.leaders);
                wsRow += org.leaders.Count;

                AppendPersonRows(cfg, sheet, fields, wsRow, org, org.managers);
                wsRow += org.managers.Count;

                AppendPersonRows(cfg, sheet, fields, wsRow, org, org.coaches_m);
                wsRow += org.coaches_m.Count;

                AppendPersonRows(cfg, sheet, fields, wsRow, org, org.coaches_w);
                wsRow += org.coaches_w.Count;

                AppendPersonRows(cfg, sheet, fields, wsRow, org, org.players);
                wsRow += org.players.Count;
            }

            //設定格線
            SetRangeBorder(sheet, fields, mnRow, wsRow);
            //設定欄寬
            SetColumnWidth(sheet, fields);
        }

        private void AppendPersonRows(TConfig cfg
            , Spire.Xls.Worksheet sheet
            , List<TField> fields
            , int wsRow
            , TOrg org
            , List<TPerson> rows)
        {
            for (var i = 0; i < rows.Count; i++)
            {
                var row = rows[i];
                var item = row.item;
                item.setProperty("no", (i + 1).ToString());
                SetItemCell(cfg, sheet, wsRow, item, fields);
                wsRow++;
            }
        }

        private void AppendStaffSheet(TConfig cfg, Spire.Xls.Workbook workbook, TReport report)
        {
            var sheet = workbook.CreateEmptySheet();
            sheet.Name = report.key;

            List<TField> fields = new List<TField>();
            fields.Add(new TField { title = "序號", property = "no", css = TCss.Center, width = 5 });
            fields.Add(new TField { title = "姓名", property = "in_name", css = TCss.None, width = 15 });
            fields.Add(new TField { title = "組別", property = "in_l3", css = TCss.None, width = 15 });
            fields.Add(new TField { title = "目前單位", property = "in_current_org", css = TCss.None, width = 32 });

            MapCharSetAndIndex(cfg, fields);

            int mnRow = 1;
            int wsRow = 1;

            foreach (var sect in report.sects)
            {
                //標題列
                SetHeadRow(sheet, wsRow, fields);
                wsRow++;

                int no = 1;
                for (var j = 0; j < sect.persons.Count; j++)
                {
                    var p = sect.persons[j];
                    var item = p.item;
                    item.setProperty("no", no.ToString());
                    SetItemCell(cfg, sheet, wsRow, item, fields);
                    no++;
                    wsRow++;
                }
            }

            //設定格線
            SetRangeBorder(sheet, fields, mnRow, wsRow);

            //設定欄寬
            SetColumnWidth(sheet, fields);
        }

        private void AppendSingleSheet(TConfig cfg, Spire.Xls.Workbook workbook, TReport report)
        {
            var sheet = workbook.CreateEmptySheet();
            sheet.Name = report.key;

            List<TField> fields = new List<TField>();
            fields.Add(new TField { title = "序號", property = "no", css = TCss.Center, width = 5 });
            fields.Add(new TField { title = "姓名", property = "in_name", css = TCss.None, width = 12 });
            fields.Add(new TField { title = "身分證字號", property = "in_sno", css = TCss.Center, width = 12 });
            fields.Add(new TField { title = "性別", property = "in_gender", css = TCss.Center, width = 5 });
            fields.Add(new TField { title = "出生年月日", property = "in_birth", css = TCss.Day, width = 12 });
            fields.Add(new TField { title = "組名", property = "program_name", css = TCss.None, width = 35 });
            fields.Add(new TField { title = "目前單位", property = "map_org_name", css = TCss.None, width = 20 });
            //fields.Add(new TField { title = "隊別", property = "in_team", css = TCss.Center, width = 5 });
            fields.Add(new TField { title = "籤號", property = "in_sign_no", css = TCss.Center, width = 5 });
            fields.Add(new TField { title = "種子籤", property = "in_seeds", css = TCss.Center, width = 5 });

            MapCharSetAndIndex(cfg, fields);

            int mnRow = 1;
            int wsRow = 1;

            foreach (var sect in report.sects)
            {
                //標題列
                SetHeadRow(sheet, wsRow, fields);
                wsRow++;

                int no = 1;
                for (var j = 0; j < sect.persons.Count; j++)
                {
                    var p = sect.persons[j];
                    var item = p.item;
                    item.setProperty("no", no.ToString());
                    item.setProperty("fix_sect", FixSectName(item.getProperty("program_name", "")));
                    SetItemCell(cfg, sheet, wsRow, item, fields);
                    no++;
                    wsRow++;
                }
            }

            //設定格線
            SetRangeBorder(sheet, fields, mnRow, wsRow);

            //設定欄寬
            SetColumnWidth(sheet, fields);
        }

        /// <summary>
        /// 取得與會者資料
        /// </summary>
        private Item GetMeetingUsers(TConfig cfg)
        {
            string sql = @"
                SELECT
                    t1.in_l1
                    , t1.in_l2
                    , t1.in_l3
                    , t1.in_index
                    , ISNULL(t1.in_l1, '') + '-' + ISNULL(t1.in_l2, '') + '-' + ISNULL(t1.in_l3, '') + '-' + ISNULL(t1.in_index, '') + '-' + ISNULL(t1.in_creator_sno, '') AS 'in_team_key'
                    , t1.in_l1 + '-' + t1.in_l3 AS 'program_name'
                    , t1.in_current_org
                    , t1.in_short_org
                    , t1.in_team
                    , t1.in_name
                    , t1.in_sno
                    , t1.in_gender
                    , t1.in_birth
                    , t1.in_creator
                    , t1.in_creator_sno
                    , t2.in_tel AS 'in_creator_tel'
                    , t1.in_expense
                    , t1.in_paynumber
                    , t1.in_exe_a1
                    , t1.in_exe_a2
                    , t1.in_exe_a3
                    , t1.in_exe_a4
					, t1.in_stuff_b1
					, t2.in_email AS 'creator_email'
					, t2.in_tel   AS 'creator_tel'
                    , t3.pay_bool
                    , t3.in_pay_amount_exp
                    , t3.in_pay_amount_real
					, t4.in_type
                    , t4.in_short_org AS 'map_org_name'
                FROM 
                    IN_MEETING_USER t1 WITH(NOLOCK)
                LEFT OUTER JOIN
                    IN_RESUME t2 WITH(NOLOCK)
                    ON t2.in_sno = t1.in_creator_sno
                LEFT OUTER JOIN
                    IN_MEETING_PAY t3 WITH(NOLOCK)
                    ON t3.in_meeting = t1.source_id 
                    AND t3.item_number = t1.in_paynumber
                LEFT OUTER JOIN
                    IN_ORG_MAP t4 WITH(NOLOCK)
                    ON t4.in_stuff_b1 = t1.in_stuff_b1 
				LEFT OUTER JOIN
					VU_MEETING_SVY_L3 t5
					ON t5.source_id = t1.source_id
					AND t5.in_grand_filter = t1.in_l1
					AND t5.in_filter = t1.in_l2
					AND t5.in_value = t1.in_l3
                WHERE 
                    t1.source_id = '{#meeting_id}'
                ORDER BY 
                    t5.sort_order
					, t1.in_stuff_b1
					, t1.in_name
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item items = cfg.inn.applySQL(sql);

            if (items.isError() || items.getItemCount() <= 0)
            {
                throw new Exception("查無與會者資料");
            }

            return items;
        }


        private List<TReport> MapReports(TConfig cfg, List<TPerson> rows)
        {
            var reports = new List<TReport>();
            for (var i = 0; i < rows.Count; i++)
            {
                var p = rows[i];
                var report = reports.Find(x => x.key == p.groupName);
                if (report == null)
                {
                    report = new TReport { key = p.groupName, name = p.sheetName, sects = new List<TSect>() };
                    reports.Add(report);
                }
                var sect = report.sects.Find(x => x.key == p.program_name);
                if (sect == null)
                {
                    sect = new TSect { key = p.program_name, persons = new List<TPerson>() };
                    report.sects.Add(sect);
                }
                sect.persons.Add(p);
            }
            return reports;
        }

        private class TReport
        {
            public string key { get; set; }
            public string name { get; set; }
            public List<TSect> sects { get; set; }
        }

        private class TSect
        {
            public string key { get; set; }
            public List<TPerson> persons { get; set; }
        }

        /// <summary>
        /// 用身分證號推算性別
        /// </summary>
        private string GetGenderBySno(string value)
        {
            if (value == "" || value.Length < 2)
            {
                return "";
            }
            if (value[1] == '1') return "男";
            if (value[1] == '2') return "女";
            return "";
        }

        private TExport ExportInfo(TConfig cfg, string in_name)
        {
            Item itmXls = cfg.inn.applyMethod("In_Meeting_Excel_Path", "<in_name>" + in_name + "</in_name>");
            string Export_Path = itmXls.getProperty("export_path", "");
            string Template_Path = itmXls.getProperty("template_path", "");

            TExport result = new TExport
            {
                template_Path = Template_Path,
                export_Path = Export_Path,
            };

            return result;
        }

        //設定資料列
        private void SetItemCell(TConfig cfg, Spire.Xls.Worksheet sheet, int wsRow, Item item, List<TField> fields)
        {
            foreach (var field in fields)
            {
                var cr = field.cs + wsRow;
                var va = "";

                if (field.getValue != null)
                {
                    va = field.getValue(cfg, field, item);
                }
                else if (field.property != "")
                {
                    va = item.getProperty(field.property, "");
                }

                SetCell(sheet, cr, va, field.css);
            }
        }

        //設定資料格
        private void SetCell(Spire.Xls.Worksheet sheet, string cr, string value, TCss css)
        {
            var range = sheet.Range[cr];
            range.Style.Font.Size = 12;

            switch (css)
            {
                case TCss.None:
                    range.Text = value;
                    break;
                case TCss.Center:
                    range.Text = value;
                    range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                    break;
                case TCss.Text:
                    range.Text = "'" + value;
                    range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                    break;
                case TCss.Number:
                    range.NumberValue = GetIntVal(value);
                    range.NumberFormat = "0";
                    range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Right;
                    break;
                case TCss.Money:
                    range.NumberValue = GetIntVal(value);
                    range.NumberFormat = "$ #,##0";
                    break;
                case TCss.Day:
                    range.Text = GetDateTimeValue(value, "yyyy/MM/dd");
                    range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                    break;
            }
        }

        private void SetMoney(Spire.Xls.Worksheet sheet, string cr, double value)
        {
            var range = sheet.Range[cr];
            range.NumberValue = value;
            range.NumberFormat = "$ #,##0";
        }

        //設定標題列
        private void SetHead(Spire.Xls.Worksheet sheet, string cr, string value)
        {
            var range = sheet.Range[cr];
            range.Text = value;
            range.Style.Font.Size = 12;
            range.Style.Font.IsBold = true;
            range.Style.Font.Color = System.Drawing.Color.White;
            range.Style.Color = System.Drawing.Color.FromArgb(41, 92, 144);//#295C90
            range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
        }

        //設定格線
        private void SetRangeBorder(Spire.Xls.Worksheet sheet, string pos)
        {
            var range = sheet.Range[pos];
            range.BorderInside(Spire.Xls.LineStyleType.Thin, System.Drawing.Color.Black);
            range.BorderAround(Spire.Xls.LineStyleType.Thin, System.Drawing.Color.Black);
        }

        //設定格線
        private void SetRangeBorder(Spire.Xls.Worksheet sheet, List<TField> fields, int mnRow, int wsRow)
        {
            var tbl_ps = fields.First().cs + mnRow;
            var tbl_pe = fields.Last().cs + (wsRow - 1);
            SetRangeBorder(sheet, tbl_ps + ":" + tbl_pe);
        }

        private void MapCharSet(TConfig cfg, List<TField> fields)
        {
            foreach (var field in fields)
            {
                field.cs = cfg.CharSet[field.ci];
            }
        }

        private void MapCharSetAndIndex(TConfig cfg, List<TField> fields)
        {
            for (int i = 0; i < fields.Count; i++)
            {
                var field = fields[i];
                field.ci = i + 1;
                field.cs = cfg.CharSet[field.ci];
            }
        }

        private void SetHeadRow(Spire.Xls.Worksheet sheet, int wsRow, List<TField> fields)
        {
            foreach (var field in fields)
            {
                string cr = field.cs + wsRow;
                SetHead(sheet, cr, field.title);
            }
        }

        private void AutoFit(Spire.Xls.Worksheet sheet, List<TField> fields)
        {
            foreach (var field in fields)
            {
                sheet.Columns[field.ci - 1].AutoFitColumns();
            }
        }

        private void SetColumnWidth(Spire.Xls.Worksheet sheet, List<TField> fields)
        {
            foreach (var field in fields)
            {
                sheet.Columns[field.ci - 1].ColumnWidth = field.width;
            }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string meeting_id { get; set; }
            public Item itmMeeting { get; set; }
            public string mt_title { get; set; }
            public string[] CharSet { get; set; }
        }

        private class TExport
        {
            public string template_Path { get; set; }
            public string export_Path { get; set; }
        }

        private class TField
        {
            public string title { get; set; }
            public string property { get; set; }
            public TCss css { get; set; }
            public int ci { get; set; }
            public string cs { get; set; }
            public int width { get; set; }
            public Func<TConfig, TField, Item, string> getValue { get; set; }

            public bool is_merge { get; set; }
        }

        private List<TOrg> MapOrgList(TConfig cfg, List<TPerson> rows)
        {
            var orgs = new List<TOrg>();
            for (int i = 0; i < rows.Count; i++)
            {
                var p = rows[i];
                var org = orgs.Find(x => x.in_stuff_b1 == p.in_stuff_b1);
                if (org == null)
                {
                    org = new TOrg
                    {
                        in_type = p.in_type,
                        in_stuff_b1 = p.in_stuff_b1,
                        in_current_org = p.in_current_org,
                        in_name = p.creator_name,
                        in_email = p.creator_email,
                        in_tel = p.creator_tel,
                        leaders = new List<TPerson>(),
                        managers = new List<TPerson>(),
                        coaches_m = new List<TPerson>(),
                        coaches_w = new List<TPerson>(),
                        insurances = new List<TPerson>(),
                        players = new List<TPerson>(),
                    };
                    orgs.Add(org);
                }
                AnalysisMUser(cfg, org, p);
            }
            return orgs;
        }

        private void AnalysisMUser(TConfig cfg, TOrg org, TPerson p)
        {
            if (p.isPlayer)
            {
                org.players.Add(p);
            }
            else if (p.isInsurance)
            {
                org.insurances.Add(p);
            }
            else if (p.isLeader)
            {
                org.leaders.Add(p);
            }
            else if (p.isManager)
            {
                org.managers.Add(p);
            }
            else if (p.isCoachM)
            {
                org.coaches_m.Add(p);
            }
            else if (p.isCoachW)
            {
                org.coaches_w.Add(p);
            }
        }

        private class TOrg
        {
            public string in_type { get; set; }
            public string in_stuff_b1 { get; set; }
            public string in_current_org { get; set; }
            public string in_name { get; set; }
            public string in_email { get; set; }
            public string in_tel { get; set; }
            public List<TPerson> leaders { get; set; }
            public List<TPerson> managers { get; set; }
            public List<TPerson> coaches_m { get; set; }
            public List<TPerson> coaches_w { get; set; }
            public List<TPerson> insurances { get; set; }
            public List<TPerson> players { get; set; }
        }

        private List<TPerson> MapMUserList(TConfig cfg, Item items)
        {
            var list = new List<TPerson>();
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                list.Add(MapMUser(cfg, item));
            }
            return list;
        }

        private TPerson MapMUser(TConfig cfg, Item item)
        {
            var x = new TPerson
            {
                item = item,
                in_type = item.getProperty("in_type", ""),
                in_stuff_b1 = item.getProperty("in_stuff_b1", ""),
                in_current_org = item.getProperty("in_current_org", ""),
                in_l1 = item.getProperty("in_l1", ""),
                in_l2 = item.getProperty("in_l2", ""),
                in_l3 = item.getProperty("in_l3", ""),
                program_name = item.getProperty("program_name", ""),
                in_name = item.getProperty("in_name", ""),
                in_gender = item.getProperty("in_gender", ""),
                creator_name = item.getProperty("in_creator", ""),
                creator_email = item.getProperty("creator_email", ""),
                creator_tel = item.getProperty("creator_tel", ""),
            };

            if (x.in_l2.Contains("自由式")) x.in_l2 = "自由式";
            else if (x.in_l2.Contains("希羅式")) x.in_l2 = "希羅式";
            x.program_name = x.in_l1 + "-" + x.in_l2 + "-" + x.in_l3;

            switch (x.in_l1)
            {
                case "隊職員":
                    x.isStaff = true;
                    AnalysisStaff(cfg, x);
                    x.groupName = x.in_l1;
                    x.sheetName = x.in_l1;
                    break;
                case "保險代表人":
                    x.isInsurance = true;
                    x.groupName = x.in_l1;
                    x.sheetName = x.in_l1;
                    break;
                default:
                    x.isPlayer = true;
                    AnalysisPlayer(cfg, x);
                    break;
            }
            x.item.setProperty("groupName", x.groupName);
            return x;
        }

        private void AnalysisPlayer(TConfig cfg, TPerson x)
        {
            if (x.in_l1.Contains("大專"))
            {
                x.groupName = "大專社會組";
            }
            else if (x.in_l1.Contains("高中"))
            {
                x.groupName = "高中組";
            }
            else if (x.in_l1.Contains("國中"))
            {
                x.groupName = "國中組";
            }
            else if (x.in_l1.Contains("國小"))
            {
                x.groupName = "國小組";
            }
            else
            {
                x.groupName = x.in_l1;
            }
            x.sheetName = x.groupName + "籤表";

        }
        private void AnalysisStaff(TConfig cfg, TPerson x)
        {
            switch (x.in_l2)
            {
                case "領隊":
                    x.isLeader = true;
                    break;
                case "管理":
                    x.isManager = true;
                    break;
                case "男教練":
                case "男子隊教練":
                    x.isCoachM = true;
                    break;
                case "女教練":
                case "女子隊教練":
                    x.isCoachW = true;
                    break;
            }
        }

        private class TPerson
        {
            public bool isStaff { get; set; }
            public bool isLeader { get; set; }
            public bool isManager { get; set; }
            public bool isCoachM { get; set; }
            public bool isCoachW { get; set; }
            public bool isPlayer { get; set; }
            public bool isInsurance { get; set; }
            public string groupName { get; set; }
            public string sheetName { get; set; }

            public string in_type { get; set; }
            public string in_stuff_b1 { get; set; }
            public string in_current_org { get; set; }
            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }
            public string program_name { get; set; }
            public string in_name { get; set; }
            public string in_gender { get; set; }
            public string creator_name { get; set; }
            public string creator_email { get; set; }
            public string creator_tel { get; set; }
            public Item item { get; set; }

        }

        private enum TCss
        {
            None = 0,
            Center = 10,
            Text = 100,
            Number = 200,
            Money = 300,
            Day = 400,
        }

        private string[] GetCharSet()
        {
            return new string[] { "", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
        }

        private int GetIntVal(string value, int def = 0)
        {
            if (value == "") return 0;
            int result = def;
            int.TryParse(value, out result);
            return result;
        }

        //轉換日期
        private string GetDateTimeValue(string value, string format, bool add8Hours = true)
        {
            if (value == "") return "";
            DateTime dt = Convert.ToDateTime(value);
            if (add8Hours) dt = dt.AddHours(8);
            return dt.ToString(format);
        }

        private string FixSectName(string value)
        {
            var result = "";
            var arr = value.Split(new char[] { '：' }, StringSplitOptions.RemoveEmptyEntries);
            var w1 = arr.Length > 0 ? arr[0] : "";
            var w2 = arr.Length > 1 ? arr[1] : "";

            if (w2.Contains("以上至"))
            {
                w2 = w2.Replace(".1公斤以上至", "~");
                w2 = w2.Replace(".0公斤以下", "");
                var arr2 = w2.Split(new char[] { '~' }, StringSplitOptions.RemoveEmptyEntries);
                var weight = arr2.Length > 1 ? arr2[1] : "";
                var w31 = w1.Replace("-自由式-", "-自由式 : ").Replace("-希羅式-", "-希羅式 : ");
                result = w31 + "(-" + weight + "公斤)";
            }
            else if (w2.Contains("公斤以上"))
            {
                w2 = w2.Replace(".1 公斤以上", "");
                var w32 = w1.Replace("-自由式-", "-自由式 : ").Replace("-希羅式-", "-希羅式 : ");
                result = w32 + "(+" + w2 + ".1公斤)";
            }
            else if (value.Contains("超輕量級"))
            {
                w2 = w2.Replace(".0公斤以下", "");
                var w33 = w1.Replace("-自由式-", "-自由式 : ").Replace("-希羅式-", "-希羅式 : ");
                result = w33 + "(-" + w2 + "公斤)";
                result = result.Replace(" 公斤以下公斤", "kg 以下公斤");
            }
            else
            {
                w2 = w2.Replace(".0公斤以下", "");
                var w33 = w1.Replace("-自由式-", "-自由式 : ").Replace("-希羅式-", "-希羅式 : ");
                result = w33 + "(-" + w2 + "公斤)";
            }
            result = result.Replace("kg 以上公斤", "公斤以上");
            result = result.Replace("kg 以下公斤", "公斤以下");
            result = result.Replace("公斤以下公斤", "公斤以下");

            return result;
        }
    }
}