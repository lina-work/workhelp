﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Wrestling.Common
{
    public class In_Meeting_DrawEvent : Item
    {
        public In_Meeting_DrawEvent(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 角力籤表對照表建立
                日誌: 
                    - 2023-02-23: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "In_Meeting_DrawEvent";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,

                meeting_id = itmR.getProperty("meeting_id", ""),
                program_id = itmR.getProperty("program_id", ""),
                in_fight_day = itmR.getProperty("in_fight_day", ""),
                scene = itmR.getProperty("scene", ""),
            };

            cfg.itmMeeting = cfg.inn.applySQL("SELECT id, in_battle_mode, in_battle_repechage FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            cfg.in_battle_mode = cfg.itmMeeting.getProperty("in_battle_mode", "");
            cfg.in_battle_repechage = cfg.itmMeeting.getProperty("in_battle_repechage", "");

            cfg.option_battle_type = GetOptionBattleType(cfg);
            if (cfg.option_battle_type == "knockout")
            {
                cfg.in_battle_mode = "knockout";
                cfg.in_battle_repechage = "N";
            }
            switch (cfg.scene)
            {
                case "create":
                    CreateEvents(cfg, itmR);
                    break;

                case "create2":
                    CreateEvents2(cfg, itmR);
                    break;

                case "reset":
                    ResetEvents(cfg, itmR);
                    break;

                case "reset2":
                    ResetEvents2(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private string GetOptionBattleType(TConfig cfg)
        {
            var sql = @"
                SELECT TOP 1
	                t2.in_battle_type
                FROM
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                LEFT JOIN
	                VU_MEETING_SVY_L3 t2 
	                ON t2.source_id = t1.in_meeting
	                AND t2.in_grand_filter = t1.in_l1
	                AND t2.in_filter = t1.in_l2
	                AND t2.in_value = t1.in_l3
                WHERE
	                t1.id = '{#program_id}'
            ";

            sql = sql.Replace("{#program_id}", cfg.program_id);

            var itmResult = cfg.inn.applySQL(sql);
            if (itmResult.getResult() == "") return "";
            return itmResult.getProperty("in_battle_type", "");
        }

        private void CreateEvents2(TConfig cfg, Item itmReturn)
        {
            var in_l1 = itmReturn.getProperty("new_in_l1", "");
            var in_l2 = itmReturn.getProperty("new_in_l2", "");
            var in_l3 = itmReturn.getProperty("new_in_l3", "");
            var sql = "SELECT id FROM IN_MEETING_PROGRAM WITH(NOLOCK)"
                + " WHERE in_meeting = '" + cfg.meeting_id + "'"
                + " AND in_l1 = N'" + in_l1 + "'"
                + " AND in_l2 = N'" + in_l2 + "'"
                + " AND in_l3 = N'" + in_l3 + "'";
            var itmProgram = cfg.inn.applySQL(sql);
            if (itmProgram.isError() || itmProgram.getResult() == "") throw new Exception("查無組別");

            var program_id = itmProgram.getProperty("id", "");
            itmReturn.setProperty("program_id", program_id);
            cfg.program_id = program_id;

            CreateEvents(cfg, itmReturn);
        }

        private string GetMeetingVariable(TConfig cfg, string in_key)
        {
            var sql = "SELECT id, in_value FROM IN_MEETING_VARIABLE WITH(NOLOCK)"
                + " WHERE source_id = '" + cfg.meeting_id + "'"
                + " AND in_key = '" + in_key + "'";

            var itmVariable = cfg.inn.applySQL(sql);

            if (itmVariable.isError() || itmVariable.getResult() == "")
            {
                return "";
            }
            else
            {
                return itmVariable.getProperty("in_value", "");
            }
        }

        private void ResetEvents(TConfig cfg, Item itmReturn)
        {
            var sql = @"SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK)"
                + " WHERE in_meeting = '" + cfg.meeting_id + "'"
                + " AND in_fight_day = '" + cfg.in_fight_day + "'"
                + " ORDER BY in_sort_order";

            var itmPrograms = cfg.inn.applySQL(sql);

            var count = itmPrograms.getItemCount();

            for (var i = 0; i < count; i++)
            {
                var itmProgram = itmPrograms.getItemByIndex(i);
                ResetEventsOneProgram(cfg, itmProgram);
            }
        }

        private void ResetEvents2(TConfig cfg, Item itmReturn)
        {
            var sql = @"SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK)"
                + " WHERE in_meeting = '" + cfg.meeting_id + "'"
                + " AND id = '" + cfg.program_id + "'";

            var itmPrograms = cfg.inn.applySQL(sql);

            var count = itmPrograms.getItemCount();

            for (var i = 0; i < count; i++)
            {
                var itmProgram = itmPrograms.getItemByIndex(i);
                ResetEventsOneProgram(cfg, itmProgram);
            }
        }

        private void ResetEventsOneProgram(TConfig cfg, Item itmProgram)
        {
            var pg = new TProgram();

            //取得量級設定
            pg.in_team_count = itmProgram.getProperty("in_team_count", "");
            pg.in_fight_day = itmProgram.getProperty("in_fight_day", "");
            pg.in_site = itmProgram.getProperty("in_site", "");

            pg.team_count = GetInt(pg.in_team_count);

            var arr = InnSport.Core.Utilities.TUtility.Cardinarity(pg.team_count);
            pg.round_count = arr[0];
            pg.sys_count = arr[1];
            pg.new_battle_type = itmProgram.getProperty("in_battle_type", "");

            switch (pg.new_battle_type)
            {
                case "TopTwo"://單淘汰
                    pg.is_robin = false;
                    pg.is_cross = false;
                    pg.is_top_two = true;
                    break;

                case "SingleRoundRobin"://五人以下循環賽制
                    pg.is_robin = true;
                    break;

                case "SixPlayers"://六人分組交叉賽制
                case "SevenPlayers"://七人分組交叉賽制
                    pg.is_robin = true;
                    pg.is_cross = true;
                    break;

                default://八人以上二柱復活賽
                    pg.is_robin = false;
                    break;
            }

            //取得籤表場次對照表
            pg.itmDrawEvents = GetMeetingDrawEvents(cfg, pg.new_battle_type, pg.team_count, pg.sys_count);
            var tree_no_map = GetTreeNoMap(cfg, pg.itmDrawEvents);

            var items = cfg.inn.applySQL("SELECT * FROM IN_MEETING_PEVENT WITH(NOLOCK) WHERE source_id = '" + cfg.program_id + "'");
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var id = item.getProperty("id", "");
                var key = item.getProperty("in_fight_id", "");
                if (tree_no_map.ContainsKey(key))
                {
                    var in_tree_no = tree_no_map[key];
                    var sql_upd = "";
                    if (pg.is_robin)
                    {
                        sql_upd = "UPDATE IN_MEETING_PEVENT SET in_tree_no = '" + in_tree_no + "', in_tree_sno = '" + in_tree_no + "' WHERE id = '" + id + "'";
                    }
                    else
                    {
                        sql_upd = "UPDATE IN_MEETING_PEVENT SET in_tree_no = '" + in_tree_no + "' WHERE id = '" + id + "'";
                    }
                    cfg.inn.applySQL(sql_upd);
                }
            }

            //重設場次連線
            ResetEventLinks(cfg, pg);

            //重設籤號
            ResetEventFootSignNo(cfg, pg);

            //重設場次編號
            ResetEventTreeNo(cfg, pg);

            //分組交叉
            if (pg.is_cross)
            {
                ResetPlayerSubSect(cfg, pg);
                ResetDetailtFootInfo(cfg, pg);
            }
        }

        private Dictionary<string, string> GetTreeNoMap(TConfig cfg, Item items)
        {
            var result = new Dictionary<string, string>();

            var count = items.getItemCount();

            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var key = item.getProperty("in_fight_id", "");
                var value = item.getProperty("in_tree_no", "");
                if (!result.ContainsKey(key))
                {
                    result.Add(key, value);
                }
            }
            return result;
        }

        private void CreateEvents(TConfig cfg, Item itmReturn)
        {
            //取得量級設定
            var pg = new TProgram();
            pg.itmProgram = cfg.inn.applySQL("SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + cfg.program_id + "'");
            pg.in_team_count = pg.itmProgram.getProperty("in_team_count", "");
            pg.in_fight_day = pg.itmProgram.getProperty("in_fight_day", "");
            pg.in_site = pg.itmProgram.getProperty("in_site", "");

            pg.team_count = GetInt(pg.in_team_count);

            //新賽制
            pg.new_battle_type = GetNewBattleType(cfg, pg.team_count);

            var arr = InnSport.Core.Utilities.TUtility.Cardinarity(pg.team_count);
            pg.round_count = arr[0];
            pg.sys_count = arr[1];

            //清除場次資料
            RemoveEvents(cfg);

            //重設量級賽制
            ResetBattleType(cfg, pg.new_battle_type);

            switch (pg.new_battle_type)
            {
                case "TopTwo"://單淘汰
                    pg.is_top_two = true;
                    pg.is_robin = false;
                    pg.is_cross = false;
                    break;

                case "SingleRoundRobin"://五人以下循環賽制
                    pg.is_robin = true;
                    break;

                case "SixPlayers"://六人分組交叉賽制
                case "SevenPlayers"://七人分組交叉賽制
                    pg.is_robin = true;
                    pg.is_cross = true;
                    break;

                default://八人以上二柱復活賽
                    pg.is_robin = false;
                    pg.is_cross = false;
                    break;
            }

            //取得籤表場次對照表
            pg.itmDrawEvents = GetMeetingDrawEvents(cfg, pg.new_battle_type, pg.team_count, pg.sys_count);

            pg.EventMap = new Dictionary<string, string>();

            //重設量級場次
            ResetEvents(cfg, pg);

            //重設場次連線
            ResetEventLinks(cfg, pg);

            //重設籤號
            ResetEventFootSignNo(cfg, pg);

            //重設場次編號
            ResetEventTreeNo(cfg, pg);

            //分組交叉
            if (pg.is_cross)
            {
                ResetPlayerSubSect(cfg, pg);
                ResetDetailtFootInfo(cfg, pg);
            }

            //重算場次數量
            ResetEventCount(cfg, pg);

            if (pg.is_top_two)
            {
                //修正準決賽無名次
                ResetTreeRankRank23(cfg, pg);
            }
        }

        //修正準決賽無名次
        private void ResetTreeRankRank23(TConfig cfg, TProgram pg)
        {
            var sql = "UPDATE IN_MEETING_PEVENT SET"
                + "  in_tree_rank = 'rank23'"
                + ", in_tree_rank_ns = '0,3'"
                + ", in_tree_rank_nss = '0,3'"
                + " WHERE source_id = '" + cfg.program_id + "'"
                + " AND in_fight_id = '{#in_fight_id}'";

            var sql1 = sql.Replace("{#in_fight_id}", "M004-01");
            var sql2 = sql.Replace("{#in_fight_id}", "M004-02");

            cfg.inn.applySQL(sql1);
            cfg.inn.applySQL(sql2);
        }

        private void ResetEventCount(TConfig cfg, TProgram pg)
        {
            if (cfg.in_battle_mode == "knockout")
            {
                var sql = "UPDATE IN_MEETING_PROGRAM SET in_event_count = in_team_count - 1 WHERE id = '" + cfg.program_id + "'";
                cfg.inn.applySQL(sql);
            }
            else if (cfg.in_battle_mode == "uww" && cfg.in_battle_repechage == "N")
            {
                var evtCnt = pg.team_count - 1;

                switch (pg.team_count)
                {
                    case 0:
                    case 1:
                        evtCnt = 0;
                        break;
                    case 2:
                        evtCnt = 1;
                        break;
                    case 3:
                        evtCnt = 3;
                        break;
                    case 4:
                        evtCnt = 6;
                        break;
                    case 5:
                        evtCnt = 10;
                        break;
                    case 6:
                        evtCnt = 9;
                        break;
                    case 7:
                        evtCnt = 12;
                        break;

                }
                var sql2 = "UPDATE IN_MEETING_PROGRAM SET in_event_count = " + evtCnt + " WHERE id = '" + cfg.program_id + "'";
                cfg.inn.applySQL(sql2);
            }
            else if (cfg.in_battle_mode == "uww" && cfg.in_battle_repechage == "Y")
            {
                WrestlingTopTwoEventCount(cfg, pg);
            }
        }

        //冠亞單淘汰復活賽
        private void WrestlingTopTwoEventCount(TConfig cfg, TProgram pg)
        {
            var sql = @"
                SELECT TOP 1
	                id
	                , in_event_count 
                FROM 
	                IN_MEETING_DRAWSHEET WITH(NOLOCK) 
                WHERE 
	                in_mode = 'uww' 
	                AND in_team_count = {#in_team_count}
            ";

            sql = sql.Replace("{#in_team_count}", pg.team_count.ToString());

            var item = cfg.inn.applySQL(sql);
            if (!item.isError() && item.getResult() != "")
            {
                var evtCnt = item.getProperty("in_event_count", "0");
                if (pg.team_count == 2) evtCnt = "1";

                var sql2 = "UPDATE IN_MEETING_PROGRAM SET in_event_count = " + evtCnt + " WHERE id = '" + cfg.program_id + "'";
                cfg.inn.applySQL(sql2);
            }
        }

        //註記籤腳訊息
        private void ResetDetailtFootInfo(TConfig cfg, TProgram pg)
        {
            UpdateDetailPlayerInfo(cfg, pg, "M004-01", "1", "A冠");
            UpdateDetailPlayerInfo(cfg, pg, "M004-01", "2", "B亞");

            UpdateDetailPlayerInfo(cfg, pg, "M004-02", "1", "A亞");
            UpdateDetailPlayerInfo(cfg, pg, "M004-02", "2", "B冠");

            // if (cfg.draw_sheet_mode == "uww")
            // {
            //     UpdateDetailPlayerInfo(cfg, pg, "M004-02", "1", "B冠");
            //     UpdateDetailPlayerInfo(cfg, pg, "M004-02", "2", "A亞");
            // }
            // else if (cfg.draw_sheet_mode == "tw")
            // {
            //     UpdateDetailPlayerInfo(cfg, pg, "M004-02", "1", "A亞");
            //     UpdateDetailPlayerInfo(cfg, pg, "M004-02", "2", "B冠");
            // }

            UpdateDetailPlayerInfo(cfg, pg, "RNK56-01", "1", "A3");
            UpdateDetailPlayerInfo(cfg, pg, "RNK56-01", "2", "B3");
        }

        private void UpdateDetailPlayerInfo(TConfig cfg, TProgram pg, string fid, string foot, string name)
        {
            string sql = @"
                UPDATE t1 SET
	                t1.in_player_name = N'{#name}'
                FROM
	                IN_MEETING_PEVENT_DETAIL t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PEVENT t2 WITH(NOLOCK)
	                ON t2.id = t1.source_id
                WHERE
	                t2.source_Id = '{#pid}'
	                AND t2.in_fight_id = '{#fid}'
	                AND t1.in_sign_foot = '{#foot}'
            ";

            sql = sql.Replace("{#pid}", cfg.program_id)
                .Replace("{#fid}", fid)
                .Replace("{#foot}", foot)
                .Replace("{#name}", name);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            cfg.inn.applySQL(sql);
        }

        private void ResetPlayerSubSect(TConfig cfg, TProgram pg)
        {
            var sql = @"
                SELECT
	                t11.*
	                , ROW_NUMBER() OVER (PARTITION BY t11.in_sub_id ORDER BY t11.in_sign_no) AS 'rn'
                FROM
                (
	                SELECT 
		                DISTINCT t1.in_sign_no
		                , t2.in_sub_id
		                , t2.in_sub_sect
	                FROM 
		                IN_MEETING_DRAWSIGNNO t1 WITH(NOLOCK) 
	                INNER JOIN
		                IN_MEETING_DRAWEVENT t2 WITH(NOLOCK)
		                ON t2.in_fight_id = t1.in_fight_id
	                WHERE 
                        t1.in_mode = '{#in_battle_mode}'
		                AND t1.in_team_count = {#in_team_count}
						AND ISNULL(t1.in_sign_no, '') NOT IN ('', '0')
                ) t11
            ";

            sql = sql.Replace("{#in_battle_mode}", cfg.in_battle_mode)
                .Replace("{#in_team_count}", pg.in_team_count);

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var in_sign_no = item.getProperty("in_sign_no", "0");
                var in_sub_id = item.getProperty("in_sub_id", "0");
                var in_sub_sect = item.getProperty("in_sub_sect", "");
                var rn = item.getProperty("rn", "0");

                var new_sub_sect = in_sub_sect + rn;

                var sql_upd = "UPDATE IN_MEETING_PTEAM SET"
                    + "  in_sub_id = " + in_sub_id
                    + ", in_sub_sect = '" + new_sub_sect + "'"
                    + " WHERE source_id = '" + cfg.program_id + "'"
                    + " AND in_sign_no = '" + in_sign_no + "'"
                    ;

                cfg.inn.applySQL(sql_upd);
            }
        }

        //重設場次編號
        private void ResetEventTreeNo(TConfig cfg, TProgram pg)
        {
            var sql_upd1 = "UPDATE IN_MEETING_PEVENT SET"
                + " in_tree_no = NULL"
                + " WHERE source_id = '" + cfg.program_id + "'"
                + " AND in_tree_name IN('rank34', 'rank56', 'rank78')";

            cfg.inn.applySQL(sql_upd1);

            var sql_upd2 = @"
                UPDATE t11 SET
	                t11.in_tree_no = t12.rn
                FROM
	                IN_MEETING_PEVENT t11 
                INNER JOIN
                (
	                SELECT 
		                t1.id
		                , t1.in_tree_name
		                , t1.in_tree_no
		                , t1.in_tree_id
		                , ROW_NUMBER() OVER (ORDER BY t1.in_tree_no) AS 'rn'
	                FROM 
		                IN_MEETING_PEVENT t1 WITH(NOLOCK)
	                WHERE
		                t1.source_id = '{#program_id}'
		                AND ISNULL(t1.in_tree_no, 0) > 0
                ) t12 ON t12.id = t11.id
            ";

            sql_upd2 = sql_upd2.Replace("{#program_id}", cfg.program_id);

            cfg.inn.applySQL(sql_upd2);
        }

        //重設籤號
        private void ResetEventFootSignNo(TConfig cfg, TProgram pg)
        {
            var sql = @"
                SELECT 
	                *
                FROM 
	                IN_MEETING_DRAWSIGNNO WITH(NOLOCK)
                WHERE 
                    in_mode = '{#in_battle_mode}'
	                AND in_team_count = {#in_team_count}
                ORDER BY 
	                in_sign_no
            ";

            sql = sql.Replace("{#in_battle_mode}", cfg.in_battle_mode)
                .Replace("{#in_team_count}", pg.in_team_count);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var in_fight_id = item.getProperty("in_fight_id", "");
                var in_sign_foot = item.getProperty("in_sign_foot", "");
                var in_sign_no = item.getProperty("in_sign_no", "");
                var in_bypass_status = item.getProperty("in_bypass_status", "");

                var target_foot = in_sign_foot == "1" ? "2" : "1";

                var evt_id = GetEventId(cfg, pg, in_fight_id);
                if (evt_id == "") continue;

                var in_status = in_bypass_status == "1" && in_sign_no != ""
                    ? "1"
                    : "";

                var sql_upd1 = "UPDATE IN_MEETING_PEVENT_DETAIL SET"
                    + "  in_sign_no = '" + in_sign_no + "'"
                    + ", in_status = '" + in_status + "'"
                    + " WHERE source_id = '" + evt_id + "'"
                    + " AND in_sign_foot = '" + in_sign_foot + "'";

                cfg.inn.applySQL(sql_upd1);

                var sql_upd2 = "UPDATE IN_MEETING_PEVENT_DETAIL SET"
                    + "  in_target_no = '" + in_sign_no + "'"
                    + " WHERE source_id = '" + evt_id + "'"
                    + " AND in_sign_foot = '" + target_foot + "'";

                cfg.inn.applySQL(sql_upd2);

                if (in_bypass_status == "1")
                {
                    var sql_upd3 = "UPDATE IN_MEETING_PEVENT SET"
                        + "  in_bypass_status = '1'"
                        + ", in_tree_no = NULL"
                        + ", in_win_status = 'bypass'"
                        + ", in_win_sign_no = '" + in_sign_no + "'"
                        + " WHERE id = '" + evt_id + "'";

                    cfg.inn.applySQL(sql_upd3);
                }
            }
        }

        //重設場次連線
        private void ResetEventLinks(TConfig cfg, TProgram pg)
        {
            var count = pg.itmDrawEvents.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = pg.itmDrawEvents.getItemByIndex(i);
                var in_fight_id = item.getProperty("in_fight_id", "");
                var in_next_win_tid = item.getProperty("in_next_win_tid", "");
                var in_next_win_foot = item.getProperty("in_next_win_foot", "");
                var in_next_lose_tid = item.getProperty("in_next_lose_tid", "");
                var in_next_lose_foot = item.getProperty("in_next_lose_foot", "");

                var eid = GetEventId(cfg, pg, in_fight_id);
                if (eid == "") continue;

                //勝出 Link
                UpdateEventWLink(cfg, pg, eid, in_next_win_tid, in_next_win_foot);
                //敗出 Link
                UpdateEventLLink(cfg, pg, eid, in_next_lose_tid, in_next_lose_foot);
            }
        }

        private void UpdateEventWLink(TConfig cfg, TProgram pg, string eid, string next_tid, string next_foot)
        {
            if (next_tid == "") return;

            var next_id = GetEventId(cfg, pg, next_tid);
            if (next_id == "" || next_foot == "") return;

            var sql = "UPDATE IN_MEETING_PEVENT SET"
                + "  in_next_win = '" + next_id + "'"
                + ", in_next_foot_win = '" + next_foot + "'"
                + " WHERE id = '" + eid + "'";

            cfg.inn.applySQL(sql);
        }

        private void UpdateEventLLink(TConfig cfg, TProgram pg, string eid, string next_tid, string next_foot)
        {
            if (next_tid == "") return;

            var next_id = GetEventId(cfg, pg, next_tid);
            if (next_id == "" || next_foot == "") return;

            var sql = "UPDATE IN_MEETING_PEVENT SET"
                + "  in_next_lose = '" + next_id + "'"
                + ", in_next_foot_lose = '" + next_foot + "'"
                + " WHERE id = '" + eid + "'";

            cfg.inn.applySQL(sql);
        }

        private string GetEventId(TConfig cfg, TProgram pg, string key)
        {
            if (pg.EventMap.ContainsKey(key))
            {
                return pg.EventMap[key];
            }
            return "";
        }

        //重設量級場次
        private void ResetEvents(TConfig cfg, TProgram pg)
        {
            var count = pg.itmDrawEvents.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = pg.itmDrawEvents.getItemByIndex(i);
                var in_tree_name = item.getProperty("in_tree_name", "");
                var in_round_code = item.getProperty("in_round_code", "0");
                var in_round_id = item.getProperty("in_round_id", "0");
                var in_fight_id = item.getProperty("in_fight_id", "0");

                var in_round = GetRoundBelong(cfg, pg, in_tree_name, in_round_code);
                var in_tree_id = GetTreeId(cfg, in_tree_name, in_round, in_round_id, in_fight_id);

                Item itmNew = cfg.inn.newItem("In_Meeting_PEvent", "add");
                itmNew.setProperty("in_meeting", cfg.meeting_id);
                itmNew.setProperty("source_id", cfg.program_id);
                itmNew.setProperty("in_date_key", pg.in_fight_day);
                itmNew.setProperty("in_site", pg.in_site);

                itmNew.setProperty("in_tree_name", in_tree_name);
                itmNew.setProperty("in_tree_sort", item.getProperty("in_tree_sort", ""));

                itmNew.setProperty("in_tree_id", in_tree_id);
                itmNew.setProperty("in_tree_no", item.getProperty("in_tree_no", ""));
                itmNew.setProperty("in_tree_rank", item.getProperty("in_rank_name", ""));
                itmNew.setProperty("in_tree_rank_ns", item.getProperty("in_rank_real", ""));
                itmNew.setProperty("in_tree_rank_nss", item.getProperty("in_rank_show", ""));
                itmNew.setProperty("in_tree_alias", item.getProperty("in_alias", ""));

                itmNew.setProperty("in_round", in_round);
                itmNew.setProperty("in_round_id", item.getProperty("in_round_id", ""));
                itmNew.setProperty("in_round_code", item.getProperty("in_round_code", ""));
                itmNew.setProperty("in_fight_id", in_fight_id);

                itmNew.setProperty("in_period", item.getProperty("in_period", ""));
                itmNew.setProperty("sort_order", item.getProperty("in_tree_no", ""));

                var nv = GetCourse(itmNew, pg.is_top_two, pg.team_count);
                itmNew.setProperty("in_course", nv.n1);
                itmNew.setProperty("in_we", nv.n2);

                if (pg.is_robin)
                {
                    itmNew.setProperty("in_tree_id", item.getProperty("in_tree_id", ""));
                    itmNew.setProperty("in_tree_sno", item.getProperty("in_tree_no", ""));
                    itmNew.setProperty("in_round", item.getProperty("in_round", ""));
                    itmNew.setProperty("in_robin_round", item.getProperty("in_round", ""));
                }

                if (pg.is_cross)
                {
                    itmNew.setProperty("in_sub_id", item.getProperty("in_sub_id", "0"));
                    itmNew.setProperty("in_sub_sect", item.getProperty("in_sub_sect", ""));
                }

                itmNew = itmNew.apply();

                var key = in_fight_id;
                var eid = itmNew.getProperty("id", "");

                pg.EventMap.Add(key, eid);

                AppendEventDetail(cfg, eid, "1", "2");
                AppendEventDetail(cfg, eid, "2", "1");
            }
        }

        private TNV GetCourse(Item item, bool is_top_two, int team_count)
        {
            if (is_top_two)
            {
                return GetCoursePlayer8(item);
            }
            else
            {
                return GetCourseB(item, team_count);
            }
        }

        private TNV GetCourseB(Item item, int team_count)
        {
            if (team_count <= 5) return new TNV { n1 = "循環賽", n2 = "" };
            if (team_count == 6) return GetCoursePlayer6(item);
            if (team_count == 7) return GetCoursePlayer7(item);
            return GetCoursePlayer8(item);
        }

        private TNV GetCoursePlayer6(Item item)
        {
            var result = new TNV { n1 = "", n2 = "" };

            var in_fight_id = item.getProperty("in_fight_id", "");

            switch (in_fight_id)
            {
                case "CRS6-01":
                case "CRS6-02":
                case "CRS6-03":
                    return new TNV { n1 = "循環賽", n2 = "(A)" };

                case "CRS6-04":
                case "CRS6-05":
                case "CRS6-06":
                    return new TNV { n1 = "循環賽", n2 = "(B)" };

                case "M004-01": return new TNV { n1 = "交叉準決賽", n2 = "(W)" };
                case "M004-02": return new TNV { n1 = "交叉準決賽", n2 = "(E)" };
                case "M002-01": return new TNV { n1 = "金牌戰", n2 = "" };
                case "RNK34-01": return new TNV { n1 = "三四名", n2 = "" };
                default: return new TNV { n1 = "", n2 = "" };
            }
        }

        private TNV GetCoursePlayer7(Item item)
        {
            var in_fight_id = item.getProperty("in_fight_id", "");

            switch (in_fight_id)
            {
                case "CRS7-01":
                case "CRS7-02":
                case "CRS7-03":
                case "CRS7-04":
                case "CRS7-05":
                case "CRS7-06":
                    return new TNV { n1 = "循環賽", n2 = "(A)" };

                case "CRS7-07":
                case "CRS7-08":
                case "CRS7-09":
                    return new TNV { n1 = "循環賽", n2 = "(B)" };

                case "M004-01": return new TNV { n1 = "交叉準決賽", n2 = "(W)" };
                case "M004-02": return new TNV { n1 = "交叉準決賽", n2 = "(E)" };
                case "M002-01": return new TNV { n1 = "金牌戰", n2 = "" };
                case "RNK34-01": return new TNV { n1 = "三四名", n2 = "" };
                case "RNK56-01": return new TNV { n1 = "五六名", n2 = "" };
                default: return new TNV { n1 = "", n2 = "" };
            }
        }

        private TNV GetCoursePlayer8(Item item)
        {
            var in_fight_id = item.getProperty("in_fight_id", "");
            var in_tree_rank = item.getProperty("in_tree_rank", "");

            switch (in_fight_id)
            {
                case "M004-01": return new TNV { n1 = "準決賽", n2 = "(W)" };
                case "M004-02": return new TNV { n1 = "準決賽", n2 = "(E)" };
                case "M002-01": return new TNV { n1 = "金牌戰", n2 = "" };
                case "RNK34-01": return new TNV { n1 = "三四名", n2 = "" };
                case "RNK56-01": return new TNV { n1 = "五六名", n2 = "" };
                case "RNK78-01": return new TNV { n1 = "七八名", n2 = "" };
            }

            var c1 = in_fight_id[0];
            var c2 = in_fight_id[in_fight_id.Length - 1];
            if (in_tree_rank == "rank35")
            {
                return new TNV { n1 = "銅牌戰", n2 = c2 == '1' ? "(W)" : "(E)" };
            }


            if (c1 == 'M') return new TNV { n1 = "預賽", n2 = GetWE(in_fight_id) };
            if (c1 == 'R') return new TNV { n1 = "複賽", n2 = c2 == '1' ? "(W)" : "(E)" };

            return new TNV { n1 = "", n2 = "" };
        }

        private string GetWE(string in_fight_id)
        {
            var arr = in_fight_id.Split('-');
            var a = GetInt(arr[0].Replace("M", "").TrimStart('0'));
            var b = GetInt(arr[1].TrimStart('0'));

            a = a / 4;
            if (b > a) return "(E)";
            return "(W)";
        }

        private string GetRoundBelong(TConfig cfg, TProgram pg, string in_tree_name, string in_round_code)
        {
            var round_code = GetInt(in_round_code);
            var round_belong = GetRoundCount(round_code);
            var round = pg.round_count - round_belong + 1;

            switch (in_tree_name)
            {
                case "main":
                case "repechage":
                    return round.ToString();

                default:
                    return "1";
            }
        }

        private string GetTreeId(TConfig cfg, string in_tree_name, string in_round, string in_round_id, string in_fight_id)
        {
            switch (in_tree_name)
            {
                case "main":
                    return "M" + in_round + in_round_id.PadLeft(2, '0');

                case "repechage":
                    return "R" + in_round + in_round_id.PadLeft(2, '0');

                case "rank34":
                    return "rank34";

                case "rank56":
                    return "rank56";

                case "rank78":
                    return "rank78";

                default:
                    return in_fight_id;
            }
        }

        private void AppendEventDetail(TConfig cfg, string eid, string in_sign_foot, string in_target_foot)
        {
            Item itmDetail = cfg.inn.newItem("In_Meeting_PEvent_Detail", "add");
            itmDetail.setProperty("source_id", eid);
            itmDetail.setProperty("in_sign_foot", in_sign_foot);
            itmDetail.setProperty("in_target_foot", in_target_foot);
            itmDetail.apply();
        }

        private string GetNewBattleType(TConfig cfg, int team_count)
        {
            var sql = @"
                SELECT TOP 1 
                    * 
                FROM 
                    IN_MEETING_DRAWBATTLE WITH(NOLOCK) 
                WHERE 
                    in_battle_mode = '{#in_battle_mode}'
                    AND {#team_count} BETWEEN in_count_s AND in_count_e
            ";

            sql = sql.Replace("{#in_battle_mode}", cfg.in_battle_mode)
                .Replace("{#team_count}", team_count.ToString());

            var itmDrawTeam = cfg.inn.applySQL(sql);

            return itmDrawTeam.getProperty("in_battle_type", "");
        }

        //重設量級賽制
        private void ResetBattleType(TConfig cfg, string new_battle_type)
        {
            var sql_upd = "UPDATE IN_MEETING_PROGRAM SET in_battle_type = '" + new_battle_type + "' WHERE id = '" + cfg.program_id + "'";
            cfg.inn.applySQL(sql_upd);
        }

        /// <summary>
        /// 清除場次資料
        /// </summary>
        private void RemoveEvents(TConfig cfg)
        {
            Item itmData = cfg.inn.newItem("In_Meeting_Program");
            itmData.setProperty("program_id", cfg.program_id);
            itmData.apply("in_meeting_program_remove_all");
        }

        private Item GetMeetingDrawEvents(TConfig cfg, string new_battle_type, int team_count, int sys_count)
        {
            switch (new_battle_type)
            {
                case "TopTwo"://單淘汰
                    return GetMeetingDrawEventsByKnockout(cfg, sys_count);

                case "SingleRoundRobin"://五人以下循環賽制
                    return GetMeetingDrawEventsByRobin(cfg, team_count, "robin");

                case "SixPlayers"://六人分組交叉賽制
                case "SevenPlayers"://七人分組交叉賽制
                    return GetMeetingDrawEventsByRobin(cfg, team_count, "cross");

                default://八人以上二柱復活賽
                    return GetMeetingDrawEventsByTree(cfg, sys_count);
            }
        }

        private Item GetMeetingDrawEventsByRobin(TConfig cfg, int sys_count, string in_group)
        {
            string sql = @"
                SELECT 
	                * 
                FROM 
	                IN_MEETING_DRAWEVENT WITH(NOLOCK) 
                WHERE 
	                in_team_count = {#sys_count}
                    AND in_group = '{#in_group}'
                ORDER BY
	                in_sort_order
            ";

            sql = sql.Replace("{#sys_count}", sys_count.ToString())
                .Replace("{#in_group}", in_group)
                ;

            return cfg.inn.applySQL(sql);
        }

        private Item GetMeetingDrawEventsByKnockout(TConfig cfg, int sys_count)
        {
            string sql = @"
                SELECT 
	                * 
                FROM 
	                IN_MEETING_DRAWEVENT WITH(NOLOCK) 
                WHERE 
	                in_team_count <= {#sys_count}
                    AND in_group IN ('main')
                ORDER BY
	                in_sort_order
            ";

            sql = sql.Replace("{#sys_count}", sys_count.ToString());

            return cfg.inn.applySQL(sql);
        }

        private Item GetMeetingDrawEventsByTree(TConfig cfg, int sys_count)
        {
            string sql = @"
                SELECT 
	                * 
                FROM 
	                IN_MEETING_DRAWEVENT WITH(NOLOCK) 
                WHERE 
	                in_team_count <= {#sys_count}
                    AND in_group IN ('main', 'repechage', 'rank')
                ORDER BY
	                in_sort_order
            ";

            sql = sql.Replace("{#sys_count}", sys_count.ToString());

            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string scene { get; set; }

            public string meeting_id { get; set; }
            public string program_id { get; set; }
            public string in_fight_day { get; set; }

            public Item itmMeeting { get; set; }
            public string in_battle_mode { get; set; }
            public string in_battle_repechage { get; set; }
            public string option_battle_type { get; set; }
        }

        private class TProgram
        {
            public Item itmProgram { get; set; }
            public string in_team_count { get; set; }
            public string in_fight_day { get; set; }
            public string in_site { get; set; }
            public string new_battle_type { get; set; }

            public Item itmDrawEvents { get; set; }
            public Dictionary<string, string> EventMap { get; set; }

            public int team_count { get; set; }
            public int sys_count { get; set; }
            public int round_count { get; set; }

            public bool is_robin { get; set; }
            public bool is_cross { get; set; }
            public bool is_top_two { get; set; }
        }

        public class TNV
        {
            public string n1 { get; set; }
            public string n2 { get; set; }
        }

        private int GetRoundCount(int value)
        {
            var arr = InnSport.Core.Utilities.TUtility.Cardinarity(value);
            return arr[0];
        }

        private int GetInt(string value)
        {
            if (value == "" || value == "0") return 0;
            int result = 0;
            if (int.TryParse(value, out result)) return result;
            return 0;
        }
    }
}