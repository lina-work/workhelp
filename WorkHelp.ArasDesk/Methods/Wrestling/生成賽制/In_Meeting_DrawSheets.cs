﻿using Aras.IOM;
using System;
using System.Collections.Generic;
using System.Web.Configuration;
using WorkHelp.ArasDesk.Samples.SqlClient;

namespace WorkHelp.ArasDesk.Methods.Wrestling.Fight
{
    public class In_Meeting_DrawSheets : Item
    {
        public In_Meeting_DrawSheets(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
    目的: 對戰表-角力
    日誌: 
        - 2024-03-28: 創建 (lina)
*/

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "In_Meeting_DrawSheets";

            var itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                meeting_id = itmR.getProperty("meeting_id", ""),
                program_id = itmR.getProperty("program_id", ""),
                in_fight_day = itmR.getProperty("in_fight_day", ""),
                in_l1 = itmR.getProperty("in_l1", ""),
                in_l2 = itmR.getProperty("in_l2", ""),
                in_l3 = itmR.getProperty("in_l3", ""),
                line_type = LineDataType.name,
                scene = itmR.getProperty("scene", ""),
            };

            cfg.report_type = GetMeetingVariable(cfg, "draw_report_type");
            if (cfg.report_type == "") cfg.report_type = "xlsx";

            //設定籤表場次編號模式
            SetDrawEventNumberType(cfg);

            cfg.itmMeeting = GetMeetingItem(cfg);
            if (cfg.itmMeeting.isError() || cfg.itmMeeting.getResult() == "")
            {
                throw new Exception("查無活動資訊");
            }
            cfg.mt_title = cfg.itmMeeting.getProperty("in_title", "");
            cfg.in_battle_mode = cfg.itmMeeting.getProperty("in_battle_mode", "");
            cfg.in_battle_repechage = cfg.itmMeeting.getProperty("in_battle_repechage", "");

            cfg.variableMap = GetMeetingVariables(cfg);
            cfg.fight_address = FindMeetingVariable(cfg.variableMap, "fight_site");
            cfg.fight_battle_mode = FindMeetingVariable(cfg.variableMap, "fight_battle_mode");
            cfg.need_one_player_sheet = FindMeetingVariable(cfg.variableMap, "need_one_player_sheet");
            cfg.is_mix_battle = cfg.fight_battle_mode == "mix";

            switch (cfg.scene)
            {
                default:
                    Export(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void Export(TConfig cfg, Item itmReturn)
        {
            var sects = GetMeetingPrograms(cfg);
            var count = sects.Count;
            if (count == 0) throw new Exception("查無級組資訊");

            var in_name = "wrestling_draw_sheets_knockout";
            if (cfg.in_battle_mode == "uww")
            {
                if (cfg.in_battle_repechage == "N")
                {
                    in_name = "wrestling_draw_sheets_no_rpc";
                }
                else
                {
                    in_name = "wrestling_draw_sheets_repechage";
                }
            }

            if (cfg.is_mix_battle)
            {
                in_name = "wrestling_draw_sheets_mix";
            }

            var exp = ExportInfo(cfg, in_name);

            //試算表
            var book = new Spire.Xls.Workbook();
            book.LoadFromFile(exp.template_Path);

            //建立樣板 Sheet 對照表
            var map = new Dictionary<string, int>();
            var max = book.Worksheets.Count - 1;
            for (var i = 0; i < book.Worksheets.Count; i++)
            {
                var sheet = book.Worksheets[i];
                map.Add(sheet.Name, i);
            }

            for (var i = 0; i < count; i++)
            {
                var sect = sects[i];
                AppendReport(cfg, sect, book, map);
            }

            if (cfg.sheetCount == 0)
            {
                throw new Exception("沒有對戰表資料");
            }

            //刪除樣板 Sheet
            for (var i = max; i >= 0; i--)
            {
                book.Worksheets.RemoveAt(i);
            }

            //總表
            CreateSectSummary(cfg, book, sects, exp, itmReturn);

            book.Worksheets[0].Activate();

            string subName = count == 1
                ? sects[0].alias + "_"
                : cfg.in_fight_day + "_";

            string guid = System.Guid.NewGuid().ToString().Substring(0, 4).ToUpper()
                + "_" + DateTime.Now.ToString("yyyyMMdd_HHmmss");

            string ext_name = "." + cfg.report_type;
            string xls_name = cfg.mt_title + "_籤表_" + subName + guid;
            string xls_file = exp.export_Path + "\\" + xls_name + ext_name;
            string xls_url = xls_name + ext_name;

            if (cfg.report_type == "PDF")
            {
                book.SaveToFile(xls_file, Spire.Xls.FileFormat.PDF);
            }
            else
            {
                book.SaveToFile(xls_file, Spire.Xls.ExcelVersion.Version2013);
            }

            itmReturn.setProperty("xls_name", xls_url);
        }


        private void CreateSectSummary(TConfig cfg, Spire.Xls.Workbook book, List<TSect> sects, TExport exp, Item itmReturn)
        {
            if (sects.Count == 1) return;

            cfg.head_color = System.Drawing.Color.FromArgb(237, 237, 237);
            var sheet = book.CreateEmptySheet();
            sheet.Name = cfg.in_fight_day;
            sheet.MoveSheet(0);

            sheet.PageSetup.CenterHorizontally = true;
            sheet.PageSetup.TopMargin = 0.8;
            sheet.PageSetup.LeftMargin = 0.8;
            sheet.PageSetup.RightMargin = 0.8;
            sheet.PageSetup.BottomMargin = 0.8;

            sheet.PageSetup.PrintTitleRows = "$1:$2";

            var mtPos = "A1:D1";
            sheet.Range[mtPos].Merge();
            sheet.Range[mtPos].RowHeight = 42;
            sheet.Range[mtPos].Style.ShrinkToFit = true;
            SetSummaryCell(sheet, "A1", cfg.mt_title, 20);

            var dtPos = "A2:D2";
            sheet.Range[dtPos].Merge();
            sheet.Range[dtPos].RowHeight = 15;
            sheet.Range[dtPos].Style.ShrinkToFit = true;
            SetSummaryCell(sheet, "A2", "比賽日期: " + cfg.in_fight_day, 12);
            sheet.Range["A2"].Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Right;

            var ri = 3;
            SetSummaryCell(sheet, "A" + ri, "場地", 14);
            SetSummaryCell(sheet, "B" + ri, "級組", 14);
            SetSummaryCell(sheet, "C" + ri, "選手數", 14);
            SetSummaryCell(sheet, "D" + ri, "場次數", 14);
            ri++;

            for (var i = 0; i < sects.Count; i++)
            {
                var pg = sects[i];
                SetSummaryCell(sheet, "A" + ri, pg.site, 14);
                SetSummaryCell(sheet, "B" + ri, pg.name, 14);
                SetSummaryCell(sheet, "C" + ri, pg.count, 14);
                SetSummaryCell(sheet, "D" + ri, pg.eventCount, 14);
                ri++;
            }

            sheet.Columns[0].ColumnWidth = 13;
            sheet.Columns[1].ColumnWidth = 24;
            sheet.Columns[2].ColumnWidth = 29;
            sheet.Columns[3].ColumnWidth = 13;
        }

        private void SetSummaryCell(Spire.Xls.Worksheet sheet, string pos, string value, int fontSize)
        {
            var range = sheet.Range[pos];
            range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
            range.Style.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;
            range.Text = value;
            range.Style.Font.Size = fontSize;
            range.Style.Font.FontName = "標楷體";
        }

        private void AppendReport(TConfig cfg, TSect pg, Spire.Xls.Workbook book, Dictionary<string, int> map)
        {
            var key = pg.count;
            if (cfg.is_mix_battle && pg.battle == "TopTwo")
            {
                key = pg.count + "k";
            }
            var index = map.ContainsKey(key) ? map[key] : -1;
            if (index <= -1) return;

            if (pg.posiMap == null) return;
            if (pg.teamMap == null || pg.teamMap.Count == 0) return;

            cfg.sheetCount++;

            //取得樣板 sheet
            var sheetTemplate = book.Worksheets[index];

            //創建並複製樣板
            var sheet = book.CreateEmptySheet();
            sheet.CopyFrom(sheetTemplate);
            sheet.Name = pg.alias;

            //活動組別資料
            SetSectInfo(cfg, pg, sheet);
            //選手資料
            SetPlayerInfo(cfg, pg, sheet);
            //場次資料
            SetEventInfo(cfg, pg, sheet);

            //將所有欄放入單一頁面
            sheet.PageSetup.FitToPagesWide = 1;
            sheet.PageSetup.FitToPagesTall = 0;

            //適合頁面
            // book.ConverterSetting.SheetFitToPage = true;
        }

        private void SetSectInfo(TConfig cfg, TSect pg, Spire.Xls.Worksheet sheet)
        {
            var map = pg.posiMap;
            FindCell(cfg, sheet, "{#mt_title}", "比賽名稱：" + cfg.mt_title);
            FindCell(cfg, sheet, "{#fight_day}", "比賽日期：" + pg.day);
            FindCell(cfg, sheet, "{#weight_name}", "組別量級：" + pg.name);
            FindCell(cfg, sheet, "{#mt_address}", "比賽場地：" + cfg.fight_address);
            FindCell(cfg, sheet, "{#site}", pg.site);
            FindCell(cfg, sheet, "{#player_count}", "比賽人數：" + pg.count + "人");
        }

        private void SetPlayerInfo(TConfig cfg, TSect pg, Spire.Xls.Worksheet sheet)
        {
            var map = pg.posiMap;
            var dic = pg.teamMap;
            if (dic == null || dic.Count == 0) return;
            foreach (var kv in dic)
            {
                var in_sign_no = kv.Key;
                var item = kv.Value;
                var player = ItemPlayer(in_sign_no, item);

                var keyOrg = "{#p" + in_sign_no + "_O}";
                var keyName = "{#p" + in_sign_no + "_N1}";
                var keyNo1 = "{#p" + in_sign_no + "_N2}";
                var keyNo2 = "{#p" + in_sign_no + "_N3}";

                FindCell(cfg, sheet, keyOrg, player.org);
                FindCell(cfg, sheet, keyName, player.name);
                //FindCell(sheet, keyNo1, player.in_sign_no);
                FindCell(cfg, sheet, keyNo2, player.lottery);

            }
        }

        private void SetEventInfo(TConfig cfg, TSect pg, Spire.Xls.Worksheet sheet)
        {
            var map = pg.posiMap;

            var list = map.listB;
            if (list == null || list.Count == 0) return;

            for (var i = 0; i < list.Count; i++)
            {
                var pos = list[i];
                if (pos.in_fight_id == "") continue;

                var evt = FindEvent(cfg, pg.eventMap, pos.in_fight_id);
                var wn = evt.in_win_sign_no;
                var n1 = evt.itmF1.getProperty("in_sign_no", "0");
                var n2 = evt.itmF2.getProperty("in_sign_no", "0");

                var p1 = FindPlayer(cfg, pg.teamMap, n1);
                var p2 = FindPlayer(cfg, pg.teamMap, n2);
                var pw = FindPlayer(cfg, pg.teamMap, wn);

                var keyEvt = "{#" + pos.in_fight_id + "}";
                var keyF1 = "{#" + pos.in_fight_id + "-F1}";
                var keyF2 = "{#" + pos.in_fight_id + "-F2}";
                var keyW = "{#" + pos.in_fight_id + "-W}";

                var keyF1O = "{#" + pos.in_fight_id + "-F1-O}";
                var keyF1N1 = "{#" + pos.in_fight_id + "-F1-N1}";
                var keyF1N2 = "{#" + pos.in_fight_id + "-F1-N2}";
                var keyF1N3 = "{#" + pos.in_fight_id + "-F1-N3}";
                var keyF2O = "{#" + pos.in_fight_id + "-F2-O}";
                var keyF2N1 = "{#" + pos.in_fight_id + "-F2-N1}";
                var keyF2N2 = "{#" + pos.in_fight_id + "-F2-N2}";
                var keyF2N3 = "{#" + pos.in_fight_id + "-F2-N3}";

                //if (cfg.isDraw)
                //{
                //    p1 = EmptyPlayer("");
                //    p2 = EmptyPlayer("");
                //    pw = EmptyPlayer("");
                //}

                if (pg.teamMap.Count <= 7)
                {
                    EventNumberCell(cfg, sheet, keyEvt, evt, pos);
                    LineCell2(cfg, sheet, keyF1, GetLineData2(cfg, p1));
                    LineCell2(cfg, sheet, keyF2, GetLineData2(cfg, p2));
                }
                else
                {
                    EventNumberCell(cfg, sheet, keyEvt, evt, pos);
                    LineCell(cfg, sheet, keyF1, GetLineData(cfg, p1));
                    LineCell(cfg, sheet, keyF2, GetLineData(cfg, p2));
                }


                var c = pos.in_fight_id[0];
                var needPlayer = c == 'R' || pg.teamCount == 7 || pg.teamCount == 6;

                if (needPlayer)
                {
                    FindCell(cfg, sheet, keyF1O, p1.org);
                    FindCell(cfg, sheet, keyF1N1, p1.name);
                    FindCell(cfg, sheet, keyF1N2, p1.number);
                    FindCell(cfg, sheet, keyF1N3, p1.lottery);

                    FindCell(cfg, sheet, keyF2O, p2.org);
                    FindCell(cfg, sheet, keyF2N1, p2.name);
                    FindCell(cfg, sheet, keyF2N2, p2.number);
                    FindCell(cfg, sheet, keyF2N3, p2.lottery);
                }

                switch (pos.in_fight_id)
                {
                    case "M002-01":
                    case "R004-01":
                    case "R004-02":
                    case "RNK78-01":
                    case "RNK56-01":
                        FindCell(cfg, sheet, keyW, GetLineData(cfg, pw));
                        break;
                }
            }
        }

        private string GetLineData(TConfig cfg, TPlayer p)
        {
            switch (cfg.line_type)
            {
                case LineDataType.name: return p.name;
                default: return p.number;
            }
        }

        private string GetLineData2(TConfig cfg, TPlayer p)
        {
            return p.lottery;
        }

        private TEvt FindEvent(TConfig cfg, Dictionary<string, TEvt> eventMap, string in_fight_id)
        {
            if (!eventMap.ContainsKey(in_fight_id))
            {
                return new TEvt
                {
                    in_fight_id = in_fight_id,
                    en_site = string.Empty,
                    in_tree_no = string.Empty,
                    in_win_sign_no = string.Empty,
                    in_win_local_note = string.Empty,
                    show_no = string.Empty,
                    itmF1 = cfg.inn.newItem(),
                    itmF2 = cfg.inn.newItem(),
                };
            }

            var evt = eventMap[in_fight_id];
            if (evt.itmF1 == null) evt.itmF1 = cfg.inn.newItem();
            if (evt.itmF2 == null) evt.itmF2 = cfg.inn.newItem();
            return evt;
        }

        private TPlayer FindPlayer(TConfig cfg, Dictionary<string, Item> teamMap, string in_sign_no)
        {
            if (in_sign_no == "" || in_sign_no == "0")
            {
                return EmptyPlayer("");
            }

            if (!teamMap.ContainsKey(in_sign_no))
            {
                return EmptyPlayer(in_sign_no);
            }

            var item = teamMap[in_sign_no];
            return ItemPlayer(in_sign_no, item);
        }

        private TPlayer ItemPlayer(string in_sign_no, Item item)
        {
            var r = new TPlayer
            {
                org = item.getProperty("map_short_org", ""),
                name = item.getProperty("in_name", ""),
                lottery = item.getProperty("in_lottery_99", ""),
                number = in_sign_no
            };
            var w = item.getProperty("in_weight_message", "");
            if (w != "") r.name += w;
            return r;
        }

        private TPlayer EmptyPlayer(string in_sign_no)
        {
            return new TPlayer
            {
                number = in_sign_no,
                org = "",
                name = "",
                lottery = ""
            };
        }

        private TSect MapProgram(TConfig cfg, Item item)
        {
            var pg = new TSect
            {
                id = item.getProperty("id", ""),
                name = item.getProperty("in_name3", ""),
                alias = item.getProperty("in_short_name", ""),
                day = item.getProperty("in_fight_day", ""),
                count = item.getProperty("in_team_count", "0"),
                eventCount = item.getProperty("in_event_count", "0"),
                battle = item.getProperty("in_battle_type", ""),
                site = item.getProperty("in_site_mat", ""),//GetEnglishSite(item.getProperty("site_code", "")),
            };
            pg.teamCount = GetInt(pg.count);
            pg.teamMap = GetMeetingTeams(cfg, pg.id);
            pg.eventMap = GetMeetingEvents(cfg, pg.id);
            pg.posiMap = GetPositionMap(cfg, pg.count, pg.battle);
            pg.name = pg.name.Replace("個人組-", "");
            return pg;
        }

        private TPosM GetPositionMap(TConfig cfg, string in_team_count, string pg_battle_type)
        {
            string battle = cfg.in_battle_mode;
            if (pg_battle_type == "TopTwo")
            {
                battle = "knockout";
            }

            var sql = "SELECT TOP 1 * FROM IN_MEETING_DRAWSHEET WITH(NOLOCK) WHERE in_mode = '{#in_mode}' AND in_team_count = {#in_team_count}";
            sql = sql.Replace("{#in_mode}", battle)
                .Replace("{#in_team_count}", in_team_count);

            var item = cfg.inn.applySQL(sql);
            if (item.isError() || item.getResult() == "") return null;

            var model = new TPosM
            {
                id = item.getProperty("id", ""),
                ci_title = item.getProperty("in_title", ""),
                ci_day = item.getProperty("in_day", ""),
                ci_weight = item.getProperty("in_weight", ""),
                ci_address = item.getProperty("in_address", ""),
                ci_site = item.getProperty("in_site", ""),
                ci_count = item.getProperty("in_count", ""),
            };

            model.listB = GetEventPositions(cfg, model.id);

            return model;
        }

        private List<TPosB> GetEventPositions(TConfig cfg, string source_id)
        {
            var sql = @"
                SELECT 
                    *
                FROM 
	                IN_MEETING_DRAWSHEET_EVENT WITH(NOLOCK) 
                WHERE 
	                source_id = '{#source_id}'
                ORDER BY
	                in_fight_id
            ";

            sql = sql.Replace("{#source_id}", source_id);

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            var rows = new List<TPosB>();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var row = new TPosB
                {
                    in_fight_id = item.getProperty("in_fight_id", ""),
                    in_word = item.getProperty("in_word", ""),
                    ci_tree_no = item.getProperty("in_tree_no", ""),
                    ci_line_f1 = item.getProperty("in_line_f1", ""),
                    ci_line_f2 = item.getProperty("in_line_f2", ""),
                    ci_line_win = item.getProperty("in_line_win", ""),
                    ci_foot1_no = item.getProperty("in_foot1_no", ""),
                    ci_foot1_org = item.getProperty("in_foot1_org", ""),
                    ci_foot1_name = item.getProperty("in_foot1_name", ""),
                    ci_foot1_lottery = item.getProperty("in_foot1_lottery", ""),
                    ci_foot2_no = item.getProperty("in_foot2_no", ""),
                    ci_foot2_org = item.getProperty("in_foot2_org", ""),
                    ci_foot2_name = item.getProperty("in_foot2_name", ""),
                    ci_foot2_lottery = item.getProperty("in_foot2_lottery", ""),
                };
                rows.Add(row);
            }
            return rows;
        }


        private void EventNumberCell(TConfig cfg, Spire.Xls.Worksheet sheet, string key, TEvt evt, TPosB pos)
        {
            var ranges = sheet.FindAllString(key, true, true);
            if (ranges == null || ranges.Length == 0) return;
            foreach (Spire.Xls.CellRange range in ranges)
            {
                switch (cfg.event_number_type)
                {
                    case EventNumberType.sect:
                        if (evt.wait_tag)
                        {
                            range.Text = "";//不秀決賽等編號
                        }
                        else
                        {
                            range.Text = pos.in_word;
                        }
                        break;
                    case EventNumberType.preliminary:
                        if (evt.wait_tag)
                        {
                            range.Text = "";//不秀決賽等編號
                        }
                        else
                        {
                            range.Text = evt.show_no;
                        }
                        break;
                    case EventNumberType.all:
                        range.Text = evt.show_no;
                        break;
                    default:
                        range.Text = "";
                        break;
                }
            }
        }

        private void LineCell(TConfig cfg, Spire.Xls.Worksheet sheet, string key, string value)
        {
            var ranges = sheet.FindAllString(key, true, true);
            if (ranges == null || ranges.Length == 0) return;

            foreach (Spire.Xls.CellRange range in ranges)
            {
                range.Text = "";
                // if (cfg.isDraw)
                // {
                //     range.Text = "";
                // }
                // else
                // {
                //     range.Text = value;
                // }
            }
        }

        private void LineCell2(TConfig cfg, Spire.Xls.Worksheet sheet, string key, string value)
        {
            var ranges = sheet.FindAllString(key, true, true);
            if (ranges == null || ranges.Length == 0) return;

            foreach (Spire.Xls.CellRange range in ranges)
            {
                range.Text = "";
                range.Text = value;
            }
        }

        private void FindCell(TConfig cfg, Spire.Xls.Worksheet sheet, string key, string value)
        {
            var ranges = sheet.FindAllString(key, true, true);
            if (ranges == null || ranges.Length == 0) return;

            foreach (Spire.Xls.CellRange range in ranges)
            {
                range.Text = value;
            }
        }

        //設定資料格
        private void SetText(Spire.Xls.Worksheet sheet, string cr, string value)
        {
            if (string.IsNullOrWhiteSpace(cr)) return;

            var range = sheet.Range[cr];
            range.Text = value == null ? string.Empty : value;
            //range.Style.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;
            //range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
            //range.IsWrapText = true;
        }

        private TExport ExportInfo(TConfig cfg, string in_name)
        {
            Item itmXls = cfg.inn.applyMethod("In_Meeting_Excel_Path", "<in_name>" + in_name + "</in_name>");
            string Export_Path = itmXls.getProperty("export_path", "");
            string Template_Path = itmXls.getProperty("template_path", "");

            TExport result = new TExport
            {
                template_Path = Template_Path,
                export_Path = Export_Path,
            };

            return result;
        }

        private string GetEnglishSite(string value)
        {
            switch (value)
            {
                case "1": return "A";
                case "2": return "B";
                case "3": return "C";
                case "4": return "D";
                case "5": return "E";
                case "6": return "F";
                default: return value;
            }
        }

        private Item GetMeetingItem(TConfig cfg)
        {
            var sql = "SELECT id, in_title, in_battle_mode, in_battle_repechage FROM IN_MEETING WITH(NOLOCK)"
                + " WHERE id = '{#meeting_id}'";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            return cfg.inn.applySQL(sql);
        }

        private List<TSect> GetMeetingPrograms(TConfig cfg)
        {
            var condition = "";

            if (cfg.program_id != "")
            {
                condition = " AND t1.id = '" + cfg.program_id + "'";
                return OneMeetingProgram(cfg, condition);
            }
            else if (cfg.in_fight_day != "")
            {
                condition = " AND t3.in_fight_day = '" + cfg.in_fight_day + "'";
                return DayMeetingPrograms(cfg, condition);
            }
            else
            {
                if (cfg.in_l1 == "NOT_U")
                {
                    condition += " AND t1.in_l1 NOT LIKE 'U%'";
                }
                else if (cfg.in_l1 != "" && cfg.in_l1 != "ALL")
                {
                    condition += " AND t1.in_l1 = N'" + cfg.in_l1 + "'";
                }
                if (cfg.in_l2 != "" && cfg.in_l2 != "ALL")
                {
                    condition += " AND t1.in_l2 = N'" + cfg.in_l2 + "'";
                }
                if (cfg.in_l3 != "" && cfg.in_l3 != "ALL")
                {
                    condition += " AND t1.in_l3 = N'" + cfg.in_l3 + "'";
                }
                return LevelMeetingPrograms(cfg, condition);
            }
        }

        private List<TSect> OneMeetingProgram(TConfig cfg, string condition)
        {
            // var teamCountCond = cfg.need_one_player_sheet == "Y"
            //     ? ""
            //     : "AND ISNULL(t1.in_team_count, 0) > 1";

            var sql = @"
                SELECT 
                    t1.*
                	, t3.in_code		AS 'site_code'
                	, t3.in_name		AS 'site_name'
                FROM 
                	IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                LEFT OUTER JOIN
                	IN_MEETING_ALLOCATION t2 WITH(NOLOCK)
                	ON t2.in_program = t1.id
                LEFT OUTER JOIN
                	IN_MEETING_SITE t3 WITH(NOLOCK)
                	ON t3.id = t2.in_site
                WHERE
	                t1.in_meeting = '{#meeting_id}'
                    {#condition}
            ";

            //sql = sql.Replace("{#teamCountCond}", teamCountCond);

            return ConvertSections(cfg, sql, condition);
        }

        private List<TSect> DayMeetingPrograms(TConfig cfg, string condition)
        {
            var teamCountCond = cfg.need_one_player_sheet == "Y"
                ? ""
                : "AND ISNULL(t3.in_team_count, 0) > 1";

            var sql = @"
                SELECT 
                	t2.in_code		AS 'site_code'
                	, t2.in_name	AS 'site_name'
                	, t3.*
                FROM 
                	IN_MEETING_PROGRAM t3 WITH(NOLOCK)
                LEFT JOIN
                	IN_MEETING_ALLOCATION t1 WITH(NOLOCK)
                	ON t1.in_program = t3.id
                LEFT JOIN
                	IN_MEETING_SITE t2 WITH(NOLOCK)
                	ON t2.id = t1.in_site
                WHERE
	                t3.in_meeting = '{#meeting_id}'
                    {#teamCountCond}
                    {#condition}
                ORDER BY
	                t3.in_site_mat2
	                , t3.in_sort_order
            ";

            sql = sql.Replace("{#teamCountCond}", teamCountCond);

            return ConvertSections(cfg, sql, condition);
        }

        private List<TSect> LevelMeetingPrograms(TConfig cfg, string condition)
        {
            var teamCountCond = cfg.need_one_player_sheet == "Y"
                ? ""
                : "AND ISNULL(t1.in_team_count, 0) > 1";

            var sql = @"
                SELECT 
                	t3.in_code		AS 'site_code'
                	, t3.in_name		AS 'site_name'
                	, t1.*
                FROM 
                	IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                LEFT OUTER JOIN
                	IN_MEETING_ALLOCATION t2 WITH(NOLOCK)
                	ON t2.in_program = t1.id
                LEFT OUTER JOIN
                	IN_MEETING_SITE t3 WITH(NOLOCK)
                	ON t3.id = t2.in_site
                WHERE
	                t1.in_meeting = '{#meeting_id}'
                    {#teamCountCond}
                    {#condition}
                ORDER BY
	                t1.in_sort_order
            ";

            sql = sql.Replace("{#teamCountCond}", teamCountCond);

            return ConvertSections(cfg, sql, condition);
        }

        private List<TSect> ConvertSections(TConfig cfg, string sql, string condition)
        {
            var sql_qry = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#condition}", condition);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql_qry);

            var rows = new List<TSect>();
            var items = cfg.inn.applySQL(sql_qry);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var row = MapProgram(cfg, item);
                rows.Add(row);
            }
            return rows;
        }

        private Dictionary<string, Item> GetMeetingTeams(TConfig cfg, string program_id)
        {
            var sql = @"
                SELECT 
	                *
                FROM 
	                VU_MEETING_PTEAM WITH(NOLOCK)
                WHERE
	                source_id = '{#program_id}'
            ";

            sql = sql.Replace("{#program_id}", program_id);

            var map = new Dictionary<string, Item>();
            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var in_sign_no = item.getProperty("in_sign_no", "0");
                if (in_sign_no == "" || in_sign_no == "0") continue;
                if (map.ContainsKey(in_sign_no)) continue;
                map.Add(in_sign_no, item);
            }
            return map;
        }

        private Dictionary<string, TEvt> GetMeetingEvents(TConfig cfg, string program_id)
        {
            var sql = @"
                SELECT
	                t1.id AS 'event_id'
	                , t1.in_fight_id
                    , t1.in_tree_no
                    , t1.in_win_sign_no
                    , t1.in_win_local_note
                    , t1.in_wait_tag
	                , t2.in_sign_foot
	                , t2.in_sign_no
                    , t2.in_score_c1
                    , t2.in_score_c2
                    , t2.in_score1
                    , t2.in_score2
                    , t2.in_score4
                    , t2.in_score5
                    , t2.in_score
                    , t2.in_cpts
                    , t2.in_status
                    , t3.in_code    AS 'site_code'
                    , t3.in_name    AS 'site_name'
                    , t3.in_code_en AS 'en_site'
                FROM
	                IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                LEFT OUTER JOIN
	                IN_MEETING_SITE t3 WITH(NOLOCK)
	                ON t3.id = t1.in_site
                WHERE
	                t1.source_id = '{#program_id}'
                ORDER BY
	                t1.in_tree_sort
	                , t1.in_fight_id
	                , t2.in_sign_foot
            ";

            sql = sql.Replace("{#program_id}", program_id);

            var map = new Dictionary<string, TEvt>();
            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var in_fight_id = item.getProperty("in_fight_id", "");
                var in_win_sign_no = item.getProperty("in_win_sign_no", "");
                var in_sign_foot = item.getProperty("in_sign_foot", "");
                if (!map.ContainsKey(in_fight_id))
                {
                    var row = new TEvt
                    {
                        in_fight_id = in_fight_id,
                        in_win_sign_no = in_win_sign_no,
                        en_site = item.getProperty("en_site", ""),
                        in_tree_no = item.getProperty("in_tree_no", ""),
                        in_win_local_note = item.getProperty("in_win_local_note", ""),
                        wait_tag = item.getProperty("in_wait_tag", "") == "1",
                        show_no = string.Empty,
                    };

                    if (row.in_tree_no != "" && row.in_tree_no != "0")
                    {
                        row.show_no = row.en_site + row.in_tree_no;
                    }

                    map.Add(in_fight_id, row);
                }

                var evt = map[in_fight_id];
                if (in_sign_foot == "1")
                {
                    evt.itmF1 = item;
                }
                else if (in_sign_foot == "2")
                {
                    evt.itmF2 = item;
                }
            }
            return map;
        }

        private string FindMeetingVariable(Dictionary<string, string> map, string in_key)
        {
            if (map.ContainsKey(in_key)) return map[in_key];
            return "";
        }

        private Dictionary<string, string> GetMeetingVariables(TConfig cfg)
        {
            var sql = @"
                SELECT 
	                in_key
	                , in_value 
                FROM 
	                IN_MEETING_VARIABLE WITH(NOLOCK)
                WHERE 
	                source_id = '{#meeting_id}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            var map = new Dictionary<string, string>();
            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var in_key = item.getProperty("in_key", "");
                var in_value = item.getProperty("in_value", "");
                map.Add(in_key, in_value);
            }
            return map;
        }

        private string GetMeetingVariable(TConfig cfg, string in_key)
        {
            var sql = "SELECT TOP 1 id, in_value FROM IN_MEETING_VARIABLE WITH(NOLOCK) WHERE source_id = '" + cfg.meeting_id + "' AND in_key = '" + in_key + "'";
            var item = cfg.inn.applySQL(sql);
            if (item.isError() || item.getResult() == "") return "";
            return item.getProperty("in_value", "");
        }

        private void SetDrawEventNumberType(TConfig cfg)
        {
            var draw_event_number = GetMeetingVariable(cfg, "draw_event_number");
            switch (draw_event_number)
            {
                case "":
                    cfg.event_number_type = EventNumberType.empty;
                    break;
                case "sect":
                    cfg.event_number_type = EventNumberType.sect;
                    break;
                case "preliminary":
                    cfg.event_number_type = EventNumberType.preliminary;
                    break;
                case "all":
                    cfg.event_number_type = EventNumberType.all;
                    break;

            }
        }

        private enum EventNumberType
        {
            empty = 100,
            sect = 200,
            preliminary = 300,
            all = 500,
        }

        private enum LineDataType
        {
            name = 1,
            number = 2
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }

            public string meeting_id { get; set; }
            public string program_id { get; set; }
            public string in_fight_day { get; set; }
            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }
            public string report_type { get; set; }
            public string scene { get; set; }

            public EventNumberType event_number_type { get; set; }

            public Item itmMeeting { get; set; }
            public string mt_title { get; set; }
            public string in_battle_mode { get; set; }
            public string in_battle_repechage { get; set; }

            public Dictionary<string, string> variableMap { get; set; }
            public string fight_address { get; set; }
            public string fight_battle_mode { get; set; }
            public string need_one_player_sheet { get; set; }
            public bool is_mix_battle { get; set; }

            public int sheetCount { get; set; }
            public LineDataType line_type { get; set; }
            public System.Drawing.Color head_color { get; set; }
        }

        private class TExport
        {
            public string template_Path { get; set; }
            public string export_Path { get; set; }
        }


        private class TPosM
        {
            public string id { get; set; }
            public string ci_title { get; set; }
            public string ci_day { get; set; }
            public string ci_weight { get; set; }
            public string ci_address { get; set; }
            public string ci_site { get; set; }
            public string ci_count { get; set; }
            public List<TPosB> listB { get; set; }
        }

        private class TPosB
        {
            public string in_fight_id { get; set; }
            public string in_word { get; set; }
            public string ci_tree_no { get; set; }
            public string ci_line_f1 { get; set; }
            public string ci_line_f2 { get; set; }
            public string ci_line_win { get; set; }
            public string ci_foot1_no { get; set; }
            public string ci_foot1_org { get; set; }
            public string ci_foot1_name { get; set; }
            public string ci_foot1_lottery { get; set; }
            public string ci_foot2_no { get; set; }
            public string ci_foot2_org { get; set; }
            public string ci_foot2_name { get; set; }
            public string ci_foot2_lottery { get; set; }
        }

        private class TSect
        {
            public string id { get; set; }
            public string name { get; set; }
            public string day { get; set; }
            public string battle { get; set; }
            public string count { get; set; }
            public string site { get; set; }
            public string alias { get; set; }
            public Dictionary<string, Item> teamMap { get; set; }
            public Dictionary<string, TEvt> eventMap { get; set; }
            public int teamCount { get; set; }
            public string eventCount { get; set; }
            public TPosM posiMap { get; set; }
        }

        private class TPlayer
        {
            public string org { get; set; }
            public string name { get; set; }
            public string lottery { get; set; }
            public string number { get; set; }
        }

        private class TEvt
        {
            public string in_fight_id { get; set; }
            public string en_site { get; set; }
            public string in_tree_no { get; set; }
            public string in_win_sign_no { get; set; }
            public string in_win_local_note { get; set; }
            public bool wait_tag { get; set; }
            public string show_no { get; set; }
            public Item itmF1 { get; set; }
            public Item itmF2 { get; set; }
        }

        private int GetInt(string value, int def = 0)
        {
            int result = def;
            int.TryParse(value, out result);
            return result;
        }
    }
}