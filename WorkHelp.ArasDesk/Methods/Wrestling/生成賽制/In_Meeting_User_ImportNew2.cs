﻿using Aras.IOM;
using System;

namespace WorkHelp.ArasDesk.Methods.Wrestling.生成賽制
{
    public class In_Meeting_User_ImportNew2 : Item
    {
        public In_Meeting_User_ImportNew2(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 新版與會者匯入
                日誌: 
                    - 2024-07-12: 創建 (lina)
            */

            System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "In_Meeting_User_ImportNew2";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,

                meeting_id = itmR.getProperty("meeting_id", ""),
                in_battle_mode = itmR.getProperty("in_battle_mode", ""),
                in_battle_repechage = itmR.getProperty("in_battle_repechage", ""),
                scene = itmR.getProperty("scene", ""),
            };

            cfg.new_battle_type = "WrestlingTopTwo";
            if (cfg.in_battle_mode == "knockout" && cfg.in_battle_repechage == "N")
            {
                cfg.new_battle_type = "TopTwo";
            }

            Run(cfg, itmR);

            return itmR;
        }

        private void Run(TConfig cfg, Item itmReturn)
        {
            var sql = "";

            sql = "UPDATE IN_MEETING SET"
                + "  in_battle_mode = '" + cfg.in_battle_mode + "'"
                + ", in_battle_repechage = '" + cfg.in_battle_repechage + "'"
                + " WHERE id = '" + cfg.meeting_id + "'";
            cfg.inn.applySQL(sql);

            //修正 公斤, kg 混淆不清
            FixSurveyOptionWeightLabel(cfg, itmReturn);
            //修正第二階與第三階的競賽形式(與會者、問項)
            FixSurveyFilter(cfg, itmReturn, "自由式");
            FixSurveyFilter(cfg, itmReturn, "希羅式");

            //重建場地資料
            RebuildMeetingSites(cfg, itmReturn.getProperty("site_count", "0"));

            //賽會參數設定
            RebuildMtVariable(cfg, itmReturn);
            //過磅狀態設定
            RebuildMtWeight(cfg, itmReturn);
            //清除賽程組別資料
            ClearAllPrograms(cfg, itmReturn);

            //建立賽程組別資料
            CreatePrograms(cfg);
            //更新隊伍資料
            UpdateTeamData(cfg);

            //建立場地分配
            CreateAllocation(cfg, itmReturn);
            //建立競賽連結
            CreateQrCode(cfg, itmReturn);
            //清除本地同步資料
            RemoveLocalSchedules(cfg, itmReturn);

            //將人數少於等於 1 者關閉
            //ClosePrograms(cfg, itmReturn);

            //修正簡稱
            FixProgramShortName(cfg, itmReturn);

            //重設組別排序
            ResetProgramSortOrder(cfg, itmReturn);

            AppendProgramList(cfg, itmReturn);
        }

        //修正簡稱
        private void FixProgramShortName(TConfig cfg, Item itmReturn)
        {
            var sql = "SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK)"
                + " WHERE in_meeting = '" + cfg.meeting_id + "'"
                + " ORDER BY in_sort_order";

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var id = item.getProperty("id", "");
                var name = item.getProperty("in_name", "");
                var in_l1 = item.getProperty("in_l1", "");
                var in_l2 = item.getProperty("in_l2", "");
                var in_l3 = item.getProperty("in_l3", "").ToLower().Replace("公斤", "kg");

                var age = "";
                if (in_l1.Contains("國小"))
                {
                    if (in_l1.Contains("高")) age = "小高";
                    else if (in_l1.Contains("中")) age = "小中";
                    else if (in_l1.Contains("低")) age = "小低";
                    else age = "小";
                }
                else if (in_l1.Contains("國中")) age = "中";
                else if (in_l1.Contains("高中")) age = "高";
                else if (in_l1.Contains("大專社會")) age = "大社";
                else if (in_l1.Contains("大專")) age = "大";
                else if (in_l1.Contains("社會")) age = "大";
                else if (in_l1.Contains("公開")) age = "公";
                else if (in_l1.Contains("一般")) age = "公";
                else age = "";
            
                var gender = "";
                if (name.Contains("混")) gender = "混";
                else if (name.Contains("男")) gender = "男";
                else if (name.Contains("女")) gender = "女";

                var style = "";
                if (in_l2.Contains("自由")) style = "自由式";
                else if (in_l2.Contains("希羅")) style = "希羅式";

                var weight = "";
                if (in_l3.Contains("第十五級")) weight = "十五";
                else if (in_l3.Contains("第十四級")) weight = "十四";
                else if (in_l3.Contains("第十三級")) weight = "十三";
                else if (in_l3.Contains("第十二級")) weight = "十二";
                else if (in_l3.Contains("第十一級")) weight = "十一";
                else if (in_l3.Contains("第十級")) weight = "十";
                else if (in_l3.Contains("第九級")) weight = "九";
                else if (in_l3.Contains("第八級")) weight = "八";
                else if (in_l3.Contains("第七級")) weight = "七";
                else if (in_l3.Contains("第六級")) weight = "六";
                else if (in_l3.Contains("第五級")) weight = "五";
                else if (in_l3.Contains("第四級")) weight = "四";
                else if (in_l3.Contains("第三級")) weight = "三";
                else if (in_l3.Contains("第二級")) weight = "二";
                else if (in_l3.Contains("第一級")) weight = "一";
                else if (in_l3.Contains("超輕量級")) weight = "超輕量";
                else weight = in_l3;

                var in_short_name = age + gender + style + weight;

                var sql_upd = "UPDATE IN_MEETING_PROGRAM SET"
                    + "  in_short_name = N'" + in_short_name + "'"
                    + ", in_n1 = N'" + in_l1 + "'"
                    + ", in_n2 = N'" + in_l2 + "'"
                    + ", in_n3 = N'第" + weight + "級'"
                    + " WHERE id = '" + id + "'";
                
                cfg.inn.applySQL(sql_upd);
            }
        }

        private void AppendProgramList(TConfig cfg, Item itmReturn)
        {
            var sql = "SELECT id, in_name FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE in_meeting = '" + cfg.meeting_id + "'";
            var itmPrograms = cfg.inn.applySQL(sql);
            var count = itmPrograms.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var itmProgram = itmPrograms.getItemByIndex(i);
                itmProgram.setType("inn_program");
                itmReturn.addRelationship(itmProgram);
            }
        }

        private void RemoveLocalSchedules(TConfig cfg, Item itmReturn)
        {
            var sql = "SELECT id, item_number FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'";
            var itmMeeting = cfg.inn.applySQL(sql);
            var item_number = itmMeeting.getProperty("item_number", "");

            sql = "DELETE FROM In_Local_Schedule WHERE MNumber = '" + item_number + "'";
            cfg.inn.applySQL(sql);
        }

        private void FixSurveyOptionWeightLabel(TConfig cfg, Item itmReturn)
        {
            var sql1 = @"
                UPDATE t3 SET
	                t3.in_value = REPLACE(t3.in_value, ' 公斤以下', 'kg 以下')
                FROM 
	                IN_MEETING_SURVEYS t1 WITH(NOLOCK) 
                INNER JOIN 
	                IN_SURVEY t2 WITH(NOLOCK) 
	                ON t2.id = t1.related_id 
                INNER JOIN 
	                IN_SURVEY_OPTION t3 WITH(NOLOCK) 
	                ON t3.source_id = t1.related_id 
                WHERE 
	                t1.source_id = '{#meeting_id}'
	                AND t2.in_property = 'in_l3'
            ";

            sql1 = sql1.Replace("{#meeting_id}", cfg.meeting_id);

            cfg.inn.applySQL(sql1);


            var sql2 = @"
                UPDATE t3 SET
	                t3.in_value = REPLACE(t3.in_value, 'Kg', 'kg')
                FROM 
	                IN_MEETING_SURVEYS t1 WITH(NOLOCK) 
                INNER JOIN 
	                IN_SURVEY t2 WITH(NOLOCK) 
	                ON t2.id = t1.related_id 
                INNER JOIN 
	                IN_SURVEY_OPTION t3 WITH(NOLOCK) 
	                ON t3.source_id = t1.related_id 
                WHERE 
	                t1.source_id = '{#meeting_id}'
	                AND t2.in_property = 'in_l3'
            ";

            sql2 = sql1.Replace("{#meeting_id}", cfg.meeting_id);

            cfg.inn.applySQL(sql2);
        }

        private void FixSurveyFilter(TConfig cfg, Item itmReturn, string itemName)
        {
            var sql1 = @"
                UPDATE t3 SET
	                t3.in_value = '{#itemName}'
                FROM 
	                IN_MEETING_SURVEYS t1 WITH(NOLOCK) 
                INNER JOIN 
	                IN_SURVEY t2 WITH(NOLOCK) 
	                ON t2.id = t1.related_id 
                INNER JOIN 
	                IN_SURVEY_OPTION t3 WITH(NOLOCK) 
	                ON t3.source_id = t1.related_id 
                WHERE 
	                t1.source_id = '{#meeting_id}'
	                AND t2.in_property = 'in_l2'
	                AND t3.in_value LIKE '%{#itemName}%'
            ";

            sql1 = sql1.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#itemName}", itemName);

            cfg.inn.applySQL(sql1);

            var sql2 = @"
                UPDATE t3 SET
	                t3.in_filter = '{#itemName}'
                FROM 
	                IN_MEETING_SURVEYS t1 WITH(NOLOCK) 
                INNER JOIN 
	                IN_SURVEY t2 WITH(NOLOCK) 
	                ON t2.id = t1.related_id 
                INNER JOIN 
	                IN_SURVEY_OPTION t3 WITH(NOLOCK) 
	                ON t3.source_id = t1.related_id 
                WHERE 
	                t1.source_id = '{#meeting_id}'
	                AND t2.in_property = 'in_l3'
	                AND t3.in_filter LIKE '%{#itemName}%'
            ";

            sql2 = sql2.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#itemName}", itemName);

            cfg.inn.applySQL(sql2);

            var sql3 = "UPDATE IN_MEETING_USER SET"
                + "  in_l2 = '" + itemName + "'"
                + " WHERE source_id = '" + cfg.meeting_id + "' AND in_l2 LIKE '%" + itemName + "%'";

            cfg.inn.applySQL(sql3);
        }

        //重設組別排序
        private void ResetProgramSortOrder(TConfig cfg, Item itmReturn)
        {
            var sql3 = @"
                UPDATE t1 SET
	                t1.in_sort_order = t2.sort_order
                FROM
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
	                [VU_MEETING_SVY_L3] t2
	                ON t2.source_id = t1.in_meeting
	                AND t2.in_grand_filter = t1.in_l1
	                AND t2.in_filter = t1.in_l2
	                AND t2.in_value = t1.in_l3
                WHERE
	                t1.in_meeting = '{#meeting_id}'
            ".Replace("{#meeting_id}", cfg.meeting_id);
            cfg.inn.applySQL(sql3);

            var sql4 = @"
                UPDATE t1 SET 
	                t1.in_sort_order = t2.rn * 100
                FROM
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
                (
	                SELECT
		                id
		                , ROW_NUMBER() OVER (ORDER BY in_sort_order) AS 'rn'
	                FROM
		                IN_MEETING_PROGRAM WITH(NOLOCK)
	                WHERE
		                in_meeting = '{#meeting_id}'
                ) t2 ON t2.id = t1.id
            ".Replace("{#meeting_id}", cfg.meeting_id);
            cfg.inn.applySQL(sql4);

            var sql5 = @"
                UPDATE t11 SET
                   t11.in_team_index = t12.rn + 1000
                FROM
                	IN_MEETING_PTEAM t11 WITH(NOLOCK)
                INNER JOIN
                (
                	SELECT
                		t2.id
                		, ROW_NUMBER() OVER (ORDER BY t1.in_sort_order, t2.in_stuff_b1, t2.in_name) AS 'rn'
                	FROM
                		IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                	INNER JOIN
                		IN_MEETING_PTEAM t2 WITH(NOLOCK)
                		ON t2.source_id = t1.id
                	WHERE 
                		t1.in_meeting = '{#meeting_id}'
                ) t12 ON t12.id = t11.id
            ".Replace("{#meeting_id}", cfg.meeting_id);
            cfg.inn.applySQL(sql5);

            var sql6 = @"
                UPDATE IN_MEETING_PTEAM SET in_player_no = in_team_index WHERE in_meeting = '{#meeting_id}'
            ".Replace("{#meeting_id}", cfg.meeting_id);
            cfg.inn.applySQL(sql6);
        }

        //將人數少於等於 1 者關閉
        private void ClosePrograms(TConfig cfg, Item itmReturn)
        {
            string sql = @"
                UPDATE t1 SET
                	t1.in_team_count = 0
                FROM
                	IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                WHERE
                	t1.in_meeting = '{#meeting_id}'
                	AND ISNULL(t1.in_team_count, 0) <= 1
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            cfg.inn.applySQL(sql);
        }

        //建立競賽連結
        private void CreateQrCode(TConfig cfg, Item itmReturn)
        {
            Item itmData = cfg.inn.newItem("In_Meeting");
            itmData.setProperty("meeting_id", cfg.meeting_id);
            itmData.apply("in_qrcode_create");
        }

        //建立場地分配
        private void CreateAllocation(TConfig cfg, Item itmReturn)
        {
            //建立場地分配
            var sql = "SELECT * FROM IN_MEETING_SITE WITH(NOLOCK) WHERE in_meeting = '{#meeting_id}' AND in_code = 1";
            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);
            var itmSite = cfg.inn.applySQL(sql);
            if (itmSite.isError() || itmSite.getResult() == "")
            {
                return;
            }

            //清除舊場地分配
            cfg.inn.applySQL("DELETE FROM IN_MEETING_ALLOCATION WHERE in_meeting = '" + cfg.meeting_id + "'");

        }

        private void UpdateTeamData(TConfig cfg)
        {
            var sql = @"
                UPDATE t1 SET
                	t1.in_sign_no = t11.in_draw_no
                	, t1.in_section_no = t11.in_draw_no
                	, t1.in_team_index = t11.in_index
                FROM
                	IN_MEETING_PTEAM t1 WITH(NOLOCK)
                INNER JOIN
                	IN_MEETING_PROGRAM t2 WITH(NOLOCK)
                	ON t2.id = t1.source_id
                INNER JOIN
                	IN_MEETING_USER t11 WITH(NOLOCK)
                	ON t11.source_id = t2.in_meeting
                	AND t11.in_l1 = t2.in_l1
                	AND t11.in_l2 = t2.in_l2
                	AND t11.in_l3 = t2.in_l3
                	AND t11.in_sno = t1.in_sno
                WHERE
                	t11.source_id = '{#meeting_id}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            cfg.inn.applySQL(sql);
        }

        private void CreatePrograms(TConfig cfg)
        {
            Item itmData = cfg.inn.newItem("In_Meeting");
            itmData.setProperty("meeting_id", cfg.meeting_id);
            itmData.setProperty("in_fight_day", "");
            itmData.setProperty("battle_type", cfg.new_battle_type);
            itmData.setProperty("battle_repechage", "");
            itmData.setProperty("rank_type", "SameRank");
            itmData.setProperty("surface_code", "32");
            itmData.setProperty("robin_player", "0");
            itmData.setProperty("sub_event", "0");
            itmData.setProperty("mode", "save");
            itmData.apply("In_Meeting_Program");
        }


        //清除所有比賽資料
        private void ClearAllPrograms(TConfig cfg, Item itmReturn)
        {
            var sql = "";

            sql = @"
                SELECT
	                DISTINCT t1.id
                FROM
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                WHERE
	                t1.in_meeting = '{#meeting_id}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            var itmPrograms = cfg.inn.applySQL(sql);
            int count = itmPrograms.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item itmProgram = itmPrograms.getItemByIndex(i);
                string program_id = itmProgram.getProperty("id", "");

                //刪除隊伍
                sql = "DELETE FROM IN_MEETING_PTEAM WHERE source_id = '" + program_id + "'";
                cfg.inn.applySQL(sql);

                //刪除明細
                sql = @"
                    DELETE FROM 
                        IN_MEETING_PEVENT_DETAIL 
                    WHERE 
                        source_id IN
                        (
                            SELECT id FROM IN_MEETING_PEVENT WITH(NOLOCK) WHERE source_id = '{#program_id}'
                        )
                ";
                sql = sql.Replace("{#program_id}", program_id);
                cfg.inn.applySQL(sql);

                //刪除場次
                sql = @"DELETE FROM IN_MEETING_PEVENT WHERE source_id = '{#program_id}'";
                sql = sql.Replace("{#program_id}", program_id);
                cfg.inn.applySQL(sql);
            }

            //刪除無歸屬隊伍
            sql = "DELETE FROM IN_MEETING_PTEAM WHERE in_meeting = '" + cfg.meeting_id + "'";
            cfg.inn.applySQL(sql);

            //移除場地分配
            sql = "UPDATE t1 SET t1.in_program_name = t2.in_name"
                 + " FROM IN_MEETING_ALLOCATION t1 INNER JOIN IN_MEETING_PROGRAM t2 ON t2.id = t1.in_program"
                 + " WHERE t1.in_meeting = '" + cfg.meeting_id + "'";
            cfg.inn.applySQL(sql);

            //移除場地分配
            sql = "UPDATE t1 SET t1.in_program = NULL"
                 + " FROM IN_MEETING_ALLOCATION t1 INNER JOIN IN_MEETING_PROGRAM t2 ON t2.id = t1.in_program"
                 + " WHERE t1.in_meeting = '" + cfg.meeting_id + "'"
                 + " AND ISNULL(t1.in_program_name, '') <> ''";
            cfg.inn.applySQL(sql);

            //移除團體量級
            sql = "UPDATE t1 SET t1.in_program = NULL"
                 + " FROM IN_MEETING_PSECT t1 INNER JOIN IN_MEETING_PROGRAM t2 ON t2.id = t1.in_program"
                 + " WHERE t1.in_meeting = '" + cfg.meeting_id + "'"
                 + " AND ISNULL(t1.in_program_name, '') <> ''";
            cfg.inn.applySQL(sql);

            //刪除組別
            sql = "DELETE FROM IN_MEETING_PROGRAM WHERE in_meeting = '" + cfg.meeting_id + "'";
            cfg.inn.applySQL(sql);

            // //刪除場地
            // sql = "DELETE FROM IN_MEETING_SITE WHERE in_meeting = '" + meeting_id + "'";
            // itmSQL = inn.applySQL(sql);

            sql = "UPDATE IN_MEETING SET in_draw_status = NULL, in_draw_file = NULL, in_site_mode = '' WHERE id = '" + cfg.meeting_id + "'";

            cfg.inn.applySQL(sql);
        }

        //賽會參數設定
        private void RebuildMtVariable(TConfig cfg, Item itmReturn)
        {
            string need_rank57 = itmReturn.getProperty("need_rank57", "1");

            cfg.inn.applySQL("DELETE FROM In_Meeting_Variable WHERE source_id = '" + cfg.meeting_id + "'");

            RebuildMtVariable(cfg, "fight_site", "板橋體育館", "100");
            RebuildMtVariable(cfg, "fight_days", "2024-09-26,2024-09-27,2024-09-28", "150");
            RebuildMtVariable(cfg, "allocate_mode", "cycle", "200");
            RebuildMtVariable(cfg, "fight_robin_player", "5", "300");
            RebuildMtVariable(cfg, "line_color", "red", "400");
            RebuildMtVariable(cfg, "need_rank78", "0", "2100");
            RebuildMtVariable(cfg, "need_rank56", "0", "2200");
            RebuildMtVariable(cfg, "need_rank57", need_rank57, "2300");
            RebuildMtVariable(cfg, "freeze_has_draw_no", "1", "3100");
            RebuildMtVariable(cfg, "draw_report_type", "PDF", "3200");
            RebuildMtVariable(cfg, "draw_event_number", "all", "3300");
        }

        private void RebuildMtVariable(TConfig cfg, string in_key, string in_value, string sort_order)
        {
            Item itmNew = cfg.inn.newItem("In_Meeting_Variable", "add");
            itmNew.setProperty("source_id", cfg.meeting_id);
            itmNew.setProperty("in_key", in_key);
            itmNew.setProperty("in_value", in_value);
            itmNew.setProperty("sort_order", sort_order);
            itmNew.apply();
        }

        //過磅狀態設定
        private void RebuildMtWeight(TConfig cfg, Item itmReturn)
        {
            cfg.inn.applySQL("DELETE FROM IN_MEETING_PWEIGHT WHERE in_meeting = '" + cfg.meeting_id + "'");
            RebuildMtWeight(cfg, "off", "0");
            RebuildMtWeight(cfg, "leave", "1");
            RebuildMtWeight(cfg, "dq", "0");
        }

        private void RebuildMtWeight(TConfig cfg, string in_status, string in_is_remove)
        {
            Item itmNew = cfg.inn.newItem("In_Meeting_PWeight", "add");
            itmNew.setProperty("in_meeting", cfg.meeting_id);
            itmNew.setProperty("in_status", in_status);
            itmNew.setProperty("in_is_remove", in_is_remove);
            itmNew.apply();
        }

        //重建場地資料
        private void RebuildMeetingSites(TConfig cfg, string value)
        {
            var max = GetInt(value);
            if (max <= 0) throw new Exception("場地數量錯誤");

            //清除所有場地資料
            Item itmData = cfg.inn.newItem();
            itmData.setType("In_Meeting");
            itmData.setProperty("meeting_id", cfg.meeting_id);
            itmData.setProperty("mode", "remove");
            itmData.apply("in_meeting_site");

            for (var i = 1; i <= max; i++)
            {
                var code = i.ToString();
                var row = SiteCodeRow(i);

                var itmNew = cfg.inn.newItem("In_Meeting_Site", "add");
                itmNew.setProperty("in_meeting", cfg.meeting_id);
                itmNew.setProperty("in_code", code);
                itmNew.setProperty("in_code_en", row.w1);
                itmNew.setProperty("in_name", "第" + row.w2 + "場地");
                itmNew.setProperty("in_rows", "1");
                itmNew.setProperty("in_cols", value);
                itmNew.setProperty("in_row_index", "0");
                itmNew.setProperty("in_col_index", (i - 1).ToString());
                itmNew.apply();
            }
        }

        private TSP SiteCodeRow(int code)
        {
            switch (code)
            {
                case 1: return new TSP { w1 = "A", w2 = "一" };
                case 2: return new TSP { w1 = "B", w2 = "二" };
                case 3: return new TSP { w1 = "C", w2 = "三" };
                case 4: return new TSP { w1 = "D", w2 = "四" };
                case 5: return new TSP { w1 = "E", w2 = "五" };
                case 6: return new TSP { w1 = "F", w2 = "六" };
                case 7: return new TSP { w1 = "G", w2 = "七" };
                case 8: return new TSP { w1 = "H", w2 = "八" };
                case 9: return new TSP { w1 = "I", w2 = "九" };
                case 10: return new TSP { w1 = "J", w2 = "十" };
                case 11: return new TSP { w1 = "K", w2 = "十一" };
                case 12: return new TSP { w1 = "L", w2 = "十二" };
                default: return new TSP { w1 = "ERR", w2 = "ERR" };
            }
        }

        private class TSP
        {
            public string w1 { get; set; }
            public string w2 { get; set; }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }

            public string meeting_id { get; set; }
            public string in_battle_mode { get; set; }
            public string in_battle_repechage { get; set; }
            public string new_battle_type { get; set; }
            public string scene { get; set; }
            public Item itmMeeting { get; set; }
        }

        private int GetInt(string value, int def = 0)
        {
            int result = def;
            int.TryParse(value, out result);
            return result;
        }
    }
}