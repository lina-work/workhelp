﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.CTOA.Fight
{
    public class in_meeting_program_ctoa : Item
    {
        public in_meeting_program_ctoa(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 定向越野
                日誌: 
                    - 2024-02-29: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "in_meeting_program_ctoa";

            var itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                mnumber = itmR.getProperty("mnumber", ""),
                mday = itmR.getProperty("mday", ""),
                startnumber = itmR.getProperty("startnumber", ""),
                data = itmR.getProperty("data", ""),
                scene = itmR.getProperty("scene", ""),
            };

            switch (cfg.scene)
            {
                case "compulsory_result_edit":
                    CompulsoryResultEdit(cfg, itmR);
                    break;

                case "runner_lap_edit":
                    RunnerLapEdit(cfg, itmR);
                    break;

                case "course_lap_query":
                    CourseLapQuery(cfg, itmR);
                    break;

                case "course_lap_edit":
                    CourseLapEdit(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private string GetRealFullFile(string folder, string fileName)
        {
            var targetFile = folder + @"\" + fileName;
            if (System.IO.File.Exists(targetFile))
            {
                return targetFile;
            }

            var result = "";
            var info = new System.IO.DirectoryInfo(folder);
            var dirs = info.GetDirectories();
            if (dirs != null && dirs.Length > 0)
            {
                for (int i = 0; i < dirs.Length; i++)
                {
                    var d = dirs[i];
                    if (d.Name == "system") continue;
                    targetFile = d.FullName.TrimEnd('\\') + @"\course.dat";
                    if (System.IO.File.Exists(targetFile))
                    {
                        result = targetFile;
                        break;
                    }
                }
            }
            return result;
        }

        //檢視雲端路線檔案
        private void CourseLapQuery(TConfig cfg, Item itmReturn)
        {
            var folder = @"c:\site\in_api\dat\CTOA\" + cfg.mday;
            var fileName = "course.dat";
            if (!System.IO.Directory.Exists(folder)) throw new Exception("參數錯誤 _# folder = " + folder);

            var realFile = GetRealFullFile(folder, fileName);
            if (realFile == "") throw new Exception("參數錯誤 _# folder = " + folder + "、" + fileName);

            var targetContents = System.IO.File.ReadAllText(realFile, System.Text.Encoding.UTF8);
            itmReturn.setProperty("dat_contents", targetContents);
        }

        //修改雲端路線檔案
        private void CourseLapEdit(TConfig cfg, Item itmReturn)
        {
            var folder = @"c:\site\in_api\dat\CTOA\" + cfg.mday;
            var fileName = "course.dat";
            if (!System.IO.Directory.Exists(folder)) throw new Exception("參數錯誤 _# folder = " + folder);

            var realFile = GetRealFullFile(folder, fileName);
            if (realFile == "") throw new Exception("參數錯誤 _# folder = " + folder + "、" + fileName);

            var newContents = itmReturn.getProperty("contents", "");
            System.IO.File.WriteAllText(realFile, newContents, System.Text.Encoding.UTF8);
        }

        private void CompulsoryResultEdit(TConfig cfg, Item itmReturn)
        {
            var p1 = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
            var p2 = "10loAKr3x0Y04cw";
            var p3 = "MAIN";
            var p4 = "COMPULSORY_RESULT_EDIT";
            var p5 = "RUNNER_" + cfg.startnumber;
            var p6 = "OK";
            var p7 = cfg.data;
            var p8 = "0";
            var p9 = "-1";
            var p10 = "0";
            var p11 = "0";
            var p12 = "0";

            var command = p1 + ","
                + p2 + ","
                + p3 + ","
                + p4 + ","
                + p5 + ","
                + p6 + ","
                + p7 + ","
                + p8 + ","
                + p9 + ","
                + p10 + ","
                + p11 + ","
                + p12;

            SaveChange(cfg, command);
        }

        private void RunnerLapEdit(TConfig cfg, Item itmReturn)
        {
            if (cfg.mnumber == "") throw new Exception("參數錯誤: mnumber");
            if (cfg.mday == "") throw new Exception("參數錯誤: mday");
            if (cfg.startnumber == "") throw new Exception("參數錯誤: startnumber");
            if (cfg.data == "") throw new Exception("參數錯誤: data");

            var list = GetEditRows(cfg, cfg.data);
            if (list == null || list.Count == 0) throw new Exception("無異動資料");

            var times = new List<string>();
            for (var i = 0; i < list.Count; i++)
            {
                var x = list[i];
                if (x.code == "S") continue;
                //if (x.code == "F") continue;
                times.Add(x.time);
            }

            var p1 = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
            var p2 = "10nPBkJ0A0407HO";
            var p3 = "MAIN";
            var p4 = "RUNNER_LAP_EDIT";
            var p5 = "RUNNER_" + cfg.startnumber;
            var p6 = string.Join(",", times);

            var command = p1 + ","
                + p2 + ","
                + p3 + ","
                + p4 + ","
                + p5 + ","
                + p6;

            SaveChange(cfg, command);
        }

        private void SaveChange(TConfig cfg, string command)
        {
            var folder = @"c:\site\in_api\dat\CTOA\" + cfg.mday;
            if (!System.IO.Directory.Exists(folder)) throw new Exception("參數錯誤: folder");

            var targetfile = folder + @"\changes.dat";
            var fullfile = folder + @"\changes-system.dat";
            var isOK = false;

            if (System.IO.File.Exists(targetfile))
            {
                isOK = true;
            }
            else
            {
                var folderInfo = new System.IO.DirectoryInfo(folder);
                var dirs = folderInfo.GetDirectories();
                if (dirs != null && dirs.Length > 0)
                {
                    isOK = true;
                    folder = @"c:\site\in_api\dat\CTOA\" + cfg.mday + @"\system";
                    if (!System.IO.Directory.Exists(folder))
                    {
                        System.IO.Directory.CreateDirectory(folder);
                    }
                    fullfile = folder + @"\changes-system.dat";
                }
            }

            if (isOK)
            {
                System.IO.File.AppendAllText(fullfile, command + "\r\n", System.Text.Encoding.UTF8);
            }

        }

        private List<TEditRow> GetEditRows(TConfig cfg, string json)
        {
            try
            {
                return Newtonsoft.Json.JsonConvert.DeserializeObject<List<TEditRow>>(json);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private class TEditRow
        {
            public string no { get; set; }
            public string code { get; set; }
            public string time { get; set; }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string mnumber { get; set; }
            public string mday { get; set; }
            public string startnumber { get; set; }
            public string data { get; set; }
            public string scene { get; set; }
        }
    }
}