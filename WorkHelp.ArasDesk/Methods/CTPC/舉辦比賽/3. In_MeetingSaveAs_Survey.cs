﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.CTPC.Common
{
    public class In_MeetingSaveAs_Survey : Item
    {
        public In_MeetingSaveAs_Survey(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 舉辦賽事-複製問項
                輸入: 
                    1. meeting_id
                    2. item_number
                修改: 
                    2022-05-31 調整為賽事講習共用 (lina)
                    2020-09-07 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]In_MeetingSaveAs_Survey";

            Item itmR = this;

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                strIdentityId = inn.getUserAliases(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                item_number = itmR.getProperty("item_number", ""),
                hideselector = itmR.getProperty("hideselector", ""),
                in_clone = itmR.getProperty("in_clone", ""),
                mode = itmR.getProperty("mode", ""),
            };

            cfg.MtTable = "IN_MEETING";
            cfg.MtSvyTable = "IN_MEETING_SURVEYS";
            cfg.MtFuncTable = "IN_MEETING_FUNCTIONTIME";

            cfg.SvyTable = "IN_SURVEY";
            cfg.SvyOptTable = "IN_SURVEY_OPTION";
            cfg.SvyPrmTable = "IN_SURVEY_PARAM";
            if (cfg.mode == "cla")
            {
                cfg.MtTable = "IN_CLA_MEETING";
                cfg.MtSvyTable = "IN_CLA_MEETING_SURVEYS";
                cfg.MtFuncTable = "IN_CLA_MEETING_FUNCTIONTIME";
                cfg.SvyTable = "IN_CLA_SURVEY";
                cfg.SvyOptTable = "IN_CLA_SURVEY_OPTION";
                cfg.SvyPrmTable = "IN_CLA_SURVEY_PARAM";
            }

            string sql = "";

            string pro_list = "AND t2.in_property IN ('in_l1', 'in_l2', 'in_l3', 'in_l4')";
            if (cfg.mode == "cla")
            { 
                pro_list = "AND t2.in_property IN ('in_l1', 'in_l2', 'in_l3', 'in_cert_yn', 'in_cert_id')";
            }

            sql = @"
                SELECT
                    t1.*
                    , t2.in_property
                FROM
                    {#table1} t1 WITH(NOLOCK)
                INNER JOIN
                    {#table2} t2 WITH(NOLOCK)
                    ON t2.id = t1.related_id
                WHERE
                    t1.source_id = '{#meeting_id}'
                    AND t1.in_surveytype = '1'
                    {#pro_list}
                ORDER BY
                    t2.in_property
            ";

            sql = sql.Replace("{#table1}", cfg.MtSvyTable)
                .Replace("{#table2}", cfg.SvyTable)
                .Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#pro_list}", pro_list);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item itmMeetingSurveys = inn.applySQL(sql);

            if (itmMeetingSurveys.isError() || itmMeetingSurveys.getResult() == "")
            {
                return itmR;
            }

            int count = itmMeetingSurveys.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item itmMeetingSurvey = itmMeetingSurveys.getItemByIndex(i);
                string meeting_survey_id = itmMeetingSurvey.getProperty("id", "");
                string old_survey_id = itmMeetingSurvey.getProperty("related_id", "");

                Item itmSurvey = inn.newItem(cfg.SvyTable, "get");
                itmSurvey.setProperty("id", old_survey_id);
                itmSurvey = itmSurvey.apply();

                Item itmData = inn.newItem();
                itmData.setProperty("meeting_id", cfg.meeting_id);
                itmData.setProperty("meeting_survey_id", meeting_survey_id);
                itmData.setProperty("old_survey_id", old_survey_id);

                CloneSurvey(cfg, itmSurvey, itmData);
            }

            if (cfg.mode == "cla")
            {
                RefreshRelations(cfg);
            }

            return itmR;
        }

        private void RefreshRelations(TConfig cfg)
        {
            string sql = "";

            sql = @"
                UPDATE t2 SET
                    t2.in_selectoption = 'Copy of ' + t2.in_selectoption
                FROM
	                {#table1} t1
                INNER JOIN
	                {#table2} t2
	                ON t2.id = t1.related_id
                WHERE
	                t1.source_id = '{#meeting_id}'
	                AND t2.in_property = 'in_l1'
            ";

            sql = sql.Replace("{#table1}", cfg.MtSvyTable)
                .Replace("{#table2}", cfg.SvyTable)
                .Replace("{#meeting_id}", cfg.meeting_id);

            cfg.inn.applySQL(sql);


            sql = @"
                UPDATE 
	                {#table} 
                SET 
	                in_purpose = REPLACE(in_purpose, 'Copy of', '') 
                WHERE 
	                source_id = '{#meeting_id}'
            ";

            sql = sql.Replace("{#table}", cfg.MtFuncTable)
                .Replace("{#meeting_id}", cfg.meeting_id);

            cfg.inn.applySQL(sql);



            sql = @"
                UPDATE 
	                {#table} 
                SET 
	                in_purpose = N'第一天簽到'
                WHERE 
	                source_id = '{#meeting_id}'
	                AND in_purpose = N'簽#1'
            ";

            sql = sql.Replace("{#table}", cfg.MtFuncTable)
                .Replace("{#meeting_id}", cfg.meeting_id);

            cfg.inn.applySQL(sql);
        }


        //複製後修改問項
        private void CloneSurvey(TConfig cfg, Item itmSurvey, Item itmData)
        {
            string meeting_survey_id = itmData.getProperty("meeting_survey_id", "");
            string old_survey_id = itmData.getProperty("old_survey_id", "");

            string in_property = itmSurvey.getProperty("in_property", "");
            string parent_property = "";
            if (in_property == "in_l2")
            {
                parent_property = "in_l1";
            }
            else if (in_property == "in_l3")
            {
                parent_property = "in_l2";
            }


            itmSurvey.setType(cfg.SvyTable);

            //複製問項
            Item itmClone = itmSurvey.clone(true);
            itmClone.apply();

            if (itmClone.isError())
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "問項複製 失敗 _# meeting id: " + cfg.meeting_id + ", table: " + cfg.MtTable);
                return;
            }

            string new_survey_id = itmClone.getID();

            //複製細項
            Item itmOptions = cfg.inn.newItem(cfg.SvyOptTable, "get");
            itmOptions.setProperty("source_id", old_survey_id);
            itmOptions = itmOptions.apply();

            if (!IsError(itmOptions, isSingle: false))
            {
                int count = itmOptions.getItemCount();
                for (int i = 0; i < count; i++)
                {
                    Item itmOption = itmOptions.getItemByIndex(i);
                    Item itmOptionClone = itmOption.clone(true);
                    itmOptionClone.setProperty("source_id", new_survey_id);
                    itmOptionClone.apply();
                }
            }

            //將[原本的問項]替換成[複製出來的問項]

            string sql = @"
                UPDATE 
                    {#table}
                SET 
                    related_id = '{#new_survey_id}'
                WHERE 
                    id = '{#meeting_survey_id}'
            ";

            sql = sql.Replace("{#table}", cfg.MtSvyTable)
                .Replace("{#meeting_survey_id}", meeting_survey_id)
                .Replace("{#new_survey_id}", new_survey_id);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item itmUpdate = cfg.inn.applySQL(sql);

            if (itmUpdate.isError())
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "問項複製 失敗 _# new survey id: " + new_survey_id + ", table: " + cfg.SvyTable);
            }

            if (parent_property == "")
            {
                return;
            }

            string parent_item_number = GetParentItemNumber(cfg, parent_property);

            if (parent_item_number == "")
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "創建參數 失敗 _# meeting id: " + cfg.meeting_id + ", table: " + cfg.MtTable);
            }
            else
            {
                //創建參數 pattern
                Item itmParam = cfg.inn.newItem(cfg.SvyPrmTable, "add");
                itmParam.setProperty("source_id", new_survey_id);
                itmParam.setProperty("in_key", "pattern");
                itmParam.setProperty("in_value", parent_item_number);
                itmParam = itmParam.apply();

                sql = "UPDATE " + cfg.SvyTable + " SET in_filter_by = '" + parent_item_number + "' WHERE id = '" + new_survey_id + "'";
                itmUpdate = cfg.inn.applySQL(sql);

                if (itmUpdate.isError())
                {
                    cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "過濾父階 更新失敗 _# new survey id: " + new_survey_id + ", table: " + cfg.SvyTable);
                }

            }
        }

        private string GetParentItemNumber(TConfig cfg, string in_property)
        {
            string sql = @"
                SELECT
                    t2.item_number
                FROM
                    {#table1} t1 WITH(NOLOCK)
                INNER JOIN
                    {#table2} t2 WITH(NOLOCK)
                    ON t2.id = t1.related_id
                WHERE
                    t1.source_id = '{#meeting_id}'
                    AND t1.in_surveytype = '1'
                    AND t2.in_property = '{#in_property}'
            ";

            sql = sql.Replace("{#table1}", cfg.MtSvyTable)
                .Replace("{#table2}", cfg.SvyTable)
                .Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_property}", in_property);

            Item itmSQL = cfg.inn.applySQL(sql);

            if (!itmSQL.isError() && itmSQL.getResult() != "")
            {
                return itmSQL.getResult();
            }
            else
            {
                return "";
            }
        }

        //是否錯誤
        private bool IsError(Item item, bool isSingle = true)
        {
            if (item.isError() || item.getResult() == "")
            {
                return true;
            }
            else if (isSingle && item.getItemCount() != 1)
            {
                return true;
            }
            else if (item.getItemCount() <= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }

            public string meeting_id { get; set; }
            public string item_number { get; set; }
            public string hideselector { get; set; }
            public string in_clone { get; set; }
            public string mode { get; set; }

            public string MtTable { get; set; }
            public string MtSvyTable { get; set; }
            public string MtFuncTable { get; set; }

            public string SvyTable { get; set; }
            public string SvyOptTable { get; set; }
            public string SvyPrmTable { get; set; }
        }
    }
}