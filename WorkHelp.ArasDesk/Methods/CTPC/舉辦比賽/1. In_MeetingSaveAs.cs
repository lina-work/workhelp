﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.CTPC.Common
{
    public class In_MeetingSaveAs : Item
    {
        public In_MeetingSaveAs(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            //System.Diagnostics.Debugger.Break();
            /*
            SaveType:
            另存為1
            建立子場為2

            1.取得含Relationship的Meeting物件並clone出另一個meeting物件(CloneMeetings)
            2.根據RelClrList將CloneMeetings中的Relationship清除
            3.根據PropertyClrList將CloneMeetings的表頭property做更新

            2018-12-13 從81的prod_leeway搬回來，建立子場的部分還沒有測過


            */


            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
            string strIdentityid = _InnH.GetIdentityByUserId(inn.getUserID()).getProperty("id", "");
            string Meetingid = this.getProperty("meetingid", "");
            string SaveType = this.getProperty("savetype", "");



            DateTime CurrentTime = System.DateTime.Today;


            string[] PropertyClrList = { "in_contract", "in_mail", "in_tel", "in_is_template", "in_close_msg", "in_real_taking", "in_real_prepare", "in_isfull", "in_title", "owned_by_id", "in_refmeeting", "in_state_start", "in_state_class", "in_state_end", "in_state_issued", "in_state_review", "in_date_s", "in_date_e", "in_main", "in_contract_no", "in_meeting_state" };

            Item Meeting = inn.newItem("In_Meeting", "get");
            Meeting.setAttribute("id", Meetingid);
            Meeting = Meeting.apply();
            Item CloneMeetings = Meeting.clone(false);

            Item itmUser = inn.getItemById("User", inn.getUserID());
            string strUser_name = itmUser.getProperty("last_name", "");
            string strUser_email = itmUser.getProperty("email", "");
            string strUser_cell = itmUser.getProperty("cell", "");

            CloneMeetings.setProperty("in_is_main", "0");
            CloneMeetings.setProperty("in_date_s", CurrentTime.ToString("yyyy-MM-ddT01:00:00"));
            CloneMeetings.setProperty("in_date_e", CurrentTime.ToString("yyyy-MM-ddT23:00:00"));
            CloneMeetings.setProperty("in_contract", strUser_name);
            CloneMeetings.setProperty("in_mail", strUser_email);
            CloneMeetings.setProperty("in_real_prepare", "0");
            CloneMeetings.setProperty("in_isfull", "0");
            CloneMeetings.setProperty("in_real_taking", "0");
            CloneMeetings.setProperty("in_close_msg", "開放報名中");
            CloneMeetings.setProperty("in_tel", strUser_cell);
            string MyTitle = Meeting.getProperty("in_title", "");//因為clone出的Item中in_title沒有被複製到所以直接取Meeting物件的in_title
            CloneMeetings.setProperty("in_title", "");
            CloneMeetings.setProperty("owned_by_id", strIdentityid);
            if (SaveType == "1")
                CloneMeetings.setProperty("in_refmeeting", "");//代表是做另存
            else
                CloneMeetings.setProperty("in_refmeeting", Meeting.getAttribute("id", ""));//代表是做建立子場

            CloneMeetings.setProperty("in_is_template", "0");
            CloneMeetings.removeProperty("in_state_class", "");
            CloneMeetings.removeProperty("in_state_issued", "");
            CloneMeetings.removeProperty("in_state_end", "");
            CloneMeetings.removeProperty("in_state_review", "");
            CloneMeetings.setProperty("in_contract_no", "");
            CloneMeetings.setProperty("in_main", "0");

            //取得來自外部的property
            string strTmpValue;//=this.getProperty("in_contract_no","");
            if ((strTmpValue = this.getProperty("in_contract_no", "")) != "") { CloneMeetings.setProperty("in_contract_no", strTmpValue); }
            if ((strTmpValue = this.getProperty("item_number", "")) != "") { CloneMeetings.setProperty("item_number", strTmpValue); }
            if ((strTmpValue = this.getProperty("in_title", "")) != "") { CloneMeetings.setProperty("in_title", strTmpValue); }
            if ((strTmpValue = this.getProperty("in_meeting_state", "")) != "") { CloneMeetings.setProperty("in_meeting_state", strTmpValue); }
            if ((strTmpValue = this.getProperty("in_date_s", "")) != "") { CloneMeetings.setProperty("in_date_s", strTmpValue); }
            if ((strTmpValue = this.getProperty("in_date_e", "")) != "") { CloneMeetings.setProperty("in_date_e", strTmpValue); }
            if ((strTmpValue = this.getProperty("in_customer_contact", "")) != "") { CloneMeetings.setProperty("in_customer_contact", strTmpValue); }
            if ((strTmpValue = this.getProperty("in_customer_add", "")) != "") { CloneMeetings.setProperty("in_customer_add", strTmpValue); }
            if ((strTmpValue = this.getProperty("in_customer_tel", "")) != "") { CloneMeetings.setProperty("in_customer_tel", strTmpValue); }
            if ((strTmpValue = this.getProperty("in_note", "")) != "") { CloneMeetings.setProperty("in_note", strTmpValue); }
            if ((strTmpValue = this.getProperty("in_datn", "")) != "") { CloneMeetings.setProperty("in_datn", strTmpValue); }
            if ((strTmpValue = this.getProperty("in_tell", "")) != "") { CloneMeetings.setProperty("in_tell", strTmpValue); }
            if ((strTmpValue = this.getProperty("in_styn", "")) != "") { CloneMeetings.setProperty("in_styn", strTmpValue); }
            if ((strTmpValue = this.getProperty("in_all_update", "")) != "") { CloneMeetings.setProperty("in_all_update", strTmpValue); }
            if ((strTmpValue = this.getProperty("in_subc", "")) != "") { CloneMeetings.setProperty("in_subc", strTmpValue); }
            if ((strTmpValue = this.getProperty("in_subc_label", "")) != "") { CloneMeetings.setProperty("in_subc_label", strTmpValue); }
            if ((strTmpValue = this.getProperty("in_tnam", "")) != "") { CloneMeetings.setProperty("in_tnam", strTmpValue); }
            if ((strTmpValue = this.getProperty("in_ovdt", "")) != "") { CloneMeetings.setProperty("in_ovdt", strTmpValue); }
            if ((strTmpValue = this.getProperty("in_sgdt", "")) != "") { CloneMeetings.setProperty("in_sgdt", strTmpValue); }
            if ((strTmpValue = this.getProperty("in_seldata", "")) != "") { CloneMeetings.setProperty("in_seldata", strTmpValue); }
            if ((strTmpValue = this.getProperty("in_seldata_label", "")) != "") { CloneMeetings.setProperty("in_seldata_label", strTmpValue); }
            if ((strTmpValue = this.getProperty("in_rmbn", "")) != "") { CloneMeetings.setProperty("in_rmbn", strTmpValue); }
            if ((strTmpValue = this.getProperty("in_tel1", "")) != "") { CloneMeetings.setProperty("in_tel1", strTmpValue); }
            if ((strTmpValue = this.getProperty("in_seldata_label", "")) != "") { CloneMeetings.setProperty("in_seldata_label", strTmpValue); }
            if ((strTmpValue = this.getProperty("in_customer", "")) != "") { CloneMeetings.setProperty("in_customer", strTmpValue); }


            //建立完會議後把此會議的建立時間寫到進行日
            CloneMeetings.setProperty("in_state_start", CurrentTime.ToString("yyyy-MM-ddTHH:mm:ss"));


            //建立完會議後在共同講師建立一筆單場負責人的資料
            Item LoginUserResume = inn.newItem("In_Resume", "get");
            LoginUserResume.setProperty("owned_by_id", strIdentityid);

            Item itmResumeList = inn.newItem("In_Meeting_Resumelist", "add");
            itmResumeList.setProperty("in_speaker", "1");
            itmResumeList.setRelatedItem(LoginUserResume);

            CloneMeetings.addRelationship(itmResumeList);

            //System.Diagnostics.Debugger.Break();
            CloneMeetings.setProperty("in_temp_meeting", Meetingid);
            CloneMeetings = CloneMeetings.apply();

            //lina 2020.05.07: start
            string aml = ""
                + "<meeting_id>" + CloneMeetings.getProperty("id", "") + "</meeting_id>"
                + "<item_number>" + CloneMeetings.getProperty("item_number", "") + "</item_number>"
                + "<hideselector>" + "1" + "</hideselector>"
                + "<in_clone>" + "1" + "</in_clone>"
            + "";
            Item itmQrList = inn.applyMethod("In_Get_Meeting_QRList", aml);
            //lina 2020.05.07: end

            //lina 2020.09.07: start
            Item itmSurveys = inn.applyMethod("In_MeetingSaveAs_Survey", aml);
            //lina 2020.09.07: end


            return CloneMeetings;



        }
    }
}