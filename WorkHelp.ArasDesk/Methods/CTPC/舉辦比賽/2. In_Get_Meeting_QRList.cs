﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;
using System.Net;

namespace WorkHelp.ArasDesk.Methods.CTPC.Common
{
    public class In_Get_Meeting_QRList : Item
    {
        public In_Get_Meeting_QRList(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            //寫死只有付費課程(in_is_advanced)才可以看到學員測驗
            //System.Diagnostics.Debugger.Break();
            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            //lina 2020.05.07: start
            string strMethodName = "In_Get_Meeting_QRList";

            string meetingid = this.getProperty("meeting_id", "");
            string item_number = this.getProperty("item_number", "");
            string in_clone = this.getProperty("in_clone", "");

            if (in_clone == "1" && (item_number == null || item_number == ""))
            {
                string sql = "SELECT item_number FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + meetingid + "'";
                Item itmSQL = inn.applySQL(sql);
                if (itmSQL.isError())
                {
                    CCO.Utilities.WriteDebug(strMethodName, "sql error: " + sql);
                }
                else
                {
                    this.setProperty("item_number", itmSQL.getProperty("item_number", ""));
                }
            }
            //CCO.Utilities.WriteDebug(strMethodName, this.dom.InnerXml);
            //lina 2020.05.07: end.

            Item Inn_Result, itmMeetings;
            string Refresh = this.getProperty("refresh", "0");


            string strUserName = inn.getItemById("User", inn.getUserID()).getProperty("keyed_name");
            string CurrentIdentityList = Aras.Server.Security.Permissions.Current.IdentitiesList;
            string CurrentUserAlias = inn.getUserAliases();
            string strMailTo = "mailto:?subject={#subject}&body={#body}";
            string strMailContent = "您好,以下是　{#date}-{#title}　課程的報名連結:{#url}";
            string aml = @"
		<AML> 
		<Item type='In_Meeting'  action='get' select='id,in_title'>
		<or>
		<owned_by_id>{#CurrentUserAlias}</owned_by_id>
		<managed_by_id>{#CurrentUserAlias}</managed_by_id>
		<in_teacher>{#CurrentUserAlias}</in_teacher>
		</or>
		<in_date_s condition='lt'>{#today}</in_date_s>
		<in_date_e condition='gt'>{#today}</in_date_e >
		<in_is_main condition='ne'>1</in_is_main>
		<state condition='in'>'start','Class','Review'</state>
		</Item>
		</AML>";

            string single_qr = "";

            if (this.getProperty("s", "") != "UserReg")
            {
                aml = aml.Replace("{#CurrentUserAlias}", CurrentUserAlias);
                aml = aml.Replace("{#today}", System.DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss"));
                itmMeetings = inn.applyAML(aml);
            }
            else
            {
                itmMeetings = inn.applyAML(@"
    <AML>
        <Item type='In_Meeting' action='get'>
            <item_number>{#item_number}</item_number>
        </Item>
    </AML>".Replace("{#item_number}", this.getProperty("item_number", "")));
            }


            if (itmMeetings.getItemCount() == 0 && meetingid == "") //查無資料時,建立一個空的Item並把in_title塞進去
            {
                Item itmfake = inn.newItem("In_Meeting");
                itmfake.setProperty("in_title", "查無資料");
                itmfake.addRelationship(itmfake);
                return itmfake;
            }


            if (meetingid == "")
            {
                meetingid = itmMeetings.getItemByIndex(0).getProperty("id", "");//第一筆會議的id
            }

            Item itmR = inn.newItem("In_Meeting");
            //itmR.setAttribute("select","in_title,in_url");
            itmR.setID(meetingid);
            itmR = itmR.apply("get");

            //塞入報名表問卷及啟動流程
            Item itmAddSurvey = inn.newItem("Method", "In_Create_MeetingSurFromTmplate");
            itmAddSurvey.setProperty("meeting_id", meetingid);
            itmAddSurvey.setProperty("surveytype", "1");
            Item itmaddRegisSurveyResult = itmAddSurvey.apply();

            //加入滿意度問卷
            itmAddSurvey.setProperty("surveytype", "4");
            Item itmAddSatSurveyRst = itmAddSurvey.apply();
            //加入筆試考卷
            itmAddSurvey.setProperty("surveytype", "5");
            Item itmAddWrittenSurveyRst = itmAddSurvey.apply();
            //加入技術考
            itmAddSurvey.setProperty("surveytype", "3");
            Item itmAddTechSurveyRst = itmAddSurvey.apply();


            if (itmR.getProperty("in_wfp_state", "") == "")
            {
                Item itmAddToWorkflow = itmR.apply("In_AddtoWorkflow_S");

            }


            bool IsSetMeetingIdInResult = false; //網址上指定的ID是否存在於查詢結果內
            for (int i = 0; i < itmMeetings.getItemCount(); i++)
            {
                Item itmMeeting = itmMeetings.getItemByIndex(i);
                if (itmMeeting.getID() == meetingid)
                {
                    IsSetMeetingIdInResult = true;
                    itmMeeting.setProperty("selected", "selected");

                }
                itmR.addRelationship(itmMeetings.getItemByIndex(i));
            }

            if (!IsSetMeetingIdInResult)
            {
                //代表網址上指定的ID沒有出現在查詢結果內
                aml = "<AML><Item type='In_Meeting' action='get' id='" + meetingid + "' select='id,in_title'></Item></AML>";
                Item itmMeeting = inn.applyAML(aml);
                if (itmMeeting.isError())
                {
                    Item itmfake = inn.newItem("In_Meeting");
                    itmfake.setProperty("in_title", "查無資料");
                    itmfake.addRelationship(itmfake);
                    return itmfake;
                }
                else
                {
                    itmMeeting.setProperty("selected", "selected");
                    itmR.addRelationship(itmMeeting);
                }


            }

            string app_url = _InnH.GetInVariable("app_url").getProperty("in_value", "In_Variable.app_url");
            Item QRList = null;
            if (this.getProperty("s", "") != "UserReg")
            {
                //學員測驗
                string UserLogin = app_url + "/pages/b.aspx?page=MeetingUserLogin.html&method=In_Get_MeetingUserLogin&meeting_id=" + meetingid;
                string UserLogin_md5 = System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile(UserLogin, "md5");
                string UserLogin_url = "";
                if (Refresh == "1")
                {
                    CCO.Application.Remove(UserLogin_md5);
                }
                if (CCO.Application[UserLogin_md5] == null)
                {
                    UserLogin_url = gettinyurl(UserLogin);
                    CCO.Application.Add(UserLogin_md5, UserLogin_url);
                }
                else
                {
                    UserLogin_url = CCO.Application[UserLogin_md5].ToString();
                }

                QRList = inn.newItem("QRList");
                QRList.setProperty("title", "學員測驗");
                QRList.setProperty("url", UserLogin_url);
                //寫死只有付費課程(in_is_advanced)才可以看到學員測驗

                single_qr = "qr.aspx?t=學員測驗&q1=" + UserLogin_url;
                QRList.setProperty("single_qr", single_qr);
                QRList.setProperty("show_single_qr", "single_qr");
                if (itmR.getProperty("in_is_advanced", "0") == "1")
                    itmR.addRelationship(QRList);

            }

            //學員報名
            /*
            string UserReg="";
            string OldMeetings = "D52073C17E11470799E21CE147F258E7,A6B8038A14DB4311A8CEB1D1BE25F40B,9C473A25FCFE4E028DCA45449DC2538D,9445DB6A9F9A4E4BB85DB5666D67C157,69EC2A2846DF4640A35989BF0B5BB220,DF65D77348A244039437D2E735C283EF,05C8A12B318744C994723116E74AB5D6,B05E1C85407D4E17B586DC234E7F5CF3,C2B846447F6140069D31FCDCE0E5D4FC,78A34EDFB8224C4D806C1D05F144FC98,BB96946D30564A139939E57A9E3CC631,055C5B2904A043A2A7727951A81FBDD6,29A5E844109C4904A03FB1C8DD49BBF2,A0120EB314B64556BD493EB5F8754518,FA5406E6E3B24E27AFAEA64254010F18,2944DF37EE784C46AECD1258C558C0F6";
            if(OldMeetings.Contains(meetingid)) 
               UserReg = app_url + "pages/b.aspx?page=" + itmR.getProperty("in_register_url","") + "&method=In_get_meeting_register&agent_id=0E53E163F74843AEAABAF65DDC39DCC6&meeting_id=" + meetingid;
            else
                UserReg = app_url + "pages/b.aspx?page=" + itmR.getProperty("in_register_url","") + "&method=In_get_meeting_register&meeting_id=" + meetingid;

            string UserReg_md5 = System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile(UserReg,"md5");
            string UserReg_url = "";
            if(Refresh=="1")
            {
                CCO.Application.Remove(UserReg_md5);
            }
            if(CCO.Application[UserReg_md5]==null)
            {
                UserReg_url = gettinyurl(UserReg);
                CCO.Application.Add(UserReg_md5,UserReg_url);
            }
            else
            {
                UserReg_url = CCO.Application[UserReg_md5].ToString();
            }

            QRList =inn.newItem("QRList");
            QRList.setProperty("title","學員報名");
            QRList.setProperty("url",UserReg_url);

            single_qr = "qr.aspx?t=學員報名&q1=" + UserReg_url;
            QRList.setProperty("single_qr",single_qr);
            QRList.setProperty("show_single_qr","single_qr");

            itmR.addRelationship(QRList);
            */
            //管理員登入
            string admin = app_url + "pages/c.aspx?page=In_MeetingSignin.html&method=In_Get_MeetingSignIn&actiontype=2";
            string admin_md5 = System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile(admin, "md5");
            string admin_url = "";
            if (Refresh == "1")
            {
                CCO.Application.Remove(admin_md5);
            }
            if (CCO.Application[admin_md5] == null)
            {
                admin_url = gettinyurl(admin);
                CCO.Application.Add(admin_md5, admin_url);
            }
            else
            {
                admin_url = CCO.Application[admin_md5].ToString();
            }
            QRList = inn.newItem("QRList");
            QRList.setProperty("title", "管理員登入");
            QRList.setProperty("url", admin_url);
            //itmR.addRelationship(QRList);






            Item itmPassengers = this.apply("In_Collect_PassengerParam");
            int intPassengerCount = itmPassengers.getItemCount();
            for (int b = 0; b < intPassengerCount; b++)
            {
                itmR.addRelationship(itmPassengers.getItemByIndex(b));
            }


            QRList = null;
            //報名說明
            string IntroInurl = itmR.getProperty("in_url", "");
            string IntroShow = app_url + "pages/b.aspx?page=" + IntroInurl + "&method=In_MeetingUserResponse&in_meetingid=" + meetingid;
            string IntroShow_md5 = System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile(IntroShow, "md5");
            string IntroShow_url = "";

            if (Refresh == "1")
            {
                CCO.Application.Remove(IntroShow_md5);
            }
            if (CCO.Application[IntroShow_md5] == null)
            {
                IntroShow_url = gettinyurl(IntroShow);
                CCO.Application.Add(IntroShow_md5, IntroShow_url);
            }
            else
            {
                IntroShow_url = CCO.Application[IntroShow_md5].ToString();
            }

            QRList = inn.newItem("QRList");
            QRList.setProperty("title", "報名說明");
            QRList.setProperty("url", IntroShow_url);
            strMailContent = strMailContent.Replace("{#date}", DateTime.Parse(itmR.getProperty("in_date_s", "")).ToString("yyyy-MM-dd　HH:mm"));
            strMailContent = strMailContent.Replace("{#title}", itmR.getProperty("in_name_lic", ""));
            strMailContent = strMailContent.Replace("{#url}", IntroShow_url);
            strMailContent = System.Web.HttpUtility.UrlEncode(strMailContent);
            strMailTo = strMailTo.Replace("{#subject}", "[" + itmR.getProperty("in_name_lic", "") + "-報名通知]");
            strMailTo = strMailTo.Replace("{#body}", strMailContent);
            //strMailTo = "line://msg/text/?hello world";
            QRList.setProperty("mailto", strMailTo);
            QRList.setProperty("show_mailto", "mailto");

            single_qr = "qr.aspx?t=報名說明&q1=" + IntroShow_url;
            QRList.setProperty("single_qr", single_qr);
            QRList.setProperty("show_single_qr", "single_qr");

            //選手檢錄
            //string IntroInurl = itmR.getProperty("in_url",""); 
            IntroInurl = "login_staff.aspx?logout=true";
            //IntroShow = app_url  + IntroInurl ;
            IntroShow = app_url + IntroInurl + "&method=In_MeetingSignin_check_info&data_mode=entrance&meeting_id=" + meetingid;
            IntroShow_md5 = System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile(IntroShow, "md5");
            IntroShow_url = "";

            if (Refresh == "1")
            {
                CCO.Application.Remove(IntroShow_md5);
            }
            if (CCO.Application[IntroShow_md5] == null)
            {
                IntroShow_url = gettinyurl(IntroShow);
                CCO.Application.Add(IntroShow_md5, IntroShow_url);
            }
            else
            {
                IntroShow_url = CCO.Application[IntroShow_md5].ToString();
            }

            QRList.setProperty("title_2", "選手檢錄");
            QRList.setProperty("url_2", IntroShow_url);

            itmR.addRelationship(QRList);


            //報名結果 
            string IntroShow2 = app_url + "pages/b.aspx?page=RegistryList.html&method=in_registry_list&meeting_id=" + meetingid;
            string IntroShow_md5_2 = System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile(IntroShow2, "md5");
            string IntroShow_url_2 = "";

            if (CCO.Application[IntroShow_md5_2] == null)
            {
                IntroShow_url_2 = gettinyurl(IntroShow2);
                CCO.Application.Add(IntroShow_md5_2, IntroShow_url_2);
            }
            else
            {
                IntroShow_url_2 = CCO.Application[IntroShow_md5_2].ToString();
            }

            QRList = inn.newItem("QRList");
            QRList.setProperty("title", "報名結果");
            QRList.setProperty("url", IntroShow_url_2);
            itmR.addRelationship(QRList);



            QRList = null;
            //登入型的報名說明
            string Regurl = itmR.getProperty("in_url", "");
            string RegShow = app_url + "pages/c.aspx?page=MeetingRegistryContinousC1.html&meeting_id=" + meetingid + "&method=In_get_meeting_register&agent_id=0E53E163F74843AEAABAF65DDC39DCC6&continous=true";
            string RegShow_md5 = System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile(RegShow, "md5");
            string RegShow_url = "";

            if (Refresh == "1")
            {
                CCO.Application.Remove(RegShow_md5);
            }
            if (CCO.Application[RegShow_md5] == null)
            {
                RegShow_url = gettinyurl(RegShow);
                CCO.Application.Add(RegShow_md5, RegShow_url);
            }
            else
            {
                RegShow_url = CCO.Application[RegShow_md5].ToString();
            }

            /*    
            QRList =inn.newItem("QRList");
            QRList.setProperty("title","批次報名");
            QRList.setProperty("url",RegShow_url);
            strMailContent = strMailContent.Replace("{#date}",DateTime.Parse(itmR.getProperty("in_date_s","")).ToString("yyyy-MM-dd　HH:mm"));
            strMailContent = strMailContent.Replace("{#title}",itmR.getProperty("in_name_lic",""));
            strMailContent = strMailContent.Replace("{#url}",RegShow_url);
            strMailContent = System.Web.HttpUtility.UrlEncode(strMailContent);
            strMailTo = strMailTo.Replace("{#subject}","[" + itmR.getProperty("in_name_lic","") + "-報名通知]");
            strMailTo = strMailTo.Replace("{#body}",strMailContent);
            //strMailTo = "line://msg/text/?hello world";
            QRList.setProperty("mailto",strMailTo);
            QRList.setProperty("show_mailto","mailto");

            single_qr = "qr.aspx?t=批次報名&q1=" + RegShow_url;
            QRList.setProperty("single_qr",single_qr);
            QRList.setProperty("show_single_qr","single_qr");



            itmR.addRelationship(QRList);

            */


            string hideselector = this.getProperty("hideselector", "");
            itmR.setProperty("hideselector", hideselector);
            return itmR;
        }
        private string gettinyurl(string url)
        {
            string tinyurl = "http://tinyurl.com/api-create.php?url=";
            string new_url = tinyurl + url;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(new_url);
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            StreamReader tReader = new StreamReader(response.GetResponseStream());
            new_url = tReader.ReadToEnd();
            return new_url;
        }
    }
}