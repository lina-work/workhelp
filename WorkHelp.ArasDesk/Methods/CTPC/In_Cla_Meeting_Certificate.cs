﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.CTPC.Common
{
    public class In_Cla_Meeting_Certificate : Item
    {
        public In_Cla_Meeting_Certificate(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
             * 目的: 審核證書
             * 日誌:
             *      2022-08-01: 調整匯出匯入格式檔欄位 (lina)
             */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]In_Cla_Meeting_Certificate";

            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            string aml = "";
            string sql = "";
            string strError = "";

            string itemid = this.getProperty("itemid", "");

            Item itmR = this;

            string meetingid = this.getProperty("meeting_id", "");
            string id = this.getProperty("id", "");
            string type = this.getProperty("type", "");

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                strMethodName = strMethodName,
                inn = inn,
                meeting_id = meetingid,
                type = type,
                id = id
            };

            if (type == "seminar")
            {
                aml = "<AML>" +
                            "<Item type='in_cla_meeting' action='get' id='" + cfg.meeting_id + "' select='in_title, in_url, in_register_url, in_need_receipt, in_meeting_type, in_echelon, in_annual, in_certificate_no, in_date_s, in_date_e, in_coursh_hours, in_address'>" +
                            "</Item></AML>";

                cfg.itmMeeting = inn.applyAML(aml);

                cfg.in_title = cfg.itmMeeting.getProperty("in_title", "");
                cfg.in_url = cfg.itmMeeting.getProperty("in_url", "");
                cfg.in_register_url = cfg.itmMeeting.getProperty("in_register_url", "");
                cfg.in_meeting_type = cfg.itmMeeting.getProperty("in_meeting_type", "");
                cfg.in_echelon = cfg.itmMeeting.getProperty("in_echelon", "");
                cfg.in_annual = cfg.itmMeeting.getProperty("in_annual", "");
                cfg.certificate_no = cfg.itmMeeting.getProperty("in_certificate_no", "");
                cfg.in_date_s = cfg.itmMeeting.getProperty("in_date_s", "");
                cfg.in_date_e = cfg.itmMeeting.getProperty("in_date_e", "");
                cfg.in_coursh_hours = cfg.itmMeeting.getProperty("in_coursh_hours", "");
                cfg.in_address = cfg.itmMeeting.getProperty("in_address", "");
                cfg.range = Convert.ToInt32(this.getProperty("range", "0"));

                exportCertificate(cfg, itmR);
            }
            else if (type == "xls")
            {
                cfg.itmMeeting = inn.applySQL("SELECT id, in_title, in_seminar_type FROM IN_CLA_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
                cfg.itmUsersView = GetUsers(cfg);
                Export(cfg, itmR);
            }
            else
            {
                itmR.setType("In_Cla_Meeting");

                sql = "SELECT * FROM  In_Cla_Meeting WITH(NOLOCK) WHERE id = '{#meeting_id}'";
                sql = sql.Replace("{#meeting_id}", itemid);
                itmR = inn.applySQL(sql);

                sql = @"
                    SELECT 
	                    t1.id
	                    , t1.source_id
	                    , t1.in_score_o_5 AS 'know_score'
	                    , t1.in_score_o_3 AS 'tech_score'
	                    , t1.in_stat_0
	                    , t1.in_valid
	                    , t1.in_valid_type
	                    , t1.in_certificate_no
	                    , CONVERT(VARCHAR, DATEADD(HOUR, 8, t1.in_certificate_date), 111) AS 'in_certificate_date'
	                    , t1.in_moe_no
	                    , t1.in_year_note
	                    , t1.in_note
	                    , t2.in_name_num
	                    , t2.in_name
	                    , t2.in_sno
	                    , t2.in_gender
	                    , CONVERT(VARCHAR, DATEADD(HOUR, 8, t2.in_birth), 111) AS 'in_birth'
	                    , t11.id AS 'rid'
	                    , t11.in_instructor_id
	                    , t11.in_instructor_level
	                    , t11.in_gl_instructor_id
	                    , t11.in_gl_instructor_level
	                    , t11.in_referee_id
	                    , t11.in_referee_level
	                    , t11.in_gl_referee_id
	                    , t11.in_gl_referee_level
                    FROM 
	                    IN_CLA_MEETING_RESUME t1　WITH(NOLOCK)
                    INNER JOIN 
	                    IN_CLA_MEETING_USER t2 WITH(NOLOCK) 
                        ON t2.id = t1.in_user
                    LEFT OUTER JOIN 
	                    IN_CLA_MEETING_TECHNICAL t3 WITH(NOLOCK)　
	                    ON t3.in_user = t1.in_user
                    INNER JOIN
	                    IN_RESUME t11 WITH(NOLOCK)
	                    ON t11.in_sno = t2.in_sno
                     WHERE 
	                    t1.source_id = '1604E6FF75224701A489B32ADF28B285'
                    ORDER BY 
	                    ISNULL(T2.in_name_num, -1)

                ";

                sql = sql.Replace("{#meeting_id}", itemid);
                Item itmTecs = inn.applySQL(sql);

                int count = itmTecs.getItemCount();

                //取得審和類型 Item
                Item itmIn_valid_types = GetIn_valid_types(cfg.CCO, cfg.strMethodName, cfg.inn);
                for (int i = 0; i < count; i++)
                {
                    Item itmTec = itmTecs.getItemByIndex(i);
                    itmTec.setType("In_Cla_Meeting_Resume");

                    itmTec.setProperty("in_certificate_date", itmTec.getProperty("in_certificate_date", "").Replace("/", "-"));

                    //取得審和類型 SELEC HTML
                    itmTec.setProperty("inn_valid_typeHtml", GetInn_valid_typeHtml(cfg.CCO, cfg.strMethodName, itmTec, itmIn_valid_types));

                    itmR.addRelationship(itmTec);
                }

            }

            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

            return itmR;

        }

        /// <summary>
        /// 取得 審核型態html
        /// </summary>
        /// <param name="itmRecord"></param>
        /// <param name="itmIn_valid_types"></param>
        /// <returns></returns>
        private string GetInn_valid_typeHtml(Aras.Server.Core.CallContext CCO, string strMethodName, Item itmRecord, Item itmIn_valid_types)
        {
            string rResult = "";
            string rIn_name_num = itmRecord.getProperty("in_name_num", "").ToString().Trim();
            string rIn_valid_type = itmRecord.getProperty("in_valid_type", "").ToString().Trim();
            string rID = itmRecord.getProperty("id", "").ToString().Trim();

            if (itmIn_valid_types.isError() || itmIn_valid_types.getItemCount() <= 0)
            {
                return rResult;
            }

            // CCO.Utilities.WriteDebug(strMethodName, itmIn_valid_types.dom.InnerXml);

            StringBuilder sb = new StringBuilder();
            // sb.AppendFormat("<select id='{0}_in_valid_type' class='form-control' >", rIn_name_num);
            sb.AppendFormat("<select class='in_inputdisplay form-control {0}_in_valid_type sel_cert' onclick='ih.handleTextEdit(this,40)' onchange='ih.updateTextContent(this)' value='{1}' id='{2}_in_valid_type'>", rIn_name_num, rIn_valid_type, rID);
            sb.AppendFormat("<option value=''></option>");
            int count = itmIn_valid_types.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = itmIn_valid_types.getItemByIndex(i);
                string inn_label = item.getProperty("label", "").ToString().Trim();
                string inn_value = item.getProperty("value", "").ToString().Trim();
                if (rIn_valid_type.Equals(inn_value))
                {
                    sb.AppendFormat("<option value='{1}' selected>{0}</option>", inn_label, inn_value);
                }
                else
                {
                    sb.AppendFormat("<option value='{1}'>{0}</option>", inn_label, inn_value);
                }
                // CCO.Utilities.WriteDebug(strMethodName, "rIn_name_num:" + rIn_name_num);
                // CCO.Utilities.WriteDebug(strMethodName, "rIn_valid_type:" + rIn_valid_type);
            }
            sb.AppendFormat("</selet>");
            rResult = sb.ToString();

            return rResult;
        }

        private void Export(TConfig cfg, Item itmReturn)
        {
            var memo = itmReturn.getProperty("memo", "");

            var workbook = new ClosedXML.Excel.XLWorkbook();

            var type_label = "匯入格式檔";

            if (memo == "score")
            {
                type_label = cfg.itmMeeting.getProperty("in_title", "")
                    + "_講習成績"
                    + "_匯入格式檔";

                AppendSheetScore(cfg, workbook, "current", itmReturn);
            }
            else if (memo == "cert")
            {
                var rsetting = MapResumeProps(cfg, cfg.itmMeeting);

                if (!rsetting.need_cert)
                {
                    throw new Exception("未設定講習類型(in_seminar_type)");
                }

                type_label = cfg.itmMeeting.getProperty("in_title", "")
                    + "_" + rsetting.cert_title
                    + "_匯入格式檔";

                AppendSheetCert(cfg, workbook, "current", rsetting, itmReturn);
            }
            else
            {
                AppendSheet(cfg, workbook, "current", itmReturn);
            }


            Item itmPath = GetExcelPath(cfg, "export_path");

            string export_path = itmPath.getProperty("export_path", "").TrimEnd('\\');
            string ext_name = ".xlsx";
            string xls_name = type_label + DateTime.Now.ToString("MMdd_HHmmss");
            string xls_file = export_path + "\\" + xls_name + ext_name;
            string xls_url = xls_name + ext_name;

            workbook.SaveAs(xls_file);
            itmReturn.setProperty("xls_name", xls_url);
        }

        //[審核證書]匯出匯入格式檔
        private void AppendSheet(TConfig cfg, ClosedXML.Excel.XLWorkbook workbook, string sheet_name, Item itmReturn)
        {
            var sheet = workbook.Worksheets.Add(sheet_name);

            List<TField> fields = new List<TField>();
            fields.Add(new TField { property = "in_name_num", title = "學員編號", format = "text" });
            fields.Add(new TField { property = "in_name", title = "人名", format = "center" });
            fields.Add(new TField { property = "in_valid", title = "是否結業(1符合/0不符合)", format = "text" });
            // fields.Add(new TField { property = "in_valid_type", title = "審核類型", format = "text" });
            // fields.Add(new TField { property = "in_certificate_no", title = "證書編號", format = "text" });
            fields.Add(new TField { property = "in_certificate_date", title = "發證日期(格式2021/01/01)", format = "yyyy/MM/dd" });
            // fields.Add(new TField { property = "in_moe_no", title = "體總編號", format = "text" });
            fields.Add(new TField { property = "in_year_note", title = "本年度講習紀錄", format = "text" });
            fields.Add(new TField { property = "in_note", title = "備註說明", format = "text" });
            fields.Add(new TField { property = "in_stat_0", title = "是否合格(O通過/X不通過)", format = "text" });

            int wsRow = 1;
            int wsCol = 1;
            int field_count = fields.Count;

            for (int i = 0; i < field_count; i++)
            {
                TField field = fields[i];
                ClosedXML.Excel.IXLCell cell = sheet.Cell(wsRow, wsCol + i);
                SetHeadCell(cell, field.title);
            }
            wsRow++;

            int count = cfg.itmUsersView.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = cfg.itmUsersView.getItemByIndex(i);
                item.setProperty("no", (i + 1).ToString());
                SetItemCell(sheet, wsRow, wsCol, item, fields);
                wsRow++;
            }

            //自動調整欄寬
            for (int i = 0; i < field_count; i++)
            {
                sheet.Column(wsCol + i).AdjustToContents();
            }
        }

        //[講習成績登錄]匯出匯入格式檔
        private void AppendSheetScore(TConfig cfg, ClosedXML.Excel.XLWorkbook workbook, string sheet_name, Item itmReturn)
        {
            var sheet = workbook.Worksheets.Add(sheet_name);

            List<TField> fields = new List<TField>();
            fields.Add(new TField { property = "in_name_num", title = "學員編號", format = "text" });
            fields.Add(new TField { property = "in_name", title = "姓名", format = "center" });
            //fields.Add(new TField { property = "in_birth", title = "出生年月日", format = "yyyy/MM/dd" });
            //fields.Add(new TField { property = "in_sno", title = "參與者登入帳號", format = "text" });

            fields.Add(new TField { property = "know_score", title = "學科_分數", format = "text" });
            fields.Add(new TField { property = "tech_score", title = "術科_分數", format = "text" });
            fields.Add(new TField { property = "in_year_note", title = "本年度講習紀錄", format = "text" });
            fields.Add(new TField { property = "in_note", title = "備註", format = "text" });

            //fields.Add(new TField { property = "in_valid", title = "是否結業(1符合/0不符合)", format = "text" });
            //fields.Add(new TField { property = "in_certificate_date", title = "發證日期(格式2021/01/01)", format = "yyyy/MM/dd" });
            //fields.Add(new TField { property = "in_year_note", title = "本年度講習紀錄", format = "text" });
            //fields.Add(new TField { property = "in_stat_0", title = "是否合格(O通過/X不通過)", format = "text" });

            int wsRow = 1;
            int wsCol = 1;
            int field_count = fields.Count;

            for (int i = 0; i < field_count; i++)
            {
                TField field = fields[i];
                ClosedXML.Excel.IXLCell cell = sheet.Cell(wsRow, wsCol + i);
                SetHeadCell(cell, field.title);
            }
            wsRow++;

            int count = cfg.itmUsersView.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = cfg.itmUsersView.getItemByIndex(i);
                item.setProperty("no", (i + 1).ToString());
                SetItemCell(sheet, wsRow, wsCol, item, fields);
                wsRow++;
            }

            //自動調整欄寬
            for (int i = 0; i < field_count; i++)
            {
                sheet.Column(wsCol + i).AdjustToContents();
            }
        }

        //[講習級證]匯出匯入格式檔
        private void AppendSheetCert(TConfig cfg, ClosedXML.Excel.XLWorkbook workbook, string sheet_name, TResumeProp rsetting, Item itmReturn)
        {
            var sheet = workbook.Worksheets.Add(sheet_name);

            List<TField> fields = new List<TField>();
            fields.Add(new TField { property = "in_name_num", title = "學員編號", format = "text" });
            fields.Add(new TField { property = "in_name", title = "姓名", format = "center" });
            fields.Add(new TField { property = "in_birth", title = "出生年月日", format = "yyyy/MM/dd" });
            fields.Add(new TField { property = "in_sno", title = "參與者登入帳號", format = "text" });

            fields.Add(new TField { property = rsetting.cert_lv_pro, title = rsetting.cert_title + "證照級別", format = "text" });
            fields.Add(new TField { property = rsetting.cert_id_pro, title = rsetting.cert_title + "證照號碼", format = "text" });
            fields.Add(new TField { property = "in_valid_type", title = "審核類型", format = "text" });
            fields.Add(new TField { property = "in_certificate_date", title = "發證日期(格式2021-01-01)", format = "yyyy-MM-dd" });
            //fields.Add(new TField { property = "in_year_note", title = "本年度講習紀錄", format = "text" });

            fields.Add(new TField { property = "in_note", title = "備註", format = "text" });

            //fields.Add(new TField { property = "in_valid", title = "是否結業(1符合/0不符合)", format = "text" });
            //fields.Add(new TField { property = "in_certificate_date", title = "發證日期(格式2021/01/01)", format = "yyyy/MM/dd" });
            //fields.Add(new TField { property = "in_year_note", title = "本年度講習紀錄", format = "text" });
            //fields.Add(new TField { property = "in_stat_0", title = "是否合格(O通過/X不通過)", format = "text" });

            int wsRow = 1;
            int wsCol = 1;
            int field_count = fields.Count;

            for (int i = 0; i < field_count; i++)
            {
                TField field = fields[i];
                ClosedXML.Excel.IXLCell cell = sheet.Cell(wsRow, wsCol + i);
                SetHeadCell(cell, field.title);
            }
            wsRow++;

            int count = cfg.itmUsersView.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = cfg.itmUsersView.getItemByIndex(i);
                item.setProperty("no", (i + 1).ToString());
                SetItemCell(sheet, wsRow, wsCol, item, fields);
                wsRow++;
            }

            //自動調整欄寬
            for (int i = 0; i < field_count; i++)
            {
                sheet.Column(wsCol + i).AdjustToContents();
            }
        }

        ClosedXML.Excel.XLColor head_bg_color = ClosedXML.Excel.XLColor.FromHtml("#D4D0C8");// #295C90
        ClosedXML.Excel.XLColor head_ft_color = ClosedXML.Excel.XLColor.FromHtml("#1E1E1E");// ClosedXML.Excel.XLColor.White

        //設定標題列
        private void SetHeadCell(ClosedXML.Excel.IXLCell cell, string title)
        {
            cell.Value = title;

            cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
            cell.Style.Font.Bold = true;
            cell.DataType = ClosedXML.Excel.XLDataType.Text;
            cell.Style.Fill.BackgroundColor = head_bg_color;
            cell.Style.Font.FontColor = head_ft_color;

            cell.Style.Border.SetRightBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetTopBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetBottomBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetLeftBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
        }

        //設定物件與資料列
        private void SetItemCell(ClosedXML.Excel.IXLWorksheet sheet, int wsRow, int wsCol, Item item, List<TField> fields)
        {
            int count = fields.Count;
            for (int i = 0; i < count; i++)
            {
                TField field = fields[i];
                if (field.format == null) field.format = "";

                ClosedXML.Excel.IXLCell cell = sheet.Cell(wsRow, wsCol + i);

                string value = "";

                if (field.property != "")
                {
                    value = item.getProperty(field.property, "");
                }

                SetBodyCell(cell, value, field.format);
            }
        }

        //設定資料列
        private void SetBodyCell(ClosedXML.Excel.IXLCell cell, string value, string format)
        {
            switch (format)
            {
                case "center":
                    cell.Value = value;
                    cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    break;

                case "date":
                    cell.Value = value;
                    cell.Style.NumberFormat.Format = "yyyy/mm/dd";
                    cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    break;

                case "yyyy/MM/dd HH:mm":
                    cell.Value = GetDateTimeValue(value, format, 8);
                    cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    break;

                case "yyyy/MM/dd":
                    cell.Value = GetDateTimeValue(value, format, 8);
                    cell.Style.NumberFormat.Format = "yyyy/mm/dd";
                    cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    break;

                case "$ #,##0":
                    cell.Value = value;
                    cell.Style.NumberFormat.Format = "$ #,##0";
                    break;

                case "tel":
                    if (value.StartsWith("09"))
                    {
                        cell.Value = value.Replace("-", "");
                        cell.Style.NumberFormat.Format = "0000-000-000";
                        cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    }
                    else
                    {
                        cell.Value = "'" + value;
                        cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    }
                    break;

                case "text":
                    cell.Value = "'" + value;
                    cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    break;

                default:
                    cell.Value = value;
                    break;
            }
            cell.Style.Border.SetRightBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetTopBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetBottomBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetLeftBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
        }

        private string GetDateTimeValue(string value, string format, int hours = 0)
        {
            if (value == "") return "";

            DateTime dt = DateTime.Now;
            if (DateTime.TryParse(value, out dt))
            {
                return dt.AddHours(hours).ToString(format);
            }
            else
            {
                return value;
            }
        }


        private Item GetExcelPath(TConfig cfg, string in_name)
        {
            Item itmResult = cfg.inn.newItem();

            //Excel 匯出路徑 (from 原創.設定參數)
            string sql = @"
    SELECT 
        TOP 1 t2.in_name AS 'variable', t1.in_name, t1.in_value 
    FROM 
        In_Variable_Detail t1 WITH(NOLOCK)
    INNER JOIN 
        In_Variable t2 WITH(NOLOCK) ON t2.ID = t1.SOURCE_ID 
    WHERE 
        t2.in_name = N'meeting_excel'  
        AND t1.in_name = N'{#in_name}'
    ";

            sql = sql.Replace("{#in_name}", in_name);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item item = cfg.inn.applySQL(sql);

            if (item.isError())
            {
                throw new Exception("未設定匯出路徑 _# " + in_name);
            }

            itmResult.setProperty("export_path", item.getProperty("in_value", ""));

            return itmResult;
        }

        //匯出結業證書
        private void exportCertificate(TConfig cfg, Item itmReturn)
        {
            string pdfName = "";

            pdfName = cfg.in_title + "_結業證書";


            //Word Template 所在路徑與匯出路徑 (from 原創.設定參數)
            string amlVariable = "<AML>" +
                "<Item type='In_Variable' action='get'>" +
                "<in_name>meeting_excel</in_name>" +
                "<Relationships>" +
                "<Item type='In_Variable_Detail' action='get'/>" +
                "</Relationships>" +
                "</Item></AML>";
            Item itmVairable = cfg.inn.applyAML(amlVariable);
            Item itmVairableDetail = itmVairable.getRelationships("In_Variable_Detail");
            int count = itmVairableDetail.getItemCount();
            string templatePath = "";
            string exportPath = "";
            for (int i = 0; i < count; i++)
            {
                Item itmDetail = itmVairableDetail.getItemByIndex(i);

                if (itmDetail.getProperty("in_name", "") == "certificate_path2")
                {
                    templatePath = itmDetail.getProperty("in_value", "");
                }

                if (itmDetail.getProperty("in_name", "") == "export_path")
                {
                    exportPath = itmDetail.getProperty("in_value", "");
                }

                if (!exportPath.EndsWith(@"\"))
                {
                    exportPath = exportPath + @"\";
                }
            }


            string pdfFile = exportPath + pdfName + ".pdf";

            Spire.Xls.Workbook book = new Spire.Xls.Workbook();
            book.LoadFromFile(templatePath);
            Spire.Xls.Worksheet sheetTemplate = book.Worksheets[0];


            Item itmUsers = GetUsers(cfg);

            if (itmUsers.isError())
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "匯出結業證書資料發生錯誤");
                throw new Exception("匯出結業證書發生錯誤");
            }

            if (itmUsers.getItemCount() < 1)
            {
                throw new Exception("目前暫無合格學員供下載!");
            }

            AppendCertificate(book, sheetTemplate, cfg, itmUsers);

            sheetTemplate.Remove();

            book.SaveToFile(pdfFile, Spire.Xls.FileFormat.PDF);

            itmReturn.setProperty("pdf_name", pdfName + ".pdf");
        }

        //取得測驗人員清單
        private Item GetUsers(TConfig cfg)
        {
            string sqlFilter = "";
            string rangeStr = "";

            if (cfg.type == "seminar" && cfg.id != "")
            {
                sqlFilter = " AND t1.id IN (" + cfg.id + ")";
            }
            else if (cfg.type == "xls")
            {
                sqlFilter = "AND ISNULL(t2.in_name_num,'') != '' ";
            }
            else
            {
                rangeStr = " WHERE rowno > {#page_s} and rowno <= {#page_e}";
            }

            string sql = @"
                SELECT * FROM 
                (
                    SELECT 
	                    t1.id
	                    , t1.source_id
	                    , t1.in_score_o_5 AS 'know_score'
	                    , t1.in_score_o_3 AS 'tech_score'
	                    , t1.in_stat_0
	                    , t1.in_valid
	                    , t1.in_valid_type
	                    , t1.in_certificate_no
	                    , CONVERT(VARCHAR, DATEADD(HOUR, 8, t1.in_certificate_date), 111) AS 'in_certificate_date'
	                    , t1.in_moe_no
	                    , t1.in_year_note
	                    , t1.in_note
	                    , t2.in_name_num
	                    , t2.in_name
	                    , t2.in_sno
	                    , t2.in_gender
	                    , CONVERT(VARCHAR, DATEADD(HOUR, 8, t2.in_birth), 111) AS 'in_birth'
	                    , t11.id AS 'rid'
	                    , t11.in_instructor_id
	                    , t11.in_instructor_level
	                    , t11.in_gl_instructor_id
	                    , t11.in_gl_instructor_level
	                    , t11.in_referee_id
	                    , t11.in_referee_level
	                    , t11.in_gl_referee_id
	                    , t11.in_gl_referee_level
                    FROM 
	                    IN_CLA_MEETING_RESUME t1　WITH(NOLOCK)
                    INNER JOIN 
	                    IN_CLA_MEETING_USER t2 WITH(NOLOCK) 
                        ON t2.id = t1.in_user
                    LEFT OUTER JOIN 
	                    IN_CLA_MEETING_TECHNICAL t3 WITH(NOLOCK)　
	                    ON t3.in_user = t1.in_user
                    INNER JOIN
	                    IN_RESUME t11 WITH(NOLOCK)
	                    ON t11.in_sno = t2.in_sno
                     WHERE 
	                    t1.source_id = '{#meeting_id}' {#sqlFilter}
                ) AS LIST
                " + rangeStr;

            sql += " ORDER BY t2.in_name_num";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                        .Replace("{#sqlFilter}", sqlFilter)
                        .Replace("{#page_s}", ((cfg.range - 1) * 80).ToString())
                        .Replace("{#page_e}", (cfg.range * 80).ToString());

            return cfg.inn.applySQL(sql);
        }

        private static void AppendCertificate(Spire.Xls.Workbook book, Spire.Xls.Worksheet sheetTemplate, TConfig cfg, Item itmUsers)
        {
            int user_count = itmUsers.getItemCount();
            DateTime currentTime = System.DateTime.Now;
            DateTime sTime = Convert.ToDateTime(cfg.in_date_s);
            DateTime eTime = Convert.ToDateTime(cfg.in_date_e);
            if (user_count != 0)
            {
                for (int i = 0; i < user_count; i++)
                {
                    Item itmUser = itmUsers.getItemByIndex(i);

                    string in_name = itmUser.getProperty("in_name", "");
                    if (in_name == "")
                    {
                        in_name = "　　　";
                    }

                    string chinese_day = convertCDay(eTime, "footer");

                    Spire.Xls.Worksheet sheet = book.CreateEmptySheet();
                    // string content = "{#in_name}　君參加本會{#in_annual}年度{#in_title}修業期滿特發此證以茲證明" ;  
                    string content = "{#in_name}\b君參加本會{#in_title}修業期滿特發此證以茲證明";
                    content = content.Replace("{#in_name}", in_name + "")
                                    .Replace("{#in_annual}", cfg.in_annual)
                                    .Replace("{#in_title}", cfg.in_title)
                                    .Replace("中華民國跆拳道協會", "")
                                    .Replace(" ", "\b");

                    sheet.CopyFrom(sheetTemplate);
                    sheet.Name = itmUser.getProperty("id", "");
                    sheet.Range["A1"].Text = "中跆爐字第" + cfg.certificate_no + "號";                  //文號
                    sheet.Range["B1"].Text = content;                                              //內文
                    sheet.Range["C1"].Text = "修業期間：" + convertCDay(sTime, "content") + "至" + convertCDay(eTime, "content") + "共計" + cfg.in_coursh_hours + "小時";   //修業時間
                    sheet.Range["D1"].Text = chinese_day;                                          //日期

                    sheet.PageSetup.FitToPagesWide = 1;
                    sheet.PageSetup.FitToPagesTall = 1;
                    sheet.PageSetup.TopMargin = 0.1;
                    sheet.PageSetup.LeftMargin = 0.1;
                    sheet.PageSetup.RightMargin = 0.1;
                    sheet.PageSetup.BottomMargin = 0.1;

                    //適合頁面
                    book.ConverterSetting.SheetFitToPage = true;
                }
            }

        }

        private static string convertCDay(DateTime CDay, string type)
        {
            string y = (CDay.Year - 1911).ToString();
            string m = CDay.Month.ToString("00");
            string d = CDay.Day.ToString("00");
            string returnStr = "";
            if (type == "footer")
            {
                returnStr = " 中華民國　" + y + "　年　" + m + "　月　" + d + "　日";
            }
            else
            {
                returnStr = y + "年" + m + "月" + d + "日";
            }

            return returnStr;
        }

        //下拉選單
        private void AppendMenu(Item items, string type_name, Item itmReturn)
        {
            if (items.isError() || items.getItemCount() <= 0)
            {
                return;
            }

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);

                item.setType(type_name);
                item.setProperty("inn_label", item.getProperty("label", ""));
                item.setProperty("inn_value", item.getProperty("value", ""));
                itmReturn.addRelationship(item);
            }
        }

        private Item GetIn_valid_types(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn)
        {
            string sql = "SELECT * FROM VU_Seminar_Valid";
            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);
            return inn.applySQL(sql);
        }

        private TResumeProp MapResumeProps(TConfig cfg, Item itmMeeting)
        {
            string in_seminar_type = itmMeeting.getProperty("in_seminar_type", "");

            var result = new TResumeProp
            {
                need_cert = false,
                cert_title = "",
                cert_id_pro = "",
                cert_lv_pro = "",
            };

            switch (in_seminar_type)
            {
                case "referee"://國內對打裁判
                case "referee_enr"://國內裁判增能
                    result.need_cert = true;
                    result.cert_title = "裁判證";
                    result.cert_id_pro = "in_referee_id";
                    result.cert_lv_pro = "in_referee_level";
                    break;
                case "referee_gl"://國際對打裁判
                case "referee_enr_gl"://國際裁判增能
                    result.need_cert = true;
                    result.cert_title = "國際裁判證";
                    result.cert_id_pro = "in_gl_referee_id";
                    result.cert_lv_pro = "in_gl_referee_level";
                    break;
                case "poomsae"://國內品勢裁判
                    result.need_cert = true;
                    result.cert_title = "品勢裁判證";
                    result.cert_id_pro = "in_poomsae_id";
                    result.cert_lv_pro = "in_poomsae_level";
                    break;
                case "poomsae_gl"://國際品勢裁判
                    result.need_cert = true;
                    result.cert_title = "國際品勢裁判證";
                    result.cert_id_pro = "in_gl_poomsae_id";
                    result.cert_lv_pro = "in_gl_poomsae_level";
                    break;
                case "coach"://國內教練
                case "coach_enr"://國內教練增能
                    result.need_cert = true;
                    result.cert_title = "教練證";
                    result.cert_id_pro = "in_instructor_id";
                    result.cert_lv_pro = "in_instructor_level";
                    break;
                case "coach_gl"://國際教練
                case "coach_enr_gl"://國際教練增能
                    result.need_cert = true;
                    result.cert_title = "國際教練證";
                    result.cert_id_pro = "in_gl_instructor_id";
                    result.cert_lv_pro = "in_gl_instructor_level";
                    break;
                case "degree_tw"://國內段
                    result.need_cert = true;
                    result.cert_title = "國內段";
                    result.cert_id_pro = "in_degree_id";
                    result.cert_lv_pro = "in_degree";
                    break;
                case "degree_gl"://國際段
                    result.need_cert = true;
                    result.cert_title = "國際段";
                    result.cert_id_pro = "in_gl_degree_id";
                    result.cert_lv_pro = "in_gl_degree";
                    break;
                default:
                    result.need_cert = false;
                    break;
            }

            return result;
        }

        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public string strMethodName { get; set; }
            public Innovator inn { get; set; }
            //外部輸入區

            /// <summary>
            /// 活動 id
            /// </summary>
            public string meeting_id { get; set; }

            // Meeting 區

            /// <summary>
            /// Meeting 資料
            /// </summary>
            public Item itmMeeting { get; set; }

            /// <summary>
            /// 活動名稱
            /// </summary>
            public string in_title { get; set; }

            /// <summary>
            /// 專屬展示網址
            /// </summary>
            public string in_url { get; set; }

            /// <summary>
            /// 專屬報名網址
            /// </summary>
            public string in_register_url { get; set; }

            /// <summary>
            /// 活動類型
            /// </summary>
            public string in_meeting_type { get; set; }

            /// <summary>
            /// 梯次
            /// </summary>
            public string in_echelon { get; set; }

            /// <summary>
            /// 年度
            /// </summary>
            public string in_annual { get; set; }

            /// <summary>
            /// 文號
            /// </summary>
            public string in_decree { get; set; }

            /// <summary>
            /// 證照號碼
            /// </summary>
            public string certificate_no { get; set; }

            /// <summary>
            /// 課程起
            /// </summary>
            public string in_date_s { get; set; }

            /// <summary>
            /// 課程迄
            /// </summary>
            public string in_date_e { get; set; }

            /// <summary>
            /// 修課時數
            /// </summary>
            public string in_coursh_hours { get; set; }

            /// <summary>
            /// 地址
            /// </summary>
            public string in_address { get; set; }
            public string type { get; set; }
            public string id { get; set; }

            public int range { get; set; }

            public Item itmUsersView { get; set; }

        }

        private class TField
        {
            public string property { get; set; }
            public string title { get; set; }
            public string css { get; set; }
            public string format { get; set; }
            public bool hide { get; set; }
            public int in_west_year { get; set; }
            public Func<TResume, TField, string> getValue { get; set; }

            public string GetStr(Item item)
            {
                return item.getProperty(this.property, "");
            }
        }

        private class TResume
        {
            public string id { get; set; }
            public string in_sno { get; set; }
            public string in_apply_year { get; set; }
            public int apply_year { get; set; }
            public string tw_apply_year { get; set; }
            public Item Value { get; set; }
        }

        private class TResumeProp
        {
            public bool need_cert { get; set; }
            public string cert_title { get; set; }
            public string cert_id_pro { get; set; }
            public string cert_lv_pro { get; set; }
        }
    }
}