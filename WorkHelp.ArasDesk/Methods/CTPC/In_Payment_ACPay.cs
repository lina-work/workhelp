﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.CTPC.Common
{
    public class In_Payment_ACPay : Item
    {
        public In_Payment_ACPay(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: ACPay 串接
                日誌: 
                    - 2022-05-24: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]" + "In_Payment_ACPay";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                strIdentityId = inn.getUserAliases(),

                item_number = itmR.getProperty("item_number", ""),
                scene = itmR.getProperty("scene", ""),
            };

            switch (cfg.scene)
            {
                case "callback":
                    CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);
                    break;

                default:
                    break;
            }

            

            return itmR;
        }

        private void Page(TConfig cfg, Item itmReturn)
        {
            string sql = "";

            sql = @"
                SELECT 
                    id
                    , item_number
                    , in_pay_amount_exp
                    , pay_bool
                    , in_code_2
                    , in_trade_no
                    , in_creator_sno
                FROM 
                    IN_MEETING_PAY WITH(NOLOCK)
                WHERE 
                    item_number = '{#item_number}'";

            sql = sql.Replace("{#item_number}", cfg.item_number);

            Item itmPay = cfg.inn.applySQL(sql);
            if (itmPay.isError() || itmPay.getResult() == "")
            {
                itmReturn.setProperty("item_number", cfg.item_number + " 查無資料");
                itmReturn.setProperty("is_error", "1");
                return;
            }

            string id = itmPay.getProperty("id", "");
            string pay_bool = itmPay.getProperty("pay_bool", "");
            string in_code_2 = itmPay.getProperty("in_code_2", "");
            string in_trade_no = itmPay.getProperty("in_trade_no", "");
            string in_pay_amount_exp = itmPay.getProperty("in_pay_amount_exp", "0");
            string in_creator_sno = itmPay.getProperty("in_creator_sno", "");

            if (pay_bool != "未繳費")
            {
                itmReturn.setProperty("item_number", cfg.item_number + " 無法繳費，訂單狀態: " + pay_bool);
                itmReturn.setProperty("is_error", "1");
                return;
            }

            string new_trade_no = GetNewTradeNo(in_code_2, in_trade_no);
            if (new_trade_no == "" || new_trade_no.Length != 20)
            {
                itmReturn.setProperty("item_number", cfg.item_number + " 訂單號碼有問題: " + in_code_2 + " (" + in_trade_no + ")");
                itmReturn.setProperty("is_error", "1");
                return;
            }

            //取得 creator 資料
            sql = "SELECT id, in_name, in_sno, in_email FROM IN_RESUME WITH(NOLOCK) WHERE in_sno = '" + in_creator_sno + "'";
            Item itmCreator = cfg.inn.applySQL(sql);
            if (itmCreator.isError() || itmCreator.getResult() == "")
            {
                itmReturn.setProperty("item_number", cfg.item_number + " 訂單查無人員資料: " + in_creator_sno);
                itmReturn.setProperty("is_error", "1");
                return;
            }

            itmReturn.setProperty("is_error", "0");
            itmReturn.setProperty("total_fee", in_pay_amount_exp);
            itmReturn.setProperty("fill_email", itmCreator.getProperty("in_email", ""));
            itmReturn.setProperty("pay_bool", pay_bool);
        }

        private string GetNewTradeNo(string in_code_2, string in_trade_no)
        {
            if (in_code_2.Length != 16)
            {
                return "";
            }

            if (in_trade_no == "" || in_trade_no == "0")
            {
                return in_code_2 + "0001";
            }
            else
            {
                int old_no = GetInt(in_trade_no.Substring(16, 4).TrimStart('0'));
                if (old_no > 9999)
                {
                    return "";
                }

                int new_no = old_no + 1;
                return in_code_2 + new_no.ToString().PadLeft(4, '0');
            }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }

            public string item_number { get; set; }
            public string scene { get; set; }
        }

        private int GetInt(string value, int defV = 0)
        {
            if (value == "") return 0;

            int result = defV;
            if (int.TryParse(value, out result))
            {
                return result;
            }
            return defV;
        }
    }
}