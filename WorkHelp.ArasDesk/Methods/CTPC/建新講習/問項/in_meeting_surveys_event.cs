﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.CTPC.Common
{
    public class in_meeting_surveys_event : Item
    {
        public in_meeting_surveys_event(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 活動問項連動事件
                輸入: 
                    1. meeting_id
                    2. meeting_survey_id
                修改: 
                    2020-09-26 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]in_meeting_surveys_event";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                strIdentityId = inn.getUserAliases(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                mode = itmR.getProperty("mode", ""),
                scene = itmR.getProperty("scene", ""),
            };

            cfg.mtsvy_table = "IN_MEETING";
            cfg.mtsvy_table = "IN_MEETING_SURVEYS";
            cfg.svy_table = "IN_SURVEY";
            cfg.svyopt_table = "IN_SURVEY_OPTION";
            cfg.svyevt_table = "IN_SURVEY_EVENT";
            if (cfg.mode == "cla")
            {
                cfg.mt_table = "IN_CLA_MEETING";
                cfg.mtsvy_table = "IN_CLA_MEETING_SURVEYS";
                cfg.svy_table = "IN_CLA_SURVEY";
                cfg.svyopt_table = "IN_CLA_SURVEY_OPTION";
                cfg.svyevt_table = "IN_CLA_SURVEY_EVENT";
            }

            if (cfg.scene == "")
            {
                Query(cfg, itmR);
            }
            else if (cfg.scene == "remove")
            {
                Remove(cfg, itmR);
            }
            else if (cfg.scene == "pick")
            {
                PickList(cfg, itmR);
            }
            else if (cfg.scene == "save")
            {
                SavePickList(cfg, itmR);
            }

            return itmR;
        }

        //儲存問項清單
        private void SavePickList(TConfig cfg, Item itmReturn)
        {
            string mid = itmReturn.getProperty("meeting_id", "");
            string sid = itmReturn.getProperty("survey_id", "");
            string option_value = itmReturn.getProperty("option_value", "");
            string survey_ids = itmReturn.getProperty("survey_ids", "");
            string in_action = "hide";

            if (mid == "")
            {
                throw new Exception("活動 id 不得為空");
            }

            if (sid == "")
            {
                throw new Exception("問項 id 不得為空");
            }

            if (option_value == "")
            {
                throw new Exception("問項值 不得為空");
            }

            if (survey_ids == "")
            {
                throw new Exception("要連動隱藏的問項 不得為空");
            }

            string[] ids = survey_ids.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            if (ids == null || ids.Length == 0)
            {
                throw new Exception("要連動隱藏的問項 不得為空");
            }

            for (int i = 0; i < ids.Length; i++)
            {
                string in_target = ids[i];

                Item itmQry = cfg.inn.newItem(cfg.svyevt_table, "get");
                itmQry.setProperty("source_id", sid);
                itmQry.setProperty("in_value", option_value);
                itmQry.setProperty("in_action", in_action);
                itmQry.setProperty("in_target", in_target);
                itmQry = itmQry.apply();

                bool can_add = false;
                if (itmQry.isError())
                {
                    can_add = true;
                }
                else if (itmQry.getItemCount() > 0)
                {
                    throw new Exception("item count > 0");
                }
                else
                {
                    throw new Exception("發生未預期的錯誤");
                }

                if (can_add)
                {
                    Item itmNew = cfg.inn.newItem(cfg.svyevt_table, "add");
                    itmNew.setProperty("source_id", sid);
                    itmNew.setProperty("in_value", option_value);
                    itmNew.setProperty("in_action", in_action);
                    itmNew.setProperty("in_target", in_target);
                    itmNew = itmNew.apply();

                    if (itmNew.isError())
                    {
                        throw new Exception("加入失敗");
                    }
                }
            }
        }

        //問項清單
        private void PickList(TConfig cfg, Item itmReturn)
        {
            string mid = itmReturn.getProperty("meeting_id", "");
            string sid = itmReturn.getProperty("survey_id", "");
            string option_value = itmReturn.getProperty("option_value", "");

            Item items = GetSurveyPickList(cfg, sid, option_value);

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                item.setType("pklist");
                itmReturn.addRelationship(item);
            }
        }

        //移除
        private void Remove(TConfig cfg, Item itmReturn)
        {
            string aml = "";
            string event_id = itmReturn.getProperty("event_id", "");

            if (event_id == "")
            {
                itmReturn.setProperty("error_message", "連動事件 id 不得為空白");
                return;
            }

            aml = "<AML>"
                + "<Item type='" + cfg.svyevt_table + "' action='delete' where=\"id='{#event_id}'\">"
                + "</Item></AML>";
            aml = aml.Replace("{#event_id}", event_id);

            Item itmResult = cfg.inn.applyAML(aml);

            if (itmResult.isError())
            {
                throw new Exception("移除失敗");
            }
        }

        //查詢
        private void Query(TConfig cfg, Item itmReturn)
        {
            string msid = itmReturn.getProperty("meeting_survey_id", "");

            if (cfg.meeting_id == "")
            {
                itmReturn.setProperty("error_message", "活動 id 不得為空白");
                return;
            }
            if (msid == "")
            {
                itmReturn.setProperty("error_message", "問項 id 不得為空白");
                return;
            }

            //取得賽事資訊
            Item itmMeeting = GetMeetingInfo(cfg);
            if (itmMeeting.isError())
            {
                itmReturn.setProperty("error_message", "查詢活動資訊發生錯誤");
                return;
            }

            Item itmMeetingSurvey = GetMeetingSurvey(cfg, msid);
            if (IsError(itmMeetingSurvey))
            {
                itmReturn.setProperty("error_message", "問項不存在");
                return;
            }

            string survey_id = itmMeetingSurvey.getProperty("survey_id", "");

            Item itmSurveyOptions = GetSurveyOptions(cfg, survey_id);
            if (IsError(itmSurveyOptions, isSingle: false))
            {
                itmReturn.setProperty("error_message", "細項不存在");
                return;
            }

            Item itmSurveyEvents = GetSurveyEvents(cfg, survey_id);
            if (IsError(itmSurveyEvents, isSingle: false))
            {
                // itmReturn.setProperty("error_message", "細項不存在");
                // return;
            }

            itmReturn.setProperty("meeting_name", itmMeeting.getProperty("in_title", ""));

            //附加問項資訊
            AppendSurvey(itmMeetingSurvey, itmReturn);

            //附加細項資訊
            AppendSurveyOptions(cfg, itmSurveyOptions, itmReturn);

            //附加連動事件資訊
            AppendSurveyEvents(cfg, itmSurveyEvents, itmReturn);
        }

        //附加問項資訊
        private void AppendSurvey(Item itmMeetingSurvey, Item itmReturn)
        {
            string in_questions = itmMeetingSurvey.getProperty("in_questions", "");
            string clear_questions = GetClearQuestions(in_questions);

            itmReturn.setProperty("in_questions", in_questions);
            itmReturn.setProperty("clear_questions", clear_questions);
            itmReturn.setProperty("survey_id", itmMeetingSurvey.getProperty("survey_id", ""));
            itmReturn.setProperty("in_property", itmMeetingSurvey.getProperty("in_property", ""));
        }

        //附加細項資訊
        private void AppendSurveyOptions(TConfig cfg, Item items, Item itmReturn)
        {
            string clear_questions = itmReturn.getProperty("clear_questions", "");

            Item itmEmpty = cfg.inn.newItem();
            itmEmpty.setType("In_Survey_Option");
            itmEmpty.setProperty("inn_selected", "selected");
            itmEmpty.setProperty("in_value", "");
            itmEmpty.setProperty("in_label", "1. 請選擇【" + clear_questions + "】");

            itmReturn.addRelationship(itmEmpty);

            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                item.setType("In_Survey_Option");
                itmReturn.addRelationship(item);
            }
        }

        //附加連動事件資訊
        private void AppendSurveyEvents(TConfig cfg, Item items, Item itmReturn)
        {
            string clear_questions = itmReturn.getProperty("clear_questions", "");
            string table_name = "survey_event_table";

            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            head.AppendLine("<thead>");
            head.AppendLine("    <th class='text-center' data-field='src_value' data-sortable='false'>" + clear_questions + "</th>");
            head.AppendLine("    <th class='text-center' data-field='src_action' data-sortable='false'>連動</th>");
            head.AppendLine("    <th class='text-center' data-field='tgt_title' data-sortable='false'>問項</th>");
            head.AppendLine("    <th class='text-center'>功能</th>");
            head.AppendLine("</thead>");

            int count = items.getItemCount(); ;

            string last_key = "";

            body.AppendLine("<tbody>");
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string event_id = item.getProperty("id", "");
                string in_action = item.getProperty("in_action", "");
                string src_key = item.getProperty("src_key", "");
                string src_value = item.getProperty("src_value", "");
                string tgt_number = item.getProperty("tgt_number", "");
                string tgt_title = item.getProperty("tgt_title", "");
                string src_action = "隱藏";

                if (last_key == src_key)
                {
                    src_value = " ";
                    src_action = " ";
                }
                else
                {
                    last_key = src_key;
                }

                body.AppendLine("<tr>");
                body.AppendLine("        <td class='text-left' data-field='src_value' >" + src_value + "</td>");
                body.AppendLine("        <td class='text-left' data-field='src_action' >" + src_action + "</td>");
                body.AppendLine("        <td class='text-left' data-field='tgt_title' >" + tgt_title + "</td>");
                body.AppendLine("        <td class='text-center'><button class='btn btn-danger' "
                    + " onclick=\"removeEvent_Click(this, '" + event_id + "', '" + tgt_title + "')\" "
                    + " >移除</button></td>");
                body.AppendLine("</tr>");
            }
            body.AppendLine("</tbody>");

            StringBuilder builder = new StringBuilder();
            builder.AppendLine(GetTableAttribute(table_name));
            builder.Append(head);
            builder.Append(body);
            builder.AppendLine("</table>");
            builder.AppendLine("<script>");
            builder.AppendLine("$('#" + table_name + "').bootstrapTable();");
            builder.AppendLine("</script>");


            itmReturn.setProperty("inn_table", builder.ToString());
        }

        private string GetTableAttribute(string table_name)
        {
            return "<table id='" + table_name + "' class='table table-hover table-bordered table-rwd rwd rwdtable' "
                + " data-toggle='table' "
                + " data-click-to-select='true' "
                + " data-striped='false' "
                + " data-search='false' "
                // + " data-pagination='true' "
                // + " data-show-pagination-switch='false'"
                + ">";
        }

        private string GetClearQuestions(string value)
        {
            if (value.Contains("."))
            {
                return value.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries).Last();
            }
            else
            {
                return value;
            }
        }

        //取得站台資訊
        private Item GetSiteInfo(TConfig cfg)
        {
            string sql = @"SELECT id, in_site_name FROM IN_SITE WITH(NOLOCK)";
            return cfg.inn.applySQL(sql);
        }

        //取得賽事資訊
        private Item GetMeetingInfo(TConfig cfg)
        {
            string sql = @"SELECT id, in_title FROM " + cfg.mt_table + " WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'";
            return cfg.inn.applySQL(sql);
        }

        //取得問項
        private Item GetMeetingSurvey(TConfig cfg, string id)
        {
            string sql = @"
                SELECT
                    t2.id AS 'survey_id'
                    , t2.in_questions
                    , t2.in_question_type
                    , t2.in_property
                    , t2.in_expense
                    , t2.in_is_limit
                    , t1.source_id
                    , t1.id
                    , t1.in_surveytype
                    , t1.in_sort_order
                FROM
                    {#mtsvy_table} t1 WITH(NOLOCK)
                INNER JOIN
                    {#svy_table} t2 WITH(NOLOCK) 
                    ON t2.id = t1.related_id
                WHERE
                    t1.id = '{#id}'
            ";
            
            sql = sql.Replace("{#mtsvy_table}", cfg.mtsvy_table)
                .Replace("{#svy_table}", cfg.svy_table)
                .Replace("{#id}", id);

            return cfg.inn.applySQL(sql);
        }

        //取得細項
        private Item GetSurveyOptions(TConfig cfg, string survey_id)
        {
            string sql = @"SELECT * FROM " + cfg.svyopt_table + " WITH(NOLOCK) WHERE source_id = '" + survey_id + "' ORDER BY sort_order";
            return cfg.inn.applySQL(sql);
        }

        //取得連動事件
        private Item GetSurveyEvents(TConfig cfg, string survey_id)
        {
            string sql = @"
                SELECT
                    t1.id
                    , t1.source_id
                    , t1.source_id + '-' + t1.in_value AS 'src_key'
                    , t2.in_questions AS 'src_title'
                    , t1.in_value     AS 'src_value'
                    , t1.in_action
                    , t3.item_number AS 'tgt_number'
                    , t3.in_questions AS 'tgt_title'
                FROM
                    {#svyevt_table} t1 WITH(NOLOCK)
                INNER JOIN
                    {#svy_table} t2 WITH(NOLOCK)
                    ON t2.id = t1.source_id
                    AND t2.id IN (SELECT related_id FROM {#mtsvy_table} WITH(NOLOCK) WHERE source_id = '{#meeting_id}')
                INNER JOIN
                    {#svy_table} t3 WITH(NOLOCK)
                    ON t3.id = t1.in_target
                    AND t3.id IN (SELECT related_id FROM {#mtsvy_table} WITH(NOLOCK) WHERE source_id = '{#meeting_id}')
                WHERE
                    t1.source_id = '{#survey_id}'
                ORDER BY
                    t2.in_questions
                    , t1.in_value
                    , t3.item_number
            ";

            sql = sql.Replace("{#mtsvy_table}", cfg.mtsvy_table)
                .Replace("{#svy_table}", cfg.svy_table)
                .Replace("{#svyevt_table}", cfg.svyevt_table)
                .Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#survey_id}", survey_id);

            return cfg.inn.applySQL(sql);
        }

        //取得連動事件待選問項
        private Item GetSurveyPickList(TConfig cfg, string survey_id, string option_value)
        {
            string sql = @"
                SELECT
                    t2.id
                    , t2.item_number
                    , t2.in_questions
                    , t2.in_property
                FROM
                    {#mtsvy_table} t1 WITH(NOLOCK)
                INNER JOIN
                    {#svy_table} t2 WITH(NOLOCK)
                    ON t2.id = t1.related_id
                WHERE
                    t1.source_id = '{#meeting_id}'
                    AND t1.related_id <> '{#survey_id}'
                    AND t2.in_property NOT IN (
                        N'in_l1'
                        , N'in_l2'
                        , N'in_l3'
                        , N'in_sno'
                        , N'in_name'
                        , N'in_birth'
                        , N'in_gender'
                        , N'in_index'
                        , N'in_group'
                        , N'in_creator'
                        , N'in_creator_sno'
                    )
                    AND t1.related_id NOT IN (
                        SELECT in_target FROM {#svyevt_table} WITH(NOLOCK) WHERE source_id = '{#survey_id}' AND in_value = N'{#option_value}'
                    )
                ORDER BY
                    t1.sort_order
            ";

            sql = sql.Replace("{#mtsvy_table}", cfg.mtsvy_table)
                .Replace("{#svy_table}", cfg.svy_table)
                .Replace("{#svyevt_table}", cfg.svyevt_table)
                .Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#survey_id}", survey_id)
                .Replace("{#option_value}", option_value);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        //是否錯誤
        private bool IsError(Item item, bool isSingle = true)
        {
            if (item.isError() || item.getResult() == "")
            {
                return true;
            }
            else if (isSingle && item.getItemCount() != 1)
            {
                return true;
            }
            else if (item.getItemCount() <= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }

            public string meeting_id { get; set; }
            public string scene { get; set; }
            public string mode { get; set; }

            public string mt_table { get; set; }
            public string mtsvy_table { get; set; }
            public string svy_table { get; set; }
            public string svyopt_table { get; set; }
            public string svyevt_table { get; set; }
            
        }

    }
}
