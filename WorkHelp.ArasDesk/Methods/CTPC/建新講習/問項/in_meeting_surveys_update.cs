﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.CTPC.Common
{
    public class in_meeting_surveys_update : Item
    {
        public in_meeting_surveys_update(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 修改活動問項
                輸入: 
                    1. meeting_id
                    2. meeting_survey_id
                記錄: 
                    2020-08-13 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]in_meeting_surveys_update";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                strIdentityId = inn.getUserAliases(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                mode = itmR.getProperty("mode", ""),
                meeting_survey_id = itmR.getProperty("meeting_survey_id", ""),
                in_sort_order = itmR.getProperty("sort_order", ""),
                in_questions = itmR.getProperty("in_questions", ""),
                in_question_type = itmR.getProperty("in_question_type", ""),
                in_request = itmR.getProperty("in_request", ""),
            };

            cfg.mtsvy_table = "IN_MEETING";
            cfg.mtsvy_table = "IN_MEETING_SURVEYS";
            cfg.svy_table = "IN_SURVEY";
            cfg.svyopt_table = "IN_SURVEY_OPTION";
            if (cfg.mode == "cla")
            {
                cfg.mt_table = "IN_CLA_MEETING";
                cfg.mtsvy_table = "IN_CLA_MEETING_SURVEYS";
                cfg.svy_table = "IN_CLA_SURVEY";
                cfg.svyopt_table = "IN_CLA_SURVEY_OPTION";
            }

            if (cfg.meeting_id == "")
            {
                throw new Exception("活動 id 不得為空白");
            }
            if (cfg.meeting_survey_id == "")
            {
                throw new Exception("問項 id 不得為空白");
            }

            if (cfg.in_sort_order == ""
                || cfg.in_questions == ""
                || cfg.in_question_type == ""
                || cfg.in_request == "")
            {
                throw new Exception("參數 不得為空白");
            }

            int sort_order = 0;
            if (!Int32.TryParse(cfg.in_sort_order, out sort_order))
            {
                throw new Exception("排序 必須為整數");
            }

            int request = 0;
            if (!Int32.TryParse(cfg.in_request, out request))
            {
                throw new Exception("必填 必須為整數");
            }

            string sql = "SELECT * FROM " + cfg.mtsvy_table + " WITH(NOLOCK) WHERE id = '" + cfg.meeting_survey_id + "'";
            Item itmMeetingSurvey = inn.applySQL(sql);
            if (IsError(itmMeetingSurvey))
            {
                throw new Exception("該問項關聯不存在");
            }

            string old_survey_id = itmMeetingSurvey.getProperty("related_id", "");
            string old_sort_order = itmMeetingSurvey.getProperty("sort_order", "");

            itmR.setProperty("old_survey_id", old_survey_id);
            itmR.setProperty("old_sort_order", old_sort_order);

            sql = @"SELECT * FROM " + cfg.svy_table + " WITH(NOLOCK) WHERE id = '" + old_survey_id + "'";
            Item itmSurvey = inn.applySQL(sql);

            if (IsError(itmSurvey))
            {
                throw new Exception("問項不存在");
            }

            string old_item_number = itmSurvey.getProperty("item_number", "");
            string old_in_questions = itmSurvey.getProperty("in_questions", "");
            string old_in_question_type = itmSurvey.getProperty("in_question_type", "");
            string old_in_request = itmSurvey.getProperty("in_request", "");
            string old_in_is_default = itmSurvey.getProperty("in_is_default", "");

            //僅修改 sort_order
            if (cfg.in_questions == old_in_questions
                && cfg.in_question_type == old_in_question_type
                && cfg.in_request == old_in_request
                && cfg.in_sort_order != old_sort_order)
            {
                UpdateMeetingSurvey(cfg, itmR);
                return itmR;
            }

            bool need_clone = false;
            if (old_in_is_default == "1")
            {
                need_clone = true;
            }

            if (!need_clone)
            {
                need_clone = !IsOwnedSurvey(cfg, cfg.meeting_survey_id, old_survey_id);
            }

            if (need_clone)
            {
                //複製問項後修改
                CloneSurvey(cfg, itmSurvey, itmR);
            }
            else
            {
                //修改排序
                if (cfg.in_sort_order != old_sort_order)
                {
                    UpdateMeetingSurvey(cfg, itmR);
                }

                //獨佔問項，可直接修改
                UpdateSurvey(cfg, old_survey_id, itmR);
            }

            return itmR;
        }

        //是否獨佔問項
        private bool IsOwnedSurvey(TConfig cfg, string meeting_survey_id, string old_survey_id)
        {
            string sql = @"
                SELECT
                    COUNT(*) AS 'use_count'
                FROM
                    {#table} WITH(NOLOCK)
                WHERE
                    id <> '{#id}'
                    AND related_id = '{#related_id}'
            ";

            sql = sql.Replace("{#table}", cfg.mtsvy_table)
                .Replace("{#id}", meeting_survey_id)
                .Replace("{#related_id}", old_survey_id);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item itmOther = cfg.inn.applySQL(sql);

            if (IsError(itmOther))
            {
                throw new Exception("檢查活動問項發生錯誤");
            }

            string use_count = itmOther.getProperty("use_count", "0");

            return use_count == "" || use_count == "0";
        }

        //複製後修改問項
        private void CloneSurvey(TConfig cfg, Item itmSurvey, Item itmReturn)
        {
            string meeting_id = itmReturn.getProperty("meeting_id", "");
            string meeting_survey_id = itmReturn.getProperty("meeting_survey_id", "");
            string old_survey_id = itmReturn.getProperty("old_survey_id", "");

            itmSurvey.setType(cfg.svy_table);

            //複製問項
            Item itmClone = itmSurvey.clone(true);
            itmClone.setProperty("in_is_default", "0");
            itmClone.apply();

            if (itmClone.isError())
            {
                throw new Exception("問項複製 失敗");
            }

            string new_survey_id = itmClone.getID();

            //複製細項
            Item itmOptions = cfg.inn.newItem(cfg.svyopt_table, "get");
            itmOptions.setProperty("source_id", old_survey_id);
            itmOptions = itmOptions.apply();
            if (!IsError(itmOptions, isSingle: false))
            {
                int count = itmOptions.getItemCount();
                for (int i = 0; i < count; i++)
                {
                    Item itmOption = itmOptions.getItemByIndex(i);
                    Item itmOptionClone = itmOption.clone(true);
                    itmOptionClone.setProperty("source_id", new_survey_id);
                    itmOptionClone.apply();
                }
            }

            //將[原本的問項]替換成[複製出來的問項]

            string sql = @"
                UPDATE 
                    {#table}
                SET 
                    related_id = '{#new_survey_id}'
                    , sort_order = '{#sort_order}'
                WHERE 
                    id = '{#meeting_survey_id}'
            ";

            sql = sql.Replace("{#table}", cfg.mtsvy_table)
                .Replace("{#meeting_survey_id}", meeting_survey_id)
                .Replace("{#new_survey_id}", new_survey_id)
                .Replace("{#sort_order}", itmReturn.getProperty("sort_order", ""));

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item itmUpdate = cfg.inn.applySQL(sql);

            if (itmUpdate.isError())
            {
                throw new Exception("更新問項 related_id 失敗");
            }

            UpdateSurvey(cfg, new_survey_id, itmReturn);
        }

        //修改問項
        private void UpdateMeetingSurvey(TConfig cfg, Item itmReturn)
        {
            string sql = @"
                UPDATE 
                    {#table}
                SET 
                    sort_order = '{#sort_order}'
                WHERE 
                    id = '{#meeting_survey_id}'
                ";

            sql = sql.Replace("{#table}", cfg.mtsvy_table)
                .Replace("{#meeting_survey_id}", cfg.meeting_survey_id)
                .Replace("{#sort_order}", itmReturn.getProperty("sort_order", ""));

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item itmUpdate = cfg.inn.applySQL(sql);

            if (itmUpdate.isError())
            {
                throw new Exception("更新問項 related_id 失敗");
            }
        }

        //修改問項
        private void UpdateSurvey(TConfig cfg, string survey_id, Item itmReturn)
        {
            string sql = @"
                UPDATE 
                    {#table}
                SET
                    in_questions = N'{#in_questions}'
                    , in_question_type = N'{#in_question_type}'
                    , in_request = N'{#in_request}'
                    , keyed_name = item_number + ' ' + N'{#in_questions}'
                WHERE
                    id = '{#survey_id}'
                ";

            sql = sql.Replace("{#table}", cfg.svy_table)
                .Replace("{#survey_id}", survey_id)
                .Replace("{#in_questions}", itmReturn.getProperty("in_questions", ""))
                .Replace("{#in_question_type}", itmReturn.getProperty("in_question_type", ""))
                .Replace("{#in_request}", itmReturn.getProperty("in_request", ""));

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item itmUpdate = cfg.inn.applySQL(sql);

            if (itmUpdate.isError())
            {
                throw new Exception("問項修改失敗");
            }
        }

        private bool IsError(Item item, bool isSingle = true)
        {
            if (item.isError() || item.getResult() == "")
            {
                return true;
            }
            else if (isSingle && item.getItemCount() != 1)
            {
                return true;
            }
            else if (item.getItemCount() <= 0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }

            public string meeting_id { get; set; }
            public string mode { get; set; }
            public string meeting_survey_id { get; set; }
            public string in_sort_order { get; set; }
            public string in_questions { get; set; }
            public string in_question_type { get; set; }
            public string in_request { get; set; }

            public string mt_table { get; set; }
            public string mtsvy_table { get; set; }
            public string svy_table { get; set; }
            public string svyopt_table { get; set; }
        }
    }
}