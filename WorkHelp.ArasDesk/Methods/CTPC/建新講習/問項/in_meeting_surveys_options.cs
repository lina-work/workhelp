﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.CTPC.Common
{
    public class in_meeting_surveys_options : Item
    {
        public in_meeting_surveys_options(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 取得活動問項細項
                輸入: 
                    1. meeting_id
                    2. meeting_survey_id
                修改: 
                    2020-08-12 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]in_meeting_surveys_options";

            Item itmR = this;

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                strIdentityId = inn.getUserAliases(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                mode = itmR.getProperty("mode", ""),
                meeting_survey_id = itmR.getProperty("meeting_survey_id", ""),
            };

            cfg.mt_table = "IN_MEETING";
            cfg.mtsvy_table = "IN_MEETING_SURVEYS";
            cfg.svy_table = "IN_SURVEY";
            cfg.svyopt_table = "IN_SURVEY_OPTION";
            if (cfg.mode == "cla")
            {
                cfg.mt_table = "IN_CLA_MEETING";
                cfg.mtsvy_table = "IN_CLA_MEETING_SURVEYS";
                cfg.svy_table = "IN_CLA_SURVEY";
                cfg.svyopt_table = "IN_CLA_SURVEY_OPTION";
            }

            if (cfg.meeting_id == "")
            {
                itmR.setProperty("error_message", "活動 id 不得為空白");
                return itmR;
            }
            if (cfg.meeting_survey_id == "")
            {
                itmR.setProperty("error_message", "問項 id 不得為空白");
                return itmR;
            }

            //取得活動資訊
            Item itmMeeting = GetMeetingInfo(cfg);
            if (itmMeeting.isError())
            {
                itmR.setProperty("error_message", "查詢活動資訊發生錯誤");
                return itmR;
            }

            Item itmMeetingSurvey = GetMeetingSurvey(cfg, cfg.meeting_survey_id);
            if (IsError(itmMeetingSurvey))
            {
                itmR.setProperty("error_message", "問項不存在");
                return itmR;
            }

            itmR.setProperty("meeting_name", itmMeeting.getProperty("in_title", ""));

            itmR.setProperty("in_questions", itmMeetingSurvey.getProperty("in_questions", ""));
            itmR.setProperty("survey_id", itmMeetingSurvey.getProperty("survey_id", ""));
            itmR.setProperty("in_property", itmMeetingSurvey.getProperty("in_property", ""));

            //費用設定是否隱藏
            itmR.setProperty("expense_hide", GetHideClass(itmMeetingSurvey, "in_expense"));
            //人數設定是否隱藏
            itmR.setProperty("limit_hide", GetHideClass(itmMeetingSurvey, "in_is_limit"));

            string in_property = itmMeetingSurvey.getProperty("in_property", "");
            if (in_property == "in_l1")
            {
                Item itmL2 = GetMeetingSurveyItem(cfg, "in_l2");
                if (itmL2.getResult() != "")
                {
                    itmMeetingSurvey.setProperty("need_detail", "1");
                }
            }
            else if (in_property == "in_l2")
            {
                Item itmL3 = GetMeetingSurveyItem(cfg, "in_l3");
                if (itmL3.getResult() != "")
                {
                    itmMeetingSurvey.setProperty("need_detail", "1");
                }

                itmMeetingSurvey.setProperty("need_l1", "1");
                AppendL1Values(cfg, itmR);
            }
            else if (in_property == "in_l3")
            {
                itmMeetingSurvey.setProperty("need_l1", "1");
                itmMeetingSurvey.setProperty("need_l2", "1");
                AppendL1Values(cfg, itmR);
                AppendL2Values(cfg, itmR);
            }

            //第一階是否隱藏
            itmR.setProperty("l1_hide", GetHideClass(itmMeetingSurvey, "need_l1"));
            //第二階是否隱藏
            itmR.setProperty("l2_hide", GetHideClass(itmMeetingSurvey, "need_l2"));
            //下一階選項設定是否隱藏
            itmR.setProperty("detail_hide", GetHideClass(itmMeetingSurvey, "need_detail"));

            //取得細項
            AppendSurveyOptions(cfg, itmMeetingSurvey, itmR);

            return itmR;
        }

        //取得站台資訊
        private Item GetSiteInfo(TConfig cfg)
        {
            string sql = @"SELECT id, in_site_name FROM IN_SITE WITH(NOLOCK)";
            return cfg.inn.applySQL(sql);
        }

        //取得活動資訊
        private Item GetMeetingInfo(TConfig cfg)
        {
            string sql = "SELECT id, in_title FROM " + cfg.mt_table + " WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'";
            return cfg.inn.applySQL(sql);
        }

        private string GetHideClass(Item itmSurvey, string in_property)
        {
            string result = "";

            string value = itmSurvey.getProperty(in_property, "");

            if (value != "1")
            {
                result = "data-visible=\"false\"";
            }

            return result;
        }

        //取得第一階選項
        private void AppendL1Values(TConfig cfg, Item itmReturn)
        {
            string sql = @"
                SELECT
                    t2.id AS 'survey_id'
                    , t2.item_number AS 'survey_number'
                    , t2.in_questions AS 'survey_name'
                    , t3.in_label
                    , t3.in_value
                FROM 
                    {#mtsvy_table} t1 WITH(NOLOCK)
                INNER JOIN
                    {#svy_table} t2 WITH(NOLOCK)
                    ON t2.id = t1.related_id
                INNER JOIN
                    {#svyopt_table} t3 WITH(NOLOCK)
                    ON t3.source_id = t1.related_id
                WHERE
                    t1.source_id = '{#meeting_id}'
                    AND t2.in_property = 'in_l1'
                ORDER BY
                    t3.sort_order
            ";

            sql = sql.Replace("{#mtsvy_table}", cfg.mtsvy_table)
                .Replace("{#svy_table}", cfg.svy_table)
                .Replace("{#svyopt_table}", cfg.svyopt_table)
                .Replace("{#meeting_id}", cfg.meeting_id);

            Item items = cfg.inn.applySQL(sql);

            if (IsError(items, isSingle: false))
            {
                return;
            }

            List<string> list = new List<string>();
            string survey_name = "";
            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_value = item.getProperty("in_value", "");
                list.Add(in_value);

                if (i == 0)
                {
                    survey_name = item.getProperty("survey_name", "");
                }
            }

            string l1_values = string.Join("@", list.Where(x => !string.IsNullOrEmpty(x)));

            itmReturn.setProperty("l1_name", survey_name);
            itmReturn.setProperty("l1_values", l1_values);

        }

        //取得第二階選項
        private void AppendL2Values(TConfig cfg, Item itmReturn)
        {
            string sql = @"
                SELECT
                    t2.id AS 'survey_id'
                    , t2.item_number AS 'survey_number'
                    , t2.in_questions AS 'survey_name'
                    , t3.in_label
                    , t3.in_value
                    , t3.in_filter
                FROM 
                    {#mtsvy_table} t1 WITH(NOLOCK)
                INNER JOIN
                    {#svy_table} t2 WITH(NOLOCK)
                    ON t2.id = t1.related_id
                INNER JOIN
                    {#svyopt_table} t3 WITH(NOLOCK)
                    ON t3.source_id = t1.related_id
                WHERE
                    t1.source_id = '{#meeting_id}'
                    AND t2.in_property = 'in_l2'
                ORDER BY
                    t3.sort_order
            ";

            sql = sql.Replace("{#mtsvy_table}", cfg.mtsvy_table)
                .Replace("{#svy_table}", cfg.svy_table)
                .Replace("{#svyopt_table}", cfg.svyopt_table)
                .Replace("{#meeting_id}", cfg.meeting_id);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item items = cfg.inn.applySQL(sql);

            if (IsError(items, isSingle: false))
            {
                return;
            }

            Dictionary<string, List<string>> dictionary = new Dictionary<string, List<string>>();

            string survey_name = "";

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_filter = item.getProperty("in_filter", "");
                string in_value = item.getProperty("in_value", "");

                List<string> list = null;
                if (dictionary.ContainsKey(in_filter))
                {
                    list = dictionary[in_filter];
                }
                else
                {
                    list = new List<string>();
                    dictionary.Add(in_filter, list);
                }

                if (!list.Contains(in_value))
                {
                    list.Add(in_value);
                }

                if (i == 0)
                {
                    survey_name = item.getProperty("survey_name", "");
                }
            }

            List<string> list2 = new List<string>();
            foreach (KeyValuePair<string, List<string>> kv in dictionary)
            {
                string key = kv.Key;
                string values = string.Join("@", kv.Value.Where(x => !string.IsNullOrEmpty(x)));
                string row = key + ":" + values;
                list2.Add(row);
            }

            string l2_values = string.Join("@@", list2.Where(x => !string.IsNullOrEmpty(x)));

            itmReturn.setProperty("l2_name", survey_name);
            itmReturn.setProperty("l2_values", l2_values);

        }

        //取得細項
        private void AppendSurveyOptions(TConfig cfg, Item itmMeetingSurvey, Item itmReturn)
        {
            string meeting_id = itmReturn.getProperty("meeting_id", "");
            string survey_id = itmReturn.getProperty("survey_id", "");
            string in_property = itmReturn.getProperty("in_property", "");

            string sql = @"SELECT * FROM " + cfg.svyopt_table + " WITH(NOLOCK) WHERE source_id = '" + survey_id + "' ORDER BY sort_order";

            Item items = cfg.inn.applySQL(sql);

            if (IsError(items, isSingle: false))
            {
                AppendSurveyOptions2(cfg, meeting_id, survey_id, itmMeetingSurvey, itmReturn);
                return;
            }

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);

                item.setType("in_survey_option");

                item.setProperty("meeting_id", meeting_id);

                //預設值
                item.setProperty("in_limit_min", "1");
                item.setProperty("in_limit_max", "1");
                item.setProperty("in_year_val1", "");
                item.setProperty("in_year_val2", "");
                item.setProperty("in_birth_val1", "");
                item.setProperty("in_birth_val2", "");
                item.setProperty("in_gender_val", "");
                item.setProperty("in_degree_val", "");

                SetExtendValue(cfg, item);

                if (in_property == "in_l2")
                {
                    item.setProperty("dynamic_filter1", item.getProperty("in_filter", ""));
                }
                else if (in_property == "in_l3")
                {
                    item.setProperty("dynamic_filter1", item.getProperty("in_grand_filter", ""));
                    item.setProperty("dynamic_filter2", item.getProperty("in_filter", ""));
                }

                itmReturn.addRelationship(item);
            }
        }

        private void AppendSurveyOptions2(TConfig cfg, string meeting_id, string survey_id, Item itmMeetingSurvey, Item itmReturn)
        {
            Item itmEmpty = cfg.inn.newItem();
            itmEmpty.setType("in_survey_option");
            itmEmpty.setProperty("source_id", survey_id);
            itmEmpty.setProperty("meeting_id", meeting_id);
            itmReturn.addRelationship(itmEmpty);

            string in_selectoption  = itmMeetingSurvey.getProperty("in_selectoption", "");
            if (in_selectoption != "")
            {
                var rows = in_selectoption.Split(new char[] { '@' }, StringSplitOptions.RemoveEmptyEntries);
                if (rows != null && rows.Length > 0)
                {
                    foreach(var row in rows)
                    {
                        Item item = cfg.inn.newItem();
                        item.setType("in_survey_option");
                        item.setProperty("source_id", survey_id);
                        item.setProperty("meeting_id", meeting_id);
                        item.setProperty("in_value", row);
                        item.addRelationship(item);
                    }
                }
            }

            itmReturn.setProperty("inn_no_option", "1");
        }

        private void SetExtendValue(TConfig cfg, Item item)
        {
            string in_extend_value = item.getProperty("in_extend_value", "");
            if (in_extend_value == "")
            {
                return;
            }

            string[] conditions = in_extend_value.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
            if (conditions == null || conditions.Length == 0)
            {
                return;
            }

            string limit_min = "";
            string limit_max = "";
            string year_val1 = "";
            string year_val2 = "";
            string birth_val1 = "";
            string birth_val2 = "";
            string in_gender_val = "";
            string in_degree_val = "";

            for (int i = 0; i < conditions.Length; i++)
            {
                Item itmTag = GetTagValues(cfg, conditions[i]);
                //CCO.Utilities.WriteDebug(strMethodName, itmTag.dom.InnerXml);

                string is_error = itmTag.getProperty("is_error", "0");
                string tag_name = itmTag.getProperty("tag_name", "");
                string tag_val1 = itmTag.getProperty("tag_val1", "");
                string tag_val2 = itmTag.getProperty("tag_val2", "");

                if (is_error == "1")
                {
                    continue;
                }

                switch (tag_name)
                {
                    case "in_limit":
                        limit_min = tag_val1;
                        limit_max = tag_val2;
                        break;

                    case "in_year":
                        year_val1 = tag_val1;
                        year_val2 = tag_val2;
                        break;

                    case "in_birth":
                        Item itmTag_2 = GetTagValues2(cfg, conditions[i]);
                        string is_error_2 = itmTag_2.getProperty("is_error", "0");
                        string tag_name_2 = itmTag_2.getProperty("tag_name", "");
                        string tag_val1_2 = itmTag_2.getProperty("tag_val1", "");
                        string tag_val2_2 = itmTag_2.getProperty("tag_val2", "");
                        if (is_error != "1")
                        {
                            birth_val1 = tag_val1_2;
                            birth_val2 = tag_val2_2;
                        }
                        break;

                    case "in_gender":
                        in_gender_val = tag_val1;
                        break;

                    case "in_degree":
                        in_degree_val = tag_val1;
                        break;

                    default:
                        break;
                }
            }

            item.setProperty("in_limit_min", limit_min);
            item.setProperty("in_limit_max", limit_max);

            item.setProperty("in_year_val1", year_val1);
            item.setProperty("in_year_val2", year_val2);

            item.setProperty("in_birth_val1", birth_val1);
            item.setProperty("in_birth_val2", birth_val2);

            item.setProperty("in_gender_val", in_gender_val);
            item.setProperty("in_degree_val", in_degree_val);
        }

        private Item GetTagValues(TConfig cfg, string condition)
        {
            Item item = cfg.inn.newItem();
            if (condition == "")
            {
                item.setProperty("is_error", "1");
                item.setProperty("err_msg", "");
                return item;
            }

            if (!condition.Contains(":"))
            {
                item.setProperty("is_error", "1");
                item.setProperty("err_msg", "無條件或條件不含分割字元");
                return item;
            }

            var comment = condition.Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
            if (comment == null || comment.Length < 2)
            {
                item.setProperty("is_error", "1");
                item.setProperty("err_msg", "條件分割後資料錯誤");
                return item;
            }

            string tag_name = comment[0];
            string tag_value = comment[1];
            if (tag_name == "" || tag_value == "")
            {
                item.setProperty("is_error", "1");
                item.setProperty("err_msg", "條件欄位或值錯誤");
                return item;
            }

            if (!tag_value.Contains("~"))
            {
                item.setProperty("tag_name", tag_name);
                item.setProperty("tag_val1", tag_value);
                item.setProperty("tag_val2", tag_value);
                return item;
            }

            var values = tag_value.Split(new char[] { '~' }, StringSplitOptions.RemoveEmptyEntries);
            if (values == null)
            {
                item.setProperty("is_error", "1");
                item.setProperty("err_msg", "條件欄位值分割後資料錯誤");
            }
            else if (values.Length == 1)
            {
                item.setProperty("tag_name", tag_name);
                item.setProperty("tag_val1", values[0]);
                item.setProperty("tag_val2", values[0]);
            }
            else if (values.Length == 2)
            {
                item.setProperty("tag_name", tag_name);
                item.setProperty("tag_val1", values[0]);
                item.setProperty("tag_val2", values[1]);
            }
            else
            {
                item.setProperty("is_error", "1");
                item.setProperty("err_msg", "條件欄位值分割後資料陣列異常");
            }

            return item;
        }

        private Item GetTagValues2(TConfig cfg, string condition)
        {
            Item item = cfg.inn.newItem();
            if (condition == "")
            {
                item.setProperty("is_error", "1");
                item.setProperty("err_msg", "");
                return item;
            }

            if (!condition.Contains(":"))
            {
                item.setProperty("is_error", "1");
                item.setProperty("err_msg", "無條件或條件不含分割字元");
                return item;
            }

            var comment = condition.Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
            if (comment == null || comment.Length < 2)
            {
                item.setProperty("is_error", "1");
                item.setProperty("err_msg", "條件分割後資料錯誤");
                return item;
            }

            string tag_name = comment[0];
            string tag_value = comment[1];
            if (tag_name == "" || tag_value == "")
            {
                item.setProperty("is_error", "1");
                item.setProperty("err_msg", "條件欄位或值錯誤");
                return item;
            }

            if (!tag_value.Contains("~"))
            {
                item.setProperty("tag_name", tag_name);
                item.setProperty("tag_val1", tag_value);
                item.setProperty("tag_val2", tag_value);
                return item;
            }

            var values = tag_value.Split('~');
            if (values == null || values.Length < 2)
            {
                item.setProperty("is_error", "1");
                item.setProperty("err_msg", "條件欄位值分割後資料錯誤");
            }
            else
            {
                item.setProperty("tag_name", tag_name);
                item.setProperty("tag_val1", values[0]);
                item.setProperty("tag_val2", values[1]);
            }

            return item;
        }

        //取得問項
        private Item GetMeetingSurvey(TConfig cfg, string id)
        {
            string sql = @"
                SELECT
                    t2.id AS 'survey_id'
                    , t2.in_questions
                    , t2.in_question_type
                    , t2.in_selectoption
                    , t2.in_property
                    , t2.in_expense
                    , t2.in_is_limit
                    , t1.source_id
                    , t1.id
                    , t1.in_surveytype
                    , t1.in_sort_order
                FROM
                    {#mtsvy_table} t1 WITH(NOLOCK)
                INNER JOIN
                    {#svy_table} t2 WITH(NOLOCK) 
                    ON t2.id = t1.related_id
                WHERE
                    t1.id = '{#id}'
            ";

            sql = sql.Replace("{#mtsvy_table}", cfg.mtsvy_table)
                .Replace("{#svy_table}", cfg.svy_table)
                .Replace("{#id}", id);

            return cfg.inn.applySQL(sql);
        }

        private Item GetMeetingSurveyItem(TConfig cfg, string in_property)
        {
            string sql = @"
                SELECT
                    t2.id
                FROM
                    {#mtsvy_table} t1 WITH(NOLOCK)
                INNER JOIN
                    {#svy_table} t2 WITH(NOLOCK) 
                    ON t2.id = t1.related_id
                WHERE
                    t1.source_id = '{#meeting_id}'
                    AND t2.in_property = '{#in_property}'
            ";

            sql = sql.Replace("{#mtsvy_table}", cfg.mtsvy_table)
                .Replace("{#svy_table}", cfg.svy_table)
                .Replace("{#in_property}", in_property);

            return cfg.inn.applySQL(sql);
        }

        //是否錯誤
        private bool IsError(Item item, bool isSingle = true)
        {
            if (item.isError() || item.getResult() == "")
            {
                return true;
            }
            else if (isSingle && item.getItemCount() != 1)
            {
                return true;
            }
            else if (item.getItemCount() <= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }

            public string meeting_id { get; set; }
            public string mode { get; set; }
            public string meeting_survey_id { get; set; }

            public string mt_table { get; set; }
            public string mtsvy_table { get; set; }
            public string svy_table { get; set; }
            public string svyopt_table { get; set; }
        }
    }
}