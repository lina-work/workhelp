﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.CTPC.Common
{
    public class in_meeting_surveys_options2 : Item
    {
        public in_meeting_surveys_options2(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 取得活動問項細項
                輸入: 
                    1. meeting_id
                    2. option_id
                修改: 
                    2020-08-12 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]in_meeting_surveys_options2";

            Item itmR = this;

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                strIdentityId = inn.getUserAliases(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                mode = itmR.getProperty("mode", ""),
                option_id = itmR.getProperty("option_id", "")
            };

            cfg.mtsvy_table = "IN_MEETING";
            cfg.mtsvy_table = "IN_MEETING_SURVEYS";
            cfg.svy_table = "IN_SURVEY";
            cfg.svyopt_table = "IN_SURVEY_OPTION";
            if (cfg.mode == "cla")
            {
                cfg.mt_table = "IN_CLA_MEETING";
                cfg.mtsvy_table = "IN_CLA_MEETING_SURVEYS";
                cfg.svy_table = "IN_CLA_SURVEY";
                cfg.svyopt_table = "IN_CLA_SURVEY_OPTION";
            }

            string meeting_id = itmR.getProperty("meeting_id", "");

            if (meeting_id == "")
            {
                throw new Exception("活動 id 不得為空白");
            }

            if (cfg.option_id == "")
            {
                throw new Exception("選項 id 不得為空白");
            }

            Item itmSite = GetSiteInfo(cfg);
            Item itmMeeting = GetMeetingInfo(cfg);
            Item itmLevel = GetLevelInfo(cfg);
            Item itmOption = GetSurveyOption(cfg, cfg.option_id);

            string survey_id = itmOption.getProperty("source_id", "");

            Item itmSurveys = GetMeetingSurveys(cfg);

            Item itmSurveyMain = FindSurvey(inn, itmSurveys, "related_id", survey_id);
            if (itmSurveyMain == null)
            {
                throw new Exception("問項資料錯誤");
            }

            string survey_lv = itmSurveyMain.getProperty("in_property", "");

            string option_lv = "";
            if (survey_lv == "in_l1")
            {
                option_lv = "in_l2";
            }
            else if (survey_lv == "in_l2")
            {
                option_lv = "in_l3";
            }

            Item itmSurveySub = FindSurvey(inn, itmSurveys, "in_property", option_lv);
            string option_survey_id = itmSurveySub.getProperty("related_id", "");

            Item itmOptions = GetSurveyOptions(cfg, itmSurveyMain, itmOption, option_lv);

            itmR.setProperty("site_name", itmSite.getProperty("in_site_name", ""));
            itmR.setProperty("meeting_name", itmMeeting.getProperty("in_title", ""));

            string in_property = itmSurveyMain.getProperty("in_property", "");
            bool is_pkey = itmSurveyMain.getProperty("in_is_pkey", "") == "1";
            bool is_nokey = itmSurveyMain.getProperty("in_is_nokey", "") == "1";
            bool is_expense = itmSurveyMain.getProperty("in_expense", "") == "1";
            bool is_limit = itmSurveyMain.getProperty("in_is_limit", "") == "1";

            itmR.setProperty("item_number", itmSurveyMain.getProperty("item_number", ""));
            itmR.setProperty("in_questions", itmSurveyMain.getProperty("in_questions", ""));
            itmR.setProperty("in_question_type_label", itmSurveyMain.getProperty("in_question_type_label", ""));
            itmR.setProperty("in_property", itmSurveyMain.getProperty("in_property", ""));
            itmR.setProperty("in_is_limit", itmSurveyMain.getProperty("in_is_limit", ""));

            string in_l2_is_expense = itmLevel.getProperty("in_l2_is_expense", "");
            string in_l2_is_limit = itmLevel.getProperty("in_l2_is_limit", "");
            string in_l3_is_expense = itmLevel.getProperty("in_l3_is_expense", "");
            string in_l3_is_limit = itmLevel.getProperty("in_l3_is_limit", "");

            string detail_hide = "data-visible=\"false\"";
            if (in_property == "in_l1")
            {
                itmR.setProperty("l1_option_name", itmOption.getProperty("in_value", ""));

                itmR.setProperty("expense_hide", in_l2_is_expense == "1" ? "" : "data-visible=\"false\"");
                itmR.setProperty("limit_hide", in_l2_is_limit == "1" ? "" : "data-visible=\"false\"");

                itmR.setProperty("l2_hide", "item_show_0");
                detail_hide = "";
            }
            else if (in_property == "in_l2")
            {
                itmR.setProperty("l1_option_name", itmOption.getProperty("in_filter", ""));
                itmR.setProperty("l2_option_name", itmOption.getProperty("in_value", ""));

                itmR.setProperty("expense_hide", in_l3_is_expense == "1" ? "" : "data-visible=\"false\"");
                itmR.setProperty("limit_hide", in_l3_is_limit == "1" ? "" : "data-visible=\"false\"");
            }
            itmR.setProperty("detail_hide", detail_hide);

            itmLevel.setProperty("in_level_array", "in_l1,in_l2");
            itmR.setProperty("in_level_json", GetOptionsJson(cfg, itmLevel));

            AppendSurveyOptions(cfg, itmOptions, option_survey_id, option_lv, itmR);

            return itmR;
        }

        //取得站台資訊
        private Item GetSiteInfo(TConfig cfg)
        {
            string sql = @"SELECT id, in_site_name FROM IN_SITE WITH(NOLOCK)";
            return cfg.inn.applySQL(sql);
        }

        //取得賽事資訊
        private Item GetMeetingInfo(TConfig cfg)
        {
            string sql = "SELECT id, in_title FROM " + cfg.mt_table + " WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'";
            return cfg.inn.applySQL(sql);
        }

        //取得問項
        private Item FindSurvey(Innovator inn, Item items, string property, string value)
        {
            Item search = null;

            if (value != "")
            {
                int count = items.getItemCount();
                for (int i = 0; i < count; i++)
                {
                    Item item = items.getItemByIndex(i);
                    if (item.getProperty(property, "") == value)
                    {
                        search = item;
                        break;
                    }
                }
            }

            if (search == null)
            {
                search = inn.newItem();
            }

            return search;
        }

        //取得三階問項
        private Item GetMeetingSurveys(TConfig cfg)
        {
            string sql = @"
                SELECT
                    t1.source_id
                    , t1.related_id
                    , t1.id
                    , t1.in_surveytype
                    , t1.in_sort_order
                    , t1.sort_order
                    , t2.item_number
                    , t2.in_questions
                    , t2.in_property
                    , t2.in_question_type
                    , t2.in_client_remove
                    , t2.in_user_template
                    , t2.in_request
                    , t2.in_expense
                    , t2.in_is_pkey
                    , t2.in_is_nokey
                    , t2.in_is_limit
                    , t2.in_is_default
                    , t3.label_zt AS 'in_question_type_label'
                FROM
                    {#mtsvy_table} t1 WITH(NOLOCK)
                INNER JOIN
                    {#svy_table} t2 WITH(NOLOCK) ON t2.id = t1.related_id
                INNER JOIN
                    (
                        SELECT
                            t11.value,
                            t11.label_zt
                        FROM
                            [VALUE] t11
                        INNER JOIN
                            [LIST] t12 ON t12.id = t11.source_id
                        WHERE
                            t12.name = N'in_question_type'
                    ) t3 ON t3.value = t2.in_question_type
                WHERE
                    t1.source_id = '{#meeting_id}'
                    AND t1.in_surveytype = '1'
                    AND t2.in_property IN ('in_l1', 'in_l2', 'in_l3')
                ORDER BY
                    t1.sort_order
                ";

            sql = sql.Replace("{#mtsvy_table}", cfg.mtsvy_table)
                .Replace("{#svy_table}", cfg.svy_table)
                .Replace("{#meeting_id}", cfg.meeting_id);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        //取得細項
        private Item GetSurveyOption(TConfig cfg, string option_id)
        {
            string sql = "SELECT * FROM " + cfg.svyopt_table + " WITH(NOLOCK) WHERE id = '" + option_id + "'";
            return cfg.inn.applySQL(sql);
        }

        //取得細項
        private Item GetSurveyOptions(TConfig cfg, Item itmSurvey, Item itmOption, string option_lv)
        {
            string sql = @"
                SELECT
                    t1.*
                    , '{#option_lv}' AS 'option_lv'
                FROM
                    {#svyopt_table} t1 WITH(NOLOCK)
                WHERE 
                    t1.source_id = (SELECT TOP 1 id FROM {#svy_table} WITH(NOLOCK) WHERE in_filter_by = N'{#item_number}')
                    AND t1.in_filter = N'{#in_filter}'
                ORDER BY
                    t1.sort_order
            ";
            
            sql = sql.Replace("{#svy_table}", cfg.svy_table)
                .Replace("{#svyopt_table}", cfg.svyopt_table)
                .Replace("{#item_number}", itmSurvey.getProperty("item_number", ""))
                .Replace("{#option_lv}", option_lv)
                .Replace("{#in_filter}", itmOption.getProperty("in_value", ""));

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        //附加細項
        private void AppendSurveyOptions(TConfig cfg, Item items, string option_survey_id, string option_lv, Item itmReturn)
        {
            string meeting_id = itmReturn.getProperty("meeting_id", "");
            string in_property = itmReturn.getProperty("in_property", "");

            if (IsError(items, isSingle: false))
            {
                Item itmEmpty = cfg.inn.newItem();

                itmEmpty.setType("in_survey_option");
                itmEmpty.setProperty("source_id", option_survey_id);
                itmEmpty.setProperty("option_lv", option_lv);
                itmReturn.addRelationship(itmEmpty);

                itmReturn.setProperty("inn_no_option", "1");
                itmReturn.setProperty("inn_option_count", "0");

                return;
            }

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);

                item.setType("in_survey_option");

                item.setProperty("meeting_id", meeting_id);

                //預設值
                item.setProperty("in_limit_min", "1");
                item.setProperty("in_limit_max", "1");
                item.setProperty("in_year_val1", "");
                item.setProperty("in_year_val2", "");
                item.setProperty("in_birth_val1", "");
                item.setProperty("in_birth_val2", "");
                item.setProperty("in_gender_val", "");
                item.setProperty("in_degree_val", "");

                if (in_property == "in_l2")
                {
                    item.setProperty("l1_value", item.getProperty("in_filter", ""));
                }
                else if (in_property == "in_l3")
                {
                    item.setProperty("l1_value", item.getProperty("in_grand_filter", ""));
                    item.setProperty("l2_value", item.getProperty("in_filter", ""));
                }

                Item itmExtend = GetExtendInfo(cfg, item.getProperty("in_extend_value", ""));

                item.setProperty("in_limit_min", itmExtend.getProperty("in_limit_min", ""));
                item.setProperty("in_limit_max", itmExtend.getProperty("in_limit_max", ""));
                item.setProperty("in_year_val1", itmExtend.getProperty("in_year_val1", ""));
                item.setProperty("in_year_val2", itmExtend.getProperty("in_year_val2", ""));
                item.setProperty("in_birth_val1", itmExtend.getProperty("in_birth_val1", ""));
                item.setProperty("in_birth_val2", itmExtend.getProperty("in_birth_val2", ""));
                item.setProperty("in_gender_val", itmExtend.getProperty("in_gender_val", ""));
                item.setProperty("in_degree_val", itmExtend.getProperty("in_degree_val", ""));

                itmReturn.addRelationship(item);
            }

            itmReturn.setProperty("inn_option_count", count.ToString());
        }

        private Item GetExtendInfo(TConfig cfg, string in_extend_value)
        {
            Item itmResult = cfg.inn.newItem();

            if (in_extend_value == "")
            {
                return itmResult;
            }

            string[] conditions = in_extend_value.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
            if (conditions == null || conditions.Length == 0)
            {
                return itmResult;
            }

            string limit_min = "";
            string limit_max = "";
            string year_val1 = "";
            string year_val2 = "";
            string birth_val1 = "";
            string birth_val2 = "";
            string in_gender_val = "";
            string in_degree_val = "";

            for (int i = 0; i < conditions.Length; i++)
            {
                Item itmTag = GetTagValues(cfg, conditions[i]);
                //CCO.Utilities.WriteDebug(strMethodName, itmTag.dom.InnerXml);

                string is_error = itmTag.getProperty("is_error", "0");
                string tag_name = itmTag.getProperty("tag_name", "");
                string tag_val1 = itmTag.getProperty("tag_val1", "");
                string tag_val2 = itmTag.getProperty("tag_val2", "");

                if (is_error == "1")
                {
                    continue;
                }

                switch (tag_name)
                {
                    case "in_limit":
                        limit_min = tag_val1;
                        limit_max = tag_val2;
                        break;

                    case "in_year":
                        year_val1 = tag_val1;
                        year_val2 = tag_val2;
                        break;

                    case "in_birth":
                        Item itmTag_2 = GetTagValues2(cfg, conditions[i]);
                        string is_error_2 = itmTag_2.getProperty("is_error", "0");
                        string tag_name_2 = itmTag_2.getProperty("tag_name", "");
                        string tag_val1_2 = itmTag_2.getProperty("tag_val1", "");
                        string tag_val2_2 = itmTag_2.getProperty("tag_val2", "");
                        if (is_error != "1")
                        {
                            birth_val1 = tag_val1_2;
                            birth_val2 = tag_val2_2;
                        }
                        break;

                    case "in_gender":
                        in_gender_val = tag_val1;
                        break;

                    case "in_degree":
                        in_degree_val = tag_val1;
                        break;

                    default:
                        break;
                }
            }

            itmResult.setProperty("in_limit_min", limit_min);
            itmResult.setProperty("in_limit_max", limit_max);

            itmResult.setProperty("in_year_val1", year_val1);
            itmResult.setProperty("in_year_val2", year_val2);

            itmResult.setProperty("in_birth_val1", birth_val1);
            itmResult.setProperty("in_birth_val2", birth_val2);

            itmResult.setProperty("in_gender_val", in_gender_val);
            itmResult.setProperty("in_degree_val", in_degree_val);

            return itmResult;
        }

        private Item GetTagValues(TConfig cfg, string condition)
        {
            Item itmResult = cfg.inn.newItem();
            if (condition == "")
            {
                itmResult.setProperty("is_error", "1");
                itmResult.setProperty("err_msg", "");
                return itmResult;
            }

            if (!condition.Contains(":"))
            {
                itmResult.setProperty("is_error", "1");
                itmResult.setProperty("err_msg", "無條件或條件不含分割字元");
                return itmResult;
            }

            var comment = condition.Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
            if (comment == null || comment.Length < 2)
            {
                itmResult.setProperty("is_error", "1");
                itmResult.setProperty("err_msg", "條件分割後資料錯誤");
                return itmResult;
            }

            string tag_name = comment[0];
            string tag_value = comment[1];
            if (tag_name == "" || tag_value == "")
            {
                itmResult.setProperty("is_error", "1");
                itmResult.setProperty("err_msg", "條件欄位或值錯誤");
                return itmResult;
            }

            if (!tag_value.Contains("~"))
            {
                itmResult.setProperty("tag_name", tag_name);
                itmResult.setProperty("tag_val1", tag_value);
                itmResult.setProperty("tag_val2", tag_value);
                return itmResult;
            }

            var values = tag_value.Split(new char[] { '~' }, StringSplitOptions.RemoveEmptyEntries);
            if (values == null)
            {
                itmResult.setProperty("is_error", "1");
                itmResult.setProperty("err_msg", "條件欄位值分割後資料錯誤");
            }
            else if (values.Length == 1)
            {
                itmResult.setProperty("tag_name", tag_name);
                itmResult.setProperty("tag_val1", values[0]);
                itmResult.setProperty("tag_val2", values[0]);
            }
            else if (values.Length == 2)
            {
                itmResult.setProperty("tag_name", tag_name);
                itmResult.setProperty("tag_val1", values[0]);
                itmResult.setProperty("tag_val2", values[1]);
            }
            else
            {
                itmResult.setProperty("is_error", "1");
                itmResult.setProperty("err_msg", "條件欄位值分割後資料陣列異常");
            }

            return itmResult;
        }

        private Item GetTagValues2(TConfig cfg, string condition)
        {
            Item item = cfg.inn.newItem();
            if (condition == "")
            {
                item.setProperty("is_error", "1");
                item.setProperty("err_msg", "");
                return item;
            }

            if (!condition.Contains(":"))
            {
                item.setProperty("is_error", "1");
                item.setProperty("err_msg", "無條件或條件不含分割字元");
                return item;
            }

            var comment = condition.Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
            if (comment == null || comment.Length < 2)
            {
                item.setProperty("is_error", "1");
                item.setProperty("err_msg", "條件分割後資料錯誤");
                return item;
            }

            string tag_name = comment[0];
            string tag_value = comment[1];
            if (tag_name == "" || tag_value == "")
            {
                item.setProperty("is_error", "1");
                item.setProperty("err_msg", "條件欄位或值錯誤");
                return item;
            }

            if (!tag_value.Contains("~"))
            {
                item.setProperty("tag_name", tag_name);
                item.setProperty("tag_val1", tag_value);
                item.setProperty("tag_val2", tag_value);
                return item;
            }

            var values = tag_value.Split('~');
            if (values == null || values.Length < 2)
            {
                item.setProperty("is_error", "1");
                item.setProperty("err_msg", "條件欄位值分割後資料錯誤");
            }
            else
            {
                item.setProperty("tag_name", tag_name);
                item.setProperty("tag_val1", values[0]);
                item.setProperty("tag_val2", values[1]);
            }

            return item;
        }

        //取得細項 Json
        private string GetOptionsJson(TConfig cfg, Item itmData)
        {
            string in_level_array = itmData.getProperty("in_level_array", "");
            if (in_level_array == "")
            {
                return "";
            }

            Item items = null;

            if (in_level_array.Contains("in_l3"))
            {
                items = GetOptions123(cfg, itmData);
            }
            else if (in_level_array.Contains("in_l2"))
            {
                items = GetOptions12(cfg, itmData);
            }
            else if (in_level_array.Contains("in_l1"))
            {
                items = GetOptions1(cfg, itmData);
            }

            if (items == null || items.isError() || items.getItemCount() <= 0)
            {
                return "";
            }

            List<TNode> nodes = new List<TNode>();

            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                TNode l1 = AddAndGetNode(nodes, item, "in_l1");
                TNode l2 = AddAndGetNode(l1.Nodes, item, "in_l2");
                TNode l3 = AddAndGetNode(l2.Nodes, item, "in_l3");
            }

            return Newtonsoft.Json.JsonConvert.SerializeObject(nodes);
        }

        private TNode AddAndGetNode(List<TNode> nodes, Item item, string lv)
        {
            string oid = item.getProperty(lv + "_oid", "");
            string value = item.getProperty(lv + "_value", "");

            TNode search = nodes.Find(x => x.Value == value);

            if (search == null)
            {
                search = new TNode
                {
                    Level = lv,
                    Id = oid,
                    Value = value,
                    Label = item.getProperty(lv + "_label", ""),
                    Extend = item.getProperty(lv + "_extend_value", ""),
                    Nodes = new List<TNode>()
                };
                nodes.Add(search);
            }

            return search;
        }

        private class TNode
        {
            public string Id { get; set; }
            public string Level { get; set; }
            public string Value { get; set; }
            public string Label { get; set; }
            public string Extend { get; set; }
            public List<TNode> Nodes { get; set; }
        }

        private Item GetLevelInfo(TConfig cfg)
        {
            string sql = @"
                SELECT 
                    t1.related_id
                    , t2.in_property
                    , t2.in_questions
                    , t2.in_expense
                    , t2.in_is_limit
                FROM
                    {#mtsvy_table} t1 WITH(NOLOCK)
                INNER JOIN
                    {#svy_table} t2 WITH(NOLOCK)
                    ON t2.id = t1.related_id
                WHERE
                    t1.source_id = '{#meeting_id}'
                    AND t1.in_surveytype = '1'
                    AND t2.in_property IN ('in_l1', 'in_l2', 'in_l3')
                ORDER BY
                    t2.in_property    
            ";

            sql = sql.Replace("{#mtsvy_table}", cfg.mtsvy_table)
                .Replace("{#svy_table}", cfg.svy_table)
                .Replace("{#meeting_id}", cfg.meeting_id);

            Item items = cfg.inn.applySQL(sql);

            Item itmResult = cfg.inn.newItem();

            if (IsError(items, isSingle: false))
            {
                return itmResult;
            }

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string related_id = item.getProperty("related_id", "");
                string in_property = item.getProperty("in_property", "");
                string in_questions = item.getProperty("in_questions", "");
                string in_expense = item.getProperty("in_expense", "");
                string in_is_limit = item.getProperty("in_is_limit", "");

                itmResult.setProperty(in_property + "_id", related_id);
                itmResult.setProperty(in_property + "_name", in_questions);
                itmResult.setProperty(in_property + "_is_expense", in_expense);
                itmResult.setProperty(in_property + "_is_limit", in_is_limit);
            }

            return itmResult;
        }

        private Item GetOptions1(TConfig cfg, Item itmData)
        {
            string in_l1_id = itmData.getProperty("in_l1_id", "");

            string sql = @"
                SELECT
                    t1.id                 AS 'in_l1_oid'
                    , t1.in_value         AS 'in_l1_value'
                    , t1.sort_order       AS 'in_l1_sort_order'
                    , t1.in_extend_value  AS 'in_l1_extend_value'
                FROM 
                    {#svyopt_table} t1 WITH(NOLOCK)
                WHERE
                    t1.source_id = '{#in_l1_id}'
                ORDER BY
                    t1.sort_order
            ";
            
            sql = sql.Replace("{#svyopt_table}", cfg.svyopt_table)
                .Replace("{#in_l1_id}", in_l1_id);

            return cfg.inn.applySQL(sql);
        }

        private Item GetOptions12(TConfig cfg, Item itmData)
        {
            string in_l1_id = itmData.getProperty("in_l1_id", "");
            string in_l2_id = itmData.getProperty("in_l2_id", "");

            string sql = @"
                SELECT
                    t1.id                 AS 'in_l1_oid'
                    , t1.in_value         AS 'in_l1_value'
                    , t1.sort_order       AS 'in_l1_sort_order'
                    , t1.in_extend_value  AS 'in_l1_extend_value'
                    , t2.id               AS 'in_l2_oid'
                    , t2.in_value         AS 'in_l2_value'
                    , t2.sort_order       AS 'in_l2_sort_order'
                    , t2.in_extend_value  AS 'in_l2_extend_value'
                FROM 
                    {#svyopt_table} t1 WITH(NOLOCK)
                LEFT OUTER JOIN (
                        SELECT id, in_filter, in_value, in_label, sort_order, in_expense_value, in_extend_value 
                        FROM {#svyopt_table} WITH(NOLOCK) 
                        WHERE source_id = '{#in_l2_id}'
                    ) t2 ON t2.in_filter = t1.in_value
                WHERE
                    t1.source_id = '{#in_l1_id}'
                ORDER BY
                    t1.sort_order
                    , t2.sort_order
            ";
            
            sql = sql.Replace("{#svyopt_table}", cfg.svyopt_table)
                .Replace("{#in_l1_id}", in_l1_id)
                .Replace("{#in_l2_id}", in_l2_id);

            return cfg.inn.applySQL(sql);
        }

        private Item GetOptions123(TConfig cfg, Item itmData)
        {
            string in_l1_id = itmData.getProperty("in_l1_id", "");
            string in_l2_id = itmData.getProperty("in_l2_id", "");
            string in_l3_id = itmData.getProperty("in_l3_id", "");

            string sql = @"
                SELECT
                    t1.id                 AS 'in_l1_oid'
                    , t1.in_value         AS 'in_l1_value'
                    , t1.sort_order       AS 'in_l1_sort_order'
                    , t1.in_extend_value  AS 'in_l1_extend_value'
                    , t2.id               AS 'in_l2_oid'
                    , t2.in_value         AS 'in_l2_value'
                    , t2.sort_order       AS 'in_l2_sort_order'
                    , t2.in_extend_value  AS 'in_l2_extend_value'
                    , t3.id               AS 'in_l3_oid'
                    , t3.in_value         AS 'in_l3_value'
                    , t3.sort_order       AS 'in_l3_sort_order'
                    , t3.in_extend_value  AS 'in_l3_extend_value'
                FROM 
                    {#svyopt_table} t1 WITH(NOLOCK)
                LEFT OUTER JOIN (
                    SELECT id, in_filter, in_value, in_label, sort_order, in_expense_value, in_extend_value FROM {#svyopt_table} WITH(NOLOCK) WHERE source_id = '{#in_l2_id}'
                    ) t2 ON t2.in_filter = t1.in_value
                LEFT OUTER JOIN (
                    SELECT id, in_filter, in_value, in_label, sort_order, in_expense_value, in_extend_value FROM {#svyopt_table} WITH(NOLOCK) WHERE source_id = '{#in_l3_id}'
                    ) t3 ON t3.in_filter = t2.in_value
                WHERE
                    t1.source_id = '{#in_l1_id}'
                ORDER BY
                    t1.sort_order
                    , t2.sort_order
                    , t3.sort_order
                ";

            sql = sql.Replace("{#svyopt_table}", cfg.svyopt_table)
                .Replace("{#in_l1_id}", in_l1_id)
                .Replace("{#in_l2_id}", in_l2_id)
                .Replace("{#in_l3_id}", in_l3_id);

            return cfg.inn.applySQL(sql);
        }

        //是否錯誤
        private bool IsError(Item item, bool isSingle = true)
        {
            if (item.isError() || item.getResult() == "")
            {
                return true;
            }
            else if (isSingle && item.getItemCount() != 1)
            {
                return true;
            }
            else if (item.getItemCount() <= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }

            public string meeting_id { get; set; }
            public string option_id { get; set; }
            public string mode { get; set; }

            public string mt_table { get; set; }
            public string mtsvy_table { get; set; }
            public string svy_table { get; set; }
            public string svyopt_table { get; set; }

        }
    }
}