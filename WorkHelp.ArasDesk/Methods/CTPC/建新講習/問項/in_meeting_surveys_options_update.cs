﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.CTPC.Common
{
    public class in_meeting_surveys_options_update : Item
    {
        public in_meeting_surveys_options_update(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 修改活動問項細項
                輸入: 
                    1. id (In_Survey_Option id)
                修改: 
                    2020-08-14 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]in_meeting_surveys_options_update";

            string sql = "";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "input dom: " + itmR.dom.InnerXml);

            string id = itmR.getProperty("id", "").Trim();
            string source_id = itmR.getProperty("source_id", "").Trim();
            string exe_type = itmR.getProperty("exe_type", "").Trim();

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                strIdentityId = inn.getUserAliases(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                mode = itmR.getProperty("mode", ""),
            };

            cfg.mtsvy_table = "IN_MEETING";
            cfg.mtsvy_table = "IN_MEETING_SURVEYS";
            cfg.svy_table = "IN_SURVEY";
            cfg.svyopt_table = "IN_SURVEY_OPTION";
            if (cfg.mode == "cla")
            {
                cfg.mt_table = "IN_CLA_MEETING";
                cfg.mtsvy_table = "IN_CLA_MEETING_SURVEYS";
                cfg.svy_table = "IN_CLA_SURVEY";
                cfg.svyopt_table = "IN_CLA_SURVEY_OPTION";
            }

            if (source_id == "")
            {
                throw new Exception("問卷 id 不得為空白");
            }
            
            Item itmSurvey = inn.applySQL("SELECT * FROM " + cfg.svy_table + " WITH(NOLOCK) WHERE id = '" + source_id + "'");
            if (itmSurvey.isError())
            {
                throw new Exception("取得問卷資訊發生錯誤");
            }

            string in_property = itmSurvey.getProperty("in_property", "");
            string in_filter_by = itmSurvey.getProperty("in_property", "");

            string in_filter = itmR.getProperty("in_filter", "");
            string in_grand_filter = itmR.getProperty("in_grand_filter", "");

            if (in_property == "in_l3" && in_filter != "" && in_grand_filter == "")
            {
                Item itmParentOption = GetParentOption(cfg, in_filter_by, in_filter);
                if (!itmParentOption.isError() && itmParentOption.getItemCount() > 0)
                {
                    in_grand_filter = itmParentOption.getProperty("in_filter", "");
                    itmR.setProperty("in_grand_filter", in_grand_filter);
                }
            }

            Item itmData = null;
            switch (exe_type)
            {
                case "simple":
                    itmData = SimpleOperation(cfg, itmR);
                    break;

                default:
                    itmData = ExtendOperation(cfg, itmR);
                    break;
            }

            if (id == "")
            {
                CreateOption(cfg, itmData);
                itmR.setProperty("new_id", itmData.getProperty("new_id", ""));
            }
            else
            {
                UpdateOption(cfg, itmData);
            }

            string not_update_survey = itmData.getProperty("not_update_survey", "");

            if (not_update_survey != "1")
            {
                UpdateSurvey(cfg, itmR);
            }

            return itmR;
        }

        //不包含費用、擴充欄位的更新
        private Item SimpleOperation(TConfig cfg, Item itmReturn)
        {
            string id = itmReturn.getProperty("id", "");
            string source_id = itmReturn.getProperty("source_id", "").Trim();

            string in_sort_order = itmReturn.getProperty("sort_order", "").ToLower().Trim();
            string in_label = itmReturn.getProperty("in_label", "").Trim();
            string in_value = itmReturn.getProperty("in_value", "").Trim();
            string in_filter = itmReturn.getProperty("in_filter", "").Trim();
            string in_grand_filter = itmReturn.getProperty("in_grand_filter", "").Trim();
            if (in_label == "") in_label = in_value;

            #region check

            if (in_sort_order == ""
                || in_label == ""
                || in_value == "")
            {
                throw new Exception("參數 不得為空白");
            }

            int sort_order = 0;
            if (!Int32.TryParse(in_sort_order, out sort_order))
            {
                throw new Exception("排序 必須為整數");
            }

            #endregion check

            Item itmData = cfg.inn.newItem();
            itmData.setProperty("id", id);
            itmData.setProperty("source_id", source_id);
            itmData.setProperty("sort_order", in_sort_order);
            itmData.setProperty("in_label", in_label);
            itmData.setProperty("in_value", in_value);
            itmData.setProperty("in_filter", in_filter);
            itmData.setProperty("in_grand_filter", in_grand_filter);

            return itmData;
        }

        //包含費用、擴充欄位的更新
        private Item ExtendOperation(TConfig cfg, Item itmReturn)
        {
            string id = itmReturn.getProperty("id", "");
            string source_id = itmReturn.getProperty("source_id", "").Trim();

            string in_sort_order = itmReturn.getProperty("sort_order", "").ToLower().Trim();
            string in_label = itmReturn.getProperty("in_label", "").Trim();
            string in_value = itmReturn.getProperty("in_value", "").Trim();
            string in_expense_value = itmReturn.getProperty("in_expense_value", "").Trim();
            string in_limit_min = itmReturn.getProperty("in_limit_min", "").ToLower().Trim();
            string in_limit_max = itmReturn.getProperty("in_limit_max", "").ToLower().Trim();

            //西元出生年-區間
            string in_year_val1 = itmReturn.getProperty("in_year_val1", "").ToLower().Trim();
            string in_year_val2 = itmReturn.getProperty("in_year_val2", "").ToLower().Trim();

            //西元年月日-區間
            string in_birth_val1 = itmReturn.getProperty("in_birth_val1", "").ToLower().Trim().Replace("/", "-");
            string in_birth_val2 = itmReturn.getProperty("in_birth_val2", "").ToLower().Trim().Replace("/", "-");

            string in_gender_val = itmReturn.getProperty("in_gender_val", "").ToLower().Trim();
            string in_degree_val = itmReturn.getProperty("in_degree_val", "").ToLower().Trim();

            string in_filter = itmReturn.getProperty("in_filter", "").ToLower().Trim();
            string in_grand_filter = itmReturn.getProperty("in_grand_filter", "").ToLower().Trim();

            string expense_hide = itmReturn.getProperty("expense_hide", "").Trim();
            string limit_hide = itmReturn.getProperty("limit_hide", "").Trim();

            #region check

            int sort_order = 0;
            if (in_sort_order == "")
            {
                throw new Exception("排序 不得為空白");
            }
            else if (!Int32.TryParse(in_sort_order, out sort_order))
            {
                throw new Exception("排序 必須為整數");
            }

            if (in_label == "")
            {
                throw new Exception("標籤 不得為空白");
            }

            if (in_value == "")
            {
                throw new Exception("值 不得為空白");
            }

            int expense_value = 0;
            if (expense_hide == "")
            {
                if (in_expense_value == "")
                {
                    throw new Exception("費用 不得為空白");
                }
                else if (!Int32.TryParse(in_expense_value, out expense_value))
                {
                    throw new Exception("費用 必須為整數");
                }
                else if (expense_value < 0)
                {
                    throw new Exception("費用 必須為正整數");
                }
            }

            int limit_min = 0;
            int limit_max = 0;
            if (limit_hide == "")
            {
                if (in_limit_min == "" || in_limit_max == "")
                {
                    throw new Exception("報名人數 不得為空白");
                }
                else if (!Int32.TryParse(in_limit_min, out limit_min))
                {
                    throw new Exception("報名人數 必須為整數");
                }
                else if (!Int32.TryParse(in_limit_max, out limit_max))
                {
                    throw new Exception("報名人數 必須為整數");
                }
            }

            int year_val1 = 0;
            if (in_year_val1 != "")
            {
                if (!Int32.TryParse(in_year_val1, out year_val1))
                {
                    throw new Exception("西元年區間 必須為整數");
                }
            }

            int year_val2 = 0;
            if (in_year_val2 != "" && in_year_val2 != "after" && in_year_val2 != "before")
            {
                if (!Int32.TryParse(in_year_val2, out year_val2))
                {
                    throw new Exception("西元年區間 必須為整數");
                }
            }

            DateTime birth_val1 = DateTime.MinValue;
            if (in_birth_val1 != "")
            {
                if (!DateTime.TryParse(in_birth_val1, out birth_val1))
                {
                    throw new Exception("西元年月日(起) 必須為日期格式");
                }
            }

            DateTime birth_val2 = DateTime.MinValue;
            if (in_birth_val2 != "")
            {
                if (!DateTime.TryParse(in_birth_val2, out birth_val2))
                {
                    throw new Exception("西元年月日(迄) 必須為日期格式");
                }
            }

            #endregion check

            TYear year_entity = new TYear
            {
                in_year_val1 = in_year_val1,
                in_year_val2 = in_year_val2,
                year_val1 = year_val1,
                year_val2 = year_val2,
            };

            TBirth birth_entity = new TBirth
            {
                in_birth_val1 = in_birth_val1,
                in_birth_val2 = in_birth_val2,
                birth_val1 = birth_val1,
                birth_val2 = birth_val2,
            };

            string in_limit = GetLimitCondition(limit_min, limit_max);
            string in_year = GetYearCondition(year_entity);
            string in_birth = GetBirthCondition(birth_entity);
            string in_gender = GetGenderCondition("in_gender", in_gender_val);
            string in_degree = GetGenderCondition("in_degree", in_degree_val);

            string in_extend_value = string.Join("|", new string[]
            {
                in_limit,
                in_year,
                in_birth,
                in_gender,
                in_degree,
            }.Where(s => !string.IsNullOrEmpty(s)));

            Item itmData = cfg.inn.newItem();
            itmData.setProperty("id", id);
            itmData.setProperty("source_id", source_id);
            itmData.setProperty("sort_order", in_sort_order);
            itmData.setProperty("in_label", in_label);
            itmData.setProperty("in_value", in_value);
            itmData.setProperty("in_filter", in_filter);
            itmData.setProperty("in_grand_filter", in_grand_filter);
            itmData.setProperty("in_expense_value", in_expense_value);
            itmData.setProperty("in_extend_value", in_extend_value);
            return itmData;
        }

        private string GetLimitCondition(int limit_min, int limit_max)
        {
            string in_limit = "";
            if (limit_min > 0 && limit_min > 0)
            {
                in_limit = "in_limit:" + limit_min.ToString() + "~" + limit_max.ToString();
                if (limit_min > limit_max)
                {
                    in_limit = "in_limit:" + limit_max.ToString() + "~" + limit_min.ToString();
                }
            }
            return in_limit;
        }

        private string GetYearCondition(TYear entity)
        {
            string in_year = "";
            if (entity.year_val1 > 0)
            {
                if (entity.in_year_val2 == "")
                {
                    in_year = "in_year:" + entity.in_year_val1 + "~" + entity.in_year_val1;
                }
                else if (entity.in_year_val2 == "after" || entity.in_year_val2 == "before")
                {
                    in_year = "in_year:" + entity.in_year_val1 + "~" + entity.in_year_val2;
                }
                else if (entity.year_val1 > entity.year_val2)
                {
                    in_year = "in_year:" + entity.in_year_val2 + "~" + entity.in_year_val1;
                }
                else
                {
                    in_year = "in_year:" + entity.in_year_val1 + "~" + entity.in_year_val2;
                }
            }
            else if (entity.year_val2 > 0)
            {
                in_year = "in_year:" + entity.in_year_val2 + "~" + entity.in_year_val2;
            }

            return in_year;
        }

        private string GetBirthCondition(TBirth entity)
        {
            string format = "yyyy/MM/dd";
            string in_birth = "";

            if (entity.in_birth_val1 != "" && entity.in_birth_val2 != "")
            {
                in_birth = "in_birth:" + entity.birth_val1.ToString(format) + "~" + entity.birth_val2.ToString(format);
            }
            else if (entity.in_birth_val1 != "")
            {
                in_birth = "in_birth:" + entity.birth_val1.ToString(format) + "~";
            }
            else if (entity.in_birth_val2 != "")
            {
                in_birth = "in_birth:" + "~" + entity.birth_val2.ToString(format) + "~";
            }

            return in_birth;
        }

        private string GetGenderCondition(string property, string value)
        {
            if (value == "")
            {
                return "";
            }
            else
            {
                return property + ":" + value;
            }
        }

        private class TYear
        {
            public string in_year_val1 { get; set; }
            public string in_year_val2 { get; set; }
            public int year_val1 { get; set; }
            public int year_val2 { get; set; }
        }

        private class TBirth
        {
            public string in_birth_val1 { get; set; }
            public string in_birth_val2 { get; set; }
            public DateTime birth_val1 { get; set; }
            public DateTime birth_val2 { get; set; }
        }

        private void CreateOption(TConfig cfg, Item itmData)
        {
            Item itmOption = cfg.inn.newItem(cfg.svyopt_table, "add");
            itmOption.setProperty("source_id", itmData.getProperty("source_id", ""));
            itmOption.setProperty("sort_order", itmData.getProperty("sort_order", ""));

            itmOption.setProperty("in_value", itmData.getProperty("in_value", ""));
            itmOption.setProperty("in_label", itmData.getProperty("in_label", ""));
            itmOption.setProperty("in_expense_value", itmData.getProperty("in_expense_value", ""));
            itmOption.setProperty("in_extend_value", itmData.getProperty("in_extend_value", ""));
            itmOption.setProperty("in_filter", itmData.getProperty("in_filter", ""));
            itmOption.setProperty("in_grand_filter", itmData.getProperty("in_grand_filter", ""));
            itmOption = itmOption.apply();

            if (itmOption.isError())
            {
                throw new Exception("細項新增失敗");
            }

            itmData.setProperty("new_id", itmOption.getID());
        }

        private void UpdateOption(TConfig cfg, Item itmData)
        {
            string id = itmData.getProperty("id", "");

            string sql = "SELECT * FROM " + cfg.svyopt_table + " WITH(NOLOCK) WHERE id = '" + id + "'";
            Item itmOption = cfg.inn.applySQL(sql);
            if (IsError(itmOption))
            {
                throw new Exception("細項不存在");
            }

            sql = @"
                UPDATE 
                    {#table} 
                SET 
                    sort_order = '{#sort_order}'
                    , in_label = N'{#in_label}' 
                    , in_value = N'{#in_value}' 
                    , in_expense_value = N'{#in_expense_value}' 
                    , in_extend_value = N'{#in_extend_value}' 
                    , in_filter = N'{#in_filter}' 
                    , in_grand_filter = N'{#in_grand_filter}' 
                WHERE 
                    id = '{#id}'
            ";

            sql = sql.Replace("{#table}", cfg.svyopt_table)
                .Replace("{#id}", id)
                .Replace("{#sort_order}", itmData.getProperty("sort_order", ""))
                .Replace("{#in_label}", itmData.getProperty("in_label", ""))
                .Replace("{#in_value}", itmData.getProperty("in_value", ""))
                .Replace("{#in_expense_value}", itmData.getProperty("in_expense_value", ""))
                .Replace("{#in_extend_value}", itmData.getProperty("in_extend_value", ""))
                .Replace("{#in_filter}", itmData.getProperty("in_filter", ""))
                .Replace("{#in_grand_filter}", itmData.getProperty("in_grand_filter", ""));

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item itmUpdate = cfg.inn.applySQL(sql);

            if (itmUpdate.isError())
            {
                throw new Exception("細項修改失敗");
            }

            //是否需更新問項的選項欄位
            string old_in_value = itmOption.getProperty("in_value", "");
            string new_in_value = itmData.getProperty("in_value", "");

            if (new_in_value == old_in_value)
            {
                itmData.setProperty("not_update_survey", "1");
            }
        }

        //更新問項
        private void UpdateSurvey(TConfig cfg, Item itmReturn)
        {
            string sql = "";
            string source_id = itmReturn.getProperty("source_id", "");

            sql = "SELECT * FROM " + cfg.svy_table + " WITH(NOLOCK) WHERE id = '" + source_id + "'";
            Item itmSurvey = cfg.inn.applySQL(sql);
            if (IsError(itmSurvey, isSingle: true))
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "無法取得問項 _# sql: " + sql);
            }

            string in_property = itmSurvey.getProperty("in_property", "");
            string in_question_type = itmSurvey.getProperty("in_question_type", "");

            if (in_property == "in_l2" || in_property == "in_l3")
            {
                return;
            }

            if (in_question_type == "date" || in_question_type == "single_text" || in_question_type == "single_value")
            {
                return;
            }

            sql = "SELECT in_label FROM " + cfg.svyopt_table + " WITH(NOLOCK) WHERE source_id = '" + source_id + "' ORDER BY sort_order";
            Item items = cfg.inn.applySQL(sql);
            if (IsError(items, isSingle: false))
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "無法取得同系列選項 _# sql: " + sql);
            }

            string in_selectoption = "";
            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string option_label = item.getProperty("in_label", "");
                if (option_label != "")
                {
                    in_selectoption += "@" + option_label;
                }
            }

            if (in_selectoption == "")
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "選項標籤錯誤 _# sql: " + sql);
            }

            sql = "UPDATE " + cfg.svy_table + " SET in_selectoption = N'" + in_selectoption + "' WHERE id = '" + source_id + "'";
            Item itmUpdate = cfg.inn.applySQL(sql);
            if (itmUpdate.isError())
            {
                throw new Exception("問項的選項欄位修改失敗");
            }
        }

        //取得父階 In_Survey_Option
        private Item GetParentOption(TConfig cfg, string parent_item_number, string parent_value)
        {
            string sql = "";

            sql = @"
                SELECT 
                    * 
                FROM 
                    {#svyopt_table} WITH(NOLOCK)
                WHERE 
                    source_id = (
                        SELECT TOP 1 id FROM {#svy_table} WITH(NOLOCK) WHERE item_number = '{#item_number}'
                    )
                    AND in_value = N'{#in_value}'
            ";

            sql = sql.Replace("{#svy_table}", cfg.svy_table)
                .Replace("{#svyopt_table}", cfg.svyopt_table)
                .Replace("{#item_number}", parent_item_number)
                .Replace("{#in_value}", parent_value);

            return cfg.inn.applySQL(sql);
        }

        //是否錯誤
        private bool IsError(Item item, bool isSingle = true)
        {
            if (item.isError() || item.getResult() == "")
            {
                return true;
            }
            else if (isSingle && item.getItemCount() != 1)
            {
                return true;
            }
            else if (item.getItemCount() <= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }

            public string meeting_id { get; set; }
            public string mode { get; set; }

            public string mt_table { get; set; }
            public string mtsvy_table { get; set; }
            public string svy_table { get; set; }
            public string svyopt_table { get; set; }

        }
    }
}