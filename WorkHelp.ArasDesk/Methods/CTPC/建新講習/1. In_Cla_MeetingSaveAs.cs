﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.CTPC.Common
{
    public class In_Cla_MeetingSaveAs : Item
    {
        public In_Cla_MeetingSaveAs(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                SaveType:
                另存為1
                建立子場為2

                1.取得含Relationship的Meeting物件並clone出另一個meeting物件(CloneMeetings)
                2.根據RelClrList將CloneMeetings中的Relationship清除
                3.根據PropertyClrList將CloneMeetings的表頭property做更新

                2019-05-11 創建學員自動編碼字頭
                目標:
                1.若為單日課程,則字頭為101開頭,若本堂課是當天第1堂課,則為 101,第二堂課則為102,以此類推
                2.若為多日課程,則字頭為201開頭

            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
            
            string strIdentityid = this.getProperty("owner", inn.getUserAliases());
            string Meetingid = this.getProperty("meetingid", "");
            string SaveType = this.getProperty("savetype", "");

            Item LoginUserResume = inn.newItem("In_Resume", "get");
            LoginUserResume.setProperty("owned_by_id", strIdentityid);
            LoginUserResume = LoginUserResume.apply();

            DateTime CurrentTime = System.DateTime.Today;

            //清除關聯類型
            List<string> RelClrList = new List<string>
            { 
                "In_Cla_Meeting_Surveys_Statistics", 
                "In_Cla_Meeting_Surveys_Result", 
                "In_Cla_Meeting_Record", 
                "In_Cla_Meeting_Resume", 
                "In_Cla_Meeting_User", 
                "In_Cla_Meeting_Resumelist",
                "In_Cla_Meeting_Staff",
                "In_Cla_Meeting_Technical",
                "In_Cla_Meeting_Promotion",
            };

            //清除 Property
            List<string> PropertyClrList = new List<string>
            { 
                "in_contract", 
                "in_mail", 
                "in_tel", 
                "in_is_template", 
                "in_close_msg", 
                "in_real_taking", 
                "in_real_prepare", 
                "in_isfull", 
                "in_title", 
                "owned_by_id", 
                "in_refmeeting", 
                "in_state_start", 
                "in_state_class", 
                "in_state_end", 
                "in_state_issued",
                "in_state_review", 
                "in_date_s", 
                "in_date_e", 
                "in_main" 
            };

            Item Meeting = inn.newItem("In_Cla_Meeting", "get");
            Meeting.setAttribute("id", Meetingid);
            Meeting.setAttribute("levels", "1");
            Meeting = Meeting.apply();
            Item CloneMeetings = Meeting.clone(true);

            Item itmUser = inn.getItemById("User", inn.getUserID());
            string strUser_name = itmUser.getProperty("last_name", "");
            string strUser_email = itmUser.getProperty("email", "");
            string strUser_cell = itmUser.getProperty("cell", "");


            for (int i = 0; i < RelClrList.Count; i++)
            {
                var rel = RelClrList[i];
                Item itmCloneMeetingRels = (CloneMeetings.fetchRelationships(rel)).getRelationships(rel);

                for (int j = 0; j < itmCloneMeetingRels.getItemCount(); j++)
                {
                    Item itmCloneMeetingRel = itmCloneMeetingRels.getItemByIndex(j);
                    CloneMeetings.removeRelationship(itmCloneMeetingRel);
                }
            }

            for (int i = 0; i < PropertyClrList.Count; i++)
            {
                var prop = PropertyClrList[i];
                CloneMeetings.removeProperty(prop);
                switch (prop)
                {
                    case "in_is_main":
                        CloneMeetings.setProperty(prop, "0");
                        break;
                    case "in_date_s":
                        CloneMeetings.setProperty(prop, (new DateTime(CurrentTime.Year, CurrentTime.Month, CurrentTime.Day, 8, 0, 0, 0)).ToString("yyyy-MM-ddTHH:mm:ss"));
                        break;
                    case "in_date_e":
                        CloneMeetings.setProperty(prop, (new DateTime(CurrentTime.Year, CurrentTime.Month, CurrentTime.Day, 17, 0, 0, 0)).ToString("yyyy-MM-ddTHH:mm:ss"));
                        break;
                    case "in_contract":
                        CloneMeetings.setProperty(prop, strUser_name);
                        break;
                    case "in_mail":
                        CloneMeetings.setProperty(prop, strUser_email);
                        break;
                    case "in_tel":
                        CloneMeetings.setProperty(prop, strUser_cell);
                        break;
                    case "in_close_msg":
                        CloneMeetings.setProperty(prop, "開放報名中");
                        break;

                    case "in_real_taking":
                    case "in_real_prepare":
                    case "in_isfull":
                        CloneMeetings.setProperty(prop, "0");
                        break;

                    case "in_title":
                        string MyTitle = Meeting.getProperty(prop, "no_title");//因為clone出的Item中in_title沒有被複製到所以直接取Meeting物件的in_title
                        CloneMeetings.setProperty(prop, "複製-" + MyTitle);
                        break;

                    case "owned_by_id":
                        CloneMeetings.setProperty(prop, strIdentityid);
                        break;

                    case "in_refmeeting":
                        if (SaveType == "1")
                        {
                            CloneMeetings.setProperty(prop, "");//代表是做另存
                        }
                        else if (SaveType == "2")
                        {
                            CloneMeetings.setProperty(prop, Meeting.getAttribute("id", ""));//代表是做建立子場
                        }
                        break;

                    case "in_is_template":
                    case "IN_STATE_START":
                    case "in_state_class":
                    case "in_state_end":
                    case "in_state_issued":
                    case "in_state_review":
                        CloneMeetings.setProperty(prop, "");
                        break;
                }
            }

            //執行複製
            CloneMeetings = CloneMeetings.apply();

            //建立完會議後把此會議的建立時間寫到進行日
            string MyCreatedOn = CloneMeetings.getProperty("created_on", " ");
            MyCreatedOn = Convert.ToDateTime(MyCreatedOn).AddHours(-8).ToString("yyyy-MM-ddTHH:mm:ss");

            string cloneMeetingid = CloneMeetings.getID();

            string sql = "UPDATE IN_CLA_MEETING SET in_state_start = '{#created_on}' WHERE id = '{#id}'";
            
            sql = sql.Replace("{#created_on}", MyCreatedOn)
                .Replace("{#id}", cloneMeetingid);
            
            Item itmUpd = inn.applySQL(sql);
            
            CloneMeetings.setProperty("in_state_start", MyCreatedOn);

            //建立完會議後在共同講師建立一筆單場負責人的資料
            Item itmResumeList = inn.newItem("In_Cla_Meeting_Resumelist", "add");
            itmResumeList.setProperty("source_id", CloneMeetings.getID());
            itmResumeList.setRelatedItem(LoginUserResume);
            itmResumeList = itmResumeList.apply();



            //lina 2022.05.31: start
            string aml = ""
                + "<meeting_id>" + CloneMeetings.getProperty("id", "") + "</meeting_id>"
                + "<item_number>" + CloneMeetings.getProperty("item_number", "") + "</item_number>"
                + "<hideselector>" + "1" + "</hideselector>"
                + "<in_clone>" + "1" + "</in_clone>"
                + "<mode>" + "cla" + "</mode>"
            + "";

            Item itmSurveys = inn.applyMethod("In_MeetingSaveAs_Survey", aml);
            //lina 2022.05.31: end

            return CloneMeetings;




        }
    }
}