﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.CTPC.Common
{
    public class In_Cla_MeetingSaveAs_UpdSurvey : Item
    {
        public In_Cla_MeetingSaveAs_UpdSurvey(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 動態調整項目
                說明:
                    - this: In_Cla_Meeting
                日誌: 
                    - 2022-05-31: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]" + "In_Cla_MeetingSaveAs_UpdSurvey";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                strIdentityId = inn.getUserAliases(),

                meeting_id = itmR.getProperty("id", ""),
                in_seminar_type = itmR.getProperty("in_seminar_type", ""),
                in_sport_type = itmR.getProperty("in_sport_type", ""),
                in_level = itmR.getProperty("in_level", ""),
                in_date_s = itmR.getProperty("in_date_s", ""),
            };

            //附加講習費用設定
            AppendSeminarItem(cfg);

            cfg.date_s = GetDateTime(cfg.in_date_s, 8).Date;

            var mt = MapMeeting(cfg);

            mt.itmSvyL1 = SurveyL1(cfg);
            if (mt.itmSvyL1.isError() || mt.itmSvyL1.getResult() == "")
            {
                return itmR;
            }

            mt.svy_id_l1 = mt.itmSvyL1.getProperty("id", "");
            mt.svy_questions_l1 = mt.itmSvyL1.getProperty("in_questions", "");
            mt.svy_selectoption_l1 = mt.itmSvyL1.getProperty("in_selectoption", "");

            if (mt.svy_selectoption_l1 == "" || !mt.svy_selectoption_l1.ToLower().Contains("copy of"))
            {
                return itmR;
            }

            mt.itmSvyL1Opts = SvyL1Opts(cfg, mt);
            if (mt.itmSvyL1Opts.isError() || mt.itmSvyL1Opts.getResult() == "")
            {
                return itmR;
            }

            //刷新問項
            RefreshSurvey(cfg, mt);

            //刷新選項時程
            RefreshFuncTime(cfg, mt);

            //刷新證照控管
            RefreshCert(cfg, mt);

            //刷新活動時數
            UpdMeetingHours(cfg);

            return itmR;
        }

        //刷新活動時數
        private void UpdMeetingHours(TConfig cfg)
        {
            string sql = @"
                UPDATE t1 SET
                	t1.in_coursh_hours = t2.in_hours
                FROM
                	IN_CLA_MEETING t1 WITH(NOLOCK)
                INNER JOIN
                	IN_CLA_SEMINAR t2 WITH(NOLOCK)
                	ON ISNULL(t2.in_seminar_type, '') = ISNULL(t1.in_seminar_type, '')
                	AND ISNULL(t2.in_level, '') = ISNULL(t1.in_level, '')
                WHERE
                	t1.id = '{#meeting_id}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            Item itmResult = cfg.inn.applySQL(sql);
        }

        private void AppendSeminarItem(TConfig cfg)
        {
            string sql = "SELECT * FROM In_Cla_Seminar WITH(NOLOCK)"
                + " WHERE ISNULL(in_seminar_type, '') = '" + cfg.in_seminar_type + "'"
                + " AND ISNULL(in_level, '') = '" + cfg.in_level + "'";

            Item itmSeminar = cfg.inn.applySQL(sql);
            if (itmSeminar.isError() || itmSeminar.getResult() == "")
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "itmSeminar. is error _# sql: " + sql);

                itmSeminar = cfg.inn.newItem();
                itmSeminar.setProperty("in_days", "4");
                itmSeminar.setProperty("in_hours", "32");
                itmSeminar.setProperty("in_expense1", "4000");
                itmSeminar.setProperty("in_expense2", "2500");
                itmSeminar.setProperty("in_suffix1", "");
            }

            cfg.itmSeminar = itmSeminar;

            sql = "SELECT * FROM In_Cla_Seminar_Certificate WITH(NOLOCK)"
                + " WHERE source_id = '" + cfg.itmSeminar.getProperty("id", "") + "'"
                + " ORDER BY sort_order";

            cfg.itmSeminarCerts = cfg.inn.applySQL(sql);
        }

        //刷新問項
        private void RefreshSurvey(TConfig cfg, TMeeting mt)
        {
            string sql = "";

            //更新 in_cert_yn、in_cert_id 問項標題
            sql = @"
                UPDATE t2 SET
                    t2.keyed_name = t2.item_number + N' {#value}'
                    , t2.in_questions = N'{#value}'
                FROM
                    IN_CLA_MEETING_SURVEYS t1
                INNER JOIN
                    IN_CLA_SURVEY t2
                    ON t2.id = t1.related_id
                WHERE
                    t1.source_id = '{#meeting_id}'
					AND t1.in_surveytype = '1'
                    AND t2.in_property = '{#property}'
            ";

            string sql_upd1 = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#property}", "in_cert_yn")
                .Replace("{#value}", mt.cert_yn_questions);

            cfg.inn.applySQL(sql_upd1);

            string sql_upd2 = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#property}", "in_cert_id")
                .Replace("{#value}", mt.cert_id_questions);

            cfg.inn.applySQL(sql_upd2);


            //更新 in_l1 問項 options (表頭)
            //string in_selectoption = "@" + mt.opts[0].value + "@" + mt.opts[1].value;
            string in_selectoption = "@" + mt.opts[0].value;
            sql = @"UPDATE IN_CLA_SURVEY SET in_selectoption = N'{#options}' WHERE id = '{#id}'";
            sql = sql.Replace("{#id}", mt.svy_id_l1)
                .Replace("{#options}", in_selectoption);
            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);
            cfg.inn.applySQL(sql);


            //更新問項值與費用
            int count = mt.itmSvyL1Opts.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item itmOpt = mt.itmSvyL1Opts.getItemByIndex(i);
                string opt_id = itmOpt.getProperty("id", "");

                sql = @"
                    UPDATE 
	                    IN_CLA_SURVEY_OPTION 
                    SET 
	                    in_value = N'{#option}'
	                    , in_label = N'{#option}' 
	                    , in_expense_value = '{#expense}'
                    WHERE 
	                    id = '{#id}'
                ";

                sql = sql.Replace("{#id}", opt_id)
                    .Replace("{#option}", mt.opts[i].value)
                    .Replace("{#expense}", mt.opts[i].amount.ToString());

                cfg.inn.applySQL(sql);
            }
        }

        //刷新選項時程
        private void RefreshFuncTime(TConfig cfg, TMeeting mt)
        {
            //簽到簽到
            string in_action = "2";

            //先刪除簽到簽到，再重建
            string sql = "DELETE FROM IN_CLA_MEETING_FUNCTIONTIME WHERE source_id = '{#meeting_id}' AND in_action = '{#in_action}'";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_action}", in_action);

            cfg.inn.applySQL(sql);

            for (var i = 1; i <= mt.sign_days; i++)
            {
                //簽到
                var sign_in = new TFunTime
                {
                    in_action = in_action,
                    in_purpose = "第" + i + "天簽到",
                    in_is_must_pass = "0",
                    in_date_s = cfg.date_s.AddDays(i - 1).ToString("yyyy-MM-dd") + "T08:00:00",
                    in_date_e = cfg.date_s.AddDays(i - 1).ToString("yyyy-MM-dd") + "T09:00:00",
                };

                //簽退
                var sign_out = new TFunTime
                {
                    in_action = in_action,
                    in_purpose = "第" + i + "天簽退",
                    in_is_must_pass = "0",
                    in_date_s = cfg.date_s.AddDays(i - 1).ToString("yyyy-MM-dd") + "T17:00:00",
                    in_date_e = cfg.date_s.AddDays(i - 1).ToString("yyyy-MM-dd") + "T18:00:00",
                };

                NewFunTime(cfg, sign_in);
                NewFunTime(cfg, sign_out);
            }
        }

        //新增選項時程
        private void NewFunTime(TConfig cfg, TFunTime obj)
        {
            Item itmNew = cfg.inn.newItem("IN_CLA_MEETING_FUNCTIONTIME", "add");
            itmNew.setProperty("source_id", cfg.meeting_id);
            itmNew.setProperty("in_action", obj.in_action);
            itmNew.setProperty("in_purpose", obj.in_purpose);
            itmNew.setProperty("in_date_s", obj.in_date_s);
            itmNew.setProperty("in_date_e", obj.in_date_e);
            itmNew.apply();
        }

        //刷新證照控管
        private void RefreshCert(TConfig cfg, TMeeting mt)
        {
            //先刪除證照清單，再重建
            string sql = "DELETE FROM IN_CLA_MEETING_CERTIFICATE WHERE source_id = '{#meeting_id}'";
            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);
            cfg.inn.applySQL(sql);

            int count = cfg.itmSeminarCerts.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var itmSrc = cfg.itmSeminarCerts.getItemByIndex(i);

                Item itmNew = cfg.inn.newItem("IN_CLA_MEETING_CERTIFICATE", "add");
                itmNew.setProperty("source_id", cfg.meeting_id);
                itmNew.setProperty("in_type", itmSrc.getProperty("in_type", ""));
                itmNew.setProperty("in_country", itmSrc.getProperty("in_country", ""));
                itmNew.setProperty("in_level", itmSrc.getProperty("in_level", ""));
                itmNew.setProperty("in_limit", itmSrc.getProperty("in_limit", ""));
                itmNew.setProperty("is_sides", itmSrc.getProperty("is_sides", ""));
                itmNew.setProperty("is_enable", itmSrc.getProperty("is_enable", ""));
                itmNew.setProperty("is_hideinfo", itmSrc.getProperty("is_hideinfo", ""));
                itmNew.setProperty("in_property", itmSrc.getProperty("in_property", ""));
                itmNew.setProperty("in_value", itmSrc.getProperty("in_value", ""));
                itmNew.setProperty("in_operator", itmSrc.getProperty("in_operator", ""));
                itmNew.setProperty("in_require", itmSrc.getProperty("in_require", ""));
                itmNew.apply();
            }
        }

        private Item SvyL1Opts(TConfig cfg, TMeeting mt)
        {
            string sql = "SELECT TOP 2 id FROM IN_CLA_SURVEY_OPTION WITH(NOLOCK) WHERE source_id = '{#source_id}' ORDER BY sort_order";
            sql = sql.Replace("{#source_id}", mt.svy_id_l1);
            return cfg.inn.applySQL(sql);
        }

        private Item SurveyL1(TConfig cfg)
        {
            string sql = @"
                SELECT TOP 1 
                    t2.id
                    , t2.in_questions
                    , t2.in_selectoption
                FROM
                    IN_CLA_MEETING_SURVEYS t1 WITH(NOLOCK)
                INNER JOIN
                    IN_CLA_SURVEY t2 WITH(NOLOCK)
                    ON t2.id = t1.related_id
                WHERE
                    t1.source_id = '{#meeting_id}'
					AND t1.in_surveytype = '1'
                    AND t2.in_property = 'in_l1'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            return cfg.inn.applySQL(sql);
        }

        //轉換資料
        private TMeeting MapMeeting(TConfig cfg)
        {
            TMeeting mt = new TMeeting
            {
                type = "",
                lv = "",
                seminar = "",
                in_expense1 = 4000,
                in_expense2 = 2500,
                sign_days = 5,
                opts = new List<TOpt>(),
                need_update = true,
            };

            if (cfg.in_seminar_type.Contains("referee_enr"))
            {
                mt.type = "裁判";
                mt.seminar = "裁判增能講習";
            }
            else if (cfg.in_seminar_type.Contains("referee"))
            {
                mt.type = "裁判";
                mt.seminar = "裁判講習";
            }
            else if (cfg.in_seminar_type.Contains("coach_enr"))
            {
                mt.type = "教練";
                mt.seminar = "教練增能講習";
            }
            else if (cfg.in_seminar_type.Contains("coach"))
            {
                mt.type = "教練";
                mt.seminar = "教練講習";
            }
            else
            {
                mt.type = "其他";
                mt.seminar = "其他";
                mt.need_update = false;
            }

            mt.sign_days = GetIntVal(cfg.itmSeminar.getProperty("in_days", "0"));
            mt.in_expense1 = GetIntVal(cfg.itmSeminar.getProperty("in_expense1", "0"));
            mt.in_expense2 = GetIntVal(cfg.itmSeminar.getProperty("in_expense2", "0"));
            mt.in_suffix1 = cfg.itmSeminar.getProperty("in_suffix1", "");
            mt.in_suffix2 = cfg.itmSeminar.getProperty("in_suffix2", "");
            if (mt.in_suffix1 != "") mt.in_suffix1 = "(" + mt.in_suffix1 + ")";
            if (mt.in_suffix2 != "") mt.in_suffix2 = "(" + mt.in_suffix2 + ")";

            if (cfg.in_level.Contains("A"))
            {
                mt.lv = "A級";
            }
            else if (cfg.in_level.Contains("B"))
            {
                mt.lv = "B級";
            }
            else if (cfg.in_level.Contains("C"))
            {
                mt.lv = "C級";
            }

            mt.cert_yn_questions = "您是否已經是" + mt.lv + mt.type;
            mt.cert_id_questions = mt.type + "證號(上題選否則免填):";

            string opt_head = cfg.in_sport_type + mt.lv + mt.seminar;

            mt.opts.Add(new TOpt { value = opt_head + mt.in_suffix1, amount = mt.in_expense1 });
            //mt.opts.Add(new TOpt { value = opt_head + mt.in_suffix2, amount = mt.in_expense2 });

            return mt;
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }

            public string meeting_id { get; set; }
            public string in_seminar_type { get; set; }
            public string in_sport_type { get; set; }
            public string in_level { get; set; }
            public string in_date_s { get; set; }

            public DateTime date_s { get; set; }
            public Item itmSeminar { get; set; }
            public Item itmSeminarCerts { get; set; }
        }

        private class TMeeting
        {
            public string type { get; set; }
            public string lv { get; set; }
            public string seminar { get; set; }
            public int in_expense1 { get; set; }
            public int in_expense2 { get; set; }
            public string in_suffix1 { get; set; }
            public string in_suffix2 { get; set; }
            public int sign_days { get; set; }
            public List<TOpt> opts { get; set; }
            public string cert_yn_questions { get; set; }
            public string cert_id_questions { get; set; }
            public bool need_update { get; set; }

            public Item itmSvyL1 { get; set; }
            public Item itmSvyL1Opts { get; set; }

            public string svy_id_l1 { get; set; }
            public string svy_questions_l1 { get; set; }
            public string svy_selectoption_l1 { get; set; }
        }

        private class TOpt
        {
            public string value { get; set; }
            public int amount { get; set; }
        }

        private class TFunTime
        {
            public string in_action { get; set; }
            public string in_purpose { get; set; }
            public string in_is_must_pass { get; set; }
            public string in_date_s { get; set; }
            public string in_date_e { get; set; }
        }

        private DateTime GetDateTime(string value, int hours)
        {
            if (value == "") return DateTime.MinValue;
            DateTime dt = DateTime.MinValue;
            DateTime.TryParse(value, out dt);
            return dt.AddHours(hours);
        }

        private int GetIntVal(string value, int def = 0)
        {
            int result = def;
            Int32.TryParse(value, out result);
            return result;
        }
    }
}