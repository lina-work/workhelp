﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.CTPC.Common
{
    public class In_Payment_List_Receipt : Item
    {
        public In_Payment_List_Receipt(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 國際版對戰表
                日誌: 
                    - 2023-06-14: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "In_Meeting_Competition_NationExport";

            var itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                meeting_id = itmR.getProperty("meeting_id", ""),
                scene = itmR.getProperty("scene", ""),
            };

            switch (cfg.scene)
            {
                case "nation_draw_sheet"://國際版對戰表
                    ExportNationDrawSheet(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private List<TProgram> MapPrograms(TConfig cfg, Item itmPrograms)
        {
            var result = new List<TProgram>();
            var count = itmPrograms.getItemCount();
            for (int i = 0; i < count; i++)
            {
                var itmProgram = itmPrograms.getItemByIndex(i);
                var program = MapProgram(cfg, itmProgram);
                program.map = MapEventList(cfg, program);
                result.Add(program);
            }
            return result;
        }

        //國際版對戰表
        private void ExportNationDrawSheet(TConfig cfg, Item itmReturn)
        {
            SetExportInof(cfg, itmReturn);

            var itmPrograms = GetProgramItems(cfg, itmReturn);
            var programs = MapPrograms(cfg, itmPrograms);

            var book = new Spire.Xls.Workbook();
            book.LoadFromFile(cfg.template_path);

            var default_sheet_count = book.Worksheets.Count;

            for (int i = 0; i < programs.Count; i++)
            {
                var program = programs[i];
                ExportXls(cfg, program, book);
            }

            if (book.Worksheets.Count == default_sheet_count)
            {
                throw new Exception("查無資料");
            }

            //移除樣板 Sheet
            var mx = default_sheet_count - 1;
            for (int i = mx; i >= 0; i--)
            {
                book.Worksheets.RemoveAt(i);
            }

            var xlsName1 = cfg.mt_title + "_DrawSheet_Step1_" + DateTime.Now.ToString("MMdd_HHmmss");
            var xlsFile1 = cfg.export_path + xlsName1 + ".xlsx";
            book.SaveToFile(xlsFile1);

            //由於 OvalShapes CopyFrom 時期無法複製，因此增加中繼檔處理
            ResetSheetsForTreeNo(cfg, xlsFile1, programs, itmReturn);
        }

        private void ResetSheetsForTreeNo(TConfig cfg, string fileSource, List<TProgram> programs, Item itmReturn)
        {
            var book = new Spire.Xls.Workbook();
            book.LoadFromFile(fileSource);

            for (int i = 0; i < programs.Count; i++)
            {
                var program = programs[i];
                var sheet = book.Worksheets[program.in_short_name];

                switch (program.sheetKey)
                {
                    case "8":
                        program.LeftColumnOffset = 50;
                        program.TopRowOffset = 75;
                        ResetPlayers08(cfg, sheet, program, program.map);
                        break;

                    case "16":
                        program.LeftColumnOffset = 50;
                        program.TopRowOffset = 50;
                        ResetPlayers16(cfg, sheet, program, program.map);
                        break;

                    case "32":
                        program.LeftColumnOffset = 50;
                        program.TopRowOffset = 25;
                        ResetPlayers32(cfg, sheet, program, program.map);
                        break;
                }
            }

            //適合頁面
            book.ConverterSetting.SheetFitToPage = true;

            var xlsName2 = cfg.mt_title + "_DrawSheet_" + DateTime.Now.ToString("MMdd_HHmmss");
            var xlsFile2 = cfg.export_path + xlsName2 + "." + cfg.export_type;

            if (cfg.export_type == "pdf")
            {
                book.SaveToFile(xlsFile2, Spire.Xls.FileFormat.PDF);
            }
            else
            {
                book.SaveToFile(xlsFile2, Spire.Xls.ExcelVersion.Version2013);
            }

            itmReturn.setProperty("xls_name", xlsName2 + "." + cfg.export_type);
        }

        private void ExportXls(TConfig cfg, TProgram program, Spire.Xls.Workbook book)
        {
            if (program.map == null || program.map.Count == 0) return;

            var sheetTemplate = book.Worksheets[program.sheetKey];

            var sheet = book.CreateEmptySheet();
            sheet.CopyFrom(sheetTemplate);
            sheet.Name = program.in_short_name;

            sheet.PageSetup.FitToPagesWide = 1;
            sheet.PageSetup.FitToPagesTall = 1;
            sheet.PageSetup.TopMargin = 0.3;
            sheet.PageSetup.LeftMargin = 0.5;
            sheet.PageSetup.RightMargin = 0.5;
            sheet.PageSetup.BottomMargin = 0.5;

            switch (program.sheetKey)
            {
                case "8":
                    ExportPlayers08(cfg, sheet, program, program.map);
                    break;

                case "16":
                    ExportPlayers16(cfg, sheet, program, program.map);
                    break;

                case "32":
                    ExportPlayers32(cfg, sheet, program, program.map);
                    break;
            }
        }

        private void ExportPlayers08(TConfig cfg, Spire.Xls.Worksheet sheet, TProgram program, Dictionary<string, TEvt> map)
        {
            //活動標題
            sheet["A1"].Text = cfg.mt_title;
            //級組
            sheet["G3"].Text = program.in_l2 + " " + program.in_l3.Replace(" ", "");
            //抽籤日期
            sheet["L3"].Text = program.signTime;

            var ridx = 6;
            for (var i = 1; i <= 4; i++)
            {
                var key = "M008-" + i.ToString().PadLeft(2, '0');
                var evt = FindEvt(cfg, map, key);

                var r1 = ridx + 0;
                var r2 = ridx + 1;

                sheet["D" + r1].Text = evt.f1.no;
                sheet["F" + r1].Text = evt.f1.og;
                sheet["G" + r1].Text = evt.f1.nm;
                SetFlagPicture(cfg, sheet, program, r1, 5, evt.f1.og);

                sheet["D" + r2].Text = evt.f2.no;
                sheet["F" + r2].Text = evt.f2.og;
                sheet["G" + r2].Text = evt.f2.nm;
                SetFlagPicture(cfg, sheet, program, r2, 5, evt.f2.og);

                ridx = ridx + 4;
            }
        }

        private void ResetPlayers08(TConfig cfg, Spire.Xls.Worksheet sheet, TProgram program, Dictionary<string, TEvt> map)
        {
            //處理場次 shape
            if (sheet.OvalShapes == null || sheet.OvalShapes.Count == 0) return;

            var shapes = sheet.OvalShapes;
            for (var i = 0; i < shapes.Count; i++)
            {
                var shape = shapes[i];
                var text = shape.Text;
                var code = GetInt(text);
                var key = "";
                if (code <= 4)
                {
                    key = "M008-" + code.ToString().PadLeft(2, '0');
                }
                else
                {
                    switch (text)
                    {
                        case "5": key = "R008-01"; break;
                        case "6": key = "R008-02"; break;

                        case "7": key = "M004-01"; break;
                        case "8": key = "M004-02"; break;

                        case "9": key = "R004-01"; break;
                        case "10": key = "R004-02"; break;

                        case "11": key = "M002-01"; break;
                    }
                }

                var evt = FindEvt(cfg, map, key);
                shape.Text = evt.tno;
                //shape.Text = "99";
            }

            //補 4 強進復活賽 (上下互換)
            var evt_M004_01 = FindEvt(cfg, map, "M004-01");
            var evt_M004_02 = FindEvt(cfg, map, "M004-02");
            sheet["H33"].Text = evt_M004_02.tno;
            sheet["H43"].Text = evt_M004_01.tno;
        }

        private void ExportPlayers16(TConfig cfg, Spire.Xls.Worksheet sheet, TProgram program, Dictionary<string, TEvt> map)
        {
            //活動標題
            sheet["A1"].Text = cfg.mt_title;
            //級組
            sheet["G3"].Text = program.in_l2 + " " + program.in_l3.Replace(" ", "");
            //抽籤日期
            sheet["L3"].Text = program.signTime;

            var ridx = 6;
            for (var i = 1; i <= 8; i++)
            {
                var key = "M016-" + i.ToString().PadLeft(2, '0');
                var evt = FindEvt(cfg, map, key);

                var r1 = ridx + 0;
                var r2 = ridx + 1;

                sheet["D" + r1].Text = evt.f1.no;
                sheet["F" + r1].Text = evt.f1.og;
                sheet["G" + r1].Text = evt.f1.nm;
                SetFlagPicture(cfg, sheet, program, r1, 5, evt.f1.og);

                sheet["D" + r2].Text = evt.f2.no;
                sheet["F" + r2].Text = evt.f2.og;
                sheet["G" + r2].Text = evt.f2.nm;
                SetFlagPicture(cfg, sheet, program, r2, 5, evt.f2.og);

                ridx = ridx + 4;
            }
        }

        private void ResetPlayers16(TConfig cfg, Spire.Xls.Worksheet sheet, TProgram program, Dictionary<string, TEvt> map)
        {
            //處理場次 shape
            if (sheet.OvalShapes == null || sheet.OvalShapes.Count == 0) return;

            var shapes = sheet.OvalShapes;
            for (var i = 0; i < shapes.Count; i++)
            {
                var shape = shapes[i];
                var text = shape.Text;
                var code = GetInt(text);
                var key = "";
                if (code <= 8)
                {
                    key = "M016-" + code.ToString().PadLeft(2, '0');
                }
                else if (code >= 9 && code <= 12)
                {
                    key = "M008-" + (code - 8).ToString().PadLeft(2, '0');
                }
                else
                {
                    switch (text)
                    {
                        case "13": key = "R008-01"; break;
                        case "14": key = "R008-02"; break;

                        case "15": key = "M004-01"; break;
                        case "16": key = "M004-02"; break;

                        case "17": key = "R004-01"; break;
                        case "18": key = "R004-02"; break;

                        case "19": key = "M002-01"; break;
                    }
                }

                var evt = FindEvt(cfg, map, key);
                shape.Text = evt.tno;
                //shape.Text = "99";
            }

            //補 4 強進復活賽 (上下互換)
            var evt_M004_01 = FindEvt(cfg, map, "M004-01");
            var evt_M004_02 = FindEvt(cfg, map, "M004-02");
            sheet["H46"].Text = evt_M004_02.tno;
            sheet["H56"].Text = evt_M004_01.tno;
        }

        private void ExportPlayers32(TConfig cfg, Spire.Xls.Worksheet sheet, TProgram program, Dictionary<string, TEvt> map)
        {
            //活動標題
            sheet["A1"].Text = cfg.mt_title;
            //級組
            sheet["G3"].Text = program.in_l2 + " " + program.in_l3.Replace(" ", "");
            //抽籤日期
            sheet["L3"].Text = program.signTime;

            var ridx = 6;
            for (var i = 1; i <= 16; i++)
            {
                var key = "M032-" + i.ToString().PadLeft(2, '0');
                var evt = FindEvt(cfg, map, key);

                var r1 = ridx + 0;
                var r2 = ridx + 1;

                sheet["D" + r1].Text = evt.f1.no;
                sheet["F" + r1].Text = evt.f1.og;
                sheet["G" + r1].Text = evt.f1.nm;
                SetFlagPicture(cfg, sheet, program, r1, 5, evt.f1.og);

                sheet["D" + r2].Text = evt.f2.no;
                sheet["F" + r2].Text = evt.f2.og;
                sheet["G" + r2].Text = evt.f2.nm;
                SetFlagPicture(cfg, sheet, program, r2, 5, evt.f2.og);

                ridx = ridx + 4;
            }
        }

        private void ResetPlayers32(TConfig cfg, Spire.Xls.Worksheet sheet, TProgram program, Dictionary<string, TEvt> map)
        {
            //處理場次 shape
            if (sheet.OvalShapes == null || sheet.OvalShapes.Count == 0) return;

            var shapes = sheet.OvalShapes;
            for (var i = 0; i < shapes.Count; i++)
            {
                var shape = shapes[i];
                var text = shape.Text;
                var code = GetInt(text);
                var key = "";
                if (code <= 16)
                {
                    key = "M032-" + code.ToString().PadLeft(2, '0');
                }
                else if (code >= 17 && code <= 24)
                {
                    key = "M016-" + (code - 16).ToString().PadLeft(2, '0');
                }
                else if (code >= 25 && code <= 28)
                {
                    key = "M008-" + (code - 24).ToString().PadLeft(2, '0');
                }
                else
                {
                    switch (text)
                    {
                        case "29": key = "R008-01"; break;
                        case "30": key = "R008-02"; break;

                        case "31": key = "M004-01"; break;
                        case "32": key = "M004-02"; break;

                        case "33": key = "R004-01"; break;
                        case "34": key = "R004-02"; break;

                        case "35": key = "M002-01"; break;
                    }
                }

                var evt = FindEvt(cfg, map, key);
                shape.Text = evt.tno;
                //shape.Text = "99";
            }

            //補 4 強進復活賽 (上下互換)
            var evt_M004_01 = FindEvt(cfg, map, "M004-01");
            var evt_M004_02 = FindEvt(cfg, map, "M004-02");
            sheet["H76"].Text = evt_M004_02.tno;
            sheet["H86"].Text = evt_M004_01.tno;
        }

        private void SetFlagPicture(TConfig cfg, Spire.Xls.Worksheet sheet, TProgram program, int topRow, int leftColumn, string og)
        {
            var fileName = @"C:\site\judo\Images\flagsXlsx\" + og + ".png";
            if (!System.IO.File.Exists(fileName)) return;

            var picture = sheet.Pictures.Add(topRow, leftColumn, fileName);
            picture.Width = 30;
            picture.Height = 16;
            picture.LeftColumnOffset = program.LeftColumnOffset;
            picture.TopRowOffset = program.TopRowOffset;
        }

        private TEvt FindEvt(TConfig cfg, Dictionary<string, TEvt> map, string in_fight_id)
        {
            var result = default(TEvt);
            if (map.ContainsKey(in_fight_id))
            {
                result = map[in_fight_id];
            }
            else
            {
                result = new TEvt { Value = cfg.inn.newItem(), tno = "" };
            }

            if (result.f1 == null)
            {
                result.f1 = NewDetail(cfg, cfg.inn.newItem());
            }

            if (result.f2 == null)
            {
                result.f2 = NewDetail(cfg, cfg.inn.newItem());
            }

            return result;
        }

        private void SetExportInof(TConfig cfg, Item itmReturn)
        {
            var sql = @"SELECT in_title, in_battle_repechage, in_uniform_color FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'";
            cfg.itmMeeting = cfg.inn.applySQL(sql);
            if (cfg.itmMeeting.isError() || cfg.itmMeeting.getResult() == "")
            {
                throw new Exception("找無該場賽事資料!");
            }

            cfg.mt_title = cfg.itmMeeting.getProperty("in_title", "");
            cfg.mt_battle_repechage = cfg.itmMeeting.getProperty("in_battle_repechage", "").ToLower();
            cfg.in_uniform_color = cfg.itmMeeting.getProperty("in_uniform_color", "");

            var target_name = "competition_nation";
            var itmXls = cfg.inn.applyMethod("In_Meeting_Excel_Path", "<in_name>" + target_name + "</in_name>");
            cfg.export_path = itmXls.getProperty("export_path", "");
            cfg.template_path = itmXls.getProperty("template_path", "");
            if (cfg.template_path == "")
            {
                throw new Exception("查無報表樣板!");
            }

            //道服顏色資訊
            cfg.itmColor = cfg.inn.newItem("In_Meeting");
            cfg.itmColor.setProperty("in_uniform_color", cfg.in_uniform_color);
            cfg.itmColor = cfg.itmColor.apply("in_meeting_uniform_color");

            cfg.export_type = itmReturn.getProperty("export_type", "").ToLower();
        }

        private Item GetProgramItems(TConfig cfg, Item itmReturn)
        {
            var program_id = itmReturn.getProperty("program_id", "");
            var sql = "SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + program_id + "'";
            var items = cfg.inn.applySQL(sql);

            if (items.isError() || items.getResult() == "")
            {
                throw new Exception("查無組別資料!");
            }

            return items;
        }

        private void CopyItemValue(Item itmA, Item itmB, string propA, string propB = "")
        {
            if (propB == "")
            {
                itmA.setProperty(propA, itmB.getProperty(propA, ""));
            }
            else
            {
                itmA.setProperty(propA, itmB.getProperty(propB, ""));
            }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }

            public string meeting_id { get; set; }
            public string scene { get; set; }

            public Item itmMeeting { get; set; }
            public Item itmColor { get; set; }


            public string mt_title { get; set; }
            public string mt_battle_repechage { get; set; }
            public string in_uniform_color { get; set; }

            public string export_path { get; set; }
            public string template_path { get; set; }
            public string export_type { get; set; }
        }

        private class TProgram
        {
            public string id { get; set; }
            public string in_name { get; set; }
            public string in_short_name { get; set; }
            public string in_weight { get; set; }
            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }
            public string in_fight_day { get; set; }
            public string in_battle_type { get; set; }
            public string in_round_code { get; set; }
            public string in_team_count { get; set; }
            public string in_sign_time { get; set; }

            public string in_place { get; set; }

            public Item Value { get; set; }

            public string sheetKey { get; set; }
            public int teamCount { get; set; }
            public bool isRobin { get; set; }
            public bool isTwoFighters { get; set; }
            public string signTime { get; set; }

            public Dictionary<string, TEvt> map { get; set; }

            public int LeftColumnOffset { get; set; }
            public int TopRowOffset { get; set; }

        }


        private TProgram MapProgram(TConfig cfg, Item itmProgram)
        {
            var result = new TProgram
            {
                id = itmProgram.getProperty("id", ""),
                in_name = itmProgram.getProperty("in_name", ""),
                in_short_name = itmProgram.getProperty("in_short_name", ""),
                in_weight = itmProgram.getProperty("in_weight", ""),
                in_l1 = itmProgram.getProperty("in_l1", ""),
                in_l2 = itmProgram.getProperty("in_l2", ""),
                in_l3 = itmProgram.getProperty("in_l3", ""),
                in_fight_day = itmProgram.getProperty("in_fight_day", ""),
                in_battle_type = itmProgram.getProperty("in_battle_type", ""),
                in_round_code = itmProgram.getProperty("in_round_code", "0"),
                in_team_count = itmProgram.getProperty("in_team_count", "0"),
                in_sign_time = itmProgram.getProperty("in_sign_time", ""),
                Value = itmProgram,
            };

            result.in_place = GetProgramPlace(cfg, result.id);
            if (result.in_short_name == "")
            {
                result.in_short_name = result.id;
            }

            result.isRobin = result.in_battle_type.Contains("SingleRoundRobin");
            result.teamCount = GetInt(result.in_team_count);
            result.signTime = GetDtmStr(result.in_sign_time, 8, "dd.MM.yyyy");

            if (result.isRobin && result.teamCount == 2)
            {
                result.isTwoFighters = true;
            }

            if (result.isTwoFighters)
            {
                result.sheetKey = "TwoOutOfThree";
            }
            else if (result.isRobin)
            {
                result.sheetKey = "SingleRoundRobin";
            }
            else
            {
                result.sheetKey = result.in_round_code;
            }

            return result;
        }

        private string GetProgramPlace(TConfig cfg, string program_id)
        {
            var item = cfg.inn.applySQL("SELECT TOP 1 in_place FROM IN_MEETING_ALLOCATION WITH(NOLOCK) WHERE in_program = '" + program_id + "'");
            if (item.isError() || item.getResult() == "")
            {
                return "";
            }
            else
            {
                return item.getProperty("in_place", "");
            }
        }

        private class TEvt
        {
            public string tno { get; set; }
            public TDtl f1 { get; set; }
            public TDtl f2 { get; set; }
            public Item Value { get; set; }

        }

        private class TDtl
        {
            public string no { get; set; }
            public string og { get; set; }
            public string nm { get; set; }
            public Item Value { get; set; }
        }


        private Dictionary<string, TEvt> MapEventList(TConfig cfg, TProgram program)
        {
            var result = new Dictionary<string, TEvt>();
            var itmDetails = GetMPEventDetails(cfg, program);
            var count = itmDetails.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var itmDetail = itmDetails.getItemByIndex(i);
                var in_fight_id = itmDetail.getProperty("in_fight_id", "");
                var in_tree_no = itmDetail.getProperty("in_tree_no", "");
                var in_sign_foot = itmDetail.getProperty("in_sign_foot", "");
                if (in_fight_id == "") continue;

                var evt = default(TEvt);
                if (result.ContainsKey(in_fight_id))
                {
                    evt = result[in_fight_id];
                }
                else
                {
                    evt = new TEvt
                    {
                        tno = in_tree_no,
                        Value = itmDetail,
                    };
                    result.Add(in_fight_id, evt);
                }

                if (in_sign_foot == "1")
                {
                    evt.f1 = NewDetail(cfg, itmDetail);
                }
                else if (in_sign_foot == "2")
                {
                    evt.f2 = NewDetail(cfg, itmDetail);
                }
            }
            return result;
        }

        private TDtl NewDetail(TConfig cfg, Item itmDetail)
        {
            var result = new TDtl
            {
                no = itmDetail.getProperty("in_section_no", ""),
                og = itmDetail.getProperty("map_short_org", ""),
                nm = itmDetail.getProperty("in_name", ""),
                Value = itmDetail,
            };

            return result;
        }

        private Item GetMPEventDetails(TConfig cfg, TProgram program)
        {
            string sql = @"
                SELECT
                    t1.id
                    , t1.in_tree_name
                    , t1.in_tree_id
                    , t1.in_fight_id
                    , t1.in_tree_no
                    , t1.in_win_time
                    , t1.in_win_sign_no
                    , t1.in_win_local_time
                    , t2.in_sign_foot
                    , t2.in_sign_no
                    , t2.in_sign_bypass
                    , t2.in_points
                    , t2.in_correct_count
                    , t2.in_status
                    , t3.in_name AS 'site_name'
                    , t3.in_code AS 'site_code'
                    , t11.in_section_no
                    , t11.in_name
                    , t11.in_team
                    , t11.map_short_org
                    , t11.in_sign_time
                    , t11.in_weight_value
                    , t11.in_weight_message
                    , t11.in_final_rank
                    , t11.in_final_count
                    , t11.in_final_points
                FROM
                    IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
                    IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK)
                    ON t2.source_id = t1.id
                LEFT OUTER JOIN
                    IN_MEETING_SITE t3 WITH(NOLOCK)
                    ON t3.id = t1.in_site
                LEFT OUTER JOIN
                    VU_MEETING_PTEAM  t11 WITH(NOLOCK)
                    ON t11.source_id = t1.source_id
                    AND t11.in_sign_no = t2.in_sign_no
                WHERE
                    t1.source_id = '{#program_id}'
                    AND in_tree_name in ('main', 'repechage')
                ORDER BY
                      t1.in_tree_sort
                    , t1.in_tree_id
                    , t1.in_tree_no
                    , t2.in_sign_foot
            ";

            sql = sql.Replace("{#program_id}", program.id);

            return cfg.inn.applySQL(sql);
        }

        private string GetDtmStr(string value, int hours, string format)
        {
            if (value == "") return "";

            var result = GetDtm(value, hours);
            if (result == DateTime.MinValue) return "";

            return result.ToString(format);
        }

        private DateTime GetDtm(string value, int hours)
        {
            if (value == "") return DateTime.MinValue;

            DateTime result = DateTime.MinValue;
            if (DateTime.TryParse(value, out result))
            {
                return result.AddHours(hours);
            }
            return DateTime.MinValue;
        }

        private int GetInt(string value, int defV = 0)
        {
            if (value == "") return 0;
            int result = defV;
            if (int.TryParse(value, out result))
            {
                return result;
            }
            return defV;
        }
    }
}