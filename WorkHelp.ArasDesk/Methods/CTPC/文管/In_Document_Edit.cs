﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;
using System.Runtime.Remoting.Contexts;

namespace WorkHelp.ArasDesk.Methods.CTPC.Doc
{
    public class In_Document_Edit : Item
    {
        public In_Document_Edit(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
    目的: 文件編輯-擴充功能
    說明:
        - 為不影響原 iplm 開發模式，繞路處理
    日誌: 
        - 2022-09-17: 創建 (lina)
*/

            System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "In_Document_Edit";

            var itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                strIdentityId = inn.getUserAliases(),

                doc_id = itmR.getProperty("doc_id", ""),
                scene = itmR.getProperty("scene", ""),
            };

            //取得登入者資訊
            Item itmResume = inn.newItem("In_Resume", "get");
            itmResume.setProperty("in_user_id", cfg.strUserId);
            cfg.LoginResume = itmResume.apply();

            if (cfg.LoginResume.isError() || cfg.LoginResume.getItemCount() != 1)
            {
                throw new Exception("個人履歷資料異常");
            }

            cfg.login_is_admin = cfg.LoginResume.getProperty("in_is_admin", "");
            if (cfg.login_is_admin != "1")
            {
                throw new Exception("無文管權限");
            }


            switch (cfg.scene)
            {
                case "kick_off"://收文: 送至文管確認 / 發文: 啟動流程
                    KickOffFlow(cfg, itmR);
                    break;

                case "dispatch_page"://文管確認頁面
                    DispatchPage(cfg, itmR);
                    break;

                case "dispatch_page2"://文管確認頁面(重新指定承辦組員)
                    DispatchPage2(cfg, itmR);
                    break;

                case "dispatch_page3"://發文-副秘、會長簽審設定
                    DispatchPage3(cfg, itmR);
                    break;

                case "change_undertaker"://變更承辦組員
                    ChangeUndertaker(cfg, itmR);
                    break;

                case "remove":
                    RemoveDoc(cfg, itmR);
                    break;

                case "to_undertaker"://啟動，送至承辦組員
                    ToUndertaker(cfg, itmR);
                    break;

                case "change_verify"://變更審核流程
                    ChangeNeedVerify(cfg, itmR);
                    break;

                case "change_deadline"://變更期限
                    ChangeDeadline(cfg, itmR);
                    break;

                case "assignment_page"://簽審頁面
                    AssignmentPage(cfg, itmR);
                    break;

                case "to_fix"://修正資料
                    ToFixDocument(cfg, itmR);
                    break;

                case "edit_page"://編輯頁面 1
                    EditPage(cfg, itmR);
                    break;

                case "edit_page2"://編輯頁面 2
                    EditPage2(cfg, itmR);
                    break;

                case "vote_page"://簽審頁面 1
                    VotePage(cfg, itmR);
                    break;

                case "view_page"://檢視頁面 2
                    ViewPage(cfg, itmR);
                    break;

                case "reject_modal"://退收文發起人 跳窗
                    RejectModal(cfg, itmR);
                    break;

                case "to_reject"://退收文發起人
                    ToReject(cfg, itmR);
                    break;

                case "cowork_modal"://加入會辦 跳窗
                    CoworkModal(cfg, itmR);
                    break;

                case "to_cowork"://加入會辦
                    ToCowork(cfg, itmR);
                    break;

                case "remove_cowork":
                    RemoveCowork(cfg, itmR);
                    break;

                case "list"://文件清單
                    DocList(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void KickOffFlow(TConfig cfg, Item itmReturn)
        {
            LoadFlowVariable(cfg);

            string in_issue_type = itmReturn.getProperty("issue_type", "");
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "in_issue_type" + in_issue_type);
            switch (in_issue_type)
            {
                case "receive":
                    ReceiveToDispatcher(cfg, in_issue_type, itmReturn);
                    SendEmail(cfg, "receive_dispatch");
                    break;

                case "launch":
                    LaunchToDispatcher(cfg, in_issue_type, itmReturn);
                    break;

                default:
                    throw new Exception("文件類型錯誤");
            }
        }

        private void LaunchToDispatcher(TConfig cfg, string in_issue_type, Item itmReturn)
        {
            //發文 Info
            var model = GetLaunchMap(cfg, itmReturn);

            //角色 Info
            var map = GetRoleMap(cfg);

            //更新文件流程
            UpdLaunchDocFlow(cfg, model, map, in_issue_type);

            //更新文件資訊
            UpdLaunchDocInfo(cfg, model, map, in_issue_type);

            //更新會辦流程
            MergeCoworker(cfg, model, map);

            var is_edit = itmReturn.getProperty("is_edit", "");
            if (is_edit == "1")
            {
                var new_issue_deadline = DtmStr(itmReturn.getProperty("new_issue_deadline", ""), "yyyy-MM-dd HH:mm:ss", -8);

                string sql_upd = "UPDATE Document SET"
                    + "  in_issue_no = '" + itmReturn.getProperty("new_issue_no", "") + "'"
                    + ", in_issue_urgency = '" + itmReturn.getProperty("new_issue_urgency", "") + "'"
                    + ", name = N'" + itmReturn.getProperty("new_name", "") + "'"
                    + ", in_issue_deadline = '" + new_issue_deadline + "'"
                    + " WHERE id = '" + cfg.doc_id + "'"
                ;

                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql_upd);

                cfg.inn.applySQL(sql_upd);
            }
            else
            {
                //啟動流程
                Item itmFlow = cfg.inn.newItem("");
                itmFlow.setType("Document");
                itmFlow.setProperty("itemtype", "Document");
                itmFlow.setProperty("itemid", cfg.doc_id);
                itmFlow.apply("In_AddtoWorkflow_App");
            }

        }

        //更新會辦流程
        private void MergeCoworker(TConfig cfg, TLaunchModel model, TRoleMap map)
        {
            //重建會辦組員
            RebuildCoworkerMembers(cfg, model.lstCoworks);

            //重建會辦主管
            RebuildCoworkerLeaders(cfg, model.mngr_idt_id);

            //檢查會辦代理
            CheckCoworkerAgent(cfg);

            //重建會辦簽審流程
            RebuildCoworkerFlow(cfg);
        }

        //檢查會辦代理
        private void CheckCoworkerAgent(TConfig cfg)
        {
            var sql1 = "SELECT id, related_id FROM IN_DOCUMENT_COWORK_MEMBER WHERE source_id = '" + cfg.doc_id + "'";
            var items1 = cfg.inn.applySQL(sql1);
            CheckCoworkerAgent(cfg, items1, "IN_DOCUMENT_COWORK_MEMBER");

            var sql2 = "SELECT id, related_id FROM IN_DOCUMENT_COWORK_MANAGER WHERE source_id = '" + cfg.doc_id + "'";
            var items2 = cfg.inn.applySQL(sql2);
            CheckCoworkerAgent(cfg, items2, "IN_DOCUMENT_COWORK_MANAGER");
        }

        private void CheckCoworkerAgent(TConfig cfg, Item items, string table)
        {
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var id = item.getProperty("id", "");
                var related_id = item.getProperty("related_id", "");
                var agent = FindUserAgent(cfg, related_id);
                if (agent.agent_identity_id == "") continue;

                var sql = "UPDATE [" + table + "] SET"
                    + "  related_id = '" + agent.agent_identity_id + "'"
                    + ", in_agent_note = '代理'"
                    + " WHERE id = '" + id + "'"
                    ;
                cfg.inn.applySQL(sql);
            }
        }

        //發文
        private void UpdLaunchDocFlow(TConfig cfg, TLaunchModel model, TRoleMap map, string in_issue_type)
        {
            //清除流程
            string sql = "DELETE FROM In_Document_WorkFlow WHERE source_id = '" + cfg.doc_id + "'";
            cfg.inn.applySQL(sql);

            //承辦組員
            var f2_row = new TWorkFlow
            {
                label = "承辦組員",
                code = "undertaker",
                group = "200",
                serial = "1",
                edit = "0",
                idt_dept_id = model.sect_idt_id,
                idt_dept_name = model.sect_idt_desc,
                idt_mmbr_id = model.me_idt_id,
                idt_mmbr_name = model.me_idt_name,
                in_need_verify = "1",
            };
            AppendDocWorkFlow(cfg, f2_row);
            if (f2_row.has_agent)
            {
                model.me_idt_id = f2_row.idt_mmbr_id;
                model.me_idt_name = f2_row.idt_mmbr_name;
            }

            //承辦單位主管
            var f3_row = new TWorkFlow
            {
                label = "承辦單位主管",
                code = "supervisor",
                group = "300",
                serial = "1",
                edit = "0",
                idt_dept_id = model.sect_idt_id,
                idt_dept_name = model.sect_idt_desc,
                idt_mmbr_id = model.mngr_idt_id,
                idt_mmbr_name = model.mngr_idt_name,
                in_need_verify = "1",
            };
            AppendDocWorkFlow(cfg, f3_row);
            if (f3_row.has_agent)
            {
                model.mngr_idt_id = f3_row.idt_mmbr_id;
                model.mngr_idt_name = f3_row.idt_mmbr_name;
            }

            //行政副主任
            var f37_row = new TWorkFlow
            {
                label = "行政副主任",
                code = "eddeputy",
                group = "370",
                serial = "1",
                edit = "1",
                idt_mmbr_id = map.act32_idt_mmbr_id,
                idt_mmbr_name = map.act32_idt_mmbr_nm,
                in_need_verify = "1",
            };
            if (model.in_mode_ad != "1")
            {
                f37_row.in_need_verify = "0";
            }
            AppendDocWorkFlow(cfg, f37_row);
            if (f37_row.has_agent)
            {
                map.act32_idt_mmbr_id = f37_row.idt_mmbr_id;
                map.act32_idt_mmbr_nm = f37_row.idt_mmbr_name;
            }

            //行政主任
            var f38_row = new TWorkFlow
            {
                label = "行政主任",
                code = "edmanager",
                group = "380",
                serial = "1",
                edit = "1",
                idt_mmbr_id = map.act31_idt_mmbr_id,
                idt_mmbr_name = map.act31_idt_mmbr_nm,
                in_need_verify = "1",
            };
            AppendDocWorkFlow(cfg, f38_row);
            if (f38_row.has_agent)
            {
                map.act31_idt_mmbr_id = f38_row.idt_mmbr_id;
                map.act31_idt_mmbr_nm = f38_row.idt_mmbr_name;
            }

            //客製節點1
            var f39_row = new TWorkFlow
            {
                label = "沈副秘",
                code = "special1",
                group = "390",
                serial = "1",
                edit = "1",
                idt_mmbr_id = map.act33_idt_mmbr_id,
                idt_mmbr_name = map.act33_idt_mmbr_nm,
                in_need_verify = "0",
            };
            AppendDocWorkFlow(cfg, f39_row);
            if (f39_row.has_agent)
            {
                map.act33_idt_mmbr_id = f39_row.idt_mmbr_id;
                map.act33_idt_mmbr_nm = f39_row.idt_mmbr_name;
            }

            //副秘
            AppendDeputySecretaryFLow(cfg, map);

            var f5_row = new TWorkFlow
            {
                label = "秘書長",
                code = "secretary",
                group = "500",
                serial = "1",
                edit = "0",
                idt_dept_id = map.act5_idt_dept_id,
                idt_dept_name = map.act5_idt_dept_ds,
                idt_mmbr_id = map.act5_idt_mmbr_id,
                idt_mmbr_name = map.act5_idt_mmbr_nm,
                in_need_verify = "1",
            };
            AppendDocWorkFlow(cfg, f5_row);
            if (f5_row.has_agent)
            {
                map.act5_idt_mmbr_id = f5_row.idt_mmbr_id;
                map.act5_idt_mmbr_nm = f5_row.idt_mmbr_name;
            }

            var f6_row = new TWorkFlow
            {
                label = "會長",
                code = "chairman",
                group = "600",
                serial = "1",
                edit = "1",
                idt_dept_id = map.act6_idt_dept_id,
                idt_dept_name = map.act6_idt_dept_ds,
                idt_mmbr_id = map.act6_idt_mmbr_id,
                idt_mmbr_name = map.act6_idt_mmbr_nm,
                in_need_verify = "0",
            };
            AppendDocWorkFlow(cfg, f6_row);
            if (f6_row.has_agent)
            {
                map.act6_idt_mmbr_id = f6_row.idt_mmbr_id;
                map.act6_idt_mmbr_nm = f6_row.idt_mmbr_name;
            }

            //承辦組員發行
            var f7_row = new TWorkFlow
            {
                label = "承辦組員發行",
                code = "release",
                group = "700",
                serial = "1",
                idt_dept_id = model.sect_idt_id,
                idt_dept_name = model.sect_idt_desc,
                idt_mmbr_id = model.me_idt_id,
                idt_mmbr_name = model.me_idt_name,
                in_need_verify = "1",
            };
            AppendDocWorkFlow(cfg, f7_row);
            if (f7_row.has_agent)
            {
                model.me_idt_id = f7_row.idt_mmbr_id;
                model.me_idt_name = f7_row.idt_mmbr_name;
            }
        }

        //副秘書長
        private void AppendDeputySecretaryFLow(TConfig cfg, TRoleMap map)
        {
            if (map.ds_cnt == 1)
            {
                var f4_row = new TWorkFlow
                {
                    label = "副秘書長",
                    code = "deputy",
                    group = "400",
                    serial = "1",
                    edit = "1",
                    idt_dept_id = map.act4_idt_dept_id,
                    idt_dept_name = map.act4_idt_dept_ds,
                    idt_mmbr_id = map.act4_idt_mmbr_id,
                    idt_mmbr_name = map.act4_idt_mmbr_nm,
                    in_need_verify = "1",
                };
                AppendDocWorkFlow(cfg, f4_row);
            }
            else if (map.ds_cnt > 1)
            {
                var v = cfg.Deputy_Secretary_Mode;
                if (v == "2") v = "1";

                var f4_row_1 = new TWorkFlow
                {
                    label = "副秘書長",
                    code = "deputy",
                    group = "400",
                    serial = "1",
                    edit = "1",
                    idt_dept_id = map.act4_idt_dept_id,
                    idt_dept_name = map.act4_idt_dept_ds,
                    idt_mmbr_id = map.itmDSs.getItemByIndex(0).getProperty("mbr_id", ""),
                    idt_mmbr_name = map.itmDSs.getItemByIndex(0).getProperty("mbr_name", ""),
                    in_need_verify = v,
                };
                AppendDocWorkFlow(cfg, f4_row_1);

                var f4_row_2 = new TWorkFlow
                {
                    label = "副秘書長",
                    code = "deputy",
                    group = "400",
                    serial = "2",
                    edit = "1",
                    idt_dept_id = map.act4_idt_dept_id,
                    idt_dept_name = map.act4_idt_dept_ds,
                    idt_mmbr_id = map.itmDSs.getItemByIndex(1).getProperty("mbr_id", ""),
                    idt_mmbr_name = map.itmDSs.getItemByIndex(1).getProperty("mbr_name", ""),
                    in_need_verify = v,
                };
                AppendDocWorkFlow(cfg, f4_row_2);
            }
        }

        private void UpdLaunchDocInfo(TConfig cfg, TLaunchModel model, TRoleMap map, string in_issue_type)
        {
            string sql = "UPDATE [Document] SET"
                + "  in_issue_status = 'launched'"
                + ", in_mode_um = ''"
                + ", in_mode_ad = '" + model.in_mode_ad + "'"
                + ", in_mode_ds = '" + cfg.in_mode_ds + "'" //副秘簽審模式
                + ", in_mode_sg = ''"
                + ", in_mode_cm = ''" //會長不簽審
                + ", in_mode_sp1 = ''" //客製節點1 (不簽審)
                + ", in_issue_type = '" + in_issue_type + "'"
                + ", in_issue_date = created_on"
                + ", in_issue_flowtype = '100'"
                + ", in_issue_viewer = '" + cfg.strIdentityId + "'"
                + ", in_wf_act0_identity = '" + cfg.strIdentityId + "'"
                + ", in_wf_act2_identity = '" + model.me_idt_id + "'"//承辦單位組員
                + ", in_wf_act2_label = N'" + model.me_idt_name + "'"
                + ", in_wf_act3_identity = '" + model.mngr_idt_id + "'" //承辦單位主管
                + ", in_wf_act3_label = N'" + model.mngr_idt_name + "'"
                + ", in_wf_act4_identity = '" + map.act4_idt_dept_id + "'"
                + ", in_wf_act4_label = N'" + map.act4_idt_dept_nm + "'"
                + ", in_wf_act5_identity = '" + map.act5_idt_mmbr_id + "'" //秘書長
                + ", in_wf_act5_label = N'" + map.act5_idt_mmbr_nm + "'"
                + ", in_wf_act6_identity = '" + map.act6_idt_mmbr_id + "'" //會長
                + ", in_wf_act6_label = N'" + map.act6_idt_mmbr_nm + "'"
                + ", in_wf_act31_identity = '" + map.act31_idt_mmbr_id + "'" //行政主任
                + ", in_wf_act32_identity = '" + map.act32_idt_mmbr_id + "'" //行政副主任
                + ", in_wf_act41_identity = '" + map.act41_idt_mmbr_id + "'" //副秘書長 A
                + ", in_wf_act42_identity = '" + map.act42_idt_mmbr_id + "'" //副秘書長 B
                + ", in_wf_act7_identity = '" + model.me_idt_id + "'"//承辦單位組員
                + ", in_wf_act7_label = N'" + model.me_idt_name + "'"
                + ", in_wf_act33_identity = '" + map.act33_idt_mmbr_id + "'" //客製節點1
                + ", owned_by_id = '" + cfg.strIdentityId + "'"
                + ", created_by_id = '" + cfg.strUserId + "'"
                + " WHERE id = '" + cfg.doc_id + "'";

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            Item itmResult = cfg.inn.applySQL(sql);

            if (itmResult.isError())
            {
                throw new Exception("文件更新失敗");
            }
        }

        private void ReceiveToDispatcher(TConfig cfg, string in_issue_type, Item itmReturn)
        {
            var map = GetRoleMap(cfg);
            //收文-更新文件
            ReceiveToDispatcherA(cfg, in_issue_type, map, itmReturn);
            //收文-更新流程
            ReceiveToDispatcherB(cfg, in_issue_type, map, itmReturn);
        }

        //收文-更新文件
        private void ReceiveToDispatcherA(TConfig cfg, string in_issue_type, TRoleMap map, Item itmReturn)
        {
            var sql = "UPDATE [Document] SET"
                + "  in_issue_status = 'created'"
                + ", in_mode_um = ''"
                + ", in_mode_cw = ''"
                + ", in_mode_cl = ''"
                + ", in_mode_ad = '1'" //行政主任須簽審
                + ", in_mode_ds = '" + cfg.in_mode_ds + "'" //副秘簽審模式
                + ", in_mode_sg = ''"
                + ", in_mode_cm = ''" //會長不簽審
                + ", in_mode_sp1 = ''" //客製節點1 (不簽審)
                + ", in_issue_type = '" + in_issue_type + "'"
                + ", in_issue_date = created_on"
                + ", in_issue_flowtype = '100'"
                + ", in_issue_viewer = '" + map.act1_idt_mmbr_id + "'"
                + ", in_wf_act0_identity = '" + cfg.strIdentityId + "'"
                + ", in_wf_act1_identity = '" + map.act1_idt_mmbr_id + "'"
                + ", in_wf_act1_label = N'" + map.act1_idt_mmbr_nm + "'"
                + ", in_wf_act4_identity = '" + map.act4_idt_dept_id + "'"
                + ", in_wf_act4_label = N'" + map.act4_idt_dept_nm + "'"
                + ", in_wf_act5_identity = '" + map.act5_idt_mmbr_id + "'" //秘書長
                + ", in_wf_act5_label = N'" + map.act5_idt_mmbr_nm + "'"
                + ", in_wf_act6_identity = '" + map.act6_idt_mmbr_id + "'" //會長
                + ", in_wf_act6_label = N'" + map.act6_idt_mmbr_nm + "'"
                + ", in_wf_act31_identity = '" + map.act31_idt_mmbr_id + "'" //行政主任
                + ", in_wf_act32_identity = '" + map.act32_idt_mmbr_id + "'" //行政副主任
                + ", in_wf_act41_identity = '" + map.act41_idt_mmbr_id + "'" //副秘書長 A
                + ", in_wf_act42_identity = '" + map.act42_idt_mmbr_id + "'" //副秘書長 B
                + ", in_wf_act33_identity = '" + map.act33_idt_mmbr_id + "'" //客製節點1
                + ", owned_by_id = '" + cfg.strIdentityId + "'"
                + ", created_by_id = '" + cfg.strUserId + "'"
                + " WHERE id = '" + cfg.doc_id + "'";

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            Item itmResult = cfg.inn.applySQL(sql);

            if (itmResult.isError())
            {
                throw new Exception("文件更新失敗");
            }
        }

        //收文-更新流程
        private void ReceiveToDispatcherB(TConfig cfg, string in_issue_type, TRoleMap map, Item itmReturn)
        {
            //清除流程
            var sql = "DELETE FROM In_Document_WorkFlow WHERE source_id = '" + cfg.doc_id + "'";
            cfg.inn.applySQL(sql);

            //文管確認
            var f1_row = new TWorkFlow
            {
                label = "文管確認",
                code = "dispatch",
                group = "100",
                serial = "1",
                edit = "0",
                idt_mmbr_id = map.act1_idt_mmbr_id,
                idt_mmbr_name = map.act1_idt_mmbr_nm,
                in_need_verify = "1",
            };
            AppendDocWorkFlow(cfg, f1_row);
            if (f1_row.has_agent)
            {
                map.act1_idt_mmbr_id = f1_row.idt_mmbr_id;
                map.act1_idt_mmbr_nm = f1_row.idt_mmbr_name;
            }

            //承辦組員
            var f2_row = new TWorkFlow
            {
                label = "承辦組員",
                code = "undertaker",
                group = "200",
                serial = "1",
                edit = "0",
                in_need_verify = "1",
            };
            AppendDocWorkFlow(cfg, f2_row);
            //if (f2_row.has_agent)
            //{
            //    map.act1_idt_mmbr_id = f2_row.idt_mmbr_id;
            //    map.act1_idt_mmbr_nm = f2_row.idt_mmbr_name;
            //}

            //承辦單位主管
            var f3_row = new TWorkFlow
            {
                label = "承辦單位主管",
                code = "supervisor",
                group = "300",
                serial = "1",
                edit = "0",
                in_need_verify = "1",
            };
            AppendDocWorkFlow(cfg, f3_row);
            //if (f3_row.has_agent)
            //{
            //    map.act1_idt_mmbr_id = f3_row.idt_mmbr_id;
            //    map.act1_idt_mmbr_nm = f3_row.idt_mmbr_name;
            //}

            //行政主任
            var f38_row = new TWorkFlow
            {
                label = "行政主任",
                code = "edmanager",
                group = "380",
                serial = "1",
                edit = "1",
                idt_mmbr_id = map.act31_idt_mmbr_id,
                idt_mmbr_name = map.act31_idt_mmbr_nm,
                in_need_verify = "1",
            };
            AppendDocWorkFlow(cfg, f38_row);
            if (f38_row.has_agent)
            {
                map.act31_idt_mmbr_id = f38_row.idt_mmbr_id;
                map.act31_idt_mmbr_nm = f38_row.idt_mmbr_name;
            }

            //客製節點1
            var f39_row = new TWorkFlow
            {
                label = "沈副秘",
                code = "special1",
                group = "390",
                serial = "1",
                edit = "1",
                idt_mmbr_id = map.act33_idt_mmbr_id,
                idt_mmbr_name = map.act33_idt_mmbr_nm,
                in_need_verify = "0",
            };
            AppendDocWorkFlow(cfg, f39_row);
            if (f39_row.has_agent)
            {
                map.act33_idt_mmbr_id = f39_row.idt_mmbr_id;
                map.act33_idt_mmbr_nm = f39_row.idt_mmbr_name;
            }

            //副秘
            AppendDeputySecretaryFLow(cfg, map);

            var f5_row = new TWorkFlow
            {
                label = "秘書長",
                code = "secretary",
                group = "500",
                serial = "1",
                edit = "0",
                idt_dept_id = map.act5_idt_dept_id,
                idt_dept_name = map.act5_idt_dept_ds,
                idt_mmbr_id = map.act5_idt_mmbr_id,
                idt_mmbr_name = map.act5_idt_mmbr_nm,
                in_need_verify = "1",
            };
            AppendDocWorkFlow(cfg, f5_row);
            if (f5_row.has_agent)
            {
                map.act5_idt_mmbr_id = f5_row.idt_mmbr_id;
                map.act5_idt_mmbr_nm = f5_row.idt_mmbr_name;
            }

            var f6_row = new TWorkFlow
            {
                label = "會長",
                code = "chairman",
                group = "600",
                serial = "1",
                edit = "1",
                idt_dept_id = map.act6_idt_dept_id,
                idt_dept_name = map.act6_idt_dept_ds,
                idt_mmbr_id = map.act6_idt_mmbr_id,
                idt_mmbr_name = map.act6_idt_mmbr_nm,
                in_need_verify = "0",
            };
            AppendDocWorkFlow(cfg, f6_row);
            if (f6_row.has_agent)
            {
                map.act6_idt_mmbr_id = f6_row.idt_mmbr_id;
                map.act6_idt_mmbr_nm = f6_row.idt_mmbr_name;
            }

            //承辦組員發行
            var f7_row = new TWorkFlow
            {
                label = "承辦組員發行",
                code = "release",
                group = "700",
                serial = "1",
                in_need_verify = "1",
            };
            AppendDocWorkFlow(cfg, f7_row);
            //if (f7_row.has_agent)
            //{
            //    map.act6_idt_mmbr_id = f7_row.idt_mmbr_id;
            //    map.act6_idt_mmbr_nm = f7_row.idt_mmbr_name;
            //}
        }

        //文管確認
        private void DispatchPage(TConfig cfg, Item itmReturn)
        {
            //發文
            cfg.is_doc_launcher = cfg.login_is_admin == "1";
            //收文
            cfg.is_doc_receiver = HasDocPermission(cfg, "ACT_DOC_Receiver");
            //文管確認
            cfg.is_doc_dispatcher = HasDocPermission(cfg, "ACT_DOC_Dispatcher");

            //文件資訊
            AppendDocInfo(cfg, itmReturn, setReturnData: true);

            string in_issue_status = cfg.itmDoc.getProperty("in_issue_status", "");
            string in_current_activity = cfg.itmDoc.getProperty("in_current_activity", "");

            switch (in_issue_status)
            {
                case "created":
                    cfg.is_waiting = false;
                    itmReturn.setProperty("page_title", "文件管理-簽審設定");
                    break;

                case "rejected":
                    cfg.is_waiting = false;
                    itmReturn.setProperty("page_title", "文件管理-內容訂正");
                    break;

                case "waiting":
                    cfg.is_waiting = true;
                    itmReturn.setProperty("page_title", "文件管理-指派承辦組員");
                    break;

                default:
                    cfg.cant_change = true;
                    itmReturn.setProperty("page_title", "文件簽審");
                    break;
            }


            //發文期限
            itmReturn.setProperty("deadline_ctrl", "<label class='control-label'>" + itmReturn.getProperty("in_issue_deadline", "") + "</label>");
            //刪除文件
            itmReturn.setProperty("remove_ctrl", " ");
            //退收文發起人
            itmReturn.setProperty("reject_ctrl", " ");
            //更多功能
            cfg.MoreActionButtons = "";

            itmReturn.setProperty("deadline_label", "<label for='in_issue_deadline' class='control-label'>發文期限</label>");

            if (cfg.is_doc_dispatcher)
            {
                cfg.is_waiting = false;

                string deadline_value = itmReturn.getProperty("in_issue_deadline", "");
                string deadline_ctrl = "<input type='text' id='in_issue_deadline' class='form-control'"
                    + "placeholder='發文期限' value='" + deadline_value + "' onchange='DoUpdateDeadline()'>";

                //發文期限
                itmReturn.setProperty("deadline_label", "<label for='in_issue_deadline' class='control-label' style='color:red'>1. 發文期限</label>");
                //發文期限
                itmReturn.setProperty("deadline_ctrl", deadline_ctrl);

                //刪除文件
                itmReturn.setProperty("remove_ctrl", "<button class='btn btn-danger' onclick='RemoveDocument()'>刪除文件</button>");

                if (in_current_activity == "")
                {
                    //退收文發起人
                    itmReturn.setProperty("reject_ctrl", "<button class='btn btn-danger' onclick='RejectToCreator()'>退　　回</button>");
                }

                string act_add = " <button class='btn btn-primary pull-right' onclick='AddCoworkOrg()' style='margin-left: 10px'> 3.  <i class='fa fa-plus'></i> 會簽 </button>";
                string act_unlock = "<button class='btn btn-success pull-right' onclick='Unlock_Click()'>承辦組員解鎖</button>";

                cfg.MoreActionButtons = act_add + act_unlock;

                itmReturn.setProperty("inn_need_lock", "1");
            }

            //簽審流程表
            itmReturn.setProperty("inn_table", FlowTable(cfg, cfg.itmDoc, cfg.doc_id));

            //簽審紀錄表
            itmReturn.setProperty("inn_assigns", AssignmentTable(cfg, cfg.itmDoc, cfg.doc_id, false));

            //文件附檔
            itmReturn.setProperty("inn_files", FileTable(cfg, cfg.itmDoc, cfg.doc_id));
        }

        //文管確認-重新指派承辦組員
        private void DispatchPage2(TConfig cfg, Item itmReturn)
        {
            //發文
            cfg.is_doc_launcher = cfg.login_is_admin == "1";
            //收文
            cfg.is_doc_receiver = HasDocPermission(cfg, "ACT_DOC_Receiver");
            //文管確認
            cfg.is_doc_dispatcher = HasDocPermission(cfg, "ACT_DOC_Dispatcher");

            //文件資訊
            AppendDocInfo(cfg, itmReturn, setReturnData: true);

            string in_issue_status = cfg.itmDoc.getProperty("in_issue_status", "");
            string in_current_activity = cfg.itmDoc.getProperty("in_current_activity", "");

            itmReturn.setProperty("page_title", "文件管理-重新指派承辦組員");


            //發文期限
            itmReturn.setProperty("deadline_ctrl", "<label class='control-label'>" + itmReturn.getProperty("in_issue_deadline", "") + "</label>");
            //刪除文件
            itmReturn.setProperty("remove_ctrl", " ");
            //退收文發起人
            itmReturn.setProperty("reject_ctrl", " ");
            //更多功能
            cfg.MoreActionButtons = "";

            itmReturn.setProperty("deadline_label", "<label for='in_issue_deadline' class='control-label'>發文期限</label>");
            if (cfg.is_doc_dispatcher)
            {
                cfg.is_waiting = false;

                string deadline_value = itmReturn.getProperty("in_issue_deadline", "");

                string deadline_ctrl = "<input type='text' id='in_issue_deadline' class='form-control'"
                    + "placeholder='發文期限' value='" + deadline_value + "' onchange='DoUpdateDeadline()'>";

                // //發文期限
                // itmReturn.setProperty("deadline_ctrl", deadline_ctrl);

                //刪除文件
                itmReturn.setProperty("remove_ctrl", "<button class='btn btn-danger' onclick='RemoveDocument()'>刪除文件</button>");

                if (in_current_activity == "")
                {
                    //退收文發起人
                    itmReturn.setProperty("reject_ctrl", "<button class='btn btn-danger' onclick='RejectToCreator()'>退　　回</button>");
                }

                // string act_add = " <button class='btn btn-primary pull-right' onclick='AddCoworkOrg()'"
                //     + ">"
                //     + "  <i class='fa fa-plus'></i> 會簽"
                //     + "</button>";

                // cfg.MoreActionButtons = act_add;
            }

            //簽審流程表
            itmReturn.setProperty("inn_table", FlowTable(cfg, cfg.itmDoc, cfg.doc_id));

            //簽審紀錄表
            itmReturn.setProperty("inn_assigns", AssignmentTable(cfg, cfg.itmDoc, cfg.doc_id, false));

            //文件附檔
            itmReturn.setProperty("inn_files", FileTable(cfg, cfg.itmDoc, cfg.doc_id));
        }

        //發文-設定副秘流程、會長流程
        private void DispatchPage3(TConfig cfg, Item itmReturn)
        {
            //發文
            cfg.is_doc_launcher = cfg.login_is_admin == "1";
            //收文
            cfg.is_doc_receiver = HasDocPermission(cfg, "ACT_DOC_Receiver");
            //文管確認
            cfg.is_doc_dispatcher = HasDocPermission(cfg, "ACT_DOC_Dispatcher");

            //文件資訊
            AppendDocInfo(cfg, itmReturn, setReturnData: true);

            itmReturn.setProperty("page_title", "文件管理-簽審設定(發文)");

            //發文期限
            itmReturn.setProperty("deadline_ctrl", "<label class='control-label'>" + itmReturn.getProperty("in_issue_deadline", "") + "</label>");
            //刪除文件
            itmReturn.setProperty("remove_ctrl", " ");
            //退收文發起人
            itmReturn.setProperty("reject_ctrl", " ");
            //更多功能
            cfg.MoreActionButtons = " ";

            itmReturn.setProperty("deadline_label", "<label for='in_issue_deadline' class='control-label'>發文期限</label>");

            cfg.is_view = true;

            //發文-行政主任特殊權限
            AppendLaunchEDManagerRole(cfg, itmReturn);
            cfg.can_edit_launch_verify = itmReturn.getProperty("can_edit_verify", "") == "1";

            //簽審流程表
            itmReturn.setProperty("inn_table", FlowTable(cfg, cfg.itmDoc, cfg.doc_id));

            //簽審紀錄表
            itmReturn.setProperty("inn_assigns", AssignmentTable(cfg, cfg.itmDoc, cfg.doc_id, false));

            //文件附檔
            itmReturn.setProperty("inn_files", FileTable(cfg, cfg.itmDoc, cfg.doc_id));
        }

        private bool HasDocPermission(TConfig cfg, string identity_name)
        {
            string sql = @"
                SELECT
                	t2.id
                FROM
                	[IDENTITY] t1 WITH(NOLOCK)
                INNER JOIN
                	[MEMBER] t2 WITH(NOLOCK)
                	ON t2.source_id = t1.id
                WHERE
                	t1.name = '{#identity_name}'
                	AND t2.related_id = '{#identity_id}'
            ";

            sql = sql.Replace("{#identity_name}", identity_name)
                .Replace("{#identity_id}", cfg.strIdentityId);

            Item itmIdentity = cfg.inn.applySQL(sql);

            return !itmIdentity.isError() && itmIdentity.getResult() != "";
        }

        private void RemoveDoc(TConfig cfg, Item itmReturn)
        {
            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            string sql = "UPDATE Document SET"
                + "  owned_by_id = '" + cfg.strIdentityId + "'"
                + ", created_by_id = '" + cfg.strUserId + "'"
                + ", modified_by_id = '" + cfg.strUserId + "'"
                + " WHERE id = '" + cfg.doc_id + "'";

            cfg.inn.applySQL(sql);

            string aml = "";

            aml = "<AML>";
            aml += "<Item type='Document' action='delete' where=\"[Document].id='" + cfg.doc_id + "'\">";
            aml += "</Item></AML>";

            Item item = cfg.inn.applyAML(aml);

            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);
        }

        private void DocList(TConfig cfg, Item itmReturn)
        {
            //文管確認
            cfg.is_doc_dispatcher = HasDocPermission(cfg, "ACT_DOC_Dispatcher");

            var status = itmReturn.getProperty("status", "");
            var itmInfo = GetDocListInfo(cfg, status);

            var page_cond_value = itmInfo.getProperty("page_cond_value", "");
            var page_cond_label = itmInfo.getProperty("page_cond_label", "");

            itmReturn.setProperty("page_cond_label", page_cond_label);

            var list = new List<Item>();
            MergeItem(list, GetDocListSource1(cfg, page_cond_value));
            MergeItem(list, GetDocListSource2(cfg, page_cond_value));
            var rows = MapDocRows(cfg, list);

            for (int i = 0; i < rows.Count; i++)
            {
                var row = rows[i];
                ResetDocRow(cfg, row);

                Item item = row.value;
                item.setType("Document");
                item.setProperty("doc_link", "<a href='" + row.doc_url + "'>" + row.name + "</a>");
                item.setProperty("issue_status", row.issue_status);
                item.setProperty("activity", row.in_current_activity + "<BR>" + row.assigns);
                item.setProperty("ver_link", row.ver_url);

                itmReturn.addRelationship(item);
            }
        }

        private void ResetDocRow(TConfig cfg, TDocRow row)
        {
            var can_vote = false;
            var contents = new StringBuilder();

            row.issue_status = "<span style='color: blue'>已進流程</span>";

            row.ver_url = DocUrl_SignOffDocument(cfg, row);
            row.ver_url = "<a href='" + row.ver_url + "'>" + "<i class='fa fa-list-alt'></i>" + "</a>";

            for (int i = 0; i < row.itmAssignments.Count; i++)
            {
                var item = row.itmAssignments[i];
                var vote_id = item.getProperty("vote_id", "");
                var vote_name = item.getProperty("vote_name", "");
                var vote_closed_on = item.getProperty("vote_closed_on", "");
                var not_closed = vote_closed_on == "";

                if (vote_id == cfg.strIdentityId && not_closed)
                {
                    can_vote = true;
                }

                if (!not_closed) vote_name += "(V)";

                contents.Append(" - " + vote_name + " <BR>");
            }

            if (cfg.is_doc_dispatcher && row.in_current_activity == "文管確認")
            {
                row.doc_url = DocUrl_ChangeUndertaker(cfg, row);
                row.issue_status = "<span style='color: red'>重新指派</span>";
            }
            else if (can_vote)
            {
                row.doc_url = DocUrl_VoteDocument(cfg, row);
            }
            else
            {
                row.doc_url = DocUrl_ViewDocument(cfg, row);

                if (row.in_current_activity == "")
                {
                    row.ver_url = "";
                    switch (row.in_issue_status)
                    {
                        case "": //草稿
                            row.doc_url = DocUrl_EditDocument(cfg, row);
                            row.issue_status = "草稿";
                            break;

                        case "rejected": //收文: 被[文管確認者]退回
                            row.doc_url = DocUrl_EditDocument(cfg, row);
                            row.issue_status = "<span style='color: red'>退回</span>";
                            break;

                        case "created": //收文: 等待確認
                            if (cfg.is_doc_dispatcher)
                            {
                                row.doc_url = DocUrl_EditUndertaker(cfg, row);
                                row.issue_status = "<span style='color: red'>等待確認</span>";
                            }
                            else
                            {
                                row.issue_status = "<span style='color: red'>等待確認</span>";
                            }
                            break;

                        case "waiting": //收文: 等待指派承辦組員
                            row.doc_url = DocUrl_EditUndertaker(cfg, row);
                            row.issue_status = "<span style='color: red'>等待指派</span>";
                            break;
                    }
                }
            }

            row.assigns = contents.ToString();
        }


        private string DocUrl_EditUndertaker(TConfig cfg, TDocRow row)
        {
            return "c.aspx"
                + "?page=DocumentDispatch.html"
                + "&method=In_Document_Edit"
                + "&doc_id=" + row.id
                + "&scene=dispatch_page";
        }

        private string DocUrl_ChangeUndertaker(TConfig cfg, TDocRow row)
        {
            return "c.aspx"
                + "?page=DocumentDispatch2.html"
                + "&method=In_Document_Edit"
                + "&doc_id=" + row.id
                + "&scene=dispatch_page2";
        }

        private string DocUrl_ViewDocument(TConfig cfg, TDocRow row)
        {
            return "c.aspx"
                + "?page=DocumentView2.html"
                + "&method=In_Document_Edit"
                + "&doc_id=" + row.id
                + "&scene=view_page";
        }

        private string DocUrl_EditDocument(TConfig cfg, TDocRow row)
        {
            return "c.aspx"
                + "?page=DocumentEdit.html"
                + "&method=In_Get_SingleItemInfoGeneral"
                + "&itemtype=Document"
                + "&itemid=" + row.id
                + "&type=direct"
                + "&fetchproperty=4"
                + "&inn_editor_type=edit";
        }

        private string DocUrl_VoteDocument(TConfig cfg, TDocRow row)
        {
            return "c.aspx"
                + "?page=DocVoteView.html"
                + "&method=In_GetAct_N"
                + "&itemtype=Document"
                + "&itemid=" + row.id;
        }

        private string DocUrl_SignOffDocument(TConfig cfg, TDocRow row)
        {
            return "c.aspx"
                + "?page=SignoffView.html"
                + "&method=In_GetSignOff_N"
                + "&itemtype=Document"
                + "&itemid=" + row.id
                + "&mode=latest"
                + "&workflow_process_id=" + row.in_wfp_id;
        }

        private Item GetDocListSource1(TConfig cfg, string cond)
        {
            string sql = @"
                SELECT 
                	t1.id
					, CONVERT(VARCHAR, DATEADD(HOUR, 8, t1.in_issue_date), 111)     AS 'issue_date'
					, CONVERT(VARCHAR, DATEADD(HOUR, 8, t1.in_issue_deadline), 111) AS 'issue_deadline'
					, t1.item_number
					, t1.in_issue_type
					, t1.in_issue_no
					, t1.name
					, t1.in_issue_status
					, t1.in_wfp_id
					, t1.in_wfp_state
					, t1.in_current_activity
					, t1.in_current_activity_id
					, t1.current_state
                	, t2.last_name                AS 'created_name'
					, t3.label                    AS 'in_issue_type_label'
					, t12.related_id              AS 'vote_id'
					, t12.comments                AS 'vote_comments'
                    , t12.closed_on               AS 'vote_closed_on'
					, t14.last_name               AS 'vote_name'
                FROM 
                	[DOCUMENT] t1 WITH(NOLOCK)
                INNER JOIN
                    [USER] t2 WITH(NOLOCK)
                    ON t2.id = t1.created_by_id
				INNER JOIN
					[VU_Doc_IssueType] t3
					ON t3.value = t1.in_issue_type
				LEFT OUTER JOIN
					Workflow_Process t11 WITH(NOLOCK) 
	                ON t11.id = t1.in_wfp_id
				LEFT OUTER JOIN
					[ACTIVITY_ASSIGNMENT] t12 WITH(NOLOCK) 
	                ON t12.source_id = t1.in_current_activity_id
				LEFT OUTER JOIN
					[IDENTITY] t13 WITH(NOLOCK) 
	                ON t13.id = t12.related_id
				LEFT OUTER JOIN
					[USER] t14 WITH(NOLOCK) 
	                ON t14.login_name = t13.in_number
                WHERE 
                    t1.is_current = 1
                	AND t1.created_by_id = '{#user_id}'
                    {#cond}
                ORDER BY
                	t1.item_number DESC
					, t13.id
            ";

            sql = sql.Replace("{#user_id}", cfg.strUserId)
                .Replace("{#identity_id}", cfg.strIdentityId)
                .Replace("{#cond}", cond);

            return cfg.inn.applySQL(sql);
        }


        private Item GetDocListSource2(TConfig cfg, string cond)
        {
            string sql = @"
                SELECT 
                	t1.id
					, CONVERT(VARCHAR, DATEADD(HOUR, 8, t1.in_issue_date), 111)     AS 'issue_date'
					, CONVERT(VARCHAR, DATEADD(HOUR, 8, t1.in_issue_deadline), 111) AS 'issue_deadline'
					, t1.item_number
					, t1.in_issue_type
					, t1.in_issue_no
					, t1.name
					, t1.in_issue_status
					, t1.in_wfp_id
					, t1.in_wfp_state
					, t1.in_current_activity
					, t1.in_current_activity_id
					, t1.current_state
                	, t2.last_name                AS 'created_name'
					, t3.label                    AS 'in_issue_type_label'
					, t12.related_id              AS 'vote_id'
					, t12.comments                AS 'vote_comments'
                    , t12.closed_on               AS 'vote_closed_on'
					, t14.last_name               AS 'vote_name'
                FROM 
                	[DOCUMENT] t1 WITH(NOLOCK)
                INNER JOIN
                    [USER] t2 WITH(NOLOCK)
                    ON t2.id = t1.created_by_id
				INNER JOIN
					[VU_Doc_IssueType] t3
					ON t3.value = t1.in_issue_type
				LEFT OUTER JOIN
					Workflow_Process t11 WITH(NOLOCK) 
	                ON t11.id = t1.in_wfp_id
				LEFT OUTER JOIN
					[ACTIVITY_ASSIGNMENT] t12 WITH(NOLOCK) 
	                ON t12.source_id = t1.in_current_activity_id
				LEFT OUTER JOIN
					[IDENTITY] t13 WITH(NOLOCK) 
	                ON t13.id = t12.related_id
				LEFT OUTER JOIN
					[USER] t14 WITH(NOLOCK) 
	                ON t14.login_name = t13.in_number
                WHERE 
                    t1.is_current = 1
                	AND t1.in_wf_act0_identity = '{#identity_id}'
                    AND t1.in_current_activity = N'文管確認'
                ORDER BY
                	t1.item_number DESC
					, t13.id
            ";

            sql = sql.Replace("{#user_id}", cfg.strUserId)
                .Replace("{#identity_id}", cfg.strIdentityId)
                .Replace("{#cond}", cond);

            return cfg.inn.applySQL(sql);
        }

        private List<TDocRow> MapDocRows(TConfig cfg, List<Item> items)
        {
            var list = new List<TDocRow>();

            int count = items.Count();

            for (int i = 0; i < count; i++)
            {
                Item item = items[i];
                string item_number = item.getProperty("item_number", "");

                var row = list.Find(x => x.item_number == item_number);
                if (row == null)
                {
                    row = new TDocRow
                    {
                        id = item.getProperty("id", ""),
                        name = item.getProperty("name", ""),
                        in_wfp_id = item.getProperty("in_wfp_id", ""),
                        in_current_activity = item.getProperty("in_current_activity", ""),
                        in_current_activity_id = item.getProperty("in_current_activity_id", ""),
                        in_issue_status = item.getProperty("in_issue_status", ""),
                        item_number = item_number,
                        value = item,
                        itmAssignments = new List<Item>(),
                    };
                    list.Add(row);
                }

                row.itmAssignments.Add(item);
            }

            return list.OrderByDescending(x => x.item_number).ToList();
        }

        private class TDocRow
        {
            public string id { get; set; }
            public string name { get; set; }
            public string item_number { get; set; }
            public string in_wfp_id { get; set; }
            public string in_current_activity { get; set; }
            public string in_current_activity_id { get; set; }
            public string in_issue_status { get; set; }
            public Item value { get; set; }
            public List<Item> itmAssignments { get; set; }

            public string doc_url { get; set; }
            public string ver_url { get; set; }

            public string assigns { get; set; }
            public string issue_status { get; set; }
        }

        private Item GetDocListInfo(TConfig cfg, string status)
        {
            Item itmResult = cfg.inn.newItem();

            string cond = "";
            string label = "";

            switch (status)
            {
                case "draft":
                    cond = "AND ISNULL(t1.in_current_activity, '') = ''";
                    label = "草稿";
                    break;

                case "launched":
                    cond = "AND ISNULL(t1.in_issue_status, '') = '" + status + "'";
                    label = "發文";
                    break;

                case "rejected":
                    cond = "AND ISNULL(t1.in_issue_status, '') = '" + status + "'";
                    label = "退回";
                    break;

                case "created":
                    cond = "AND ISNULL(t1.in_issue_status, '') = '" + status + "'";
                    label = "等待確認";
                    break;

                case "waiting":
                    cond = "AND ISNULL(t1.in_issue_status, '') = '" + status + "'";
                    label = "需指派";
                    break;

            }

            string page_cond_label = "<label class='inn_page_cond' onclick='RemoveCond()'>"
                + label
                + " <i class='fa fa-remove'></i></label>";

            if (label == "") page_cond_label = "";

            itmResult.setProperty("page_cond_value", cond);
            itmResult.setProperty("page_cond_label", page_cond_label);

            return itmResult;
        }

        private void ToFixDocument(TConfig cfg, Item itmReturn)
        {
            AppendDocInfo(cfg, itmReturn, setReturnData: false);

            string in_issue_no = itmReturn.getProperty("in_issue_no", "");
            string in_issue_urgency = itmReturn.getProperty("in_issue_urgency", "");
            string name = itmReturn.getProperty("name", "");

            string sql = "UPDATE [Document] SET"
                + "  keyed_name = item_number + ' ' + N'" + name + "'"
                + ", in_issue_status = 'created'"
                + ", in_issue_no = N'" + in_issue_no + "'"
                + ", in_issue_urgency = N'" + in_issue_urgency + "'"
                + ", name = N'" + name + "'"
                + " WHERE id = '" + cfg.doc_id + "'";

            Item itmResult = cfg.inn.applySQL(sql);
        }

        private void ToReject(TConfig cfg, Item itmReturn)
        {
            UpdateReject(cfg, itmReturn);
            SendEmail(cfg, "receive_reject");
        }

        private void UpdateReject(TConfig cfg, Item itmReturn)
        {
            string in_issue_note = itmReturn.getProperty("in_issue_note", "");

            string sql = "UPDATE [Document] SET"
                + "  in_issue_status = 'rejected'"
                + ", in_issue_note = N'" + in_issue_note + "'"
                + " WHERE id = '" + cfg.doc_id + "'";

            Item itmResult = cfg.inn.applySQL(sql);
        }

        private void RejectModal(TConfig cfg, Item itmReturn)
        {
            Item itmDoc = cfg.inn.newItem("Document", "get");
            itmDoc.setProperty("id", cfg.doc_id);
            itmDoc = itmDoc.apply();

            if (itmDoc.isError() || itmDoc.getResult() == "")
            {
                return;
            }
        }

        private void CoworkModal(TConfig cfg, Item itmReturn)
        {
            Item itmDoc = cfg.inn.newItem("Document", "get");
            itmDoc.setProperty("id", cfg.doc_id);
            itmDoc = itmDoc.apply();

            if (itmDoc.isError() || itmDoc.getResult() == "")
            {
                return;
            }

            //取得組別成員
            Item items = GetAllSectMembers(cfg, "");
            //已選取的會辦人員
            var selected_keys = GetCoworkerKeys(cfg);
            //轉型
            var rows = MapCoworkers(cfg, GetSectMemberList(cfg, items), selected_keys);
            //轉換為 Json
            itmReturn.setProperty("coworker_json", Newtonsoft.Json.JsonConvert.SerializeObject(rows));
        }

        private List<TCW> MapCoworkers(TConfig cfg, List<Item> list, List<string> selected_keys)
        {
            List<TCW> result = new List<TCW>();
            for (int i = 0; i < list.Count; i++)
            {
                var item = list[i];

                var row = new TCW
                {
                    sect_id = item.getProperty("sect_id", ""),
                    sect_desc = item.getProperty("sect_desc", ""),
                    id = item.getProperty("id", ""),
                    name = item.getProperty("name", ""),
                    role = item.getProperty("role", ""),
                };

                string key = row.sect_id + "-" + row.id;
                row.selected = selected_keys.Contains(key);
                result.Add(row);
            }
            return result;
        }

        private List<string> GetCoworkerKeys(TConfig cfg)
        {
            var result = new List<string>();

            string sql = @"
                SELECT DISTINCT
	                in_dept_id
	                , related_id 
                FROM 
	                IN_DOCUMENT_COWORK_MEMBER WITH(NOLOCK) 
                WHERE 
	                source_id = '{#doc_id}'
            ";

            sql = sql.Replace("{#doc_id}", cfg.doc_id);
            Item items = cfg.inn.applySQL(sql);
            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_dept_id = item.getProperty("in_dept_id", "");
                string related_id = item.getProperty("related_id", "");
                string key = in_dept_id + "-" + related_id;
                result.Add(key);
            }

            return result;
        }

        private List<Item> GetSectMemberList(TConfig cfg, Item items)
        {
            var result = new List<Item>();

            int count = items.getItemCount();
            string last_sect = "";
            string type_name = "inn_user";

            for (int i = 0; i < count; i++)
            {
                Item itmSrc = items.getItemByIndex(i);
                string mmbr_id = itmSrc.getProperty("mmbr_id", "");
                string sect_name = itmSrc.getProperty("sect_name", "");
                if (sect_name != last_sect)
                {
                    last_sect = sect_name;

                    Item itmLeader = cfg.inn.newItem();
                    itmLeader.setType(type_name);
                    itmLeader.setProperty("role", "<label style=\"color: red\">主管<label>");
                    itmLeader.setProperty("sect_id", itmSrc.getProperty("sect_id", ""));
                    itmLeader.setProperty("sect_desc", itmSrc.getProperty("sect_desc", ""));
                    itmLeader.setProperty("id", itmSrc.getProperty("leader_id", ""));
                    itmLeader.setProperty("name", itmSrc.getProperty("leader_name", ""));
                    result.Add(itmLeader);
                }

                Item itmMember = cfg.inn.newItem();
                itmMember.setType(type_name);
                itmMember.setProperty("role", "組員");
                itmMember.setProperty("sect_id", itmSrc.getProperty("sect_id", ""));
                itmMember.setProperty("sect_desc", itmSrc.getProperty("sect_desc", ""));
                itmMember.setProperty("id", mmbr_id);
                itmMember.setProperty("name", itmSrc.getProperty("mmbr_name", ""));
                result.Add(itmMember);
            }

            return result;
        }

        private void AppendSectMembers(TConfig cfg, List<Item> list, Item itmReturn)
        {
            for (int i = 0; i < list.Count; i++)
            {
                var item = list[i];
                itmReturn.addRelationship(item);
            }
        }

        private void ToCowork(TConfig cfg, Item itmReturn)
        {
            var rows = JsonDsrlList<TCW>(cfg, itmReturn.getProperty("ids", ""));
            if (rows == null || rows.Count == 0)
            {
                throw new Exception("會辦資料異常");
            }

            //重建會辦組員
            RebuildCoworkerMembers(cfg, rows);

            //重建會辦主管
            RebuildCoworkerLeaders(cfg, "");

            //重建會辦簽審流程
            RebuildCoworkerFlow(cfg);
        }

        //重建會辦組員
        private void RebuildCoworkerMembers(TConfig cfg, List<TCW> rows)
        {
            string sql_delete = "DELETE FROM In_Document_Cowork_Member WHERE source_id = '" + cfg.doc_id + "'";
            cfg.inn.applySQL(sql_delete);

            for (int i = 0; i < rows.Count; i++)
            {
                var row = rows[i];
                AddCoworker(cfg, "In_Document_Cowork_Member", row.sect_id, row.sect_desc, row.id, row.name);
            }
        }

        //重建會辦主管
        private void RebuildCoworkerLeaders(TConfig cfg, string in_wf_act3_identity)
        {
            string sql_delete = "DELETE FROM In_Document_Cowork_Manager WHERE source_id = '" + cfg.doc_id + "'";
            cfg.inn.applySQL(sql_delete);

            var itmLeaders = GetCoworkerLeaders(cfg);
            var leaders = MapLeaders(cfg, itmLeaders);
            for (int i = 0; i < leaders.Count; i++)
            {
                var leader = leaders[i];
                var sects = string.Join(", ", leader.sects);
                if (leader.id != in_wf_act3_identity)
                {
                    //不為承辦單位主管
                    AddCoworker(cfg, "In_Document_Cowork_Manager", leader.sect_id, sects, leader.id, leader.name);
                }
            }
        }

        //重建會辦簽審流程
        private void RebuildCoworkerFlow(TConfig cfg)
        {
            //清除會辦流程
            string sql_delete1 = "DELETE FROM IN_DOCUMENT_WORKFLOW WHERE source_id = '" + cfg.doc_id + "' AND in_code = 'cowork'";
            cfg.inn.applySQL(sql_delete1);

            Item itmMembers = cfg.inn.applySQL("SELECT id FROM IN_DOCUMENT_COWORK_MEMBER WITH(NOLOCK) WHERE source_id = '" + cfg.doc_id + "'");
            Item itmLeaders = cfg.inn.applySQL("SELECT id FROM IN_DOCUMENT_COWORK_MANAGER WITH(NOLOCK) WHERE source_id = '" + cfg.doc_id + "'");

            int cnt_mbr = itmMembers.getItemCount();
            int cnt_ldr = itmLeaders.getItemCount();

            string in_mode_cw = cnt_mbr > 0 ? "1" : "";
            string in_mode_cl = cnt_ldr > 0 ? "1" : "";

            if (cnt_mbr <= 0)
            {
                //無會辦組員，清除會辦單位主管資料
                string sql_delete2 = "DELETE FROM IN_DOCUMENT_COWORK_MANAGER WHERE source_id = '" + cfg.doc_id + "'";
                cfg.inn.applySQL(sql_delete2);

                in_mode_cl = "";
            }
            else
            {
                var f31_row = new TWorkFlow
                {
                    label = "會辦人員",
                    code = "cowork",
                    group = "310",
                    serial = "1",
                    edit = "1",
                    idt_dept_id = "",
                    idt_dept_name = "會辦人員",
                    idt_mmbr_id = "",
                    idt_mmbr_name = "會辦人員",
                    in_need_verify = "1",
                };

                AppendDocWorkFlow(cfg, f31_row);
            }

            string sql_update = "UPDATE [Document] SET"
                + "  in_mode_cw = '" + in_mode_cw + "'"
                + ", in_mode_cl = '" + in_mode_cl + "'"
                + " WHERE id = '" + cfg.doc_id + "'";

            cfg.inn.applySQL(sql_update);
        }

        private List<TLeader> MapLeaders(TConfig cfg, Item items)
        {
            var list = new List<TLeader>();
            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string leader_id = item.getProperty("leader_id", "");
                string sect_desc = item.getProperty("sect_desc", "");
                var row = list.Find(x => x.id == leader_id);
                if (row == null)
                {
                    row = new TLeader
                    {
                        id = leader_id,
                        name = item.getProperty("leader_name", ""),
                        sect_id = item.getProperty("sect_id", ""),
                        sect_desc = sect_desc,
                        sects = new List<string>(),
                    };
                    list.Add(row);
                }
                row.sects.Add(sect_desc);
            }
            return list;
        }

        private Item GetCoworkerLeaders(TConfig cfg)
        {
            string sql = @"
                SELECT DISTINCT
	                t2.id              AS 'sect_id'
	                , t2.description   AS 'sect_desc'
                    , t3.id            AS 'leader_id'
	                , t4.last_name     AS 'leader_name'
                FROM 
	                In_Document_Cowork_Member t1 WITH(NOLOCK) 
                INNER JOIN
	                [IDENTITY] t2 WITH(NOLOCK)
	                ON t2.id = t1.in_dept_id
                INNER JOIN
	                [IDENTITY] t3 WITH(NOLOCK)
	                ON t3.id = t2.owned_by_id
                INNER JOIN
	                [USER] t4 WITH(NOLOCK)
	                ON t4.login_name = t3.in_number
                WHERE
	                t1.source_id = '{#doc_id}'
            ";

            sql = sql.Replace("{#doc_id}", cfg.doc_id);

            return cfg.inn.applySQL(sql);
        }

        private Item GetSectMembers(TConfig cfg, string sect_id)
        {
            string sql = @"
                SELECT DISTINCT
	                t1.id            AS 'dept_id'
	                , t1.owned_by_id AS 'leader_id'
	                , t2.related_id  AS 'mbr_id'
	                , t3.name        AS 'mbr_name'
	                , t4.name        AS 'leader_name'
                FROM
	                [IDENTITY] t1 WITH(NOLOCK)
                INNER JOIN
	                [MEMBER] t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                INNER JOIN
	                [IDENTITY] t3 WITH(NOLOCK)
	                ON t3.id = t2.related_id
                LEFT OUTER JOIN
	                [IDENTITY] t4 WITH(NOLOCK)
	                ON t4.id = t1.owned_by_id
                WHERE
	                t1.id = '{#sect_id}'
	                AND t3.in_number NOT IN ('00000', 'M001', '')
	                AND t3.id <> t1.owned_by_id
            ";

            sql = sql.Replace("{#sect_id}", sect_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        //取得組別成員
        private Item GetAllSectMembers(TConfig cfg, string identity_id)
        {
            string cond = identity_id == ""
                ? ""
                : "AND t4.id <> '" + identity_id + "'";

            string sql = @"
                SELECT
                	t1.id            AS 'sect_id'
                	, t1.name        AS 'sect_name'
                	, t1.description AS 'sect_desc'
                	, t2.id                              AS 'leader_id'
                	, REPLACE(t2.name, t2.in_number, '') AS 'leader_name'
					, t4.id                              AS 'mmbr_id'
					, REPLACE(t4.name, t4.in_number, '') AS 'mmbr_name'
					, t4.in_number                       AS 'mmbr_sno'
                FROM
                	[IDENTITY] t1 WITH(NOLOCK)
                LEFT OUTER JOIN
                	[IDENTITY] t2 WITH(NOLOCK)
                	ON t2.id = t1.owned_by_id
                INNER JOIN
                	[MEMBER] t3 WITH(NOLOCK)
                	ON t3.source_id = t1.id
				INNER JOIN
					[IDENTITY] t4 WITH(NOLOCK)
					ON t4.id = t3.related_id
                WHERE
                	ISNULL(t1.in_is_org, 0) = 1
					AND t4.in_number NOT IN ('00000', 'M001', '')
					AND t4.id <> t1.owned_by_id
					{#cond}
                ORDER BY
                	t1.id
					, t4.in_number
            ";

            sql = sql.Replace("{#doc_id}", cfg.doc_id)
                .Replace("{#cond}", cond);

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        private void ToUndertaker(TConfig cfg, Item itmReturn)
        {
            AppendDocInfo(cfg, itmReturn, setReturnData: false);

            Item itmDoc = cfg.itmDoc;
            string in_wfp_state = itmDoc.getProperty("in_wfp_state", "");
            string in_issue_deadline = itmDoc.getProperty("in_issue_deadline", "");
            string in_wf_act2_identity = itmDoc.getProperty("in_wf_act2_identity", "");
            string in_wf_act3_identity = itmDoc.getProperty("in_wf_act3_identity", "");

            if (in_issue_deadline == "") throw new Exception("請輸入發文期限");
            if (in_wf_act2_identity == "") throw new Exception("請先設定承辦人員");
            if (in_wf_act3_identity == "") throw new Exception("請先設定承辦單位主管");
            if (in_wfp_state != "") throw new Exception("文件已啟動流程");


            //刷新文件參數
            string sql = "UPDATE [Document] SET"
                + "  in_issue_status = 'dispatched'"
                + ", in_issue_viewer = in_wf_act2_identity"
                + " WHERE id = '" + cfg.doc_id + "'";

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            cfg.inn.applySQL(sql);


            //重整會辦人員
            ResetCoworkers(cfg);


            //啟動流程
            Item itmFlow = cfg.inn.newItem("");
            itmFlow.setType("Document");
            itmFlow.setProperty("itemtype", "Document");
            itmFlow.setProperty("itemid", cfg.doc_id);
            itmFlow.apply("In_AddtoWorkflow_App");
        }

        //重整會辦人員
        private void ResetCoworkers(TConfig cfg)
        {
            string in_wf_act2_identity = cfg.itmDoc.getProperty("in_wf_act2_identity", "");
            string in_wf_act3_identity = cfg.itmDoc.getProperty("in_wf_act3_identity", "");

            //從[會辦組員]移除承辦人員
            string sql_delete1 = "DELETE FROM IN_DOCUMENT_COWORK_MEMBER"
                + " WHERE source_id = '" + cfg.doc_id + "'"
                + " AND related_id = '" + in_wf_act2_identity + "'";
            cfg.inn.applySQL(sql_delete1);

            //從[會辦主管]移除承辦單位主管
            string sql_delete2 = "DELETE FROM IN_DOCUMENT_COWORK_MANAGER"
                + " WHERE source_id = '" + cfg.doc_id + "'"
                + " AND related_id = '" + in_wf_act3_identity + "'";
            cfg.inn.applySQL(sql_delete2);

            RebuildCoworkerFlow(cfg);
        }

        private void ChangeNeedVerify(TConfig cfg, Item itmReturn)
        {
            string dwf_id = itmReturn.getProperty("dwf_id", "");

            Item itmOld = cfg.inn.newItem("In_Document_WorkFlow", "get");
            itmOld.setProperty("id", dwf_id);
            itmOld = itmOld.apply();

            string old_code = itmOld.getProperty("in_code", "");
            string old_need_verify = itmOld.getProperty("in_need_verify", "");
            string new_need_verify = itmReturn.getProperty("in_need_verify", "");

            string sql = "";
            Item itmResult = null;

            if (new_need_verify == "A")
            {
                sql = @"
                    UPDATE In_Document_WorkFlow SET
	                    in_need_verify = 'A'
                    WHERE
	                    source_id = '{#doc_id}'
	                    AND in_code = '{#in_code}'
                    ";

                sql = sql.Replace("{#doc_id}", cfg.doc_id)
                    .Replace("{#dwf_id}", dwf_id)
                    .Replace("{#in_code}", old_code);

                itmResult = cfg.inn.applySQL(sql);
            }
            else
            {
                sql = "UPDATE In_Document_WorkFlow SET"
                    + " in_need_verify = '" + new_need_verify + "'"
                    + " WHERE id = '" + dwf_id + "'";
                itmResult = cfg.inn.applySQL(sql);
            }

            if (old_need_verify == "A" && new_need_verify != "A")
            {
                sql = @"
                    UPDATE In_Document_WorkFlow SET
	                    in_need_verify = '0'
                    WHERE
	                    source_id = '{#doc_id}'
	                    AND in_code = '{#in_code}'
	                    AND id <> '{#dwf_id}'
	                    AND ISNULL(in_need_verify, '') = 'A'
                    ";

                sql = sql.Replace("{#doc_id}", cfg.doc_id)
                    .Replace("{#dwf_id}", dwf_id)
                    .Replace("{#in_code}", old_code);

                itmResult = cfg.inn.applySQL(sql);
            }

            switch (old_code)
            {
                case "edmanager"://刷新[行政主任]自動節點
                    RefreshVerifyMode(cfg, cfg.doc_id, new_need_verify, "in_mode_ad");
                    break;

                case "chairman"://刷新[會長]自動節點
                    RefreshVerifyMode(cfg, cfg.doc_id, new_need_verify, "in_mode_cm");
                    break;

                case "deputy"://刷新副秘自動節點
                    RefreshDSVerifyMode(cfg, cfg.doc_id, old_code);
                    break;

                case "special1"://刷新[客製節點1]自動節點
                    RefreshVerifyMode(cfg, cfg.doc_id, new_need_verify, "in_mode_sp1");
                    break;
            }
        }

        //刷新自動節點
        private void RefreshVerifyMode(TConfig cfg, string doc_id, string new_need_verify, string prop)
        {
            string value = "1";

            if (new_need_verify != "1")
            {
                value = "";
            }

            string sql = "UPDATE [Document] SET " + prop + " = '" + value + "' WHERE id = '" + doc_id + "'";

            cfg.inn.applySQL(sql);
        }

        //刷新[副秘]自動節點
        private void RefreshDSVerifyMode(TConfig cfg, string doc_id, string in_code)
        {
            string sql = "SELECT * FROM In_Document_WorkFlow WITH(NOLOCK)"
                + " WHERE source_id = '{#doc_id}' AND in_code = '{#in_code}'"
                + " ORDER BY in_serial"
                ;

            sql = sql.Replace("{#doc_id}", doc_id)
                .Replace("{#in_code}", in_code);

            Item items = cfg.inn.applySQL(sql);
            if (items.isError() || items.getResult() == "")
            {
                throw new Exception("副秘流程有誤");
            }

            var count = items.getItemCount();
            var list = new List<string>();
            var is_any = false;

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_idt_mmbr_id = item.getProperty("in_idt_mmbr_id", "");
                string verify = item.getProperty("in_need_verify", "");

                switch (verify)
                {
                    case "A":
                        is_any = true;
                        list.Add(in_idt_mmbr_id);
                        break;

                    case "1":
                        list.Add(in_idt_mmbr_id);
                        break;

                }
            }

            var vcnt = list.Count;
            var in_mode_ds = "";
            var in_wf_act41_identity = vcnt > 0 ? list[0] : "";
            var in_wf_act42_identity = vcnt > 1 ? list[1] : "";

            if (vcnt <= 0)
            {
                //跳過
                in_mode_ds = "";
            }
            else if (vcnt == 1)
            {
                //指定
                in_mode_ds = "3";
            }
            else
            {
                //都必簽
                in_mode_ds = "2";
            }

            if (is_any)
            {
                //Any
                in_mode_ds = "1";
            }

            sql = "UPDATE [Document] SET"
                + "  in_mode_ds = '" + in_mode_ds + "'"
                + ", in_wf_act41_identity = '" + in_wf_act41_identity + "'"
                + ", in_wf_act42_identity = '" + in_wf_act42_identity + "'"
                + " WHERE id = '" + doc_id + "'";

            Item itmResult = cfg.inn.applySQL(sql);
        }

        //變更[發文期限]
        private void ChangeDeadline(TConfig cfg, Item itmReturn)
        {
            string doc_id = itmReturn.getProperty("doc_id", "");
            string in_issue_deadline = itmReturn.getProperty("in_issue_deadline", "");

            string sql = "";

            if (in_issue_deadline == "")
            {
                //throw new Exception("請輸入發文期限

                sql = "UPDATE [Document] SET"
                    + "  in_issue_deadline = NULL"
                    + " WHERE id = '" + doc_id + "'";
            }
            else
            {
                DateTime dt = DtmVal(in_issue_deadline, -8);

                sql = "UPDATE [Document] SET"
                    + "  in_issue_deadline = '" + dt.ToString("yyyy-MM-dd HH:mm:ss") + "'"
                    + " WHERE id = '" + doc_id + "'";
            }

            Item itmResult = cfg.inn.applySQL(sql);
        }

        private class TAssignModel
        {
            public string ui_dept_id { get; set; }
            public string ui_mngr_id { get; set; }
            public string ui_mmbr_id { get; set; }
            public string type { get; set; }
        }

        //變更[承辦組員]
        private void ChangeUndertaker(TConfig cfg, Item itmReturn)
        {
            var model = new TAssignModel
            {
                ui_dept_id = itmReturn.getProperty("dept_id", ""),
                ui_mngr_id = itmReturn.getProperty("mngr_id", ""),
                ui_mmbr_id = itmReturn.getProperty("mmbr_id", ""),
                type = itmReturn.getProperty("type", ""),
            };

            switch (model.type)
            {
                case "waiting"://等待指派
                    WaitingUndertaker(cfg, model, itmReturn);
                    SendEmail(cfg, "receive_assign");
                    break;

                default:
                    AssignUndertaker(cfg, model, itmReturn);
                    ToUndertaker(cfg, itmReturn);
                    break;
            }
        }

        //等待指派[承辦組員]
        private void WaitingUndertaker(TConfig cfg, TAssignModel model, Item itmReturn)
        {
            string sql = "SELECT [id], [name], [description], [in_number] FROM [Identity] WITH(NOLOCK) WHERE id = '{#id}'";
            Item itmDept = cfg.inn.applySQL(sql.Replace("{#id}", model.ui_dept_id));
            Item itmMngr = cfg.inn.applySQL(sql.Replace("{#id}", model.ui_mngr_id));

            if (itmDept.isError() || itmDept.getResult() == "") throw new Exception("查無部門組織資料");
            if (itmMngr.isError() || itmMngr.getResult() == "") throw new Exception("查無部門主管資料");

            sql = "SELECT TOP 1 id FROM [USER] WITH(NOLOCK) WHERE login_name = '" + itmMngr.getProperty("in_number", "") + "'";
            Item itmMngrUser = cfg.inn.applySQL(sql);
            if (itmMngrUser.isError() || itmMngrUser.getResult() == "") throw new Exception("查無部門主管使用者資料");

            string dept_id = itmDept.getProperty("id", "");
            string dept_desc = itmDept.getProperty("description", "");

            string mngr_id = itmMngr.getProperty("id", "");
            string mngr_keyed_name = itmMngr.getProperty("name", "");

            string mngr_user_id = itmMngrUser.getProperty("id", "");

            //變更[文件]主檔
            sql = "UPDATE [Document] SET"
                + "  in_issue_status = 'waiting'"
                + ", in_issue_viewer = '" + mngr_id + "'"
                + ", in_wf_act21_identity = '" + dept_id + "'"
                + ", in_wf_act3_identity = '" + mngr_id + "'"
                + ", in_wf_act3_label = N'" + mngr_keyed_name + "'"
                + " WHERE id = '" + cfg.doc_id + "'";

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            Item itmResult = cfg.inn.applySQL(sql);

            //承辦組員: 接受指派
            sql = "UPDATE In_Document_WorkFlow SET"
                + "  in_idt_dept_id = '{#dept_id}'"
                + ", in_idt_dept_name = N'{#dept_desc}'"
                + ", in_idt_mmbr_id = '{#id}'"
                + ", in_idt_mmbr_name = N'{#name}'"
                + " WHERE source_id = '" + cfg.doc_id + "'"
                + " AND in_code = '{#code}'";

            string sql1 = sql.Replace("{#code}", "undertaker")
                .Replace("{#dept_id}", dept_id)
                .Replace("{#dept_desc}", dept_desc)
                .Replace("{#id}", dept_id)
                .Replace("{#name}", "單位主管指定");

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql1);

            Item itmResult1 = cfg.inn.applySQL(sql1);


            //承辦單位主管
            string sql2 = sql.Replace("{#code}", "supervisor")
                .Replace("{#dept_id}", dept_id)
                .Replace("{#dept_desc}", dept_desc)
                .Replace("{#id}", itmMngr.getProperty("id", ""))
                .Replace("{#name}", itmMngr.getProperty("name", ""));

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql2);

            Item itmResult2 = cfg.inn.applySQL(sql2);
        }

        //變更[承辦組員]
        private void AssignUndertaker(TConfig cfg, TAssignModel model, Item itmReturn)
        {
            string sql = @"
                SELECT TOP 1
                	t1.id
                	, t1.name
                	, t1.description
                	, t1.in_number
                	, t2.id AS 'user_id'
                	, t2.last_name
                FROM 
                	[Identity] t1 WITH(NOLOCK) 
                LEFT OUTER JOIN
                	[USER] t2 WITH(NOLOCK)
                	ON t2.login_name = t1.in_number
                WHERE 
                	t1.id = '{#id}'
            ";

            Item itmDept = cfg.inn.applySQL(sql.Replace("{#id}", model.ui_dept_id));
            Item itmMngr = cfg.inn.applySQL(sql.Replace("{#id}", model.ui_mngr_id));
            Item itmMmbr = cfg.inn.applySQL(sql.Replace("{#id}", model.ui_mmbr_id));

            if (itmDept.isError() || itmDept.getResult() == "") throw new Exception("查無部門組織資料");
            if (itmMngr.isError() || itmMngr.getResult() == "") throw new Exception("查無部門主管資料");
            if (itmMmbr.isError() || itmMmbr.getResult() == "") throw new Exception("查無部門成員資料");

            string dept_id = itmDept.getProperty("id", "");
            string dept_desc = itmDept.getProperty("description", "");

            string mngr_id = itmMngr.getProperty("id", "");
            string mngr_keyed_name = itmMngr.getProperty("name", "");

            string mmbr_id = itmMmbr.getProperty("id", "");
            string mmbr_sno = itmMmbr.getProperty("in_number", "");
            string mmbr_name = itmMmbr.getProperty("last_name", "");
            string mmbr_user = itmMmbr.getProperty("user_id", "");
            string mmbr_keyed_name = itmMmbr.getProperty("name", "");

            var mngr_agent = FindUserAgent(cfg, mngr_id);
            var mmbr_agent = FindUserAgent(cfg, mmbr_id);

            var mngr_agent_note = "";
            if (mngr_agent.agent_identity_id != "")
            {
                mngr_id = mngr_agent.agent_identity_id;
                mngr_keyed_name = mngr_agent.agent_keyed_name;
                mngr_agent_note = "代理";
            }

            var mmbr_agent_note = "";
            if (mmbr_agent.agent_identity_id != "")
            {
                mmbr_id = mmbr_agent.agent_identity_id;
                mmbr_keyed_name = mmbr_agent.agent_keyed_name;
                mmbr_agent_note = "代理";
            }

            //變更[文件]主檔
            sql = "UPDATE [Document] SET"
                + "  in_issue_dept = N'" + dept_desc + "'"
                + ", in_issue_mmbr = N'" + mmbr_name + "'"
                + ", in_issue_sno = N'" + mmbr_sno + "'"
                + ", in_wf_act2_identity = '" + mmbr_id + "'"
                + ", in_wf_act2_label = N'" + mmbr_keyed_name + "'"
                + ", in_wf_act3_identity = '" + mngr_id + "'"
                + ", in_wf_act3_label = N'" + mngr_keyed_name + "'"
                + ", in_wf_act7_identity = '" + mmbr_id + "'"
                + ", in_wf_act7_label = N'" + mmbr_keyed_name + "'"
                + ", created_by_id = '" + mmbr_user + "'"
                + ", owned_by_id = '" + mmbr_id + "'"
                + " WHERE id = '" + cfg.doc_id + "'";

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            cfg.inn.applySQL(sql);


            //承辦組員: 接受指派
            sql = "UPDATE In_Document_WorkFlow SET"
                + "  in_idt_dept_id = '{#dept_id}'"
                + ", in_idt_dept_name = N'{#dept_desc}'"
                + ", in_idt_mmbr_id = '{#id}'"
                + ", in_idt_mmbr_name = N'{#name}'"
                + ", in_agent_note = N'{#agent}'"
                + " WHERE source_id = '" + cfg.doc_id + "'"
                + " AND in_code = '{#code}'";

            string sql1 = sql.Replace("{#code}", "undertaker")
                .Replace("{#dept_id}", dept_id)
                .Replace("{#dept_desc}", dept_desc)
                .Replace("{#id}", mmbr_id)
                .Replace("{#name}", mmbr_keyed_name)
                .Replace("{#agent}", mmbr_agent_note);

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql1);

            cfg.inn.applySQL(sql1);


            //承辦單位主管
            string sql2 = sql.Replace("{#code}", "supervisor")
                .Replace("{#dept_id}", dept_id)
                .Replace("{#dept_desc}", dept_desc)
                .Replace("{#id}", mngr_id)
                .Replace("{#name}", mngr_keyed_name)
                .Replace("{#agent}", mngr_agent_note);

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql2);

            cfg.inn.applySQL(sql2);


            //承辦組員: 發行
            string sql3 = sql.Replace("{#code}", "release")
                .Replace("{#dept_id}", dept_id)
                .Replace("{#dept_desc}", dept_desc)
                .Replace("{#id}", mmbr_id)
                .Replace("{#name}", mmbr_keyed_name);

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql3);

            cfg.inn.applySQL(sql3);
        }

        private void EditPage2(TConfig cfg, Item itmReturn)
        {
            itmReturn.setProperty("page_title", "文件訂正");

            //文件資訊
            AppendDocInfo(cfg, itmReturn, setReturnData: true);

            Item itmEmpty = cfg.inn.newItem();
            itmEmpty.setType("inn_level");
            itmEmpty.setProperty("value", "");
            itmEmpty.setProperty("label", "請選擇");
            itmReturn.addRelationship(itmEmpty);

            Item itmUrgencies = cfg.inn.applySQL("SELECT * FROM VU_Doc_LevelUrgency ORDER BY sort_order");
            if (!itmUrgencies.isError() && itmUrgencies.getResult() != "")
            {
                int count = itmUrgencies.getItemCount();
                for (int i = 0; i < count; i++)
                {
                    Item itmUrgency = itmUrgencies.getItemByIndex(i);
                    itmUrgency.setType("inn_level");
                    itmReturn.addRelationship(itmUrgency);
                }
            }

            //文件附檔
            itmReturn.setProperty("inn_files", FileTable(cfg, cfg.itmDoc, cfg.doc_id));
        }

        private void AssignmentPage(TConfig cfg, Item itmReturn)
        {
            itmReturn.setProperty("page_title", "文件簽審");

            //文件資訊
            AppendDocInfo(cfg, itmReturn, setReturnData: true);

            //簽審紀錄表
            itmReturn.setProperty("inn_assigns", AssignmentTable(cfg, cfg.itmDoc, cfg.doc_id));

            //文件附檔
            itmReturn.setProperty("inn_files", FileTable(cfg, cfg.itmDoc, cfg.doc_id));
        }

        private string AssignmentTable(TConfig cfg, Item itmDoc, string doc_id, bool need_button = true)
        {
            var list = new List<Item>();
            MergeItem(list, GetFlowItems1(cfg, itmDoc, doc_id));
            MergeItem(list, GetFlowItems2(cfg, itmDoc, doc_id));

            var count = list.Count;
            if (!need_button && count <= 0)
            {
                return "";
            }

            string tbName = "data_table";

            var sb = new StringBuilder();
            sb.Append("<h3>簽審歷程</h3>");
            sb.Append("<table id='" + tbName + "' class='table table-bordered' data-search='false' >");
            sb.Append("	<thead>");
            sb.Append("	  <tr>");
            sb.Append("    <th class='text-center' data-width='20%'>Activity</th>");
            sb.Append("    <th class='text-center' data-width='20%'>State</th>");
            sb.Append("    <th class='text-center' data-width='20%'>Assigned To</th>");
            sb.Append("    <th class='text-center' data-width='20%'>When</th>");
            sb.Append("    <th class='text-center' data-width='20%'>Comments</th>");
            sb.Append("	  </tr>");
            sb.Append("	</thead>");

            sb.Append("	<tbody>");
            for (int i = 0; i < count; i++)
            {
                Item item = list[i];
                string act_label = item.getProperty("act_label", "");
                string state_label = item.getProperty("state_label", "");
                string last_name = item.getProperty("last_name", "");
                string comments = item.getProperty("comments", "");
                string closed_on = DtmStr(item.getProperty("closed_on", ""), "yyyy-MM-dd HH:mm:ss");

                sb.Append("<tr>");
                sb.Append(" <td class='text-left'>" + act_label + "</td>");
                sb.Append(" <td class='text-left'>" + state_label + "</td>");
                sb.Append(" <td class='text-left'>" + last_name + "</td>");
                sb.Append(" <td class='text-left'>" + closed_on + "</td>");
                sb.Append(" <td class='text-left'><label style='color: red'>" + comments + "</label></td>");
                sb.Append("</tr>");
            }

            sb.Append("	</tbody>");
            sb.Append("</table>");
            sb.Append("<script>");
            sb.Append("  $('#" + tbName + "').bootstrapTable();");
            sb.Append("  if($(window).width() <= 768) $('#" + tbName + "').bootstrapTable('toggleView');;");
            sb.Append("</script>");

            return sb.ToString();
        }

        private void MergeItem(List<Item> list, Item items)
        {
            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                list.Add(item);
            }
        }

        private Item GetFlowItems2(TConfig cfg, Item itmDoc, string doc_id)
        {
            string in_current_activity_id = itmDoc.getProperty("in_current_activity_id", "");

            string sql = @"
                SELECT 
	                t1.id
	                , t1.label_zt                     AS 'act_label'
					, t3.name
	                , t2.comments
	                , t4.keyed_name                   AS 'state_label'
	                , t11.login_name
	                , t11.last_name
                FROM 
	                [Activity] t1 WITH(NOLOCK)
                INNER JOIN
	                [Activity_Assignment] t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
				INNER JOIN 
					[IDENTITY] t3 WITH(NOLOCK) 
					ON t3.id = t2.related_id
                LEFT OUTER JOIN
	                [LIFE_CYCLE_STATE] t4 WITH(NOLOCK)
	                ON t4.id = t1.current_state
	            INNER JOIN
	                [USER] t11 WITH(NOLOCK)
	                ON t11.login_name = t3.in_number
                WHERE
	                t1.id = '{#id}'
	                AND t2.closed_on IS NULL
                ORDER BY 
	                t2.created_on
            ";

            sql = sql.Replace("{#id}", in_current_activity_id);

            return cfg.inn.applySQL(sql);

        }

        private Item GetFlowItems1(TConfig cfg, Item itmDoc, string doc_id)
        {
            string config_id = itmDoc.getProperty("config_id", "");

            if (config_id == "") config_id = doc_id;

            string sql = @"
                SELECT 
	                t1.id
	                , t1.label_zt                     AS 'act_label'
	                , DATEADD(HOUR, 8, t2.closed_on)  AS 'closed_on'
	                , t2.comments
	                , t3.login_name
	                , t3.last_name
	                , t4.keyed_name                   AS 'state_label'
                FROM 
	                [Activity] t1 WITH(NOLOCK)
                INNER JOIN
	                [Activity_Assignment] t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                INNER JOIN
	                [USER] t3 WITH(NOLOCK)
	                ON t3.id = t2.closed_by
                LEFT OUTER JOIN
	                [LIFE_CYCLE_STATE] t4 WITH(NOLOCK)
	                ON t4.id = t1.current_state
                WHERE
	                t1.in_itemtype = 'Document'
	                AND t1.in_config_id = '{#id}'
                ORDER BY 
	                t2.closed_on
            ";

            sql = sql.Replace("{#id}", config_id);

            return cfg.inn.applySQL(sql);
        }

        //文件資訊
        private void AppendDocInfo(TConfig cfg, Item itmReturn, bool setReturnData = true)
        {
            //lina, 2024.03.01: 即使加入 All Employees，在 In_GetAct_N 能成功取到資料，但這裡就是會取不到 Document 資料
            //  只好使用 Super User

            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            Item itmDoc = cfg.inn.newItem("Document", "get");
            itmDoc.setProperty("id", cfg.doc_id);
            itmDoc = itmDoc.apply();

            if (itmDoc.isError() || itmDoc.getResult() == "")
            {
                throw new Exception("查無文件");
            }

            cfg.itmDoc = itmDoc;
            cfg.is_in_flow = itmDoc.getProperty("in_wfp_state", "") != "";
            string in_issue_type = itmDoc.getProperty("in_issue_type", "");
            string in_issue_urgency = itmDoc.getProperty("in_issue_urgency", "");

            switch (in_issue_type)
            {
                case "launch":
                    itmReturn.setProperty("issue_type_label", "發文");
                    break;

                case "receive":
                    itmReturn.setProperty("issue_type_label", "收文");
                    break;
            }

            if (setReturnData)
            {
                var issue_date = DtmVal(itmDoc.getProperty("in_issue_date", ""));
                var issue_deadline = DtmStr(itmDoc.getProperty("in_issue_deadline", ""));
                var ws_issue_date = issue_date.ToString("yyyy-MM-dd");
                var tw_issue_date = "中華民國" + (issue_date.Year - 1911) + "年" + issue_date.ToString("MM月dd日");

                var in_wf_act2_identity = itmDoc.getProperty("in_wf_act2_identity", "");
                var in_current_activity = itmDoc.getProperty("in_current_activity", "");
                if (cfg.strIdentityId == in_wf_act2_identity && in_current_activity == "承辦組員修正")
                {
                    itmReturn.setProperty("can_edit", "1");
                }
                else
                {
                    itmReturn.setProperty("can_edit", "0");
                }

                itmReturn.setProperty("name", itmDoc.getProperty("name", ""));
                itmReturn.setProperty("item_number", itmDoc.getProperty("item_number", ""));
                itmReturn.setProperty("in_issue_note", itmDoc.getProperty("in_issue_note", ""));
                itmReturn.setProperty("in_issue_no", itmDoc.getProperty("in_issue_no", ""));
                itmReturn.setProperty("in_issue_date", ws_issue_date);
                itmReturn.setProperty("tw_issue_date", tw_issue_date);
                itmReturn.setProperty("in_issue_deadline", issue_deadline);
                itmReturn.setProperty("in_issue_urgency", itmDoc.getProperty("in_issue_urgency", ""));
                itmReturn.setProperty("in_issue_status", itmDoc.getProperty("in_issue_status", ""));
                itmReturn.setProperty("in_wfp_id", itmDoc.getProperty("in_wfp_id", ""));
                itmReturn.setProperty("in_wfp_state", itmDoc.getProperty("in_wfp_state", ""));
                itmReturn.setProperty("current_state", itmDoc.getProperty("current_state", ""));
                itmReturn.setProperty("in_current_activity", itmDoc.getProperty("in_current_activity", ""));

                Item itmUrgency = cfg.inn.applySQL("SELECT * FROM VU_Doc_LevelUrgency WHERE [value] = '" + in_issue_urgency + "'");
                if (!itmUrgency.isError() && itmUrgency.getResult() != "")
                {
                    itmReturn.setProperty("urgency_label", itmUrgency.getProperty("label", ""));
                }
            }

            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);
        }

        private string FlowTable(TConfig cfg, Item itmDoc, string doc_id)
        {
            string sql = "SELECT * FROM In_Document_WorkFlow WITH(NOLOCK)"
                + " WHERE source_id = '" + doc_id + "'"
                + " AND in_group > 100"
                + " AND in_code <> 'secretary'"
                + " ORDER BY in_group, in_serial";

            Item itmFlows = cfg.inn.applySQL(sql);

            string tbName = "flow_table";

            string flow_title = "是否簽審";
            string member_title = "簽審人員";
            if (cfg.MoreActionButtons != "")
            {
                flow_title = "<label style='color:red'>2.是否簽審</label>";
                member_title = "<label style='color:red'>4.簽審人員</label>";
            }

            var sb = new StringBuilder();
            sb.Append("<h3>簽審流程" + cfg.MoreActionButtons + "</h3>");
            sb.Append("<table id='" + tbName + "' class='table table-bordered' data-search='false' >");
            sb.Append("	<thead>");
            sb.Append("	  <tr>");
            sb.Append("    <th class='text-center' data-width='25%'>簽審標籤</th>");
            sb.Append("    <th class='text-center' data-width='30%'>" + member_title + "</th>");
            sb.Append("    <th class='text-center' data-width='20%'>" + flow_title + "</th>");
            sb.Append("	  </tr>");
            sb.Append("	</thead>");

            int count = itmFlows.getItemCount();
            sb.Append("	<tbody>");
            for (int i = 0; i < count; i++)
            {
                Item itmFlow = itmFlows.getItemByIndex(i);
                string in_label = itmFlow.getProperty("in_label", "");

                sb.Append("<tr>");
                sb.Append(" <td class='text-left'>" + in_label + "</td>");
                sb.Append(" <td class='text-left'>" + MemberMenu(cfg, itmFlow) + "</td>");
                sb.Append(" <td class='text-left'>" + VerifyMenu(cfg, itmFlow) + "</td>");
                sb.Append("</tr>");
            }

            sb.Append("	</tbody>");
            sb.Append("</table>");
            sb.Append("<script>");
            sb.Append("  $('#" + tbName + "').bootstrapTable();");
            sb.Append("  if($(window).width() <= 768) $('#" + tbName + "').bootstrapTable('toggleView');;");
            sb.Append("</script>");

            return sb.ToString();
        }

        private string FileTable(TConfig cfg, Item itmDoc, string doc_id)
        {
            string sql = @"
                SELECT 
	                t2.id
	                , t2.filename
	                , t2.mimetype
	                , t2.comments
                FROM
	                [DOCUMENT_FILE] t1 WITH(NOLOCK)
                INNER JOIN
	                [FILE] t2 WITH(NOLOCK)
	                ON t2.id = t1.related_id
                WHERE
	                t1.source_id = '{#doc_id}'
                ORDER BY
	                t1.sort_order
            ";

            sql = sql.Replace("{#doc_id}", doc_id);

            Item items = cfg.inn.applySQL(sql);
            string tbName = "file_table";

            var sb = new StringBuilder();
            sb.Append("<h3>文件附檔</h3>");
            sb.Append("<table id='" + tbName + "' class='table table-bordered' data-search='false' >");
            sb.Append("	<thead>");
            sb.Append("	  <tr>");
            sb.Append("    <th class='text-center' data-width='100%'>檔案</th>");
            sb.Append("	  </tr>");
            sb.Append("	</thead>");

            int count = items.getItemCount();
            sb.Append("	<tbody>");
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string id = item.getProperty("id", "");
                string filename = item.getProperty("filename", "");

                string url = "<a onclick=\"ih.handleItem('File', '" + id + "')\">"
                    + " <img class='in_thumbnail' width='auto' height='100' src='../Images/paperClip2.png' "
                    + "   onerror='this.onerror=null;this.src='../Images/paperClip2.png';'>"
                    + filename
                    + "</a>";

                sb.Append("<tr>");
                sb.Append(" <td class='text-left'>" + url + "</td>");
                sb.Append("</tr>");
            }

            sb.Append("	</tbody>");
            sb.Append("</table>");
            sb.Append("<script>");
            sb.Append("  $('#" + tbName + "').bootstrapTable();");
            sb.Append("  if($(window).width() <= 768) $('#" + tbName + "').bootstrapTable('toggleView');;");
            sb.Append("</script>");

            return sb.ToString();
        }

        private bool IsCanVote(TConfig cfg, Item itmDoc)
        {
            string in_current_activity = itmDoc.getProperty("in_current_activity", "");

            string prop = "";
            switch (in_current_activity)
            {
                case "":
                case "文管確認":
                    prop = "in_wf_act1_identity";
                    break;

                case "承辦組員":
                case "承辦組員 (F1)":
                case "承辦組員 (F2)":
                case "退承辦組員修改":
                    prop = "in_wf_act2_identity";
                    break;

                case "承辦單位主管":
                    prop = "in_wf_act3_identity";
                    break;

                case "副秘其一簽審":
                case "副秘都須簽審":
                    prop = "in_wf_act4_identity";
                    break;

                case "秘書長":
                case "秘書長簽審":
                    prop = "in_wf_act5_identity";
                    break;

                case "會長":
                case "會長簽審":
                    prop = "in_wf_act6_identity";
                    break;

                case "承辦組員發行":
                    prop = "in_wf_act7_identity";
                    break;
            }

            if (prop == "")
            {
                return false;
            }

            string viewer_id = itmDoc.getProperty(prop, "");

            if (!IsInIdentitList(cfg, viewer_id, cfg.strIdentityId))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private bool IsInIdentitList(TConfig cfg, string view_id, string login_identity_id)
        {
            string sql = @"
                SELECT
                	*
                FROM
                (
                	SELECT id AS 'vid' FROM [IDENTITY] WITH(NOLOCK) WHERE id = '{#view_id}'
                	UNION 
                	SELECT related_id AS 'vid' FROM [MEMBER] WITH(NOLOCK) WHERE source_id = '{#view_id}'
                ) t1
                WHERE t1.vid = '{#login_identity_id}'
            ";

            sql = sql.Replace("{#view_id}", view_id)
                .Replace("{#login_identity_id}", login_identity_id);

            Item itmResult = cfg.inn.applySQL(sql);

            return !itmResult.isError() && itmResult.getResult() != "";
        }

        private string VerifyMenu(TConfig cfg, Item itmFlow)
        {
            if (cfg.can_edit_launch_verify)
            {
                return VerifyMenu2(cfg, itmFlow);
            }
            else
            {
                return VerifyMenu1(cfg, itmFlow);
            }
        }

        private string VerifyMenu1(TConfig cfg, Item itmFlow)
        {
            string in_code = itmFlow.getProperty("in_code", "");
            string in_can_edit = itmFlow.getProperty("in_can_edit", "");
            string in_need_verify = itmFlow.getProperty("in_need_verify", "");

            if (in_can_edit == "0"
                || cfg.is_view
                || cfg.is_in_flow
                || cfg.is_waiting)
            {
                return VerifyLabel(cfg, in_need_verify);
            }

            switch (in_code)
            {
                case "cowork":
                    return CoworkFunc(cfg, itmFlow);

                case "edmanager":
                    return VerifyMenuCtrl(cfg, itmFlow, false);

                case "deputy":
                    return VerifyMenuCtrl(cfg, itmFlow, true);

                case "chairman":
                    return VerifyMenuCtrl(cfg, itmFlow, false);

                case "special1":
                    return VerifyMenuCtrl(cfg, itmFlow, false);

                default:
                    return "";
            }
        }

        private string VerifyMenu2(TConfig cfg, Item itmFlow)
        {
            string in_code = itmFlow.getProperty("in_code", "");
            string in_need_verify = itmFlow.getProperty("in_need_verify", "");

            switch (in_code)
            {
                case "deputy":
                    return VerifyMenuCtrl(cfg, itmFlow, true);

                case "chairman":
                    return VerifyMenuCtrl(cfg, itmFlow, false);

                case "special1":
                    return VerifyMenuCtrl(cfg, itmFlow, false);

                default:
                    return VerifyLabel(cfg, in_need_verify);
            }
        }

        private string VerifyMenuCtrl(TConfig cfg, Item itmFlow, bool can_any)
        {
            string id = itmFlow.getProperty("id", "");
            string in_code = itmFlow.getProperty("in_code", "");
            string in_need_verify = itmFlow.getProperty("in_need_verify", "");

            var sb = new StringBuilder();

            sb.Append("<select class='form-control verify_menu'"
                + " onchange='DocWorkFlowVerChange(this)'"
                + " data-id='" + id + "'"
                + " data-code='" + in_code + "'"
                + " data-val='" + in_need_verify + "'"
                + ">");

            sb.Append("  <option value=''>請選擇</option>");
            sb.Append("  <option value='0'>不簽審</option>");
            sb.Append("  <option value='1'>必須簽審</option>");

            if (can_any)
            {
                sb.Append("  <option value='A'>擇一簽審</option>");
            }

            sb.Append("</select>");

            return sb.ToString();
        }

        private string VerifyLabel(TConfig cfg, string value)
        {
            switch (value)
            {
                case "A": return "擇一簽審";
                case "1": return "必須簽審";
                default: return "不簽審";
            }
        }

        private string CoworkFunc(TConfig cfg, Item itmFlow)
        {
            string id = itmFlow.getProperty("id", "");
            string in_idt_dept_id = itmFlow.getProperty("in_idt_dept_id", "");

            return "<button class='btn btn-sm btn-danger'"
                + " data-id='" + id + "'"
                + " data-dept='" + in_idt_dept_id + "'"
                + " onclick='RemoveCoworkOrg(this)'>刪除會辦單位</button>";
        }

        private string MemberMenu(TConfig cfg, Item itmFlow)
        {
            string in_code = itmFlow.getProperty("in_code", "");

            switch (in_code)
            {
                case "undertaker": return FlowNameFromMemberMenu(cfg, itmFlow);
                case "supervisor": return FlowNameFromIdtName2(cfg, itmFlow);
                case "release": return FlowNameFromIdtName2(cfg, itmFlow);
                case "cowork": return FlowNameFromCoworker(cfg, itmFlow);
                default: return FlowNameFromIdtName(cfg, itmFlow);
            }
        }

        private string FlowNameFromCoworker(TConfig cfg, Item itmFlow)
        {
            Item itmMembers = GetCoworkerItems(cfg, "IN_DOCUMENT_COWORK_MEMBER");
            Item itmLeaders = GetCoworkerItems(cfg, "IN_DOCUMENT_COWORK_MANAGER");

            return GetCoworkerContens(cfg, itmMembers, "")
                //+ "<BR>"
                + GetCoworkerContens(cfg, itmLeaders, "<label style='color: red'>主管</label>");
        }

        private string GetCoworkerContens(TConfig cfg, Item items, string sub)
        {
            var sb = new StringBuilder();
            var count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var dept = item.getProperty("in_dept_name", "");
                var name = item.getProperty("last_name", "");
                sb.Append(sub + " " + name + "(" + dept + ")" + "<BR>");
            }

            return sb.ToString();
        }

        private Item GetCoworkerItems(TConfig cfg, string table)
        {
            string sql = @"
                SELECT 
                	t1.in_dept_name
                    , t2.id
                	, t3.last_name
                FROM 
                	{#table} t1 WITH(NOLOCK)
                INNER JOIN
                	[IDENTITY] t2 WITH(NOLOCK)
                	ON t2.id = t1.related_id
                INNER JOIN
                	[USER] t3 WITH(NOLOCK)
                	ON t3.login_name = t2.in_number
                WHERE 
                	t1.source_id = '{#doc_id}'
                ORDER BY 
                	t1.created_on DESC
            ";

            sql = sql.Replace("{#table}", table)
                .Replace("{#doc_id}", cfg.doc_id);

            return cfg.inn.applySQL(sql);
        }

        private string FlowNameFromMemberMenu(TConfig cfg, Item itmFlow)
        {
            if (cfg.is_view)
            {
                return FlowNameFromIdtName2(cfg, itmFlow);
            }

            if (cfg.cant_change)
            {
                return FlowNameFromIdtName2(cfg, itmFlow);
            }

            string in_wf_act3_identity = cfg.itmDoc.getProperty("in_wf_act3_identity", "");
            string in_wf_act21_identity = cfg.itmDoc.getProperty("in_wf_act21_identity", "");
            bool has_permission = cfg.is_doc_dispatcher || cfg.strIdentityId == in_wf_act3_identity;
            if (!has_permission)
            {
                return FlowNameFromIdtName2(cfg, itmFlow);
            }

            string in_idt_dept_id = itmFlow.getProperty("in_idt_dept_id", "");
            string in_idt_mmbr_id = itmFlow.getProperty("in_idt_mmbr_id", "");
            string key = in_idt_dept_id + "-" + in_idt_mmbr_id;
            if (in_idt_dept_id == in_idt_mmbr_id)
            {
                key = in_idt_dept_id;
            }

            var items = GetOrgMembers(cfg);
            var count = items.getItemCount();

            var last_dept = "";
            var options = new List<TMmbrOpt>();

            for (int i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                string dept_id = item.getProperty("dept_id", "");
                string dept_name = item.getProperty("dept_name", "");
                string dept_desc = item.getProperty("dept_desc", "");
                string mbr_id = item.getProperty("mbr_id", "");
                string mbr_name = item.getProperty("mbr_name", "");
                string mgr_id = item.getProperty("mgr_id", "");
                string mgr_name = item.getProperty("mgr_name", "");

                var value = dept_id + "-" + mbr_id;
                var label = " - " + dept_desc + " " + mbr_name;

                var need_sct_opt = cfg.is_doc_dispatcher || (in_wf_act21_identity != dept_id);
                var need_mbr_opt = cfg.is_doc_dispatcher || (in_wf_act21_identity == dept_id);
                var need_mgr_opt = cfg.is_doc_dispatcher;

                if (last_dept != dept_id)
                {
                    //單位主管指定
                    last_dept = dept_id;
                    if (need_sct_opt)
                    {
                        options.Add(new TMmbrOpt
                        {
                            df = "dept",
                            dept_id = dept_id,
                            dept_nm = dept_desc,
                            mngr = mgr_id,
                            mmbr = "",
                            type = "waiting",
                            color = "red",
                            value = dept_id,
                            label = dept_desc + " (單位主管指定)",
                        });
                    }

                    if (need_mbr_opt)
                    {
                        options.Add(new TMmbrOpt
                        {
                            df = "member",
                            dept_id = dept_id,
                            dept_nm = dept_desc,
                            mngr = mgr_id,
                            mmbr = mgr_id,
                            type = "assign",
                            color = "blue",
                            value = dept_id + "-" + mgr_id,
                            label = " - " + dept_desc + " 主管 " + mgr_name,
                        });
                    }
                }

                if (mgr_id == mbr_id)
                {
                    continue;
                }

                if (need_mbr_opt)
                {
                    options.Add(new TMmbrOpt
                    {
                        df = "member",
                        dept_id = dept_id,
                        dept_nm = dept_desc,
                        mngr = mgr_id,
                        mmbr = mbr_id,
                        type = "assign",
                        color = "black",
                        value = value,
                        label = label,
                    });
                }
            }

            return MemberMenuCtrl(cfg, options, key);
        }

        private string MemberMenuCtrl(TConfig cfg, List<TMmbrOpt> options, string key)
        {
            var sb = new StringBuilder();

            sb.Append("<select class='form-control undertaker_menu'"
                + " data-val='" + key + "'"
                + " onchange='Undertaker_Change(this)'"
                + " >");

            sb.Append("  <option value=''>請選擇</option>");

            foreach (var op in options)
            {
                sb.Append("  <option style='color: " + op.color + "'"
                    + " df='" + op.df + "'"
                    + " data-dtid='" + op.dept_id + "'"
                    + " data-dtnm='" + op.dept_nm + "'"
                    + " data-mgid='" + op.mngr + "'"
                    + " data-mbid='" + op.mmbr + "'"
                    + " data-type='" + op.type + "'"
                    + " value='" + op.value + "'>" + op.label + "</option>");
            }
            sb.Append("</select>");

            return sb.ToString();

        }

        private class TMmbrOpt
        {
            public string dept_id { get; set; }
            public string dept_nm { get; set; }
            public string mngr { get; set; }
            public string mmbr { get; set; }
            public string type { get; set; }
            public string value { get; set; }
            public string label { get; set; }
            public string color { get; set; }
            public string df { get; set; }
        }

        private string FlowNameFromIdtName(TConfig cfg, Item itmFlow)
        {
            string in_idt_mmbr_name = itmFlow.getProperty("in_idt_mmbr_name", "");
            string in_agent_note = itmFlow.getProperty("in_agent_note", "");
            if (in_idt_mmbr_name == "" || !in_idt_mmbr_name.Contains(" "))
            {
                return in_idt_mmbr_name;
            }
            in_idt_mmbr_name = in_idt_mmbr_name.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)[1];
            if (in_agent_note != "") in_idt_mmbr_name += "<span style='color: red'>(代理)</span>";
            return in_idt_mmbr_name;
        }

        private string FlowNameFromIdtName2(TConfig cfg, Item itmFlow)
        {
            string mmbr_name = FlowNameFromIdtName(cfg, itmFlow);
            string dept_name = itmFlow.getProperty("in_idt_dept_name", "");

            if (dept_name != "") dept_name += " ";
            return "<span style='color: blue;'>" + dept_name + mmbr_name + "</span>";
        }

        private TRoleMap GetRoleMap(TConfig cfg)
        {
            var map = new TRoleMap();
            //文管確認人 
            map.itmDD = GetIdentityItem(cfg, "ACT_DOC_Dispatcher", "文管確認");
            //行政組 
            map.itmADs = GetIdentityItem(cfg, "ACT_ASC_Admin", "行政組");
            //行政組副主任
            map.itmADD = GetIdentityItem(cfg, "ACT_ASC_Admin_Deputy", "行政組副主任");
            //副秘書長s 成員
            map.itmDSs = GetIdentityItem(cfg, "ACT_ASC_Deputy_Secretary", "副秘書長");
            //秘書長 成員
            map.itmSG = GetIdentityItem(cfg, "ACT_ASC_Secretary", "秘書長");
            //會長 成員
            map.itmCM = GetIdentityItem(cfg, "ACT_ASC_Chairman", "會長");
            //客製節點1 成員
            map.itmSP1 = GetIdentityItem(cfg, "ACT_ASC_Deputy_Secretary2", "副秘書長");


            map.dd_cnt = map.itmDD.getItemCount();
            map.ads_cnt = map.itmADs.getItemCount();
            map.add_cnt = map.itmADD.getItemCount();
            map.ds_cnt = map.itmDSs.getItemCount();
            map.sg_cnt = map.itmSG.getItemCount();
            map.cm_cnt = map.itmCM.getItemCount();
            map.sp1_cnt = map.itmSP1.getItemCount();


            if (map.dd_cnt <= 0 || map.dd_cnt > 1) throw new Exception("[文管確認]成員人數錯誤");
            if (map.ads_cnt <= 0) throw new Exception("[行政組]成員人數錯誤");
            if (map.add_cnt <= 0) throw new Exception("[行政組副主任]成員人數錯誤");
            if (map.ds_cnt <= 0 || map.ds_cnt > 2) throw new Exception("[副秘書長]成員人數錯誤");
            if (map.sg_cnt <= 0 || map.sg_cnt > 2) throw new Exception("[秘書長]成員人數錯誤");
            if (map.cm_cnt <= 0 || map.cm_cnt > 2) throw new Exception("[會長]成員人數錯誤");
            if (map.sp1_cnt <= 0 || map.sp1_cnt > 2) throw new Exception("[客製節點1]成員人數錯誤");


            //文管確認人 
            map.act1_idt_mmbr_id = map.itmDD.getProperty("mbr_id", "");
            map.act1_idt_mmbr_nm = map.itmDD.getProperty("mbr_name", "");

            //承辦組員(動態指定)
            map.act2_idt_mmbr_id = "";
            map.act2_idt_mmbr_nm = "";

            //承辦單位主管(動態指定)
            map.act3_idt_mmbr_id = "";
            map.act3_idt_mmbr_nm = "";

            //行政主任
            map.act31_idt_mmbr_id = ClearIdentityId(cfg, map.itmADs, 0, "leader_id");
            map.act31_idt_mmbr_nm = ClearIdentityId(cfg, map.itmADs, 0, "leader_name");
            //行政副主任
            map.act32_idt_mmbr_id = ClearIdentityId(cfg, map.itmADD, 0, "mbr_id");
            map.act32_idt_mmbr_nm = ClearIdentityId(cfg, map.itmADD, 0, "mbr_name");

            //副秘書長(預設為單位)
            map.act4_idt_dept_id = map.itmDSs.getItemByIndex(0).getProperty("dept_id", "");
            map.act4_idt_dept_nm = map.itmDSs.getItemByIndex(0).getProperty("dept_name", "");
            map.act4_idt_dept_ds = map.itmDSs.getItemByIndex(0).getProperty("dept_desc", "");

            if (map.ds_cnt == 1)
            {
                map.act4_idt_mmbr_id = map.itmDSs.getItemByIndex(0).getProperty("mbr_id", "");
                map.act4_idt_mmbr_nm = map.itmDSs.getItemByIndex(0).getProperty("mbr_name", "");
            }
            else
            {
                map.act4_idt_mmbr_id = "";
                map.act4_idt_mmbr_nm = "";
            }

            //副秘書長 A
            map.act41_idt_mmbr_id = ClearIdentityId(cfg, map.itmDSs, 0, "mbr_id");
            //副秘書長 B
            map.act42_idt_mmbr_id = ClearIdentityId(cfg, map.itmDSs, 1, "mbr_id");

            //秘書長
            map.act5_idt_dept_id = map.itmSG.getProperty("dept_id", "");
            map.act5_idt_dept_nm = map.itmSG.getProperty("dept_name", "");
            map.act5_idt_dept_ds = map.itmSG.getProperty("dept_desc", "");
            map.act5_idt_mmbr_id = map.itmSG.getProperty("mbr_id", "");
            map.act5_idt_mmbr_nm = map.itmSG.getProperty("mbr_name", "");

            //會長
            map.act6_idt_dept_id = map.itmCM.getProperty("dept_id", "");
            map.act6_idt_dept_nm = map.itmCM.getProperty("dept_name", "");
            map.act6_idt_dept_ds = map.itmCM.getProperty("dept_desc", "");
            map.act6_idt_mmbr_id = map.itmCM.getProperty("mbr_id", "");
            map.act6_idt_mmbr_nm = map.itmCM.getProperty("mbr_name", "");

            //承辦組員(動態指定)
            map.act7_idt_mmbr_id = map.act2_idt_mmbr_id;
            map.act7_idt_mmbr_nm = map.act2_idt_mmbr_nm;

            //客製節點1
            map.act33_idt_mmbr_id = map.itmSP1.getProperty("mbr_id", "");
            map.act33_idt_mmbr_nm = map.itmSP1.getProperty("mbr_name", "");

            return map;
        }

        private string ClearIdentityId(TConfig cfg, Item items, int idx, string prop)
        {
            string value = items.getItemByIndex(idx).getProperty(prop, "");
            return value;

            // int max_idx = items.getItemCount() - 1;
            // if (max_idx < idx) return "NULL";

            // if (value == "") return "NULL";

            // return "'" + value + "'";
        }

        private class TAgent
        {
            public string base_user_id { get; set; }
            public string base_user_name { get; set; }
            public string agent_user_id { get; set; }
            public string agent_keyed_name { get; set; }
            public string agent_name { get; set; }
            public string agent_identity_id { get; set; }
        }

        private TAgent FindUserAgent(TConfig cfg, string identity_id)
        {
            var agent = new TAgent();
            var sql = @"
                SELECT TOP 1
	                t2.id				AS 'base_user_id'
	                , t2.keyed_name		AS 'base_user_name'
	                , t3.id				AS 'agent_user_id'
	                , t3.keyed_name		AS 'agent_keyed_name'
	                , t3.last_name		AS 'agent_name'
	                , t4.id				AS 'agent_identity_id'
                FROM 
	                [IDENTITY] t1 WITH(NOLOCK)
                INNER JOIN
	                [USER] t2 WITH(NOLOCK)
	                ON t2.login_name = t1.in_number
                LEFT JOIN
	                [USER] t3 WITH(NOLOCK)
	                ON t3.id = t2.in_agent
                LEFT JOIN
	                [IDENTITY] t4 WITH(NOLOCK)
	                ON t4.in_number = t3.login_name
                WHERE 
	                t1.id = '{#identity_id}'
            ";
            sql = sql.Replace("{#identity_id}", identity_id);

            var item = cfg.inn.applySQL(sql);
            agent.base_user_id = item.getProperty("base_user_id", "");
            agent.base_user_name = item.getProperty("base_user_name", "");
            agent.agent_user_id = item.getProperty("agent_user_id", "");
            agent.agent_keyed_name = item.getProperty("agent_keyed_name", "");
            agent.agent_name = item.getProperty("agent_name", "");
            agent.agent_identity_id = item.getProperty("agent_identity_id", "");
            return agent;

        }
        private void AppendDocWorkFlow(TConfig cfg, TWorkFlow row)
        {
            Item itmDocWF = cfg.inn.newItem("In_Document_WorkFlow", "add");
            itmDocWF.setProperty("source_id", cfg.doc_id);
            itmDocWF.setProperty("in_label", row.label);
            itmDocWF.setProperty("in_can_edit", row.edit);

            if (!string.IsNullOrWhiteSpace(row.idt_mmbr_id))
            {
                var agent = FindUserAgent(cfg, row.idt_mmbr_id);

                if (agent.agent_identity_id != "")
                {
                    row.has_agent = true;//有代理人
                    row.idt_mmbr_id = agent.agent_identity_id;
                    row.idt_mmbr_name = agent.agent_keyed_name;//F227537924 黃鈺惠
                }

                itmDocWF.setProperty("in_idt_dept_id", row.idt_dept_id);
                itmDocWF.setProperty("in_idt_dept_name", row.idt_dept_name);
                itmDocWF.setProperty("in_idt_mmbr_id", row.idt_mmbr_id);
                itmDocWF.setProperty("in_idt_mmbr_name", row.idt_mmbr_name);
                if (row.has_agent)
                {
                    itmDocWF.setProperty("in_agent_note", "代理");
                }
            }

            itmDocWF.setProperty("in_code", row.code);
            itmDocWF.setProperty("in_group", row.group);
            itmDocWF.setProperty("in_serial", row.serial);
            itmDocWF.setProperty("in_mode", "1");
            itmDocWF.setProperty("in_need_verify", row.in_need_verify);
            itmDocWF.setProperty("in_status", "");
            itmDocWF.setProperty("in_note", "");

            Item itmResult = itmDocWF.apply();

            if (itmResult.isError())
            {
                throw new Exception("文件建立簽審流程失敗: " + row.label);
            }
        }

        private Item GetIdentityItem(TConfig cfg, string name, string label)
        {
            string sql = @"
                SELECT
                    t1.id               AS 'dept_id'
                    , t1.keyed_name     AS 'dept_name'
                    , t1.description    AS 'dept_desc'
                    , t1.owned_by_id    AS 'leader_id'
	                , t3.id             AS 'mbr_id'
	                , t3.keyed_name     AS 'mbr_name'
	                , t3.description    AS 'mbr_desc'
	                , t4.keyed_name     AS 'leader_name'
	                , t4.description    AS 'leader_desc'
                FROM 
	                [IDENTITY] t1 WITH(NOLOCK)
                INNER JOIN
	                [MEMBER] t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                INNER JOIN
	                [IDENTITY] t3 WITH(NOLOCK)
	                ON t3.id = t2.related_id
				LEFT OUTER JOIN
	                [IDENTITY] t4 WITH(NOLOCK)
	                ON t4.id = t1.owned_by_id
                WHERE 
	                t1.name = '{#idt_name}'
            ";

            sql = sql.Replace("{#idt_name}", name);

            Item items = cfg.inn.applySQL(sql);

            if (items.isError() || items.getResult() == "")
            {
                throw new Exception("未設定[" + label + "]角色或成員");
            }

            return items;
        }

        private void EditPage(TConfig cfg, Item itmReturn)
        {
            string doc_issue_type = itmReturn.getProperty("doc_issue_type", "");

            //附加收[發文期限]、[發文類型]欄位資訊
            AppendDocPropInfo(cfg, itmReturn);

            //登入者資訊
            string sql = "SELECt id, keyed_name, login_name from [USER] WITH(NOLOCK) WHERE id = '" + cfg.strUserId + "'";
            cfg.itmUser = cfg.inn.applySQL(sql);
            if (!cfg.itmUser.isError() && cfg.itmUser.getResult() != "")
            {
                cfg.login_name = cfg.itmUser.getProperty("login_name", "");
                itmReturn.setProperty("created_name", cfg.itmUser.getProperty("keyed_name", ""));
                itmReturn.setProperty("created_date", DateTime.Now.ToString("yyyy-MM-dd"));
            }

            //文件資訊
            if (cfg.doc_id != "")
            {
                Item itmDoc = cfg.inn.newItem("Document", "get");
                itmDoc.setProperty("id", cfg.doc_id);
                itmDoc = itmDoc.apply();

                if (!itmDoc.isError() && itmDoc.getResult() != "")
                {
                    itmReturn.setProperty("inn_name", itmDoc.getProperty("name", ""));
                    itmReturn.setProperty("in_issue_status", itmDoc.getProperty("in_issue_status", ""));
                    itmReturn.setProperty("in_issue_note", itmDoc.getProperty("in_issue_note", ""));
                    itmReturn.setProperty("in_issue_type", itmDoc.getProperty("in_issue_type", ""));
                    doc_issue_type = itmDoc.getProperty("in_issue_type", "");
                }
                else
                {
                    itmReturn.setProperty("in_issue_type", doc_issue_type);
                }
            }
            else
            {
                itmReturn.setProperty("in_issue_type", doc_issue_type);
            }

            if (doc_issue_type == "launch")
            {
                AppendDeptMenu(cfg, "", "inn_asc_dept", itmReturn);
                AppendDeptMenu(cfg, cfg.strIdentityId, "inn_my_dept", itmReturn);

                //取得組別成員
                Item items = GetAllSectMembers(cfg, cfg.strIdentityId);
                //轉型
                var list = GetSectMemberList(cfg, items);
                //附加組別成員
                AppendSectMembers(cfg, list, itmReturn);
            }
        }

        private void AppendDeptMenu(TConfig cfg, string identity_id, string type_name, Item itmReturn)
        {
            string mmbr_cond = identity_id == ""
                ? ""
                : "AND t1.id IN ( SELECT source_id FROM [MEMBER] WITH(NOLOCK) WHERE related_id = '" + identity_id + "' )";

            string sql = @"
                SELECT 
                	t1.id              AS 'dept_id'
                	, t1.name          AS 'dept_name'
                	, t1.description   AS 'dept_desc'
                	, t1.owned_by_id   AS 'leader_id'
                	, t2.name          AS 'leader_name'
                	, t2.description   AS 'leader_desc'
	                , TRIM(REPLACE(t2.name, t2.in_number, '')) AS 'leader_name2'
                FROM 
                	[IDENTITY] t1 WITH(NOLOCK)
                LEFT OUTER JOIN
                	[IDENTITY] t2 WITH(NOLOCK)
                	ON t2.id = t1.owned_by_id
                WHERE 
                	ISNULL(t1.in_is_org, 0) = 1 
                	{#mmbr_cond}
                ORDER BY
                	t1.id
            ";

            sql = sql.Replace("{#mmbr_cond}", mmbr_cond);

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                item.setType(type_name);
                itmReturn.addRelationship(item);
            }
        }

        //附加收發文類型欄位資訊
        private void AppendDocPropInfo(TConfig cfg, Item itmReturn)
        {
            string sql = @"
                SELECT 
	                t2.id
	                , t2.name
	                , t2.label_zt
                FROM
	                [ITEMTYPE] t1 WITH(NOLOCK)
                INNER JOIN
	                [PROPERTY] t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                WHERE
	                t1.is_current = 1
	                AND t2.is_current = 1
	                AND t1.name = 'Document'
	                AND t2.name IN ('in_issue_type', 'in_issue_deadline')
            ";

            Item items = cfg.inn.applySQL(sql);
            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string id = item.getProperty("id", "");
                string name = item.getProperty("name", "");
                string label_zt = item.getProperty("label_zt", "");

                itmReturn.setProperty(name + "_prop_id", id);
                itmReturn.setProperty(name + "_prop_name", name);
                itmReturn.setProperty(name + "_prop_label", label_zt);
            }
        }

        private void VotePage(TConfig cfg, Item itmReturn)
        {
            //文件資訊
            AppendDocInfo(cfg, itmReturn, setReturnData: true);

            //附加主旨欄位資訊
            AppendDocNamePropInfo(cfg, itmReturn);

            //發文-行政主任特殊權限
            AppendLaunchEDManagerRole(cfg, itmReturn);

            string in_wf_act2_identity = cfg.itmDoc.getProperty("in_wf_act2_identity", "");
            if (in_wf_act2_identity == cfg.strIdentityId)
            {
                itmReturn.setProperty("btn_delete", "<button id='btn_delete' class='btn btn-danger'  onclick='RemoveDocument()'>刪除</button>");
            }
            else
            {
                itmReturn.setProperty("btn_delete", "");
            }
        }

        //發文-行政主任特殊權限
        private void AppendLaunchEDManagerRole(TConfig cfg, Item itmReturn)
        {
            itmReturn.setProperty("can_edit_verify", "0");

            string in_issue_type = cfg.itmDoc.getProperty("in_issue_type", "");
            string in_current_activity = cfg.itmDoc.getProperty("in_current_activity", "");
            string in_wf_act31_identity = cfg.itmDoc.getProperty("in_wf_act31_identity", "");

            if (in_issue_type != "launch") return; //該文件非[發文]
            if (in_current_activity != "行政主任") return; //該流程非[行政主任]
            if (cfg.strIdentityId != in_wf_act31_identity) return; //當前登入者非[行政主任]

            itmReturn.setProperty("can_edit_verify", "1");
        }

        //附加主旨欄位資訊
        private void AppendDocNamePropInfo(TConfig cfg, Item itmReturn)
        {
            string sql = @"
                SELECT
                	t1.id
                FROM
                	PROPERTY t1 WITH(NOLOCK)
                INNER JOIN
                	ITEMTYPE t2 WITH(NOLOCK)
                	ON t2.id = t1.source_id
                WHERE 
                	t2.is_current = 1
                	AND t1.is_current = 1
                	AND t2.name = 'Document'
                	AND t1.name = 'name'
            ";

            Item item = cfg.inn.applySQL(sql);
            if (!item.isError() && item.getResult() != "")
            {
                string name_prop_id = item.getProperty("id", "");
                itmReturn.setProperty("name_prop_id", name_prop_id);
            }
        }

        private void ViewPage(TConfig cfg, Item itmReturn)
        {
            cfg.is_view = true;

            //發文
            cfg.is_doc_launcher = cfg.login_is_admin == "1";
            //收文
            cfg.is_doc_receiver = HasDocPermission(cfg, "ACT_DOC_Receiver");
            //文管確認
            cfg.is_doc_dispatcher = HasDocPermission(cfg, "ACT_DOC_Dispatcher");

            itmReturn.setProperty("page_title", "文件檢視");

            //文件資訊
            AppendDocInfo(cfg, itmReturn, setReturnData: true);

            string owned_by_id = cfg.itmDoc.getProperty("owned_by_id", "");
            string in_issue_type = cfg.itmDoc.getProperty("in_issue_type", "");

            Item itmOwned = cfg.inn.applySQL("SELECT TRIM(REPLACE(t1.name, t1.in_number, '')) AS 'name' FROM [IDENTITY] t1 WITH(NOLOCK) WHERE id = '" + cfg.strIdentityId + "'");
            if (!itmOwned.isError() && itmOwned.getResult() != "")
            {
                itmReturn.setProperty("owned_name", itmOwned.getProperty("name", ""));
            }
            else
            {
                itmReturn.setProperty("owned_name", " ");
            }

            var btn_delete = "<button id='btn_delete' class='btn btn-sm btn-danger'  onclick='RemoveDocument()'>刪除</button>";
            var btn_record = "<button id='btn_record' class='btn btn-sm btn-primary' onclick='RecordDocument()'>紀錄</button>";

            if (owned_by_id == cfg.strIdentityId)
            {
                itmReturn.setProperty("btn_delete", btn_delete);
                itmReturn.setProperty("btn_record", btn_record);
            }
            else if (cfg.is_doc_dispatcher)
            {
                itmReturn.setProperty("btn_delete", btn_delete);
                //itmReturn.setProperty("btn_record", btn_record);
            }
            else if (cfg.is_doc_receiver && in_issue_type == "receive")
            {
                itmReturn.setProperty("btn_delete", btn_delete);
                //itmReturn.setProperty("btn_record", btn_record);
            }
            else
            {
                itmReturn.setProperty("btn_delete", " ");
                itmReturn.setProperty("btn_record", " ");
            }

            //簽審流程表
            itmReturn.setProperty("inn_table", FlowTable(cfg, cfg.itmDoc, cfg.doc_id));

            //簽審紀錄表
            itmReturn.setProperty("inn_assigns", AssignmentTable(cfg, cfg.itmDoc, cfg.doc_id, false));

            //文件附檔
            itmReturn.setProperty("inn_files", FileTable(cfg, cfg.itmDoc, cfg.doc_id));

            //目前流程資訊
            AppendWorkFlowInfo(cfg, itmReturn);

            //附加主旨欄位資訊
            AppendDocNamePropInfo(cfg, itmReturn);
        }

        private void AppendWorkFlowInfo(TConfig cfg, Item itmReturn)
        {
            string in_current_activity = "";
            string in_wfp_state = cfg.itmDoc.getProperty("in_wfp_state", "");
            string in_wfp_id = cfg.itmDoc.getProperty("in_wfp_id", "");

            string sql = @"
                SELECT 
                	in_current_activity
                	, in_current_activity_name 
                FROM 
                	Workflow_Process WITH(NOLOCK) 
                WHERE 
                	is_current = 1 
                	AND id = '{#wfp_id}'
            ";

            sql = sql.Replace("{#wfp_id}", in_wfp_id);

            Item item = cfg.inn.applySQL(sql);
            if (!item.isError() && item.getResult() != "")
            {
                in_current_activity = item.getProperty("in_current_activity", "");

            }
            else
            {
                in_current_activity = GetCurrentActivity(cfg);
            }

            itmReturn.setProperty("in_current_activity", in_current_activity);
            itmReturn.setProperty("in_wfp_state", in_wfp_state);
        }

        private string GetCurrentActivity(TConfig cfg)
        {
            string result = "";
            string in_issue_status = cfg.itmDoc.getProperty("in_issue_status", "");
            string in_wf_act21_identity = cfg.itmDoc.getProperty("in_wf_act21_identity", "");

            switch (in_issue_status)
            {
                case "waiting":
                    Item itmSect = cfg.inn.applySQL("SELECT t1.id, t1.description FROM [Identity] t1 WITH(NOLOCK) WHERE t1.id = '" + in_wf_act21_identity + "'");
                    if (itmSect.isError() || itmSect.getResult() == "")
                    {
                        result = "等待單位主管指派";
                    }
                    else
                    {
                        result = "等待[" + itmSect.getProperty("description", "") + "]主管指派";
                    }
                    break;

                case "rejected":
                    result = "退回";
                    break;

                default:
                    result = "等待確認";
                    break;
            }
            return result;
        }

        //附加承辦單位成員選單
        private void AppendMembers(TConfig cfg, Item itmReturn)
        {
            var items = GetOrgMembers(cfg);
            var count = items.getItemCount();

            var rows = new List<TMember>();
            var last_dept = "";
            for (int i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var current_dept = item.getProperty("dept_name", "");
                if (current_dept != last_dept)
                {
                    last_dept = current_dept;
                    rows.Add(MapDept(cfg, item));
                }
                rows.Add(MapMember(cfg, item));
            }

            var json = Newtonsoft.Json.JsonConvert.SerializeObject(rows);
            itmReturn.setProperty("inn_member_json", json);
        }

        //附加承辦單位成員選單
        private Item GetOrgMembers(TConfig cfg)
        {
            string sql = @"
                SELECT 
	                t1.id            AS 'dept_id'
	                , t1.name        AS 'dept_name'
	                , t1.description AS 'dept_desc'
	                , t3.id			                           AS 'mbr_id'
	                , TRIM(REPLACE(t3.name, t3.in_number, '')) AS 'mbr_name'
	                , t3.in_number	                           AS 'mbr_sno'
	                , t4.id			                           AS 'mgr_id'
	                , TRIM(REPLACE(t4.name, t4.in_number, '')) AS 'mgr_name'
	                , t4.in_number	                           AS 'mgr_sno'
                FROM
	                [IDENTITY] t1 WITH(NOLOCK)
                INNER JOIN
	                [MEMBER] t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                INNER JOIN
	                [IDENTITY] t3 WITH(NOLOCK)
	                ON t3.id = t2.related_id
                LEFT OUTER JOIN
	                [IDENTITY] t4 WITH(NOLOCK)
	                ON t4.id = t1.owned_by_id
                WHERE
	                ISNULL(t1.in_is_org, 0) = 1
	                AND ISNULL(t3.in_number, '') NOT IN ('', '00000', 'M001')
                ORDER BY
	                t1.id
            ";

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        #region 發文

        private TLaunchModel GetLaunchMap(TConfig cfg, Item itmReturn)
        {
            var result = new TLaunchModel();

            //會辦人員
            result.lstCoworks = JsonDsrlList<TCW>(cfg, itmReturn.getProperty("ids", ""));
            if (result.lstCoworks == null) result.lstCoworks = new List<TCW>();

            //承辦單位
            result.sect_undertaker = itmReturn.getProperty("sect_undertaker", "");
            //送[行政副主任]簽審
            result.mode_executive_deputy = itmReturn.getProperty("mode_executive_deputy", "");

            string sql = "SELECT t1.id, t1.name, t1.description, t1.owned_by_id FROM [IDENTITY] t1 WITH(NOLOCK) WHERE id = '{#id}'";

            result.itmMe = cfg.inn.applySQL(sql.Replace("{#id}", cfg.strIdentityId));
            if (result.itmMe.isError() || result.itmMe.getResult() == "")
            {
                throw new Exception("查無登入者角色");
            }

            result.itmSect = cfg.inn.applySQL(sql.Replace("{#id}", result.sect_undertaker));
            if (result.itmSect.isError() || result.itmSect.getResult() == "")
            {
                throw new Exception("查無承辦單位角色");
            }

            string owned_by_id = result.itmSect.getProperty("owned_by_id", "");
            result.itmMngr = cfg.inn.applySQL(sql.Replace("{#id}", owned_by_id));
            if (result.itmMngr.isError() || result.itmMngr.getResult() == "")
            {
                throw new Exception("查無承辦單位主管角色");
            }

            //簽審規則(行政自節)
            result.in_mode_ad = "1";
            if (result.mode_executive_deputy == "0")
            {
                result.in_mode_ad = "";
            }

            result.me_idt_id = result.itmMe.getProperty("id", "");
            result.me_idt_name = result.itmMe.getProperty("name", "");
            result.me_idt_desc = result.itmMe.getProperty("description", "");

            result.sect_idt_id = result.itmSect.getProperty("id", "");
            result.sect_idt_name = result.itmSect.getProperty("name", "");
            result.sect_idt_desc = result.itmSect.getProperty("description", "");

            result.mngr_idt_id = result.itmMngr.getProperty("id", "");
            result.mngr_idt_name = result.itmMngr.getProperty("name", "");
            result.mngr_idt_desc = result.itmMngr.getProperty("description", "");

            return result;
        }

        private void AppendCowork(TConfig cfg, List<Item> list, string id, string cowork)
        {
            if (cowork != "1") return;

            string sql = "SELECT t1.id, t1.name, t1.description, t1.owned_by_id FROM [IDENTITY] t1 WITH(NOLOCK) WHERE id = '{#id}'";
            Item item = cfg.inn.applySQL(sql.Replace("{#id}", id));

            if (item.isError()) return;
            if (item.getResult() == "") return;

            list.Add(item);
        }

        private class TLaunchModel
        {
            public Item itmMe { get; set; }
            public Item itmSect { get; set; }
            public Item itmMngr { get; set; }

            public List<TCW> lstCoworks { get; set; }

            public string sect_undertaker { get; set; }
            public string mode_executive_deputy { get; set; }

            public string in_mode_ad { get; set; }

            public string me_idt_id { get; set; }
            public string me_idt_name { get; set; }
            public string me_idt_desc { get; set; }

            public string sect_idt_id { get; set; }
            public string sect_idt_name { get; set; }
            public string sect_idt_desc { get; set; }

            public string mngr_idt_id { get; set; }
            public string mngr_idt_name { get; set; }
            public string mngr_idt_desc { get; set; }
        }
        #endregion 發文

        private void AddCoworker(TConfig cfg, string item_type, string sect_id, string sect_nm, string related_id, string name)
        {
            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, item_type + ": " + name + " = " + related_id);

            Item itmNew = cfg.inn.newItem(item_type, "merge");
            itmNew.setAttribute("where", "source_id = '" + cfg.doc_id + "' AND related_id = '" + related_id + "'");
            itmNew.setProperty("in_dept_id", sect_id);
            itmNew.setProperty("in_dept_name", sect_nm);
            itmNew.setProperty("source_id", cfg.doc_id);
            itmNew.setProperty("related_id", related_id);
            itmNew.apply();
        }

        private void RemoveCowork(TConfig cfg, Item itmReturn)
        {
            string dwf_id = itmReturn.getProperty("dwf_id", "");

            string sql_delete1 = "DELETE FROM IN_DOCUMENT_WORKFLOW WHERE source_id = '" + cfg.doc_id + "' AND id = '" + dwf_id + "'";
            cfg.inn.applySQL(sql_delete1);

            string sql_delete2 = "DELETE FROM IN_DOCUMENT_COWORK_MANAGER WHERE source_id = '" + cfg.doc_id + "'";
            cfg.inn.applySQL(sql_delete2);

            string sql_delete3 = "DELETE FROM IN_DOCUMENT_COWORK_MEMBER WHERE source_id = '" + cfg.doc_id + "'";
            cfg.inn.applySQL(sql_delete3);

            string sql_update = "UPDATE [Document] SET"
                + "  in_mode_cw = ''"
                + ", in_mode_cl = ''"
                + " WHERE id = '" + cfg.doc_id + "'";

            cfg.inn.applySQL(sql_update);
        }

        //載入簽審流程相關參數
        private void LoadFlowVariable(TConfig cfg)
        {
            cfg.Deputy_Secretary_Mode = "";

            var sql = @"
                SELECT
	                t2.in_name
	                , t2.in_value
                FROM
	                IN_VARIABLE t1 WITH(NOLOCK)
                INNER JOIN
	                IN_VARIABLE_DETAIL t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                WHERE
	                t1.in_name = 'Document_Flow'
            ";

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var in_name = item.getProperty("in_name", "");
                var in_value = item.getProperty("in_value", "");
                switch (in_name)
                {
                    case "Deputy_Secretary":
                        cfg.Deputy_Secretary_Mode = in_value;
                        break;
                }
            }

            cfg.in_mode_ds = "1";
            switch (cfg.Deputy_Secretary_Mode)
            {
                case "A":
                    cfg.in_mode_ds = "1";//副秘其一簽審
                    break;

                case "2":
                    cfg.in_mode_ds = "2";//副秘皆必簽審
                    break;

                case "1":
                    cfg.in_mode_ds = "3";//指定副秘簽審
                    break;

                case "0":
                    cfg.in_mode_ds = "0";//跳過副秘
                    break;
            }

        }

        private void SendEmail(TConfig cfg, string scene)
        {
            Item itmData = cfg.inn.newItem();
            itmData.setType("Document");
            itmData.setProperty("strUserId", cfg.strUserId);
            itmData.setProperty("strIdentityId", cfg.strIdentityId);
            itmData.setProperty("doc_id", cfg.doc_id);
            itmData.setProperty("scene", scene);
            itmData.apply("In_Document_Email");
        }

        private TMember MapDept(TConfig cfg, Item item)
        {
            return new TMember
            {
                dept_id = item.getProperty("dept_id", ""),
                dept_name = item.getProperty("dept_name", ""),
                dept_desc = item.getProperty("dept_desc", ""),
                mbr_id = item.getProperty("dept_id", ""),
                mbr_name = item.getProperty("dept_name", ""),
                mbr_sno = "",
                is_dept = true,
            };
        }

        private TMember MapMember(TConfig cfg, Item item)
        {
            return new TMember
            {
                dept_id = item.getProperty("dept_id", ""),
                dept_name = item.getProperty("dept_name", ""),
                dept_desc = item.getProperty("dept_desc", ""),
                mbr_id = item.getProperty("mbr_id", ""),
                mbr_name = item.getProperty("mbr_name", ""),
                mbr_sno = item.getProperty("mbr_sno", ""),
            };
        }

        private class TMember
        {
            public string dept_id { get; set; }
            public string dept_name { get; set; }
            public string dept_desc { get; set; }
            public string mbr_id { get; set; }
            public string mbr_name { get; set; }
            public string mbr_sno { get; set; }
            public bool is_dept { get; set; }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }
            public string login_name { get; set; }

            public string itemtype { get; set; }
            public string doc_id { get; set; }
            public string scene { get; set; }

            public Item itmUser { get; set; }
            public Item itmDoc { get; set; }

            public bool is_in_flow { get; set; }
            public bool is_view { get; set; }

            public Item LoginResume { get; set; }
            public string login_is_admin { get; set; }
            public bool is_doc_receiver { get; set; }
            public bool is_doc_launcher { get; set; }
            public bool is_doc_dispatcher { get; set; }

            public string in_issue_status { get; set; }
            public bool is_waiting { get; set; }

            public bool cant_change { get; set; }

            public string MoreActionButtons { get; set; }

            public bool can_edit_launch_verify { get; set; }

            public string Deputy_Secretary_Mode { get; set; }
            public string in_mode_ds { get; set; }

        }

        private class TWorkFlow
        {
            public string label { get; set; }

            public string idt_dept_id { get; set; }
            public string idt_dept_name { get; set; }
            public string idt_mmbr_id { get; set; }
            public string idt_mmbr_name { get; set; }

            public string group { get; set; }
            public string serial { get; set; }

            //簽審模式(1: 一人簽即可、2: 全部必簽)
            public string mode { get; set; }
            public string code { get; set; }
            public string verify { get; set; }
            public string edit { get; set; }

            public string in_need_verify { get; set; }

            public bool has_agent { get; set; }
            public string agent_user_id { get; set; }
            public string agent_identity_id { get; set; }
            public string agent_name { get; set; }
        }

        private class TRoleMap
        {
            public Item itmDD { get; set; }
            public Item itmADs { get; set; }
            public Item itmADD { get; set; }
            public Item itmDSs { get; set; }
            public Item itmSG { get; set; }
            public Item itmCM { get; set; }
            public Item itmSP1 { get; set; }

            public int dd_cnt { get; set; }
            public int ads_cnt { get; set; }
            public int add_cnt { get; set; }
            public int ds_cnt { get; set; }
            public int sg_cnt { get; set; }
            public int cm_cnt { get; set; }
            public int sp1_cnt { get; set; }

            public string act1_idt_mmbr_id { get; set; }
            public string act1_idt_mmbr_nm { get; set; }

            public string act2_idt_mmbr_id { get; set; }
            public string act2_idt_mmbr_nm { get; set; }

            public string act3_idt_mmbr_id { get; set; }
            public string act3_idt_mmbr_nm { get; set; }

            public string act31_idt_mmbr_id { get; set; }
            public string act31_idt_mmbr_nm { get; set; }
            public string act32_idt_mmbr_id { get; set; }
            public string act32_idt_mmbr_nm { get; set; }
            public string act33_idt_mmbr_id { get; set; }
            public string act33_idt_mmbr_nm { get; set; }

            public string act4_idt_dept_id { get; set; }
            public string act4_idt_dept_nm { get; set; }
            public string act4_idt_dept_ds { get; set; }
            public string act4_idt_mmbr_id { get; set; }
            public string act4_idt_mmbr_nm { get; set; }

            public string act41_idt_mmbr_id { get; set; }
            public string act41_idt_mmbr_nm { get; set; }
            public string act42_idt_mmbr_id { get; set; }
            public string act42_idt_mmbr_nm { get; set; }

            public string act5_idt_dept_id { get; set; }
            public string act5_idt_dept_nm { get; set; }
            public string act5_idt_dept_ds { get; set; }
            public string act5_idt_mmbr_id { get; set; }
            public string act5_idt_mmbr_nm { get; set; }

            public string act6_idt_dept_id { get; set; }
            public string act6_idt_dept_nm { get; set; }
            public string act6_idt_dept_ds { get; set; }
            public string act6_idt_mmbr_id { get; set; }
            public string act6_idt_mmbr_nm { get; set; }

            public string act7_idt_mmbr_id { get; set; }
            public string act7_idt_mmbr_nm { get; set; }

            public string act8_idt_mmbr_id { get; set; }
            public string act8_idt_mmbr_nm { get; set; }

        }

        private class TLeader
        {
            public string id { get; set; }
            public string name { get; set; }
            public string sect_id { get; set; }
            public string sect_desc { get; set; }
            public List<string> sects { get; set; }
        }

        private class TCW
        {
            public bool selected { get; set; }
            public string sect_id { get; set; }
            public string sect_desc { get; set; }
            public string id { get; set; }
            public string name { get; set; }
            public string role { get; set; }
        }

        private List<TOut> JsonDsrlList<TOut>(TConfig cfg, string value)
        {
            try
            {
                return Newtonsoft.Json.JsonConvert.DeserializeObject<List<TOut>>(value);
            }
            catch (Exception ex)
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, ex.Message);
                return null;
            }
        }

        private DateTime DtmVal(string value, int hours = 0)
        {
            if (value == "") return DateTime.MinValue;

            DateTime result = DateTime.MinValue;
            if (DateTime.TryParse(value, out result))
            {
                return result.AddHours(hours);
            }
            else
            {
                return DateTime.MinValue;
            }
        }

        private string DtmStr(string value, string format = "yyyy-MM-dd", int hours = 0)
        {
            if (value == "") return "";

            DateTime result = DateTime.MinValue;
            if (DateTime.TryParse(value, out result))
            {
                return result.AddHours(hours).ToString(format);
            }
            else
            {
                return "";
            }
        }
    }
}