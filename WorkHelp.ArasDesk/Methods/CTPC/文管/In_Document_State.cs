﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.CTPC.Doc
{
    public class In_Document_State : Item
    {
        public In_Document_State(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 登入者與文件狀態
                參數: 
                    - this: Document
                日誌: 
                    - 2022-10-20: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]" + "In_Document_State";

            Item itmDoc = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmDoc.dom.InnerXml);

            Item itmR = inn.newItem();

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,

                strUserId = itmDoc.getProperty("strUserId", ""),
                strIdentityId = itmDoc.getProperty("strIdentityId", ""),
            };

            string id = itmDoc.getProperty("id", "");
            string in_wf_act3_identity = itmDoc.getProperty("in_wf_act3_identity", "");
            string in_issue_status = itmDoc.getProperty("in_issue_status", "");
            string in_current_activity = itmDoc.getProperty("in_current_activity", "");
            string in_current_activity_id = itmDoc.getProperty("in_current_activity_id", "");

            string prop = "";
            string relt = "";

            string page = "vote";
            string vote_state = "";

            string url = "";
            string not_start = "";
            string is_rejected = "";
            string need_assign_undertaker = "";
            string need_reset_undertaker = "";
            string can_vote = "";

            if (in_wf_act3_identity == "" && in_issue_status == "created")
            {
                not_start = "1";
            }

            switch (in_current_activity)
            {
                case "":
                    switch (in_issue_status)
                    {
                        case "rejected":
                            prop = "in_wf_act0_identity";
                            page = "reject";
                            is_rejected = "1";
                            break;

                        case "created":
                            prop = "in_wf_act1_identity";
                            page = "dispatch";
                            break;

                        case "waiting":
                            prop = "in_wf_act3_identity";
                            page = "dispatch";
                            need_assign_undertaker = "1";
                            break;

                        default:
                            prop = "owned_by_id";
                            page = "edit";
                            break;

                    }
                    break;

                case "文管確認":
                    prop = "in_wf_act1_identity";
                    page = "fix";
                    need_reset_undertaker = "1";
                    break;

                case "承辦組員":
                case "承辦組員 (F1)":
                case "承辦組員 (F2)":
                case "承辦組員修正":
                case "退承辦組員修改":
                case "退承辦組員協同會辦":
                    prop = "in_wf_act2_identity";
                    break;

                case "會辦組員":
                case "會辦自節":
                    relt = "In_Document_Cowork_Member";
                    prop = "";
                    break;

                case "會辦主管":
                    relt = "In_Document_Cowork_Manager";
                    prop = "";
                    break;

                case "承辦單位主管":
                    prop = "in_wf_act3_identity";
                    break;

                case "行政主任":
                    prop = "in_wf_act31_identity";
                    break;

                case "行政副主任":
                    prop = "in_wf_act32_identity";
                    break;

                case "副秘簽審":
                    prop = "in_wf_act41_identity";
                    break;

                case "副秘其一簽審":
                case "副秘都須簽審":
                    prop = "in_wf_act4_identity";
                    break;

                case "秘書長":
                case "秘書長簽審":
                    prop = "in_wf_act5_identity";
                    break;

                case "會長":
                case "會長簽審":
                    prop = "in_wf_act6_identity";
                    break;

                case "承辦組員發行":
                    prop = "in_wf_act7_identity";
                    break;
            }

            if (prop != "")
            {
                string viewer_id = itmDoc.getProperty(prop, "");

                if (IsInIdentityMembers(cfg, viewer_id))
                {
                    can_vote = "1";
                }
            }
            else if (relt != "")
            {
                if (IsInDocumentRelation(cfg, relt, id))
                {
                    can_vote = "1";
                }
            }

            //string inn_url = "c.aspx?page=" + page + "&method=In_Document_Edit&doc_id=" + id + "&scene=" + scene;

            if (page == "edit")
            {

            }
            else if (can_vote != "1")
            {
                page = "view";
            }
            else if (page == "vote")
            {
                var really_can_vote = CanVote1(cfg, in_current_activity_id);

                if (!really_can_vote)
                {
                    really_can_vote = CanVote2(cfg, in_current_activity_id);
                }

                if (!really_can_vote)
                {
                    vote_state = "voted";
                    page = "view";
                }
            }

            switch (page)
            {
                case "edit":
                    url = "c.aspx?page=DocumentEdit.html"
                        + "&method=In_Get_SingleItemInfoGeneral"
                        + "&itemtype=Document"
                        + "&itemid=" + id
                        + "&type=direct"
                        + "&fetchproperty=4"
                        + "&inn_editor_type=edit";
                    break;

                case "edit2":
                    url = "c.aspx?page=DocumentDispatch.html"
                        + "&method=In_Document_Edit"
                        + "&doc_id=" + id
                        + "&scene=dispatch_page";
                    break;

                case "dispatch":
                    url = "c.aspx?page=DocumentDispatch.html"
                        + "&method=In_Document_Edit"
                        + "&doc_id=" + id
                        + "&scene=dispatch_page";
                    break;

                case "fix":
                    url = "c.aspx?page=DocumentDispatch2.html"
                        + "&method=In_Document_Edit"
                        + "&doc_id=" + id
                        + "&scene=dispatch_page2";
                    break;

                case "reject":
                    url = "c.aspx?page=DocumentEdit.html"
                        + "&method=In_Get_SingleItemInfoGeneral"
                        + "&itemtype=Document"
                        + "&itemid=" + id
                        + "&type=direct"
                        + "&fetchproperty=4"
                        + "&inn_editor_type=edit";
                    break;

                case "view":
                    url = "c.aspx?page=DocumentView2.html"
                        + "&method=In_Document_Edit"
                        + "&doc_id=" + id
                        + "&scene=view_page";
                    break;

                case "vote":
                    url = "c.aspx?page=DocVoteView.html"
                        + "&method=In_GetAct_N"
                        + "&itemtype=Document"
                        + "&itemid=" + id;
                    break;
            }

            itmR.setProperty("url", url);
            itmR.setProperty("not_start", not_start);
            itmR.setProperty("is_rejected", is_rejected);
            itmR.setProperty("need_assign_undertaker", need_assign_undertaker);
            itmR.setProperty("need_reset_undertaker", need_reset_undertaker);
            itmR.setProperty("can_vote", can_vote);
            itmR.setProperty("vote_state", vote_state);

            return itmR;
        }

        private bool CanVote1(TConfig cfg, string in_current_activity_id)
        {
            string sql = @"
                SELECT 
	                t1.id
                FROM 
	                [Activity_Assignment] t1 WITH(NOLOCK)
                WHERE
	                t1.source_id = '{#id}'
					AND t1.related_id = '{#login_identity_id}'
	                AND t1.closed_on IS NULL
            ";

            sql = sql.Replace("{#id}", in_current_activity_id)
                .Replace("{#login_identity_id}", cfg.strIdentityId);

            Item itmResult = cfg.inn.applySQL(sql);

            return !itmResult.isError() && itmResult.getResult() != "";
        }

        private bool CanVote2(TConfig cfg, string in_current_activity_id)
        {
            string sql = @"
                SELECT 
	                t1.id
                FROM 
	                [Activity_Assignment] t1 WITH(NOLOCK)
	            INNER JOIN 
	                [MEMBER] t2 WITH(NOLOCK)
	                ON t2.source_id = t1.related_id
                WHERE
	                t1.source_id = '{#id}'
	                AND t1.closed_on IS NULL
					AND t2.related_id = '{#login_identity_id}'
            ";

            sql = sql.Replace("{#id}", in_current_activity_id)
                .Replace("{#login_identity_id}", cfg.strIdentityId);

            Item itmResult = cfg.inn.applySQL(sql);

            return !itmResult.isError() && itmResult.getResult() != "";
        }

        private bool IsInIdentityMembers(TConfig cfg, string view_id)
        {
            string sql = @"
                SELECT
                	*
                FROM
                (
                	SELECT id AS 'vid' FROM [IDENTITY] WITH(NOLOCK) WHERE id = '{#view_id}'
                	UNION 
                	SELECT related_id AS 'vid' FROM [MEMBER] WITH(NOLOCK) WHERE source_id = '{#view_id}'
                ) t1
                WHERE t1.vid = '{#login_identity_id}'
            ";

            sql = sql.Replace("{#view_id}", view_id)
                .Replace("{#login_identity_id}", cfg.strIdentityId);

            Item itmResult = cfg.inn.applySQL(sql);

            return !itmResult.isError() && itmResult.getResult() != "";
        }

        private bool IsInDocumentRelation(TConfig cfg, string item_type, string doc_id)
        {
            string sql = @"
                SELECT
                	*
                FROM
                    {#table} WITH(NOLOCK)
                WHERE
                    source_id = '{#doc_id}'
                    AND related_id = '{#idt_id}'
            ";

            sql = sql.Replace("{#table}", item_type)
                .Replace("{#doc_id}", doc_id)
                .Replace("{#idt_id}", cfg.strIdentityId);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            Item itmResult = cfg.inn.applySQL(sql);

            return !itmResult.isError() && itmResult.getResult() != "";
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }
        }
    }
}