﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.CTPC.Doc
{
    public class in_user_dashboard : Item
    {
        public in_user_dashboard(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 會員儀錶板
                日期: 2020-07-29 改版 (lina)
                //2023.05.16 過濾已經完成的簽核 by panda
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]in_user_dashboard";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "input dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                strIdentityId = inn.getUserAliases(),
                Now = DateTime.Now,
                apply_meeting_id = "A9F45C0EFD944ADAB1D7975D8C2E09C3",
                isSpecialUser = false,
            };

            //取得登入者權限
            Item itmPermit = inn.applyMethod("In_CheckIdentity", "<method>" + strMethodName + "</method><code>ALL</code>");
            cfg.isMeetingAdmin = itmPermit.getProperty("isMeetingAdmin", "") == "1";
            cfg.isCommittee = itmPermit.getProperty("isCommittee", "") == "1";
            cfg.isGymOwner = itmPermit.getProperty("isGymOwner", "") == "1";
            cfg.isGymAssistant = itmPermit.getProperty("isGymAssistant", "") == "1";

            cfg.NowStr = cfg.Now.AddHours(-8).ToString("yyyy-MM-ddTHH:mm:ss");//sql用-8H

            itmR.setProperty("inn_meeting_admin", itmPermit.getProperty("isMeetingAdmin", ""));
            itmR.setProperty("inn_committee", itmPermit.getProperty("isCommittee", ""));

            //取得登入者資訊
            Item itmResume = inn.newItem("In_Resume", "get");
            itmResume.setProperty("in_user_id", cfg.strUserId);
            cfg.LoginResume = itmResume.apply();

            if (cfg.LoginResume.isError() || cfg.LoginResume.getItemCount() != 1)
            {
                throw new Exception("個人履歷資料異常");
            }

            cfg.login_resume_id = cfg.LoginResume.getProperty("id", "");
            cfg.login_name = cfg.LoginResume.getProperty("in_name", "");
            cfg.login_sno = cfg.LoginResume.getProperty("in_sno", "");
            cfg.login_group = cfg.LoginResume.getProperty("in_group", "");
            cfg.login_in_org = cfg.LoginResume.getProperty("in_org", "");
            cfg.login_member_type = cfg.LoginResume.getProperty("in_member_type", "");
            cfg.login_member_role = cfg.LoginResume.getProperty("in_member_role", "");
            cfg.login_member_unit = cfg.LoginResume.getProperty("in_member_unit", "");
            cfg.login_is_admin = cfg.LoginResume.getProperty("in_is_admin", "");

            //取得登入者資訊(使用者)
            Item itmUser = cfg.inn.applySQL("SELECT id, in_line_notify FROM [USER] WITH(NOLOCK) WHERE id = '" + cfg.strUserId + "'");
            if (!itmUser.isError() && itmUser.getResult() != "")
            {
                itmR.setProperty("user_line_notify", itmUser.getProperty("in_line_notify", ""));
            }

            Item itmYUReq = cfg.inn.newItem();
            itmYUReq.setType("In_Resume");
            itmYUReq.setProperty("user_id", cfg.strUserId);
            var itmYURes = itmYUReq.apply("In_User_Special_01");
            if (itmYURes.getProperty("answer", "") == "1")
            {
                cfg.isSpecialUser = true;
            }

            //取得活動列表
            Dictionary<string, List<Item>> mapMeeting = GetAllMeetings(cfg);
            //取得活動類型列表
            Item itmTypes = GetValues(cfg, list_name: "In_Meeting_Type");
            //活動類型數量
            int type_count = itmTypes.getItemCount();

            for (int i = 0; i < type_count; i++)
            {
                Item itmType = itmTypes.getItemByIndex(i);
                string value = itmType.getProperty("value", "");
                itmR.setProperty(value + "_" + "count", GetMeetingCount(mapMeeting, value));
            }
            //驗證照片是否無上傳
            string isPhoto = checkPhoto(cfg);


            itmR.setProperty("group_id", cfg.login_resume_id);
            itmR.setProperty("in_group", cfg.login_group);
            itmR.setProperty("in_org", cfg.login_in_org);
            itmR.setProperty("in_member_type", cfg.login_member_type);
            itmR.setProperty("in_member_role", cfg.login_member_role);
            itmR.setProperty("isPhoto", isPhoto);

            //待繳費單
            AppendPayment(cfg, itmR);

            //審核未通過
            AppendReject(cfg, itmR);

            //最新公告
            AppendAnnouncements(cfg, itmR);

            //相關活動(個人訊息)
            AppendMeetings(cfg, itmR);

            //復權
            AppendRecoverFunc(cfg, itmR);

            ////各類申請
            //AppendApply(cfg, itmR);

            string identity_id = cfg.strIdentityId;

            //if (cfg.isMeetingAdmin || cfg.login_is_admin == "1")
            //{
            //    //各類申請審核
            //    AppendApplyVerify(cfg, identity_id, itmR);
            //    //各類申請審核記錄
            //    AppendApplyVerify(cfg, identity_id, itmR, is_finished: true);

            //}
            //else
            //{
            //    itmR.setProperty("hide_verify_box", "item_show_0");
            //    itmR.setProperty("hide_verify_box_finished", "item_show_0");
            //    itmR.setProperty("hide_editMeeting_box", "item_show_0");
            //}

            //退費申請
            AppendApplyRefunds(cfg, identity_id, itmR, is_admin: true);
            AppendApplyRefunds(cfg, identity_id, itmR);

            //報名變更
            AppendApplyMeeting(cfg, identity_id, itmR, is_admin: true);
            AppendApplyMeeting(cfg, identity_id, itmR);

            //財務區
            FinanceBox(cfg, identity_id, itmR);

            //文管區
            DocumentBox(cfg, identity_id, itmR);

            return itmR;
        }

        #region 文管

        //文管區
        private void DocumentBox(TConfig cfg, string identity_id, Item itmReturn)
        {
            bool is_admin = cfg.login_is_admin == "1";

            if (is_admin)
            {
                itmReturn.setProperty("hide_doc_box", "item_show_1");
            }
            else
            {
                itmReturn.setProperty("hide_doc_box", "item_show_0");
            }

            //發文
            cfg.is_doc_launcher = is_admin;
            //收文
            cfg.is_doc_receiver = HasDocPermission(cfg, identity_id, "ACT_DOC_Receiver");
            //文管確認
            cfg.is_doc_dispatcher = HasDocPermission(cfg, identity_id, "ACT_DOC_Dispatcher");

            string act_add = "<a href='c.aspx?page=DocumentEdit.html"
                + "&method=In_Get_SingleItemInfoGeneral"
                + "&fetchproperty=5"
                + "&itemtype=Document"
                + "&inn_editor_type=add"
                + "&inn_issue_type={#type}"
                + "'>"
                + "  <i class='fa fa-plus'></i>{#title}"
                + "</a>";

            string act_qry = "<a href='c.aspx?page=DocumentSearch.html"
                + "&method=In_Search_ItemWithParameter"
                + "&embedded=false"
                + "&in_do_search=1"
                + "&q_type=Document"
                + "&inn_original_query_string="
                + "'>"
                + "  <i class='fa fa-search'></i>查詢"
                + "</a>";


            string act_add_l = act_add.Replace("{#type}", "launch")
                .Replace("{#title}", "發文");

            string act_add_r = act_add.Replace("{#type}", "receive")
                .Replace("{#title}", "收文");

            var sb = new StringBuilder();
            if (cfg.is_doc_receiver) sb.Append("　" + act_add_r);
            if (cfg.is_doc_launcher) sb.Append("　" + act_add_l);
            //if (cfg.is_doc_dispatcher) sb.Append("　" + act_qry);
            if (cfg.is_doc_launcher) sb.Append("　" + act_qry);
            if (cfg.is_doc_launcher) sb.Append("　" + GetDocSummary(cfg, identity_id));

            itmReturn.setProperty("inn_doc_funcs", sb.ToString());

            AppendDocumentTasks(cfg, identity_id, itmReturn);
        }


        private string GetDocSummary(TConfig cfg, string identity_id)
        {
            string sql = @"
                SELECT 
                	id
                	, config_id
                	, name
                	, in_issue_no
                	, in_issue_date
                	, in_issue_note
                	, in_issue_status
                	, in_issue_type
                	, in_current_activity
                	, in_wf_act3_identity
                	, created_by_id
                FROM 
                	[DOCUMENT] WITH(NOLOCK) 
                WHERE
                    is_current = 1
                    AND created_by_id = '{#user_id}'
                    AND ISNULL(in_wfp_state, '') NOT IN ('Closed')
                	--AND ( (created_by_id = '{#user_id}') OR (in_wf_act3_identity = '{#identity_id}') )
                ORDER BY 
                	created_on DESC
            ";

            sql = sql.Replace("{#user_id}", cfg.strUserId)
                .Replace("{#identity_id}", cfg.strIdentityId);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            int draft = 0;
            int created = 0;
            int rejected = 0;
            int waiting = 0;
            int launched = 0;

            Item items = cfg.inn.applySQL(sql);
            int count = items.getItemCount();
            string draft_id = "";
            string created_id = "";
            string rejected_id = "";
            string waiting_id = "";
            string launched_id = "";

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string id = item.getProperty("id", "");
                string in_issue_type = item.getProperty("in_issue_type", "");
                string in_issue_status = item.getProperty("in_issue_status", "");
                string in_current_activity = item.getProperty("in_current_activity", "");
                string in_wf_act3_identity = item.getProperty("in_wf_act3_identity", "");
                string created_by_id = item.getProperty("created_by_id", "");

                bool not_start = in_current_activity == "";
                bool is_my_doc = created_by_id == cfg.strUserId;
                bool is_my_task = in_wf_act3_identity == cfg.strIdentityId;

                switch (in_issue_status)
                {
                    case ""://草稿
                        if (is_my_doc)
                        {
                            draft++;
                            draft_id = id;
                        }
                        break;

                    case "created"://收文，未啟動
                        if (is_my_doc && not_start)
                        {
                            created++;
                            created_id = id;
                        }
                        break;

                    case "rejected"://收文，發起人被退件
                        if (is_my_doc && not_start)
                        {
                            rejected++;
                            rejected_id = id;
                        }
                        break;

                    case "waiting"://收文，等待主管指派承辦組員
                        if (is_my_task && not_start)
                        {
                            waiting++;
                            waiting_id = id;
                        }
                        break;

                    case "launched"://發文
                        if (is_my_doc)
                        {
                            launched++;
                            launched_id = id;
                        }
                        break;
                }
            }

            var sb = new StringBuilder();
            sb.Append(DocLinkEdit(cfg, "draft", "草稿", draft, draft_id, identity_id));
            sb.Append(DocLinkEdit(cfg, "rejected", "被退回", rejected, rejected_id, identity_id));
            sb.Append(DocLinkView(cfg, "created", "等待確認", created, created_id, identity_id, "receive"));
            sb.Append(DocLinkDisp(cfg, "waiting", "需指派", waiting, waiting_id, identity_id));
            sb.Append(DocLinkView(cfg, "launched", "我的發文", launched, launched_id, identity_id, "launch"));
            return sb.ToString();
        }

        private string DocLinkEdit(TConfig cfg, string status, string label, int count, string doc_id, string identity_id)
        {
            if (count <= 0) return "";

            string url = "c.aspx?page=DocumentEdit.html"
                + "&method=In_Get_SingleItemInfoGeneral"
                + "&itemtype=Document"
                + "&itemid=" + doc_id
                + "&type=direct"
                + "&fetchproperty=4"
                + "&inn_editor_type=edit"
            ;

            if (count > 1)
            {
                url = "c.aspx?page=DocumentList.html"
                    + "&method=In_Document_Edit"
                    + "&scene=list"
                    + "&status=" + status;
            }

            return " <a href='" + url + "' style='font-size:14px'>" + label + ": " + count + "</a>";
        }

        private string DocLinkDisp(TConfig cfg, string status, string label, int count, string doc_id, string identity_id)
        {
            if (count <= 0) return "";

            string url = "c.aspx?page=DocumentDispatch.html"
                + "&method=In_Document_Edit"
                + "&doc_id=" + doc_id
                + "&scene=dispatch_page"
            ;

            if (count > 1)
            {
                url = "c.aspx?page=DocumentList.html"
                    + "&method=In_Document_Edit"
                    + "&scene=list"
                    + "&status=" + status;
            }

            return " <a href='" + url + "' style='font-size:14px'>" + label + ": " + count + "</a>";
        }

        private string DocLinkView(TConfig cfg, string status, string label, int count, string doc_id, string identity_id, string type)
        {
            if (count <= 0) return "";

            string url = "c.aspx?page=DocumentView2.html"
                + "&method=In_Document_Edit"
                + "&doc_id=" + doc_id
                + "&scene=view_page";

            if (count > 1)
            {
                url = "c.aspx?page=DocumentList.html"
                    + "&method=In_Document_Edit"
                    + "&scene=list"
                    + "&status=" + status
                    + "&type=" + type;
            }

            return " <a href='" + url + "' style='font-size:14px'>" + label + ": " + count + "</a>";
        }

        private void AppendDocumentTasks(TConfig cfg, string identity_id, Item itmReturn)
        {
            List<TDocItem> list = new List<TDocItem>();
            MergerItems(list, DocumentTasks1(cfg, identity_id));
            MergerItems(list, DocumentTasks2(cfg, identity_id));
            MergerItems(list, DocumentTasks3(cfg, identity_id));

            var sorted_list = list.OrderByDescending(x => x.item_number).ToList();
            if (sorted_list == null) sorted_list = new List<TDocItem>();


            for (int i = 0; i < sorted_list.Count; i++)
            {
                var obj = sorted_list[i];
                var item = obj.value;

                var row = MapDoc(cfg, item);
                var in_current_activity = item.getProperty("in_current_activity", "");
                var in_issue_status = item.getProperty("in_issue_status", "");

                item.setType("Document");
                item.setProperty("strUserId", cfg.strUserId);
                item.setProperty("strIdentityId", cfg.strIdentityId);

                //檢查權限
                var itmMethodResult = item.apply("In_Document_State");

                row.url = itmMethodResult.getProperty("url", "");
                row.not_start = itmMethodResult.getProperty("not_start", "") == "1";
                row.is_rejected = itmMethodResult.getProperty("is_rejected", "") == "1";
                row.need_assign_undertaker = itmMethodResult.getProperty("need_assign_undertaker", "") == "1";
                row.need_reset_undertaker = itmMethodResult.getProperty("need_reset_undertaker", "") == "1";
                row.can_vote = itmMethodResult.getProperty("can_vote", "") == "1";
                row.vote_state = itmMethodResult.getProperty("vote_state", "");
                if (in_current_activity == "文管確認" && cfg.isSpecialUser)
                {
                    row.can_vote = true;
                }

                if (!row.can_vote)
                {
                    continue;
                }

                item.setProperty("inn_url", row.url);
                item.setProperty("inn_title", GetDocTitle(cfg, row));
                itmReturn.addRelationship(item);
            }
        }

        private void MergerItems(List<TDocItem> list, Item items)
        {
            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var findList = list.Where(doc => doc.item_number == item.getProperty("item_number", "")).FirstOrDefault();
                //2023.05.16 過濾已經完成的簽核 by panda
                if (findList == null)
                {
                    list.Add(new TDocItem
                    {
                        item_number = item.getProperty("item_number", ""),
                        value = item,
                    });
                }
            }
        }

        private Item DocumentTasks1(TConfig cfg, string identity_id)
        {
            string sql = @"
                SELECT * FROM [Document] WITH(NOLOCK)
                WHERE is_current = 1 AND ISNULL(in_current_activity, '') = ''
            ";

            sql = sql.Replace("{#id}", cfg.strIdentityId);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        private Item DocumentTasks2(TConfig cfg, string identity_id)
        {
            string sql = @"
                SELECT t1.* FROM [Document] t1 WITH(NOLOCK)
                INNER JOIN [Activity_Assignment] t2 WITH(NOLOCK)
                ON t2.source_id = t1.in_current_activity_id
                WHERE t1.is_current = 1 AND t2.related_id = '{#id}'
            ";

            sql = sql.Replace("{#id}", cfg.strIdentityId);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        private Item DocumentTasks3(TConfig cfg, string identity_id)
        {
            string sql = @"
                SELECT t1.* FROM [Document] t1 WITH(NOLOCK)
                INNER JOIN [Activity_Assignment] t2 WITH(NOLOCK)
                ON t2.source_id = t1.in_current_activity_id
				INNER JOIN [MEMBER] t3  WITH(NOLOCK)
				ON t3.source_id = t2.related_id
                WHERE t1.is_current = 1 AND t3.related_id = '{#id}'
            ";

            sql = sql.Replace("{#id}", cfg.strIdentityId);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        private string GetDocTitle(TConfig cfg, TDoc row)
        {
            var note = row.value.getProperty("item_number", "")
                    + ". " + row.value.getProperty("in_issue_no", "")
                    + ": "
                    + row.value.getProperty("name", "")
                    ;
            if (row.vote_state != "")
            {
                note = "[已簽審]" + note;
            }

            if (row.not_start)
            {
                return "<label style='color:red'>等待確認：</label>"
                    + note;
            }
            else if (row.is_rejected)
            {
                return "<label style='color:red'>收文退回：</label>"
                    + note;
            }
            else if (row.need_assign_undertaker)
            {
                return "<label style='color:red'>需指派承辦組員：</label>"
                    + note;
            }
            else if (row.need_reset_undertaker)
            {
                return "<label style='color:red'>重新指派承辦組員：</label>"
                    + note;
            }
            else
            {
                return note;
            }
        }

        private bool HasDocPermission(TConfig cfg, string identity_id, string identity_name)
        {
            string sql = @"
                SELECT
                	t2.id
                FROM
                	[IDENTITY] t1 WITH(NOLOCK)
                INNER JOIN
                	[MEMBER] t2 WITH(NOLOCK)
                	ON t2.source_id = t1.id
                WHERE
                	t1.name = '{#identity_name}'
                	AND t2.related_id = '{#identity_id}'
            ";

            sql = sql.Replace("{#identity_name}", identity_name)
                .Replace("{#identity_id}", identity_id);

            Item itmIdentity = cfg.inn.applySQL(sql);

            return !itmIdentity.isError() && itmIdentity.getResult() != "";
        }

        #endregion 文管

        //財務區
        private void FinanceBox(TConfig cfg, string identity_id, Item itmReturn)
        {
            bool is_fd = false;
            if (cfg.isMeetingAdmin || cfg.login_is_admin == "1")
            {
                Item itmRoleResult = cfg.inn.applyMethod("In_Association_Role"
                    , "<in_sno>" + cfg.login_sno + "</in_sno>"
                    + "<idt_names>ACT_ASC_Accounting</idt_names>");

                is_fd = itmRoleResult.getProperty("inn_result", "") == "1";
            }

            if (is_fd)
            {
                //財務審核
                AppendApplyRefunds(cfg, identity_id, itmReturn, is_fd: true);
                itmReturn.setProperty("hide_refundView_box", "item_show_1");
            }
            else
            {
                itmReturn.setProperty("hide_refundView_box", "item_show_0");
            }
        }

        //驗證照片是否無上傳
        private string checkPhoto(TConfig cfg)
        {
            string isPhoto = "";
            string in_photo = cfg.LoginResume.getProperty("in_photo", "");
            string in_photo1_p01 = "";
            string in_photo1_p02 = "";

            string sql = @"SELECT * FROM in_resume_certificate 
                            WHERE SOURCE_ID ='{#login_resume_id}' AND IN_PHOTOKEY ='in_photo1'  ";
            sql = sql.Replace("{#login_resume_id}", cfg.login_resume_id);

            Item itmSql = cfg.inn.applySQL(sql);



            if (!itmSql.isError() && itmSql.getItemCount() > 0)
            {
                in_photo1_p01 = itmSql.getProperty("in_file1", "");
                in_photo1_p02 = itmSql.getProperty("in_file2", "");
            }

            if (in_photo == "" || in_photo1_p01 == "" || in_photo1_p02 == "")
            {
                isPhoto = "0";
            }
            else
            {
                isPhoto = "1";
            }


            return isPhoto;
        }

        //退費申請
        private void AppendApplyRefunds(TConfig cfg, string identity_id, Item itmReturn, bool is_admin = false, bool is_fd = false)
        {
            string type = "";
            string sqlFilter = "";
            string btn_func = "go_refund_ass_verify";

            if (is_admin)
            {
                btn_func = "go_refund_ass_verify";
                type = "inn_apply_refund_verify";

                if (cfg.isMeetingAdmin)
                {
                    sqlFilter = "ISNULL(t1.in_ass_ver_result, '') = ''";
                    itmReturn.setProperty("hide_editMeeting_s_box", "item_show_0");
                }
                else
                {
                    sqlFilter = "ISNULL(t1.in_ass_ver_result, '') = '' AND t2.in_meeting IN "
                        + "(SELECT source_id FROM IN_MEETING_RESUMELIST WITH(NOLOCK) WHERE related_id = '{#resume_id}')";
                }
            }
            else if (is_fd)
            {
                btn_func = "go_refund_fd_verify";
                type = "inn_apply_refund_fd";
                //sqlFilter = "ISNULL(t1.in_ass_ver_result, '') = '1' AND ISNULL(t1.in_fd_ver_result, '') = ''";
                sqlFilter = "ISNULL(t1.in_ass_ver_result, '') = '1'";
            }
            else
            {
                btn_func = "";
                type = "inn_apply_refund";
                sqlFilter = "t2.in_creator_sno = '" + cfg.login_sno + "'";
            }

            string sql = @"
                SELECT 
	                t1.id AS 'refund_id'
	                , t1.created_on AS 'in_cancel_time'
	                , t1.in_name
	                , t1.in_reason
	                , t1.in_ass_ver_result
	                , t1.in_ass_ver_memo
	                , t1.in_fd_ver_result
	                , t1.in_fd_ver_memo
	                , t2.in_creator
	                , t2.in_creator_sno
	                , t2.item_number    AS 'in_paynumber'
	                , t3.id             AS 'resume_id'
	                , t3.in_member_type AS 'resume_member_type'
	                , ISNULL(t2.in_meeting,t2.in_cla_meeting)       AS 'meeting_id'
	                , ISNULL(t4.in_title, t5.in_title)              AS 'in_title'
	                , ISNULL(t4.in_meeting_type,t5.in_meeting_type) AS 'in_meeting_type'
	                , ISNULL(t2.in_meeting, 'cla')                  AS 'mtype'
                FROM 
	                IN_MEETING_REFUND t1 WITH(NOLOCK)
                INNER JOIN 
	                IN_MEETING_PAY t2 WITH(NOLOCK)
	                ON t2.id = t1.source_id
                LEFT JOIN 
	                IN_RESUME t3 WITH(NOLOCK)
	                ON t3.in_sno = t2.in_creator_sno
                LEFT JOIN 
	                IN_MEETING t4 WITH(NOLOCK)
	                ON t4.id = t2.in_meeting
                LEFT JOIN 
	                IN_CLA_MEETING t5 WITH(NOLOCK)
	                ON t5.id = t2.in_cla_meeting
                WHERE 
	                {#sqlFilter}
                ORDER BY 
	                t1.created_on DESC
            ";

            sql = sql.Replace("{#sqlFilter}", sqlFilter)
                    .Replace("{#resume_id}", cfg.login_resume_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item itmApplys = cfg.inn.applySQL(sql);
            int apply_count = itmApplys.getItemCount();
            int no = 0;

            if (apply_count <= 0)
            {
                if (is_admin)
                {
                    itmReturn.setProperty("hide_refundMeeting_box", "item_show_0");
                }
                else
                {
                    itmReturn.setProperty("hide_refundMeeting_s_box", "item_show_0");
                }
                return;
            }

            for (int i = 0; i < apply_count; i++)
            {
                no++;

                Item itmApply = itmApplys.getItemByIndex(i);
                itmApply.setType(type);
                itmApply.setProperty("no", no.ToString());

                string mtype = itmApply.getProperty("mtype", "").Trim();
                string in_meeting_type = itmApply.getProperty("in_meeting_type", "");
                string in_ass_ver_result = itmApply.getProperty("in_ass_ver_result", "");
                string in_ass_ver_memo = itmApply.getProperty("in_ass_ver_memo", "");
                string in_fd_ver_result = itmApply.getProperty("in_fd_ver_result", "");
                string in_fd_ver_memo = itmApply.getProperty("in_fd_ver_memo", "");
                string in_paynumber = itmApply.getProperty("in_paynumber", "");

                string verify_status = "";
                string verify_color = "red";
                string verify_memo = "";

                if (in_ass_ver_result == "")
                {
                    verify_status = "退費申請中";
                    if (!is_admin)
                    {
                        verify_color = "black";
                    }
                }
                else if (in_ass_ver_result == "0")
                {
                    verify_status = "退費申請不通過";
                    verify_memo = in_ass_ver_memo;
                }
                else if (in_ass_ver_result == "1")
                {
                    if (in_fd_ver_result == "")
                    {
                        verify_status = "財務審核中";//退費申請通過
                    }
                    else if (in_fd_ver_result == "0")
                    {
                        verify_status = "退費申請不通過";
                        verify_memo = in_fd_ver_memo;
                    }
                    else if (in_fd_ver_result == "1")
                    {
                        verify_status = "退費申請通過";
                        verify_color = "green";
                    }
                }

                string in_meeting_type_name = "";
                switch (in_meeting_type)
                {
                    case "game":
                        in_meeting_type_name = "競賽";
                        break;
                    case "payment":
                        in_meeting_type_name = "會費";
                        break;
                    case "seminar":
                        in_meeting_type_name = "講習";
                        break;
                    case "degree":
                        in_meeting_type_name = "晉段";
                        break;
                }

                string status = "<span style='color:" + verify_color + ";'>" + verify_status + "</span>";
                if (is_fd && in_paynumber != "")
                {
                    status += "(" + in_paynumber + ")";
                }

                itmApply.setProperty("mtype", mtype);
                itmApply.setProperty("meeting_type", in_meeting_type_name);
                itmApply.setProperty("inn_status", status);
                itmApply.setProperty("inn_memo", verify_memo);
                itmApply.setProperty("inn_regdate", GetDateFormat(itmApply, "in_cancel_time", "yyyy年MM月dd日", 8));

                if (is_admin || is_fd)
                {
                    itmApply.setProperty("inn_ass_ver_btn", "<a class='btn btn-sm btn-primary' onclick='" + btn_func + "(this)'>審核</a>");
                }

                itmApply.setProperty("mode", "group_table_fold");
                itmReturn.addRelationship(itmApply);
            }
        }

        //報名變更
        private void AppendApplyMeeting(TConfig cfg, string identity_id, Item itmReturn, bool is_admin = false)
        {
            string type = "";
            string sqlFilter = "";
            if (is_admin)
            {
                type = "inn_apply_edit_verify";
                if (cfg.isMeetingAdmin)
                {
                    sqlFilter = "AND t1.in_cancel_status IN (N'變更申請中') ";
                    itmReturn.setProperty("hide_editMeeting_s_box", "item_show_0");
                }
                else
                {
                    sqlFilter = "AND t1.in_cancel_status IN (N'變更申請中')"
                        + " AND t2.in_meeting IN (SELECT source_id FROM IN_MEETING_RESUMELIST WITH(NOLOCK) WHERE related_id = '{#resume_id}')";
                }
            }
            else
            {
                type = "inn_apply_edit";
                sqlFilter = "AND t1.in_creator_sno ='" + cfg.login_sno + "' AND t1.in_cancel_status IN (N'變更申請中')";
            }

            string sql = @"
                SELECT 
	                t1.in_verify_memo
	                , t1.in_cancel_time
	                , t1.in_cancel_status
	                , t1.in_creator
	                , t1.in_creator_sno
	                , t2.item_number    AS 'in_paynumber'
	                , t3.id             AS 'resume_id'
	                , t3.in_member_type AS 'resume_member_type'
	                , ISNULL(t2.in_meeting,t2.in_cla_meeting)       AS 'meeting_id'
	                , ISNULL(t4.in_title, t5.in_title)              AS 'in_title'
	                , ISNULL(t4.in_meeting_type,t5.in_meeting_type) AS 'in_meeting_type'
	                , ISNULL(t2.in_meeting, 'cla')                  AS 'mtype'
                FROM 
	                IN_MEETING_NEWS t1 WITH(NOLOCK)
                INNER JOIN 
	                IN_MEETING_PAY t2 WITH(NOLOCK)
	                ON t2.id = t1.source_id
                LEFT JOIN 
	                IN_RESUME t3 WITH(NOLOCK)
	                ON t3.in_sno = t1.in_creator_sno
                LEFT JOIN 
	                IN_MEETING t4 WITH(NOLOCK)
	                ON t4.id = t2.in_meeting
                LEFT JOIN 
	                IN_CLA_MEETING t5 WITH(NOLOCK)
	                ON t5.id = t2.in_cla_meeting
                WHERE 
	                ISNULL(t1.in_cancel_status,'') != '' 
	                {#sqlFilter}
                ORDER BY 
	                in_cancel_time DESC
            ";

            sql = sql.Replace("{#sqlFilter}", sqlFilter)
                    .Replace("{#resume_id}", cfg.login_resume_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item itmApplys = cfg.inn.applySQL(sql);

            int apply_count = itmApplys.getItemCount();

            int no = 0;
            if (apply_count <= 0)
            {
                if (is_admin)
                {
                    itmReturn.setProperty("hide_editMeeting_box", "item_show_0");
                }
                else
                {
                    itmReturn.setProperty("hide_editMeeting_s_box", "item_show_0");
                }
                return;
            }

            for (int i = 0; i < apply_count; i++)
            {
                no++;
                Item itmApply = itmApplys.getItemByIndex(i);
                itmApply.setType(type);
                itmApply.setProperty("no", no.ToString());

                string in_cancel_status = itmApply.getProperty("in_cancel_status", "");
                string mtype = itmApply.getProperty("mtype", "").Trim();


                if (in_cancel_status.Contains("不通過"))
                {
                    in_cancel_status = "<span style='color:red;'>" + in_cancel_status + "</span>";
                }
                else if (in_cancel_status.Contains("通過"))
                {
                    string meeting_id = itmApply.getProperty("meeting_id", "");
                    string number = itmApply.getProperty("item_number", "");

                    if (in_cancel_status == "退費申請通過")
                    {
                        in_cancel_status = "<a onclick='go_payInfo(\"" + meeting_id + "\",\"" + number + "\",\"" + mtype + "\")'>" + in_cancel_status + "</a>";
                    }
                    else
                    {
                        in_cancel_status = "<span style='color:green;'>" + in_cancel_status + "</span>";
                    }
                }

                string in_meeting_type = itmApply.getProperty("in_meeting_type", "");
                string in_meeting_type_name = "";
                switch (in_meeting_type)
                {
                    case "game":
                        in_meeting_type_name = "競賽";
                        break;
                    case "payment":
                        in_meeting_type_name = "會費";
                        break;
                    case "seminar":
                        in_meeting_type_name = "講習";
                        break;
                    case "degree":
                        in_meeting_type_name = "晉段";
                        break;
                }
                itmApply.setProperty("mtype", mtype);
                itmApply.setProperty("meeting_type", in_meeting_type_name);
                itmApply.setProperty("in_cancel_status", in_cancel_status);
                itmApply.setProperty("inn_regdate", GetDateFormat(itmApply, "in_cancel_time", "yyyy年MM月dd日", 8));
                itmApply.setProperty("inn_ass_ver_btn", "<a class='btn btn-sm btn-primary' onclick='go_payment_verify(this)'>審核</a>");
                itmApply.setProperty("mode", "exchange_table_fold");
                itmReturn.addRelationship(itmApply);
            }
        }

        //各類申請審核
        private void AppendApplyVerify(TConfig cfg, string identity_id, Item itmReturn, bool is_finished = false)
        {
            //lina 2021.04.30 資料太多，審核通過改到歷史記錄查看
            string ver_condition = is_finished
                ? "AND ISNULL(t1.in_ass_ver_result, '') = '0'"
                : "AND ISNULL(t1.in_ass_ver_result, '') = ''";

            string ver_type = is_finished
                ? "inn_verify_finished"
                : "inn_verify";

            string ver_box = is_finished
                ? "hide_verify_box_finished"
                : "hide_verify_box";

            string sql = @"
                SELECT 
                    IIF( ISNULL(t1.IN_EMAIL,'') ='', IIF(t2.in_email='na@na.n','',t2.in_email),t1.IN_EMAIL) AS ms
					, t4.IN_LINE_NOTIFY AS ls
	                , t1.*
	                , t2.id AS 'resume_id'
	                , t2.in_member_type
	                , t2.in_member_role
	                , t2.in_member_unit
                	, t3.in_pay_amount_real
                	, t3.in_pay_photo
                	, t3.in_return_mark
                	, t3.in_collection_agency
                FROM 
	                IN_MEETING_USER t1 WITH(NOLOCK) 
	            LEFT OUTER JOIN
	                IN_RESUME t2 WITH(NOLOCK) 
	                ON t2.in_sno = t1.in_creator_sno
	            LEFT OUTER JOIN
	                IN_MEETING_PAY t3 WITH(NOLOCK)
	                ON t3.item_number = t1.in_paynumber
                LEFT OUTER JOIN
					innovator.[user] t4 WITH(NOLOCK)
					ON T4.LOGIN_NAME = t2.in_sno and T4.IN_RESUME = t2.id
                WHERE 
	                t1.source_id = '{#meeting_id}' 
	                AND t1.in_ass_ver_identity IN (SELECT source_id FROM MEMBER WHERE related_id = '{#identity_id}')
	                {#ver_condition}
	            ORDER BY
	                t1.in_l1
	                , t1.in_l2
	                , t1.in_ass_ver_result
	                , t1.in_sno
            ";

            sql = sql.Replace("{#meeting_id}", cfg.apply_meeting_id)
                .Replace("{#identity_id}", identity_id)
                .Replace("{#ver_condition}", ver_condition);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item itmApplys = cfg.inn.applySQL(sql);
            int apply_count = itmApplys.getItemCount();
            int no = 0;

            for (int i = 0; i < apply_count; i++)
            {
                string imgStr = "";
                Item itmApply = itmApplys.getItemByIndex(i);

                if (Over6Months(cfg, itmApply))
                {
                    continue;
                }

                no++;

                itmApply.setType(ver_type);
                itmApply.setProperty("no", no.ToString());
                itmApply.setProperty("inn_item", GetApplyDetail_in_l2(cfg, itmApply));
                itmApply.setProperty("inn_regdate", GetDateFormat(itmApply, "in_regdate", "yyyy年MM月dd日", 8));
                itmApply.setProperty("inn_ass_ver_status", GetVerify(itmApply));
                itmApply.setProperty("inn_ass_ver_btn", GetVerifyBtn(itmApply));
                itmApply.setProperty("inn_scene", "verify");
                if (itmApply.getProperty("ms", "") != "")
                {
                    imgStr += "<img src='../Images/email.png' width='20px' height='20px'>";
                }
                if (itmApply.getProperty("ls", "") != "")
                {
                    imgStr += "<img src='../pages/Images/round-default-small.png' width='20px' height='20px'>";
                }
                itmApply.setProperty("in_notice", imgStr);
                itmApply.setProperty("inn_freq", "<button class='notice-btn freq_" + itmApply.getProperty("id", "") + "'>" + itmApply.getProperty("in_notice_freq", "0") + "</button>");
                itmApply.setProperty("resume_member_type", itmApply.getProperty("in_member_type", ""));
                itmApply.setProperty("resume_member_role", itmApply.getProperty("in_member_role", ""));
                itmApply.setProperty("inn_changed", GetApplyChanged(cfg, itmApply));

                itmReturn.addRelationship(itmApply);
            }

            Item itmApplys2 = GetApplyMembers(cfg, is_finished);
            int apply_count2 = itmApplys2.getItemCount();
            for (int i = 0; i < apply_count2; i++)
            {
                Item itmApply = itmApplys2.getItemByIndex(i);
                if (Over6Months(cfg, itmApply))
                {
                    continue;
                }

                no++;

                string resume_id = itmApply.getProperty("resume_id", "");
                string resume_member_type = itmApply.getProperty("resume_member_type", "");
                string resume_member_role = itmApply.getProperty("resume_member_role", "");
                string resume_member_unit = itmApply.getProperty("resume_member_unit", "");

                string in_member_display = "";
                if (resume_member_type == "vip_group")
                {
                    in_member_display = "<span style='color: red'>會員申請(一般團體)<span>";
                }
                else if (resume_member_type == "vip_mbr")
                {
                    in_member_display = "<span style='color: red'>會員申請(個人會員)<span>";
                }
                else if (resume_member_type == "vip_minority")
                {
                    in_member_display = "<span style='color: red'>會員申請(準會員)<span>";
                }
                else if (resume_member_type == "vip_gym")
                {
                    in_member_display = resume_member_unit == "館"
                        ? "<span style='color: red'>會員申請(道館)<span>"
                        : "<span style='color: red'>會員申請(學校社團)<span>";
                }
                string imgStr = "";

                if (itmApply.getProperty("ms", "") != "")
                {
                    imgStr += "<img src='../Images/email.png' width='18px' height='18px'>";
                }
                if (itmApply.getProperty("ls", "") != "")
                {
                    imgStr += "<img src='../pages/Images/round-default-small.png' width='18px' height='18px'>";
                }
                itmApply.setProperty("inn_freq", "<button class='notice-btn freq_" + itmApply.getProperty("id", "") + "'>" + itmApply.getProperty("in_notice_freq", "0") + "</button>");
                itmApply.setType(ver_type);
                itmApply.setProperty("no", no.ToString());
                itmApply.setProperty("in_l1", "會員類");
                itmApply.setProperty("inn_item", in_member_display);
                itmApply.setProperty("in_creator", itmApply.getProperty("in_name", ""));
                itmApply.setProperty("in_creator_sno", itmApply.getProperty("in_sno", ""));
                itmApply.setProperty("inn_regdate", GetDateFormat(itmApply, "in_regdate", "yyyy年MM月dd日", 8));

                itmApply.setProperty("inn_ass_ver_status", "待審核");
                itmApply.setProperty("inn_ass_ver_status", GetVerify(itmApply));
                itmApply.setProperty("in_notice", imgStr);
                itmApply.setProperty("inn_ass_ver_btn", "<a class='btn btn-sm btn-primary' onclick='go_member(this)'>審核</a>");
                itmApply.setProperty("inn_scene", "verify");
                itmApply.setProperty("inn_changed", GetApplyChanged(cfg, itmApply));

                itmReturn.addRelationship(itmApply);
            }

            if (apply_count > 0 || apply_count2 > 0)
            {
                itmReturn.setProperty(ver_box, "");
            }
            else
            {
                itmReturn.setProperty(ver_box, "item_show_0");
            }
        }

        private bool Over6Months(TConfig cfg, Item item)
        {
            bool is_over = false;

            string in_ass_ver_result = item.getProperty("in_ass_ver_result", "");
            string in_ass_ver_time = item.getProperty("in_ass_ver_time", "");

            DateTime ver_time = GetDateTime(in_ass_ver_time, 8);
            TimeSpan ts = cfg.Now - ver_time;
            if (ts.TotalDays > 60)
            {
                is_over = true;
            }

            return is_over;
        }

        private Item GetApplyMembers(TConfig cfg, bool is_finished)
        {
            string verify_condition = is_finished
                ? "AND ISNULL(t1.in_ass_ver_result, '') <> ''"
                : "AND ISNULL(t1.in_ass_ver_result, '') = ''";

            string member_condition = "";

            if (!cfg.isMeetingAdmin)
            {
                string chairman_id = "B8F2DA2ED4BF4AC1885270084185C557";//理事長
                string secretary_id = "05292A9E596C4C9DB734B2DC3E247A60";//秘書長(含副秘書長)
                string admin_id = "7833055F8A3E44948E4E1F70F4F6C4A5";//行政組
                string degree_id = "1FDEB413DD614E24841DF5559A955695";//晉段組
                string login_identity_list = Aras.Server.Security.Permissions.Current.IdentitiesList;

                if (cfg.CCO.Permissions.IdentityListHasId(login_identity_list, chairman_id))
                {
                    member_condition = "";
                }
                else if (cfg.CCO.Permissions.IdentityListHasId(login_identity_list, secretary_id))
                {
                    member_condition = "";
                }
                else if (cfg.CCO.Permissions.IdentityListHasId(login_identity_list, admin_id))
                {
                    member_condition = "AND in_member_type in ('u_mbr', 'vip_mbr', 'vip_minority', 'vip_group')";
                }
                else if (cfg.CCO.Permissions.IdentityListHasId(login_identity_list, degree_id))
                {
                    member_condition = "AND in_member_type in ('gym', 'vip_gym')";
                }
                else
                {
                    member_condition = "AND ISNULL(in_member_type, '') = ''";
                }
            }

            string sql = @"
                SELECT 
                    IIF( ISNULL(t1.IN_EMAIL,'') ='', IIF(t2.in_email='na@na.n','',t2.in_email),t1.IN_EMAIL) AS ms
					, t3.IN_LINE_NOTIFY AS ls
	                , t1.*
	                , t2.id AS 'resume_id'
	                , t2.in_member_type AS 'resume_member_type'
	                , t2.in_member_role AS 'resume_member_role'
	                , t2.in_member_unit AS 'resume_member_unit'
                FROM 
	                IN_MEETING_USER t1 WITH(NOLOCK) 
                INNER JOIN
	                IN_RESUME t2 WITH(NOLOCK)
	                ON t2.in_sno = t1.in_sno
                LEFT OUTER JOIN
					innovator.[user] t3 WITH(NOLOCK)
					ON T3.LOGIN_NAME = t2.in_sno and T3.IN_RESUME = t2.id
                WHERE 
	                t1.source_id in ('249FDB244E534EB0AA66C8E9C470E930', '5F73936711E04DC799CB02587F4FF7E0', '83B87AE0033640AA8DBA7AF2CF659479', '38CAB90DF1274E048520A801948AC65C')
	                AND t1.IN_MEMBER_APPLY = N'是'
	                AND t1.in_name NOT IN (N'鍾依', N'新創里道館', N'張朵朵', N'田純嫣', N'跆拳', N'A')
                    {#member_condition}
                    {#verify_condition}
                ORDER BY
	                t2.in_member_type
	                , t2.in_member_unit
	                , t1.created_on
            ";

            sql = sql.Replace("{#member_condition}", member_condition)
                .Replace("{#verify_condition}", verify_condition);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        //各類申請
        private void AppendApply(TConfig cfg, Item itmReturn)
        {
            if (cfg.login_sno == "M001" || cfg.login_sno == "lwu001")
            {
                itmReturn.setProperty("hide_apply_box", "item_show_0");
            }
            else
            {
                itmReturn.setProperty("hide_apply_box", "");
            }


            // //暫時停止申請
            // if(cfg.login_member_type !="asc"){
            //     itmReturn.setProperty("hide_apply_btn", "item_show_0");
            // }else{
            //     itmReturn.setProperty("hide_apply_btn", "");
            // }

            #region 各類申請選單

            string in_member_status = cfg.LoginResume.getProperty("in_member_status", "");
            string v_condition = "AND in_value LIKE N'%資料變更'";
            if (in_member_status == "暫時會員")
            {
                v_condition = "AND (in_value LIKE N'%資料變更' OR in_value LIKE N'%申請表%')";
            }

            string sql = @"
                SELECT 
                	source_id	AS 'survey_id'
                	, id		AS 'option_id'
                	, in_filter
                	, in_value
                	, in_label
                	, in_expense_value
                	, in_list
                	, in_verify_identity
                	, in_register_org
                FROM 
	                IN_SURVEY_OPTION WITH(NOLOCK)
                WHERE 
	                source_id IN 
	                (
		                SELECT t2.id FROM IN_MEETING_SURVEYS t1 WITH(NOLOCK) INNER JOIN IN_SURVEY t2 WITH(NOLOCK) ON t2.id = t1.related_id 
		                WHERE t1.source_id = '{#meeting_id}' AND t2.in_property = N'in_l2'
	                )
					AND in_filter = N'會員類'
					{#v_condition}
	            ORDER BY
	                sort_order
            ";

            sql = sql.Replace("{#meeting_id}", cfg.apply_meeting_id)
                .Replace("{#v_condition}", v_condition);

            // cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();

            Dictionary<string, List<Item>> map = new Dictionary<string, List<Item>>();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_filter = item.getProperty("in_filter", "");
                string in_value = item.getProperty("in_value", "");
                string in_register_org = item.getProperty("in_register_org", "");

                List<Item> list = null;
                if (map.ContainsKey(in_filter))
                {
                    list = map[in_filter];
                }
                else
                {
                    list = new List<Item>();
                    map.Add(in_filter, list);
                }

                if (cfg.isMeetingAdmin)
                {
                    list.Add(item);
                }
                else if (in_register_org == "" || in_register_org.Contains(cfg.login_member_type))
                {
                    if (cfg.login_member_unit == "社" && in_value == "道館會員申請表")
                    {
                        item.setProperty("in_value", in_value.Replace("道館", "學校社團"));
                    }
                    list.Add(item);
                }
            }

            StringBuilder contents = new StringBuilder();
            foreach (var kv in map)
            {
                var key = kv.Key;
                contents.Append("<optgroup label='" + key + "'>");
                foreach (var item in kv.Value)
                {
                    string label = item.getProperty("in_value", "");
                    string value = key + "_" + label;
                    contents.Append("<option value='" + value + "'>" + label + "</option>");
                }
                contents.Append("</optgroup>");
            }

            StringBuilder ctrl = new StringBuilder();

            ctrl.Append("<select id='apply_options'"
                + " class='form-select form-select-lg mb-3'"
                + " style='font-size: 18px; min-height: 28px; '"
                + " data-meeting='" + cfg.apply_meeting_id + "'"
                + " onchange='Apply_Change(this)'>");

            ctrl.Append(contents);
            ctrl.Append("</select>");

            itmReturn.setProperty("inn_apply_select", ctrl.ToString());

            #endregion 各類申請選單


            sql = @"
                SELECT 
                	t1.in_l2, count(*) as count
                FROM 
                	IN_MEETING_USER t1 WITH(NOLOCK)
                LEFT OUTER JOIN
                    IN_RESUME t2 WITH(NOLOCK)
                    ON t2.in_sno = t1.in_sno
                LEFT OUTER JOIN
                	IN_MEETING_PAY t3 WITH(NOLOCK)
                	ON t3.item_number = t1.in_paynumber
                WHERE 
                	t1.source_id = '{#meeting_id}' 
                	AND t1.in_creator_sno = '{#in_creator_sno}' 
                	AND IN_ASS_VER_RESULT IS NULL
                GROUP BY t1.in_l2
            ";

            sql = sql.Replace("{#meeting_id}", cfg.apply_meeting_id)
                .Replace("{#in_creator_sno}", cfg.login_sno);
            Item itmCounts = cfg.inn.applySQL(sql);
            itmReturn.setProperty("countME", "0");
            itmReturn.setProperty("countIRU", "0");
            itmReturn.setProperty("countI", "0");
            itmReturn.setProperty("countR", "0");
            if (!itmCounts.isError() && itmCounts.getItemCount() > 0)
            {
                for (int i = 0; i < itmCounts.getItemCount(); i++)
                {
                    Item itmCount = itmCounts.getItemByIndex(i);
                    string in_l2_type = itmCount.getProperty("in_l2", "");
                    string in_l2_count = itmCount.getProperty("count", "");
                    switch (in_l2_type)
                    {
                        case "個人資料變更":
                        case "個人會員資料變更":
                            itmReturn.setProperty("countME", in_l2_count);
                            break;
                        case "國際裁判講習結業證書上傳":
                            itmReturn.setProperty("countIRU", in_l2_count);
                            break;
                        case "教練證補發":
                            itmReturn.setProperty("countI", in_l2_count);
                            break;
                        case "裁判證補發":
                            itmReturn.setProperty("countR", in_l2_count);
                            break;
                    }
                }
            }


            int no = 0;

            sql = @"
                SELECT 
                	t1.*
                	, t2.id AS 'resume_id'
                	, t3.in_pay_amount_real
                	, t3.in_pay_photo
                	, t3.in_return_mark
                	, t3.in_collection_agency
                FROM 
                	IN_MEETING_USER t1 WITH(NOLOCK)
                LEFT OUTER JOIN
                    IN_RESUME t2 WITH(NOLOCK)
                    ON t2.in_sno = t1.in_sno
                LEFT OUTER JOIN
                	IN_MEETING_PAY t3 WITH(NOLOCK)
                	ON t3.item_number = t1.in_paynumber
                WHERE 
                	t1.source_id = '{#meeting_id}' 
                	AND t1.in_creator_sno = '{#in_creator_sno}' 
                ORDER BY 
                	t1.created_on DESC
            ";


            sql = sql.Replace("{#meeting_id}", cfg.apply_meeting_id)
                .Replace("{#in_creator_sno}", cfg.login_sno);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item itmApplys = cfg.inn.applySQL(sql);
            int apply_count = itmApplys.getItemCount();
            for (int i = 0; i < apply_count; i++)
            {
                no++;

                Item itmApply = itmApplys.getItemByIndex(i);
                string in_l2 = itmApply.getProperty("in_l2", "");
                string in_ass_ver_result = itmApply.getProperty("in_ass_ver_result", "");
                string pay_bool = itmApply.getProperty("pay_bool", "");

                itmApply.setType("inn_apply");
                itmApply.setProperty("no", no.ToString());
                itmApply.setProperty("inn_item", GetApplyDetail_in_l2(cfg, itmApply));
                itmApply.setProperty("inn_regdate", GetDateFormat(itmApply, "in_regdate", "yyyy年MM月dd日", 8));
                itmApply.setProperty("inn_verify", GetVerify(itmApply));
                itmApply.setProperty("inn_detail", GetApplyDetail_1(cfg, itmApply));
                itmApply.setProperty("inn_changed", GetApplyChanged(cfg, itmApply));


                if (in_ass_ver_result == "0")
                {
                    itmApply.setProperty("edit_btn", "<button class='btn btn-sm btn-success' onclick='ApplEdit_Click(this)'>修改</button>");
                    itmApply.setProperty("del_btn", "&nbsp;<button class='btn btn-sm btn-danger' onclick='CancelApply_Click(this)'>取消申請</button>");
                }
                else if (in_l2.Contains("補發"))
                {
                    itmApply.setProperty("edit_btn", "<button class='btn btn-sm btn-success' onclick='DownloadReissue_Click(this)'>下載</button>&ensp;");
                }


                if (in_ass_ver_result == "" && (pay_bool == "未繳費" || pay_bool == ""))
                {
                    itmApply.setProperty("edit_btn2", "<button class='btn btn-sm btn-danger' onclick='CancelApply_Click(this)'>取消申請</button>");
                }


                itmReturn.addRelationship(itmApply);
            }

            string sql2 = @"
                SELECT 
					t1.id         AS 'meeting_id'
                    , t1.in_title AS 'meeting_title'
					, t2.*
                    , t3.id             AS 'resume_id'
                    , t3.in_member_type AS 'resume_member_type'
                    , t3.in_member_role AS 'resume_member_role'
                FROM 
                    IN_MEETING t1 WITH(NOLOCK)
				INNER JOIN
                    IN_MEETING_USER t2 WITH(NOLOCK)
					ON t2.source_id = t1.id
                INNER JOIN
					IN_RESUME t3 WITH(NOLOCK)
                    ON t3.login_name = t2.in_sno
                WHERE 
                    t1.in_meeting_type = 'registry'
                    AND t2.in_member_apply = N'是'
                    AND t2.in_sno = '{#in_sno}'
                ORDER BY
                    t2.created_on
            ";

            sql2 = sql2.Replace("{#in_sno}", cfg.login_sno);
            Item itmApplys2 = cfg.inn.applySQL(sql2);
            int apply_count2 = itmApplys2.getItemCount();

            for (int i = 0; i < apply_count2; i++)
            {
                no++;
                Item itmApply = itmApplys2.getItemByIndex(i);


                string resume_id = itmApply.getProperty("resume_id", "");
                string resume_member_type = itmApply.getProperty("resume_member_type", "");
                string resume_member_role = itmApply.getProperty("resume_member_role", "");
                string resume_member_unit = itmApply.getProperty("resume_member_unit", "");


                string in_member_display = "";
                if (resume_member_type == "vip_group")
                {
                    in_member_display = "<span style='color: red'>會員申請(一般團體)<span>";
                }
                else if (resume_member_type == "vip_mbr")
                {
                    in_member_display = "<span style='color: red'>會員申請(個人會員)<span>";
                }
                else if (resume_member_type == "vip_minority")
                {
                    in_member_display = "<span style='color: red'>會員申請(準會員)<span>";
                }
                else if (resume_member_type == "vip_gym")
                {
                    in_member_display = resume_member_unit == "館"
                        ? "<span style='color: red'>會員申請(道館)<span>"
                        : "<span style='color: red'>會員申請(學校社團)<span>";
                }


                itmApply.setType("inn_apply");
                itmApply.setProperty("no", no.ToString());
                itmApply.setProperty("in_l1", "會員類");
                itmApply.setProperty("in_l2", in_member_display);
                itmApply.setProperty("in_creator", itmApply.getProperty("in_name", ""));
                itmApply.setProperty("in_creator_sno", itmApply.getProperty("in_sno", ""));
                itmApply.setProperty("inn_regdate", GetDateFormat(itmApply, "in_regdate", "yyyy年MM月dd日", 8));
                itmApply.setProperty("inn_verify", GetVerify(itmApply));
                itmApply.setProperty("inn_detail", GetApplyDetail_2(cfg, itmApply));
                itmApply.setProperty("inn_changed", GetApplyChanged(cfg, itmApply));


                itmReturn.addRelationship(itmApply);
            }
        }

        private string GetApplyDetail_in_l2(TConfig cfg, Item item)
        {

            string in_l2 = item.getProperty("in_l2", "");
            string in_paynumber = item.getProperty("in_paynumber", "");

            string result = in_l2;

            if (in_paynumber != "")
            {
                result = in_l2 + " <a onclick='PaymentView_Click(this)'>" + GetPayStatusIcon(cfg.inn, item) + "</a>";
            }
            else
            {
                result = in_l2;
            }

            return result;
        }

        private string GetApplyChanged(TConfig cfg, Item item)
        {
            string in_is_changed = item.getProperty("in_is_changed", "");
            return in_is_changed == "1"
                ? "已修改"
                : "";
        }

        private string GetApplyDetail_1(TConfig cfg, Item item)
        {
            return "<a onclick='ApplyDetail_Click(this)'><i class='fa fa-commenting'></i> 明細</a>";
        }

        private string GetApplyDetail_2(TConfig cfg, Item item)
        {
            return "入會申請";
        }

        private string GetPayStatusIcon(Innovator inn, Item item)
        {
            StringBuilder builder = new StringBuilder();

            string[] properties = new string[]
            {
                "in_paynumber", //繳費單號
                "in_pay_amount_real", //實際收款金額
                "in_pay_photo", //上傳繳費照片(繳費收據)
                "in_return_mark", //審核退回說明
                "in_collection_agency", //代收機構
            };

            foreach (string property in properties)
            {
                builder.Append("<" + property + ">" + item.getProperty(property, "") + "</" + property + ">");
            }
            //未確認
            builder.Append("<option>1</option>");

            Item itmResult = inn.applyMethod("In_PayType_Icon", builder.ToString());

            return itmResult.getProperty("in_pay_type", "");
        }

        private string GetVerify(Item item)
        {
            string in_ass_ver_result = item.getProperty("in_ass_ver_result", "");
            if (in_ass_ver_result == "")
            {
                //return "<span style='color: #2C4198; cursor: pointer;'><i class='fa fa-list-alt'></i> 審核中 </span>";
                return "<span style='color: #2C4198; '> 審核中 </span>";
            }
            else if (in_ass_ver_result == "0")
            {
                //return "<span style='color: red; cursor: pointer;'><i class='fa fa-list-alt'></i> 不通過 </span>";
                return "<span style='color: red; '> 不通過 </span>";
            }
            else
            {
                //return "<span style='color: green; cursor: pointer;'><i class='fa fa-list-alt'></i> 審核通過 </span>";
                return "<span style='color: green; '> 審核通過 </span>";
            }
        }

        private string GetVerifyBtn(Item item)
        {
            string muid = item.getProperty("id", "");
            string in_l1 = item.getProperty("in_l1", "");
            string in_l2 = item.getProperty("in_l2", "");
            return "<button class='btn btn-sm btn-primary' onclick='ApplyVerify_Click(this)'>審核</button>";
        }

        //復權
        private void AppendRecoverFunc(TConfig cfg, Item itmReturn)
        {
            //民國年度
            string chinese_year = (DateTime.Now.Year - 1911).ToString();

            string in_member_type = cfg.login_member_type;
            //館社
            string in_member_unit = cfg.LoginResume.getProperty("in_member_unit", "");
            string in_member_status = cfg.LoginResume.getProperty("in_member_status", "");

            string hide_status = "item_show_0";
            string hide_recover = "item_show_0";

            switch (in_member_status)
            {
                case "暫時會員":
                case "暫時停權":
                case "除名":
                    hide_status = "";
                    break;

                case "出會":
                    hide_status = "";
                    //hide_recover = "";
                    break;

                default:
                    break;
            }

            itmReturn.setProperty("in_member_status", in_member_status);
            itmReturn.setProperty("hide_status", hide_status);
            itmReturn.setProperty("hide_recover", hide_recover);

            if (hide_recover == "")
            {
                Item itmPayMeeting = null;

                switch (in_member_type)
                {
                    case "mbr":
                    case "vip_mbr": //個人會員
                    case "vip_minority": //個人會員
                    case "vip_group"://第一類團體會員
                        itmPayMeeting = cfg.inn.applySQL("SELECT id FROM IN_MEETING WITH(NOLOCK) WHERE in_annual = '" + chinese_year + "' AND in_member_type = N'個人會員'");
                        break;

                    case "gym"://第二類團體會員
                    case "vip_gym":
                        itmPayMeeting = cfg.inn.applySQL("SELECT id FROM IN_MEETING WITH(NOLOCK) WHERE in_annual = '" + chinese_year + "' AND in_member_type = N'團體會員'");
                        break;

                    default:
                        break;
                }

                if (itmPayMeeting != null && !itmPayMeeting.isError())
                {
                    itmReturn.setProperty("recover_meeting", itmPayMeeting.getProperty("id", ""));
                }
            }
        }

        private string GetMeetingCount(Dictionary<string, List<Item>> map, string in_meeting_type)
        {
            if (map.ContainsKey(in_meeting_type))
            {
                return map[in_meeting_type].Count.ToString();
            }
            else
            {
                return "0";
            }
        }

        private Dictionary<string, List<Item>> GetAllMeetings(TConfig cfg)
        {
            Dictionary<string, List<Item>> result = new Dictionary<string, List<Item>>();
            AppendMap(result, GetMeetings(cfg, "IN_MEETING"));
            AppendMap(result, GetMeetings(cfg, "IN_CLA_MEETING"));
            return result;
        }

        private void AppendMap(Dictionary<string, List<Item>> map, Item items)
        {
            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_meeting_type = item.getProperty("in_meeting_type", "");

                List<Item> list = null;
                if (map.ContainsKey(in_meeting_type))
                {
                    list = map[in_meeting_type];
                }
                else
                {
                    list = new List<Item>();
                    map.Add(in_meeting_type, list);
                }
                list.Add(item);
            }
        }

        //取得下拉選單
        private Item GetValues(TConfig cfg, string list_name)
        {
            string sql = @"
                SELECT
                    t2.value
			        , t2.label_zt AS 'label'
                FROM
                    [LIST] t1 WITH(NOLOCK)
		        INNER JOIN
			        [VALUE] t2 WITH(NOLOCK)
			        ON t2.source_id = t1.id
                WHERE
			        t1.name = N'{#list_name}'
		        ORDER BY
			        t2.sort_order
            ";

            sql = sql.Replace("{#list_name}", list_name);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        private Item GetMeetings(TConfig cfg, string item_tye)
        {
            //統計該單位的各項服務(排除掉報名用的)
            string sql = @"
                SELECT 
                    id
                    , in_meeting_type
                FROM 
                    {#item_tye} WITH(NOLOCK)
                WHERE
                    id NOT IN (
                        '249FDB244E534EB0AA66C8E9C470E930'
                        , '5F73936711E04DC799CB02587F4FF7E0'
                        , 'A82D77E6DCF84271ACFFC9742321057E'
                    )
                    AND in_is_template = '0'
                    AND in_is_main = '0'
                    AND in_date_e > '{#in_date_e}'
            ";

            sql = sql.Replace("{#item_tye}", item_tye)
                .Replace("{#in_date_e}", cfg.NowStr);

            //CCO.Utilities.WriteDebug("in_user_dashboard", "sql:" + sql);

            return cfg.inn.applySQL(sql);
        }

        //待繳費記錄
        private void AppendPayment(TConfig cfg, Item itmReturn)
        {
            Item items = GetPayments(cfg);

            if (items.isError() || items.getItemCount() <= 0)
            {
                return;
            }

            int count = items.getItemCount();
            int max_index = count - 1;
            string pay_detail_data = "";
            string type = "";
            string pay_html = "";
            string pay_method = "";
            string func1 = "";
            for (int i = 0; i < count; i++)
            {
                Item itm = items.getItemByIndex(i);
                string item_number = itm.getProperty("item_number", "");
                string in_title = itm.getProperty("in_title", "");
                string meeting_url = itm.getProperty("meeting_url", "").ToLower();
                string in_member_type = itm.getProperty("in_member_type", "");
                bool is_team_bill = in_member_type == "團隊計費";
                if (in_member_type == "人頭計費") is_team_bill = true;

                string item_name = itm.getProperty("item_name", "");
                string item_amount = itm.getProperty("item_amount", "");
                string item_count = itm.getProperty("item_count", "");
                string subtotal = GetSubtotal(item_count, item_amount);
                type = itm.getProperty("sort", "");

                string detail = item_name + " $" + item_amount + " x " + item_count + " 小計 $" + subtotal;
                if (is_team_bill)
                {
                    detail = item_name + " x " + item_count + " (" + in_member_type + ")";
                }
                pay_detail_data += "<span class='product-description'>" + detail + "</span>";


                if (type == "1")
                {
                    //賽事
                    pay_html = "barcode_list.html";
                    pay_method = "In_Payment_List_Barcode";
                    func1 = "player_group";
                }
                else
                {
                    if (meeting_url == "in_cla_meeting_list_b.html")
                    {
                        //課程-盲報
                        pay_html = "Cla_barcode_list_V1.html";
                        pay_method = "In_Cla_Payment_List_Barcode_V1";
                        func1 = "in_sno";

                    }
                    else
                    {
                        //課程
                        pay_html = "Cla_barcode_list.html";
                        pay_method = "In_Cla_Payment_List_Barcode_V1";
                        func1 = "in_group";
                    }
                }
                //最後一筆
                if (i == max_index)
                {
                    itm.setType("inn_payment");
                    itm.setProperty("inn_title", in_title + " " + item_number);
                    itm.setProperty("inn_pay_detail", pay_detail_data);
                    itm.setProperty("pay_html", pay_html);
                    itm.setProperty("pay_method", pay_method);
                    itm.setProperty("func1", func1);
                    itmReturn.addRelationship(itm);

                    pay_detail_data = "";
                }
                else
                {
                    Item itmNext = items.getItemByIndex(i + 1);
                    string next_item_number = itmNext.getProperty("item_number", "");

                    if (item_number != next_item_number)
                    {
                        itm.setType("inn_payment");
                        itm.setProperty("inn_title", in_title + " " + item_number);
                        itm.setProperty("inn_pay_detail", pay_detail_data);
                        itm.setProperty("pay_html", pay_html);
                        itm.setProperty("pay_method", pay_method);
                        itm.setProperty("func1", func1);
                        itmReturn.addRelationship(itm);

                        pay_detail_data = "";
                    }
                }
            }
        }

        //審核未通過
        private void AppendReject(TConfig cfg, Item itmReturn)
        {
            List<TMeeting> list = new List<TMeeting>();
            MergeMeetins(list, GetRejects(cfg, "", cfg.login_sno), no_time: true);
            MergeMeetins(list, GetRejects(cfg, "cla", cfg.login_sno), no_time: true);

            if (list.Count > 0)
            {
                itmReturn.setProperty("hide_reject_box", "");
            }
            else
            {
                itmReturn.setProperty("hide_reject_box", "item_show_0");
            }

            for (int i = 0; i < list.Count; i++)
            {
                TMeeting meeting = list[i];
                Item item = meeting.Value;
                item.setType("inn_reject");
                item.setProperty("no", (i + 1).ToString());
                item.setProperty("inn_regdate", GetDateFormat(item, "in_regdate", "yyyy年MM月dd日", 0));
                itmReturn.addRelationship(item);
            }
        }

        //相關活動(個人訊息)
        private void AppendMeetings(TConfig cfg, Item itmReturn)
        {
            List<TMeeting> list = new List<TMeeting>();

            //協助報名
            MergeMeetins(list, GetCreatorMeetings(cfg, "IN_MEETING", ""));
            MergeMeetins(list, GetCreatorMeetings(cfg, "IN_CLA_MEETING", "cla"));
            //被報名
            MergeMeetins(list, GetRegistryMeetings(cfg, "IN_MEETING", ""));
            MergeMeetins(list, GetRegistryMeetings(cfg, "IN_CLA_MEETING", "cla"));
            //被列為工作人員
            MergeMeetins(list, GetWorkMeetings(cfg, "IN_MEETING", ""));
            MergeMeetins(list, GetWorkMeetings(cfg, "IN_CLA_MEETING", "cla"));

            if (cfg.isMeetingAdmin)
            {
                //比賽待審核
                MergeMeetins(list, GetVerifyMeetings2(cfg, "IN_MEETING", ""));
                //講習待審核
                MergeMeetins(list, GetVerifyMeetings2(cfg, "IN_CLA_MEETING", "cla"));
                //晉段待審核
                MergeMeetins(list, GetVerifyMeetings3(cfg, "IN_CLA_MEETING", "cla", true));
            }
            else if (cfg.isCommittee)
            {
                //晉段待審核
                MergeMeetins(list, GetVerifyMeetings3(cfg, "IN_CLA_MEETING", "cla", false));
            }
            else
            {
                //共同講師待審核
                Item itmCoMeetings1 = GetVerifyMeetingsCO(cfg, "IN_CLA_MEETING", "cla", "講習");
                Item itmCoMeetings2 = GetVerifyMeetingsCO(cfg, "IN_MEETING", "", "競賽");

                MergeMeetins(list, itmCoMeetings1);
                MergeMeetins(list, itmCoMeetings2);

                string inn_meeting_teacher = "";
                if (!itmCoMeetings1.isError() && itmCoMeetings1.getResult() != "")
                {
                    inn_meeting_teacher = "1";
                }
                else if (!itmCoMeetings2.isError() && itmCoMeetings2.getResult() != "")
                {
                    inn_meeting_teacher = "1";
                }
                itmReturn.setProperty("inn_meeting_teacher", inn_meeting_teacher);
            }

            //年費繳納
            MergeMeetins(list, GetCreatorPayment(cfg));

            int count = list.Count;

            IOrderedEnumerable<TMeeting> entities = list.OrderByDescending(x => x.SortTime);

            int i = 0;
            foreach (TMeeting entity in entities)
            {
                Item item = entity.Value;
                string meeting_id = item.getProperty("meeting_id", "");
                string meeting_mode = item.getProperty("meeting_mode", "").Replace("IN", "").ToLower();
                string in_title = item.getProperty("in_title", "");

                string inn_code = item.getProperty("inn_code", "");
                string inn_contents = item.getProperty("inn_contents", "");
                string inn_ctrl_value = item.getProperty("inn_ctrl_value", "");
                string inn_time_value = GetDateFormat(item, "inn_time_value", "yyyy/MM/dd");
                string item_number = item.getProperty("item_number", "");

                item.setType("inn_meeting");
                item.setProperty("meeting_id", meeting_id);
                item.setProperty("meeting_mode", meeting_mode);
                item.setProperty("show_category", i == 0 ? "item_show_1" : "item_show_0");
                item.setProperty("default_category", i == 0 ? "item_show_0" : "item_show_1");

                string inn_desc = "";
                string inn_func = "詳細資訊";
                string inn_apply_btn = "item_show_0";

                switch (inn_code)
                {
                    case "creator": //協助報名
                        inn_desc = "您協助報名【" + in_title + "】";
                        inn_ctrl_value = inn_ctrl_value + "人";
                        break;

                    case "registered": //被報名
                        inn_desc = "您已報名【" + in_title + "】" + inn_contents + "等項目";
                        break;

                    case "staff": //工作人員
                        inn_desc = "您已被設為【" + in_title + "】" + inn_contents;
                        break;

                    case "verify": //等待審核
                        inn_desc = in_title + "有 <span class='meeting-verify' onclick='go_verify(this)'>" + inn_ctrl_value + "</span> 筆資料等待審核";
                        break;

                    case "paid": //年費已繳納
                        inn_desc = "您已繳納【" + in_title + "】" + inn_contents;
                        inn_func = "繳費結果";
                        inn_apply_btn = "";
                        break;

                    default:
                        break;
                }

                item.setProperty("inn_code", inn_code);
                item.setProperty("inn_desc", inn_desc);
                item.setProperty("inn_func", inn_func);
                item.setProperty("inn_ctrl_value", inn_ctrl_value);
                item.setProperty("inn_time_value", inn_time_value);
                item.setProperty("inn_apply_btn", inn_apply_btn);
                item.setProperty("item_number", item_number);
                itmReturn.addRelationship(item);

                i++;
            }
        }

        /// <summary>
        /// 合併活動
        /// </summary>
        private void MergeMeetins(List<TMeeting> list, Item items, bool no_time = false)
        {
            if (items.isError() || items.getItemCount() <= 0)
            {
                return;
            }

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                if (no_time)
                {
                    list.Add(new TMeeting
                    {
                        Value = item
                    });
                }
                else
                {
                    string in_time_value = item.getProperty("inn_time_value", "");
                    if (in_time_value == "")
                    {
                        //時間異常
                        continue;
                    }

                    list.Add(new TMeeting
                    {
                        SortTime = Convert.ToDateTime(in_time_value).AddHours(-8),
                        Value = item
                    });
                }
            }
        }

        //年費繳納
        private Item GetCreatorPayment(TConfig cfg)
        {
            string sql = @"
                SELECT
                    'paid'               AS 'inn_code'
                    , t2.in_title
                    , t2.in_url
                    , t2.in_date_s
                    , t2.in_date_e
                    , t2.in_meeting_type
                    , t2.in_meeting_photo
                    , t2.id                 AS 'meeting_id'
                    , '會員年費'             AS 'inn_contents'
                    , '會員人數'             AS 'inn_ctrl_label'
                    , t1.registry_count     AS 'inn_ctrl_value'
                    , '建檔時間'            AS 'inn_time_label'
                    , t1.last_registry_time AS 'inn_time_value'
                    , IN_PAYNUMBER as item_number
                FROM
                    (
                        SELECT
                            P1.source_id
                            , COUNT(P1.in_sno)   AS 'registry_count'
                            , MAX(P1.created_on) AS 'last_registry_time'
                            , MAX(P1.IN_PAYNUMBER) AS 'IN_PAYNUMBER'
                        FROM
                            IN_MEETING_USER AS P1  WITH(NOLOCK) 
                        JOIN 
							innovator. IN_MEETING_PAY AS P2  WITH(NOLOCK) 
						ON P1.IN_PAYNUMBER = P2.ITEM_NUMBER
                        WHERE
                            P1.in_creator_sno = N'{#in_sno}'
                            AND (P1.in_paytime IS NOT NULL OR P2.PAY_BOOL='已繳費')
                        GROUP BY
                            P1.source_id
                    )
                    t1
                INNER JOIN
                    IN_MEETING t2 WITH(NOLOCK)
                    ON t2.id = t1.source_id
                WHERE
                    t2.in_meeting_type in ('payment')
                    AND t2.in_date_e > GetDate()
                ";

            sql = sql.Replace("{#in_sno}", cfg.login_sno);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        //協助報名
        private Item GetCreatorMeetings(TConfig cfg, string mtType, string mode)
        {
            string sql = @"
                SELECT
                    'creator'               AS 'inn_code'
                    , t2.in_title
                    , t2.in_url
                    , t2.in_date_s
                    , t2.in_date_e
                    , t2.in_meeting_type
                    , t2.in_meeting_photo
                    , t2.id                   AS 'meeting_id'
                    , '{#mode}'               AS 'meeting_mode'
                    , '協助報名'              AS 'inn_contents'
                    , '報名人數'              AS 'inn_ctrl_label'
                    , t1.registry_count       AS 'inn_ctrl_value'
                    , '最後報名時間'          AS 'inn_time_label'
                    , t1.last_registry_time   AS 'inn_time_value'
                FROM
                    (
                        SELECT
                            source_id
                            , COUNT(in_sno)   AS 'registry_count'
                            , MAX(created_on) AS 'last_registry_time'
                        FROM
                            {#mtType}_USER WITH(NOLOCK)
                        WHERE
                            in_creator_sno = N'{#in_sno}'
                            AND in_sno <> in_creator_sno
                        GROUP BY
                            source_id
                    )
                    t1
                INNER JOIN
                    {#mtType} t2 WITH(NOLOCK)
                    ON t2.id = t1.source_id
                WHERE
                    t2.in_meeting_type in ('game', 'seminar', 'degree')
                    AND t2.in_date_e > GetDate()
                ";

            sql = sql.Replace("{#mtType}", mtType)
                .Replace("{#in_sno}", cfg.login_sno)
                .Replace("{#mode}", mode);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        //被報名
        private Item GetRegistryMeetings(TConfig cfg, string mtType, string mode)
        {
            //, t2.in_url
            string url = "";
            if (mtType == "IN_MEETING")
            {
                url = "MeetingRegistryContinous_Base_SingleUser.html";
            }
            else
            {
                url = "Cla_MeetingRegistryContinous_Base_SingleUser.html";
            }

            string sql = @"
                SELECT
                    'registered'           AS 'inn_code'
                    , t2.in_title
                    , '{#url}' AS in_url
                    , t2.in_date_s
                    , t2.in_date_e
                    , t2.in_meeting_type
                    , t2.in_meeting_photo
                    , t2.id                AS 'meeting_id'
                    , '{#mode}'            AS 'meeting_mode'
                    , t1.in_section_name   AS 'inn_contents'
                    , '報名人員'            AS 'inn_ctrl_label'
                    , t1.in_creator        AS 'inn_ctrl_value'
                    , '報名時間'            AS 'inn_time_label'
                    , t1.created_on        AS 'inn_time_value'
                FROM
                    {#mtType}_USER t1 WITH(NOLOCK)
                INNER JOIN
                    {#mtType} t2 WITH(NOLOCK)
                    ON t2.id = t1.source_id
                WHERE
                    t2.in_meeting_type in ('game', 'seminar', 'degree')
                    AND t2.in_date_e > GetDate()
                    AND t1.in_sno = N'{#in_sno}'
                ";

            sql = sql.Replace("{#mtType}", mtType)
                .Replace("{#in_sno}", cfg.login_sno)
                .Replace("{#mode}", mode)
                .Replace("{#url}", url);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        //被列為工作人員
        private Item GetWorkMeetings(TConfig cfg, string mtType, string mode)
        {
            string sql = @"
                SELECT
                    'staff'                AS 'inn_code'
                    , t2.in_title
                    , t2.in_url
                    , t2.in_date_s
                    , t2.in_date_e
                    , t2.in_meeting_type
                    , t2.in_meeting_photo
                    , t2.id                AS 'meeting_id'
                    , '{#mode}'            AS 'meeting_mode'
                    , t1.in_job            AS 'inn_contents'
                    , '設定人員'            AS 'inn_ctrl_label'
                    , '管理者'             AS 'inn_ctrl_value'
                    , '設定時間'            AS 'inn_time_label'
                    , t1.created_on       AS 'inn_time_value'
                FROM
                    {#mtType}_STAFF t1 WITH(NOLOCK)
                INNER JOIN
                    {#mtType} t2 WITH(NOLOCK)
                    ON t2.id = t1.source_id
                WHERE
                    t2.in_date_e > GetDate()
                    AND t1.in_sno = N'{#in_sno}'
                ";

            sql = sql.Replace("{#mtType}", mtType)
                .Replace("{#in_sno}", cfg.login_sno)
                .Replace("{#mode}", mode);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        //待審核(協會審核)
        private Item GetVerifyMeetings2(TConfig cfg, string mtType, string mode)
        {
            string sql = @"
                SELECT
                    'verify'               AS 'inn_code'
                    , t2.in_title
                    , t2.in_url
                    , t2.in_date_s
                    , t2.in_date_e
                    , t2.in_meeting_type
                    , t2.in_meeting_photo
                    , t2.id                 AS 'meeting_id'
                    , '{#mode}'             AS 'meeting_mode'
                    , '報名講習'             AS 'inn_contents'
                    , '待審人數'             AS 'inn_ctrl_label'
                    , t1.registry_count     AS 'inn_ctrl_value'
                    , '最後申請時間'          AS 'inn_time_label'
                    , t1.last_registry_time AS 'inn_time_value'
                FROM
                    (
                        SELECT
                            source_id
                            , COUNT(in_sno)   AS 'registry_count'
                            , MAX(created_on) AS 'last_registry_time'
                        FROM
                            {#mtType}_USER WITH(NOLOCK)
                        WHERE
                            ISNULL(in_ass_ver_result, '') = ''
                        GROUP BY
                            source_id
                    )
                    t1
                INNER JOIN
                    {#mtType} t2 WITH(NOLOCK)
                    ON t2.id = t1.source_id
                WHERE
                    t2.in_verify_mode = '2'
                    AND t2.in_date_e > GetDate()
                ";

            sql = sql.Replace("{#mtType}", mtType)
                .Replace("{#in_sno}", cfg.login_sno)
                .Replace("{#mode}", mode);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        //待審核(委員會需先審核)
        private Item GetVerifyMeetings3(TConfig cfg, string mtType, string mode, bool isMeetingAdmin)
        {
            string condition = isMeetingAdmin
                ? "ISNULL(in_verify_result, '') = '1' AND ISNULL(in_ass_ver_result, '') = ''"
                : "in_committee = N'" + cfg.login_name + "' AND ISNULL(in_verify_result, '') = ''";

            string sql = @"
                SELECT
                    'verify'               AS 'inn_code'
                    , t2.in_title
                    , t2.in_url
                    , t2.in_date_s
                    , t2.in_date_e
                    , t2.in_meeting_type
                    , t2.in_meeting_photo
                    , t2.id                 AS 'meeting_id'
                    , '{#mode}'             AS 'meeting_mode'
                    , '申請晉段'             AS 'inn_contents'
                    , '申請人數'             AS 'inn_ctrl_label'
                    , t1.registry_count     AS 'inn_ctrl_value'
                    , '最後申請時間'          AS 'inn_time_label'
                    , t1.last_registry_time AS 'inn_time_value'
                FROM
                    (
                        SELECT
                            source_id
                            , COUNT(in_sno)   AS 'registry_count'
                            , MAX(created_on) AS 'last_registry_time'
                        FROM
                            {#mtType}_USER WITH(NOLOCK)
                        WHERE
                            {#condition}
                        GROUP BY
                            source_id
                    )
                    t1
                INNER JOIN
                    {#mtType} t2 WITH(NOLOCK)
                    ON t2.id = t1.source_id
                WHERE
                    t2.in_verify_mode = '3'
                    AND t2.in_date_e > GetDate()
                ";

            sql = sql.Replace("{#mtType}", mtType)
                .Replace("{#mode}", mode)
                .Replace("{#condition}", condition);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        //共同講師審核(協會審核)
        private Item GetVerifyMeetingsCO(TConfig cfg, string mtType, string mode, string type)
        {
            string sql = @"
                SELECT
                    'verify'               AS 'inn_code'
                    , t2.in_title
                    , t2.in_url
                    , t2.in_date_s
                    , t2.in_date_e
                    , t2.in_meeting_type
                    , t2.in_meeting_photo
                    , t2.id                 AS 'meeting_id'
                    , '{#mode}'             AS 'meeting_mode'
                    , '報名{#type}'         AS 'inn_contents'
                    , '待審人數'            AS 'inn_ctrl_label'
                    , t1.registry_count     AS 'inn_ctrl_value'
                    , '最後申請時間'        AS 'inn_time_label'
                    , t1.last_registry_time AS 'inn_time_value'
                FROM
                    (
                        SELECT
                            t11.source_id
                            , COUNT(t11.in_sno)   AS 'registry_count'
                            , MAX(t11.created_on) AS 'last_registry_time'
                        FROM
                            {#mtType}_USER t11 WITH(NOLOCK)
						INNER JOIN
							{#mtType} t12 WITH(NOLOCK)
							ON t12.id = t11.source_id
						WHERE
							(
								(t12.in_verify_mode = '2' AND ISNULL(t11.in_ass_ver_result, '') = '')
								OR
								(t12.in_verify_mode = '3' AND ISNULL(t11.in_verify_result, '') = '1' AND ISNULL(t11.in_ass_ver_result, '') = '')
							)
							AND t12.id IN 
							(
								SELECT source_id FROM {#mtType}_RESUMELIST WHERE related_id = '{#resume_id}'
							)
                        GROUP BY
                            source_id
                    ) t1
                INNER JOIN
                    {#mtType} t2 WITH(NOLOCK)
                    ON t2.id = t1.source_id
                WHERE
                    t2.in_date_e > GetDate()
                ";

            sql = sql.Replace("{#mtType}", mtType)
                .Replace("{#resume_id}", cfg.login_resume_id)
                .Replace("{#mode}", mode)
                .Replace("{#type}", type);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        //取得活動狀態
        private Item GetMeetingStatus(Innovator inn, Item item)
        {
            //活動類型 (1: 賽事, 2: 賽事工作人員, 3: 講習)
            string inn_code = item.getProperty("inn_code", "");
            string inn_type = item.getProperty("inn_type", "");
            //活動開始
            string in_date_s = item.getProperty("in_date_s", "");
            //活動結束
            string in_date_e = item.getProperty("in_date_e", "");
            //可報名狀態
            string in_signup_state = item.getProperty("in_signup_state", "");
            //報名開始日
            string in_state_time_start = item.getProperty("in_state_time_start", "");
            //報名截止日
            string in_state_time_end = item.getProperty("in_state_time_end", "");

            string in_value = "";
            string in_class = "label-default";

            try
            {
                DateTime dtNow = DateTime.Now;

                DateTime date_s = Convert.ToDateTime(in_date_s);
                DateTime date_e = Convert.ToDateTime(in_date_e);
                DateTime state_time_start = Convert.ToDateTime(in_state_time_start);
                DateTime state_time_end = Convert.ToDateTime(in_state_time_end);

                if (date_e < dtNow)
                {
                    in_value = "活動結束";
                    in_class = "label-default";
                }
                else if (date_s < dtNow)
                {
                    if (inn_code == "3")
                    {
                        in_value = "開課中";
                        in_class = "label-success";
                    }
                    else
                    {
                        in_value = "賽事進行中";
                        in_class = "label-success";
                    }
                }
                else if (state_time_start < dtNow)
                {
                    in_value = "報名中";
                    in_class = "label-warning";
                }
                else
                {
                    in_value = "籌備中";
                    in_class = "label-danger";
                }
            }
            catch
            {
                in_value = "已結束";
                in_class = "label-default";
            }

            Item itmResult = inn.newItem();
            itmResult.setProperty("in_value", in_value);
            itmResult.setProperty("in_class", in_class);
            return itmResult;

        }

        //待繳費記錄
        private Item GetPayments(TConfig cfg)
        {
            string sql0 = @"
                SELECT
                    t1.item_number
	                , t2.in_meeting_photo
                    , t1.in_current_org
                    , t1.in_pay_date_exp
                    , t1.in_pay_date_exp1
                    , t1.in_pay_amount_exp
                    , t1.pay_bool
                    , t2.id AS 'meeting_id'
                    , t2.in_title
                    , t2.in_url AS 'meeting_url'
                    , t2.in_member_type
                    , t3.item_name
                    , t3.item_amount
                    , t3.item_count
                    , {#sort} as sort
                FROM
                    IN_MEETING_PAY t1 WITH(NOLOCK)
                INNER JOIN
                    {#meeting_type} t2 WITH(NOLOCK)  ON t2.id = t1.{#pay_property}
                INNER JOIN
                    VU_MEETING_PAY_DETAIL t3 WITH(NOLOCK)
                    ON t3.source_id = t1.id
                WHERE
                    t1.pay_bool = N'未繳費'
                    AND t1.in_creator_sno = '{#in_creator_sno}'
            ";

            string sql1 = sql0.Replace("{#meeting_type}", "IN_MEETING")
                .Replace("{#pay_property}", "in_meeting")
                .Replace("{#in_creator_sno}", cfg.login_sno)
                .Replace("{#sort}", "1");

            string sql2 = sql0.Replace("{#meeting_type}", "IN_CLA_MEETING")
                .Replace("{#pay_property}", "in_cla_meeting")
                .Replace("{#in_creator_sno}", cfg.login_sno)
                .Replace("{#sort}", "2");

            StringBuilder builder = new StringBuilder();
            builder.AppendLine(sql1);
            builder.AppendLine("UNION ALL");
            builder.AppendLine(sql2);
            builder.AppendLine("ORDER BY sort, meeting_id, item_number, item_name");

            string sql = builder.ToString();

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        //審核未通過
        private Item GetRejects(TConfig cfg, string mode, string login_sno)
        {
            string MTName = "IN_MEETING";
            string MUserName = "IN_MEETING_USER";
            string seminar_type = "'' AS 'in_seminar_type'";

            if (mode == "cla")
            {
                MTName = "IN_CLA_MEETING";
                MUserName = "IN_CLA_MEETING_USER";
                seminar_type = "t1.in_seminar_type";
            }

            string sql = @"
                SELECT 
	                t1.id AS 'meeting_id'
	                , t1.in_title
	                , t1.in_meeting_type
	                , {#seminar_type}
	                , DATEADD(HOUR, 8, t1.in_date_s) AS 'in_date_s'
	                , DATEADD(HOUR, 8, t1.in_date_e) AS 'in_date_e'
					, t1.in_register_url
	                , t2.in_current_org
	                , t2.in_name
	                , t2.in_sno_display
	                , t2.in_regdate
	                , t2.in_section_name
	                , t2.in_l1
	                , t2.in_l2
	                , DATEADD(HOUR, 8, t2.in_ass_ver_time) AS 'in_ass_ver_time'
	                , t2.in_ass_ver_memo
	                , '{#mode}' AS 'meeting_mode'
                FROM 
	                {#MTName} t1 WITH(NOLOCK)
                INNER JOIN
	                {#MUserName} t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                WHERE 
	                t1.in_meeting_type NOT IN ('registry', 'verify')
	                AND t2.in_creator_sno = '{#login_sno}'
	                AND ISNULL(in_ass_ver_result, '') = '0'
            ";

            sql = sql.Replace("{#MTName}", MTName)
                .Replace("{#MUserName}", MUserName)
                .Replace("{#seminar_type}", seminar_type)
                .Replace("{#mode}", mode)
                .Replace("{#login_sno}", login_sno);


            return cfg.inn.applySQL(sql);
        }

        //最新公告
        private void AppendAnnouncements(TConfig cfg, Item itmReturn)
        {
            string sql = @"
                SELECT
                    TOP 5
                    DATEADD(hour, 8, t1.in_released_day) AS 'in_released_day'
                    , t1.in_type
                    , t1.in_title
                    , t1.in_contents
                    , t1.in_file1
                    , t1.in_meeting
                    , t2.in_title
                    , t2.in_meeting_type
                    , t2.in_url
                FROM 
                    In_Announcement t1 WITH(NOLOCK)
                LEFT OUTER JOIN
                    IN_Meeting t2 WITH(NOLOCK) ON t2.id = t1.in_meeting
                WHERE 
                    t1.in_is_all = N'1' 
                    AND ISNULL(t1.in_closed, 0) <> 1
                ORDER BY 
                    t1.in_released_day DESC
            ";

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item items = cfg.inn.applySQL(sql);

            if (items.isError() || items.getItemCount() == 0)
            {
                return;
            }

            DateTime dtNow = DateTime.Now;

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_meeting = item.getProperty("in_meeting", "");
                string in_title = item.getProperty("in_title", "");
                string in_file1 = item.getProperty("in_file1", "");
                string in_released_day = GetDateFormat(item, "in_released_day", "yyyy/MM/dd");

                string in_meeting_type = item.getProperty("in_meeting_type", "");
                string in_url = item.getProperty("in_url", "");

                //string inn_time = GetTimeScope(dtNow, in_released_day);

                item.setType("inn_announcement");
                item.setProperty("show_category", i == 0 ? "item_show_1" : "item_show_0");
                item.setProperty("default_category", i == 0 ? "item_show_0" : "item_show_1");

                item.setProperty("inn_no", (i + 1).ToString());
                //item.setProperty("in_released_day", GetDateFormat(item, "in_released_day", "yyyy/MM/dd"));
                //item.setProperty("inn_time", inn_time);

                string inn_title_link = "";
                if (in_meeting != "")
                {
                    string[] links = GetMeetingInfoClick(in_meeting, in_url);
                    inn_title_link = links[0] + "<i class=\"fa fa-clock-o\"></i> " + in_released_day + " " + in_title + links[1];
                }
                else
                {
                    inn_title_link = "<i class=\"fa fa-clock-o\"></i> " + in_released_day + " " + in_title;
                }
                item.setProperty("inn_title_link", inn_title_link);

                if (in_file1 != "")
                {
                    item.setProperty("inn_file1_class", "item_show_1");
                }
                else
                {
                    item.setProperty("inn_file1_class", "item_show_0");
                }

                itmReturn.addRelationship(item);
            }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }

            public bool isSpecialUser { get; set; }

            /// <summary>
            /// 登入者 Resume 資訊
            /// </summary>
            public Item LoginResume { get; set; }

            /// <summary>
            /// 登入者 Resume id
            /// </summary>
            public string login_resume_id { get; set; }

            /// <summary>
            /// 登入者 姓名
            /// </summary>
            public string login_name { get; set; }

            /// <summary>
            /// 登入者 身分證號
            /// </summary>
            public string login_sno { get; set; }

            /// <summary>
            /// 登入者 所屬群組
            /// </summary>
            public string login_group { get; set; }

            /// <summary>
            /// 登入者 是否為單位
            /// </summary>
            public string login_in_org { get; set; }

            /// <summary>
            /// 登入者 會員類型
            /// </summary>
            public string login_member_type { get; set; }

            /// <summary>
            /// 登入者 會員角色
            /// </summary>
            public string login_member_role { get; set; }

            /// <summary>
            /// 登入者 社館
            /// </summary>
            public string login_member_unit { get; set; }

            public string login_is_admin { get; set; }

            public bool isMeetingAdmin { get; set; }
            public bool isCommittee { get; set; }
            public bool isGymOwner { get; set; }
            public bool isGymAssistant { get; set; }

            public DateTime Now { get; set; }

            public string NowStr { get; set; }


            public string apply_meeting_id { get; set; }

            public bool is_doc_receiver { get; set; }
            public bool is_doc_launcher { get; set; }
            public bool is_doc_dispatcher { get; set; }

        }

        /// <summary>
        /// 活動資料模型
        /// </summary>
        private class TMeeting
        {
            /// <summary>
            /// 排序用欄位
            /// </summary>
            public DateTime SortTime { get; set; }

            /// <summary>
            /// 物件
            /// </summary>
            public Item Value { get; set; }
        }

        private class TDocItem
        {
            public string item_number { get; set; }
            public Item value { get; set; }
        }

        private class TDoc
        {
            public string url { get; set; }
            public bool not_start { get; set; }
            public bool is_rejected { get; set; }
            public bool need_assign_undertaker { get; set; }
            public bool need_reset_undertaker { get; set; }
            public bool can_vote { get; set; }
            public string vote_state { get; set; }
            public Item value { get; set; }
        }

        private TDoc MapDoc(TConfig cfg, Item item)
        {
            var result = new TDoc
            {
                value = item,
            };

            return result;
        }

        #region 通用函式

        private string GetDateFormat(Item item, string property_name, string format, int hours = 0)
        {
            string value = item.getProperty(property_name, "");
            if (value == "")
            {
                return "";
            }

            try
            {
                return GetDateTime(value, hours).ToString(format);
            }
            catch
            {
                return "";
            }
        }

        private DateTime GetDateTime(string value, int hours)
        {
            DateTime dt = DateTime.MinValue;
            DateTime.TryParse(value, out dt);
            return dt.AddHours(hours);
        }

        private string GetTimeScope(DateTime now, string in_released_day)
        {
            if (in_released_day == "")
            {
                return "";
            }

            DateTime released_day = Convert.ToDateTime(in_released_day);
            TimeSpan ts = now - released_day;
            int seconds = (int)ts.TotalSeconds;
            int minutes = (int)ts.TotalMinutes;
            int hours = (int)ts.TotalHours;
            int days = (int)ts.TotalDays;
            int weeks = (int)(days / 7);
            int months = (int)(days / 30);

            if (seconds < 60)
            {
                return seconds + " sec";
            }
            else if (minutes < 60)
            {
                return minutes + " min" + (minutes == 1 ? "" : "s");
            }
            else if (hours < 24)
            {
                return hours + " hour" + (hours == 1 ? "" : "s");
            }
            else if (days < 7)
            {
                return days + " day" + (days == 1 ? "" : "s");
            }
            else if (weeks < 4)
            {
                return weeks + " week" + (weeks == 1 ? "" : "s");
            }
            else
            {
                return months + " month" + (months == 1 ? "" : "s");
            }
        }

        private string GetSubtotal(string count, string amount)
        {
            int quantity = 0;
            int price = 0;
            if (!int.TryParse(count, out quantity) || !int.TryParse(amount, out price))
            {
                return "0";
            }
            return (quantity * price).ToString("###,###");
        }

        #endregion 通用函式

        private string[] GetMeetingInfoClick(string meeting_id, string in_url)
        {
            return new string[]
            {
                "<a href=\"javascript:void()\" onclick=\"MeetingInfo_Click('" + meeting_id + "', '" + in_url + "')\">",
                "</a>"
            };
        }
    }
}