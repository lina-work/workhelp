﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.CTPC.Doc
{
    public class In_Document_Email : Item
    {
        public In_Document_Email(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 文管-特殊流程的 Email 發送
                日誌: 
                    - 2022-10-21: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]" + "In_Document_Email";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,

                strUserId = itmR.getProperty("strUserId", ""),
                strIdentityId = itmR.getProperty("strIdentityId", ""),

                doc_id = itmR.getProperty("doc_id", ""),
                scene = itmR.getProperty("scene", ""),
            };

            //iplm 站台網址
            cfg.iplm_url = GetVariable(cfg, "app_url");

            //管理者資訊
            cfg.itmUserAdmin = GetUserAdminItem(cfg);

            //文件資訊
            cfg.itmDoc = GetDocItem(cfg);
            //文管 收發文類型
            cfg.in_issue_type = cfg.itmDoc.getProperty("in_issue_type", "");
            //文管 收文 發起
            cfg.in_wf_act0_identity = cfg.itmDoc.getProperty("in_wf_act0_identity", "");
            //文管 收文 確認
            cfg.in_wf_act1_identity = cfg.itmDoc.getProperty("in_wf_act1_identity", "");
            //文管 收文 承辦單位主管
            cfg.in_wf_act3_identity = cfg.itmDoc.getProperty("in_wf_act3_identity", "");

            if (cfg.in_issue_type == "receive")
            {
                var row = default(TRow);

                switch (cfg.scene)
                {
                    case "receive_reject":
                        row = GetReceiverRow(cfg, cfg.in_wf_act0_identity);
                        break;

                    case "receive_dispatch":
                        row = GetDispatcherRow(cfg, cfg.in_wf_act1_identity);
                        break;

                    case "receive_assign":
                        row = GetManagerRow(cfg, cfg.in_wf_act3_identity);
                        break;
                }

                if (row != null)
                {
                    SendEmail(cfg, row);
                }
            }

            return itmR;
        }

        //發給[文管 收文 發起者]的 Email
        private TRow GetReceiverRow(TConfig cfg, string identity_id)
        {
            if (identity_id == "") return null;

            cfg.itmUserReceiver = GetUserReceiverItem(cfg, identity_id);

            var row = new TRow
            {
                action = "修正",
                activity = "文件修正",
                in_issue_type_label = cfg.itmDoc.getProperty("in_issue_type_label", ""),
                tw_date = GetTWDate(cfg.itmDoc.getProperty("in_issue_date", ""), 8),
                in_issue_no = cfg.itmDoc.getProperty("in_issue_no", ""),
                name = cfg.itmDoc.getProperty("name", ""),
            };

            row.url = cfg.iplm_url
                + "/pages/c.aspx"
                + "?page=DocumentEdit.html"
                + "&method=In_Get_SingleItemInfoGeneral"
                + "&itemtype=Document"
                + "&itemid=" + cfg.doc_id
                + "&type=direct"
                + "&fetchproperty=4"
                + "&inn_editor_type=edit";
                ;

            return row;
        }

        //發給[文管 收文 確認者]的 Email
        private TRow GetDispatcherRow(TConfig cfg, string identity_id)
        {
            if (identity_id == "") return null;

            cfg.itmUserReceiver = GetUserReceiverItem(cfg, identity_id);

            var row = new TRow
            {
                action = "確認",
                activity = "文管確認",
                in_issue_type_label = cfg.itmDoc.getProperty("in_issue_type_label", ""),
                tw_date = GetTWDate(cfg.itmDoc.getProperty("in_issue_date", ""), 8),
                in_issue_no = cfg.itmDoc.getProperty("in_issue_no", ""),
                name = cfg.itmDoc.getProperty("name", ""),
            };

            row.url = cfg.iplm_url
                + "/pages/c.aspx"
                + "?page=DocumentDispatch.html"
                + "&method=In_Document_Edit"
                + "&doc_id=" + cfg.doc_id
                + "&scene=dispatch_page"
                ;

            return row;
        }

        //發給[文管 收文 承辦單位主管]的 Email
        private TRow GetManagerRow(TConfig cfg, string identity_id)
        {
            if (identity_id == "") return null;

            cfg.itmUserReceiver = GetUserReceiverItem(cfg, identity_id);

            var row = new TRow
            {
                action = "指派承辦組員",
                activity = "指派承辦組員",
                in_issue_type_label = cfg.itmDoc.getProperty("in_issue_type_label", ""),
                tw_date = GetTWDate(cfg.itmDoc.getProperty("in_issue_date", ""), 8),
                in_issue_no = cfg.itmDoc.getProperty("in_issue_no", ""),
                name = cfg.itmDoc.getProperty("name", ""),
            };

            row.url = cfg.iplm_url
                + "/pages/c.aspx"
                + "?page=DocumentDispatch.html"
                + "&method=In_Document_Edit"
                + "&doc_id=" + cfg.doc_id
                + "&scene=dispatch_page"
                ;

            return row;
        }

        private void SendEmail(TConfig cfg, TRow row)
        {
            row.email_subject = EmailSubject(cfg, row);
            row.email_body = EmailBody(cfg, row);

            var fm_addr = cfg.itmUserAdmin.getProperty("email", "");
            var fm_name = cfg.itmUserAdmin.getProperty("keyed_name", "");

            var to_addr = cfg.itmUserReceiver.getProperty("email", "");
            var to_name = cfg.itmUserReceiver.getProperty("last_name", ""); //不能包含身分證號

            var MyFrom = new System.Net.Mail.MailAddress(fm_addr, fm_name);
            var MyTo = new System.Net.Mail.MailAddress(to_addr, to_name);
            var myMail = new System.Net.Mail.MailMessage(MyFrom, MyTo);

            myMail.Subject = row.email_subject;
            myMail.SubjectEncoding = System.Text.Encoding.UTF8;

            myMail.Body = row.email_body;
            myMail.BodyEncoding = System.Text.Encoding.UTF8;

            myMail.IsBodyHtml = true;

            try
            {
                cfg.CCO.Email.SetupSmtpMailServerAndSend(myMail);
            }
            catch (Exception ex)
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, ex.Message);
            }
        }

        private string EmailSubject(TConfig cfg, TRow row)
        {
            return "[PLM簽審]:" + row.activity
                + " - 文件"
                + " - " + row.item_number
                + " " + row.name
                ;
        }

        private string EmailBody(TConfig cfg, TRow row)
        {
            var builder = new StringBuilder();
            builder.Append("<BODY>");

            builder.Append("<font size='+1'>");
            builder.Append("<br/>");

            builder.Append("您好,目前在PLM系統上有一份文件需要您" + row.action + ":<br/>");
            builder.Append("關卡名稱:" + row.activity + " <br/>");
            builder.Append("文件類型:" + row.in_issue_type_label + " <br/>");
            builder.Append("發文日期:" + row.tw_date + " <br/>");
            builder.Append("文件字號:" + row.in_issue_no + " <br/>");
            builder.Append("文件主旨:" + row.name + "<br/>");

            builder.Append("<p>請至<a href='" + row.url + "'>APP審查</a></p>");

            builder.Append("</font>");

            builder.Append("</BODY>");

            return builder.ToString();
        }

        private string GetVariable(TConfig cfg, string in_name)
        {
            Item item = cfg.inn.applySQL("SELECT TOP 1 in_value FROM IN_VARIABLE WITH(NOLOCK) WHERE in_name = '" + in_name + "'");
            if (item.isError() || item.getResult() == "")
            {
                throw new Exception("查無 " + in_name + " 站台網址");
            }
            return item.getProperty("in_value", "").TrimEnd('/');
        }

        private Item GetUserReceiverItem(TConfig cfg, string identity_id)
        {
            string sql = @"
                SELECT TOP 1
	                t2.keyed_name
	                , t2.last_name
	                , t2.email
                FROM
	                [IDENTITY] t1 WITH(NOLOCK)
                LEFT OUTER JOIN
	                [USER] t2 WITH(NOLOCK)
	                ON t2.login_name = t1.in_number
                WHERE
	                t1.id = '{#id}'
            ";

            sql = sql.Replace("{#id}", identity_id);

            Item item = cfg.inn.applySQL(sql);

            if (item.isError() || item.getResult() == "")
            {
                throw new Exception("查無收件者者資料");
            }

            return item;
        }

        private Item GetUserAdminItem(TConfig cfg)
        {
            string sql = @"
                SELECT 
					keyed_name
					, email
				FROM
					[USER] WITH(NOLOCK) 
				WHERE
					login_name = 'admin'
            ";

            Item item = cfg.inn.applySQL(sql);

            if (item.isError() || item.getResult() == "")
            {
                throw new Exception("查無管理者資料");
            }

            return item;
        }

        private Item GetDocItem(TConfig cfg)
        {
            string sql = @"
                SELECT TOP 1
	                t1.id
	                , t1.item_number
	                , t1.in_issue_no
	                , t1.in_issue_type
	                , t1.name
	                , t1.in_issue_date
	                , t1.in_wf_act0_identity -- 文管 收文 發起
	                , t1.in_wf_act1_identity -- 文管 收文 確認
	                , t1.in_wf_act3_identity -- 文管 承辦 主管
	                , t2.label AS 'in_issue_type_label'
                FROM 
	                [DOCUMENT] t1 WITH(NOLOCK) 
                LEFT OUTER JOIN 
	                VU_Doc_IssueType t2
	                ON t2.value = t1.in_issue_type 
                WHERE
	                t1.is_current = 1
	                AND t1.id = '{#doc_id}'
                ORDER BY 
	                item_number DESC
            ";

            sql = sql.Replace("{#doc_id}", cfg.doc_id);

            Item item = cfg.inn.applySQL(sql);

            if (item.isError() || item.getResult() == "")
            {
                throw new Exception("查無文件資料");
            }

            return item;
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }

            public string strUserId { get; set; }
            public string strIdentityId { get; set; }

            public string doc_id { get; set; }
            public string scene { get; set; }

            public string iplm_url { get; set; }

            public Item itmDoc { get; set; }
            public Item itmUserAdmin { get; set; }
            public Item itmUserReceiver { get; set; }
            
            public string in_issue_type { get; set; }
            public string in_wf_act0_identity { get; set; }
            public string in_wf_act1_identity { get; set; }
            public string in_wf_act3_identity { get; set; }
        }

        private class TRow
        {
            public string item_number { get; set; }
            public string in_issue_no { get; set; }
            public string in_issue_date { get; set; }
            public string in_issue_type { get; set; }
            public string in_issue_type_label { get; set; }
            public string name { get; set; }

            public string action { get; set; }
            public string tw_date { get; set; }
            public string activity { get; set; }
            public string url { get; set; }

            public string email_subject { get; set; }
            public string email_body { get; set; }
        }

        private string GetTWDate(string value, int hours = 0)
        {
            var dt = DtmVal(value, hours);
            if (dt == DateTime.MinValue) return "";

            var tw_y = (dt.Year - 1911).ToString();
            var tw_m = dt.Month.ToString().PadLeft(2, '0');
            var tw_d = dt.Day.ToString().PadLeft(2, '0');

            return "中華民國" + tw_y + "年" + tw_m + "月" + tw_d + "日";
        }

        private DateTime DtmVal(string value, int hours = 0)
        {
            if (value == "") return DateTime.MinValue;

            DateTime result = DateTime.MinValue;
            if (DateTime.TryParse(value, out result))
            {
                return result.AddHours(hours);
            }
            else
            {
                return DateTime.MinValue;
            }
        }
    }
}
