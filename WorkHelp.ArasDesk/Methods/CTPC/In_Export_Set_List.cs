﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;
using Spire.Xls;
using System.Drawing;

namespace WorkHelp.ArasDesk.Methods.PLMCTA.Common
{
    public class In_Export_Set_List : Item
    {
        public In_Export_Set_List(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 匯出套版報表
                日誌: 
                    - 2022-02-10: 創建 (lina)
            */

            System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]" + "In_Export_Set_List";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                id = itmR.getProperty("id", ""),
                export_type = itmR.getProperty("export_type", ""),
                CharSet = GetCharSet(),
            };

            //活動資訊
            cfg.itmMeeting = MeetingInfo(cfg);
            if (cfg.itmMeeting.isError() || cfg.itmMeeting.getResult() == "")
            {
                throw new Exception("查無活動資訊");
            }

            cfg.mt_title = cfg.itmMeeting.getProperty("in_title", "");
            cfg.mt_date_s = cfg.itmMeeting.getProperty("in_date_s", "");
            cfg.mt_date_e = cfg.itmMeeting.getProperty("in_date_e", "");
            cfg.mt_coursh_hours = cfg.itmMeeting.getProperty("in_coursh_hours", "");
            cfg.mt_decree = cfg.itmMeeting.getProperty("in_decree", "");
            cfg.mt_annual = cfg.itmMeeting.getProperty("in_annual", "");
            cfg.mt_sport_type = cfg.itmMeeting.getProperty("in_sport_type", "");
            cfg.mt_meeting_type = cfg.itmMeeting.getProperty("in_meeting_type", "");

            //匯出檔資訊
            cfg.itmXls = cfg.inn.applyMethod("In_Variable_File", "<in_name>" + cfg.export_type + "</in_name>");
            cfg.export_path = cfg.itmXls.getProperty("export_path", "");
            cfg.template_path = cfg.itmXls.getProperty("template_path", "");
            cfg.exp_name = cfg.itmXls.getProperty("template_name", "");
            cfg.exp_type = cfg.itmXls.getProperty("template_exp_type", "").ToLower();
            if (cfg.exp_name == "") cfg.exp_name = "未設定";
            if (cfg.exp_type == "") cfg.exp_type = "xlsx";

            switch (cfg.exp_type)
            {
                //Word 套版
                case "word_print": cfg.ext_type = "xlsx"; cfg.is_word = true; break;
                case "xlsx": cfg.ext_type = "xlsx"; cfg.is_word = false; break;
                case "pdf": cfg.ext_type = "pdf"; cfg.is_word = false; break;
            }

            cfg.NowInfo = MapDay(DateTime.Now);
            cfg.MtDaySInfo = MapDay(cfg.mt_date_s);

            switch (cfg.export_type)
            {
                case "new_seminar_xlsx"://講習會名冊
                    cfg.exp_name = "講習會名冊";
                    cfg.GetItemTitle = "與會者";
                    cfg.GetItemFunc = MtResumeItems;
                    cfg.SetXlsSheet = MeetingResumeListXls;
                    break;

                case "study_certificate"://研習證明
                    cfg.GetItemTitle = "與會者";
                    cfg.GetItemFunc = StudyCertItems;
                    if (cfg.is_word)
                    {
                        cfg.prefix = "[word]";
                        cfg.SetXlsSheet = StudyCertListXls;
                    }
                    else
                    {
                        cfg.prefix = "";
                        cfg.SetXlsSheet = StudyCertSheetXls;
                    }
                    break;

                case "appointment_letter"://聘書
                    cfg.GetItemTitle = "工作人員";
                    cfg.GetItemFunc = AppointmentItems;
                    if (cfg.is_word)
                    {
                        cfg.prefix = "[word]";
                        cfg.SetXlsSheet = AppointmentListXls;
                    }
                    else
                    {
                        cfg.prefix = "";
                        cfg.SetXlsSheet = AppointmentSheetXls;
                    }
                    break;
            }

            //執行匯出
            Export(cfg, itmR);

            return itmR;
        }

        private void Export(TConfig cfg, Item itmReturn)
        {
            Item items = cfg.GetItemFunc(cfg);

            if (items.isError() || items.getResult() == "")
            {
                throw new Exception("查無" + cfg.GetItemTitle + "資訊");
            }

            //試算表
            var workbook = new Spire.Xls.Workbook();

            //填入資料
            cfg.SetXlsSheet(cfg, items, workbook);

            string guid = System.Guid.NewGuid().ToString().Substring(0, 6).ToUpper()
                + "_" + DateTime.Now.ToString("yyyyMMddHHmmssfff");

            string ext_name = "." + cfg.ext_type;
            string xls_name = cfg.prefix + cfg.mt_title + "_" + cfg.exp_name + "_" + guid;
            string xls_file = cfg.export_path + "\\" + xls_name + ext_name;

            if (cfg.exp_type == "pdf")
            {
                workbook.SaveToFile(xls_file, Spire.Xls.FileFormat.PDF);
            }
            else
            {
                workbook.SaveToFile(xls_file, Spire.Xls.ExcelVersion.Version2013);
            }

            itmReturn.setProperty("xls_name", xls_name + ext_name);
        }
        
        //講習會名冊(清單)
        private void MeetingResumeListXls(TConfig cfg, Item items, Spire.Xls.Workbook workbook)
        {
            //預設值
            int def_fs = 14;
            string ft_num = "Times New Roman";
            string ft_str = "標楷體";

            //列設定
            TRow trow = new TRow { height = 43.8, fn = ft_str, fs = 16, fb = false };
            TRow hrow = new TRow { height = 23, fn = ft_str, fs = 16, fb = false };
            TRow brow = new TRow { height = 21 };

            //欄設定
            List<TField> fields = new List<TField>();
            fields.Add(new TField { title = "編號", property = "in_name_num", css = TCss.Squence, width = 6.44, fs = 12, fn = ft_num });
            fields.Add(new TField { title = "姓名", property = "in_name", css = TCss.Center, width = 11.22, fs = 14, fn = ft_str });
            fields.Add(new TField { title = "性別", property = "in_gender", css = TCss.Center, width = 6.44, fs = def_fs, fn = ft_str });
            fields.Add(new TField { title = "身分證字號", property = "in_sno", css = TCss.Center, width = 16.22, fs = def_fs, fn = ft_num });
            fields.Add(new TField { title = "出生年月日", property = "in_birth", css = TCss.Center, width = 16.22, fs = def_fs, fn = ft_num });
            fields.Add(new TField { title = "學科成績", property = "in_score_o_5", css = TCss.Center, width = 12.33, fs = def_fs, fn = ft_num });
            fields.Add(new TField { title = "術科成績", property = "in_score_o_3", css = TCss.Center, width = 12.33, fs = def_fs, fn = ft_num });
            fields.Add(new TField { title = "是否通過", property = "in_stat_0", css = TCss.Center, width = 12.33, fs = def_fs, fn = ft_str });
            fields.Add(new TField { title = "備註", property = "in_note", css = TCss.Left, width = 10.44, fs = def_fs, fn = ft_str });

            MapCharSetAndIndex(cfg, fields);

            //工作表
            Spire.Xls.Worksheet sheet = workbook.CreateEmptySheet();
            sheet.Name = "工作表1";

            int mnRow = 1;
            int wsRow = 1;

            var fs = fields[0];
            var fe = fields.Last();

            //活動標題
            var meeting_text = "中華帕拉林匹克總會" + Environment.NewLine + cfg.mt_title + "學員名冊";
            var meeting_pos = fs.cs + wsRow + ":" + fe.cs + wsRow;
            var meeting_mr = sheet.Range[meeting_pos];
            meeting_mr.Merge();
            SetMainCell(meeting_mr, trow, meeting_text);
            sheet.SetRowHeight(wsRow, trow.height);
            wsRow++;


            //標題列
            SetHeadRow(sheet, wsRow, hrow, fields);
            sheet.SetRowHeight(wsRow, hrow.height);
            wsRow++;

            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                SetItemCell(cfg, sheet, wsRow, item, fields);
                sheet.SetRowHeight(wsRow, brow.height);
                wsRow++;

            }

            //設定格線
            SetRangeBorder(sheet, fields, mnRow, wsRow);

            //設定欄寬
            SetColumnWidth(sheet, fields);

            //移除空 sheet
            workbook.Worksheets[0].Remove();
            workbook.Worksheets[0].Remove();
            workbook.Worksheets[0].Remove();
        }

        //研習證明(清單)
        private void StudyCertListXls(TConfig cfg, Item items, Spire.Xls.Workbook workbook)
        {
            //承辦人對照表
            var undertaker_map = UnderTakerMap(cfg);
            var undertaker = undertaker_map.ContainsKey(cfg.mt_sport_type)
                ? undertaker_map[cfg.mt_sport_type]
                : "未設定";

            //預設值
            int def_fs = 14;
            double def_w = 11.78;
            string ft_num = "Times New Roman";
            string ft_str = "標楷體";

            //列設定
            TRow hrow = new TRow { height = 19.8, fn = ft_str, fs = 14, fb = false };
            TRow brow = new TRow { height = 19.8 };

            //欄設定
            List<TField> fields = new List<TField>();
            fields.Add(new TField { title = "序號", property = "no", css = TCss.Squence, width = 6.22, fs = 12, fn = ft_num });
            fields.Add(new TField { title = "發證日期", property = "issue_day", css = TCss.Center, width = def_w, fs = def_fs, fn = ft_num });
            fields.Add(new TField { title = "證書字號", property = "cert_word", css = TCss.Center, width = 24.78, fs = def_fs, fn = ft_str });
            fields.Add(new TField { title = "文號", property = "name_num", css = TCss.Center, width = 8.22, fs = def_fs, fn = ft_num });
            fields.Add(new TField { title = "發聘對象", property = "in_name", css = TCss.Center, width = def_w, fs = def_fs, fn = ft_str });
            fields.Add(new TField { title = "課程名稱", property = "in_title", css = TCss.Center, width = 64, fs = def_fs, fn = ft_str });
            fields.Add(new TField { title = "證書日期", property = "cert_day", css = TCss.Center, width = def_w, fs = def_fs, fn = ft_num });
            fields.Add(new TField { title = "時數", property = "cert_hour", css = TCss.Center, width = def_w, fs = def_fs, fn = ft_str });
            fields.Add(new TField { title = "承辦人", property = "undertaker", css = TCss.Center, width = 8.11, fs = def_fs, fn = ft_str });

            MapCharSetAndIndex(cfg, fields);

            //工作表
            Spire.Xls.Worksheet sheet = workbook.CreateEmptySheet();
            sheet.Name = "工作表1";

            int mnRow = 1;
            int wsRow = 1;

            //標題列
            SetHeadRow(sheet, wsRow, hrow, fields);
            sheet.SetRowHeight(wsRow, hrow.height);
            wsRow++;

            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_name_num = item.getProperty("in_name_num", "");
                string cert_word = "CTPC(" + cfg.mt_annual + ")研習字第";
                string name_num = in_name_num.PadLeft(4, '0');

                item.setProperty("no", (i + 1).ToString());
                item.setProperty("issue_day", cfg.NowInfo.en_day2);
                item.setProperty("cert_word", cert_word);
                item.setProperty("name_num", name_num);
                item.setProperty("in_title", cfg.mt_title);
                item.setProperty("cert_day", cfg.MtDaySInfo.en_day2);
                item.setProperty("cert_hour", cfg.mt_coursh_hours + "小時");
                item.setProperty("undertaker", undertaker);

                SetItemCell(cfg, sheet, wsRow, item, fields);
                sheet.SetRowHeight(wsRow, brow.height);
                wsRow++;

            }

            //設定格線
            SetRangeBorder(sheet, fields, mnRow, wsRow);

            //設定欄寬
            SetColumnWidth(sheet, fields);

            //移除空 sheet
            workbook.Worksheets[0].Remove();
            workbook.Worksheets[0].Remove();
            workbook.Worksheets[0].Remove();
        }

        //研習證明(每筆資料一個sheet)
        private void StudyCertSheetXls(TConfig cfg, Item items, Spire.Xls.Workbook workbook)
        {
            //載入 xlsx 範例檔
            workbook.LoadFromFile(cfg.template_path);

            //載入 sheet 樣板
            Spire.Xls.Worksheet sheetTemplate = workbook.Worksheets[0];

            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_sno = item.getProperty("in_sno", "");
                string in_name = item.getProperty("in_name", "");
                string in_name_num = item.getProperty("in_name_num", "").PadLeft(4, '0');

                string decree = "CTPC({#annual})研習字第 {#num} 號";
                decree = decree.Replace("{#annual}", cfg.mt_annual)
                    .Replace("{#num}", in_name_num);

                string contents = "茲證明 {#name} 先生/小姐，於{#tw_day}，參與本總會辦理之「{#mt_title}」，研習時數共計{#hours}小時。";
                contents = contents.Replace("{#name}", in_name)
                    .Replace("{#tw_day}", cfg.MtDaySInfo.tw_long_day2)
                    .Replace("{#mt_title}", cfg.mt_title)
                    .Replace("{#hours}", cfg.mt_coursh_hours);

                //工作表
                Spire.Xls.Worksheet sheet = workbook.CreateEmptySheet();
                sheet.CopyFrom(sheetTemplate);
                sheet.Name = (i + 1).ToString().PadLeft(3, '0');

                sheet.Range["J2"].Text = decree;//文號
                sheet.Range["J4"].Text = cfg.NowInfo.tw_y;
                sheet.Range["J5"].Text = cfg.NowInfo.day.Month.ToString();
                sheet.Range["J6"].Text = cfg.NowInfo.day.Day.ToString();
                sheet.Range["B20"].Text = contents;//內文
            }

            //移除空 sheet
            workbook.Worksheets[0].Remove();
        }

        //聘書(清單)
        private void AppointmentListXls(TConfig cfg, Item items, Spire.Xls.Workbook workbook)
        {
            //承辦人對照表
            var undertaker_map = UnderTakerMap(cfg);
            var undertaker = undertaker_map.ContainsKey(cfg.mt_sport_type)
                ? undertaker_map[cfg.mt_sport_type]
                : "未設定";

            //預設值
            int def_fs = 14;
            double def_w = 11.78;
            string ft_num = "Times New Roman";
            string ft_str = "標楷體";

            //列設定
            TRow hrow = new TRow { height = 39.6, fn = ft_str, fs = 14, fb = false };
            TRow brow = new TRow { height = 19.8 };

            //欄設定
            List<TField> fields = new List<TField>();
            fields.Add(new TField { title = "單" + Environment.NewLine + "位", property = "org", css = TCss.Center, width = 4, fs = 12, fn = ft_str });
            fields.Add(new TField { title = "序" + Environment.NewLine + "號", property = "no", css = TCss.Squence, width = 4, fs = 12, fn = ft_str });
            fields.Add(new TField { title = "發證日期", property = "issue_day", css = TCss.Center, width = 10.89, fs = 12, fn = ft_num });
            fields.Add(new TField { title = "聘書字號", property = "cert_word", css = TCss.Center, width = 21.89, fs = def_fs, fn = ft_str });
            fields.Add(new TField { title = "發證" + Environment.NewLine + "文號", property = "in_decree", css = TCss.Center, width = 10.22, fs = def_fs, fn = ft_num });
            fields.Add(new TField { title = "賽會名稱", property = "in_title", css = TCss.Center, width = 64, fs = 12, fn = ft_str });
            fields.Add(new TField { title = "職稱", property = "in_job_name", css = TCss.Center, width = 21, fs = 12, fn = ft_str });
            fields.Add(new TField { title = "發聘對象", property = "in_name", css = TCss.Center, width = def_w, fs = def_fs, fn = ft_str });
            fields.Add(new TField { title = "稱謂", property = "staff_title", css = TCss.Center, width = 8.11, fs = def_fs, fn = ft_str });
            fields.Add(new TField { title = "聘期起迄", property = "work_term", css = TCss.Center, width = 24, fs = 12, fn = ft_str });
            fields.Add(new TField { title = "聘　書" + Environment.NewLine + "製作人", property = "undertaker", css = TCss.Center, width = 9, fs = 12, fn = ft_str });

            MapCharSetAndIndex(cfg, fields);

            //工作表
            Spire.Xls.Worksheet sheet = workbook.CreateEmptySheet();
            sheet.Name = "工作表1";

            int mnRow = 1;
            int wsRow = 1;

            //標題列
            SetHeadRow(sheet, wsRow, hrow, fields);
            sheet.SetRowHeight(wsRow, hrow.height);
            wsRow++;

            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string cert_word = "CTPC(" + cfg.mt_annual + ")聘字第";

                var term_s = MapDay(item.getProperty("in_term_s", ""));
                var term_e = MapDay(item.getProperty("in_term_e", ""));
                string term = TermRanngeShort(term_s, term_e);

                item.setProperty("no", (i + 1).ToString());
                item.setProperty("org", " ");

                item.setProperty("issue_day", cfg.NowInfo.en_day2);
                item.setProperty("cert_word", cert_word);
                item.setProperty("in_title", cfg.mt_title);
                item.setProperty("staff_title", "君");
                item.setProperty("work_term", term);
                item.setProperty("undertaker", undertaker);

                SetItemCell(cfg, sheet, wsRow, item, fields);
                sheet.SetRowHeight(wsRow, brow.height);
                wsRow++;
            }

            //設定格線
            SetRangeBorder(sheet, fields, mnRow, wsRow);

            //設定欄寬
            SetColumnWidth(sheet, fields);

            //移除空 sheet
            workbook.Worksheets[0].Remove();
            workbook.Worksheets[0].Remove();
            workbook.Worksheets[0].Remove();
        }

        //聘書(每筆資料一個sheet)
        private void AppointmentSheetXls(TConfig cfg, Item items, Spire.Xls.Workbook workbook)
        {
            //載入 xlsx 範例檔
            workbook.LoadFromFile(cfg.template_path);

            //載入 sheet 樣板
            Spire.Xls.Worksheet sheetTemplate = workbook.Worksheets[0];

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_name = item.getProperty("in_name", "");
                string in_job_name = item.getProperty("in_job_name", "");

                string in_decree = item.getProperty("in_decree", "");
                string cert_word = "CTPC(" + cfg.mt_annual + ")聘字第 " + in_decree + " 號";

                var term_s = MapDay(item.getProperty("in_term_s", ""));
                var term_e = MapDay(item.getProperty("in_term_e", ""));
                string term = TermRannge(term_s, term_e);

                string contents = "　　　　 君 於{#term}擔任本會「{#mt_title}」{#in_job_name}。";
                contents = contents.Replace("{#name}", in_name)
                    .Replace("{#term}", term)
                    .Replace("{#mt_title}", cfg.mt_title)
                    .Replace("{#in_job_name}", in_job_name);

                //工作表
                Spire.Xls.Worksheet sheet = workbook.CreateEmptySheet();
                sheet.CopyFrom(sheetTemplate);
                sheet.Name = (i + 1).ToString().PadLeft(3, '0');

                sheet.Range["T1"].Text = in_name;
                sheet.Range["L13"].Text = cert_word;//聘書字號
                sheet.Range["G36"].Text = cfg.NowInfo.tw_y.ToString();
                sheet.Range["J36"].Text = cfg.NowInfo.day.Month.ToString();
                sheet.Range["M36"].Text = cfg.NowInfo.day.Day.ToString();
                sheet.Range["C17"].Text = contents;//內文
            }

            //移除空 sheet
            workbook.Worksheets[0].Remove();
        }

        #region Spire

        private void MapCharSetAndIndex(TConfig cfg, List<TField> fields)
        {
            for (int i = 0; i < fields.Count; i++)
            {
                var field = fields[i];
                field.ci = i + 1;
                field.cs = cfg.CharSet[field.ci];
            }
        }

        //設定欄寬
        private void SetColumnWidth(Spire.Xls.Worksheet sheet, List<TField> fields)
        {
            foreach (var field in fields)
            {
                sheet.Columns[field.ci - 1].ColumnWidth = field.width;
            }
        }

        //設定格線
        private void SetRangeBorder(Spire.Xls.Worksheet sheet, List<TField> fields, int mnRow, int wsRow)
        {
            var tbl_ps = fields.First().cs + mnRow;
            var tbl_pe = fields.Last().cs + (wsRow - 1);
            var pos = tbl_ps + ":" + tbl_pe;

            var range = sheet.Range[pos];
            range.BorderInside(Spire.Xls.LineStyleType.Thin, System.Drawing.Color.Black);
            range.BorderAround(Spire.Xls.LineStyleType.Thin, System.Drawing.Color.Black);

            var edgeBottom = range.Borders[Spire.Xls.BordersLineType.EdgeBottom];
            edgeBottom.LineStyle = LineStyleType.Thin;
            edgeBottom.Color = Color.Black;


        }

        private void SetMainCell(Spire.Xls.CellRange range, TRow prow, string text)
        {
            range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
            range.Style.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;

            range.Style.Font.FontName = prow.fn;
            range.Style.Font.Size = prow.fs;
            range.Style.Font.IsBold = prow.fb;
            range.Text = text;
        }

        //設定標題列
        private void SetHeadRow(Spire.Xls.Worksheet sheet, int wsRow, TRow hrow, List<TField> fields)
        {
            foreach (var field in fields)
            {
                string cr = field.cs + wsRow;
                SetHead(sheet, cr, hrow, field.title);
            }
        }

        //設定標題列
        private void SetHead(Spire.Xls.Worksheet sheet, string cr, TRow hrow, string value)
        {
            var range = sheet.Range[cr];
            range.Text = value;
            //range.Style.Font.Color = System.Drawing.Color.White;
            //range.Style.Color = System.Drawing.Color.FromArgb(41, 92, 144);//#295C90
            range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
            range.Style.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;
            range.Style.Font.FontName = hrow.fn;
            range.Style.Font.Size = hrow.fs;
            range.Style.Font.IsBold = hrow.fb;
        }
        //設定資料列
        private void SetItemCell(TConfig cfg, Spire.Xls.Worksheet sheet, int wsRow, Item item, List<TField> fields)
        {
            foreach (var field in fields)
            {
                var cr = field.cs + wsRow;
                var va = "";

                if (field.getValue != null)
                {
                    va = field.getValue(cfg, field, item);
                }
                else if (field.property != "")
                {
                    va = item.getProperty(field.property, "");
                }

                SetCell(sheet, cr, va, field);
            }
        }

        //設定資料格
        private void SetCell(Spire.Xls.Worksheet sheet, string cr, string value, TField field)
        {
            var range = sheet.Range[cr];
            range.Style.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;
            range.Style.Font.FontName = field.fn;
            range.Style.Font.Size = field.fs;
            range.Style.Font.IsBold = field.fb;

            switch (field.css)
            {
                case TCss.None:
                    range.Text = value;
                    break;

                case TCss.Center:
                    range.Text = value;
                    range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                    break;

                case TCss.Left:
                    range.Text = value;
                    range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Left;
                    break;

                case TCss.Text:
                    range.Text = "'" + value;
                    range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                    break;

                case TCss.Squence:
                    range.NumberValue = GetInt(value);
                    range.NumberFormat = "0";
                    range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                    break;

                case TCss.Number:
                    range.NumberValue = GetInt(value);
                    range.NumberFormat = "0";
                    range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Right;
                    break;

                case TCss.Phone:
                    var obj = MapPhone(value);
                    if (obj.is_phone_number)
                    {
                        range.NumberValue = obj.code;
                        range.Style.NumberFormat = "0000-000-000";
                        range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                    }
                    else
                    {
                        range.Value = "'" + value;
                        range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                    }
                    break;
            }
        }

        private class TRow
        {
            public double height { get; set; }
            public string fn { get; set; }
            public int fs { get; set; }
            public bool fb { get; set; }
        }

        private class TField
        {
            public string title { get; set; }
            public string property { get; set; }
            public TCss css { get; set; }
            public int ci { get; set; }
            public string cs { get; set; }
            public double width { get; set; }
            public Func<TConfig, TField, Item, string> getValue { get; set; }

            public bool is_merge { get; set; }

            public string fn { get; set; }
            public int fs { get; set; }
            public bool fb { get; set; }
        }

        private class TTEL
        {
            public string source { get; set; }
            public string value { get; set; }
            public bool is_phone_number { get; set; }
            public int code { get; set; }
        }

        private enum TCss
        {
            None = 0,
            Center = 10,
            Left = 15, 
            Text = 100,
            Number = 200,
            Money = 300,
            Day = 400,
            Phone = 500,
            Squence = 600,
        }

        private TTEL MapPhone(string source)
        {
            var result = new TTEL { source = source, is_phone_number = false };

            if (source != "")
            {
                string value = source.Replace("+886", "0")
                    .Replace("-", "")
                    .Replace(" ", "")
                    .Replace(".", "");

                result.value = value;
                result.is_phone_number = value.Length == 10 && value.StartsWith("09");

                if (result.is_phone_number)
                {
                    result.code = GetInt(value);
                }
            }

            return result;
        }

        private string[] GetCharSet()
        {
            return new string[] { "", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
        }

        #endregion Spire

        private Item MeetingInfo(TConfig cfg)
        {
            string sql = @"
                SELECT 
                    id
                    , in_title
                    , DATEADD(hour, 8, in_date_s) AS 'in_date_s'
                    , DATEADD(hour, 8, in_date_e) AS 'in_date_e'
                    , in_decree
                    , in_coursh_hours
                    , in_meeting_type
                    , in_sport_type
                    , in_annual
                FROM
                    IN_CLA_MEETING WITH(NOLOCK)
                WHERE
                    id = '{#meeting_id}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            return cfg.inn.applySQL(sql);
        }

        private Dictionary<string, string> UnderTakerMap(TConfig cfg)
        {
            Dictionary<string, string> map = new Dictionary<string, string>();
            string sql = "SELECT * FROM VU_SportType ORDER BY sort_order";
            Item items = cfg.inn.applySQL(sql);
            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string value = item.getProperty("value", "");
                string in_undertaker = item.getProperty("in_undertaker", "");
                if (!map.ContainsKey(value))
                {
                    map.Add(value, in_undertaker);
                }
            }
            return map;
        }

        private Item MtResumeItems(TConfig cfg)
        {
            string sql = @"
                SELECT
                    t1.id AS 'muid'
                    , t1.in_name
                    , t1.in_gender
                    , t1.in_sno
                    , CONVERT(VARCHAR, DATEADD(hour, 8, t1.in_birth), 111) AS 'in_birth'
                    , t1.in_name_num
                    , t2.id AS 'mrid'
                    , t2.in_score_o_5 -- '學科成績'
                    , t2.in_score_o_3 -- '術科成績'
                    , t2.in_stat_0
                    , t2.in_note
                    , t3.id AS 'rid'
                    , t3.in_instructor_id
                    , t3.in_instructor_level
                    , t3.in_gl_instructor_id
                    , t3.in_gl_instructor_level
                    , t3.in_referee_id
                    , t3.in_referee_level
                    , t3.in_gl_referee_id
                    , t3.in_gl_referee_level
                FROM
                    IN_CLA_MEETING_USER t1 WITH(NOLOCK)
                INNER JOIN
                    IN_CLA_MEETING_RESUME t2 WITH(NOLOCK)
                    ON t2.in_user = t1.id
                LEFT OUTER JOIN
                    IN_RESUME t3 WITH(NOLOCK)
                    ON t3.in_sno = t1.in_sno
                WHERE
                    t1.source_id = '{#meeting_id}'
                ORDER BY
                    t1.in_name_num
                    , t1.in_birth
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        private Item StudyCertItems(TConfig cfg)
        {
            string sql = @"
               SELECT 
                    t1.id AS 'muid'
                    , t1.in_name
                    , t1.in_sno
                    , t1.in_gender
                    , DATEADD(hour, 8, t1.in_birth) AS 'in_birth'
                    , t1.in_l1
                    , t1.in_l2
                    , t1.in_l3
                    , t1.in_creator
                    , t1.in_current_org
                    , t1.in_name_num
                FROM
                    IN_CLA_MEETING_USER t1 WITH(NOLOCK)
                LEFT OUTER JOIN
                    IN_RESUME t2 WITH(NOLOCK)
                    ON t2.login_name = t1.in_sno
                WHERE 
                    t1.source_id = '{#meeting_id}'
                ORDER BY
                    t1.in_birth
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            return cfg.inn.applySQL(sql);
        }

        private Item AppointmentItems(TConfig cfg)
        {
            List<string> filters = new List<string>();

            filters.Add("t1.source_id = '" + cfg.meeting_id + "'");

            if (cfg.id != "")
            {
                filters.Add("t1.id = '" + cfg.id + "'");
            }

            switch (cfg.mt_meeting_type)
            {
                case "game":
                    filters.Add("t2.value LIKE 'gameStaff%'");
                    break;
                case "seminar":
                    filters.Add("t2.value LIKE 'semStaff%'");
                    break;
                case "degree":
                    filters.Add("t2.value LIKE 'porStaff%'");
                    break;
            }

            string sql = @"
               SELECT 
                    t1.id
                    , t1.in_name
                    , t1.in_sno
                    , t1.in_gender
                    , t1.in_job
                    , t1.in_job_name
                    , t1.in_job_title
                    , DATEADD(hour, 8, t1.in_term_s) AS 'in_term_s'
                    , DATEADD(hour, 8, t1.in_term_e) AS 'in_term_e'
                    , t1.in_decree
                FROM
                    IN_CLA_MEETING_STAFF t1 WITH(NOLOCK)
                LEFT OUTER JOIN
                    [VU_Mt_Staff] t2
                    ON t2.label_zt = t1.in_job
                WHERE 
                    
                    {#filter}
                ORDER BY
                    t2.sort_order
                    , t1.in_gender
            ";

            sql = sql.Replace("{#filter}", string.Join(" AND ", filters));

            return cfg.inn.applySQL(sql);
        }

        private TDay MapDay(string value, int hours = 0)
        {
            var dt = GetDateTime(value, hours);
            return MapDay(dt);
        }

        private TDay MapDay(DateTime dt)
        {
            TDay result = new TDay
            {
                value = dt.ToString("yyyy-MM-dd hh:mm:ss"),
                day = dt,
                tw_y = "",
                tw_m = "",
                tw_d = "",
                tw_long_day = "",
                tw_long_day2 = "",
                tw_short_day = "",
                tw_short_day2 = "",
                en_day = "",
                en_day2 = "",
                entw_short_day = "",
            };

            if (result.day != DateTime.MinValue)
            {
                result.tw_y = (result.day.Year - 1911).ToString();
                result.tw_m = result.day.Month.ToString("00");
                result.tw_d = result.day.Day.ToString("00");

                result.en_day = result.day.ToString("dd.MMM.yyyy", new System.Globalization.CultureInfo("EN-US"));
                result.en_day2 = result.day.Year + "/" + result.tw_m + "/" + result.tw_d;
                result.entw_short_day = result.day.Year + "年" + result.tw_m + "月" + result.tw_d + "日";
                result.tw_long_day = "中華民國" + result.tw_y + "年" + result.tw_m + "月" + result.tw_d + "日";
                result.tw_long_day2 = "民國" + result.tw_y + "年" + result.day.Month + "月" + result.day.Day + "日";
                result.tw_short_day = result.tw_y + "年" + result.tw_m + "月" + result.tw_d + "日";
                result.tw_short_day2 = result.tw_y + "." + result.day.Month + "." + result.day.Day;
            }

            return result;
        }

        private string TermRannge(TDay s, TDay e)
        {
            string result = "";
            if (!string.IsNullOrEmpty(s.tw_long_day2))
            {
                result += s.tw_long_day2;
            }
            if (!string.IsNullOrEmpty(e.tw_long_day2))
            {
                result += "至" + e.tw_long_day2.Replace("民國", "");
            }
            if (string.IsNullOrEmpty(result))
            {
                result = "未設定任期起迄";
            }
            return result;
        }

        private string TermRanngeShort(TDay s, TDay e)
        {
            string result = "";
            if (!string.IsNullOrEmpty(s.tw_short_day2))
            {
                result += s.tw_short_day2;
            }
            if (!string.IsNullOrEmpty(e.tw_short_day2))
            {
                result += "~" + e.tw_short_day2;
            }
            if (string.IsNullOrEmpty(result))
            {
                result = "未設定任期起迄";
            }
            return result;
        }

        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }

            public string meeting_id { get; set; }
            public string id { get; set; }
            public string export_type { get; set; }

            public Item itmMeeting { get; set; }
            public string mt_title { get; set; }
            public string mt_date_s { get; set; }
            public string mt_date_e { get; set; }
            public string mt_coursh_hours { get; set; }
            public string mt_decree { get; set; }
            public string mt_annual { get; set; }
            public string mt_sport_type { get; set; }
            public string mt_meeting_type { get; set; }

            public Item itmXls { get; set; }
            public string export_path { get; set; }
            public string template_path { get; set; }
            public string exp_name { get; set; }
            public string exp_type { get; set; }
            public string ext_type { get; set; }
            public string prefix { get; set; }
            public bool is_word { get; set; }

            public TDay NowInfo { get; set; }
            public TDay MtDaySInfo { get; set; }

            public string GetItemTitle { get; set; }
            public Func<TConfig, Item> GetItemFunc { get; set; }

            public Action<TConfig, Item, Spire.Xls.Workbook> SetXlsSheet { get; set; }

            public string[] CharSet { get; set; }
        }


        private class TDay
        {
            public string value { get; set; }
            public DateTime day { get; set; }

            public string tw_y { get; set; }
            public string tw_m { get; set; }
            public string tw_d { get; set; }

            /// <summary>
            /// 中華民國yyy年MM月dd日
            /// </summary>
            public string tw_long_day { get; set; }
            public string tw_long_day2 { get; set; }

            /// <summary>
            /// yyy年MM月dd日
            /// </summary>
            public string tw_short_day { get; set; }
            /// <summary>
            /// yyy.M.d
            /// </summary>
            public string tw_short_day2 { get; set; }

            /// <summary>
            /// dd.MMM.yyyy
            /// </summary>
            public string en_day { get; set; }

            /// <summary>
            /// yyyy/MM/dd
            /// </summary>
            public string en_day2 { get; set; }

            /// <summary>
            /// yyyy年MM月dd日
            /// </summary>
            public string entw_short_day { get; set; }
        }

        private DateTime GetDateTime(string value, int hours)
        {
            if (value == "") return DateTime.MinValue;
            DateTime dt = DateTime.MinValue;
            DateTime.TryParse(value, out dt);
            return dt.AddHours(hours);
        }

        private int GetInt(string value, int defV = 0)
        {
            if (value == "") return 0;

            int result = defV;
            if (int.TryParse(value, out result))
            {
                return result;
            }
            return defV;
        }
    }
}