﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.CTPC.Common
{
    public class In_Cla_MResume : Item
    {
        public In_Cla_MResume(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 講習成績登錄
                日誌: 
                    - 2022-08-29: 證書資料從學員履歷複寫至講師履歷 (lina)
                    - 2022-08-01: 整合審核證書 (lina)
                    - 2022-05-27: 自動計算時數 (lina)
                    - 2022-05-27: 自動判斷成績是否合格 (lina)
                    - 2022-05-23: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]" + "In_Cla_MResume";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                strIdentityId = inn.getUserAliases(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                scene = itmR.getProperty("scene", ""),
            };

            //取得登入者資訊
            string sql = "SELECT id, in_name, in_sno, in_is_admin FROM IN_RESUME WITH(NOLOCK) WHERE in_user_id = '" + cfg.strUserId + "'";
            cfg.itmLoginResume = inn.applySQL(sql);
            if (cfg.itmLoginResume.isError() || cfg.itmLoginResume.getResult() == "")
            {
                throw new Exception("登入者履歷異常");
            }
            cfg.in_is_admin = cfg.itmLoginResume.getProperty("in_is_admin", "");

            //權限檢核
            cfg.itmPermit = inn.applyMethod("In_CheckIdentity", "<method>" + strMethodName + "</method><code>" + "ALL" + "</code>");
            cfg.isMeetingAdmin = cfg.itmPermit.getProperty("isMeetingAdmin", "") == "1";
            if (cfg.in_is_admin == "1")
            {
                cfg.isMeetingAdmin = true;
            }

            if (!cfg.isMeetingAdmin)
            {
                itmR.setProperty("show_func", "0");
                throw new Exception("無權限");
            }
            else
            {
                itmR.setProperty("show_func", "1");
            }

            switch (cfg.scene)
            {
                case "update"://更新資料
                    Update(cfg, itmR);
                    break;

                case "update_all"://更新全部資料
                    UpdateAll(cfg, itmR);
                    RefreshScore(cfg, itmR);
                    RefreshServices(cfg, itmR);
                    break;

                case "all_hours"://更新上課時數
                    RefreshHours(cfg, itmR);
                    break;

                case "verify_page"://審核證書
                    VerifyPage(cfg, itmR);
                    break;

                default:
                    Page(cfg, itmR);
                    break;
            }
            return itmR;
        }

        //登錄至講習服務紀錄與年度
        private void RefreshServices(TConfig cfg, Item itmReturn)
        {
            Item itmData = cfg.inn.newItem("In_Meeting");
            itmData.setProperty("source_id", cfg.meeting_id);
            Item itmResult = itmData.apply("In_Cla_Meeting_Certificate_Update");
        }

        //更新上課時數
        private void RefreshHours(TConfig cfg, Item itmReturn)
        {
            var itmMeeting = MeetingInfo(cfg);
            var rows = AnalysisRecords(cfg);
            foreach (var row in rows)
            {
                string sql = "UPDATE IN_CLA_MEETING_RESUME SET in_hours = '" + row.hours + "' WHERE in_user = '" + row.muid + "'";
                cfg.inn.applySQL(sql);
            }
        }

        private List<TMResume> AnalysisRecords(TConfig cfg)
        {
            //必須落在有效範圍
            string sql = @"
                SELECT
	                t1.id AS 'muid'
	                , t1.in_sno
	                , t1.in_name
	                , t2.in_date
	                , LEFT(t2.in_purpose, 3) AS 'time_group'
	                , t2.in_purpose
                FROM 
					IN_CLA_MEETING_USER t1 WITH(NOLOCK) 
				LEFT OUTER JOIN
				(
					SELECT
						t11.in_participant
						, t11.in_date
						, t11.in_purpose
					FROM
						IN_CLA_MEETING_RECORD t11 WITH(NOLOCK)
					INNER JOIN
						IN_CLA_MEETING_FUNCTIONTIME t12 WITH(NOLOCK)
						ON t12.source_id = t11.source_id
						AND t12.in_action = '2'
						AND t12.in_purpose = t11.in_purpose
					WHERE
						t11.source_id = '{#meeting_id}' -- 簽到退時間必須落在指定區間中，保留 15 分鐘彈性
						AND t11.in_date BETWEEN DATEADD(minute, -15, t12.in_date_s) AND DATEADD(minute, 15, t12.in_date_e)
                ) t2 ON t2.in_participant = t1.id
                WHERE
	                t1.source_id = '{#meeting_id}'
                ORDER BY
	                t1.in_sno
	                , t2.in_date
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();

            List<TMResume> rows = new List<TMResume>();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string muid = item.getProperty("muid", "");
                string in_sno = item.getProperty("in_sno", "");
                string in_name = item.getProperty("in_name", "");
                string time_group = item.getProperty("time_group", "");
                string in_date = item.getProperty("in_date", "");

                var row = rows.Find(x => x.in_sno == in_sno);
                if (row == null)
                {
                    row = new TMResume
                    {
                        muid = muid,
                        in_sno = in_sno,
                        in_name = in_name,
                        days = new List<TDay>(),
                        hours = 0,
                    };
                    rows.Add(row);
                }

                var dt = GetDtm(in_date);
                if (dt == DateTime.MinValue)
                {
                    continue;
                }

                var day = row.days.Find(x => x.time_group == time_group);
                if (day == null)
                {
                    day = new TDay
                    {
                        time_group = time_group,
                        time_s = DateTime.MaxValue,
                        time_e = DateTime.MinValue,
                        hours = 0,
                        items = new List<Item>(),
                    };
                    row.days.Add(day);
                }

                if (dt <= day.time_s) day.time_s = dt;
                if (dt >= day.time_e) day.time_e = dt;

                day.items.Add(item);
            }

            foreach (var row in rows)
            {
                int total = 0;
                foreach (var day in row.days)
                {
                    var ts = day.time_e - day.time_s;
                    var hs = ts.Hours;
                    if (hs > 8) hs = 8;

                    day.hours = hs;
                    total += hs;
                }
                row.hours = total;
            }

            return rows;
        }

        //更新全部資料
        private void UpdateAll(TConfig cfg, Item itmReturn)
        {
            var itmMeeting = MeetingInfo(cfg);
            var rsetting = MapResumeProps(cfg, itmMeeting);

            string value = itmReturn.getProperty("value", "");
            var list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<TRow>>(value);
            if (list == null || list.Count == 0)
            {
                throw new Exception("資料不可為空值");
            }

            foreach (var row in list)
            {
                if (string.IsNullOrWhiteSpace(row.muid)
                    || string.IsNullOrWhiteSpace(row.mrid)
                    || string.IsNullOrWhiteSpace(row.rid))
                {
                    continue;
                }

                var mr_list = new List<TPV>();
                mr_list.Add(new TPV { p = "in_score_o_5", v = GetInt(row.in_score_o_5).ToString() });
                mr_list.Add(new TPV { p = "in_score_o_3", v = GetInt(row.in_score_o_3).ToString() });
                mr_list.Add(new TPV { p = "in_stat_0", v = row.in_stat_0 });
                mr_list.Add(new TPV { p = "in_note", v = row.in_note });
                mr_list.Add(new TPV { p = "in_hours", v = row.in_hours });

                mr_list.Add(new TPV { p = "in_valid_type", v = row.in_valid_type });
                mr_list.Add(new TPV { p = "in_year_note", v = row.in_year_note });
                mr_list.Add(new TPV { p = "in_certificate_no", v = row.in_certificate_no });
                mr_list.Add(new TPV { p = "in_certificate_level", v = row.in_certificate_level });
                mr_list.Add(new TPV { p = "in_certificate_date", v = row.in_certificate_date });
                UpdateProperty(cfg, "In_Cla_Meeting_Resume", row.mrid, mr_list);


                //更新講師履歷
                UpdResumeCertInfo(cfg, itmMeeting, rsetting, row.mrid, row.rid);
            }
        }

        //更新資料
        private void Update(TConfig cfg, Item itmReturn)
        {
            string muid = itmReturn.getProperty("muid", "");
            string mrid = itmReturn.getProperty("mrid", "");
            string rid = itmReturn.getProperty("rid", "");
            string in_name = itmReturn.getProperty("in_name", "");
            string in_sno = itmReturn.getProperty("in_sno", "");
            string prop = itmReturn.getProperty("prop", "");
            string value = itmReturn.getProperty("value", "");

            var list = new List<TPV> { new TPV { p = prop, v = value } };

            switch (prop)
            {
                case "in_hours":
                case "in_score_o_5":
                case "in_score_o_3":
                case "in_note":
                case "in_valid_type":
                case "in_year_note":
                case "in_certificate_no":
                case "in_certificate_level":
                case "in_certificate_date":
                    UpdateProperty(cfg, "In_Cla_Meeting_Resume", mrid, list);
                    break;

                case "in_stat_0":
                    if (value == "--" || value == "請選擇") value = "";
                    UpdateProperty(cfg, "In_Cla_Meeting_Resume", mrid, list);
                    break;

                default:
                    throw new Exception("該欄位不允許更新");
            }

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, in_name + "(" + in_sno + "): " + prop + "變更為: " + value + " (" + muid + ")");

            if (prop == "in_score_o_3" || prop == "in_score_o_5")
            {
                itmReturn.setProperty("need_refresh", "0");
                RefreshScore(cfg, itmReturn);
            }
            else
            {
                itmReturn.setProperty("need_refresh", "0");
            }

            //更新講師履歷
            Item itmMeeting = MeetingInfo(cfg);
            var rsetting = MapResumeProps(cfg, itmMeeting);
            UpdResumeCertInfo(cfg, itmMeeting, rsetting, mrid, rid);
        }

        private void UpdResumeCertInfo(TConfig cfg, Item itmMeeting, TResumeProp rsetting, string mrid, string rid)
        {
            //不需更新相關證書資訊
            if (!rsetting.need_cert) return;

            Item itmMResume = cfg.inn.applySQL("SELECT source_id, in_user, in_valid_type FROM IN_CLA_MEETING_RESUME WITH(NOLOCK) WHERE id = '" + mrid + "'");
            if (itmMResume.isError() || itmMResume.getResult() == "")
            {
                return;
            }

            string in_valid_type = itmMResume.getProperty("in_valid_type", "");
            bool need_update = in_valid_type == "合格" || in_valid_type == "由主辦單位提供(合格)";
            if (!need_update)
            {
                return;
            }

            string sql = @"
                UPDATE t1 SET
	                t1.{#id_prop} = t2.in_certificate_no
	                , t1.{#lv_prop} = t2.in_certificate_level
	                , t1.{#dt_prop} = t2.in_certificate_date
                FROM
	                IN_RESUME t1 WITH(NOLOCK)
                INNER JOIN
	                IN_CLA_MEETING_RESUME t2 WITH(NOLOCK)
	                ON t2.id = '{#mrid}'
                WHERE
	                t1.id = '{#rid}'
            ";

            sql = sql.Replace("{#id_prop}", rsetting.cert_id_pro)
                .Replace("{#lv_prop}", rsetting.cert_lv_pro)
                .Replace("{#dt_prop}", rsetting.cert_dt_pro)
                .Replace("{#mrid}", mrid)
                .Replace("{#rid}", rid);

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            cfg.inn.applySQL(sql);
        }

        //自動刷新合格狀態
        private void RefreshScore(TConfig cfg, Item itmReturn)
        {
            // Item itmMeeting = MeetingInfo(cfg);
            // var mt_title = itmMeeting.getProperty("in_title", "");
            // var mt_level = itmMeeting.getProperty("in_level", "");
            // var seminar_type = itmMeeting.getProperty("in_seminar_type", "");
            // var pass_score = GetPassScore(seminar_type, mt_level);

            // string mrid = itmReturn.getProperty("mrid", "");
            // string mrid_cond = mrid == "" ? "" : "AND id = '" + mrid + "'";

            // string pass_cond = ">= " + pass_score;
            // string fail_cond = "<  " + pass_score;

            // //查無講習合格分數，無法自動判斷
            // if (pass_score <= 0)
            // {
            //     if (mt_title.Contains("增能"))
            //     {
            //         //無視成績都合格
            //         RefreshScore(cfg, mrid_cond, pass_cond, "O");
            //     }
            //     else
            //     {
            //         return;
            //     }
            // }

            // RefreshScore(cfg, mrid_cond, pass_cond, "O");
            // RefreshScore(cfg, mrid_cond, fail_cond, "X");

            // if (mrid != "")
            // {
            //     Item itmMResume = cfg.inn.applySQL("SELECT in_stat_0, ((ISNULL(in_score_o_5, 0) + ISNULL(in_score_o_3, 0)) / 2) AS 'avg' FROM IN_CLA_MEETING_RESUME WITH(NOLOCK) WHERE id = '" + mrid + "'");
            //     if (!itmMResume.isError() && itmMResume.getResult() != "")
            //     {
            //         itmReturn.setProperty("in_stat_0", itmMResume.getProperty("in_stat_0", ""));
            //         itmReturn.setProperty("inn_avg", itmMResume.getProperty("avg", ""));
            //     }
            // }
        }

        //自動刷新成績
        private void RefreshScore(TConfig cfg, string mrid_cond, string score_cond, string in_stat_0)
        {
            string sql = @"
                UPDATE IN_CLA_MEETING_RESUME SET
	                in_stat_0 = '{#in_stat_0}'
                WHERE
	                source_id = '{#meeting_id}' {#mrid_cond}
	                AND ((ISNULL(in_score_o_5, 0) + ISNULL(in_score_o_3, 0)) / 2) {#score_cond}
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#mrid_cond}", mrid_cond)
                .Replace("{#score_cond}", score_cond)
                .Replace("{#in_stat_0}", in_stat_0);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            cfg.inn.applySQL(sql);
        }


        private int GetPassScore(string seminar_type, string mt_level)
        {
            if (seminar_type.Contains("referee"))
            {   //裁判講習
                if (mt_level.Contains("A")) return 85;
                else if (mt_level.Contains("B")) return 80;
                else if (mt_level.Contains("C")) return 70;
                else return 0;
            }
            else if (seminar_type.Contains("coach"))
            {   //教練講習
                return 75;
            }
            else
            {
                return 0;
            }
        }

        private void UpdateProperty(TConfig cfg, string table_name, string id, List<TPV> list)
        {
            Item itmOld = cfg.inn.newItem(table_name, "merge");
            itmOld.setAttribute("where", "id = '" + id + "'");

            foreach (var row in list)
            {
                itmOld.setProperty(row.p, row.v);
            }
            var d = System.DateTime.Now;
            
            Item itmResult = itmOld = itmOld.apply();

            if (itmResult.isError())
            {
                throw new Exception(table_name + " 更新失敗");
            }
        }

        private void Page(TConfig cfg, Item itmReturn)
        {
            Item itmMeeting = MeetingInfo(cfg);
            itmReturn.setProperty("in_title", itmMeeting.getProperty("in_title", ""));
            itmReturn.setProperty("in_meeting_type", itmMeeting.getProperty("in_meeting_type", ""));
            itmReturn.setProperty("in_seminar_type", itmMeeting.getProperty("in_seminar_type", ""));

            Item items = MUserItems(cfg);
            int count = items.getItemCount();
            if (count <= 0)
            {
                throw new Exception("查無學員資料");
            }

            var rsetting = MapResumeProps(cfg, itmMeeting);

            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            head.Append("<thead>");
            head.Append("<tr>");
            head.Append("  <th class='mailbox-subject text-center' data-width='3%' >編號</th>");
            head.Append("  <th class='mailbox-subject text-center' data-width='8%' >姓名</th>");
            head.Append("  <th class='mailbox-subject text-center' data-width='4%' >性別</th>");
            head.Append("  <th class='mailbox-subject text-center' data-width='6%' >身分證字號</th>");
            head.Append("  <th class='mailbox-subject text-center' data-width='7%' >出生年月日</th>");

            head.Append("  <th class='mailbox-subject text-center' data-width='8%' >研習時數</th>");
            head.Append("  <th class='mailbox-subject text-center' data-width='8%' >學科分數</th>");
            head.Append("  <th class='mailbox-subject text-center' data-width='8%' >術科分數</th>");
            // head.Append("  <th class='mailbox-subject text-center' data-width='8%' >平均</th>");
            head.Append("  <th class='mailbox-subject text-center' data-width='8%' >是否通過</th>");
            head.Append("  <th class='mailbox-subject text-center' data-width='10%' >備註</th>");

            if (rsetting.need_cert)
            {
                head.Append("  <th class='mailbox-subject text-center' data-width='8%' >" + rsetting.cert_title + "<br>證照級別</th>");
                head.Append("  <th class='mailbox-subject text-center' data-width='14%' >" + rsetting.cert_title + "<br>證照號碼</th>");
            }

            head.Append("</tr>");
            head.Append("</thead>");


            body.Append("<tbody>");
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string no = item.getProperty("in_name_num", "");
                string muid = item.getProperty("muid", "");
                string mrid = item.getProperty("mrid", "");
                string rid = item.getProperty("rid", "");
                string in_name = item.getProperty("in_name", "");
                string in_gender = item.getProperty("in_gender", "");
                string in_sno = item.getProperty("in_sno", "");
                string average = GetAverage(cfg, item);

                body.Append("<tr class='show_all'"
                    + " data-no='" + no + "'"
                    + " data-muid='" + muid + "'"
                    + " data-mrid='" + mrid + "'"
                    + " data-rid='" + rid + "'"
                    + " data-rname='" + in_name + "'"
                    + " data-rsno='" + in_sno + "'"
                    + ">");

                body.Append("  <td class='text-center'> " + no + " </td>");
                body.Append("  <td class='text-center'> " + in_name + " </td>");
                body.Append("  <td class='text-center'> " + in_gender + " </td>");
                body.Append("  <td class='text-center'> " + in_sno + " </td>");
                body.Append("  <td class='text-center'> " + item.getProperty("in_birth", "") + " </td>");

                body.Append("  <td class='text-center'> " + InputText(cfg, item, "in_hours", "研習時數", "text-right") + " </td>");
                body.Append("  <td class='text-center'> " + InputText(cfg, item, "in_score_o_5", "學科分數", "text-right") + " </td>");
                body.Append("  <td class='text-center'> " + InputText(cfg, item, "in_score_o_3", "術科分數", "text-right") + " </td>");

                // body.Append("  <td class='text-center'> " + InputReadOnly(cfg, average, "inn_avg", "平均", "text-right") + " </td>");


                body.Append("  <td class='text-center'> " + StateMenu(cfg, item, "in_stat_0", "是否通過") + " </td>");
                body.Append("  <td class='text-center'> " + InputText(cfg, item, "in_note", "備註", "", 16) + " </td>");

                if (rsetting.need_cert)
                {
                    body.Append("  <td class='text-center'> " + CertLvMenu(cfg, item, rsetting.cert_lv_pro, rsetting.cert_title + "證照級別") + " </td>");
                    body.Append("  <td class='text-center'> " + CertNoInput(cfg, item, rsetting.cert_id_pro, rsetting.cert_title + "證照號碼", "", 16) + " </td>");
                }

                body.Append("</tr>");
            }
            body.Append("</tbody>");

            string table_name = "data_table";

            StringBuilder builder = new StringBuilder();
            builder.Append("<div class='box-body tablenobg' style='position: relative'>");
            builder.AppendLine(GetTableAttribute(table_name));
            builder.Append(head);
            builder.Append(body);
            builder.AppendLine("</table>");
            builder.Append("</div>");
            builder.AppendLine("<script>");
            builder.AppendLine("$('#" + table_name + "').bootstrapTable();");
            builder.AppendLine("if ($(window).width() <= 768) { $('#" + table_name + "').bootstrapTable('toggleView'); }");
            builder.AppendLine("</script>");

            itmReturn.setProperty("inn_table", builder.ToString());
        }

        private void VerifyPage(TConfig cfg, Item itmReturn)
        {
            Item itmMeeting = MeetingInfo(cfg);
            itmReturn.setProperty("in_title", itmMeeting.getProperty("in_title", ""));
            itmReturn.setProperty("in_meeting_type", itmMeeting.getProperty("in_meeting_type", ""));
            itmReturn.setProperty("in_seminar_type", itmMeeting.getProperty("in_seminar_type", ""));

            Item items = MUserItems(cfg);
            int count = items.getItemCount();
            if (count <= 0)
            {
                throw new Exception("查無學員資料");
            }

            var rsetting = MapResumeProps(cfg, itmMeeting);
            var valid_opts = GetValidTypes(cfg);

            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            head.Append("<thead>");
            head.Append("<tr>");
            head.Append("  <th class='mailbox-subject text-center' data-width='3%' >編號</th>");
            head.Append("  <th class='mailbox-subject text-center' data-width='7%' >姓名</th>");
            // head.Append("  <th class='mailbox-subject text-center' data-width='4%' >性別</th>");
            // head.Append("  <th class='mailbox-subject text-center' data-width='6%' >身分證字號</th>");
            // head.Append("  <th class='mailbox-subject text-center' data-width='7%' >出生年月日</th>");

            head.Append("  <th class='mailbox-subject text-center' data-width='7%'  >學科<br>分數</th>");
            head.Append("  <th class='mailbox-subject text-center' data-width='7%'  >術科<br>分數</th>");
            head.Append("  <th class='mailbox-subject text-center' data-width='10%' >本年度講習紀錄</th>");
            head.Append("  <th class='mailbox-subject text-center' data-width='10%' >備註</th>");

            if (rsetting.need_cert)
            {
                head.Append("  <th class='mailbox-subject text-center' data-width='8%' >" + rsetting.cert_title + "<br>證照級別</th>");
                head.Append("  <th class='mailbox-subject text-center' data-width='14%' >" + rsetting.cert_title + "<br>證照號碼</th>");
                head.Append("  <th class='mailbox-subject text-center' data-width='8%' >" + rsetting.cert_title + "<br>發證日期</th>");
            }

            head.Append("  <th class='mailbox-subject text-center' data-width='15%'>審核類型</th>");
            head.Append("  <th class='mailbox-subject text-center' data-width='6%' >證書</th>");

            head.Append("</tr>");
            head.Append("</thead>");


            body.Append("<tbody>");
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string no = item.getProperty("in_name_num", "");
                string muid = item.getProperty("muid", "");
                string mrid = item.getProperty("mrid", "");
                string rid = item.getProperty("rid", "");
                string in_name = item.getProperty("in_name", "");
                string in_gender = item.getProperty("in_gender", "");
                string in_sno = item.getProperty("in_sno", "");
                string average = GetAverage(cfg, item);

                body.Append("<tr class='show_all'"
                    + " data-no='" + no + "'"
                    + " data-muid='" + muid + "'"
                    + " data-mrid='" + mrid + "'"
                    + " data-rid='" + rid + "'"
                    + " data-rname='" + in_name + "'"
                    + " data-rsno='" + in_sno + "'"
                    + ">");

                body.Append("  <td class='text-center'> " + no + " </td>");
                body.Append("  <td class='text-center'> " + LinkResume(cfg, rid, in_name) + " </td>");
                // body.Append("  <td class='text-center'> " + in_gender + " </td>");
                // body.Append("  <td class='text-center'> " + in_sno + " </td>");
                // body.Append("  <td class='text-center'> " + item.getProperty("in_birth", "") + " </td>");

                body.Append("  <td class='text-center'> " + InputText(cfg, item, "in_score_o_5", "學科分數", "text-right") + " </td>");
                body.Append("  <td class='text-center'> " + InputText(cfg, item, "in_score_o_3", "術科分數", "text-right") + " </td>");
                body.Append("  <td class='text-center'> " + CertInput(cfg, item, "in_year_note", "本年度講習紀錄") + "</td>");
                body.Append("  <td class='text-center'> " + CertInput(cfg, item, "in_note", "備註") + "</td>");

                if (rsetting.need_cert)
                {
                    body.Append("  <td class='text-center'> " + CertLvMenu(cfg, item, rsetting.cert_lv_pro, rsetting.cert_title + "證照級別") + " </td>");
                    body.Append("  <td class='text-center'> " + CertNoInput(cfg, item, rsetting.cert_id_pro, rsetting.cert_title + "證照號碼", "", 16) + " </td>");
                    body.Append("  <td class='text-center'> " + CertDtInput(cfg, item, rsetting.cert_dt_pro, rsetting.cert_title + "發證日期") + "</td>");
                }

                body.Append("  <td class='text-center'> " + ValidMenu(cfg, item, "in_valid_type", "審核類型", valid_opts) + "</td>");
                body.Append("  <td class='text-center'> " + CertPDFButton(mrid) + "</td>");

                body.Append("</tr>");
            }
            body.Append("</tbody>");

            string table_name = "data_table";

            StringBuilder builder = new StringBuilder();
            builder.Append("<div class='box-body tablenobg' style='position: relative'>");
            builder.AppendLine(GetTableAttribute(table_name));
            builder.Append(head);
            builder.Append(body);
            builder.AppendLine("</table>");
            builder.Append("</div>");
            builder.AppendLine("<script>");
            builder.AppendLine("$('#" + table_name + "').bootstrapTable();");
            builder.AppendLine("if ($(window).width() <= 768) { $('#" + table_name + "').bootstrapTable('toggleView'); }");
            builder.AppendLine("</script>");

            itmReturn.setProperty("inn_table", builder.ToString());
        }

        private string LinkResume(TConfig cfg, string resume_id, string in_name)
        {
            return "<a target='_blank' href='c.aspx?page=SingleMemberView.html&method=In_Group_Resume_View&id=" + resume_id + "'>"
                + in_name
                + "</a>"
                ;
        }

        private string CertInput(TConfig cfg, Item item, string prop, string title)
        {
            string mrid = item.getProperty("mrid", "");
            string num = item.getProperty("in_name_num", "");

            string eid = num + "_" + prop;
            string tid = num + "_" + "textcontent";
            string css = num + "_" + prop;
            string value = item.getProperty(prop, "");

            StringBuilder builder = new StringBuilder();
            builder.Append("<div name='" + prop + "' data-proplabel='" + title + "' class='in_input'>");
            builder.Append("  <input");
            builder.Append("    id='" + eid + "'");
            builder.Append("    data-prop='" + prop + "'");
            builder.Append("    class='in_inputdisplay form-control " + css + "'");
            builder.Append("    placeholder='" + title + "'");
            builder.Append("    onclick='ih.handleTextEdit(this,40)'");
            //builder.Append("    onchange='ih.updateTextContent(this)'");
            builder.Append("    onkeyup='cacheElement(this)'");
            builder.Append("    onfocus='cacheElement(this)'");
            builder.Append("    value='" + value + "' autocomplete='off'");
            builder.Append("  >");
            builder.Append("  <template class='in_value' id='" + tid + "'></template>");
            builder.Append("<script> ih.textDisplay('" + eid + "', 1, 40, '...');</script>");
            builder.Append("</div>");
            return builder.ToString();
        }

        private string CertPDFButton(string mrid)
        {
            return "<button class='btn btn-primary' data-mrid='" + mrid + "' onclick='exportCertificate(this)'> <i class='fa fa-print'></i> </button>";
        }

        private string GetAverage(TConfig cfg, Item item)
        {
            string in_score_o_5 = item.getProperty("in_score_o_5", "0");
            string in_score_o_3 = item.getProperty("in_score_o_3", "0");
            int know_score = GetInt(in_score_o_5);
            int tech_score = GetInt(in_score_o_3);
            var avg = (decimal)(know_score + tech_score) / 2;
            return avg.ToString("##.#");
        }

        private string ValidMenu(TConfig cfg, Item item, string prop, string title, string opts)
        {
            string mrid = item.getProperty("mrid", "");
            string num = item.getProperty("in_name_num", "");

            string eid = num + "_" + prop;
            string value = item.getProperty(prop, "");

            StringBuilder builder = new StringBuilder();
            builder.Append("<select id=" + eid + " class='form-control valid_menu " + prop + "'");
            builder.Append("  data-value='" + value + "'");
            builder.Append("  data-prop='" + prop + "'");
            builder.Append("  onchange='doUpdateOption(this)'>");
            builder.Append(opts);
            builder.Append("</select>");
            return builder.ToString();
        }

        private string StateMenu(TConfig cfg, Item item, string prop, string title)
        {
            string mrid = item.getProperty("mrid", "");
            string num = item.getProperty("in_name_num", "");

            string eid = num + "_" + prop;
            string value = item.getProperty(prop, "");

            StringBuilder builder = new StringBuilder();
            builder.Append("<select id=" + eid + " class='form-control score_menu " + prop + "'");
            builder.Append("  data-value='" + value + "'");
            builder.Append("  data-prop='" + prop + "'");
            builder.Append("  onchange='doUpdateOption(this)'>");
            builder.Append("    <option value=''>--</option>");
            builder.Append("    <option value='O'>O</option>");
            builder.Append("    <option value='X'>X</option>");
            builder.Append("</select>");
            return builder.ToString();
        }

        private string CertLvMenu(TConfig cfg, Item item, string propSrc, string title)
        {
            string prop = "in_certificate_level";

            string mrid = item.getProperty("mrid", "");
            string num = item.getProperty("in_name_num", "");

            string mu_cert_val = item.getProperty("in_cert_level", "");
            string mr_cert_val = item.getProperty(prop, "");
            string rm_cert_val = item.getProperty(propSrc, "");

            string eid = num + "_" + prop;
            string value = MatchOfThree(mu_cert_val, mr_cert_val, rm_cert_val);

            StringBuilder builder = new StringBuilder();
            builder.Append("<select id=" + eid + " class='form-control score_menu " + prop + "'");
            builder.Append("  data-value='" + value + "'");
            builder.Append("  data-prop='" + prop + "'");
            builder.Append("  onchange='doUpdateOption(this)'>");
            builder.Append("    <option value=''>--</option>");
            builder.Append("    <option value='A'>A</option>");
            builder.Append("    <option value='B'>B</option>");
            builder.Append("    <option value='C'>C</option>");
            builder.Append("</select>");
            return builder.ToString();
        }

        private string CertNoInput(TConfig cfg, Item item, string propSrc, string title, string text_css, int maxlength = 10)
        {
            string prop = "in_certificate_no";

            string mrid = item.getProperty("mrid", "");
            string num = item.getProperty("in_name_num", "");

            string mu_cert_val = item.getProperty("in_cert_id", "");
            string mr_cert_val = item.getProperty(prop, "");
            string rm_cert_val = item.getProperty(propSrc, "");

            string eid = num + "_" + prop;
            string value = MatchOfThree(mu_cert_val, mr_cert_val, rm_cert_val);

            return "<input id=" + eid + " type='text' class='form-control inn_ctrl inn_text " + text_css + "'"
                + " maxlength='" + maxlength + "'"
                + " onkeyup='cacheElement(this)'"
                + " onfocus='cacheElement(this)'"
                + " data-prop='" + prop + "'"
                + " value='" + value + "'"
                + " placeholder='" + title + "'"
                + " />";
        }

        private string CertDtInput(TConfig cfg, Item item, string propSrc, string title)
        {
            string prop = "in_certificate_date";

            string mrid = item.getProperty("mrid", "");
            string num = item.getProperty("in_name_num", "");

            string mu_cert_val = item.getProperty("in_cert_date", "").Replace("/", "-");
            string mr_cert_val = item.getProperty(prop, "").Replace("/", "-");
            string rm_cert_val = item.getProperty(propSrc, "").Replace("/", "-");

            string eid = num + "_" + prop;
            string tid = num + "_" + "textcontent";
            string css = num + "_" + prop;
            string value = MatchOfThree(mu_cert_val, mr_cert_val, rm_cert_val);

            StringBuilder builder = new StringBuilder();
            builder.Append("<div name='" + prop + "' data-proplabel='" + title + "' class='in_input'>");
            builder.Append("  <input type='date'");
            builder.Append("    id='" + eid + "'");
            builder.Append("    data-prop='" + prop + "'");
            builder.Append("    class='in_inputdisplay form-control " + css + "'");
            builder.Append("    onclick='ih.handleTextEdit(this,40)'");
            //builder.Append("    onchange='ih.updateTextContent(this)'");
            builder.Append("    onchange='doUpdateOption(this)'");
            builder.Append("    onkeyup='cacheElement(this)'");
            builder.Append("    onfocus='cacheElement(this)'");
            builder.Append("    value='" + value + "' autocomplete='off'");
            builder.Append("  >");
            builder.Append("  <template class='in_value' id='" + tid + "'></template>");
            builder.Append("<script> ih.textDisplay('" + eid + "', 1, 40, '...');</script>");
            builder.Append("</div>");
            return builder.ToString();
        }

        private string MatchOfThree(string v1, string v2, string v3)
        {
            if (v1 != "") return v1;
            if (v2 != "") return v2;
            return v3;
        }

        private string InputText(TConfig cfg, Item item, string prop, string title, string text_css, int maxlength = 10)
        {
            string mrid = item.getProperty("mrid", "");
            string num = item.getProperty("in_name_num", "");

            string eid = num + "_" + prop;
            string value = item.getProperty(prop, "");

            return "<input id=" + eid + " type='text' class='form-control inn_ctrl inn_text " + text_css + "'"
                + " maxlength='" + maxlength + "'"
                + " onkeyup='cacheElement(this)'"
                + " onfocus='cacheElement(this)'"
                + " data-prop='" + prop + "'"
                + " value='" + value + "'"
                + " placeholder='" + title + "'"
                + " />";
        }

        private string InputReadOnly(TConfig cfg, string value, string prop, string title, string text_css, int maxlength = 10)
        {
            return "<input type='text' class='form-control inn_ctrl inn_text " + text_css + "'"
                + " maxlength='" + maxlength + "'"
                + " readonly='readonly'"
                + " onfocus='cacheElement(this)'"
                + " data-prop='" + prop + "'"
                + " value='" + value + "'"
                + " placeholder='" + title + "'"
                + " />";
        }

        private string GetTableAttribute(string table_name)
        {
            return "<table id='" + table_name + "'"
                + " class='table table-hover table-striped'"
                + " data-toggle='table'"
                + " data-show-toggle='false'"
                + " data-show-columns='false'"
                + " data-search-align='left'"
                + " data-sort-stable='true'"
                + " data-search='false'"
                + ">";
        }

        private string CertDownload(TConfig cfg, Item item)
        {
            return " <a href='#' onclick='CertDownload_Click(this)' > <i class='fa fa-cloud-download'><i/> </a>";
        }

        private string CheckBox(TConfig cfg, Item item, string prop)
        {
            var value = item.getProperty(prop, "");
            var chked = value == "" ? "" : "checked";

            return "<div class='form-control'>"
                + " <input type='checkbox' class='checkbox inn_chk'"
                + " onclick='IssueCheck_Click(this)'"
                + " data-prop='" + prop + "'"
                + " " + chked
                + " > </div>";
        }
        private Item MeetingInfo(TConfig cfg)
        {
            List<string> cols = new List<string>
            {
                "id",
                "in_title",
                "CONVERT(VARCHAR, DATEADD(hour, 8, in_date_s), 111) AS 'in_date_s'",
                "CONVERT(VARCHAR, DATEADD(hour, 8, in_date_e), 111) AS 'in_date_e'",
                "in_meeting_type",
                "in_seminar_type",
                "in_level",
            };

            string sql = "SELECT " + string.Join(", ", cols) + " FROM IN_CLA_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'";

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item itmMeeting = cfg.inn.applySQL(sql);

            if (itmMeeting.isError() || itmMeeting.getResult() == "")
            {
                throw new Exception("活動資料錯誤");
            }

            return itmMeeting;

        }

        private string GetValidTypes(TConfig cfg)
        {
            var items = cfg.inn.applySQL("SELECT * FROm VU_Seminar_Valid ORDER BY sort_order");

            var builder = new StringBuilder();
            builder.Append("<option value=''>--</option>");

            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string value = item.getProperty("value", "");
                string label = item.getProperty("label", "");
                builder.Append("<option value='" + value + "'>" + label + "</option>");
            }
            return builder.ToString();
        }

        private Item MUserItems(TConfig cfg)
        {
            string sql = @"
                SELECT
                    t1.id AS 'muid'
                    , t1.in_name
                    , t1.in_gender
                    , t1.in_sno
                    , CONVERT(VARCHAR, DATEADD(hour, 8, t1.in_birth), 111) AS 'in_birth'
                    , t1.in_name_num
                    , t1.in_cert_yn
                    , t1.in_cert_id
                    , CONVERT(VARCHAR, DATEADD(hour, 8, t1.in_cert_date), 111) AS 'in_cert_date'
                    , t2.id AS 'mrid'
                    , t2.in_hours     -- '課程時數'
                    , t2.in_score_o_5 -- '學科分數'
                    , t2.in_score_o_3 -- '術科分數'
                    , t2.in_stat_0
                    , t2.in_certificate_no
                    , t2.in_certificate_level
                    , CONVERT(VARCHAR, DATEADD(HOUR, 8, t2.in_certificate_date), 111) AS 'in_certificate_date'
                    , t2.in_valid_type
                    , t2.in_year_note
                    , t2.in_note
                    , t3.id AS 'rid'
                    , t3.in_instructor_id
                    , t3.in_instructor_level
                    , t3.in_gl_instructor_id
                    , t3.in_gl_instructor_level
                    , t3.in_referee_id
                    , t3.in_referee_level
                    , t3.in_gl_referee_id
                    , t3.in_gl_referee_level
                FROM
                    IN_CLA_MEETING_USER t1 WITH(NOLOCK)
                INNER JOIN
                    IN_CLA_MEETING_RESUME t2 WITH(NOLOCK)
                    ON t2.in_user = t1.id
                LEFT OUTER JOIN
                    IN_RESUME t3 WITH(NOLOCK)
                    ON t3.in_sno = t1.in_sno
                WHERE
                    t1.source_id = '{#meeting_id}'
                ORDER BY
                    t1.in_name_num
                    , t1.in_birth
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }

            public string meeting_id { get; set; }
            public string scene { get; set; }

            public Item itmLoginResume { get; set; }
            public string in_is_admin { get; set; }
            public Item itmPermit { get; set; }
            public bool isMeetingAdmin { get; set; }
        }

        private class TResumeProp
        {
            public bool need_cert { get; set; }
            public string cert_title { get; set; }
            public string cert_id_pro { get; set; }
            public string cert_lv_pro { get; set; }
            public string cert_dt_pro { get; set; }
        }

        private class TRow
        {
            public string muid { get; set; }
            public string mrid { get; set; }
            public string rid { get; set; }
            public string in_name { get; set; }
            public string in_sno { get; set; }

            public string in_hours { get; set; }
            public string in_score_o_5 { get; set; }
            public string in_score_o_3 { get; set; }
            public string in_stat_0 { get; set; }
            public string in_note { get; set; }

            public string in_instructor_level { get; set; }
            public string in_instructor_id { get; set; }
            public string in_referee_level { get; set; }
            public string in_referee_id { get; set; }

            public string in_valid_type { get; set; }
            public string in_year_note { get; set; }

            public string in_certificate_no { get; set; }
            public string in_certificate_level { get; set; }
            public string in_certificate_date { get; set; }
        }

        private class TPV
        {
            public string p { get; set; }
            public string v { get; set; }
        }

        private class TMResume
        {
            public string muid { get; set; }
            public string in_name { get; set; }
            public string in_sno { get; set; }
            public List<TDay> days { get; set; }
            public int hours { get; set; }
        }

        private class TDay
        {
            public string time_group { get; set; }
            public DateTime time_s { get; set; }
            public DateTime time_e { get; set; }
            public int hours { get; set; }
            public List<Item> items { get; set; }
        }

        private TResumeProp MapResumeProps(TConfig cfg, Item itmMeeting)
        {
            string in_seminar_type = itmMeeting.getProperty("in_seminar_type", "");

            var result = new TResumeProp
            {
                need_cert = false,
                cert_title = "",
                cert_id_pro = "",
                cert_lv_pro = "",
                cert_dt_pro = "",
            };

            switch (in_seminar_type)
            {
                case "referee"://國內對打裁判
                case "referee_enr"://國內裁判增能
                    result.need_cert = true;
                    result.cert_title = "裁判證";
                    result.cert_id_pro = "in_referee_id";
                    result.cert_lv_pro = "in_referee_level";
                    result.cert_dt_pro = "in_referee_date";
                    break;
                case "referee_gl"://國際對打裁判
                case "referee_enr_gl"://國際裁判增能
                    result.need_cert = true;
                    result.cert_title = "國際裁判證";
                    result.cert_id_pro = "in_gl_referee_id";
                    result.cert_lv_pro = "in_gl_referee_level";
                    result.cert_dt_pro = "in_gl_referee_date";
                    break;
                case "poomsae"://國內品勢裁判
                    result.need_cert = true;
                    result.cert_title = "品勢裁判證";
                    result.cert_id_pro = "in_poomsae_id";
                    result.cert_lv_pro = "in_poomsae_level";
                    result.cert_dt_pro = "in_poomsae_date";
                    break;
                case "poomsae_gl"://國際品勢裁判
                    result.need_cert = true;
                    result.cert_title = "國際品勢裁判證";
                    result.cert_id_pro = "in_gl_poomsae_id";
                    result.cert_lv_pro = "in_gl_poomsae_level";
                    result.cert_dt_pro = "in_gl_poomsae_date";
                    break;
                case "coach"://國內教練
                case "coach_enr"://國內教練增能
                    result.need_cert = true;
                    result.cert_title = "教練證";
                    result.cert_id_pro = "in_instructor_id";
                    result.cert_lv_pro = "in_instructor_level";
                    result.cert_dt_pro = "in_instructor_date";
                    break;
                case "coach_gl"://國際教練
                case "coach_enr_gl"://國際教練增能
                    result.need_cert = true;
                    result.cert_title = "國際教練證";
                    result.cert_id_pro = "in_gl_instructor_id";
                    result.cert_lv_pro = "in_gl_instructor_level";
                    result.cert_dt_pro = "in_gl_instructor_date";
                    break;
                case "degree_tw"://國內段
                    result.need_cert = true;
                    result.cert_title = "國內段";
                    result.cert_id_pro = "in_degree_id";
                    result.cert_lv_pro = "in_degree";
                    result.cert_dt_pro = "in_degree_date";
                    break;
                case "degree_gl"://國際段
                    result.need_cert = true;
                    result.cert_title = "國際段";
                    result.cert_id_pro = "in_gl_degree_id";
                    result.cert_lv_pro = "in_gl_degree";
                    result.cert_dt_pro = "in_gl_degree_date";
                    break;
                default:
                    result.need_cert = false;
                    break;
            }

            return result;
        }

        private DateTime GetDtm(string value)
        {
            if (value == "") return DateTime.MinValue;

            DateTime result = DateTime.MinValue;
            if (DateTime.TryParse(value, out result))
            {
                return result;
            }
            return DateTime.MinValue;
        }

        private int GetInt(string value, int defV = 0)
        {
            if (value == "") return 0;
            int result = defV;
            if (int.TryParse(value, out result)) return result;
            return defV;
        }
    }
}