﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.CTPC.Common
{
    public class In_Close_CheckPhoto : Item
    {
        public In_Close_CheckPhoto(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 檢查證照規則(賽事講習共用)
                輸入: 
                    - meeting id
                    - in_group (作廢)
                    - in_creator_sno
                    - scene (batch=批次審核)
                日誌:
                    - 2022.01.10 增加批次審核 (lina)
                    - 2020.12.06 改為賽事講習共用 (lina)
                    - 2020.12.16 改版為證照設定卡控 (lina)
                    - 2020.09.15 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]In_Close_CheckPhoto";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "input dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                inn = inn,
                meeting_id = itmR.getProperty("meeting_id", ""),
                in_group = itmR.getProperty("in_group", ""),
                in_creator_sno = itmR.getProperty("in_creator_sno", ""),
                mode = itmR.getProperty("mode", ""),
                scene = itmR.getProperty("scene", ""),
            };

            if (cfg.mode == "cla")
            {
                cfg.muser_type = "IN_CLA_MEETING_USER";
                cfg.mcert_type = "IN_CLA_MEETING_CERTIFICATE";
            }
            else
            {
                cfg.muser_type = "IN_MEETING_USER";
                cfg.mcert_type = "IN_MEETING_CERTIFICATE";
            }

            //取得證照設定規則
            var itmRules = GetRules(cfg);
            //轉化規則
            var rules = MapRules(cfg, itmRules);

            //取得與會者
            var itmMUsers = GetMUsers(cfg);
            //取得與會者證照資料
            var itmMUserCertficates = GetMUserCertficates(cfg);
            //轉換與會者證照資料
            var cert_map = ConvertMap(itmMUserCertficates);
            //轉換與會者
            var users = MapMUsers(itmMUsers, cert_map);


            //執行證照驗證
            var result = RunCheck(cfg, rules, users);

            if (result.message != "")
            {
                itmR.setProperty("is_err", "0");
                itmR.setProperty("err_msg", result.message);
                itmR.setProperty("first_muid", result.first_muid);
                itmR.setProperty("first_sno", result.first_sno);
            }
            else
            {
                itmR.setProperty("is_err", "0");
                itmR.setProperty("err_msg", "");
            }

            return itmR;
        }

        /// <summary>
        /// 執行證照驗證
        /// </summary>
        private TResult RunCheck(TConfig cfg, List<TCert> rules, List<TMUser> users)
        {
            TResult result = new TResult
            {
                first_muid = "",
                first_sno = "",
            };

            var builder = new StringBuilder();

            foreach (var rule in rules)
            {
                var messages = new List<string>();
                var un_upload_count = 0;

                var un_muser = new List<TMUser>();
                foreach (var user in users)
                {
                    bool is_ok = IsOk(rule, user);

                    if (!is_ok)
                    {
                        var old = un_muser.Find(x => x.in_sno == user.in_sno);
                        if (old == null)
                        {
                            un_muser.Add(user);
                            user.new_cert_valid = "0";
                            un_upload_count++;
                            messages.Add("姓名:[" + user.in_name + "] 身分證字號:[" + user.in_sno + "]");
                        }

                        if (result.first_muid == "")
                        {
                            result.first_muid = user.muid;
                            result.first_sno = user.in_sno;
                        }
                    }
                }

                if (un_upload_count > 0)
                {
                    builder.AppendLine("<span style='color: red ; background-color: yellow;'>"
                        + "<b>" + rule.label + "</b></span>"
                        + "<span style='color: red ;'>-尚有 " + un_upload_count.ToString() + " 人未上傳: </span></br>"
                        + string.Join("</br>", messages)
                        + "</br>");
                }
            }

            //將驗證結果更新至與會者
            foreach (var user in users)
            {
                if (user.new_cert_valid != user.in_cert_valid)
                {
                    string sql = "UPDATE " + cfg.muser_type + " SET in_cert_valid = '" + user.new_cert_valid + "' WHERE id = '" + user.muid + "'";
                    cfg.inn.applySQL(sql);
                }
            }

            string sql_staff = "UPDATE " + cfg.muser_type + " SET in_cert_valid = '1' WHERE source_id = '" + cfg.meeting_id + "' AND in_l1 = N'隊職員' AND ISNULL(in_sno, '') = ''";
            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql_staff: " + sql_staff);
            cfg.inn.applySQL(sql_staff);

            result.message = builder.ToString();

            return result;
        }


        private bool IsOk(TCert rule, TMUser user)
        {
            //不卡控
            if (rule.is_bypass)
            {
                return true;
            }

            bool need_check = false;
            if (rule.PreMatchFuncs.Count == 0)
            {
                need_check = true;
            }
            else
            {
                foreach (var func in rule.PreMatchFuncs)
                {
                    bool is_match = func(rule, user.Value);
                    if (is_match)
                    {
                        need_check = true;
                        break;
                    }
                }
            }

            bool is_ok = true;

            if (need_check)
            {
                if (rule.is_headshot)
                {
                    if (user.Value.getProperty("in_photo1", "") == "")
                    {
                        //查無大頭照
                        is_ok = false;
                    }
                }
                else if (!user.Certificates.ContainsKey(rule.key))
                {
                    //查無這張證照
                    is_ok = false;
                }
                else
                {
                    TCert ucert = user.Certificates[rule.key];
                    foreach (var property in rule.file_property_list)
                    {
                        //這張證照的檔案之一不存在
                        if (ucert.Value.getProperty(property, "").Trim() == "")
                        {
                            is_ok = false;
                        }
                    }
                }
            }

            return is_ok;
        }

        private List<TCert> MapRules(TConfig cfg, Item itmRules)
        {
            List<TCert> list = new List<TCert>();

            int count = itmRules.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item itmRule = itmRules.getItemByIndex(i);
                TCert cert = new TCert
                {
                    label = itmRule.getProperty("label", ""),
                    in_type = itmRule.getProperty("in_type", ""),
                    in_country = itmRule.getProperty("in_country", ""),
                    in_level = itmRule.getProperty("in_level", ""),
                    in_limit = itmRule.getProperty("in_limit", ""),
                    is_sides = itmRule.getProperty("is_sides", ""),
                    is_enable = itmRule.getProperty("is_enable", ""),
                    in_property = itmRule.getProperty("in_property", ""),
                    in_value = itmRule.getProperty("in_value", ""),
                    in_operator = itmRule.getProperty("in_operator", ""),
                    in_require = itmRule.getProperty("in_require", ""),

                    file_property_list = new List<string>(),
                    PreMatchFuncs = new List<Func<TCert, Item, bool>>(),
                };

                if (cert.in_country == "TW" && !cert.label.Contains("國內"))
                {
                    cert.label = "國內" + cert.label;
                }
                else if (cert.in_country == "GL" && !cert.label.Contains("國際"))
                {
                    cert.label = "國際" + cert.label;
                }

                if (cert.in_type == "" || cert.is_enable != "1")
                {
                    continue;
                }

                if (cert.in_type == "0")
                {
                    cert.is_headshot = true;
                }

                cert.key = cert.in_type + cert.in_country + cert.in_level;
                cert.key = cert.key.ToLower();

                if (cert.is_sides == "1")
                {
                    //正反面
                    cert.file_property_list.Add("in_file1");
                    cert.file_property_list.Add("in_file2");
                }
                else
                {
                    //正面
                    cert.file_property_list.Add("in_file1");
                }

                switch (cert.in_limit)
                {
                    case "0":
                        cert.is_bypass = true;
                        break;

                    case "1":
                        cert.PreMatchFuncs.Add(IsPlayer);
                        break;

                    case "2":
                        cert.PreMatchFuncs.Add(IsPlayer);
                        cert.PreMatchFuncs.Add(IsTutorship);
                        break;

                    default:
                        break;
                }

                switch (cert.in_require)
                {
                    case "1": //必填證書編號
                        cert.file_property_list.Add("in_certificate_no");
                        break;

                    case "2"://必填證書編號+期限
                        cert.file_property_list.Add("in_certificate_no");
                        cert.file_property_list.Add("in_effective_start");
                        cert.file_property_list.Add("in_effective_end");
                        break;

                    default:
                        break;
                }

                if (cert.in_property != "" && cert.in_value != "")
                {
                    cert.PreMatchFuncs.Add(NeedCheck);
                }

                list.Add(cert);
            }
            return list;
        }

        private bool NeedCheck(TCert rule, Item item)
        {
            string v = item.getProperty(rule.in_property, "");
            if (v == "") return false;

            switch (rule.in_property)
            {
                case "in_birth":
                    return CompareDtm(v, rule.in_value, rule.in_operator, bAdd8Hours: true);

                default:
                    return CompareStr(v, rule.in_value, rule.in_operator);
            }
        }

        //字串比較
        private bool CompareStr(string a, string b, string op)
        {
            switch (op)
            {
                case "eq": return a == b; //於
                case "ne": return a != b; //不等於
                case "contains": return CompareContains(a, b);//包含
                case "nc": return !(a.Contains(b)); //不包含
                case "nin": return !(b.Contains(a)); //不內含
                default: return false; //未設定 or 其他
            }
        }

        //字串比較(包含)
        private bool CompareContains(string a, string b)
        {
            if (b.Contains("|"))
            {
                var rows = b.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                if (rows == null || rows.Length == 0)
                {
                    return false;
                }

                var f = false;
                foreach (var row in rows)
                {
                    if (a.Contains(row.Trim()))
                    {
                        f = true;
                        break;
                    }
                }
                return f;

            }
            else
            {
                return a.Contains(b);
            }
        }

        //整數比較
        private bool CompareInt(string a, string b, string op)
        {
            var v1 = GetInt(a);
            var v2 = GetInt(b);

            switch (op)
            {
                case "eq": return v1 == v2; //於
                case "ne": return v1 != v2; //不等於
                case "gt": return v1 > v2;  //大於
                case "ge": return v1 >= v2; //大於等於
                case "lt": return v1 < v2; //小於
                case "le": return v1 <= v2; //小於等於
                case "contains": return (v1 & v2) == v2; //包含(位元算法)
                default: return false; //未設定
            }
        }

        //日期比較
        private bool CompareDtm(string a, string b, string op, bool bAdd8Hours = false)
        {
            var d1 = GetDateTime(a);
            var d2 = GetDateTime(b);

            if (bAdd8Hours)
            {
                d1 = d1.AddHours(8);
            }

            switch (op)
            {
                case "eq": return d1 == d2; //於
                case "ne": return d1 != d2; //不等於
                case "gt": return d1 > d2;  //大於
                case "ge": return d1 >= d2; //大於等於
                case "lt": return d1 < d2; //小於
                case "le": return d1 <= d2; //小於等於
                case "contains": return d1 == d2; //包含
                default: return false; //未設定
            }
        }

        private bool IsPlayer(TCert rule, Item item)
        {
            string in_l1 = item.getProperty("in_l1", "");
            return in_l1 != "隊職員";
        }

        private bool IsTutorship(TCert rule, Item item)
        {
            string in_l2 = item.getProperty("in_l2", "");
            return in_l2 == "教練";
        }

        private Dictionary<string, Dictionary<string, TCert>> ConvertMap(Item items)
        {
            Dictionary<string, Dictionary<string, TCert>> map = new Dictionary<string, Dictionary<string, TCert>>();
            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_type = item.getProperty("in_type", "");
                string in_phototype = item.getProperty("in_phototype", "");

                if (in_phototype == "")
                {
                    continue;
                }

                string user_key = item.getProperty("in_sno", "").ToUpper();
                string type_key = GetTypeKeyFromResume(in_phototype);

                if (type_key == "")
                {
                    continue;
                }

                Dictionary<string, TCert> sub = null;
                if (map.ContainsKey(user_key))
                {
                    sub = map[user_key];
                }
                else
                {
                    sub = new Dictionary<string, TCert>();
                    map.Add(user_key, sub);
                }

                TCert cert = new TCert
                {
                    key = type_key,
                    name = in_type,
                    Value = item,
                };

                if (sub.ContainsKey(type_key))
                {
                    //異常
                }
                else
                {
                    sub.Add(type_key, cert);
                }
            }
            return map;
        }

        private List<TMUser> MapMUsers(Item itmMUsers, Dictionary<string, Dictionary<string, TCert>> cert_map)
        {
            List<TMUser> entities = new List<TMUser>();

            int count = itmMUsers.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item itmMUser = itmMUsers.getItemByIndex(i);
                string muid = itmMUser.getProperty("id", "");
                string in_sno = itmMUser.getProperty("in_sno", "").ToUpper();
                string in_name = itmMUser.getProperty("in_name", "");
                string in_cert_valid = itmMUser.getProperty("in_cert_valid", "");

                TMUser entity = new TMUser
                {
                    muid = muid,
                    in_sno = in_sno,
                    in_name = in_name,
                    Value = itmMUser,
                    in_cert_valid = in_cert_valid,
                    new_cert_valid = "1",
                };

                if (cert_map.ContainsKey(in_sno))
                {
                    entity.Certificates = cert_map[in_sno];
                }
                else
                {
                    entity.Certificates = new Dictionary<string, TCert>();
                }

                entities.Add(entity);
            }

            return entities;
        }

        private string GetTypeKeyFromResume(string value)
        {
            //in_photo7twa_p01,in_photo7twa_n01
            //return 7twa (裁判證-國內-A級)
            if (!value.Contains("in_photo"))
            {
                return "";
            }

            string v = value.Replace("in_photo", "");

            if (string.IsNullOrWhiteSpace(v))
            {
                return "";
            }

            return v.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries).First();
        }

        /// <summary>
        /// 取得證照設定(卡控規則)
        /// </summary>
        private Item GetRules(TConfig cfg)
        {
            string sql = @"
                SELECT
	                t1.*
	                , t2.label
                FROM 
	                {#mcert_type} t1 WITH(NOLOCK)
                LEFT OUTER JOIN
                (
	                SELECT
		                t12.value
		                , t12.label_zt   AS 'label'
		                , t12.sort_order AS 'sort_order'
	                FROM
		                [LIST] t11 WITH(NOLOCK)
	                INNER JOIN
		                [VALUE] t12 WITH(NOLOCK)
		                ON t12.source_id = t11.id
	                WHERE
		                t11.name = N'In_Meeting_CertificateType'
                ) t2
	                ON t2.value = t1.in_type
                WHERE 
	                t1.source_id = '{#meeting_id}'
                ORDER BY
	                t2.sort_order
            ";

            sql = sql.Replace("{#mcert_type}", cfg.mcert_type)
                .Replace("{#meeting_id}", cfg.meeting_id);

            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// 取得與會者
        /// </summary>
        private Item GetMUsers(TConfig cfg)
        {
            string sql = @"SELECT * FROM {#muser_type} WITH(NOLOCK) WHERE source_id = '{#meeting_id}' AND in_creator_sno = N'{#in_creator_sno}' AND ISNULL(in_sno, '') <> ''";

            if (cfg.scene == "batch")
            {
                sql = @"SELECT * FROM {#muser_type} WITH(NOLOCK) WHERE source_id = '{#meeting_id}' AND ISNULL(in_sno, '') <> ''";
            }

            sql = sql.Replace("{#muser_type}", cfg.muser_type)
                .Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_creator_sno}", cfg.in_creator_sno);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// 取得與會者證照(IN_RESUME_CERTIFICATE)
        /// </summary>
        private Item GetMUserCertficates(TConfig cfg)
        {
            string sql = "";
            string condition1 = "";

            condition1 = "SELECT in_sno FROM {#muser_type} WITH(NOLOCK) WHERE source_id = '{#meeting_id}' AND in_creator_sno = N'{#in_creator_sno}'";


            if (cfg.scene == "batch")
            {
                condition1 = "SELECT in_sno FROM {#muser_type} WITH(NOLOCK) WHERE source_id = '{#meeting_id}'";
            }

            condition1 = condition1.Replace("{#muser_type}", cfg.muser_type)
                .Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_creator_sno}", cfg.in_creator_sno);

            sql = @"SELECT t2.in_sno, t1.* FROM IN_RESUME_CERTIFICATE t1 WITH(NOLOCK) INNER JOIN IN_RESUME t2 WITH(NOLOCK) ON t2.id = t1.source_id WHERE t2.in_sno IN (" + condition1 + ")";

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// 與會者
        /// </summary>
        private class TMUser
        {
            /// <summary>
            /// 與會者 id
            /// </summary>
            public string muid { get; set; }

            /// <summary>
            /// 姓名
            /// </summary>
            public string in_name { get; set; }

            /// <summary>
            /// 身分證號
            /// </summary>
            public string in_sno { get; set; }

            /// <summary>
            /// 證照驗證通過
            /// </summary>
            public string in_cert_valid { get; set; }

            /// <summary>
            /// 證照驗證通過 (本次新值)
            /// </summary>
            public string new_cert_valid { get; set; }

            /// <summary>
            /// 資料來源
            /// </summary>
            public Item Value { get; set; }

            /// <summary>
            /// 證照列表
            /// </summary>
            public Dictionary<string, TCert> Certificates { get; set; }
        }

        /// <summary>
        /// 證照
        /// </summary>
        private class TCert
        {
            /// <summary>
            /// 鍵
            /// </summary>
            public string key { get; set; }

            /// <summary>
            /// 證照名稱 (In_Resume_Certificate.in_type)
            /// </summary>
            public string name { get; set; }

            /// <summary>
            /// 證照類型名稱 (下拉選單 In_Meeting_CertificateType label_zt)
            /// </summary>
            public string label { get; set; }

            /// <summary>
            /// 資料來源
            /// </summary>
            public Item Value { get; set; }

            /// <summary>
            /// 證照類別 (0: 大頭照、1: 身分證明、2: 良民證、3: 學歷、4: 戶籍、5: 健康檢查、6: 級段證、7: 裁判證、8: 教練證)
            /// </summary>
            public string in_type { get; set; }

            /// <summary>
            /// 國家別 (TW: 國內、GL: 國際)
            /// </summary>
            public string in_country { get; set; }

            /// <summary>
            /// 級別 (1-9、A-C)
            /// </summary>
            public string in_level { get; set; }

            /// <summary>
            /// 卡控 (0: 不限制、1: 選手必須上傳、2: 選手與教練必須上傳、3: 所有人員必須上傳)
            /// </summary>
            public string in_limit { get; set; }

            /// <summary>
            /// 有正反面
            /// </summary>
            public string is_sides { get; set; }

            /// <summary>
            /// 是否啟用
            /// </summary>
            public string is_enable { get; set; }

            /// <summary>
            /// 篩選欄位 (In_Meeting_User)
            /// </summary>
            public string in_property { get; set; }

            /// <summary>
            /// 比較子
            /// </summary>
            public string in_operator { get; set; }

            /// <summary>
            /// 篩選值(符合者進行卡控判斷)
            /// </summary>
            public string in_value { get; set; }

            /// <summary>
            /// 必填項目
            /// </summary>
            public string in_require { get; set; }

            /// <summary>
            /// 不卡控
            /// </summary>
            public bool is_bypass { get; set; }

            /// <summary>
            /// 大頭照卡控
            /// </summary>
            public bool is_headshot { get; set; }

            /// <summary>
            /// 有正反面就會有兩筆 (in_file1，in_file2)
            /// </summary>
            public List<string> file_property_list { get; set; }

            /// <summary>
            /// 先成立條件才卡控 (無資料代表直接卡控) (其中一個成立就卡控)
            /// </summary>
            public List<Func<TCert, Item, bool>> PreMatchFuncs { get; set; }
        }

        private class TResult
        {
            public string message { get; set; }
            public string first_muid { get; set; }
            public string first_sno { get; set; }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public Innovator inn { get; set; }

            /// <summary>
            /// 與會者 ItemType
            /// </summary>
            public string muser_type { get; set; }
            /// <summary>
            /// 證照設定 ItemType
            /// </summary>
            public string mcert_type { get; set; }
            /// <summary>
            /// 活動 id
            /// </summary>
            public string meeting_id { get; set; }
            /// <summary>
            /// 協助報名者 所屬群組
            /// </summary>
            public string in_group { get; set; }
            /// <summary>
            /// 協助報名者 身分證號
            /// </summary>
            public string in_creator_sno { get; set; }
            /// <summary>
            /// 模式
            /// </summary>
            public string mode { get; set; }
            /// <summary>
            /// 場景
            /// </summary>
            public string scene { get; set; }
        }

        private int GetInt(string value, int def = 0)
        {
            int result = def;
            int.TryParse(value, out result);
            return result;
        }

        private DateTime GetDateTime(string value)
        {
            DateTime result = DateTime.MinValue;
            DateTime.TryParse(value, out result);
            return result;

        }
    }
}