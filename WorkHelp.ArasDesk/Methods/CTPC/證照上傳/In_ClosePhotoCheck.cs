﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.CTPC.Common
{
    public class In_ClosePhotoCheck : Item
    {
        public In_ClosePhotoCheck(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的:抓出該單位在賽事裡面[未上傳照片]的學員
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]" + "In_ClosePhotoCheck";

            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            string aml = "";
            string sql = "";
            string strError = "";
            string strUserId = inn.getUserID();
            string strIdentityId = inn.getUserAliases();

            string meeting_id = this.getProperty("meeting_id", "");
            string mode = this.getProperty("mode", "");
            string func = this.getProperty("func", "");

            string muser_table = "";
            string mcert_table = "";

            if (mode == "cla")
            {
                muser_table = "In_Cla_Meeting_User";
                mcert_table = "In_Cla_Meeting_Certificate";
            }
            else
            {
                muser_table = "In_Meeting_User";
                mcert_table = "In_Meeting_Certificate";
            }

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            //取得登入者權限
            Item itmPermit = inn.applyMethod("In_CheckIdentity", "<method>" + strMethodName + "</method><code>ALL</code>");
            bool isMeetingAdmin = itmPermit.getProperty("isMeetingAdmin", "") == "1";
            bool isCommittee = itmPermit.getProperty("isCommittee", "") == "1";
            bool isGymOwner = itmPermit.getProperty("isGymOwner", "") == "1";
            bool isGymAssistant = itmPermit.getProperty("isGymAssistant", "") == "1";

            //如果是屬於 ACT_GymOwner 則只能看到自己道館的資料
            string SQL_OnlyMyGym = "";
            aml = "<AML>" +
                "<Item type='In_Resume' action='get'>" +
                "<in_user_id>" + strUserId + "</in_user_id>" +
                "</Item></AML>";
            Item Resume = inn.applyAML(aml);

            if (!isMeetingAdmin)
            {
                //道館負責人
                SQL_OnlyMyGym = " AND t1.in_creator_sno = N'" + Resume.getProperty("in_sno", "") + "'";
            }

            //檢查該活動證照設定
            sql = @"
                SELECT 
					t1.in_type
                    , t1.in_country
                    , t1.in_level
                    , t1.in_limit
                    , t1.is_sides
                    , t1.is_hideinfo
                    , t1.in_option
                    , t1.in_photo_width
                    , t1.in_photo_height
                    , t1.in_photo_no_compress
                    , t1.in_property
                    , t1.in_operator
                    , t1.in_value
                    , t2.label_zt AS 'in_type_name'
                    , t3.label_zt AS 'in_country_name'
                    , t4.label_zt AS 'in_level_name'
                FROM 
					{#mcert_table} t1 WITH(NOLOCK)
                INNER JOIN
                    VU_Mt_CertType t2 
					ON t2.value = t1.in_type
                LEFT OUTER JOIN
					VU_Mt_CertCountry t3
					ON t3.value = t1.in_country
                LEFT OUTER JOIN
					VU_Mt_CertLevel t4
					ON t4.value = t1.in_level
                WHERE 
					t1.source_id = '{#meeting_id}'
                    AND t1.is_enable = 1
                    AND t1.in_type != 0
                ORDER BY 
					t1.in_type
            ";

            sql = sql.Replace("{#mcert_table}", mcert_table)
                .Replace("{#meeting_id}", meeting_id);

            Item itmCertificates = inn.applySQL(sql);

            string hide_photo = "";
            string presentation = "";
            string id = "";
            string name = "";
            string in_type = "";
            string in_country = "";
            string in_level = "";
            string is_sides = "";
            string is_hideInfo = "";
            string in_photo_width = "";
            string in_photo_height = "";
            string in_photo_no_compress = "";
            string in_option = "";
            string[] sides = { "(正)", "(反)" };
            string typeName = "";
            string countryName = "";
            string levelName = "";
            string hide_photo_str = "";
            string presentation_str = "";
            string tabpanel_str = "";
            string in_photo = "";
            string in_photo_str = "";

            string in_v_str = "";

            string centvaild_str = "AND ISNULL(in_cert_valid, '') <> '1'";
            if (func == "noCV")
            {
                centvaild_str = "";
            }

            string tabpanel = "";
            Item itmCertificate;
            string in_type_str = "";
            if (!itmCertificates.isError() && itmCertificates.getItemCount() > 0)
            {
                hide_photo = "<input type='hidden' value='{#in_photo}' class='in_photo' id='hide_photo{#id}'/>";
                presentation = "<li role='presentation' onclick=\"selPhotoFunc('a{#id}')\" class='proof_photo_show_O'><a href='#in_photo_a{#id}' aria-controls='in_photo_a{#id}' role='tab' data-toggle='tab'>{#Name}</a></li>";
                for (int j = 0; j < itmCertificates.getItemCount(); j++)
                {
                    itmCertificate = itmCertificates.getItemByIndex(j);
                    is_sides = itmCertificate.getProperty("is_sides", "");

                    in_type = itmCertificate.getProperty("in_type", "");
                    in_country = itmCertificate.getProperty("in_country", "");
                    in_level = itmCertificate.getProperty("in_level", "");
                    typeName = itmCertificate.getProperty("in_type_name", "");
                    levelName = itmCertificate.getProperty("in_level_name", "");
                    countryName = itmCertificate.getProperty("in_country_name", "");

                    if (in_level != "" && typeName == "裁判證")
                    {
                        typeName = "級" + typeName;
                    }
                    id = in_type + in_country + in_level + "_p01";
                    if (is_sides == "1") //判斷是否為正反面功能
                    {
                        name = countryName + levelName + typeName + sides[0];
                    }
                    else
                    {
                        name = countryName + levelName + typeName;
                    }
                    id = id.ToLower();
                    hide_photo_str += hide_photo.Replace("{#in_photo}", in_photo).Replace("{#id}", id);
                    presentation_str += presentation.Replace("{#id}", id).Replace("{#Name}", name);
                    in_type_str += ",a" + id;
                    if (is_sides == "1") //判斷是否為正反面功能(需增加反面)
                    {
                        in_photo = itmR.getProperty("in_photo" + id.Replace("_p01", "_n01"), "");
                        hide_photo_str += hide_photo.Replace("{#id}", id).Replace("{#in_photo}", in_photo).Replace("_p01", "_n01");
                        presentation_str += presentation.Replace("{#id}", id)
                                                    .Replace("{#Name}", name).Replace("_p01", "_n01").Replace(sides[0], sides[1]);
                        in_type_str += ",a" + id.Replace("_p01", "_n01");
                    }

                }
                itmR.setProperty("inn_hide_photo", hide_photo_str);
                itmR.setProperty("inn_presentation", presentation_str);
                itmR.setProperty("inn_type_list", in_type_str);
            }


            sql = @"
                SELECT 
                    T1.in_sno
                    , T3.*
                FROM {#muser_table} AS T1 WITH (NOLOCK)
                    JOIN in_resume AS T2 WITH (NOLOCK) ON T1.in_sno = T2.in_sno
                    LEFT JOIN IN_RESUME_CERTIFICATE AS T3 WITH (NOLOCK) ON T2.id = T3.SOURCE_ID
                WHERE T1.source_id = N'{#meeting_id}'
                    {#centvaild_str} {#SQL_OnlyMyGym} AND T3.IN_PHOTOTYPE like '%photo%'
            ";

            sql = sql.Replace("{#muser_table}", muser_table)
                    .Replace("{#meeting_id}", meeting_id)
                    .Replace("{#SQL_OnlyMyGym}", SQL_OnlyMyGym)
                    .Replace("{#centvaild_str}", centvaild_str);

            Item itmPhotos = inn.applySQL(sql);

            string in_file = "";
            string[] in_photoType = { };
            string in_certificate_no = "";
            string in_certificate_name = "";
            string in_effective_start = "";
            string in_effective_end = "";
            string in_certificate_level = "";
            string in_cer_no = "";
            string in_cer_name = "";
            string in_eff_start = "";
            string in_eff_end = "";
            string in_cer_sno = "";
            string in_cer_level = "";
            Dictionary<string, string> un_upload_map = new Dictionary<string, string>();

            if (!itmPhotos.isError() && itmPhotos.getItemCount() > 0)
            {
                for (int i = 0; i < itmPhotos.getItemCount(); i++)
                {
                    Item itmPhoto = itmPhotos.getItemByIndex(i);
                    in_photoType = itmPhoto.getProperty("in_phototype", "").Split(',');
                    in_cer_sno = itmPhoto.getProperty("in_sno", "");
                    //前台動態id
                    in_cer_no = in_photoType[0].Replace("in_photo", "in_cer_no");
                    in_cer_name = in_photoType[0].Replace("in_photo", "in_cer_name");
                    in_eff_start = in_photoType[0].Replace("in_photo", "in_eff_start");
                    in_eff_end = in_photoType[0].Replace("in_photo", "in_eff_end");
                    in_cer_level = in_photoType[0].Replace("in_photo", "in_cer_level");
                    //取值
                    in_certificate_no = itmPhoto.getProperty("in_certificate_no", "");
                    in_certificate_name = itmPhoto.getProperty("in_certificate_name", "");
                    in_effective_start = getDateTimeValue(itmPhoto.getProperty("in_effective_start", ""));
                    in_effective_end = getDateTimeValue(itmPhoto.getProperty("in_effective_end", ""));
                    in_certificate_level = itmPhoto.getProperty("in_certificate_level", "");

                    itmR.setProperty(in_cer_no + in_cer_sno, in_certificate_no);
                    itmR.setProperty(in_cer_name + in_cer_sno, in_certificate_name);
                    itmR.setProperty(in_eff_start + in_cer_sno, in_effective_start);
                    itmR.setProperty(in_eff_end + in_cer_sno, in_effective_end);
                    itmR.setProperty(in_cer_level + in_cer_sno, in_certificate_level);

                    for (int j = 0; j < in_photoType.Length; j++)
                    {
                        in_file = itmPhoto.getProperty("in_file" + (j + 1).ToString(), "");
                        string property = in_photoType[j] + in_cer_sno;
                        itmR.setProperty(property, in_file);

                        if (!un_upload_map.ContainsKey(property))
                        {
                            un_upload_map.Add(property, in_file);
                            //CCO.Utilities.WriteDebug(strMethodName, "property: " + property);
                        }
                    }
                }
            }


            sql = "SELECT * FROM {#muser_table} AS T1 WITH (NOLOCK) WHERE source_id = '" + meeting_id + "' AND ISNULL(in_sno, '') <> '' {#centvaild_str} " + SQL_OnlyMyGym;

            sql = sql.Replace("{#muser_table}", muser_table)
                .Replace("{#centvaild_str}", centvaild_str);

            //CCO.Utilities.WriteDebug("In_ClosePhotoCheck", "in_meeting_users:" + in_meeting_users.dom.InnerXml);

            Item in_meeting_users = inn.applySQL(sql);

            int muser_count = in_meeting_users.getItemCount();

            Dictionary<string, Item> Uset_sno = new Dictionary<string, Item>();
            string player_show = "O";


            //抓出第一個學員的resume id當作預設值
            string in_sno = this.getProperty("in_sno", "");
            if (in_sno == "" && muser_count <= 0)
            {
                Item itmEmpty = inn.newItem();
                itmEmpty.setType("Inn_Meeting_User");
                itmEmpty.setProperty("hide_save_btn", "item_show_0");
                itmR.addRelationship(itmEmpty);

                itmR.setProperty("close_modal", "1");
                //結束邏輯
                return itmR;
            }
            else if (in_sno != "")
            {
                sql = "SELECT id FROM In_Resume WITH(NOLOCK) where in_sno = N'" + in_sno + "'";
            }
            else
            {
                string _sno = in_meeting_users.getItemByIndex(0).getProperty("in_sno", "");
                sql = "SELECT id FROM In_Resume WITH(NOLOCK) where in_sno= N'" + _sno + "'";
            }

            Item In_Resumet = inn.applySQL(sql);
            itmR.setProperty("id", In_Resumet.getProperty("id", ""));//第一個學員or指定進來的學員

            var muser_map = new Dictionary<string, List<string>>();
            var sno_map = GetMUserSnoList(in_meeting_users);

            for (int i = 0; i < in_meeting_users.getItemCount(); i++)
            {
                Item in_meeting_user = in_meeting_users.getItemByIndex(i);
                in_meeting_user.setType("Inn_Meeting_User");
                in_sno = in_meeting_user.getProperty("in_sno", "");
                in_photo = in_meeting_user.getProperty("in_photo1", "");
                in_photo_str += "\"img_a_" + in_sno + "\":\"" + in_photo + "\",";
                if (!Uset_sno.ContainsKey(in_sno))
                {
                    tabpanel_str = "";
                    sql = "select id from In_Resume WITH(NOLOCK) where in_sno=N'" + in_sno + "'";
                    Item In_Resume = inn.applySQL(sql);
                    if (!itmCertificates.isError() && itmCertificates.getItemCount() > 0)
                    {
                        tabpanel = "<div role='tabpanel' class='col-lg-8 col-md-8 col-sm-12 col-xs-12 text-center border-right tab-pane photo-panel photo_show_X' id='in_photo_a{#id}_{#in_sno}'><div id='in_cer{#id}_{#in_sno}' class='col-md-6' style='display:'><div id='certificate_no{#id}_{#in_sno}' name='certificate_no{#id}_{#in_sno}' class='in_input form-group in-org-show'><input class='in_display form-control' onclick='ih.handleTextEdit(this,25)' onchange='onChangeText(this);' value='{#in_cer_no}' placeholder='證書編號' maxlength='16'><textarea class='in_invisibleparam in_value'></textarea></div><div id='certificate_name{#id}_{#in_sno}' name='certificate_name{#id}_{#in_sno}' class='in_input form-group in-org-show'><input class='in_display form-control' onclick='ih.handleTextEdit(this,25)' onchange='onChangeText(this);'  value='{#in_cer_name}' placeholder='證書名稱' maxlength='16'><textarea class='in_invisibleparam in_value'></textarea></div><div id='effective_s{#id}_{#in_sno}'  name='effective_s{#id}_{#in_sno}' class='in_input  form-group in-org-show'><p style='text-align: left;'>效期起(發證日):<br>(例1990/01/01)</p><input type='date' class='in_display form-control' onclick='ih.handleTextEdit(this,25)' onchange='onChangeText(this);'  value='{#in_eff_start}' ><textarea class='in_invisibleparam in_value'></textarea></div><div id='effective_e{#id}_{#in_sno}' name='effective_e{#id}_{#in_sno}' class='in_input form-group in-org-show'><p style='text-align: left;'>效期迄:</p><input type='date' class='in_display form-control' onclick='ih.handleTextEdit(this,25)' onchange='onChangeText(this);'  value='{#in_eff_end}' ><textarea class='in_invisibleparam in_value'></textarea></div><div id='certificate_option{#id}_{#in_sno}' name='certificate_option{#id}_{#in_sno}' class='in_input form-group in-org-show' style='color:black;display:{#in_option_status}'><select onchange='onChangeText(this);'  class='in_display form-control' value='{#in_cer_level}' ><option value='' disabled selected >選擇級別</option>{#in_option_str}<textarea class='in_invisibleparam in_value'></textarea></div></div><div class='portrait in_input' id='in_photoa{#id}_{#in_sno}' name='in_photo{#id}_{#in_sno}' data-file_maxwidth='{#in_photo_width}' data-file_maxheight='{#in_photo_height}' data-file_quality='0.5' data-file_extension='jpeg,jpg,png,gif,bmp'  data-file_size='3' data-file_proportion='0'><img id='img_{#id}{#in_sno}' class='in_thumbnail' style='width:100%;' src='../Images/taekwondo/default_photo.png' alt=''><textarea class='in_invisibleparam in_value'></textarea></div><button class='btn btn-primary' style='margin-left: auto;margin-right: auto; margin-top: 10px; margin-bottom: 20px;' onclick=\"ih.ShowSelectModal('file',$('#in_photoa{#id}_{#in_sno}'))\"><i class='fa fa-cloud-upload' aria-hidden='true' style='margin-right: 5px;' ></i>上傳相片</button><button class='btn btn-default rotate-img' onclick='rotateImage(this)' style='margin-left: auto;margin-right: auto; margin-top: 10px; margin-bottom: 20px; display: none;' ><i class='fa fa-rotate-right' aria-hidden='true' style='margin-right: 5px;'></i>旋轉</button></div>";

                        for (int j = 0; j < itmCertificates.getItemCount(); j++)
                        {
                            itmCertificate = itmCertificates.getItemByIndex(j);
                            AddMUserCertStatus(CCO, muser_map, in_meeting_user, itmCertificate, un_upload_map);



                            is_hideInfo = itmCertificate.getProperty("is_hideinfo", "");
                            if (is_hideInfo == "1") //判斷是否隱藏證照資訊
                            {
                                tabpanel = tabpanel.Replace("style='display:'", "style='display : none'");
                            }
                            else
                            {
                                tabpanel = tabpanel.Replace("style='display : none", "style='display:'");
                            }

                            is_sides = itmCertificate.getProperty("is_sides", "");

                            in_type = itmCertificate.getProperty("in_type", "");
                            in_country = itmCertificate.getProperty("in_country", "");
                            in_level = itmCertificate.getProperty("in_level", "");

                            id = in_type + in_country + in_level + "_p01";

                            id = id.ToLower();
                            in_cer_no = itmR.getProperty("in_cer_no" + id + in_sno, "");
                            in_cer_name = itmR.getProperty("in_cer_name" + id + in_sno, "");
                            in_eff_start = itmR.getProperty("in_eff_start" + id + in_sno, "");
                            in_eff_end = itmR.getProperty("in_eff_end" + id + in_sno, "");
                            in_cer_level = itmR.getProperty("in_cer_level" + id + in_sno, "");
                            tabpanel_str += tabpanel.Replace("{#id}", id)
                                                     .Replace("{#in_cer_no}", in_cer_no)
                                                     .Replace("{#in_cer_name}", in_cer_name)
                                                     .Replace("{#in_eff_start}", in_eff_start)
                                                     .Replace("{#in_eff_end}", in_eff_end)
                                                     .Replace("{#in_cer_level}", in_cer_level);

                            //新增照片像素設定
                            in_photo_width = itmCertificate.getProperty("in_photo_width", "");
                            in_photo_height = itmCertificate.getProperty("in_photo_height", "");
                            in_photo_no_compress = itmCertificate.getProperty("in_photo_no_compress", "");

                            //不要壓縮
                            if (in_photo_no_compress == "1")
                            {
                                in_photo_width = "999";
                                in_photo_height = "999";
                            }

                            if (in_photo_width == "")
                            {
                                tabpanel_str = tabpanel_str.Replace("{#in_photo_width}", "500");
                            }
                            else
                            {
                                tabpanel_str = tabpanel_str.Replace("{#in_photo_width}", in_photo_width);
                            }

                            if (in_photo_height == "")
                            {
                                tabpanel_str = tabpanel_str.Replace("{#in_photo_height}", "500");
                            }
                            else
                            {
                                tabpanel_str = tabpanel_str.Replace("{#in_photo_height}", in_photo_height);
                            }

                            //新增級別選項
                            in_option = itmCertificate.getProperty("in_option", "");
                            if (in_option == "")
                            {
                                tabpanel_str = tabpanel_str.Replace("{#in_option_status}", "none");
                            }
                            else
                            {
                                tabpanel_str = tabpanel_str.Replace("{#in_option_status}", "");
                                string in_option_str = "";
                                string[] optionArr = in_option.Split(',');
                                foreach (var option in optionArr)
                                {
                                    if (in_cer_level == option)
                                    {
                                        in_option_str += "<option selected value='" + option + "'>" + option + "</option>";
                                    }
                                    else
                                    {
                                        in_option_str += "<option value='" + option + "'>" + option + "</option>";
                                    }
                                }

                                tabpanel_str = tabpanel_str.Replace("{#in_option_str}", in_option_str);
                            }


                            in_photo = itmR.getProperty("in_photo" + id + in_sno, "");
                            if (in_photo != "")
                            {
                                in_photo_str += "\"img_" + id + in_sno + "\":\"" + in_photo + "\",";
                            }

                            if (is_sides == "1") //判斷是否為正反面功能(需增加反面)
                            {

                                in_photo = itmR.getProperty("in_photo" + id.Replace("_p01", "_n01") + in_sno, "");
                                if (in_photo != "")
                                {
                                    in_photo_str += "\"img_" + id.Replace("_p01", "_n01") + in_sno + "\":\"" + in_photo + "\",";
                                }
                                tabpanel_str += tabpanel.Replace("{#id}", id).Replace("_p01", "_n01").Replace("style='display:'", "style='display:none'");
                            }

                        }
                        in_meeting_user.setProperty("inn_tabpanel", tabpanel_str);

                    }

                    //var empty_photo_list = muser_map.ContainsKey(in_sno) ? muser_map[in_sno]: new List<string>();
                    //if (in_v_str == "" && empty_photo_list.Count != 0)
                    //{
                    //    itmR.setProperty("first_player", "var first_player = '" + in_sno + "';");
                    //}

                    //in_v_str += "'" + in_sno + "':'" + in_meeting_user.getProperty("inn_no_verify", "") + "',";

                    in_meeting_user.setProperty("resume_id", In_Resume.getProperty("id", ""));
                    Uset_sno.Add(in_sno, in_meeting_user);
                    in_meeting_user.setProperty("player_show", player_show);//是否秀出
                    itmR.addRelationship(in_meeting_user);
                    player_show = "X";
                    in_meeting_user.setProperty("in_birth", getDateTimeValue(in_meeting_user.getProperty("in_birth", "")));

                    var posi = GetPosition(sno_map, in_sno);
                    if (posi.prev != "")
                    {
                        in_meeting_user.setProperty("show_btn_1", posi.prev);//上一位
                    }
                    else
                    {
                        in_meeting_user.setProperty("show_btn_1", "X");//上一位
                    }

                    if (posi.next != "")
                    {
                        in_meeting_user.setProperty("show_btn_2", posi.next);//下一位
                    }
                    else
                    {
                        in_meeting_user.setProperty("show_btn_2", "X");//下一位
                    }
                }
            }

            if (muser_map.Count > 0)
            {
                //必有人員缺少證照
                var kv = muser_map.FirstOrDefault();
                itmR.setProperty("first_player", "var first_player = '" + kv.Key + "';");
                itmR.setProperty("CV_title", "(必須上傳照片)");
            }

            if (func == "noCV")
            {
                itmR.setProperty("CV_title", "");
                itmR.setProperty("first_player", "var first_player = '" + in_meeting_users.getItemByIndex(0).getProperty("in_sno", "") + "';");
            }

            itmR.setProperty("mode", mode);
            itmR.setProperty("inn_photos", "var photoData = {" + in_photo_str + "}");
            itmR.setProperty("inn_v_str", "var vData = " + Newtonsoft.Json.JsonConvert.SerializeObject(muser_map) + "");


            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

            return itmR;
        }

        private class TPosi
        {
            public string prev { get; set; }
            public string next { get; set; }
        }

        private TPosi GetPosition(List<string> rows, string in_sno)
        {
            var result = new TPosi { prev = "", next = "" };
            if (rows.Count == 0) return result;
            
            var val = in_sno.Trim().ToUpper();
            var min = 0;
            var max = rows.Count - 1;

            for (int i = 0; i < rows.Count; i++)
            {
                var row = rows[i];
                if (row == val)
                {
                    result.prev = i == min ? "" : rows[i - 1];
                    result.next = i == max ? "" : rows[i + 1];
                }
            }
            return result;
        }

        private List<string> GetMUserSnoList(Item itmMUsers)
        {
            var result = new List<string>();
            var cnt = itmMUsers.getItemCount();
            for (var i = 0; i < cnt; i++)
            {
                var itmMUser = itmMUsers.getItemByIndex(i);
                var in_sno = itmMUser.getProperty("in_sno", "").Trim().ToUpper();
                if (in_sno == "") continue;
                if (result.Contains(in_sno)) continue;
                result.Add(in_sno);
            }
            return result;
        }

        private void AddMUserCertStatus(Aras.Server.Core.CallContext CCO, Dictionary<string, List<string>> muser_map, Item itmMUser, Item itmMCert, Dictionary<string, string> map)
        {
            string in_type = itmMCert.getProperty("in_type", "");
            string in_phototype = itmMCert.getProperty("in_type", "");
            string in_country = itmMCert.getProperty("in_country", "").ToLower();
            string in_level = itmMCert.getProperty("in_level", "").ToLower();

            string in_sno = itmMUser.getProperty("in_sno", "").ToUpper();
            string is_sides = itmMCert.getProperty("is_sides", "");

            string p01_property = "in_photo" + in_type + in_country + in_level + "_p01" + in_sno;
            string n01_property = "in_photo" + in_type + in_country + in_level + "_n01" + in_sno;

            string head_shot_file = itmMUser.getProperty("in_photo1", "");

            string p01_file = map.ContainsKey(p01_property) ? map[p01_property] : "";
            string n01_file = map.ContainsKey(n01_property) ? map[n01_property] : "";


            //是否需檢查
            string p01 = "in_photo" + in_type + in_country + in_level + "_p01";
            string n01 = "in_photo" + in_type + in_country + in_level + "_n01";

            //大頭照
            if (NeedCheckHeadShot(itmMUser, itmMCert))
            {
                if (head_shot_file == "")
                {
                    AddMUserNoPhoto(muser_map, in_sno, "in_photo");
                }
            }

            if (NeedCheckCert(itmMUser, itmMCert))
            {
                if (is_sides == "1")
                {
                    //有正反面
                    if (p01_file == "")
                    {
                        AddMUserNoPhoto(muser_map, in_sno, p01);
                    }
                    if (n01_file == "")
                    {
                        AddMUserNoPhoto(muser_map, in_sno, n01);
                    }
                }
                else
                {
                    if (p01_file == "")
                    {
                        AddMUserNoPhoto(muser_map, in_sno, p01);
                    }
                }
            }

        }

        private void AddMUserNoPhoto(Dictionary<string, List<string>> muser_map, string in_sno, string in_photo_type)
        {
            List<string> list = null;
            if (muser_map.ContainsKey(in_sno))
            {
                list = muser_map[in_sno];
            }
            else
            {
                list = new List<string>();
                muser_map.Add(in_sno, list);
            }
            list.Add(in_photo_type);
        }


        //是否需檢查大頭照
        private bool NeedCheckHeadShot(Item itmMUser, Item itmMCert)
        {
            bool need_check = false;

            //1: 選手、2: 選手與教練、3: 全部
            string in_limit = itmMCert.getProperty("in_limit", "");

            string in_l1 = itmMUser.getProperty("in_l1", "");
            string in_l2 = itmMUser.getProperty("in_l2", "");

            switch (in_limit)
            {
                case "1":
                    if (in_l1 != "隊職員")
                    {
                        need_check = true;
                    }
                    break;
                case "2":
                    if (in_l1 != "隊職員")
                    {
                        need_check = true;
                    }
                    else if (in_l2 == "教練")
                    {
                        need_check = true;
                    }
                    break;
                case "3":
                    need_check = true;
                    break;

                default:
                    break;
            }
            return need_check;
        }

        //是否需檢查證照
        private bool NeedCheckCert(Item itmMUser, Item itmMCert)
        {
            string in_property = itmMCert.getProperty("in_property", "");
            string in_operator = itmMCert.getProperty("in_operator", "");
            string in_value = itmMCert.getProperty("in_value", "");
            if (in_property == "") return true;

            string v = itmMUser.getProperty(in_property, "");
            if (v == "") return false;

            switch (in_property)
            {
                case "in_birth":
                    return CompareDtm(v, in_value, in_operator, bAdd8Hours: true);

                default:
                    return CompareStr(v, in_value, in_operator);
            }
        }

        //字串比較
        private bool CompareStr(string a, string b, string op)
        {
            switch (op)
            {
                case "eq": return a == b; //於
                case "ne": return a != b; //不等於
                case "contains": return CompareContains(a, b);//包含
                case "nc": return !(a.Contains(b)); //不包含
                case "nin": return !(b.Contains(a)); //不內含
                default: return false; //未設定 or 其他
            }
        }

        //字串比較(包含)
        private bool CompareContains(string a, string b)
        {
            if (b.Contains("|"))
            {
                var rows = b.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                if (rows == null || rows.Length == 0)
                {
                    return false;
                }

                var f = false;
                foreach (var row in rows)
                {
                    if (a.Contains(row.Trim()))
                    {
                        f = true;
                        break;
                    }
                }
                return f;

            }
            else
            {
                return a.Contains(b);
            }
        }

        //整數比較
        private bool CompareInt(string a, string b, string op)
        {
            var v1 = GetInt(a);
            var v2 = GetInt(b);

            switch (op)
            {
                case "eq": return v1 == v2; //於
                case "ne": return v1 != v2; //不等於
                case "gt": return v1 > v2;  //大於
                case "ge": return v1 >= v2; //大於等於
                case "lt": return v1 < v2; //小於
                case "le": return v1 <= v2; //小於等於
                case "contains": return (v1 & v2) == v2; //包含(位元算法)
                case "nc": return (v1 & v2) != v2; //不包含(位元算法)
                default: return false; //未設定
            }
        }

        //日期比較
        private bool CompareDtm(string a, string b, string op, bool bAdd8Hours = false)
        {
            var d1 = GetDateTime(a);
            var d2 = GetDateTime(b);

            if (bAdd8Hours)
            {
                d1 = d1.AddHours(8);
            }

            switch (op)
            {
                case "eq": return d1 == d2; //於
                case "ne": return d1 != d2; //不等於
                case "gt": return d1 > d2;  //大於
                case "ge": return d1 >= d2; //大於等於
                case "lt": return d1 < d2; //小於
                case "le": return d1 <= d2; //小於等於
                case "contains": return d1 == d2; //包含
                case "nc": return d1 != d2; //不包含
                default: return false; //未設定
            }
        }

        private int GetInt(string value, int def = 0)
        {
            int result = def;
            Int32.TryParse(value, out result);
            return result;
        }

        private DateTime GetDateTime(string value)
        {
            DateTime result = DateTime.MinValue;
            DateTime.TryParse(value, out result);
            return result;
        }

        private string getDateTimeValue(string value, string format = "yyyy-MM-dd", bool bAdd8Hour = true)
        {
            if (value == "")
            {
                return "";
            }


            DateTime dt = DateTime.Now;
            if (DateTime.TryParse(value, out dt))
            {
                if (bAdd8Hour)
                {
                    return dt.AddHours(8).ToString(format);
                }
                else
                {
                    return dt.ToString(format);
                }
            }
            else
            {
                return value;
            }
        }
    }
}