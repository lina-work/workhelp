﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.CTPC.Donate
{
    public class In_Meeting_Donate : Item
    {
        public In_Meeting_Donate(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 捐款
                日誌: 
                    - 2022-09-01: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]" + "In_Meeting_Donate";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                strIdentityId = inn.getUserAliases(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                in_name = itmR.getProperty("in_name", ""),
                in_sno = itmR.getProperty("in_sno", ""),
                in_gender = itmR.getProperty("in_gender", ""),
                in_birth = itmR.getProperty("in_birth", ""),
                in_email = itmR.getProperty("in_email", ""),
                in_tel = itmR.getProperty("in_tel", ""),
                in_expense = itmR.getProperty("in_expense", "0"),
            };

            //活動資訊
            MeetingInfo(cfg);

            //選項時程資訊
            MtFuncTimeInfo(cfg);

            //與會者資訊
            var applicant = NewMUser(cfg);

            //建立與會者
            var itmMUser = applicant.apply("add");

            var muid = itmMUser.getProperty("id", "");
            var item_number = "";

            if (muid != "")
            {
                //重算正取人數
                cfg.itmMeeting.apply("In_Update_MeetingOnMUEdit");

                //建立繳費單
                CreateMeetingPay(cfg, applicant, muid);

                //取得繳費單號
                item_number = GetPayNumber(cfg, muid);

            }

            //回傳繳費單號
            itmR.setProperty("item_number", item_number);

            return itmR;
        }

        //取得繳費單號
        private string GetPayNumber(TConfig cfg, string muid)
        {
            string item_number = "";
            
            string sql = "SELECT TOP 1 in_paynumber FROM IN_MEETING_USER WITH(NOLOCK) WHERE id = '"+muid+"'";
            Item itmResult = cfg.inn.applySQL(sql);

            if (!itmResult.isError() && itmResult.getResult() != "")
            {
                item_number = itmResult.getProperty("in_paynumber", "");
            }

            if (item_number == "")
            {
                throw new Exception("捐贈記錄建立失敗");
            }

            return item_number;
        }

        //建立繳費單
        private void CreateMeetingPay(TConfig cfg, Item applicant, string muid)
        {
            string org_array = "";            //所屬單位陣列
            string invoice_up_array = "";     //發票抬頭陣列
            string uniform_number_array = ""; //統一編號陣列

            org_array += ",";
            invoice_up_array += ",";
            uniform_number_array += ",";
            org_array += applicant.getProperty("in_current_org", "");

            Item itmMeetingPay = cfg.inn.newItem();
            itmMeetingPay.setType("In_Meeting_Pay");
            itmMeetingPay.setProperty("in_creator", cfg.in_name);
            itmMeetingPay.setProperty("in_creator_sno", cfg.in_sno);
            itmMeetingPay.setProperty("new_creator_email", cfg.in_email);
            itmMeetingPay.setProperty("in_group", applicant.getProperty("in_group", ""));
            itmMeetingPay.setProperty("in_current_org", applicant.getProperty("in_current_org", ""));
            itmMeetingPay.setProperty("current_orgs", "," + org_array);
            itmMeetingPay.setProperty("invoice_up", "," + invoice_up_array);
            itmMeetingPay.setProperty("uniform_numbers", "," + uniform_number_array);
            itmMeetingPay.setProperty("cla_meeting_id_group", cfg.meeting_id);
            itmMeetingPay.setProperty("meeting_id", cfg.meeting_id);
            itmMeetingPay.setProperty("muid", muid);
            itmMeetingPay.setProperty("in_expense", cfg.in_expense);
            Item itmPayResult = itmMeetingPay.apply("In_Payment_List_Add");
        }

        //活動資訊
        private void MeetingInfo(TConfig cfg)
        {
            string aml = "<AML><Item type='In_Meeting' action='get' id='" + cfg.meeting_id + "' select='*'/></AML>";
            cfg.itmMeeting = cfg.inn.applyAML(aml);

            string in_meeting_type = cfg.itmMeeting.getProperty("in_meeting_type", "");
            string in_isfull = cfg.itmMeeting.getProperty("in_isfull", "0");

            //檢查是否仍可報名
            if (in_isfull == "1")
            {
                throw new Exception("已額滿");
            }

            if (in_meeting_type != "donate")
            {
                throw new Exception("活動非捐款類型");
            }
        }

        //選項時程資訊
        private void MtFuncTimeInfo(TConfig cfg)
        {
            string aml = "<AML>"
                + "<Item type='In_Meeting_Functiontime' action='get' select='id,in_type,in_date_s,in_date_e'>"
                + "  <source_id>" + cfg.meeting_id + "</source_id>"
                + "  <in_action>sheet1</in_action>"
                + "</Item>"
                + "</AML>";

            cfg.itmMFuncTime = cfg.inn.applyAML(aml);

            //是否在開放報名期間
            DateTime now = DateTime.Now;
            DateTime dts = GetDtmVal(cfg.itmMFuncTime.getProperty("in_date_s", ""));
            DateTime dte = GetDtmVal(cfg.itmMFuncTime.getProperty("in_date_e", ""));

            if (now < dts || now > dte)
            {
                throw new Exception("未開放捐款");
            }
        }

        private Item NewMUser(TConfig cfg)
        {
            var applicant = cfg.inn.newItem("In_Meeting_User");

            applicant.setProperty("source_id", cfg.meeting_id);

            //姓名
            applicant.setProperty("in_name", cfg.in_name);
            
            //身分證號
            applicant.setProperty("in_sno", cfg.in_sno);
            
            //性別
            applicant.setProperty("in_gender", cfg.in_gender);

            //西元生日
            var local_birth = GetDtmVal(cfg.in_birth);
            if (local_birth != DateTime.MinValue)
            {
                applicant.getProperty("in_birth", local_birth.AddHours(8).ToString("yyyy-MM-dd HH:mm:ss"));
            }

            //電子信箱
            applicant.setProperty("in_email", cfg.in_email);

            //手機號碼
            applicant.setProperty("in_tel", cfg.in_tel);

            //協助報名者姓名
            applicant.setProperty("in_creator", cfg.in_name);

            //協助報名者身分號
            applicant.setProperty("in_creator_sno", cfg.in_sno);

            //所屬群組
            applicant.setProperty("in_group", cfg.in_sno + cfg.in_name); //F103277376測試者

            //所屬單位
            applicant.setProperty("in_current_org", cfg.in_name); //

            //預設身分均為選手
            applicant.setProperty("in_role", "player");

            //正取
            applicant.setProperty("in_note_state", "official");

            //組別
            applicant.setProperty("in_gameunit", "小額捐款");

            //組名
            applicant.setProperty("in_section_name", "小額捐款");

            //捐款金額
            applicant.setProperty("in_expense", cfg.in_expense);

            //競賽組別
            applicant.setProperty("in_l1", "小額捐款");

            //序號
            Item itmIndex = MaxIndexItem(cfg, applicant);
            applicant.setProperty("in_index", itmIndex.getProperty("new_idx", "00001"));

            //非實名制
            string in_mail = cfg.in_sno + "-" + DateTime.Now.ToString("MMddHHmmssfff");
            applicant.setProperty("in_mail", in_mail);

            //報名時間
            applicant.setProperty("in_regdate", System.DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss"));

            //繳費單號
            applicant.setProperty("in_paynumber", "");
            return applicant;
        }


        private string SnoDisplay(TConfig cfg, string in_sno)
        {
            if (in_sno.Length == 10)
            {
                //10碼(身分證處理)
                string in_sno_1 = in_sno.Substring(0, 2);
                string in_sno_2 = in_sno.Substring(6, 4);
                return in_sno_1 + "****" + in_sno_2;//A1****6789
            }
            else if (in_sno.Length == 18)
            {
                //18碼(身分證處理-大陸)
                string in_sno_1 = in_sno.Substring(0, 6);
                string in_sno_2 = in_sno.Substring(14, 3);
                return in_sno_1 + "********" + in_sno_2 + "X";//110102********888X
            }
            else
            {
                return in_sno;
            }
        }

        //取得該組當前最大序號
        private Item MaxIndexItem(TConfig cfg, Item applicant)
        {
            string sql = "";

            string in_l1 = applicant.getProperty("in_l1", "");
            string in_l2 = applicant.getProperty("in_l2", "");
            string in_l3 = applicant.getProperty("in_l3", "");
            string in_l4 = applicant.getProperty("in_l4", "");

            sql = "SELECT MAX(in_index) AS 'max_idx' FROM In_Meeting_User WITH(NOLOCK) WHERE source_id = '" + cfg.meeting_id + "'"
                + " AND in_l1 = N'" + in_l1 + "'";

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql_index: " + sql);

            Item itmResult = cfg.inn.applySQL(sql);

            if (itmResult.isError() || itmResult.getResult() == "")
            {
                itmResult = cfg.inn.newItem();
                itmResult.setProperty("new_idx", "00001");
            }
            else
            {
                var max_idx = itmResult.getProperty("max_idx", "1");
                var new_idx = GetIntVal(max_idx) + 1;
                applicant.setProperty("new_idx", new_idx.ToString("00000"));

            }

            return itmResult;
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }

            public string meeting_id { get; set; }
            public string in_name { get; set; }
            public string in_sno { get; set; }
            public string in_gender { get; set; }
            public string in_birth { get; set; }
            public string in_email { get; set; }
            public string in_tel { get; set; }
            public string in_expense { get; set; }


            public Item itmMeeting { get; set; }
            public Item itmMFuncTime { get; set; }

        }

        private DateTime GetDtmVal(string value)
        {
            DateTime result = DateTime.MinValue;
            DateTime.TryParse(value, out result);
            return result;
        }

        private int GetIntVal(string value, int def = 0)
        {
            int result = def;
            Int32.TryParse(value, out result);
            return result;
        }

    }
}
