﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;
using Aras.Server.Core;
using System.Diagnostics;
using Newtonsoft.Json.Linq;
using System.Web.UI;

namespace WorkHelp.ArasDesk.Methods.CTPC.Common
{
    public class in_meeting_excel_draw_class : Item
    {
        public in_meeting_excel_draw_class(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
    目的: 分級籤表
    日誌: 
        - 2024-03-04: 創建 (lina)
*/

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "in_meeting_excel_draw_class";

            var itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                meeting_id = itmR.getProperty("meeting_id", ""),
                scene = itmR.getProperty("scene", ""),
                CharSet = GetCharSet(),
            };

            cfg.meeting_id = "28F7B4792334468DBE7AF9BA412DDEEF";

            cfg.itmMeeting = GetMeetingItem(cfg);
            if (cfg.itmMeeting.isError() || cfg.itmMeeting.getResult() == "")
            {
                throw new Exception("查無活動資訊");
            }

            cfg.mt_title = cfg.itmMeeting.getProperty("in_title", "");
            cfg.mt_sport = cfg.itmMeeting.getProperty("in_sport_type", "");

            cfg.itmL1Items = GetLv1Items(cfg);
            cfg.itmL2Items = GetLv2Items(cfg);
            cfg.itmL3Items = GetLv3Items(cfg);

            AnalysisMeetingSurvey(cfg);

            if (cfg.itmLevels.isError() || cfg.itmLevels.getResult() == "")
            {
                throw new Exception("查無競賽項目資訊");
            }

            Export(cfg, AppendSheets, itmR);

            return itmR;
        }

        //加入 Sheets
        private void AppendSheets(TConfig cfg, Spire.Xls.Workbook workbook)
        {
            var count = cfg.itmLevels.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var itmLevel = cfg.itmLevels.getItemByIndex(i);
                var in_value = itmLevel.getProperty("in_value", "");
                var in_filter = itmLevel.getProperty("in_filter", "");

                var report = new TReport
                {
                    is_solo = CheckIsSolo(cfg, itmLevel)
                };

                if (cfg.is_lv2)
                {
                    report.name = in_filter + "-" + in_value;
                    report.in_l1 = in_filter;
                    report.in_l2 = in_value;
                }
                else
                {
                    report.name = in_value;
                    report.in_l1 = in_value;
                }

                report.itmMUsers = GetMUserItems(cfg, report.in_l1, report.in_l2);
                report.sections = MapSection(cfg, report.itmMUsers, report.is_solo);

                if (report.is_solo)
                {
                    AppendSoloSheet(cfg, workbook, report);
                }
                else
                {
                    AppendTeamSheet1(cfg, workbook, report);
                    AppendTeamSheet2(cfg, workbook, report);
                }

            }
        }

        //多人類型籤表
        private void AppendTeamSheet1(TConfig cfg, Spire.Xls.Workbook workbook, TReport report)
        {
            var sections = report.sections;

            Spire.Xls.Worksheet sheet = workbook.CreateEmptySheet();
            sheet.Name = report.name + "籤表";

            var fields = new List<TField>();
            fields.Add(new TField { title = "序號", property = "no", css = TCss.Center, width = 5 });
            fields.Add(new TField { title = "流水號", property = "in_index", css = TCss.Center, width = 6 });
            fields.Add(new TField { title = "目前單位", property = "in_current_org", css = TCss.None, width = 30 });
            fields.Add(new TField { title = "隊別", property = "in_team", css = TCss.Center, width = 5 });
            fields.Add(new TField { title = "組名", property = "section", css = TCss.None, width = 25 });
            fields.Add(new TField { title = "籤號", property = "in_sign_no", css = TCss.Center, width = 6 });

            MapCharSetAndIndex(cfg, fields);

            int mnRow = 3;
            int wsRow = 3;

            //資料容器
            Item item = cfg.inn.newItem();

            foreach (var sect in sections)
            {
                //標題列
                SetHeadRow(sheet, wsRow, fields);
                wsRow++;

                int no = 1;
                foreach (var team in sect.teams)
                {
                    var source = team.list[0].value;
                    item.setProperty("no", no.ToString());
                    item.setProperty("in_sign_no", source.getProperty("in_sign_no", ""));
                    item.setProperty("in_section_no", source.getProperty("in_section_no", ""));
                    item.setProperty("in_name", source.getProperty("in_name", ""));
                    item.setProperty("in_sno", source.getProperty("in_sno", ""));
                    item.setProperty("in_gender", source.getProperty("in_gender", ""));
                    item.setProperty("in_birth", source.getProperty("in_birth", ""));
                    item.setProperty("section", sect.name);
                    item.setProperty("in_current_org", source.getProperty("in_current_org", ""));
                    item.setProperty("in_team", source.getProperty("in_team", ""));
                    item.setProperty("in_index", source.getProperty("in_index", ""));
                    item.setProperty("in_seeds", source.getProperty("in_seeds", ""));

                    SetItemCell(cfg, sheet, wsRow, item, fields);
                    wsRow++;
                    no++;
                }
            }

            //設定格線
            SetRangeBorder(sheet, fields, mnRow, wsRow);

            //設定欄寬
            SetColumnWidth(sheet, fields);
        }

        //多人類型籤表
        private void AppendTeamSheet2(TConfig cfg, Spire.Xls.Workbook workbook, TReport report)
        {
            var sections = report.sections;

            Spire.Xls.Worksheet sheet = workbook.CreateEmptySheet();
            sheet.Name = report.name + "明細";

            var fields = new List<TField>();
            fields.Add(new TField { title = "序號", property = "no", css = TCss.Center, width = 5, is_merge = true });
            fields.Add(new TField { title = "流水號", property = "in_index", css = TCss.Center, width = 6, is_merge = true });
            fields.Add(new TField { title = "目前單位", property = "in_current_org", css = TCss.None, width = 30, is_merge = true });
            fields.Add(new TField { title = "隊別", property = "in_team", css = TCss.Center, width = 5, is_merge = true });
            fields.Add(new TField { title = "姓名", property = "in_name", css = TCss.None, width = 15, is_merge = false });
            fields.Add(new TField { title = "身分證字號", property = "in_sno", css = TCss.Center, width = 11, is_merge = false });
            fields.Add(new TField { title = "性別", property = "in_gender", css = TCss.Center, width = 5, is_merge = false });
            fields.Add(new TField { title = "出生年月日", property = "in_birth", css = TCss.Center, width = 12, is_merge = false });
            fields.Add(new TField { title = "組名", property = "section", css = TCss.None, width = 25, is_merge = true });
            //fields.Add(new TField { title = "隊別", property = "in_team", css = TCss.Center, width = 5, is_merge = true });
            fields.Add(new TField { title = "籤號", property = "in_sign_no", css = TCss.Center, width = 6, is_merge = true });
            fields.Add(new TField { title = "種子籤", property = "in_seeds", css = TCss.Center, width = 6, is_merge = true });

            MapCharSetAndIndex(cfg, fields);

            int mnRow = 3;
            int wsRow = 3;

            //資料容器
            Item item = cfg.inn.newItem();

            foreach (var sect in sections)
            {
                //標題列
                SetHeadRow(sheet, wsRow, fields);
                wsRow++;

                int no = 1;
                foreach (var team in sect.teams)
                {
                    foreach (var player in team.list)
                    {
                        var source = player.value;
                        item.setProperty("no", no.ToString());
                        item.setProperty("in_sign_no", source.getProperty("in_sign_no", ""));
                        item.setProperty("in_section_no", source.getProperty("in_section_no", ""));
                        item.setProperty("in_name", source.getProperty("in_name", ""));
                        item.setProperty("in_sno", source.getProperty("in_sno", ""));
                        item.setProperty("in_gender", source.getProperty("in_gender", ""));
                        item.setProperty("in_birth", source.getProperty("in_birth", ""));
                        item.setProperty("section", sect.name);
                        item.setProperty("in_current_org", source.getProperty("in_current_org", ""));
                        item.setProperty("in_team", source.getProperty("in_team", ""));
                        item.setProperty("in_index", source.getProperty("in_index", ""));
                        item.setProperty("in_seeds", source.getProperty("in_seeds", ""));

                        SetItemCell(cfg, sheet, wsRow, item, fields);
                        wsRow++;
                    }

                    int rs = wsRow - team.list.Count;
                    var re = wsRow - 1;

                    //合併處理
                    foreach (var field in fields)
                    {
                        if (!field.is_merge) continue;

                        string cr1 = field.cs + rs;
                        string cr2 = field.cs + re;
                        if (cr1 == cr2) continue;

                        var mr = sheet.Range[cr1 + ":" + cr2];
                        mr.Merge();
                        mr.Style.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;
                    }

                    no++;
                }
            }

            //設定格線
            SetRangeBorder(sheet, fields, mnRow, wsRow);

            //設定欄寬
            SetColumnWidth(sheet, fields);
        }

        //個人類型籤表
        private void AppendSoloSheet(TConfig cfg, Spire.Xls.Workbook workbook, TReport report)
        {
            var sections = report.sections;

            Spire.Xls.Worksheet sheet = workbook.CreateEmptySheet();
            sheet.Name = report.name + "籤表";

            var fields = new List<TField>();
            fields.Add(new TField { title = "序號", property = "no", css = TCss.Center, width = 5 });
            fields.Add(new TField { title = "姓名", property = "in_name", css = TCss.None, width = 15 });
            fields.Add(new TField { title = "身分證字號", property = "in_sno", css = TCss.Center, width = 11 });
            fields.Add(new TField { title = "性別", property = "in_gender", css = TCss.Center, width = 5 });
            fields.Add(new TField { title = "出生年月日", property = "in_birth", css = TCss.Center, width = 12 });
            fields.Add(new TField { title = "組名", property = "section", css = TCss.None, width = 25 });
            fields.Add(new TField { title = "目前單位", property = "in_current_org", css = TCss.None, width = 30 });
            fields.Add(new TField { title = "隊別", property = "in_team", css = TCss.Center, width = 5 });
            fields.Add(new TField { title = "籤號", property = "in_sign_no", css = TCss.Center, width = 6 });
            fields.Add(new TField { title = "種子籤", property = "in_seeds", css = TCss.Center, width = 6 });

            MapCharSetAndIndex(cfg, fields);

            int mnRow = 3;
            int wsRow = 3;

            //資料容器
            Item item = cfg.inn.newItem();

            foreach (var sect in sections)
            {
                //標題列
                SetHeadRow(sheet, wsRow, fields);
                wsRow++;

                int no = 1;
                foreach (var source in sect.items)
                {
                    item.setProperty("no", no.ToString());
                    item.setProperty("in_sign_no", source.getProperty("in_sign_no", ""));
                    item.setProperty("in_section_no", source.getProperty("in_section_no", ""));
                    item.setProperty("in_name", source.getProperty("in_name", ""));
                    item.setProperty("in_sno", source.getProperty("in_sno", ""));
                    item.setProperty("in_gender", source.getProperty("in_gender", ""));
                    item.setProperty("in_birth", source.getProperty("in_birth", ""));
                    item.setProperty("section", sect.name);
                    item.setProperty("in_current_org", source.getProperty("in_current_org", ""));
                    item.setProperty("in_team", source.getProperty("in_team", ""));
                    item.setProperty("in_index", source.getProperty("in_index", ""));
                    item.setProperty("in_seeds", source.getProperty("in_seeds", ""));

                    SetItemCell(cfg, sheet, wsRow, item, fields);

                    no++;
                    wsRow++;
                }
            }

            //設定格線
            SetRangeBorder(sheet, fields, mnRow, wsRow);

            //設定欄寬
            SetColumnWidth(sheet, fields);
        }

        //設定格線
        private void SetRangeBorder(Spire.Xls.Worksheet sheet, List<TField> fields, int mnRow, int wsRow)
        {
            try
            {
                var tbl_ps = fields.First().cs + mnRow;
                var tbl_pe = fields.Last().cs + (wsRow - 1);
                var pos = tbl_ps + ":" + tbl_pe;
                var range = sheet.Range[pos];
                range.BorderInside(Spire.Xls.LineStyleType.Thin, System.Drawing.Color.Black);
                range.BorderAround(Spire.Xls.LineStyleType.Thin, System.Drawing.Color.Black);
            }
            catch (Exception ex)
            {

            }
        }

        //設定欄寬
        private void SetColumnWidth(Spire.Xls.Worksheet sheet, List<TField> fields)
        {
            foreach (var field in fields)
            {
                sheet.Columns[field.ci - 1].ColumnWidth = field.width;
            }
        }

        //設定資料列
        private void SetItemCell(TConfig cfg, Spire.Xls.Worksheet sheet, int wsRow, Item item, List<TField> fields)
        {
            foreach (var field in fields)
            {
                var cr = field.cs + wsRow;
                var va = "";

                if (field.getValue != null)
                {
                    va = field.getValue(cfg, field, item);
                }
                else if (field.property != "")
                {
                    va = item.getProperty(field.property, "");
                }

                SetCell(sheet, cr, va, field.css);
            }
        }

        //設定資料格
        private void SetCell(Spire.Xls.Worksheet sheet, string cr, string value, TCss css)
        {
            var range = sheet.Range[cr];
            range.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;

            switch (css)
            {
                case TCss.None:
                    range.Text = value;
                    range.IsWrapText = true;
                    break;
                case TCss.Center:
                    range.Text = value;
                    range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                    range.IsWrapText = true;
                    break;
                case TCss.Text:
                    range.Text = "'" + value;
                    range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                    range.IsWrapText = true;
                    break;
                case TCss.Number:
                    range.NumberValue = GetIntVal(value);
                    range.NumberFormat = "0";
                    range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Right;
                    break;
                case TCss.Money:
                    range.NumberValue = GetIntVal(value);
                    range.NumberFormat = "$ #,##0";
                    break;
            }
        }

        private void SetHeadRow(Spire.Xls.Worksheet sheet, int wsRow, List<TField> fields)
        {
            foreach (var field in fields)
            {
                string cr = field.cs + wsRow;
                SetHead(sheet, cr, field.title);
            }
        }

        //設定標題列
        private void SetHead(Spire.Xls.Worksheet sheet, string cr, string value)
        {
            var range = sheet.Range[cr];
            range.Text = value;
            range.Style.Font.IsBold = true;
            range.Style.Font.Color = System.Drawing.Color.White;
            range.Style.Color = System.Drawing.Color.FromArgb(41, 92, 144);//#295C90
            range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
        }

        private void MapCharSetAndIndex(TConfig cfg, List<TField> fields)
        {
            for (int i = 0; i < fields.Count; i++)
            {
                var field = fields[i];
                field.ci = i + 2;
                field.cs = cfg.CharSet[field.ci];
            }
        }

        //匯出
        private void Export(TConfig cfg, Action<TConfig, Spire.Xls.Workbook> appendSheets, Item itmReturn)
        {
            var exp = ExportInfo(cfg, "");

            //試算表
            var workbook = new Spire.Xls.Workbook();

            //加入 Sheets
            appendSheets(cfg, workbook);

            if (workbook.Worksheets.Count > 1) workbook.Worksheets[0].Remove();
            if (workbook.Worksheets.Count > 1) workbook.Worksheets[0].Remove();
            if (workbook.Worksheets.Count > 1) workbook.Worksheets[0].Remove();

            string ext_name = ".xlsx";
            string xls_name = cfg.mt_title + "_籤表_" + DateTime.Now.ToString("MMdd_HHmmss");
            string xls_file = exp.export_Path + "\\" + xls_name + ext_name;
            string xls_url = xls_name + ext_name;

            workbook.SaveToFile(xls_file, Spire.Xls.ExcelVersion.Version2013);

            itmReturn.setProperty("xls_name", xls_url);
        }

        private List<TSection> MapSection(TConfig cfg, Item items, bool isSolo)
        {
            var sects = new List<TSection>();
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var obj = NewPlayer(cfg, item);

                var sect = sects.Find(x => x.key == obj.sect_key);
                if (sect == null)
                {
                    sect = new TSection
                    {
                        key = obj.sect_key,
                        name = obj.sect_name,
                        items = new List<Item>(),
                        teams = new List<TTeam>()
                    };
                    sects.Add(sect);
                }

                if (isSolo)
                {
                    sect.items.Add(item);
                }
                else
                {
                    MatchTeam(cfg, sect, obj);
                }
            }

            return sects;
        }

        private TPlayer NewPlayer(TConfig cfg, Item item)
        {
            var obj = new TPlayer
            {
                in_l1 = item.getProperty("in_l1", "").Trim(),
                in_l2 = item.getProperty("in_l2", "").Trim(),
                in_l3 = item.getProperty("in_l3", "").Trim(),
                in_l4 = item.getProperty("in_l4", "").Trim(),
                in_index = item.getProperty("in_index", "").Trim(),
                in_creator_sno = item.getProperty("in_creator_sno", "").Trim(),
                in_current_org = item.getProperty("in_current_org", "").Trim(),
                value = item
            };

            var fix_l4 = obj.in_l4 == "" ? "" : "-" + obj.in_l4;
            obj.sect_key = obj.in_l1 + "-" + obj.in_l2 + "-" + obj.in_l3 + fix_l4;
            obj.sect_name = GetSectName(obj.in_l1, obj.in_l2, obj.in_l3) + fix_l4;
            obj.team_key = obj.in_l1 + "-" + obj.in_l2 + "-" + obj.in_l3 + fix_l4 + obj.in_creator_sno + obj.in_index;

            if (cfg.is_lv2)
            {
                obj.sect_name = obj.in_l2 + "-" + obj.in_l3 + fix_l4;
            }

            return obj;
        }

        private string GetSectName(string in_l1, string in_l2, string in_l3)
        {
            var b1 = in_l1 == in_l2;
            var b2 = in_l1 == in_l3;
            var b3 = in_l2 == in_l3;

            if (b1 && b2 && b3) return in_l1;
            if (b1) return in_l2 + "-" + in_l3;
            if (b2) return in_l2 + "-" + in_l3;
            if (b3) return in_l1 + "-" + in_l2;
            return in_l1 + "-" + in_l2 + "-" + in_l3;
        }


        private void MatchTeam(TConfig cfg, TSection sect, TPlayer obj)
        {
            var team = sect.teams.Find(x => x.key == obj.team_key);
            if (team == null)
            {
                team = new TTeam
                {
                    key = obj.team_key,
                    list = new List<TPlayer>()
                };
                sect.teams.Add(team);
            }
            team.list.Add(obj);
        }

        private Item GetMUserItems(TConfig cfg, string in_l1, string in_l2)
        {
            if (cfg.has_sport_classification)
            {
                //田徑分級
                return GetMUserItemsS(cfg, in_l1, in_l2);
            }
            else if (cfg.has_lv3)
            {
                //標準三階
                return GetMUserItemsA(cfg, in_l1, in_l2);
            }
            else if (cfg.has_lv2)
            {
                //標準二階
                return GetMUserItemsB(cfg, in_l1, in_l2);
            }
            else
            {
                return GetMUserItemsC(cfg, in_l1, in_l2);
            }
        }

        //標準三階
        private Item GetMUserItemsA(TConfig cfg, string in_l1, string in_l2)
        {
            var sql = @"
                SELECT
	                t1.id
	                , t1.in_name
	                , t1.in_sno
	                , t1.in_gender
	                , CONVERT(VARCHAR, DATEADD(hour, 8, t1.in_birth), 111) AS 'in_birth'
	                , t1.in_current_org
	                , t1.in_l1
	                , t1.in_l2
	                , t1.in_l3
	                , t1.in_index
	                , t1.in_team
	                , t1.in_sport_classification AS 'in_l4'
	                , t1.in_creator
	                , t1.in_creator_sno
                FROM
	                IN_MEETING_USER t1 WITH(NOLOCK)
                LEFT OUTER JOIN
	                VU_MEETING_SVY_L2 t12
	                ON t12.source_id = t1.source_id
	                AND t12.in_filter = t1.in_l1
	                AND t12.in_value = t1.in_l2
                LEFT OUTER JOIN
	                VU_MEETING_SVY_L3 t13
	                ON t13.source_id = t1.source_id
	                AND t13.in_grand_filter = t1.in_l1
	                AND t13.in_filter = t1.in_l2
	                AND t13.in_value = t1.in_l3
                WHERE
	                t1.source_id = '{#meeting_id}'
	                AND t1.in_l1 = N'{#in_l1}'
	                {#l2_cond}
                ORDER BY
	                t12.sort_order
	                , t13.sort_order
	                , t1.in_sport_classification
	                , t1.in_current_org
	                , t1.in_team
	                , t1.in_index
	                , t1.in_gender
	                , t1.in_name
                ";

            var l2_cond = cfg.is_lv2 ? "AND t1.in_l2 = N'" + in_l2 + "'" : "";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_l1}", in_l1)
                .Replace("{#l2_cond}", l2_cond);

            return cfg.inn.applySQL(sql);
        }

        //標準兩階
        private Item GetMUserItemsB(TConfig cfg, string in_l1, string in_l2)
        {
            var sql = @"
                SELECT
	                t1.id
	                , t1.in_name
	                , t1.in_sno
	                , t1.in_gender
	                , CONVERT(VARCHAR, DATEADD(hour, 8, t1.in_birth), 111) AS 'in_birth'
	                , t1.in_current_org
	                , t1.in_l1
	                , t1.in_l2
	                , t1.in_l3
	                , t1.in_index
	                , t1.in_team
	                , t1.in_sport_classification AS 'in_l4'
	                , t1.in_creator
	                , t1.in_creator_sno
                FROM
	                IN_MEETING_USER t1 WITH(NOLOCK)
                LEFT OUTER JOIN
	                VU_MEETING_SVY_L2 t12
	                ON t12.source_id = t1.source_id
	                AND t12.in_filter = t1.in_l1
	                AND t12.in_value = t1.in_l2
                WHERE
	                t1.source_id = '{#meeting_id}'
	                AND t1.in_l1 = N'{#in_l1}'
	                {#l2_cond}
                ORDER BY
	                t12.sort_order
	                , t1.in_sport_classification
	                , t1.in_current_org
	                , t1.in_team
	                , t1.in_index
	                , t1.in_gender
	                , t1.in_name
                ";

            var l2_cond = cfg.is_lv2 ? "AND t1.in_l2 = N'" + in_l2 + "'" : "";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_l1}", in_l1)
                .Replace("{#l2_cond}", l2_cond);

            return cfg.inn.applySQL(sql);
        }

        //只有一階
        private Item GetMUserItemsC(TConfig cfg, string in_l1, string in_l2)
        {
            var sql = @"
                SELECT
	                t1.id
	                , t1.in_name
	                , t1.in_sno
	                , t1.in_gender
	                , CONVERT(VARCHAR, DATEADD(hour, 8, t1.in_birth), 111) AS 'in_birth'
	                , t1.in_current_org
	                , t1.in_l1
	                , t1.in_l2
	                , t1.in_l3
	                , t1.in_index
	                , t1.in_team
	                , t1.in_sport_classification AS 'in_l4'
	                , t1.in_creator
	                , t1.in_creator_sno
                FROM
	                IN_MEETING_USER t1 WITH(NOLOCK)
                WHERE
	                t1.source_id = '{#meeting_id}'
	                AND t1.in_l1 = N'{#in_l1}'
	                {#l2_cond}
                ORDER BY
	                t1.in_sport_classification
	                , t1.in_current_org
	                , t1.in_team
	                , t1.in_index
	                , t1.in_gender
	                , t1.in_name
                ";

            var l2_cond = cfg.is_lv2 ? "AND t1.in_l2 = N'" + in_l2 + "'" : "";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_l1}", in_l1)
                .Replace("{#l2_cond}", l2_cond);

            return cfg.inn.applySQL(sql);
        }

        //田徑分級
        private Item GetMUserItemsS(TConfig cfg, string in_l1, string in_l2)
        {
            var sql = @"
                SELECT
	                t1.id
	                , t1.in_name
	                , t1.in_sno
	                , t1.in_gender
	                , CONVERT(VARCHAR, DATEADD(hour, 8, t1.in_birth), 111) AS 'in_birth'
	                , t1.in_current_org
	                , t1.in_l1
	                , t1.in_l2
	                , t1.in_l3
	                , t1.in_index
	                , t1.in_team
	                , t1.in_sport_classification AS 'in_l4'
	                , t1.in_creator
	                , t1.in_creator_sno
                FROM
	                IN_MEETING_USER t1 WITH(NOLOCK)
                LEFT OUTER JOIN
	                VU_MEETING_SVY_L2 t2
	                ON t2.source_id = t1.source_id
	                AND t2.in_filter = t1.in_l1
	                AND t2.in_value = t1.in_l2
                WHERE
	                t1.source_id = '{#meeting_id}'
	                AND t1.in_l1 = N'{#in_l1}'
	                {#l2_cond}
                ORDER BY
                    t2.sort_order
	                , t1.in_l3
	                , t1.in_sport_classification
	                , t1.in_current_org
	                , t1.in_team
	                , t1.in_index
	                , t1.in_gender
	                , t1.in_name
            ";

            var l2_cond = cfg.is_lv2 ? "AND t1.in_l2 = N'" + in_l2 + "'" : "";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_l1}", in_l1)
                .Replace("{#l2_cond}", l2_cond);

            return cfg.inn.applySQL(sql);
        }

        private void AnalysisMeetingSurvey(TConfig cfg)
        {
            var sql = @"
                SELECT
	                t2.id
					, t2.in_questions
					, t2.in_property
					, t2.in_is_limit
                FROM
	                IN_MEETING_SURVEYS t1 WITH(NOLOCK)
                INNER JOIN
	                IN_SURVEY t2 WITH(NOLOCK)
	                ON t2.id = t1.related_id
                WHERE
	                t1.source_id = '{#meeting_id}'
	                AND t2.in_property IN ('in_l1', 'in_l2', 'in_l3', 'in_sport_classification')
				ORDER BY
					t1.sort_order
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var in_property = item.getProperty("in_property", "");
                var in_is_limit = item.getProperty("in_is_limit", "");

                switch (in_property)
                {
                    case "in_l1":
                        cfg.has_lv1 = true;
                        cfg.limit_on_lv1 = in_is_limit == "1";
                        break;

                    case "in_l2":
                        cfg.has_lv2 = true;
                        cfg.limit_on_lv2 = in_is_limit == "1";
                        break;

                    case "in_l3":
                        cfg.has_lv3 = true;
                        cfg.limit_on_lv3 = in_is_limit == "1";
                        break;

                    case "in_sport_classification":
                        cfg.has_sport_classification = true;
                        break;
                }

            }

            if (cfg.has_sport_classification)
            {
                cfg.is_lv2 = true;
                cfg.itmLevels = cfg.itmL2Items;
            }
            else if (cfg.limit_on_lv2)
            {
                cfg.is_lv2 = true;
                cfg.itmLevels = cfg.itmL2Items;
            }
            else
            {
                cfg.is_lv2 = false;
                cfg.itmLevels = cfg.itmL1Items;
                cfg.limit_on_lv1 = true;
            }
        }

        private Item GetMeetingItem(TConfig cfg)
        {
            var sql = "SELECT id, in_title, LOWER(in_sport) AS 'in_sport_type' FROM IN_MEETING WITH(NOLOCK)"
                + " WHERE id = '{#meeting_id}'";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            return cfg.inn.applySQL(sql);
        }

        private Item GetLv1Items(TConfig cfg)
        {
            var sql = "SELECT * FROM VU_MEETING_SVY_L1"
                + " WHERE source_id = '{#meeting_id}'"
                + " ORDER BY sort_order";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            return cfg.inn.applySQL(sql);
        }

        private Item GetLv2Items(TConfig cfg)
        {
            var sql = "SELECT * FROM VU_MEETING_SVY_L2"
                + " WHERE source_id = '{#meeting_id}'"
                + " ORDER BY sort_order";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            return cfg.inn.applySQL(sql);
        }

        private Item GetLv3Items(TConfig cfg)
        {
            var sql = "SELECT * FROM VU_MEETING_SVY_L3"
                + " WHERE source_id = '{#meeting_id}'"
                + " ORDER BY sort_order";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            return cfg.inn.applySQL(sql);
        }

        private TExport ExportInfo(TConfig cfg, string in_name)
        {
            Item itmXls = cfg.inn.applyMethod("In_Meeting_Excel_Path", "<in_name>" + in_name + "</in_name>");
            string Export_Path = itmXls.getProperty("export_path", "");
            string Template_Path = itmXls.getProperty("template_path", "");

            TExport result = new TExport
            {
                template_Path = Template_Path,
                export_Path = Export_Path,
            };

            return result;
        }

        private bool CheckIsSolo(TConfig cfg, Item itmLevel)
        {
            //棒球一定是團體
            if (cfg.mt_sport == "baseball") return false;
            //龍舟一定是團體
            if (cfg.mt_sport == "dragonboat") return false;

            var isSolo = false;
            var in_extend = itmLevel.getProperty("in_extend", "");

            //未設定報名人數上下限，視為個人
            if (in_extend == "")
            {
                isSolo = true;
            }
            else if (!in_extend.Contains("in_limit"))
            {
                isSolo = true;
            }
            else if (in_extend.Contains("in_limit:1~1"))
            {
                isSolo = true;
            }

            return isSolo;
        }

        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }

            public string meeting_id { get; set; }
            public string scene { get; set; }

            public Item itmMeeting { get; set; }
            public Item itmLevels { get; set; }
            public Item itmL1Items { get; set; }
            public Item itmL2Items { get; set; }
            public Item itmL3Items { get; set; }

            public string mt_title { get; set; }
            public string mt_sport { get; set; }

            public bool is_lv2 { get; set; }

            public bool has_lv1 { get; set; }
            public bool has_lv2 { get; set; }
            public bool has_lv3 { get; set; }
            public bool has_sport_classification { get; set; }

            public bool limit_on_lv1 { get; set; }
            public bool limit_on_lv2 { get; set; }
            public bool limit_on_lv3 { get; set; }

            public string[] CharSet { get; set; }
        }

        private class TLevel
        {
            public string in_value { get; set; }
            public string in_extend { get; set; }
        }


        private class TReport
        {
            public string name { get; set; }
            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }
            public bool is_solo { get; set; }
            public Item itmMUsers { get; set; }
            public List<TSection> sections { get; set; }
        }

        private class TSection
        {
            public string key { get; set; }
            public string name { get; set; }
            public List<Item> items { get; set; }
            public List<TTeam> teams { get; set; }
        }

        private class TTeam
        {
            public string key { get; set; }
            public List<TPlayer> list { get; set; }
        }

        private class TPlayer
        {
            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }
            public string in_l4 { get; set; }
            public string in_index { get; set; }
            public string in_creator_sno { get; set; }
            public string in_current_org { get; set; }
            public string sect_key { get; set; }
            public string sect_name { get; set; }
            public string team_key { get; set; }
            public Item value { get; set; }
        }

        private class TExport
        {
            public string template_Path { get; set; }
            public string export_Path { get; set; }
        }

        private class TField
        {
            public string title { get; set; }
            public string property { get; set; }
            public TCss css { get; set; }
            public int ci { get; set; }
            public string cs { get; set; }
            public int width { get; set; }
            public Func<TConfig, TField, Item, string> getValue { get; set; }

            public bool is_merge { get; set; }
        }

        private enum TCss
        {
            None = 0,
            Center = 10,
            Text = 100,
            Number = 200,
            Money = 300,
            Day = 400,
        }

        private string[] GetCharSet()
        {
            return new string[] { "", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
        }

        private int GetIntVal(string value, int def = 0)
        {
            int result = def;
            int.TryParse(value, out result);
            return result;
        }
    }
}