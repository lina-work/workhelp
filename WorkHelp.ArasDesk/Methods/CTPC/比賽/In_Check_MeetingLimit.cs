﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.CTPC.Common
{
    public class In_Check_MeetingLimit : Item
    {
        public In_Check_MeetingLimit(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 賽事報名人數限制
                日誌: 
                    - 2023-01-09: 創建 (lina)
            */

            System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]" + "In_Check_MeetingLimit";

            Item itmR = this;
            CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);
           
            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                strIdentityId = inn.getUserAliases(),

                meeting_id = itmR.getProperty("source_id", ""),
                in_creator_sno = itmR.getProperty("in_creator_sno", ""),
                in_current_org = itmR.getProperty("in_current_org", ""),
                in_l1 = itmR.getProperty("in_l1", ""),
                in_l2 = itmR.getProperty("in_l2", ""),
                in_l3 = itmR.getProperty("in_l3", ""),
                in_index = itmR.getProperty("in_index", ""),
                in_sno = itmR.getProperty("in_sno", ""),
            };


            Item itmRules = GetMeetingRules(cfg);
            if (itmRules.isError() || itmRules.getResult() == "")
            {
                return this;
            }

            int count = itmRules.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item itmRule = itmRules.getItemByIndex(i);
                TRule rule = MapRule(cfg, itmRule);

                if (rule.limit <= 0)
                {
                    throw new Exception("不開放報名");
                }

                CheckMeetingRules(cfg, rule);
            }
            
            return inn.newItem();
        }

        private void CheckMeetingRules(TConfig cfg, TRule rule)
        {
            var items = GetMeetingUsers(cfg, rule);
            var count = items.getItemCount();
            var teams = new List<string>();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string _in_current_org = item.getProperty("in_current_org", "");
                string _in_l1 = item.getProperty("in_l1", "");
                string _in_l2 = item.getProperty("in_l2", "");
                string _in_l3 = item.getProperty("in_l3", "");
                string _in_index = item.getProperty("in_index", "");

                string _sect_key = string.Join("", new List<string>
                {
                    _in_current_org,
                    _in_l1,
                    _in_l2,
                    _in_l3,
                });

                string _team_key = string.Join("", new List<string>
                {
                    _in_current_org,
                    _in_l1,
                    _in_l2,
                    _in_l3,
                    _in_index,
                });

                teams.Add(_team_key);
            }

            if (teams.Count > rule.limit)
            {
                var message = "";

                switch (rule.in_type)
                {
                    case "in_creator_sno":
                        message = "限制一位協助報名者最多只能報名 " + rule.in_limit + " 種項目";
                        break;

                    case "in_current_org":
                        message = "限制一個單位最多只能報名 " + rule.in_limit + " 種項目";
                        break;

                    case "in_sno":
                        message = "限制一位選手最多只能報名 " + rule.in_limit + " 種項目";
                        break;

                    default:
                        message = "未設定";
                        break;
                }

                throw new Exception(message);
            }

        }

        private TRule MapRule(TConfig cfg, Item itmRule)
        {
            var result = new TRule
            {
                in_type = itmRule.getProperty("in_type", ""),
                in_property = itmRule.getProperty("in_property", ""),
                in_operator = itmRule.getProperty("in_operator", ""),
                in_value = itmRule.getProperty("in_value", ""),
                in_limit = itmRule.getProperty("in_limit", "0"),
                in_type_label = itmRule.getProperty("in_type_label", ""),
            };

            result.limit = GetInt(result.in_limit);

            return result;
        }

        private class TRule
        {
            public string in_type { get; set; }
            public string in_property { get; set; }
            public string in_operator { get; set; }
            public string in_value { get; set; }
            public string in_limit { get; set; }
            public string in_type_label { get; set; }

            public int limit { get; set; }
        }
        
        private Item GetMeetingUsers(TConfig cfg, TRule rule)
        {
            List<string> conds = new List<string>();

            conds.Add("[source_id] = '" + cfg.meeting_id + "'");
            
            switch (rule.in_type)
            {
                case "in_creator_sno":
                    conds.Add("[in_creator_sno] = N'" + cfg.in_creator_sno + "'");
                    break;

                case "in_current_org":
                    conds.Add("[in_creator_sno] = N'" + cfg.in_creator_sno + "'");
                    conds.Add("[in_current_org] = N'" + cfg.in_current_org + "'");
                    break;

                case "in_sno":
                    //conds.Add("[in_creator_sno] = N'" + cfg.in_creator_sno + "'");
                    conds.Add("[in_sno] = N'" + cfg.in_sno + "'");
                    break;
            }

            if (rule.in_property != "")
            {
                conds.Add("[" + rule.in_property + "] = N'" + rule.in_value + "'");
            }
            else
            {
                conds.Add("[in_l1] = N'" + cfg.in_l1 + "'");
                conds.Add("[in_l2] = N'" + cfg.in_l2 + "'");
                conds.Add("[in_l3] = N'" + cfg.in_l3 + "'");
            }

            string sql = @"
                SELECT DISTINCT
                    in_current_org
				    , in_short_org
                    , in_l1
                    , in_l2
                    , in_l3
                    , in_index
                    , in_creator_sno
                FROM
                    IN_MEETING_USER t1 WITH(NOLOCK)
                WHERE
                    {#conds}
                ORDER BY
                    in_current_org
                    , in_short_org
                    , in_l1
                    , in_l2
                    , in_l3
                    , in_index
            ";

            sql = sql.Replace("{#conds}", string.Join(" AND ", conds));

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        private Item GetMeetingRules(TConfig cfg)
        {
            string sql = @"
                SELECT 
	                t1.*
					, t2.label AS 'in_type_label'
                FROM 
	                IN_MEETING_RULES t1 WITH(NOLOCK) 
				INNER JOIN
				(
					SELECT 
						t12.value 
						, t12.label_zt AS 'label' 
					FROM 
						[LIST] t11 WITH(NOLOCK) 
					INNER JOIN 
						[VALUE] t12 WITH(NOLOCK) 
						ON t12.source_id = t11.id 
					WHERE 
						t11.name = 'In_Register_Type'
				) t2 ON t2.value = t1.in_type
                WHERE 
	                t1.source_id = '{#meeting_id}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }

            public string meeting_id { get; set; }
            public string in_creator_sno { get; set; }
            public string in_current_org { get; set; }
            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }
            public string in_index { get; set; }
            public string in_sno { get; set; }
        }

        private int GetInt(string value, int defV = 0)
        {
            if (value == "") return 0;

            int result = defV;
            if (int.TryParse(value, out result))
            {
                return result;
            }
            return defV;
        }
    }
}