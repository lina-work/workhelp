﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.CTPC.Common
{
    public class In_Payment_List_Head : Item
    {
        public In_Payment_List_Head(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 依人頭計費
                日誌: 
                    - 2023-01-07: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]" + "In_Payment_List_Head";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),

                item_type = itmR.getProperty("item_type", ""),
                meeting_id = itmR.getProperty("meeting_id", ""),
                in_creator_sno = itmR.getProperty("in_creator_sno", ""),
                scene = itmR.getProperty("scene", ""),
            };

            if (cfg.item_type == "" || cfg.meeting_id == "")
            {
                throw new Exception("賽事 id 不可為空值");
            }

            cfg.meeting_table = cfg.item_type;
            cfg.meeting_column = cfg.item_type.ToLower();
            cfg.muser_table = cfg.item_type + "_USER";

            cfg.itmMeeting = cfg.inn.applySQL("SELECT in_title, in_course_fees FROM " + cfg.meeting_table + " WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            if (cfg.itmMeeting.isError() || cfg.itmMeeting.getResult() == "")
            {
                throw new Exception("活動資料錯誤");
            }

            cfg.in_title = cfg.itmMeeting.getProperty("in_title", "");
            cfg.in_course_fees = cfg.itmMeeting.getProperty("in_course_fees", "0");
            cfg.course_fees = GetIntVal(cfg.in_course_fees);

            switch (cfg.scene)
            {
                case "pay":
                    Calculate(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void Calculate(TConfig cfg, Item itmReturn)
        {
            //已建立繳費單
            var pusers = GetPayUsers(cfg);
            
            //單位應付費用統計
            var orgs = MUsersToOrgs(cfg, pusers);

            //計算費用
            CalculateAmount(cfg, orgs);

            //附加 Team Item Relationship
            AppendOrgTeams(cfg, orgs, itmReturn);
        }

        private List<TPUser> GetPayUsers(TConfig cfg)
        {
            var result = new List<TPUser>();

            var sql = @"
                SELECT 
	                t1.id
	                , t1.item_number
	                , t1.in_pay_date_real
	                , t1.pay_bool
	                , t1.in_creator
	                , t1.in_creator_sno
	                , t2.in_sno
	                , t2.in_l1
                FROM
	                IN_MEETING_PAY t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_NEWS t2 WITH(NOLOCK) 
	                ON t2.source_id = t1.id
                WHERE 
	                t1.{#item_type} = '{#meeting_id}'
	                AND t1.in_creator_sno = '{#in_creator_sno}'
	                AND t2.in_l1 <> N'隊職員'
	                AND t1.pay_bool <> N'已取消'
	                AND ISNULL(t2.in_sno, '') <> ''
            ";
            
            sql = sql.Replace("{#item_type}", cfg.meeting_column)
                .Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_creator_sno}", cfg.in_creator_sno);

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var in_sno = item.getProperty("in_sno", "");
                var obj = result.Find(x => x.in_sno == in_sno);
                if (obj == null)
                {
                    obj = new TPUser
                    {
                        in_sno = in_sno,
                        pay_id = item.getProperty("id", ""),
                        in_paynumber = item.getProperty("item_number", ""),
                        in_paytime = GetDtmVal(item.getProperty("in_pay_date_real", ""), 8, "yyyy-MM-dd HH:mm:ss"),
                        item = item,
                    };
                    result.Add(obj);
                }
            }

            return result;
        }

        //附加 Team Item Relationship
        private void AppendOrgTeams(TConfig cfg, List<TOrg> orgs, Item itmReturn)
        {
            foreach (var org in orgs)
            {
                Item item = cfg.inn.newItem();
                item.setType("inn_org_teampay");
                item.setProperty("in_creator_sno", cfg.in_creator_sno);
                item.setProperty("in_current_org", org.name);
                item.setProperty("bill_amount", org.bill_amount.ToString());
                item.setProperty("strpay", org.wait_amount.ToString());
                itmReturn.addRelationship(item);
            }
        }

        //計算費用
        private void CalculateAmount(TConfig cfg, List<TOrg> orgs)
        {
            foreach (var org in orgs)
            {
                //該單位 應付款項
                org.bill_amount = org.Players.Count * cfg.course_fees;
                org.wait_amount = org.bill_amount;
            }
        }

        private List<TOrg> MUsersToOrgs(TConfig cfg, List<TPUser> pusers)
        {
            var result = new List<TOrg>();

            var sql = @"
                SELECT 
                    id
                    , in_current_org
                    , in_name
                    , in_sno
                    , in_gender
                    , in_l1
                    , in_l2
                    , in_l3
                    , in_l3
                    , in_index
                    , in_team
                    , in_expense
                    , in_paynumber
                    , in_gameunit
                    , in_section_name
                    , in_l1_sort
                    , in_regdate
                    , in_creator
                    , in_creator_sno
                FROM 
                    {#meeting_table} WITH(NOLOCK)
                WHERE 
                    source_id = '{#meeting_id}'
                    AND in_creator_sno = '{#in_creator_sno}'
                    AND ISNULL(in_paynumber, '') = ''
            ";

            sql = sql.Replace("{#meeting_table}", cfg.meeting_table)
                .Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_creator_sno}", cfg.in_creator_sno);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            var itmMUsers = cfg.inn.applySQL(sql);

            var count = itmMUsers.getItemCount();
            
            for (int i = 0; i < count; i++)
            {
                var itmMUser = itmMUsers.getItemByIndex(i);
                var muser = MapMUser(cfg, itmMUser);

                if (muser.is_player)
                {
                    var puser = pusers.Find(x => x.in_sno == muser.player_key);
                    if (puser != null)
                    {
                        //已產生過繳費單
                        MergePayMeetingUser(cfg, muser, puser);
                        continue;
                    }
                }

                TOrg org = result.Find(x => x.name == muser.in_current_org);
                if (org == null)
                {
                    org = new TOrg
                    {
                        name = muser.in_current_org,
                        bill_amount = 0,
                        Items = new List<TMUser>(),
                        Players = new List<TMUser>(),
                        Staffs = new List<TMUser>(),
                    };
                    result.Add(org);
                }

                var item = org.Items.Find(x => x.item_key == muser.item_key);
                if (item == null) org.Items.Add(muser);

                var player = org.Players.Find(x => x.player_key == muser.player_key);
                if (player == null) org.Players.Add(muser);

                if (muser.is_staff)
                {
                    var staff = org.Staffs.Find(x => x.staff_key == muser.staff_key);
                    if (staff == null) org.Staffs.Add(muser);
                }
            }

            return result;
        }

        private void MergePayMeetingUser(TConfig cfg, TMUser muser, TPUser puser)
        {
            var itmMUser = muser.item;
            var in_regdate = GetDtmVal(itmMUser.getProperty("in_regdate", ""), 8, "yyyy-MM-ddTHH:mm:ss");

            //新增到繳費資訊下
            Item itmNew = cfg.inn.newItem("In_Meeting_news", "add");
            itmNew.setProperty("source_id", puser.pay_id);
            itmNew.setProperty("in_ans_l3", itmMUser.getProperty("in_gameunit", ""));//組別彙整結果
            itmNew.setProperty("in_pay_amount", itmMUser.getProperty("in_expense", ""));//應繳金額
            itmNew.setProperty("in_name", itmMUser.getProperty("in_name", ""));//姓名
            itmNew.setProperty("in_sno", itmMUser.getProperty("in_sno", ""));//身分證
            itmNew.setProperty("in_creator", itmMUser.getProperty("in_creator", ""));//協助報名者
            itmNew.setProperty("in_creator_sno", itmMUser.getProperty("in_creator_sno", ""));//協助報名者帳號
            itmNew.setProperty("in_regdate", in_regdate);//報名日期

            itmNew.setProperty("in_l1", itmMUser.getProperty("in_l1", ""));//第一層
            itmNew.setProperty("in_l2", itmMUser.getProperty("in_l2", ""));//第二層
            itmNew.setProperty("in_l3", itmMUser.getProperty("in_l3", ""));//第三層
            itmNew.setProperty("in_index", itmMUser.getProperty("in_index", ""));//序號
                                                                                          //in_meeting_news.setProperty("in_team_index", itmMUser.getProperty("in_team_index", ""));//隊伍序號
            itmNew.setProperty("in_section_name", itmMUser.getProperty("in_section_name", ""));//組名
            itmNew.setProperty("in_l1_sort", itmMUser.getProperty("in_l1_sort", ""));//第一層排序
            itmNew.setProperty("in_muid", itmMUser.getProperty("id", ""));//與會者id
            itmNew = itmNew.apply();

            var sql_upd = "UPDATE " + cfg.muser_table + " SET"
                + "  in_paynumber = '" + puser.in_paynumber + "'"
                + ", in_paytime = '" + puser.in_paytime + "'"
                + " WHERE id = '" + muser.id + "'";

            cfg.inn.applySQL(sql_upd);
        }

        private TMUser MapMUser(TConfig cfg, Item item)
        {
            TMUser result = new TMUser
            {
                id = item.getProperty("id", ""),
                in_name = item.getProperty("in_name", ""),
                in_sno = item.getProperty("in_sno", ""),
                in_gender = item.getProperty("in_gender", ""),
                in_creator_sno = item.getProperty("in_creator_sno", ""),
                in_current_org = item.getProperty("in_current_org", "").Trim(),
                in_l1 = item.getProperty("in_l1", ""),
                in_l2 = item.getProperty("in_l2", ""),
                in_l3 = item.getProperty("in_l3", ""),
                in_index = item.getProperty("in_index", ""),
                in_team = item.getProperty("in_team", ""),
                in_expense = item.getProperty("in_expense", "0"),
                in_paynumber = item.getProperty("in_paynumber", ""),
                item_key = "",
                player_key = "",
                staff_key = "",
                item = item,
            };

            //result.expense = GetIntVal(result.in_expense);
            result.is_staff = result.in_l1 == "隊職員";
            result.has_paynumber = result.in_paynumber != "";

            result.item_key = ClearKey(result.in_l1 + result.in_l2 + result.in_l3 + result.in_index);
            result.player_key = ClearKey(result.in_sno);
            
            if (result.is_staff)
            {
                result.staff_key = ClearKey(result.in_sno + result.in_name);
            }
            else
            {
                result.is_player = true;
            }

            return result;
        }

        //清理鍵值
        private string ClearKey(string key)
        {
            return key.Trim().ToUpper();
        }

        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }

            public string item_type { get; set; }
            public string meeting_id { get; set; }
            public string in_creator_sno { get; set; }
            public string scene { get; set; }

            public Item itmMeeting { get; set; }
            public string in_title { get; set; }
            public string in_course_fees { get; set; }
            public int course_fees { get; set; }

            public Item itmLogin { get; set; }
            public bool isMeetingAdmin { get; set; }

            public string meeting_table { get; set; }
            public string muser_table { get; set; }
            public string meeting_column { get; set; }
        }

        private class TOrg
        {
            public string name { get; set; }
            public int bill_amount { get; set; }
            public int wait_amount { get; set; }
            public List<TMUser> Players { get; set; }
            public List<TMUser> Staffs { get; set; }
            public List<TMUser> Items { get; set; }
        }

        private class TMUser
        {
            public string id { get; set; }
            public string in_name { get; set; }
            public string in_gender { get; set; }
            public string in_sno { get; set; }
            public string in_current_org { get; set; }
            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }
            public string in_index { get; set; }
            public string in_team { get; set; }
            public string in_expense { get; set; }
            public string in_paynumber { get; set; }
            public string in_creator_sno { get; set; }

            public int expense { get; set; }
            public bool is_staff { get; set; }
            public bool is_player { get; set; }
            public bool has_paynumber { get; set; }
            
            public string item_key { get; set; }
            public string player_key { get; set; }
            public string staff_key { get; set; }

            public Item item { get; set; }
        }

        private class TPUser
        {
            public string in_sno { get; set; }
            public string pay_id { get; set; }
            public string in_paynumber { get; set; }
            public string in_paytime { get; set; }
            public Item item { get; set; }
        }

        private string GetDtmVal(string value, int hours = 0, string format = "yyyy-MM-dd")
        {
            if (value == "") return "";

            var day = value.Replace("/", "-");
            var dt = DateTime.Now;
            if (DateTime.TryParse(day, out dt))
            {
                return dt.AddHours(hours).ToString(format);
            }
            else
            {
                return "";
            }
        }

        private int GetIntVal(string value, int def = 0)
        {
            if (value == "" || value == "0") return 0;

            int result = def;
            int.TryParse(value, out result);
            return result;
        }
    }
}