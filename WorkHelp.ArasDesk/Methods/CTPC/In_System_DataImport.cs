﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.CTPC
{
    public class In_System_DataImport : Item
    {
        public In_System_DataImport(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 系統資料匯入
                日誌: 
                    - 2023-01-6: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]" + "In_System_DataImport";

            Item itmR = this;
            CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                strIdentityId = inn.getUserAliases(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                scene = itmR.getProperty("scene", ""),
            };

            switch (cfg.scene)
            {
                case "import":
                    Import(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void Import(TConfig cfg, Item itmReturn)
        {
            var item_type = itmReturn.getProperty("item_type", "");
            var str_keys = itmReturn.getProperty("keys", "");
            var str_cols = itmReturn.getProperty("cols", "");
            var str_rows = itmReturn.getProperty("rows", "");

            if (item_type == "" || str_cols == "" || str_rows == "")
            {
                throw new Exception("資料錯誤");
            }

            var itmType = cfg.inn.applySQL("SELECT id, [name] FROM ITEMTYPE WITH(NOLOCK) WHERE is_current = 1 AND [name] = '" + item_type + "'");
            if (itmType.isError() || itmType.getResult() == "")
            {
                throw new Exception("資料錯誤");
            }

            var pkg = new TPkg
            {
                //避免因大小寫導致失敗
                item_type = itmType.getProperty("name", ""),
                properies = GetProperties(cfg, itmType),
            };

            pkg.keys = str_keys == ""
                ? new List<string>()
                : Newtonsoft.Json.JsonConvert.DeserializeObject<List<string>>(str_keys);

            pkg.cols = Newtonsoft.Json.JsonConvert.DeserializeObject<List<string>>(str_cols);


            for(var i = pkg.keys.Count - 1; i >= 0; i--) 
            {
                if (!pkg.properies.Contains(pkg.keys[i]))
                {
                    pkg.keys.RemoveAt(i);
                }
            }

            for (var i = pkg.cols.Count - 1; i >= 0; i--)
            {
                if (!pkg.properies.Contains(pkg.cols[i]))
                {
                    pkg.cols.RemoveAt(i);
                }
            }

            var rows = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Newtonsoft.Json.Linq.JObject>>(str_rows);

            foreach (var row in rows)
            {
                MergeItem(cfg, pkg, row);
            }
        }

        private void MergeItem(TConfig cfg, TPkg pkg, Newtonsoft.Json.Linq.JObject row)
        {
            Item item = cfg.inn.newItem(pkg.item_type, "merge");

            var conds = new List<string>();
            foreach (var key in pkg.keys)
            {
                var val = GetJsonValue(row, key);
                conds.Add("[" + key + "]=N'" + val + "'");
            }

            if (conds.Count > 0)
            {
                item.setAttribute("where", string.Join(" AND ", conds));
            }

            foreach (var col in pkg.cols)
            {
                var val = GetJsonValue(row, col);
                item.setProperty(col, val);
            }

            item = item.apply();
        }

        private string GetJsonValue(Newtonsoft.Json.Linq.JObject obj, string key)
        {
            try
            {
                Newtonsoft.Json.Linq.JToken value;
                if (obj.TryGetValue(key, out value))
                {
                    return value.ToString().Trim();
                }
                return "";
            }
            catch
            {
                return "";
            }
        }

        private List<string> GetProperties(TConfig cfg, Item itmType)
        {
            var result = new List<string>();

            string sql = @"
                SELECT 
	                [name] 
                FROM 
	                PROPERTY WITH(NOLOCK) 
                WHERE 
	                is_current = 1 
	                AND [source_id] = '{#id}'
                ORDER BY
	                sort_order
            ";

            sql = sql.Replace("{#id}", itmType.getProperty("id", ""));

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                result.Add(item.getProperty("name", ""));
            }

            return result;
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }

            public string meeting_id { get; set; }
            public string scene { get; set; }
        }

        private class TPkg
        {
            public string item_type { get; set; }
            public List<string> properies { get; set; }

            public List<string> keys { get; set; }
            public List<string> cols { get; set; }
        }
    }
}