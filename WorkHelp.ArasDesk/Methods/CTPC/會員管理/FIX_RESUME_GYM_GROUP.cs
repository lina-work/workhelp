﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.CTPC.Common
{
    public class FIX_RESUME_GYM_GROUP : Item
    {
        public FIX_RESUME_GYM_GROUP(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;            
            /*
                目的: 匯入 112 年團體會員資料
                日誌: 
                    - 2022-12-12: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]" + "FIX_RESUME_GYM_GROUP";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                strIdentityId = inn.getUserAliases(),

                scene = itmR.getProperty("scene", ""),
            };

            cfg.scene = "run";

            switch (cfg.scene)
            {
                case "run":
                    Run(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void Run(TConfig cfg, Item itmReturn)
        {
            //MergerResume(cfg);
            //ClearOldResume(cfg);
            MergerGymGroupMember(cfg);
            RemoveGymGroupMemberRelation(cfg);
        }

        private void RemoveGymGroupMemberRelation(TConfig cfg)
        {
            string sql = @"
                SELECT
	                t1.id
	                , t2.id       AS 'org_id'
	                , t2.in_name  AS 'org_name'
	                , t2.in_sno   AS 'org_sno'
	                , t3.id		  AS 'mmbr_id'
	                , t3.in_name  AS 'mmbr_name'
	                , t3.in_sno   AS 'mmbr_sno'
	                , t3.in_add   AS 'mmbr_addr'
	                , t3.in_tel   AS 'mmbr_tele'
	                , t3.in_title AS 'mmbr_title'
                FROM
	                IN_RESUME_RESUME t1 WITH(NOLOCK)
                INNER JOIN
	                IN_RESUME t2 WITH(NOLOCK)
	                ON t2.id = t1.source_id
                INNER JOIN
	                IN_RESUME t3 WITH(NOLOCK)
	                ON t3.id = t1.related_id
                WHERE
	                t2.in_member_type = 'vip_gym'
	                AND t3.in_member_type = 'gym_member'
                ORDER BY
	                t2.in_sno
	                , t3.in_sno
            ";
            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var id = item.getProperty("id", "");
                var mmbr_name = item.getProperty("mmbr_name", "");
                if (mmbr_name == "" || mmbr_name == "未定")
                {
                    var sql_del = "DELETE FROM IN_RESUME_RESUME WHERE id = '" + id + "'";
                    cfg.inn.applySQL(sql_del);
                }
            }
        }

        private void MergerGymGroupMember(TConfig cfg)
        {
            string sql = @"
                SELECT 
	                t1.id
	                , t1.in_name
	                , t1.in_sno
	                , t1.in_add
	                , t1.in_tel
	                , t1.in_tel_1
	                , t1.in_note
	                , t1.in_current_org
	                , t1.in_group
	                , t1.in_title
	                , t1.in_manager_org
	                , t1.in_manager_name
	                , t2.*
                FROM 
	                IN_RESUME t1 WITH(NOLOCK) 
                LEFT OUTER JOIN
	                AAA_Gym_Group_Member_20221212 t2 WITH(NOLOCK) 
	                ON t2.c1 = t1.login_name
                WHERE 
	                LEN(t1.login_name) = 7 
	                AND t1.in_member_type = 'gym_member'
                ORDER BY 
	                t1.login_name
            ";

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var id = item.getProperty("id", "");
                var c1 = item.getProperty("c1", "");
                var org_id = item.getProperty("in_manager_org", "");

                var need_clear = c1 == "";

                //舊資料
                var itmOld = cfg.inn.newItem("In_Resume", "merge");
                itmOld.setAttribute("where", "id = '" + id + "'");
                itmOld.setProperty("id", id);

                //所屬單位
                var itmOrg = cfg.inn.newItem("In_Resume", "get");
                itmOrg.setProperty("id", org_id);
                itmOrg = itmOrg.apply();

                if (need_clear)
                {
                    ClearGymGroupMember(cfg, itmOld, item);
                }
                else
                {
                    MergeGymGroupMember(cfg, itmOld, item, itmOrg);
                }
                itmOld.apply();

                //成員角色
                Item itmOldRole = cfg.inn.newItem("In_Resume_Resume", "merge");
                itmOldRole.setAttribute("where", "source_id = '" + org_id + "' AND related_id = '" + id + "'");
                itmOldRole.setProperty("source_id", org_id);
                itmOldRole.setProperty("id", id);
                if (need_clear)
                {
                    itmOldRole.setProperty("in_resume_remark", "");
                }
                else
                {
                    itmOldRole.setProperty("in_resume_role", "gym_2600");
                    itmOldRole.setProperty("in_resume_remark", "會員代表");
                }
                itmOldRole.apply();
            }
        }

        private void MergeGymGroupMember(TConfig cfg, Item itmOld, Item itmData, Item itmOrg)
        {
            string c1 = itmData.getProperty("c1", "");
            string c2 = itmData.getProperty("c2", "");
            string c3 = itmData.getProperty("c3", "");
            string c4 = itmData.getProperty("c4", "");
            string c5 = itmData.getProperty("c5", "");

            string org_name = itmOrg.getProperty("in_name", "");

            itmOld.setProperty("in_name", c2);
            itmOld.setProperty("in_add", c3);
            itmOld.setProperty("in_tel", c4);
            itmOld.setProperty("in_title", "會員代表");
            itmOld.setProperty("in_email", "na@na.n");
            itmOld.setProperty("in_member_type", "gym_member");
            itmOld.setProperty("in_note", "112年團體會員資料");

            itmOld.setProperty("in_manager_name", org_name);
            itmOld.setProperty("in_current_org", org_name);
            itmOld.setProperty("in_group", org_name);
            itmOld.setProperty("in_stuff_a1", org_name);
            itmOld.setProperty("in_short_name", org_name);
            itmOld.setProperty("in_short_org", org_name);

            itmOld.setProperty("in_member_cat", "");
            itmOld.setProperty("in_member_status", "");
            itmOld.setProperty("in_member_unit", "");

            itmOld.setProperty("in_pay_year3", "0");
            itmOld.setProperty("in_pay_year4", "0");
            itmOld.setProperty("in_pay_year5", "0");
        }

        private void ClearGymGroupMember(TConfig cfg, Item itmOld, Item itmData)
        {
            var c2 = "未定";

            itmOld.setProperty("in_name", c2);
            itmOld.setProperty("in_add", "");
            itmOld.setProperty("in_tel", "");
            itmOld.setProperty("in_title", "");
            itmOld.setProperty("in_email", "na@na.n");
            itmOld.setProperty("in_member_type", "gym_member");

            itmOld.setProperty("in_manager_name", c2);
            itmOld.setProperty("in_current_org", c2);
            itmOld.setProperty("in_group", c2);
            itmOld.setProperty("in_stuff_a1", c2);
            itmOld.setProperty("in_short_name", c2);
            itmOld.setProperty("in_short_org", c2);

            itmOld.setProperty("in_member_cat", "");
            itmOld.setProperty("in_member_status", "");
            itmOld.setProperty("in_member_unit", "");

            itmOld.setProperty("in_pay_year3", "0");
            itmOld.setProperty("in_pay_year4", "0");
            itmOld.setProperty("in_pay_year5", "0");
        }

        private void ClearOldResume(TConfig cfg)
        {
            var sql = @"
                SELECT
	                id
	                , in_name
	                , in_sno
                FROM
	                IN_RESUME WITH(NOLOCK)
                WHERE 
	                ISNULL(in_member_type, '') = 'vip_gym' 
	                AND in_sno NOT IN 
	                (
		                SELECT c1 FROM AAA_Gym_Group_20221212 WITH(NOLOCK)
	                )
                ORDER BY
	                in_sno
            ";

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var id = item.getProperty("id", "");
                var c2 = "未定";

                var itmResume = cfg.inn.newItem("In_Resume", "merge");
                itmResume.setAttribute("where", "id = '" + id + "'");
                itmResume.setProperty("id", id);
                itmResume.setProperty("in_name", c2);
                itmResume.setProperty("in_current_org", c2);
                itmResume.setProperty("in_group", c2);
                itmResume.setProperty("in_stuff_a1", c2);
                itmResume.setProperty("in_short_name", c2);
                itmResume.setProperty("in_short_org", c2);

                itmResume.setProperty("in_member_cat", "C00");
                itmResume.setProperty("in_member_status", "未入會");
                itmResume.setProperty("in_member_unit", "無");
                itmResume.setProperty("in_member_type", "other");
                itmResume.setProperty("in_email", "na@na.n");

                itmResume.setProperty("in_principal", "");
                itmResume.setProperty("in_add", "");
                itmResume.setProperty("in_tel", "");
                itmResume.setProperty("in_tel_1", "");
                itmResume.setProperty("in_pay_year4", "0");
                itmResume.setProperty("in_pay_year5", "0");

                itmResume.apply();
            }
        }

        private void MergerResume(TConfig cfg)
        {
            var sql = @"
                SELECT 
                	t1.*
                	, t2.id
                FROM 
                	AAA_Gym_Group_20221212 t1 WITH(NOLOCK)
                INNER JOIN
                	IN_RESUME t2 WITH(NOLOCK)
                	ON t2.login_name = t1.c1
                ORDER BY 
                	t1.c1
            ";
            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var id = item.getProperty("id", "");
                var c1 = item.getProperty("c1", "");
                var c2 = item.getProperty("c2", "");
                var c3 = item.getProperty("c3", "");
                var c4 = item.getProperty("c4", "");
                var c5 = item.getProperty("c5", "");
                var c6 = item.getProperty("c6", "");
                var c7 = item.getProperty("c7", "");
                var c8 = item.getProperty("c8", "");
                var c9 = item.getProperty("c9", "");

                var itmResume = cfg.inn.newItem("In_Resume", "merge");
                itmResume.setAttribute("where", "id = '" + id + "'");
                itmResume.setProperty("id", id);
                itmResume.setProperty("in_name", c2);
                itmResume.setProperty("in_current_org", c2);
                itmResume.setProperty("in_group", c2);
                itmResume.setProperty("in_stuff_a1", c2);
                itmResume.setProperty("in_short_name", c2);
                itmResume.setProperty("in_short_org", c2);

                //CTPC 會員類別
                itmResume.setProperty("in_member_cat", "C00");
                itmResume.setProperty("in_member_status", "合格會員");
                itmResume.setProperty("in_member_unit", c5);
                itmResume.setProperty("in_email", "na@na.n");

                itmResume.setProperty("in_principal", c3);
                itmResume.setProperty("in_add", c6);
                itmResume.setProperty("in_tel", c7);
                itmResume.setProperty("in_tel_1", c7);
                itmResume.setProperty("in_pay_year4", "1");
                itmResume.setProperty("in_pay_year5", "0");

                itmResume.apply();
            }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }

            public string scene { get; set; }
        }
    }
}