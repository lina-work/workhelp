﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.CTPC.Common
{
    public class in_group_dashboard_mbrs : Item
    {
        public in_group_dashboard_mbrs(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 成員列表
                日期: 
                    - 2021-09-07 modify (David) 
                    - 2020-12-24 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]in_group_dashboard_mbrs";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "input dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                inn = inn,
                mode = itmR.getProperty("mode", ""),
                member_type = itmR.getProperty("type", ""),
                member_status = itmR.getProperty("in_member_status", ""),
                committee_resume_id = itmR.getProperty("committee_resume_id", ""),
                in_member_unit = itmR.getProperty("in_member_unit", ""),
                in_filter = itmR.getProperty("in_filter", ""),
                scene = itmR.getProperty("scene", ""),
                resume_id = itmR.getProperty("rid", ""),
                resident_date = itmR.getProperty("in_resident_date", ""),
            };

            //取得登入者資訊
            string sql = "SELECT * FROM IN_RESUME WITH(NOLOCK) WHERE in_user_id = N'" + cfg.strUserId + "'";
            cfg.itmLoginResume = inn.applySQL(sql);
            if (IsError(cfg.itmLoginResume))
            {
                throw new Exception("登入者履歷異常");
            }
            cfg.in_is_admin = cfg.itmLoginResume.getProperty("in_is_admin", "");

            //要檢視的對象 Resume
            cfg.itmResumeView = GetTargetResume(CCO, strMethodName, inn, cfg.itmLoginResume, itmR);
            cfg.view_resume_id = cfg.itmResumeView.getProperty("id", "");
            cfg.view_member_type = cfg.itmResumeView.getProperty("in_member_type", "");

            //權限檢核
            cfg.itmPermit = inn.applyMethod("In_CheckIdentity", "<method>" + strMethodName + "</method><code>" + "ALL" + "</code>");
            cfg.isMeetingAdmin = cfg.itmPermit.getProperty("isMeetingAdmin", "") == "1";
            cfg.isCommittee = cfg.itmPermit.getProperty("isCommittee", "") == "1";
            if (cfg.in_is_admin == "1")
            {
                cfg.isMeetingAdmin = true;
            }

            Item itmResumes = null;
            string raw_count = "0";

            var isVipMember = cfg.member_type == "vip_mbr" || cfg.member_type == "vip_minority";
            if (cfg.view_member_type == "asc" || cfg.view_member_type == "sys")
            {
                raw_count = GetMembersCount(cfg, "", cfg.member_type, isVipMember);
                if (cfg.scene == "page")
                {

                }
                else
                {
                    if (isVipMember)
                    {
                        //個人會員 or 準會員
                        itmResumes = GetVipMembers(cfg, cfg.committee_resume_id);
                    }
                    else
                    {
                        //一般團體
                        itmResumes = GetMembers(cfg, cfg.committee_resume_id);
                    }
                }
            }
            else if (cfg.view_member_type == "area_cmt" || cfg.view_member_type == "prjt_cmt")
            {
                raw_count = GetMembersCount(cfg, cfg.view_resume_id, cfg.member_type, isVipMember);
                itmResumes = GetMembers(cfg, cfg.view_resume_id);
            }

            var resumes = MapResumes(itmResumes);

            if (cfg.mode == "")
            {
                if (cfg.isMeetingAdmin)
                {
                    AppendCommitteeMenu(CCO, strMethodName, inn, itmR);
                    AppendStatusMenu(CCO, strMethodName, inn, itmR);
                    itmR.setProperty("CreatePaysBtn", "<button class='btn btn-warning' onclick='CreatePays_Click()'>產生繳費單</button>");

                    switch (cfg.member_type)
                    {
                        case "vip_gym":
                            itmR.setProperty("hide_cmt_menu", "");
                            itmR.setProperty("ExportPdfBtn", "<button class='btn btn-primary' onclick='ExportPDF_Click()'>匯出PDF</button>");
                            break;

                        default:
                            itmR.setProperty("hide_cmt_menu", "item_show_0");
                            break;
                    }
                }

                itmR.setProperty("raw_count", raw_count);
                Query(CCO, strMethodName, inn, cfg.itmResumeView, resumes, cfg.itmPermit, itmR);

            }
            else if (cfg.mode == "xls")
            {
                Export(CCO, strMethodName, inn, cfg.itmResumeView, resumes, itmR);
            }
            else if (cfg.mode == "CertPDF")
            {
                exportCertificate(cfg, itmR);
            }
            return itmR;
        }

        //匯出團體會員證書
        private void exportCertificate(TConfig cfg, Item itmReturn)
        {
            string pdfName = "";

            pdfName = "團體會員證書";

            string in_name = "certificateMbr_path";
            Item itmXls = cfg.inn.applyMethod("In_Variable_File", "<in_name>" + in_name + "</in_name>");

            string export_path = itmXls.getProperty("export_path", "");
            string template_path = itmXls.getProperty("template_path", "");

            Spire.Xls.Workbook book = new Spire.Xls.Workbook();
            book.LoadFromFile(template_path);
            Spire.Xls.Worksheet sheetTemplate = book.Worksheets[0];

            Item itmUsers = GetUsers(cfg);
            UpdateUsers(cfg);

            if (itmUsers.isError())
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "匯出團體會員證書資料發生錯誤");
                throw new Exception("匯出團體會員證書發生錯誤");
            }

            if (itmUsers.getItemCount() < 1)
            {
                throw new Exception("目前暫無團體會員證書供下載!");
            }
            // pdfName = itmUsers.getProperty("in_name","") + pdfName;
            string pdfFile = export_path + pdfName + ".pdf";
            AppendCertificate(book, sheetTemplate, cfg, itmUsers);

            sheetTemplate.Remove();
            book.SaveToFile(pdfFile, Spire.Xls.FileFormat.PDF);


            itmReturn.setProperty("pdf_name", pdfName + ".pdf");
        }

        //取得團體會員資料
        private Item GetUsers(TConfig cfg)
        {
            string sql = @"SELECT * FROM IN_RESUME WITH(NOLOCK)
                         WHERE id in (" + cfg.resume_id.Substring(0, cfg.resume_id.Length - 1) + ")";
            return cfg.inn.applySQL(sql);
        }

        //更新館證日期
        private void UpdateUsers(TConfig cfg)
        {
            string now = System.DateTime.Now.ToString("yyyy/MM/dd");
            string sql = @"UPDATE in_resume SET in_resident_date ='{#in_resident_date}'
                         WHERE id in (" + cfg.resume_id.Substring(0, cfg.resume_id.Length - 1) + ") AND in_resident_date IS NULL";
            sql = sql.Replace("{#in_resident_date}", now);
            cfg.inn.applySQL(sql);
        }

        private void AppendCertificate(Spire.Xls.Workbook book, Spire.Xls.Worksheet sheetTemplate, TConfig cfg, Item itmUsers)
        {
            int user_count = itmUsers.getItemCount();
            DateTime currentTime = System.DateTime.Now;
            if (user_count != 0)
            {
                for (int i = 0; i < user_count; i++)
                {
                    Item itmUser = itmUsers.getItemByIndex(i);

                    string in_name = itmUser.getProperty("in_name", "");

                    string chinese_day = "";

                    Spire.Xls.Worksheet sheet = book.CreateEmptySheet();
                    string content = "茲核定{#in_name}為本會合格團體會員";
                    content = content.Replace("{#in_name}", in_name);
                    DateTime now = System.DateTime.Now;
                    string reg_date = itmUser.getProperty("in_reg_date", "");


                    sheet.CopyFrom(sheetTemplate);
                    sheet.Name = itmUser.getProperty("id", "");
                    sheet.Range["B18"].Text = content;                                          //內文
                    // sheet.Range["E28"].Text = itmUser.getProperty("in_principal", "");           //負責人
                    // sheet.Range["E29"].Text = itmUser.getProperty("in_head_coach", "");          //總教練
                    // sheet.Range["E30"].Text = GetAssistants(itmUser);   //助理教練列表
                    // sheet.Range["E31"].Text = itmUser.getProperty("in_resident_add", "");        //設立地址
                    sheet.Range["M1"].Text = itmUser.getProperty("in_principal", "");           //負責人
                    sheet.Range["M2"].Text = itmUser.getProperty("in_head_coach", "");          //總教練
                    sheet.Range["M3"].Text = GetAssistants(itmUser);   //助理教練列表
                    sheet.Range["M4"].Text = itmUser.getProperty("in_resident_add", "");        //設立地址

                    string in_photo = itmUser.getProperty("in_photo", "");

                    string url = @"C:\Aras\Vault\PLMCTA\";//讀取大頭照的路徑
                                                          //路徑切出來

                    if (in_photo != "")
                    {
                        string id_1 = in_photo.Substring(0, 1);
                        string id_2 = in_photo.Substring(1, 2);
                        string id_3 = in_photo.Substring(3, 29);
                        string filename = "";
                        string aml = "<AML>" +
                        "<Item type='File' action='get'>" +
                        "<id>" + in_photo + "</id>" +
                        "</Item></AML>";
                        Item Files = cfg.inn.applyAML(aml);

                        if (Files.getItemCount() > 0)
                        {
                            filename = Files.getProperty("filename", "");//取得檔名
                        }

                        Spire.Xls.ExcelPicture picture = sheet.Pictures.Add(24, 8, url + id_1 + @"\" + id_2 + @"\" + id_3 + @"\" + filename);
                        picture.Width = 120;
                        picture.Height = 140;
                    }

                    if (reg_date != "")
                    {
                        reg_date = GetDateTimeValue(reg_date, "yyyy/MM/dd", 8);
                        if (reg_date == "1900/01/01")
                        {
                            //sheet.Range["E32"].Text = "";   //入會日期
                            sheet.Range["M5"].Text = "";   //入會日期
                        }
                        else
                        {
                            //sheet.Range["E32"].Text = convertCDay(Convert.ToDateTime(reg_date), "");   //入會日期
                            sheet.Range["M5"].Text = convertCDay(Convert.ToDateTime(reg_date), "");   //入會日期
                        }
                    }
                    else
                    {
                        //sheet.Range["E32"].Text = "";   //入會日期
                        sheet.Range["M5"].Text = "";   //入會日期
                    }

                    // sheet.Range["E33"].Text = itmUser.getProperty("in_manager_name", "");        //所屬委員會
                    // sheet.Range["E34"].Text = itmUser.getProperty("in_resident_id", "") + "號";   //會員證字號
                    sheet.Range["M6"].Text = itmUser.getProperty("in_manager_name", "");        //所屬委員會
                    sheet.Range["M7"].Text = itmUser.getProperty("in_resident_id", "") + "號";   //會員證字號

                    if (itmUser.getProperty("in_resident_date", "") != "")
                    {
                        sheet.Range["C45"].Text = convertCDay(Convert.ToDateTime(itmUser.getProperty("in_resident_date", "")), "footer"); ; //中華民國
                    }
                    else
                    {
                        sheet.Range["C45"].Text = convertCDay(Convert.ToDateTime(now), "footer"); ; //中華民國
                    }
                    sheet.PageSetup.FitToPagesWide = 1;
                    sheet.PageSetup.FitToPagesTall = 1;
                    sheet.PageSetup.TopMargin = 0.3;
                    sheet.PageSetup.LeftMargin = 0.5;
                    sheet.PageSetup.RightMargin = 0.5;
                    sheet.PageSetup.BottomMargin = 0.5;

                    //適合頁面
                    book.ConverterSetting.SheetFitToPage = true;
                }
            }
        }

        private string GetAssistants(Item itmUser)
        {
            string in_assistant_coaches = itmUser.getProperty("in_assistant_coaches", "");
            List<string> list = new List<string>();

            string in_aider_1 = itmUser.getProperty("in_aider_1", "");
            string in_aider_2 = itmUser.getProperty("in_aider_2", "");
            string in_aider_3 = itmUser.getProperty("in_aider_3", "");

            if (in_aider_1 != "") list.Add(in_aider_1);
            if (in_aider_2 != "") list.Add(in_aider_2);
            if (in_aider_3 != "") list.Add(in_aider_3);

            return string.Join("、", list);
        }

        private static string convertCDay(DateTime CDay, string type)
        {
            string y = (CDay.Year - 1911).ToString();
            string m = CDay.Month.ToString("00");
            string d = CDay.Day.ToString("00");
            string returnStr = "";
            if (type == "footer")
            {
                returnStr = " 中華民國　" + y + "　年　" + m + "　月　" + d + "　日";
            }
            else
            {
                returnStr = y + "年" + m + "月" + d + "日";
            }

            return returnStr;
        }
        private List<TResume> MapResumes(Item itmResumes)
        {
            List<TResume> resumes = new List<TResume>();

            if (itmResumes != null)
            {
                int count = itmResumes.getItemCount();
                for (int i = 0; i < count; i++)
                {
                    Item itmResume = itmResumes.getItemByIndex(i);
                    string id = itmResume.getProperty("id", "");
                    string in_sno = itmResume.getProperty("in_sno", "");
                    string in_apply_year = itmResume.getProperty("in_apply_year", "");
                    int apply_year = GetIntValue(in_apply_year);

                    string tw_apply_year = apply_year > 1911
                        ? (apply_year - 1911).ToString()
                        : "";

                    TResume resume = new TResume
                    {
                        id = id,
                        in_sno = in_sno,
                        in_apply_year = in_apply_year,
                        apply_year = apply_year,
                        tw_apply_year = tw_apply_year,
                        Value = itmResume
                    };

                    resumes.Add(resume);
                }
            }

            return resumes;
        }

        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public Innovator inn { get; set; }

            public string mode { get; set; }
            public string member_type { get; set; }
            public string member_status { get; set; }
            public string committee_resume_id { get; set; }
            public string in_member_unit { get; set; }
            public string in_filter { get; set; }
            public string scene { get; set; }
            public string resume_id { get; set; }
            public string resident_date { get; set; }

            public Item itmLoginResume { get; set; }
            public string in_is_admin { get; set; }

            public Item itmResumeView { get; set; }
            public string view_resume_id { get; set; }
            public string view_member_type { get; set; }

            public Item itmPermit { get; set; }
            public bool isMeetingAdmin { get; set; }
            public bool isCommittee { get; set; }
        }

        private void AppendCommitteeMenu(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            Item itmEmpty = inn.newItem();
            itmEmpty.setType("inn_committee");
            itmEmpty.setProperty("value", "");
            itmEmpty.setProperty("label", "請選擇");
            itmReturn.addRelationship(itmEmpty);

            string sql = @"
                SELECT
                    id AS 'value'
                    , in_name AS 'label'
                FROM
                    IN_RESUME WITH (NOLOCK) 
                WHERE
                    in_member_type = 'area_cmt'
                    AND in_member_role = 'sys_9999'
                ORDER BY 
                    login_name
            ";

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item items = inn.applySQL(sql);
            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                item.setType("inn_committee");
                itmReturn.addRelationship(item);
            }
        }

        private void AppendStatusMenu(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            Item itmEmpty = inn.newItem();
            itmEmpty.setType("inn_status");
            itmEmpty.setProperty("value", "");
            itmEmpty.setProperty("label", "請選擇");
            itmReturn.addRelationship(itmEmpty);

            string sql = @"
                SELECT
                    [value]         AS 'value'
                    , [label_zt]    AS 'label'
                FROM
                    [VALUE] t1 WITH(NOLOCK) 
				INNER JOIN
					[LIST] t2 WITH(NOLOCK)
					ON t2.id = t1.source_id
                WHERE
                    t2.name = 'In_Member_Status'
                ORDER BY 
                    t1.sort_order
            ";

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item items = inn.applySQL(sql);
            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                item.setType("inn_status");
                itmReturn.addRelationship(item);
            }
        }

        #region 匯出
        private void Export(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmResumeView, List<TResume> resumes, Item itmReturn)
        {
            string member_type = itmReturn.getProperty("type", "");
            string view_name = itmResumeView.getProperty("in_name", "");

            string member_type_label = "";

            ClosedXML.Excel.XLWorkbook workbook = new ClosedXML.Excel.XLWorkbook();

            if (resumes.Count > 0)
            {
                switch (member_type)
                {
                    case "mbr":
                        member_type_label = "一般會員";
                        AppendMemberSheet(CCO, strMethodName, inn, workbook, member_type_label, resumes);
                        break;

                    case "vip_mbr":
                        member_type_label = "個人會員";
                        AppendMemberSheet(CCO, strMethodName, inn, workbook, member_type_label, resumes);
                        break;

                    case "vip_minority":
                        member_type_label = "準會員";
                        AppendMemberSheet(CCO, strMethodName, inn, workbook, member_type_label, resumes);
                        break;

                    case "vip_group":
                        member_type_label = "一般團體";
                        AppendGroupSheet(CCO, strMethodName, inn, workbook, member_type_label, resumes);
                        break;

                    case "gym":
                        member_type_label = "道館";
                        AppendGymSheet(CCO, strMethodName, inn, workbook, member_type_label, resumes);
                        break;

                    case "vip_gym":
                        member_type_label = "團體會員";
                        AppendGymSheet(CCO, strMethodName, inn, workbook, member_type_label, resumes);
                        break;

                    case "area_cmt":
                        member_type_label = "縣市委員會";
                        AppendCmtSheet(CCO, strMethodName, inn, workbook, member_type_label, resumes);
                        break;

                    default:
                        break;
                }

                //itmReturn.setProperty("raw_count", itmResumes.getItemCount().ToString());
            }


            Item itmPath = GetExcelPath(CCO, strMethodName, inn, "export_path");

            string export_path = itmPath.getProperty("export_path", "").TrimEnd('\\');
            string ext_name = ".xlsx";
            string xls_name = member_type_label + DateTime.Now.ToString("MMdd_HHmmss");
            string xls_file = export_path + "\\" + xls_name + ext_name;
            string xls_url = xls_name + ext_name;

            workbook.SaveAs(xls_file);

            itmReturn.setProperty("xls_name", xls_url);
        }

        /// <summary>
        /// 抓Y-3年的資訊
        /// </summary>
        /// <param name="CCO"></param>
        /// <param name="strMethodName"></param>
        /// <param name="data">承接資料集</param>
        /// <param name="getValueType">處理functionName</param>
        /// <returns></returns>
        private List<TField> GetPre3YearsData(Aras.Server.Core.CallContext CCO, string strMethodName, List<TField> data, string getValueType)
        {
            List<TField> fields = data;
            List<TField> tempBox = new List<TField>();
            DateTime now = DateTime.Now;
            if (now.Month == 12)
            {
                now = new DateTime(now.Year + 1, 1, 1);
            }

            int nowYer = now.Year;

            tempBox.Add(new TField { property = "in_pay_year3", in_west_year = 2019 });
            tempBox.Add(new TField { property = "in_pay_year2", in_west_year = 2020 });
            tempBox.Add(new TField { property = "in_pay_year1", in_west_year = 2021 });
            tempBox.Add(new TField { property = "in_pay_year4", in_west_year = 2022 });
            tempBox.Add(new TField { property = "in_pay_year5", in_west_year = 2023 });
            tempBox.Add(new TField { property = "in_pay_year6", in_west_year = 2024 });
            tempBox.Add(new TField { property = "in_pay_year7", in_west_year = 2025 });
            tempBox.Add(new TField { property = "in_pay_year8", in_west_year = 2026 });
            tempBox.Add(new TField { property = "in_pay_year9", in_west_year = 2027 });
            tempBox.Add(new TField { property = "in_pay_year10", in_west_year = 2028 });
            tempBox.Add(new TField { property = "in_pay_yea11", in_west_year = 2029 });
            tempBox.Add(new TField { property = "in_pay_year12", in_west_year = 2030 });
            tempBox.Add(new TField { property = "in_pay_year13", in_west_year = 2031 });
            // string customString = tempBox.Find(x => x.in_west_year == 2030).property;

            for (int i = nowYer; i > (nowYer - 4); i--)
            {
                int int_year = i;
                string str_reYear = Convert.ToString(int_year - 1911);
                string str_property = tempBox.Find(x => x.in_west_year == int_year).property;
                switch (getValueType)
                {
                    case "payyear":
                        fields.Add(new TField { property = str_property, title = str_reYear, format = "center", getValue = PayYear, in_west_year = int_year });
                        break;
                    case "payyear2":
                        fields.Add(new TField { property = str_property, title = str_reYear, format = "center", getValue = PayYear2, in_west_year = int_year });
                        break;
                    default:
                        fields.Add(new TField { property = str_property, title = str_reYear, format = "center", getValue = PayYear, in_west_year = int_year });
                        break;
                }
            }
            return fields;
        }


        //附加個人會員 Sheet
        private void AppendMemberSheet(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, ClosedXML.Excel.XLWorkbook workbook, string sheet_name, List<TResume> resumes)
        {
            ClosedXML.Excel.IXLWorksheet sheet = workbook.Worksheets.Add(sheet_name);

            List<TField> fields = new List<TField>();
            fields.Add(new TField { property = "no", title = "編號", format = "center" });
            fields.Add(new TField { property = "in_name", title = "姓名", format = "center" });
            fields.Add(new TField { property = "in_gender", title = "性別", format = "center" });
            fields.Add(new TField { property = "in_sno", title = "身分證/居留證", format = "center" });
            fields.Add(new TField { property = "in_birth_display", title = "生日", format = "date" });

            fields.Add(new TField { property = "in_area", title = "出生地", format = "center" });
            fields.Add(new TField { property = "in_resident_add", title = "戶籍地址", format = "" });
            fields.Add(new TField { property = "in_add", title = "通訊地址", });
            fields.Add(new TField { property = "in_email", title = "電子信箱", format = "" });

            fields.Add(new TField { property = "in_tel_1", title = "住家電話", format = "tel" });
            fields.Add(new TField { property = "in_tel", title = "行動電話", format = "tel" });

            fields.Add(new TField { property = "in_member_status", title = "會員狀態", format = "center" });
            fields.Add(new TField { property = "in_degree_display", title = "段位", format = "center" });

            fields = GetPre3YearsData(CCO, strMethodName, fields, "payyear2");
            // fields.Add(new TField { property = "in_pay_year3", title = "108", format = "center", getValue = PayYear2, in_west_year = 2019 });
            // fields.Add(new TField { property = "in_pay_year2", title = "109", format = "center", getValue = PayYear2, in_west_year = 2020 });
            // fields.Add(new TField { property = "in_pay_year1", title = "110", format = "center", getValue = PayYear2, in_west_year = 2021 });

            fields.Add(new TField { property = "in_reg_date", title = "入會日期", format = "yyy年MM月dd日" });

            // fields.Add(new TField { property = "in_member_last_year", title = "最後繳費年度", format = "center" });
            // fields.Add(new TField { property = "in_member_last_fee", title = "最後繳費金額", format = "$ #,##0" });


            fields.Add(new TField { property = "in_member_pay_no", title = "會費繳費單", format = "center" });
            fields.Add(new TField { property = "in_member_pay_amount", title = "會費金額", format = "$ #,##0" });
            fields.Add(new TField { property = "pay_bool", title = "會費繳納狀態", format = "center" });
            fields.Add(new TField { property = "mt_annual", title = "繳納單年度", format = "center" });

            // fields.Add(new TField { property = "login_name", title = "系統帳號", format = "" });
            fields.Add(new TField { property = "in_sys_note", title = "系統備註", format = "" });
            fields.Add(new TField { property = "in_note", title = "備註", format = "" });


            int wsRow = 1;
            int wsCol = 1;
            int field_count = fields.Count;

            for (int i = 0; i < field_count; i++)
            {
                TField field = fields[i];
                ClosedXML.Excel.IXLCell cell = sheet.Cell(wsRow, wsCol + i);
                SetHeadCell(cell, field.title);
            }
            wsRow++;

            int count = resumes.Count;

            for (int i = 0; i < count; i++)
            {
                var resume = resumes[i];
                Item item = resume.Value;

                string in_add = item.getProperty("in_add", "");
                string in_add_code = item.getProperty("in_add_code", "");

                string in_resident_add = item.getProperty("in_resident_add", "");
                string in_resident_add_code = item.getProperty("in_resident_add_code", "");

                string in_email = item.getProperty("in_email", "");
                string in_reg_date = item.getProperty("in_reg_date", "");
                string in_change_date = item.getProperty("in_change_date", "");

                if (in_email == "na@na.n")
                {
                    item.setProperty("in_email", "");
                }

                item.setProperty("in_birth_display", GetBirthDisplay(item));
                item.setProperty("in_add", in_add_code + in_add);
                item.setProperty("in_resident_add", in_resident_add + in_resident_add_code);
                item.setProperty("in_degree_display", GetDegreeDisplay(item));

                if (in_reg_date.Contains("1900"))
                {
                    item.setProperty("in_reg_date", "");
                }

                if (in_change_date.Contains("1900"))
                {
                    item.setProperty("in_change_date", "");
                }

                item.setProperty("no", (i + 1).ToString());

                SetItemCell(sheet, wsRow, wsCol, resume, fields);

                wsRow++;
            }

            //凍結窗格
            sheet.SheetView.Freeze(1, 1);
        }

        //附加縣市委員會 Sheet
        private void AppendCmtSheet(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, ClosedXML.Excel.XLWorkbook workbook, string sheet_name, List<TResume> resumes)
        {
            ClosedXML.Excel.IXLWorksheet sheet = workbook.Worksheets.Add(sheet_name);

            List<TField> fields = new List<TField>();
            fields.Add(new TField { property = "no", title = "編號", format = "" });
            fields.Add(new TField { property = "in_name", title = "委員會", format = "" });
            fields.Add(new TField { property = "in_principal", title = "主委", format = "" });
            fields.Add(new TField { property = "in_head_coach", title = "總幹事", format = "" });
            fields.Add(new TField { property = "in_tel", title = "行動電話", format = "tel" });

            fields.Add(new TField { property = "in_emrg_contact1", title = "業務承辦人", format = "" });
            fields.Add(new TField { property = "in_emrg_tel1", title = "承辦人電話", format = "tel" });

            fields.Add(new TField { property = "in_add_code", title = "郵遞區號", format = "text" });
            fields.Add(new TField { property = "in_add", title = "通信地址", });

            fields.Add(new TField { property = "in_member_status", title = "會員狀態", format = "center" });

            fields = GetPre3YearsData(CCO, strMethodName, fields, "payyear2");
            fields.Add(new TField { property = "in_apply_year", title = "新申請", format = "center", getValue = ApplyYear });
            fields.Add(new TField { property = "in_member_pay_no", title = "會費繳費單", format = "center" });
            fields.Add(new TField { property = "in_pay_amount_exp", title = "會費金額", format = "$ #,##0" });
            fields.Add(new TField { property = "pay_bool", title = "繳費狀態", format = "center" });
            fields.Add(new TField { property = "mt_annual", title = "繳納單年度", format = "center" });

            fields.Add(new TField { property = "login_name", title = "系統帳號", format = "" });


            int wsRow = 1;
            int wsCol = 1;
            int field_count = fields.Count;

            for (int i = 0; i < field_count; i++)
            {
                TField field = fields[i];
                ClosedXML.Excel.IXLCell cell = sheet.Cell(wsRow, wsCol + i);
                SetHeadCell(cell, field.title);
            }
            wsRow++;

            int count = resumes.Count;

            for (int i = 0; i < count; i++)
            {
                var resume = resumes[i];
                Item item = resume.Value;

                string in_add = item.getProperty("in_add", "");
                string in_add_code = item.getProperty("in_add_code", "");

                string in_resident_add = item.getProperty("in_resident_add", "");
                string in_resident_add_code = item.getProperty("in_resident_add_code", "");

                string in_email = item.getProperty("in_email", "");
                string in_reg_date = item.getProperty("in_reg_date", "");
                string in_change_date = item.getProperty("in_change_date", "");

                if (in_email == "na@na.n")
                {
                    item.setProperty("in_email", "");
                }

                item.setProperty("in_add", in_add_code + in_add);
                item.setProperty("in_resident_add", in_resident_add + in_resident_add_code);

                if (in_reg_date.Contains("1900"))
                {
                    item.setProperty("in_reg_date", "");
                }

                if (in_change_date.Contains("1900"))
                {
                    item.setProperty("in_change_date", "");
                }


                item.setProperty("no", (i + 1).ToString());

                SetItemCell(sheet, wsRow, wsCol, resume, fields);

                wsRow++;
            }

            //凍結窗格
            sheet.SheetView.Freeze(1, 1);
        }

        //附加道館社團 Sheet
        private void AppendGymSheet(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, ClosedXML.Excel.XLWorkbook workbook, string sheet_name, List<TResume> resumes)
        {
            ClosedXML.Excel.IXLWorksheet sheet = workbook.Worksheets.Add(sheet_name);

            List<TField> fields = new List<TField>();
            fields.Add(new TField { property = "no", title = "編號", format = "center" });
            fields.Add(new TField { property = "login_name", title = "系統帳號", format = "" });
            fields.Add(new TField { property = "committee_short_name", title = "所屬跆委會", format = "" });
            fields.Add(new TField { property = "in_name", title = "設立名稱", format = "" });
            fields.Add(new TField { property = "in_resident_id", title = "會員編號", format = "" });
            fields.Add(new TField { property = "in_principal", title = "負責人", format = "" });
            fields.Add(new TField { property = "in_head_coach", title = "總教練", format = "" });
            fields.Add(new TField { property = "in_assistant_coaches", title = "助理教練", format = "" });

            fields.Add(new TField { property = "in_resident_add", title = "道館地址", format = "" });
            fields.Add(new TField { property = "in_add", title = "通信地址", });

            fields.Add(new TField { property = "in_tel_1", title = "住家電話", format = "tel" });
            fields.Add(new TField { property = "in_tel_2", title = "道館電話", format = "tel" });
            fields.Add(new TField { property = "in_tel", title = "行動電話", format = "tel" });
            fields.Add(new TField { property = "in_fax", title = "傳真號碼", format = "tel" });

            fields.Add(new TField { property = "in_url", title = "網路地址", format = "" });
            fields.Add(new TField { property = "in_email", title = "電子信箱", format = "" });
            fields.Add(new TField { property = "in_note", title = "備註", format = "" });

            fields.Add(new TField { property = "in_member_unit", title = "社館", format = "center" });
            fields.Add(new TField { property = "in_reg_date", title = "入會日期", format = "yyy年MM月dd日" });
            fields.Add(new TField { property = "in_change_date", title = "變更日期", format = "yyy年MM月dd日" });

            fields.Add(new TField { property = "in_email_exam", title = "發信狀況", format = "" });
            fields.Add(new TField { property = "in_member_status", title = "會員狀態", format = "center" });

            fields = GetPre3YearsData(CCO, strMethodName, fields, "payyear2");
            fields.Add(new TField { property = "in_apply_year", title = "新申請", format = "center", getValue = ApplyYear });
            fields.Add(new TField { property = "in_member_pay_no", title = "會費繳費單", format = "center" });
            fields.Add(new TField { property = "in_pay_amount_exp", title = "會費金額", format = "$ #,##0" });
            fields.Add(new TField { property = "pay_bool", title = "繳費狀態", format = "center" });
            fields.Add(new TField { property = "mt_annual", title = "繳納單年度", format = "center" });
            fields.Add(new TField { property = "in_sys_note", title = "系統備註", format = "" });


            int wsRow = 1;
            int wsCol = 1;
            int field_count = fields.Count;

            for (int i = 0; i < field_count; i++)
            {
                TField field = fields[i];
                ClosedXML.Excel.IXLCell cell = sheet.Cell(wsRow, wsCol + i);
                SetHeadCell(cell, field.title);
            }
            wsRow++;

            int count = resumes.Count;

            for (int i = 0; i < count; i++)
            {
                var resume = resumes[i];
                Item item = resume.Value;
                string in_add = item.getProperty("in_add", "");
                string in_add_code = item.getProperty("in_add_code", "");

                string in_resident_add = item.getProperty("in_resident_add", "");
                string in_resident_add_code = item.getProperty("in_resident_add_code", "");

                string in_email = item.getProperty("in_email", "");
                string in_reg_date = item.getProperty("in_reg_date", "");
                string in_change_date = item.getProperty("in_change_date", "");

                if (in_email == "na@na.n")
                {
                    item.setProperty("in_email", "");
                }

                item.setProperty("in_add", in_add_code + in_add);
                item.setProperty("in_resident_add", in_resident_add + in_resident_add_code);

                if (in_reg_date.Contains("1900"))
                {
                    item.setProperty("in_reg_date", "");
                }

                if (in_change_date.Contains("1900"))
                {
                    item.setProperty("in_change_date", "");
                }

                item.setProperty("no", (i + 1).ToString());

                SetItemCell(sheet, wsRow, wsCol, resume, fields);

                wsRow++;
            }

            //凍結窗格
            sheet.SheetView.Freeze(1, 1);
        }

        //附加其他團體會員 Sheet
        private void AppendGroupSheet(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, ClosedXML.Excel.XLWorkbook workbook, string sheet_name, List<TResume> resumes)
        {
            ClosedXML.Excel.IXLWorksheet sheet = workbook.Worksheets.Add(sheet_name);

            List<TField> fields = new List<TField>();
            fields.Add(new TField { property = "no", title = "編號", format = "" });
            fields.Add(new TField { property = "login_name", title = "系統帳號", format = "" });
            fields.Add(new TField { property = "in_name", title = "團體會員名稱", format = "" });
            fields.Add(new TField { property = "in_principal", title = "法定代理人", format = "" });
            fields.Add(new TField { property = "in_emrg_contact1", title = "聯絡人", format = "" });
            fields.Add(new TField { property = "in_tel_1", title = "電話", format = "tel" });
            fields.Add(new TField { property = "in_add_code", title = "郵遞區號", format = "text" });
            fields.Add(new TField { property = "in_add", title = "團體會員地址", format = "" });
            //fields.Add(new TField { property = "in_reg_date", title = "入會日期", format = "yyy年MM月dd日" });

            fields.Add(new TField { property = "in_member_status", title = "會員狀態", format = "center" });

            fields = GetPre3YearsData(CCO, strMethodName, fields, "payyear2");
            fields.Add(new TField { property = "in_apply_year", title = "新申請", format = "center", getValue = ApplyYear });
            fields.Add(new TField { property = "in_member_pay_no", title = "會費繳費單", format = "center" });
            fields.Add(new TField { property = "in_pay_amount_exp", title = "會費金額", format = "$ #,##0" });
            fields.Add(new TField { property = "pay_bool", title = "繳費狀態", format = "center" });
            fields.Add(new TField { property = "mt_annual", title = "繳納單年度", format = "center" });
            fields.Add(new TField { property = "in_sys_note", title = "系統備註", format = "" });



            int wsRow = 1;
            int wsCol = 1;
            int field_count = fields.Count;

            for (int i = 0; i < field_count; i++)
            {
                TField field = fields[i];
                ClosedXML.Excel.IXLCell cell = sheet.Cell(wsRow, wsCol + i);
                SetHeadCell(cell, field.title);
            }
            wsRow++;

            int count = resumes.Count;

            for (int i = 0; i < count; i++)
            {
                var resume = resumes[i];
                Item item = resume.Value;

                string in_add = item.getProperty("in_add", "");
                string in_add_code = item.getProperty("in_add_code", "");
                string in_email = item.getProperty("in_email", "");
                string in_reg_date = item.getProperty("in_reg_date", "");

                if (in_email == "na@na.n")
                {
                    item.setProperty("in_email", "");
                }

                item.setProperty("in_add", in_add_code + in_add);

                if (in_reg_date.Contains("1900"))
                {
                    item.setProperty("in_reg_date", "");
                }

                item.setProperty("no", (i + 1).ToString());

                SetItemCell(sheet, wsRow, wsCol, resume, fields);

                wsRow++;
            }

            //凍結窗格
            sheet.SheetView.Freeze(1, 1);
        }

        //設定物件與資料列
        private void SetItemCell(ClosedXML.Excel.IXLWorksheet sheet, int wsRow, int wsCol, TResume resume, List<TField> fields)
        {
            int count = fields.Count;
            for (int i = 0; i < count; i++)
            {
                TField field = fields[i];
                if (field.format == null) field.format = "";

                ClosedXML.Excel.IXLCell cell = sheet.Cell(wsRow, wsCol + i);

                string value = "";

                if (field.getValue != null)
                {
                    value = field.getValue(resume, field);
                }
                else if (field.property != "")
                {
                    value = resume.Value.getProperty(field.property, "");
                }

                SetBodyCell(cell, value, field.format);
            }
        }

        //設定資料列
        private void SetBodyCell(ClosedXML.Excel.IXLCell cell, string value, string format)
        {
            switch (format)
            {
                case "center":
                    cell.Value = value;
                    cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    break;

                case "date":
                    cell.Value = value;
                    cell.Style.NumberFormat.Format = "yyyy/mm/dd";
                    cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    break;

                case "yyyy/MM/dd HH:mm":
                    cell.Value = GetDateTimeValue(value, format, 8);
                    cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    break;

                case "yyyy/MM/dd":
                    cell.Value = GetDateTimeValue(value, format, 8);
                    cell.Style.NumberFormat.Format = "yyyy/mm/dd";
                    cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    break;

                case "yyy年MM月dd日":
                    cell.Value = GetTWDay(value, format, 8);
                    cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    break;

                case "$ #,##0":
                    cell.Value = value;
                    cell.Style.NumberFormat.Format = "$ #,##0";
                    break;

                case "tel":
                    if (value.StartsWith("09"))
                    {
                        cell.Value = value.Replace("-", "");
                        cell.Style.NumberFormat.Format = "0000-000-000";
                        cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    }
                    else
                    {
                        cell.Value = "'" + value;
                        cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    }
                    break;

                case "text":
                    cell.Value = "'" + value;
                    cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    break;

                default:
                    cell.Value = value;
                    break;
            }
            cell.Style.Border.SetRightBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetTopBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetBottomBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetLeftBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
        }

        ClosedXML.Excel.XLColor head_bg_color = ClosedXML.Excel.XLColor.FromHtml("#295C90");

        //設定標題列
        private void SetHeadCell(ClosedXML.Excel.IXLCell cell, string title)
        {
            cell.Value = title;

            cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
            cell.Style.Font.Bold = true;
            cell.DataType = ClosedXML.Excel.XLDataType.Text;
            cell.Style.Fill.BackgroundColor = head_bg_color;
            cell.Style.Font.FontColor = ClosedXML.Excel.XLColor.White;

            cell.Style.Border.SetRightBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetTopBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetBottomBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetLeftBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
        }
        private Item GetExcelPath(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string in_name)
        {
            Item itmResult = inn.newItem();

            //Excel 匯出路徑 (from 原創.設定參數)
            string sql = @"
            SELECT 
                TOP 1 t2.in_name AS 'variable', t1.in_name, t1.in_value 
            FROM 
                In_Variable_Detail t1 WITH(NOLOCK)
            INNER JOIN 
                In_Variable t2 WITH(NOLOCK) ON t2.ID = t1.SOURCE_ID 
            WHERE 
                t2.in_name = N'meeting_excel'  
                AND t1.in_name = N'{#in_name}'
            ";

            sql = sql.Replace("{#in_name}", in_name);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item item = inn.applySQL(sql);

            if (item.isError())
            {
                throw new Exception("未設定匯出路徑 _# " + in_name);
            }

            itmResult.setProperty("export_path", item.getProperty("in_value", ""));

            return itmResult;
        }
        #endregion 匯出

        private void Query(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmResumeView, List<TResume> resumes, Item itmPermit, Item itmReturn)
        {
            string member_type = itmReturn.getProperty("type", "");
            string view_name = itmResumeView.getProperty("in_name", "");

            string member_type_label = "";

            switch (member_type)
            {
                case "mbr":
                    member_type_label = "一般會員";
                    AppendMemberTable(CCO, strMethodName, inn, resumes, itmPermit, itmReturn);
                    break;

                case "vip_mbr":
                    member_type_label = "個人會員";
                    AppendVipMemberTable(CCO, strMethodName, inn, resumes, itmPermit, itmReturn);
                    break;

                case "vip_minority":
                    member_type_label = "準會員";
                    AppendVipMemberTable(CCO, strMethodName, inn, resumes, itmPermit, itmReturn);
                    break;

                case "vip_group":
                    member_type_label = "一般團體";
                    AppendGroupTable(CCO, strMethodName, inn, resumes, itmPermit, itmReturn);
                    break;

                case "gym":
                    member_type_label = "道館";
                    AppendGymTable(CCO, strMethodName, inn, resumes, itmPermit, itmReturn);
                    break;

                case "vip_gym":
                    member_type_label = "團體會員";
                    AppendGymTable(CCO, strMethodName, inn, resumes, itmPermit, itmReturn);
                    break;

                case "area_cmt":
                    member_type_label = "縣市委員會";
                    AppendCmtTable(CCO, strMethodName, inn, resumes, itmPermit, itmReturn);
                    break;

                case "prjt_cmt":
                    member_type_label = "專項委員會";
                    AppendCmtTable(CCO, strMethodName, inn, resumes, itmPermit, itmReturn);
                    break;

                default:
                    break;
            }


            itmReturn.setProperty("page_title", member_type_label);
            itmReturn.setProperty("page_sub_title", view_name);
        }

        //附加委員會 Table
        private void AppendCmtTable(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, List<TResume> resumes, Item itmPermit, Item itmReturn)
        {
            bool isMeetingAdmin = itmPermit.getProperty("isMeetingAdmin", "") == "1";
            bool isCommittee = itmPermit.getProperty("isCommittee", "") == "1";

            List<TField> fields = new List<TField>();
            fields.Add(new TField { property = "in_name", title = "委員會", css = "text-left", hide = true });
            fields.Add(new TField { property = "in_member_pay_no", title = "繳費單號", css = "text-left", hide = true });
            fields.Add(new TField { property = "in_sno", title = "系統帳號", css = "text-left", hide = false });

            fields.Add(new TField { property = "in_link", title = "委員會", css = "text-left" });
            fields.Add(new TField { property = "in_principal", title = "主委", css = "text-left" });
            fields.Add(new TField { property = "in_head_coach", title = "總幹事", css = "text-left" });
            fields.Add(new TField { property = "in_tel", title = "行動電話", css = "text-left" });
            fields.Add(new TField { property = "in_emrg_contact1", title = "業務<br>承辦人", css = "text-left" });
            fields.Add(new TField { property = "in_emrg_tel1", title = "承辦人<br>電話", css = "text-left" });
            //fields.Add(new TField { property = "in_add_code", title = "郵遞<br>區號", css = "text-left" });
            //fields.Add(new TField { property = "in_add", title = "通信地址", css = "text-left",  });
            fields.Add(new TField { property = "in_member_status", title = "會員<br>狀態", css = "text-left" });
            fields.Add(new TField { property = "in_member_last_year", title = "最後<br>繳費年度", css = "text-left" });
            fields.Add(new TField { property = "in_pay_link", title = "會員年費", css = "text-center" });
            fields.Add(new TField { property = "pay_bool", title = "繳費狀態", css = "text-center" });

            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            head.AppendLine("<thead>");
            foreach (var field in fields)
            {
                if (field.hide)
                {
                    head.AppendLine("<th class='text-center' data-field='" + field.property + "' data-sortable='true' data-visible='false'>" + field.title + "</th>");
                }
                else
                {
                    head.AppendLine("<th class='text-center' data-field='" + field.property + "' data-sortable='true'>" + field.title + "</th>");
                }
            }
            head.AppendLine("</thead>");

            int count = resumes.Count;

            body.AppendLine("<tbody>");
            for (int i = 0; i < count; i++)
            {
                Item item = resumes[i].Value;
                item.setProperty("in_link", GetDashboardLink(item));
                //item.setProperty("inn_funcs", GetFuncsLink(item));
                item.setProperty("in_pay_link", GetPayLink(item, isMeetingAdmin));

                body.AppendLine("<tr>");
                foreach (var field in fields)
                {
                    body.AppendLine("<td class='" + field.css + "' data-field='" + field.property + "' >" + field.GetStr(item) + "</td>");
                }
                body.AppendLine("</tr>");
            }
            body.AppendLine("</tbody>");


            string table_name = "resume_table";

            StringBuilder builder = new StringBuilder();
            builder.AppendLine(GetTableAttribute(table_name));
            builder.Append(head);
            builder.Append(body);
            builder.AppendLine("</table>");
            builder.AppendLine("<script>");
            builder.AppendLine("$('#" + table_name + "').bootstrapTable();");
            builder.AppendLine("if ($(window).width() <= 768) { $('#" + table_name + "').bootstrapTable('toggleView'); }");
            builder.AppendLine("</script>");

            itmReturn.setProperty("inn_table", builder.ToString());
        }

        //附加其他團體會員 Table
        private void AppendGroupTable(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, List<TResume> resumes, Item itmPermit, Item itmReturn)
        {
            bool isMeetingAdmin = itmPermit.getProperty("isMeetingAdmin", "") == "1";
            bool isCommittee = itmPermit.getProperty("isCommittee", "") == "1";

            List<TField> fields = new List<TField>();
            fields.Add(new TField { property = "in_name", title = "團體會員名稱", css = "text-left", hide = true });
            fields.Add(new TField { property = "in_member_pay_no", title = "繳費單號", css = "text-left", hide = true });
            fields.Add(new TField { property = "in_sno", title = "系統帳號", css = "text-left", hide = false });

            fields.Add(new TField { property = "in_link", title = "團體會員名稱", css = "text-left" });
            fields.Add(new TField { property = "in_principal", title = "法定代理人", css = "text-left" });
            fields.Add(new TField { property = "in_emrg_contact1", title = "聯絡人", css = "text-left" });
            fields.Add(new TField { property = "in_tel_1", title = "電話", css = "text-left" });
            fields.Add(new TField { property = "in_add_code", title = "郵遞區號", css = "text-left" });
            fields.Add(new TField { property = "in_add", title = "團體會員地址", css = "text-left" });
            fields.Add(new TField { property = "in_member_status", title = "會員<br>狀態", css = "text-left" });
            fields.Add(new TField { property = "in_member_last_year", title = "最後<br>繳費年度", css = "text-left" });
            fields.Add(new TField { property = "in_pay_link", title = "會員年費", css = "text-center" });
            fields.Add(new TField { property = "pay_bool", title = "繳費狀態", css = "text-center" });

            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            head.AppendLine("<thead>");
            foreach (var field in fields)
            {
                if (field.hide)
                {
                    head.AppendLine("<th class='text-center' data-field='" + field.property + "' data-sortable='true' data-visible='false'>" + field.title + "</th>");
                }
                else
                {
                    head.AppendLine("<th class='text-center' data-field='" + field.property + "' data-sortable='true'>" + field.title + "</th>");
                }
            }
            head.AppendLine("</thead>");

            int count = resumes.Count;

            body.AppendLine("<tbody>");
            for (int i = 0; i < count; i++)
            {
                Item item = resumes[i].Value;
                item.setProperty("in_link", GetDashboardLink(item));
                //item.setProperty("inn_funcs", GetFuncsLink(item));
                item.setProperty("in_pay_link", GetPayLink(item, isMeetingAdmin));

                body.AppendLine("<tr>");
                foreach (var field in fields)
                {
                    body.AppendLine("<td class='" + field.css + "' data-field='" + field.property + "' >" + field.GetStr(item) + "</td>");
                }
                body.AppendLine("</tr>");
            }
            body.AppendLine("</tbody>");


            string table_name = "resume_table";

            StringBuilder builder = new StringBuilder();
            builder.AppendLine(GetTableAttribute(table_name));
            builder.Append(head);
            builder.Append(body);
            builder.AppendLine("</table>");
            builder.AppendLine("<script>");
            builder.AppendLine("$('#" + table_name + "').bootstrapTable();");
            builder.AppendLine("if ($(window).width() <= 768) { $('#" + table_name + "').bootstrapTable('toggleView'); }");
            builder.AppendLine("</script>");

            itmReturn.setProperty("inn_table", builder.ToString());
        }

        //附加道館社團 Table
        private void AppendGymTable(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, List<TResume> resumes, Item itmPermit, Item itmReturn)
        {
            bool isMeetingAdmin = itmPermit.getProperty("isMeetingAdmin", "") == "1";
            bool isCommittee = itmPermit.getProperty("isCommittee", "") == "1";

            List<TField> fields = new List<TField>();
            fields.Add(new TField { property = "in_name", title = "設立名稱", css = "text-left", hide = true });
            fields.Add(new TField { property = "in_member_pay_no", title = "繳費單號", css = "text-left", hide = true });
            fields.Add(new TField { property = "in_sno", title = "系統帳號", css = "text-left", hide = false });

            fields.Add(new TField { property = "in_link", title = "設立名稱", css = "text-left" });
            // fields.Add(new TField { property = "in_principal", title = "負責人", css = "text-left" });
            // fields.Add(new TField { property = "in_head_coach", title = "總教練", css = "text-left" });
            fields.Add(new TField { property = "in_member_unit", title = "社館", css = "text-left" });
            fields.Add(new TField { property = "committee_short_name", title = "所屬跆委會", css = "text-left" });
            fields.Add(new TField { property = "in_email_exam", title = "發信<br>狀況", css = "text-left" });
            fields.Add(new TField { property = "in_member_status", title = "會員<br>狀態", css = "text-left" });
            fields = GetPre3YearsData(CCO, strMethodName, fields, "payyear");
            // fields.Add(new TField { property = "in_pay_year3", title = "108", css = "text-center", getValue = PayYear, in_west_year = 2019 });
            // fields.Add(new TField { property = "in_pay_year2", title = "109", css = "text-center", getValue = PayYear, in_west_year = 2020 });
            // fields.Add(new TField { property = "in_pay_year1", title = "110", css = "text-center", getValue = PayYear, in_west_year = 2021 });
            fields.Add(new TField { property = "in_apply_year", title = "新申請", css = "text-center", getValue = ApplyYear });
            // fields.Add(new TField { property = "in_member_last_year", title = "最後<br>繳費年度", css = "text-left" });
            fields.Add(new TField { property = "in_pay_link", title = "會員年費", css = "text-center" });
            // fields.Add(new TField { property = "pay_bool", title = "繳費狀態", css = "text-center" });
            // fields.Add(new TField { title = "繳費單", css = "text-center", getValue = CreatePayBtn });
            fields.Add(new TField { title = "功能", css = "text-center", getValue = GymViewBtn });

            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            head.AppendLine("<thead>");
            foreach (var field in fields)
            {
                if (field.hide)
                {
                    head.AppendLine("<th class='text-center' data-field='" + field.property + "' data-sortable='true' data-visible='false'>" + field.title + "</th>");
                }
                else
                {
                    head.AppendLine("<th class='text-center' data-field='" + field.property + "' data-sortable='true'>" + field.title + "</th>");
                }
            }
            head.AppendLine("</thead>");

            int count = resumes.Count;

            body.AppendLine("<tbody>");
            for (int i = 0; i < count; i++)
            {
                var resume = resumes[i];
                var item = resume.Value;

                item.setProperty("in_link", GetDashboardLink(item));
                //item.setProperty("inn_funcs", GetFuncsLink(item));
                item.setProperty("in_pay_link", GetPayLink(item, isMeetingAdmin));

                body.AppendLine("<tr>");
                foreach (var field in fields)
                {
                    string value = "";

                    if (field.getValue != null)
                    {
                        value = field.getValue(resume, field);
                    }
                    else
                    {
                        value = field.GetStr(item);
                    }

                    body.AppendLine("<td class='" + field.css + "' data-field='" + field.property + "' >" + value + "</td>");
                }
                body.AppendLine("</tr>");
            }
            body.AppendLine("</tbody>");


            string table_name = "resume_table";

            StringBuilder builder = new StringBuilder();
            builder.AppendLine(GetTableAttribute(table_name));
            builder.Append(head);
            builder.Append(body);
            builder.AppendLine("</table>");
            builder.AppendLine("<script>");
            builder.AppendLine("$('#" + table_name + "').bootstrapTable();");
            builder.AppendLine("if ($(window).width() <= 768) { $('#" + table_name + "').bootstrapTable('toggleView'); }");
            builder.AppendLine("</script>");

            itmReturn.setProperty("inn_table", builder.ToString());
        }

        private string ApplyYear(TResume resume, TField field)
        {
            return resume.tw_apply_year;
        }

        private string PayYear(TResume resume, TField field)
        {
            string value = resume.Value.getProperty(field.property, "");

            if (resume.apply_year > field.in_west_year)
            {
                return "-";
            }

            if (value == "1")
            {
                return "V";
            }
            else
            {
                return "<span style='color: red'>欠繳</span>";
            }
        }

        private string PayYear2(TResume resume, TField field)
        {
            string value = resume.Value.getProperty(field.property, "");

            if (resume.apply_year > field.in_west_year)
            {
                return "-";
            }

            if (value == "1")
            {
                return "V";
            }
            else
            {
                return "欠繳";
            }
        }

        //附加個人會員 Table
        private void AppendVipMemberTable(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, List<TResume> resumes, Item itmPermit, Item itmReturn)
        {
            bool isMeetingAdmin = itmPermit.getProperty("isMeetingAdmin", "") == "1";
            bool isCommittee = itmPermit.getProperty("isCommittee", "") == "1";

            List<TField> fields = new List<TField>();
            fields.Add(new TField { property = "in_name", title = "姓名", css = "text-left", hide = true });
            fields.Add(new TField { property = "in_sno", title = "系統帳號", css = "text-left", hide = true });
            fields.Add(new TField { property = "in_member_pay_no", title = "繳費單號", css = "text-left", hide = true });

            fields.Add(new TField { property = "in_link", title = "姓名", css = "text-left" });
            fields.Add(new TField { property = "in_gender", title = "性別", css = "text-left" });
            fields.Add(new TField { property = "in_sno_display", title = "身分證號", css = "text-left" });
            fields.Add(new TField { property = "in_birth_display", title = "生日", css = "text-left" });
            fields.Add(new TField { property = "in_tel", title = "行動電話", css = "text-left" });
            // fields.Add(new TField { property = "in_add", title = "通信地址", css = "text-left" });
            // fields.Add(new TField { property = "in_email_exam", title = "發信狀況", css = "text-left" });
            fields.Add(new TField { property = "in_member_status", title = "會員狀態", css = "text-left" });
            fields.Add(new TField { property = "in_degree_display", title = "段位", css = "text-left" });

            fields = GetPre3YearsData(CCO, strMethodName, fields, "payyear");
            // fields.Add(new TField { property = "in_pay_year3", title = "108", css = "text-center", getValue = PayYear, in_west_year = 2019 });
            // fields.Add(new TField { property = "in_pay_year2", title = "109", css = "text-center", getValue = PayYear, in_west_year = 2020 });
            // fields.Add(new TField { property = "in_pay_year1", title = "110", css = "text-center", getValue = PayYear, in_west_year = 2021 });
            fields.Add(new TField { property = "in_apply_year", title = "入會年", css = "text-center", getValue = ApplyYear });
            fields.Add(new TField { property = "in_pay_link", title = "會員年費", css = "text-center" });

            fields.Add(new TField { title = "功能", css = "text-center", getValue = MemberViewBtn });

            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            head.AppendLine("<thead>");
            foreach (var field in fields)
            {
                if (field.hide)
                {
                    head.AppendLine("<th class='text-center' data-field='" + field.property + "' data-sortable='true' data-visible='false'>" + field.title + "</th>");
                }
                else
                {
                    head.AppendLine("<th class='text-center' data-field='" + field.property + "' data-sortable='true'>" + field.title + "</th>");
                }
            }
            head.AppendLine("</thead>");

            int count = resumes.Count;

            body.AppendLine("<tbody>");
            for (int i = 0; i < count; i++)
            {
                var resume = resumes[i];
                Item item = resume.Value;
                item.setProperty("in_link", GetDashboardLink(item));
                item.setProperty("in_sno_display", GetSidDisplay(item));
                item.setProperty("in_birth_display", GetBirthDisplay(item));
                item.setProperty("in_degree_display", GetDegreeDisplay(item));
                item.setProperty("in_pay_link", GetPayLink(item, isMeetingAdmin));

                body.AppendLine("<tr>");
                foreach (var field in fields)
                {
                    string value = "";

                    if (field.getValue != null)
                    {
                        value = field.getValue(resume, field);
                    }
                    else
                    {
                        value = field.GetStr(item);
                    }

                    body.AppendLine("<td class='" + field.css + "' data-field='" + field.property + "' >" + value + "</td>");
                }
                body.AppendLine("</tr>");
            }
            body.AppendLine("</tbody>");


            string table_name = "resume_table";

            StringBuilder builder = new StringBuilder();
            builder.AppendLine(GetTableAttribute(table_name));
            builder.Append(head);
            builder.Append(body);
            builder.AppendLine("</table>");
            builder.AppendLine("<script>");
            builder.AppendLine("$('#" + table_name + "').bootstrapTable();");
            builder.AppendLine("if ($(window).width() <= 768) { $('#" + table_name + "').bootstrapTable('toggleView'); }");
            builder.AppendLine("</script>");

            itmReturn.setProperty("inn_table", builder.ToString());
        }

        //附加個人資料 Table
        private void AppendMemberTable(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, List<TResume> resumes, Item itmPermit, Item itmReturn)
        {
            bool isMeetingAdmin = itmPermit.getProperty("isMeetingAdmin", "") == "1";
            bool isCommittee = itmPermit.getProperty("isCommittee", "") == "1";

            List<TField> fields = new List<TField>();
            fields.Add(new TField { property = "in_name", title = "姓名", css = "text-left", hide = true });
            fields.Add(new TField { property = "in_sno", title = "系統帳號", css = "text-left", hide = true });
            fields.Add(new TField { property = "in_member_pay_no", title = "繳費單號", css = "text-left", hide = true });

            fields.Add(new TField { property = "in_link", title = "姓名", css = "text-left" });
            fields.Add(new TField { property = "in_gender", title = "性別", css = "text-left" });
            fields.Add(new TField { property = "in_sno_display", title = "身分證號", css = "text-left" });
            fields.Add(new TField { property = "in_birth_display", title = "生日", css = "text-left" });
            fields.Add(new TField { property = "in_tel", title = "行動電話", css = "text-left" });
            // fields.Add(new TField { property = "in_add", title = "通信地址", css = "text-left" });
            fields.Add(new TField { property = "in_email_exam", title = "發信狀況", css = "text-left" });
            fields.Add(new TField { property = "in_member_status", title = "會員狀態", css = "text-left" });
            fields.Add(new TField { property = "in_degree_display", title = "段位", css = "text-left" });
            fields.Add(new TField { property = "in_member_last_year", title = "最後<br>繳費年度", css = "text-left" });
            fields.Add(new TField { property = "in_pay_link", title = "會員年費", css = "text-center" });
            fields.Add(new TField { property = "pay_bool", title = "繳費狀態", css = "text-center" });

            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            head.AppendLine("<thead>");
            foreach (var field in fields)
            {
                if (field.hide)
                {
                    head.AppendLine("<th class='text-center' data-field='" + field.property + "' data-sortable='true' data-visible='false'>" + field.title + "</th>");
                }
                else
                {
                    head.AppendLine("<th class='text-center' data-field='" + field.property + "' data-sortable='true'>" + field.title + "</th>");
                }
            }
            head.AppendLine("</thead>");

            int count = resumes.Count;

            body.AppendLine("<tbody>");
            for (int i = 0; i < count; i++)
            {
                Item item = resumes[i].Value;
                item.setProperty("in_link", GetDashboardLink(item));
                item.setProperty("in_sno_display", GetSidDisplay(item));
                item.setProperty("in_birth_display", GetBirthDisplay(item));
                item.setProperty("in_degree_display", GetDegreeDisplay(item));
                item.setProperty("in_pay_link", GetPayLink(item, isMeetingAdmin));

                body.AppendLine("<tr>");
                foreach (var field in fields)
                {
                    body.AppendLine("<td class='" + field.css + "' data-field='" + field.property + "' >" + field.GetStr(item) + "</td>");
                }
                body.AppendLine("</tr>");
            }
            body.AppendLine("</tbody>");


            string table_name = "resume_table";

            StringBuilder builder = new StringBuilder();
            builder.AppendLine(GetTableAttribute(table_name));
            builder.Append(head);
            builder.Append(body);
            builder.AppendLine("</table>");
            builder.AppendLine("<script>");
            builder.AppendLine("$('#" + table_name + "').bootstrapTable();");
            builder.AppendLine("if ($(window).width() <= 768) { $('#" + table_name + "').bootstrapTable('toggleView'); }");
            builder.AppendLine("</script>");

            itmReturn.setProperty("inn_table", builder.ToString());
        }

        private string GetTableAttribute(string table_name)
        {
            return "<table id='" + table_name + "' class='table table-hover table-bordered table-rwd rwd rwdtable' "
                + " data-toggle='table' "
                + " data-click-to-select='true' "
                + " data-striped='false' "
                + " data-search='false' "
                + " data-pagination='true' "
                + " data-page-size='10' "
                + " data-show-pagination-switch='false'"
                + ">";
        }

        //取得成員
        private Item GetVipMembers(TConfig cfg, string view_resume_id)
        {
            string in_member_type = cfg.member_type;
            string in_member_status = cfg.member_status;
            string in_member_unit = cfg.in_member_unit;

            string manager_condition = view_resume_id != "" ? " AND t1.in_manager_org = '" + view_resume_id + "' " : "";
            string status_condition = in_member_status != "" ? " AND t1.in_member_status = '" + in_member_status + "' " : "";

            string unit_condition = "";
            if (in_member_unit == "0")
            {
                unit_condition = " AND ISNULL(t1.in_member_unit, '') = ''";
            }
            else if (in_member_unit != "")
            {
                unit_condition = " AND ISNULL(t1.in_member_unit, '') = N'" + in_member_unit + "'";
            }

            string filter_condition = "";
            if (cfg.in_filter != "")
            {
                //單位帳號格式 A~Z 0~9(9)(8~12位英數字)
                var custom_regex = new System.Text.RegularExpressions.Regex("^[A-Za-z0-9]{8,12}$");
                if (custom_regex.IsMatch(cfg.in_filter))
                {
                    filter_condition = "AND t1.in_sno = '" + cfg.in_filter + "'";
                }
                else
                {
                    List<string> or_cond = new List<string>();
                    or_cond.Add("(t1.in_name LIKE '%" + cfg.in_filter + "%')");
                    or_cond.Add("(t1.in_sno LIKE '%" + cfg.in_filter + "%')");
                    or_cond.Add("(t1.in_gender LIKE '%" + cfg.in_filter + "%')");
                    or_cond.Add("(t1.in_birth LIKE '%" + cfg.in_filter + "%')");
                    or_cond.Add("(t1.in_tel LIKE '%" + cfg.in_filter + "%')");
                    or_cond.Add("(t1.in_current_org LIKE '%" + cfg.in_filter + "%')");
                    or_cond.Add("(t1.in_email LIKE '%" + cfg.in_filter + "%')");
                    or_cond.Add("(t3.in_pay_amount_exp LIKE '" + cfg.in_filter + "%')");
                    filter_condition = "AND ( " + string.Join(" OR ", or_cond) + " )";
                }
            }

            string sql = @"
                SELECT TOP 2000
	                t1.*
	                , t2.in_short_org AS 'committee_short_name'
	                , t3.in_meeting   AS 'pay_meeting'
	                , t3.pay_bool
	                , t3.in_pay_amount_exp
	                , t4.in_annual    AS 'mt_annual'
                FROM 
	                IN_RESUME t1 WITH(NOLOCK)
				LEFT OUTER JOIN
					IN_RESUME t2 WITH(NOLOCK)
					ON t2.id = t1.in_manager_org
				LEFT OUTER JOIN
				    IN_MEETING_PAY t3 WITH(NOLOCK)
				    ON t3.item_number = t1.in_member_pay_no
				LEFT OUTER JOIN
				    IN_MEETING t4 WITH(NOLOCK)
				    ON t4.id = t3.in_meeting
                WHERE 
                    {#mtype}
	                AND t1.in_sno NOT IN ('M001', 'OG0001')
                    AND t1.in_member_status NOT IN ('未入會')
	                {#status_condition}
                    {#manager_condition}
                    {#unit_condition}
                    {#filter_condition}
                ORDER BY 
                    t1.in_degree DESC
                    , t1.in_birth
            ";

            string mtype = in_member_type == "vip_mbr"
                ? "t1.in_member_type IN ('asc', 'vip_mbr')"
                : "t1.in_member_type = '" + in_member_type + "'";

            sql = sql.Replace("{#mtype}", mtype)
                .Replace("{#status_condition}", status_condition)
                .Replace("{#unit_condition}", unit_condition)
                .Replace("{#manager_condition}", manager_condition)
                .Replace("{#filter_condition}", filter_condition);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        //取得成員
        private Item GetMembers(TConfig cfg, string view_resume_id)
        {
            string in_member_type = cfg.member_type;
            string in_member_status = cfg.member_status;
            string in_member_unit = cfg.in_member_unit;
            string in_resident_date = cfg.resident_date;

            string manager_condition = view_resume_id != "" ? " AND t1.in_manager_org = '" + view_resume_id + "' " : "";
            string status_condition = in_member_status != "" ? " AND t1.in_member_status = '" + in_member_status + "' " : "";
            string resDate_condition = in_resident_date != "" ? " AND t1.in_resident_date = '" + in_resident_date + "' " : "";

            string unit_condition = "";
            if (in_member_unit == "0")
            {
                unit_condition = " AND ISNULL(t1.in_member_unit, '') = ''";
            }
            else if (in_member_unit != "")
            {
                unit_condition = " AND ISNULL(t1.in_member_unit, '') = N'" + in_member_unit + "'";
            }

            string filter_condition = "";
            if (cfg.in_filter != "")
            {
                //單位帳號格式 A~Z 0~9(9)(8~12位英數字)
                var custom_regex = new System.Text.RegularExpressions.Regex("^[A-Za-z0-9]{8,12}$");
                if (custom_regex.IsMatch(cfg.in_filter))
                {
                    filter_condition = "AND t1.in_sno = '" + cfg.in_filter + "'";
                }
                else
                {
                    List<string> or_cond = new List<string>();
                    or_cond.Add("(t1.in_name LIKE '%" + cfg.in_filter + "%')");
                    or_cond.Add("(t1.in_sno LIKE '%" + cfg.in_filter + "%')");
                    or_cond.Add("(t1.in_gender LIKE '%" + cfg.in_filter + "%')");
                    or_cond.Add("(t1.in_birth LIKE '%" + cfg.in_filter + "%')");
                    or_cond.Add("(t1.in_tel LIKE '%" + cfg.in_filter + "%')");
                    or_cond.Add("(t1.in_current_org LIKE '%" + cfg.in_filter + "%')");
                    or_cond.Add("(t1.in_email LIKE '%" + cfg.in_filter + "%')");
                    or_cond.Add("(t3.in_pay_amount_exp LIKE '" + cfg.in_filter + "%')");
                    filter_condition = "AND ( " + string.Join(" OR ", or_cond) + " )";
                }
            }

            string sql = @"
                SELECT TOP 2000
	                t1.*
	                , t2.in_short_org AS 'committee_short_name'
	                , t3.in_meeting   AS 'pay_meeting'
	                , t3.pay_bool
	                , t3.in_pay_amount_exp
	                , t4.in_annual    AS 'mt_annual'
                FROM 
	                IN_RESUME t1 WITH(NOLOCK)
				LEFT OUTER JOIN
					IN_RESUME t2 WITH(NOLOCK)
					ON t2.id = t1.in_manager_org
				LEFT OUTER JOIN
				    IN_MEETING_PAY t3 WITH(NOLOCK)
				    ON t3.item_number = t1.in_member_pay_no
				LEFT OUTER JOIN
				    IN_MEETING t4 WITH(NOLOCK)
				    ON t4.id = t3.in_meeting
                WHERE 
	                t1.in_member_role = 'sys_9999'
	                AND t1.in_member_type IN ('{#in_member_type}')
	                {#status_condition}
                    {#manager_condition}
                    {#resDate_condition}
                    {#unit_condition}
                    {#filter_condition}
                ORDER BY 
                    CASE WHEN t2.login_name IS NULL THEN 1 ELSE 0 END
                    , t1.login_name
            ";

            sql = sql.Replace("{#in_member_type}", in_member_type)
                .Replace("{#status_condition}", status_condition)
                .Replace("{#unit_condition}", unit_condition)
                .Replace("{#manager_condition}", manager_condition)
                .Replace("{#resDate_condition}", resDate_condition)
                .Replace("{#filter_condition}", filter_condition);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);

        }

        //取得成員
        private string GetMembersCount(TConfig cfg, string view_resume_id, string in_member_type, bool isVipMember)
        {
            string manager_condition = view_resume_id != "" ? " AND t1.in_manager_org = '" + view_resume_id + "' " : "";
            string vip_member_cond = isVipMember ? "AND t1.in_member_status NOT IN ('未入會')" : "";

            string sql = @"
                SELECT 
	                count(*) AS 'raw_count'
                FROM 
	                IN_RESUME t1 WITH(NOLOCK)
                WHERE 
	                t1.in_member_role = 'sys_9999'
	                AND t1.in_member_type = '{#in_member_type}'
                    {#manager_condition}
            ";

            sql = sql.Replace("{#in_member_type}", in_member_type)
                .Replace("{#manager_condition}", manager_condition)
                .Replace("{#vip_member_cond}", vip_member_cond);

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item itmResult = cfg.inn.applySQL(sql);

            if (itmResult.isError() || itmResult.getResult() == "")
            {
                return "0";
            }
            else
            {
                return itmResult.getProperty("raw_count", "");
            }
        }

        //取得檢視對象 Resume
        private Item GetTargetResume(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmLoginResume, Item itmReturn)
        {
            itmLoginResume.setType("In_Resume");
            itmLoginResume.setProperty("target_resume_id", itmReturn.getProperty("id", ""));
            itmLoginResume.setProperty("login_resume_id", itmLoginResume.getID());
            itmLoginResume.setProperty("login_member_type", itmLoginResume.getProperty("in_member_type", ""));
            itmLoginResume.setProperty("login_is_admin", itmLoginResume.getProperty("in_is_admin", ""));
            return itmLoginResume.apply("In_Get_Target_Resume");
        }

        private class TResume
        {
            public string id { get; set; }
            public string in_sno { get; set; }
            public string in_apply_year { get; set; }
            public int apply_year { get; set; }
            public string tw_apply_year { get; set; }
            public Item Value { get; set; }
        }

        private class TField
        {
            public string property { get; set; }
            public string title { get; set; }
            public string css { get; set; }
            public string format { get; set; }
            public bool hide { get; set; }
            public int in_west_year { get; set; }
            public Func<TResume, TField, string> getValue { get; set; }

            public string GetStr(Item item)
            {
                return item.getProperty(this.property, "");
            }
        }


        private string GetDashboardLink(Item itmResume)
        {
            string resume_id = itmResume.getProperty("id", "");
            string in_name = itmResume.getProperty("in_name", "");
            string in_org = itmResume.getProperty("in_org", "");
            string in_member_type = itmResume.getProperty("in_member_type", "");

            if (in_org == "1")
            {
                if (in_member_type == "area_cmt")
                {
                    return "<a href='../pages/c.aspx?page=ManagerView.html&method=In_Group_Resume_View&id=" + resume_id + "' >"
                        + in_name
                        + "</a>";
                }
                else if (in_member_type == "vip_group")
                {
                    return "<a href='../pages/c.aspx?page=GroupMemberView.html&method=In_Group_Resume_View&id=" + resume_id + "' >"
                        + in_name
                        + "</a>";
                }
                else
                {
                    return "<a href='../pages/c.aspx?page=GymMemberView.html&method=In_Group_Resume_View&id=" + resume_id + "' >"
                        + in_name
                        + "</a>";
                }
            }
            else
            {
                //onclick=\"ajaxindicatorstart('載入中...');\"
                return "<a href='../pages/c.aspx?page=SingleMemberView.html&method=In_Group_Resume_View&id=" + resume_id + "' >"
                    + in_name
                    + "</a>";
            }
        }

        private string CreatePayBtn(TResume resume, TField field)
        {
            return "<button class='btn btn-sm btn-primary' onclick='CreatePayment_Click(this)'"
                + " data-sid='" + resume.in_sno + "'>"
                + "重建"
                + "</button>";
        }

        private string GymViewBtn(TResume resume, TField field)
        {
            string str = "";
            str = "<button class='btn btn-sm btn-primary keyCert' onclick='GymView_Click(this)' data-rid='" + resume.id + "'>"
                + "檢視"
                + "</button>";

            // if (resume.Value.getProperty("in_member_status", "") =="合格會員")
            // {
            //     str += "&nbsp;<button class='btn btn-sm btn-primary' onclick='exportCertPDF_Click(this)' data-rid='" + resume.id + "'>"
            //     + "證書"
            //     + "</button>";
            // }
            return str;
        }

        private string MemberViewBtn(TResume resume, TField field)
        {
            return "<button class='btn btn-sm btn-primary' onclick='MemberView_Click(this)' data-rid='" + resume.id + "'>"
                + "檢視"
                + "</button>";
        }

        private string GetPayLink(Item itmResume, bool isMeetingAdmin)
        {
            string in_sno = itmResume.getProperty("in_sno", "");
            string in_member_pay_no = itmResume.getProperty("in_member_pay_no", "");
            string in_member_pay_amount = itmResume.getProperty("in_member_pay_amount", "");
            string pay_meeting = itmResume.getProperty("pay_meeting", "");
            string pay_bool = itmResume.getProperty("pay_bool", "");

            string inn_new_apply = "0";
            if (in_sno[0] == 'T' && in_sno.Length == 6)
            {
                inn_new_apply = "1";
            }

            if (in_member_pay_no != "")
            {
                string css = "btn-primary";
                if (pay_bool == "已繳費")
                {
                    css = "btn-success";
                }
                else if (pay_bool == "已取消")
                {
                    css = "btn-default";
                }

                return "<button class='btn " + css + " badge choseme edit_mode' "
                + " data-meeting='" + pay_meeting + "' data-no='" + in_member_pay_no + "' onclick='GoPayment_Click(this)'>"
                + " <i class='fa fa-dollar'></i> " + in_member_pay_amount + "</button>";
            }
            else if (isMeetingAdmin)
            {
                return "<button class='btn btn-sm btn-primary' onclick='CreatePayment_Click(this)'"
                    + " data-tmp='" + inn_new_apply + "'"
                    + " data-sid='" + in_sno + "'>"
                    + "產生繳費單"
                    + "</button>";
            }
            else
            {
                return "&nbsp;";
            }
        }

        private string GetSidDisplay(Item itmResume)
        {
            return GetSidDisplay(itmResume.getProperty("in_sno", ""));
        }

        //生日(外顯)
        private string GetBirthDisplay(Item itmResume)
        {
            string value = itmResume.getProperty("in_birth", "");
            if (value.Contains("1900") || value.Contains("1899"))
            {
                return "";
            }
            return GetDateTimeValue(value, "yyyy年MM月dd日", 8);
        }

        //身分證字號(外顯)
        private string GetSidDisplay(string in_sno)
        {
            if (in_sno.Length == 10)
            {
                //10碼(身分證處理)
                string in_sno_1 = in_sno.Substring(0, 2);
                string in_sno_2 = in_sno.Substring(6, 4);
                return in_sno_1 + "****" + in_sno_2;//A1****6789
            }
            else
            {
                return "";
            }
        }

        private string GetDegreeDisplay(Item itmResume)
        {
            return GetDegreeDisplay(itmResume.getProperty("in_degree", ""));
        }

        private string GetDegreeDisplay(string value)
        {
            switch (value)
            {
                case "": return "無段位";
                case "10": return "白帶";
                case "30": return "黃帶";
                case "40": return "黃綠帶";
                case "50": return "綠帶";
                case "60": return "綠藍帶";
                case "70": return "藍帶";
                case "80": return "藍紅帶";
                case "90": return "紅帶";
                case "100": return "紅黑帶";
                case "150": return "黑帶一品";
                case "250": return "黑帶二品";
                case "350": return "黑帶三品";
                case "1000": return "壹段";
                case "2000": return "貳段";
                case "3000": return "參段";
                case "4000": return "肆段";
                case "5000": return "伍段";
                case "6000": return "陸段";
                case "7000": return "柒段";
                case "8000": return "捌段";
                case "9000": return "玖段";

                case "11000": return "11";
                case "12000": return "12";
                case "13000": return "13";
                default: return "";
            }
        }

        private string GetTWDay(string value, string format, int hours = 0)
        {
            if (value == "") return "";

            DateTime dt = DateTime.Now;
            if (DateTime.TryParse(value, out dt))
            {
                var v = dt.AddHours(hours);
                var twy = v.Year - 1911;

                if (twy < 100)
                {
                    return "";
                }

                var y = (v.Year - 1911).ToString().PadLeft(3, '0');
                var m = v.Month.ToString().PadLeft(2, '0');
                var d = v.Day.ToString().PadLeft(2, '0');
                return y + "年" + m + "月" + d + "日";
            }
            else
            {
                return value;
            }
        }

        private string GetDateTimeValue(string value, string format, int hours = 0)
        {
            if (value == "") return "";
            DateTime dt = DateTime.Now;
            if (DateTime.TryParse(value, out dt))
            {
                return dt.AddHours(hours).ToString(format);
            }
            return value;
        }

        private int GetIntValue(string value)
        {
            if (value == "") return 0;
            int result = 0;
            int.TryParse(value, out result);
            return result;
        }

        private bool IsError(Item item, bool isSingle = true)
        {
            if (item.isError() || item.getResult() == "")
            {
                return true;
            }
            else if (isSingle && item.getItemCount() != 1)
            {
                return true;
            }
            else if (item.getItemCount() <= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}