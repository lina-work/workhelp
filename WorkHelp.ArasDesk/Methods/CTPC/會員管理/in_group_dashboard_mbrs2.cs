﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.CTPC.Common
{
    public class in_group_dashboard_mbrs2 : Item
    {
        public in_group_dashboard_mbrs2(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
    目的: 所有人員
    日誌:  
        - 2024-03-01 創建 (lina)
*/

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]in_group_dashboard_mbrs2";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "input dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),

                resume_id = itmR.getProperty("rid", ""),
                in_filter = itmR.getProperty("in_filter", ""),
                mode = itmR.getProperty("mode", ""),
                scene = itmR.getProperty("scene", ""),
                query = itmR.getProperty("query", ""),
            };

            //取得登入者資訊
            string sql = "SELECT * FROM IN_RESUME WITH(NOLOCK) WHERE in_user_id = N'" + cfg.strUserId + "'";
            cfg.itmLoginResume = inn.applySQL(sql);
            if (IsError(cfg.itmLoginResume))
            {
                throw new Exception("登入者履歷異常");
            }
            cfg.in_is_admin = cfg.itmLoginResume.getProperty("in_is_admin", "");

            //要檢視的對象 Resume
            cfg.itmResumeView = GetTargetResume(CCO, strMethodName, inn, cfg.itmLoginResume, itmR);
            cfg.view_resume_id = cfg.itmResumeView.getProperty("id", "");
            cfg.view_member_type = cfg.itmResumeView.getProperty("in_member_type", "");

            //權限檢核
            cfg.itmPermit = inn.applyMethod("In_CheckIdentity", "<method>" + strMethodName + "</method><code>" + "ALL" + "</code>");
            cfg.isMeetingAdmin = cfg.itmPermit.getProperty("isMeetingAdmin", "") == "1";
            cfg.isCommittee = cfg.itmPermit.getProperty("isCommittee", "") == "1";
            if (cfg.in_is_admin == "1")
            {
                cfg.isMeetingAdmin = true;
            }

            var itmResumes = GetMembersItem(cfg, cfg.committee_resume_id);
            var raw_count = GetMembersCount(cfg, cfg.committee_resume_id);

            var resumes = MapResumes(itmResumes);

            itmR.setProperty("raw_count", raw_count);

            Query(CCO, strMethodName, inn, cfg.itmResumeView, resumes, cfg.itmPermit, itmR);

            return itmR;
        }

        private List<TResume> MapResumes(Item itmResumes)
        {
            List<TResume> resumes = new List<TResume>();

            if (itmResumes != null)
            {
                int count = itmResumes.getItemCount();
                for (int i = 0; i < count; i++)
                {
                    Item itmResume = itmResumes.getItemByIndex(i);
                    string id = itmResume.getProperty("id", "");
                    string in_sno = itmResume.getProperty("in_sno", "");
                    string in_apply_year = itmResume.getProperty("in_apply_year", "");
                    int apply_year = GetIntValue(in_apply_year);

                    string tw_apply_year = apply_year > 1911
                        ? (apply_year - 1911).ToString()
                        : "";

                    TResume resume = new TResume
                    {
                        id = id,
                        in_sno = in_sno,
                        in_apply_year = in_apply_year,
                        apply_year = apply_year,
                        tw_apply_year = tw_apply_year,
                        Value = itmResume
                    };

                    resumes.Add(resume);
                }
            }

            return resumes;
        }

        #region 匯出
        private void Export(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmResumeView, List<TResume> resumes, Item itmReturn)
        {
            string member_type = itmReturn.getProperty("type", "");
            string view_name = itmResumeView.getProperty("in_name", "");

            string member_type_label = "";

            ClosedXML.Excel.XLWorkbook workbook = new ClosedXML.Excel.XLWorkbook();

            if (resumes.Count > 0)
            {
                switch (member_type)
                {
                    default:
                        member_type_label = "一般學員";
                        AppendMemberSheet(CCO, strMethodName, inn, workbook, member_type_label, resumes);
                        break;
                }
            }

            Item itmPath = GetExcelPath(CCO, strMethodName, inn, "export_path");

            string export_path = itmPath.getProperty("export_path", "").TrimEnd('\\');
            string ext_name = ".xlsx";
            string xls_name = member_type_label + DateTime.Now.ToString("MMdd_HHmmss");
            string xls_file = export_path + "\\" + xls_name + ext_name;
            string xls_url = xls_name + ext_name;

            workbook.SaveAs(xls_file);

            itmReturn.setProperty("xls_name", xls_url);
        }

        //附加個人會員 Sheet
        private void AppendMemberSheet(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, ClosedXML.Excel.XLWorkbook workbook, string sheet_name, List<TResume> resumes)
        {
            ClosedXML.Excel.IXLWorksheet sheet = workbook.Worksheets.Add(sheet_name);

            List<TField> fields = new List<TField>();
            fields.Add(new TField { property = "no", title = "編號", format = "center" });
            fields.Add(new TField { property = "in_name", title = "姓名", format = "center" });
            fields.Add(new TField { property = "in_gender", title = "性別", format = "center" });
            fields.Add(new TField { property = "in_sno", title = "身分證/居留證", format = "center" });
            fields.Add(new TField { property = "in_birth_display", title = "生日", format = "date" });

            fields.Add(new TField { property = "in_area", title = "出生地", format = "center" });
            fields.Add(new TField { property = "in_resident_add", title = "戶籍地址", format = "" });
            fields.Add(new TField { property = "in_add", title = "通訊地址", });
            fields.Add(new TField { property = "in_email", title = "電子信箱", format = "" });

            fields.Add(new TField { property = "in_tel", title = "住家電話", format = "tel" });
            fields.Add(new TField { property = "in_tel_1", title = "行動電話", format = "tel" });


            fields.Add(new TField { property = "in_note", title = "備註", format = "" });


            int wsRow = 1;
            int wsCol = 1;
            int field_count = fields.Count;

            for (int i = 0; i < field_count; i++)
            {
                TField field = fields[i];
                ClosedXML.Excel.IXLCell cell = sheet.Cell(wsRow, wsCol + i);
                SetHeadCell(cell, field.title);
            }
            wsRow++;

            int count = resumes.Count;

            for (int i = 0; i < count; i++)
            {
                var resume = resumes[i];
                Item item = resume.Value;

                string in_add = item.getProperty("in_add", "");
                string in_add_code = item.getProperty("in_add_code", "");

                string in_resident_add = item.getProperty("in_resident_add", "");
                string in_resident_add_code = item.getProperty("in_resident_add_code", "");

                string in_email = item.getProperty("in_email", "");
                string in_reg_date = item.getProperty("in_reg_date", "");
                string in_change_date = item.getProperty("in_change_date", "");

                if (in_email == "na@na.n")
                {
                    item.setProperty("in_email", "");
                }

                item.setProperty("in_birth_display", GetBirthDisplay(item));
                item.setProperty("in_add", in_add_code + in_add);
                item.setProperty("in_resident_add", in_resident_add + in_resident_add_code);

                if (in_reg_date.Contains("1900"))
                {
                    item.setProperty("in_reg_date", "");
                }

                if (in_change_date.Contains("1900"))
                {
                    item.setProperty("in_change_date", "");
                }

                item.setProperty("no", (i + 1).ToString());

                SetItemCell(sheet, wsRow, wsCol, resume, fields);

                wsRow++;
            }

            //凍結窗格
            sheet.SheetView.Freeze(1, 1);
        }

        //設定物件與資料列
        private void SetItemCell(ClosedXML.Excel.IXLWorksheet sheet, int wsRow, int wsCol, TResume resume, List<TField> fields)
        {
            int count = fields.Count;
            for (int i = 0; i < count; i++)
            {
                TField field = fields[i];
                if (field.format == null) field.format = "";

                ClosedXML.Excel.IXLCell cell = sheet.Cell(wsRow, wsCol + i);

                string value = "";

                if (field.getValue != null)
                {
                    value = field.getValue(resume, field);
                }
                else if (field.property != "")
                {
                    value = resume.Value.getProperty(field.property, "");
                }

                SetBodyCell(cell, value, field.format);
            }
        }

        //設定資料列
        private void SetBodyCell(ClosedXML.Excel.IXLCell cell, string value, string format)
        {
            switch (format)
            {
                case "center":
                    cell.Value = value;
                    cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    break;

                case "date":
                    cell.Value = value;
                    cell.Style.NumberFormat.Format = "yyyy/mm/dd";
                    cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    break;

                case "yyyy/MM/dd HH:mm":
                    cell.Value = GetDateTimeValue(value, format, 8);
                    cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    break;

                case "yyyy/MM/dd":
                    cell.Value = GetDateTimeValue(value, format, 8);
                    cell.Style.NumberFormat.Format = "yyyy/mm/dd";
                    cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    break;

                case "yyy年MM月dd日":
                    cell.Value = GetTWDay(value, format, 8);
                    cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    break;

                case "$ #,##0":
                    cell.Value = value;
                    cell.Style.NumberFormat.Format = "$ #,##0";
                    break;

                case "tel":
                    if (value.StartsWith("09"))
                    {
                        cell.Value = value.Replace("-", "");
                        cell.Style.NumberFormat.Format = "0000-000-000";
                        cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    }
                    else
                    {
                        cell.Value = "'" + value;
                        cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    }
                    break;

                case "text":
                    cell.Value = "'" + value;
                    cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    break;

                default:
                    cell.Value = value;
                    break;
            }
            cell.Style.Border.SetRightBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetTopBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetBottomBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetLeftBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
        }

        ClosedXML.Excel.XLColor head_bg_color = ClosedXML.Excel.XLColor.FromHtml("#295C90");

        //設定標題列
        private void SetHeadCell(ClosedXML.Excel.IXLCell cell, string title)
        {
            cell.Value = title;

            cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
            cell.Style.Font.Bold = true;
            cell.DataType = ClosedXML.Excel.XLDataType.Text;
            cell.Style.Fill.BackgroundColor = head_bg_color;
            cell.Style.Font.FontColor = ClosedXML.Excel.XLColor.White;

            cell.Style.Border.SetRightBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetTopBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetBottomBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetLeftBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
        }
        private Item GetExcelPath(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string in_name)
        {
            Item itmResult = inn.newItem();

            //Excel 匯出路徑 (from 原創.設定參數)
            string sql = @"
            SELECT 
                TOP 1 t2.in_name AS 'variable', t1.in_name, t1.in_value 
            FROM 
                In_Variable_Detail t1 WITH(NOLOCK)
            INNER JOIN 
                In_Variable t2 WITH(NOLOCK) ON t2.ID = t1.SOURCE_ID 
            WHERE 
                t2.in_name = N'meeting_excel'  
                AND t1.in_name = N'{#in_name}'
            ";

            sql = sql.Replace("{#in_name}", in_name);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item item = inn.applySQL(sql);

            if (item.isError())
            {
                throw new Exception("未設定匯出路徑 _# " + in_name);
            }

            itmResult.setProperty("export_path", item.getProperty("in_value", ""));

            return itmResult;
        }
        #endregion 匯出

        private void Query(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmResumeView, List<TResume> resumes, Item itmPermit, Item itmReturn)
        {
            string member_type = itmReturn.getProperty("type", "");
            string view_name = itmResumeView.getProperty("in_name", "");

            string member_type_label = "";

            member_type_label = "所有人員";
            AppendMemberTable(CCO, strMethodName, inn, resumes, itmPermit, itmReturn);

            itmReturn.setProperty("page_title", member_type_label);
            itmReturn.setProperty("page_sub_title", view_name);
        }

        private string FieldValue(TResume resume, TField field, Item item)
        {
            string value = "";
            if (field.getValue != null)
            {
                value = field.getValue(resume, field);
            }
            else
            {
                value = field.GetStr(item);
            }
            return value;
        }

        //附加個人資料 Table
        private void AppendMemberTable(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, List<TResume> resumes, Item itmPermit, Item itmReturn)
        {
            bool isMeetingAdmin = itmPermit.getProperty("isMeetingAdmin", "") == "1";
            bool isCommittee = itmPermit.getProperty("isCommittee", "") == "1";

            List<TField> fields = new List<TField>();
            fields.Add(new TField { property = "in_name", title = "姓名", css = "text-left", hide = true });
            fields.Add(new TField { property = "in_sno", title = "系統帳號", css = "text-left", hide = true });

            fields.Add(new TField { property = "in_link", title = "姓名", css = "text-left" });
            fields.Add(new TField { property = "in_gender", title = "性別", css = "text-left" });
            fields.Add(new TField { property = "in_sno_display", title = "身分證號", css = "text-left" });
            fields.Add(new TField { property = "in_birth_display", title = "生日", css = "text-left" });

            fields.Add(new TField { property = "in_email_display", title = "電子信箱", css = "text-left" });
            fields.Add(new TField { property = "in_tel_display", title = "連絡電話", css = "text-left" });

            fields.Add(new TField { property = "in_current_org", title = "所屬單位", css = "text-left" });
            //fields.Add(new TField { property = "in_line_notify", title = "LINE", css = "text-left" });
            fields.Add(new TField { title = "功能", css = "text-center", getValue = MemberViewBtn });

            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            head.AppendLine("<thead>");
            foreach (var field in fields)
            {
                if (field.hide)
                {
                    head.AppendLine("<th class='text-center' data-field='" + field.property + "' data-sortable='true' data-visible='false'>" + field.title + "</th>");
                }
                else
                {
                    head.AppendLine("<th class='text-center' data-field='" + field.property + "' data-sortable='true'>" + field.title + "</th>");
                }
            }
            head.AppendLine("</thead>");

            int count = resumes.Count;

            body.AppendLine("<tbody>");
            for (int i = 0; i < count; i++)
            {
                var resume = resumes[i];
                var item = resume.Value;

                item.setProperty("in_link", GetDashboardLink(item));
                item.setProperty("in_sno_display", GetSidDisplay(item));
                item.setProperty("in_birth_display", GetBirthDisplay(item));
                item.setProperty("in_email_display", GetEmailDisplay(item));
                item.setProperty("in_tel_display", GetTelDisplay(item));

                body.AppendLine("<tr>");
                foreach (var field in fields)
                {
                    string value = FieldValue(resume, field, item);
                    body.AppendLine("<td class='" + field.css + "' data-field='" + field.property + "' >" + value + "</td>");
                }
                body.AppendLine("</tr>");
            }
            body.AppendLine("</tbody>");


            string table_name = "resume_table";

            StringBuilder builder = new StringBuilder();
            builder.AppendLine(GetTableAttribute(table_name));
            builder.Append(head);
            builder.Append(body);
            builder.AppendLine("</table>");
            builder.AppendLine("<script>");
            builder.AppendLine("$('#" + table_name + "').bootstrapTable();");
            builder.AppendLine("if ($(window).width() <= 768) { $('#" + table_name + "').bootstrapTable('toggleView'); }");
            builder.AppendLine("</script>");

            itmReturn.setProperty("inn_table", builder.ToString());
        }

        private string GetTableAttribute(string table_name)
        {
            return "<table id='" + table_name + "' class='table table-hover table-bordered table-rwd rwd rwdtable' "
                + " data-toggle='table' "
                + " data-click-to-select='true' "
                + " data-striped='false' "
                + " data-search='false' "
                + " data-pagination='true' "
                + " data-page-size='10' "
                + " data-show-pagination-switch='false'"
                + ">";
        }

        //取得成員
        private string GetMembersCount(TConfig cfg, string view_resume_id)
        {
            var admin_cond = cfg.isMeetingAdmin ? "" : "AND t1.id = '" + cfg.view_resume_id + "'";

            var filter_condition = "";
            if (cfg.in_filter != "")
            {
                //單位帳號格式 A~Z 0~9(9)(8~12位英數字)
                var custom_regex = new System.Text.RegularExpressions.Regex("^[A-Za-z0-9]{8,12}$");
                if (custom_regex.IsMatch(cfg.in_filter))
                {
                    filter_condition = "AND t1.in_sno = '" + cfg.in_filter + "'";
                }
                else
                {
                    List<string> or_cond = new List<string>();
                    or_cond.Add("(t1.in_name LIKE '%" + cfg.in_filter + "%')");
                    or_cond.Add("(t1.in_sno LIKE '%" + cfg.in_filter + "%')");
                    or_cond.Add("(t1.in_gender LIKE '%" + cfg.in_filter + "%')");
                    or_cond.Add("(t1.in_birth LIKE '%" + cfg.in_filter + "%')");
                    or_cond.Add("(t1.in_tel LIKE '%" + cfg.in_filter + "%')");
                    or_cond.Add("(t1.in_current_org LIKE '%" + cfg.in_filter + "%')");
                    or_cond.Add("(t1.in_email LIKE '%" + cfg.in_filter + "%')");
                    filter_condition = "AND ( " + string.Join(" OR ", or_cond) + " )";
                }
            }

            string sql = @"
                SELECT 
                    COUNT(*) AS 'cnt'
                FROM 
	                IN_RESUME t1 WITH(NOLOCK)
				LEFT OUTER JOIN
					[USER] t2 WITH(NOLOCK)
					ON t2.login_name = t1.login_name
                WHERE 
                    LEN(t1.in_sno) = 10
	                AND ISNULL(t1.in_member_role, '') IN ('', 'sys_9999')
                    {#admin_cond}
                    {#filter_condition}
                ORDER BY 
                    t1.in_sno
            ";

            sql = sql.Replace("{#admin_cond}", admin_cond)
                .Replace("{#filter_condition}", filter_condition);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            var itmData = cfg.inn.applySQL(sql);

            if (itmData.getResult() != "")
            {
                return itmData.getProperty("cnt", "0");
            }
            return "0";
        }

        //取得成員
        private Item GetMembersItem(TConfig cfg, string view_resume_id)
        {
            var admin_cond = cfg.isMeetingAdmin ? "" : "AND t1.id = '" + cfg.view_resume_id + "'";
            if (admin_cond == "" && cfg.query == "") return null;

            var filter_condition = "";
            if (cfg.in_filter != "")
            {
                //單位帳號格式 A~Z 0~9(9)(8~12位英數字)
                var custom_regex = new System.Text.RegularExpressions.Regex("^[A-Za-z0-9]{8,12}$");
                if (custom_regex.IsMatch(cfg.in_filter))
                {
                    filter_condition = "AND t1.in_sno = '" + cfg.in_filter + "'";
                }
                else
                {
                    List<string> or_cond = new List<string>();
                    or_cond.Add("(t1.in_name LIKE '%" + cfg.in_filter + "%')");
                    or_cond.Add("(t1.in_sno LIKE '%" + cfg.in_filter + "%')");
                    or_cond.Add("(t1.in_gender LIKE '%" + cfg.in_filter + "%')");
                    or_cond.Add("(t1.in_birth LIKE '%" + cfg.in_filter + "%')");
                    or_cond.Add("(t1.in_tel LIKE '%" + cfg.in_filter + "%')");
                    or_cond.Add("(t1.in_current_org LIKE '%" + cfg.in_filter + "%')");
                    or_cond.Add("(t1.in_email LIKE '%" + cfg.in_filter + "%')");
                    filter_condition = "AND ( " + string.Join(" OR ", or_cond) + " )";
                }
            }

            string sql = @"
                SELECT TOP 3000
	                t1.id
	                , t1.in_name
	                , t1.in_gender
	                , t1.in_birth
	                , UPPER(t1.in_sno) AS 'in_sno'
	                , t1.in_current_org
	                , t1.in_email
	                , t1.in_tel
	                , t1.in_tel_1
	                , t1.in_photo
	                , t1.in_add
                    , t1.in_add_code
                    , t1.in_resident_add
                    , t1.in_resident_add_code
					, t2.in_line_notify
                FROM 
	                IN_RESUME t1 WITH(NOLOCK)
				LEFT OUTER JOIN
					[USER] t2 WITH(NOLOCK)
					ON t2.login_name = t1.login_name
                WHERE 
                    LEN(t1.in_sno) = 10
	                AND ISNULL(t1.in_member_role, '') IN ('', 'sys_9999')
                    {#admin_cond}
                    {#filter_condition}
                ORDER BY 
                    t1.in_sno
            ";

            sql = sql.Replace("{#admin_cond}", admin_cond)
                .Replace("{#filter_condition}", filter_condition);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        //取得檢視對象 Resume
        private Item GetTargetResume(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmLoginResume, Item itmReturn)
        {
            itmLoginResume.setType("In_Resume");
            itmLoginResume.setProperty("target_resume_id", itmReturn.getProperty("id", ""));
            itmLoginResume.setProperty("login_resume_id", itmLoginResume.getID());
            itmLoginResume.setProperty("login_member_type", itmLoginResume.getProperty("in_member_type", ""));
            itmLoginResume.setProperty("login_is_admin", itmLoginResume.getProperty("in_is_admin", ""));
            return itmLoginResume.apply("In_Get_Target_Resume");
        }

        private class TResume
        {
            public string id { get; set; }
            public string in_sno { get; set; }
            public string in_apply_year { get; set; }
            public int apply_year { get; set; }
            public string tw_apply_year { get; set; }
            public Item Value { get; set; }
        }

        private class TField
        {
            public string property { get; set; }
            public string title { get; set; }
            public string css { get; set; }
            public string format { get; set; }
            public bool hide { get; set; }
            public int in_west_year { get; set; }
            public Func<TResume, TField, string> getValue { get; set; }

            public string GetStr(Item item)
            {
                if (string.IsNullOrWhiteSpace(this.property))
                {
                    return "";
                }

                return item.getProperty(this.property, "");
            }
        }

        private string GetDashboardLink(Item itmResume)
        {
            string resume_id = itmResume.getProperty("id", "");
            string in_name = itmResume.getProperty("in_name", "");
            string in_org = itmResume.getProperty("in_org", "");
            string in_member_type = itmResume.getProperty("in_member_type", "");

            return "<a href='../pages/c.aspx?page=SingleMemberView.html&method=In_Group_Resume_View&id=" + resume_id + "' >"
                + in_name
                + "</a>";
        }

        private string MemberViewBtn(TResume resume, TField field)
        {
            return "<button class='btn btn-sm btn-primary' onclick='MemberView_Click(this)' data-rid='" + resume.id + "'>"
                + "檢視"
                + "</button>";
        }

        private string GetSidDisplay(Item itmResume)
        {
            return GetSidDisplay(itmResume.getProperty("in_sno", ""));
        }

        //連絡電話(外顯)
        private string GetTelDisplay(Item itmResume)
        {
            string v1 = itmResume.getProperty("in_tel", "");
            string v2 = itmResume.getProperty("in_tel_1", "");
            if (v1 != "" && v2 != "") return v1 + "<br>" + v2;
            if (v1 == "") return v2;
            return v1;
        }

        //電子信件(外顯)
        private string GetEmailDisplay(Item itmResume)
        {
            string value = itmResume.getProperty("in_email", "");

            if (value == "na@na.n")
            {
                return "";
            }

            return value;
        }

        //生日(外顯)
        private string GetBirthDisplay(Item itmResume)
        {
            string value = itmResume.getProperty("in_birth", "");

            if (value.Contains("1900") || value.Contains("1899"))
            {
                return "";
            }

            return GetDateTimeValue(value, "yyyy年MM月dd日", 8);
        }

        //身分證字號(外顯)
        private string GetSidDisplay(string in_sno)
        {
            if (in_sno.Length == 10)
            {
                //10碼(身分證處理)
                string in_sno_1 = in_sno.Substring(0, 2);
                string in_sno_2 = in_sno.Substring(6, 4);
                return in_sno_1 + "****" + in_sno_2;//A1****6789
            }
            else
            {
                return "";
            }
        }


        private string GetTWDay(string value, string format, int hours = 0)
        {
            if (value == "") return "";

            DateTime dt = DateTime.Now;
            if (DateTime.TryParse(value, out dt))
            {
                var v = dt.AddHours(hours);
                var twy = v.Year - 1911;

                if (twy < 100)
                {
                    return "";
                }

                var y = (v.Year - 1911).ToString().PadLeft(3, '0');
                var m = v.Month.ToString().PadLeft(2, '0');
                var d = v.Day.ToString().PadLeft(2, '0');
                return y + "年" + m + "月" + d + "日";
            }
            else
            {
                return value;
            }
        }

        private string GetDateTimeValue(string value, string format, int hours = 0)
        {
            if (value == "") return "";

            DateTime dt = DateTime.Now;
            if (DateTime.TryParse(value, out dt))
            {
                return dt.AddHours(hours).ToString(format);
            }
            else
            {
                return value;
            }
        }

        private int GetIntValue(string value)
        {
            if (value == "") return 0;

            int result = 0;
            int.TryParse(value, out result);
            return result;
        }

        private bool IsError(Item item, bool isSingle = true)
        {
            if (item.isError() || item.getResult() == "")
            {
                return true;
            }
            else if (isSingle && item.getItemCount() != 1)
            {
                return true;
            }
            else if (item.getItemCount() <= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public Innovator inn { get; set; }

            public string member_type { get; set; }
            public string member_status { get; set; }
            public string committee_resume_id { get; set; }
            public string in_member_unit { get; set; }
            public string in_filter { get; set; }
            public string resume_id { get; set; }
            public string resident_date { get; set; }

            public string mode { get; set; }
            public string scene { get; set; }
            public string query { get; set; }

            public Item itmLoginResume { get; set; }
            public string in_is_admin { get; set; }

            public Item itmResumeView { get; set; }
            public string view_resume_id { get; set; }
            public string view_member_type { get; set; }

            public Item itmPermit { get; set; }
            public bool isMeetingAdmin { get; set; }
            public bool isCommittee { get; set; }
        }
    }
}