﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.CTPC.Common
{
    public class in_user_application : Item
    {
        public in_user_application(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
    目的: 各類申請下載
        - 個人會員入會申請表
        - 一般團體會員入會申請表
        - 晉段報名表
    輸出: 
        docx
    重點: 
        - 需 Xceed.Document.NET.dll
        - 需 Xceed.Words.NET.dll
        - 請於 method-config.xml 引用 DLL
    日期: 
        - 2021-01-19 創建 (Lina)
*/

            System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]in_user_application";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, this.dom.InnerXml);

            string login_name = itmR.getProperty("login_name", "");
            string in_user_id = inn.getUserID();

            string where = login_name != ""
                ? " WHERE login_name = N'" + login_name + "'"
                : " WHERE in_user_id = '" + in_user_id + "'";

            string sql = "";

            //取得登入者資訊
            sql = "SELECT * FROM IN_RESUME WITH(NOLOCK)" + where;
            Item loginResume = inn.applySQL(sql);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                inn = inn,
                aras_vault_url = @"C:\Aras\Vault\" + strDatabaseName,

                scene = itmR.getProperty("scene", ""),
                in_type = itmR.getProperty("in_type", ""),
                in_value = itmR.getProperty("in_value", ""),

                meeting_id = itmR.getProperty("meeting_id", ""),
                meeting_type = itmR.getProperty("meeting_type", ""),
                resume_sno = itmR.getProperty("resume_sno", ""),
                muid = itmR.getProperty("muid", ""),

                loginResume = loginResume,
                login_resume_id = loginResume.getProperty("id", ""),
                login_resume_name = loginResume.getProperty("in_name", ""),
                login_resume_sno = loginResume.getProperty("in_sno", ""),
                login_user_id = loginResume.getProperty("in_user_id", ""),
                login_photo = loginResume.getProperty("in_photo", ""),
            };

            cfg.login_headshot = GetFileFullPath(cfg, cfg.login_photo);

            DateTime dtNow = System.DateTime.Now;
            cfg.loginResume.setProperty("in_year", dtNow.Year.ToString());
            cfg.loginResume.setProperty("in_month", dtNow.Month.ToString());
            cfg.loginResume.setProperty("in_day", dtNow.Day.ToString());
            cfg.loginResume.setProperty("in_number", "");

            string codeMethod = "ALL";
            string bodyMethod = "<method>" + strMethodName + "</method><code>" + codeMethod + "</code>";
            Item itmPermit = inn.applyMethod("In_CheckIdentity", bodyMethod);
            cfg.isMeetingAdmin = itmPermit.getProperty("isMeetingAdmin", "") == "1";
            cfg.isCommittee = itmPermit.getProperty("isCommittee", "") == "1";
            cfg.isGymOwner = itmPermit.getProperty("isGymOwner", "") == "1";
            cfg.isGymAssistant = itmPermit.getProperty("isGymAssistant", "") == "1";

            if (cfg.meeting_id != "")
            {
                //是否為共同講師
                bool open = GetUserResumeListStatus(cfg.inn, cfg.meeting_id, cfg.login_user_id);
                if (open) cfg.isMeetingAdmin = true;
            }

            GetReceipt(cfg);
            bool is_err = false;

            switch (cfg.scene)
            {
                case "gym"://道館入會申請表
                    var coach_cert_map = MapCertificates(cfg, cfg.login_resume_id);
                    cfg.login_sid_01 = GetFileFullPath(cfg, coach_cert_map, "1", "in_file1");
                    cfg.login_sid_02 = GetFileFullPath(cfg, coach_cert_map, "1", "in_file2");

                    cfg.meeting_excel_parameter = "application_vip_gym_path";
                    cfg.doc_name = cfg.login_resume_name + "_道館、訓練站入會申請表";
                    ExportVipGymWord(cfg, itmR);
                    break;

                case "school"://學校社團入會申請表
                    var principal_cert_map1 = MapCertificates(cfg, cfg.login_resume_id);
                    cfg.login_sid_01 = GetFileFullPath(cfg, principal_cert_map1, "1", "in_file1");
                    cfg.login_sid_02 = GetFileFullPath(cfg, principal_cert_map1, "1", "in_file2");

                    cfg.meeting_excel_parameter = "application_vip_school_path";
                    cfg.doc_name = cfg.login_resume_name + "_學校社團入會申請表";
                    ExportVipSchoolWord(cfg, itmR);
                    break;

                case "group"://一般團體會員入會申請表
                    var principal_cert_map = MapCertificates(cfg, cfg.login_resume_id);
                    cfg.login_sid_01 = GetFileFullPath(cfg, principal_cert_map, "1", "in_file1");
                    cfg.login_sid_02 = GetFileFullPath(cfg, principal_cert_map, "1", "in_file2");

                    cfg.meeting_excel_parameter = "application_vip_group_path";
                    cfg.doc_name = cfg.login_resume_name + "_一般團體會員入會申請表";
                    ExportVipGroupWord(cfg, itmR);
                    break;

                case "member": //個人會員入會申請表
                    var cert_map = MapCertificates(cfg, cfg.login_resume_id);
                    cfg.login_sid_01 = GetFileFullPath(cfg, cert_map, "1", "in_file1");
                    cfg.login_sid_02 = GetFileFullPath(cfg, cert_map, "1", "in_file2");
                    cfg.login_tw_degree = GetFileFullPath(cfg, cert_map, "6", "in_file1", "6tw");

                    cfg.meeting_excel_parameter = "application_vip_mbr_path";
                    cfg.doc_name = cfg.login_resume_name + "_個人會員入會申請表";

                    ExportVipMemberWord(cfg, itmR);
                    break;

                case "degree"://晉段報名表
                    cfg.meeting_excel_parameter = "application_degree_path";
                    ExportDegreeWord(cfg, itmR);
                    break;

                case "seminar"://講習報名表
                    cfg.doc_name = cfg.login_resume_name + "_講習報名表";
                    ExportSeminarWord(cfg, itmR);
                    break;

                case "reissue"://補發證照
                    ExportReissueWord(cfg, itmR);
                    break;

                case "cancelApply"://取消申請


                    string loginID = loginResume.getProperty("in_sno", "");
                    string loginName = loginResume.getProperty("in_name", "");
                    sql = @"SELECT in_l1 +'_' + in_l2 AS in_title,in_sno,in_name,in_ass_ver_memo
                            FROM innovator.in_meeting_user WITH (NOLOCK)
                            WHERE id ='{#muid}'";
                    sql = sql.Replace("{#muid}", cfg.muid);

                    Item itmSql = inn.applySQL(sql);
                    string del_sno = itmSql.getProperty("in_sno", "");
                    string del_name = itmSql.getProperty("in_name", "");
                    string del_title = itmSql.getProperty("in_title", "");
                    string memo = itmSql.getProperty("in_ass_ver_memo", "");

                    string strMethodNameLog = "[" + strDatabaseName + "]" + "cancelLog";
                    CCO.Utilities.WriteDebug(strMethodNameLog,
                                            "【取消申請】" + Environment.NewLine +
                                            "項目名稱：" + del_title + Environment.NewLine +
                                            "登入者：" + loginID + " " + loginName + Environment.NewLine +
                                            "審核備註：" + memo + Environment.NewLine +
                                            "被刪除者：" + del_sno + " " + del_name);

                    CancelApply(cfg, itmR);
                    break;

                default:
                    is_err = true;
                    break;
            }

            if (is_err)
            {
                throw new Exception("請選擇要下載的申請類別");
            }

            return itmR;
        }

        //取得共同講師 (判斷現在的登入者是否為此課程的共同講師)
        private bool GetUserResumeListStatus(Innovator inn, string meeting_id, string strUserId)
        {
            string sql = @"
                SELECT
                    t2.id
                FROM 
                    In_Cla_Meeting_Resumelist t1 WITH(NOLOCK) 
                INNER JOIN 
                    IN_RESUME t2 WITH(NOLOCK) 
                    ON t2.id = t1.related_id    
                WHERE 
                    t1.source_id = '{#meeting_id}' 
                    AND t2.in_user_id = '{#in_user_id}';
            ";

            sql = sql.Replace("{#meeting_id}", meeting_id)
                .Replace("{#in_user_id}", strUserId);

            Item item = inn.applySQL(sql);

            if (item.getResult() != "")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void GetReceipt(TConfig cfg)
        {
            string sql = "";
            try
            {
                switch (cfg.scene)
                {
                    case "gym"://道館、訓練站
                        sql = "SELECT in_receipt FROM IN_MEETING WITH(NOLOCK) WHERE id = '83B87AE0033640AA8DBA7AF2CF659479'";
                        break;
                    case "school"://學校社團
                        sql = "SELECT in_receipt FROM IN_MEETING WITH(NOLOCK) WHERE id = '38CAB90DF1274E048520A801948AC65C'";
                        break;
                    case "group":
                        sql = "SELECT in_receipt FROM IN_MEETING WITH(NOLOCK) WHERE id = '5F73936711E04DC799CB02587F4FF7E0'";
                        break;
                    case "member":
                        sql = "SELECT in_receipt FROM IN_MEETING WITH(NOLOCK) WHERE id = '249FDB244E534EB0AA66C8E9C470E930'";
                        break;
                    case "degree":
                    case "seminar":
                        sql = "SELECT in_receipt FROM IN_CLA_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'";
                        break;

                    case "reissue"://補發證件
                        sql = "SELECT in_receipt FROM IN_MEETING WITH(NOLOCK)"
                            + " WHERE id = (SELECT source_id FROM IN_MEETING_USER WITH(NOLOCK) WHERE id = '" + cfg.muid + "')";
                        break;
                }
                Item itmSql = cfg.inn.applySQL(sql);

                //人員名冊
                string in_receipt = itmSql.getProperty("in_receipt", "");
                string[] staffArr = in_receipt.Split(',');
                foreach (var staff in staffArr)
                {

                    switch (staff.Split(':')[0])
                    {
                        case "承辦人":
                            cfg.loginResume.setProperty("undertaker", staff.Split(':')[1]);
                            break;
                        case "經收人":
                            cfg.loginResume.setProperty("received", staff.Split(':')[1]);
                            break;
                        case "秘書長":
                            cfg.loginResume.setProperty("sg", staff.Split(':')[1]);
                            break;
                        case "副秘書長":
                            cfg.loginResume.setProperty("deputy_sg", staff.Split(':')[1]);
                            break;
                        case "會長":
                            cfg.loginResume.setProperty("chairman", staff.Split(':')[1]);
                            break;
                        case "會計":
                            cfg.loginResume.setProperty("account", staff.Split(':')[1]);
                            break;
                        case "行政組":
                            cfg.loginResume.setProperty("admin", staff.Split(':')[1]);
                            break;
                        case "裁判組":
                            cfg.loginResume.setProperty("referee", staff.Split(':')[1]);
                            break;
                        case "訓練組":
                            cfg.loginResume.setProperty("training", staff.Split(':')[1]);
                            break;
                    }
                }
            }
            catch
            {

            }
        }

        /// <summary>
        /// 晉段申請表
        /// </summary>
        private void ExportDegreeWord(TConfig cfg, Item itmReturn)
        {
            string sql = "";

            sql = "SELECT in_title, in_area, in_meeting_type, in_echelon FROM IN_CLA_MEETING WITH(NOLOCK)"
                + " WHERE id = '" + cfg.meeting_id + "'";

            Item itmMeeting = cfg.inn.applySQL(sql);
            if (itmMeeting.isError() || itmMeeting.getResult() == "")
            {
                return;
            }

            string in_meeting_type = itmMeeting.getProperty("in_meeting_type", "");
            string mt_echelon = itmMeeting.getProperty("in_echelon", "");

            //晉段組可擁有管理者權限
            if (!cfg.isMeetingAdmin && in_meeting_type == "degree")
            {
                Item itmRoleResult = cfg.inn.applyMethod("In_Association_Role"
                    , "<in_sno>" + cfg.login_resume_sno + "</in_sno>"
                    + "<idt_names>ACT_ASC_Degree</idt_names>");

                cfg.isMeetingAdmin = itmRoleResult.getProperty("inn_result", "") == "1";
            }

            sql = "SELECT t1.*"
                + " , t3.id AS 'resume_id'"
                + " , ISNULL(t2.in_short_org, t1.in_committee) AS 'committee_short_name'"
                + " FROM IN_CLA_MEETING_USER t1 WITH(NOLOCK) "
                + " LEFT OUTER JOIN IN_RESUME t2 WITH(NOLOCK) ON t2.in_name = t1.in_committee AND t2.in_member_type = 'area_cmt' AND t2.in_member_role = 'sys_9999'"
                + " INNER JOIN IN_RESUME t3 WITH(NOLOCK) ON t3.in_sno = t1.in_sno";

            if (cfg.isMeetingAdmin)
            {
                sql += " WHERE t1.source_id = '" + cfg.meeting_id + "'";
            }
            else if (cfg.isCommittee)
            {
                sql += " WHERE t1.source_id = '" + cfg.meeting_id + "' AND t1.in_committee = N'" + cfg.login_resume_name + "'";
            }
            else
            {
                sql += " WHERE t1.source_id = '" + cfg.meeting_id + "' AND t1.in_creator_sno = N'" + cfg.resume_sno + "'";
            }

            if (cfg.muid != "")
            {
                sql += " AND t1.id = '" + cfg.muid + "'";
            }

            sql += " ORDER BY t1.in_degree DESC, t1.in_birth";

            Item itmMUsers = cfg.inn.applySQL(sql);
            if (itmMUsers.isError() || itmMUsers.getResult() == "")
            {
                return;
            }

            int count = itmMUsers.getItemCount();
            if (count == 1)
            {
                cfg.doc_name = itmMUsers.getItemByIndex(0).getProperty("in_name", "") + "_晉段申請表";
            }
            else
            {
                cfg.doc_name = cfg.login_resume_name + "_下載_晉段申請表";
            }

            TExport export = GetExportInfo(cfg);

            TDocConfig docConfig = new TDocConfig
            {
                FontName = "標楷體",
                FontSize = 16,
                Fields = new List<TField>(),
                HeadShot = new TUnit { Height = 144, Width = 112 },
                Degree = new TUnit { Height = 100, Width = 200 },
            };

            string meeting_area = itmMeeting.getProperty("in_area", "");

            //載入模板
            using (var doc = Xceed.Words.NET.DocX.Load(export.Source))
            {
                for (int i = 0; i < count; i++)
                {
                    Item itmMUser = itmMUsers.getItemByIndex(i);

                    var resume_id = itmMUser.getProperty("resume_id", "");
                    var in_photo1 = itmMUser.getProperty("in_photo1", "");

                    cfg.login_headshot = GetFileFullPath(cfg, in_photo1);

                    var degree_cert_map = MapCertificates(cfg, resume_id);
                    cfg.login_tw_degree = GetFileFullPath(cfg, degree_cert_map, "6tw", "in_file1");
                    cfg.login_gl_degree = GetFileFullPath(cfg, degree_cert_map, "6gl", "in_file1");

                    string in_number = itmMUser.getProperty("in_number", "");
                    string in_l1 = itmMUser.getProperty("in_l1", "");
                    string in_birth = itmMUser.getProperty("in_birth", "");

                    //SQL抓日期若沒預先加好要+ 8 hours
                    if (in_birth != "")
                    {
                        DateTime birthday = DateTime.Parse(in_birth);
                        in_birth = birthday.AddHours(8).ToString("yyyy-MM-dd");
                    }

                    string in_en_name = itmMUser.getProperty("in_en_name", "");
                    string in_resident_add_code = itmMUser.getProperty("in_resident_add_code", "");
                    string in_resident_add = itmMUser.getProperty("in_resident_add", "")
                        .Replace(in_resident_add_code, "").Trim();


                    //string in_committee = itmMUser.getProperty("in_committee", "");
                    string in_committee = itmMUser.getProperty("committee_short_name", "");
                    string in_stuff_c1 = itmMUser.getProperty("in_stuff_c1", "");
                    string in_regdate = itmMUser.getProperty("in_regdate", "");

                    itmMUser.setProperty("mt_echelon", mt_echelon + " 梯次");
                    itmMUser.setProperty("inn_committee", "所屬 " + in_committee);
                    itmMUser.setProperty("inn_stuff_c1", "所屬 " + in_stuff_c1 + " 教練");

                    //八級晉升日期
                    string in_exe_a2 = itmMUser.getProperty("in_exe_a2", "");

                    itmMUser.setProperty("tw_birth", GetTwDay(in_birth, addYTitle: true));
                    itmMUser.setProperty("tw_exe_a2", GetTwDay(in_exe_a2));
                    itmMUser.setProperty("tw_regdate", GetTwDay(in_regdate));

                    string[] in_ens = PartialName(in_en_name);
                    itmMUser.setProperty("in_ens_1", in_ens[0]);
                    itmMUser.setProperty("in_ens_2", in_ens[1]);
                    itmMUser.setProperty("in_ens_3", in_ens[2]);

                    string inn_rac = in_resident_add_code.PadRight(6, ' ');
                    itmMUser.setProperty("inn_rac_1", inn_rac[0].ToString().Trim());
                    itmMUser.setProperty("inn_rac_2", inn_rac[1].ToString().Trim());
                    itmMUser.setProperty("inn_rac_3", inn_rac[2].ToString().Trim());
                    itmMUser.setProperty("inn_rac_4", inn_rac[3].ToString().Trim());
                    itmMUser.setProperty("inn_rac_5", inn_rac[4].ToString().Trim());
                    itmMUser.setProperty("inn_rac_6", inn_rac[5].ToString().Trim());

                    string[] inn_radd = PartialAdd(in_resident_add);
                    itmMUser.setProperty("inn_radd_1", inn_radd[0]);
                    itmMUser.setProperty("inn_radd_2", inn_radd[1]);

                    string form_title = in_l1 == "壹段"
                        ? "中華民國跆拳道協會晉段(入會)申請表"
                        : "中華民國跆拳道協會晉段申請表";

                    //插入標題段落
                    var p = doc.InsertParagraph();
                    p.Append(form_title)
                       .Font(new Xceed.Document.NET.Font(docConfig.FontName))
                       .FontSize(26);
                    p.Alignment = Xceed.Document.NET.Alignment.center;


                    //插入申請段別
                    p = doc.InsertParagraph();

                    p.Append("申請段別 ")
                       .Font(new Xceed.Document.NET.Font(docConfig.FontName))
                       .FontSize(12);

                    p.Append(in_l1)
                       .Font(new Xceed.Document.NET.Font(docConfig.FontName))
                       .FontSize(20)
                       .UnderlineStyle(Xceed.Document.NET.UnderlineStyle.singleLine);

                    p.Append("　申請日期：" + itmMUser.getProperty("tw_regdate", "") + "　地區：")
                       .Font(new Xceed.Document.NET.Font(docConfig.FontName))
                       .FontSize(12);

                    p.Append(itmMUser.getProperty("in_degree_area", ""))
                       .Font(new Xceed.Document.NET.Font(docConfig.FontName))
                       .FontSize(22)
                       .UnderlineStyle(Xceed.Document.NET.UnderlineStyle.singleLine);

                    p.Append("　編號：")
                       .Font(new Xceed.Document.NET.Font(docConfig.FontName))
                       .FontSize(12);

                    p.Append("            ")
                       .Font(new Xceed.Document.NET.Font(docConfig.FontName))
                       .FontSize(12)
                       .UnderlineStyle(Xceed.Document.NET.UnderlineStyle.singleLine);

                    p.Alignment = Xceed.Document.NET.Alignment.left;

                    if (in_l1 == "壹段")
                    {
                        SetDegreeWordTable1(doc, cfg, docConfig, itmMUser);
                    }
                    else
                    {
                        SetDegreeWordTable2(doc, cfg, docConfig, itmMUser);
                    }

                    //插入標題段落
                    var p_sign = doc.InsertParagraph();
                    p_sign.Append("已確認以上資料正確無誤    簽名：")
                       .Font(new Xceed.Document.NET.Font(docConfig.FontName))
                       .FontSize(16);
                    p_sign.Alignment = Xceed.Document.NET.Alignment.center;

                    p_sign.Append("            ")
                       .Font(new Xceed.Document.NET.Font(docConfig.FontName))
                       .FontSize(16)
                       .UnderlineStyle(Xceed.Document.NET.UnderlineStyle.singleLine);

                    var is_end = i == (count - 1);
                    if (!is_end)
                    {
                        var pend = doc.InsertParagraph();
                        pend.InsertPageBreakAfterSelf();
                    }
                }

                doc.Tables[0].Remove();
                doc.Tables[0].Remove();
                doc.RemoveParagraphAt(0);
                doc.RemoveParagraphAt(0);
                doc.RemoveParagraphAt(0);


                doc.SaveAs(export.File);
            }

            itmReturn.setProperty("xls_name", export.Url);
        }

        //申請段位:壹段
        private void SetDegreeWordTable1(Xceed.Words.NET.DocX doc, TConfig cfg, TDocConfig docConfig, Item itmMUser)
        {
            //取得模板表格
            var template_table = doc.Tables[0];
            var docTable = doc.InsertTable(template_table);
            var defFName = "新細明體";//標楷體、Times New Roman

            List<TField> fields = new List<TField>();
            fields.Add(new TField { PT = PEnm.RP, RIdx = 0, CIdx = 1, Property = "in_name", FName = defFName, FSize = 14 });

            fields.Add(new TField { PT = PEnm.RP, RIdx = 1, CIdx = 2, Property = "in_ens_1", FName = defFName, FSize = 12 });
            fields.Add(new TField { PT = PEnm.RP, RIdx = 2, CIdx = 2, Property = "in_ens_2", FName = defFName, FSize = 12 });
            fields.Add(new TField { PT = PEnm.RP, RIdx = 3, CIdx = 2, Property = "in_ens_3", FName = defFName, FSize = 12 });

            fields.Add(new TField { PT = PEnm.RP, RIdx = 4, CIdx = 1, Property = "in_sno", FName = defFName, FSize = 12 });
            fields.Add(new TField { PT = PEnm.RP, RIdx = 5, CIdx = 1, Property = "tw_birth", FName = defFName, FSize = 12 });

            fields.Add(new TField { PT = PEnm.RP, RIdx = 6, CIdx = 1, Property = "in_gender", FName = defFName, FSize = 12 });
            fields.Add(new TField { PT = PEnm.RP, RIdx = 6, CIdx = 3, Property = "in_area", FName = defFName, FSize = 12 });
            fields.Add(new TField { PT = PEnm.RP, RIdx = 7, CIdx = 1, Property = "in_guardian", FName = defFName, FSize = 12 });


            fields.Add(new TField { PT = PEnm.RP, RIdx = 0, CIdx = 3, Property = "inn_rac_1", FName = defFName, FSize = 12 });
            fields.Add(new TField { PT = PEnm.RP, RIdx = 0, CIdx = 4, Property = "inn_rac_2", FName = defFName, FSize = 12 });
            fields.Add(new TField { PT = PEnm.RP, RIdx = 0, CIdx = 5, Property = "inn_rac_3", FName = defFName, FSize = 12 });
            fields.Add(new TField { PT = PEnm.RP, RIdx = 0, CIdx = 6, Property = "inn_rac_4", FName = defFName, FSize = 12 });
            fields.Add(new TField { PT = PEnm.RP, RIdx = 0, CIdx = 7, Property = "inn_rac_5", FName = defFName, FSize = 12 });
            fields.Add(new TField { PT = PEnm.RP, RIdx = 0, CIdx = 8, Property = "inn_rac_6", FName = defFName, FSize = 12 });

            //戶籍地址
            fields.Add(new TField { PT = PEnm.RP, RIdx = 0, CIdx = 9, Property = "inn_radd_1", FName = defFName, FSize = 12 });
            fields.Add(new TField { PT = PEnm.RP, RIdx = 1, CIdx = 4, Property = "inn_radd_2", FName = defFName, FSize = 12 });
            //fields.Add(new TField { PT = PEnm.RP, RIdx = 1, CIdx = 4, Property = "in_resident_add", FName = defFName, FSize = 12 });

            fields.Add(new TField { PT = PEnm.RP, RIdx = 2, CIdx = 4, Property = "in_country", FName = defFName, FSize = 12 });
            fields.Add(new TField { PT = PEnm.RP, RIdx = 2, CIdx = 6, Property = "in_tel", FName = defFName, FSize = 12 });

            fields.Add(new TField { PT = PEnm.RP, RIdx = 3, CIdx = 4, Property = "in_exe_a1", FName = defFName, FSize = 12 });
            fields.Add(new TField { PT = PEnm.RP, RIdx = 3, CIdx = 6, Property = "tw_exe_a2", FName = defFName, FSize = 12 });

            fields.Add(new TField { PT = PEnm.RP, RIdx = 4, CIdx = 2, Property = "inn_committee", FName = defFName, FSize = 12 });
            fields.Add(new TField { PT = PEnm.RP, RIdx = 4, CIdx = 3, Property = "inn_stuff_c1", FName = defFName, FSize = 12 });
            fields.Add(new TField { PT = PEnm.RP, RIdx = 5, CIdx = 3, Property = "in_current_org", FName = defFName, FSize = 12 });

            //晉段測驗成績評分表
            fields.Add(new TField { PT = PEnm.RP, RIdx = 10, CIdx = 1, Property = "mt_echelon", FName = defFName, FSize = 12 });

            //照片
            fields.Add(new TField { PT = PEnm.DegreeTw, RIdx = 18, CIdx = 0, Title = "國內段證", FName = defFName, FSize = 12 });
            fields.Add(new TField { PT = PEnm.DegreeGl, RIdx = 18, CIdx = 1, Title = "國際段證", FName = defFName, FSize = 12 });
            fields.Add(new TField { PT = PEnm.HeadShot, RIdx = 18, CIdx = 2, Title = "照片", FName = defFName, FSize = 12 });

            SetWordTableCore(doc, cfg, docConfig, docTable, fields, itmMUser);

            var p = doc.InsertParagraph();
            p.Append("備註：壹段(黃色)卡紙列印。")
               .Font(new Xceed.Document.NET.Font("新細明體"))
               .FontSize(12);
        }

        //申請段位
        private void SetDegreeWordTable2(Xceed.Words.NET.DocX doc, TConfig cfg, TDocConfig docConfig, Item itmMUser)
        {
            //取得模板表格
            var template_table = doc.Tables[1];
            var docTable = doc.InsertTable(template_table);
            var defFName = "新細明體";//標楷體、Times New Roman

            List<TField> fields = new List<TField>();
            fields.Add(new TField { PT = PEnm.RP, RIdx = 0, CIdx = 1, Property = "in_name", FName = defFName, FSize = 14 });

            fields.Add(new TField { PT = PEnm.RP, RIdx = 1, CIdx = 2, Property = "in_ens_1", FName = defFName, FSize = 12 });
            fields.Add(new TField { PT = PEnm.RP, RIdx = 2, CIdx = 2, Property = "in_ens_2", FName = defFName, FSize = 12 });
            fields.Add(new TField { PT = PEnm.RP, RIdx = 3, CIdx = 2, Property = "in_ens_3", FName = defFName, FSize = 12 });

            fields.Add(new TField { PT = PEnm.RP, RIdx = 4, CIdx = 1, Property = "in_sno", FName = defFName, FSize = 12 });
            fields.Add(new TField { PT = PEnm.RP, RIdx = 5, CIdx = 1, Property = "tw_birth", FName = defFName, FSize = 12 });

            fields.Add(new TField { PT = PEnm.RP, RIdx = 6, CIdx = 1, Property = "in_gender", FName = defFName, FSize = 12 });
            fields.Add(new TField { PT = PEnm.RP, RIdx = 6, CIdx = 3, Property = "in_area", FName = defFName, FSize = 12 });
            fields.Add(new TField { PT = PEnm.RP, RIdx = 7, CIdx = 1, Property = "in_guardian", FName = defFName, FSize = 12 });


            fields.Add(new TField { PT = PEnm.RP, RIdx = 0, CIdx = 3, Property = "inn_rac_1", FName = defFName, FSize = 12 });
            fields.Add(new TField { PT = PEnm.RP, RIdx = 0, CIdx = 4, Property = "inn_rac_2", FName = defFName, FSize = 12 });
            fields.Add(new TField { PT = PEnm.RP, RIdx = 0, CIdx = 5, Property = "inn_rac_3", FName = defFName, FSize = 12 });
            fields.Add(new TField { PT = PEnm.RP, RIdx = 0, CIdx = 6, Property = "inn_rac_4", FName = defFName, FSize = 12 });
            fields.Add(new TField { PT = PEnm.RP, RIdx = 0, CIdx = 7, Property = "inn_rac_5", FName = defFName, FSize = 12 });
            fields.Add(new TField { PT = PEnm.RP, RIdx = 0, CIdx = 8, Property = "inn_rac_6", FName = defFName, FSize = 12 });

            //戶籍地址
            fields.Add(new TField { PT = PEnm.RP, RIdx = 0, CIdx = 9, Property = "inn_radd_1", FName = defFName, FSize = 12 });
            fields.Add(new TField { PT = PEnm.RP, RIdx = 1, CIdx = 4, Property = "inn_radd_2", FName = defFName, FSize = 12 });
            //fields.Add(new TField { PT = PEnm.RP, RIdx = 1, CIdx = 4, Property = "in_resident_add", FName = defFName, FSize = 12 });

            //fields.Add(new TField { PT = PEnm.RP, RIdx = 2, CIdx = 4, Property = "in_country", FName = defFName, FSize = 12 });
            fields.Add(new TField { PT = PEnm.RP, RIdx = 2, CIdx = 6, Property = "in_tel", FName = defFName, FSize = 12 });

            fields.Add(new TField { PT = PEnm.RP, RIdx = 3, CIdx = 4, Property = "in_degree_id", FName = defFName, FSize = 12 });
            fields.Add(new TField { PT = PEnm.RP, RIdx = 3, CIdx = 6, Property = "in_gl_degree_id", FName = defFName, FSize = 12 });

            fields.Add(new TField { PT = PEnm.RP, RIdx = 4, CIdx = 2, Property = "inn_committee", FName = defFName, FSize = 12 });
            fields.Add(new TField { PT = PEnm.RP, RIdx = 4, CIdx = 3, Property = "inn_stuff_c1", FName = defFName, FSize = 12 });
            fields.Add(new TField { PT = PEnm.RP, RIdx = 5, CIdx = 3, Property = "in_current_org", FName = defFName, FSize = 12 });

            //晉段測驗成績評分表
            fields.Add(new TField { PT = PEnm.RP, RIdx = 10, CIdx = 1, Property = "mt_echelon", FName = defFName, FSize = 12 });

            //照片
            fields.Add(new TField { PT = PEnm.DegreeTw, RIdx = 18, CIdx = 0, Title = "國內段證", FName = defFName, FSize = 12 });
            fields.Add(new TField { PT = PEnm.DegreeGl, RIdx = 18, CIdx = 1, Title = "國際段證", FName = defFName, FSize = 12 });
            fields.Add(new TField { PT = PEnm.HeadShot, RIdx = 18, CIdx = 2, Title = "照片", FName = defFName, FSize = 12 });


            SetWordTableCore(doc, cfg, docConfig, docTable, fields, itmMUser);


            var p = doc.InsertParagraph();
            p.Append("備註：貳段(淺藍色)、參段(淺綠色)、肆段以上(淺紅色)卡紙列印。")
               .Font(new Xceed.Document.NET.Font("新細明體"))
               .FontSize(12);
        }

        /// <summary>
        /// 講習報名表
        /// </summary>
        private void ExportSeminarWord(TConfig cfg, Item itmReturn)
        {
            string sql = "";

            sql = "SELECT in_title, in_area, in_meeting_type, in_echelon, in_report_type, in_date_s, in_date_e"
                + " FROM IN_CLA_MEETING WITH(NOLOCK)"
                + " WHERE id = '" + cfg.meeting_id + "'";

            Item itmMeeting = cfg.inn.applySQL(sql);
            if (itmMeeting.isError() || itmMeeting.getResult() == "")
            {
                throw new Exception("講習活動查無資料");
            }

            string mt_title = itmMeeting.getProperty("in_title", "");
            string mt_date_s = itmMeeting.getProperty("in_date_s", "");
            string mt_date_e = itmMeeting.getProperty("in_date_e", "");
            string mt_report_type = itmMeeting.getProperty("in_report_type", "");
            string mt_time = MeetingTwDayRange(mt_date_s, mt_date_e);

            cfg.meeting_excel_parameter = "application_seminar_path";
            // switch(mt_report_type)
            // {
            //     case "level_c":
            //         cfg.meeting_excel_parameter = "application_seminar_path_c";
            //         break;

            //     case "en_name":
            //         cfg.meeting_excel_parameter = "application_seminar_path_en";
            //         break;
            // }

            //講習報名表
            string reg_id = cfg.muid;
            string sqlFilter = "";

            if (reg_id != "")
            {
                sqlFilter = " AND t2.id = '" + reg_id + "'";
            }
            else
            {
                if (cfg.isMeetingAdmin)
                {
                    sqlFilter = "";
                }
                else if (cfg.isCommittee)
                {
                    sqlFilter = " AND t2.IN_COMMITTEE = '" + cfg.resume_sno + "'";
                }
                else
                {
                    sqlFilter = " AND t2.in_creator_sno = '" + cfg.resume_sno + "'";
                }
            }

            sql = @"
                SELECT 
                	t1.in_title AS 'mt_title'
                	, t1.in_level
                	, t2.in_name
                	, t2.in_en_name
                	, t2.in_sno
                	, dateadd(hour, 8, t2.in_birth) AS 'in_birth'
                	, t2.in_gender
                	, t2.in_email
                	, t2.in_tel
                	, t2.in_tel_1
                	, t2.in_tel_2
                	, t2.in_add
                	, t2.in_add_code as 'ad_code'
                	, t2.in_area
                	, t2.in_committee
                	, t2.in_education
                	, t2.in_work_org
                	, t2.in_title
                	, t2.in_current_org
                	, t2.in_cert_add
                	, t2.in_l1
                	, t2.in_exe_a1
                	, t2.in_exe_a2
                	, t2.in_exe_a7
                	, t2.in_exe_a8
                	, t2.in_exe_a9
                	, t2.in_lunch
                	, t2.in_degree_label + ' 證號:'+ t2.in_degree_id AS 'in_degree'
                	, t2.in_official_leave_org
                	, t2.in_official_leave_note
                	, t2.in_official_leave_add
                	, t2.in_stuff_c5
                    , t2.in_photo1
                	, T3.in_manager_name
                FROM 
                	IN_CLA_MEETING t1 WITH (NOLOCK)
                JOIN 
                	IN_CLA_MEETING_USER t2  WITH (NOLOCK)
                	ON t2.source_id = t1.id
                JOIN 
                	IN_RESUME t3 WITH (NOLOCK)
                    ON t3.in_sno = t2.in_sno
                WHERE 
                	t1.id ='{#meeting_id}' 
                	{#sqlFilter}
                ORDER BY
                	t2.in_degree DESC
                	, t2.in_birth
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                    .Replace("{#sqlFilter}", sqlFilter);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item itmMUserResumes = cfg.inn.applySQL(sql);

            TExport export = GetExportInfo(cfg);

            TDocConfig docConfig = new TDocConfig
            {
                FontName = "標楷體",
                FontSize = 16,
                Fields = new List<TField>(),
                HeadShot = new TUnit { Height = 144, Width = 112 },
            };

            int count = itmMUserResumes.getItemCount();

            //載入模板
            using (var doc = Xceed.Words.NET.DocX.Load(export.Source))
            {
                for (int i = 0; i < count; i++)
                {
                    Item itmMUserResume = itmMUserResumes.getItemByIndex(i);

                    string in_level = itmMUserResume.getProperty("in_level", "");
                    in_level = in_level.Replace("級", "");

                    string level_tag = "";
                    string level_head = "";
                    string level_title = "";

                    switch (in_level)
                    {
                        case "增能":
                            level_tag = "";
                            level_head = "講習類別： 增能";
                            level_title = "證號：";
                            break;

                        default:
                            level_tag = in_level + "級";
                            level_head = "講習類別： " + in_level + "級";
                            level_title = level_tag + " 證號：";
                            break;
                    }

                    string in_birth = itmMUserResume.getProperty("in_birth", "");

                    string in_exe_a1 = itmMUserResume.getProperty("in_exe_a1", "");
                    string in_exe_a2 = itmMUserResume.getProperty("in_exe_a2", "");
                    string in_stuff_c5 = itmMUserResume.getProperty("in_stuff_c5", "");
                    string in_official_leave_org = itmMUserResume.getProperty("in_official_leave_org", "");

                    string inn_official = "是";//■是
                    if (in_official_leave_org == "" || in_official_leave_org == "無")
                    {
                        inn_official = "否";//■
                    }

                    string inn_type = "教練證";
                    if (mt_title.Contains("裁判")) inn_type = "裁判證";

                    string inn_cert = "";
                    if (in_stuff_c5 != "")
                    {
                        inn_cert = level_title + in_stuff_c5;
                    }
                    else if (in_exe_a2 != "")
                    {
                        inn_cert = level_title + in_exe_a2;
                    }
                    else
                    {
                        inn_cert = "無";
                    }

                    itmMUserResume.setProperty("mt_title", mt_title);
                    itmMUserResume.setProperty("mt_time", mt_time);
                    itmMUserResume.setProperty("inn_type", inn_type);
                    itmMUserResume.setProperty("inn_type", inn_type);
                    itmMUserResume.setProperty("inn_cert", inn_cert);
                    itmMUserResume.setProperty("inn_birth", GetTwDay(in_birth));
                    itmMUserResume.setProperty("inn_level", level_head);
                    itmMUserResume.setProperty("inn_official", inn_official);

                    //取得模板表格
                    var template_table = doc.Tables[0];
                    var docTable = doc.InsertTable(template_table);
                 
                    var dic = new Dictionary<string, TPic>();
                    dic.Add("[$HeadShot$]", new TPic());

                    MakeTableByTemplate(cfg, docConfig, docTable, itmMUserResume, dic);

                    var head_shot = dic["[$HeadShot$]"];
                    if (head_shot.exists)
                    {
                        AppendHeadShot(cfg, doc, docTable, itmMUserResume, 144, 112, head_shot.ridx, head_shot.cidx);
                    }
                }

                doc.Tables[0].Remove();
                doc.RemoveParagraphAt(0);

                doc.SaveAs(export.File);
            }

            itmReturn.setProperty("xls_name", export.Url);
        }

        private class TPic
        {
            public bool exists { get; set; }
            public int ridx { get; set; }
            public int cidx { get; set; }
        }

        private void AppendHeadShot(TConfig cfg, Xceed.Words.NET.DocX doc, Xceed.Document.NET.Table docTable, Item itmMUser, int h, int w, int l_x, int l_y)
        {
            try
            {
                string in_photo = itmMUser.getProperty("in_photo1", "");
                if (in_photo == "") return;

                string filename = "";
                string aml = "<AML>" +
                "<Item type='File' action='get'>" +
                "<id>" + in_photo + "</id>" +
                "</Item></AML>";
                Item Files = cfg.inn.applyAML(aml);

                if (Files.getItemCount() > 0)
                {
                    filename = Files.getProperty("filename", "");//取得檔名
                }

                string id_1 = "";
                string id_2 = "";
                string id_3 = "";
                string url = @"C:\Aras\Vault\" + cfg.strDatabaseName + @"\";//讀取大頭照的路徑

                //路徑切出來
                id_1 = in_photo.Substring(0, 1);
                id_2 = in_photo.Substring(1, 2);
                id_3 = in_photo.Substring(3, 29);

                string photo = url + id_1 + @"\" + id_2 + @"\" + id_3 + @"\" + filename;
                if (System.IO.File.Exists(photo))
                {
                    Xceed.Document.NET.Image img = doc.AddImage(photo);//取得路徑圖片
                    Xceed.Document.NET.Picture p = img.CreatePicture(h, w);

                    docTable.Rows[l_x].Cells[l_y].Paragraphs.First().AppendPicture(p);
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void AppendImg(TConfig cfg, Xceed.Words.NET.DocX doc, Xceed.Document.NET.Table docTable, string in_sno, string in_type, int h, int w, int l_x, int l_y, int side)
        {
            string sql = @"
                SELECT 
                	in_file1
                	, in_file2 
                FROM
                	IN_RESUME AS t2 WITH (NOLOCK)
                INNER JOIN 
                	IN_RESUME_CERTIFICATE AS T3 WITH (NOLOCK)
                	ON T3.source_id = T2.id
                WHERE 
                	t2.in_sno = N'{#in_sno}' 
                	AND in_type like N'%{#in_type}%' 
                	AND in_photokey IN 
                	(
                		SELECT 
                			'in_photo' + in_type + ISNULL(in_country,'') + ISNULL(in_level,'')  AS  'in_photokey'
                		FROM 
                			IN_CLA_MEETING_CERTIFICATE AS T1 WITH (NOLOCK)
                		INNER JOIN 
                			[value] AS T3 WITH (NOLOCK)
                			ON T1.in_type = T3.VALUE
                		INNER JOIN  [list] AS T2 WITH (NOLOCK)
                			ON T2.id = T3.SOURCE_ID
                		WHERE 
                			t1.source_id ='{#meeting_id}' 
                			AND T2.name = N'In_Meeting_CertificateType'  
                    )
            ";

            sql = sql.Replace("{#in_sno}", in_sno)
                    .Replace("{#in_type}", in_type)
                    .Replace("{#meeting_id}", cfg.meeting_id);

            Item itmFile = cfg.inn.applySQL(sql);


            string file1 = itmFile.getProperty("in_file1", "");
            string file2 = itmFile.getProperty("in_file2", "");
            string in_photo = file1;

            if (side == 1)
            {
                in_photo = file2;
            }

            string filename = "";
            string aml = "<AML>" +
            "<Item type='File' action='get'>" +
            "<id>" + in_photo + "</id>" +
            "</Item></AML>";
            Item Files = cfg.inn.applyAML(aml);

            if (Files.getItemCount() > 0)
            {
                filename = Files.getProperty("filename", "");//取得檔名
            }

            string id_1 = "";
            string id_2 = "";
            string id_3 = "";
            string url = @"C:\Aras\Vault\PLMCTA\";//讀取大頭照的路徑
            //路徑切出來
            id_1 = in_photo.Substring(0, 1);
            id_2 = in_photo.Substring(1, 2);
            id_3 = in_photo.Substring(3, 29);

            Xceed.Document.NET.Image img = doc.AddImage(url + id_1 + @"\" + id_2 + @"\" + id_3 + @"\" + filename);//取得路徑圖片
            Xceed.Document.NET.Picture p = img.CreatePicture(h, w);

            docTable.Rows[l_x].Cells[l_y].Paragraphs.First().AppendPicture(p);
        }

        private void MakeTableByTemplate(TConfig cfg, TDocConfig docConfig, Xceed.Document.NET.Table table, Item source, Dictionary<string, TPic> pics)
        {
            int row_count = table.Rows.Count;
            for (int i = 0; i < row_count; i++)
            {
                var row = table.Rows[i];
                int cell_count = row.Cells.Count;
                for (int j = 0; j < cell_count; j++)
                {
                    var cell = row.Cells[j];
                    string text = cell.Paragraphs.First().Text;
                    if (!text.Contains("[$")) continue;

                    string property = GetCellProperty(text);
                    string key = "[$" + property + "$]";

                    if (pics.ContainsKey(key))
                    {
                        pics[key].ridx = i;
                        pics[key].cidx = j;
                    }

                    if (property == "") continue;

                    string value = source.getProperty(property, "");
                    cell.ReplaceText(key, value);
                }
            }

        }

        private string GetCellProperty(string value)
        {
            int posS = value.IndexOf("[$");
            int posE = value.IndexOf("$]");

            if (posS == -1 || posE == -1)
            {
                return "";
            }
            else
            {
                return value.Substring(posS + 2, posE - posS - 2);
            }
        }
        //取消申請
        private void CancelApply(TConfig cfg, Item itmReturn)
        {
            string sql = "";
            string aml = "";
            sql = @"SELECT in_paynumber FROM in_meeting_user WHERE id ='" + cfg.muid + "'";
            Item itmSql = cfg.inn.applySQL(sql);



            aml = "<AML>" +
                "<Item type='in_meeting_userlog' action='delete' where=\"in_user='" + cfg.muid + "'\">" +
                "</Item></AML>";

            cfg.inn.applyAML(aml);

            aml = "<AML>" +
                "<Item type='in_meeting_resume' action='delete' where=\"in_user='" + cfg.muid + "'\">" +
                "</Item></AML>";

            cfg.inn.applyAML(aml);

            aml = "<AML>" +
                "<Item type='in_meeting_record' action='delete' where=\"in_participant='" + cfg.muid + "'\">" +
                "</Item></AML>";

            cfg.inn.applyAML(aml);


            aml = "<AML>" +
                "<Item type='in_meeting_user' action='delete' id='" + cfg.muid + "'>" +
                "</Item></AML>";

            cfg.inn.applyAML(aml);

            if (!itmSql.isError() && itmSql.getItemCount() > 0)
            {
                string in_paynumber = itmSql.getProperty("in_paynumber", "");


                if (in_paynumber != "")
                {

                    aml = "<AML>" +
                        "<Item type='in_meeting_news' action='delete' where=\"in_muid='" + cfg.muid + "'\">" +
                        "</Item></AML>";

                    cfg.inn.applyAML(aml);

                    //刪除繳費單
                    sql = "DELETE FROM [IN_MEETING_PAY] WHERE [item_number] = '" + in_paynumber + "'";
                    itmSql = cfg.inn.applySQL(sql);

                    // throw new Exception("查無執行中的任務,請指定執行中任務");
                }
            }
        }

        #region 補發證件

        /// <summary>
        /// 補發證件
        /// </summary>
        private void ExportReissueWord(TConfig cfg, Item itmReturn)
        {
            Item itmMUser = cfg.inn.applySQL("SELECT * FROM IN_MEETING_USER WITH(NOLOCK) WHERE id = '" + cfg.muid + "'");
            if (itmMUser.isError() || itmMUser.getResult() == "") throw new Exception("與會者 資料錯誤");
            string in_l2 = itmMUser.getProperty("in_l2", "");
            string in_regdate = itmMUser.getProperty("in_regdate", "");
            string in_paynumber = itmMUser.getProperty("in_paynumber", "");
            string in_note = itmMUser.getProperty("in_note", "");

            Item itmViewResume = cfg.inn.applySQL("SELECT * FROM IN_RESUME WITH(NOLOCK) WHERE in_sno = '" + itmMUser.getProperty("in_sno", "") + "'");
            if (itmViewResume.isError() || itmViewResume.getResult() == "") throw new Exception("講師履歷 資料錯誤");
            string resume_id = itmViewResume.getProperty("id", "");

            Item itmMULogs = cfg.inn.applySQL("SELECT * FROM IN_MEETING_USERLOG WITH(NOLOCK) WHERE in_user = '" + cfg.muid + "'");
            if (itmMULogs.isError() || itmMULogs.getResult() == "") throw new Exception("與會者日誌 資料錯誤");
            var map = MULogValMap(itmMULogs);

            Item itmMPay = cfg.inn.applySQL("SELECT * FROM IN_MEETING_PAY WITH(NOLOCK) WHERE item_number = '" + in_paynumber + "'");
            if (itmMPay.isError() || itmMPay.getResult() == "") throw new Exception("繳費單 資料錯誤");
            string pay_undertake = itmMPay.getProperty("in_undertake", "");

            var cert_map_r = MapCertificates(cfg, resume_id);
            cfg.login_sid_01 = GetFileFullPath(cfg, cert_map_r, "1", "in_file1");
            cfg.login_sid_02 = GetFileFullPath(cfg, cert_map_r, "1", "in_file2");
            //cfg.login_degree = GetFileFullPath(cfg, cert_map_r, "6", "in_file1");

            cfg.meeting_excel_parameter = "application_reissue_path";
            cfg.doc_name = cfg.login_resume_name + "_" + in_l2 + "_" + System.DateTime.Now.ToString("yyyyMMdd");

            //申請者的資料
            Item itmData = cfg.inn.newItem();
            MULogVal(itmData, map, "in_name");
            MULogVal(itmData, map, "in_sno");
            MULogVal(itmData, map, "in_birth");
            MULogVal(itmData, map, "in_gender");
            MULogVal(itmData, map, "in_degree_label");
            MULogVal(itmData, map, "in_degree_id");
            MULogVal(itmData, map, "in_instructor_level");
            MULogVal(itmData, map, "in_instructor_id");
            MULogVal(itmData, map, "in_referee_level");
            MULogVal(itmData, map, "in_referee_id");
            MULogVal(itmData, map, "in_resident_add");
            MULogVal(itmData, map, "in_recipient");
            MULogVal(itmData, map, "in_tel_1");
            MULogVal(itmData, map, "in_tel");
            MULogVal(itmData, map, "in_add");
            MULogVal(itmData, map, "in_add_code");


            DateTime dtRegDate = GetDateTime(in_regdate).AddHours(8);
            itmData.setProperty("in_year", dtRegDate.Year.ToString());
            itmData.setProperty("in_month", dtRegDate.Month.ToString());
            itmData.setProperty("in_day", dtRegDate.Day.ToString());

            //原因
            itmData.setProperty("in_note", in_note);

            itmData.setProperty("in_degree_title", "段證號碼");
            itmData.setProperty("in_instructor_title", "教練證號");
            itmData.setProperty("in_referee_title", "裁判證號");

            string tw_birth = GetTwDay(itmData.getProperty("in_birth", ""), addYTitle: true, addHours: true);
            string tw_tel_1 = TelValue(itmData.getProperty("in_tel_1", ""));//(04)12345678
            itmData.setProperty("tw_birth", tw_birth);
            itmData.setProperty("tw_tel_1", tw_tel_1);

            string apply_item = "■教練證　□裁判證";
            string in_level = itmData.getProperty("in_instructor_level", "");
            string undertaker = cfg.loginResume.getProperty("training", "");

            if (pay_undertake == "裁判組")
            {
                apply_item = "□教練證　■裁判證";
                in_level = itmData.getProperty("in_referee_level", "");
                undertaker = cfg.loginResume.getProperty("referee", "");
            }

            itmData.setProperty("apply_item", apply_item);
            itmData.setProperty("in_level", in_level);
            itmData.setProperty("undertaker", undertaker);
            itmData.setProperty("sg", cfg.loginResume.getProperty("sg", ""));
            itmData.setProperty("deputy_sg", cfg.loginResume.getProperty("deputy_sg", ""));
            itmData.setProperty("account", cfg.loginResume.getProperty("account", ""));


            TExport export = GetExportInfo(cfg);

            TDocConfig docConfig = new TDocConfig
            {
                FontName = "標楷體",
                FontSize = 16,
                Fields = new List<TField>(),
                HeadShot = new TUnit { Height = 144, Width = 112 },
                SID = new TUnit { Height = 140, Width = 280 },
            };

            docConfig.AddKPField("in_year", "申請日期-年", font_size: 14);
            docConfig.AddKPField("in_month", "申請日期-月", font_size: 14);
            docConfig.AddKPField("in_day", "申請日期-日", font_size: 14);
            docConfig.AddRPField(0, 1, "apply_item", "申請項目", font_size: 14, is_bold: true);
            docConfig.AddRPField(1, 1, "in_name", "中文姓名", font_size: 14);
            docConfig.AddRPField(2, 1, "in_sno", "身分證號", font_size: 14);
            docConfig.AddRPField(2, 3, "in_gender", "性別", font_size: 14);
            docConfig.AddRFField(3, 1, TwBirthDay, "出生日", font_size: 14);
            docConfig.AddRFField(4, 1, DegreeValue2, "目前段數", font_size: 14);
            docConfig.AddRFField(5, 1, InstructorValue, "教練記錄", font_size: 14);
            docConfig.AddRFField(6, 1, RefereeValue, "裁判記錄", font_size: 14);
            docConfig.AddRPField(7, 1, "in_resident_add", "戶籍地址", font_size: 14);
            docConfig.AddRPField(8, 1, "in_recipient", "收件人姓名", font_size: 14);

            docConfig.AddTableField(8, 3, TelsValue, "聯絡電話、行動電話", font_size: 12);
            docConfig.AddRPField(9, 2, "in_add", "收件人地址", font_size: 14);
            docConfig.AddRPField(10, 1, "in_add_code", "郵遞區號", font_size: 14);


            docConfig.AddTableField(13, 2, WordLine, "秘書長", font_size: 12, property: "sg");
            docConfig.AddTableField(13, 4, WordLine, "副秘書長", font_size: 12, property: "deputy_sg");
            docConfig.AddTableField(13, 6, WordLine, "會計", font_size: 12, property: "account");
            docConfig.AddTableField(13, 8, WordLine, "承辦人", font_size: 12, property: "undertaker");


            docConfig.AddKPField("in_level", "級數", font_size: 26);
            docConfig.AddKPField("in_name", "姓名", font_size: 26);
            docConfig.AddKPField("in_note", "原因", font_size: 26);
            docConfig.AddKPField("in_sno", "身分證號", font_size: 26);
            docConfig.AddKPField("tw_birth", "出生年月日", font_size: 26);
            docConfig.AddKPField("in_add", "連絡地址", font_size: 26);
            docConfig.AddKPField("tw_tel_1", "連絡電話", font_size: 26);

            docConfig.Fields.Add(new TField { PT = PEnm.HeadShot, RIdx = 0, CIdx = 2, Title = "照片", FSize = 16, FName = docConfig.FontName });

            List<TField> fields2 = new List<TField>();
            fields2.Add(new TField { PT = PEnm.SID01, RIdx = 0, CIdx = 0, Title = "身分證正面", FSize = 12, FName = docConfig.FontName });
            fields2.Add(new TField { PT = PEnm.SID02, RIdx = 0, CIdx = 1, Title = "身分證反面", FSize = 12, FName = docConfig.FontName });

            //載入模板
            using (var doc = Xceed.Words.NET.DocX.Load(export.Source))
            {
                //主表格
                SetWordTableCore(doc, cfg, docConfig, doc.Tables[0], docConfig.Fields, itmData);

                //身分證表格
                SetWordTableCore(doc, cfg, docConfig, doc.Tables[1], fields2, itmData);

                //插入標題段落
                var p_sign = doc.InsertParagraph();
                p_sign.Append("已確認以上資料正確無誤    簽名：")
                   .Font(new Xceed.Document.NET.Font(docConfig.FontName))
                   .FontSize(16);
                p_sign.Alignment = Xceed.Document.NET.Alignment.center;

                p_sign.Append("            ")
                   .Font(new Xceed.Document.NET.Font(docConfig.FontName))
                   .FontSize(16)
                   .UnderlineStyle(Xceed.Document.NET.UnderlineStyle.singleLine);

                doc.SaveAs(export.File);
            }

            itmReturn.setProperty("xls_name", export.Url);
        }

        private Dictionary<string, Item> MULogValMap(Item items)
        {
            Dictionary<string, Item> map = new Dictionary<string, Item>();
            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string key = item.getProperty("in_property", "");

                if (!map.ContainsKey(key))
                {
                    map.Add(key, item);
                }
            }
            return map;
        }

        private void MULogVal(Item item, Dictionary<string, Item> map, string property)
        {
            if (!map.ContainsKey(property))
            {
                item.setProperty(property, "");
            }
            else
            {
                item.setProperty(property, map[property].getProperty("new_value", ""));
            }
        }

        #endregion 補發證件

        /// <summary>
        /// 道館團體會員入會申請表
        /// </summary>
        private void ExportVipGymWord(TConfig cfg, Item itmReturn)
        {
            TExport export = GetExportInfo(cfg);

            TDocConfig docConfig = new TDocConfig
            {
                FontName = "標楷體",
                FontSize = 16,
                Fields = new List<TField>(),
                HeadShot = new TUnit { Height = 144, Width = 112 },
                SID = new TUnit { Height = 70, Width = 140 },
            };

            docConfig.AddKPField("in_year", "申請日期-年", font_size: 14);
            docConfig.AddKPField("in_month", "申請日期-月", font_size: 14);
            docConfig.AddKPField("in_day", "申請日期-日", font_size: 14);
            //docConfig.AddKPField("in_number", "核准字號", font_size: 14);

            docConfig.AddRPField(0, 1, "in_manager_name", "所屬委員會", font_size: 20);
            docConfig.AddRPField(1, 1, "in_current_org", "團體會員名稱", font_size: 14);

            docConfig.AddRPField(2, 2, "in_resident_add", "設立地址", font_size: 14);
            docConfig.AddRPField(3, 1, "in_resident_add_code", "設立地址-郵遞區號", font_size: 12);

            docConfig.AddRPField(4, 2, "in_add", "通訊地址", font_size: 14);
            docConfig.AddRPField(5, 1, "in_add_code", "通訊地址-郵遞區號", font_size: 12);

            docConfig.AddRPField(6, 1, "in_url", "網址", font_size: 14);
            docConfig.AddRPField(6, 3, "in_email", "Email", font_size: 14);

            docConfig.AddRPField(7, 1, "in_principal", "法定代理人姓名", font_size: 14);
            //docConfig.AddRPField(7, 1, "in_principal_sno", "法定代理人身分證號", font_size: 14);
            docConfig.AddRFField(7, 3, TelValue, "法定代理人電話", font_size: 14, property: "in_principal_tel");

            docConfig.AddRPField(8, 1, "in_head_coach", "總教練姓名", font_size: 14);
            docConfig.AddRFField(8, 3, TelValue, "總教練電話", font_size: 14, property: "in_head_coach_tel");
            docConfig.AddRPField(9, 1, "in_head_coach_sno", "總教練身份證字號", font_size: 14);
            docConfig.AddRFField(9, 3, TelValue, "道館電話", font_size: 14, property: "in_tel_2");

            docConfig.AddRFField(10, 1, TwBirthDay, "總教練生日", font_size: 14);//年　　　月　　　日
            docConfig.AddRPField(10, 3, "in_tel", "行動電話", font_size: 14);
            docConfig.AddRFField(11, 1, DegreeValue2, "目前段位", font_size: 14);//段  段號：
            docConfig.AddRFField(11, 3, Aider1, "助理教練", font_size: 14);//段　姓名：
            docConfig.AddRFField(12, 1, InstructorValue, "教練級數", font_size: 14);//級  證號：
            docConfig.AddRFField(12, 3, Aider2, "助理教練", font_size: 14);//段　姓名：

            docConfig.AddTableField(15, 2, WordLine, "秘書長", font_size: 12, property: "sg");
            //docConfig.AddTableField(15, 4, WordLine, "執行秘書", font_size: 12, property: "sg");
            docConfig.AddTableField(15, 6, WordLine, "行政組", font_size: 12, property: "admin");
            docConfig.AddTableField(15, 8, WordLine, "會計", font_size: 12, property: "account");

            docConfig.Fields.Add(new TField { PT = PEnm.HeadShot, RIdx = 0, CIdx = 2, Title = "照片", FSize = 16, FName = docConfig.FontName });

            //載入模板
            using (var doc = Xceed.Words.NET.DocX.Load(export.Source))
            {
                SetWordTable(doc, cfg, docConfig);

                doc.SaveAs(export.File);
            }

            itmReturn.setProperty("xls_name", export.Url);
        }

        /// <summary>
        /// 學校社團會員入會申請表
        /// </summary>
        private void ExportVipSchoolWord(TConfig cfg, Item itmReturn)
        {
            TExport export = GetExportInfo(cfg);

            TDocConfig docConfig = new TDocConfig
            {
                FontName = "標楷體",
                FontSize = 16,
                Fields = new List<TField>(),
                HeadShot = new TUnit { Height = 144, Width = 112 },
                SID = new TUnit { Height = 70, Width = 140 },
            };

            docConfig.AddKPField("in_year", "申請日期-年", font_size: 14);
            docConfig.AddKPField("in_month", "申請日期-月", font_size: 14);
            docConfig.AddKPField("in_day", "申請日期-日", font_size: 14);
            //docConfig.AddKPField("in_number", "核准字號", font_size: 14);

            docConfig.AddRPField(0, 1, "in_manager_name", "所屬委員會", font_size: 20);
            docConfig.AddRPField(1, 1, "in_current_org", "團體會員名稱", font_size: 14);

            docConfig.AddRPField(2, 2, "in_resident_add", "設立地址", font_size: 14);
            docConfig.AddRPField(3, 1, "in_resident_add_code", "設立地址-郵遞區號", font_size: 12);

            docConfig.AddRPField(4, 2, "in_add", "通訊地址", font_size: 14);
            docConfig.AddRPField(5, 1, "in_add_code", "通訊地址-郵遞區號", font_size: 12);

            docConfig.AddRPField(6, 1, "in_url", "網址", font_size: 14);
            docConfig.AddRPField(6, 3, "in_email", "Email", font_size: 14);

            docConfig.AddRPField(7, 1, "in_principal", "法定代理人姓名", font_size: 14);
            //docConfig.AddRPField(7, 1, "in_principal_sno", "法定代理人身分證號", font_size: 14);
            docConfig.AddRFField(7, 3, TelValue, "法定代理人電話", font_size: 14, property: "in_principal_tel");

            docConfig.AddRPField(8, 1, "in_head_coach", "總教練姓名", font_size: 14);
            docConfig.AddRFField(8, 3, TelValue, "總教練電話", font_size: 14, property: "in_head_coach_tel");
            docConfig.AddRPField(9, 1, "in_head_coach_sno", "總教練身份證字號", font_size: 14);
            docConfig.AddRFField(9, 3, TelValue, "道館電話", font_size: 14, property: "in_tel_2");

            docConfig.AddRFField(10, 1, TwBirthDay, "總教練生日", font_size: 14);//年　　　月　　　日
            docConfig.AddRPField(10, 3, "in_tel", "行動電話", font_size: 14);
            docConfig.AddRFField(11, 1, DegreeValue2, "目前段位", font_size: 14);//段  段號：
            docConfig.AddRFField(11, 3, Aider1, "助理教練", font_size: 14);//段　姓名：
            docConfig.AddRFField(12, 1, InstructorValue, "教練級數", font_size: 14);//級  證號：
            docConfig.AddRFField(12, 3, Aider2, "助理教練", font_size: 14);//段　姓名：

            docConfig.AddTableField(15, 3, WordLine, "秘書長", font_size: 12, property: "sg");
            //docConfig.AddTableField(15, 5, WordLine, "執行秘書", font_size: 12, property: "sg");
            docConfig.AddTableField(15, 7, WordLine, "行政組", font_size: 12, property: "admin");
            docConfig.AddTableField(15, 9, WordLine, "會計", font_size: 12, property: "account");

            docConfig.Fields.Add(new TField { PT = PEnm.HeadShot, RIdx = 0, CIdx = 2, Title = "照片", FSize = 16, FName = docConfig.FontName });

            //載入模板
            using (var doc = Xceed.Words.NET.DocX.Load(export.Source))
            {
                SetWordTable(doc, cfg, docConfig);

                doc.SaveAs(export.File);
            }

            itmReturn.setProperty("xls_name", export.Url);
        }

        /// <summary>
        /// 一般團體會員入會申請表
        /// </summary>
        private void ExportVipGroupWord(TConfig cfg, Item itmReturn)
        {
            TExport export = GetExportInfo(cfg);

            TDocConfig docConfig = new TDocConfig
            {
                FontName = "標楷體",
                FontSize = 16,
                Fields = new List<TField>(),
                HeadShot = new TUnit { Height = 144, Width = 112 },
                SID = new TUnit { Height = 70, Width = 140 },
            };

            docConfig.AddKPField("in_year", "申請日期-年", font_size: 14);
            docConfig.AddKPField("in_month", "申請日期-月", font_size: 14);
            docConfig.AddKPField("in_day", "申請日期-日", font_size: 14);
            //docConfig.AddKPField("in_number", "核准字號", font_size: 14);

            docConfig.AddRPField(0, 1, "in_current_org", "團體會員名稱", font_size: 20);

            docConfig.AddRPField(1, 2, "in_resident_add", "設立地址", font_size: 14);
            docConfig.AddRPField(2, 1, "in_resident_add_code", "設立地址-郵遞區號", font_size: 12);

            docConfig.AddRPField(3, 2, "in_add", "通訊地址", font_size: 14);
            docConfig.AddRPField(4, 1, "in_add_code", "通訊地址-郵遞區號", font_size: 12);

            docConfig.AddRPField(5, 1, "in_url", "網址", font_size: 14);
            docConfig.AddRPField(5, 3, "in_email", "Email", font_size: 14);

            docConfig.AddRPField(6, 1, "in_principal", "法定代理人姓名", font_size: 14);
            docConfig.AddRPField(7, 1, "in_principal_sno", "法定代理人身分證號", font_size: 14);
            docConfig.AddRPField(7, 3, "in_principal_tel", "法定代理人電話", font_size: 14);

            docConfig.AddRPField(8, 1, "in_emrg_contact1", "聯絡人姓名", font_size: 14);
            docConfig.AddRPField(8, 3, "in_emrg_tel1", "聯絡人電話", font_size: 14);
            docConfig.AddRPField(15, 1, "undertaker", "承辦人", font_size: 14);
            docConfig.AddRPField(15, 2, "account", "會計", font_size: 14);

            docConfig.Fields.Add(new TField { PT = PEnm.HeadShot, RIdx = 0, CIdx = 2, Title = "照片", FSize = 16, FName = docConfig.FontName });
            docConfig.Fields.Add(new TField { PT = PEnm.SID01, RIdx = 10, CIdx = 0, Title = "正面(浮貼)", FSize = 16, FName = docConfig.FontName });
            docConfig.Fields.Add(new TField { PT = PEnm.SID02, RIdx = 11, CIdx = 0, Title = "反面(浮貼)", FSize = 16, FName = docConfig.FontName });

            //載入模板
            using (var doc = Xceed.Words.NET.DocX.Load(export.Source))
            {
                //doc.Tables[0].Remove();

                //doc.RemoveParagraphAt(0);
                //doc.RemoveParagraphAt(0);

                SetWordTable(doc, cfg, docConfig);

                doc.SaveAs(export.File);

            }

            itmReturn.setProperty("xls_name", export.Url);
        }

        /// <summary>
        /// 個人會員入會申請表
        /// </summary>
        private void ExportVipMemberWord(TConfig cfg, Item itmReturn)
        {
            TExport export = GetExportInfo(cfg);

            TDocConfig docConfig = new TDocConfig
            {
                FontName = "標楷體",
                FontSize = 16,
                Fields = new List<TField>(),
                HeadShot = new TUnit { Height = 144, Width = 112 },
                SID = new TUnit { Height = 100, Width = 200 },
                Degree = new TUnit { Height = 100, Width = 200 },
            };

            docConfig.AddKPField("in_year", "申請日期-年", font_size: 16);
            docConfig.AddKPField("in_month", "申請日期-月", font_size: 16);
            docConfig.AddKPField("in_day", "申請日期-日", font_size: 16);
            docConfig.AddKPField("in_number", "會員編號", font_size: 14);

            docConfig.AddRPField(0, 1, "in_name", "姓名", font_size: 16);
            docConfig.AddRPField(0, 3, "in_gender", "性別", font_size: 16);

            docConfig.AddRPField(1, 1, "in_sno", "身分證號", font_size: 16);
            docConfig.AddRPField(1, 3, "in_tel", "行動電話", font_size: 16);

            docConfig.AddRPField(2, 1, "in_tel_1", "連絡電話", font_size: 16);
            docConfig.AddRPField(2, 3, "in_education", "最高學歷", font_size: 12);

            docConfig.AddRPField(3, 1, "in_work_history", "主要經歷", font_size: 12);

            docConfig.AddRPField(4, 2, "in_work_org", "服務單位", font_size: 16);
            docConfig.AddRPField(5, 2, "in_title", "職稱", font_size: 16);

            docConfig.AddRPField(6, 1, "in_resident_add", "戶籍地址", font_size: 16);
            docConfig.AddRPField(7, 1, "in_add", "通訊地址", font_size: 16);
            docConfig.AddRPField(8, 1, "in_email", "e-mail", font_size: 16);

            docConfig.AddRPField(12, 1, "received", "承辦人", font_size: 16);
            docConfig.AddRPField(12, 3, "account", "會計", font_size: 16);
            docConfig.AddRPField(12, 5, "deputy_sg", "副秘書長", font_size: 16);
            docConfig.AddRPField(13, 1, "sg", "秘書長", font_size: 16);
            docConfig.AddRPField(13, 3, "chairman", "理事長", font_size: 16);

            docConfig.Fields.Add(new TField { PT = PEnm.HeadShot, RIdx = 0, CIdx = 4, Title = "照片", FSize = 16, FName = docConfig.FontName });
            docConfig.Fields.Add(new TField { PT = PEnm.SID01, RIdx = 9, CIdx = 1, Title = "身分證影本(正面)", FSize = 16, FName = docConfig.FontName });
            docConfig.Fields.Add(new TField { PT = PEnm.SID02, RIdx = 10, CIdx = 2, Title = "身分證影本(反面)", FSize = 16, FName = docConfig.FontName });
            // docConfig.Fields.Add(new TField { PT = PEnm.Degree, RIdx = 10, CIdx = 1, Title = "級段證影本", FSize = 16, FName = docConfig.FontName });

            //載入模板
            using (var doc = Xceed.Words.NET.DocX.Load(export.Source))
            {
                //doc.Tables[0].Remove();

                //doc.RemoveParagraphAt(0);
                //doc.RemoveParagraphAt(0);

                SetWordTable(doc, cfg, docConfig);

                //插入標題段落
                var p_sign = doc.InsertParagraph();
                p_sign.Append("已確認以上資料正確無誤    簽名：")
                   .Font(new Xceed.Document.NET.Font(docConfig.FontName))
                   .FontSize(16);
                p_sign.Alignment = Xceed.Document.NET.Alignment.center;

                p_sign.Append("            ")
                   .Font(new Xceed.Document.NET.Font(docConfig.FontName))
                   .FontSize(16)
                   .UnderlineStyle(Xceed.Document.NET.UnderlineStyle.singleLine);

                doc.SaveAs(export.File);

            }

            itmReturn.setProperty("xls_name", export.Url);
        }

        private void SetWordTable(Xceed.Words.NET.DocX doc, TConfig cfg, TDocConfig docConfig)
        {
            //取得模板表格
            SetWordTableCore(doc, cfg, docConfig, doc.Tables[0], docConfig.Fields, cfg.loginResume);
        }

        private void SetWordTableCore(Xceed.Words.NET.DocX doc, TConfig cfg, TDocConfig docConfig, Xceed.Document.NET.Table table, List<TField> fields, Item source)
        {
            foreach (var field in fields)
            {
                var ridx = field.RIdx;
                var cidx = field.CIdx;
                var value = "";

                switch (field.PT)
                {
                    case PEnm.KP:
                        value = source.getProperty(field.Property, "");
                        doc.ReplaceText("[$" + field.Property + "$]", value);
                        break;

                    case PEnm.KF:
                        value = field.FuncVal(source, field);
                        doc.ReplaceText("[$" + field.Property + "$]", value);
                        break;

                    case PEnm.RP:
                        value = source.getProperty(field.Property, "");
                        var p = table.Rows[ridx].Cells[cidx].Paragraphs.First();
                        if (field.IsBold)
                        {
                            p.Append(value).Font(field.FName).FontSize(field.FSize).Bold();
                        }
                        else
                        {
                            p.Append(value).Font(field.FName).FontSize(field.FSize);
                        }
                        break;

                    case PEnm.RF:
                        value = field.FuncVal(source, field);
                        table.Rows[ridx]
                            .Cells[cidx]
                            .Paragraphs.First()
                            .Append(value)
                            .Font(field.FName).FontSize(field.FSize);
                        break;

                    case PEnm.HeadShot:
                        SetPhoto(doc, cfg, table, cfg.login_headshot, field, docConfig.HeadShot);
                        break;

                    case PEnm.SID01:
                        SetPhoto(doc, cfg, table, cfg.login_sid_01, field, docConfig.SID);
                        break;

                    case PEnm.SID02:
                        SetPhoto(doc, cfg, table, cfg.login_sid_02, field, docConfig.SID);
                        break;

                    case PEnm.DegreeTw:
                        SetPhoto(doc, cfg, table, cfg.login_tw_degree, field, docConfig.Degree);
                        break;

                    case PEnm.DegreeGl:
                        SetPhoto(doc, cfg, table, cfg.login_gl_degree, field, docConfig.Degree);
                        break;

                    case PEnm.Table:
                        field.TableVal(table, cfg, source, field);
                        break;
                }
            }
        }

        private void SetPhoto(Xceed.Words.NET.DocX doc, TConfig cfg, Xceed.Document.NET.Table table, TPhoto photo, TField field, TUnit unit)
        {
            if (photo.valid)
            {
                var img = doc.AddImage(photo.path);
                var pic = img.CreatePicture(unit.Height, unit.Width);

                table.Rows[field.RIdx]
                    .Cells[field.CIdx]
                    .Paragraphs.First()
                    .AppendPicture(pic);
            }
            else
            {
                string message = photo.message == ""
                    ? field.Title
                    : photo.message;

                table.Rows[field.RIdx]
                    .Cells[field.CIdx]
                    .Paragraphs.First()
                    .Append(message)
                    .Font(field.FName).FontSize(field.FSize);
            }
        }

        //取得檔案完整絕對路徑
        private TPhoto GetFileFullPath(TConfig cfg, Dictionary<string, Item> map, string in_type, string property, string map_type = "")
        {
            if (!map.ContainsKey(in_type))
            {
                if (map_type != "" && map.ContainsKey(map_type))
                {
                    Item item = map[map_type];
                    string fileid = item.getProperty(property);
                    return GetFileFullPath(cfg, fileid);
                }
                else
                {
                    return GetFileFullPath(cfg, "");
                }
            }
            else
            {
                Item item = map[in_type];
                string fileid = item.getProperty(property);
                return GetFileFullPath(cfg, fileid);
            }
        }

        //取得檔案完整絕對路徑
        private TPhoto GetFileFullPath(TConfig cfg, string fileid)
        {
            TPhoto result = new TPhoto { name = "" };

            if (fileid != "")
            {
                Item itmFile = cfg.inn.applySQL("SELECT * FROM [FILE] WITH(NOLOCK) WHERE id = '" + fileid + "'");
                if (!itmFile.isError() && itmFile.getResult() != "")
                {
                    result.name = itmFile.getProperty("filename", "");
                }
            }

            if (result.name != "")
            {
                //路徑切出來
                string id_1 = fileid.Substring(0, 1);
                string id_2 = fileid.Substring(1, 2);
                string id_3 = fileid.Substring(3, 29);
                string path = cfg.aras_vault_url + @"\" + id_1 + @"\" + id_2 + @"\" + id_3 + @"\" + result.name;
                //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "headShot path: " + path);

                if (!System.IO.File.Exists(path))
                {
                    result.valid = false;
                    result.message = "檔案不存在";
                }
                else if (!IsImage(path))
                {
                    result.valid = false;
                    result.message = "非圖片";
                }
                else
                {
                    result.valid = true;
                    result.message = "";
                    result.path = path;
                }
            }
            else
            {
                //無圖片，不提示訊息
                result.valid = false;
                result.message = "";
            }

            return result;
        }

        //判斷檔案是否真為圖片
        private bool IsImage(string path)
        {
            bool result = false;
            try
            {
                using (var img = System.Drawing.Image.FromFile(path))
                {
                    result = true;
                }
            }
            catch (Exception e)
            {
            }
            return result;
        }

        //段位
        private string DegreeValue(Item source, TField field)
        {
            //__段 NO:
            //無

            //段位(數值)
            string in_degree = source.getProperty("in_degree", ""); ;
            //段位(標籤)
            string in_degree_label = source.getProperty("in_degree_label", "");
            //段位證號
            string in_degree_id = source.getProperty("in_degree_id", "");

            if (in_degree_label == "")
            {
                if (in_degree == "")
                {
                    in_degree_label = "無";
                }
                else
                {
                    in_degree_label = GetDegreeDisplay(in_degree);
                    if (in_degree_id != "")
                    {
                        in_degree_label += "  NO: " + in_degree_id;
                    }
                }
            }
            else
            {
                if (in_degree_id != "")
                {
                    in_degree_label += "  NO: " + in_degree_id;
                }
            }

            return in_degree_label;
        }

        //段位
        private string DegreeValue2(Item source, TField field)
        {
            //__段  段號：
            //無

            //段位(數值)
            string level = source.getProperty("in_degree", ""); ;
            //段位(標籤)
            string label = source.getProperty("in_degree_label", "");
            //段位證號
            string id = source.getProperty("in_degree_id", "");
            //標題
            string title = source.getProperty("in_degree_title", "");

            if (label == "")
            {
                if (level == "")
                {
                    label = "無";
                }
                else
                {
                    label = GetDegreeDisplay(level);
                    if (id != "")
                    {
                        if (title == "")
                        {
                            label += "  段號：" + id;
                        }
                        else
                        {
                            label += "  " + title + "：" + id;

                        }
                    }
                }
            }
            else
            {
                if (id != "")
                {
                    if (title == "")
                    {
                        label += "  段號：" + id;
                    }
                    else
                    {
                        label += "  " + title + "：" + id;

                    }
                }
            }

            return label;
        }

        //教練證
        private string InstructorValue(Item source, TField field)
        {
            //__級  證號：
            //無
            string result = "";
            //級數
            string level = source.getProperty("in_instructor_level", "");
            //證號
            string id = source.getProperty("in_instructor_id", "").Trim();
            //標題
            string title = source.getProperty("in_instructor_title", "").Trim();

            return CertValue(level, id, title);
        }

        //裁判證
        private string RefereeValue(Item source, TField field)
        {
            //級數
            string level = source.getProperty("in_referee_level", "");
            //證號
            string id = source.getProperty("in_referee_id", "").Trim();
            //標題
            string title = source.getProperty("in_referee_title", "").Trim();

            return CertValue(level, id, title);
        }

        //教練證 or 裁判證
        private string CertValue(string level, string id, string title)
        {
            //__級  證號：
            //無
            string result = "";

            if (id == "")
            {
                if (level == "無")
                {
                    result = "無";
                }
                else
                {
                    result = level + "級";
                }
            }
            else
            {
                if (level == "無")
                {
                    result = "無";
                }
                else
                {
                    if (title == "")
                    {
                        result = level + "級  證號：" + id;
                    }
                    else
                    {
                        result = level + "級  " + title + "：" + id;

                    }
                }
            }

            return result;
        }

        //助理教練
        private string Aider1(Item source, TField field)
        {
            string in_aider = source.getProperty("in_aider_1", ""); ;
            string in_aider_degree = source.getProperty("in_aider_degree_1", "");
            return AiderValue(in_aider, in_aider_degree);
        }

        //助理教練
        private string Aider2(Item source, TField field)
        {
            string in_aider = source.getProperty("in_aider_2", ""); ;
            string in_aider_degree = source.getProperty("in_aider_degree_2", "");
            return AiderValue(in_aider, in_aider_degree);
        }

        //助理教練
        private string AiderValue(string name, string degree)
        {
            //__段  姓名：
            if (name != "")
            {
                return degree + "  姓名：" + name;
            }
            else
            {
                return degree;
            }
        }


        //電話
        private string TelValue(Item source, TField field)
        {
            return TelValue(source.getProperty(field.Property, ""));
        }

        //電話
        private string TelValue(string source)
        {
            string value = source.Replace("-", "");
            if (value.Length < 2)
            {
                return value;
            }
            else
            {
                return "（" + value.Substring(0, 2) + "）" + value.Substring(2, value.Length - 2);
            }
        }

        //民國生日
        private string TwBirthDay(Item source, TField field)
        {
            string in_birth = source.getProperty("in_birth", "");
            return GetTwDay(in_birth, addYTitle: true, addHours: true);
        }

        private void WordLine(Xceed.Document.NET.Table table, TConfig cfg, Item source, TField field)
        {
            string value = source.getProperty(field.Property, "");

            var p = table.Rows[field.RIdx]
                .Cells[field.CIdx]
                .Paragraphs.First();

            var end = value.Length - 1;
            for (int i = 0; i < value.Length; i++)
            {
                var c = value[i].ToString();
                if (i == end)
                {
                    p.Append(c).Font(field.FName).FontSize(field.FSize);
                }
                else
                {
                    p.Append(c).Font(field.FName).FontSize(field.FSize).AppendLine();

                }
            }
        }

        private void TelsValue(Xceed.Document.NET.Table table, TConfig cfg, Item source, TField field)
        {
            string in_tel = source.getProperty("in_tel", "");
            string tw_tel_1 = source.getProperty("tw_tel_1", "");

            var p = table.Rows[field.RIdx]
                .Cells[field.CIdx]
                .Paragraphs.First();

            p.Append(tw_tel_1).Font(field.FName).FontSize(field.FSize).AppendLine();
            p.Append(in_tel).Font(field.FName).FontSize(field.FSize);
        }

        private string GetDegreeDisplay(string value)
        {
            switch (value)
            {
                case "": return "無段位";
                case "10": return "白帶";
                case "30": return "黃帶";
                case "40": return "黃綠帶";
                case "50": return "綠帶";
                case "60": return "綠藍帶";
                case "70": return "藍帶";
                case "80": return "藍紅帶";
                case "90": return "紅帶";
                case "100": return "紅黑帶";
                case "150": return "黑帶一品";
                case "250": return "黑帶二品";
                case "350": return "黑帶三品";
                case "1000": return "壹段";
                case "2000": return "貳段";
                case "3000": return "參段";
                case "4000": return "肆段";
                case "5000": return "伍段";
                case "6000": return "陸段";
                case "7000": return "柒段";
                case "8000": return "捌段";
                case "9000": return "玖段";

                case "11000": return "11";
                case "12000": return "12";
                case "13000": return "13";
                default: return "";
            }
        }

        /// <summary>
        /// 取得匯出設定資訊
        /// </summary>
        private TExport GetExportInfo(TConfig cfg)
        {
            Item itmPath = GetDocPaths(cfg, cfg.meeting_excel_parameter);

            string export_path = itmPath.getProperty("export_path", "").TrimEnd('\\');
            string ext_name = ".docx";
            string doc_name = cfg.doc_name;

            string guid = System.Guid.NewGuid().ToString().Substring(0, 6).ToUpper()
                + "_" + DateTime.Now.ToString("yyyyMMddHHmmssfff");

            string fname = doc_name + "_" + guid + ext_name;
            string doc_file = export_path + "\\" + fname;
            string doc_url = fname;

            return new TExport
            {
                Source = itmPath.getProperty("template_path", ""),
                File = doc_file,
                Url = doc_url,
            };
        }

        /// <summary>
        /// 取得樣板與匯出路徑
        /// </summary>
        private Item GetDocPaths(TConfig cfg, string in_name)
        {
            Item itmResult = cfg.inn.newItem();

            //Excel 匯出路徑 (from 原創.設定參數)
            string sql = @"
                SELECT 
                    t2.in_name AS 'variable'
                    , t1.in_name
                    , t1.in_value 
                FROM 
                    In_Variable_Detail t1 WITH(NOLOCK)
                INNER JOIN 
                    In_Variable t2 WITH(NOLOCK) 
                    ON t2.ID = t1.SOURCE_ID 
                WHERE 
                    t2.in_name = N'meeting_excel'  
                    AND t1.in_name IN (N'export_path', N'{#in_name}')
            ";

            sql = sql.Replace("{#in_name}", in_name);

            //cfg.CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item items = cfg.inn.applySQL(sql);

            if (items.isError() || items.getItemCount() < 1)
            {
                throw new Exception("未設定樣板與匯出路徑 _# " + in_name);
            }

            for (int i = 0; i < items.getItemCount(); i++)
            {
                Item item = items.getItemByIndex(i);
                string xls_name = item.getProperty("in_name", "");
                string xls_value = item.getProperty("in_value", "");

                if (xls_name == "export_path")
                {
                    itmResult.setProperty("export_path", xls_value);
                }
                else
                {
                    itmResult.setProperty("template_path", xls_value);
                }
            }

            return itmResult;
        }

        //取得證照列表
        private Dictionary<string, Item> MapCertificates(TConfig cfg, string resume_id)
        {
            string sql = "SELECT * FROM IN_RESUME_CERTIFICATE WITH(NOLOCK) WHERE source_id = '" + resume_id + "'";
            Item items = cfg.inn.applySQL(sql);
            int count = items.getItemCount();
            Dictionary<string, Item> map = new Dictionary<string, Item>();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_photokey = item.getProperty("in_photokey", "").Replace("in_photo", "");
                if (!map.ContainsKey(in_photokey))
                {
                    map.Add(in_photokey, item);
                }
            }
            return map;
        }

        /// <summary>
        /// 取得名次資訊
        /// </summary>
        private Item GetRanks(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmMeeting)
        {
            string meeting_id = itmMeeting.getProperty("id", "");
            string meeting_title = itmMeeting.getProperty("in_title", "");

            string sql = @"
        SELECT
            t2.id AS 'program_id'     
            , ISNULL(t2.in_short_name, t2.in_display) AS 'in_short_name'
            , t2.in_l1
            , t2.in_l2
            , t2.in_l3
            , t2.in_team_count
            , t1.in_current_org
            , t1.in_short_org
            , t1.map_short_org
            , t1.map_org_name
            , t1.in_team
            , t1.in_name
            , t1.in_sign_no
            , t1.in_section_no
            , t1.in_sno
            , t1.in_final_rank
        FROM
            VU_MEETING_PTEAM t1 WITH(NOLOCK)
        INNER JOIN
            IN_MEETING_PROGRAM t2 WITH(NOLOCK)
            ON t2.id = t1.source_id
        WHERE
            t2.in_meeting = '{#meeting_id}'
            AND ISNULL(t2.in_program, '') = ''
            AND ISNULL(t1.in_final_rank, '') NOT IN ('', '0')
        ORDER BY
            t2.in_sort_order
            , t1.in_stuff_b1
        ";

            sql = sql.Replace("{#meeting_id}", meeting_id);

            return inn.applySQL(sql);

        }

        #region 資料模型

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public Innovator inn { get; set; }

            /// <summary>
            /// Aras Vault Site 絕對路徑
            /// </summary>
            public string aras_vault_url { get; set; }

            //外部輸入區

            public string scene { get; set; }
            public string in_type { get; set; }
            public string in_value { get; set; }

            public string meeting_id { get; set; }
            public string meeting_type { get; set; }
            public string resume_sno { get; set; }
            public string muid { get; set; }

            //權限區

            /// <summary>
            /// 最高管理者
            /// </summary>
            public bool isMeetingAdmin { get; set; }

            /// <summary>
            /// 地區管理者
            /// </summary>
            public bool isCommittee { get; set; }

            /// <summary>
            /// 道館主
            /// </summary>
            public bool isGymOwner { get; set; }

            /// <summary>
            /// 道館協助
            /// </summary>
            public bool isGymAssistant { get; set; }


            //登入者資訊區

            /// <summary>
            /// 登入者 Resume 資料
            /// </summary>
            public Item loginResume { get; set; }

            /// <summary>
            /// 登入者 Resume id
            /// </summary>
            public string login_resume_id { get; set; }

            /// <summary>
            /// 登入者姓名
            /// </summary>
            public string login_resume_name { get; set; }

            /// <summary>
            /// 登入者身分證號
            /// </summary>
            public string login_resume_sno { get; set; }

            /// <summary>
            /// 登入者 User id
            /// </summary>
            public string login_user_id { get; set; }

            /// <summary>
            /// 登入者大頭照 file id
            /// </summary>
            public string login_photo { get; set; }

            /// <summary>
            /// 登入者大頭照 完整資訊
            /// </summary>
            public TPhoto login_headshot { get; set; }

            /// <summary>
            /// 範本參數
            /// </summary>
            public string meeting_excel_parameter { get; set; }

            /// <summary>
            /// 匯出檔案名稱
            /// </summary>
            public string doc_name { get; set; }

            /// <summary>
            /// 報名者資料
            /// </summary>
            public Item itmRegInfo { get; set; }

            /// <summary>
            /// 登入者身分證正面 完整資訊
            /// </summary>
            public TPhoto login_sid_01 { get; set; }

            /// <summary>
            /// 登入者身分證反面 完整資訊
            /// </summary>
            public TPhoto login_sid_02 { get; set; }

            /// <summary>
            /// 登入者級段證 完整資訊 (國內)
            /// </summary>
            public TPhoto login_tw_degree { get; set; }

            /// <summary>
            /// 登入者級段證 完整資訊 (國際)
            /// </summary>
            public TPhoto login_gl_degree { get; set; }

        }

        /// <summary>
        /// 匯出資料模型
        /// </summary>
        private class TExport
        {
            /// <summary>
            /// 樣板來源完整路徑
            /// </summary>
            public string Source { get; set; }

            /// <summary>
            /// 檔案名稱
            /// </summary>
            public string File { get; set; }

            /// <summary>
            /// 檔案網址
            /// </summary>
            public string Url { get; set; }
        }

        /// <summary>
        /// 組態
        /// </summary>
        private class TDocConfig
        {
            /// <summary>
            /// 字型名稱
            /// </summary>
            public string FontName { get; set; }

            /// <summary>
            /// 字型大小
            /// </summary>
            public int FontSize { get; set; }

            /// <summary>
            /// 欄位清單
            /// </summary>
            public List<TField> Fields { get; set; }

            public TUnit HeadShot { get; set; }
            public TUnit SID { get; set; }
            public TUnit Degree { get; set; }

            public void AddKPField(string property, string title, int font_size = 0, string font_name = "")
            {
                var fsize = font_size == 0 ? this.FontSize : font_size;
                var fname = font_name == "" ? this.FontName : font_name;

                this.Fields.Add(new TField { PT = PEnm.KP, Property = property, Title = title, FName = fname, FSize = fsize });
            }

            public void AddRPField(int ridx, int cidx, string property, string title, int font_size = 0, string font_name = "", bool is_bold = false)
            {
                var fsize = font_size == 0 ? this.FontSize : font_size;
                var fname = font_name == "" ? this.FontName : font_name;

                this.Fields.Add(new TField { PT = PEnm.RP, RIdx = ridx, CIdx = cidx, Property = property, Title = title, FName = fname, FSize = fsize, IsBold = is_bold });
            }

            public void AddRFField(int ridx, int cidx, Func<Item, TField, string> func, string title, int font_size = 0, string font_name = "", string property = "")
            {
                var fsize = font_size == 0 ? this.FontSize : font_size;
                var fname = font_name == "" ? this.FontName : font_name;

                this.Fields.Add(new TField { PT = PEnm.RF, RIdx = ridx, CIdx = cidx, FuncVal = func, Title = title, FName = fname, FSize = fsize, Property = property });
            }

            public void AddTableField(int ridx, int cidx, Action<Xceed.Document.NET.Table, TConfig, Item, TField> acion, string title, int font_size = 0, string font_name = "", string property = "")
            {
                var fsize = font_size == 0 ? this.FontSize : font_size;
                var fname = font_name == "" ? this.FontName : font_name;

                this.Fields.Add(new TField { PT = PEnm.Table, RIdx = ridx, CIdx = cidx, TableVal = acion, Title = title, FName = fname, FSize = fsize, Property = property });
            }
        }

        private class TUnit
        {
            /// <summary>
            /// 照片-高
            /// </summary>
            public int Height { get; set; }

            /// <summary>
            /// 照片-寬
            /// </summary>
            public int Width { get; set; }
        }


        /// <summary>
        /// 取值
        /// </summary>
        private enum PEnm
        {
            /// <summary>
            /// Key + Property
            /// </summary>
            KP = 100,
            /// <summary>
            /// Key + Function
            /// </summary>
            KF = 110,
            /// <summary>
            /// RC + Property
            /// </summary>
            RP = 200,
            /// <summary>
            /// RC + Function
            /// </summary>
            RF = 210,
            /// <summary>
            /// RC + Image
            /// </summary>
            HeadShot = 1100,
            /// <summary>
            /// RC + Image
            /// </summary>
            SID01 = 2110,
            /// <summary>
            /// RC + Image
            /// </summary>
            SID02 = 2120,
            /// <summary>
            /// RC + Image
            /// </summary>
            DegreeTw = 2600,
            /// <summary>
            /// RC + Image
            /// </summary>
            DegreeGl = 2700,
            /// <summary>
            /// Custom
            /// </summary>
            Table = 3100,
        }

        /// <summary>
        /// 欄位
        /// </summary>
        private class TField
        {
            /// <summary>
            /// Row Index
            /// </summary>
            public int RIdx { get; set; }

            /// <summary>
            /// Col Index
            /// </summary>
            public int CIdx { get; set; }

            /// <summary>
            /// 標題
            /// </summary>
            public string Title { get; set; }

            /// <summary>
            /// 屬性
            /// </summary>
            public string Property { get; set; }

            /// <summary>
            /// 格式
            /// </summary>
            public string Format { get; set; }

            /// <summary>
            /// 字型名稱
            /// </summary>
            public string FName { get; set; }

            /// <summary>
            /// 字型 Size
            /// </summary>
            public int FSize { get; set; }

            /// <summary>
            /// 欄寬
            /// </summary>
            public int Width { get; set; }

            /// <summary>
            /// 是否為粗體
            /// </summary>
            public bool IsBold { get; set; }

            /// <summary>
            /// 取值
            /// </summary>
            public PEnm PT { get; set; }

            /// <summary>
            /// 特殊 Func
            /// </summary>
            public Func<Item, TField, string> FuncVal { get; set; }

            /// <summary>
            /// Table Action
            /// </summary>
            public Action<Xceed.Document.NET.Table, TConfig, Item, TField> TableVal { get; set; }

        }

        private class TPhoto
        {
            public bool valid { get; set; }
            public string message { get; set; }
            public string name { get; set; }
            public string path { get; set; }
        }


        #endregion 資料模型

        /// <summary>
        /// 轉換為整數
        /// </summary>
        private int GetIntVal(string value, int def = 0)
        {
            int result = def;
            Int32.TryParse(value, out result);
            return result;
        }

        /// <summary>
        /// 轉換為日期時間
        /// </summary>
        private string GetDateTimeVal(string value, string format)
        {
            DateTime dt = GetDateTime(value);
            if (dt != DateTime.MinValue)
            {
                return dt.ToString(format);
            }
            else
            {
                return "";
            }
        }

        /// <summary>
        /// 轉換為日期時間
        /// </summary>
        private DateTime GetDateTime(string value)
        {
            DateTime result = DateTime.MinValue;
            DateTime.TryParse(value.Replace("/", "-"), out result);
            return result;
        }

        private string[] PartialName(string value)
        {
            var empty = new string[] { "", "", "" };
            if (value == "")
            {
                return empty;
            }

            char c = ' ';
            if (!value.Contains(" "))
            {
                if (!value.Contains("-"))
                {
                    return empty;
                }
                else
                {
                    c = '-';
                }
            }

            string[] names = value.Split(new char[] { c }, StringSplitOptions.RemoveEmptyEntries);
            if (names == null || names.Length == 0)
            {
                return empty;
            }
            else if (names.Length == 1)
            {
                return new string[] { names[0], "", "" };
            }
            else if (names.Length == 2)
            {
                return new string[] { names[0], "", names[1] };
            }
            else
            {
                return new string[] { names[0], names[1], names[2] };
            }
        }

        private string[] PartialAdd(string value)
        {
            if (value == "" || value.Length <= 6)
            {
                return new string[] { "", "" };
            }

            string code1 = value[2].ToString();
            string code2 = value[5].ToString();

            switch (code2)
            {
                case "區":
                case "鄉":
                case "鎮":
                    return new string[] { value.Substring(0, 6), value.Substring(6, value.Length - 6) };
                default:
                    if (code1 == "市" || code1 == "縣")
                    {
                        return new string[] { value.Substring(0, 3), value.Substring(3, value.Length - 3) };
                    }
                    else
                    {
                        return new string[] { "", value };
                    }
            }
        }

        private string GetTwDay(string value, bool addYTitle = false, bool addHours = false)
        {
            if (value == "")
            {
                return "";
            }

            DateTime dt = GetDateTime(value);
            if (dt == DateTime.MinValue)
            {
                return "";
            }

            if (addHours)
            {
                dt = dt.AddHours(8);
            }

            int y = dt.Year - 1911;
            string suffix = dt.ToString("MM月dd日");

            if (addYTitle)
            {
                if (y < 0)
                {
                    return "民國前" + System.Math.Abs(y) + "年" + suffix;

                }

                return "民國" + y.ToString() + "年" + suffix;
            }
            else
            {
                return y.ToString() + "年" + suffix;
            }
        }

        private class TDay
        {
            public string value { get; set; }
            public DateTime day { get; set; }

            public string tw_y { get; set; }
            public string tw_m { get; set; }
            public string tw_d { get; set; }

            /// <summary>
            /// 中華民國yyy年MM月dd日
            /// </summary>
            public string tw_long_day { get; set; }
            public string tw_long_day2 { get; set; }

            /// <summary>
            /// yyy年MM月dd日
            /// </summary>
            public string tw_short_day { get; set; }
            /// <summary>
            /// yyy.M.d
            /// </summary>
            public string tw_short_day2 { get; set; }

            /// <summary>
            /// dd.MMM.yyyy
            /// </summary>
            public string en_day { get; set; }

            /// <summary>
            /// yyyy/MM/dd
            /// </summary>
            public string en_day2 { get; set; }

            /// <summary>
            /// yyyy年MM月dd日
            /// </summary>
            public string entw_short_day { get; set; }
        }

        private TDay MapDay(string value, int hours = 0)
        {
            var dt = GetDateTime(value);
            return MapDay(dt.AddHours(hours));
        }

        private TDay MapDay(DateTime dt)
        {
            TDay result = new TDay
            {
                value = dt.ToString("yyyy-MM-dd hh:mm:ss"),
                day = dt,
                tw_y = "",
                tw_m = "",
                tw_d = "",
                tw_long_day = "",
                tw_long_day2 = "",
                tw_short_day = "",
                tw_short_day2 = "",
                en_day = "",
                en_day2 = "",
                entw_short_day = "",
            };

            if (result.day != DateTime.MinValue)
            {
                result.tw_y = (result.day.Year - 1911).ToString();
                result.tw_m = result.day.Month.ToString("00");
                result.tw_d = result.day.Day.ToString("00");

                result.en_day = result.day.ToString("dd.MMM.yyyy", new System.Globalization.CultureInfo("EN-US"));
                result.en_day2 = result.day.Year + "/" + result.tw_m + "/" + result.tw_d;
                result.entw_short_day = result.day.Year + "年" + result.tw_m + "月" + result.tw_d + "日";
                result.tw_long_day = "中華民國" + result.tw_y + "年" + result.tw_m + "月" + result.tw_d + "日";
                result.tw_long_day2 = "民國" + result.tw_y + "年" + result.day.Month + "月" + result.day.Day + "日";
                result.tw_short_day = result.tw_y + "年" + result.tw_m + "月" + result.tw_d + "日";
                result.tw_short_day2 = result.tw_y + "." + result.day.Month + "." + result.day.Day;
            }

            return result;
        }

        private string MeetingTwDayRange(string date_s, string date_e)
        {
            var result = "";
            var dts = MapDay(date_s);
            var dte = MapDay(date_e);
            result = "講習日期：" + dts.tw_short_day;
            if (string.IsNullOrWhiteSpace(dte.tw_short_day))
            {
                result += "至" + dte.tw_short_day;
            }
            return result;
        }
    }
}