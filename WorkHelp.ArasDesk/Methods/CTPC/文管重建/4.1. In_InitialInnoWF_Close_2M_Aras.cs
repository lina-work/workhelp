﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.CTPC.Doc
{
    public class In_InitialInnoWF_Close_2M_Aras : Item
    {
        public In_InitialInnoWF_Close_2M_Aras(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                只處理最近兩個月的Close流程,若要處理全部的流程,請執行In_InitialInnoWF_Close_All_Aras
                注意:Innosoft Workflow環境不可執行這個Method
                目的:填滿簽核物件、Activity,Workflow Process等 Item 的 Inno Workflow 屬性
            */
            
            string strMethodName = "In_InitialInnoWF_Close_2M_Aras";
            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            string strWorkflowProcessNames = "";
            Item itmR = inn.applyMethod("In_InitialInnoWF_Close_All_Aras", "<month>-2</month>");
            strWorkflowProcessNames = itmR.getResult();
            return inn.newResult(strWorkflowProcessNames);
        }
    }
}