﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.CTPC.Doc
{
	public class In_GenerateFlowChart_All : Item
	{
		public In_GenerateFlowChart_All(IServerConnection arg) : base(arg) { }

		/// <summary>
		/// 編程啟動點 (Code 在此撰寫)
		/// </summary>
		public Item Run()
		{
			Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
			Aras.Server.Core.IContextState RequestState = CCO.RequestState;

			/*
				Inno與Aras共用
				目的:將所有的Workflow Process都加上 flowchart
				用法:可重複執行,只會找 in_flowchart 為 null 的workflow process
				平均1秒鐘處理10個workflow process(i5cpu, 8GRam)
			*/

			//System.Diagnostics.Debugger.Break();
			string strMethodName = "In_GenerateFlowChart_All";

			Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
			bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

			Innovator inn = this.getInnovator();
			Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
			string aml = "";
			string sql = "";


			Item itmR = this;
			string strWFPIDs = "";
			try
			{
				aml = "<AML>";
				aml += "<Item type='Workflow Process' action='get' orderBy='state'>";
				aml += "<in_flowchart condition='is null'></in_flowchart>";
				aml += "</Item></AML>";
				CCO.Utilities.WriteDebug("In_GenerateFlowChart_All", "開始獲取未產生流程圖的Workflow Process");
				Item itmWFPs = inn.applyAML(aml);
				CCO.Utilities.WriteDebug("In_GenerateFlowChart_All", "完成獲取未產生流程圖的Workflow Process:" + itmWFPs.getItemCount() + "筆");
				for (int i = 0; i < itmWFPs.getItemCount(); i++)
				{
					Item itmWFP = itmWFPs.getItemByIndex(i);
					if (itmWFP.getProperty("in_flowchart", "") != "")
					{
						CCO.Utilities.WriteDebug("In_GenerateFlowChart_All", (i + 1).ToString() + "/" + itmWFPs.getItemCount().ToString() + ":[忽略]:" + itmWFP.getProperty("state") + "-" + itmWFP.getProperty("keyed_name"));
						continue;
					}
					itmWFP.apply("In_UpdateFlowchart");
					CCO.Utilities.WriteDebug("In_GenerateFlowChart_All", (i + 1).ToString() + "/" + itmWFPs.getItemCount().ToString() + ":[執行]:" + itmWFP.getProperty("state") + "-" + itmWFP.getProperty("keyed_name"));
					strWFPIDs += itmWFP.getID() + ",";
				}


			}
			catch (Exception ex)
			{
				if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);


				string strFullMethodInfo = "[" + this.getType() + "]." + this.getID() + ":" + strMethodName;


				string strError = ex.Message + "\n";

				if (aml != "")
					strError += "無法執行AML:" + aml + "\n";

				if (sql != "")
					strError += "無法執行SQL:" + sql + "\n";

				string strErrorDetail = "";
				strErrorDetail = strFullMethodInfo + "\n" + strError + "\n" + ex.ToString() + "\n" + ex.StackTrace.ToString();

				strError = strError.Replace("\n", "</br>");
				throw new Exception((strError));
			}
			if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

			return inn.newResult(strWFPIDs);
		}
	}
}