﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.CTPC.Doc
{
    public class In_UpdateFlowchart : Item
    {
        public In_UpdateFlowchart(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的:產生flowchart的語法
                做法:
                1.取得整個 workflow 樹
                2.取得所有的Activity
                3.取得所有 Activity 下面的Path
                P.S. 如果要修改過濾特特殊字元請到最下方的function修改陣列(Replace_Array)
            */

            //System.Diagnostics.Debugger.Break();
            string strMethodName = "In_UpdateFlowchart";
            
            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
            string aml = "";
            string sql = "";
            string str_acts = "";
            string str_paths = "";

            Item itmR = this;
            string workflowProcessId = this.getID();
            try
            {
                //flowchart	
                aml = "<AML>";
                aml += "<Item type='Workflow Process'  id='" + workflowProcessId + "' action='get'>";
                aml += "	<Relationships>";
                aml += "		<Item type='Workflow Process Activity'>";
                aml += "			<related_id>";
                aml += "			<Item type='Activity' action='get'>";
                aml += "				<Relationships>";
                aml += "					<Item type='Workflow Process Path' action='get'>";
                aml += "					</Item>";
                aml += "					<Item type='Activity Assignment' action='get'>";
                aml += "					</Item>";
                aml += "				</Relationships>";
                aml += "			</Item>";
                aml += "			</related_id>";
                aml += "		</Item>";
                aml += "	</Relationships>";
                aml += "</Item>		";
                aml += "</AML>";
                Item itmFlowChart = inn.applyAML(aml);
                if (itmFlowChart.isError())
                {
                    throw new Exception(itmFlowChart.getErrorString());
                }

                string strIgnoreActIds = "";
                string strActiveId = "";

                Item itmActivitys = itmFlowChart.getItemsByXPath("//Item[@type='Workflow Process Activity']/related_id/Item[@type='Activity']");
                for (int i = 0; i < itmActivitys.getItemCount(); i++)
                {
                    Item itmActivity = itmActivitys.getItemByIndex(i);
                    if (itmActivity.getProperty("cloned_as", "") != "")
                    {
                        strIgnoreActIds += itmActivity.getID() + ",";
                        continue;
                    }

                    if (itmActivity.getProperty("state", "") == "Active")
                    {
                        strActiveId = itmActivity.getID();
                    }

                    string strLabel = itmActivity.getProperty("label", "");
                    if (strLabel == "")
                    {
                        strLabel = itmActivity.getProperty("name", "");
                    }

                    strLabel = ReplaceChar(strLabel);
                    /*
                        strLabel = strLabel.Replace("["," ");
                        strLabel = strLabel.Replace("]"," ");
                        strLabel = strLabel.Replace("，",",");
                        strLabel = strLabel.Replace("。",".");
                        strLabel = strLabel.Replace("、",",");
                        strLabel = strLabel.Replace("「"," ");
                        strLabel = strLabel.Replace("」"," ");	
                        strLabel = strLabel.Replace("("," ");
                        strLabel = strLabel.Replace(")"," ");
                    */
                    strLabel = itmActivity.getID() + "[" + strLabel + "];";
                    if (!str_acts.Contains(strLabel))
                    {
                        str_acts += strLabel;
                    }
                }

                strIgnoreActIds = "," + strIgnoreActIds;
                Item itmPaths = itmFlowChart.getItemsByXPath("//Item[@type='Workflow Process Path']");
                for (int i = 0; i < itmPaths.getItemCount(); i++)
                {
                    Item itmPath = itmPaths.getItemByIndex(i);
                    if (strIgnoreActIds.Contains("," + itmPath.getProperty("related_id") + ","))
                    {
                        continue;
                    }

                    if (strIgnoreActIds.Contains("," + itmPath.getProperty("source_id") + ","))
                    {
                        continue;
                    }
                    
                    string strLabel = itmPath.getProperty("label", "");
                    if (strLabel == "")
                    {
                        strLabel = itmPath.getProperty("name", " ");
                    }

                    if (strLabel == "")
                    {
                        strLabel = " ";
                    }
                    
                    strLabel = ReplaceChar(strLabel);

                    //可以抓出哪個節點名稱有問題
                    //CCO.Utilities.WriteDebug("In_UpdateFlowchart",strLabel); 
                    /*
                        strLabel = strLabel.Replace("["," ");
                        strLabel = strLabel.Replace("]"," ");
                        strLabel = strLabel.Replace("，",",");
                        strLabel = strLabel.Replace("。",".");
                        strLabel = strLabel.Replace("、",",");
                        strLabel = strLabel.Replace("「"," ");
                        strLabel = strLabel.Replace("」"," ");	
                        strLabel = strLabel.Replace("("," ");
                        strLabel = strLabel.Replace(")"," ");
                    */

                    strLabel = itmPath.getProperty("source_id") + "-->|" + strLabel + "|" + itmPath.getProperty("related_id") + ";";
                    str_paths += strLabel;
                }

                //標註Active

                str_acts = " graph TD;" + str_acts + str_paths;
                str_acts += " style " + strActiveId + " fill:#f39c12";

                sql = "Update [Workflow_Process] set in_flowchart = N'" + str_acts + "' where id = '" + workflowProcessId + "'";
                inn.applySQL(sql);
            }
            catch (Exception ex)
            {
                if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);


                string strFullMethodInfo = "[" + this.getType() + "]." + this.getID() + ":" + strMethodName;


                string strError = ex.Message + "\n";

                if (aml != "")
                    strError += "無法執行AML:" + aml + "\n";

                if (sql != "")
                    strError += "無法執行SQL:" + sql + "\n";

                string strErrorDetail = "";
                strErrorDetail = strFullMethodInfo + "\n" + strError + "\n" + ex.ToString() + "\n" + ex.StackTrace.ToString();

                strError = strError.Replace("\n", "</br>");
                throw new Exception(strError);
            }
            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

            return itmR;
        }

        private string ReplaceChar(string s)
        {
            string[] @Replace_Array =
            {
        "\"　\", ",
        "\" \", ",
        "[, ",
        "！, ",
        "!, ",
        "＂, ",
        "\", ",
        "＃, ",
        "#, ",
        "＄, ",
        "$, ",
        "％, ",
        "%, ",
        "＆, ",
        "&, ",
        "＇, ",
        "\', ",
        "（, ",
        "(, ",
        "）, ",
        "), ",
        "＊, ",
        "*, ",
        "＋, ",
        "+, ",
        "，, ",
        "－, ",
        "-, ",
        "．, ",
        "., ",
        "／, ",
        "/, ",
        "０,0",
        "１,1",
        "２,2",
        "３,3",
        "４,4",
        "５,5",
        "６,6",
        "７,7",
        "８,8",
        "９,9",
        "：, ",
        ":, ",
        "；, ",
        ";, ",
        "＜, ",
        "<, ",
        "＝, ",
        "=, ",
        "＞, ",
        ">, ",
        "？, ",
        "?, ",
        "＠, ",
        "@, ",
        "Ａ,A",
        "Ｂ,B",
        "Ｃ,C",
        "Ｄ,D",
        "Ｅ,E",
        "Ｆ,F",
        "Ｇ,G",
        "Ｈ,H",
        "Ｉ,I",
        "Ｊ,J",
        "Ｋ,K",
        "Ｌ,L",
        "Ｍ,M",
        "Ｎ,N",
        "Ｏ,O",
        "Ｐ,P",
        "Ｑ,Q",
        "Ｒ,R",
        "Ｓ,S",
        "Ｔ,T",
        "Ｕ,U",
        "Ｖ,V",
        "Ｗ,W",
        "Ｘ,X",
        "Ｙ,Y",
        "Ｚ,Z",
        "［, ",
        "[, ",
        "＼, ",
        "\\, ",
        "］, ",
        "], ",
        "＾, ",
        "^, ",
        "＿, ",
        "_, ",
        "｀, ",
        "`, ",
        "ａ,a",
        "ｂ,b",
        "ｃ,c",
        "ｄ,d",
        "ｅ,e",
        "ｆ,f",
        "ｇ,g",
        "ｈ,h",
        "ｉ,i",
        "ｊ,j",
        "ｋ,k",
        "ｌ,l",
        "ｍ,m",
        "ｎ,n",
        "ｏ,o",
        "ｐ,p",
        "ｑ,q",
        "ｒ,r",
        "ｓ,s",
        "ｔ,t",
        "ｕ,u",
        "ｖ,v",
        "ｗ,w",
        "ｘ,x",
        "ｙ,y",
        "ｚ,z",
        "｛, ",
        "{, ",
        "｜, ",
        "|, ",
        "｝, ",
        "}, ",
        "～, ",
        "~, ",

    };

            for (int i = 0; i < Replace_Array.Length; i++)
            {
                string[] str = Replace_Array[i].Split(',');
                s = s.Replace(str[0], str[1]);

            }
            s = s.Replace(",", "");
            return s;





        }
    }
}