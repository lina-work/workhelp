﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.CTPC.Common
{
    public class In_Cla_Meeting_CertificateOLD : Item
    {
        public In_Cla_Meeting_CertificateOLD(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的:審核證書 
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]" + "In_Cla_Meeting_Certificate";

            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            string aml = "";
            string sql = "";
            string strError = "";

            Item itmR = this;
            string itemid = this.getProperty("itemid", "");
            string meetingid = this.getProperty("meeting_id", "");
            string id = this.getProperty("id", "");
            string type = this.getProperty("type", "");

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                strMethodName = strMethodName,
                inn = inn,
                meeting_id = meetingid,
                type = type,
                id = id
            };

            if (type == "seminar")
            {
                aml = "<AML>" +
                            "<Item type='in_cla_meeting' action='get' id='" + cfg.meeting_id + "' select='in_title, in_url, in_register_url, in_need_receipt, in_meeting_type, in_echelon, in_annual, in_certificate_no, in_date_s, in_date_e, in_coursh_hours, in_address'>" +
                            "</Item></AML>";
                cfg.itmMeeting = inn.applyAML(aml);

                cfg.in_title = cfg.itmMeeting.getProperty("in_title", "");
                cfg.in_url = cfg.itmMeeting.getProperty("in_url", "");
                cfg.in_register_url = cfg.itmMeeting.getProperty("in_register_url", "");
                cfg.in_meeting_type = cfg.itmMeeting.getProperty("in_meeting_type", "");
                cfg.in_echelon = cfg.itmMeeting.getProperty("in_echelon", "");
                cfg.in_annual = cfg.itmMeeting.getProperty("in_annual", "");
                cfg.in_date_s = cfg.itmMeeting.getProperty("in_date_s", "");
                cfg.in_date_e = cfg.itmMeeting.getProperty("in_date_e", "");
                cfg.in_coursh_hours = cfg.itmMeeting.getProperty("in_coursh_hours", "");
                cfg.in_address = cfg.itmMeeting.getProperty("in_address", "");
                cfg.certificate_no = cfg.itmMeeting.getProperty("in_certificate_no", "");
                cfg.range = Convert.ToInt32(this.getProperty("range", "0"));

                exportCertificate(cfg, itmR);
            }
            else if (type == "xls")
            {
                cfg.itmUsersView = GetUsers(cfg);
                Export(cfg, itmR);
            }
            else
            {

                itmR.setType("In_Cla_Meeting");

                sql = "SELECT * FROM IN_CLA_MEETING WITH(NOLOCK) WHERE id = '{#meeting_id}'";
                sql = sql.Replace("{#meeting_id}", itemid);

                itmR = inn.applySQL(sql);
                sql = @"
                    SELECT 
	                    t1.id
	                    ,　t1.source_id
	                    , t1.in_valid
	                    , t1.in_valid_type
	                    , t1.in_stat_0
	                    , t1.in_certificate_no
	                    , CONVERT(VARCHAR, DATEADD(hour, 8, t1.in_certificate_date), 111) AS 'in_certificate_date'
	                    , t1.in_moe_no
	                    , t1.in_year_note
	                    , t1.in_note
	                    , t2.in_name_num
	                    , t2.in_name
                    FROM 
	                    In_Cla_Meeting_Resume t1 WITH(NOLOCK)
                    INNER JOIN 
	                    IN_CLA_MEETING_USER t2 WITH(NOLOCK) 
	                    ON t2.id = t1.in_user
                    LEFT OUTER JOIN 
	                    IN_CLA_MEETING_TECHNICAL t3 WITH(NOLOCK)　
                        ON t3.in_user = t1.in_user
                    WHERE 
	                    t1.source_id = '{#meeting_id}'
                    ORDER BY 
	                    ISNULL(t2.in_name_num, -1)
                ";

                sql = sql.Replace("{#meeting_id}", itemid);

                Item itmTecs = inn.applySQL(sql);

                int count = itmTecs.getItemCount();

                //取得審和類型 Item
                Item itmIn_valid_types = GetIn_valid_types(cfg.CCO, cfg.strMethodName, cfg.inn);
                for (int i = 0; i < count; i++)
                {
                    Item itmTec = itmTecs.getItemByIndex(i);
                    itmTec.setType("In_Cla_Meeting_Resume");

                    itmTec.setProperty("in_certificate_date", itmTec.getProperty("in_certificate_date", "").Replace("/", "-"));

                    //取得審和類型 SELEC HTML
                    itmTec.setProperty("inn_valid_typeHtml", GetInn_valid_typeHtml(cfg.CCO, cfg.strMethodName, itmTec, itmIn_valid_types));

                    itmR.addRelationship(itmTec);
                }

            }

            return itmR;

        }

        /// <summary>
        /// 取得 審核型態html
        /// </summary>
        /// <param name="itmRecord"></param>
        /// <param name="itmIn_valid_types"></param>
        /// <returns></returns>
        private string GetInn_valid_typeHtml(Aras.Server.Core.CallContext CCO, string strMethodName, Item itmRecord, Item itmIn_valid_types)
        {
            string rResult = "";
            string rIn_name_num = itmRecord.getProperty("in_name_num", "").ToString().Trim();
            string rIn_valid_type = itmRecord.getProperty("in_valid_type", "").ToString().Trim();
            string rID = itmRecord.getProperty("id", "").ToString().Trim();

            if (itmIn_valid_types.isError() || itmIn_valid_types.getItemCount() <= 0)
            {
                return rResult;
            }

            // CCO.Utilities.WriteDebug(strMethodName, itmIn_valid_types.dom.InnerXml);

            StringBuilder sb = new StringBuilder();
            // sb.AppendFormat("<select id='{0}_in_valid_type' class='form-control' >", rIn_name_num);
            sb.AppendFormat("<select class='in_inputdisplay form-control {0}_in_valid_type sel_cert' onclick='ih.handleTextEdit(this,40)' onchange='ih.updateTextContent(this)' value='{1}' id='{2}_in_valid_type'>", rIn_name_num, rIn_valid_type, rID);
            sb.AppendFormat("<option value=''></option>");
            int count = itmIn_valid_types.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = itmIn_valid_types.getItemByIndex(i);
                string inn_label = item.getProperty("label", "").ToString().Trim();
                string inn_value = item.getProperty("value", "").ToString().Trim();
                if (rIn_valid_type.Equals(inn_value))
                {
                    sb.AppendFormat("<option value='{1}' selected>{0}</option>", inn_label, inn_value);
                }
                else
                {
                    sb.AppendFormat("<option value='{1}'>{0}</option>", inn_label, inn_value);
                }
                // CCO.Utilities.WriteDebug(strMethodName, "rIn_name_num:" + rIn_name_num);
                // CCO.Utilities.WriteDebug(strMethodName, "rIn_valid_type:" + rIn_valid_type);
            }
            sb.AppendFormat("</selet>");
            rResult = sb.ToString();

            return rResult;
        }

        private void Export(TConfig cfg, Item itmReturn)
        {
            ClosedXML.Excel.XLWorkbook workbook = new ClosedXML.Excel.XLWorkbook();

            string type_label = "匯入格式檔";

            AppendSheet(cfg, workbook, "current");

            Item itmPath = GetExcelPath(cfg, "export_path");

            string export_path = itmPath.getProperty("export_path", "").TrimEnd('\\');
            string ext_name = ".xlsx";
            string xls_name = type_label + DateTime.Now.ToString("MMdd_HHmmss");
            string xls_file = export_path + "\\" + xls_name + ext_name;
            string xls_url = xls_name + ext_name;

            workbook.SaveAs(xls_file);
            itmReturn.setProperty("xls_name", xls_url);
        }

        //附加個人會員 Sheet
        private void AppendSheet(TConfig cfg, ClosedXML.Excel.XLWorkbook workbook, string sheet_name)
        {
            ClosedXML.Excel.IXLWorksheet sheet = workbook.Worksheets.Add(sheet_name);


            List<TField> fields = new List<TField>();
            fields.Add(new TField { property = "in_name_num", title = "學員編號", format = "text" });
            fields.Add(new TField { property = "in_name_s", title = "人名", format = "center" });
            fields.Add(new TField { property = "in_valid", title = "是否結業(1符合/0不符合)", format = "text" });
            // fields.Add(new TField { property = "in_valid_type", title = "審核類型", format = "text" });
            // fields.Add(new TField { property = "in_certificate_no", title = "證書編號", format = "text" });
            fields.Add(new TField { property = "in_certificate_date", title = "發證日期(格式2021/01/01)", format = "yyyy/MM/dd" });
            // fields.Add(new TField { property = "in_moe_no", title = "體總編號", format = "text" });
            fields.Add(new TField { property = "in_year_note", title = "本年度講習紀錄", format = "text" });
            fields.Add(new TField { property = "in_note", title = "備註說明", format = "text" });
            fields.Add(new TField { property = "in_stat_0", title = "是否合格(O通過/X不通過)", format = "text" });
            int wsRow = 1;
            int wsCol = 1;
            int field_count = fields.Count;

            for (int i = 0; i < field_count; i++)
            {
                TField field = fields[i];
                ClosedXML.Excel.IXLCell cell = sheet.Cell(wsRow, wsCol + i);
                SetHeadCell(cell, field.title);
            }
            wsRow++;

            int count = cfg.itmUsersView.getItemCount();

            for (int i = 0; i < count; i++)
            {

                Item item = cfg.itmUsersView.getItemByIndex(i);


                item.setProperty("no", (i + 1).ToString());

                SetItemCell(sheet, wsRow, wsCol, item, fields);

                wsRow++;
            }

            //自動調整欄寬
            for (int i = 0; i < field_count; i++)
            {
                sheet.Column(wsCol + i).AdjustToContents();
            }
        }
        ClosedXML.Excel.XLColor head_bg_color = ClosedXML.Excel.XLColor.FromHtml("#295C90");
        //設定標題列
        private void SetHeadCell(ClosedXML.Excel.IXLCell cell, string title)
        {
            cell.Value = title;

            cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
            cell.Style.Font.Bold = true;
            cell.DataType = ClosedXML.Excel.XLDataType.Text;
            cell.Style.Fill.BackgroundColor = head_bg_color;
            cell.Style.Font.FontColor = ClosedXML.Excel.XLColor.White;

            cell.Style.Border.SetRightBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetTopBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetBottomBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetLeftBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
        }

        //設定物件與資料列
        private void SetItemCell(ClosedXML.Excel.IXLWorksheet sheet, int wsRow, int wsCol, Item item, List<TField> fields)
        {
            int count = fields.Count;
            for (int i = 0; i < count; i++)
            {
                TField field = fields[i];
                if (field.format == null) field.format = "";

                ClosedXML.Excel.IXLCell cell = sheet.Cell(wsRow, wsCol + i);

                string value = "";

                if (field.property != "")
                {
                    value = item.getProperty(field.property, "");
                }

                SetBodyCell(cell, value, field.format);
            }
        }

        //設定資料列
        private void SetBodyCell(ClosedXML.Excel.IXLCell cell, string value, string format)
        {
            switch (format)
            {
                case "center":
                    cell.Value = value;
                    cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    break;

                case "date":
                    cell.Value = value;
                    cell.Style.NumberFormat.Format = "yyyy/mm/dd";
                    cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    break;

                case "yyyy/MM/dd HH:mm":
                    cell.Value = GetDateTimeValue(value, format, 8);
                    cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    break;

                case "yyyy/MM/dd":
                    cell.Value = GetDateTimeValue(value, format, 8);
                    cell.Style.NumberFormat.Format = "yyyy/mm/dd";
                    cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    break;

                case "$ #,##0":
                    cell.Value = value;
                    cell.Style.NumberFormat.Format = "$ #,##0";
                    break;

                case "tel":
                    if (value.StartsWith("09"))
                    {
                        cell.Value = value.Replace("-", "");
                        cell.Style.NumberFormat.Format = "0000-000-000";
                        cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    }
                    else
                    {
                        cell.Value = "'" + value;
                        cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    }
                    break;

                case "text":
                    cell.Value = "'" + value;
                    cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    break;

                default:
                    cell.Value = value;
                    break;
            }
            cell.Style.Border.SetRightBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetTopBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetBottomBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetLeftBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
        }

        private string GetDateTimeValue(string value, string format, int hours = 0)
        {
            if (value == "") return "";

            DateTime dt = DateTime.Now;
            if (DateTime.TryParse(value, out dt))
            {
                return dt.AddHours(hours).ToString(format);
            }
            else
            {
                return value;
            }
        }


        private Item GetExcelPath(TConfig cfg, string in_name)
        {
            Item itmResult = cfg.inn.newItem();

            //Excel 匯出路徑 (from 原創.設定參數)
            string sql = @"
                SELECT 
                    TOP 1 t2.in_name AS 'variable', t1.in_name, t1.in_value 
                FROM 
                    In_Variable_Detail t1 WITH(NOLOCK)
                INNER JOIN 
                    In_Variable t2 WITH(NOLOCK) ON t2.ID = t1.SOURCE_ID 
                WHERE 
                    t2.in_name = N'meeting_excel'  
                    AND t1.in_name = N'{#in_name}'
            ";

            sql = sql.Replace("{#in_name}", in_name);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item item = cfg.inn.applySQL(sql);

            if (item.isError())
            {
                throw new Exception("未設定匯出路徑 _# " + in_name);
            }

            itmResult.setProperty("export_path", item.getProperty("in_value", ""));

            return itmResult;
        }

        //匯出研習證明
        private void exportCertificate(TConfig cfg, Item itmReturn)
        {
            Item itmUsers = GetUsers(cfg);
            if (itmUsers.isError())
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "匯出研習證明資料發生錯誤");
                throw new Exception("匯出研習證明發生錯誤");
            }
            if (itmUsers.getItemCount() < 1)
            {
                throw new Exception("目前暫無合格學員供下載!");
            }

            Item itmXls = cfg.inn.applyMethod("In_Variable_File", "<in_name>certificate_path2</in_name>");
            string export_path = itmXls.getProperty("export_path", "");
            string template_path = itmXls.getProperty("template_path", "");

            string pdfName = cfg.in_title + "_研習證明";
            string pdfFile = export_path + pdfName + ".pdf";

            Spire.Xls.Workbook book = new Spire.Xls.Workbook();
            book.LoadFromFile(template_path);
            Spire.Xls.Worksheet sheetTemplate = book.Worksheets[0];

            //研習證明
            AppendCertificate(book, sheetTemplate, cfg, itmUsers);

            //sheetTemplate.Remove();

            book.SaveToFile(pdfFile, Spire.Xls.FileFormat.PDF);

            itmReturn.setProperty("pdf_name", pdfName + ".pdf");
        }

        private void AppendCertificate(Spire.Xls.Workbook book, Spire.Xls.Worksheet sheetTemplate, TConfig cfg, Item itmUsers)
        {
            int user_count = itmUsers.getItemCount();
            var NowInfo = MapDay(System.DateTime.Now);
            var MtDaySInfo = MapDay(cfg.in_date_s);

            for (int i = 0; i < user_count; i++)
            {
                Item itmUser = itmUsers.getItemByIndex(i);
                string in_name = itmUser.getProperty("in_name", "");
                string in_name_num = itmUser.getProperty("in_name_num", "").PadLeft(4, '0');
                string in_hours = itmUser.getProperty("in_hours", "0");

                if (in_name == "")
                {
                    in_name = "　　　";
                }

                string doc_num = "CTPC({#annual})研習字第 {#num} 號";
                doc_num = doc_num.Replace("{#annual}", cfg.in_annual)
                    .Replace("{#num}", in_name_num);

                string contents = "茲證明 {#name} 先生/小姐，於{#tw_day}，參與本總會辦理之「{#mt_title}」，研習時數共計{#hours}小時。";
                contents = contents.Replace("{#name}", in_name)
                    .Replace("{#tw_day}", MtDaySInfo.tw_long_day2)
                    .Replace("{#mt_title}", cfg.in_title)
                    .Replace("{#hours}", in_hours);

                Spire.Xls.Worksheet sheet = book.Worksheets[0];
                //sheet.CopyFrom(sheetTemplate);

                sheet.Name = in_name_num;

                sheet.Range["A1"].Text = doc_num;
                //sheet.Range["B1"].Text = contents;
                // sheet.Range["M4"].Text = NowInfo.tw_y;
                // sheet.Range["M5"].Text = NowInfo.day.Month.ToString();
                // sheet.Range["M6"].Text = NowInfo.day.Day.ToString();

                sheet.PageSetup.FitToPagesWide = 1;
                sheet.PageSetup.FitToPagesTall = 1;
                sheet.PageSetup.TopMargin = 0.1;
                sheet.PageSetup.LeftMargin = 0.1;
                sheet.PageSetup.RightMargin = 0.1;
                sheet.PageSetup.BottomMargin = 0.1;

                //適合頁面
                book.ConverterSetting.SheetFitToPage = true;
            }
        }

        private List<string> SplitContents(string value)
        {
            var result = new List<string>();
            var lines = 5;
            var codes = 24;
            var len = value.Length;
            for (int i = 0; i < lines; i++)
            {
                int idxs = i * codes;
                int idxe = (i + 1) * codes - 1;
                if (ids > len)
                {
                    result.Add("");
                }
                else
                {

                }
            }
            return result;
        }
        //取得測驗人員清單
        private Item GetUsers(TConfig cfg)
        {
            string sqlFilter = "";
            string rangeStr = "";

            if (cfg.type == "seminar" && cfg.id != "")
            {
                sqlFilter = " AND t1.id IN (" + cfg.id + ")";
            }
            else if (cfg.type == "xls")
            {
                sqlFilter = "AND ISNULL(t2.in_name_num, '') <> '' ";
            }
            else
            {
                rangeStr = " WHERE rowno > {#page_s} and rowno <= {#page_e}";
            }

            string sql = @"
                SELECT * FROM 
                (
                    SELECT 
						t1.id
						, t1.in_name_s
						, t1.in_valid
						, t1.in_valid_type
						, t1.in_stat_0
						, t1.in_certificate_no
						, t1.in_certificate_date
						, t1.in_moe_no
						, t1.in_year_note
						, t1.in_note
						, t1.in_hours
						, t2.in_name_num
						, t2.in_name
                    FROM 
						IN_CLA_MEETING_RESUME t1 WITH(NOLOCK)　
                    INNER JOIN
						IN_CLA_MEETING_USER t2 WITH(NOLOCK)
						ON t2.id = t1.in_user
                    WHERE 
						t1.source_id = '{#meeting_id}' 
						{#sqlFilter}
                ) AS LIST
                " + rangeStr;

            sql += " ORDER BY in_name_num";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                        .Replace("{#sqlFilter}", sqlFilter)
                        .Replace("{#page_s}", ((cfg.range - 1) * 80).ToString())
                        .Replace("{#page_e}", (cfg.range * 80).ToString());

            return cfg.inn.applySQL(sql);
        }

        private string convertCDay(DateTime CDay, string type)
        {
            string y = (CDay.Year - 1911).ToString();
            string m = CDay.Month.ToString("00");
            string d = CDay.Day.ToString("00");
            string returnStr = "";
            if (type == "footer")
            {
                returnStr = " 中華民國　" + y + "　年　" + m + "　月　" + d + "　日";
            }
            else
            {
                returnStr = y + "年" + m + "月" + d + "日";
            }

            return returnStr;
        }

        //下拉選單
        private void AppendMenu(Item items, string type_name, Item itmReturn)
        {
            if (items.isError() || items.getItemCount() <= 0)
            {
                return;
            }

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);

                item.setType(type_name);
                item.setProperty("inn_label", item.getProperty("label", ""));
                item.setProperty("inn_value", item.getProperty("value", ""));
                itmReturn.addRelationship(item);
            }
        }

        private Item GetIn_valid_types(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn)
        {
            string sql = @"
        SELECT 
            Val.LABEL_ZT as 'label',
            Val.[VALUE] as 'value'
        FROM list Lis WITH(NOLOCK)
        LEFT JOIN [VALUE] Val WITH(NOLOCK)
            ON Lis.ID = Val.SOURCE_ID
        WHERE Lis.[NAME] = 'in_valid_type'
        ORDER BY Val.SORT_ORDER ASC
    ";

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return inn.applySQL(sql);
        }

        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public string strMethodName { get; set; }
            public Innovator inn { get; set; }
            //外部輸入區

            /// <summary>
            /// 活動 id
            /// </summary>
            public string meeting_id { get; set; }

            // Meeting 區

            /// <summary>
            /// Meeting 資料
            /// </summary>
            public Item itmMeeting { get; set; }

            /// <summary>
            /// 活動名稱
            /// </summary>
            public string in_title { get; set; }

            /// <summary>
            /// 專屬展示網址
            /// </summary>
            public string in_url { get; set; }

            /// <summary>
            /// 專屬報名網址
            /// </summary>
            public string in_register_url { get; set; }

            /// <summary>
            /// 活動類型
            /// </summary>
            public string in_meeting_type { get; set; }

            /// <summary>
            /// 梯次
            /// </summary>
            public string in_echelon { get; set; }

            /// <summary>
            /// 年度
            /// </summary>
            public string in_annual { get; set; }

            /// <summary>
            /// 文號
            /// </summary>
            public string in_decree { get; set; }

            /// <summary>
            /// 證照號碼
            /// </summary>
            public string certificate_no { get; set; }

            /// <summary>
            /// 課程起
            /// </summary>
            public string in_date_s { get; set; }

            /// <summary>
            /// 課程迄
            /// </summary>
            public string in_date_e { get; set; }

            /// <summary>
            /// 修課時數
            /// </summary>
            public string in_coursh_hours { get; set; }

            /// <summary>
            /// 地址
            /// </summary>
            public string in_address { get; set; }
            public string type { get; set; }
            public string id { get; set; }

            public int range { get; set; }

            public Item itmUsersView { get; set; }

        }

        private class TField
        {
            public string property { get; set; }
            public string title { get; set; }
            public string css { get; set; }
            public string format { get; set; }
            public bool hide { get; set; }
            public int in_west_year { get; set; }
            public Func<TResume, TField, string> getValue { get; set; }

            public string GetStr(Item item)
            {
                return item.getProperty(this.property, "");
            }
        }

        private class TResume
        {
            public string id { get; set; }
            public string in_sno { get; set; }
            public string in_apply_year { get; set; }
            public int apply_year { get; set; }
            public string tw_apply_year { get; set; }
            public Item Value { get; set; }
        }

        private class TDay
        {
            public string value { get; set; }
            public DateTime day { get; set; }

            public string tw_y { get; set; }
            public string tw_m { get; set; }
            public string tw_d { get; set; }

            /// <summary>
            /// 中華民國yyy年MM月dd日
            /// </summary>
            public string tw_long_day { get; set; }
            public string tw_long_day2 { get; set; }

            /// <summary>
            /// yyy年MM月dd日
            /// </summary>
            public string tw_short_day { get; set; }
            /// <summary>
            /// yyy.M.d
            /// </summary>
            public string tw_short_day2 { get; set; }

            /// <summary>
            /// dd.MMM.yyyy
            /// </summary>
            public string en_day { get; set; }

            /// <summary>
            /// yyyy/MM/dd
            /// </summary>
            public string en_day2 { get; set; }

            /// <summary>
            /// yyyy年MM月dd日
            /// </summary>
            public string entw_short_day { get; set; }
        }

        private TDay MapDay(string value, int hours = 0)
        {
            var dt = GetDateTime(value, hours);
            return MapDay(dt);
        }

        private TDay MapDay(DateTime dt)
        {
            TDay result = new TDay
            {
                value = dt.ToString("yyyy-MM-dd hh:mm:ss"),
                day = dt,
                tw_y = "",
                tw_m = "",
                tw_d = "",
                tw_long_day = "",
                tw_long_day2 = "",
                tw_short_day = "",
                tw_short_day2 = "",
                en_day = "",
                en_day2 = "",
                entw_short_day = "",
            };

            if (result.day != DateTime.MinValue)
            {
                result.tw_y = (result.day.Year - 1911).ToString();
                result.tw_m = result.day.Month.ToString("00");
                result.tw_d = result.day.Day.ToString("00");

                result.en_day = result.day.ToString("dd.MMM.yyyy", new System.Globalization.CultureInfo("EN-US"));
                result.en_day2 = result.day.Year + "/" + result.tw_m + "/" + result.tw_d;
                result.entw_short_day = result.day.Year + "年" + result.tw_m + "月" + result.tw_d + "日";
                result.tw_long_day = "中華民國" + result.tw_y + "年" + result.tw_m + "月" + result.tw_d + "日";
                result.tw_long_day2 = "民國" + result.tw_y + "年" + result.day.Month + "月" + result.day.Day + "日";
                result.tw_short_day = result.tw_y + "年" + result.tw_m + "月" + result.tw_d + "日";
                result.tw_short_day2 = result.tw_y + "." + result.day.Month + "." + result.day.Day;
            }

            return result;
        }

        private string TermRannge(TDay s, TDay e)
        {
            string result = "";
            if (!string.IsNullOrEmpty(s.tw_long_day2))
            {
                result += s.tw_long_day2;
            }
            if (!string.IsNullOrEmpty(e.tw_long_day2))
            {
                result += "至" + e.tw_long_day2.Replace("民國", "");
            }
            if (string.IsNullOrEmpty(result))
            {
                result = "未設定任期起迄";
            }
            return result;
        }

        private string TermRanngeShort(TDay s, TDay e)
        {
            string result = "";
            if (!string.IsNullOrEmpty(s.tw_short_day2))
            {
                result += s.tw_short_day2;
            }
            if (!string.IsNullOrEmpty(e.tw_short_day2))
            {
                result += "~" + e.tw_short_day2;
            }
            if (string.IsNullOrEmpty(result))
            {
                result = "未設定任期起迄";
            }
            return result;
        }

        private DateTime GetDateTime(string value, int hours)
        {
            if (value == "") return DateTime.MinValue;
            DateTime dt = DateTime.MinValue;
            DateTime.TryParse(value, out dt);
            return dt.AddHours(hours);
        }

        private int GetInt(string value, int defV = 0)
        {
            if (value == "") return 0;

            int result = defV;
            if (int.TryParse(value, out result))
            {
                return result;
            }
            return defV;
        }
    }
}