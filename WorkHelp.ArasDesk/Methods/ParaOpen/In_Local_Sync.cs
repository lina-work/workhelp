﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.ParaOpen.Common
{
    public class In_Local_Sync : Item
    {
        public In_Local_Sync(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 現場系統整合(成績同步)
                輸入: meeting_id
                日期: 
                    2023-06-09: 調整為帕拉桌球版 (lina)
             */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]In_Local_Sync";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "input dom: " + itmR.dom.InnerXml);

            CCO.Utilities.WriteDebug(strMethodName, "in");
            return itmR;

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
            };

            //新同步資料
            Item itmSchedules = GetLocalSchedules(cfg);
            RefreshScore(cfg, itmSchedules);

            ////成績修正
            //Item itmChanges = GetChangeScores(cfg);
            //RefreshScore(cfg, itmChanges);

            //調整為成績更新完成後再刷新日誌
            CCO.Utilities.WriteDebug(strMethodName, "out");

            return itmR;
        }

        private void RefreshScore(TConfig cfg, Item itmSchedules)
        {
            if (itmSchedules.isError() || itmSchedules.getResult() == "" || itmSchedules.getItemCount() <= 0)
            {
                return;
            }

            int count = itmSchedules.getItemCount();
            for (int i = 0; i < count; i++)
            {
                var itmSchedule = itmSchedules.getItemByIndex(i);
                var row = MapRow(cfg, itmSchedule);

                if (row.MId == "")
                {
                    //更新現場 Schedule
                    UpdateSchedule(cfg, row, "2");
                    continue;
                }

                var itmDetails = GetEventDetailById(cfg, row);
                if (itmDetails.isError() || itmDetails.getResult() == "" || itmDetails.getItemCount() <= 0)
                {
                    UpdateSchedule(cfg, row, "3");
                    continue;
                }

                var evt = MapEvent(cfg, itmDetails);
                var is_f1_win = row.Win == "1";
                var is_f2_win = row.Win == "2";

                //遞迴勝出
                if (is_f1_win)
                {
                    ApplySyncNew(cfg, row, evt.Value, evt.Foot1, evt.Foot2);

                    //更新現場 Schedule
                    UpdateSchedule(cfg, row, "1");
                }

                if (is_f2_win)
                {
                    ApplySyncNew(cfg, row, evt.Value, evt.Foot2, evt.Foot1);

                    //更新現場 Schedule
                    UpdateSchedule(cfg, row, "1");
                }
            }
        }

        /// <summary>
        /// 更新現場 Schedule
        /// </summary>
        private void UpdateSchedule(TConfig cfg, TRow row, string sync_flag)
        {
            var keys = new List<string>();
            keys.Add(row.item.getProperty("foot1w", ""));
            keys.Add(row.item.getProperty("foot2w", ""));
            keys.Add(row.item.getProperty("win", ""));

            string upd_result = string.Join("-", keys);

            string sql = "UPDATE In_Local_Schedule SET"
                + "  SyncFlag = '" + sync_flag + "'"
                + ", UpdResult = '" + upd_result + "'"
                + " WHERE enumber = '" + row.ENumber + "'";

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            cfg.inn.applySQL(sql);
        }

        private void ApplySyncNew(TConfig cfg, TRow row, Item itmEvent, Item itmWinner, Item itmLoser)
        {
            Item itmData = cfg.inn.newItem();
            itmData.setType("In_Meeting_PEvent");
            itmData.setProperty("meeting_id", row.MId);
            itmData.setProperty("program_id", itmEvent.getProperty("program_id", ""));
            itmData.setProperty("event_id", itmEvent.getProperty("event_id", ""));
            itmData.setProperty("detail_id", itmWinner.getProperty("detail_id", ""));
            itmData.setProperty("target_detail_id", itmLoser.getProperty("detail_id", ""));
            itmData.setProperty("points_type", "HWIN");
            itmData.setProperty("mode", "sync");

            itmData.apply("in_meeting_program_score_new");
        }

        private void ApplySyncOld(TConfig cfg, TRow row, Item itmEvent, Item itmWinner, Item itmLoser)
        {
            Item itmData = cfg.inn.newItem();
            itmData.setType("In_Meeting_PEvent");
            itmData.setProperty("meeting_id", row.MId);
            itmData.setProperty("program_id", itmEvent.getProperty("program_id", ""));
            itmData.setProperty("event_id", itmEvent.getProperty("event_id", ""));
            itmData.setProperty("detail_id", itmWinner.getProperty("detail_id", ""));
            itmData.setProperty("target_detail_id", itmLoser.getProperty("detail_id", ""));
            itmData.setProperty("mode", "sync");

            itmData.apply("in_meeting_program_score");
        }

        private TRow MapRow(TConfig cfg, Item itmSchedule)
        {
            TRow result = new TRow
            {
                No = itmSchedule.getProperty("no", ""),
                Site = itmSchedule.getProperty("site", ""),
                ERound = itmSchedule.getProperty("eround", ""),
                EGame = itmSchedule.getProperty("egame", ""),
                Weight = itmSchedule.getProperty("weight", ""),
                Win = itmSchedule.getProperty("win", ""),
                MNumber = itmSchedule.getProperty("mnumber", ""),
                MDay = itmSchedule.getProperty("mday", ""),
                ENumber = itmSchedule.getProperty("enumber", ""),
                MId = itmSchedule.getProperty("mid", ""),
                EventId = itmSchedule.getProperty("eventid", ""),
                item = itmSchedule,
            };

            return result;
        }

        private TEvent MapEvent(TConfig cfg, Item items)
        {
            var itmEvent = items.getItemByIndex(0);

            var evt = new TEvent
            {
                Id = itmEvent.getProperty("event_id", ""),
                Value = itmEvent,
            };

            var count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_sign_foot = item.getProperty("in_sign_foot", "");
                if (in_sign_foot == "1")
                {
                    evt.Foot1 = item;
                }
                else
                {
                    evt.Foot2 = item;
                }
            }

            if (evt.Foot1 == null) evt.Foot1 = cfg.inn.newItem();
            if (evt.Foot2 == null) evt.Foot2 = cfg.inn.newItem();

            return evt;
        }

        private Item GetLocalSchedules(TConfig cfg)
        {
            string sql = @"
                SELECT 
	                t1.*
	                , t2.id AS 'MId'
                FROM 
	                In_Local_Schedule t1 WITH(NOLOCK) 
                INNER JOIN 
	                IN_MEETING t2 WITH(NOLOCK) 
	                ON t2.id = t1.MeetingId 
	            LEFT OUTER JOIN
	                IN_MEETING_PEVENT t3 WITH(NOLOCK)
	                ON t3.id = t1.EventId
                WHERE
                    t1.win <> ISNULL(t3.in_win_local, '')
                    AND ISNULL(t1.SyncFlag, '') = ''
            ";

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }


        private Item GetChangeScores(TConfig cfg)
        {
            string sql = @"
                SELECT 
	                t1.*
	                , t2.id AS 'MId'
                FROM 
	                In_Local_Schedule t1 WITH(NOLOCK) 
                INNER JOIN 
	                IN_MEETING t2 WITH(NOLOCK) 
	                ON t2.item_number = t1.MNumber 
				WHERE
                    ISNULL(t1.SyncFlag, '') = '1'
					AND (
                        ISNULL(t1.WhiteI, '') 
                        + '-' + ISNULL(t1.WhiteW, '') 
                        + '-' + ISNULL(t1.WhiteS, '') 
                        + '-' + ISNULL(t1.BlueI, '') 
                        + '-' + ISNULL(t1.BlueW, '') 
                        + '-' + ISNULL(t1.BlueS, '') 
                        + '-' + ISNULL(t1.Win, '')
                        <> 
                        ISNULL(t1.UpdResult, '')
                    )
            ";

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        private Item GetEventDetailById(TConfig cfg, TRow row)
        {
            string sql = @"
                SELECT 
	                t1.source_id    AS 'program_id'
	                , t1.id         AS 'event_id'
	                , t2.id         AS 'detail_id'
	                , t2.in_sign_foot
	                , t2.in_sign_no
                FROM 
	                IN_MEETING_PEVENT t1 WITH(NOLOCK) 
                INNER JOIN 
	                IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                WHERE
	                t1.id = '{#event_id}'
                ORDER BY
	                t2.in_sign_foot
            ";

            sql = sql.Replace("{#event_id}", row.EventId);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        private Item GetEventDetailByItemNumber(TConfig cfg, TRow row)
        {
            string sql = @"
                SELECT 
	                t1.source_id    AS 'program_id'
	                , t1.id         AS 'event_id'
	                , t2.id         AS 'detail_id'
	                , t2.in_sign_foot
	                , t2.in_sign_no
                FROM 
	                IN_MEETING_PEVENT t1 WITH(NOLOCK) 
                INNER JOIN 
	                IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                WHERE
	                t1.item_number = '{#item_number}'
                ORDER BY
	                t2.in_sign_foot
            ";

            sql = sql.Replace("{#item_number}", row.ENumber);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }

            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
        }

        private class TEvent
        {
            /// <summary>
            /// 編號
            /// </summary>
            public string Id { get; set; }

            /// <summary>
            /// 場次資料
            /// </summary>
            public Item Value { get; set; }

            /// <summary>
            /// 白方
            /// </summary>
            public Item Foot1 { get; set; }

            /// <summary>
            /// 藍方
            /// </summary>
            public Item Foot2 { get; set; }
        }

        private class TRow
        {
            /// <summary>
            /// 編號
            /// </summary>
            public string No { get; set; }

            /// <summary>
            /// 場地
            /// </summary>
            public string Site { get; set; }

            /// <summary>
            /// 場次
            /// </summary>
            public string ERound { get; set; }

            /// <summary>
            /// 局次
            /// </summary>
            public string EGame { get; set; }
            
            /// <summary>
            /// 級別
            /// </summary>
            public string Weight { get; set; }

            /// <summary>
            /// 白單位
            /// </summary>
            public string Foot1D { get; set; }

            /// <summary>
            /// 白姓名
            /// </summary>
            public string Foot1N { get; set; }

            /// <summary>
            /// 藍單位
            /// </summary>
            public string Foot2D { get; set; }

            /// <summary>
            /// 藍姓名
            /// </summary>
            public string Foot2N { get; set; }

            /// <summary>
            /// 勝負
            /// </summary>
            public string Win { get; set; }

            /// <summary>
            /// 賽事編號
            /// </summary>
            public string MNumber { get; set; }

            /// <summary>
            /// 賽事日期
            /// </summary>
            public string MDay { get; set; }

            /// <summary>
            /// 場次編號
            /// </summary>
            public string ENumber { get; set; }

            /// <summary>
            /// 賽事 Id
            /// </summary>
            public string MId { get; set; }

            /// <summary>
            /// Event Id
            /// </summary>
            public string EventId { get; set; }
            
            /// <summary>
            /// 資料
            /// </summary>
            public Item item { get; set; }
        }
    }
}