﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.ParaOpen.Common
{
    public class In_Meeting_PEvent_Time : Item
    {
        public In_Meeting_PEvent_Time(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
    目的: 場次時間表(賽程表)
    日誌: 
        - 2022-06-06: 創建 (lina)
*/

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "In_Meeting_PEvent_Time";

            var itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                strIdentityId = inn.getUserAliases(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                scene = itmR.getProperty("scene", ""),
            };

            switch (cfg.scene)
            {
                case "page":
                    Page(cfg, itmR);
                    break;
                case "draw":
                    Draw(cfg, itmR);
                    break;
                case "clear":
                    ClearByFightDay(cfg, itmR);
                    break;
                case "query":
                    Query(cfg, itmR);
                    break;
                case "edit_type":
                    EditType(cfg, itmR);
                    Query(cfg, itmR);
                    break;
                case "edit_title":
                    EditTitle(cfg, itmR);
                    break;
                case "edit_event":
                    EditEvent(cfg, itmR);
                    break;
                case "clear_event":
                    ClearEvent(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void ClearEvent(TConfig cfg, Item itmReturn)
        {
            var event_id = itmReturn.getProperty("event_id", "");
            var sql = "UPDATE In_Meeting_PEvent SET in_time_num = NULL WHERE id = '" + event_id + "'";
            cfg.inn.applySQL(sql);
        }

        private void ClearByFightDay(TConfig cfg, Item itmReturn)
        {
            var in_fight_day = itmReturn.getProperty("in_fight_day", "");
            var sql = "DELETE FROM In_Meeting_PEvent_Time WHERE in_meeting = '{#meeting_id}' AND in_fight_day = '{#in_fight_day}'";
            sql = sql.Replace("{#meeting_id}", cfg.meeting_id).Replace("{#in_fight_day}", in_fight_day);
            cfg.inn.applySQL(sql);

            sql = "UPDATE In_Meeting_PEvent SET in_site = NULL, in_site_code = NULL, in_show_site = NULL, in_time_num = NULL WHERE in_meeting = '{#meeting_id}' AND in_date_key = '{#in_fight_day}'";
            sql = sql.Replace("{#meeting_id}", cfg.meeting_id).Replace("{#in_fight_day}", in_fight_day);
            cfg.inn.applySQL(sql);
        }

        private void EditEvent(TConfig cfg, Item itmReturn)
        {
            var in_fight_day = itmReturn.getProperty("in_fight_day", "");
            var event_id = itmReturn.getProperty("event_id", "");
            var in_time_num = itmReturn.getProperty("in_time_num", "");
            var site_code = itmReturn.getProperty("site_code", "");

            var itmSite = cfg.inn.applySQL("SELECT TOP 1 id FROM IN_MEETING_SITE WITH(NOLOCK) WHERE in_meeting = '" + cfg.meeting_id + "' AND in_code = '" + site_code + "'");
            var site_id = itmSite.getProperty("id", "");

            var sql = "UPDATE IN_MEETING_PEVENT SET"
                + "  in_site = '" + site_id + "'"
                + ", in_site_code = '" + site_code + "'"
                + ", in_show_site = '" + site_code + "'"
                + ", in_show_serial = '" + site_code + "'"
                + ", in_time_num = '" + in_time_num + "'"
                + " WHERE id = '" + event_id + "'";

            cfg.inn.applySQL(sql);
        }

        private void EditType(TConfig cfg, Item itmReturn)
        {
            var in_fight_day = itmReturn.getProperty("in_fight_day", "");
            var in_time_num = itmReturn.getProperty("in_time_num", "");
            var in_type = itmReturn.getProperty("in_type", "");
            var in_label = "";

            switch (in_type)
            {
                case "start": in_label = "開幕"; break;
                case "lunch": in_label = "午餐、休息"; break;
                case "close": in_label = "閉幕"; break;
            }

            var sql = "UPDATE In_Meeting_PEvent_Time SET"
                + "  in_type = '" + in_type + "'"
                + ", in_label = N'" + in_label + "'"
                + " WHERE in_meeting = '" + cfg.meeting_id + "'"
                + " AND in_fight_day = '" + in_fight_day + "'"
                + " AND in_time_num = '" + in_time_num + "'";

            cfg.inn.applySQL(sql);
        }

        private void EditTitle(TConfig cfg, Item itmReturn)
        {
            var in_fight_day = itmReturn.getProperty("in_fight_day", "");
            var in_time_num = itmReturn.getProperty("in_time_num", "");
            var in_title = itmReturn.getProperty("in_title", "");

            var sql = "UPDATE In_Meeting_PEvent_Time SET"
                + " in_title = N'" + in_title + "'"
                + " WHERE in_meeting = '" + cfg.meeting_id + "'"
                + " AND in_fight_day = '" + in_fight_day + "'"
                + " AND in_time_num = '" + in_time_num + "'";

            cfg.inn.applySQL(sql);
        }

        private void Query(TConfig cfg, Item itmReturn)
        {
            var in_fight_day = itmReturn.getProperty("in_fight_day", "");
            if (in_fight_day != "")
            {
                var itmTimes = GetMeetingTimeItems(cfg, in_fight_day);
                var time_count = itmTimes.getItemCount();
                itmReturn.setProperty("time_count", time_count.ToString());

                for (var i = 0; i < time_count; i++)
                {
                    var itmTime = itmTimes.getItemByIndex(i);
                    itmTime.setType("inn_time");
                    itmReturn.addRelationship(itmTime);
                }

                var itmEvents = GetMeetingEventItems(cfg, in_fight_day);
                var evt_count = itmEvents.getItemCount();

                for (var i = 0; i < evt_count; i++)
                {
                    var itmEvent = itmEvents.getItemByIndex(i);
                    itmEvent.setType("inn_event");
                    itmReturn.addRelationship(itmEvent);
                }
            }
            else
            {
                itmReturn.setProperty("time_count", "0");
            }
        }

        private void Draw(TConfig cfg, Item itmReturn)
        {
            var in_fight_day = itmReturn.getProperty("in_fight_day", "0");

            var min = 1;
            var max = GetInt(itmReturn.getProperty("in_time_count", "0"));

            for (var i = min; i <= max; i++)
            {
                var in_time_num = i;
                var itmNew = cfg.inn.newItem("In_Meeting_PEvent_Time", "merge");
                itmNew.setAttribute("where", "in_meeting = '" + cfg.meeting_id + "' AND in_fight_day = '" + in_fight_day + "' AND in_time_num = " + in_time_num);
                itmNew.setProperty("in_meeting", cfg.meeting_id);
                itmNew.setProperty("in_fight_day", in_fight_day);
                itmNew.setProperty("in_time_num", in_time_num.ToString());
                itmNew.setProperty("in_note", "");
                itmNew = itmNew.apply();
            }
        }

        //頁面
        private void Page(TConfig cfg, Item itmReturn)
        {
            var itmMeeting = cfg.inn.applySQL("SELECT in_title FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            CopyItemValue(itmReturn, itmMeeting, "in_title");

            var itmSites = GetMeetingSiteItems(cfg);
            var site_count = itmSites.getItemCount();
            itmReturn.setProperty("site_count", site_count.ToString());

            var in_fight_day = itmReturn.getProperty("in_fight_day", "");
            if (in_fight_day != "")
            {
                var itmTimes = GetMeetingTimeItems(cfg, in_fight_day);
                var time_count = itmTimes.getItemCount();
                itmReturn.setProperty("time_count", time_count.ToString());
            }
            else
            {
                itmReturn.setProperty("time_count", "0");
            }

            AppendDayItems(cfg, itmReturn);
        }

        private void AppendDayItems(TConfig cfg, Item itmReturn)
        {
            var itmEmpty = cfg.inn.newItem();
            itmEmpty.setType("inn_day");
            itmEmpty.setProperty("value", "");
            itmEmpty.setProperty("text", "請選擇比賽日期");
            itmReturn.addRelationship(itmEmpty);

            var itmDays = GetMeetingDayItems(cfg);
            var count = itmDays.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var itmDay = itmDays.getItemByIndex(i);
                var in_date_key = itmDay.getProperty("in_date_key", "");
                itmDay.setType("inn_day");
                itmDay.setProperty("value", in_date_key);
                itmDay.setProperty("text", in_date_key);
                itmReturn.addRelationship(itmDay);
            }
        }

        private Item GetMeetingSiteItems(TConfig cfg)
        {
            var sql = @"SELECT * FROM In_Meeting_Site WITH(NOLOCK) WHERE in_meeting = '{#meeting_id}' ORDER BY in_code";
            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);
            return cfg.inn.applySQL(sql);
        }

        private Item GetMeetingDayItems(TConfig cfg)
        {
            var sql = @"SELECT DISTINCT in_date_key from IN_MEETING_PEVENT with(nolock) where in_meeting = '{#meeting_id}' ORDER BY in_date_key";
            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);
            return cfg.inn.applySQL(sql);
        }

        private Item GetMeetingTimeItems(TConfig cfg, string in_fight_day)
        {
            var sql = @"SELECT id, in_time_num, in_type, in_title, in_label FROM In_Meeting_PEvent_Time WITH(NOLOCK) WHERE in_meeting = '{#meeting_id}' AND in_fight_day = '{#in_fight_day}' ORDER BY in_time_num";
            sql = sql.Replace("{#meeting_id}", cfg.meeting_id).Replace("{#in_fight_day}", in_fight_day);
            return cfg.inn.applySQL(sql);
        }

        private Item GetMeetingEventItems(TConfig cfg, string in_fight_day)
        {
            var sql = @"
                SELECT
	                t1.id AS 'pg_id'
	                , t1.in_name
	                , t1.in_name2
	                , t1.in_short_name
	                , t1.in_weight
	                , t1.in_l1
	                , t1.in_l2
	                , t1.in_l3
	                , t1.in_team_count
	                , t2.id AS 'evt_id'
	                , t2.in_tree_no
	                , t2.in_tree_sno
	                , t2.in_tree_name
	                , t2.in_tree_id
	                , t2.in_fight_id
	                , t2.in_tree_alias
	                , t2.in_sub_sect
	                , t2.in_date_key
	                , t2.in_site_code
	                , t2.in_time_num
	                , ISNULL(t3.in_sign_no, '') AS 'in_sign_no'
	                , ISNULL(t3.in_target_no, '') AS 'in_target_no'
                FROM
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PEVENT t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
	            INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t3 WITH(NOLOCK)
	                ON t3.source_id = t2.id
	                AND t3.in_sign_foot = 1
                WHERE 
	                t1.in_meeting = '{#meeting_id}'
	                AND t2.in_date_key = '{#in_fight_day}'
                ORDER BY
	                t1.in_sort_order
	                , t2.in_tree_no
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_fight_day}", in_fight_day);

            return cfg.inn.applySQL(sql);
        }

        private void CopyItemValue(Item itmA, Item itmB, string propA, string propB = "")
        {
            if (propB == "")
            {
                itmA.setProperty(propA, itmB.getProperty(propA, ""));
            }
            else
            {
                itmA.setProperty(propA, itmB.getProperty(propB, ""));
            }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }

            public string meeting_id { get; set; }
            public string scene { get; set; }
        }

        private int GetInt(string value, int def = 0)
        {
            int result = def;
            int.TryParse(value, out result);
            return result;
        }
    }
}