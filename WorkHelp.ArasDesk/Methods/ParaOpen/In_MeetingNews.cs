﻿using Aras.IOM;
using System.Collections.Generic;

namespace WorkHelp.ArasDesk.Methods.ParaOpen.Common
{
    public class In_MeetingNews : Item
    {
        public In_MeetingNews(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 最新消息
                日誌: 
                    - 2023-08-11: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "In_MeetingNews";

            var itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                meeting_id = itmR.getProperty("meeting_id", ""),
                news_id = itmR.getProperty("news_id", ""),
                scene = itmR.getProperty("scene", ""),
            };

            switch (cfg.scene)
            {
                case "remove_news":
                    RemoveNews(cfg, itmR);
                    break;

                case "remove_files":
                    RemoveNewsFile(cfg, itmR);
                    break;

                case "save":
                    if (cfg.news_id == "")
                    {
                        AddNews(cfg, itmR);
                    }
                    else
                    {
                        MergeNews(cfg, itmR);
                    }
                    break;

                case "edit_page":
                    EditPage(cfg, itmR);
                    break;

                case "page":
                    Page(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void RemoveNews(TConfig cfg, Item itmReturn)
        {
            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            //刪除關連與檔案
            var itmFiles = cfg.inn.applySQL("SELECT related_id FROM IN_NEWS_FILES WITH(NOLOCK) WHERE source_id = '" + cfg.news_id + "'");
            var count = itmFiles.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var itmFile = itmFiles.getItemByIndex(i);
                var file_id = itmFile.getProperty("related_id", "");
                RemoveNewsFile(cfg, cfg.news_id, file_id);
            }

            var itmOld = cfg.inn.newItem("In_News", "delete");
            itmOld.setAttribute("where", "id = '" + cfg.news_id + "'");
            itmOld = itmOld.apply();

            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);
        }

        private void RemoveNewsFile(TConfig cfg, Item itmReturn)
        {
            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            string file_id = itmReturn.getProperty("file_id", "");
            RemoveNewsFile(cfg, cfg.news_id, file_id);
           
            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);
        }

        private void RemoveNewsFile(TConfig cfg, string news_id, string file_id)
        {
            var itmOld = cfg.inn.newItem("In_News_Files", "delete");
            itmOld.setAttribute("where", "source_id = '" + news_id + "' AND related_id='" + file_id + "'");
            itmOld = itmOld.apply();

            var itmFile = cfg.inn.newItem("File", "delete");
            itmFile.setAttribute("where", "id = '" + file_id + "'");
            itmFile = itmFile.apply();
        }

        private void MergeNews(TConfig cfg, Item itmReturn)
        {
            var itmOld = cfg.inn.newItem("In_News", "merge");
            itmOld.setAttribute("where", "id = '" + cfg.news_id + "'");
            itmOld.setProperty("id", cfg.news_id);
            itmOld.setProperty("in_meeting", cfg.meeting_id);
            CopyItemValue(itmOld, itmReturn, "in_category");
            CopyItemValue(itmOld, itmReturn, "in_level");
            CopyItemValue(itmOld, itmReturn, "in_mode");
            CopyItemValue(itmOld, itmReturn, "in_write_org");
            CopyItemValue(itmOld, itmReturn, "in_head");
            CopyItemValue(itmOld, itmReturn, "in_body");
            CopyItemValue(itmOld, itmReturn, "in_link_url");
            CopyItemValue(itmOld, itmReturn, "in_link_tag");
            itmOld = itmOld.apply();

            AppendNewsFile(cfg, cfg.news_id, "new_file1", itmReturn);
            AppendNewsFile(cfg, cfg.news_id, "new_file2", itmReturn);
            AppendNewsFile(cfg, cfg.news_id, "new_file3", itmReturn);
        }

        private void AddNews(TConfig cfg, Item itmReturn)
        {
            var oNew = cfg.inn.newItem("In_News", "add");
            oNew.setProperty("in_meeting", cfg.meeting_id);
            CopyItemValue(oNew, itmReturn, "in_category");
            CopyItemValue(oNew, itmReturn, "in_level");
            CopyItemValue(oNew, itmReturn, "in_mode");
            CopyItemValue(oNew, itmReturn, "in_write_org");
            CopyItemValue(oNew, itmReturn, "in_head");
            CopyItemValue(oNew, itmReturn, "in_body");
            CopyItemValue(oNew, itmReturn, "in_link_url");
            CopyItemValue(oNew, itmReturn, "in_link_tag");
            oNew = oNew.apply();

            var news_id = oNew.getID();
            AppendNewsFile(cfg, news_id, "new_file1", itmReturn);
            AppendNewsFile(cfg, news_id, "new_file2", itmReturn);
            AppendNewsFile(cfg, news_id, "new_file3", itmReturn);
        }

        private void AppendNewsFile(TConfig cfg, string news_id, string property, Item itmReturn)
        {
            var new_file_id = itmReturn.getProperty(property, "");
            if (new_file_id == "") return;

            var oChild = cfg.inn.newItem("In_News_Files", "add");
            oChild.setProperty("source_id", news_id);
            oChild.setProperty("related_id", new_file_id);
            oChild = oChild.apply();
        }

        //編輯頁面
        private void EditPage(TConfig cfg, Item itmReturn)
        {
            AppendMeeting(cfg, itmReturn);
            AppendMenu(cfg, "VU_News_Category", "inn_category", itmReturn);
            AppendMenu(cfg, "VU_News_Level", "inn_level", itmReturn);
            AppendMenu(cfg, "VU_News_Mode", "inn_mode", itmReturn);

            if (cfg.news_id != "")
            {
                AppendOneNews(cfg, cfg.news_id, itmReturn);
            }
        }

        //頁面
        private void Page(TConfig cfg, Item itmReturn)
        {
            AppendMeeting(cfg, itmReturn);

            var file_map = GetNewsFilesMap(cfg, "");

            var sql = @"
				SELECT 
				    t1.*
					, CONVERT(VARCHAR, DATEADD(HOUR, 8, t1.created_on), 120) AS 'time'
					, t2.in_title
					, t3.label_en AS 'category_en'
					, t3.label_zt AS 'category_zt'
				FROM 
					IN_NEWS t1 WITH(NOLOCK)
				INNER JOIN
					IN_MEETING t2 WITH(NOLOCK)
					ON t2.id = t1.in_meeting
				INNER JOIN
					VU_NEWS_CATEGORY t3 WITH(NOLOCK)
					ON t3.value = t1.in_category
				ORDER BY
					t1.created_on DESC
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                item.setType("inn_news");
                item.setProperty("no", (i + 1).ToString());
                AppendNewsFiles(cfg, item, file_map, "1");
                itmReturn.addRelationship(item);
            }
        }

        private void AppendNewsFiles(TConfig cfg, Item item, Dictionary<string, List<Item>> map, string css)
        {
            item.setProperty("inn_files", " ");
            if (map == null || map.Count == 0) return;

            var key = item.getProperty("id", "");
            if (!map.ContainsKey(key)) return;

            var list = map[key];
            if (list == null || list.Count == 0) return;

            var links = css == "1"
                ? getFileLinks1(list)
                : getFileLinks2(list);


            item.setProperty("inn_files", string.Join("", links));
        }

        private List<string> getFileLinks1(List<Item> list)
        {
            var links = new List<string>();
            links.Add("<div style=\"display: flex; flex-direction: column;\">");
            for (var i = 0; i < list.Count; i++)
            {
                var itmFile = list[i];
                var fileid = itmFile.getProperty("fileid", "");
                var fileName = itmFile.getProperty("filename", "");
                var in_desc = itmFile.getProperty("in_desc", "");
                if (in_desc == "") in_desc = fileName;

                var link = ""
                    + "<a title='" + in_desc + "' href='javascript:void(0)'"
                    + " onclick=\"SwitchDownloadMode('../DownloadMeetingFile.aspx?fileid=" + fileid + "')\">"
                    + "(" + (i + 1) + "). " + in_desc
                    + "</a>";

                links.Add(link);
            }
            links.Add("</div>");
            return links;
        }

        private List<string> getFileLinks2(List<Item> list)
        {
            var del_func = "<i style='color: red' class='fa fa-remove' onclick='DeleteOneFile(this)' data-fileid='{#fileid}'></i>";

            var links = new List<string>();
            for (var i = 0; i < list.Count; i++)
            {
                var itmFile = list[i];
                var fileid = itmFile.getProperty("fileid", "");
                var fileName = itmFile.getProperty("filename", "");
                var in_desc = itmFile.getProperty("in_desc", "");
                if (in_desc == "") in_desc = fileName;

                var del = del_func.Replace("{#fileid}", fileid);

                var link = "<div class='news-file-item'>"
                    + "<a title='" + in_desc + "' href='javascript:void(0)'"
                    + " onclick=\"SwitchDownloadMode('../DownloadMeetingFile.aspx?fileid=" + fileid + "')\">"
                    + "(" + (i + 1) + "). " + in_desc
                    + "</a>"
                    + "&nbsp;" + del
                    + "</div>";

                links.Add(link);
            }
            return links;
        }

        //取得附加檔案
        private Dictionary<string, List<Item>> GetNewsFilesMap(TConfig cfg, string news_id)
        {
            var result = new Dictionary<string, List<Item>>();
            var cond = news_id == ""
                ? ""
                : "WHERE source_id = '" + news_id + "'";

            var sql = @"
                SELECT
	                t1.source_id
	                , t1.in_desc
	                , t2.id AS 'fileid'
	                , t2.filename
	                , t2.mimetype
                FROM
	                IN_NEWS_FILES t1 WITH(NOLOCK)
                INNER JOIN
	                [FILE] t2 WITH(NOLOCK)
	                ON t2.id = t1.related_id
	            {#cond}
                ORDER BY
	                t1.source_id
	                , t1.sort_order
            ";

            sql = sql.Replace("{#cond}", cond);

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var key = item.getProperty("source_id", "");
                if (result.ContainsKey(key))
                {
                    result[key].Add(item);
                }
                else
                {
                    var list = new List<Item>();
                    list.Add(item);
                    result.Add(key, list);
                }
            }

            return result;
        }

        private void AppendMenu(TConfig cfg, string viewName, string typeName, Item itmReturn)
        {
            var itmEmpty = cfg.inn.newItem();
            itmEmpty.setType(typeName);
            itmEmpty.setProperty("value", "");
            itmEmpty.setProperty("label_zt", "請選擇");
            itmEmpty.setProperty("label_en", "- select -");
            itmReturn.addRelationship(itmEmpty);

            var sql = "SELECT * FROM [" + viewName + "] ORDER BY sort_order";
            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                item.setType(typeName);
                itmReturn.addRelationship(item);
            }
        }

        private void AppendOneNews(TConfig cfg, string news_id, Item itmReturn)
        {
            var sql = "SELECT * FROM IN_NEWS WITH(NOLOCK) WHERE id = '" + news_id + "'";
            var itmNews = cfg.inn.applySQL(sql);
            CopyItemValue(itmReturn, itmNews, "in_category");
            CopyItemValue(itmReturn, itmNews, "in_level");
            CopyItemValue(itmReturn, itmNews, "in_mode");
            CopyItemValue(itmReturn, itmNews, "in_write_org");
            CopyItemValue(itmReturn, itmNews, "in_head");
            CopyItemValue(itmReturn, itmNews, "in_body");
            CopyItemValue(itmReturn, itmNews, "in_link_url");
            CopyItemValue(itmReturn, itmNews, "in_link_tag");

            var file_map = GetNewsFilesMap(cfg, cfg.news_id);
            AppendNewsFiles(cfg, itmNews, file_map, "2");

            CopyItemValue(itmReturn, itmNews, "inn_files");
        }

        private void AppendMeeting(TConfig cfg, Item itmReturn)
        {
            if (cfg.meeting_id == "") return;
            var sql = "SELECT in_title FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'";
            var itmMeeting = cfg.inn.applySQL(sql);
            CopyItemValue(itmReturn, itmMeeting, "in_title");
        }

        private void CopyItemValue(Item itmA, Item itmB, string propA, string propB = "")
        {
            if (propB == "")
            {
                itmA.setProperty(propA, itmB.getProperty(propA, ""));
            }
            else
            {
                itmA.setProperty(propA, itmB.getProperty(propB, ""));
            }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string meeting_id { get; set; }
            public string news_id { get; set; }
            public string scene { get; set; }
        }
    }
}