﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.ParaOpen.Common
{
    public class in_meeting_program_score_new : Item
    {
        public in_meeting_program_score_new(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 登錄成績(新版)
                日誌: 
                    - 2023-04-07: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "in_meeting_program_score_new";

            var itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,

                meeting_id = itmR.getProperty("meeting_id", ""),
                program_id = itmR.getProperty("program_id", ""),
                mode = itmR.getProperty("mode", ""),
                scene = itmR.getProperty("scene", ""),
                in_name = "",
                save_log = false,
            };

            if (cfg.scene == "robin_rank")
            {
                UpadteRobinRank(cfg, itmR);
            }
            else if (cfg.scene == "para_action")
            {
                DoParaAction(cfg, itmR);
            }
            else if (cfg.scene == "para_score")
            {
                DoParaScore(cfg, itmR);
            }
            else
            {
                FreshScore(cfg, itmR);
            }

            return itmR;
        }

        private void DoParaScore(TConfig cfg, Item itmReturn)
        {
            var event_id = itmReturn.getProperty("event_id", "");
            var in_sign_foot = itmReturn.getProperty("foot", "");
            var prop = itmReturn.getProperty("prop", "");
            var value = itmReturn.getProperty("value", "");

            if (event_id == "") throw new Exception("參數錯誤");
            if (in_sign_foot == "") throw new Exception("參數錯誤");
            if (prop == "") throw new Exception("參數錯誤");

            in_sign_foot = in_sign_foot.Replace("foot", "");

            var sql = "SELECT id FROM IN_MEETING_PEVENT_DETAIL WITH(NOLOCK) WHERE source_id = '" + event_id + "' AND in_sign_foot = '" + in_sign_foot + "'";
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);
            var itmDetail = cfg.inn.applySQL(sql);

            var detail_id = itmDetail.getProperty("id", "");
            var key = "in_" + prop;

            var sql_upd = "";
            if (key == "in_score")
            {
                sql_upd = "UPDATE IN_MEETING_PEVENT_DETAIL SET " + key + " = '" + value + "', in_points = '" + value + "' WHERE id = '" + detail_id + "'";
            }
            else
            {
                sql_upd = "UPDATE IN_MEETING_PEVENT_DETAIL SET " + key + " = '" + value + "' WHERE id = '" + detail_id + "'";
            }

            cfg.inn.applySQL(sql_upd);

        }

        private void DoParaAction(TConfig cfg, Item itmReturn)
        {
            var event_id = itmReturn.getProperty("event_id", "");
            var in_sign_foot = itmReturn.getProperty("foot", "");
            var act = itmReturn.getProperty("act", "");
            var value = itmReturn.getProperty("value", "");

            if (event_id == "") throw new Exception("參數錯誤");
            if (in_sign_foot == "") throw new Exception("參數錯誤");
            if (act == "") throw new Exception("參數錯誤");

            in_sign_foot = in_sign_foot.Replace("foot", "");

            var sql = "SELECT id FROM IN_MEETING_PEVENT_DETAIL WITH(NOLOCK) WHERE source_id = '" + event_id + "' AND in_sign_foot = '" + in_sign_foot + "'";
            var itmDetail = cfg.inn.applySQL(sql);

            if (act == "change_player")
            {
                //變更籤號
                if (value == "clear") value = "";
                LinkChallengeDetail(cfg, event_id, in_sign_foot, value);
            }
            else
            {
                //變更成績或狀態
                var itmData = cfg.inn.newItem();
                itmData.setType("In_Meeting_PEvent");
                itmData.setProperty("meeting_id", cfg.meeting_id);
                itmData.setProperty("program_id", cfg.program_id);
                itmData.setProperty("event_id", event_id);
                itmData.setProperty("detail_id", itmDetail.getProperty("id", ""));
                itmData.setProperty("points_type", act);
                itmData.setProperty("mode", "score");

                cfg.mode = "score";
                FreshScore(cfg, itmData);
            }
        }

        //更新分組名次
        private void UpadteRobinRank(TConfig cfg, Item itmReturn)
        {
            var in_sign_no = itmReturn.getProperty("in_sign_no", "");
            var in_robin_rank = itmReturn.getProperty("in_robin_rank", "");
            if (in_sign_no == "" || in_robin_rank == "") return;

            if (in_robin_rank == "clear")
            {
                ClearPlayerRobinRank(cfg, cfg.program_id, in_sign_no);
            }
            else
            {
                RefreshPlayerRobinRank1(cfg, cfg.program_id, in_sign_no, in_robin_rank);
            }
        }

        private void RefreshPlayerRobinRank1(TConfig cfg, string program_id, string in_sign_no, string in_robin_rank)
        {
            //清空已設定該名次的選手
            var sql1 = "UPDATE IN_MEETING_PTEAM SET in_robin_rank = NULL WHERE source_id = '" + program_id + "' AND in_robin_rank = '" + in_robin_rank + "'";
            cfg.inn.applySQL(sql1);

            var sql2 = "UPDATE IN_MEETING_PTEAM SET in_robin_rank = '" + in_robin_rank + "' WHERE source_id = '" + program_id + "' AND in_sign_no = '" + in_sign_no + "'";
            cfg.inn.applySQL(sql2);

            if (in_robin_rank.Length == 1)
            {
                UpdateTeamRank(cfg, cfg.program_id, in_sign_no, in_robin_rank);
            }
            else
            {
                var itmProgram = cfg.inn.applySQL("SELECT in_team_count FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + cfg.program_id + "'");
                var in_team_count = itmProgram.getProperty("in_team_count", "");

                LinkTreeEventFromRobinRank(cfg, cfg.program_id, in_team_count, "1", in_robin_rank, in_sign_no);
                LinkTreeEventFromRobinRank(cfg, cfg.program_id, in_team_count, "2", in_robin_rank, in_sign_no);
            }
        }

        private void RefreshPlayerRobinRank2(TConfig cfg, string program_id, string in_sign_no, string in_robin_rank, string in_team_count)
        {
            LinkTreeEventFromRobinRank(cfg, cfg.program_id, in_team_count, "1", in_robin_rank, in_sign_no);
            LinkTreeEventFromRobinRank(cfg, cfg.program_id, in_team_count, "2", in_robin_rank, in_sign_no);
        }

        private void UpdateTeamRank(TConfig cfg, string program_id, string in_sign_no, string in_robin_rank)
        {
            var sql = "UPDATE IN_MEETING_PTEAM SET"
                + "  in_final_rank = '" + in_robin_rank + "'"
                + ", in_show_rank = '" + in_robin_rank + "'"
                + " WHERE source_id = '" + program_id + "'"
                + " AND in_sign_no = '" + in_sign_no + "'";

            cfg.inn.applySQL(sql);
        }

        private void LinkTreeEventFromRobinRank(TConfig cfg, string program_id, string in_team_count, string foot, string in_robin_rank, string in_sign_no)
        {
            var sql = @"
                SELECT TOP 1
                	t1.in_alias
                	, t2.id
                FROM
                	IN_MEETING_DRAWEVENT t1 WITH(NOLOCK)
                INNER JOIN
                	IN_MEETING_PEVENT t2 WITH(NOLOCK)
                	ON t2.source_id = '{#program_id}'
                	AND t2.in_fight_id = t1.in_fight_id
                WHERE
                	t1.in_team_count = {#in_team_count}
                	AND t1.in_f{#foot} = '{#in_robin_rank}'
            ";

            sql = sql.Replace("{#program_id}", program_id)
                .Replace("{#in_team_count}", in_team_count)
                .Replace("{#foot}", foot)
                .Replace("{#in_robin_rank}", in_robin_rank);

            var item = cfg.inn.applySQL(sql);
            if (item.isError() || item.getResult() == "") return;

            var eid = item.getProperty("id", "");
            LinkChallengeDetail(cfg, eid, foot, in_sign_no);
        }

        private void ClearPlayerRobinRank(TConfig cfg, string program_id, string in_sign_no)
        {
            var sql = "UPDATE IN_MEETING_PTEAM SET in_robin_rank = NULL WHERE source_id = '" + program_id + "' AND in_sign_no = '" + in_sign_no + "'";
            cfg.inn.applySQL(sql);

            sql = "UPDATE IN_MEETING_PTEAM SET in_final_rank = NULL, in_show_rank = NULL WHERE source_id = '" + program_id + "' AND in_sign_no = '" + in_sign_no + "'";
            cfg.inn.applySQL(sql);
        }

        private void FreshScore(TConfig cfg, Item itmReturn)
        {
            //組別
            cfg.itmProgram = cfg.inn.applySQL("SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + cfg.program_id + "'");
            if (cfg.itmProgram.isError() || cfg.itmProgram.getResult() == "")
            {
                throw new Exception("查無組別");
            }

            cfg.in_name = cfg.itmProgram.getProperty("in_name", "0");
            cfg.in_team_count = cfg.itmProgram.getProperty("in_team_count", "0");
            cfg.in_rank_count = cfg.itmProgram.getProperty("in_rank_count", "0");
            cfg.in_fight_time = cfg.itmProgram.getProperty("in_fight_time", "0");
            cfg.team_count = GetIntVal(cfg.in_team_count);
            cfg.rank_count = GetIntVal(cfg.in_rank_count);
            cfg.fight_time = GetIntVal(cfg.in_fight_time);

            var act = new TAct
            {
                event_id = itmReturn.getProperty("event_id", ""),
                detail_id = itmReturn.getProperty("detail_id", ""),
                points_type = itmReturn.getProperty("points_type", ""),
            };

            //自身明細
            act.itmDetail = cfg.inn.applySQL("SELECT * FROM IN_MEETING_PEVENT_DETAIL WITH(NOLOCK) WHERE id = '" + act.detail_id + "'");
            if (act.itmDetail.isError() || act.itmDetail.getResult() == "")
            {
                throw new Exception("查無場次明細");
            }

            //對手明細
            act.itmOpponent = cfg.inn.applySQL("SELECT * FROM IN_MEETING_PEVENT_DETAIL WITH(NOLOCK) WHERE source_id = '" + act.event_id + "' AND id <> '" + act.detail_id + "'");
            if (act.itmOpponent.isError() || act.itmOpponent.getResult() == "")
            {
                throw new Exception("查無場次明細");
            }

            //場次
            act.itmEvent = cfg.inn.applySQL("SELECT * FROM IN_MEETING_PEVENT WITH(NOLOCK) WHERE id = '" + act.event_id + "'");
            if (act.itmEvent.isError() || act.itmEvent.getResult() == "")
            {
                throw new Exception("查無場次");
            }

            var msg = act.itmEvent.getProperty("in_fight_id", "")
                + "(" + act.itmEvent.getProperty("in_tree_no", "") + ")"
                + " _# " + act.points_type + " | " + cfg.mode;

            cfg.WgtLog(msg);


            //賽會參數
            cfg.variable_map = MapVariable(cfg);

            switch (cfg.mode)
            {
                case "score": //登錄成績
                    cfg.can_check_next = true;
                    EnterScore(cfg, act);
                    break;

                case "sync": //同步成績
                    cfg.can_check_next = true;
                    EnterScore(cfg, act);
                    break;

                case "status":
                    cfg.can_check_next = false;
                    EnterScore(cfg, act);
                    break;
            }
        }

        //登錄成績
        private void EnterScore(TConfig cfg, TAct act)
        {
            switch (act.points_type)
            {
                case "HWIN": //勝出
                    RunResetWinStatus(cfg, act.itmEvent, act.itmDetail, act.itmOpponent, WinStatus.FIGHT);
                    break;

                case "10": //全勝
                    ResetWinPointsForIppon(cfg, act.itmEvent, act.itmDetail, act.itmOpponent);
                    break;

                case "1": //半勝
                    ResetWinPointsForWaza_ari(cfg, act.itmEvent, act.itmDetail, act.itmOpponent);
                    break;

                case "0": //指導
                    ResetWinPointsForShido(cfg, act.itmEvent, act.itmDetail, act.itmOpponent);
                    break;

                case "DQ": //取消資格
                    ResetDisqualified(cfg, act.itmEvent, act.itmDetail, act.itmOpponent);
                    break;

                case "Initial": //歸零
                    ResetEventAndDetail(cfg, act.itmEvent, act.itmDetail, act.itmOpponent);
                    break;
            }
        }

        //歸零
        private void ResetEventAndDetail(TConfig cfg, Item itmEvent, Item itmDetailA, Item itmDetailB)
        {
            var sql = "";

            var eid = itmEvent.getProperty("id", "");
            var a_id = itmDetailA.getProperty("id", "");
            var b_id = itmDetailB.getProperty("id", "");

            //更新明細
            sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET"
                + "  in_sign_action = NULL"
                + ", in_points = NULL"
                + ", in_points_text = NULL"
                + ", in_correct_count = NULL"
                + ", in_status = NULL"
                + ", in_score1 = NULL"
                + ", in_score2 = NULL"
                + ", in_score3 = NULL"
                + ", in_score4 = NULL"
                + ", in_score5 = NULL"
                + ", in_score6 = NULL"
                + ", in_score7 = NULL"
                + ", in_score = NULL"
                + " WHERE id = '{#did}'";

            var sql_upd1 = sql.Replace("{#did}", a_id);
            cfg.inn.applySQL(sql_upd1);

            var sql_upd2 = sql.Replace("{#did}", b_id);
            cfg.inn.applySQL(sql_upd2);

            //更新場次
            sql = "UPDATE IN_MEETING_PEVENT SET"
                + "  in_win_time = NULL"
                + ", in_win_status = NULL"
                + ", in_win_sign_no = NULL"
                + ", in_win1 = NULL"
                + ", in_win2 = NULL"
                + ", in_win3 = NULL"
                + ", in_win4 = NULL"
                + ", in_win5 = NULL"
                + ", in_win6 = NULL"
                + ", in_win7 = NULL"
                + ", in_game_no = '1'"
                + ", in_game_kickoff = '1'"
                + ", in_game_leftfoot = '1'"
                + " WHERE id = '{#eid}'";

            var sql_upd3 = sql.Replace("{#eid}", eid);
            cfg.inn.applySQL(sql_upd3);

            var in_next_win = itmEvent.getProperty("in_next_win", "");
            var in_next_foot_win = itmEvent.getProperty("in_next_foot_win", "");
            var in_next_lose = itmEvent.getProperty("in_next_lose", "");
            var in_next_foot_lose = itmEvent.getProperty("in_next_foot_lose", "");

            ResetNextEventLink(cfg, in_next_win, in_next_foot_win, "");
            ResetNextEventLink(cfg, in_next_lose, in_next_foot_lose, "");
        }

        //取消資格
        private void ResetDisqualified(TConfig cfg, Item itmEvent, Item itmDetailA, Item itmDetailB)
        {
            var sql = "";

            var program_id = itmEvent.getProperty("source_id", "");

            var w_id = itmDetailB.getProperty("id", "");
            var w_no = itmDetailB.getProperty("in_sign_no", "");

            var l_id = itmDetailA.getProperty("id", "");
            var l_no = itmDetailA.getProperty("in_sign_no", "");

            //更新隊伍資料
            sql = "UPDATE IN_MEETING_PTEAM SET"
                + " in_check_result = '0'"
                + " , in_check_status = N'失格'"
                + " WHERE source_id = '" + program_id + "'"
                + " AND in_sign_no = '" + l_no + "'";
            cfg.inn.applySQL(sql);

            //我方失格
            sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET"
                + "  in_sign_action = N'失格'"
                + " WHERE id = '" + l_id + "'";
            cfg.inn.applySQL(sql);

            //對手不戰而勝
            sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET"
                + " in_sign_action = ''"
                + " WHERE id = '" + w_id + "'";
            cfg.inn.applySQL(sql);

            //刷新勝出狀態
            RunResetWinStatus(cfg, itmEvent, itmDetailB, itmDetailA, WinStatus.FIGHT);
        }

        //指導
        private void ResetWinPointsForShido(TConfig cfg, Item itmEvent, Item itmDetailA, Item itmDetailB)
        {
            var sql = "";

            var old_id = itmDetailA.getProperty("id", "");
            var old_shido = itmDetailA.getProperty("in_correct_count", "0");

            var new_shido = "1";
            var new_text = "";

            var need_reset_winner = false;
            if (old_shido == "" || old_shido == "0")
            {
                new_shido = "1";
                new_text = "";
            }
            else if (old_shido == "1")
            {
                new_shido = "2";
                new_text = "";
            }
            else if (old_shido == "2")
            {
                new_shido = "3";
                new_text = "因指導落敗";
                need_reset_winner = true;
            }
            else
            {
                new_shido = "3";
                new_text = "因指導落敗";
                need_reset_winner = true;
            }

            //我方因指導滿次數落敗
            sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET"
                + "  in_sign_action = ''"
                + ", in_points_text = N'" + new_text + "'"
                + ", in_correct_count = '" + new_shido + "'"
                + " WHERE id = '" + old_id + "'";

            cfg.inn.applySQL(sql);

            if (need_reset_winner)
            {
                //因指導勝出，對方分數加10
                var target_id = itmDetailB.getProperty("id", "0");
                int target_old_points = GetIntVal(itmDetailB.getProperty("in_points", "0"));
                int target_new_points = target_old_points;

                if (target_old_points == 0)
                {
                    target_new_points = 10;
                }
                else if (target_old_points == 1)
                {
                    target_new_points = 11;
                }

                //對手勝出
                sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET"
                    + "  in_sign_action = N'勝出'"
                    + ", in_points_text = N'因指導勝出'"
                    + ", in_points = " + target_new_points
                    + " WHERE id = '" + target_id + "'";

                cfg.inn.applySQL(sql);

                //刷新勝出狀態
                RunResetWinStatus(cfg, itmEvent, itmDetailB, itmDetailA, WinStatus.FIGHT);
            }
        }

        //半勝
        private void ResetWinPointsForWaza_ari(TConfig cfg, Item itmEvent, Item itmDetailW, Item itmDetailL)
        {
            var sql = "";

            var old_id = itmDetailW.getProperty("id", "");
            var old_points = itmDetailW.getProperty("in_points", "0");
            var new_points = "1";
            var new_text = "";

            var need_reset_winner = false;
            if (old_points == "" || old_points == "0")
            {
                new_text = "半勝";
                new_points = "1";
            }
            else if (old_points == "1")
            {
                new_text = "半勝+半勝";
                new_points = "10";
                need_reset_winner = true;
            }
            else if (old_points == "10")
            {
                new_text = "半勝+全勝";
                new_points = "11";
                need_reset_winner = true;
            }
            else if (old_points == "11")
            {
                new_text = "半勝+全勝";
                new_points = "11";
                need_reset_winner = true;
            }
            else
            {
                new_text = "半勝";
                new_points = "1";
            }

            var in_sign_action = need_reset_winner ? "勝出" : "";

            sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET"
                + "  in_sign_action = N'" + in_sign_action + "'"
                + ", in_points = '" + new_points + "'"
                + ", in_points_text = N'" + new_text + "'"
                + " WHERE id = '" + old_id + "'";

            cfg.inn.applySQL(sql);

            if (need_reset_winner)
            {
                //刷新勝出狀態
                RunResetWinStatus(cfg, itmEvent, itmDetailW, itmDetailL, WinStatus.FIGHT);
            }
        }

        //一勝勝出
        private void ResetWinPointsForIppon(TConfig cfg, Item itmEvent, Item itmDetailW, Item itmDetailL)
        {
            var sql = "";

            var old_id = itmDetailW.getProperty("id", "");
            var old_points = itmDetailW.getProperty("in_points", "0");
            var new_points = "10";
            var new_text = "";

            if (old_points == "" || old_points == "0")
            {
                new_text = "全勝";
                new_points = "10";
            }
            else if (old_points == "1")
            {
                new_text = "半勝+全勝";
                new_points = "11";
            }
            else
            {
                new_text = "全勝";
                new_points = "10";
            }

            sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET"
                + "  in_sign_action = N'勝出'"
                + ", in_points = '" + new_points + "'"
                + ", in_points_text = N'" + new_text + "'"
                + " WHERE id = '" + old_id + "'";

            cfg.inn.applySQL(sql);

            //刷新勝出狀態
            RunResetWinStatus(cfg, itmEvent, itmDetailW, itmDetailL, WinStatus.FIGHT);
        }

        //刷新勝出狀態
        private void RunResetWinStatus(TConfig cfg, Item itmEvent, Item itmDetailW, Item itmDetailL, WinStatus status, bool need_rpc_flow = true)
        {
            //轉型
            var row = GetEventRow(cfg, itmEvent, itmDetailW, itmDetailL, status);

            //刷新勝出狀態
            ResetWinStatus(cfg, row);

            //連結到下一場 - 勝
            ResetNextEventLink(cfg, row.w_eid, row.w_foot, row.w_no);
            //連結到下一場 - 敗
            ResetNextEventLink(cfg, row.l_eid, row.l_foot, row.l_no);

            //更新名次
            var rinfo = GetRankInfo(cfg, itmEvent);
            if (rinfo.need_update)
            {
                ResetPlayerRank(cfg, row.pid, row.eid, row.w_no, row.w_ft, rinfo.w_real_rank, rinfo.w_show_rank);
                ResetPlayerRank(cfg, row.pid, row.eid, row.l_no, row.l_ft, rinfo.l_real_rank, rinfo.l_show_rank);
            }

            var in_battle_type = cfg.itmProgram.getProperty("in_battle_type", "");

            if (need_rpc_flow)
            {
                //檢查敗部
                var rpc_arr = new string[] { "JudoTopFour", "Challenge" };
                if (rpc_arr.Contains(in_battle_type))
                {
                    //敗部改為動態設定
                    LinkForRepechagePlayer(cfg, itmEvent);

                    //挑戰賽加賽
                    var in_tree_id = itmEvent.getProperty("in_tree_id", "").ToUpper();
                    if (in_tree_id == "CA02")
                    {
                        LinkChallengeBEvent(cfg);
                    }
                }
            }

            if (in_battle_type.Contains("RoundRobin") || in_battle_type == "Cross")
            {
                //循環賽
                SummaryResults(cfg, itmEvent);
            }

            if (cfg.can_check_next)
            {
                //可以檢查下一場狀態
                ExeCheckNextEventId(cfg, row.eid);
            }
        }

        //遞迴
        private void ExeCheckNextEventId(TConfig cfg, string event_id)
        {
            if (event_id == "") return;
            var itmEvent = cfg.inn.applySQL("SELECT in_next_win FROM IN_MEETING_PEVENT WITH(NOLOCK) WHERE id = '" + event_id + "'");
            if (itmEvent.isError() || itmEvent.getResult() == "") return;

            var next_eid = itmEvent.getProperty("in_next_win", "");

            var sql_qry = "SELECT * FROM IN_MEETING_PEVENT_DETAIL WITH(NOLOCK) WHERE source_id = '" + next_eid + "' AND in_sign_foot = {#foot}";
            var itmDetailA = cfg.inn.applySQL(sql_qry.Replace("{#foot}", "1"));
            var itmDetailB = cfg.inn.applySQL(sql_qry.Replace("{#foot}", "2"));
            if (itmDetailA.isError() || itmDetailA.getResult() == "") return;
            if (itmDetailB.isError() || itmDetailB.getResult() == "") return;

            var a_name = itmDetailA.getProperty("in_player_name", "");
            var b_name = itmDetailB.getProperty("in_player_name", "");
            var a_did = itmDetailA.getProperty("id", "");
            var b_did = itmDetailB.getProperty("id", "");

            if (a_name == "消失" && b_name == "消失")
            {
                //雙方消失
                RunCancelEvent(cfg, cfg.program_id, next_eid);
            }
            else if (a_name == "消失")
            {
                //此方消失
                RunScore(cfg, next_eid, b_did, "HWIN");
            }
            if (b_name == "消失")
            {
                //對手消失
                RunScore(cfg, next_eid, a_did, "HWIN");
            }
        }

        private void RunScore(TConfig cfg, string event_id, string detail_id, string points_type)
        {
            var itmData = cfg.inn.newItem("In_Meeting_PEvent_Detail");
            itmData.setProperty("meeting_id", cfg.meeting_id);
            itmData.setProperty("program_id", cfg.program_id);
            itmData.setProperty("event_id", event_id);
            itmData.setProperty("detail_id", detail_id);
            itmData.setProperty("points_type", points_type);
            itmData.setProperty("mode", "score");
            itmData.apply("in_meeting_program_score_new");
        }

        #region 循環賽

        /// <summary>
        /// 總結成績
        /// </summary>
        private void SummaryResults(TConfig cfg, Item itmEvent)
        {
            var in_sub_sect = itmEvent.getProperty("in_sub_sect", "");
            if (in_sub_sect == "R") return;
            if (in_sub_sect == "S") return;
            if (in_sub_sect == "T") return;

            var evts = GetRobinEvents(cfg, in_sub_sect);
            var robin = MapRobin(cfg, evts);

            if (cfg.team_count == 2)
            {
                //三戰兩勝

                if (robin.finished_count <= 1)
                {
                    //已進行一場
                    return;
                }
                else if (robin.finished_count == 2)
                {
                    //已進行兩場

                    if (robin.players[0].win_count == 2)
                    {
                        //一人勝出兩場，取消第三場
                        robin.players[0].rank = 1;
                        robin.players[1].rank = 2;
                        UpdateRobinRanksOneGroup(cfg, robin.players, need_cancel_3rd: true);
                    }
                    else
                    {
                        //各贏一場: 復原名次與第三場
                        RecoverTwoFight3Rd(cfg);
                    }
                }
                else
                {
                    //已進行三場
                    robin.players[0].rank = 1;
                    robin.players[1].rank = 2;
                    UpdateRobinRanksOneGroup(cfg, robin.players, need_cancel_3rd: false);
                }
            }
            else if (robin.event_count == robin.finished_count)
            {
                //已盡數完賽

                int no = 1;
                int rank = 1;

                var last = default(TRobinPlayer);
                for (int i = 0; i < robin.players.Count; i++)
                {
                    var current = robin.players[i];
                    current.rank = 0;

                    if (last == null)
                    {
                        rank = GetRankByNo(no);
                    }
                    else
                    {
                        if (current.win_count == last.win_count
                            && current.win_points == last.win_points
                            && current.win_times == last.win_times
                            && current.in_weight_value == last.in_weight_value)
                        {
                            rank = last.rank;
                        }
                        else
                        {
                            rank = GetRankByNo(no);
                        }
                    }

                    current.rank = rank;
                    no++;
                    last = current;
                }

                ////該組取名次
                //int group_ranks = entity.team_count - 2;
                //var rank_list = robin.players.FindAll(x => x.rank > 0 && x.rank <= group_ranks);
                //SetRobinRanks(cfg, entity, rank_list, need_cancel_3rd: false);

                if (in_sub_sect == "")
                {
                    UpdateRobinRanksOneGroup(cfg, robin.players, need_cancel_3rd: false);
                }
                else
                {
                    UpdateRobinRanksManyGroups(cfg, robin.players, in_sub_sect, need_cancel_3rd: false);
                }
            }
        }

        private void RecoverTwoFight3Rd(TConfig cfg)
        {
            //復原名次
            string sql_upd1 = "UPDATE IN_MEETING_PTEAM SET"
                + " in_final_rank = NULL"
                + " , in_show_rank = NULL"
                + " WHERE source_id = '" + cfg.program_id + "'";

            cfg.inn.applySQL(sql_upd1);


            //復原沒打的那一場
            string sql_upd3 = "UPDATE IN_MEETING_PEVENT SET"
                + " in_win_status = N''"
                + " WHERE source_id = '" + cfg.program_id + "'"
                + " AND in_tree_id = 'M103'";

            cfg.inn.applySQL(sql_upd3);
        }

        private void UpdateRobinRanksManyGroups(TConfig cfg, List<TRobinPlayer> players, string in_sub_sect, bool need_cancel_3rd = false)
        {
            foreach (var player in players)
            {
                var rank = in_sub_sect + player.rank;

                //設定名次
                string sql_upd2 = "UPDATE IN_MEETING_PTEAM SET"
                    + " in_robin_rank = '" + rank + "'"
                    + " WHERE source_id = '" + cfg.program_id + "'"
                    + " AND in_sign_no = '" + player.in_sign_no + "'";

                cfg.inn.applySQL(sql_upd2);

                RefreshPlayerRobinRank2(cfg, cfg.program_id, player.in_sign_no, rank, cfg.in_team_count);
            }
        }

        private void UpdateRobinRanksOneGroup(TConfig cfg, List<TRobinPlayer> players, bool need_cancel_3rd = false)
        {
            foreach (var player in players)
            {
                //設定名次
                string sql_upd2 = "UPDATE IN_MEETING_PTEAM SET"
                    + "  in_final_rank = '" + player.rank + "'"
                    + ", in_show_rank = '" + player.rank + "'"
                    + ", in_robin_rank = '" + player.rank + "'"
                    + ", in_final_count = '" + player.win_count + "'"
                    + ", in_final_points = '" + player.win_points + "'"
                    + ", in_final_sands = '" + player.win_times + "'"
                    + " WHERE source_id = '" + cfg.program_id + "'"
                    + " AND in_sign_no = '" + player.in_sign_no + "'";

                cfg.inn.applySQL(sql_upd2);
            }

            if (need_cancel_3rd)
            {
                //取消沒打的那一場
                string sql_upd3 = "UPDATE IN_MEETING_PEVENT SET"
                    + " in_win_status = N'cancel'"
                    + " WHERE source_id = '" + cfg.program_id + "'"
                    + " AND in_tree_id = 'M103'";

                cfg.inn.applySQL(sql_upd3);
            }
        }

        private int GetRankByNo(int no)
        {
            switch (no)
            {
                case 1:
                    return 1;
                case 2:
                    return 2;
                case 3:
                    return 3;
                case 4:
                    return 4;
                case 5:
                    return 5;
                case 6:
                    return 6;
                case 7:
                    return 7;
                case 8:
                    return 8;
                case 9:
                    return 9;
                case 10:
                    return 10;
                default:
                    return no;
            }
        }

        private TRobin MapRobin(TConfig cfg, List<TEvent> evts)
        {
            var finished_count = 0;
            var players = new List<TRobinPlayer>();

            for (int i = 0; i < evts.Count; i++)
            {
                var evt = evts[i];
                if (evt.f1 == null) evt.f1 = new TDetail { value = cfg.inn.newItem() };
                if (evt.f2 == null) evt.f2 = new TDetail { value = cfg.inn.newItem() };

                var itmEvent = evt.value;
                var itmFoot1 = evt.f1.value;
                var itmFoot2 = evt.f2.value;
                var f1_sign_no = itmFoot1.getProperty("in_sign_no", "");
                var f2_sign_no = itmFoot2.getProperty("in_sign_no", "");

                AppendRobinPlayer(players, f1_sign_no);
                AppendRobinPlayer(players, f2_sign_no);

                var in_win_sign_no = itmEvent.getProperty("in_win_sign_no", "");
                if (in_win_sign_no == "" || in_win_sign_no == "0")
                {
                    continue;
                }

                finished_count++;


                var winner = players.Find(x => x.in_sign_no == in_win_sign_no);
                if (winner == null)
                {
                    cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "循環戰資料錯誤");
                }

                var itmTarget = default(Item);
                if (in_win_sign_no == f1_sign_no)
                {
                    itmTarget = itmFoot1;
                }
                else if (in_win_sign_no == f2_sign_no)
                {
                    itmTarget = itmFoot2;
                }
                else
                {
                    cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "三戰兩勝 勝出者籤號異常" + cfg.program_id);
                    continue;
                }

                var in_points = itmTarget.getProperty("in_points", "0");
                var in_win_local_time = itmTarget.getProperty("in_win_local_time", "").Trim();
                var in_weight_value = itmTarget.getProperty("in_weight_value", "0");

                winner.in_weight_value = GetDcmVal(in_weight_value);
                winner.win_count++;
                winner.win_points += GetIntVal(in_points);
                winner.win_times += SumTime(cfg, in_win_local_time);
                winner.times.Add(in_win_local_time);
            }

            //(1)比較選手勝場數，勝場數多者，名次在前。 

            //(2)若勝場數相同時，則比較選手積分(黃金得分積分與正規時間內得分相同)，
            //    ㄧ勝換算積分 10 分，半勝換算積分 1 分，被判犯規輸等同對方獲一勝之積分。

            //(3)若積分仍相同時，則比較該兩人比賽勝負關係，勝者名次在前。如勝場數、積分及勝負關係仍無法判定時，則依下列順序判定：
            //    A.比較選手勝場之時間總和，時間最短者，名次在前。
            //    B.比較選手之過磅單，體重較輕者，名次在前。
            //    C.若以上方式仍無法判定名次時，採用淘汰賽制重新抽籤加賽。

            var orderList = players
                .OrderByDescending(x => x.win_count) //勝場數
                .ThenByDescending(x => x.win_points) //積分
                .ThenBy(x => x.win_times)            //時間總和
                .ThenBy(x => x.in_weight_value)      //體重
                .ToList();

            TRobin result = new TRobin
            {
                event_count = evts.Count,
                finished_count = finished_count,
                players = orderList,
            };

            return result;
        }

        private int SumTime(TConfig cfg, string value)
        {
            if (cfg.fight_time <= 0 || value == "") return 0;

            int fight_seconds = cfg.fight_time * 60; //比賽總秒數
            int sands = 0;

            if (value.Contains("GS"))
            {
                string tm = value.Trim().Replace("GS", "").Trim();//GS3:10
                string[] arr = tm.Split(':');
                if (arr != null && arr.Length == 2)
                {
                    sands = GetIntVal(arr[0]) * 60 + GetIntVal(arr[1]);
                }
            }
            else
            {
                string[] arr = value.Trim().Split(':');
                if (arr != null && arr.Length == 2)
                {
                    sands = GetIntVal(arr[0]) * 60 + GetIntVal(arr[1]);
                    sands *= -1;
                }
            }

            int total_seconds = fight_seconds + sands;

            if (total_seconds < 0) total_seconds = 0;

            return total_seconds;
        }

        private void AppendRobinPlayer(List<TRobinPlayer> players, string in_sign_no)
        {
            var player = players.Find(x => x.in_sign_no == in_sign_no);

            if (player == null)
            {
                player = new TRobinPlayer
                {
                    in_sign_no = in_sign_no,
                    win_count = 0,
                    win_points = 0,
                    win_times = 0,
                    times = new List<string>(),
                };

                players.Add(player);
            }
        }

        private List<TEvent> GetRobinEvents(TConfig cfg, string in_sub_sect)
        {
            var sql = @"
                SELECT 
	                t1.in_tree_id
	                , t1.in_tree_no
	                , t2.in_sign_foot
	                , t2.in_sign_no
	                , t1.in_win_sign_no
	                , t1.in_win_local_time
	                , t2.in_points
	                , t2.in_correct_count
					, t3.in_weight_value
                FROM 
	                IN_MEETING_PEVENT t1 WITH(NOLOCK) 
                INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
				LEFT OUTER JOIN
					IN_MEETING_PTEAM t3
					ON t3.source_id = t1.source_id
					AND t3.in_sign_no = t2.in_sign_no
                WHERE 
	                t1.source_id = '{#program_id}' 
					AND ISNULL(t1.in_sub_sect, '') = '{#in_sub_sect}'
                ORDER BY
	                t1.in_tree_no
	                , t2.in_sign_foot
            ";

            sql = sql.Replace("{#program_id}", cfg.program_id)
                .Replace("{#in_sub_sect}", in_sub_sect);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            var evts = new List<TEvent>();

            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var in_tree_id = item.getProperty("in_tree_id", "");
                var in_sign_foot = item.getProperty("in_sign_foot", "");

                var evt = evts.Find(x => x.in_tree_id == in_tree_id);
                if (evt == null)
                {
                    evt = new TEvent
                    {
                        in_tree_id = in_tree_id,
                        value = item,
                    };
                    evts.Add(evt);
                }

                if (in_sign_foot == "1")
                {
                    evt.f1 = new TDetail { value = item };
                }
                else if (in_sign_foot == "2")
                {
                    evt.f2 = new TDetail { value = item };
                }
            }

            return evts;
        }

        private class TEvent
        {
            public string in_tree_id { get; set; }
            public Item value { get; set; }
            public TDetail f1 { get; set; }
            public TDetail f2 { get; set; }
        }

        private class TDetail
        {
            public Item value { get; set; }
        }

        private class TRobin
        {
            public int event_count { get; set; }
            public int finished_count { get; set; }
            public List<TRobinPlayer> players { get; set; }
        }

        private class TRobinPlayer
        {
            public string in_sign_no { get; set; }
            public decimal in_weight_value { get; set; }
            public int rank { get; set; }

            public int win_count { get; set; }
            public int win_points { get; set; }
            public int win_times { get; set; }

            public List<string> times { get; set; }
        }

        #endregion 循環賽

        //刷新勝出狀態
        private void ResetWinStatus(TConfig cfg, TRow row)
        {
            var sql = "";

            //更新場次勝敗狀態
            sql = "UPDATE IN_MEETING_PEVENT SET"
                + "  in_win_time = '" + row.new_win_time + "'"
                + ", in_win_status = '" + row.new_win_status + "'"
                + ", in_win_sign_no = '" + row.w_no + "'"
                + " WHERE id = '" + row.eid + "' ";
            cfg.inn.applySQL(sql);

            //更新為敗
            sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET in_status = '0' WHERE id = '" + row.l_id + "'";
            cfg.inn.applySQL(sql);

            //更新為勝
            sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET in_status = '1' WHERE id = '" + row.w_id + "'";
            cfg.inn.applySQL(sql);

            if (row.in_type != "s")
            {
                //將未決的子場註取消，並註記主場決出時間
                sql = "UPDATE IN_MEETING_PEVENT SET"
                    + "  in_win_status = 'cancel'"
                    + ", in_win_time = '" + row.new_win_time + "'"
                    + " WHERE in_parent = '" + row.eid + "'"
                    + " AND in_win_time IS NULL";
                cfg.inn.applySQL(sql);
            }
        }

        #region 挑戰賽

        /// <summary>
        /// 如果挑戰賽的勝方不為 main 的勝方
        /// </summary>
        private void LinkChallengeBEvent(TConfig cfg)
        {
            //挑戰賽加賽不換道服
            var itmMtVrl = cfg.inn.applyMethod("In_Meeting_Variable"
                    , "<meeting_id>" + cfg.meeting_id + "</meeting_id>"
                    + "<name>challenge_end_no_swap</name>"
                    + "<scene>get</scene>");

            var challenge_end_no_swap = itmMtVrl.getProperty("inn_result", "");

            RunLinkChallengeBEvent(cfg, cfg.program_id, challenge_end_no_swap);
        }

        /// <summary>
        /// 如果挑戰賽的勝方不為 main 的勝方
        /// </summary>
        private void RunLinkChallengeBEvent(TConfig cfg, string program_id, string challenge_end_no_swap)
        {
            string sql = "";

            //查找 2 vs 1 的勝出者
            sql = "SELECT TOP 1 id, in_win_sign_no FROM IN_MEETING_PEVENT WITH(NOLOCK)"
                + " WHERE source_id = '" + program_id + "' AND in_tree_id = 'CA02'";

            var itmCA02Champion = cfg.inn.applySQL(sql);

            if (itmCA02Champion.isError() || itmCA02Champion.getResult() == "")
            {
                return;
            }

            //查找主線冠軍
            sql = "SELECT TOP 1 id, in_win_sign_no FROM IN_MEETING_PEVENT WITH(NOLOCK)"
                + " WHERE source_id = '" + program_id + "' AND in_tree_name = 'main' AND in_round_code = 2";

            var itmMainChampion = cfg.inn.applySQL(sql);

            if (itmMainChampion.isError() || itmMainChampion.getResult() == "")
            {
                return;
            }

            var itmEventB = GetOneEvent(cfg, program_id, "CB01");
            var event_id = itmEventB.getProperty("id", "");

            //主線冠軍籤號
            string main_champion_sign_no = itmMainChampion.getProperty("in_win_sign_no", "");
            //挑戰線冠軍籤號
            string ca02_champion_sign_no = itmCA02Champion.getProperty("in_win_sign_no", "");

            if (ca02_champion_sign_no == main_champion_sign_no)
            {
                //維持第一名勝出，不打最終決賽
                cfg.inn.applySQL("UPDATE IN_MEETING_PEVENT SET in_win_status = 'cancel' WHERE id = '" + event_id + "'");
                cfg.inn.applySQL("UPDATE IN_MEETING_PROGRAM SET in_challenge = 'no_more' WHERE id = '" + program_id + "'");
                return;
            }

            cfg.inn.applySQL("UPDATE IN_MEETING_PEVENT SET in_win_status = '' WHERE id = '" + event_id + "'");
            cfg.inn.applySQL("UPDATE IN_MEETING_PROGRAM SET in_challenge = 'one_more' WHERE id = '" + program_id + "'");

            var old_champion_foot = "1";
            var new_champion_foot = "2";
            if (challenge_end_no_swap == "1")
            {
                //挑戰賽加賽換道服   
                old_champion_foot = "2";
                new_champion_foot = "1";
            }

            LinkChallengeDetail(cfg, event_id, old_champion_foot, main_champion_sign_no);
            LinkChallengeDetail(cfg, event_id, new_champion_foot, ca02_champion_sign_no);
        }

        /// <summary>
        /// 將籤號更新至下個場次
        /// </summary>
        private void LinkChallengeDetail(TConfig cfg, string eid, string foot, string sign_no)
        {
            var sql = "";
            var oppo_foot = foot == "1" ? "2" : "1";

            sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET"
                + " in_sign_no = '" + sign_no + "'"
                + ", in_sign_status = ''"
                + ", in_player_name = ''"
                + " WHERE source_id = '" + eid + "'"
                + " AND in_sign_foot = '" + foot + "'";

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            cfg.inn.applySQL(sql);


            sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET"
                + " in_target_no = '" + sign_no + "'"
                + ", in_target_status = ''"
                + " WHERE source_id = '" + eid + "'"
                + " AND in_sign_foot = '" + oppo_foot + "'";

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            cfg.inn.applySQL(sql);
        }

        /// 取得場次
        /// </summary>
        private Item GetOneEvent(TConfig cfg, string program_id, string in_tree_id)
        {
            string sql = @"
                SELECT
                    TOP 1 t1.*
                FROM
                    IN_MEETING_PEVENT t1 WITH(NOLOCK)
                WHERE
                    t1.source_id = '{#program_id}'
                    AND t1.in_tree_id = '{#in_tree_id}'
            ";

            sql = sql.Replace("{#program_id}", program_id)
                .Replace("{#in_tree_id}", in_tree_id);

            return cfg.inn.applySQL(sql);
        }

        #endregion 挑戰賽

        #region 四柱復活

        private void LinkForRepechagePlayer(TConfig cfg, Item itmEvent)
        {
            var program_id = cfg.itmProgram.getProperty("id", "");
            var in_team_count = cfg.itmProgram.getProperty("in_team_count", "0");

            var in_fight_id = itmEvent.getProperty("in_fight_id", "");

            var items = GetActionItemsByTeamCnt(cfg, program_id, in_team_count, in_fight_id);
            if (items.isError() || items.getResult() == "") return;

            var count = items.getItemCount();
            var list = new List<string>();

            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var in_target_sign_foot = item.getProperty("in_target_sign_foot", "");
                var evt_win_no = item.getProperty("evt_win_no", "");
                var act_round = item.getProperty("act_round", "");

                //下一場
                var next_evt_id = item.getProperty("next_evt_id", "");
                var next_cancel = item.getProperty("next_cancel", "");

                if (next_evt_id == "") continue;

                if (!list.Contains(next_evt_id))
                {
                    list.Add(next_evt_id);
                }

                //對手
                var itmFighter = GetOpponentItem(cfg, program_id, evt_win_no, act_round);
                if (itmFighter.isError() || itmFighter.getResult() == "")
                {
                    itmFighter = cfg.inn.newItem();
                }

                //if (next_cancel == "1")
                //{
                //    return;
                //}

                var row = new TRpcDetail
                {
                    in_tree_id = itmFighter.getProperty("in_tree_id", ""),
                    in_tree_no = itmFighter.getProperty("in_tree_no", ""),
                    team_id = itmFighter.getProperty("team_id", ""),
                    sign_no = itmFighter.getProperty("in_sign_no", ""),
                    sign_status = itmFighter.getProperty("in_sign_status", ""),
                    weight_message = itmFighter.getProperty("in_weight_message", ""),
                    player_name = itmFighter.getProperty("in_player_name", ""),
                };

                if (row.sign_status != "cancel")
                {
                    row.sign_status = "";
                }


                row.next_sign_foot = in_target_sign_foot;
                row.next_target_foot = row.next_sign_foot == "1" ? "2" : "1";
                row.next_event_id = next_evt_id;

                if (row.player_name == "消失")
                {
                    row.sign_bypass = "1";
                    UpdateRepechageDetailDQ(cfg, row);
                }
                else if (row.weight_message == "")
                {
                    row.sign_bypass = row.team_id == "" ? "1" : "0";
                    UpdateRepechageDetailOK(cfg, row);
                }
                else
                {
                    row.sign_bypass = "1";
                    UpdateRepechageDetailDQ(cfg, row);
                }

            }

            //復活賽名單已 assigned
            for (var i = 0; i < list.Count; i++)
            {
                var next_evt_id = list[i];
                FixRepechageLink(cfg, next_evt_id);
            }

            var end_evt_id = list.Last();
            ExeCheckNextEventId(cfg, end_evt_id);
        }

        //修復復活賽
        private void FixRepechageLink(TConfig cfg, string eid)
        {
            if (eid == "") return;

            var itmEvent = cfg.inn.applySQL("SELECT * FROM IN_MEETING_PEVENT WITH(NOLOCK) WHERE id = '" + eid + "'");
            if (itmEvent.isError() || itmEvent.getResult() == "") return;

            var itmDetails = cfg.inn.applySQL("SELECT * FROM IN_MEETING_PEVENT_DETAIL WITH(NOLOCK) WHERE source_id = '" + eid + "' ORDER BY in_sign_foot");
            if (itmDetails.isError() || itmDetails.getResult() == "") return;


            var count = itmDetails.getItemCount();
            if (count != 2) return;
            var itmFoot1 = default(Item);
            var itmFoot2 = default(Item);
            for (var i = 0; i < count; i++)
            {
                var itmDetail = itmDetails.getItemByIndex(i);
                var foot = itmDetail.getProperty("in_sign_foot", "");
                if (foot == "1") itmFoot1 = itmDetail;
                else if (foot == "2") itmFoot2 = itmDetail;
            }
            if (itmFoot1 == null) itmFoot1 = cfg.inn.newItem();
            if (itmFoot2 == null) itmFoot2 = cfg.inn.newItem();

            var in_tree_no = itmEvent.getProperty("in_tree_no", "");
            var f1_assigned = itmFoot1.getProperty("in_assigned", "");
            var f2_assigned = itmFoot2.getProperty("in_assigned", "");
            var msg = itmEvent.getProperty("in_fight_id", "") + "(" + itmEvent.getProperty("in_tree_no", "") + ")";


            if (in_tree_no == "" || in_tree_no == "0")
            {
                //表示此場次輪空
                AnalysisRepechageEvent(cfg, itmEvent, itmFoot1, itmFoot2);
            }
            else
            {
                //非輪空，復活賽名單已分配
                if (f1_assigned == "1" && f2_assigned == "1")
                {
                    AnalysisRepechageEvent(cfg, itmEvent, itmFoot1, itmFoot2);
                }
                else if (f1_assigned == "1" || f2_assigned == "1")
                {
                    //僅一方配置籤號
                    var in_win_local = itmEvent.getProperty("in_win_local", "");
                    if (in_win_local == "")
                    {
                        //尚無計分成績，復歸狀態
                        cfg.RpcLog(msg + " _# 尚無計分成績，復歸狀態(1)");
                        ResetEventAndDetail(cfg, itmEvent, itmFoot1, itmFoot2);
                    }
                    else
                    {
                        cfg.RpcLog(msg + " _# 不處理");
                    }
                }
                else
                {
                    cfg.RpcLog(msg + " _# 不處理");
                }
            }
        }

        private void AnalysisRepechageEvent(TConfig cfg, Item itmEvent, Item itmFoot1, Item itmFoot2)
        {
            var evt_id = itmEvent.getProperty("id", "");
            var in_win_local = itmEvent.getProperty("in_win_local", "");
            var msg = itmEvent.getProperty("in_fight_id", "") + "(" + itmEvent.getProperty("in_tree_no", "") + ")";

            var f1_sign_no = itmFoot1.getProperty("in_sign_no", "");
            var f1_points = itmFoot1.getProperty("in_points", "0");
            var f1_shido = itmFoot1.getProperty("in_correct_count", "0");
            var f1_player_name = itmFoot1.getProperty("in_player_name", "");

            var f2_sign_no = itmFoot2.getProperty("in_sign_no", "");
            var f2_points = itmFoot2.getProperty("in_points", "0");
            var f2_shido = itmFoot2.getProperty("in_correct_count", "0");
            var f2_player_name = itmFoot2.getProperty("in_player_name", "");

            var f1_team_id = GetTeamId(cfg, cfg.program_id, f1_sign_no);
            var f2_team_id = GetTeamId(cfg, cfg.program_id, f2_sign_no);

            if (f1_team_id != "" && f2_team_id != "")
            {
                if (f2_shido == "3")
                {
                    cfg.RpcLog(msg + " _# Foot1 指導勝出");
                    RunResetWinStatus(cfg, itmEvent, itmFoot1, itmFoot2, WinStatus.FIGHT, need_rpc_flow: false);
                }
                else if (f1_shido == "3")
                {
                    cfg.RpcLog(msg + " _# Foot2 指導勝出");
                    RunResetWinStatus(cfg, itmEvent, itmFoot2, itmFoot1, WinStatus.FIGHT, need_rpc_flow: false);
                }
                else if (f1_points == "10" || f1_points == "11")
                {
                    cfg.RpcLog(msg + " _# Foot1 勝出");
                    RunResetWinStatus(cfg, itmEvent, itmFoot1, itmFoot2, WinStatus.FIGHT, need_rpc_flow: false);
                }
                else if (f2_points == "10" || f2_points == "11")
                {
                    cfg.RpcLog(msg + " _# Foot2 勝出");
                    RunResetWinStatus(cfg, itmEvent, itmFoot2, itmFoot1, WinStatus.FIGHT, need_rpc_flow: false);
                }
                else if (in_win_local == "")
                {
                    cfg.RpcLog(msg + " _# 尚無計分成績，復歸狀態(2)");
                    ResetEventAndDetail(cfg, itmEvent, itmFoot1, itmFoot2);
                }
                else
                {
                    cfg.RpcLog(msg + " _# 不處理");
                }
            }
            else if (f1_team_id != "")
            {
                cfg.RpcLog(msg + " _# Foot1 輪空勝出");
                RunResetWinStatus(cfg, itmEvent, itmFoot1, itmFoot2, WinStatus.BYPASS, need_rpc_flow: false);
            }
            else if (f2_team_id != "")
            {
                cfg.RpcLog(msg + " _# Foot2 輪空勝出");
                RunResetWinStatus(cfg, itmEvent, itmFoot2, itmFoot1, WinStatus.BYPASS, need_rpc_flow: false);
            }
            else
            {
                if (f1_player_name == "消失" && f2_player_name == "消失")
                {
                    //雙方消失，該場取消
                    cfg.RpcLog(msg + " _# 雙方消失");
                    RunCancelEvent(cfg, cfg.program_id, evt_id);
                    RunCancelEventAndAssigned(cfg, cfg.program_id, itmEvent);
                }
                else if (f1_player_name == "消失" || f2_player_name == "消失")
                {
                    //一方消失，該場取消
                    cfg.RpcLog(msg + " _# 一方消失");
                    RunCancelEvent(cfg, cfg.program_id, evt_id);
                    RunCancelEventAndAssigned(cfg, cfg.program_id, itmEvent);
                }
                else
                {
                    cfg.RpcLog(msg + " _# 不處理");
                }
            }
        }

        private void RunCancelEventAndAssigned(TConfig cfg, string program_id, Item itmEvent)
        {
            var in_next_win = itmEvent.getProperty("in_next_win", "");
            var in_next_foot_win = itmEvent.getProperty("in_next_foot_win", "");
            if (in_next_win == "") return;

            var sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET in_assigned = '1' WHERE source_id = '" + in_next_win + "' AND in_sign_foot = '" + in_next_foot_win + "'";
            cfg.inn.applySQL(sql);
        }

        private void RunCancelEvent(TConfig cfg, string program_id, string event_id)
        {
            //該場取消
            var itmData = cfg.inn.newItem("In_Meeting_PEvent_Detail");
            itmData.setProperty("meeting_id", cfg.meeting_id);
            itmData.setProperty("program_id", program_id);
            itmData.setProperty("event_id", event_id);
            itmData.setProperty("scene", "cancel");
            itmData.apply("in_meeting_pevent");
        }

        private void RunRollbackEvent(TConfig cfg, string program_id, string event_id)
        {
            //該場復原
            var itmData = cfg.inn.newItem("In_Meeting_PEvent_Detail");
            itmData.setProperty("meeting_id", cfg.meeting_id);
            itmData.setProperty("program_id", program_id);
            itmData.setProperty("event_id", event_id);
            itmData.setProperty("scene", "rollback");
            itmData.apply("in_meeting_pevent");
        }

        private string GetTeamId(TConfig cfg, string program_id, string in_sign_no)
        {
            if (in_sign_no == "" || in_sign_no == "0") return "";

            var sql = "SELECT id FROM IN_MEETING_PTEAM WITH(NOLOCK)"
                + " WHERE source_id = '" + program_id + "'"
                + " AND in_sign_no = '" + in_sign_no + "'";

            var itemTeam = cfg.inn.applySQL(sql);
            if (itemTeam.isError() || itemTeam.getResult() == "")
            {
                return "";
            }
            else
            {
                return itemTeam.getProperty("id", "");
            }
        }

        //更新對戰明細
        private void UpdateRepechageDetailOK(TConfig cfg, TRpcDetail row)
        {
            string sql = "";

            sql = @"
                UPDATE
                    IN_MEETING_PEVENT_DETAIL 
                SET
                    in_sign_no = '{#sign_no}'
                    , in_sign_bypass = '{#sign_bypass}'
                    , in_sign_status = '{#sign_status}'
                    , in_player_org = ''
                    , in_player_name = ''
                    , in_assigned = '1'
                WHERE
                    source_id = '{#event_id}' 
                    AND in_sign_foot = '{#sign_foot}'
            ";

            sql = sql.Replace("{#event_id}", row.next_event_id)
                .Replace("{#sign_foot}", row.next_sign_foot)
                .Replace("{#sign_no}", row.sign_no)
                .Replace("{#sign_bypass}", row.sign_bypass)
                .Replace("{#sign_status}", row.sign_status);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            cfg.inn.applySQL(sql);

            sql = @"
                UPDATE
                    IN_MEETING_PEVENT_DETAIL 
                SET
                    in_target_no = '{#sign_no}'
                    , in_target_bypass = '{#sign_bypass}'
                    , in_target_status = '{#sign_status}'
                WHERE
                    source_id = '{#event_id}' 
                    AND in_sign_foot = '{#sign_foot}'
            ";

            sql = sql.Replace("{#event_id}", row.next_event_id)
                .Replace("{#sign_foot}", row.next_target_foot)
                .Replace("{#sign_no}", row.sign_no)
                .Replace("{#sign_bypass}", row.sign_bypass)
                .Replace("{#sign_status}", row.sign_status);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            cfg.inn.applySQL(sql);
        }

        //更新對戰明細
        private void UpdateRepechageDetailDQ(TConfig cfg, TRpcDetail row)
        {
            string sql = "";

            sql = @"
                UPDATE
                    IN_MEETING_PEVENT_DETAIL 
                SET
                    in_sign_no = '-'
                    , in_sign_bypass = '{#sign_bypass}'
                    , in_sign_status = '{#sign_status}'
                    , in_player_org = N''
                    , in_player_name = N'消失'
                    , in_assigned = '1'
                WHERE
                    source_id = '{#event_id}' 
                    AND in_sign_foot = '{#sign_foot}'
            ";

            sql = sql.Replace("{#event_id}", row.next_event_id)
                .Replace("{#sign_foot}", row.next_sign_foot)
                .Replace("{#sign_no}", row.sign_no)
                .Replace("{#sign_bypass}", row.sign_bypass)
                .Replace("{#sign_status}", row.sign_status);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            cfg.inn.applySQL(sql);

            sql = @"
                UPDATE
                    IN_MEETING_PEVENT_DETAIL 
                SET
                    in_target_no = ''
                    , in_target_bypass = '{#sign_bypass}'
                    , in_target_status = '{#sign_status}'
                WHERE
                    source_id = '{#event_id}' 
                    AND in_sign_foot = '{#sign_foot}'
            ";

            sql = sql.Replace("{#event_id}", row.next_event_id)
                .Replace("{#sign_foot}", row.next_target_foot)
                .Replace("{#sign_no}", row.sign_no)
                .Replace("{#sign_bypass}", row.sign_bypass)
                .Replace("{#sign_status}", row.sign_status);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            cfg.inn.applySQL(sql);
        }

        private Item GetActionItemsByTeamCnt(TConfig cfg, string program_id, string in_team_count, string in_fight_id)
        {
            string sql = @"
                SELECT
	                t1.in_player_type
	                , t1.in_target_type
	                , t1.in_target_fight_id
	                , t1.in_target_sign_foot
	                , t1.in_round        AS 'act_round'
	                , t11.id             AS 'evt_id'
	                , t11.in_win_sign_no AS 'evt_win_no'
	                , t12.id             AS 'next_evt_id'
	                , t12.in_cancel      AS 'next_cancel'
                FROM 
	                In_Meeting_DrawAction t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PEVENT t11 WITH(NOLOCK)
	                ON t11.source_id = '{#program_id}'
	                AND t11.in_fight_id = t1.in_fight_id
                INNER JOIN
	                IN_MEETING_PEVENT t12 WITH(NOLOCK)
	                ON t12.source_id = '{#program_id}'
	                AND t12.in_fight_id = t1.in_target_fight_id
                WHERE
	                ISNULL(t1.in_team_count, 0) = {#in_team_count}
	                AND t1.in_fight_id = '{#in_fight_id}'
                ORDER BY
	                t1.in_round
            ";

            sql = sql.Replace("{#program_id}", program_id)
                .Replace("{#in_team_count}", MapActionTeamCount(in_team_count).ToString())
                .Replace("{#in_fight_id}", in_fight_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// 取得準決賽之前的對手清單
        /// </summary>
        private Item GetOpponentItem(TConfig cfg, string program_id, string in_sign_no, string in_round)
        {
            string sql = @"
                SELECT
                    t1.id                   AS 'event_id'
                    , t1.in_tree_id
                    , t1.in_tree_no
                    , t1.in_round
                    , t1.in_round_code
                    , t1.in_round_id
                    , t1.in_win_status
                    , t1.in_type
                    , t2.id                 AS 'detail_id'
                    , t2.in_sign_foot
                    , t2.in_sign_no
                    , t2.in_sign_status
                    , t2.in_player_name
                    , t3.id                 AS 'team_id'
                    , t3.in_current_org
                    , t3.in_short_org
                    , t3.in_name
                    , t3.in_sno
                    , t3.in_section_no
                    , t3.in_weight_message
                FROM
                    IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
                    IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK)
                    ON t2.source_id = t1.id
                LEFT OUTER JOIN
                    IN_MEETING_PTEAM t3 WITH(NOLOCK)
                    ON t3.source_id = t1.source_id
                    AND t3.in_sign_no = t2.in_sign_no
                WHERE
                    t1.source_id = '{#program_id}'
                    AND t1.in_tree_name = 'main'
                    AND t1.in_round = {#in_round}
                    AND t2.in_target_no = '{#in_target_no}'
                ORDER BY
                    t1.in_round
            ";

            sql = sql.Replace("{#program_id}", program_id)
                .Replace("{#in_round}", in_round)
                .Replace("{#in_target_no}", in_sign_no);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        private int MapActionTeamCount(string in_team_count)
        {
            var cnt = GetIntVal(in_team_count);
            if (cnt <= 4) return 4;
            else if (cnt <= 8) return 8;
            else if (cnt <= 16) return 16;
            else if (cnt <= 32) return 32;
            else if (cnt <= 64) return 64;
            else return 128;
        }

        private class TRpcDetail
        {
            public string in_tree_id { get; set; }
            public string in_tree_no { get; set; }
            public string team_id { get; set; }
            public string sign_no { get; set; }
            public string sign_status { get; set; }
            public string weight_message { get; set; }
            public string sign_bypass { get; set; }

            public string next_event_id { get; set; }
            public string next_tree_id { get; set; }
            public string next_sign_foot { get; set; }
            public string next_target_foot { get; set; }

            public string player_name { get; set; }
        }

        #endregion 四柱復活

        //刷新名次
        private void ResetPlayerRank(TConfig cfg, string program_id, string event_id, string in_sign_no, string in_sign_foot, string real_rank, string show_rank)
        {
            if (in_sign_no == "" || in_sign_no == "0") return;

            var sql = "";

            var need_clear = real_rank == "" || real_rank == "0";
            var rk1 = need_clear ? "NULL" : real_rank;
            var rk2 = need_clear ? "NULL" : show_rank;
            var eid = need_clear ? "NULL" : "'" + event_id + "'";
            var foot = need_clear ? "NULL" : "'" + in_sign_foot + "'";

            sql = "UPDATE IN_MEETING_PTEAM SET"
                + "  in_final_rank = " + rk1
                + ", in_show_rank = " + rk2
                + ", in_event = " + eid
                + ", in_foot = " + foot
                + " WHERE source_id = '" + program_id + "'"
                + " AND in_sign_no = '" + in_sign_no + "'";

            cfg.inn.applySQL(sql);
        }

        //連結到下一場
        private void ResetNextEventLink(TConfig cfg, string eid, string foot, string no)
        {
            if (eid == "" || foot == "") return;

            var sql = "";

            var sign_no = no == "" || no == "0" ? "NULL" : "'" + no + "'";
            var assigned = no == "" || no == "0" ? "NULL" : "'1'";
            var oppt_foot = foot == "1" ? "2" : "1";

            sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET"
                + "  in_sign_no = " + sign_no
                + ", in_assigned = " + assigned
                + ", in_player_name = ''"
                + " WHERE source_id = '" + eid + "'"
                + " AND in_sign_foot = " + foot;

            cfg.inn.applySQL(sql);

            sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET"
                + "  in_target_no = " + sign_no
                + " WHERE source_id = '" + eid + "'"
                + " AND in_sign_foot = " + oppt_foot;

            cfg.inn.applySQL(sql);
        }

        private TRow GetEventRow(TConfig cfg, Item itmEvent, Item itmDetailW, Item itmDetailL, WinStatus status)
        {
            var row = new TRow();
            //場次
            row.pid = itmEvent.getProperty("source_id", "");
            row.eid = itmEvent.getProperty("id", "");
            row.in_type = itmEvent.getProperty("in_type", "");
            row.w_eid = itmEvent.getProperty("in_next_win", "");
            row.w_foot = itmEvent.getProperty("in_next_foot_win", "");
            row.l_eid = itmEvent.getProperty("in_next_lose", "");
            row.l_foot = itmEvent.getProperty("in_next_foot_lose", "");

            //勝者
            row.w_id = itmDetailW.getProperty("id", "");
            row.w_no = itmDetailW.getProperty("in_sign_no", "");
            row.w_ft = itmDetailW.getProperty("in_sign_foot", "");
            //敗者
            row.l_id = itmDetailL.getProperty("id", "");
            row.l_no = itmDetailL.getProperty("in_sign_no", "");
            row.l_ft = itmDetailL.getProperty("in_sign_foot", "");

            //原勝出時間
            row.old_win_time = itmEvent.getProperty("in_win_time", "");

            //新勝出時間 (不覆蓋)
            row.new_win_time = row.old_win_time == ""
                ? DateTime.Now.AddHours(-8).ToString("yyyy-MM-ddTHH:mm:ss")
                : Convert.ToDateTime(row.old_win_time).ToString("yyyy-MM-ddTHH:mm:ss");

            //新勝出狀態
            row.new_win_status = WinStatusWord(status);

            return row;
        }

        #region 賽會參數

        private TVariable MapVariable(TConfig cfg)
        {
            var map = new TVariable
            {
                need_rank34 = "0",
                need_rank55 = "",
                need_rank56 = "0",
                need_rank77 = "",
                need_rank78 = "0",
            };

            var items = GetMtVariableItems(cfg);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var in_key = item.getProperty("in_key", "");
                var in_value = item.getProperty("in_value", "");
                switch (in_key)
                {
                    case "need_rank34":
                        map.need_rank34 = in_value;
                        break;

                    case "need_rank55":
                        map.need_rank55 = in_value;
                        break;

                    case "need_rank56":
                        map.need_rank56 = in_value;
                        break;

                    case "need_rank77":
                        map.need_rank77 = in_value;
                        break;

                    case "need_rank78":
                        map.need_rank78 = in_value;
                        break;
                }
            }
            return map;
        }

        private Item GetMtVariableItems(TConfig cfg)
        {
            var sql = @"
                SELECT 
	                in_key
	                , in_value
                FROM 
	                IN_MEETING_VARIABLE WITH(NOLOCK) 
                WHERE
	                source_id = '{#meeting_id}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            return cfg.inn.applySQL(sql);
        }

        private class TVariable
        {
            public string need_rank34 { get; set; }
            public string need_rank55 { get; set; }
            public string need_rank56 { get; set; }
            public string need_rank77 { get; set; }
            public string need_rank78 { get; set; }
        }

        #endregion 賽會參數

        private class TRow
        {
            public string pid { get; set; }
            public string eid { get; set; }
            public string in_type { get; set; }
            public string w_eid { get; set; }
            public string w_foot { get; set; }
            public string l_eid { get; set; }
            public string l_foot { get; set; }

            public string w_id { get; set; }
            public string w_no { get; set; }
            public string w_ft { get; set; }

            public string l_id { get; set; }
            public string l_no { get; set; }
            public string l_ft { get; set; }
            public string old_win_time { get; set; }
            public string new_win_time { get; set; }
            public string new_win_status { get; set; }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }

            public string meeting_id { get; set; }
            public string program_id { get; set; }
            public string mode { get; set; }
            public string scene { get; set; }

            public Item itmProgram { get; set; }
            public string in_name { get; set; }
            public string in_fight_time { get; set; }
            public string in_team_count { get; set; }
            public string in_rank_count { get; set; }

            public int team_count { get; set; }
            public int rank_count { get; set; }
            public int fight_time { get; set; }

            public TVariable variable_map { get; set; }

            public bool can_check_next { get; set; }

            public bool save_log { get; set; }

            public void WgtLog(string msg)
            {
                if (save_log)
                {
                    CCO.Utilities.WriteDebug("[Judo]Analysis.ScoreLink", msg + " - " + in_name);
                }
            }

            public void RpcLog(string msg)
            {
                if (save_log)
                {
                    CCO.Utilities.WriteDebug("[Judo]Analysis.Repechage", msg + " - " + in_name);
                }
            }
        }

        private class TAct
        {
            public string event_id { get; set; }
            public string detail_id { get; set; }
            public string points_type { get; set; }

            public Item itmEvent { get; set; }
            public Item itmDetail { get; set; }
            public Item itmOpponent { get; set; }
        }

        private class TRank
        {
            public string w_real_rank { get; set; }
            public string w_show_rank { get; set; }

            public string l_real_rank { get; set; }
            public string l_show_rank { get; set; }

            public bool need_update { get; set; }
        }

        private string WinStatusWord(WinStatus e)
        {
            switch (e)
            {
                case WinStatus.BYPASS: return "bypass";
                case WinStatus.CANCEL: return "cancel";
                case WinStatus.FIGHT: return "fight";
                case WinStatus.NOFIGHT: return "nofight";
                case WinStatus.ROLLCALL: return "rollcall";
                default: return "";
            }
        }

        public enum WinStatus
        {
            EMPTY = 0,
            BYPASS = 100,
            CANCEL = 200,
            FIGHT = 300,
            NOFIGHT = 400,
            ROLLCALL = 500,
        }

        /// <summary>
        /// 取得名次資訊
        /// </summary>
        private TRank GetRankInfo(TConfig cfg, Item itmEvent)
        {
            var in_tree_rank = itmEvent.getProperty("in_tree_rank", "");
            var in_tree_rank_ns = itmEvent.getProperty("in_tree_rank_ns", "");
            var in_tree_rank_nss = itmEvent.getProperty("in_tree_rank_nss", "");

            if (in_tree_rank == "" || !in_tree_rank_ns.Contains(","))
            {
                return new TRank { need_update = false };
            }

            if (in_tree_rank_nss == "")
            {
                in_tree_rank_nss = in_tree_rank_ns;
            }

            if (in_tree_rank_ns == "3,5")
            {
                var r3 = "3";
                var r5 = "5";
                if (cfg.variable_map.need_rank55 == "0") r5 = "0";
                in_tree_rank_ns = r3 + "," + r5;
                in_tree_rank_nss = in_tree_rank_ns;
            }
            else if (in_tree_rank_ns == "5,6")
            {
                var r5 = "5";
                var r6 = "6";
                if (cfg.variable_map.need_rank56 == "2" && cfg.rank_count == 7)
                {
                    r6 = "0";//七人才打，清空第六名
                }
                in_tree_rank_ns = r5 + "," + r6;
                in_tree_rank_nss = in_tree_rank_ns;
            }
            else if (in_tree_rank_ns == "5,7")
            {
                var r5 = "5";
                var r7 = "7";
                if (cfg.variable_map.need_rank55 == "0") r5 = "0";
                if (cfg.variable_map.need_rank77 == "0") r7 = "0";
                in_tree_rank_ns = r5 + "," + r7;
                in_tree_rank_nss = in_tree_rank_ns;
            }
            else if (in_tree_rank_ns == "0,7")
            {
                var r5 = "0";
                var r7 = "7";
                if (cfg.variable_map.need_rank55 == "0") r5 = "0";
                if (cfg.variable_map.need_rank77 == "0") r7 = "0";
                in_tree_rank_ns = r5 + "," + r7;
                in_tree_rank_nss = in_tree_rank_ns;
            }
            else if (in_tree_rank_ns == "7,8")
            {
                var r7 = "7";
                var r8 = "8";
                if (cfg.variable_map.need_rank78 == "2" && cfg.rank_count == 9)
                {
                    r8 = "0";//九人才打，清空第八名
                }
                in_tree_rank_ns = r7 + "," + r8;
                in_tree_rank_nss = in_tree_rank_ns;
            }

            var need_update_rank = false;
            var real_ranks = GetRankList(in_tree_rank_ns, "0,0");
            var show_ranks = GetRankList(in_tree_rank_nss, "0,0");

            if (real_ranks.Count == 2)
            {
                need_update_rank = true;
            }

            return new TRank
            {
                w_real_rank = real_ranks[0],
                l_real_rank = real_ranks[1],
                w_show_rank = show_ranks[0],
                l_show_rank = show_ranks[1],
                need_update = need_update_rank,
            };
        }

        /// <summary>
        /// 取得名次陣列
        /// </summary>
        private List<string> GetRankList(string value, string ranks)
        {
            var arr = string.IsNullOrEmpty(value)
                ? ranks.Split(new char[] { ',' }, StringSplitOptions.None)
                : value.Split(new char[] { ',' }, StringSplitOptions.None);

            return arr.ToList();
        }

        private decimal GetDcmVal(string value)
        {
            if (value == "") return 0;

            decimal result = 0;
            decimal.TryParse(value, out result);
            return result;
        }

        private int GetIntVal(string value, int def = 0)
        {
            int result = def;
            int.TryParse(value, out result);
            return result;
        }
    }
}