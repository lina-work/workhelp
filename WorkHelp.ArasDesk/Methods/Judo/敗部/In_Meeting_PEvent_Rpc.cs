﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;
using Spire.Doc;

namespace WorkHelp.ArasDesk.Methods.Judo.敗部
{
    internal class In_Meeting_PEvent_Rpc : Item
    {
        public In_Meeting_PEvent_Rpc(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 敗部
                日誌: 
                    - 2024-11-11: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "In_Meeting_PEvent_Rpc";

            var itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                meeting_id = itmR.getProperty("meeting_id", ""),
                in_fight_day = itmR.getProperty("in_fight_day", ""),
                scene = itmR.getProperty("scene", ""),
            };

            cfg.meeting_id = "95C1607DDA434EFD9BE95FDC042A49D2";
            cfg.in_fight_day = "2024-11-11";

            switch (cfg.scene)
            {
                case "page":
                    Page(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void Page(TConfig cfg, Item itmReturn)
        {
            Item itmDays = GetDays(cfg);
            AppendMeeting(cfg, itmReturn);
            AppendDayMenu(cfg, itmDays, itmReturn);

            var items = GetM008Items(cfg);
            var rows = MapRows(cfg, items);
            var contents = GetTableContents(cfg, rows);
            itmReturn.setProperty("inn_table", contents);
        }

        private Item GetDays(TConfig cfg)
        {
            string sql = "SELECT DISTINCT in_date_key FROM IN_MEETING_ALLOCATION WITH(NOLOCK) WHERE in_meeting = '" + cfg.meeting_id + "' ORDER BY in_date_key";
            Item itmDays = cfg.inn.applySQL(sql);
            return itmDays;
        }

        private void AppendMeeting(TConfig cfg, Item itmReturn)
        {
            cfg.itmMeeting = cfg.inn.applySQL("SELECT id, in_title FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            cfg.mt_title = cfg.itmMeeting.getProperty("in_title", "");
        }

        private void AppendDayMenu(TConfig cfg, Item itmDays, Item itmReturn)
        {
            int count = itmDays.getItemCount();

            for (int i = 0; i < count; i++)
            {
                var itmDay = itmDays.getItemByIndex(i);
                string in_date_key = itmDay.getProperty("in_date_key", "");

                itmDay.setType("inn_date");
                itmDay.setProperty("value", in_date_key);
                itmDay.setProperty("label", in_date_key);
                itmReturn.addRelationship(itmDay);
            }

            if (cfg.in_fight_day == "" && count > 0)
            {
                cfg.in_fight_day = itmDays.getItemByIndex(0).getProperty("in_date_key", "");
                itmReturn.setProperty("in_fight_day", cfg.in_fight_day);
            }
        }

        private List<TRow> MapRows(TConfig cfg, Item items)
        {
            var map = new Dictionary<string, TRow>();
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var program_id = item.getProperty("program_id", "");
                var in_fight_id = item.getProperty("in_fight_id", "");
                if (!map.ContainsKey(program_id))
                {
                    map.Add(program_id, new TRow 
                    {
                        id = program_id,
                        in_l1 = item.getProperty("in_l1", ""),
                        in_l2 = item.getProperty("in_l2", ""),
                        in_l3 = item.getProperty("in_l3", ""),
                    });
                }
                var sect = map[program_id];
                switch(in_fight_id)
                {
                    case "M008-01":
                        sect.evt01 = MapEvt(cfg, item);
                        break;
                    case "M008-02":
                        sect.evt02 = MapEvt(cfg, item);
                        break;
                    case "M008-03":
                        sect.evt03 = MapEvt(cfg, item);
                        break;
                    case "M008-04":
                        sect.evt04 = MapEvt(cfg, item);
                        break;
                }
            }
            return map.Values.ToList(); ;
        }

        private string GetTableContents(TConfig cfg, List<TRow> rows)
        {
            var head = new StringBuilder();
            var body = new StringBuilder();

            var headCss = "mailbox-subject text-center bg-primary";
            var bodyCss = "mailbox-subject text-center";

            head.Append("<tr>");
            head.Append("  <th class='" + headCss + "' data-width='20%'>競賽項目</th>");
            head.Append("  <th class='" + headCss + "' data-width='20%'>競賽組別</th>");
            head.Append("  <th class='" + headCss + "' data-width='20%'>競賽量級</th>");
            head.Append("  <th class='" + headCss + "' data-width='10%'>八強<BR>第一場</th>");
            head.Append("  <th class='" + headCss + "' data-width='10%'>八強<BR>第二場</th>");
            head.Append("  <th class='" + headCss + "' data-width='10%'>八強<BR>第三場</th>");
            head.Append("  <th class='" + headCss + "' data-width='10%'>八強<BR>第四場</th>");
            head.Append("</tr>");

            for (var i = 0; i < rows.Count; i++)
            {
                var x = rows[i];
                body.Append("  <tr>");
                body.Append("  <td class='" + bodyCss + "'>" + x.in_l1 + "</td>");
                body.Append("  <td class='" + bodyCss + "'>" + x.in_l2 + "</td>");
                body.Append("  <td class='" + bodyCss + "'>" + x.in_l3 + "</td>");
                body.Append("  <td class='" + bodyCss + "'>" + GetEventCell(cfg, x, x.evt01) + "</td>");
                body.Append("  <td class='" + bodyCss + "'>" + GetEventCell(cfg, x, x.evt02) + "</td>");
                body.Append("  <td class='" + bodyCss + "'>" + GetEventCell(cfg, x, x.evt03) + "</td>");
                body.Append("  <td class='" + bodyCss + "'>" + GetEventCell(cfg, x, x.evt04) + "</td>");
                body.Append("</tr>");
            }


            var table_name = "data-table";
            var builder = new StringBuilder();
            builder.Append("<div class='box-body tablenobg' style='position: relative; margin-bottom: 10px'>");
            builder.AppendLine(GetTableAttribute(table_name));
            builder.Append("<thead>");
            builder.Append(head);
            builder.Append("</thead>");
            builder.Append("<tbody>");
            builder.Append(body);
            builder.Append("</tbody>");
            builder.AppendLine("</table>");
            builder.Append("</div>");
            builder.AppendLine("<script>");
            builder.AppendLine("$('#" + table_name + "').bootstrapTable();");
            builder.AppendLine("if ($(window).width() <= 768) { $('#" + table_name + "').bootstrapTable('toggleView'); }");
            builder.AppendLine("</script>");
            return builder.ToString();
        }


        private string GetEventCell(TConfig cfg, TRow sect, TEvt evt)
        {
            if (evt == null || evt.in_tree_no == null) return "";
            return evt.text + "<BR>" + evt.in_win_status;
        }

        private string GetTableAttribute(string table_name)
        {
            return "<table id='" + table_name + "'"
                + " class='table table-hover table-striped'"
                + " data-toggle='table'"
                + " data-show-toggle='false'"
                + " data-show-columns='false'"
                + " data-search-align='left'"
                + " data-sort-stable='true'"
                + " data-search='false'"
                + ">";
        }
        private TEvt MapEvt(TConfig cfg, Item item)
        {
            var x = new TEvt();
            x.id = item.getProperty("event_id", "");
            x.in_fight_id = item.getProperty("in_fight_id", "");
            x.in_tree_no = item.getProperty("in_tree_no", "");
            x.site_code = item.getProperty("site_code", "");
            x.site_code_en = item.getProperty("site_code_en", "");
            x.in_win_status = item.getProperty("in_win_status", "");
            x.text = x.site_code_en + x.in_tree_no;
            return x;
        }

        private class TRow
        {
            public string id { get; set; }
            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }
            public TEvt evt01 { get; set; }
            public TEvt evt02 { get; set; }
            public TEvt evt03 { get; set; }
            public TEvt evt04 { get; set; }
        }

        private class TEvt
        {
            public string id { get; set; }
            public string status { get; set; }
            public string site_code { get; set; }
            public string site_code_en { get; set; }
            public string in_tree_no { get; set; }
            public string in_fight_id { get; set; }
            public string in_win_status { get; set; }
            public string text { get; set; }
        }

        private Item GetM008Items(TConfig cfg)
        {
            var sql = @"
                SELECT 
	                t1.id AS 'program_id'
	                , t1.in_l1
	                , t1.in_l2
	                , t1.in_l3
	                , t1.in_short_name
	                , t2.in_tree_name
	                , t3.in_cod		AS 'site_code'
	                , t3.in_code_en		AS 'site_code_en'
	                , t2.id AS 'event_id'
	                , t2.in_tree_no
	                , t2.in_fight_id
	                , t2.in_win_status
                FROM
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PEVENT t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                INNER JOIN
	                IN_MEETING_SITE t3 WITH(NOLOCK)
	                ON t3.id = t2.in_site
                WHERE
	                t1.in_meeting = '{#meeting_id}'
	                AND t1.in_fight_day = '{#in_fight_day}'
	                AND t2.in_fight_id IN ('M008-01', 'M008-02', 'M008-03', 'M008-04')
	                AND ISNULL(t2.in_tree_no, 0) > 0
                ORDER BY
	                t1.in_fight_day
	                , t1.in_sort_order
	                , t3.in_code
	                , t2.in_tree_no
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_fight_day}", cfg.in_fight_day);

            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string meeting_id { get; set; }
            public string in_fight_day { get; set; }
            public string scene { get; set; }

            public Item itmMeeting { get; set; }
            public string mt_title { get; set; }
        }

    }
}
