﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;
using Spire.Doc;
using System.Security.Policy;

namespace WorkHelp.ArasDesk.Methods.Judo.U15
{
    internal class In_Meeting_User_Check : Item
    {
        public In_Meeting_User_Check(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: U15 報名記錄檢查
                日誌: 
                    - 2025-01-22: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "In_Meeting_User_Check";

            var itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                in_date_s = itmR.getProperty("in_date_s", ""),
                in_date_e = itmR.getProperty("in_date_e", ""),
                scene = itmR.getProperty("scene", ""),
            };
            cfg.meeting_id = "EE3C73272A314C239CC9040CE72CE671";
            switch (cfg.scene)
            {
                case "u15-query-2025":
                    U15Query2025(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void U15Query2025(TConfig cfg, Item itmReturn)
        {
            var items = GetU15Query2025Items(cfg);
            var rows = MapU15Rows(cfg, items);
            var table = RenderU15Query2025Table(cfg, rows);
        }

        private string RenderU15Query2025Table(TConfig cfg, List<TU15Row> rows)
        {
            var headCss = "class='mailbox-subject text-center bg-primary'";
            var body = new StringBuilder();
            body.Append("<th " + headCss + " data-width='4%'>No.</th>");
            body.Append("<th " + headCss + " data-width='10%'>姓名</th>");
            body.Append("<th " + headCss + " data-width='10%'>單位</th>");
            body.Append("<th " + headCss + " data-width='10%'>競賽組別</th>");
            body.Append("<th " + headCss + " data-width='10%'>競賽項目</th>");
            body.Append("<th " + headCss + " data-width='24%'>競賽量級</th>");

            for (var i = 0; i < rows.Count; i++)
            {
                var x = rows[i];
                for (var j = 0; j < x.children.Count; j++)
                {
                    var child = x.children[j];
                    body.Append("<tr>");
                    if (j == 0)
                    {
                        body.Append("  <td class='text-center'> " + (i + 1) + " </td>");
                        body.Append("  <td class='text-center'> " + x.in_name + " </td>");
                        body.Append("  <td class='text-center'> " + x.in_short_org + " </td>");
                        body.Append("  <td class='text-center'> " + x.in_l1 + " </td>");
                        body.Append("  <td class='text-center'> " + x.in_l2 + " </td>");
                        body.Append("  <td class='text-center'> " + x.in_l3 + " </td>");
                    }
                    else
                    {
                        body.Append("  <td class='text-center'> &nbsp; </td>");
                        body.Append("  <td class='text-center'> &nbsp; </td>");
                        body.Append("  <td class='text-center'> &nbsp; </td>");
                        body.Append("  <td class='text-center'> &nbsp; </td>");
                        body.Append("  <td class='text-center'> &nbsp; </td>");
                        body.Append("  <td class='text-center'> &nbsp; </td>");
                    }
                    body.Append("</tr>");
                }
            }

            var table_name = "data-table";

            var builder = new StringBuilder();
            builder.Append("<div class='box-body tablenobg' style='position: relative; margin-bottom: 10px'>");
            builder.AppendLine(GetTableAttribute(table_name));
            builder.Append("<tbody>");
            builder.Append(body);
            builder.Append("</tbody>");
            builder.AppendLine("</table>");
            builder.Append("</div>");
            builder.AppendLine("<script>");
            builder.AppendLine("$('#" + table_name + "').bootstrapTable();");
            builder.AppendLine("if ($(window).width() <= 768) { $('#" + table_name + "').bootstrapTable('toggleView'); }");
            builder.AppendLine("</script>");

            return builder.ToString();
        }

        private List<TU15Row> MapU15Rows(TConfig cfg, Item items)
        {
            var map = new Dictionary<string, TU15Row>();
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var key = item.getProperty("in_sno", "");
                if (!map.ContainsKey(key))
                {
                    var x = new TU15Row
                    {
                        id = item.getProperty("id", ""),
                        in_sno = item.getProperty("in_sno", ""),
                        in_name = item.getProperty("in_name", ""),
                        in_short_org = item.getProperty("in_short_org", ""),
                        in_l1 = item.getProperty("in_l1", ""),
                        in_l2 = item.getProperty("in_l2", ""),
                        in_l3 = item.getProperty("in_l3", ""),
                        in_index = item.getProperty("in_index", ""),
                        children = new List<TU15Sub>(),
                    };
                    map.Add(key, x);
                }
                var row = map[key];
                row.children.Add(new TU15Sub
                {
                    old_name = item.getProperty("old_name", ""),
                    old_mt_title = item.getProperty("old_mt_title", ""),
                    old_l1 = item.getProperty("old_l1", ""),
                    old_l2 = item.getProperty("old_l2", ""),
                    old_l3 = item.getProperty("old_l3", ""),
                    old_weight = item.getProperty("old_weight", ""),
                });
            }
            return map.Values.ToList();
        }

        private Item GetU15Query2025Items(TConfig cfg)
        {
            var sql = @"
                SELECT 
	                t1.id
	                , UPPER(t1.in_sno) AS 'in_sno'
	                , t1.in_name
	                , t1.in_short_org
	                , t1.in_l1
	                , t1.in_l2
	                , t1.in_l3
	                , t1.in_index
	                , t2.*
                FROM 
	                IN_MEETING_USER t1 WITH(NOLOCK)
                LEFT JOIN 
                (
	                SELECT 
		                t12.in_name		AS 'old_name'
		                , t12.in_sno	AS 'old_sno'
		                , t11.in_title	AS 'old_mt_title'
		                , t11.in_date_s AS 'old_mt_date_s'
		                , t12.in_l1		AS 'old_l1'
		                , t12.in_l2		AS 'old_l2'
		                , t12.in_l3		AS 'old_l3'
		                , t12.in_weight	AS 'old_weight'
	                FROM 
		                IN_MEETING_USER t12 WITH(NOLOCK)
	                INNER JOIN 
		                IN_MEETING t11 WITH(NOLOCK) 
		                ON t11.id = t12.source_id
	                WHERE 
		                t11.id <> '{#meeting_id}'
		                AND DATEADD(HOUR, 8, t11.in_date_s) >= '{#in_date_s}'
		                AND DATEADD(HOUR, 8, t11.in_date_s) <= '{#in_date_e}'
		                AND t12.in_l1 NOT IN ('隊職員', '團體組')
                )
                t2 ON t2.old_sno = t1.in_sno
                LEFT JOIN
	                VU_MEETING_SVY_L1 t31
	                ON t31.source_id = t1.source_id
	                AND t31.in_value = t1.in_l1
                LEFT JOIN
	                VU_MEETING_SVY_L2 t32
	                ON t32.source_id = t1.source_id
	                AND t32.in_filter = t1.in_l1
	                AND t32.in_value = t1.in_l2
                LEFT JOIN
	                VU_MEETING_SVY_L3 t33
	                ON t33.source_id = t1.source_id
	                AND t33.in_grand_filter = t1.in_l1
	                AND t33.in_filter = t1.in_l2
	                AND t33.in_value = t1.in_l3
                WHERE 
	                t1.source_id = '{#meeting_id}' 
	                AND t1.in_l1 <> '隊職員'
                ORDER BY
	                t31.sort_order
	                , t32.sort_order
	                , t33.sort_order
	                , t2.old_mt_date_s
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_date_s}", cfg.in_date_s)
                .Replace("{#in_date_e}", cfg.in_date_e);

            return cfg.inn.applySQL(sql);
        }

        private string GetTableAttribute(string table_name)
        {
            return "<table id='" + table_name + "'"
                + " class='table table-hover table-striped'"
                + " data-toggle='table'"
                + " data-show-toggle='false'"
                + " data-show-columns='false'"
                + " data-search-align='left'"
                + " data-sort-stable='true'"
                + " data-search='false'"
                + ">";
        }

        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string meeting_id { get; set; }
            public string in_date_s { get; set; }
            public string in_date_e { get; set; }
            public string scene { get; set; }
        }

        private class TU15Row
        {
            public string id { get; set; }
            public string in_sno { get; set; }
            public string in_name { get; set; }
            public string in_short_org { get; set; }
            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }
            public string in_index { get; set; }
            public List<TU15Sub> children { get; set; }
        }

        private class TU15Sub
        {
            public string old_name { get; set; }
            public string old_mt_title { get; set; }
            public string old_mt_date_s { get; set; }
            public string old_l1 { get; set; }
            public string old_l2 { get; set; }
            public string old_l3 { get; set; }
            public string old_weight { get; set; }
        }
    }
}
