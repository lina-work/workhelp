﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Judo.Register
{
    public class In_Meeting_InsUser : Item
    {
        public In_Meeting_InsUser(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
    目的: excel匯入與會者
    說明: 快速建立與會者資訊
    位置: In_Meeting_User_Import.html
    日期: 
        - 2023/03/20 不產生 Resume (lina)
        - 2021/09/22 個人帳號補刀 (lina)
        - 2021/03/10 創建 (Alan)
*/

            System.Diagnostics.Debugger.Break();

            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]In_Meeting_InsUser";

            Item itmR = this;

            //登入者 Resume
            Item itmLogin = inn.newItem("In_Resume", "get");
            itmLogin.setProperty("in_user_id", inn.getUserID());
            itmLogin = itmLogin.apply();

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                strIndId = inn.getUserAliases(),

                login_resume_id = itmLogin.getProperty("id", ""),
                login_resume_current_org = itmLogin.getProperty("in_current_org", ""),
                login_resume_group = itmLogin.getProperty("in_group", ""),
                in_creator = itmLogin.getProperty("in_name", ""),
                in_sno = itmR.getProperty("in_sno", ""),

                meeting_id = itmR.getProperty("meeting_id", ""),
                mode = itmR.getProperty("mode", ""),
                isPayAdd = itmR.getProperty("isPayAdd", ""),
                OnlyInsMUser = itmR.getProperty("OnlyInsMUser", ""),

                mtName = "IN_MEETING",
                muName = "IN_MEETING_USER",
                msName = "IN_MEETING_SURVEYS",
                mtProperty = "in_meeting",
                svName = "IN_SURVEY",
                soName = "IN_SURVEY_OPTION",
            };

            if (cfg.mode == "cla")
            {
                cfg.mtName = "IN_CLA_MEETING";
                cfg.muName = "IN_CLA_MEETING_USER";
                cfg.msName = "IN_CLA_MEETING_SURVEYS";
                cfg.mtProperty = "in_cla_meeting";
                cfg.svName = "IN_CLA_SURVEY";
                cfg.soName = "IN_CLA_SURVEY_OPTION";
            }

            if (cfg.meeting_id == "")
            {
                throw new Exception("活動 id 不可空白");
            }

            //新增與會者
            if (cfg.OnlyInsMUser == "1")
            {
                OnlyInsMUser(cfg, itmR);
            }
            else
            {
                InsUserFields(cfg, itmR);
            }

            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

            return itmR;

        }

        #region 只新增與會者

        private void OnlyInsMUser(TConfig cfg, Item itmR)
        {
            var more_svy_list = GetMoreMeetingSurveyList(cfg);

            //與會者資訊
            Item applicant = NewMUser(cfg, more_svy_list, itmR);
            //建立與會者
            applicant = applicant.apply("add");

            string muid = applicant.getID();
            string sql = "DELETE FROM IN_MEETING_RESUME WHERE in_user = '" + muid + "'";
            cfg.inn.applySQL(sql);
        }

        private Item NewMUser(TConfig cfg, List<Item> more_svy_list,  Item item)
        {
            var applicant = cfg.inn.newItem("In_Meeting_User");

            //活動 id
            applicant.setProperty("source_id", cfg.meeting_id);

            //所屬單位
            applicant.setProperty("in_current_org", item.getProperty("in_current_org", ""));

            //團體會員編號與單位簡稱
            SetStuffB1AndShortOrg(cfg, applicant, item);

            //姓名
            applicant.setProperty("in_name", item.getProperty("in_name", ""));

            //身分證號
            applicant.setProperty("in_sno", item.getProperty("in_sno", ""));

            //性別
            applicant.setProperty("in_gender", GetGender(cfg, item));

            //西元生日
            applicant.getProperty("in_birth", GetBirthDay(cfg, item));

            //電子信箱
            applicant.setProperty("in_email", "na@na.n");

            //手機號碼
            applicant.setProperty("in_tel", "");

            //協助報名者姓名
            applicant.setProperty("in_creator", "lwu001");

            //協助報名者身分號
            applicant.setProperty("in_creator_sno", "lwu001");

            //所屬群組
            applicant.setProperty("in_group", GetGroup(cfg, item)); //F103277376測試者


            //預設身分均為選手
            applicant.setProperty("in_role", "player");

            //正取
            applicant.setProperty("in_note_state", "official");
            applicant.setProperty("in_note_state_pre", "official");

            //組別
            applicant.setProperty("in_gameunit", GetGameUnit(cfg, item));

            //組名
            applicant.setProperty("in_section_name", GetSectionName(cfg, item));

            //報名費用
            applicant.setProperty("in_expense", "0");

            //競賽項目
            applicant.setProperty("in_l1", item.getProperty("in_l1", ""));

            //競賽組別
            applicant.setProperty("in_l2", item.getProperty("in_l2", ""));

            //競賽分級
            applicant.setProperty("in_l3", item.getProperty("in_l3", ""));

            //序號
            Item itmIndex = MaxIndexItem(cfg, applicant);
            applicant.setProperty("in_index", itmIndex.getProperty("new_idx", ""));

            //非實名制
            applicant.setProperty("in_mail", item.getProperty("in_sno", ""));

            //報名時間
            applicant.setProperty("in_regdate", System.DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss"));

            //繳費單號
            applicant.setProperty("in_paynumber", "");

            //更多的問項
            for(var i = 0; i< more_svy_list.Count;i++)
            {
                var itmSvy = more_svy_list[i];
                var in_property = itmSvy.getProperty("in_property", "");
                applicant.setProperty(in_property, item.getProperty(in_property, ""));
            }

            return applicant;
        }

        private List<Item> GetMoreMeetingSurveyList(TConfig cfg)
        {
            var sql = @"
                SELECT 
	                in_questions
	                , in_property
                FROM
	                IN_MEETING_SURVEYS t1 WITH(NOLOCK)
                INNER JOIN
	                IN_SURVEY t2 WITH(NOLOCK)
	                ON t2.id = t1.related_id
                WHERE
	                t1.source_id = '{#meeting_id}'
	                AND t1.in_surveytype = '1'
	                AND t2.in_property NOT IN 
	                (
		                'source_id'
		                , 'in_current_org'
		                , 'in_short_org'
		                , 'in_stuff_b1'
		                , 'in_name'
		                , 'in_sno'
		                , 'in_gender'
		                , 'in_birth'
		                , 'in_email'
		                , 'in_creator'
		                , 'in_creator_sno'
		                , 'in_group'
		                , 'in_note_state'
		                , 'in_note_state_pre'
		                , 'in_gameunit'
		                , 'in_section_name'
		                , 'in_expense'
		                , 'in_l1'
		                , 'in_l2'
		                , 'in_l3'
		                , 'in_index'
	                )
                ORDER BY
	                t1.in_sort_order
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            var list = new List<Item>();
            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                list.Add(items.getItemByIndex(i));
            }
            return list;
        }

        private string GetGameUnit(TConfig cfg, Item item)
        {
            string in_l1 = item.getProperty("in_l1", "");
            string in_l2 = item.getProperty("in_l2", "");
            string in_l3 = item.getProperty("in_l3", "");

            in_l2 = in_l2.Replace("個-", "").Replace("團-", "");

            return in_l2;
        }

        private string GetSectionName(TConfig cfg, Item item)
        {
            string in_l1 = item.getProperty("in_l1", "");
            string in_l2 = item.getProperty("in_l2", "");
            string in_l3 = item.getProperty("in_l3", "");

            in_l2 = in_l2.Replace("個-", "").Replace("團-", "");

            return in_l1
                + "-" + in_l2
                + "-" + in_l3;
        }

        private string GetGroup(TConfig cfg, Item item)
        {
            string in_group = item.getProperty("in_group", "");
            string in_current_org = item.getProperty("in_current_org", "");
            if (in_group != "") return in_group;
            return in_current_org;
        }

        private void SetStuffB1AndShortOrg(TConfig cfg, Item applicant, Item item)
        {
            string in_stuff_b1 = item.getProperty("in_stuff_b1", "");
            string in_current_org = item.getProperty("in_current_org", "");
            string in_short_org = item.getProperty("in_short_org", "");

            if (in_stuff_b1 == "")
            {
                in_stuff_b1 = GetStffB1(in_current_org);
            }

            if (in_stuff_b1 != "")
            {
                applicant.setProperty("in_stuff_b1", in_stuff_b1);
            }

            if (in_short_org == "")
            {
                if (in_stuff_b1 != "")
                {
                    string sql = "SELECT TOP 1 * FROM IN_ORG_MAP WITH(NOLOCK) WHERE in_stuff_b1 = '" + in_stuff_b1 + "'";
                    Item itmOrgMap = cfg.inn.applySQL(sql);
                    if (!itmOrgMap.isError() && itmOrgMap.getResult() != "")
                    {
                        in_short_org = itmOrgMap.getProperty("in_short_org", "");
                    }
                }

                if (in_short_org == "")
                {
                    in_short_org = in_current_org;
                }

                applicant.setProperty("in_short_org", in_short_org);
            }
        }

        private string GetBirthDay(TConfig cfg, Item item)
        {
            string in_birth = item.getProperty("in_birth", "");
            if (in_birth == "") return "1990-01-01";

            var dt = GetDtmVal(in_birth);
            var result = dt.ToString("yyyy-MM-dd");
            return result;
        }

        private string GetGender(TConfig cfg, Item item)
        {
            string in_l2 = item.getProperty("in_l2", "");
            string in_gender = item.getProperty("in_gender", "");
            if (in_gender != "") return in_gender;

            return GetGender(cfg, in_l2);
        }

        private string GetGender(TConfig cfg, string value)
        {
            if (value.Contains("男")) return "男";
            if (value.Contains("女")) return "女";
            return "";
        }

        //取得該組當前最大序號
        private Item MaxIndexItem(TConfig cfg, Item item)
        {
            Item itmResult = cfg.inn.newItem();

            string in_l1 = item.getProperty("in_l1", "");
            string in_l2 = item.getProperty("in_l2", "");
            string in_l3 = item.getProperty("in_l3", "");
            string in_l4 = item.getProperty("in_l4", "");

            string sql = "SELECT MAX(in_index) AS 'max_idx' FROM In_Meeting_User WITH(NOLOCK)"
                + " WHERE source_id = '" + cfg.meeting_id + "'"
                + " AND in_l1 = N'" + in_l1 + "'";

            Item itmIndex = cfg.inn.applySQL(sql);
            if (!itmIndex.isError() && itmIndex.getResult() != "")
            {
                var max_idx = itmIndex.getProperty("max_idx", "1");
                var new_idx = GetInt(max_idx.TrimStart('0')) + 1;
                itmResult.setProperty("new_idx", new_idx.ToString("00000"));
            }
            else
            {
                itmResult.setProperty("new_idx", "00001");
            }

            return itmResult;
        }

        private string GetStffB1(string value)
        {
            if (value == null || value.Length < 3) return "";

            string result = value.Substring(0, 3);
            bool is_digit = true;

            foreach (var c in result)
            {
                if (!char.IsDigit(c))
                {
                    is_digit = false;
                    break;
                }
            }

            if (is_digit)
            {
                return result;
            }
            else
            {
                return "";
            }
        }

        #endregion 只新增與會者

        private void InsUserFields(TConfig cfg, Item itmR)
        {
            string sql = @"
		        SELECT 
			        t2.*
		        FROM 
			        {#msName} t1 WITH(NOLOCK)
		        INNER JOIN 
			        {#svName} t2 WITH(NOLOCK) 
                    ON t2.id = t1.related_id
		        WHERE 
			        t1.source_id = '{#meeting_id}'
			        AND ISNULL(t2.in_property, '') NOT IN ('', 'in_creator', 'in_creator_sno')
		        ORDER by 
			        t1.sort_order
	        ";

            sql = sql.Replace("{#msName}", cfg.msName)
                .Replace("{#svName}", cfg.svName)
                .Replace("{#meeting_id}", cfg.meeting_id);

            Item items = cfg.inn.applySQL(sql);
            int count = items.getItemCount();

            string parameter = "";
            string in_l1 = itmR.getProperty("in_l1", "");
            string in_l2 = itmR.getProperty("in_l2", "");
            string in_l3 = itmR.getProperty("in_l3", "");
            string in_current_org = itmR.getProperty("in_current_org", "");

            string register_section_name = "";
            register_section_name += in_l1;
            if (in_l2 != "")
            {
                register_section_name += "-" + in_l2;
            }
            if (in_l3 != "")
            {
                register_section_name += "-" + in_l3;
            }
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string survey_id = item.getProperty("id", "");
                string in_property = item.getProperty("in_property", ""); //欄位屬性
                string in_questions = item.getProperty("in_questions", ""); //欄位名稱
                string in_question_type = item.getProperty("in_question_type", "");
                string in_selectoption = item.getProperty("in_selectoption", "");
                string in_request = item.getProperty("in_request", ""); //必填欄位
                string ins_val = itmR.getProperty(in_property, "").Trim();


                if (in_question_type == "date")
                {
                    DateTime DateVal = Convert.ToDateTime(ins_val);
                    ins_val = DateVal.ToString("yyyy-MM-ddTHH:mm:ss");
                }
                parameter += survey_id + "=" + ins_val + "&";
            }

            //共用
            Item mUser = cfg.inn.newItem("Method");
            mUser.setProperty("OnlyInsMUser", "0");

            string in_creator = itmR.getProperty("in_creator", "");
            string in_creator_sno = cfg.in_sno;
            if (in_creator != "")
            {
                cfg.in_creator = in_creator;
                mUser.setProperty("current_user_name_sno", cfg.in_sno);
            }
            else
            {
                mUser.setProperty("isUserId", cfg.strUserId);
                Item CurrentUser = cfg.inn.getItemById("User", cfg.strUserId);
                //用偷藏的ID取得當前登入者
                cfg.in_creator = CurrentUser.getProperty("last_name", "");//協助報名者
                in_creator_sno = CurrentUser.getProperty("login_name", "");//協助報名者帳號
            }
            mUser.setProperty("isIndId", cfg.strIndId);
            mUser.setProperty("meeting_id", cfg.meeting_id);
            mUser.setProperty("method", "register_meeting");
            mUser.setProperty("login_resume_id", cfg.login_resume_id);
            mUser.setProperty("login_resume_current_org", cfg.login_resume_current_org);
            mUser.setProperty("login_resume_group", cfg.login_resume_group);
            //團體須研究 client_user_index in_index
            mUser.setProperty("client_user_index", "1");
            mUser.setProperty("in_index", "");

            if (cfg.mode == "cla")
            {
                parameter += "22531BA9D5284BBF96C8E648F8E257E9=";
                mUser.setProperty("parameter", parameter);
                mUser.setProperty("agent_id", "x");
                mUser.setProperty("surveytype", "1a");
                mUser.setProperty("mode", cfg.mode);
                mUser.setProperty("email", cfg.in_sno);

                itmR = mUser.apply("In_Cla_Meeting_Register");
            }
            else
            {
                parameter += "8466E57C08F64D8BB9AADF99A4F5C844=";
                mUser.setProperty("parameter", parameter);
                mUser.setProperty("in_locked", "1");
                mUser.setProperty("surveytype", "1");
                mUser.setProperty("in_creator", cfg.in_creator);
                mUser.setProperty("in_locked_id", cfg.in_sno);
                mUser.setProperty("in_l1", in_l1);
                mUser.setProperty("email", cfg.in_sno + "-" + register_section_name);
                mUser.setProperty("register_section_name", register_section_name);
                //mUser.setProperty("OnlyInsMUser","1");
                itmR = mUser.apply("In_meeting_register");

                //自動產生繳費單
                if (cfg.isPayAdd == "1")
                {
                    //為了配合 In_Payment_List_Add AML 查詢，一定要把 繳費單號  更新成空白
                    //否則繳費單號一定產生失敗!!!!!!!!

                    string sql_update = @"UPDATE IN_MEETING_USER SET in_paynumber = N'' WHERE source_id = '" + cfg.meeting_id + "' AND in_paynumber IS NULL AND in_sno = '" + cfg.in_sno + "'";
                    cfg.inn.applySQL(sql_update);

                    Item itmResume = cfg.inn.applySQL("SELECT * FROM IN_RESUME WITH(NOLOCK) WHERE in_sno = '" + in_creator_sno + "'");

                    string resume_id = itmResume.getProperty("id", "");
                    string in_group = itmResume.getProperty("in_group", "");

                    // Item itmMClose = cfg.inn.newItem();
                    // itmMClose.setType("In_Meeting_GymList");
                    // itmMClose.setProperty("meeting_id", cfg.meeting_id);
                    // itmMClose.setProperty("in_group", in_group);
                    // Item itmMCloseResult = itmMClose.apply("In_Close_GymReg");

                    Item itmMPay = cfg.inn.newItem();
                    itmMPay.setType("In_Meeting_Pay");
                    itmMPay.setProperty("meeting_id", cfg.meeting_id);
                    itmMPay.setProperty("in_group", in_group);
                    itmMPay.setProperty("in_creator_sno", in_creator_sno);
                    itmMPay.setProperty("resume_id", resume_id);
                    itmMPay.setProperty("current_orgs", "," + in_current_org);
                    itmMPay.setProperty("invoice_up", ",");
                    itmMPay.setProperty("uniform_numbers", ",");
                    Item itmMPayResult = itmMPay.apply("In_Payment_List_Add2");
                }


            }
        }

        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIndId { get; set; }
            public string login_resume_id { get; set; }
            public string login_resume_current_org { get; set; }
            public string login_resume_group { get; set; }
            public string meeting_id { get; set; }
            public string mode { get; set; }
            public string in_sno { get; set; }
            public string in_creator { get; set; }
            public string isPayAdd { get; set; }
            public string OnlyInsMUser { get; set; }

            public string mtName { get; set; }
            public string muName { get; set; }
            public string msName { get; set; }
            public string mtProperty { get; set; }
            public string svName { get; set; }
            public string soName { get; set; }

            public Item itmInsUsers { get; set; }
        }

        private DateTime GetDtmVal(string value)
        {
            DateTime result = DateTime.MinValue;
            DateTime.TryParse(value, out result);
            return result;
        }

        private int GetInt(string value, int def = 0)
        {
            int result = def;
            int.TryParse(value, out result);
            return result;
        }
    }
}