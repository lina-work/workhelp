﻿using Aras.IOM;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace WorkHelp.ArasDesk.Methods.Judo.Register
{
    public class In_Meeting_Coach : Item
    {
        public In_Meeting_Coach(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "In_Meeting_Coach";

            var itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                meeting_id = itmR.getProperty("meeting_id", ""),
                in_sno = itmR.getProperty("in_sno", ""),
                in_name = itmR.getProperty("in_name", ""),
                scene = itmR.getProperty("scene", ""),
            };

            switch (cfg.scene)
            {
                case "export-word":
                    SetPhotoKey(cfg);
                    ExportWord(cfg, itmR);
                    break;

                case "verify":
                    Verify(cfg, itmR);
                    break;

                case "send-email":
                    SendEmail(cfg, itmR);
                    break;

                case "run":
                    Run(cfg, itmR);
                    break;

                case "send-notice-email":
                    SendNoticeEmail(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void SendNoticeEmail(TConfig cfg, Item itmReturn)
        {
            AppendMeeting(cfg);

            var items = GetCollegeItems(cfg);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var in_email = item.getProperty("in_email", "");
                if (in_email == "" || in_email == "na@na.n") continue;

                var subject = "柔道中正盃大專個人組賽程時間修改通知";
                var body = GetNoticeEmailBody(cfg, item);
                var in_creator = item.getProperty("in_creator", "");

                //RundSendEmailSportOpen(cfg, "straying1@gmail.com", "田純嫣", subject, body);
                //RundSendEmailSportOpen(cfg, "elsa@innosoft.com.tw", "Elsa", subject, body);
                RundSendEmailSportOpen(cfg, in_email, in_creator, subject, body);
            }
        }

        private string GetNoticeEmailBody(TConfig cfg, Item item)
        {
            var builder = new StringBuilder();
            builder.AppendLine("<!DOCTYPE html>");
            builder.AppendLine("<html>");
            builder.AppendLine("<head>");
            builder.AppendLine("    <meta name=\"viewport\" content=\"width=device-width\" />");
            builder.AppendLine("    <title></title>");
            builder.AppendLine("</head>");
            builder.AppendLine("<body>");


            builder.AppendLine("<div>");
            builder.AppendLine("  <h3>各位教練~日安</h3>");
            builder.AppendLine("  <p>113 年中正盃抽籤完成，因應賽程安排調整</p>");
            builder.AppendLine("  <p>個人組（大專甲、乙組）調整為：</p>");
            builder.AppendLine("  <p>11/12（二）16:30開始正式過磅</p>");
            builder.AppendLine("  <p>11/13（三）大專甲、乙組個人賽</p>");
            builder.AppendLine("  <p>11/14（四）大專甲、乙組團體賽</p>");
            builder.AppendLine("  <p></p>");
            builder.AppendLine("  <p>敬請大專甲、乙組各單位教練</p>");
            builder.AppendLine("  <p>知悉調整~謝謝</p>");
            builder.AppendLine("  <p></p>");
            //builder.AppendLine("  <p><a href='https://act.innosoft.com.tw/judo'>https://act.innosoft.com.tw/judo</a></p>");
            builder.AppendLine("</div>");

            builder.AppendLine("</body>");
            builder.AppendLine("</html>");

            return builder.ToString();
        }

        private Item GetCollegeItems(TConfig cfg)
        {
            var sql = @"
                SELECT DISTINCT
                   t1.in_creator
                   , t2.in_tel
                   , t2.in_email
                   , t1.in_stuff_b1
                   , t1.in_current_org
                FROM
	                IN_MEETING_USER t1 WITH(NOLOCK)
                INNER JOIN
	                IN_RESUME t2 WITH(NOLOCK)
	                ON t2.login_name = t1.in_creator_sno
                WHERE
	                t1.source_id = '{#meeting_id}'
	                AND t1.in_l1 = '個人組'
	                AND t1.in_l2 LIKE '%大專%'
                ORDER BY
	                t1.in_stuff_b1
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            return cfg.inn.applySQL(sql);
        }

        private void ExportWord(TConfig cfg, Item itmReturn)
        {
            AppendMeeting(cfg);

            var exp = ExportInfo(cfg, "template_4_coach");

            var items = GetCoachAndStaffItems(cfg);
            var map = GetCoachMap(cfg, items);
            var coaches = map.Values.ToList();

            //載入模板
            Xceed.Words.NET.DocX doc = Xceed.Words.NET.DocX.Load(exp.template_path);

            for (var i = 0; i < coaches.Count; i++)
            {
                var coach = coaches[i];
                AppendWord(cfg, coach, doc);
            }

            //移除表格樣板與多餘換行
            doc.Tables[0].Remove();
            doc.RemoveParagraphAt(0);
            doc.RemoveParagraphAt(0);

            if (!System.IO.Directory.Exists(exp.export_path))
            {
                System.IO.Directory.CreateDirectory(exp.export_path);
            }

            var xls_name = cfg.mt_title;

            xls_name = xls_name.Replace("+", "")
                            .Replace("-", "")
                            .Replace(".", "")
                            .Replace(" ", "");

            if (cfg.in_name != "") xls_name += "_" + cfg.in_name;

            xls_name = xls_name + "_教練證_" + DateTime.Now.ToString("MMdd_HHmmss");

            var ext_name = ".docx";
            var xls_file = exp.export_path + "\\" + xls_name + ext_name;
            var xls_url = xls_name + ext_name;

            doc.SaveAs(xls_file);

            itmReturn.setProperty("xls_name", xls_url);
        }

        private void AppendWord(TConfig cfg, TCoachRow coach, Xceed.Words.NET.DocX doc)
        {
            doc.InsertParagraph();
            var table = doc.InsertTable(doc.Tables[0]);

            var cert_name = "教練證";
            var work_name = "隊職員 教練";

            if (cfg.is_special_coach)
            {
                if (coach.is_m_coach && coach.is_w_coach)
                {
                    work_name = "男子隊教練、女子隊教練";
                }
                else if (coach.is_m_coach)
                {
                    work_name = "男子隊教練";
                }
                else if (coach.is_w_coach)
                {
                    work_name = "女子隊教練";
                }
            }
            else
            {
                work_name = "隊職員 教練";
            }

            if (coach.is_manager && coach.is_leader)
            {
                cert_name = "教練證、領隊證、管理證";
                work_name += "、領隊、管理";
            }
            else if (coach.is_manager)
            {
                cert_name = "教練證、管理證";
                work_name += "、管理";
            }
            else if (coach.is_leader)
            {
                cert_name = "教練證、領隊證";
                work_name += "、領隊";
            }

            var mt_size = cfg.mt_title.Length >= 32 ? 12 : 14;
            //賽事名稱
            table.Rows[1].Cells[0].Paragraphs.First().Append(cfg.mt_title).Font("標楷體").FontSize(mt_size);
            //隊職員證
            table.Rows[1].Cells[0].Paragraphs.First().Append("\n" + cert_name).Font("標楷體").FontSize(18).Alignment = Xceed.Document.NET.Alignment.center;
            //單位(簡稱)
            //table.Rows[2].Cells[1].Paragraphs.First().Append(org.ShortName).Font("標楷體").FontSize(14);
            table.Rows[2].Cells[1].Paragraphs.First().Append(coach.in_stuff_b1 + " " + coach.in_short_org).Font("標楷體").FontSize(14);

            int name_fontSize = 14;
            if (coach.in_name.Length >= 20) name_fontSize = 10;
            else if (coach.in_name.Length >= 10) name_fontSize = 12;
            //姓名
            table.Rows[3].Cells[1].Paragraphs.First().Append(coach.in_name).Font("標楷體").FontSize(name_fontSize);

            int work_fonSize = 12;
            if (cert_name.Length >= 6) work_fonSize = 11;
            table.Rows[4].Cells[1].Paragraphs.First().Append(work_name).Font("標楷體").FontSize(work_fonSize);

            if (coach.in_coach_full != "")
            {
                SetHeadShot(cfg, doc, table, 6, 0, 150, 250, coach, coach.in_coach_full, "教練證");
            }
            else if (coach.in_head_full != "")
            {
                SetHeadShot(cfg, doc, table, 6, 0, 150, 100, coach, coach.in_head_full, "大頭照");
            }
        }

        //設定大頭照
        private void SetHeadShot(TConfig cfg
            , Xceed.Words.NET.DocX doc
            , Xceed.Document.NET.Table table
            , int ri
            , int ci
            , int height
            , int width
            , TCoachRow coach
            , string picFull
            , string picName)
        {
            try
            {
                Xceed.Document.NET.Image img = doc.AddImage(picFull);
                Xceed.Document.NET.Picture p = img.CreatePicture(height, width);
                table.Rows[ri].Cells[ci].Paragraphs.First().AppendPicture(p);
            }
            catch (Exception ex)
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, picName + "出錯 _# " + coach.in_name + ", " + picFull);
            }
        }

        private Dictionary<string, TCoachRow> GetCoachMap(TConfig cfg, Item items)
        {
            var map = new Dictionary<string, TCoachRow>();
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var in_current_org = item.getProperty("in_current_org", "");
                var in_l2 = item.getProperty("in_l2", "");
                var in_l3 = item.getProperty("in_l3", "");
                var in_sno = item.getProperty("in_sno", "").ToUpper();
                var key = in_current_org + in_sno;

                if (!map.ContainsKey(key))
                {
                    map.Add(key, MapCoachRow(cfg, item));
                }

                var old = map[key];
                switch(in_l3)
                {
                    case "女子隊教練":
                        old.is_w_coach = true;
                        cfg.is_special_coach = true;
                        break;

                    case "男子隊教練":
                        old.is_m_coach = true;
                        cfg.is_special_coach = true;
                        break;

                    case "管理":
                        old.is_manager = true;
                        break;

                    case "領隊":
                        old.is_leader = true;
                        break;
                }

                old.items.Add(item);
            }
            return map;
        }

        private TCoachRow MapCoachRow(TConfig cfg, Item item)
        {
            var x = new TCoachRow
            {
                in_stuff_b1 = item.getProperty("in_stuff_b1", ""),
                in_current_org = item.getProperty("in_current_org", ""),
                in_short_org = item.getProperty("in_short_org", ""),
                in_show_org = item.getProperty("in_show_org", ""),
                in_name = item.getProperty("in_name", ""),
                in_sno = item.getProperty("in_sno", "").ToUpper(),
                in_gender = item.getProperty("in_gender", ""),
                in_head_photo = item.getProperty("in_head_photo", ""),
                in_head_filename = item.getProperty("in_head_filename", ""),
                in_coach_photo = item.getProperty("in_coach_photo", ""),
                in_coach_filename = item.getProperty("in_coach_filename", ""),
                in_l3 = item.getProperty("in_l3", ""),
                items = new List<Item>(),
            };
            x.org_code = GetInt(x.in_stuff_b1);

            if (x.in_gender == "男") x.gender_code = 1;
            else if (x.in_gender == "女") x.gender_code = 2;
            else x.gender_code = 3;

            x.in_head_full = GetFullfile(cfg, x.in_head_photo, x.in_head_filename);
            x.in_coach_full = GetFullfile(cfg, x.in_coach_photo, x.in_coach_filename);

            if (x.in_head_full != "")
            {
                if (!System.IO.File.Exists(x.in_head_full))
                {
                    x.in_head_full = "";
                }
            }

            if (x.in_coach_full != "")
            {
                if (!System.IO.File.Exists(x.in_coach_full))
                {
                    x.in_coach_full = "";
                }
            }

            return x;
        }

        private string GetFullfile(TConfig cfg, string file_id, string file_name)
        {
            if (file_id == "" || file_id.Length != 32) return "";

            string id_1 = file_id.Substring(0, 1);
            string id_2 = file_id.Substring(1, 2);
            string id_3 = file_id.Substring(3, 29);

            return @"C:\Aras\Vault\judo\" + id_1 + @"\" + id_2 + @"\" + id_3 + @"\" + file_name;
        }

        private class TCoachRow
        {
            public string in_stuff_b1 { get; set; }
            public string in_current_org { get; set; }
            public string in_short_org { get; set; }
            public string in_show_org { get; set; }
            public string in_name { get; set; }
            public string in_sno { get; set; }
            public string in_gender { get; set; }

            public string in_head_photo { get; set; }
            public string in_head_filename { get; set; }
            public string in_head_full { get; set; }

            public string in_coach_photo { get; set; }
            public string in_coach_filename { get; set; }
            public string in_coach_full { get; set; }

            public string in_l3 { get; set; }

            public bool is_m_coach { get; set; }
            public bool is_w_coach { get; set; }
            public bool is_manager { get; set; }
            public bool is_leader { get; set; }

            public int org_code { get; set; }
            public int gender_code { get; set; }

            public List<Item> items { get; set; }
        }

        private Item GetCoachAndStaffItems(TConfig cfg)
        {
            var coachCond = cfg.in_sno == ""
                ? "AND ISNULL(t1.in_verify_result, '') NOT IN ('', '審核不通過')"
                : "AND t1.in_sno = '" + cfg.in_sno + "'";

            var sql = @"
                SELECT
	                t11.in_current_org
	                , t11.in_stuff_b1
	                , t11.in_short_org
	                , t11.in_show_org
	                , t11.in_name
	                , t11.in_sno
	                , t11.in_gender
	                , t11.in_l2
	                , t11.in_l3
	                , t11.in_photo1				AS 'in_head_photo'
	                , t21.filename				AS 'in_head_filename'
	                , t14.in_file1				AS 'in_coach_photo'
	                , t22.filename				AS 'in_coach_filename'
                FROM
	                IN_MEETING_USER t11 WITH(NOLOCK)
                INNER JOIN
                (
	                SELECT
		                t1.in_current_org
		                , t1.in_name
		                , t1.in_sno
	                FROM
		                IN_MEETING_USER t1 WITH(NOLOCK)
	                WHERE
		                t1.source_id = '{#meeting_id}'
		                AND t1.in_l2 = '教練'
		                {#coachCond}
                ) t12 
                    ON t12.in_current_org = t11.in_current_org 
                    AND t12.in_sno = t11.in_sno
                INNER JOIN
	                IN_RESUME t13 WITH(NOLOCK)
	                ON t13.login_name = t11.in_sno
                INNER JOIN
	                IN_RESUME_CERTIFICATE t14 WITH(NOLOCK)
	                ON t14.source_id = t13.id
					AND t14.in_photokey = '{#in_photokey}'
                LEFT OUTER JOIN
	                [FILE] t21 WITH(NOLOCK)
	                ON t21.id = t11.in_photo1
                LEFT OUTER JOIN
	                [FILE] t22 WITH(NOLOCK)
	                ON t22.id = t14.in_file1
                WHERE
	                t11.source_id = '{#meeting_id}'
	                AND t11.in_l1 = '隊職員'
                ORDER BY
	                t11.in_gender DESC
	                , t11.in_current_org
	                , t11.in_name
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_photokey}", cfg.in_photokey)
                .Replace("{#coachCond}", coachCond);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        private void SendEmail(TConfig cfg, Item itmReturn)
        {
            AppendMeeting(cfg);

            var items = GetCreatorItems(cfg);
            var creatorMap = GetCreatorMap(cfg, items);
            var creatorList = creatorMap.Values.ToList();
            for (var i = 0; i < creatorList.Count; i++)
            {
                var creator = creatorList[i];
                var subject = GetEmailSubject(cfg);
                var body = GetEmailBody(cfg, creator);
                if (creator.in_email == "" || creator.in_email == "na@na.n")
                {
                    continue;
                }
                else
                {
                    //RundSendEmailSportOpen(cfg, "straying1@gmail.com", "田純嫣", subject, body);
                    //RundSendEmailSportOpen(cfg, "elsa@innosoft.com.tw", "Elsa", subject, body);
                    RundSendEmailSportOpen(cfg, creator.in_email, creator.in_name, subject, body);
                }
            }
        }

        private string GetEmailSubject(TConfig cfg)
        {
            return "教練證審核未通過";
        }

        private string GetEmailBody(TConfig cfg, TCreator creator)
        {
            var builder = new StringBuilder();
            builder.AppendLine("<!DOCTYPE html>");
            builder.AppendLine("<html>");
            builder.AppendLine("<head>");
            builder.AppendLine("    <meta name=\"viewport\" content=\"width=device-width\" />");
            builder.AppendLine("    <title></title>");
            builder.AppendLine("</head>");
            builder.AppendLine("<body>");
            builder.AppendLine("    <div>");
            builder.AppendLine("        <h3>" + creator.in_name + "教練您好：</h3>");

            builder.AppendLine("        <p>您在【" + cfg.mt_title + "】的報名: </p>");
            for (var i = 0; i < creator.coaches.Count; i++)
            {
                var coach = creator.coaches[i];
                builder.AppendLine("        <p>" + GetVerifyRow(cfg, creator, coach) + "</p>");
            }
            builder.AppendLine("        <p>以上教練證正面照片未通過審核，請您登入系統，重新上傳教練證照片。</p>");
            builder.AppendLine("        <p></p>");
            builder.AppendLine("        <p style='color:red'>※請上傳由體育署所發之新式教練證，符合新式教練證才會提供比賽當天之教練證</p>");
            builder.AppendLine("        <p><img src='https://act.innosoft.com.tw/judo/images/coach_sample2.png' /></p>");
            builder.AppendLine("        <p></p>");
            builder.AppendLine("        <p><a href='https://act.innosoft.com.tw/judo'>https://act.innosoft.com.tw/judo</a></p>");
            builder.AppendLine("    </div>");
            builder.AppendLine("</body>");
            builder.AppendLine("</html>");
            return builder.ToString();
        }

        private string GetVerifyRow(TConfig cfg, TCreator creator, TCoach coach)
        {
            return " - " + coach.in_name + " / " + coach.in_current_org + " / 隊職員 / 教練";
        }

        private void RundSendEmailSportOpen(TConfig cfg, string address, string displayName, string subject, string body)
        {
            var itmEmailUrl = cfg.inn.applySQL("SELECT in_value FROM IN_VARIABLE WITH(NOLOCK) WHERE in_name = 'email_api_url'");
            if (itmEmailUrl.isError() || itmEmailUrl.getResult() == "") return;

            var email_api_url = itmEmailUrl.getProperty("in_value", "").TrimEnd('/');

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "RundSendEmailSportOpen"
                + "\r\nApi網址: " + email_api_url
                + "\r\n收信地址: " + address
                + "\r\n收信姓名: " + displayName
                + "\r\n" + subject
                + "\r\n" + body);

            var data = new TEmailData
            {
                Token = "28076206",
                FromAddress = "justmove913@gmail.com",
                FromDisplayName = "柔道報名系統",
                ToAddress = address,
                ToDisplayName = displayName,
                Subject = subject,
                Body = body,
            };
            try
            {
                var json = Newtonsoft.Json.JsonConvert.SerializeObject(data);
                var postUrl = email_api_url;
                HttpWebRequest hwrSendRequest = (HttpWebRequest)WebRequest.Create(postUrl);
                hwrSendRequest.Method = "POST";
                hwrSendRequest.ContentType = "application/json";

                WebResponse wrToDoResponse;
                Stream stmReqStream = hwrSendRequest.GetRequestStream();
                byte[] byarRequestBody = Encoding.UTF8.GetBytes(json);
                stmReqStream.Write(byarRequestBody, 0, byarRequestBody.Length);
                wrToDoResponse = hwrSendRequest.GetResponse();
                StreamReader srResponse = new StreamReader(wrToDoResponse.GetResponseStream());
                var strResponse = srResponse.ReadToEnd();
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "Email 發送結果: " + strResponse);
            }
            catch (Exception ex)
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "Email 發送失敗");
                throw new Exception("Email 發送失敗");
            }
        }

        private class TEmailData
        {
            public string Token { get; set; }
            public string FromAddress { get; set; }
            public string FromDisplayName { get; set; }
            public string ToAddress { get; set; }
            public string ToDisplayName { get; set; }
            public string Subject { get; set; }
            public string Body { get; set; }
        }

        private void RundSendEmailByAras(TConfig cfg, string address, string displayName, string subject, string body)
        {
            var MyFrom = new System.Net.Mail.MailAddress("justmove913@gmail.com", "柔道報名系統");
            var MyTo = new System.Net.Mail.MailAddress(address, displayName);
            var myMail = new System.Net.Mail.MailMessage(MyFrom, MyTo);

            myMail.Subject = subject;
            myMail.SubjectEncoding = System.Text.Encoding.UTF8;

            myMail.Body = body;
            myMail.BodyEncoding = System.Text.Encoding.UTF8;
            myMail.IsBodyHtml = true;

            try
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "收信地址: " + address
                    + "\r\n收信姓名: " + displayName
                    + "\r\n" + subject
                    + "\r\n" + body);
                cfg.CCO.Email.SetupSmtpMailServerAndSend(myMail);
            }
            catch (Exception ex)
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "Email 發送失敗");
                throw new Exception("Email 發送失敗");
            }
        }

        private Dictionary<string, TCreator> GetCreatorMap(TConfig cfg, Item items)
        {
            var map = new Dictionary<string, TCreator>();
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var creator_sno = item.getProperty("creator_sno", "");
                if (!map.ContainsKey(creator_sno))
                {
                    map.Add(creator_sno, new TCreator
                    {
                        in_sno = creator_sno,
                        in_name = item.getProperty("creator_name", ""),
                        in_email = item.getProperty("creator_email", ""),
                        in_tel = item.getProperty("creator_tel", ""),
                        coaches = new List<TCoach>(),
                    });
                }

                var creator = map[creator_sno];

                creator.coaches.Add(new TCoach
                {
                    in_current_org = item.getProperty("in_current_org", ""),
                    in_name = item.getProperty("in_name", ""),
                });

            }
            return map;
        }

        private Item GetCreatorItems(TConfig cfg)
        {
            var sql = @"
                SELECT DISTINCT
	                UPPER(t101.in_sno)  AS 'creator_sno'
	                , t101.in_name      AS 'creator_name'
	                , t101.in_email     AS 'creator_email'
	                , t101.in_tel       AS 'creator_tel'
	                , '教練' AS 'in_l2'
	                , t102.in_current_org
	                , t102.in_name
                FROM
	                IN_RESUME t101 WITH(NOLOCK)
                INNER JOIN
                (
	                SELECT DISTINCT in_current_org, in_name, in_sno, UPPER(in_creator_sno) AS 'in_creator_sno'
	                FROM IN_MEETING_USER WITH(NOLOCK)
	                WHERE source_id = '{#meeting_id}'
	                AND in_l2 = '教練'
	                AND ISNULL(in_verify_result, '') = '審核不通過'
                ) t102 ON t102.in_creator_sno = t101.in_sno
                ORDER BY 
					t101.in_name
					, t102.in_current_org
					, t102.in_name
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            return cfg.inn.applySQL(sql);
        }

        private class TCreator
        {
            public string in_sno { get; set; }
            public string in_name { get; set; }
            public string in_email { get; set; }
            public string in_tel { get; set; }
            public List<TCoach> coaches { get; set; }
        }

        private class TCoach
        {
            public string in_current_org { get; set; }
            public string in_name { get; set; }
            public string in_sno { get; set; }
        }

        private void Verify(TConfig cfg, Item itmReturn)
        {
            var muid = itmReturn.getProperty("muid", "");
            var in_verify_result = itmReturn.getProperty("value", "");
            var sql = "UPDATE IN_MEETING_USER SET in_verify_result = N'" + in_verify_result + "', in_verify_time = getutcdate() WHERE id = '" + muid + "'";
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);
            cfg.inn.applySQL(sql);
        }

        private void Run(TConfig cfg, Item itmReturn)
        {
            var rows = new List<TRow>();
            var items = GetCoachItems(cfg);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var code = item.getProperty("in_stuff_b1", "");
                var in_short_org = item.getProperty("in_short_org", "");
                var in_current_org = item.getProperty("in_current_org", "");
                var in_name = item.getProperty("in_name", "");
                var row = rows.Find(x => x.code == code);
                if (row == null)
                {
                    row = new TRow
                    {
                        code = code,
                        name = in_short_org,
                        text = code + in_short_org,
                        full = in_current_org,
                        coachs = new List<string>()
                    };
                    rows.Add(row);
                }
                row.coachs.Add(in_name);
            }

            var map = GetL3Items(cfg);
            var builder = new StringBuilder();
            builder.AppendLine();
            for (var i = 0; i < rows.Count; i++)
            {
                var row = rows[i];
                var no = i + 1;
                var l3s = "";
                if (map.ContainsKey(row.code))
                {
                    l3s = string.Join("、", map[row.code]);
                }
                builder.AppendLine(row.text + "\t" + string.Join("、", row.coachs) + "\t" + l3s);
            }
            builder.AppendLine();
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, builder.ToString());
        }

        private Item GetCoachItems(TConfig cfg)
        {
            var sql = @"
                SELECT in_stuff_b1, in_short_org, in_current_org, in_name  FROM IN_MEETING_USER WITH(NOLOCK)
                WHERE source_id = '{#meeting_id}'
                AND in_l1 = N'隊職員'
                AND in_l2 = '教練'
                AND in_current_org IN (
                	SELECT DISTINCT in_current_org FROM IN_MEETING_USER WITH(NOLOCK)
                	WHERE source_id = '{#meeting_id}'
                	AND in_l1 = N'團體組'
                	AND in_l3 NOT LIKE '%國小%'
                )
                ORDER BY in_stuff_b1, in_gender, in_name
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            return cfg.inn.applySQL(sql);
        }

        private Dictionary<string, List<string>> GetL3Items(TConfig cfg)
        {
            var map = new Dictionary<string, List<string>>();

            var sql = @"
            	SELECT DISTINCT 
            		t1.in_stuff_b1
            		, t1.in_current_org
            		, t1.in_l3 
            		, t2.in_sort_order
            	FROM 
            		IN_MEETING_USER t1 WITH(NOLOCK)
            	INNER JOIN
            		IN_MEETING_PROGRAM t2 WITH(NOLOCK)
            		ON t2.in_meeting = t1.source_id
            		AND t2.in_l1 = t1.in_l1
            		AND t2.in_l2 = t1.in_l2
            		AND t2.in_l3 = t1.in_l3
            	WHERE 
            		t1.source_id = '{#meeting_id}'
            		AND t1.in_l1 = N'團體組'
            		AND t1.in_l3 NOT LIKE '%國小%'
            	ORDER BY
            		t1.in_stuff_b1
            		, t2.in_sort_order
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var code = item.getProperty("in_stuff_b1", "");
                var in_l3 = item.getProperty("in_l3", "");
                if (!map.ContainsKey(code))
                {
                    map.Add(code, new List<string>());
                }
                map[code].Add(in_l3 + "：_______");
            }

            return map;
        }

        private class TRow
        {
            public string code { get; set; }
            public string name { get; set; }
            public string text { get; set; }
            public string full { get; set; }
            public List<string> coachs { get; set; }
        }

        private void AppendMeeting(TConfig cfg)
        {
            cfg.itmMeeting = cfg.inn.applySQL("SELECT id, in_title FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            cfg.mt_title = cfg.itmMeeting.getProperty("in_title", "");
        }

        private TExport ExportInfo(TConfig cfg, string in_name)
        {
            Item itmXls = cfg.inn.applyMethod("In_Meeting_Excel_Path", "<in_name>" + in_name + "</in_name>");

            return new TExport
            {
                template_path = itmXls.getProperty("template_path", ""),
                export_path = itmXls.getProperty("export_path", ""),
            };
        }

        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string meeting_id { get; set; }
            public string scene { get; set; }

            public Item itmMeeting { get; set; }
            public string mt_title { get; set; }

            public string in_sno { get; set; }
            public string in_name { get; set; }

            public string in_photokey { get; set; }
            public bool is_special_coach { get; set; }
        }

        private void SetPhotoKey(TConfig cfg)
        {
            var sql = "SELECT id, in_type, in_country FROM IN_MEETING_CERTIFICATE WITH(NOLOCK) WHERE source_id = '" + cfg.meeting_id + "' AND in_type = '8'";
            var itmData = cfg.inn.applySQL(sql);
            if (itmData.isError() || itmData.getResult() == "") throw new Exception("未設定教練證");

            cfg.in_photokey = "in_photo" + itmData.getProperty("in_type", "") + itmData.getProperty("in_country", "");
        }

        private class TExport
        {
            public string template_path { get; set; }
            public string export_path { get; set; }
        }

        private int GetInt(string value, int defV = 0)
        {
            if (value == "") return 0;
            int result = defV;
            if (int.TryParse(value, out result)) return result;
            return defV;
        }
    }
}