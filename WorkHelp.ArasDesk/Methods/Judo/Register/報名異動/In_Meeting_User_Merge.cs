﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;
using Innosoft;
using System.Data.SqlTypes;

namespace WorkHelp.ArasDesk.Methods.Judo.Register
{
    public class In_Meeting_User_Merge : Item
    {
        public In_Meeting_User_Merge(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
    目的: 併組、修改姓名
    日誌: 
        - 2023-02-08: 創建 (lina)
*/

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "In_Meeting_User_Merge";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                meeting_id = itmR.getProperty("meeting_id", ""),
                muid = itmR.getProperty("muid", ""),
                in_name = itmR.getProperty("in_name", ""),
                scene = itmR.getProperty("scene", ""),
            };

            switch (cfg.scene)
            {
                case "page":
                    Page(cfg, itmR);
                    break;

                case "players":
                    Players(cfg, itmR);
                    break;

                case "merge":
                    Merge(cfg, itmR);
                    break;

                case "query":
                    Query(cfg, itmR);
                    break;

                case "fix_name":
                    FixName(cfg, itmR);
                    break;

                case "cancel_register":
                    CancelRegister(cfg, itmR);
                    break;

                case "find_player":
                    FindPlayer(cfg, itmR);
                    break;

                case "find_org_player":
                    FindOrgPlayer(cfg, itmR);
                    break;

                case "modal":
                    Modal(cfg, itmR);
                    break;

                case "change_weight":
                    ChangeWeight(cfg, itmR);
                    break;

                case "change_team":
                    ChangeTeam(cfg, itmR);
                    break;

                case "change_player":
                    ChangePlayer(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void ChangePlayer(TConfig cfg, Item itmReturn)
        {
            var itmMUser = cfg.inn.applySQL("SELECT * FROM IN_MEETING_USER WITH(NOLOCK) WHERE id = '" + cfg.muid + "'");
            if (itmMUser.getResult() == "") throw new Exception("查無與會者");

            var old_mail = itmMUser.getProperty("in_mail", "").ToUpper();
            var old_sno = itmMUser.getProperty("in_sno", "").ToUpper();

            var new_name = itmReturn.getProperty("new_name", "");
            var new_sno = itmReturn.getProperty("new_sno", "").ToUpper();
            var new_birth = GetDtm(itmReturn.getProperty("new_birth", "")).AddHours(-8).ToString("yyyy-MM-dd HH:mm:ss");
            var new_mail = old_mail.Replace(old_sno, new_sno);

            var sql = "";

            sql = "UPDATE IN_MEETING_USER SET"
                + "  keyed_name = N'" + new_name + "'"
                + ", in_name = N'" + new_name + "'"
                + ", in_sno = '" + new_sno + "'"
                + ", in_birth = '" + new_birth + "'"
                + ", in_mail = '" + new_mail + "'"
                + " WHERE id = '" + cfg.muid + "'";

            RunApplySQL(cfg, sql);


            sql = "UPDATE IN_MEETING_NEWS SET"
                + "  keyed_name = N'" + new_name + "'"
                + ", in_name = N'" + new_name + "'"
                + ", in_sno = '" + new_sno + "'"
                + " WHERE in_muid = '" + cfg.muid + "'";

            RunApplySQL(cfg, sql);
        }

        private void ChangeTeam(TConfig cfg, Item itmReturn)
        {
            var itmMUser = cfg.inn.applySQL("SELECT * FROM IN_MEETING_USER WITH(NOLOCK) WHERE id = '" + cfg.muid + "'");
            if (itmMUser.getResult() == "") throw new Exception("查無與會者");

            var muid = itmMUser.getProperty("id", "");
            var in_l1 = itmMUser.getProperty("in_l1", "");
            var in_l2 = itmMUser.getProperty("in_l2", "");
            var in_l3 = itmMUser.getProperty("in_l3", "");
            var new_team = itmReturn.getProperty("in_team", "");
            var in_creator_sno = itmMUser.getProperty("in_creator_sno", "");
            var in_current_org = itmMUser.getProperty("in_current_org", "");

            var sql = "SELECT TOP 1 * FROM IN_MEETING_USER WITH(NOLOCK)"
                + " WHERE source_id = '" + cfg.meeting_id + "'"
                + " AND in_creator_sno = '" + in_creator_sno + "'"
                + " AND in_l1 = N'" + in_l1 + "'"
                + " AND in_l2 = N'" + in_l2 + "'"
                + " AND in_l3 = N'" + in_l3 + "'"
                + " AND in_current_org = N'" + in_current_org + "'"
                + " AND in_team = '" + new_team + "'";

            var itmData = cfg.inn.applySQL(sql);
            if (itmData.isError() || itmData.getResult() == "")
            {
                throw new Exception("查無新隊別資料 = " + new_team);
            }

            var new_index = itmData.getProperty("in_index", "");

            sql = "UPDATE IN_MEETING_USER SET in_index = '" + new_index + "', in_team = '" + new_team + "' WHERE id = '" + muid + "'";
            RunApplySQL(cfg, sql);


            sql = "UPDATE IN_MEETING_NEWS SET in_index = '" + new_index + "' WHERE in_muid = '" + muid + "'";
            RunApplySQL(cfg, sql);
        }

        private void RunApplySQL(TConfig cfg, string sql)
        {
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);
            cfg.inn.applySQL(sql);
        }

        private void CancelRegister(TConfig cfg, Item itmReturn)
        {
            var itmMUser = cfg.inn.applySQL("SELECT * FROM IN_MEETING_USER WITH(NOLOCK) WHERE id = '" + cfg.muid + "'");
            if (itmMUser.getResult() == "") throw new Exception("查無與會者");

            var in_l1 = itmMUser.getProperty("in_l1", "");
            if (in_l1.Contains("團體"))
            {
                CancelRegisterTeam(cfg, itmMUser, itmReturn);
            }
            else
            {
                CancelRegisterSolo(cfg, itmMUser, itmReturn);
                CancelFightSolo(cfg, itmMUser, itmReturn);
            }
        }

        private void CancelRegisterTeam(TConfig cfg, Item itmMUser, Item itmReturn)
        {
            var in_l1 = itmMUser.getProperty("in_l1", "");
            var in_l2 = itmMUser.getProperty("in_l2", "");
            var in_l3 = itmMUser.getProperty("in_l3", "");
            var in_index = itmMUser.getProperty("in_index", "");
            var in_creator_sno = itmMUser.getProperty("in_creator_sno", "");
            var in_paynumber = itmMUser.getProperty("in_paynumber", "0");
            var in_expense = itmMUser.getProperty("in_expense", "0");

            var itmMPay = cfg.inn.applySQL("SELECT * FROM IN_MEETING_PAY WITH(NOLOCK) WHERE item_number = '" + in_paynumber + "'");
            if (itmMPay.getResult() == "") throw new Exception("查無繳費單");

            var pay_id = itmMPay.getProperty("id", "");
            var old_expense = GetInt(in_expense);
            var old_write_note = itmMPay.getProperty("in_write_note", "");
            var old_refund_amount = GetInt(itmMPay.getProperty("in_refund_amount", "0"));

            var text = "取消報名:" + in_l3;
            var new_write_note = old_write_note != "" ? old_write_note + "; " + text : text;
            var new_refund_amount = old_refund_amount + old_expense;

            var sql = "";

            // 退費申請通過
            sql = @"
                UPDATE IN_MEETING_NEWS SET 
	                in_verify_time = GETUTCDATE()
	                , in_verify_by_id = 'DC14C94BA2FA4ED7B0D0E4CA28743DE1'
	                , in_cancel_status = N'退費申請通過'
	                , in_refund_amount = {#in_refund_amount}
                WHERE 
                    source_id = '{#pay_id}'
                    AND in_l1 = N'{#in_l1}'
                    AND in_l2 = N'{#in_l2}'
                    AND in_l3 = N'{#in_l3}'
                    AND in_index = '{#in_index}'
                    AND in_creator_sno = '{#in_creator_sno}'
            ";

            sql = sql.Replace("{#pay_id}", pay_id)
                .Replace("{#in_l1}", in_l1)
                .Replace("{#in_l2}", in_l2)
                .Replace("{#in_l3}", in_l3)
                .Replace("{#in_index}", in_index)
                .Replace("{#in_creator_sno}", in_creator_sno)
                .Replace("{#in_refund_amount}", in_expense);

            RunApplySQL(cfg, sql);


            // 更新繳費單退款與手續費資訊
            sql = @"
                UPDATE IN_MEETING_PAY SET
                    in_fee_amount = '0'
                    , in_refund_amount = '{#new_refund_amount}'
                    , in_write_note = N'{#new_write_note}'
                WHERE id = '{#pay_id}'
            ";

            sql = sql.Replace("{#pay_id}", pay_id)
                .Replace("{#new_refund_amount}", new_refund_amount.ToString())
                .Replace("{#new_write_note}", new_write_note);

            RunApplySQL(cfg, sql);


            //移除[申請退費的學員的報名資料]
            sql = @"
                DELETE FROM IN_MEETING_USER
                WHERE 
                    source_id = '{#meeting_id}'
                    AND in_l1 = N'{#in_l1}'
                    AND in_l2 = N'{#in_l2}'
                    AND in_l3 = N'{#in_l3}'
                    AND in_index = '{#in_index}'
                    AND in_creator_sno = '{#in_creator_sno}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_l1}", in_l1)
                .Replace("{#in_l2}", in_l2)
                .Replace("{#in_l3}", in_l3)
                .Replace("{#in_index}", in_index)
                .Replace("{#in_creator_sno}", in_creator_sno);

            RunApplySQL(cfg, sql);
        }

        private void CancelFightSolo(TConfig cfg, Item itmMUser, Item itmReturn)
        {
            var in_l1 = itmMUser.getProperty("in_l1", "");
            var in_l2 = itmMUser.getProperty("in_l2", "");
            var in_l3 = itmMUser.getProperty("in_l3", "");
            var in_index = itmMUser.getProperty("in_index", "");
            var in_creator_sno = itmMUser.getProperty("in_creator_sno", "");

            var sql = @"
                SELECT TOP 1
                    * 
                FROM 
                    IN_MEETING_PROGRAM WITH(NOLOCK)
                WHERE 
                    source_id = '{#meeting_id}'
                    AND in_l1 = N'{#in_l1}'
                    AND in_l2 = N'{#in_l2}'
                    AND in_l3 = N'{#in_l3}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_l1}", in_l1)
                .Replace("{#in_l2}", in_l2)
                .Replace("{#in_l3}", in_l3);

            var itmProgram = cfg.inn.applySQL(sql);
            if (itmProgram.getResult() == "") return;

            var program_id = itmProgram.getProperty("id", "");

            sql = @"
                SELECT TOP 1
                    * 
                FROM 
                    IN_MEETING_PTEAM WITH(NOLOCK)
                WHERE 
                    in_meeting = '{#meeting_id}'
                    AND source_id = '{#program_id}'
                    AND in_index = '{#in_index}'
                    AND in_creator_sno = '{#in_creator_sno}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#program_id}", program_id)
                .Replace("{#in_index}", in_index)
                .Replace("{#in_creator_sno}", in_creator_sno);

            var itmPTeam = cfg.inn.applySQL(sql);
            if (itmPTeam.getResult() == "") return;

            //重設組別隊伍欄位 (含重建場次)
            ResetProgram(cfg, in_l1, in_l2, in_l3);

            //統計單位隊伍數量
            UpdateOrgTeams(cfg, program_id);

            //更新與會者 No
            UpdateMUserShowNo(cfg, in_l1, in_l2, in_l3);

            //更新單位隊伍 No
            UpdatePTeamShowNo(cfg, program_id);
        }

        //更新單位隊伍 No
        private void UpdatePTeamShowNo(TConfig cfg, string program_id)
        {
            string sql = @"
                UPDATE t1 SET
	                t1.in_show_no = t2.rno
                FROM 
	                IN_MEETING_PTEAM t1 WITH(NOLOCK)
                INNER JOIN
                (
	                SELECT
		                id
		                , ROW_NUMBER() OVER (PARTITION BY source_id ORDER BY in_city_no, in_stuff_b1, in_current_org, in_team, in_sno) AS 'rno'
	                FROM
		                IN_MEETING_PTEAM WITH(NOLOCK)
	                WHERE
		                source_id = '{#program_id}'
                ) t2
                ON t2.id = t1.id
                WHERE
	                t1.source_id = '{#program_id}'
            ";

            sql = sql.Replace("{#program_id}", program_id);

            RunApplySQL(cfg, sql);
        }

        //更新與會者 No
        private void UpdateMUserShowNo(TConfig cfg, string in_l1, string in_l2, string in_l3)
        {
            string sql = @"
                UPDATE t1 SET
	                t1.in_show_no = t2.rno
	                , t1.in_section_no = t2.rno
                FROM 
	                IN_MEETING_USER t1 WITH(NOLOCK)
                INNER JOIN
                (
	                SELECT
		                id
						, in_name
						, in_sno
		                , ROW_NUMBER() OVER (PARTITION BY in_l1, in_l2, in_l3 ORDER BY in_city_no, in_stuff_b1, in_current_org, in_team, in_sno) AS 'rno'
	                FROM
		                IN_MEETING_USER WITH(NOLOCK)
	                WHERE
		                source_id = '{#meeting_id}'
    	                AND in_l1 = N'{#in_l1}'
    	                AND in_l2 = N'{#in_l2}'
    	                AND in_l3 = N'{#in_l3}'
                ) t2
                ON t2.id = t1.id
                WHERE
	                t1.source_id = '{#meeting_id}'
	                AND t1.in_l1 = N'{#in_l1}'
	                AND t1.in_l2 = N'{#in_l2}'
	                AND t1.in_l3 = N'{#in_l3}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_l1}", in_l1)
                .Replace("{#in_l2}", in_l2)
                .Replace("{#in_l3}", in_l3);

            RunApplySQL(cfg, sql);
        }

        //重設組別隊伍欄位
        private void ResetProgram(TConfig cfg, string in_l1, string in_l2, string in_l3)
        {
            Item itmData = cfg.inn.newItem();
            itmData.setType("In_Meeting_Program");
            itmData.setProperty("scene", "");
            itmData.setProperty("meeting_id", cfg.meeting_id);
            itmData.setProperty("l1_value", in_l1);
            itmData.setProperty("l2_value", in_l2);
            itmData.setProperty("l3_value", in_l3);
            itmData.setProperty("no_straw", "1");
            itmData.apply("in_meeting_straw");
        }

        //更新單位隊伍數
        private void UpdateOrgTeams(TConfig cfg, string program_id)
        {
            string sql = @"
                UPDATE
                    t1
                SET
                    t1.in_org_teams = t2.in_org_teams
                FROM
                    IN_MEETING_PTEAM t1 WITH(NOLOCK)
                LEFT OUTER JOIN
                (
                    SELECT 
                        source_id
                        , in_current_org
                        , count(id) AS 'in_org_teams'
                    FROM
                        IN_MEETING_PTEAM WITH(NOLOCK)
                    GROUP BY
                        source_id
                        , in_current_org
                ) t2 
                    ON t2.source_id = t1.source_id
                    AND t2.in_current_org = t1.in_current_org
                WHERE
                    t1.source_id = '{#program_id}'
            ";

            sql = sql.Replace("{#program_id}", program_id);

            RunApplySQL(cfg, sql);
        }

        private void CancelRegisterSolo(TConfig cfg, Item itmMUser, Item itmReturn)
        {
            var in_name = itmMUser.getProperty("in_name", "");
            var in_l2 = itmMUser.getProperty("in_l2", "");
            var clear_l2 = GetClearL2(in_l2);
            var in_paynumber = itmMUser.getProperty("in_paynumber", "0");
            var in_expense = itmMUser.getProperty("in_expense", "0");

            var itmMPay = cfg.inn.applySQL("SELECT * FROM IN_MEETING_PAY WITH(NOLOCK) WHERE item_number = '" + in_paynumber + "'");
            if (itmMPay.getResult() == "") throw new Exception("查無繳費單");

            var pay_id = itmMPay.getProperty("id", "");
            var old_expense = GetInt(in_expense);
            var old_write_note = itmMPay.getProperty("in_write_note", "");
            var old_refund_amount = GetInt(itmMPay.getProperty("in_refund_amount", "0"));

            var text = "取消報名:" + in_name + "-" + clear_l2;
            var new_write_note = old_write_note != "" ? old_write_note + "; " + text : text;
            var new_refund_amount = old_refund_amount + old_expense;

            var sql = "";

            // 退費申請通過
            sql = @"
                UPDATE IN_MEETING_NEWS SET 
	                in_verify_time = GETUTCDATE()
	                , in_verify_by_id = 'DC14C94BA2FA4ED7B0D0E4CA28743DE1'
	                , in_cancel_status = N'退費申請通過'
	                , in_refund_amount = {#in_refund_amount}
                WHERE in_muid = '{#muid}'
            ";

            sql = sql.Replace("{#muid}", cfg.muid)
                .Replace("{#in_refund_amount}", in_expense);

            RunApplySQL(cfg, sql);


            // 更新繳費單退款與手續費資訊
            sql = @"
                UPDATE IN_MEETING_PAY SET
                    in_fee_amount = '0'
                    , in_refund_amount = '{#new_refund_amount}'
                    , in_write_note = N'{#new_write_note}'
                WHERE id = '{#pay_id}'
            ";

            sql = sql.Replace("{#pay_id}", pay_id)
                .Replace("{#new_refund_amount}", new_refund_amount.ToString())
                .Replace("{#new_write_note}", new_write_note);

            RunApplySQL(cfg, sql);


            //移除[問卷紀錄]
            sql = "DELETE FROM IN_MEETING_SURVEYS_RESULT WHERE in_participant = '" + cfg.muid + "'";
            RunApplySQL(cfg, sql);


            //移除[學員履歷]
            sql = "DELETE FROM IN_MEETING_RESUME WHERE in_user = '" + cfg.muid + "'";
            RunApplySQL(cfg, sql);


            //移除[簽到記錄]
            sql = "DELETE FROM IN_MEETING_RECORD WHERE in_participant = '" + cfg.muid + "'";
            RunApplySQL(cfg, sql);


            //移除[申請退費的學員的報名資料]
            sql = "DELETE FROM IN_MEETING_USER WHERE id = '" + cfg.muid + "'";
            RunApplySQL(cfg, sql);
        }

        private string GetClearL2(string value)
        {
            return value.Replace("個-", "").Replace("團-", "").Replace("格-", "");
        }

        private void ChangeWeight(TConfig cfg, Item itmReturn)
        {
            cfg.itmMeeting = cfg.inn.applySQL("SELECT id, in_title FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");

            //活動資訊
            AppendMeeting(cfg, itmReturn);

            var svy_model = GetMtSvy(cfg);

            var itmMUser = FindMeetingUser(cfg, cfg.muid, "");
            var old_current_org = itmMUser.getProperty("in_current_org", "");
            var old_name = itmMUser.getProperty("in_name", "");
            var old_l1 = itmMUser.getProperty("in_l1", "");
            var old_l2 = itmMUser.getProperty("in_l2", "");
            var old_l3 = itmMUser.getProperty("in_l3", "");
            var old_index = itmMUser.getProperty("in_index", "");
            var old_team = itmMUser.getProperty("in_team", "");
            var old_creator_sno = itmMUser.getProperty("in_creator_sno", "");
            var old_expense = itmMUser.getProperty("in_expense", "0");
            var old_paynumber = itmMUser.getProperty("in_paynumber", "");

            var new_l1 = itmReturn.getProperty("in_l1", "");
            var new_l2 = itmReturn.getProperty("in_l2", "");
            var new_l3 = itmReturn.getProperty("in_l3", "");
            var new_team = itmReturn.getProperty("in_team", "");
            if (new_team == "") new_team = old_team;

            var itmLv1 = GetL1Item(cfg, svy_model, new_l1);
            var itmLv2 = GetL2Item(cfg, svy_model, new_l1, new_l2);
            var itmLv3 = GetL3Item(cfg, svy_model, new_l1, new_l2, new_l3);

            var clear_l2 = itmLv2.getProperty("in_label", "");
            var new_expense = GetExpense(svy_model, itmLv1, itmLv2, itmLv3);

            if (new_expense != old_expense)
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "費用不符"
                    + ", 與會者 id = " + cfg.muid
                    + ", 單位 = " + old_current_org
                    + ", 姓名 = " + old_name
                    + ", 繳費單 = " + old_paynumber
                    + ", 費用 = " + old_expense
                    + ", 新費用 = " + new_expense
                    + ", 序號 = " + old_index
                    );
            }

            if (old_l1.Contains("團體"))
            {
                var existed = ExistedOrgSectTeam(cfg, old_current_org, new_l1, new_l2, new_l3, new_team);
                if (existed)
                {
                    throw new Exception(old_current_org + " " + new_l1 + " " + new_l2 + " " + new_l3 + "已有" + new_team + "隊");
                }
            }

            var sql = @"
                SELECT
	                *
                FROM
	                IN_MEETING_USER WITH(NOLOCK)
                WHERE
	                source_id = '{#meeting_id}'
	                AND in_l1 = '{#in_l1}'
	                AND in_l2 = '{#in_l2}'
	                AND in_l3 = '{#in_l3}'
	                AND in_index = '{#in_index}'
	                AND in_creator_sno = '{#in_creator_sno}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_l1}", old_l1)
                .Replace("{#in_l2}", old_l2)
                .Replace("{#in_l3}", old_l3)
                .Replace("{#in_index}", old_index)
                .Replace("{#in_creator_sno}", old_creator_sno);

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var muid = item.getProperty("id", "");
                var in_sno = item.getProperty("in_sno", "");

                var new_mail = in_sno + "-" + new_l1 + "-" + new_l2 + "-" + new_l3;
                var new_section_name = new_l1 + "-" + clear_l2 + "-" + new_l3;

                sql = @"
                    UPDATE IN_MEETING_USER SET 
                          in_l2_old = in_l2
                        , in_l3_old = in_l3
                        , in_l1 = N'{#new_l1}'
                        , in_l2 = N'{#new_l2}'
                        , in_l3 = N'{#new_l3}'
                        , in_mail = N'{#new_mail}'
                        , in_section_name = N'{#new_section_name}'
                        , in_expense = '{#new_expense}'
                        , in_team = '{#new_team}'
                    WHERE
                        id = '{#muid}'
                ";

                sql = sql.Replace("{#muid}", muid)
                    .Replace("{#new_l1}", new_l1)
                    .Replace("{#new_l2}", new_l2)
                    .Replace("{#new_l3}", new_l3)
                    .Replace("{#new_mail}", new_mail)
                    .Replace("{#new_section_name}", new_section_name)
                    .Replace("{#new_expense}", new_expense)
                    .Replace("{#new_team}", new_team);

                RunApplySQL(cfg, sql);

                sql = @"
                    UPDATE IN_MEETING_NEWS SET 
                        in_l1 = N'{#new_l1}'
                        , in_l2 = N'{#new_l2}'
                        , in_l3 = N'{#new_l3}'
                        , in_ans_l3 = N'{#new_l3}'
                        , in_section_name = N'{#new_section_name}'
                        , in_pay_amount = '{#new_expense}'
                    WHERE
                        in_muid = '{#muid}'
                ";

                sql = sql.Replace("{#muid}", muid)
                    .Replace("{#new_l1}", new_l1)
                    .Replace("{#new_l2}", new_l2)
                    .Replace("{#new_l3}", new_l3)
                    .Replace("{#new_mail}", new_mail)
                    .Replace("{#new_section_name}", new_section_name)
                    .Replace("{#new_expense}", new_expense);

                RunApplySQL(cfg, sql);
            }
        }

        private bool ExistedOrgSectTeam(TConfig cfg, string in_current_org, string in_l1, string in_l2, string in_l3, string in_team)
        {
            var sql = "SELECT TOP 1 * FROM IN_MEETING_USER WITH(NOLOCK)"
                + " WHERE source_id = '" + cfg.meeting_id + "'"
                + " AND in_current_org = N'" + in_current_org + "'"
                + " AND in_l1 = N'" + in_l1 + "'"
                + " AND in_l2 = N'" + in_l2 + "'"
                + " AND in_l3 = N'" + in_l3 + "'"
                + " AND in_team = '" + in_team + "'";

            var item = cfg.inn.applySQL(sql);
            if (item.isError() || item.getResult() == "")
            {
                return false;
            }

            var id = item.getProperty("id", "");
            return id != "";
        }

        private Item GetL1Item(TConfig cfg, TMtSvy svy_model, string in_value)
        {
            var sql = @"
                SELECT TOP 1
	                * 
                FROM 
	                IN_SURVEY_OPTION WITH(NOLOCK) 
                WHERE 
	                source_id = '{#source_id}' 
	                AND in_value = N'{#in_value}'
            "; ;

            sql = sql.Replace("{#source_id}", svy_model.l1_id)
                .Replace("{#in_value}", in_value);

            return cfg.inn.applySQL(sql);
        }

        private Item GetL2Item(TConfig cfg, TMtSvy svy_model, string in_filter, string in_value)
        {
            var sql = @"
                SELECT TOP 1
	                * 
                FROM 
	                IN_SURVEY_OPTION WITH(NOLOCK) 
                WHERE 
	                source_id = '{#source_id}' 
	                AND in_filter = N'{#in_filter}' 
	                AND in_value = N'{#in_value}'
            "; ;

            sql = sql.Replace("{#source_id}", svy_model.l2_id)
                .Replace("{#in_filter}", in_filter)
                .Replace("{#in_value}", in_value);

            return cfg.inn.applySQL(sql);
        }

        private Item GetL3Item(TConfig cfg, TMtSvy svy_model, string in_grand_filter, string in_filter, string in_value)
        {
            var sql = @"
                SELECT TOP 1
	                * 
                FROM 
	                IN_SURVEY_OPTION WITH(NOLOCK) 
                WHERE 
	                source_id = '{#source_id}' 
	                AND in_grand_filter = N'{#in_grand_filter}' 
	                AND in_filter = N'{#in_filter}' 
	                AND in_value = N'{#in_value}'
            "; ;

            sql = sql.Replace("{#source_id}", svy_model.l3_id)
                .Replace("{#in_grand_filter}", in_grand_filter)
                .Replace("{#in_filter}", in_filter)
                .Replace("{#in_value}", in_value);

            return cfg.inn.applySQL(sql);
        }

        private void Modal(TConfig cfg, Item itmReturn)
        {
            cfg.itmMeeting = cfg.inn.applySQL("SELECT id, in_title FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");

            //活動資訊
            AppendMeeting(cfg, itmReturn);

            var svy_model = GetMtSvy(cfg);

            //附加選單資訊(查詢)
            Item itmJson1 = cfg.inn.newItem("In_Meeting");
            itmJson1.setProperty("meeting_id", cfg.meeting_id);
            itmJson1.setProperty("l1_id", svy_model.l1_id);
            itmJson1.setProperty("l2_id", svy_model.l2_id);
            itmJson1.setProperty("l3_id", svy_model.l3_id);
            itmJson1 = itmJson1.apply("in_meeting_user_options2");
            itmReturn.setProperty("in_level_json", itmJson1.getProperty("json", ""));


            var itmMUser = FindMeetingUser(cfg, cfg.muid, "");
            itmReturn.setProperty("in_current_org", itmMUser.getProperty("in_current_org", ""));
            itmReturn.setProperty("in_name", itmMUser.getProperty("in_name", ""));
            itmReturn.setProperty("in_l1", itmMUser.getProperty("in_l1", ""));
            itmReturn.setProperty("in_l2", itmMUser.getProperty("in_l2", ""));
            itmReturn.setProperty("in_l3", itmMUser.getProperty("in_l3", ""));
            itmReturn.setProperty("in_index", itmMUser.getProperty("in_index", ""));
            itmReturn.setProperty("in_team", itmMUser.getProperty("in_team", ""));
            itmReturn.setProperty("in_expense", itmMUser.getProperty("in_expense", ""));
        }

        private void FindOrgPlayer(TConfig cfg, Item itmReturn)
        {
            var items = FindMeetingOrgUser(cfg, cfg.in_name);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                item.setType("inn_muser");
                item.setProperty("no", (i + 1).ToString());
                itmReturn.addRelationship(item);
            }
        }

        private void FindPlayer(TConfig cfg, Item itmReturn)
        {
            var items = FindMeetingUser(cfg, "", cfg.in_name);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                item.setType("inn_muser");
                item.setProperty("no", (i + 1).ToString());
                itmReturn.addRelationship(item);
            }
        }

        private Item FindMeetingOrgUser(TConfig cfg, string in_current_org)
        {
            var cond = "AND t1.in_current_org LIKE N'%" + in_current_org + "%'";

            var sql = @"
                SELECT 
	                t1.id
	                , t1.in_stuff_b1
	                , t1.in_current_org
	                , t1.in_short_org
	                , t1.in_show_org
	                , t1.in_name
	                , t1.in_sno
	                , t1.in_l1
	                , t1.in_l2
	                , t1.in_l3
	                , t1.in_index
	                , t1.in_team
	                , t1.in_paynumber
	                , t1.in_creator
	                , t1.in_creator_sno
	                , t2.in_password_plain
	                , t3.pay_bool
                FROM 
	                IN_MEETING_USER t1 WITH(NOLOCK)
                INNER JOIN
	                IN_RESUME t2 WITH(NOLOCK) ON t2.in_sno = t1.in_creator_sno
                LEFT OUTER JOIN
	                IN_MEETING_PAY t3 WITH(NOLOCK) ON t3.item_number = t1.in_paynumber
				LEFT OUTER JOIN
					VU_MEETING_SVY_L3 t4
					ON t4.source_id = t1.source_id
					AND t4.in_grand_filter = t1.in_l1
					AND t4.in_filter = t1.in_l2
					AND t4.in_value = t1.in_l3
                WHERE 
	                t1.source_id = '{#meeting_id}'
	                {#cond}
                ORDER BY
	                t1.in_stuff_b1
	                , t1.in_current_org
	                , t4.sort_order
	                , t1.in_team
	                , t1.in_name
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#cond}", cond);

            return cfg.inn.applySQL(sql);
        }
        private Item FindMeetingUser(TConfig cfg, string muid, string in_name)
        {
            var cond = muid == ""
                ? "AND t1.in_name LIKE N'%" + in_name + "%'"
                : "AND t1.id = '" + muid + "'";

            var sql = @"
                SELECT 
	                t1.id
	                , t1.in_stuff_b1
	                , t1.in_current_org
	                , t1.in_short_org
	                , t1.in_show_org
	                , t1.in_name
	                , t1.in_sno
	                , t1.in_l1
	                , t1.in_l2
	                , t1.in_l3
	                , t1.in_index
	                , t1.in_team
	                , t1.in_paynumber
	                , t1.in_creator
	                , t1.in_creator_sno
                    , t1.in_expense
	                , t2.in_password_plain
	                , t3.pay_bool
                FROM 
	                IN_MEETING_USER t1 WITH(NOLOCK)
                INNER JOIN
	                IN_RESUME t2 WITH(NOLOCK) ON t2.in_sno = t1.in_creator_sno
                LEFT OUTER JOIN
	                IN_MEETING_PAY t3 WITH(NOLOCK) ON t3.item_number = t1.in_paynumber
                WHERE 
	                t1.source_id = '{#meeting_id}'
	                {#cond}
                ORDER BY
	                t1.in_creator_sno
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#cond}", cond);

            return cfg.inn.applySQL(sql);
        }

        private void FixName(TConfig cfg, Item itmReturn)
        {
            var muid = itmReturn.getProperty("muid", "");
            var new_name = itmReturn.getProperty("new_name", "");
            if (muid == "") throw new Exception("查無與會者 id");
            if (new_name == "") throw new Exception("請輸入修正後姓名");

            var itmOldMUser = cfg.inn.applySQL("SELECT * FROM IN_MEETING_USER WITH(NOLOCK) WHERE id = '" + muid + "'");
            if (itmOldMUser.isError() || itmOldMUser.getResult() == "") throw new Exception("查無與會者資料");

            var itmProgram = GetProgramItem(cfg, itmOldMUser);
            if (itmProgram.isError() || itmProgram.getResult() == "")
            {
                //throw new Exception("查無組別資料");
                itmProgram = cfg.inn.newItem();
            }

            var row = new TFixRow
            {
                muid = muid,
                new_name = new_name,
                old_name = itmOldMUser.getProperty("in_name", ""),
                itmOldMUser = itmOldMUser,
                itmProgram = itmProgram,
            };

            MUserFixName(cfg, row);

            PTeamFIxName2(cfg, row);
        }

        private void MUserFixName(TConfig cfg, TFixRow row)
        {
            //日誌
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "與會者姓名修正"
                + ": meeting_id = " + cfg.meeting_id
                + ": muid = " + row.muid
                + ", old name = " + row.old_name
                + ", new_name = " + row.new_name);


            string sql = "UPDATE IN_MEETING_USER SET in_name = N'" + row.new_name + "' WHERE id = '" + row.muid + "'";

            cfg.inn.applySQL(sql);
        }

        private void PTeamFIxName1(TConfig cfg, TFixRow row)
        {
            var program_id = row.itmProgram.getProperty("id", "");
            if (program_id == "") return;

            var itmPTeam = GetProgramTeamItem(cfg, row.itmOldMUser, row.itmProgram);
            //if (itmPTeam.isError() || itmPTeam.getResult() == "") throw new Exception("查無比賽隊伍資料");

            if (itmPTeam.isError() || itmPTeam.getResult() == "") return;

            var team_id = itmPTeam.getProperty("id", "");

            var sql = "UPDATE IN_MEETING_PTEAM SET"
                + "  in_name = REPLACE(in_name, N'" + row.old_name + "', N'" + row.new_name + "')"
                + ", in_names = REPLACE(in_names, N'" + row.old_name + "', N'" + row.new_name + "')"
                + " WHERE id = '" + team_id + "'";

            cfg.inn.applySQL(sql);
        }

        private void PTeamFIxName2(TConfig cfg, TFixRow row)
        {
            var program_id = row.itmProgram.getProperty("id", "");
            if (program_id == "") return;

            var in_l1 = row.itmOldMUser.getProperty("in_l1", "");
            var in_l2 = row.itmOldMUser.getProperty("in_l2", "");
            var in_l3 = row.itmOldMUser.getProperty("in_l3", "");
            var in_index = row.itmOldMUser.getProperty("in_index", "");
            var in_team = row.itmOldMUser.getProperty("in_team", "").Trim();
            var in_creator_sno = row.itmOldMUser.getProperty("in_creator_sno", "");

            var is_team = in_l1.Contains("團體");

            var sql_qry = @"
                SELECT
                    in_name
                    , in_waiting_list
                FROM
                    IN_MEETING_USER t1 WITH(NOLOCK)
                WHERE
                    t1.source_id = '{#meeting_id}'
                    AND ISNULL(t1.in_l1, '') = N'{#in_l1}'
                    AND ISNULL(t1.in_l2, '') = N'{#in_l2}'
                    AND ISNULL(t1.in_l3, '') = N'{#in_l3}'
                    AND ISNULL(t1.in_index, '') = '{#in_index}'
                    AND ISNULL(t1.in_creator_sno, '') = '{#in_creator_sno}'
                ORDER BY
                    t1.in_stuff_b1
                    , t1.in_team
                    , t1.in_sno
            ";

            sql_qry = sql_qry.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_l1}", in_l1)
                .Replace("{#in_l2}", in_l2)
                .Replace("{#in_l3}", in_l3)
                .Replace("{#in_index}", in_index)
                .Replace("{#in_creator_sno}", in_creator_sno)
                ;

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql_qry);

            var itmMUsers = cfg.inn.applySQL(sql_qry);
            if (itmMUsers.isError() || itmMUsers.getResult() == "") return;

            var count = itmMUsers.getItemCount();
            var in_name = "";
            var in_names = "";

            if (!is_team)
            {
                if (count != 1)
                {
                    throw new Exception("與會者資料錯誤");
                }
                else
                {
                    in_name = itmMUsers.getProperty("in_name", "");
                    in_names = in_name;
                }
            }
            else
            {
                var names = NameList(cfg, itmMUsers);
                var split = ", ";
                in_names = string.Join(split, names);

                if (in_team != "")
                {
                    in_name = in_team;
                    in_names = in_names + " (" + in_team + "隊)";
                }
                else
                {
                    in_name = "";
                }
            }

            var sql = "UPDATE IN_MEETING_PTEAM SET"
                + "  in_name = N'" + in_name + "'"
                + ", in_names = N'" + in_names + "'"
                + " WHERE source_id = '" + program_id + "'"
                + " AND in_index = '" + in_index + "'"
                + " AND in_creator_sno = '" + in_creator_sno + "'"
                + " AND ISNULL(in_type, '') <> 's'"
                ;

            cfg.inn.applySQL(sql);
        }

        private List<string> NameList(TConfig cfg, Item itmMUsers)
        {
            var result = new List<string>();
            var count = itmMUsers.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var itmMUser = itmMUsers.getItemByIndex(i);
                var in_name = itmMUser.getProperty("in_name", "").Trim();
                var in_waiting_list = itmMUser.getProperty("in_waiting_list", "");
                if (in_name == "") continue;

                if (in_waiting_list.Contains("備"))
                {
                    in_name = in_name + "[備]";
                }

                result.Add(in_name);
            }
            return result;
        }

        private Item GetProgramTeamItem(TConfig cfg, Item itmOldMUser, Item itmProgram)
        {
            var program_id = itmProgram.getProperty("id", "");
            var in_index = itmOldMUser.getProperty("in_index", "");
            var in_creator_sno = itmOldMUser.getProperty("in_creator_sno", "");
            var old_name = itmOldMUser.getProperty("in_name", "");

            string sql = @"
                SELECT 
	                * 
                FROM 
	                IN_MEETING_PTEAM WITH(NOLOCK) 
                WHERE 
	                source_id = '{#program_id}' 
	                AND in_index = '{#in_index}'
	                AND in_creator_sno = N'{#in_creator_sno}'
					AND ISNULL(in_type, '') <> 's'
					AND in_names LIKE N'%{#old_name}%'
            ";

            sql = sql.Replace("{#program_id}", program_id)
                .Replace("{#in_index}", in_index)
                .Replace("{#in_creator_sno}", in_creator_sno)
                .Replace("{#old_name}", old_name);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        private Item GetProgramItem(TConfig cfg, Item itmOldMUser)
        {
            var in_l1 = itmOldMUser.getProperty("in_l1", "");
            var in_l2 = itmOldMUser.getProperty("in_l2", "");
            var in_l3 = itmOldMUser.getProperty("in_l3", "");

            string sql = @"
                SELECT 
	                * 
                FROM 
	                IN_MEETING_PROGRAM WITH(NOLOCK) 
                WHERE 
	                in_meeting = '{#meeting_id}' 
	                AND in_l1 = N'{#in_l1}'
	                AND in_l2 = N'{#in_l2}'
	                AND in_l3 = N'{#in_l3}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_l1}", in_l1)
                .Replace("{#in_l2}", in_l2)
                .Replace("{#in_l3}", in_l3);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        private void Merge(TConfig cfg, Item itmReturn)
        {
            var svy_model = GetMtSvy(cfg);

            string in_l1 = itmReturn.getProperty("in_l1", "");
            string in_l2 = itmReturn.getProperty("in_l2", "");
            string in_l3 = itmReturn.getProperty("in_l3", "");

            string new_in_l2 = itmReturn.getProperty("new_in_l2", "");
            string new_in_l3 = itmReturn.getProperty("new_in_l3", "");
            string ids = itmReturn.getProperty("ids", "");

            string sql = @"
                SELECT 
	                sort_order 
                FROM 
	                IN_SURVEY_OPTION WITH(NOLOCK) 
                WHERE 
	                source_id = '{#l3_id}' 
	                AND in_grand_filter = N'{#in_l1}'
	                AND in_filter = N'{#in_l2}'
	                AND in_value = N'{#in_l3}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_l1}", in_l1)
                .Replace("{#in_l2}", in_l2)
                .Replace("{#in_l3}", in_l3)
                .Replace("{#l3_id}", svy_model.l3_id);

            Item itmOldOpt = cfg.inn.applySQL(sql);
            if (itmOldOpt.isError() || itmOldOpt.getResult() == "")
            {
                itmOldOpt = cfg.inn.newItem();
                itmOldOpt.setProperty("sort_order", "");
            }

            sql = @"
                UPDATE IN_MEETING_USER SET 
	                  in_l2_old = in_l2
	                , in_l3_old = in_l3
                WHERE
	                source_id = '{#meeting_id}'
	                AND id IN ({#ids})
	                AND ISNULL(in_l1, '') = N'{#in_l1}'
	                AND ISNULL(in_l2, '') = N'{#in_l2}'
	                AND ISNULL(in_l3, '') = N'{#in_l3}'
	                AND ISNULL(in_l2_old, '') = ''
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#ids}", ids)
                .Replace("{#in_l1}", in_l1)
                .Replace("{#in_l2}", in_l2)
                .Replace("{#in_l3}", in_l3);

            cfg.inn.applySQL(sql);

            sql = @"
                UPDATE IN_MEETING_USER SET 
	                  in_l2 = N'{#new_in_l2}'
	                , in_l3 = N'{#new_in_l3}'
                WHERE
	                source_id = '{#meeting_id}'
	                AND id IN ({#ids})
	                AND ISNULL(in_l1, '') = N'{#in_l1}'
	                AND ISNULL(in_l2, '') = N'{#in_l2}'
	                AND ISNULL(in_l3, '') = N'{#in_l3}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#ids}", ids)
                .Replace("{#in_l1}", in_l1)
                .Replace("{#in_l2}", in_l2)
                .Replace("{#in_l3}", in_l3)
                .Replace("{#new_in_l2}", new_in_l2)
                .Replace("{#new_in_l3}", new_in_l3);

            cfg.inn.applySQL(sql);

            sql = "SELECT id FROM In_Survey_Option WITH(NOLOCK) WHERE source_id = '" + svy_model.l3_id + "' AND in_value = N'" + new_in_l3 + "'";
            var itmExists = cfg.inn.applySQL(sql);
            if (itmExists.getResult() == "")
            {
                var str_sort_order = itmOldOpt.getProperty("sort_order", "");
                var int_sort_order = GetInt(str_sort_order);

                //新增至 IN_SURVEY_OPTION
                Item itmNew = cfg.inn.newItem("In_Survey_Option", "merge");
                itmNew.setAttribute("where", "source_id = '" + svy_model.l3_id + "' AND in_value = '" + new_in_l3 + "'");
                itmNew.setProperty("source_id", svy_model.l3_id);
                itmNew.setProperty("in_label", new_in_l3);
                itmNew.setProperty("in_value", new_in_l3);
                itmNew.setProperty("in_filter", new_in_l2);
                itmNew.setProperty("in_grand_filter", in_l1);
                if (int_sort_order > 0)
                {
                    itmNew.setProperty("sort_order", (int_sort_order + 50).ToString());
                }
                itmNew.apply();
            }
        }

        private void Players(TConfig cfg, Item itmReturn)
        {
            string in_l1 = itmReturn.getProperty("in_l1", "");
            string in_l2 = itmReturn.getProperty("in_l2", "");
            string in_l3 = itmReturn.getProperty("in_l3", "");

            string sql = @"
                SELECT
	                id
	                , in_name
	                , in_gender
	                , in_current_org
	                , in_team
	                , in_l1
	                , in_l2
	                , in_l3
	                , in_index
	                , in_l2_old
	                , in_l3_old
                FROM
	                IN_MEETING_USER WITH(NOLOCK)
                WHERE
	                source_id = '{#meeting_id}'
	                AND ISNULL(in_l1, '') = N'{#in_l1}'
	                AND ISNULL(in_l2, '') = N'{#in_l2}'
	                AND ISNULL(in_l3, '') = N'{#in_l3}'
                ORDER BY
                    in_l1
                    , in_l2_old
                    , in_l3_old
	                , in_current_org
	                , in_team
	                , in_sno
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_l1}", in_l1)
                .Replace("{#in_l2}", in_l2)
                .Replace("{#in_l3}", in_l3);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                item.setType("inn_player");
                item.setProperty("no", (i + 1).ToString());
                itmReturn.addRelationship(item);
            }
        }

        private void Query(TConfig cfg, Item itmReturn)
        {
            string condition = itmReturn.getProperty("condition", "");
            string in_l1 = itmReturn.getProperty("in_l1", "");
            string in_l2 = itmReturn.getProperty("in_l2", "");
            string in_l3 = itmReturn.getProperty("in_l3", "");
            string query_org = itmReturn.getProperty("query_org", "");
            string query_name = itmReturn.getProperty("query_name", "");

            string order_by1 = "t1.in_current_org, t1.in_name, t1.in_l1, t1.in_l2, t1.in_l3";
            string order_by2 = "t1.in_l1, t1.in_l2, t1.in_l3, t1.in_current_org, t1.in_team, t1.in_sno";
            string order_by = order_by2;

            var list = new List<string>();
            if (condition == "lv")
            {
                list.Add("AND ISNULL(t1.in_l1, '') = N'" + in_l1 + "'");
                list.Add("AND ISNULL(t1.in_l2, '') = N'" + in_l2 + "'");
                list.Add("AND ISNULL(t1.in_l3, '') = N'" + in_l3 + "'");
            }
            else
            {
                if (query_org != "")
                {
                    list.Add("AND (t1.in_current_org LIKE '%' + N'" + query_org + "' + '%' OR t1.in_show_org LIKE '%' + N'" + query_org + "' + '%')");
                }
                if (query_name != "")
                {
                    list.Add("AND (t1.in_name LIKE '%' + N'" + query_name + "' + '%' OR t1.in_sno = '" + query_name + "')");
                    order_by = order_by1;
                }
            }

            string sql = @"
                SELECT
	                t1.id
	                , t1.in_name
	                , t1.in_gender
	                , t1.in_stuff_b1
	                , t1.in_current_org
	                , t1.in_short_org
	                , t1.in_show_org
	                , t1.in_team
	                , t1.in_l1
	                , t1.in_l2
	                , t1.in_l3
	                , t1.in_index
	                , t1.in_creator
	                , t1.in_creator_sno
	                , ISNULL(t1.in_paynumber, '') AS 'in_paynumber'
	                , '***' AS 'in_password_plain'
	                , t3.pay_bool
                FROM
	                IN_MEETING_USER t1 WITH(NOLOCK)
                INNER JOIN
	                IN_RESUME t2 WITH(NOLOCK) ON t2.in_sno = t1.in_creator_sno
	            LEFT OUTER JOIN
	                IN_MEETING_PAY t3 WITH(NOLOCK)
	                ON  t3.in_meeting = t1.source_id
	                AND t3.item_number = t1.in_paynumber
                WHERE
	                t1.source_id = '{#meeting_id}'
	                {#cond_list}
                ORDER BY
	                {#order_by}
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#cond_list}", string.Join(" ", list))
                .Replace("{#order_by}", order_by);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var in_current_org = item.getProperty("in_current_org", "");
                var in_show_org = item.getProperty("in_show_org", "");
                if (in_show_org != "")
                {
                    item.setProperty("in_current_org", in_show_org + " / " + in_current_org);
                }

                var _in_l2 = item.getProperty("in_l2", "");
                _in_l2 = _in_l2.Replace("個-", "").Replace("團-", "");
                item.setProperty("in_l2", _in_l2);
                item.setType("inn_player");
                item.setProperty("no", (i + 1).ToString());
                itmReturn.addRelationship(item);
            }
        }

        //頁面
        private void Page(TConfig cfg, Item itmReturn)
        {
            cfg.itmMeeting = cfg.inn.applySQL("SELECT id, in_title FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");

            //活動資訊
            AppendMeeting(cfg, itmReturn);

            var svy_model = GetMtSvy(cfg);

            //附加選單資訊(查詢)
            Item itmJson1 = cfg.inn.newItem("In_Meeting");
            itmJson1.setProperty("meeting_id", cfg.meeting_id);
            itmJson1.setProperty("l1_id", svy_model.l1_id);
            itmJson1.setProperty("l2_id", svy_model.l2_id);
            itmJson1.setProperty("l3_id", svy_model.l3_id);
            itmJson1 = itmJson1.apply("in_meeting_user_options");
            itmReturn.setProperty("in_level_json", itmJson1.getProperty("json", ""));
        }

        //活動資訊
        private void AppendMeeting(TConfig cfg, Item itmReturn)
        {
            itmReturn.setProperty("in_title", cfg.itmMeeting.getProperty("in_title", ""));
        }

        private TMtSvy GetMtSvy(TConfig cfg)
        {
            TMtSvy result = new TMtSvy { l1_id = "", l2_id = "", l3_id = "", expense_lv = "" };

            string sql = @"
                SELECT
	                t2.id
	                , t2.in_property
	                , t2.in_questions
	                , t2.in_expense
                FROM
	                IN_MEETING_SURVEYS t1 WITH(NOLOCK)
                INNER JOIN
	                IN_SURVEY t2 WITH(NOLOCK)
	                ON t2.id = t1.related_id
                WHERE
	                t1.source_id = '{#meeting_id}'
	                AND t2.in_property IN ('in_l1', 'in_l2', 'in_l3')
                ORDER BY
	                t2.in_property 
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string id = item.getProperty("id", "");
                string in_property = item.getProperty("in_property", "");
                string in_expense = item.getProperty("in_expense", "");
                switch (in_property)
                {
                    case "in_l1": result.l1_id = id; break;
                    case "in_l2": result.l2_id = id; break;
                    case "in_l3": result.l3_id = id; break;
                }
                if (in_expense == "1")
                {
                    result.expense_lv = in_property;
                }
            }
            return result;
        }


        private string GetExpense(TMtSvy svy_model, Item itmLv1, Item itmLv2, Item itmLv3)
        {
            switch (svy_model.expense_lv)
            {
                case "in_l1": return itmLv1.getProperty("in_expense_value", "0");
                case "in_l2": return itmLv2.getProperty("in_expense_value", "0");
                case "in_l3": return itmLv3.getProperty("in_expense_value", "0");
                default: return "0";
            }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }

            public string meeting_id { get; set; }
            public string muid { get; set; }
            public string in_name { get; set; }
            public string scene { get; set; }

            public Item itmMeeting { get; set; }
        }

        private class TFixRow
        {
            public string muid { get; set; }
            public string new_name { get; set; }
            public string old_name { get; set; }
            public Item itmOldMUser { get; set; }
            public Item itmProgram { get; set; }
        }

        private class TMtSvy
        {
            public string l1_id { get; set; }
            public string l2_id { get; set; }
            public string l3_id { get; set; }

            public string expense_lv { get; set; }
        }

        private int GetInt(string value)
        {
            if (value == "") return 0;
            var result = 0;
            if (int.TryParse(value, out result))
            {
                return result;
            }
            return 0;
        }

        private DateTime GetDtm(string value)
        {
            if (value == "") return DateTime.MinValue;
            var result = DateTime.MinValue;
            if (DateTime.TryParse(value, out result))
            {
                return result;
            }
            return DateTime.MinValue;
        }
    }
}