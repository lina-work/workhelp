﻿using Aras.IOM;
using Aras.Server.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WorkHelp.ArasDesk.Methods.Judo.Register
{
    public class In_Meeting_User_Statistics : Item
    {
        public In_Meeting_User_Statistics(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 統計
                日誌: 
                    - 2024-09-30: 匯出 (lina)
                    - 2023-10-27: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "In_Meeting_User_Statistics";

            var itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                meeting_id = itmR.getProperty("meeting_id", ""),
                in_fight_day = itmR.getProperty("in_fight_day", ""),
                scene = itmR.getProperty("scene", ""),
                export_type = "xlsx",
                font_name = "Source Sans Pro",
            };

            switch (cfg.scene)
            {
                case "page":
                    Page(cfg, itmR);
                    break;
                case "status_page":
                    StatusPage(cfg, itmR);
                    break;
                case "status_export":
                    StatusExport(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void StatusExport(TConfig cfg, Item itmReturn)
        {
            //賽事資訊
            AppendMeetingInfo(cfg, itmReturn);

            var source = GetDQMUserItems(cfg, itmReturn);
            var items = DistinctMeetingUserItems(cfg, source);
            var pack = MapPack(cfg, items);
            var evts = GetDQEventList(cfg, items);
            var sortedEvts = evts.OrderBy(x => x.in_code_en).ThenBy(x => x.in_tree_no).ToList();

            var xls_parm_name = "";
            cfg.itmXlsx = cfg.inn.applyMethod("In_Variable_File", "<in_name>" + xls_parm_name + "</in_name>");
            cfg.template_path = cfg.itmXlsx.getProperty("template_path", "");
            cfg.export_path = cfg.itmXlsx.getProperty("export_path", "");

            cfg.head_color = System.Drawing.Color.FromArgb(192, 230, 245);
            cfg.male_color = System.Drawing.Color.FromArgb(218, 242, 208);
            cfg.mix_color = System.Drawing.Color.FromArgb(234, 206, 255);

            var book = new Spire.Xls.Workbook();
            AppendWeightStatusSectSheet(cfg, book, pack);
            AppendWeightStatusOrgSheet(cfg, book, pack);
            AppendWeightEventsSheet(cfg, book, sortedEvts);

            if (book.Worksheets.Count > 1) book.Worksheets[0].Remove();
            if (book.Worksheets.Count > 1) book.Worksheets[0].Remove();
            if (book.Worksheets.Count > 1) book.Worksheets[0].Remove();

            //匯出檔名
            var xlsName = cfg.itmMeeting.getProperty("in_title", "")
                + "_未過磅名單"
                + "_" + itmReturn.getProperty("in_fight_day", "")
                + "_" + System.DateTime.Now.ToString("HHmmss") + "";

            xlsName = xlsName.Replace("+", "")
                            .Replace("-", "")
                            .Replace(".", "")
                            .Replace(" ", "");

            string extName = cfg.export_type != "" && cfg.export_type.ToLower() == "pdf" ? ".pdf" : ".xlsx";
            string xls_file = cfg.export_path + xlsName + extName;
            string xls_name = xlsName + extName;

            if (!System.IO.Directory.Exists(cfg.export_path))
            {
                System.IO.Directory.CreateDirectory(cfg.export_path);
            }

            //儲存檔案          
            if (extName == ".pdf")
            {
                book.SaveToFile(xls_file, Spire.Xls.FileFormat.PDF);
            }
            else
            {
                book.SaveToFile(xls_file, Spire.Xls.ExcelVersion.Version2013);
            }

            //下載路徑
            itmReturn.setProperty("xls_name", xls_name);
        }

        private void AppendWeightEventsSheet(TConfig cfg, Spire.Xls.Workbook book, List<TEvt> evts)
        {
            if (evts.Count == 0) return;

            var sheet = book.CreateEmptySheet();
            sheet.Name = "依場次";

            //將所有欄放入單一頁面
            sheet.PageSetup.FitToPagesWide = 1;
            sheet.PageSetup.FitToPagesTall = 0;

            sheet.PageSetup.TopMargin = 0.4;
            sheet.PageSetup.LeftMargin = 0.3;
            sheet.PageSetup.RightMargin = 0.3;
            sheet.PageSetup.BottomMargin = 0.6;

            var titlePos = "A1:I1";
            var titleTxt = cfg.mt_title + " 未過磅名單";
            SetStrCell(cfg, sheet, titlePos, titleTxt, 18, 30, true, true, HA.C);

            var fightDayPos = "A2";
            var fightDayTxt = "比賽日期：" + cfg.in_fight_day;
            SetStrCell(cfg, sheet, fightDayPos, fightDayTxt, 14, 0, true, false, HA.L);

            var countLabelPos = "E2";
            var countLabelTxt = "場次數：" + evts.Count.ToString();
            SetStrCell(cfg, sheet, countLabelPos, countLabelTxt, 14, 0, true, false, HA.L);

            var weightLabelPos = "I2";
            var weightLabelTxt = "過磅日期：" + GetWeightDay(cfg);
            SetStrCell(cfg, sheet, weightLabelPos, weightLabelTxt, 14, 0, true, false, HA.R);

            var ri = 3;

            var lastSite = "";
            for (var i = 0; i < evts.Count; i++)
            {
                var evt = evts[i];
                if (evt.in_code_en != lastSite)
                {
                    lastSite = evt.in_code_en;
                    SetStrCell(cfg, sheet, "A" + ri, "No.", 14, 0, true, false, HA.C, CT.Head);
                    SetStrCell(cfg, sheet, "B" + ri, "比賽日期", 14, 0, true, false, HA.C, CT.Head);
                    SetStrCell(cfg, sheet, "C" + ri, "場次", 14, 0, true, false, HA.C, CT.Head);
                    SetStrCell(cfg, sheet, "D" + ri, "競賽組別", 14, 0, true, false, HA.C, CT.Head);
                    SetStrCell(cfg, sheet, "E" + ri, "競賽項目", 14, 0, true, false, HA.C, CT.Head);
                    SetStrCell(cfg, sheet, "F" + ri, "競賽量級", 14, 0, true, false, HA.C, CT.Head);
                    SetStrCell(cfg, sheet, "G" + ri, "白方", 14, 0, true, false, HA.C, CT.Head);
                    SetStrCell(cfg, sheet, "H" + ri, "vs", 14, 0, true, false, HA.C, CT.Head);
                    SetStrCell(cfg, sheet, "I" + ri, "藍方", 14, 0, true, false, HA.C, CT.Head);
                    ri++;
                }

                var siteNo = evt.in_code_en + evt.in_tree_no;
                SetStrCell(cfg, sheet, "A" + ri, (i + 1).ToString(), 12, 0, false, false, HA.C);
                SetStrCell(cfg, sheet, "B" + ri, evt.in_fight_day, 12, 0, false, false, HA.C);
                SetStrCell(cfg, sheet, "C" + ri, siteNo, 12, 0, false, false, HA.C);
                SetStrCell(cfg, sheet, "D" + ri, evt.in_l1, 12, 0, false, false, HA.C);
                SetStrCell(cfg, sheet, "E" + ri, evt.sect, 12, 0, false, false, HA.C);
                SetStrCell(cfg, sheet, "F" + ri, evt.in_l3, 12, 0, false, false, HA.C);
                SetStrCell(cfg, sheet, "G" + ri, GetFootInfoB(cfg, evt, evt.f1), 12, 0, false, false, HA.C);
                SetStrCell(cfg, sheet, "H" + ri, "vs", 12, 0, false, false, HA.C);
                SetStrCell(cfg, sheet, "I" + ri, GetFootInfoB(cfg, evt, evt.f2), 12, 0, false, false, HA.C);
                ri++;
            }

            sheet.Columns[0].ColumnWidth = 5;
            sheet.Columns[1].ColumnWidth = 12;
            sheet.Columns[2].ColumnWidth = 8;
            sheet.Columns[3].ColumnWidth = 12;
            sheet.Columns[4].ColumnWidth = 12;
            sheet.Columns[5].ColumnWidth = 24;
            sheet.Columns[6].ColumnWidth = 20;
            sheet.Columns[7].ColumnWidth = 5;
            sheet.Columns[8].ColumnWidth = 20;

            SetRangeBorder(sheet, "A3" + ":" + "I" + (ri - 1));

            sheet.PageSetup.PrintTitleRows = "$1:$2";
            sheet.PageSetup.CenterHorizontally = true;
        }

        private void AppendWeightStatusSectSheet(TConfig cfg, Spire.Xls.Workbook book, TPack pack)
        {
            var sheet = book.CreateEmptySheet();
            sheet.Name = "依組別";

            //將所有欄放入單一頁面
            sheet.PageSetup.FitToPagesWide = 1;
            sheet.PageSetup.FitToPagesTall = 0;

            sheet.PageSetup.TopMargin = 0.4;
            sheet.PageSetup.LeftMargin = 0.3;
            sheet.PageSetup.RightMargin = 0.3;
            sheet.PageSetup.BottomMargin = 0.6;

            var titlePos = "A1:F1";
            var titleTxt = cfg.mt_title + " 未過磅名單";
            SetStrCell(cfg, sheet, titlePos, titleTxt, 18, 30, true, true, HA.C);

            var fightDayPos = "A2";
            var fightDayTxt = "比賽日期：" + cfg.in_fight_day;
            SetStrCell(cfg, sheet, fightDayPos, fightDayTxt, 14, 0, true, false, HA.L);

            var countLabelPos = "C2";
            var countLabelTxt = "人數";
            SetStrCell(cfg, sheet, countLabelPos, countLabelTxt, 14, 0, true, false, HA.R);

            var countValuePos = "D2";
            var countValueTxt = pack.total.ToString();
            SetStrCell(cfg, sheet, countValuePos, countValueTxt, 14, 0, true, false, HA.L);

            var weightLabelPos = "E2";
            var weightLabelTxt = "過磅日期：";
            SetStrCell(cfg, sheet, weightLabelPos, weightLabelTxt, 14, 0, true, false, HA.R);

            var weightDatePos = "F2";
            var weightDateTxt = GetWeightDay(cfg);
            SetStrCell(cfg, sheet, weightDatePos, weightDateTxt, 14, 0, true, false, HA.R);

            var ri = 3;
            SetStrCell(cfg, sheet, "A" + ri, "單位", 14, 0, true, false, HA.C, CT.Head);
            SetStrCell(cfg, sheet, "B" + ri, "選手", 14, 0, true, false, HA.C, CT.Head);
            SetStrCell(cfg, sheet, "C" + ri, "競賽組別", 14, 0, true, false, HA.C, CT.Head);
            SetStrCell(cfg, sheet, "D" + ri, "競賽項目", 14, 0, true, false, HA.C, CT.Head);
            SetStrCell(cfg, sheet, "E" + ri, "競賽量級", 14, 0, true, false, HA.C, CT.Head);
            SetStrCell(cfg, sheet, "F" + ri, "狀態", 14, 0, true, false, HA.C, CT.Head);
            ri++;

            for (var i = 0; i < pack.sects.Count; i++)
            {
                var sect = pack.sects[i];
                var sorted = sect.players.OrderBy(x => x.in_stuff_b1).ToList();
                for (var j = 0; j < sorted.Count; j++)
                {
                    var player = sorted[j];
                    if (player.id == "") continue;
                    SetStrCell(cfg, sheet, "A" + ri, player.map_short_org, 12, 0, false, false, HA.C, player.color);
                    SetStrCell(cfg, sheet, "B" + ri, player.in_name, 12, 0, false, false, HA.C, player.color);
                    SetStrCell(cfg, sheet, "C" + ri, player.in_l1, 12, 0, false, false, HA.C, player.color);
                    SetStrCell(cfg, sheet, "D" + ri, player.sect_name, 12, 0, false, false, HA.C, player.color);
                    SetStrCell(cfg, sheet, "E" + ri, player.in_l3, 12, 0, false, false, HA.C, player.color);
                    SetStrCell(cfg, sheet, "F" + ri, player.weight_status_label, 12, 0, false, false, HA.C, player.color);
                    ri++;
                }
            }

            sheet.Columns[0].ColumnWidth = 24;
            sheet.Columns[1].ColumnWidth = 20;
            sheet.Columns[2].ColumnWidth = 12;
            sheet.Columns[3].ColumnWidth = 12;
            sheet.Columns[4].ColumnWidth = 24;
            sheet.Columns[5].ColumnWidth = 14;

            SetRangeBorder(sheet, "A3" + ":" + "F" + (ri - 1));

            sheet.PageSetup.PrintTitleRows = "$1:$2";
            sheet.PageSetup.CenterHorizontally = true;
        }

        private void AppendWeightStatusOrgSheet(TConfig cfg, Spire.Xls.Workbook book, TPack pack)
        {
            var sheet = book.CreateEmptySheet();
            sheet.Name = "依單位";

            //將所有欄放入單一頁面
            sheet.PageSetup.FitToPagesWide = 1;
            sheet.PageSetup.FitToPagesTall = 0;

            sheet.PageSetup.TopMargin = 0.4;
            sheet.PageSetup.LeftMargin = 0.3;
            sheet.PageSetup.RightMargin = 0.3;
            sheet.PageSetup.BottomMargin = 0.6;

            var titlePos = "A1:F1";
            var titleTxt = cfg.mt_title + " 未過磅名單";
            SetStrCell(cfg, sheet, titlePos, titleTxt, 18, 30, true, true, HA.C);

            var fightDayPos = "A2";
            var fightDayTxt = "比賽日期：" + cfg.in_fight_day;
            SetStrCell(cfg, sheet, fightDayPos, fightDayTxt, 14, 0, true, false, HA.L);

            var countLabelPos = "C2";
            var countLabelTxt = "人數";
            SetStrCell(cfg, sheet, countLabelPos, countLabelTxt, 14, 0, true, false, HA.R);

            var countValuePos = "D2";
            var countValueTxt = pack.total.ToString();
            SetStrCell(cfg, sheet, countValuePos, countValueTxt, 14, 0, true, false, HA.L);

            var weightLabelPos = "E2";
            var weightLabelTxt = "過磅日期：";
            SetStrCell(cfg, sheet, weightLabelPos, weightLabelTxt, 14, 0, true, false, HA.R);

            var weightDatePos = "F2";
            var weightDateTxt = GetWeightDay(cfg);
            SetStrCell(cfg, sheet, weightDatePos, weightDateTxt, 14, 0, true, false, HA.R);

            var ri = 3;
            SetStrCell(cfg, sheet, "A" + ri, "單位", 14, 0, true, false, HA.C, CT.Head);
            SetStrCell(cfg, sheet, "B" + ri, "選手", 14, 0, true, false, HA.C, CT.Head);
            SetStrCell(cfg, sheet, "C" + ri, "競賽組別", 14, 0, true, false, HA.C, CT.Head);
            SetStrCell(cfg, sheet, "D" + ri, "競賽項目", 14, 0, true, false, HA.C, CT.Head);
            SetStrCell(cfg, sheet, "E" + ri, "競賽量級", 14, 0, true, false, HA.C, CT.Head);
            SetStrCell(cfg, sheet, "F" + ri, "狀態", 14, 0, true, false, HA.C, CT.Head);
            ri++;

            for (var i = 0; i < pack.orgs.Count; i++)
            {
                var org = pack.orgs[i];
                var sorted = org.players.OrderBy(x => x.sort_order).ToList();
                for (var j = 0; j < sorted.Count; j++)
                {
                    var player = sorted[j];
                    if (player.id == "") continue;
                    SetStrCell(cfg, sheet, "A" + ri, player.map_short_org, 12, 0, false, false, HA.C, player.color);
                    SetStrCell(cfg, sheet, "B" + ri, player.in_name, 12, 0, false, false, HA.C, player.color);
                    SetStrCell(cfg, sheet, "C" + ri, player.in_l1, 12, 0, false, false, HA.C, player.color);
                    SetStrCell(cfg, sheet, "D" + ri, player.sect_name, 12, 0, false, false, HA.C, player.color);
                    SetStrCell(cfg, sheet, "E" + ri, player.in_l3, 12, 0, false, false, HA.C, player.color);
                    SetStrCell(cfg, sheet, "F" + ri, player.weight_status_label, 12, 0, false, false, HA.C, player.color);
                    ri++;
                }
            }

            sheet.Columns[0].ColumnWidth = 24;
            sheet.Columns[1].ColumnWidth = 20;
            sheet.Columns[2].ColumnWidth = 12;
            sheet.Columns[3].ColumnWidth = 12;
            sheet.Columns[4].ColumnWidth = 24;
            sheet.Columns[5].ColumnWidth = 14;

            SetRangeBorder(sheet, "A3" + ":" + "F" + (ri - 1));

            sheet.PageSetup.PrintTitleRows = "$1:$2";
            sheet.PageSetup.CenterHorizontally = true;
        }

        private void AppendWeightStatusSectSheet(TConfig cfg, Spire.Xls.Workbook book, TPack pack, List<TOrg> orgs)
        {

        }

        private void SetStrCell(TConfig cfg, Spire.Xls.Worksheet sheet
            , string pos
            , string text
            , int size
            , int height = 0
            , bool needBold = false
            , bool needMerge = false
            , HA ha = HA.C
            , CT color = CT.None)
        {
            var range = sheet.Range[pos];
            if (needMerge) range.Merge();
            range.Text = text;
            SetUtlCell(cfg, range, size, height, needBold, ha, color);
        }

        private void SetUtlCell(TConfig cfg, Spire.Xls.CellRange range
            , int size
            , int height
            , bool needBold
            , HA ha = HA.C
            , CT color = CT.None)
        {
            range.Style.Font.FontName = cfg.font_name;
            range.Style.Font.Size = size;

            if (needBold) range.Style.Font.IsBold = true;
            if (height > 0) range.RowHeight = height;


            //range.Style.Font.Color = System.Drawing.Color.White;
            switch (color)
            {
                case CT.Head: range.Style.Color = cfg.head_color; break;
                case CT.Male: range.Style.Color = cfg.male_color; break;
                case CT.Mix: range.Style.Color = cfg.mix_color; break;
            }

            range.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;

            switch (ha)
            {
                case HA.C:
                    range.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                    break;
                case HA.L:
                    range.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Left;
                    break;
                case HA.R:
                    range.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Right;
                    break;
            }
        }

        private enum HA
        {
            L = 100,
            C = 200,
            R = 300
        }

        private enum CT
        {
            None = 1,
            Head = 100,
            Male = 200,
            Female = 300,
            Mix = 500
        }

        //設定格線
        private void SetRangeBorder(Spire.Xls.Worksheet sheet, string pos)
        {
            var range = sheet.Range[pos];
            range.BorderInside(Spire.Xls.LineStyleType.Thin, System.Drawing.Color.Black);
            range.BorderAround(Spire.Xls.LineStyleType.Thin, System.Drawing.Color.Black);
        }

        private void StatusPage(TConfig cfg, Item itmReturn)
        {
            //賽事資訊
            AppendMeetingInfo(cfg, itmReturn);

            //日期選單
            var days = GetDateMenu(cfg);
            AppendItems(cfg, days, "inn_date", "in_date_key", "in_date_key", "比賽日期", "", itmReturn);

            var source = GetDQMUserItems(cfg, itmReturn);
            var items = DistinctMeetingUserItems(cfg, source);
            var count = items.Count;
            if (count == -1) count = 0;
            if (cfg.no_data) count = 0;
            itmReturn.setProperty("player_count", count.ToString());

            var evts = GetDQEventList(cfg, items);
            var sortedEvts = evts.OrderBy(x => x.in_code_en).ThenBy(x => x.in_tree_no).ToList();

            itmReturn.setProperty("muser_table", GenerateMeetingUserTable(cfg, items, count));
            itmReturn.setProperty("event_table", GenerateMeetingEventTable(cfg, sortedEvts));
        }

        private List<TEvt> GetDQEventList(TConfig cfg, List<Item> items)
        {
            var map = new Dictionary<string, TEvt>();
            for (var i = 0; i < items.Count; i++)
            {
                var item = items[i];
                var event_id = item.getProperty("event_id", "");
                var in_sign_foot = item.getProperty("in_sign_foot", "");
                if (event_id == "") continue;

                if (!map.ContainsKey(event_id))
                {
                    var in_l2 = item.getProperty("in_l2", "");
                    var sect = GetShortSect(in_l2);
                    map.Add(event_id, new TEvt
                    {
                        event_id = event_id,
                        program_id = item.getProperty("program_id", ""),
                        in_tree_name = item.getProperty("in_tree_name", ""),
                        in_fight_id = item.getProperty("in_fight_id", ""),
                        in_l1 = item.getProperty("in_l1", ""),
                        in_l2 = in_l2,
                        in_l3 = item.getProperty("in_l3", ""),
                        sect = sect,
                        in_fight_day = item.getProperty("in_fight_day", ""),
                        in_code_en = item.getProperty("in_code_en", ""),
                        in_tree_no = GetInt(item.getProperty("in_tree_no", "0")),
                        in_site_mat = item.getProperty("in_site_mat", ""),
                    });
                }

                var evt = map[event_id];
                if (in_sign_foot == "1")
                {
                    evt.f1 = MapEvtFoot(cfg, item);
                }
                else if (in_sign_foot == "2")
                {
                    evt.f2 = MapEvtFoot(cfg, item);
                }
            }
            return map.Values.ToList();
        }

        private TFoot MapEvtFoot(TConfig cfg, Item item)
        {
            return new TFoot
            {
                in_sign_foot = item.getProperty("in_sign_foot", ""),
                in_sign_no = item.getProperty("in_sign_no", ""),
                value = item
            };
        }

        private class TEvt
        {
            public string program_id { get; set; }
            public string event_id { get; set; }

            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }
            public string sect { get; set; }
            public string in_fight_day { get; set; }
            public string in_code_en { get; set; }
            public int in_tree_no { get; set; }
            public string in_site_mat { get; set; }

            public string in_tree_name { get; set; }
            public string in_fight_id { get; set; }

            public TFoot f1 { get; set; }
            public TFoot f2 { get; set; }
        }

        private class TFoot
        {
            public string in_sign_foot { get; set; }
            public string in_sign_no { get; set; }
            public Item value { get; set; }
        }

        private string GenerateMeetingEventTable(TConfig cfg, List<TEvt> evts)
        {
            var body = new StringBuilder();

            var headCss = "class='mailbox-subject text-center bg-primary'";

            var lastSite = "";
            for (var i = 0; i < evts.Count; i++)
            {
                var evt = evts[i];
                if (evt.in_code_en != lastSite)
                {
                    lastSite = evt.in_code_en;
                    body.Append("<th " + headCss + " data-width='4%'>No.</th>");
                    body.Append("<th " + headCss + " data-width='8%'>比賽日期</th>");
                    body.Append("<th " + headCss + " data-width='8%'>場次</th>");
                    // body.Append("<th " + headCss + " data-width='46%'>競賽級組</th>");
                    body.Append("<th " + headCss + " data-width='10%'>競賽組別</th>");
                    body.Append("<th " + headCss + " data-width='10%'>競賽項目</th>");
                    body.Append("<th " + headCss + " data-width='22%'>競賽量級</th>");
                    body.Append("<th " + headCss + " data-width='4%'>MAT</th>");
                    body.Append("<th " + headCss + " data-width='16%'>白方</th>");
                    body.Append("<th " + headCss + " data-width='16%'>藍方</th>");

                }

                var siteNo = evt.in_code_en + evt.in_tree_no;
                var sectLink = evt.in_l3 + " <a href='javascript:;' onclick='GoFightTreePage(this)' data-pid='" + evt.program_id + "'><i class='fa fa-link'></i></>";

                body.Append("<tr>");
                body.Append("  <td class='text-center'> " + (i + 1) + " </td>");
                body.Append("  <td class='text-center'> " + evt.in_fight_day + " </td>");
                body.Append("  <td class='text-center'> " + siteNo + " </td>");
                // body.Append("  <td class='text-center'> " + evt.in_l1 + " " + evt.in_l2 + " " + sectLink + " </td>");
                body.Append("  <td class='text-center'> " + evt.in_l1 + " </td>");
                body.Append("  <td class='text-center'> " + evt.sect + " </td>");
                body.Append("  <td class='text-center'> " + sectLink + " </td>");
                body.Append("  <td class='text-center'> " + evt.in_site_mat + "<BR>" + evt.in_tree_no + " </td>");
                body.Append("  <td class='text-center'> " + GetFootInfoA(cfg, evt, evt.f1) + " </td>");
                body.Append("  <td class='text-center'> " + GetFootInfoA(cfg, evt, evt.f2) + " </td>");
                body.Append("</tr>");
            }

            var table_name = "event_table";

            var builder = new StringBuilder();
            builder.Append("<div class='box-body tablenobg' style='position: relative; margin-bottom: 10px'>");
            builder.AppendLine("<h3>DQ場次清單 共" + evts.Count + "場(依場地、場次排序)</h3>");
            builder.AppendLine(GetTableAttribute(table_name));
            builder.Append("<tbody>");
            builder.Append(body);
            builder.Append("</tbody>");
            builder.AppendLine("</table>");
            builder.Append("</div>");
            builder.AppendLine("<script>");
            builder.AppendLine("$('#" + table_name + "').bootstrapTable();");
            builder.AppendLine("if ($(window).width() <= 768) { $('#" + table_name + "').bootstrapTable('toggleView'); }");
            builder.AppendLine("</script>");

            return builder.ToString();
        }

        private string GetFootInfoA(TConfig cfg, TEvt evt, TFoot foot)
        {
            if (foot == null) return "";

            var item = foot.value;
            var label = GetWeightStatusLabel(item.getProperty("in_weight_status", ""));

            return "<span style='color:red'>(" + label + ")</span>" + item.getProperty("in_name", "")
                + "<BR>" + item.getProperty("map_short_org", "")
                ;
        }

        private string GetFootInfoB(TConfig cfg, TEvt evt, TFoot foot)
        {
            if (foot == null) return "";

            var item = foot.value;
            var label = GetWeightStatusLabel(item.getProperty("in_weight_status", ""));

            return "(" + label + ")" + item.getProperty("in_name", "")
                + Environment.NewLine + item.getProperty("map_short_org", "")
                ;
        }

        private List<Item> DistinctMeetingUserItems(TConfig cfg, Item items)
        {
            var map = new Dictionary<string, Item>();
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var id = item.getProperty("id", "");
                if (!map.ContainsKey(id)) map.Add(id, item);
            }
            return map.Values.ToList();
        }

        private string GenerateMeetingUserTable(TConfig cfg, List<Item> items, int count)
        {
            var head = new StringBuilder();
            var body = new StringBuilder();

            head.Append("<th class='mailbox-subject text-center' data-width='10px'>No.</th>");
            head.Append("<th class='mailbox-subject text-center' data-width='15%'>單位</th>");
            head.Append("<th class='mailbox-subject text-center' data-width='15%'>選手</th>");
            head.Append("<th class='mailbox-subject text-center' data-width='10%'>競賽組別</th>");
            head.Append("<th class='mailbox-subject text-center' data-width='10%'>競賽項目</th>");
            head.Append("<th class='mailbox-subject text-center' data-width='20%'>競賽量級</th>");
            head.Append("<th class='mailbox-subject text-center' data-width='12%'>比賽日期</th>");
            head.Append("<th class='mailbox-subject text-center' data-width='10%'>狀態</th>");
            head.Append("<th class='mailbox-subject text-center' data-width='8%'>場次</th>");

            for (var i = 0; i < count; i++)
            {
                var item = items[i];
                var player = MapPlayer(cfg, item);
                if (player.id == "") continue;
                if (player.in_weight_status == "") continue;

                var in_code_en = item.getProperty("in_code_en", "");
                var in_tree_no = item.getProperty("in_tree_no", "");
                var siteNo = in_code_en + in_tree_no;

                var sectLink = player.in_l3 + " <a href='javascript:;' onclick='GoFightTreePage(this)' data-pid='" + player.program_id + "'><i class='fa fa-link'></i></>";

                body.Append("<tr>");
                body.Append("  <td class='text-center'> " + (i + 1) + " </td>");
                body.Append("  <td class='text-center'> " + player.map_short_org + " </td>");
                body.Append("  <td class='text-center'> <span style='color: blue'>" + player.in_name + "</span> </td>");
                body.Append("  <td class='text-center'> " + player.in_l1 + " </td>");
                body.Append("  <td class='text-center'> " + player.sect_name + " </td>");
                body.Append("  <td class='text-center'> " + sectLink + " </td>");
                body.Append("  <td class='text-center'> " + player.in_fight_day + " </td>");
                body.Append("  <td class='text-center'> " + player.weight_status_label + " </td>");
                body.Append("  <td class='text-center'> " + siteNo + " </td>");
                body.Append("</tr>");
            }

            var table_name = "muser_table";

            var builder = new StringBuilder();
            builder.Append("<div class='box-body tablenobg' style='position: relative; margin-bottom: 10px'>");
            builder.AppendLine("<h3>DQ選手清單 共" + count + "人(依團體會員編號排序)</h3>");
            builder.AppendLine(GetTableAttribute(table_name));
            builder.Append("<thead>");
            builder.Append("<tr>");
            builder.Append(head);
            builder.Append("</tr>");
            builder.Append("</thead>");
            builder.Append("<tbody>");
            builder.Append(body);
            builder.Append("</tbody>");
            builder.AppendLine("</table>");
            builder.Append("</div>");
            builder.AppendLine("<script>");
            builder.AppendLine("$('#" + table_name + "').bootstrapTable();");
            builder.AppendLine("if ($(window).width() <= 768) { $('#" + table_name + "').bootstrapTable('toggleView'); }");
            builder.AppendLine("</script>");

            return builder.ToString();
        }

        private Item GetDQMUserItems(TConfig cfg, Item itmReturn)
        {
            var in_fight_day = itmReturn.getProperty("in_fight_day", "");
            var in_weight_status = itmReturn.getProperty("in_weight_status", "");

            if (in_fight_day == "" && in_weight_status == "")
            {
                cfg.no_data = true;
                return cfg.inn.newItem();
            }

            var cond_day = in_fight_day == ""
                ? ""
                : "AND t2.in_fight_day = '" + in_fight_day + "'";

            var cond_status = "";
            switch (in_weight_status)
            {
                case "all":
                    cond_status = "";
                    break;

                case "leave"://請假
                    cond_status = "AND ISNULL(t1.in_weight_status, '') = 'leave'";
                    break;

                case "dq"://DQ
                    cond_status = "AND ISNULL(t1.in_weight_status, '') = 'dq'";
                    break;

                case "off"://未到
                    cond_status = "AND ISNULL(t1.in_weight_status, '') = 'off'";
                    break;

                case "giveup"://棄賽
                    cond_status = "AND ISNULL(t1.in_weight_status, '') = 'giveup'";
                    break;

                case "injuried"://傷棄
                    cond_status = "AND ISNULL(t1.in_weight_status, '') = 'injuried'";
                    break;

                case "bypass_leave"://請假以外的異常
                    cond_status = "AND ISNULL(t1.in_weight_status, '') <> 'leave'";
                    break;

                case "bypass_giveup"://請假與DQ以外的異常
                    cond_status = "AND ISNULL(t1.in_weight_status, '') NOT IN ('leave', 'dq')";
                    break;
            }

            var sql = @"
                SELECT 
                	t1.id
                	, t1.in_stuff_b1
                	, t1.in_current_org
                	, t1.in_name
                	, t1.in_l1
                	, t1.in_l2
                	, t1.in_l3
                	, t1.in_sign_no
                	, t1.in_draw_no
                	, t1.in_weight_result
                	, t1.in_weight_status
                	, t2.id AS 'pg_id'
                	, t2.in_fight_day
                	, t2.in_short_name
                	, t2.in_sort_order
                	, t2.in_site_mat
					, t3.in_short_org AS 'map_short_org'
					, t11.*
                FROM 
                	IN_MEETING_USER t1 WITH(NOLOCK) 
				INNER JOIN
					IN_MEETING_PROGRAM t2 WITH(NOLOCK) 
					ON t2.in_meeting = t1.source_id
					AND t2.in_l1 = t1.in_l1
					AND t2.in_l2 = t1.in_l2
					AND t2.in_l3 = t1.in_l3
				LEFT OUTER JOIN
				    IN_ORG_MAP t3 WITH(NOLOCK) 
				    ON t3.in_stuff_b1 = t1.in_stuff_b1
				LEFT OUTER JOIN
					VU_MEETING_FOOT t11
					ON t11.program_id = t2.id
					AND t11.in_sign_no = t1.in_sign_no
                WHERE 
                	t1.source_id = '{#meeting_id}'
                	AND ISNULL(t1.in_weight_status, '') NOT IN ('', 'on')
                	{#cond_day}
                	{#cond_status}
                ORDER BY
                	t1.in_city_no
                	, t1.in_stuff_b1
                	, t1.in_current_org
                	, t1.in_name
                	, t11.in_code_en
                	, t11.in_tree_no
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#cond_day}", cond_day)
                .Replace("{#cond_status}", cond_status);

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        //頁面
        private void Page(TConfig cfg, Item itmReturn)
        {
            //賽事資訊
            AppendMeetingInfo(cfg, itmReturn);
            //與會者資訊
            var pkg = AppendMeetingUsers(cfg, itmReturn);
            //比賽日期統計
            var table1 = GenerateSectionTable(pkg.days, "weight_summary_table", "比賽日期");
            //比賽量級人數統計
            var table2 = GenerateSectionTable(pkg.groups, "weight_section_table", "競賽級組");

            var table3 = GenerateDaySectionTable(cfg, "day_section_table");

            itmReturn.setProperty("register_table", GetRegisterTable(cfg));
            itmReturn.setProperty("weight_summary_table", table1);
            itmReturn.setProperty("weight_section_table", table2);
            itmReturn.setProperty("day_section_table", table3);
        }

        private class TGroup
        {
            public string in_l1 { get; set; }
            public string code { get; set; }
            public string name { get; set; }

            public int team_total_m { get; set; }
            public int team_total_w { get; set; }
            public int team_total_x { get; set; }
            public int team_total_s { get; set; }

            public int event_total_m { get; set; }
            public int event_total_w { get; set; }
            public int event_total_x { get; set; }
            public int event_total_s { get; set; }

            public Dictionary<string, TSect2> map { get; set; }
        }

        private class TSect2
        {
            public bool is_team { get; set; }
            public string in_l1 { get; set; }
            public string name { get; set; }
            public Item item_m { get; set; }
            public Item item_w { get; set; }
            public Item item_x { get; set; }
        }

        private Dictionary<string, TGroup> MapRegisterMap(TConfig cfg, Item items)
        {
            var map = new Dictionary<string, TGroup>();
            var count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var in_l1 = item.getProperty("in_l1", "");
                var in_l2 = item.getProperty("in_l2", "").Replace("個-", "").Replace("團-", "");
                var in_l3 = item.getProperty("in_l3", "");
                var l2l3 = in_l2 + in_l3;
                var is_team = in_l1 == "團體組";
                if (is_team) in_l3 = "";

                var age = in_l2;
                if (in_l2.Contains("公開")) age = "公開組";
                else if (in_l2.Contains("一般")) age = "一般組";
                else if (in_l2.Contains("社會") && in_l2.Contains("甲")) age = "社會-甲組";
                else if (in_l2.Contains("社會") && in_l2.Contains("乙")) age = "社會-乙組";
                else if (in_l2.Contains("大專") && in_l2.Contains("甲")) age = "大專-甲組";
                else if (in_l2.Contains("大專") && in_l2.Contains("乙")) age = "大專-乙組";
                else if (in_l2.Contains("國小") && in_l2.Contains("高")) age = "國小-高年級組";
                else if (in_l2.Contains("國小") && in_l2.Contains("中")) age = "國小-中年級組";
                else if (in_l2.Contains("國小") && in_l2.Contains("低")) age = "國小-低年級組";
                else if (in_l2.Contains("國小")) age = "國小組";
                else if (in_l2.Contains("高中")) age = "高中組";
                else if (in_l2.Contains("國中")) age = "國中組";

                var weight = in_l3;
                if (is_team) weight = "";
                else if (in_l3.Contains("第十二級")) weight = "第十二級";
                else if (in_l3.Contains("第十一級")) weight = "第十一級";
                else if (in_l3.Contains("第十級")) weight = "第十級";
                else if (in_l3.Contains("第九級")) weight = "第九級";
                else if (in_l3.Contains("第八級")) weight = "第八級";
                else if (in_l3.Contains("第七級")) weight = "第七級";
                else if (in_l3.Contains("第六級")) weight = "第六級";
                else if (in_l3.Contains("第五級")) weight = "第五級";
                else if (in_l3.Contains("第四級")) weight = "第四級";
                else if (in_l3.Contains("第三級")) weight = "第三級";
                else if (in_l3.Contains("第二級")) weight = "第二級";
                else if (in_l3.Contains("第一級")) weight = "第一級";

                var key1 = in_l1 + age;
                var key2 = in_l1 + age + "-" + weight;
                if (is_team)
                {
                    key1 = "團體組";
                    key2 = in_l1 + age;
                }

                if (!map.ContainsKey(key1))
                {
                    map.Add(key1, new TGroup
                    {
                        in_l1 = in_l1,
                        code = "group-" + (map.Count + 1),
                        name = is_team ? "" : age,
                        map = new Dictionary<string, TSect2>(),
                    });
                }
                var group = map[key1];
                if (!group.map.ContainsKey(key2))
                {
                    group.map.Add(key2, new TSect2
                    {
                        in_l1 = in_l1,
                        name = is_team ? age : age + weight,
                        is_team = is_team,
                    });
                }

                var team_count = GetInt(item.getProperty("team_count", "0"));

                var event_count = FindEventCount(l2l3, is_team, team_count);
                item.getProperty("event_count", event_count.ToString());

                var x = group.map[key2];
                if (l2l3.Contains("混合"))
                {
                    x.item_x = item;
                    group.team_total_x += team_count;
                    group.event_total_x += event_count;
                }
                else if (l2l3.Contains("男"))
                {
                    x.item_m = item;
                    group.team_total_m += team_count;
                    group.event_total_m += event_count;
                }
                else if (l2l3.Contains("女"))
                {
                    x.item_w = item;
                    group.team_total_w += team_count;
                    group.event_total_w += event_count;
                }
                group.team_total_s += team_count;
                group.event_total_s += event_count;
            }
            return map;
        }

        private string GetRegisterTable(TConfig cfg)
        {
            var items = GetRegisterItems(cfg);
            var map = MapRegisterMap(cfg, items);
            var groups = map.Values.ToList();

            var builder = new StringBuilder();
            GetRegisterTableA(cfg, groups, builder);

            for (var i = 0; i < groups.Count; i++)
            {
                var group = groups[i];
                GetRegisterTableB(cfg, group, builder);
            }
            return builder.ToString();
        }

        private void GetRegisterTableA(TConfig cfg, List<TGroup> group, StringBuilder builder)
        {
            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            head.Append("<tr>");
            head.Append("<th class='mailbox-subject text-center' data-width='20%'>競賽項目</th>");
            head.Append("<th class='mailbox-subject text-center' data-width='20%'>競賽級組</th>");
            head.Append("<th class='mailbox-subject text-center' data-width='20%' colspan='2'>男</th>");
            head.Append("<th class='mailbox-subject text-center' data-width='20%' colspan='2'>女</th>");
            head.Append("<th class='mailbox-subject text-center' data-width='20%' colspan='2'>合計</th>");
            head.Append("</tr>");

            head.Append("<tr>");
            head.Append("<th class='mailbox-subject text-center' data-width='20%' colspan='1'></th>");
            head.Append("<th class='mailbox-subject text-center' data-width='20%' colspan='1'></th>");
            head.Append("<th class='mailbox-subject text-center' data-width='10%' colspan='1'>人數</th>");
            head.Append("<th class='mailbox-subject text-center' data-width='10%' colspan='1'>場次</th>");
            head.Append("<th class='mailbox-subject text-center' data-width='10%' colspan='1'>人數</th>");
            head.Append("<th class='mailbox-subject text-center' data-width='10%' colspan='1'>場次</th>");
            head.Append("<th class='mailbox-subject text-center' data-width='10%' colspan='1'>人數</th>");
            head.Append("<th class='mailbox-subject text-center' data-width='10%' colspan='1'>場次</th>");
            head.Append("</tr>");

            var count = group.Count;
            for (int i = 0; i < count; i++)
            {
                var x = group[i];
                body.Append("<tr>");
                body.Append("  <td class='text-center'> " + x.in_l1 + " </td>");
                body.Append("  <td class='text-center'> " + x.name + " </td>");
                body.Append("  <td class='text-center'> " + x.team_total_m + " </td>");
                body.Append("  <td class='text-center'> " + x.event_total_m + " </td>");
                body.Append("  <td class='text-center'> " + x.team_total_w + " </td>");
                body.Append("  <td class='text-center'> " + x.event_total_w + " </td>");
                body.Append("  <td class='text-center'> " + (x.team_total_m + x.team_total_w) + " </td>");
                body.Append("  <td class='text-center'> " + (x.event_total_m + x.event_total_w) + " </td>");
                body.Append("</tr>");
            }

            var table_name = "register-table-summary";
            builder.Append("<div class='box-body tablenobg' style='position: relative; margin-bottom: 10px'>");
            builder.AppendLine(GetTableAttribute(table_name));
            builder.Append("<thead>");
            builder.Append(head);
            builder.Append("</thead>");
            builder.Append("<tbody>");
            builder.Append(body);
            builder.Append("</tbody>");
            builder.AppendLine("</table>");
            builder.Append("</div>");
        }

        private void GetRegisterTableB(TConfig cfg, TGroup group, StringBuilder builder)
        {
            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            head.Append("<th class='mailbox-subject text-center' data-width='25%'>競賽項目</th>");
            head.Append("<th class='mailbox-subject text-center' data-width='25%'>競賽級組</th>");
            head.Append("<th class='mailbox-subject text-center' data-width='25%'>男</th>");
            head.Append("<th class='mailbox-subject text-center' data-width='25%'>女</th>");

            var list = group.map.Values.ToList();
            var count = list.Count;

            for (int i = 0; i < count; i++)
            {
                var x = list[i];
                if (x.item_m == null) x.item_m = cfg.inn.newItem();
                if (x.item_w == null) x.item_w = cfg.inn.newItem();
                if (x.item_x == null) x.item_x = cfg.inn.newItem();

                var team_count_m = x.item_m.getProperty("team_count", "0");
                var team_count_w = x.item_w.getProperty("team_count", "0");


                body.Append("<tr>");
                body.Append("  <td class='text-center'> " + x.in_l1 + " </td>");
                body.Append("  <td class='text-center'> " + x.name + " </td>");
                body.Append("  <td class='text-center'> " + team_count_m + " </td>");
                body.Append("  <td class='text-center'> " + team_count_w + " </td>");
                body.Append("</tr>");
            }

            body.Append("<tr>");
            body.Append("  <td class='text-center'> - </td>");
            body.Append("  <td class='text-center'> 小計 </td>");
            body.Append("  <td class='text-center'> " + group.team_total_m + " </td>");
            body.Append("  <td class='text-center'> " + group.team_total_w + " </td>");
            body.Append("</tr>");
            var table_name = "register-table-" + group.code;
            builder.Append("<div class='box-body tablenobg' style='position: relative; margin-bottom: 10px'>");
            builder.AppendLine(GetTableAttribute(table_name));
            builder.Append("<thead>");
            builder.Append("<tr>");
            builder.Append(head);
            builder.Append("</tr>");
            builder.Append("</thead>");
            builder.Append("<tbody>");
            builder.Append(body);
            builder.Append("</tbody>");
            builder.AppendLine("</table>");
            builder.Append("</div>");
        }

        private Item GetRegisterItems(TConfig cfg)
        {
            var sql = @"
                SELECT
	                t101.in_l1
	                , t201.sort_order
	                , t101.in_l2
	                , t202.sort_order
	                , t101.in_l3
	                , t203.sort_order
	                , COUNT(*) AS 'team_count'
                FROM
                (
	                SELECT DISTINCT
		                source_id
		                , in_l1
		                , in_l2
		                , in_l3
		                , in_index
		                , in_current_org
		                , in_short_org
	                FROM
		                IN_MEETING_USER WITH(NOLOCK)
	                WHERE
		                source_id = '{#meeting_id}'
                        AND in_l1 NOT IN ('隊職員', '常年會費', '格式組')
                ) t101
                LEFT OUTER JOIN
	                VU_MEETING_SVY_L1 t201
	                ON t201.source_id = t101.source_id
	                AND t201.in_value = t101.in_l1
                LEFT OUTER JOIN
	                VU_MEETING_SVY_L2 t202
	                ON t202.source_id = t101.source_id
	                AND t202.in_filter = t101.in_l1
	                AND t202.in_value = t101.in_l2
                LEFT OUTER JOIN
	                VU_MEETING_SVY_L3 t203
	                ON t203.source_id = t101.source_id
	                AND t203.in_grand_filter = t101.in_l1
	                AND t203.in_filter = t101.in_l2
	                AND t203.in_value = t101.in_l3
                GROUP BY 
	                t101.in_l1
	                , t201.sort_order
	                , t101.in_l2
	                , t202.sort_order
	                , t101.in_l3
	                , t203.sort_order
                ORDER BY
	                t201.sort_order
	                , t202.sort_order
	                , t203.sort_order
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        private string GenerateDaySectionTable(TConfig cfg, string table_name)
        {
            var head = new StringBuilder();
            var body = new StringBuilder();

            var items = GetDaySectionItems(cfg);
            var count = items.getItemCount();
            var lastDay = "";
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var currentDay = item.getProperty("in_fight_day", "");
                if (currentDay == lastDay)
                {
                    currentDay = "";
                }
                else
                {
                    lastDay = currentDay;
                }

                body.Append("<tr>");
                body.Append("  <td class='text-center'> " + currentDay + " </td>");
                body.Append("  <td class='text-center'> " + item.getProperty("in_l1", "") + " </td>");
                body.Append("  <td class='text-center'> " + item.getProperty("in_l2", "") + " </td>");
                body.Append("  <td class='text-center'> " + item.getProperty("cnt", "") + " </td>");
                body.Append("</tr>");
            }

            head.Append("<th class='mailbox-subject text-center' data-width='25%'>比賽日期</th>");
            head.Append("<th class='mailbox-subject text-center' data-width='25%'>組別</th>");
            head.Append("<th class='mailbox-subject text-center' data-width='25%'>形式</th>");
            head.Append("<th class='mailbox-subject text-center' data-width='25%'>人數</th>");

            return GenerateTableContents(table_name, head, body);
        }

        private Item GetDaySectionItems(TConfig cfg)
        {
            var sql = @"
                SELECT DISTINCT
                	t1.in_fight_day
                	, t1.in_l1
                	, t1.in_l2
                	, t2.sort_order
                	, SUM(t1.in_team_count) AS 'cnt'
                FROM
                	IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                LEFT OUTER JOIN
                	VU_MEETING_SVY_L1 t2 WITH(NOLOCK)
                	ON t2.source_id = t1.in_meeting 
                	AND t2.in_value = t1.in_l1
                WHERE
                	t1.in_meeting = '{#meeting_id}'
                	AND ISNULL(t1.in_fight_day, '') <> ''
                GROUP BY
                	t1.in_fight_day
                	, t1.in_l1
                	, t1.in_l2
                	, t2.sort_order
                ORDER BY
                	t1.in_fight_day
                	, t1.in_l1
                	, t1.in_l2
                	, t2.sort_order
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            return cfg.inn.applySQL(sql);
        }
        private TPkg AppendMeetingUsers(TConfig cfg, Item itmReturn)
        {
            var days = new List<TContainer>();
            var groups = new List<TContainer>();

            var items = GetMeetingUsers(cfg);
            var count = items.getItemCount();

            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var day = item.getProperty("in_fight_day", "");
                var in_gender = item.getProperty("in_gender", "");
                var in_l1 = item.getProperty("in_l1", "");
                var in_l2 = item.getProperty("in_l2", "");

                var gkey = Analysis(in_l1, in_l2);

                var dobj = AddContainer(days, day);
                var gobj = AddContainer(groups, gkey);

                switch (in_gender)
                {
                    case "男":
                        dobj.MBox.items.Add(item);
                        gobj.MBox.items.Add(item);
                        break;

                    case "女":
                        dobj.WBox.items.Add(item);
                        gobj.WBox.items.Add(item);
                        break;
                }
            }

            return new TPkg
            {
                days = days.OrderBy(x => x.key).ToList(),
                groups = groups,
            };
        }

        private string Analysis(string in_l1, string in_l2)
        {
            if (in_l1 == "團體組")
            {
                return in_l2;
            }
            if (in_l1 == "格式組")
            {
                return in_l2;
            }

            if (in_l2.Contains("社會"))
            {
                if (in_l2.Contains("甲組"))
                {
                    return "社會甲組";
                }
                else if (in_l2.Contains("乙組"))
                {
                    return "社會乙組";
                }
            }
            else if (in_l2.Contains("大專"))
            {
                if (in_l2.Contains("甲組"))
                {
                    return "大專甲組";
                }
                else if (in_l2.Contains("乙組"))
                {
                    return "大專乙組";
                }
            }
            else if (in_l2.Contains("高中"))
            {
                return "高中組";
            }
            else if (in_l2.Contains("國中"))
            {
                return "國中組";
            }
            else if (in_l2.Contains("國小高"))
            {
                return "國小組高年級";
            }
            else if (in_l2.Contains("國小中"))
            {
                return "國小組中年級";
            }
            else if (in_l2.Contains("國小低"))
            {
                return "國小組低年級";
            }
            else if (in_l2.Contains("國小"))
            {
                return "國小組";
            }
            else if (in_l2.Contains("青少年"))
            {
                return "青少年組";
            }
            else if (in_l2.Contains("青年"))
            {
                return "青年組";
            }
            else if (in_l2.Contains("成年"))
            {
                return "成年組";
            }
            return in_l2;
        }

        private string GenerateSectionTable(List<TContainer> list, string table_name, string category)
        {
            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            head.Append("<th class='mailbox-subject text-center' data-width='25%'>" + category + "</th>");
            head.Append("<th class='mailbox-subject text-center' data-width='25%'>男</th>");
            head.Append("<th class='mailbox-subject text-center' data-width='25%'>女</th>");
            head.Append("<th class='mailbox-subject text-center' data-width='25%'>合計</th>");

            var m = 0;
            var w = 0;
            for (int i = 0; i < list.Count; i++)
            {
                var row = list[i];
                body.Append("<tr>");
                body.Append("  <td class='text-center'> " + row.key + " </td>");
                body.Append("  <td class='text-center'> " + row.MBox.items.Count + " </td>");
                body.Append("  <td class='text-center'> " + row.WBox.items.Count + " </td>");
                body.Append("  <td class='text-center'> " + (row.MBox.items.Count + row.WBox.items.Count) + " </td>");
                body.Append("</tr>");

                m += row.MBox.items.Count;
                w += row.WBox.items.Count;
            }
            body.Append("<tr>");
            body.Append("  <td class='text-center'> 總計 </td>");
            body.Append("  <td class='text-center'> " + m.ToString("###,###") + " </td>");
            body.Append("  <td class='text-center'> " + w.ToString("###,###") + " </td>");
            body.Append("  <td class='text-center'> " + (m + w).ToString("###,###") + " </td>");
            body.Append("</tr>");

            return GenerateTableContents(table_name, head, body);
        }

        private string GenerateTableContents(string table_name, StringBuilder head, StringBuilder body)
        {
            var builder = new StringBuilder();
            builder.Append("<div class='box-body tablenobg' style='position: relative; margin-bottom: 10px'>");
            builder.AppendLine(GetTableAttribute(table_name));
            builder.Append("<thead>");
            builder.Append("<tr>");
            builder.Append(head);
            builder.Append("</tr>");
            builder.Append("</thead>");
            builder.Append("<tbody>");
            builder.Append(body);
            builder.Append("</tbody>");
            builder.AppendLine("</table>");
            builder.Append("</div>");
            builder.AppendLine("<script>");
            builder.AppendLine("$('#" + table_name + "').bootstrapTable();");
            builder.AppendLine("if ($(window).width() <= 768) { $('#" + table_name + "').bootstrapTable('toggleView'); }");
            builder.AppendLine("</script>");
            return builder.ToString();
        }

        private string GetTableAttribute(string table_name)
        {
            return "<table id='" + table_name + "'"
                + " class='table table-hover table-striped'"
                + " data-toggle='table'"
                + " data-show-toggle='false'"
                + " data-show-columns='false'"
                + " data-search-align='left'"
                + " data-sort-stable='true'"
                + " data-search='false'"
                + ">";
        }


        private TContainer AddContainer(List<TContainer> list, string key)
        {
            var obj = list.Find(x => x.key == key);
            if (obj == null)
            {
                obj = new TContainer
                {
                    key = key,
                    WBox = new TBox { in_gender = "女", items = new List<Item>() },
                    MBox = new TBox { in_gender = "男", items = new List<Item>() },
                };
                list.Add(obj);
            }
            return obj;
        }

        private class TPkg
        {
            public List<TContainer> groups { get; set; }
            public List<TContainer> days { get; set; }
        }

        private class TContainer
        {
            public string key { get; set; }
            public TBox WBox { get; set; }
            public TBox MBox { get; set; }
        }

        private class TBox
        {
            public string in_gender { get; set; }
            public List<Item> items { get; set; }
        }

        //與會者資訊
        private Item GetMeetingUsers(TConfig cfg)
        {
            var sql = @"
                SELECT
	                t1.in_fight_day
	                , t1.in_l1
	                , t1.in_l2
	                , t1.in_l3
	                , t1.in_name
	                , t1.in_team_count
	                , t2.in_city_no
	                , t2.in_stuff_b1
	                , t2.in_current_org
	                , t2.in_team
	                , t2.in_gender
	                , t2.in_sno
					, t3.in_short_org AS 'map_short_org'
                FROM 
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_USER t2 WITH(NOLOCK)
	                ON t2.source_id = t1.in_meeting
	                AND t2.in_l1 = t1.in_l1
	                AND t2.in_l2 = t1.in_l2
	                AND t2.in_l3 = t1.in_l3
				LEFT OUTER JOIN
				    IN_ORG_MAP t3 WITH(NOLOCK) 
				    ON t3.in_stuff_b1 = t2.in_stuff_b1
                WHERE
	                t1.in_meeting = '{#meeting_id}'
	                AND ISNULL(t1.in_fight_day, '') <> ''
	                AND ISNULL(t1.in_team_count, 0) > 1
                ORDER BY
	                t1.in_sort_order
	                , t2.in_city_no
	                , t2.in_stuff_b1
	                , t2.in_current_org
	                , t2.in_team
	                , t2.in_gender
	                , t2.in_sno
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            return cfg.inn.applySQL(sql);

        }

        //賽事資訊
        private void AppendMeetingInfo(TConfig cfg, Item itmReturn)
        {
            if (cfg.meeting_id == "") return;

            cfg.itmMeeting = cfg.inn.applySQL("SELECT in_title, in_weight_mode FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            if (cfg.itmMeeting.isError() || cfg.itmMeeting.getResult() == "")
            {
                throw new Exception("查無 賽事資料");
            }

            cfg.mt_title = cfg.itmMeeting.getProperty("in_title", "");

            CopyItemValue(itmReturn, cfg.itmMeeting, "in_title");
        }

        private void CopyItemValue(Item itmA, Item itmB, string propA, string propB = "")
        {
            if (propB == "")
            {
                itmA.setProperty(propA, itmB.getProperty(propA, ""));
            }
            else
            {
                itmA.setProperty(propA, itmB.getProperty(propB, ""));
            }
        }

        //日期選單
        private List<Item> GetDateMenu(TConfig cfg)
        {
            string sql = @"
                SELECT 
	                DISTINCT t1.in_date_key
                FROM 
	                IN_MEETING_PEVENT t1 WITH(NOLOCK) 
                WHERE 
	                t1.in_meeting = '{#meeting_id}'
	                AND ISNULL(t1.in_date_key, '') <> ''
                ORDER BY 
	                t1.in_date_key
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return MapList(cfg.inn.applySQL(sql));
        }

        private List<Item> MapList(Item items)
        {
            int count = items.getItemCount();
            List<Item> list = new List<Item>();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                list.Add(item);
            }
            return list;
        }

        private void AppendItems(
            TConfig cfg
            , List<Item> list
            , string type_name
            , string val_property
            , string lbl_property
            , string title
            , string ext_property
            , Item itmReturn)
        {

            Item itmEmpty = cfg.inn.newItem();
            itmEmpty.setType(type_name);
            itmEmpty.setProperty("label", "請選擇" + title);
            itmEmpty.setProperty("value", "");
            itmReturn.addRelationship(itmEmpty);

            bool need_ext = ext_property != "";

            int count = list.Count;
            for (int i = 0; i < count; i++)
            {
                Item item = list[i];
                item.setType(type_name);
                item.setProperty("value", item.getProperty(val_property, ""));
                item.setProperty("label", item.getProperty(lbl_property, ""));

                if (need_ext)
                {
                    item.setProperty(ext_property, item.getProperty(ext_property, ""));
                }

                itmReturn.addRelationship(item);
            }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }

            public string meeting_id { get; set; }
            public string in_fight_day { get; set; }
            public string scene { get; set; }

            public string template_path { get; set; }
            public string export_path { get; set; }
            public string export_type { get; set; }

            public Item itmMeeting { get; set; }
            public Item itmXlsx { get; set; }

            public string mt_title { get; set; }
            public string font_name { get; set; }

            public bool no_data { get; set; }

            public System.Drawing.Color head_color { get; set; }
            public System.Drawing.Color male_color { get; set; }
            public System.Drawing.Color female_color { get; set; }
            public System.Drawing.Color mix_color { get; set; }
        }

        private class TPack
        {
            public List<TOrg> orgs { get; set; }
            public List<TSect> sects { get; set; }
            public int total { get; set; }
        }

        private class TOrg
        {
            public string in_stuff_b1 { get; set; }
            public string in_current_org { get; set; }
            public string map_short_org { get; set; }
            public List<TMUser> players { get; set; }
        }

        private class TSect
        {
            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }
            public string in_short_name { get; set; }
            public string in_fight_day { get; set; }
            public int sort_order { get; set; }
            public List<TMUser> players { get; set; }
        }

        private class TMUser
        {
            public string in_stuff_b1 { get; set; }
            public string in_current_org { get; set; }
            public string map_short_org { get; set; }

            public string program_id { get; set; }
            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }
            public string in_short_name { get; set; }
            public string in_fight_day { get; set; }
            public string in_sort_order { get; set; }

            public string id { get; set; }
            public string in_name { get; set; }
            public string in_sno { get; set; }
            public string in_gener { get; set; }
            public string in_sign_no { get; set; }
            public string in_draw_no { get; set; }
            public string in_weight_result { get; set; }
            public string in_weight_status { get; set; }

            public string sect_age { get; set; }
            public string sect_gender { get; set; }
            public string sect_name { get; set; }

            public string levels { get; set; }
            public string weight_status_label { get; set; }

            public int sort_order { get; set; }
            public CT color { get; set; }
        }

        private TPack MapPack(TConfig cfg, List<Item> items)
        {
            var count = items.Count;
            var total = count;
            if (total <= 0) total = 0;

            var pack = new TPack
            {
                orgs = new List<TOrg>(),
                sects = new List<TSect>(),
                total = total,
            };

            for (var i = 0; i < count; i++)
            {
                var item = items[i];
                var player = MapPlayer(cfg, item);
                var org = pack.orgs.Find(x => x.in_current_org == player.in_current_org);
                if (org == null)
                {
                    org = MapOrg(cfg, player);
                    pack.orgs.Add(org);
                }

                var sect = pack.sects.Find(x => x.in_short_name == player.in_short_name);
                if (sect == null)
                {
                    sect = MapSect(cfg, player);
                    pack.sects.Add(sect);
                }
                org.players.Add(player);
                sect.players.Add(player);
                player.sort_order = sect.sort_order;
            }

            pack.orgs = pack.orgs.OrderBy(x => x.in_stuff_b1).ToList();
            pack.sects = pack.sects.OrderBy(x => x.sort_order).ToList();


            return pack;
        }

        private TOrg MapOrg(TConfig cfg, TMUser player)
        {
            var x = new TOrg
            {
                in_stuff_b1 = player.in_stuff_b1,
                in_current_org = player.in_current_org,
                map_short_org = player.map_short_org,
                players = new List<TMUser>(),
            };
            return x;
        }

        private TSect MapSect(TConfig cfg, TMUser player)
        {
            var x = new TSect
            {
                in_l1 = player.in_l1,
                in_l2 = player.in_l2,
                in_l3 = player.in_l3,
                in_fight_day = player.in_fight_day,
                in_short_name = player.in_short_name,
                sort_order = GetInt(player.in_sort_order),
                players = new List<TMUser>(),
            };
            return x;
        }

        private TMUser MapPlayer(TConfig cfg, Item item)
        {
            var x = new TMUser
            {
                in_stuff_b1 = item.getProperty("in_stuff_b1", ""),
                in_current_org = item.getProperty("in_current_org", ""),
                map_short_org = item.getProperty("map_short_org", ""),

                program_id = item.getProperty("pg_id", ""),
                in_l1 = item.getProperty("in_l1", ""),
                in_l2 = item.getProperty("in_l2", ""),
                in_l3 = item.getProperty("in_l3", ""),
                in_short_name = item.getProperty("in_short_name", ""),
                in_fight_day = item.getProperty("in_fight_day", ""),
                in_sort_order = item.getProperty("in_sort_order", ""),

                id = item.getProperty("id", ""),
                in_name = item.getProperty("in_name", ""),
                in_sno = item.getProperty("in_sno", ""),
                in_gener = item.getProperty("in_gener", ""),
                in_sign_no = item.getProperty("in_sign_no", ""),
                in_draw_no = item.getProperty("in_draw_no", ""),
                in_weight_result = item.getProperty("in_weight_result", ""),
                in_weight_status = item.getProperty("in_weight_status", ""),
            };

            if (x.map_short_org == "") x.map_short_org = x.in_current_org;
            x.weight_status_label = GetWeightStatusLabel(x.in_weight_status);

            x.levels = x.in_l1 + x.in_l2 + x.in_l3;
            if (x.levels.Contains("社會"))
            {
                if (x.levels.Contains("甲")) x.sect_age = "社甲";
                else if (x.levels.Contains("乙")) x.sect_age = "社乙";
                else x.sect_age = "社會";
            }
            else if (x.levels.Contains("大專"))
            {
                if (x.levels.Contains("甲")) x.sect_age = "大甲";
                else if (x.levels.Contains("乙")) x.sect_age = "大乙";
                else x.sect_age = "大專";
            }
            else if (x.levels.Contains("高中")) x.sect_age = "高";
            else if (x.levels.Contains("國中")) x.sect_age = "國";
            else if (x.levels.Contains("國小"))
            {
                if (x.levels.Contains("高年級")) x.sect_age = "小高";
                else if (x.levels.Contains("中年級")) x.sect_age = "小中";
                else if (x.levels.Contains("低年級")) x.sect_age = "小低";
                else x.sect_age = "國小";
            }
            else if (x.levels.Contains("公開")) x.sect_age = "公開";
            else if (x.levels.Contains("一般")) x.sect_age = "一般";

            if (x.levels.Contains("混合"))
            {
                x.sect_gender = "混";
                x.color = CT.Mix;
            }
            else if (x.levels.Contains("男"))
            {
                x.sect_gender = "男";
                x.color = CT.Male;
            }
            else if (x.levels.Contains("女"))
            {
                x.sect_gender = "女";
                x.color = CT.Female;
            }
            else
            {
                x.sect_gender = "無";
                x.color = CT.None;
            }

            x.sect_name = x.sect_age + x.sect_gender;
            x.in_l2 = x.in_l2.Replace("個-", "").Replace("團-", "");

            return x;
        }

        private string GetWeightStatusLabel(string v)
        {
            if (string.IsNullOrWhiteSpace(v)) return "";
            var text = "";
            switch (v)
            {
                case "leave": text = "請假"; break;
                case "off": text = "未到"; break;
                case "on": text = "過磅合格"; break;
                case "dq": text = "體重不合格"; break;
                case "giveup": text = "棄賽"; break;
                case "injuried": text = "傷棄"; break;
            }
            return text;
        }

        private string GetWeightDay(TConfig cfg)
        {
            var fightDay = GetDtm(cfg.in_fight_day);
            var in_weight_mode = cfg.itmMeeting.getProperty("in_weight_mode", "");
            if (in_weight_mode == "fight-day")
            {
                return fightDay.ToString("yyyy-MM-dd");
            }
            else
            {
                return fightDay.AddDays(-1).ToString("yyyy-MM-dd");
            }
        }

        private DateTime GetDtm(string value)
        {
            var result = DateTime.MinValue;
            DateTime.TryParse(value.Replace("/", "-"), out result);
            return result;
        }

        private int GetInt(string value, int def = 0)
        {
            int result = def;
            int.TryParse(value, out result);
            return result;
        }

        private int FindEventCount(string key, bool is_team, int v)
        {
            if (v <= 3) return GetEventCountRobin(v, is_team);
            if (is_team) return GetEventCountTopTwoA(v);
            if (key.Contains("國中")) return GetEventCountTopTwoM(v);
            if (key.Contains("高中")) return GetEventCountTopTwoM(v);
            return GetEventCountTopTwoA(v);
        }

        private string GetShortSect(string value)
        {
            switch (value)
            {
                case "個-社會男子甲組": return "社男甲";
                case "個-社會男子乙組": return "社男乙";
                case "個-社會女子甲組": return "社女甲";
                case "個-社會女子乙組": return "社女乙";

                case "個-大專男子甲組": return "大男甲";
                case "個-大專男子乙組": return "大男乙";
                case "個-大專女子甲組": return "大女甲";
                case "個-大專女子乙組": return "大女乙";

                case "個-高中男子組": return "高男";
                case "個-高中女子組": return "高女";
                case "個-國中男子組": return "國男";
                case "個-國中女子組": return "國女";

                case "個-國小男生高年級組": return "小男高";
                case "個-國小男生中年級組": return "小男中";
                case "個-國小男生低年級組": return "小男低";

                case "個-國小女生高年級組": return "小女高";
                case "個-國小女生中年級組": return "小女中";
                case "個-國小女生低年級組": return "小女低";

                case "團-社會甲組": return "社會甲組";
                case "團-社會乙組": return "社會乙組";
                case "團-大專甲組": return "大專甲組";
                case "團-大專乙組": return "大專乙組";
                case "團-高中組": return "高中組";
                case "團-國中組": return "國中組";
                case "團-國小組": return "國小組";

                case "團-五人團體賽制": return "五人團體賽制";
                case "團-三人團體賽制": return "三人團體賽制";

                default: return value;
            }
        }

        private int GetEventCountTopTwoA(int v)
        {
            return v - 1;
        }

        private int GetEventCountTopTwoM(int v)
        {
            var a = v - 1;
            var b = 0;
            if (v >= 68) b = 20;
            else if (v == 67) b = 19;
            else if (v == 66) b = 18;
            else if (v == 65) b = 17;
            else if (v >= 36) b = 16;
            else if (v == 35) b = 15;
            else if (v == 34) b = 14;
            else if (v == 33) b = 13;
            else if (v >= 20) b = 12;
            else if (v == 19) b = 11;
            else if (v == 18) b = 10;
            else if (v == 17) b = 9;
            else if (v >= 12) b = 8;
            else if (v == 11) b = 7;
            else if (v == 10) b = 6;
            else if (v == 9) b = 5;
            else if (v == 8) b = 4;
            else if (v == 7) b = 3;
            else if (v == 6) b = 2;
            else if (v == 5) b = 2;
            else if (v == 4) b = 1;
            else b = 0;
            return a + b;
        }

        private int GetEventCountRobin(int v, bool is_team)
        {
            if (v == 2) return is_team ? 1 : 3;
            if (v == 3) return 3;
            //if (v == 4) return 6;
            //if (v == 5) return 10;
            return 0;
        }
    }
}