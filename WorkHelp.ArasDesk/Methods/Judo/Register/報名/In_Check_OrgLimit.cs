﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Judo.Register
{
    public class In_Check_OrgLimit : Item
    {
        public In_Check_OrgLimit(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 限制單位報名組數
                日誌: 
                    - 2022-07-14: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]" + "In_Check_OrgLimit";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            string meeting_id = itmR.getProperty("source_id", "");
            string in_creator_sno = itmR.getProperty("in_creator_sno", "");
            string in_current_org = itmR.getProperty("in_current_org", "");
            string in_l1 = itmR.getProperty("in_l1", "");
            string in_l2 = itmR.getProperty("in_l2", "");
            string in_l3 = itmR.getProperty("in_l3", "");
            string in_index = itmR.getProperty("in_index", "");

            if (in_l1 == "隊職員")
            {
                return this;
            }

            Item itmMeeting = inn.applySQL("SELECT id, in_org_limit FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + meeting_id + "'");
            if (itmMeeting.isError() || itmMeeting.getResult() == "")
            {
                return this;
            }

            var in_org_limit = itmMeeting.getProperty("in_org_limit", "1");
            var org_limit = GetInt(in_org_limit, 1);

            string sql = @"
                SELECT DISTINCT
				    in_current_org
				    , in_short_org
                    , in_l1
                    , in_l2
                    , in_l3
                    , in_index
                    , in_creator_sno
                FROM 
                    IN_MEETING_USER t1 WITH(NOLOCK)
                WHERE 
                    source_id = '{#meeting_id}'
                    AND in_l1 = N'{#in_l1}'
                    AND in_l2 = N'{#in_l2}'
                    AND in_l3 = N'{#in_l3}'
                    AND in_creator_sno = '{#in_creator_sno}'
                ORDER BY
                    in_current_org
                    , in_short_org
                    , in_l1
                    , in_l2
                    , in_l3
                    , in_index
            ";

            sql = sql.Replace("{#meeting_id}", meeting_id)
                .Replace("{#in_l1}", in_l1)
                .Replace("{#in_l2}", in_l2)
                .Replace("{#in_l3}", in_l3)
                .Replace("{#in_creator_sno}", in_creator_sno);

            //CCO.Utilities.WriteDebug(strMethodName, sql);

            Item items = inn.applySQL(sql);

            if (items.isError())
            {
                throw new Exception("發生錯誤");
            }

            if (items.getResult() == "")
            {
                return inn.newItem();
            }

            var count = items.getItemCount();
            List<string> teams = new List<string>();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string _in_current_org = item.getProperty("in_current_org", "");
                string _in_l1 = item.getProperty("in_l1", "");
                string _in_l2 = item.getProperty("in_l2", "");
                string _in_l3 = item.getProperty("in_l3", "");
                string _in_index = item.getProperty("in_index", "");

                string _sect_key = string.Join("", new List<string>
                {
                    _in_current_org,
                    _in_l1,
                    _in_l2,
                    _in_l3,
                });

                string _team_key = string.Join("", new List<string>
                {
                    _in_current_org,
                    _in_l1,
                    _in_l2,
                    _in_l3,
                    _in_index,
                });

                teams.Add(_team_key);
            }

            if (teams.Count > org_limit)
            {
                throw new Exception(in_l1 + " " + " " + in_l3 + ": 每個單位限報 " + in_org_limit + " 組");
            }

            return inn.newItem();
        }

        private int GetInt(string value, int defV = 0)
        {
            if (value == "") return 0;

            int result = defV;
            if (int.TryParse(value, out result))
            {
                return result;
            }
            return defV;

        }
    }
}