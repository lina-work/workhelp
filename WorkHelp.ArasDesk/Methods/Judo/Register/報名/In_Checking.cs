﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Judo.Register
{
    public class In_Checking : Item
    {
        public In_Checking(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
            目的: 檢查學校代碼,身分證,居留證,護照
            注意事項:
                - 因提供快速建立單位註冊，單位帳號與名稱檢查邏輯有異動時，需修改In_Meeting_InsCurrent_Validate
            日誌:
                - 2022/04/20: 增加居留證

            */
            string strMethodName = "In_Checking";
            //System.Diagnostics.Debugger.Break();
            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
            string aml = "";
            string sql = "";
            string strError = "";
            DateTime CurrentTime = System.DateTime.Today;
            //CCO.Utilities.WriteDebug("In_Checking", "this:" + this.dom.InnerXml);
            //檢查格式A~Z(1) 0~9(9)(台灣身分證)
            var regex = new System.Text.RegularExpressions.Regex("^[A-Z]{1}[0-9]{9}$");
            //檢查格式A~Z(1)A~Z(2) 0~9(8)(居留證)
            var pat = new System.Text.RegularExpressions.Regex("^[A-Z]{1}[A-Z]{1}[0-9]{8}$");
            //檢查格式A~Z(1)3~0(2) 0~9(8)(居留證)
            var pat2 = new System.Text.RegularExpressions.Regex("^[A-Z]{1}[89]{1}[0-9]{8}$");
            //檢查格式A~Z(1)A~Z(2) 0~9(6)(外國護照)
            var pas = new System.Text.RegularExpressions.Regex("^[A-Z]{1}[A-Z]{1}[0-9]{6}$");

            //接收傳進來的身分證
            string identity_card = this.getProperty("in_sno", "").ToUpper();
            CCO.Utilities.WriteDebug("In_Checking", "identity_card:" + identity_card);
            //判定類型
            string id_type = ""; //taiwanid,passport,bbb,cc

            string in_sno_type = this.getProperty("in_sno_type", "");

            //長度判定
            if (identity_card.Length == 4)//學校代碼
            {
                id_type = "school_code";
            }
            else if (identity_card.Length == 6)//學校代碼
            {
                id_type = "school_code";
            }
            else if (identity_card.Length == 8)//外國護照
            {
                id_type = "passport";
            }
            else if (identity_card.Length == 10)//身分證or居留證
            {
                if (pat2.IsMatch(identity_card)) //居留證
                {
                    id_type = "resident_permit2";
                }
                else if (regex.IsMatch(identity_card))//身分證
                {
                    id_type = "identity_card";
                }
                else if (pat.IsMatch(identity_card))//居留證
                {
                    id_type = "resident_permit";
                }
                else
                {
                    //不符合以上兩個格式
                    //throw new Exception("身分證_居留證_格式錯誤");
                    strError += "身分格式錯誤" + "\n";
                }
            }
            else if (identity_card.Length >= 11 && identity_card.Length <= 12)//港澳居留
            {
                id_type = "HKM_code";
            }
            else
            {
                //非6,8,10碼
                //throw new Exception("輸入格式錯誤");
                strError += "輸入格式錯誤,請確認字數" + "\n";
            }



            switch (id_type)
            {
                case "school_code"://學校代碼
                    this.setProperty("state", "ok");
                    this.setProperty("message", "此為學校代碼");
                    break;
                case "passport"://外國護照

                    if (!pas.IsMatch(identity_card))//身分證
                    {
                        strError += "護照格式錯誤" + "\n";
                    }
                    else
                    {
                        this.setProperty("state", "ok");
                        this.setProperty("message", "此為外國護照");
                    }
                    break;
                case "identity_card"://身分證
                                     //存放檢查碼之外的數字
                    int[] seed = new int[10];
                    //字母陣列
                    string[] charMapping = new string[] { "A", "B", "C", "D", "E", "F", "G", "H", "J", "K", "L", "M", "N", "P", "Q", "R", "S", "T", "U", "V", "X", "Y", "W", "Z", "I", "O" };

                    string target = identity_card.Substring(0, 1);
                    string gender = "";
                    //依據身分證第2碼判定性別
                    if (identity_card.Substring(1, 1) == "1")
                    {
                        gender = "男";
                        this.setProperty("inn_gender", "1");
                    }
                    else if (identity_card.Substring(1, 1) == "2")
                    {
                        gender = "女";
                        this.setProperty("inn_gender", "2");
                    }

                    if (this.getProperty("in_gender", "") != gender)
                    {
                        //throw new Exception("性別錯誤");
                        strError += "性別驗證錯誤" + "\n";
                    }

                    for (int index = 0; index < charMapping.Length; index++)
                    {
                        if (charMapping[index] == target)
                        {
                            index += 10;
                            seed[0] = index / 10;
                            seed[1] = (index % 10) * 9;
                            break;
                        }
                    }
                    for (int index = 2; index < 10; index++)
                    {
                        seed[index] = Convert.ToInt32(identity_card.Substring(index - 1, 1)) * (10 - index);
                    }
                    //檢查是否為正確的身分證(依據身分證檢查規則)
                    if ((10 - (seed.Sum() % 10)) % 10 != Convert.ToInt32(identity_card.Substring(9, 1)))
                    {
                        this.setProperty("state", "ng");
                        this.setProperty("message", "請輸入正確身分證");
                        strError += "請輸入正確身分證" + "\n";
                    }
                    else
                    {
                        this.setProperty("state", "ok");
                        this.setProperty("message", "身分證號碼正確");
                    }
                    break;
                case "resident_permit"://居留證
                    /*
                        string sex = "";
                        string nationality = "";
                        char[] strArr = identity_card.ToCharArray(); // 字串轉成char陣列

                        int verifyNum = 0;
                        int[] pidResidentFirstInt = { 1, 10, 9, 8, 7, 6, 5, 4, 9, 3, 2, 2, 11, 10, 8, 9, 8, 7, 6, 5, 4, 3, 11, 3, 12, 10 };
                        char[] pidCharArray = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };
                        // 第一碼
                        verifyNum += pidResidentFirstInt[Array.BinarySearch(pidCharArray, strArr[0])];
                        // 原居留證第二碼英文字應轉換為10~33，並僅取個位數*6，這裡直接取[(個位數*6) mod 10]
                        int[] pidResidentSecondInt = { 0, 8, 6, 4, 2, 0, 8, 6, 2, 4, 2, 0, 8, 6, 0, 4, 2, 0, 8, 6, 4, 2, 6, 0, 8, 4 };
                        // 第二碼
                        verifyNum += pidResidentSecondInt[Array.BinarySearch(pidCharArray, strArr[1])];
                        // 第三~八碼
                        for (int i = 2, j = 7; i < 9; i++, j--)
                        {
                            verifyNum += Convert.ToInt32(strArr[i].ToString(), 10) * j;
                        }
                        // 檢查碼
                        verifyNum = (10 - (verifyNum % 10)) % 10;
                        bool ok = verifyNum == Convert.ToInt32(strArr[9].ToString(), 10);
                        if (ok)
                        {
                            // 判斷性別 & 國籍
                            sex = "男";
                            if (strArr[1] == 'B' || strArr[1] == 'D') sex = "女";
                            nationality = "外籍人士";
                            if (strArr[1] == 'A' || strArr[1] == 'B') nationality += "(臺灣地區無戶籍國民、大陸地區人民、港澳居民)";
                            {
                                this.setProperty("state","ok");
                                this.setProperty("message","此為居留證");
                            }
                        }
                        else
                        {
                            this.setProperty("state","ng");
                            this.setProperty("message","請輸入正確居留證");
                            strError += "請輸入正確居留證" + "\n";
                        }
                    */
                    break;

                case "resident_permit2": //居留證
                    var resident_result = CheckResidentSno(identity_card, this);
                    if (resident_result.errs.Count == 0)
                    {
                        this.setProperty("state", "ok");
                        this.setProperty("message", "此為居留證(new)");
                    }
                    else
                    {
                        strError += string.Join("\n", resident_result.errs);
                    }
                    break;

                case "HKM_code": //港澳居留
                    this.setProperty("state", "ok");
                    this.setProperty("message", "此为港澳居留证");
                    break;
            }


            if (in_sno_type == "judo_age_check")
            {
                //生日定判定
                switch (id_type)
                {
                    case "school_code"://學校代碼
                        this.setProperty("state", "ok");
                        this.setProperty("message", "此為學校代碼");
                        break;

                    case "resident_permit"://居留證
                    case "passport"://外國護照
                    case "identity_card"://身分證


                        //取得生日
                        string birth = this.getProperty("in_birth", CurrentTime.ToString("yyyy/MM/dd"));
                        //取得現在時間
                        //string nowtime = DateTime.Now.ToString("yyyy/MM/dd"); 
                        string nowtime = "2005/12/31";//柔道限定此日期以前都可以報名
                        CCO.Utilities.WriteDebug("In_Checking", "birth:" + birth);

                        DateTime dt1 = DateTime.Parse(birth);//接收的生日
                        DateTime dt2 = DateTime.Parse(nowtime);//當天的日期

                        TimeSpan ts = dt2.Subtract(dt1);//比較
                        double day = ts.TotalDays;//生日到當天的天數
                        int d = Convert.ToInt16(Math.Floor(day / 365));


                        string cccc = CurrentTime.AddYears(-5).ToString("yyyy/MM/dd");


                        this.setProperty("NowTime", cccc);

                        CCO.Utilities.WriteDebug("In_Checking", "d:" + d);
                        /*
                        //年齡小於10歲
                        if(d < 15)
                        {
                            //this.setProperty("state","ng");
                            this.setProperty("age","小於");
                            CCO.Utilities.WriteDebug("In_Checking", "小於");
                            strError += "生日驗證錯誤!" + "輸入生日為"+ "(" + birth.Split('T')[0] + ")" + "未達可報名年齡" + "\n";
                        }
                        else
                        {
                            //this.setProperty("state","ok");
                            CCO.Utilities.WriteDebug("In_Checking", "大於");
                            this.setProperty("age","大於");
                        }
                        */

                        if (DateTime.Compare(dt1, dt2) > 0)
                        {
                            this.setProperty("age", "小於");
                            CCO.Utilities.WriteDebug("In_Checking", "小於");
                            strError += "生日驗證錯誤!" + "輸入生日為" + "(" + birth.Split('T')[0] + ")" + "未達可報名年齡" + "\n";
                        }
                        else
                        {
                            CCO.Utilities.WriteDebug("In_Checking", "大於");
                            this.setProperty("age", "大於");
                        }

                        break;

                }
            }




            string[] Special_text = new string[] { "(", "?", "=", ".", "*", "[", "@", "#", "$", "%", "^", "&", ".", "+", "-", "]", ")", " ", "/", @"\" };//特殊字元

            string in_current_org = this.getProperty("in_current_org", "");

            //檢查是否有特殊字元存在
            foreach (string _text in Special_text)
            {
                if (in_current_org.Contains(_text))
                {
                    if (!strError.Contains("所屬單位包含特殊字元"))
                    {
                        strError += "所屬單位包含特殊字元" + "\n";
                    }
                }
            }

            if (strError != "")
            {
                throw new Exception(strError);
            }

            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

            return this;

        }

        private class TPerson
        {
            public string gender { get; set; }
            public List<string> errs { get; set; }
        }

        private TPerson CheckResidentSno(string identity_card, Item itmReturn)
        {
            var result = new TPerson
            {
                gender = "",
                errs = new List<string>(),
            };

            //In_Resume 性別
            string in_gender = itmReturn.getProperty("in_gender", "");


            //存放檢查碼之外的數字
            int[] seed = new int[10];
            //字母陣列
            string[] charMapping = new string[] { "A", "B", "C", "D", "E", "F", "G", "H", "J", "K", "L", "M", "N", "P", "Q", "R", "S", "T", "U", "V", "X", "Y", "W", "Z", "I", "O" };

            string target = identity_card.Substring(0, 1);

            //依據身分證第2碼判定性別
            if (identity_card.Substring(1, 1) == "8")
            {
                result.gender = "男";
                itmReturn.setProperty("inn_gender", "1");
            }
            else if (identity_card.Substring(1, 1) == "9")
            {
                result.gender = "女";
                itmReturn.setProperty("inn_gender", "2");
            }

            if (in_gender != result.gender)
            {
                result.errs.Add("性別驗證錯誤");
            }

            for (int index = 0; index < charMapping.Length; index++)
            {
                if (charMapping[index] == target)
                {
                    index += 10;
                    seed[0] = index / 10;
                    seed[1] = (index % 10) * 9;
                    break;
                }
            }
            for (int index = 2; index < 10; index++)
            {
                seed[index] = Convert.ToInt32(identity_card.Substring(index - 1, 1)) * (10 - index);
            }
            //檢查是否為正確的身分證(依據身分證檢查規則)
            if ((10 - (seed.Sum() % 10)) % 10 != Convert.ToInt32(identity_card.Substring(9, 1)))
            {
                itmReturn.setProperty("state", "ng");
                itmReturn.setProperty("message", "請輸入正確身分證");
                result.errs.Add("請輸入正確身分證");
            }
            else
            {
                itmReturn.setProperty("state", "ok");
                itmReturn.setProperty("message", "身分證號碼正確");
            }
            return result;
        }
    }
}