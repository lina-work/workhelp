﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;
using ClosedXML.Excel;

namespace WorkHelp.ArasDesk.Methods.Judo.Register
{
    public class In_Check_Meeting_Rules : Item
    {
        public In_Check_Meeting_Rules(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 賽事報名規則限制
                日誌: 
                    - 2023-01-09: 創建 (lina)
            */

            System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "In_Check_Meeting_Rules";

            var itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strItemType = itmR.getType(),
                meeting_id = itmR.getProperty("source_id", ""),
                in_creator_sno = itmR.getProperty("in_creator_sno", ""),
                in_current_org = itmR.getProperty("in_current_org", ""),
                in_l1 = itmR.getProperty("in_l1", ""),
                in_l2 = itmR.getProperty("in_l2", ""),
                in_l3 = itmR.getProperty("in_l3", ""),
                in_index = itmR.getProperty("in_index", ""),
                in_sno = itmR.getProperty("in_sno", ""),
                in_gender = itmR.getProperty("in_gender", ""),
                in_birth = itmR.getProperty("in_birth", ""),
                scene = itmR.getProperty("scene", ""),
            };

            if (cfg.meeting_id == "")
            {
                cfg.meeting_id = itmR.getProperty("meeting_id", "");
            }

            if (cfg.scene == "")
            {
                cfg.scene = "register";
            }

            cfg.strItemType = cfg.strItemType.ToUpper();
            cfg.mt_table = "IN_MEETING";
            cfg.mt_user_table = "IN_MEETING_USER";
            cfg.mt_rules_table = "IN_MEETING_RULES";
            if (cfg.strItemType == "IN_CLA_MEETING_USER")
            {
                cfg.mt_table = "IN_CLA_MEETING";
                cfg.mt_user_table = "IN_CLA_MEETING_USER";
                cfg.mt_rules_table = "IN_CLA_MEETING_RULES";
            }

            switch (cfg.scene)
            {
                case "payment"://[產生繳費單時]檢查
                    CheckAtPayment(cfg, itmR);
                    break;

                case "register"://[送出報名時]檢查
                    CheckDegree(cfg, itmR);
                    CheckAtRegister(cfg, itmR);
                    break;
            }

            return itmR;
        }

        //檢查晉段資料
        private void CheckDegree(TConfig cfg, Item itmReturn)
        {
            var need_check1 = false;
            var need_check2 = false;
            var in_l1 = itmReturn.getProperty("in_l1", "");
            var in_l2 = itmReturn.getProperty("in_l2", "");
            var in_l3 = itmReturn.getProperty("in_l3", "");
            var in_name = itmReturn.getProperty("in_name", "");
            var in_sno = itmReturn.getProperty("in_sno", "");
            if (in_sno == "") return;

            if (in_l1.Contains("個人") || in_l1.Contains("團體")) need_check1 = true;
            if (!need_check1) return;

            if (in_l2.Contains("乙組") || in_l3.Contains("乙組")) need_check2 = true;
            if (!need_check2) return;

            var sql = "SELECT TOP 1 in_sheet_name FROM AAA_Degree_Record WITH(NOLOCK) WHERE in_sno = '" + in_sno + "'";
            var item = cfg.inn.applySQL(sql);
            if (!item.isError() && item.getResult() != "")
            {
                throw new Exception(in_name + " 有升段資料，不可報乙組");
            }
        }

        //產生繳費單時檢查
        private void CheckAtPayment(TConfig cfg, Item itmReturn)
        {
            var itmRules = GetMeetingRules(cfg, new List<string> { "payment", "register_payment" });
            var rules = MapRules(cfg, itmRules);

            var itmMUsers = GetMeetingUsersFromMeeting(cfg, "");
            var orgMap = MapOrgs(cfg, itmMUsers);
            var orgs = orgMap.Values.ToList();

            var errs = new List<string>();
            for (int i = 0; i < rules.Count; i++)
            {
                var rule = rules[i];
                for (int j = 0; j < orgs.Count; j++)
                {
                    var org = orgs[j];
                    var prefix = "";//org.in_current_org + ": ";
                    switch (rule.in_func)
                    {
                        case "one_team"://選手不能跨隊
                            break;

                        case "coach_rule"://N個選手以上才能報M位教練
                            MoreCoaches(cfg, rule, org, errs, prefix: prefix);
                            break;

                        case "at_least_a_captain"://至少一位領隊
                            AtLeastOneCaptain(cfg, rule, org, errs, prefix: prefix);
                            break;

                        default:
                            break;
                    }
                }
            }

            if (errs.Count > 0)
            {
                if (errs.Count == 1)
                {
                    throw new Exception(errs[0]);
                }

                var builder = new StringBuilder();
                for (var i = 0; i < errs.Count; i++)
                {
                    var no = i + 1;
                    var err = errs[i];
                    if (i > 0) builder.Append("、<BR>");
                    builder.Append(no + ". " + err);
                }
                throw new Exception(builder.ToString());
            }
        }

        //送出報名時檢查
        private void CheckAtRegister(TConfig cfg, Item itmReturn)
        {
            var itmRules = GetMeetingRules(cfg, new List<string> { "register", "register_payment" });
            var rules = MapRules(cfg, itmRules);

            var errs = new List<string>();
            for (int i = 0; i < rules.Count; i++)
            {
                var rule = rules[i];
                switch (rule.in_func)
                {
                    case "bigger_than_n_years"://年滿N歲方可報名
                        BiggerThanNYears(cfg, rule, errs);
                        break;

                    case "one_team"://選手不能跨隊
                        PlayerOnlyOneTeam(cfg, rule, errs);
                        break;

                    case "coach_rule"://N個選手以上才能報M位教練
                        MoreCoachesFromOneOrg(cfg, rule, errs);
                        break;

                    case "staff_rule"://隊職員卡控
                        StaffLimitFromOneOrg(cfg, rule, errs);
                        break;

                    case "at_least_a_captain"://至少一位領隊
                        break;

                    case "team_fight_at_most"://團體組各賽制單位至多報名N隊
                        TeamFightAtMostFromOneOrg(cfg, rule, errs);
                        break;

                    default:
                        if (rule.limit <= 0)
                        {
                            errs.Add("不開放報名");
                        }
                        else
                        {
                            CheckMeetingRules(cfg, rule, errs);
                        }
                        break;
                }

            }

            if (errs.Count > 0)
            {
                throw new Exception(string.Join("、", errs));
            }
        }

        private void BiggerThanNYears(TConfig cfg, TRule rule, List<string> errs)
        {
            string sql = "SELECT in_meeting_type, in_seminar_type FROM " + cfg.mt_table + " WHERE id = '" + cfg.meeting_id + "'";
            Item itmMeeting = cfg.inn.applySQL(sql);
            if (itmMeeting.isError() || itmMeeting.getResult() == "")
            {
                errs.Add("查無課程資訊");
                return;
            }

            string in_meeting_type = itmMeeting.getProperty("in_meeting_type", "");
            string in_seminar_type = itmMeeting.getProperty("in_seminar_type", "");

            //if (in_meeting_type != "seminar")
            //{
            //    //非講習 bypass
            //    return;
            //}

            int age_limit = 0;
            if (in_seminar_type.Contains("coach"))
            {
                age_limit = 20;
            }
            else if (in_seminar_type.Contains("referee"))
            {
                age_limit = 18;
            }
            else
            {
                //非教練、亦非裁判，不檢查
                return;
            }

            var today = DateTime.Now;
            var target_birth = today.AddYears(-1 * age_limit);
            var user_birth = GetDtm(cfg.in_birth);

            if (user_birth == DateTime.MinValue)
            {
                errs.Add("生日錯誤");
                return;
            }

            if (user_birth.Date > target_birth.Date)
            {
                if (rule.in_message != "")
                {
                    errs.Add(rule.in_message);
                }
                else
                {
                    errs.Add("參加資格必須年滿" + age_limit + "歲以上 (於" + target_birth.Date.ToString("yyyy-MM-dd") + "以前出生)");
                }
            }
        }

        //至少一位領隊
        private void AtLeastOneCaptain(TConfig cfg, TRule rule, TOrg org, List<string> errs, string prefix = "")
        {
            var message = "";
            if (org.leaders.Count <= 0)
            {
                message = prefix + "至少一位領隊";
            }

            if (message != "")
            {
                if (rule.in_message != "")
                {
                    message = prefix + rule.in_message;
                }

                errs.Add(message);
            }
        }

        //團體組各賽制單位至多報名[上限]隊
        private void TeamFightAtMostFromOneOrg(TConfig cfg, TRule rule, List<string> errs)
        {
            if (cfg.in_l1 != "團體組") return;

            var in_l3 = cfg.in_l3;
            var itmTeams = GetMeetingTeamsFromMeeting(cfg, cfg.in_current_org, in_l3);
            var count = itmTeams.getItemCount();
            if (count > rule.limit)
            {
                errs.Add("[" + in_l3 + "]限制單位至多報名 " + rule.limit + " 隊");
            }
        }

        private Item GetMeetingTeamsFromMeeting(TConfig cfg, string in_current_org, string in_l3)
        {
            var org_cond = in_current_org == ""
                ? ""
                : "AND in_current_org = N'" + in_current_org + "'";

            string sql = @"                
                SELECT DISTINCT
                    in_l1
                    , in_l2
                    , in_l3
                    , in_index
                    , in_team
                    , in_creator_sno
                FROM
                    {#table} WITH(NOLOCK)
                WHERE
                    source_id = '{#meeting_id}'
					AND in_l1 = N'團體組'
					AND in_l3 = N'{#in_l3}'
                    AND in_creator_sno = '{#in_creator_sno}'
                    {#org_cond}
                ORDER BY
                    in_l1
                    , in_l2
                    , in_l3
                    , in_index
                    , in_team
                    , in_creator_sno
            ";

            sql = sql.Replace("{#table}", cfg.mt_user_table)
                .Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_l3}", in_l3)
                .Replace("{#in_creator_sno}", cfg.in_creator_sno)
                .Replace("{#org_cond}", org_cond);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        //N個選手以上才能報M位教練
        private void MoreCoachesFromOneOrg(TConfig cfg, TRule rule, List<string> errs)
        {
            var itmMUsers = GetMeetingUsersFromMeeting(cfg, cfg.in_current_org);
            var orgMap = MapOrgs(cfg, itmMUsers);
            var orgs = orgMap.Values.ToList();
            for (var i = 0; i < orgs.Count; i++)
            {
                MoreCoaches(cfg, rule, orgs[i], errs);
            }
        }

        //隊職員管理
        private void StaffLimitFromOneOrg(TConfig cfg, TRule rule, List<string> errs)
        {
            var itmMUsers = GetMeetingUsersFromMeeting(cfg, cfg.in_current_org);
            var orgMap = MapOrgs(cfg, itmMUsers);
            var orgs = orgMap.Values.ToList();
            for (var i = 0; i < orgs.Count; i++)
            {
                StaffLimitCheck(cfg, rule, orgs[i], errs);
            }
        }

        //隊職員數量限制
        private void StaffLimitCheck(TConfig cfg, TRule rule, TOrg org, List<string> errs, string prefix = "")
        {
            var message = "";

            if (rule.in_value == "管理" && rule.limit > 0)
            {
                if (org.managers.Count > rule.limit)
                {
                    message = prefix + "管理已達可報名上限(限" + rule.limit + "位)";
                }
            }
            else if (rule.in_value == "領隊" && rule.limit > 0)
            {
                if (org.leaders.Count > rule.limit)
                {
                    message = prefix + "領隊已達可報名上限(限" + rule.limit + "位)";
                }
            }
            else if (rule.in_value == "男子隊教練" && rule.limit > 0)
            {
                if (org.m_coaches.Count > rule.limit)
                {
                    message = prefix + "男子隊教練已達可報名上限(限" + rule.limit + "位)";
                }
            }
            else if (rule.in_value == "女子隊教練" && rule.limit > 0)
            {
                if (org.w_coaches.Count > rule.limit)
                {
                    message = prefix + "女子隊教練已達可報名上限(限" + rule.limit + "位)";
                }
            }

            if (message != "")
            {
                if (rule.in_message != "")
                {
                    message = prefix + rule.in_message;
                }

                errs.Add(message);
            }
        }

        //N個選手以上才能報M位教練
        private void MoreCoaches(TConfig cfg, TRule rule, TOrg org, List<string> errs, string prefix = "")
        {
            var message = "";

            if (rule.n1 > 0 && rule.n2 > 0)
            {
                if (org.a_coaches.Count > rule.n2)
                {
                    message = prefix + "教練已達可報名上限(限" + rule.n2 + "位)";
                }
                else if (org.a_coaches.Count == rule.n2)
                {
                    if (org.players.Count < rule.n1)
                    {
                        message = prefix + rule.in_func_label.Replace("[N1]", rule.n1.ToString())
                            .Replace("[N2]", rule.n2.ToString());
                    }
                }
            }
            else if (rule.limit > 0 && org.a_coaches.Count > rule.limit)
            {
                //超過預設上限
                message = prefix + "教練已達可報名上限(限" + rule.in_limit + "位)";
            }

            if (message != "")
            {
                if (rule.in_message != "")
                {
                    message = prefix + rule.in_message;
                }

                errs.Add(message);
            }
        }

        //選手不能跨隊
        private void PlayerOnlyOneTeam(TConfig cfg, TRule rule, List<string> errs)
        {
            if (cfg.in_l1 == "隊職員") return;

            string sql = @"
                SELECT DISTINCT
                    in_current_org
				    , in_short_org
                    , in_l1
                    , in_l2
                    , in_l3
                    , in_index
                    , in_team
                    , in_creator_sno
                FROM
                    {#table} t1 WITH(NOLOCK)
                WHERE
                    source_id = '{#meeting_id}'
                    AND in_l1 <> N'隊職員'
                    AND in_creator_sno = '{#in_creator_sno}'
                    AND in_sno = '{#in_sno}'
                ORDER BY
                    in_current_org
                    , in_short_org
                    , in_l1
                    , in_l2
                    , in_l3
                    , in_index
            ";

            sql = sql.Replace("{#table}", cfg.mt_user_table)
                .Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_creator_sno}", cfg.in_creator_sno)
                .Replace("{#in_sno}", cfg.in_sno);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            var teams = new List<string>();
            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var in_team = item.getProperty("in_team", "").ToUpper();
                if (in_team == "")
                {
                    continue;
                }

                if (!teams.Contains(in_team))
                {
                    teams.Add(in_team);
                }
            }

            if (teams.Count > 1)
            {
                errs.Add("選手不能跨隊");
            }
        }

        private void CheckMeetingRules(TConfig cfg, TRule rule, List<string> errs)
        {
            var items = GetMeetingUsersFromRule(cfg, rule);
            var count = items.getItemCount();
            var teams = new List<string>();

            for (int i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var team_key = TeamKey(item);
                teams.Add(team_key);
            }

            if (teams.Count <= rule.limit)
            {
                return;
            }

            var message = "";

            switch (rule.in_type)
            {
                case "in_creator_sno":
                    message = "限制一位協助報名者最多只能報名 " + rule.in_limit + " 種項目";
                    break;

                case "in_current_org":
                    switch (rule.in_property)
                    {
                        case "in_l1":
                            message = "[" + cfg.in_l1 + "]一個單位至多 " + rule.in_limit + " 名(組)";
                            break;

                        case "in_l2":
                            if (cfg.in_l1.Contains("團體"))
                            {
                                message = "[" + cfg.in_l1 + "-" + cfg.in_l3 + "]一個單位至多 " + rule.in_limit + " 名(組)";
                            }
                            else
                            {
                                var l2 = cfg.in_l2.Replace("個-", "")
                                    .Replace("團-", "")
                                    .Replace("格-", "");

                                var text = l2 == cfg.in_l3 ? l2 : l2 + "-" + cfg.in_l3;

                                message = "[" + cfg.in_l1 + "-" + text + "]一個單位至多 " + rule.in_limit + " 名(組)";
                            }
                            break;

                        case "in_l3":
                            message = "[" + cfg.in_l1 + "-" + cfg.in_l3 + "]一個單位至多 " + rule.in_limit + " 名(組)";
                            break;
                    }
                    break;

                case "in_sno":
                    message = "[" + rule.in_value + "]限制一位選手最多只能報名 " + rule.in_limit + " 種項目";
                    break;

                default:
                    message = "報名規則的[卡控類型]欄位未設定";
                    break;
            }

            if (message != "")
            {
                if (rule.in_message != "")
                {
                    message = rule.in_message;
                }

                errs.Add(message);
            }
        }

        private Dictionary<string, TOrg> MapOrgs(TConfig cfg, Item items)
        {
            var orgs = new Dictionary<string, TOrg>();

            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var in_current_org = item.getProperty("in_current_org", "").ToUpper();
                var in_l1 = item.getProperty("in_l1", "").ToUpper();
                var in_l2 = item.getProperty("in_l2", "").ToUpper();
                var in_l3 = item.getProperty("in_l3", "").ToUpper();

                var staff_key = StaffKey(in_l1, in_l2, in_l3, item);
                var player_key = PlayerKey(in_l1, in_l2, in_l3, item);

                if (staff_key == "") continue;

                if (!orgs.ContainsKey(in_current_org))
                {
                    orgs.Add(in_current_org, new TOrg 
                    {
                        in_current_org = in_current_org,
                        players = new Dictionary<string, Item>(),
                        managers = new Dictionary<string, Item>(),
                        leaders = new Dictionary<string, Item>(),
                        a_coaches = new Dictionary<string, Item>(),
                        m_coaches = new Dictionary<string, Item>(),
                        w_coaches = new Dictionary<string, Item>(),
                        others = new Dictionary<string, Item>(),
                    });
                }

                var org = orgs[in_current_org];

                var is_staff = in_l1 == "隊職員";

                if (is_staff)
                {
                    switch (in_l2)
                    {
                        case "管理":
                            AppendStaffRow(cfg, org, org.managers, staff_key, item);
                            break;
                        case "領隊":
                            AppendStaffRow(cfg, org, org.leaders, staff_key, item);
                            break;
                        case "教練":
                            if (in_l3 == "男子隊教練")
                            {
                                cfg.is_special_coach = true;
                                AppendStaffRow(cfg, org, org.m_coaches, staff_key, item);
                            }
                            else if (in_l3 == "女子隊教練")
                            {
                                cfg.is_special_coach = true;
                                AppendStaffRow(cfg, org, org.w_coaches, staff_key, item);
                            }
                            else
                            {
                                cfg.is_special_coach = false;
                                AppendStaffRow(cfg, org, org.a_coaches, staff_key, item);
                            }
                            break;
                        default:
                            AppendStaffRow(cfg, org, org.others, staff_key, item);
                            break;
                    }
                }
                else
                {
                    AppendStaffRow(cfg, org, org.players, player_key, item);
                }
            }
            return orgs;
        }

        private void AppendStaffRow(TConfig cfg, TOrg org, Dictionary<string, Item> map, string key, Item item)
        {
            if (map.ContainsKey(key)) throw new Exception(key + " 重複報名");
            map.Add(key, item);
        }

        //private string PlayerKey(Item item)
        //{
        //    return item.getProperty("in_sno", "").Trim().ToUpper();
        //}

        //private string StaffKey(Item item)
        //{
        //    string in_name = item.getProperty("in_name", "").Trim().ToUpper();
        //    string in_sno = item.getProperty("in_sno", "").Trim().ToUpper();
        //    return string.Join("-", new List<string>
        //    {
        //        in_name,
        //        in_sno,
        //    });
        //}

        private string PlayerKey(string in_l1, string in_l2, string in_l3, Item item)
        {
            string in_sno = item.getProperty("in_sno", "").Trim().ToUpper();
            return string.Join("-", new List<string>
            {
                in_sno,
                in_l1,
                in_l2,
                in_l3,
            });
        }

        private string StaffKey(string in_l1, string in_l2, string in_l3, Item item)
        {
            string in_name = item.getProperty("in_name", "").Trim().ToUpper();
            string in_sno = item.getProperty("in_sno", "").Trim().ToUpper();
            return string.Join("-", new List<string>
            {
                in_name,
                in_sno,
                in_l1,
                in_l2,
                in_l3,
            });
        }

        private string SectKey(Item item)
        {
            string in_current_org = item.getProperty("in_current_org", "").Trim().ToUpper();
            string in_l1 = item.getProperty("in_l1", "").Trim().ToUpper();
            string in_l2 = item.getProperty("in_l2", "").Trim().ToUpper();
            string in_l3 = item.getProperty("in_l3", "").Trim().ToUpper();
            string in_index = item.getProperty("in_index", "").Trim().ToUpper();

            return string.Join("", new List<string>
            {
                in_current_org,
                in_l1,
                in_l2,
                in_l3,
            });
        }

        private string TeamKey(Item item)
        {
            string in_current_org = item.getProperty("in_current_org", "").Trim().ToUpper();
            string in_l1 = item.getProperty("in_l1", "").Trim().ToUpper();
            string in_l2 = item.getProperty("in_l2", "").Trim().ToUpper();
            string in_l3 = item.getProperty("in_l3", "").Trim().ToUpper();
            string in_index = item.getProperty("in_index", "").Trim().ToUpper();

            return string.Join("", new List<string>
            {
                in_current_org,
                in_l1,
                in_l2,
                in_l3,
                in_index,
            });
        }

        private List<TRule> MapRules(TConfig cfg, Item itmRules)
        {
            var rules = new List<TRule>();
            var count = itmRules.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var itmRule = itmRules.getItemByIndex(i);
                rules.Add(MapRule(cfg, itmRule));
            }
            return rules;
        }

        private TRule MapRule(TConfig cfg, Item itmRule)
        {
            var result = new TRule
            {
                in_mode = itmRule.getProperty("in_mode", ""),
                in_type = itmRule.getProperty("in_type", ""),
                in_property = itmRule.getProperty("in_property", ""),
                in_operator = itmRule.getProperty("in_operator", ""),
                in_value = itmRule.getProperty("in_value", ""),
                in_limit = itmRule.getProperty("in_limit", "0"),
                in_func = itmRule.getProperty("in_func", ""),
                in_n1 = itmRule.getProperty("in_n1", "0"),
                in_n2 = itmRule.getProperty("in_n2", "0"),
                in_message = itmRule.getProperty("in_message", ""),
                in_type_label = itmRule.getProperty("in_type_label", ""),
                in_func_label = itmRule.getProperty("in_func_label", ""),
            };

            result.limit = GetInt(result.in_limit);
            result.n1 = GetInt(result.in_n1);
            result.n2 = GetInt(result.in_n2);

            return result;
        }

        private class TRule
        {
            public string in_mode { get; set; }
            public string in_type { get; set; }
            public string in_property { get; set; }
            public string in_operator { get; set; }
            public string in_value { get; set; }
            public string in_limit { get; set; }
            public string in_func { get; set; }
            public string in_n1 { get; set; }
            public string in_n2 { get; set; }
            public string in_message { get; set; }
            public string in_type_label { get; set; }
            public string in_func_label { get; set; }

            public int limit { get; set; }
            public int n1 { get; set; }
            public int n2 { get; set; }
        }

        private Item GetMeetingUsersFromMeeting(TConfig cfg, string in_current_org)
        {
            var org_cond = in_current_org == ""
                ? ""
                : "AND in_current_org = N'" + in_current_org + "'";

            string sql = @"
                SELECT 
                    id
                    , in_current_org
                    , in_name
                    , in_sno
                    , in_l1
                    , in_l2
                    , in_l3
                    , in_index
                    , in_team
                    , in_creator_sno
                FROM
                    {#table} t1 WITH(NOLOCK)
                WHERE
                    source_id = '{#meeting_id}'
                    AND t1.in_l1 <> N'常年會費'
                    AND in_creator_sno = '{#in_creator_sno}'
                    {#org_cond}
                ORDER BY
                    in_current_org
                    , in_short_org
                    , in_l1
                    , in_l2
                    , in_l3
                    , in_index
            ";

            sql = sql.Replace("{#table}", cfg.mt_user_table)
                .Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_creator_sno}", cfg.in_creator_sno)
                .Replace("{#org_cond}", org_cond);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        private Item GetMeetingUsersFromRule(TConfig cfg, TRule rule)
        {
            List<string> conds = new List<string>();

            conds.Add("[source_id] = '" + cfg.meeting_id + "'");

            switch (rule.in_type)
            {
                case "in_creator_sno":
                    conds.Add("[in_creator_sno] = N'" + cfg.in_creator_sno + "'");
                    break;

                case "in_current_org":
                    conds.Add("[in_creator_sno] = N'" + cfg.in_creator_sno + "'");
                    conds.Add("[in_current_org] = N'" + cfg.in_current_org + "'");
                    break;

                case "in_sno":
                    //conds.Add("[in_creator_sno] = N'" + cfg.in_creator_sno + "'");
                    conds.Add("[in_sno] = N'" + cfg.in_sno + "'");
                    break;
            }

            if (rule.in_property != "")
            {
                conds.Add("[" + rule.in_property + "] = N'" + rule.in_value + "'");
            }
            else
            {
                conds.Add("[in_l1] = N'" + cfg.in_l1 + "'");
                conds.Add("[in_l2] = N'" + cfg.in_l2 + "'");
                conds.Add("[in_l3] = N'" + cfg.in_l3 + "'");
                conds.Add("[in_l1] <> N'隊職員'");
            }

            string sql = @"
                SELECT DISTINCT
                    in_current_org
				    , in_short_org
                    , in_l1
                    , in_l2
                    , in_l3
                    , in_index
                    , in_creator_sno
                FROM
                    {#table} t1 WITH(NOLOCK)
                WHERE
                    {#conds}
                ORDER BY
                    in_current_org
                    , in_short_org
                    , in_l1
                    , in_l2
                    , in_l3
                    , in_index
            ";

            sql = sql.Replace("{#table}", cfg.mt_user_table)
                .Replace("{#conds}", string.Join(" AND ", conds));

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        private Item GetMeetingRules(TConfig cfg, List<string> modes)
        {
            string sql = @"
                SELECT 
	                t1.*
					, t2.label AS 'in_type_label'
					, t3.label AS 'in_func_label'
                FROM 
	                {#table} t1 WITH(NOLOCK) 
				LEFT OUTER JOIN
                    VU_Mt_RuleType t2
				    ON t2.value = t1.in_type
				LEFT OUTER JOIN
                    VU_Mt_RuleFunc t3
				    ON t3.value = t1.in_func
                WHERE 
	                t1.source_id = '{#meeting_id}'
                    AND t1.in_mode IN ({#mode})
					AND ISNULL(t1.in_disabled, 0) = 0
            ";

            sql = sql.Replace("{#table}", cfg.mt_rules_table)
                .Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#mode}", string.Join(", ", modes.Select(x => "'" + x + "'")));

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strItemType { get; set; }

            public string meeting_id { get; set; }
            public string in_creator_sno { get; set; }
            public string in_current_org { get; set; }
            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }
            public string in_index { get; set; }
            public string in_sno { get; set; }
            public string in_gender { get; set; }
            public string in_birth { get; set; }
            public string scene { get; set; }

            public string mt_table { get; set; }
            public string mt_user_table { get; set; }
            public string mt_rules_table { get; set; }

            public bool is_special_coach { get; set; }
        }

        private class TOrg
        {
            public string in_current_org { get; set; }
            public Dictionary<string, Item> players { get; set; }
            public Dictionary<string, Item> managers { get; set; }
            public Dictionary<string, Item> leaders { get; set; }
            public Dictionary<string, Item> a_coaches { get; set; }
            public Dictionary<string, Item> m_coaches { get; set; }
            public Dictionary<string, Item> w_coaches { get; set; }
            public Dictionary<string, Item> others { get; set; }
        }

        private DateTime GetDtm(string value)
        {
            if (value == "") return DateTime.MinValue;
            var result = DateTime.MinValue;
            if (DateTime.TryParse(value, out result)) return result;
            return DateTime.MinValue;
        }

        private int GetInt(string value, int defV = 0)
        {
            if (value == "") return 0;
            int result = defV;
            if (int.TryParse(value, out result)) return result;
            return defV;
        }
    }
}