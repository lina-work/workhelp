﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Judo.Fight
{
    public class in_playerid_judo : Item
    {
        public in_playerid_judo(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的:創建[柔道]選手證
                說明:2020.10.29 還原到依著組別出 2020.08.17(因著109年全國柔道錦標賽做在取資料時做了調整並切割字串)
                輸入:
                2020.11.03 用此邏輯出選手證
                T0.隊職員(隊職員單獨出一份)
                T1.社會(匯出前檢查是否報名過大專,高中,國中如果有則不出)(只有報名社會組才出)
                T2.大專(包含社會 但不包含格式組)
                T3.高中(包含社會 但不包含格式組)
                T4.國中(包含社會 但不包含格式組)
                T5.國小(不包含格式組)
                T6.團體(只有報名團體組的才出)
                T7.格式(格式組單獨出一份)
                T8.非以上組別(人數超過1200會出錯)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]in_playerid_judo";

            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
                bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            string aml = "";
            string sql = "";
            string strError = "";
            string strUserId = inn.getUserID();
            //string strIdentityId = inn.getUserAliases();

            _InnH.AddLog(strMethodName, "MethodSteps");

            Item itmR = this;
            //CCO.Utilities.WriteDebug("in_playerid_judo", "this:" + this.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),

                meeting_id = itmR.getProperty("meeting_id", ""),

                url = @"C:\Aras\Vault\judo\",//讀取大頭照的路徑
            };

            bool open = IsInResumelists(cfg);
            Item Resume = this;
            string SQL_OnlyMyGym = "";


            aml = "<AML>" +
            "<Item type='In_Resume' action='get'>" +
            "<in_user_id>" + strUserId + "</in_user_id>" +
            "</Item></AML>";
            Resume = inn.applyAML(aml);

            if (!open)//不是共同講師走這
            {
                if (CCO.Permissions.IdentityListHasId(Aras.Server.Security.Permissions.Current.IdentitiesList, "7AC8B33DA0F246DDA70BB244AB231E29"))
                {

                    SQL_OnlyMyGym = " AND [in_meeting_user].in_group=N'" + Resume.getProperty("in_group", "") + "'";
                }
            }

            if (cfg.meeting_id == "")
            {
                throw new Exception("課程id不得為空白");
            }


            //1.取得課程主檔
            cfg.itmMeeting = GetMeetingInfo(cfg);
            cfg.mt_title = cfg.itmMeeting.getProperty("in_title", "");//賽事名稱

            //樣板與匯出資訊
            Item itmXls = cfg.inn.applyMethod("In_Meeting_Excel_Path", "<in_name>template_4_path</in_name>");
            string Export_Path = itmXls.getProperty("export_path", "");
            string Template_Path = itmXls.getProperty("template_path", "");

            //Word
            Xceed.Words.NET.DocX doc;//word
            doc = Xceed.Words.NET.DocX.Load(Template_Path);//載入模板
            var docTables = doc.Tables[0];//取得模板第一個表格
            //var d = doc.Pictures[0];

            //CCO.Utilities.WriteDebug("in_playerid", "0");

            string output_type = this.getProperty("output_type", "T9");//區分匯出的組別
            string type_name = "";//拚進去的字串
            string meeting_id = cfg.meeting_id;

            switch (output_type)
            {
                case "T0":
                    //隊職員單獨出一份

                    type_name = "隊職員";
                    sql = "select * from in_meeting_user where in_l1=N'隊職員' and source_id='" + meeting_id + "' and [In_Meeting_User].in_note_state='official'" + SQL_OnlyMyGym + "order By in_gender desc,in_current_org,in_name,in_excel_order,in_excel_order_2,in_excel_order_3";
                    break;

                case "T1":
                    //出社會組之前判斷有沒有報過大專 高中 國中 有的則不出(只報名社會組的才出) 

                    type_name = "只報社會";

                    sql = "select string_agg(IN_SECTION_NAME, ', ') as in_lx,in_name,in_short_org,in_photo1,in_l1,in_l2,in_l3,in_gender,in_current_org,in_excel_order,in_excel_order_2,in_excel_order_3,in_sign_no,in_section_no from In_Meeting_User where source_id='" + meeting_id + "' and in_l2 LIKE '%社會%' and in_sno NOT IN (select in_sno from in_meeting_user where (in_l2 LIKE '%大專%' or in_l2 LIKE '%高中%' or in_l2 LIKE '%國中%' or in_l2 LIKE '%國小%') and source_id='" + meeting_id + "') group by in_name,in_short_org,in_photo1,in_l1,in_l2,in_l3,in_gender,in_current_org,in_excel_order,in_excel_order_2,in_excel_order_3,in_sign_no,in_section_no order By in_gender desc,in_current_org,in_name,in_excel_order,in_excel_order_2,in_excel_order_3,in_sign_no";


                    break;

                case "T2":
                    //大專可跨社會(出大專有報社會 就一樣把組別拚進去  格式組不包含在這裡)
                    type_name = "大專(含社會)";

                    sql = "select string_agg(IN_SECTION_NAME, ', ') as in_lx,in_name,in_short_org,in_photo1,in_l1,in_l2,in_l3,in_gender,in_current_org,in_excel_order,in_excel_order_2,in_excel_order_3,in_sign_no,in_section_no from In_Meeting_User where source_id='" + meeting_id + "'and (in_l2 LIKE '%社會%' or in_l2 LIKE '%大專%') and in_l1 <> '格式組' and in_sno IN (select in_sno from in_meeting_user where in_l2 LIKE '%大專%' and source_id='" + meeting_id + "') group by in_name,in_short_org,in_photo1,in_l1,in_l2,in_l3,in_gender,in_current_org,in_excel_order,in_excel_order_2,in_excel_order_3,in_sign_no,in_section_no order By in_gender desc,in_current_org,in_name,in_excel_order,in_excel_order_2,in_excel_order_3,in_sign_no";

                    break;

                case "T3":
                    //高中可跨社會(出高中有報社會 就一樣把組別拚進去  格式組不包含在這裡)

                    type_name = "高中(含社會)";

                    sql = "select string_agg(IN_SECTION_NAME, ', ') as in_lx,in_name,in_short_org,in_photo1,in_l1,in_l2,in_l3,in_gender,in_current_org,in_excel_order,in_excel_order_2,in_excel_order_3,in_sign_no,in_section_no from In_Meeting_User where source_id='" + meeting_id + "'and (in_l2 LIKE '%社會%' or in_l2 LIKE '%高中%') and in_l1 <> '格式組' and in_sno IN (select in_sno from in_meeting_user where in_l2 LIKE '%高中%' and source_id='" + meeting_id + "') group by in_name,in_short_org,in_photo1,in_l1,in_l2,in_l3,in_gender,in_current_org,in_excel_order,in_excel_order_2,in_excel_order_3,in_sign_no,in_section_no order By in_gender desc,in_current_org,in_name,in_excel_order,in_excel_order_2,in_excel_order_3,in_sign_no";

                    break;

                case "T4":
                    //國中可跨社會(出國中有報社會 就一樣把組別拚進去  格式組不包含在這裡)

                    type_name = "國中(含社會)";
                    sql = "select string_agg(IN_SECTION_NAME, ', ') as in_lx,in_name,in_short_org,in_photo1,in_l1,in_l2,in_l3,in_gender,in_current_org,in_excel_order,in_excel_order_2,in_excel_order_3,in_sign_no,in_section_no from In_Meeting_User where source_id='" + meeting_id + "'and (in_l2 LIKE '%社會%' or in_l2 LIKE '%國中%') and in_l1 <> '格式組' and in_sno IN (select in_sno from in_meeting_user where in_l2 LIKE '%國中%' and source_id='" + meeting_id + "') group by in_name,in_short_org,in_photo1,in_l1,in_l2,in_l3,in_gender,in_current_org,in_excel_order,in_excel_order_2,in_excel_order_3,in_sign_no,in_section_no order By in_gender desc,in_current_org,in_name,in_excel_order,in_excel_order_2,in_excel_order_3,in_sign_no";

                    break;

                case "T5":
                    //國小不會報社會(格式組不包含在這裡)

                    type_name = "國小";
                    sql = "select string_agg(IN_SECTION_NAME, ', ') as in_lx,in_name,in_short_org,in_photo1,in_l1,in_l2,in_l3,in_gender,in_current_org,in_excel_order,in_excel_order_2,in_excel_order_3,in_sign_no,in_section_no from in_meeting_user where in_l2 LIKE '%國小%' and in_l1 <> '格式組' and source_id='" + meeting_id + "' and [In_Meeting_User].in_note_state='official'" + SQL_OnlyMyGym + " group by in_name,in_short_org,in_photo1,in_l1,in_l2,in_l3,in_gender,in_current_org,in_excel_order,in_excel_order_2,in_excel_order_3,in_sign_no,in_section_no order By in_gender desc,in_current_org,in_name,in_excel_order,in_excel_order_2,in_excel_order_3,in_sign_no";
                    break;

                case "T6":
                    // 2.團體組單獨出一份(只有報團體)

                    type_name = "只報團體";

                    sql = "select string_agg(IN_SECTION_NAME, ', ') as in_lx,in_name,in_short_org,in_photo1,in_l1,in_l2,in_l3,in_gender,in_current_org,in_excel_order,in_excel_order_2,in_excel_order_3,in_sign_no,in_section_no from In_Meeting_User where source_id='" + meeting_id + "' and in_l1=N'團體組' and in_sno NOT IN (select in_sno from in_meeting_user where (in_l1 = N'個人組' or in_l1 = N'格式組') and source_id='" + meeting_id + "') group by in_name,in_short_org,in_photo1,in_l1,in_l2,in_l3,in_gender,in_current_org,in_excel_order,in_excel_order_2,in_excel_order_3,in_sign_no,in_section_no order By in_gender desc,in_current_org,in_name,in_excel_order,in_excel_order_2,in_excel_order_3,in_sign_no";

                    break;

                case "T7":
                    // 4.格式組單獨出一份

                    type_name = "格式";
                    sql = "select string_agg(IN_SECTION_NAME, ', ') as in_lx,in_name,in_short_org,in_photo1,in_l1,in_l2,in_l3,in_gender,in_current_org,in_excel_order,in_excel_order_2,in_excel_order_3,in_sign_no,in_section_no from in_meeting_user where in_l1=N'格式組' and source_id='" + meeting_id + "' and [In_Meeting_User].in_note_state='official'" + SQL_OnlyMyGym + " group by in_name,in_short_org,in_photo1,in_l1,in_l2,in_l3,in_gender,in_current_org,in_excel_order,in_excel_order_2,in_excel_order_3,in_sign_no,in_section_no order By in_gender desc,in_current_org,in_name,in_excel_order,in_excel_order_2,in_excel_order_3,in_sign_no";
                    break;

                case "T8":
                    //沒有以上組別(人數1200以下)

                    type_name = "全部(1200人以下)";
                    sql = "select * from in_meeting_user where source_id='" + meeting_id + "' and [In_Meeting_User].in_note_state='official'" + SQL_OnlyMyGym + "order By in_gender desc,in_current_org,in_name,in_excel_order,in_excel_order_2,in_excel_order_3,in_sign_no";
                    break;
                case "T9":
                    // 4.格式組單獨出一份

                    type_name = "成年";
                    sql = "select string_agg(IN_SECTION_NAME, ', ') as in_lx,in_name,in_short_org,in_photo1,in_l1,in_l2,in_l3,in_gender,in_current_org,in_excel_order,in_excel_order_2,in_excel_order_3,in_sign_no,in_section_no from in_meeting_user where in_l1=N'成年組' and source_id='" + meeting_id + "' and [In_Meeting_User].in_note_state='official'" + SQL_OnlyMyGym + " group by in_name,in_short_org,in_photo1,in_l1,in_l2,in_l3,in_gender,in_current_org,in_excel_order,in_excel_order_2,in_excel_order_3,in_sign_no,in_section_no order By in_gender desc,in_current_org,in_name,in_excel_order,in_excel_order_2,in_excel_order_3,in_sign_no";
                    break;
            }


            /*
            //抓取指定的筆數(2020.10.14 超過1000筆會掛掉)
            sql = "select * from in_meeting_user where source_id='" + meeting_id + "' and [In_Meeting_User].in_note_state='official'" + SQL_OnlyMyGym + "order By in_gender desc,in_current_org,in_name offset 0 row fetch next 1000 rows only";
            */

            CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            Item itmsOrgs = inn.applySQL(sql);

            Dictionary<string, Item> DicOrgs = MapOrgs(cfg, itmsOrgs);

            AppendWordPlayer(cfg, doc, docTables, DicOrgs);

            AppendWordStaff(cfg, doc, docTables, DicOrgs);


            //CCO.Utilities.WriteDebug("in_playerid", "3");
            doc.Tables[0].Remove();
            doc.RemoveParagraphAt(0);
            doc.RemoveParagraphAt(0);

            string xlsName = cfg.itmMeeting.getProperty("in_title", "");
            xlsName = xlsName.Replace("+", "")
                            .Replace("-", "")
                            .Replace(".", "")
                            .Replace(" ", "");
            doc.SaveAs(Export_Path + xlsName + "_選手證(" + type_name + ").docx");
            itmR.setProperty("xls_name", xlsName + "_選手證(" + type_name + ").docx");


            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

            return itmR;

        }

        private Dictionary<string, Item> MapOrgs(TConfig cfg, Item itmOrgs)
        {
            Dictionary<string, Item> DicOrgs = new Dictionary<string, Item>();

            string in_address = cfg.itmMeeting.getProperty("in_address", "");//比賽地點
            string in_date_s = cfg.itmMeeting.getProperty("in_date_s", "");//賽事開始日
            string in_date_e = cfg.itmMeeting.getProperty("in_date_e", "");//賽事結束日

            DateTime in_date_s_dt = DateTime.Parse(in_date_s);
            DateTime in_date_e_dt = DateTime.Parse(in_date_e);
            var time_date_s = string.Format("{0}年{1}月{2}", new System.Globalization.TaiwanCalendar().GetYear(in_date_s_dt), in_date_s_dt.Month, in_date_s_dt.Day);
            var time_date_e = string.Format("{2}日", new System.Globalization.TaiwanCalendar().GetYear(in_date_e_dt), in_date_e_dt.Month, in_date_e_dt.Day);

            var opt_map = MapL3Opts(cfg);

            string filename = "";

            int count = itmOrgs.getItemCount();
            count = 10;

            for (int i = 0; i < count; i++)
            {
                Item Org = itmOrgs.getItemByIndex(i);

                string in_current_org = Org.getProperty("in_current_org", "");
                string in_name = Org.getProperty("in_name", "");
                string in_l1 = Org.getProperty("in_l1", "");
                string in_l2 = Org.getProperty("in_l2", "");
                string in_l3 = Org.getProperty("in_l3", "");
                string l2_weight = GetL2Weight(cfg, opt_map, Org);

                string OrgName = "";

                //若是隊職員 則多拚二階進去當key值
                if (in_l1 == "隊職員")
                {
                    OrgName = in_current_org + "_" + in_name + "_" + in_l2;
                }
                else
                {
                    OrgName = in_current_org + "_" + in_name;
                }


                string in_sno = Org.getProperty("in_sno", "");
                string in_lx = Org.getProperty("in_lx", "");
                //CCO.Utilities.WriteDebug("in_playerid", "in_lx:" + in_lx);



                //CCO.Utilities.WriteDebug("in_playerid", "OrgName_158:" + OrgName);
                Item orgnode = cfg.inn.newItem();
                orgnode.setProperty("sheet_index", "0");
                orgnode.setProperty("last_row", "3");
                
                string aml = "<AML>" +
                "<Item type='File' action='get'>" +
                "<id>" + Org.getProperty("in_photo1", "") + "</id>" +
                "</Item></AML>";
                Item Files = cfg.inn.applyAML(aml);
                //CCO.Utilities.WriteDebug("in_playerid", "Files:" + Files.dom.InnerXml); 
                if (Files.getItemCount() > 0)
                {
                    filename = Files.getProperty("filename", "");//取得檔名
                }

                orgnode.setProperty("in_photo", Org.getProperty("in_photo1", ""));
                orgnode.setProperty("filename", filename);
                orgnode.setProperty("in_name", Org.getProperty("in_name", ""));
                orgnode.setProperty("in_current_org", Org.getProperty("in_current_org", ""));
                orgnode.setProperty("in_l1", Org.getProperty("in_l1", ""));
                orgnode.setProperty("in_l2", Org.getProperty("in_l2", ""));
                orgnode.setProperty("in_stuff_c1", Org.getProperty("in_stuff_c1", ""));
                orgnode.setProperty("in_short_org", Org.getProperty("in_short_org", ""));
                orgnode.setProperty("in_sign_no", Org.getProperty("in_sign_no", ""));
                orgnode.setProperty("in_section_no", Org.getProperty("in_section_no", ""));
                orgnode.setProperty("l2_weight", l2_weight + "\n");


                //2020.08.17(應著109年全國柔道錦標賽的新格式)
                if (Org.getProperty("in_l1", "").Contains("團體"))
                {
                    string re_in_l3 = text_check(Org.getProperty("in_l3", ""));

                    if (re_in_l3.Contains('('))
                        re_in_l3 = re_in_l3.Split('(')[0];

                    orgnode.setProperty("in_l3_group", Org.getProperty("in_section_no", "") + re_in_l3);
                    //orgnode.setProperty("in_l3_group",in_lx);
                }
                else if (Org.getProperty("in_l1", "").Contains("個人"))
                {
                    string re_in_l2 = text_check(Org.getProperty("in_l2", ""));

                    if (re_in_l2.Contains('('))
                        re_in_l2 = re_in_l2.Split('(')[0];

                    orgnode.setProperty("in_l3", Org.getProperty("in_section_no", "") + re_in_l2 + "-" + Org.getProperty("in_l3", ""));
                    //orgnode.setProperty("in_l3",in_lx);

                }
                else if (Org.getProperty("in_l1", "").Contains("格式"))
                {

                    string re_in_l2 = text_check(Org.getProperty("in_l2", ""));

                    if (re_in_l2.Contains('('))
                        re_in_l2 = re_in_l2.Split('(')[0];

                    orgnode.setProperty("in_l3", Org.getProperty("in_section_no", "") + re_in_l2 + "-" + Org.getProperty("in_l3", ""));
                    //orgnode.setProperty("in_l3",in_lx);
                }
                else if (Org.getProperty("in_l1", "").Contains("隊職員"))
                {
                    string re_in_l2 = text_check(Org.getProperty("in_l2", ""));

                    if (re_in_l2.Contains('('))
                        re_in_l2 = re_in_l2.Split('(')[0];

                    orgnode.setProperty("in_l3", Org.getProperty("in_l3", ""));
                    //orgnode.setProperty("in_l3",in_lx);

                }
                else
                {
                    string re_in_l2 = text_check(Org.getProperty("in_l2", ""));

                    if (re_in_l2.Contains('('))
                        re_in_l2 = re_in_l2.Split('(')[0];


                    orgnode.setProperty("in_l3", Org.getProperty("in_section_no", "") + Org.getProperty("in_l1", "") + "-" + re_in_l2 + "-" + Org.getProperty("in_l3", ""));
                }


                //in_meeting
                orgnode.setProperty("in_title", cfg.mt_title);
                orgnode.setProperty("in_address", in_address);
                orgnode.setProperty("in_date_se", time_date_s + "~" + time_date_e);
                orgnode.setProperty("in_lx", in_lx);

                if (!DicOrgs.ContainsKey(OrgName))
                {
                    DicOrgs.Add(OrgName, orgnode);
                }
                else
                {
                    //同個人在一個單位可能被報多次 所以隊職員不處理
                    if (Org.getProperty("in_l1", "") != "隊職員")
                    {
                        string l3 = DicOrgs[OrgName].getProperty("in_l3", "");
                        string l3_group = "";

                        if (Org.getProperty("in_l1", "").Contains("團體"))
                        {

                            string re_in_l2 = text_check(Org.getProperty("in_l2", ""));

                            if (re_in_l2.Contains('('))
                            {
                                re_in_l2 = re_in_l2.Split('(')[0];
                            }
                            //DicOrgs[OrgName].setProperty("in_l3_group",re_in_l2.Trim('\n'));
                            DicOrgs[OrgName].setProperty("in_l3_group", Org.getProperty("in_section_no", "") + in_lx.Replace("團體組-", ""));
                        }
                        else if (Org.getProperty("in_l1", "").Contains("個人"))
                        {
                            string re_in_l2 = text_check(Org.getProperty("in_l2", ""));

                            if (re_in_l2.Contains('('))
                            {
                                re_in_l2 = re_in_l2.Split('(')[0];
                            }
                            l3 += "\n" + Org.getProperty("in_section_no", "") + re_in_l2 + "-" + Org.getProperty("in_l3", "");

                            //DicOrgs[OrgName].setProperty("in_l3",l3.Trim('\n'));
                            //l3 += "\n" + in_lx.Replace("個人組-","");
                            DicOrgs[OrgName].setProperty("in_l3", l3.Trim('\n'));
                        }
                        else
                        {
                            string re_in_l2 = text_check(Org.getProperty("in_l2", ""));

                            if (re_in_l2.Contains('('))
                                re_in_l2 = re_in_l2.Split('(')[0];


                            l3 += "\n" + Org.getProperty("in_section_no", "") + Org.getProperty("in_l1", "") + "-" + re_in_l2 + "-" + Org.getProperty("in_l3", "");

                            //DicOrgs[OrgName].setProperty("in_l3",l3.Trim('\n'));
                            //l3 += "\n" + in_lx.Replace("個人組-","");
                            DicOrgs[OrgName].setProperty("in_l3", l3.Trim('\n'));
                        }
                    }
                }
            }

            return DicOrgs;
        }

        private void AppendWordPlayer(TConfig cfg, Xceed.Words.NET.DocX doc, Xceed.Document.NET.Table docTables, Dictionary<string, Item> DicOrgs)
        {
            string id_1 = "";
            string id_2 = "";
            string id_3 = "";

            foreach (KeyValuePair<string, Item> DicOrg in DicOrgs)
            {
                Item Org = DicOrg.Value;
                string in_l1 = Org.getProperty("in_l1", "");

                if (in_l1.Contains("隊職員"))
                {
                    continue;
                }

                //賽事名稱
                string in_title = Org.getProperty("in_title", "");
                //單位(簡稱)
                string in_short_org = Org.getProperty("in_short_org", "");
                //姓名
                string in_name = Org.getProperty("in_name", "");

                doc.InsertParagraph();
                docTables = doc.InsertTable(doc.Tables[0]);

                //賽事名稱
                docTables.Rows[1].Cells[0].Paragraphs.First().Append(in_title).Font("標楷體").FontSize(16);
                //選手證
                docTables.Rows[1].Cells[0].Paragraphs.First().Append("\n" + "選手證").Font("標楷體").FontSize(16).Alignment = Xceed.Document.NET.Alignment.center;
                //單位(簡稱)
                docTables.Rows[2].Cells[1].Paragraphs.First().Append(in_short_org).Font("標楷體").FontSize(MapFS(in_short_org));
                //姓名
                docTables.Rows[3].Cells[1].Paragraphs.First().Append(in_name).Font("標楷體").FontSize(MapFS(in_name));


                //沒有第三階就抓第二階
                string in_lx = "";
                string in_lx_group = "";

                string l1_type = "";
                string in_section_no = Org.getProperty("in_section_no", "");
                if (in_l1.Contains("團體"))
                {
                    l1_type = "in_l3_group";
                }
                else
                {
                    l1_type = "in_l3";
                }

                string l1_val = Org.getProperty(l1_type, "");

                if (l1_val == "")
                {
                    //無第三階 則用 第一+第二階呈現(青少年男子組:限18歲以下 第一級: - 50kg/500)
                    in_lx = in_l1 + " " + text_check(Org.getProperty("in_l2", ""));//第二階會放金額所以切['/']取前面
                }
                else
                {
                    in_lx_group = Org.getProperty("in_l3_group", "");
                    //in_lx = Org.getProperty("in_l3", "");
                    in_lx = Org.getProperty("l2_weight", "").Trim('\n');
                }

                //2020.08.17(應著109年全國柔道錦標賽的新格式 量級>個人,教練>團體)
                //x量級(個人)
                docTables.Rows[4].Cells[1].Paragraphs.First().Append(in_lx).Font("標楷體").FontSize(12);

                //x教練(團體)
                docTables.Rows[5].Cells[1].Paragraphs.First().Append(in_lx_group.Replace("團體組-", "")).Font("標楷體").FontSize(12);


                //CCO.Utilities.WriteDebug("in_playerid", "1111_s:" + Org.getProperty("in_name",""));
                //有大頭照才切
                if (Org.getProperty("in_photo", "") != "")
                {
                    //路徑切出來
                    id_1 = Org.getProperty("in_photo", "").Substring(0, 1);
                    id_2 = Org.getProperty("in_photo", "").Substring(1, 2);
                    id_3 = Org.getProperty("in_photo", "").Substring(3, 29);
                    //CCO.Utilities.WriteDebug("in_playerid", url + id_1 + @"\" + id_2 + @"\" + id_3 + @"\" + Org.getProperty("filename",""));

                    try
                    {
                        Xceed.Document.NET.Image img = doc.AddImage(cfg.url + id_1 + @"\" + id_2 + @"\" + id_3 + @"\" + Org.getProperty("filename", ""));//取得路徑圖片
                        Xceed.Document.NET.Picture p = img.CreatePicture(150, 100);

                        docTables.Rows[6].Cells[0].Paragraphs.First().AppendPicture(p);
                    }
                    catch (Exception ex)
                    {

                    }
                }
            }
        }

        private void AppendWordStaff(TConfig cfg, Xceed.Words.NET.DocX doc, Xceed.Document.NET.Table docTables, Dictionary<string, Item> DicOrgs)
        {
            string id_1 = "";
            string id_2 = "";
            string id_3 = "";

            foreach (KeyValuePair<string, Item> DicOrg in DicOrgs)
            {
                Item Org = DicOrg.Value;
                string in_l1 = Org.getProperty("in_l1", "");

                if (!in_l1.Contains("隊職員"))
                {
                    continue;
                }

                //賽事名稱
                string in_title = Org.getProperty("in_title", "");
                //單位(簡稱)
                string in_short_org = Org.getProperty("in_short_org", "");
                //姓名
                string in_name = Org.getProperty("in_name", "");

                doc.InsertParagraph();
                docTables = doc.InsertTable(doc.Tables[0]);

                //賽事名稱
                docTables.Rows[1].Cells[0].Paragraphs.First().Append(in_title).Font("標楷體").FontSize(16);
                //選手證
                docTables.Rows[1].Cells[0].Paragraphs.First().Append("\n" + Org.getProperty("in_l2", "") + "證").Font("標楷體").FontSize(18).Alignment = Xceed.Document.NET.Alignment.center; ;
                //單位(簡稱)
                docTables.Rows[2].Cells[1].Paragraphs.First().Append(in_short_org).Font("標楷體").FontSize(MapFS(in_short_org));
                //姓名
                docTables.Rows[3].Cells[1].Paragraphs.First().Append(in_name).Font("標楷體").FontSize(MapFS(in_name));

                //沒有第三階就抓第二階
                string in_lx = "";
                if (Org.getProperty("in_l3", "") == "")
                {
                    in_lx = Org.getProperty("in_l1", "") + " " + text_check(Org.getProperty("in_l2", ""));//第二階會放金額所以切['/']取前面
                }
                else
                {
                    in_lx = Org.getProperty("in_l1", "") + " " + Org.getProperty("in_l3", "");
                }

                //量級

                docTables.Rows[4].Cells[1].Paragraphs.First().Append(in_lx).Font("標楷體").FontSize(MapFS(in_lx));

                //有大頭照才切
                if (Org.getProperty("in_photo", "") != "")
                {
                    //路徑切出來
                    id_1 = Org.getProperty("in_photo", "").Substring(0, 1);
                    id_2 = Org.getProperty("in_photo", "").Substring(1, 2);
                    id_3 = Org.getProperty("in_photo", "").Substring(3, 29);
                    //CCO.Utilities.WriteDebug("in_playerid", url + id_1 + @"\" + id_2 + @"\" + id_3 + @"\" + Org.getProperty("filename",""));

                    try
                    {
                        Xceed.Document.NET.Image img = doc.AddImage(cfg.url + id_1 + @"\" + id_2 + @"\" + id_3 + @"\" + Org.getProperty("filename", ""));//取得路徑圖片
                        Xceed.Document.NET.Picture p = img.CreatePicture(150, 100);
                        docTables.Rows[6].Cells[0].Paragraphs.First().AppendPicture(p);
                    }
                    catch (Exception ex)
                    {

                    }
                }
            }
        }


        private int MapFS(string value)
        {
            int fontSize = 14;
            if (value.Length > 9) fontSize = 12;
            if (value.Length > 14) fontSize = 10;
            return fontSize;
        }

        private string GetL2Weight(TConfig cfg, Dictionary<string, Item> map, Item item)
        {
            string result = "";

            string in_l1 = item.getProperty("in_l1", "");
            string in_l2 = item.getProperty("in_l2", "");
            string in_l3 = item.getProperty("in_l3", "");

            string key = string.Join("-", new List<string>
            {
                in_l1,
                in_l2,
                in_l3,
            });

            if (map.ContainsKey(key))
            {
                Item itmOpt = map[key];

                result = itmOpt.getProperty("in_filter", "")
                    + itmOpt.getProperty("in_weight", "");
            }

            return result;
        }

        private Dictionary<string, Item> MapL3Opts(TConfig cfg)
        {
            Dictionary<string, Item> result = new Dictionary<string, Item>();

            string sql = @"
                SELECT
	                t3.in_grand_filter
	                , t3.in_filter
	                , t3.in_value
	                , t3.in_weight
                FROM 
	                IN_MEETING_SURVEYS t1 WITH(NOLOCK) 
                INNER JOIN 
	                IN_SURVEY t2 WITH(NOLOCK)
	                ON t2.id = t1.related_id
                INNER JOIN
	                IN_SURVEY_OPTION t3 WITH(NOLOCK)
	                ON t3.source_id = t1.related_id
                WHERE
	                t1.source_id = '{#meeting_id}'
	                AND t2.in_property = 'in_l3'
                ORDER BY
	                t3.sort_order
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_l1 = item.getProperty("in_grand_filter", "");
                string in_l2 = item.getProperty("in_filter", "");
                string in_l3 = item.getProperty("in_value", "");
                string key = string.Join("-", new List<string>
                {
                    in_l1,
                    in_l2,
                    in_l3,
                });

                if (!result.ContainsKey(key))
                {
                    result.Add(key, item);
                }
            }

            return result;
        }

        private bool IsInResumelists(TConfig cfg)
        {
            bool open = false;

            //判斷現在的登入者是否為此課程的共同講師 
            string aml = "<AML>" +
                    "<Item type='In_Meeting_Resumelist' action='get'>" +
                    "<source_id>" + cfg.meeting_id + "</source_id>" +
                    "</Item></AML>";
            Item Meeting_Resumelists = cfg.inn.applyAML(aml);

            for (int i = 0; i < Meeting_Resumelists.getItemCount(); i++)
            {
                Item Meeting_Resumelist = Meeting_Resumelists.getItemByIndex(i);
                //判定登入者ID是否存在於該課程共同講師裡面
                if (cfg.strUserId == Meeting_Resumelist.getProperty("in_user_id", ""))//此次登入者是共同講師
                {
                    open = true;
                }
                else
                {
                    open = false;
                }
            }

            return open;
        }

        private Item GetMeetingInfo(TConfig cfg)
        {
            string aml = "<AML>" +
                "<Item type='In_Meeting' action='get' id='" + cfg.meeting_id + "'>" +
                "<Relationships>" +
                "<Item type='In_Meeting_Surveys' action='get' select='related_id(id,in_questions,in_request,in_selectoption,in_question_type,in_property,in_expense,in_is_pkey)'>" +
                "<in_surveytype>1</in_surveytype>" +
                "<in_property condition='in'>'in_l1','in_l2','in_l3'</in_property>" +
                "</Item>" +
                "</Relationships>" +
            "</Item></AML>";

            return cfg.inn.applyAML(aml); ;
        }

        private string SectionName(string name)
        {
            string result = "";
            string[] rows = name.Split(new char[] { '\n' });

            foreach (var row in rows)
            {
                if (result != "") result += "\n";

                string[] arr = row.Split(new char[] { '-', '+', '：' });
                for (int i = 0; i < arr.Length - 1; i++)
                {
                    if (arr[i].ToLower().Contains("kg"))
                    {
                        continue;
                    }
                    // if (result.Contains(arr[i]))
                    // {
                    //     continue;
                    // }
                    result += arr[i];
                }
            }
            return result;
        }

        private string text_check(string value)
        {
            //2020.11.03 檢查丟進來的字串 是否包含{-,/} 以便切割
            bool a1 = false;
            bool a2 = false;
            bool a3 = false;

            if (value.Contains('-'))
                a1 = true;

            if (value.Contains('/'))
                a2 = true;

            if (value.Contains('~'))
                a3 = true;


            if (a1 && a2)
            {
                string[] values = value.Split(new char[2] { '-', '/' });
                return values[1];
            }
            else if (a1)
            {
                return value.Split('-')[1];
            }
            else if (a2)
            {
                return value.Split('/')[0];
            }
            else if (a3)
            {
                return value.Split('~')[1];
            }
            else
            {
                return value;
            }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }

            public string meeting_id { get; set; }
            public string url { get; set; }

            public Item itmMeeting { get; set; }
            public string mt_title { get; set; }
        }
    }
}
