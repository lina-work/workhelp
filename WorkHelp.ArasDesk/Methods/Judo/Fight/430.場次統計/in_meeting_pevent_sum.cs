﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Judo.Fight
{
    public class in_meeting_pevent_sum : Item
    {
        public in_meeting_pevent_sum(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 
                    場次統計
                日誌: 
                    - 2022-03-01: 預賽、決賽、加賽 (lina)
                    - 2021-12-02: 計算時間 (lina)
                    - 2021-11-13: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]in_meeting_pevent_sum";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,

                meeting_id = itmR.getProperty("meeting_id", ""),
                in_date = itmR.getProperty("in_date", ""),
                in_time = itmR.getProperty("in_time", ""),
                in_qry = itmR.getProperty("in_qry", ""),

                site_id = itmR.getProperty("site_id", ""),
                site_name = itmR.getProperty("site_name", ""),
                evts = itmR.getProperty("evts", ""),
                scene = itmR.getProperty("scene", ""),
                space_seconds = 0,
            };

            if (cfg.in_time == "NA")
            {
                cfg.in_time = "";
            }

            if (cfg.in_qry == "" || cfg.in_qry == "all")
            {
                //1+2+4+8
                cfg.in_qry = "15";
            }

            switch (cfg.scene)
            {
                case "change":
                    ChangeSite(cfg, itmR);
                    break;

                case "change_tag":
                    ChangeSiteTag(cfg, itmR);
                    break;

                case "save_time":
                    cfg.needUpdateTime = true;
                    SiteTabs(cfg, itmR);
                    break;

                default:
                    SiteTabs(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void ChangeSiteTag(TConfig cfg, Item itmReturn)
        {
            if (cfg.evts == "")
            {
                throw new Exception("要調場的場次不可為空值");
            }

            List<string> ids = GetEventIdList(cfg);
            if (ids.Count == 0)
            {
                throw new Exception("要調場的場次不可為空值");
            }

            Item itmSite = cfg.inn.applySQL("SELECT * FROM IN_MEETING_SITE WITH(NOLOCK) WHERE id = '" + cfg.site_id + "'");
            if (itmSite.isError() || itmSite.getResult() == "")
            {
                throw new Exception("查無場地資料");
            }

            string new_code = itmSite.getProperty("in_code", "");

            string sql_qry = @"
                SELECT
                	t1.id
                	, t1.in_type
                	, t1.in_parent
                	, t1.in_meeting
                	, t1.in_date_key
                	, t1.in_site
                	, t1.in_period
                	, t1.in_tree_no
                	, t2.id      AS 'stid'
                	, t2.in_code AS 'site_code'
                FROM 
                	IN_MEETING_PEVENT t1 WITH(NOLOCK) 
                INNER JOIN 
                	IN_MEETING_SITE t2 WITH(NOLOCK) 
                	ON t2.id = t1.in_site
                WHERE 
                	t1.id = '{#eid}'
            ";

            var pevts = new List<TPrnt>();
            var sql_upd_r = "UPDATE In_Meeting_PEvent SET in_site_move = NULL, in_site_time = NULL, in_show_site = {#code}, in_show_serial = {#serial} WHERE id = '{#id}'";
            var sql_upd_m = "UPDATE In_Meeting_PEvent SET in_site_move = '{#code}', in_site_time = getutcdate(), in_show_site = {#code}, in_show_serial = {#serial} WHERE id = '{#id}'";

            var sql_upd_r2 = "UPDATE In_Meeting_PEvent SET in_site_move = NULL, in_site_time = NULL WHERE in_parent = '{#id}'";
            var sql_upd_m2 = "UPDATE In_Meeting_PEvent SET in_site_move = '{#code}', in_site_time = getutcdate() WHERE in_parent = '{#id}'";

            foreach (string id in ids)
            {
                bool rollback = false;

                Item itmOld = cfg.inn.applySQL(sql_qry.Replace("{#eid}", id));
                if (itmOld.isError() || itmOld.getResult() == "")
                {
                    continue;
                }

                string old_type = itmOld.getProperty("in_type", "");
                string old_code = itmOld.getProperty("site_code", "");
                string old_parent = itmOld.getProperty("in_parent", "");
                string old_serial = itmOld.getProperty("in_tree_no", "");

                string new_serial = "";
                if (old_code == new_code)
                {
                    rollback = true;
                    new_serial = old_serial;
                }
                else
                {
                    new_serial = GetNewSerial(cfg, new_code, itmOld);
                }

                if (old_type == "s" && old_parent != "")
                {
                    var pevt = pevts.Find(x => x.id == old_parent);
                    if (pevt == null)
                    {
                        pevt = new TPrnt { id = old_parent, is_rollback = rollback, serial = new_serial };
                        pevts.Add(pevt);
                    }
                    continue;
                }

                string sql_upd = rollback ? sql_upd_r : sql_upd_m;

                sql_upd = sql_upd.Replace("{#id}", id)
                    .Replace("{#code}", new_code)
                    .Replace("{#serial}", new_serial);

                Item itmUpd = cfg.inn.applySQL(sql_upd);

                if (itmUpd.isError())
                {
                    throw new Exception("調場失敗");
                }
            }

            if (pevts.Count > 0)
            {
                foreach (var pevt in pevts)
                {
                    //主場次
                    string sql_upd1 = pevt.is_rollback ? sql_upd_r : sql_upd_m;

                    sql_upd1 = sql_upd1.Replace("{#id}", pevt.id)
                        .Replace("{#code}", new_code);

                    cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql_upd1);

                    cfg.inn.applySQL(sql_upd1);


                    //子場次
                    string sql_upd2 = pevt.is_rollback ? sql_upd_r2 : sql_upd_m2;

                    sql_upd2 = sql_upd2.Replace("{#id}", pevt.id)
                        .Replace("{#code}", new_code);

                    cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql_upd2);

                    cfg.inn.applySQL(sql_upd2);
                }
            }

            // string monitor_log = "[" + cfg.strDatabaseName + "]In_Local_Sync";
            // cfg.CCO.Utilities.WriteDebug(monitor_log, "change");
        }

        private string GetNewSerial(TConfig cfg, string new_site_code, Item itmOld)
        {
            string result = "";

            string in_meeting = itmOld.getProperty("in_meeting", "");
            string in_date_key = itmOld.getProperty("in_date_key", "");
            string in_period = itmOld.getProperty("in_period", "");
            string in_tree_no = itmOld.getProperty("in_tree_no", "");

            string period_cond = in_period == "0" || in_period == "1"
                ? "AND ISNULL(in_period, 0) IN (0, 1)"
                : "AND ISNULL(in_period, 0) IN (4, 7)";

            string sql = @"
                SELECT
                	MAX(ISNULL(in_show_serial, 0)) + 1 AS 'new_serial'
                FROM 
                	IN_MEETING_PEVENT t1 WITH(NOLOCK) 
                WHERE
					in_meeting = '{#meeting_id}'
					AND in_date_key = '{#in_date_key}'
					AND in_site_code = '{#site_code}'
					{#period_cond}
            ";

            sql = sql.Replace("{#meeting_id}", in_meeting)
                .Replace("{#in_date_key}", in_date_key)
                .Replace("{#site_code}", new_site_code)
                .Replace("{#period_cond}", period_cond);

            Item itmData = cfg.inn.applySQL(sql);
            if (!itmData.isError() && itmData.getResult() != "")
            {
                string new_serial = itmData.getProperty("new_serial", "");
                if (new_serial != "" && new_serial != "0")
                {
                    result = new_serial;
                }
            }

            if (result == "")
            {
                result = in_tree_no;
            }

            return result;
        }

        private class TPrnt
        {
            public string id { get; set; }
            public string serial { get; set; }
            public bool is_rollback { get; set; }
        }

        private void ChangeSite(TConfig cfg, Item itmReturn)
        {
            if (cfg.evts == "")
            {
                throw new Exception("要調場的場次不可為空值");
            }

            List<string> ids = GetEventIdList(cfg);
            if (ids.Count == 0)
            {
                throw new Exception("要調場的場次不可為空值");
            }

            List<TChangeEvent> list = GetEventListForChange(cfg, ids);

            int start_tree_no = GetNewTreeNoStart(cfg);

            for (int i = 0; i < list.Count; i++)
            {
                TChangeEvent evt = list[i];
                evt.new_site_id = cfg.site_id;
                evt.new_site_name = cfg.site_name;
                evt.new_tree_no = start_tree_no.ToString();

                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, cfg.meeting_id
                    + " " + evt.in_date_key
                    + " " + evt.old_site_name
                    + " 場次: " + evt.old_tree_no
                    + " 經調場變更為: " + evt.new_site_name
                    + " 場次: " + evt.new_tree_no
                    );

                string sql_upd = "UPDATE IN_MEETING_PEVENT SET"
                    + " in_site = '" + evt.new_site_id + "'"
                    + ", in_tree_no = '" + evt.new_tree_no + "'"
                    + " WHERE id = '" + evt.event_id + "'"
                    + "";

                Item itmUpd = cfg.inn.applySQL(sql_upd);

                if (itmUpd.isError())
                {
                    throw new Exception("調場失敗");
                }

                start_tree_no++;
            }
        }

        private int GetNewTreeNoStart(TConfig cfg)
        {
            int result = 0;
            int defv = 9000;

            string sql = "SELECT MAX(in_tree_no) AS 'max_tree_no'"
                + " FROM IN_MEETING_PEVENT WITH(NOLOCK)"
                + " WHERE in_meeting = '" + cfg.meeting_id + "'"
                + " AND in_date_key = '" + cfg.in_date + "'"
                + " AND in_site = '" + cfg.site_id + "'"
                + " AND ISNULL(in_tree_no, 0) > " + defv
                + "";

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item itmData = cfg.inn.applySQL(sql);

            if (!itmData.isError() && itmData.getResult() != "")
            {
                string max_tree_no = itmData.getProperty("max_tree_no", "0");
                result = GetInt(max_tree_no);
            }

            if (result < defv)
            {
                result = defv;
            }

            result = result + 1;

            return result;
        }

        private List<TChangeEvent> GetEventListForChange(TConfig cfg, List<string> ids)
        {
            List<TChangeEvent> result = new List<TChangeEvent>();

            string sql = @"
                SELECT 
	                t1.id
	                , t1.in_date_key
	                , t1.in_tree_id
	                , t1.in_tree_no
	                , t2.id      AS 'site_id'
	                , t2.in_name AS 'site_name'
                FROM 
	                IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_SITE t2 WITH(NOLOCK)
	                ON t2.id = t1.in_site
                WHERE
	                t1.id IN ({#ids})
                ORDER BY
	                t1.in_tree_no
            ";

            sql = sql.Replace("{#ids}", string.Join(",", ids.Select(x => "'" + x + "'")));

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);

                TChangeEvent obj = new TChangeEvent
                {
                    event_id = item.getProperty("id", ""),
                    in_date_key = item.getProperty("in_date_key", ""),
                    in_tree_id = item.getProperty("in_tree_id", ""),

                    old_site_id = item.getProperty("site_id", ""),
                    old_site_name = item.getProperty("site_name", ""),
                    old_tree_no = item.getProperty("in_tree_no", ""),

                };

                result.Add(obj);
            }

            return result;
        }

        private class TChangeEvent
        {
            public string event_id { get; set; }
            public string in_date_key { get; set; }
            public string in_tree_id { get; set; }

            public string old_site_id { get; set; }
            public string old_site_name { get; set; }
            public string old_tree_no { get; set; }

            public string new_site_id { get; set; }
            public string new_site_name { get; set; }
            public string new_tree_no { get; set; }
        }

        private List<string> GetEventIdList(TConfig cfg)
        {
            List<string> list = new List<string>();

            ////這行會避免json轉換為XML十日期被轉換格式。
            //Newtonsoft.Json.JsonConvert.DefaultSettings = () => new Newtonsoft.Json.JsonSerializerSettings
            //{
            //    DateParseHandling = Newtonsoft.Json.DateParseHandling.None
            //};

            var jlist = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Newtonsoft.Json.Linq.JObject>>(cfg.evts);

            foreach (var jobj in jlist)
            {
                var eid = GetJsonValue(jobj, "eid");
                if (eid != "")
                {
                    list.Add(eid);
                }
            }

            return list;
        }

        private string GetJsonValue(Newtonsoft.Json.Linq.JObject obj, string key)
        {
            try
            {
                Newtonsoft.Json.Linq.JToken value;
                if (obj.TryGetValue(key, out value))
                {
                    return value.ToString().Trim();
                }
                return "";
            }
            catch
            {
                return "";
            }
        }

        private void SiteTabs(TConfig cfg, Item itmReturn)
        {
            string sql = "SELECT in_title, in_battle_type, in_fight_time_incryn FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'";
            cfg.itmMeeting = cfg.inn.applySQL(sql);
            cfg.in_title = cfg.itmMeeting.getProperty("in_title", "");
            cfg.in_battle_type = cfg.itmMeeting.getProperty("in_battle_type", "");
            cfg.in_fight_time_incryn = cfg.itmMeeting.getProperty("in_fight_time_incryn", "");
            itmReturn.setProperty("in_title", cfg.in_title);

            if (cfg.in_fight_time_incryn == "1")
            {
                cfg.space_seconds = 60;
            }

            //日期選單
            List<TDay> days = MapDateMenu(cfg);
            AppendItems(cfg, days, "inn_date", "in_date_key", "in_date_key", itmReturn);

            if (cfg.in_date == "")
            {
                string today = DateTime.Now.ToString("yyyy-MM-dd");
                if (days.Exists(x => x.day == today))
                {
                    cfg.in_date = today;
                    itmReturn.setProperty("in_date", cfg.in_date);
                }
                else
                {
                    cfg.in_date = days[0].day;
                    itmReturn.setProperty("in_date", cfg.in_date);
                }
            }

            if (cfg.in_date != "")
            {
                Table(cfg, itmReturn);
            }
        }

        private void Table(TConfig cfg, Item itmReturn)
        {
            //場地選單
            List<Item> sites = GetSiteMenu(cfg);

            Item itmEmptySite = cfg.inn.newItem();
            itmEmptySite.setType("inn_site");
            itmEmptySite.setProperty("in_name", "請選擇");
            itmReturn.addRelationship(itmEmptySite);

            for (int i = 0; i < sites.Count; i++)
            {
                Item itmSite = sites[i];
                itmSite.setType("inn_site");
                itmReturn.addRelationship(itmSite);
            }

            //頁籤列表
            List<TTab> tabs = new List<TTab>();
            Dictionary<int, List<Item>> map = GetAllRows(cfg);
            for (int i = 0; i < sites.Count; i++)
            {
                Item itmSite = sites[i];
                string no = (i + 1).ToString();
                string id = "site_tab_" + no;
                string active = i == 0 ? "active" : "";
                var tab = new TTab
                {
                    id = id,
                    active = active,
                    title = itmSite.getProperty("in_name", ""),
                    in_site = itmSite.getProperty("id", ""),
                    in_code = GetInt(itmSite.getProperty("in_code", "0")),
                };
                tab.rows = GetRows(cfg, map, tab);
                tabs.Add(tab);
            }

            foreach (var tab in tabs)
            {
                FixTab(cfg, tab);
            }

            cfg.day_total_event = tabs.Sum(x => x.total_event);
            cfg.day_fought_event = tabs.Sum(x => x.fought_event);
            cfg.day_wait_event = tabs.Sum(x => x.wait_event);

            StringBuilder builder = new StringBuilder();

            builder.Append("<ul class='nav nav-pills' style='justify-content: start ;' id='pills-tab' role='tablist'>");
            AppendTabHeads(cfg, tabs, builder);
            builder.Append("</ul>");

            builder.Append("<div class='tab-content' id='pills-tabContent' style='margin-top: -9px'>");
            AppendTabContents(cfg, tabs, builder);
            builder.Append("</div>");

            itmReturn.setProperty("inn_total", cfg.day_total_event.ToString());
            itmReturn.setProperty("inn_fought", cfg.day_fought_event.ToString());
            itmReturn.setProperty("inn_wait", cfg.day_wait_event.ToString());
            itmReturn.setProperty("inn_tabs", builder.ToString());
        }

        private Dictionary<int, List<Item>> GetAllRows(TConfig cfg)
        {
            Dictionary<int, List<Item>> map = new Dictionary<int, List<Item>>();

            string sql = @"
                SELECT
	                t1.id		        AS 'program_id'
	                , t1.in_name2
	                , t1.in_team_count
	                , t1.in_site_mat
	                , t1.in_site_mat2
	                , t1.in_fight_time
                    , t1.in_battle_type
	                , t2.id				AS 'event_id'
	                , t2.in_tree_name
	                , t2.in_round
	                , t2.in_round_code
	                , t2.in_tree_id
	                , t2.in_tree_no
	                , t2.in_tree_sno
	                , t2.in_win_time
	                , t2.in_type
	                , t2.in_parent
	                , t3.id				AS 'detail_id'
	                , t3.in_sign_foot
	                , t3.in_sign_no
	                , t4.id				AS 'team_id'
	                , ISNULL(t4.map_short_org, t3.in_player_org) AS 'map_short_org'
	                , ISNULL(t4.in_name, t3.in_player_name) AS 'in_name'
	                , t4.in_weight_message
                    , t11.in_code       AS 'site_code'
                    , t2.in_site_move   AS 'site_move'
                FROM 
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PEVENT t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t3 WITH(NOLOCK)
	                ON t3.source_id = t2.id
                LEFT OUTER JOIN
                    VU_MEETING_PTEAM t4 WITH(NOLOCK)
                    ON t4.source_id = t1.id
                    AND t4.in_sign_no = t3.in_sign_no
                INNER JOIN
                	IN_MEETING_SITE t11 WITH(NOLOCK)
                	ON t11.id = t2.in_site
                WHERE 
	                t2.in_meeting = '{#meeting_id}'
	                AND t2.in_date_key = '{#in_date_key}'
	                AND ISNULL(t2.in_tree_no, 0) > 0
					AND ISNULL(t2.in_win_status, '') NOT IN ('bypass', 'cancel', 'nofight')
					AND ISNULL(t2.in_sub_sect, '') <> N'代表戰'
                    AND ({#in_qry} & t2.in_period) = t2.in_period
                ORDER BY
                    t11.in_code
	                , t2.in_tree_no
	                , t2.id
                    , t3.in_sign_foot
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_date_key}", cfg.in_date)
                .Replace("{#in_qry}", cfg.in_qry);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                int site_code = GetInt(item.getProperty("site_code", "0"));
                int site_move = GetInt(item.getProperty("site_move", "0"));

                List<Item> list = null;
                if (site_move > 0)
                {
                    site_code = site_move;
                }

                if (map.ContainsKey(site_code))
                {
                    list = map[site_code];
                }
                else
                {
                    list = new List<Item>();
                    map.Add(site_code, list);
                }

                list.Add(item);
            }

            return map;
        }

        private List<TRow> GetRows(TConfig cfg, Dictionary<int, List<Item>> map, TTab tab)
        {
            if (!map.ContainsKey(tab.in_code)) return new List<TRow>();

            List<TEvt> evts = MapEvts(cfg, map[tab.in_code]);

            List<TRow> rows = new List<TRow>();

            //總計列
            TRow sum_row = new TRow
            {
                program_id = "",
                program_name = "總計",
                total_event = 0,
                fought_event = 0,
                in_fight_time = "",
                EvtList = new List<TEvt>(),
                WaitingList = new List<TEvt>(),
                FoughtList = new List<TEvt>(),
                is_sum = true,
            };

            //起算時間
            if (cfg.in_time != "")
            {
                string start_time = cfg.in_date + " " + cfg.in_time;
                cfg.lastTime = GetDateTime(start_time);
                if (cfg.lastTime == DateTime.MinValue)
                {
                    cfg.lastTime = DateTime.Now;
                }
            }
            else
            {
                cfg.lastTime = DateTime.Now;
            }

            //場次打上時間與分箱
            foreach (var evt in evts)
            {
                if (evt.Children.Count > 0)
                {
                    foreach (var child in evt.Children)
                    {
                        BindRowAndTime(cfg, sum_row, rows, child);
                    }
                }
                else
                {
                    BindRowAndTime(cfg, sum_row, rows, evt);
                }
            }

            //押上時間
            if (cfg.needUpdateTime)
            {
                //場次打上時間與分箱
                foreach (var evt in evts)
                {
                    if (evt.Children.Count > 0)
                    {
                        foreach (var child in evt.Children)
                        {
                            UpdateEventTime(cfg, child);
                        }
                    }
                    else
                    {
                        UpdateEventTime(cfg, evt);
                    }
                }
            }

            //計算時間與標示各種場次
            foreach (var row in rows)
            {
                Analyze(row);
            }
            Analyze(sum_row);

            //將統計資料更新至頁籤
            tab.wait_event = sum_row.waiting_event;
            tab.fought_event = sum_row.fought_event;
            tab.total_event = sum_row.total_event;

            //將列排序
            List<TRow> result = rows.OrderBy(x => x.in_site_mat2).ToList();
            result.Add(sum_row);
            return result;
        }

        //更新開賽時間至 Event
        private void UpdateEventTime(TConfig cfg, TEvt evt)
        {
            if (evt.has_fought) return;

            string start_time = evt.start_time.ToString("HH:mm");
            string end_time = evt.end_time.ToString("HH:mm");

            string sql = "UPDATE IN_MEETING_PEVENT SET"
                + " in_time_start = '" + start_time + "'"
                + ", in_time_end = '" + end_time + "'"
                + " WHERE id = '" + evt.event_id + "'";

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            cfg.inn.applySQL(sql);
        }

        //計算時間與標示各種場次
        private void Analyze(TRow row)
        {
            row.waiting_event = row.WaitingList.Count;
            row.fought_event = row.FoughtList.Count;
            row.total_event = row.EvtList.Count;

            if (row.WaitingList.Count > 0)
            {
                var first = row.WaitingList.First();
                var last = row.WaitingList.Last();

                row.wait_tree_no = first.in_tree_no;

                row.need_seconds = row.WaitingList.Sum(x => x.total_seconds);
                if (row.need_seconds > 0)
                {
                    var time = MapTime(row.need_seconds);
                    row.start_time = first.start_time;
                    row.end_time = last.end_time;
                    row.display = time.totalMinutes + " (" + time.show + ")";
                }
                else
                {
                    row.display = "";
                }
            }
            else
            {
                row.wait_tree_no = "--";
                row.start_time = DateTime.MinValue;
                row.end_time = DateTime.MinValue;
            }

            if (row.start_time != DateTime.MinValue)
            {
                row.in_start_time = row.start_time.ToString("HH:mm");
            }
            else
            {
                row.in_start_time = "";
            }

            if (row.end_time != DateTime.MinValue)
            {
                row.in_end_time = row.end_time.ToString("HH:mm");
            }
            else
            {
                row.in_end_time = "";
            }

            if (row.FoughtList.Count > 0)
            {
                row.fight_tree_no = row.FoughtList.Last().in_tree_no;
            }
            else
            {
                row.fight_tree_no = "--";
            }
        }

        //場次打上時間與分箱
        private void BindRowAndTime(TConfig cfg, TRow srow, List<TRow> rows, TEvt evt)
        {
            TRow row = rows.Find(x => x.program_id == evt.program_id);

            if (row == null)
            {
                row = new TRow
                {
                    program_id = evt.program_id,
                    program_name = evt.program_name + "(" + evt.program_team_count + ")",
                    in_site_mat = evt.program_site_mat,
                    in_site_mat2 = evt.program_site_mat2,
                    in_fight_time = evt.program_fight_time,
                    fight_seconds = evt.fight_seconds,
                    total_seconds = evt.total_seconds,
                    total_event = 0,
                    fought_event = 0,
                    EvtList = new List<TEvt>(),
                    WaitingList = new List<TEvt>(),
                    FoughtList = new List<TEvt>(),
                };

                rows.Add(row);
            }

            srow.EvtList.Add(evt);
            row.EvtList.Add(evt);

            if (evt.has_fought)
            {
                row.FoughtList.Add(evt);
                srow.FoughtList.Add(evt);
            }
            else
            {
                evt.start_time = cfg.lastTime;
                evt.end_time = cfg.lastTime.AddSeconds(evt.total_seconds);
                cfg.lastTime = evt.end_time;
                row.WaitingList.Add(evt);
                srow.WaitingList.Add(evt);
            }
        }

        private List<TEvt> MapEvts(TConfig cfg, List<Item> items)
        {
            List<TEvt> evts = new List<TEvt>();

            int count = items.Count;

            for (int i = 0; i < count; i = i + 2)
            {
                if (i + 1 >= count) continue; //bug 超過index ，選手數量奇數panda
                Item item = items[i];
                // Item f2 = items[i + 1];

                TEvt evt = new TEvt
                {
                    program_id = item.getProperty("program_id", ""),
                    program_name = item.getProperty("in_name2", "0"),
                    program_team_count = item.getProperty("in_team_count", "0"),
                    program_site_mat = item.getProperty("in_site_mat", ""),
                    program_site_mat2 = item.getProperty("in_site_mat2", ""),
                    program_fight_time = item.getProperty("in_fight_time", "4:00"),
                    program_battle_type = item.getProperty("in_battle_type", ""),

                    event_id = item.getProperty("event_id", ""),

                    in_tree_name = item.getProperty("in_tree_name", ""),
                    in_round = item.getProperty("in_round", "0"),
                    in_round_code = item.getProperty("in_round_code", "0"),

                    in_tree_no = item.getProperty("in_tree_no", ""),
                    in_tree_sno = item.getProperty("in_tree_sno", "0"),
                    in_win_time = item.getProperty("in_win_time", ""),
                    in_type = item.getProperty("in_type", ""),
                    in_parent = item.getProperty("in_parent", ""),
                    site_code = item.getProperty("site_code", "0"),
                    site_move = item.getProperty("site_move", "0"),

                    Foot1 = item,
                    Foot2 = items[i + 1],
                    Children = new List<TEvt>(),
                };

                evt.round = GetInt(evt.in_round);
                evt.round_code = GetInt(evt.in_round_code);
                evt.fight_seconds = GetFightSeconds(evt.program_fight_time);
                evt.total_seconds = cfg.space_seconds + evt.fight_seconds;
                evt.has_fought = evt.in_win_time != "";

                if (evt.in_type == "s")
                {
                    TEvt parent = evts.Find(x => x.event_id == evt.in_parent);
                    if (parent != null)
                    {
                        parent.Children.Add(evt);
                    }
                }
                else
                {
                    evt.inn_desc = GetEventDesc(evt);
                    evts.Add(evt);
                }
            }

            return evts;
        }

        private string GetEventDesc(TEvt evt)
        {
            string result = "";

            if (evt.program_team_count == "2")
            {
                return "三戰兩勝";
            }
            else if (evt.program_battle_type.Contains("Robin"))
            {
                result = evt.program_team_count + "人戰#" + evt.in_tree_sno;
            }
            else
            {
                switch (evt.in_tree_name)
                {
                    case "main":
                        if (evt.round_code == 2)
                        {
                            result = "勝部-決賽";
                        }
                        else if (evt.round_code == 4)
                        {
                            result = "勝部-準決賽";
                        }
                        else
                        {
                            result = "勝部-" + evt.round_code + "強";
                        }
                        break;
                    case "repechage":
                        if (evt.round_code == 2)
                        {
                            result = "敗部-三四名";
                        }
                        else if (evt.round_code == 4)
                        {
                            result = "敗部-銅牌戰";
                        }
                        else
                        {
                            result = "敗部-R" + evt.round;
                        }
                        break;
                    case "challenge-a":
                        if (evt.round_code == 4)
                        {
                            result = "挑戰賽 3 vs 2";
                        }
                        if (evt.round_code == 2)
                        {
                            result = "挑戰賽 2 vs 1";
                        }
                        break;
                    case "challenge-b":
                        result = "挑戰成功加賽";
                        break;
                    case "rank34":
                        result = "三四名";
                        break;
                    case "rank56":
                        result = "五六名";
                        break;
                    case "rank78":
                        result = "七八名";
                        break;
                    case "sub":
                        break;

                    default:
                        break;
                }

            }
            return result;
        }

        private void AppendTabHeads(TConfig cfg, List<TTab> tabs, StringBuilder builder)
        {
            foreach (var tab in tabs)
            {
                if (tab.is_error) continue;

                builder.Append("<li id='" + tab.li_id + "' class='nav-item " + tab.active + "'>");
                builder.Append("<a id='" + tab.a_id + "' href='#" + tab.content_id + "' class='nav-link' data-toggle='pill' role='tab' aria-controls='pills-home' aria-selected='true' onclick='StoreTab_Click(this)'>");
                builder.Append(tab.title + "(<span style='color: red'>" + tab.wait_event + "</span>/" + tab.fought_event + "/" + tab.total_event + ")");
                builder.Append("</a>");
                builder.Append("</li>");
            }
        }

        private void AppendTabContents(TConfig cfg, List<TTab> tabs, StringBuilder builder)
        {
            foreach (var tab in tabs)
            {
                if (tab.is_error) continue;

                builder.Append("<div class='tab-pane fade in " + tab.active + "' id='" + tab.content_id + "' role='tabpanel'>");
                builder.Append("    <div class='showsheet_ clearfix'>");
                builder.Append("        <div id='" + tab.folder_id + "'>");
                builder.Append("            <div class='float-btn clearfix'>");
                builder.Append("            </div>");

                AppendTable(cfg, tab, builder);

                builder.Append("        </div>");

                builder.Append("    </div>");
                builder.Append("</div>");
            }
        }

        private void AppendTable(TConfig cfg, TTab tab, StringBuilder builder)
        {
            StringBuilder head = new StringBuilder();
            head.Append("<thead>");
            head.Append("<tr>");
            head.Append("<th class='text-center'>&nbsp;</th>");
            head.Append("<th class='text-center'>場地</th>");
            head.Append("<th class='text-center'>組別</th>");
            head.Append("<th class='text-center'>總場次</th>");
            head.Append("<th class='text-center'>已完賽</th>");
            head.Append("<th class='text-center'>未完賽</th>");
            head.Append("<th class='text-center'>每場時間</th>");
            head.Append("<th class='text-center'>預估時長</th>");
            head.Append("<th class='text-center'>預估開始</th>");
            head.Append("<th class='text-center'>預估結束</th>");

            head.Append("<th class='text-center'>場上<br>場次</th>");
            head.Append("<th class='text-center'>最新成績<br>場次</th>");

            head.Append("</tr>");
            head.Append("</thead>");

            StringBuilder body = new StringBuilder();
            body.Append("<tbody>");
            for (int i = 0; i < tab.rows.Count; i++)
            {
                var row = tab.rows[i];
                var wait_event = row.total_event - row.fought_event;
                var no_wait = row.is_sum || wait_event <= 0;

                var wait_tr_id = tab.in_code + "_" + row.program_id + "_" + "wait";

                body.Append("<tr>");

                if (no_wait)
                {
                    body.Append("<td class='text-center'> &nbsp; </td>");
                }
                else
                {
                    body.Append("<td class='text-center'> <span class='event-btn' onclick='ToggleWaitRow_Click(this)' data-id='" + wait_tr_id + "' data-state='hide'> &nbsp; <i class='fa fa-plus'></i> &nbsp; </span> </td>");
                }

                body.Append("<td class='text-center'>" + row.in_site_mat + "</td>");
                body.Append("<td class='text-center'>" + ProgramLink(cfg, row) + "</td>");
                body.Append("<td class='text-center'>" + row.total_event + "</td>");
                body.Append("<td class='text-center'>" + row.fought_event + "</td>");
                body.Append("<td class='text-center'><span style='color: red'>" + wait_event + "</span></td>");
                if (row.program_id == "")
                {
                    body.Append("<td class='text-center'>" + row.in_fight_time + "</td>");
                }
                else if (cfg.space_seconds > 0)
                {
                    body.Append("<td class='text-center'>" + row.in_fight_time + "+" + cfg.space_seconds + "(sec.)</td>");
                }
                else
                {
                    body.Append("<td class='text-center'>" + row.in_fight_time + "</td>");
                }

                body.Append("<td class='text-center btn-success'>" + row.display + "</td>");
                body.Append("<td class='text-center'>" + row.in_start_time + "</td>");
                body.Append("<td class='text-center'>" + row.in_end_time + "</td>");

                body.Append("<td class='text-center'><span style='color: blue'>" + row.wait_tree_no + "</span></td>");
                body.Append("<td class='text-center'>" + row.fight_tree_no + "</td>");

                body.Append("</tr>");

                if (!no_wait)
                {
                    body.Append("<tr id='" + wait_tr_id + "' style='display: none'>");
                    body.Append("<td class='text-center' style='vertical-align: middle'> <button class='btn btn-sm btn-danger'"
                        + " data-scode='" + tab.in_code + "'"
                        + " data-pid='" + row.program_id + "'"
                        + " onclick='ChangeSite_Click(this)'> 調場 </td>");
                    body.Append("<td class='text-center' colspan='11'>" + AppendEvents(cfg, tab, row) + "</td>");

                    //body.Append("<td class='text-center'> &nbsp; </td>");
                    //body.Append("<td class='text-center' colspan='10'>" + AppendEvents(cfg, row) + "</td>");
                    //body.Append("<td class='text-center' style='vertical-align: middle'> <button class='btn btn-sm btn-danger'> 調場 </td>");

                    body.Append("</tr>");
                }

            }
            body.Append("</tbody>");

            //builder.Append("<h4 class='box-header with-border'>" + table.title + " (" + table.count + ")</h4>");
            builder.Append("<table id='" + tab.table_id + "' class='table table-bordered table-rwd rwd'>");
            builder.Append(head);
            builder.Append(body);
            builder.Append("</table>");
            //builder.Append("<script>");
            //builder.Append("    $('#" + table.id + "').bootstrapTable({});");
            //builder.Append("    if($(window).width() <= 768) { $('#" + table.id + "').bootstrapTable('toggleView'); }");
            //builder.Append("</script>");
        }

        private string ProgramLink(TConfig cfg, TRow row)
        {
            string url = "c.aspx"
                + "?page=In_Competition_Preview.html"
                + "&method=in_meeting_program_preview"
                + "&meeting_id=" + cfg.meeting_id
                + "&program_id=" + row.program_id;

            return "<a target='_blank' href='" + url + "'>"
                + row.program_name
                + "</a>";
        }

        private StringBuilder AppendEvents(TConfig cfg, TTab tab, TRow row)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("<table class='table table-bordered table-rwd rwd'>");
            builder.Append("<tbody>");

            int no = 1;
            foreach (var evt in row.WaitingList)
            {
                string f1_team_id = evt.Foot1.getProperty("team_id", "");
                string f2_team_id = evt.Foot2.getProperty("team_id", "");

                builder.Append("<tr>");

                //勾選
                builder.Append("<td class='text-center'>"
                    + "<div class='form-check'>  <input class='form-check-input' type='checkbox' value=''"
                    + " name='chk_" + tab.in_code + "_" + evt.program_id + "'"
                    + " data-eid='" + evt.event_id + "'"
                    + " data-tno='" + evt.in_tree_no + "'"
                    + " >  <label class='form-check-label' for='flexCheckDefault'> </label></div>"
                    + "</td>");

                builder.Append("<td class='text-center btn-info'>" + no + "</td>");
                builder.Append("<td class='text-center'>" + MapTreeNo(evt) + "</td>");
                builder.Append("<td class='text-center'>" + evt.inn_desc + "</td>");

                // if (f1_team_id == "")
                // {
                //     builder.Append("<td class='text-center'> &nbsp; </td>");
                // }
                // else
                // {
                //     builder.Append("<td class='text-center'>" + evt.Foot1.getProperty("in_sign_no", "") + "</td>");
                // }

                builder.Append("<td class='text-center'>" + evt.Foot1.getProperty("map_short_org", "") + "</td>");
                builder.Append("<td class='text-center'>" + evt.Foot1.getProperty("in_name", "") + "</td>");
                builder.Append("<td class='text-center'>vs</td>");

                builder.Append("<td class='text-center'>" + evt.Foot2.getProperty("in_name", "") + "</td>");
                builder.Append("<td class='text-center'>" + evt.Foot2.getProperty("map_short_org", "") + "</td>");

                // if (f2_team_id == "")
                // {
                //     builder.Append("<td class='text-center'> &nbsp; </td>");
                // }
                // else
                // {
                //     builder.Append("<td class='text-center'>" + evt.Foot2.getProperty("in_sign_no", "") + "</td>");
                // }

                builder.Append("<td class='text-center'>" + evt.start_time.ToString("HH:mm")
                    + "-"
                    + evt.end_time.ToString("HH:mm") + "</td>");

                builder.Append("<td class='text-center'>"
                    + " <button class='btn btn-sm btn-primary'"
                    + " data-id='" + evt.event_id + "'"
                    + " onclick='SetWinTime_Click(this)'"
                    + ">完賽</button> "
                    + "</td>");


                builder.Append("</tr>");
                no++;
            }
            builder.Append("</table>");
            return builder;
        }

        private string MapTreeNo(TEvt evt)
        {
            if (evt.site_move == "0")
            {
                return evt.in_tree_no;
            }

            switch (evt.site_code)
            {
                case "1": return "A" + evt.in_tree_no;
                case "2": return "B" + evt.in_tree_no;
                case "3": return "C" + evt.in_tree_no;
                case "4": return "D" + evt.in_tree_no;
                case "5": return "E" + evt.in_tree_no;
                case "6": return "F" + evt.in_tree_no;
                case "7": return "G" + evt.in_tree_no;
                case "8": return "H" + evt.in_tree_no;
                case "9": return "I" + evt.in_tree_no;
                case "10": return "J" + evt.in_tree_no;
                case "11": return "K" + evt.in_tree_no;
                case "12": return "L" + evt.in_tree_no;
                default: return "？" + evt.in_tree_no;
            }
        }

        private void FixTab(TConfig cfg, TTab tab)
        {
            tab.li_id = "li_" + tab.id;
            tab.a_id = "a_" + tab.id;
            tab.content_id = "box_" + tab.id;
            tab.folder_id = "folder_" + tab.id;
            tab.toggle_id = "toggle_" + tab.id;
            tab.table_id = "table_" + tab.id;
        }

        //日期選單
        private List<TDay> MapDateMenu(TConfig cfg)
        {
            List<TDay> list = new List<TDay>();
            Item items = GetDateMenu(cfg);
            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_date_key = item.getProperty("in_date_key", "");
                list.Add(new TDay { day = in_date_key, Value = item });
            }
            return list;
        }

        //日期選單
        private Item GetDateMenu(TConfig cfg)
        {
            string sql = @"
                SELECT 
	                DISTINCT t1.in_date_key
                FROM 
	                IN_MEETING_PEVENT t1 WITH(NOLOCK) 
                WHERE 
	                t1.in_meeting = '{#meeting_id}'
	                AND ISNULL(t1.in_date_key, '') <> ''
                ORDER BY 
	                t1.in_date_key
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        //場地選單
        private List<Item> GetSiteMenu(TConfig cfg)
        {
            string sql = @"
                SELECT 
	                t2.id
                    , t2.in_code
                    , t2.in_name
                FROM 
                    IN_MEETING_SITE t2 WITH(NOLOCK)
                WHERE 
	                t2.in_meeting = '{#meeting_id}'
                ORDER BY 
	                t2.in_code
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return MapList(cfg.inn.applySQL(sql));
        }

        //場地選單
        private List<Item> GetSiteMenuOld(TConfig cfg)
        {
            string sql = @"
                SELECT 
	                DISTINCT t2.id
                    , t2.in_code
                    , t2.in_name
                FROM 
	                IN_MEETING_PEVENT t1 WITH(NOLOCK) 
                INNER JOIN
                    IN_MEETING_SITE t2 WITH(NOLOCK)
                    ON t2.id = t1.in_site
                WHERE 
	                t1.in_meeting = '{#meeting_id}'
	                AND ISNULL(t1.in_site, '') <> ''
                ORDER BY 
	                t2.in_code
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return MapList(cfg.inn.applySQL(sql));
        }

        private List<Item> MapList(Item items)
        {
            int count = items.getItemCount();
            List<Item> list = new List<Item>();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                list.Add(item);
            }
            return list;
        }

        private void AddItem(TConfig cfg, List<Item> list, string type, string value, string label)
        {
            Item item = cfg.inn.newItem();
            item.setType(type);
            item.setProperty("value", value);
            item.setProperty("label", label);
            list.Add(item);
        }

        private void AppendItems(
            TConfig cfg
            , List<TDay> list
            , string type_name
            , string val_property
            , string lbl_property
            , Item itmReturn)
        {

            Item itmEmpty = cfg.inn.newItem();
            itmEmpty.setType(type_name);
            itmEmpty.setProperty("label", "比賽日期");
            itmEmpty.setProperty("value", "");
            itmReturn.addRelationship(itmEmpty);

            int count = list.Count;
            for (int i = 0; i < count; i++)
            {
                Item item = list[i].Value;
                item.setType(type_name);
                item.setProperty("value", item.getProperty(val_property, ""));
                item.setProperty("label", item.getProperty(lbl_property, ""));
                itmReturn.addRelationship(item);
            }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }

            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }

            public string meeting_id { get; set; }
            public string in_date { get; set; }
            public string in_time { get; set; }
            public string in_qry { get; set; }
            public string site_id { get; set; }
            public string site_name { get; set; }
            public string evts { get; set; }
            public string scene { get; set; }
            public string default_site_nav { get; set; }

            public Item itmMeeting { get; set; }
            public string in_title { get; set; }
            public string in_battle_type { get; set; }
            public string in_fight_time_incryn { get; set; }

            public int day_total_event { get; set; }
            public int day_fought_event { get; set; }
            public int day_wait_event { get; set; }

            public DateTime lastTime { get; set; }

            public bool needUpdateTime { get; set; }

            public int space_seconds { get; set; }
        }

        private class TDay
        {
            public string day { get; set; }
            public Item Value { get; set; }
        }

        private class TTab
        {
            public string id { get; set; }
            public string active { get; set; }
            public string title { get; set; }
            public string groups { get; set; }

            public string li_id { get; set; }
            public string a_id { get; set; }
            public string content_id { get; set; }
            public string folder_id { get; set; }
            public string toggle_id { get; set; }
            public string table_id { get; set; }
            public string in_site { get; set; }
            public int in_code { get; set; }
            public bool is_error { get; set; }

            public int total_event { get; set; }
            public int fought_event { get; set; }
            public int wait_event { get; set; }

            public List<TRow> rows { get; set; }
        }

        private class TRow
        {
            public string program_id { get; set; }
            public string program_name { get; set; }
            public string in_site_mat { get; set; }
            public string in_site_mat2 { get; set; }
            public string in_fight_time { get; set; }

            public int waiting_event { get; set; }
            public int fought_event { get; set; }
            public int total_event { get; set; }

            public string wait_tree_no { get; set; }
            public string fight_tree_no { get; set; }

            public int fight_seconds { get; set; }
            public int total_seconds { get; set; }

            public int need_seconds { get; set; }

            public DateTime start_time { get; set; }
            public DateTime end_time { get; set; }

            public string in_start_time { get; set; }
            public string in_end_time { get; set; }

            public List<TEvt> EvtList { get; set; }
            public List<TEvt> WaitingList { get; set; }
            public List<TEvt> FoughtList { get; set; }

            public bool is_sum { get; set; }
            public string display { get; set; }
        }

        private class TEvt
        {
            public string program_id { get; set; }
            public string program_name { get; set; }
            public string program_team_count { get; set; }
            public string program_site_mat { get; set; }
            public string program_site_mat2 { get; set; }
            public string program_fight_time { get; set; }
            public string program_battle_type { get; set; }

            public string event_id { get; set; }

            public string in_tree_name { get; set; }
            public string in_round { get; set; }
            public string in_round_code { get; set; }
            public string in_tree_sno { get; set; }
            public int round { get; set; }
            public int round_code { get; set; }

            public string in_tree_id { get; set; }
            public string in_tree_no { get; set; }
            public string in_win_time { get; set; }
            public string in_type { get; set; }
            public string in_parent { get; set; }

            public string site_code { get; set; }
            public string site_move { get; set; }

            public bool has_fought { get; set; }
            public List<TEvt> Children { get; set; }

            public int fight_seconds { get; set; }
            public int total_seconds { get; set; }

            public DateTime start_time { get; set; }
            public DateTime end_time { get; set; }

            public Item Foot1 { get; set; }
            public Item Foot2 { get; set; }

            public string inn_desc { get; set; }
        }

        //轉換日期
        private DateTime GetDateTime(string value)
        {
            if (value == "") return DateTime.MinValue;
            DateTime dt = DateTime.MinValue;
            DateTime.TryParse(value, out dt);
            return dt;
        }

        //轉換日期
        private string GetDateTimeValue(string value, string format)
        {
            if (value == "") return "";
            DateTime dt = DateTime.MinValue;
            DateTime.TryParse(value, out dt);
            return dt.ToString(format);
        }

        private int GetInt(string value)
        {
            if (value == "" || value == "0") return 0;
            int result = 0;
            int.TryParse(value, out result);
            return result;
        }

        private TTime MapTime(int seconds)
        {
            var result = new TTime();

            var minutes = (seconds - (seconds % 60)) / 60;
            result.totalSeconds = seconds;
            result.totalMinutes = minutes;

            result.s = seconds % 60;
            result.m = minutes % 60;
            result.h = (minutes - result.m) / 60;

            result.hh = result.h.ToString().PadLeft(2, '0');
            result.mm = result.m.ToString().PadLeft(2, '0');
            result.ss = result.s.ToString().PadLeft(2, '0');

            result.show = result.hh + ":" + result.mm + ":" + result.ss;

            return result;
        }

        private int GetFightSeconds(string in_fight_time)
        {
            var df = 4 * 60;
            if (in_fight_time == "") return df;
            var arr = in_fight_time.Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
            if (arr.Length != 2) return df;
            var m = GetInt(arr[0]);
            var s = GetInt(arr[1]);
            return m * 60 + s;
        }

        private class TTime
        {
            public int h { get; set; }
            public int m { get; set; }
            public int s { get; set; }
            public int totalSeconds { get; set; }
            public int totalMinutes { get; set; }
            public string hh { get; set; }
            public string mm { get; set; }
            public string ss { get; set; }
            public string show { get; set; }
        }
    }
}