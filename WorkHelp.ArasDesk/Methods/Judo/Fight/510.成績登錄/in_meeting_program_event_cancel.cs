﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Judo.Fight
{
    public class in_meeting_program_event_cancel : Item
    {
        public in_meeting_program_event_cancel(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 提供正取的報名清單
                日誌: 
                    - 2022-02-10: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]" + "AAA_Method";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                strIdentityId = inn.getUserAliases(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                scene = itmR.getProperty("scene", ""),
            };

            cfg.meeting_id = "3C26E884604A457A9CFFB30450EBF7CF";
            cfg.in_fight_day = "2022-11-13";

            Run(cfg, itmR);

            return itmR;
        }

        private void Run(TConfig cfg, Item itmReturn)
        {
            var items = GetCancelEventItems(cfg, itmReturn);
            var count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                string site_name = item.getProperty("site_name", "");
                string in_tree_no = item.getProperty("in_tree_no", "");
                string in_next_win = item.getProperty("in_next_win", "");
                string in_next_foot_win = item.getProperty("in_next_foot_win", "");
                string in_next_lose = item.getProperty("in_next_lose", "");
                string in_next_foot_lose = item.getProperty("in_next_foot_lose", "");

                UpdateNoPlayer(cfg, in_next_win, in_next_foot_win);
                UpdateNoPlayer(cfg, in_next_lose, in_next_foot_lose);
            }
        }

        private void UpdateNoPlayer(TConfig cfg, string evt_id, string foot)
        {
            if (evt_id == "" || foot == "") return;

            string sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET"
                + " in_player_name = N'消失'"
                + " WHERE source_id = '"+ evt_id + "'"
                + " AND in_sign_foot = '" + foot + "'"
                ;

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);
            
            cfg.inn.applySQL(sql);
        }

        private Item GetCancelEventItems(TConfig cfg, Item itmReturn)
        {
            string sql = @"
                SELECT 
	                t1.id
	                , t1.in_tree_id
	                , t1.in_tree_no
	                , t1.in_next_win
	                , t1.in_next_foot_win
	                , t1.in_next_lose
	                , t1.in_next_foot_lose
	                , t2.in_code AS 'site_code'
	                , t2.in_name AS 'site_name'
                FROM 
	                IN_MEETING_PEVENT t1 WITH(NOLOCK) 
                WHERE 
	                t1.in_meeting = '{#meeting_id}'
	                AND t1.in_date_key = '{#in_fight_day}'
	                AND ISNULL(t1.in_tree_no, 0) > 0
	                AND t1.in_win_status = 'cancel'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_fight_day}", cfg.in_fight_day);

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }

            public string meeting_id { get; set; }
            public string in_fight_day { get; set; }
            public string scene { get; set; }
        }
    }
}