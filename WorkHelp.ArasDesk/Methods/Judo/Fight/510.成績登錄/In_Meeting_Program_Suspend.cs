﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Judo.Common
{
    public class In_Meeting_Program_Suspend : Item
    {
        public In_Meeting_Program_Suspend(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 犯規輸與不戰勝
                日期: 
                    - 2025-02-1: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]In_Meeting_Program_Suspend";

            var itmR = this;

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                detail_id = itmR.getProperty("detail_id", ""),
                in_type = itmR.getProperty("in_type", ""),
                in_suspend = itmR.getProperty("in_suspend", ""),
                in_fettle = itmR.getProperty("in_fettle", ""),
                scene = itmR.getProperty("scene", ""),
            };

            if (cfg.in_type == "clear")
            {
                cfg.is_rollback = true;
            }

            SetData(cfg);

            switch (cfg.scene)
            {
                case "ippon-edit"://一勝設定
                    IpponEdit(cfg, itmR);
                    break;

                case "wazaari-edit"://半勝設定
                    WazaariEdit(cfg, itmR);
                    break;

                case "yuko-edit"://有效設定
                    YukoEdit(cfg, itmR);
                    break;

                case "yuko-increase"://有效+1
                    YukoIncrease(cfg, itmR);
                    break;

                case "shido-edit"://指導設定
                    ShidoEdit(cfg, itmR);
                    break;

                case "suspend-edit"://停賽註記設定
                    SuspendEdit(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void SuspendEdit(TConfig cfg, Item itmReturn)
        {
            //在隊伍上註記
            if (cfg.team_id != "")
            {
                var sql_upd1 = "UPDATE IN_MEETING_PTEAM SET"
                    + "  in_event_message = '" + cfg.in_suspend + "'"
                    + ", in_fight_id = '" + cfg.in_fight_id + "'"
                    + " WHERE id = '" + cfg.team_id + "'"
                    ;
                cfg.inn.applySQL(sql_upd1);
            }

            //場次停賽
            if (cfg.event_id != "")
            {
                var sql_upd3 = "UPDATE IN_MEETING_PEVENT SET"
                    + " in_suspend = '" + cfg.in_suspend + "'"
                    + " WHERE id = '" + cfg.event_id + "'"
                    ;
                cfg.inn.applySQL(sql_upd3);
            }

            //明細註記停賽
            if (cfg.detail_id != "")
            {
                var sql_upd3 = "UPDATE IN_MEETING_PEVENT_DETAIL SET"
                    + " in_suspend = '" + cfg.in_suspend + "'"
                    + " WHERE id = '" + cfg.detail_id + "'"
                    ;
                cfg.inn.applySQL(sql_upd3);
            }
        }

        private void ShidoEdit(TConfig cfg, Item itmReturn)
        {
            var in_shido = itmReturn.getProperty("value", "");

            if (cfg.detail_id != "")
            {
                var sql_upd3 = "UPDATE IN_MEETING_PEVENT_DETAIL SET"
                    + " in_correct_count = '" + in_shido + "'"
                    + " WHERE id = '" + cfg.detail_id + "'"
                    ;
                cfg.inn.applySQL(sql_upd3);
            }

            // if (in_shido == "3")
            // {
            //     var itmData = cfg.inn.newItem("In_Meeting_PEvent_Detail");
            //     itmData.setProperty("meeting_id", cfg.meeting_id);
            //     itmData.setProperty("program_id", cfg.program_id);
            //     itmData.setProperty("event_id", cfg.event_id);
            //     itmData.setProperty("detail_id", cfg.detail_id);
            //     itmData.setProperty("points_type", "0");
            //     itmData.setProperty("scene", "score"); 
            //     itmData.setProperty("mode", "score");
            //     itmData.apply("in_meeting_program_score_new");
            // }
        }

        private void YukoIncrease(TConfig cfg, Item itmReturn)
        {
            if (cfg.detail_id == "") throw new Exception("明細 id 錯誤");

            var sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET"
                + " in_yuko = ISNULL(in_yuko, 0) + 1"
                + " WHERE id = '" + cfg.detail_id + "'"
                ;

            cfg.inn.applySQL(sql);
        }

        private void YukoEdit(TConfig cfg, Item itmReturn)
        {
            if (cfg.detail_id == "") throw new Exception("明細 id 錯誤");

            var in_yuko = itmReturn.getProperty("value", "");

            var sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET"
                + " in_yuko = '" + in_yuko + "'"
                + " WHERE id = '" + cfg.detail_id + "'"
                ;

            cfg.inn.applySQL(sql);
        }

        private void IpponEdit(TConfig cfg, Item itmReturn)
        {
            var in_ippon = itmReturn.getProperty("value", "");
            var in_wazaari = cfg.itmDetail.getProperty("in_score2", "");
            var wIsZero = in_wazaari == "" || in_wazaari == "0";

            var in_points = "";
            if (in_ippon == "1")
            {
                in_points = wIsZero ? "10" : "11";
            }
            else
            {
                in_points = in_wazaari;
            }

            if (cfg.detail_id != "")
            {
                var sql_upd3 = "UPDATE IN_MEETING_PEVENT_DETAIL SET"
                    + "  in_score1 = '" + in_ippon + "'"
                    + ", in_points = '" + in_points + "'"
                    + " WHERE id = '" + cfg.detail_id + "'"
                    ;

                cfg.inn.applySQL(sql_upd3);
            }
        }

        private void WazaariEdit(TConfig cfg, Item itmReturn)
        {
            var in_ippon = cfg.itmDetail.getProperty("in_score1", "");
            var in_wazaari = itmReturn.getProperty("value", "");
            var wIsZero = in_wazaari == "" || in_wazaari == "0";

            var in_points = "";
            if (in_ippon == "1")
            {
                in_points = wIsZero ? "10" : "11";
            }
            else
            {
                in_points = in_wazaari;
            }

            if (cfg.detail_id != "")
            {
                var sql_upd3 = "UPDATE IN_MEETING_PEVENT_DETAIL SET"
                    + "  in_score2 = '" + in_wazaari + "'"
                    + ", in_points = ISNULL(in_score1, '') + '" + in_wazaari + "'"
                    + " WHERE id = '" + cfg.detail_id + "'"
                    ;
                cfg.inn.applySQL(sql_upd3);
            }
        }

        private void SetData(TConfig cfg)
        {
            var sql = "SELECT * FROM IN_MEETING_PEVENT_DETAIL WITH(NOLOCK) WHERE id = '" + cfg.detail_id + "'";
            cfg.itmDetail = cfg.inn.applySQL(sql);
            if (cfg.itmDetail.getResult() == "") throw new Exception("查無場次明細資料");

            cfg.event_id = cfg.itmDetail.getProperty("source_id", "");
            cfg.in_sign_no = cfg.itmDetail.getProperty("in_sign_no", "");

            sql = "SELECT * FROM IN_MEETING_PEVENT WITH(NOLOCK) WHERE id = '" + cfg.event_id + "'";
            cfg.itmEvent = cfg.inn.applySQL(sql);
            if (cfg.itmEvent.getResult() == "") throw new Exception("查無場次資料");

            cfg.meeting_id = cfg.itmEvent.getProperty("in_meeting", "");
            cfg.program_id = cfg.itmEvent.getProperty("source_id", "");
            cfg.in_fight_id = cfg.itmEvent.getProperty("in_fight_id", "");
            cfg.is_sub = cfg.itmEvent.getProperty("in_type", "") == "s";
            cfg.is_parent = cfg.itmEvent.getProperty("in_type", "") == "p";

            sql = "SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + cfg.program_id + "'";
            cfg.itmProgram = cfg.inn.applySQL(sql);
            if (cfg.itmProgram.getResult() == "") throw new Exception("查無組別資料");

            sql = "SELECT TOP 1 * FROM IN_MEETING_PTEAM WITH(NOLOCK) WHERE source_id = '" + cfg.program_id + "' AND in_sign_no = '" + cfg.in_sign_no + "'";
            cfg.itmTeam = cfg.inn.applySQL(sql);
            if (cfg.itmTeam.getResult() == "") cfg.itmTeam = cfg.inn.newItem();
            cfg.team_id = cfg.itmTeam.getProperty("id", "");
        }

        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }

            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }

            public string detail_id { get; set; }
            public string in_type { get; set; }
            public string in_suspend { get; set; }
            public string in_fettle { get; set; }
            public string scene { get; set; }

            public string meeting_id { get; set; }
            public string program_id { get; set; }
            public string event_id { get; set; }
            public string team_id { get; set; }
            public string in_sign_no { get; set; }
            public string in_fight_id { get; set; }

            public string event_type { get; set; }
            public bool is_sub { get; set; }
            public bool is_parent { get; set; }
            public bool is_rollback { get; set; }

            public Item itmProgram { get; set; }
            public Item itmEvent { get; set; }
            public Item itmDetail { get; set; }
            public Item itmTeam { get; set; }
        }
    }
}