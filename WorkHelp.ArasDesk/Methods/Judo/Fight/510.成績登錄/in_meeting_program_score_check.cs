﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Judo.Fight
{
    public class in_meeting_program_score_check : Item
    {
        public in_meeting_program_score_check(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 檢查成績
                日誌: 
                    - 2023-04-14: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "in_meeting_program_score_check";

            var itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,

                meeting_id = itmR.getProperty("meeting_id", ""),
                program_id = itmR.getProperty("program_id", ""),
                scene = itmR.getProperty("scene", ""),

                in_name = "",
                save_log = true,
            };

            var muid = itmR.getProperty("muid", "");
            if (cfg.program_id == "")
            {
                cfg.program_id = GetProgramId(cfg, itmR);
            }

            switch (cfg.scene)
            {
                case "exists_players"://檢查該場次是否仍有選手
                    if (muid != "")
                    {
                        CheckEventExistsPlayers(cfg, muid);
                    }
                    break;
            }

            return itmR;
        }

        //檢查該場次是否仍有選手
        private void CheckEventExistsPlayers(TConfig cfg, string muid)
        {
            var itmMUser = GetMUserItem(cfg, muid);
            if (itmMUser.isError() || itmMUser.getResult() == "") return;

            var in_sign_no = itmMUser.getProperty("in_sign_no", "");
            if (in_sign_no == "" || in_sign_no == "0") return;

            var itmEvent = GetEventItem(cfg, in_sign_no, "1");
            CheckFromEvent(cfg, itmEvent);
        }

        private void CheckFromEvent(TConfig cfg, string event_id)
        {
            if (event_id == "") return;
            var itmEvent = GetEventItemFromId(cfg, event_id);
            CheckFromEvent(cfg, itmEvent);
        }

        //遞迴
        private void CheckFromEvent(TConfig cfg, Item itmEvent)
        {
            if (itmEvent.isError() || itmEvent.getResult() == "") return;


            var event_id = itmEvent.getProperty("id", "");
            var fight_id = itmEvent.getProperty("in_fight_id", "") + "(" + itmEvent.getProperty("in_tree_no", "") + ")";
            var win_status = itmEvent.getProperty("in_win_status", "");
            var next_eid = itmEvent.getProperty("in_next_win", "");

            var itmDetailA = GetDetailItem(cfg, event_id, "1");
            var itmDetailB = GetDetailItem(cfg, event_id, "2");

            var a_sign_no = itmDetailA.getProperty("in_sign_no", "");
            var b_sign_no = itmDetailB.getProperty("in_sign_no", "");

            var a_name = itmDetailA.getProperty("in_player_name", "");
            var b_name = itmDetailB.getProperty("in_player_name", "");

            var a_did = itmDetailA.getProperty("id", "");
            var b_did = itmDetailB.getProperty("id", "");

            if (win_status == "bypass")
            {
                cfg.WgtLog(fight_id + " _# bypass");
                CheckFromEvent(cfg, next_eid);
            }
            else
            {
                if (a_name == "消失" && b_name == "消失")
                {
                    cfg.WgtLog(fight_id + " _# 雙方消失");
                }
                else if (a_name == "消失")
                {
                    cfg.WgtLog(fight_id + " _# 此方消失");
                    RunScore(cfg, event_id, b_did, "HWIN");
                }
                else if (b_name == "消失")
                {
                    cfg.WgtLog(fight_id + " _# 對手消失");
                    RunScore(cfg, event_id, a_did, "HWIN");
                }
                else
                {
                    var itmPTeamA = GetMTeamItem(cfg, a_sign_no);
                    var itmPTeamB = GetMTeamItem(cfg, b_sign_no);
                    CheckTeams(cfg, itmEvent, itmPTeamA, itmPTeamB);
                }

                CheckFromEvent(cfg, next_eid);
            }
        }

        private void CheckTeams(TConfig cfg, Item itmEvent, Item itmPTeamA, Item itmPTeamB)
        {
            var event_id = itmEvent.getProperty("id", "");
            var fight_id = itmEvent.getProperty("in_fight_id", "") + "(" + itmEvent.getProperty("in_tree_no", "") + ")";

            if (itmPTeamA.isError() || itmPTeamA.getResult() == "")
            {
                cfg.WgtLog(fight_id + " _# A 無隊伍資料");
                return;
            }

            if (itmPTeamB.isError() || itmPTeamB.getResult() == "")
            {
                cfg.WgtLog(fight_id + " _# B 無隊伍資料");
                return;
            }

            var a_weight_message = itmPTeamA.getProperty("in_weight_message", "");
            var b_weight_message = itmPTeamB.getProperty("in_weight_message", "");

            var next_event_id = itmEvent.getProperty("in_next_win", "");
            var next_event_foot = itmEvent.getProperty("in_next_foot_win", "");

            var target_foot = next_event_foot == "1" ? "2" : "1";
            var itmNextDetail = GetDetailItem(cfg, next_event_id, target_foot);
            var next_detail_id = itmNextDetail.getProperty("id", "");

            if (a_weight_message != "" && b_weight_message != "")
            {
                cfg.WgtLog(fight_id + " _# 該場取消，下一場對手自動勝出");

                //該場取消
                var itmData = cfg.inn.newItem("In_Meeting_PEvent_Detail");
                itmData.setProperty("meeting_id", cfg.meeting_id);
                itmData.setProperty("program_id", cfg.program_id);
                itmData.setProperty("event_id", event_id);
                itmData.setProperty("scene", "cancel");
                itmData.apply("in_meeting_pevent");

                // //下一場對手自動勝出
                // RunScore(cfg, next_event_id, next_detail_id, "HWIN");
            }
            else
            {
                cfg.WgtLog(fight_id + " _# 該場復原，下一場狀態復歸");

                //該場不取消，執行復原
                var itmData = cfg.inn.newItem("In_Meeting_PEvent_Detail");
                itmData.setProperty("meeting_id", cfg.meeting_id);
                itmData.setProperty("program_id", cfg.program_id);
                itmData.setProperty("event_id", event_id);
                itmData.setProperty("scene", "rollback");
                itmData.apply("in_meeting_pevent");

                //下一場歸零
                RunScore(cfg, next_event_id, next_detail_id, "Initial");
            }
        }

        private void RunScore(TConfig cfg, string event_id, string detail_id, string points_type)
        {
            var itmData = cfg.inn.newItem("In_Meeting_PEvent_Detail");
            itmData.setProperty("meeting_id", cfg.meeting_id);
            itmData.setProperty("program_id", cfg.program_id);
            itmData.setProperty("event_id", event_id);
            itmData.setProperty("detail_id", detail_id);
            itmData.setProperty("points_type", points_type);
            itmData.setProperty("mode", "status");
            itmData.apply("in_meeting_program_score_new");
        }

        //拿場次明細資料
        private Item GetDetailItem(TConfig cfg, string eid, string foot)
        {
            var sql = @"
                SELECT
	                *
                FROM
	                IN_MEETING_PEVENT_DETAIL WITH(NOLOCK)
                WHERE
	                source_id = '{#eid}'
	                AND in_sign_foot = '{#foot}'
            ";

            sql = sql.Replace("{#eid}", eid)
                .Replace("{#foot}", foot);

            return cfg.inn.applySQL(sql);
        }

        //拿場次資料
        private Item GetEventItemFromId(TConfig cfg, string id)
        {
            var sql = @"
                SELECT 
                    *
                FROM 
	                IN_MEETING_PEVENT WITH(NOLOCK)
                WHERE
	                id = '{#event_id}'
            ";

            sql = sql.Replace("{#event_id}", id);

            return cfg.inn.applySQL(sql);
        }

        //拿場次資料
        private Item GetEventItem(TConfig cfg, string in_sign_no, string in_round)
        {
            var sql = @"
                SELECT TOP 1
	                t1.*
                FROM 
	                IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                WHERE
	                t1.source_id = '{#program_id}'
	                AND t1.in_round = {#in_round}
	                AND t2.in_sign_no = '{#in_sign_no}'
	                AND ISNULL(t1.in_type, '') IN ('', 'p')
            ";

            sql = sql.Replace("{#program_id}", cfg.program_id)
                .Replace("{#in_round}", in_round)
                .Replace("{#in_sign_no}", in_sign_no);

            return cfg.inn.applySQL(sql);
        }

        //拿與會者資料
        private Item GetMUserItem(TConfig cfg, string muid)
        {
            var sql = @"
                SELECT 
	                id
	                , in_creator_sno
	                , in_index
	                , in_sign_no
                FROM 
	                IN_MEETING_USER WITH(NOLOCK) 
                WHERE id = '{#muid}'
            ";

            sql = sql.Replace("{#muid}", muid);

            return cfg.inn.applySQL(sql);
        }

        //拿選手資料
        private Item GetMTeamItem(TConfig cfg, string in_sign_no)
        {
            var sql = @"
                    SELECT 
	                    t1.in_name
	                    , t1.in_current_org
	                    , t1.in_sign_no
	                    , t1.in_weight_message
                    FROM 
	                    IN_MEETING_PTEAM t1 WITH(NOLOCK)
                    WHERE
	                    t1.source_id = '{#program_id}'
	                    AND t1.in_sign_no = '{#in_sign_no}'
	                    AND ISNULL(t1.in_type, '') IN ('', 'p')
                ";
            sql = sql.Replace("{#program_id}", cfg.program_id)
                .Replace("{#in_sign_no}", in_sign_no);

            return cfg.inn.applySQL(sql);
        }

        private string GetProgramId(TConfig cfg, Item itmReturn)
        {
            var in_l1 = itmReturn.getProperty("in_l1", "");
            var in_l2 = itmReturn.getProperty("in_l2", "");
            var in_l3 = itmReturn.getProperty("in_l3", "");

            var sql = @"
                SELECT 
                    id
                FROM 
                    IN_MEETING_PROGRAM WITH(NOLOCK)
                WHERE
                    in_meeting = '{#meeting_id}'
                    AND in_l1 = N'{#in_l1}'
                    AND in_l2 = N'{#in_l2}'
                    AND in_l3 = N'{#in_l3}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_l1}", in_l1)
                .Replace("{#in_l2}", in_l2)
                .Replace("{#in_l3}", in_l3);

            var itmResult = cfg.inn.applySQL(sql);

            return itmResult.getProperty("id", "");
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }

            public string meeting_id { get; set; }
            public string program_id { get; set; }
            public string scene { get; set; }

            public string in_name { get; set; }
            public bool save_log { get; set; }

            public void WgtLog(string msg)
            {
                if (save_log)
                {
                    CCO.Utilities.WriteDebug("[Judo]Analysis.ScoreLink", msg + " - " + in_name);
                }
            }
        }
    }
}