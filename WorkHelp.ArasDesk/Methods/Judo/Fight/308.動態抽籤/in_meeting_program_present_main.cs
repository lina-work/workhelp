﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Judo.Fight
{
    public class in_meeting_program_present_main : Item
    {
        public in_meeting_program_present_main(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 單淘預覽 - 動態抽籤
                日誌: 
                    - 2022-03-28: 修正 line bug (lina)
                    - 2020-11-24: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]in_meeting_program_present_main";

            Item itmR = inn.newItem();
            //CCO.Utilities.WriteDebug(strMethodName, this.dom.InnerXml);

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,

                meeting_id = this.getProperty("meeting_id", ""),
                program_id = this.getProperty("program_id", ""),
                mode = this.getProperty("mode", ""),
                eno = this.getProperty("eno", ""),
                in_sign_time = this.getProperty("in_sign_time", ""),
                serial = this.getProperty("serial", ""),
                auto_next = this.getProperty("auto_next", ""),
                page_language = this.getProperty("page_language", ""),

                itmProgram = this,
            };

            cfg.showSignNo = cfg.mode == "draw";
            cfg.showOrgCount = cfg.mode == "draw" && cfg.in_sign_time != "";
            cfg.isEdit = cfg.eno == "edit";
            cfg.canNAEdit = cfg.eno == "recover";

            //附加比賽選手
            //Item itmEventPlayers = null;
            //if (cfg.serial == "" || cfg.serial == "0")
            //{
            //    itmEventPlayers = GetEventPlayersAll(cfg);
            //}
            //else
            //{
            //    itmEventPlayers = GetEventPlayers(cfg);
            //}

            var itmEventPlayers = new List<Item>();
            var itmAllPlayers = GetEventPlayersAll(cfg);


            if (cfg.serial == "" || cfg.serial == "0")
            {
                AppendList1(itmEventPlayers, itmAllPlayers);
            }
            else
            {
                var judo_ns = GetJudoNoList(cfg);
                cfg.end_judo_no = judo_ns.Last();
                AppendList2(itmEventPlayers, itmAllPlayers, judo_ns);
            }

            AppendEvents(cfg, itmEventPlayers, itmR);

            itmR.setProperty("serial_text", "<input id='methodSerial' type='hidden' value='" + cfg.serial + "' />");

            AppendPlayerModal(cfg, itmR);

            return itmR;
        }

        private List<int> GetJudoNoList(TConfig cfg)
        {
            var sql = "SELECT in_team_count from IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '{#program_id}'";
            sql = sql.Replace("{#program_id}", cfg.program_id);
            var itmProgram = cfg.inn.applySQL(sql);
            var in_team_count = GetIntVal(itmProgram.getProperty("in_team_count", "0"));

            var value = GetIntVal(cfg.serial);
            var svc = new InnSport.Core.Services.Impl.JudoSportService();
            var judo_ns = svc.NoListByOrder(in_team_count);
            return judo_ns.Take(value).ToList();
        }

        private void AppendList1(List<Item> list, Item items)
        {
            var count = items.getItemCount();
            for (var i = 0; i < count; i ++)
            {
                var item = items.getItemByIndex(i);
                list.Add(item);
            }
        }

        private void AppendList2(List<Item> list, Item items, List<int> judo_ns)
        {

            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var in_judo_no = GetIntVal(item.getProperty("in_judo_no", "0"));
                if (judo_ns.Contains(in_judo_no))
                {
                    list.Add(item);
                }
            }
        }

        private void AppendPlayerModal(TConfig cfg, Item itmReturn)
        {
            if (cfg.serial == "" || cfg.serial == "0") return;

            Item itmTeam = GetPlayerItems(cfg);
            if (itmTeam.isError()) return;
            if (itmTeam.getResult() == "") return;

            var page = GetPageInfo(cfg, itmTeam, itmReturn);
            if (page.is_english)
            {
                ResetModalEN(cfg, page, itmTeam, itmReturn);
            }
            else
            {
                ResetModalTW(cfg, page, itmTeam, itmReturn);
            }

            //籤號定錨
            string player_anchor = "#ssn_lbl_" + itmTeam.getProperty("in_section_no", "");

            itmReturn.setProperty("player_anchor", player_anchor);
            itmReturn.setProperty("player_modal", page.modal_contents);
        }

        private void ResetModalTW(TConfig cfg, TPage page, Item itmTeam, Item itmReturn)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("<div class='modal fade' id='playerModal' tabindex='-1' role='dialog' aria-labelledby='exampleModalLabel' aria-hidden='true' data-backdrop='false'>");
            builder.Append("  <div class='modal-dialog modal-dialog-centered modal_competition_container' role='document'>");
            builder.Append("    <div class='modal-content'>");

            builder.Append("      <div class='modal-header'>");
            builder.Append("        <h5 class='modal-title' id='playerModalLabel'> " + page.program_name2 + page.page_no + "</h5>");
            builder.Append("      </div>");

            //第二版
            AppendPlayerModalBody_02(cfg, itmTeam, builder);

            builder.Append("      <div class='modal-footer'>");

            if (page.is_end)
            {
                if (page.run_auto)
                {
                    builder.Append("        <label id='itvl_lbl' style='margin-right: 20px'></label>");
                    builder.Append("        <button type='button' class='btn btn-warning' onclick='EndTimer_Click1()'>抽籤結束</button>");
                }
                else
                {
                    builder.Append("        <button type='button' class='btn btn-warning' onclick='EndTimer_Click2()'>抽籤結束</button>");
                }
            }
            else
            {
                if (page.run_auto)
                {
                    builder.Append("        <label id='itvl_lbl' style='margin-right: 20px'></label>");
                    builder.Append("        <button type='button' class='btn btn-secondary' onclick='CloseModal()'>取消</button>");
                }
                else
                {
                    builder.Append("        <label id='itvl_lbl' style='margin-right: 20px'></label>");
                    builder.Append("        <button type='button' class='btn btn-primary' onclick='NextPlayer_Click()'>下一位</button>");
                    builder.Append("        <button type='button' class='btn btn-secondary' onclick='CloseModal()'>取消</button>");
                }
            }

            builder.Append("      </div>");
            builder.Append("    </div>");
            builder.Append("  </div>");
            builder.Append("</div>");


            builder.Append("<script>");
            builder.AppendLine("var modal_interval = null;");

            //自動倒數
            if (page.is_auto)
            {
                builder.Append("    var try_count = 1; ");
                builder.Append("    $('#itvl_lbl').html('倒數 ' + try_count + ' 秒');");

                builder.Append("    modal_interval = setInterval(function() {"
                    + " try_count = try_count - 1;"
                    + " $('#itvl_lbl').html('倒數 ' + try_count + ' 秒');"
                    + " if (try_count <= 0) {"
                    + "     window.clearInterval(modal_interval);"
                    + "     NextPlayer_Click(); } "
                    + " }, 1000); ");

            }
            else if (page.is_end && page.run_auto)
            {
                builder.Append("    var try_count = 1; ");
                builder.Append("    $('#itvl_lbl').html('倒數 ' + try_count + ' 秒');");

                builder.Append("    modal_interval = setInterval(function() {"
                    + " try_count = try_count - 1;"
                    + " $('#itvl_lbl').html('倒數 ' + try_count + ' 秒');"
                    + " if (try_count <= 0) {"
                    + "     window.clearInterval(modal_interval);"
                    + "     modal_interval = null;"
                    + "     EndTimer_Click1();"
                    + "   }"
                    + " }, 1000); ");
            }

            //取消
            builder.Append("    function CloseModal() { if (modal_interval != null && modal_interval != undefined) { window.clearInterval(modal_interval); } $('#playerModal').modal('hide'); }");

            builder.Append("</script>");

            page.modal_contents = builder.ToString();
        }

        private void ResetModalEN(TConfig cfg, TPage page, Item itmTeam, Item itmReturn)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("<div class='modal fade' id='playerModal' tabindex='-1' role='dialog' aria-labelledby='exampleModalLabel' aria-hidden='true' data-backdrop='false'>");
            builder.Append("  <div class='modal-dialog modal-dialog-centered modal_competition_container' role='document'>");
            builder.Append("    <div class='modal-content'>");

            builder.Append("      <div class='modal-header'>");
            builder.Append("        <h5 class='modal-title' id='playerModalLabel'> " + page.program_name2 + page.page_no + "</h5>");
            builder.Append("      </div>");

            //第三版
            AppendPlayerModalBody_03(cfg, itmTeam, builder);

            builder.Append("      <div class='modal-footer'>");

            if (page.is_end)
            {
                if (page.run_auto)
                {
                    builder.Append("        <label id='itvl_lbl' style='margin-right: 20px'></label>");
                    builder.Append("        <button type='button' class='btn btn-warning' onclick='EndTimer_Click1()'>Finished</button>");
                }
                else
                {
                    builder.Append("        <button type='button' class='btn btn-warning' onclick='EndTimer_Click2()'>Finished</button>");
                }
            }
            else
            {
                if (page.run_auto)
                {
                    builder.Append("        <label id='itvl_lbl' style='margin-right: 20px'></label>");
                    builder.Append("        <button type='button' class='btn btn-secondary' onclick='CloseModal()'>Cancel</button>");
                }
                else
                {
                    builder.Append("        <label id='itvl_lbl' style='margin-right: 20px'></label>");
                    builder.Append("        <button type='button' class='btn btn-primary' onclick='NextPlayer_Click()'>Next</button>");
                    builder.Append("        <button type='button' class='btn btn-secondary' onclick='CloseModal()'>Cancel</button>");
                }
            }

            builder.Append("      </div>");
            builder.Append("    </div>");
            builder.Append("  </div>");
            builder.Append("</div>");


            builder.Append("<script>");
            builder.AppendLine("var modal_interval = null;");

            //自動倒數
            if (page.is_auto)
            {
                builder.Append("    var try_count = 2; ");
                builder.Append("    $('#itvl_lbl').html(' ' + try_count + ' seconds');");

                builder.Append("    modal_interval = setInterval(function() {"
                    + " try_count = try_count - 1;"
                    + " $('#itvl_lbl').html(' ' + try_count + ' seconds');"
                    + " if (try_count <= 0) {"
                    + "     window.clearInterval(modal_interval);"
                    + "     NextPlayer_Click(); } "
                    + " }, 1000); ");

            }
            else if (page.is_end && page.run_auto)
            {
                builder.Append("    var try_count = 2; ");
                builder.Append("    $('#itvl_lbl').html(' ' + try_count + ' seconds');");

                builder.Append("    modal_interval = setInterval(function() {"
                    + " try_count = try_count - 1;"
                    + " $('#itvl_lbl').html(' ' + try_count + ' seconds');"
                    + " if (try_count <= 0) {"
                    + "     window.clearInterval(modal_interval);"
                    + "     modal_interval = null;"
                    + "     EndTimer_Click1();"
                    + "   }"
                    + " }, 1000); ");
            }

            //取消
            builder.Append("    function CloseModal() { if (modal_interval != null && modal_interval != undefined) { window.clearInterval(modal_interval); } $('#playerModal').modal('hide'); }");

            builder.Append("</script>");

            page.modal_contents = builder.ToString();
        }

        private void AppendPlayerModalBody_01(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmTeam, StringBuilder builder)
        {
            string program_name3 = itmTeam.getProperty("program_name3", "");
            string map_short_org = itmTeam.getProperty("map_short_org", "");
            string in_name = itmTeam.getProperty("in_name", "");
            string in_section_no = itmTeam.getProperty("in_section_no", "");

            string usr_photo = GetPhoto(itmTeam);


            builder.Append("<div class='modal-body'>");


            builder.Append("<div class='col-md-6'>");
            builder.Append("    <div class='portrait in_input' id='in_photo' name='in_photo' data-file_maxwidth='220' data-file_maxheight='220'");
            builder.Append("        data-file_quality='0.5'>");
            builder.Append("        <img class='in_thumbnail' src='" + usr_photo + "'>");
            builder.Append("        <textarea class='in_invisibleparam in_value'></textarea>");
            builder.Append("    </div>");
            builder.Append("</div>");

            builder.Append("<div class='col-md-6'>");

            builder.Append("    <div class='form-group'>");
            //builder.Append("        <label class='player_modal_title'>量級：</label>");
            builder.Append("        <label class='player_modal_text'>" + program_name3 + "</label>");
            builder.Append("    </div>");

            builder.Append("    <div class='form-group'>");
            //builder.Append("        <label class='player_modal_title'>所屬單位：</label>");
            builder.Append("        <label class='player_modal_text'>" + map_short_org + "</label>");
            builder.Append("    </div>");

            builder.Append("    <div class='form-group'>");
            //builder.Append("        <label class='player_modal_title'>姓名：</label>");
            builder.Append("        <label class='player_modal_text'>" + in_name + "</label>");
            builder.Append("    </div>");

            builder.Append("    <div class='form-group'>");
            builder.Append("        <label class='player_modal_title'>籤號：</label>");
            builder.Append("        <label class='player_modal_no'>" + in_section_no + "</label>");
            builder.Append("    </div>");

            builder.Append("</div>");


            builder.Append("</div>");
        }

        private void AppendPlayerModalBody_02(TConfig cfg, Item itmTeam, StringBuilder builder)
        {
            string program_name3 = itmTeam.getProperty("program_name3", "");
            string map_short_org = itmTeam.getProperty("map_short_org", "");
            string in_name = itmTeam.getProperty("in_name", "");
            string in_section_no = itmTeam.getProperty("in_section_no", "");

            string usr_photo = GetPhoto(itmTeam);

            string show_name = in_name;
            string show_org = map_short_org;
            if (in_name == "" && map_short_org != "")
            {
                show_name = map_short_org;
                show_org = "&nbsp;";
            }

            builder.Append("<div class='modal-body text-primary'>");



            builder.Append("<div class='card text-white bg-primary mb-3' style='max-width: 540px; '>");
            builder.Append("	<div class='row g-0'>");
            builder.Append("		<div class='col-md-4'>");
            builder.Append("			<div class='portrait in_input player-img-box' id='in_photo' name='in_photo' data-file_maxwidth='220'");
            builder.Append("				data-file_maxheight='220' data-file_quality='0.5'>");
            builder.Append("				<img class='in_thumbnail' src='" + usr_photo + "'> &nbsp;");
            builder.Append("				<textarea class='in_invisibleparam in_value'></textarea>");
            builder.Append("			</div>");
            builder.Append("		</div>");
            builder.Append("		<div class='col-md-8'>");
            builder.Append("			<div class='card-body'>");
            builder.Append("				<h5 class='card-title player-name-box' >" + show_name + "</h5>");
            builder.Append("				<p class='card-text player-org-box'>" + show_org + "</p>");
            builder.Append("				<h5 class='card-title text-center player-no-box'>" + in_section_no + "</h5>");
            builder.Append("			</div>");
            builder.Append("		</div>");
            builder.Append("	</div>");
            builder.Append("</div>");


            builder.Append("</div>");
        }

        private void AppendPlayerModalBody_03(TConfig cfg, Item itmTeam, StringBuilder builder)
        {
            string program_name3 = itmTeam.getProperty("program_name3", "");
            string map_short_org = itmTeam.getProperty("map_short_org", "");
            string in_name = itmTeam.getProperty("in_name", "");
            string in_section_no = itmTeam.getProperty("in_section_no", "");

            string usr_photo = GetPhoto(itmTeam);

            string show_name = in_name;
            string show_org = map_short_org;

            if (in_name == "" && map_short_org != "")
            {
                show_name = map_short_org;
                show_org = "&nbsp;";
            }
            else
            {
                show_name = "<img src='../Images/flags/" + show_org + ".png' height='32px' style='padding: 5px'>"
                    + show_name;
            }

            builder.Append("<div class='modal-body text-primary'>");

            builder.Append("<div class='card text-white bg-primary mb-3' style='max-width: 540px; '>");
            builder.Append("	<div class='row g-0'>");
            builder.Append("		<div class='col-md-12'>");
            builder.Append("			<div class='card-body'>");
            builder.Append("				<h5 class='card-title player-name-box' >" + show_name + "</h5>");
            builder.Append("				<p class='card-text player-org-box'>" + show_org + "</p>");
            builder.Append("				<h5 class='card-title text-center player-no-box'>" + in_section_no + "</h5>");
            builder.Append("			</div>");
            builder.Append("		</div>");
            builder.Append("	</div>");
            builder.Append("</div>");


            builder.Append("</div>");
        }

        private TPage GetPageInfo(TConfig cfg, Item itmTeam, Item itmReturn)
        {
            var result = new TPage();
            result.program_name2 = itmTeam.getProperty("program_name2", "");
            result.team_count = itmTeam.getProperty("program_team_count", "0");
            result.battle_type = itmTeam.getProperty("program_battle_type", "");
            result.page_no = "&nbsp;(" + cfg.serial + "/" + result.team_count + ")";

            result.run_auto = cfg.auto_next == "Y";

            result.is_end = result.team_count == cfg.serial;
            result.is_auto = result.run_auto;
            if (result.is_end) result.is_auto = false;

            result.is_english = cfg.page_language == "en";
            result.modal_contents = "";
            return result;
        }

        private Item GetPlayerItems(TConfig cfg)
        {
            string sql = @"
                SELECT 
	                t1.in_name2             AS 'program_name2'
	                , t1.in_name3           AS 'program_name3'
	                , t1.in_team_count      AS 'program_team_count'
	                , t1.in_battle_type     AS 'program_battle_type'
	                , t2.*
	                , t3.in_photo           AS 'resume_photo'
	                , t11.in_photo          AS 'org_photo'
                FROM 
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
	                VU_MEETING_PTEAM t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
	                AND ISNULL(t2.in_type, '') IN ('', 'p')
                    AND ISNULL(t2.in_not_draw, 0) = 0
                LEFT OUTER JOIN
	                IN_RESUME t3 WITH(NOLOCK)
	                ON t3.in_sno = t2.in_sno
                LEFT OUTER JOIN
	                IN_ORG t11 WITH(NOLOCK)
	                ON t11.in_group = t2.in_group
	                AND t11.in_current_org = t2.in_current_org
                WHERE 
	                t1.id = '{#program_id}'
	                AND t2.in_judo_no = '{#in_judo_no}'
            ";

            sql = sql.Replace("{#program_id}", cfg.program_id)
                .Replace("{#in_judo_no}", cfg.end_judo_no.ToString());

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        private class TPage
        {
            public string program_name2 { get; set; }
            public string team_count { get; set; }
            public string battle_type { get; set; }
            public string page_no { get; set; }
            public bool run_auto { get; set; }
            public bool is_end { get; set; }
            public bool is_auto { get; set; }
            public bool is_english { get; set; }
            public string modal_contents { get; set; }

        }

        private string GetPhoto(Item itmTeam)
        {
            string usr_photo = "";

            string resume_photo = itmTeam.getProperty("resume_photo", "");
            string org_photo = itmTeam.getProperty("org_photo", "");
            string def_photo = "../Images/taekwondo/default_photo.png";

            if (resume_photo != "")
            {
                usr_photo = "../DownloadMeetingFile.aspx?fileid=" + resume_photo;
            }
            else if (org_photo != "")
            {
                usr_photo = "../DownloadMeetingFile.aspx?fileid=" + org_photo;
            }
            else
            {
                usr_photo = def_photo;
            }
            return usr_photo;
        }

        //附加比賽選手
        private void AppendEvents(TConfig cfg, List<Item> itmEventPlayers, Item itmReturn)
        {
            TMap map = MapEvents(cfg, itmEventPlayers);

            BindMap(map);

            itmReturn.setProperty("bracket_table", GetBracketTableContents(cfg, map));
        }
        #region 賽程 Table

        /// <summary>
        /// 繪製賽程表資料表
        /// </summary>
        private string GetBracketTableContents(TConfig cfg, TMap map)
        {
            var objs = GetMapRows(cfg, map, map.Mains);

            var body = new StringBuilder();

            body.AppendLine("<tbody>");

            for (int i = 1; i <= map.rows; i++)
            {
                body.AppendLine("<tr>");

                var obj = objs[i - 1];

                for (int j = 1; j <= map.cols; j++)
                {
                    var field = obj.Cols[j - 1];

                    if (field.IsRemove)
                    {
                        continue;
                    }

                    body.Append("<td");
                    body.Append(field.RowSpan);
                    body.Append(GetClasses(field.Classes));
                    body.Append(" >");
                    body.Append(field.Value);
                    body.Append("</td>");
                }
                body.AppendLine("</tr>");
            }
            body.AppendLine("</tbody>");

            var builder = new StringBuilder();

            builder.AppendLine("<table class='tb-box'>");
            builder.Append(GetHeadBuilder(cfg, map));
            builder.Append(body);
            builder.AppendLine("</table>");

            return builder.ToString();
        }

        private List<TRow> GetMapRows(TConfig cfg, TMap map, Dictionary<string, List<Item>> dictionary)
        {
            List<TRow> rows = GetMapRowsData(cfg, map, dictionary);

            List<TSignGroup> signs = map.real_team_count == 2 ? GetSigns2(map) : GetSigns(map);

            AppendMapLines(cfg, map, dictionary, rows, signs);

            return rows;
        }

        private List<TSignGroup> GetSigns2(TMap map)
        {
            List<TSignGroup> result = new List<TSignGroup>();

            int center_cidx = 6;

            result.Add(new TSignGroup { cidx = center_cidx, ys = 0, ye = 0, val = "1", id = "M101", is_end = true });

            return result;
        }

        private List<TSignGroup> GetSigns(TMap map)
        {
            List<TSignGroup> result = new List<TSignGroup>();

            int xs = 0;
            int xcount = map.tree_team_count / 2;
            int yspan = map.team_rows;

            int last_cidx = 0;
            for (int i = 0; i < map.round - 1; i++)
            {
                int round = i + 1;
                int l_cidx = map.team_col_end + i;
                int r_cidx = map.cols - l_cidx - 1;
                List<int> cidxs = new List<int> { l_cidx, r_cidx };

                AppendSignGroup(map, result, round, cidxs, xs, xcount, yspan);

                xs += yspan / 2;
                xcount = xcount / 2;
                yspan = yspan * 2;
                last_cidx = l_cidx;
            }

            var m_cidx = last_cidx + 2;
            var m_ridx = map.rows / 2 - 1;
            var m_tree_id = GetRowId(map.round, 1);

            result.Add(new TSignGroup { cidx = m_cidx, ys = m_ridx, ye = m_ridx, val = m_tree_id, id = m_tree_id, is_end = true });

            return result;
        }

        private void AppendSignGroup(TMap map, List<TSignGroup> groups, int round, List<int> cidxs, int xs, int xcount, int yspan)
        {
            int round_id = 1;
            for (int i = 0; i < cidxs.Count; i++)
            {
                int cidx = cidxs[i];
                bool is_right = i > 0;

                for (int j = 0; j < xcount; j++)
                {
                    if ((j % 2) == 1) continue;

                    int ridx = j * yspan;
                    int ys = ridx + xs;
                    int ye = ys + yspan;
                    string in_tree_id = GetRowId(round, round_id);

                    var group = new TSignGroup
                    {
                        cidx = cidx,
                        ys = ys,
                        ye = ye,
                        is_right = is_right,
                        val = in_tree_id,
                        id = in_tree_id,
                    };

                    groups.Add(group);
                    round_id++;
                }
            }
        }

        private string GetRowId(int round, int seq)
        {
            return "M" + round + seq.ToString().PadLeft(2, '0');
        }

        private void AppendMapLines(TConfig cfg, TMap map, Dictionary<string, List<Item>> dictionary, List<TRow> rows, List<TSignGroup> signs)
        {
            for (int i = 0; i < signs.Count; i++)
            {
                var sg = signs[i];
                var cidx = sg.cidx;

                if (sg.is_long)
                {
                    rows[sg.ridx].Cols[cidx].Classes.Add("line-bottom");
                }
                else
                {
                    int ys = sg.ys;
                    int ye = sg.ye;
                    int ym = (ys + ye) / 2;

                    rows[ys].Cols[cidx].Classes.Add("line-bottom");
                    rows[ye + 1].Cols[cidx].Classes.Add("line-top");

                    Item itmSign1 = null;
                    Item itmSign2 = null;

                    if (sg.id != null && sg.id != "")
                    {
                        itmSign1 = GetTeam(cfg, dictionary, sg.id, "1");
                        itmSign2 = GetTeam(cfg, dictionary, sg.id, "2");
                    }
                    else
                    {
                        itmSign1 = cfg.inn.newItem();
                        itmSign2 = cfg.inn.newItem();
                    }

                    string in_tree_no = itmSign1.getProperty("in_tree_no", "");
                    string win_sign_no = itmSign1.getProperty("in_win_sign_no", "");
                    string f1_sign_no = itmSign1.getProperty("in_sign_no", "");
                    string f2_sign_no = itmSign2.getProperty("in_sign_no", "");

                    bool f1_win = f1_sign_no == win_sign_no;
                    bool f2_win = f2_sign_no == win_sign_no;

                    if (sg.is_end)
                    {
                        rows[ym].Cols[cidx - 1].Classes.Add("line-bottom");
                        rows[ym].Cols[cidx + 1].Classes.Add("line-bottom");
                        rows[ym].Cols[cidx - 1].Classes.Add(GetSignClass(itmSign1));
                        rows[ym].Cols[cidx + 1].Classes.Add(GetSignClass(itmSign2));

                        rows[ym].Cols[cidx - 1].Value = GetSignNoInfo(itmSign1);
                        rows[ym].Cols[cidx].Value = GetRowIdLink(cfg, sg.id, in_tree_no);
                        rows[ym].Cols[cidx + 1].Value = GetSignNoInfo(itmSign2);

                        rows[ym + 1].Cols[cidx - 1].Value = GetStatusDisplay(itmSign1);
                        rows[ym + 1].Cols[cidx + 1].Value = GetStatusDisplay(itmSign2);

                        if (itmSign1.getProperty("in_status") == "1")
                        {
                            rows[ym].Cols[cidx].Classes.Add(GetSignClass(itmSign1));
                        }
                        else if (itmSign2.getProperty("in_status") == "1")
                        {
                            rows[ym].Cols[cidx].Classes.Add(GetSignClass(itmSign2));
                        }
                    }
                    else
                    {
                        rows[ys].Cols[cidx].Value = GetSignNoInfo(itmSign1);
                        rows[ym].Cols[cidx].Value = GetRowIdLink(cfg, sg.id, in_tree_no);
                        rows[ye + 1].Cols[cidx].Value = GetSignNoInfo(itmSign2);

                        rows[ys].Cols[cidx].Classes.Add(GetSignClass(itmSign1));
                        rows[ye + 1].Cols[cidx].Classes.Add(GetSignClass(itmSign2));
                    }

                    for (int y = ys + 1; y <= ye; y++)
                    {
                        if (sg.is_right)
                        {
                            rows[y].Cols[cidx].Classes.Add("line-left");

                        }
                        else
                        {
                            rows[y].Cols[cidx].Classes.Add("line-right");
                        }

                        if (y <= ym)
                        {
                            if (f1_win)
                            {
                                rows[y].Cols[cidx].Classes.Add(GetSignClass(itmSign1));
                            }
                        }
                        else
                        {
                            if (f2_win)
                            {
                                rows[y].Cols[cidx].Classes.Add(GetSignClass(itmSign2));
                            }
                        }
                    }
                }
            }
        }

        private string GetSignClass(Item itmSign)
        {
            string org_name = itmSign.getProperty("map_short_org", "");
            if (org_name == "")
            {
                return "";
            }

            if (itmSign.getProperty("in_sign_bypass", "") == "1")
            {
                return "";
            }

            return "sgn-" + itmSign.getProperty("in_section_no", "").PadLeft(3, '0');
        }

        private Item GetTeam(TConfig cfg, Dictionary<string, List<Item>> dictionary, string key, string foot)
        {
            Item result = null;
            if (!string.IsNullOrEmpty(key) && dictionary.ContainsKey(key))
            {
                List<Item> list = dictionary[key];

                for (int i = 0; i < list.Count; i++)
                {
                    Item item = list[i];
                    string in_sign_foot = item.getProperty("in_sign_foot", "");
                    if (in_sign_foot == foot)
                    {
                        result = item;
                    }
                }
            }

            if (result == null)
            {
                result = cfg.inn.newItem();
            }

            return result;
        }

        private string GetRowIdLink(TConfig cfg, string tree_id, string tno, string btn_class = "")
        {
            if (cfg.isEdit)
            {
                return GetRowIdLinkEdit(cfg, tree_id, tno);
            }
            else
            {
                return GetRowIdLinkView(cfg, tree_id, tno, btn_class);
            }
        }

        private string GetRowIdLinkView(TConfig cfg, string tree_id, string tno, string btn_class = "")
        {
            string current_class = btn_class == "" ? "event-btn" : btn_class;

            if (tno == "0")
            {
                if (cfg.canNAEdit)
                {
                    return "<a class='" + current_class + "' href='javascript:void(0)' onclick='Event_Click(this)' data-pid='" + cfg.program_id + "' data-rid='" + tree_id + "'>NA</a>";
                }
                else
                {
                    return "<a class='no-event-btn' href='javascript:void(0)' onclick='Event_Click(this)' data-pid='" + cfg.program_id + "' data-rid='" + tree_id + "'>　</a>";
                }
            }
            else if (tno != "")
            {
                string val = "&nbsp;" + tno + "&nbsp;";

                return "<a class='" + current_class + "' href='javascript:void(0)' onclick='Event_Click(this)' data-pid='" + cfg.program_id + "' data-rid='" + tree_id + "'>" + val + "</a>";
            }
            else
            {
                if (cfg.canNAEdit)
                {
                    return "<a class='" + current_class + "' href='javascript:void(0)' onclick='Event_Click(this)' data-pid='" + cfg.program_id + "' data-rid='" + tree_id + "'>NA</a>";
                }
                else
                {
                    return "<a class='no-event-btn' href='javascript:void(0)' onclick='Event_Click(this)' data-pid='" + cfg.program_id + "' data-rid='" + tree_id + "'>　</a>";
                    //return "&nbsp;";
                }
            }
        }

        private string GetRowIdLinkEdit(TConfig cfg, string tree_id, string tno)
        {
            string value = tno != "" ? tno : "NA";

            return "<input type='text' value='" + value + "' class='event-input'"
            + " data-pid='" + cfg.program_id + "' data-rid='" + tree_id + "' data-old='" + tno + "'"
            + " onkeyup='Event_KeyUp(this)'"
            + " ondblclick='Event_Click(this)'"
            + " >";
        }

        private List<TRow> GetMapRowsData(TConfig cfg, TMap map, Dictionary<string, List<Item>> dictionary)
        {
            List<TRow> result = new List<TRow>();

            var body_merge = new List<int>();
            body_merge.Add(1);
            body_merge.Add(3);
            body_merge.Add(5);
            body_merge.Add(map.cols - 0);
            body_merge.Add(map.cols - 2);
            body_merge.Add(map.cols - 4);

            for (int i = 1; i <= map.rows; i++)
            {
                int rno = i % 4 == 0 ? i / 4 : i / 4 + 1;

                TRow row = new TRow
                {
                    RNo = rno,
                    Cols = new List<TCol>(),
                };

                row.IsTeamRow = i % 4 == 1;
                row.IsMergeRow = i % 4 == 2;

                for (int j = 1; j <= map.cols; j++)
                {
                    TCol col = new TCol
                    {
                        Classes = new List<string>()
                    };
                    row.Cols.Add(col);
                }

                if (row.IsTeamRow)
                {
                    row.Cols[1 - 1].Classes.Add("rpc-number");
                    row.Cols[3 - 1].Classes.Add("rpc-org inn-left");
                    row.Cols[5 - 1].Classes.Add("rpc-name");
                    row.Cols[map.cols - 1].Classes.Add("rpc-number");
                    row.Cols[map.cols - 3].Classes.Add("rpc-org inn-right");
                    row.Cols[map.cols - 5].Classes.Add("rpc-name");
                }

                for (int x = 0; x < body_merge.Count; x++)
                {
                    var idx = body_merge[x];
                    var field = row.Cols[idx - 1];

                    if (row.IsTeamRow)
                    {
                        field.RowSpan = " rowspan='2'";
                    }
                    else if (row.IsMergeRow)
                    {
                        field.IsRemove = true;
                    }
                }

                if (row.IsTeamRow)
                {
                    int xs = map.tree_team_count / 4;
                    int rs = row.RNo % 2 == 0 ? row.RNo / 2 : row.RNo / 2 + 1;

                    string l_tree_id = GetRowId(1, rs);
                    string r_tree_id = GetRowId(1, rs + xs);
                    string sign_foot = rno % 2 != 0 ? "1" : "2";

                    var itmLeft = GetTeam(cfg, dictionary, l_tree_id, sign_foot);
                    var itmRight = GetTeam(cfg, dictionary, r_tree_id, sign_foot);

                    string left_check_result = itmLeft.getProperty("in_check_result", "");
                    string right_check_result = itmRight.getProperty("in_check_result", "");

                    row.Cols[1 - 1].Value = GetSignNoInfoA(cfg, itmLeft);
                    row.Cols[3 - 1].Value = GetOrgInfo(cfg, itmLeft);
                    row.Cols[5 - 1].Value = GetNameInfo(cfg, itmLeft);

                    row.Cols[map.cols - 1].Value = GetSignNoInfoA(cfg, itmRight);
                    row.Cols[map.cols - 3].Value = GetOrgInfo(cfg, itmRight);
                    row.Cols[map.cols - 5].Value = GetNameInfo(cfg, itmRight);
                }

                result.Add(row);
            }
            return result;
        }

        /// <summary>
        /// 取得標題內文
        /// </summary>
        private StringBuilder GetHeadBuilder(TConfig cfg, TMap map)
        {
            var hdr_num = "籤號";
            var hdr_org = "單位";
            var hdr_nam = "姓名";
            var head_titles = GetClearArr(map.arrs);
            if (cfg.page_language == "en")
            {
                hdr_num = "No.";
                hdr_org = "Org.";
                hdr_nam = "Name";
            }

            head_titles[1] = hdr_num;
            head_titles[3] = hdr_org;
            head_titles[5] = hdr_nam;
            head_titles[map.cols - 0] = hdr_num;
            head_titles[map.cols - 2] = hdr_org;
            head_titles[map.cols - 4] = hdr_nam;

            for (int i = 1; i < map.round; i++)
            {
                var s = map.team_col_end;
                var e = map.cols;
                head_titles[s + i] = "R" + i;
                head_titles[e - 4 - i] = "R" + i;
            }
            head_titles[map.cols_half] = "R" + map.round;

            var head_widths = GetClearArr(map.arrs, "width='50px' ");
            head_widths[1] = "";
            head_widths[2] = "width='1px' ";
            head_widths[3] = "";
            head_widths[4] = "width='1px' ";
            head_widths[5] = "";
            head_widths[map.cols - 0] = "";
            head_widths[map.cols - 1] = "width='1px' ";
            head_widths[map.cols - 2] = "";
            head_widths[map.cols - 3] = "width='1px' ";
            head_widths[map.cols - 4] = "";

            var head = new StringBuilder();
            head.AppendLine("<thead>");
            head.AppendLine("<tr>");
            for (int j = 1; j <= map.cols; j++)
            {
                head.Append("<th ");
                head.Append(head_widths[j]);
                head.Append(" >");
                head.Append(head_titles[j]);
                head.Append("</th>");
                head.AppendLine();
            }
            head.AppendLine("</tr>");
            head.AppendLine("</thead>");

            return head;
        }

        /// <summary>
        /// 資料列
        /// </summary>
        private class TRow
        {
            /// <summary>
            /// 列序號
            /// </summary>
            public int RNo { get; set; }

            /// <summary>
            /// 是否為隊伍資料列
            /// </summary>
            public bool IsTeamRow { get; set; }

            /// <summary>
            /// 是否為合併列
            /// </summary>
            public bool IsMergeRow { get; set; }

            /// <summary>
            /// 資料欄
            /// </summary>
            public List<TCol> Cols { get; set; }
        }

        /// <summary>
        /// 資料欄
        /// </summary>
        private class TCol
        {
            /// <summary>
            /// 是否移除 (不繪製該 td)
            /// </summary>
            public bool IsRemove { get; set; }

            /// <summary>
            /// 跨列合併數
            /// </summary>
            public string RowSpan { get; set; }

            /// <summary>
            /// 樣式集
            /// </summary>
            public List<string> Classes { get; set; }

            /// <summary>
            /// 值
            /// </summary>
            public string Value { get; set; }
        }

        private class TRowTeam
        {
            public TTeam WTeam { get; set; }
            public TTeam ETeam { get; set; }
        }

        private class TTeam
        {
            public string in_tree_id { get; set; }

            public string in_sign_foot { get; set; }
        }

        private class TSignGroup
        {
            public bool is_right { get; set; }
            public bool is_end { get; set; }
            public bool is_long { get; set; }
            public int cidx { get; set; }
            public int ridx { get; set; }
            public int ys { get; set; }
            public int ye { get; set; }
            public string id { get; set; }
            public string val { get; set; }
        }

        /// <summary>
        /// 繫結圖面資料
        /// </summary>
        /// <param name="map"></param>
        private void BindMap(TMap map)
        {
            int team_col_end = 5;

            map.round = GetRounds(map.tree_team_count);

            map.team_rows = 4;
            map.group_rows = map.team_rows * 2;

            map.surface_teams = map.tree_team_count / 2;
            map.surface_events = map.surface_teams / 2;

            map.team_cols = (3 + 2) * 2;
            map.round_cols = map.round * 2 + 1;

            map.rows = map.surface_teams * map.team_rows - 2;//最後一列不需 4 列
            map.cols = map.team_cols + map.round_cols;

            map.rows_half = map.rows / 2;
            map.cols_half = map.cols / 2 + 1;

            map.arrs = map.cols + 1;

            map.m_idxs = new List<int>();
            map.m_idxs.Add(map.cols_half - 1);
            map.m_idxs.Add(map.cols_half);
            map.m_idxs.Add(map.cols_half + 1);


            map.team_col_end = team_col_end;

            map.l_idxs = new List<int>();
            map.r_idxs = new List<int>();

            for (int i = 1; i < map.round; i++)
            {
                var s = map.team_col_end;
                var e = map.cols;

                map.l_idxs.Add(s + i);
                map.r_idxs.Add(e - 4 - i);
            }
        }

        /// <summary>
        /// 圖面資料模型
        /// </summary>
        private class TMap
        {
            /// <summary>
            /// 賽事 id
            /// </summary>
            public string meeting_id { get; set; }

            /// <summary>
            /// 組別 id
            /// </summary>
            public string program_id { get; set; }

            /// <summary>
            /// 組別隊伍數
            /// </summary>
            public int real_team_count { get; set; }

            /// <summary>
            /// 賽程表隊伍數
            /// </summary>
            public int tree_team_count { get; set; }

            /// <summary>
            /// 回合數
            /// </summary>
            public int round { get; set; }

            /// <summary>
            /// 隊-資料列數
            /// </summary>
            public int team_rows { get; set; }

            /// <summary>
            /// 競賽群組-資料列數
            /// </summary>
            public int group_rows { get; set; }

            /// <summary>
            /// 組面-隊伍數
            /// </summary>
            public int surface_teams { get; set; }

            /// <summary>
            /// 組面-場次數
            /// </summary>
            public int surface_events { get; set; }

            /// <summary>
            /// 隊資-欄位數
            /// </summary>
            public int team_cols { get; set; }

            /// <summary>
            /// 回合-欄位數
            /// </summary>
            public int round_cols { get; set; }

            /// <summary>
            /// 列數
            /// </summary>
            public int rows { get; set; }

            /// <summary>
            /// 欄數
            /// </summary>
            public int cols { get; set; }

            /// <summary>
            /// y軸 中數 (rows 除以 2)
            /// </summary>
            public int rows_half { get; set; }

            /// <summary>
            /// x軸 中數 (cols 除以 2)
            /// </summary>
            public int cols_half { get; set; }

            /// <summary>
            /// 隊資欄位-結束位置
            /// </summary>
            public int team_col_end { get; set; }

            /// <summary>
            /// 字串陣列(由於從 1 取值，因此長度為 cols + 1)
            /// </summary>
            public int arrs { get; set; }

            /// <summary>
            /// 回合-左面欄位索引集
            /// </summary>
            public List<int> l_idxs { get; set; }

            /// <summary>
            /// 回合-中間欄位索引集
            /// </summary>
            public List<int> m_idxs { get; set; }

            /// <summary>
            /// 回合-右間欄位索引集
            /// </summary>
            public List<int> r_idxs { get; set; }

            /// <summary>
            /// 主線資料集
            /// </summary>
            public Dictionary<string, List<Item>> Mains { get; set; }

            /// <summary>
            /// 隊資料集
            /// </summary>
            public Dictionary<string, Item> Teams { get; set; }

            /// <summary>
            /// 場次資料集
            /// </summary>
            public Dictionary<string, Dictionary<string, Item>> Events { get; set; }
        }

        #endregion 賽程 Table

        //轉換比賽分組
        private TMap MapEvents(TConfig cfg, List<Item> itmEventPlayers)
        {
            TMap result = new TMap
            {
                meeting_id = cfg.itmProgram.getProperty("meeitng_id", ""),
                program_id = cfg.itmProgram.getProperty("program_id", ""),
                real_team_count = GetIntVal(cfg.itmProgram.getProperty("in_team_count", "0")),
                tree_team_count = GetIntVal(cfg.itmProgram.getProperty("in_round_code", "0")),

                Mains = new Dictionary<string, List<Item>>(),

                Teams = new Dictionary<string, Item>(),
                Events = new Dictionary<string, Dictionary<string, Item>>(),

            };

            int count = itmEventPlayers.Count;

            for (int i = 0; i < count; i++)
            {
                Item item = itmEventPlayers[i];
                string round = item.getProperty("in_round", "");
                string in_tree_id = item.getProperty("in_tree_id", "");

                AppendDetail(result.Mains, item, in_tree_id);
            }

            return result;
        }

        private void AppendEvents(Dictionary<string, Dictionary<string, Item>> dictionary, Item item)
        {
            string in_tree_id = item.getProperty("in_tree_id", "");
            string in_sign_foot = item.getProperty("in_sign_foot", "");

            //M101
            string key = in_tree_id;
            string sub_key = in_sign_foot;

            Dictionary<string, Item> sub = null;
            if (dictionary.ContainsKey(key))
            {
                sub = dictionary[key];
            }
            else
            {
                sub = new Dictionary<string, Item>();
                dictionary.Add(key, sub);
            }

            if (sub.ContainsKey(sub_key))
            {
                //異常
            }
            else
            {
                sub.Add(sub_key, item);
            }
        }

        private void AppendTeams(Dictionary<string, Item> dictionary, Item item)
        {
            string mdid = item.getProperty("in_sign_no", "");
            string in_tree_id = item.getProperty("in_tree_id", "");
            string in_sign_foot = item.getProperty("in_sign_foot", "");

            //M101-1
            string key = in_tree_id + "-" + in_sign_foot;

            if (dictionary.ContainsKey(key))
            {
                //異常
            }
            else
            {
                if (mdid == "")
                {
                    item.setProperty("no_team", "1");
                }
                dictionary.Add(key, item);
            }
        }

        private void AppendDetail(Dictionary<string, List<Item>> dictionary, Item item, string key)
        {
            if (dictionary.ContainsKey(key))
            {
                dictionary[key].Add(item);
            }
            else
            {
                List<Item> list = new List<Item>();
                list.Add(item);
                dictionary.Add(key, list);
            }
        }

        //取得勝敗呈現(LINE)
        private string GetStatusDisplay(Item item)
        {
            string in_status = item.getProperty("in_status", "");
            string in_points = item.getProperty("in_points", "0");

            switch (in_status)
            {
                case "1": return "<span class='team_score_b'>" + in_points + "</span>";
                case "0": return "<span class='team_score_c'>" + in_points + "</span>";
                default: return "<span class='team_score_a'>&nbsp;</span>";
            }
        }

        private string GetSignNoInfo(Item item)
        {
            string in_sign_bypass = item.getProperty("in_sign_bypass", "");
            if (in_sign_bypass == "1")
            {
                return "";
            }

            string in_current_org = item.getProperty("in_current_org", "");
            if (in_current_org == "")
            {
                return "";
            }

            return item.getProperty("in_section_no", "");
        }

        private string GetSignNoInfoA(TConfig cfg, Item item)
        {
            string in_sign_bypass = item.getProperty("in_sign_bypass", "");
            if (in_sign_bypass == "1")
            {
                return "";
            }

            string in_current_org = item.getProperty("in_current_org", "");
            if (in_current_org == "")
            {
                return "";
            }

            string in_section_no = item.getProperty("in_section_no", "");
            string archor = "ssn_lbl_" + in_section_no;
            string position = "<input type='text' id='" + archor + "' class='text_archor'></label>";
            return in_section_no + position;
        }

        private string GetNameInfo(TConfig cfg, Item item)
        {
            string in_name = item.getProperty("in_name", "");
            string in_sign_no = item.getProperty("in_sign_no", "");
            string in_check_result = item.getProperty("in_check_result", "");
            string in_weight_message = item.getProperty("in_weight_message", "");

            //string display = in_name + in_weight_message;
            string display = in_name.Replace("(", "<br>(") + in_weight_message;

            if (in_name == "")
            {
                display = "&nbsp;";
            }

            if (cfg.showSignNo)
            {
                display += " (" + in_sign_no + ")";
            }

            switch (in_check_result)
            {
                // case "1": return "<p class='text-muted text-center btn-success'>" + display + "</p>";
                // case "0": return "<p class='text-muted text-center btn-danger'>" + display + "</p>";
                // default: return "<p class='text-muted text-center btn-default'>" + display + "</p>";

                case "1": return "<span class='player_on'>" + display + "</span>";
                case "0": return "<span class='player_off'>" + display + "</span>";
                default: return "<span  class='player_default'>" + display + "</span>";
            }
        }

        private string GetOrgInfo(TConfig cfg, Item item)
        {
            string org_name = item.getProperty("map_short_org", "");
            if (org_name == "")
            {
                return "";
            }

            if (cfg.showOrgCount)
            {
                string in_org_teams = item.getProperty("in_org_teams", "");
                if (in_org_teams != "" && in_org_teams != "0" && in_org_teams != "1")
                {
                    org_name += " (" + in_org_teams + ")";
                }
            }

            return org_name;
        }

        //取得賽事組別場次
        private Item GetEventPlayersEmpty(TConfig cfg)
        {
            string sql = @"
                SELECT 
                    t1.id AS 'event_id'
                    , t1.in_tree_name
                    , t1.in_tree_id
                    , t1.in_tree_no
                    , t1.in_round
                    , t1.in_round_id
                    , t1.in_round_code
                    , t1.in_site_code
                    , t1.in_site_no
                    , t1.in_site_id
                    , t1.in_site_allocate
                    , t1.in_next_win
                    , t1.in_next_foot_win
                    , t1.in_next_lose
                    , t1.in_next_foot_lose
                    , t1.in_detail_ns
                    , t1.in_win_sign_no
                    , t2.id AS 'detail_id'
                    , t2.in_sign_foot
                    , t2.in_sign_no
                    , t2.in_sign_bypass
                    , t2.in_target_no
                    , t2.in_points
                    , t2.in_status
                FROM 
                    IN_MEETING_PEVENT t1 WITH(NOLOCK)
                LEFT OUTER JOIN
                    IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK)
                    ON t2.source_id = t1.id
                WHERE
                    t1.source_id = '{#program_id}'
                    AND t1.in_tree_name = 'main'
                ORDER BY
                    t1.in_tree_name
                    , t1.in_round
                    , t1.in_round_id
                    , t2.in_sign_foot
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#program_id}", cfg.program_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        //取得賽事組別場次
        private Item GetEventPlayersAll(TConfig cfg)
        {
            string sql = @"
                SELECT 
                    t1.id AS 'event_id'
                    , t1.in_tree_name
                    , t1.in_tree_id
                    , t1.in_tree_no
                    , t1.in_round
                    , t1.in_round_id
                    , t1.in_round_code
                    , t1.in_site_code
                    , t1.in_site_no
                    , t1.in_site_id
                    , t1.in_site_allocate
                    , t1.in_next_win
                    , t1.in_next_foot_win
                    , t1.in_next_lose
                    , t1.in_next_foot_lose
                    , t1.in_detail_ns
                    , t1.in_win_sign_no
                    , t2.id AS 'detail_id'
                    , t2.in_sign_foot
                    , t2.in_sign_no
                    , t2.in_sign_bypass
                    , t2.in_target_no
                    , t2.in_points
                    , t2.in_status
                    , t3.id AS 'mdid'
                    , t3.in_current_org
                    , t3.in_short_org
                    , t3.map_short_org
                    , t3.in_name
                    , t3.in_names
                    , t3.in_sno
                    , t3.in_org_teams
                    , t3.in_section_no
                    , t3.in_judo_no
                    , t3.in_seeds
                    , t3.in_check_result
                    , t3.in_weight_message
                FROM 
                    IN_MEETING_PEVENT t1 WITH(NOLOCK)
                LEFT OUTER JOIN
                    IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK)
                    ON t2.source_id = t1.id
                LEFT OUTER JOIN
                    VU_MEETING_PTEAM t3 WITH(NOLOCK)
                    ON t3.source_id = t1.source_id
                    AND ISNULL(t3.in_sign_no, '') <> ''
                    AND t3.in_sign_no = t2.in_sign_no
                    AND t3.in_type = 'p'
                    AND ISNULL(t3.in_not_draw, 0) = 0
                WHERE
                    t1.source_id = '{#program_id}'
                    AND t1.in_tree_name = 'main'
                ORDER BY
                    t1.in_tree_name
                    , t1.in_round
                    , t1.in_round_id
                    , t2.in_sign_foot
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#program_id}", cfg.program_id)
                .Replace("{#top_count}", cfg.serial);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        //取得賽事組別場次
        private Item GetEventPlayers(TConfig cfg)
        {
            string sql = @"
                SELECT 
                    t1.id AS 'event_id'
                    , t1.in_tree_name
                    , t1.in_tree_id
                    , t1.in_tree_no
                    , t1.in_round
                    , t1.in_round_id
                    , t1.in_round_code
                    , t1.in_site_code
                    , t1.in_site_no
                    , t1.in_site_id
                    , t1.in_site_allocate
                    , t1.in_next_win
                    , t1.in_next_foot_win
                    , t1.in_next_lose
                    , t1.in_next_foot_lose
                    , t1.in_detail_ns
                    , t1.in_win_sign_no
                    , t2.id AS 'detail_id'
                    , t2.in_sign_foot
                    , t2.in_sign_no
                    , t2.in_sign_bypass
                    , t2.in_target_no
                    , t2.in_points
                    , t2.in_status
                    , t3.id AS 'mdid'
                    , t3.in_current_org
                    , t3.in_short_org
                    , t3.map_short_org
                    , t3.in_name
                    , t3.in_names
                    , t3.in_sno
                    , t3.in_org_teams
                    , t3.in_section_no
                    , t3.in_seeds
                    , t3.in_check_result
                    , t3.in_weight_message
                FROM 
                    IN_MEETING_PEVENT t1 WITH(NOLOCK)
                LEFT OUTER JOIN
                    IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK)
                    ON t2.source_id = t1.id
                LEFT OUTER JOIN
                    VU_MEETING_PTEAM t3 WITH(NOLOCK)
                    ON t3.source_id = t1.source_id
                    AND t3.in_sign_no = t2.in_sign_no
                    AND ISNULL(t3.in_sign_no, '') <> ''
                    AND ISNULL(t3.in_type, '') IN ('', 'p')
                    AND ISNULL(t3.in_not_draw, 0) = 0
			        AND t3.id IN
			        (
				        SELECT TOP {#top_count} id FROM IN_MEETING_PTEAM WITH(NOLOCK) 
                        WHERE source_id = '{#program_id}'
				        AND ISNULL(in_type, '') IN ('', 'p')
				        AND ISNULL(in_not_draw, 0) = 0
				        order by in_stuff_b1, in_team, in_sno
			        )
                WHERE
                    t1.source_id = '{#program_id}'
                    AND t1.in_tree_name = 'main'
                ORDER BY
                    t1.in_tree_name
                    , t1.in_round
                    , t1.in_round_id
                    , t2.in_sign_foot
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#program_id}", cfg.program_id)
                .Replace("{#top_count}", cfg.serial);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// 取得樣式集
        /// </summary>
        private string GetClasses(List<string> list)
        {
            if (list.Count == 0)
            {
                return " class='td_empty'";
            }
            else
            {
                return " class='" + string.Join(" ", list) + "'";
            }
        }

        /// <summary>
        /// 取得回合數
        /// </summary>
        private int GetRounds(int value, int code = 2, int count = 0)
        {
            while (value > 1)
            {
                value = value / code;
                count++;
            }
            return count;
        }

        /// <summary>
        /// 取得空字串陣列
        /// </summary>
        private string[] GetClearArr(int len, string def = "")
        {
            string[] result = new string[len];
            for (int i = 0; i < len; i++)
            {
                result[i] = def;
            }
            return result;
        }

        private int GetIntVal(string value, int def = 0)
        {
            int result = def;
            Int32.TryParse(value, out result);
            return result;
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }

            public string meeting_id { get; set; }
            public string program_id { get; set; }
            public string mode { get; set; }
            public string eno { get; set; }
            public string in_sign_time { get; set; }
            public string serial { get; set; }
            public string auto_next { get; set; }
            public string page_language { get; set; }
            public int end_judo_no { get; set; }

            public Item itmProgram { get; set; }

            /// <summary>
            /// 要秀出籤號
            /// </summary>
            public bool showSignNo { get; set; }
            /// <summary>
            /// 要秀出同隊數量
            /// </summary>
            public bool showOrgCount { get; set; }
            /// <summary>
            /// 是否場次編號可編輯模式
            /// </summary>
            public bool isEdit { get; set; }
            /// <summary>
            /// 是否場次全秀 (NA 可點擊)
            /// </summary>
            public bool canNAEdit { get; set; }
        }
    }
}