﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Judo.Fight
{
    public class in_meeting_program_kata : Item
    {
        public in_meeting_program_kata(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 格式組
                備註:
                    - 投固柔極護，年齡由大到小
                日誌: 
                    - 2022-12-09: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]in_meeting_program_kata";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                strIdentityId = inn.getUserAliases(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                program_id = itmR.getProperty("program_id", ""),
                battle_type = itmR.getProperty("battle_type", ""),
            };

            if (cfg.meeting_id == "" || cfg.program_id == "" || cfg.battle_type == "")
            {
                throw new Exception("參數錯誤");
            }

            //檢查頁面權限
            Item itmPermit = inn.applyMethod("In_CheckIdentity", "<method>" + strMethodName + "</method><code>" + "ALL" + "</code>");
            bool isMeetingAdmin = itmPermit.getProperty("isMeetingAdmin", "") == "1";
            bool isMeetingLeader = itmPermit.getProperty("isMeetingLeader", "") == "1";
            bool can_view = isMeetingAdmin || isMeetingLeader;
            if (!can_view)
            {
                itmR.setProperty("error_message", "您無權限瀏覽此頁面");
                return itmR;
            }

            Item itmMeeting = GetMeeting(cfg);
            if (itmMeeting.isError())
            {
                throw new Exception("賽事資料錯誤");
            }

            Item itmProgram = GetProgram(cfg);
            if (itmMeeting.isError())
            {
                throw new Exception("賽程組別資料錯誤");
            }

            //更新組別賽制
            UpdateProgram(cfg, itmProgram);

            //清除場次資料
            RemoveEvents(cfg, itmProgram);

            //新增賽事場次
            AddKataEvents(cfg, itmProgram);

            //新增賽事場次明細
            AddKataEventDetails(cfg, itmMeeting, itmProgram);

            //更新場地編號
            UpdateSiteCode(cfg, itmMeeting, itmProgram);

            //重算場次數
            RecountEvents(cfg, itmProgram);

            //修補隊伍的量級序號
            FixTeamSectionNo(cfg, itmProgram);

            return itmR;
        }

        //重算場次數(個人賽)
        private void RecountEvents(TConfig cfg, Item itmProgram)
        {
            string sql = "";
            Item itmSQL = null;

            string program_id = itmProgram.getProperty("id", "");

            sql = @"
                UPDATE t1 SET
	                t1.in_event_count = t2.cnt
                FROM
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
                (
	                SELECT
		                source_id
		                , count(*) AS 'cnt'
	                FROM
		                IN_MEETING_PEVENT WITH(NOLOCK)
	                WHERE
		                source_id = '{#program_id}'
		                AND ISNULL(in_tree_no, 0) > 0
		                AND ISNULL(in_win_status, '') NOT IN ('cancel', 'nofight', 'bypass')
	                GROUP BY
		                source_id
                ) t2
                ON t2.source_id = t1.id
            ";

            sql = sql.Replace("{#program_id}", program_id);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            itmSQL = cfg.inn.applySQL(sql);

            if (itmSQL.isError()) throw new Exception("發生錯誤: 重算場次數");

        }

        /// <summary>
        /// 更新場地編號
        /// </summary>
        private void UpdateSiteCode(TConfig cfg, Item itmMeeting, Item itmProgram)
        {
            string program_id = itmProgram.getProperty("id", "");

            string sql = @"
				 UPDATE t1 SET
             	    t1.in_date_key = t3.in_date_key
             	    , t1.in_site = t4.id
             	    , t1.in_site_code = t4.in_code
             	    , t1.in_tree_state = 0
                 FROM
             	    IN_MEETING_PEVENT t1
                 INNER JOIN
                    IN_MEETING_PROGRAM t2
                    ON t2.id = t1.source_id
                 INNER JOIN
             	    IN_MEETING_ALLOCATION t3
             	    ON t3.in_program = t2.id
                 INNER JOIN
             	    IN_MEETING_SITE t4
             	    ON t4.id = t3.in_site
                 WHERE
                    t2.id = '{#program_id}'
            ";

            sql = sql.Replace("{#program_id}", program_id);

            cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// 更新組別賽制
        /// </summary>
        private void UpdateProgram(TConfig cfg, Item itmProgram)
        {
            string program_id = itmProgram.getProperty("id", "");

            string sql = "UPDATE IN_MEETING_PROGRAM SET"
                + "  in_battle_type = '" + cfg.battle_type + "'"
                + ", in_tiebreaker = ''"
                + ", in_challenge = ''"
                + " WHERE id = '" + program_id + "'";

            Item itmSQL = cfg.inn.applySQL(sql);

            if (itmSQL.isError())
            {
                throw new Exception("更新組別賽制發生錯誤");
            }
        }

        /// <summary>
        /// 清除場次資料
        /// </summary>
        private void RemoveEvents(TConfig cfg, Item itmProgram, bool is_delete_pteam = false)
        {
            string program_id = itmProgram.getProperty("id", "");

            Item itmData = cfg.inn.newItem("In_Meeting_Program");
            itmData.setProperty("program_id", program_id);
            itmData.apply("in_meeting_program_remove_all");
        }

        /// <summary>
        /// 修正隊伍量級序號
        /// </summary>
        private void FixTeamSectionNo(TConfig cfg, Item itmProgram)
        {
            string program_id = itmProgram.getProperty("id", "");

            string sql = "UPDATE IN_MEETING_PTEAM SET in_section_no = in_sign_no WHERE source_id = '" + program_id + "'";
            cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// 建立[格式組]場次
        /// </summary>
        private void AddKataEvents(TConfig cfg, Item itmProgram)
        {
            string in_meeting = itmProgram.getProperty("in_meeting", "");
            string program_id = itmProgram.getProperty("id", "");
            string in_fight_day = itmProgram.getProperty("in_fight_day", "");
            string in_site = itmProgram.getProperty("in_site", "");
            int team_count = GetIntVal(itmProgram.getProperty("in_team_count", "0"));

            int event_count = team_count;
            if (event_count <= 0) return;

            string tree_code = "K";
            string tree_name = "kata";
            int tree_sort = 700;
            int current_round = 1;

            for (int i = 1; i <= event_count; i++)
            {
                //回合序號
                int current_round_id = i;

                //樹圖序號(空間)
                string in_tree_id = GetTreeId(tree_code, current_round, current_round_id);

                TEvent evt = new TEvent
                {
                    in_meeting = in_meeting,
                    source_id = program_id,
                    in_date_key = in_fight_day,
                    in_site = in_site,

                    in_tree_name = tree_name,
                    in_tree_sort = tree_sort,
                    in_tree_id = in_tree_id,
                    in_tree_no = i.ToString(),

                    in_round = current_round,
                    in_round_code = event_count,
                    in_round_id = current_round_id,

                    in_sign_code = event_count,
                    in_sign_no = i,

                    in_detail_ns = i + ",-1",

                    in_robin_key = "kata",

                    in_period = 1,
                };

                ApplyEvent(cfg, evt);

            }
        }

        /// <summary>
        /// 新增比賽場次
        /// </summary>
        private void ApplyEvent(TConfig cfg, TEvent evt)
        {
            Item itmEvent = cfg.inn.newItem("In_Meeting_PEvent", "add");
            itmEvent.setProperty("in_meeting", evt.in_meeting);
            itmEvent.setProperty("source_id", evt.source_id);
            itmEvent.setProperty("in_date_key", evt.in_date_key);
            itmEvent.setProperty("in_site", evt.in_site);

            itmEvent.setProperty("in_tree_name", evt.in_tree_name);
            itmEvent.setProperty("in_tree_sort", evt.in_tree_sort.ToString());
            itmEvent.setProperty("in_tree_id", evt.in_tree_id);
            itmEvent.setProperty("in_tree_no", evt.in_tree_no);

            itmEvent.setProperty("in_round", evt.in_round.ToString());
            itmEvent.setProperty("in_round_code", evt.in_round_code.ToString());
            itmEvent.setProperty("in_round_id", evt.in_round_id.ToString());

            itmEvent.setProperty("in_sign_code", evt.in_sign_code.ToString());
            itmEvent.setProperty("in_sign_no", evt.in_sign_no.ToString());

            itmEvent.setProperty("in_detail_ns", evt.in_detail_ns);
            itmEvent.setProperty("in_robin_key", evt.in_robin_key);
            itmEvent.setProperty("in_period", evt.in_period.ToString());


            itmEvent = itmEvent.apply();

            if (itmEvent.isError())
            {
                throw new Exception("建立賽事組別場次發生失敗");
            }

            evt.id = itmEvent.getID();
        }

        /// <summary>
        /// 新增比賽分組
        /// </summary>
        private void AddKataEventDetails(TConfig cfg, Item itmMeeting, Item itmProgram)
        {
            string program_id = itmProgram.getProperty("id", "");

            Item itmEvents = cfg.inn.applySQL("SELECT * FROM IN_MEETING_PEVENT WITH(NOLOCK) WHERE source_id = '" + program_id + "' ");

            int count = itmEvents.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item itmEvent = itmEvents.getItemByIndex(i);

                string event_id = itmEvent.getProperty("id", "");
                string in_detail_ns = itmEvent.getProperty("in_detail_ns", "");

                string[] ns = in_detail_ns.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                if (ns == null || ns.Length <= 1)
                {
                    throw new Exception("對戰籤號清單錯誤");
                }

                for (int j = 1; j <= ns.Length; j++)
                {
                    string in_sign_no = "";
                    string in_sign_foot = "";
                    string in_target_no = "";
                    string in_target_foot = "";

                    if (j % 2 != 0)
                    {
                        in_sign_no = ns[0];
                        in_sign_foot = "1";
                        in_target_no = ns[1];
                        in_target_foot = "2";
                    }
                    else
                    {
                        in_sign_no = ns[1];
                        in_sign_foot = "2";
                        in_target_no = ns[0];
                        in_target_foot = "1";
                    }

                    Item itmDetail = cfg.inn.newItem("In_Meeting_PEvent_Detail", "add");
                    itmDetail.setProperty("source_id", event_id);
                    itmDetail.setProperty("in_sign_foot", in_sign_foot);
                    itmDetail.setProperty("in_sign_no", in_sign_no);

                    itmDetail.setProperty("in_target_no", in_target_no);
                    itmDetail.setProperty("in_target_foot", in_target_foot);
                    itmDetail = itmDetail.apply();
                }
            }
        }

        /// <summary>
        /// 取得樹圖序號
        /// </summary>
        private string GetTreeId(string code, int round, int id)
        {
            return code + round + id.ToString().PadLeft(2, '0');
        }

        //取得賽事資訊
        private Item GetMeeting(TConfig cfg)
        {
            string aml = ""
                + "<AML>"
                + "  <Item type='In_Meeting' action='get' id='{#meeting_id}' select='in_title,in_battle_type,in_surface_code'>"
                + "  </Item>"
                + "</AML>";

            aml = aml.Replace("{#meeting_id}", cfg.meeting_id);

            return cfg.inn.applyAML(aml);
        }

        //取得賽程組別資訊
        private Item GetProgram(TConfig cfg)
        {
            string sql = "SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + cfg.program_id + "'";
            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }

            public string meeting_id { get; set; }
            public string program_id { get; set; }
            public string battle_type { get; set; }
        }

        /// <summary>
        /// 場次資料結構
        /// </summary>
        private class TEvent
        {
            /// <summary>
            /// 賽程 id
            /// </summary>
            public string in_meeting { get; set; }

            /// <summary>
            /// 賽程組別 id
            /// </summary>
            public string source_id { get; set; }

            /// <summary>
            /// 賽程場次 id
            /// </summary>
            public string id { get; set; }

            /// <summary>
            /// 樹圖名稱
            /// </summary>
            public string in_tree_name { get; set; }

            /// <summary>
            /// 樹圖排序
            /// </summary>
            public int in_tree_sort { get; set; }

            /// <summary>
            /// 樹圖序號(空間)
            /// </summary>
            public string in_tree_id { get; set; }

            /// <summary>
            /// 樹圖序號(時間)
            /// </summary>
            public string in_tree_no { get; set; }

            /// <summary>
            /// 回合
            /// </summary>
            public int in_round { get; set; }

            /// <summary>
            /// 回合代碼
            /// </summary>
            public int in_round_code { get; set; }

            /// <summary>
            /// 回合序號
            /// </summary>
            public int in_round_id { get; set; }

            /// <summary>
            /// 場次籤數
            /// </summary>
            public int in_sign_code { get; set; }

            /// <summary>
            /// 場次籤號
            /// </summary>
            public int in_sign_no { get; set; }

            /// <summary>
            /// 對戰籤號清單
            /// </summary>
            public string in_detail_ns { get; set; }

            /// <summary>
            /// 循環賽鍵
            /// </summary>
            public string in_robin_key { get; set; }

            /// <summary>
            /// 比賽日期
            /// </summary>
            public string in_date_key { get; set; }

            /// <summary>
            /// 比賽場地 id
            /// </summary>
            public string in_site { get; set; }

            /// <summary>
            /// 賽別類型(1: 預賽、4: 決賽、8: 加賽)
            /// </summary>
            public int in_period { get; set; }
        }

        private int GetIntVal(string value, int def = 0)
        {
            int result = def;
            Int32.TryParse(value, out result);
            return result;
        }
    }
}