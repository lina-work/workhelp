﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Judo.Fight
{
    public class in_meeting_program_robin : Item
    {
        public in_meeting_program_robin(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 國際版對戰表
                日誌: 
                    - 2023-06-14: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "In_Meeting_Competition_NationExport";

            var itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                meeting_id = itmR.getProperty("meeting_id", ""),
                scene = itmR.getProperty("scene", ""),
            };

            switch (cfg.scene)
            {
                case "nation_draw_sheet"://國際版對戰表
                    ExportNationDrawSheet(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private List<TProgram> MapPrograms(TConfig cfg, Item itmPrograms)
        {
            var result = new List<TProgram>();
            var count = itmPrograms.getItemCount();
            for (int i = 0; i < count; i++)
            {
                var itmProgram = itmPrograms.getItemByIndex(i);
                var program = MapOneProgram(cfg, itmProgram);
                program.map = MapEventList(cfg, program);

                if (program.isRobin)
                {
                    program.players = MapPlayerList(cfg, program);
                }

                result.Add(program);
            }
            return result;
        }

        //國際版對戰表
        private void ExportNationDrawSheet(TConfig cfg, Item itmReturn)
        {
            SetExportInof(cfg, itmReturn);

            var itmPrograms = GetProgramItems(cfg, itmReturn);
            var programs = MapPrograms(cfg, itmPrograms);

            var book = new Spire.Xls.Workbook();
            book.LoadFromFile(cfg.template_path);

            var default_sheet_count = book.Worksheets.Count;

            for (int i = 0; i < programs.Count; i++)
            {
                var program = programs[i];
                ExportXls(cfg, program, book);
            }

            if (book.Worksheets.Count == default_sheet_count)
            {
                throw new Exception("查無資料");
            }

            //移除樣板 Sheet
            var mx = default_sheet_count - 1;
            for (int i = mx; i >= 0; i--)
            {
                book.Worksheets.RemoveAt(i);
            }

            var xlsName1 = cfg.mt_title + "_DrawSheet_Step1_" + DateTime.Now.ToString("MMdd_HHmmss");
            var xlsFile1 = cfg.export_path + xlsName1 + ".xlsx";
            book.SaveToFile(xlsFile1);

            //由於 OvalShapes CopyFrom 時期無法複製，因此增加中繼檔處理
            ResetSheetsForTreeNo(cfg, xlsFile1, programs, itmReturn);
        }

        private void ResetSheetsForTreeNo(TConfig cfg, string fileSource, List<TProgram> programs, Item itmReturn)
        {
            var book = new Spire.Xls.Workbook();
            book.LoadFromFile(fileSource);

            for (int i = 0; i < programs.Count; i++)
            {
                var program = programs[i];
                var sheet = book.Worksheets[program.in_short_name];
                if (sheet == null) continue;

                switch (program.sheetKey)
                {
                    case "2":
                        ResetPlayers(cfg, sheet, program, program.map);
                        break;

                    case "3":
                        ResetPlayers(cfg, sheet, program, program.map);
                        break;

                    case "3_Cross":
                        ResetPlayers(cfg, sheet, program, program.map);
                        break;

                    case "4":
                        ResetPlayers(cfg, sheet, program, program.map);
                        break;

                    case "4_Cross":
                        ResetPlayers(cfg, sheet, program, program.map);
                        break;

                    case "5":
                        ResetPlayers(cfg, sheet, program, program.map);
                        break;

                    case "5_Cross":
                        ResetPlayers(cfg, sheet, program, program.map);
                        break;

                    case "8":
                        ResetPlayers(cfg, sheet, program, program.map);
                        break;

                    case "16":
                        ResetPlayers(cfg, sheet, program, program.map);
                        break;

                    case "32":
                        break;
                }
            }

            // //適合頁面
            // book.ConverterSetting.SheetFitToPage = true;

            var xlsName2 = cfg.mt_title + "_DrawSheet_" + DateTime.Now.ToString("MMdd_HHmmss");
            var xlsFile2 = cfg.export_path + xlsName2 + "." + cfg.export_type;

            if (cfg.export_type == "pdf")
            {
                book.SaveToFile(xlsFile2, Spire.Xls.FileFormat.PDF);
            }
            else
            {
                book.SaveToFile(xlsFile2, Spire.Xls.ExcelVersion.Version2013);
            }

            itmReturn.setProperty("xls_name", xlsName2 + "." + cfg.export_type);
        }

        private void ExportXls(TConfig cfg, TProgram program, Spire.Xls.Workbook book)
        {
            if (program.map == null || program.map.Count == 0) return;

            var sheetTemplate = book.Worksheets[program.sheetKey];

            var sheet = book.CreateEmptySheet();
            sheet.CopyFrom(sheetTemplate);
            sheet.Name = program.in_short_name;

            // sheet.PageSetup.FitToPagesWide = 1;
            // sheet.PageSetup.FitToPagesTall = 1;
            // sheet.PageSetup.TopMargin = 0.3;
            // sheet.PageSetup.LeftMargin = 0.5;
            // sheet.PageSetup.RightMargin = 0.5;
            // sheet.PageSetup.BottomMargin = 0.5;

            switch (program.sheetKey)
            {
                case "3":
                    program.LeftColumnOffset = 50;
                    program.TopRowOffset = 50;
                    ExportPlayers3Robin(cfg, sheet, program, program.map);
                    break;

                case "3_Cross":
                    program.LeftColumnOffset = 50;
                    program.TopRowOffset = 50;
                    ExportPlayers3Robin(cfg, sheet, program, program.map);
                    break;

                case "4":
                    program.LeftColumnOffset = 50;
                    program.TopRowOffset = 50;
                    ExportPlayers4Robin(cfg, sheet, program, program.map);
                    break;

                case "4_Cross":
                    program.LeftColumnOffset = 150;
                    program.TopRowOffset = 100;
                    ExportPlayers4Cross(cfg, sheet, program, program.map);
                    break;

                case "5":
                    program.LeftColumnOffset = 50;
                    program.TopRowOffset = 50;
                    ExportPlayers5Robin(cfg, sheet, program, program.map);
                    break;

                case "5_Cross":
                    program.LeftColumnOffset = 50;
                    program.TopRowOffset = 50;
                    ExportPlayers5Cross(cfg, sheet, program, program.map);
                    break;

                case "8":
                    program.LeftColumnOffset = 50;
                    program.TopRowOffset = 100;
                    ExportPlayers08(cfg, sheet, program, program.map);
                    break;

                case "16":
                    program.LeftColumnOffset = 50;
                    program.TopRowOffset = 50;
                    ExportPlayers16(cfg, sheet, program, program.map);
                    break;

                case "32":
                    program.LeftColumnOffset = 50;
                    program.TopRowOffset = 25;
                    ExportPlayers32(cfg, sheet, program, program.map);
                    break;
            }
        }

        private void ExportPlayers3Robin(TConfig cfg, Spire.Xls.Worksheet sheet, TProgram program, Dictionary<string, TEvt> map)
        {
            var player1 = FindPlayer(cfg, program.players, "1");
            var player2 = FindPlayer(cfg, program.players, "2");
            var player3 = FindPlayer(cfg, program.players, "3");

            SetRobinBox(cfg, sheet, program, player1, new List<int> { 7, 15, 19 });
            SetRobinBox(cfg, sheet, program, player2, new List<int> { 8, 16, 23 });
            SetRobinBox(cfg, sheet, program, player3, new List<int> { 9, 20, 24 });
        }

        private void ExportPlayers4Robin(TConfig cfg, Spire.Xls.Worksheet sheet, TProgram program, Dictionary<string, TEvt> map)
        {
            var player1 = FindPlayer(cfg, program.players, "1");
            var player2 = FindPlayer(cfg, program.players, "2");
            var player3 = FindPlayer(cfg, program.players, "3");
            var player4 = FindPlayer(cfg, program.players, "4");

            SetRobinBox(cfg, sheet, program, player1, new List<int> { 7, 15, 23, 31 });
            SetRobinBox(cfg, sheet, program, player2, new List<int> { 8, 16, 27, 35 });
            SetRobinBox(cfg, sheet, program, player3, new List<int> { 9, 19, 24, 36 });
            SetRobinBox(cfg, sheet, program, player4, new List<int> { 10, 20, 28, 32 });
        }

        private void ExportPlayers5Robin(TConfig cfg, Spire.Xls.Worksheet sheet, TProgram program, Dictionary<string, TEvt> map)
        {
            var player1 = FindPlayer(cfg, program.players, "1");
            var player2 = FindPlayer(cfg, program.players, "2");
            var player3 = FindPlayer(cfg, program.players, "3");
            var player4 = FindPlayer(cfg, program.players, "4");
            var player5 = FindPlayer(cfg, program.players, "5");

            SetRobinBox(cfg, sheet, program, player1, new List<int> { 7, 15, 23, 35, 47 });
            SetRobinBox(cfg, sheet, program, player2, new List<int> { 8, 16, 27, 39, 51 });
            SetRobinBox(cfg, sheet, program, player3, new List<int> { 9, 19, 28, 36, 43 });
            SetRobinBox(cfg, sheet, program, player4, new List<int> { 10, 20, 31, 40, 48 });
            SetRobinBox(cfg, sheet, program, player5, new List<int> { 11, 24, 32, 44, 52 });
        }

        private void SetRobinBox(TConfig cfg, Spire.Xls.Worksheet sheet, TProgram program, TDtl player, List<int> list)
        {
            foreach (var ri in list)
            {
                sheet["E" + ri].Text = player.nm;
                sheet["F" + ri].Text = player.og;
                SetFlagPicture(cfg, sheet, program, ri, 4, player.og);
            }
        }

        private void ExportPlayers4Cross(TConfig cfg, Spire.Xls.Worksheet sheet, TProgram program, Dictionary<string, TEvt> map)
        {
            var player1 = FindPlayer(cfg, program.players, "1");
            var player2 = FindPlayer(cfg, program.players, "2");
            var player3 = FindPlayer(cfg, program.players, "3");
            var player4 = FindPlayer(cfg, program.players, "4");


            sheet["C6"].Text = player1.nm;
            sheet["D6"].Text = player1.og;
            SetFlagPicture(cfg, sheet, program, 6, 2, player1.og);

            sheet["C7"].Text = player3.nm;
            sheet["D7"].Text = player3.og;
            SetFlagPicture(cfg, sheet, program, 7, 2, player3.og);


            sheet["C10"].Text = player2.nm;
            sheet["D10"].Text = player2.og;
            SetFlagPicture(cfg, sheet, program, 10, 2, player2.og);

            sheet["C11"].Text = player4.nm;
            sheet["D11"].Text = player4.og;
            SetFlagPicture(cfg, sheet, program, 11, 2, player4.og);
        }

        private void ExportPlayers5Cross(TConfig cfg, Spire.Xls.Worksheet sheet, TProgram program, Dictionary<string, TEvt> map)
        {
            var player1 = FindPlayer(cfg, program.players, "1");
            var player2 = FindPlayer(cfg, program.players, "2");
            var player3 = FindPlayer(cfg, program.players, "3");
            var player4 = FindPlayer(cfg, program.players, "4");
            var player5 = FindPlayer(cfg, program.players, "5");

            // A Group
            sheet["E7"].Text = player1.nm;
            sheet["F7"].Text = player1.og;
            SetFlagPicture(cfg, sheet, program, 7, 4, player1.og);

            sheet["E8"].Text = player3.nm;
            sheet["F8"].Text = player3.og;
            SetFlagPicture(cfg, sheet, program, 8, 4, player3.og);

            sheet["E9"].Text = player5.nm;
            sheet["F9"].Text = player5.og;
            SetFlagPicture(cfg, sheet, program, 9, 4, player5.og);

            // B Group
            sheet["J12"].Text = player2.nm;
            sheet["O12"].Text = player2.og;
            SetFlagPicture(cfg, sheet, program, 12, 9, player2.og);

            sheet["J13"].Text = player4.nm;
            sheet["O13"].Text = player4.og;
            SetFlagPicture(cfg, sheet, program, 13, 9, player4.og);

            // Event 1: 1-3
            sheet["E16"].Text = player1.nm;
            sheet["F16"].Text = player1.og;
            SetFlagPicture(cfg, sheet, program, 16, 4, player1.og);
            sheet["E17"].Text = player3.nm;
            sheet["F17"].Text = player3.og;
            SetFlagPicture(cfg, sheet, program, 17, 4, player3.og);

            // Event 3: 1-5
            sheet["E20"].Text = player1.nm;
            sheet["F20"].Text = player1.og;
            SetFlagPicture(cfg, sheet, program, 20, 4, player1.og);
            sheet["E21"].Text = player5.nm;
            sheet["F21"].Text = player5.og;
            SetFlagPicture(cfg, sheet, program, 21, 4, player5.og);

            // Event 4: 3-5
            sheet["E24"].Text = player3.nm;
            sheet["E24"].Text = player3.og;
            SetFlagPicture(cfg, sheet, program, 24, 4, player3.og);
            sheet["E25"].Text = player5.nm;
            sheet["F25"].Text = player5.og;
            SetFlagPicture(cfg, sheet, program, 25, 4, player5.og);

            // Event 2: 2-4
            sheet["J28"].Text = player2.nm;
            sheet["O28"].Text = player2.og;
            SetFlagPicture(cfg, sheet, program, 28, 9, player2.og);
            sheet["J29"].Text = player4.nm;
            sheet["O29"].Text = player4.og;
            SetFlagPicture(cfg, sheet, program, 29, 9, player4.og);
        }

        private void ExportPlayers08(TConfig cfg, Spire.Xls.Worksheet sheet, TProgram program, Dictionary<string, TEvt> map)
        {
            var ridx = 6;
            for (var i = 1; i <= 4; i++)
            {
                var key = "M008-" + i.ToString().PadLeft(2, '0');
                var evt = FindEvt(cfg, map, key);

                var r1 = ridx + 0;
                var r2 = ridx + 1;

                sheet["C" + r1].Text = evt.f1.nm;
                sheet["D" + r1].Text = evt.f1.og;
                SetFlagPicture(cfg, sheet, program, r1, 2, evt.f1.og);

                sheet["C" + r2].Text = evt.f2.nm;
                sheet["D" + r2].Text = evt.f2.og;
                SetFlagPicture(cfg, sheet, program, r2, 2, evt.f2.og);

                ridx += 4;
            }
        }

        private void ExportPlayers16(TConfig cfg, Spire.Xls.Worksheet sheet, TProgram program, Dictionary<string, TEvt> map)
        {
            var ridx = 6;
            for (var i = 1; i <= 8; i++)
            {
                var key = "M016-" + i.ToString().PadLeft(2, '0');
                var evt = FindEvt(cfg, map, key);

                var r1 = ridx + 0;
                var r2 = ridx + 1;

                sheet["C" + r1].Text = evt.f1.nm;
                sheet["D" + r1].Text = evt.f1.og;
                SetFlagPicture(cfg, sheet, program, r1, 2, evt.f1.og);

                sheet["C" + r2].Text = evt.f2.nm;
                sheet["D" + r2].Text = evt.f2.og;
                SetFlagPicture(cfg, sheet, program, r2, 2, evt.f2.og);

                if (i % 2 == 0)
                {
                    ridx += 4;
                }
                else
                {
                    ridx += 2;
                }
            }
        }

        private void ExportPlayers32(TConfig cfg, Spire.Xls.Worksheet sheet, TProgram program, Dictionary<string, TEvt> map)
        {
            var ridx = 6;
            for (var i = 1; i <= 16; i++)
            {
                var key = "M032-" + i.ToString().PadLeft(2, '0');
                var evt = FindEvt(cfg, map, key);

                var r1 = ridx + 0;
                var r2 = ridx + 1;

                sheet["C" + r1].Text = evt.f1.nm;
                sheet["D" + r1].Text = evt.f1.og;
                SetFlagPicture(cfg, sheet, program, r1, 2, evt.f1.og);

                sheet["C" + r2].Text = evt.f2.nm;
                sheet["D" + r2].Text = evt.f2.og;
                SetFlagPicture(cfg, sheet, program, r2, 2, evt.f2.og);

                if (i % 4 == 0)
                {
                    ridx += 4;
                }
                else
                {
                    ridx += 2;
                }
            }
        }

        private void ResetPlayers(TConfig cfg, Spire.Xls.Worksheet sheet, TProgram program, Dictionary<string, TEvt> map)
        {
            //處理標題
            if (sheet.RectangleShapes != null && sheet.RectangleShapes.Count > 0)
            {
                var shapes = sheet.RectangleShapes;
                for (var i = 0; i < shapes.Count; i++)
                {
                    var shape = shapes[i];
                    var text = ((Spire.Xls.Core.IPrstGeomShape)shape).Text;
                    switch (text)
                    {
                        case "weight":
                            ((Spire.Xls.Core.IPrstGeomShape)shape).HtmlString = @"<Font Style=""FONT-WEIGHT: bold;FONT-FAMILY: Calibri;FONT-SIZE: 18pt;COLOR: #000000;TEXT-ALIGN: left;"">" + program.in_l3 + "</Font>";
                            break;
                        case "grade":
                            ((Spire.Xls.Core.IPrstGeomShape)shape).HtmlString = @"<Font Style=""FONT-WEIGHT: bold;FONT-FAMILY: Calibri;FONT-SIZE: 12pt;COLOR: #000000;TEXT-ALIGN: left;"">" + program.in_l1 + "s (" + program.in_team_count + ")</Font>";
                            break;
                    }
                }
            }
        }

        private void SetFlagPicture(TConfig cfg, Spire.Xls.Worksheet sheet, TProgram program, int topRow, int leftColumn, string og)
        {
            var fileName = @"C:\site\judo\Images\flagsXlsx\" + og + ".png";
            if (!System.IO.File.Exists(fileName)) return;

            var picture = sheet.Pictures.Add(topRow, leftColumn, fileName);
            picture.Width = 30;
            picture.Height = 16;
            picture.LeftColumnOffset = program.LeftColumnOffset;
            picture.TopRowOffset = program.TopRowOffset;
            picture.Line.ForeColor = System.Drawing.Color.Black;
        }

        private TEvt FindEvt(TConfig cfg, Dictionary<string, TEvt> map, string in_fight_id)
        {
            var result = default(TEvt);
            if (map.ContainsKey(in_fight_id))
            {
                result = map[in_fight_id];
            }
            else
            {
                result = new TEvt { Value = cfg.inn.newItem(), tno = "" };
            }

            if (result.f1 == null)
            {
                result.f1 = NewDetail(cfg, cfg.inn.newItem());
            }

            if (result.f2 == null)
            {
                result.f2 = NewDetail(cfg, cfg.inn.newItem());
            }

            return result;
        }

        private TDtl FindPlayer(TConfig cfg, Dictionary<string, TDtl> map, string in_sign_no)
        {
            if (map.ContainsKey(in_sign_no))
            {
                return map[in_sign_no];
            }
            else
            {
                return new TDtl
                {
                    no = in_sign_no,
                    sno = in_sign_no,
                    og = "",
                    nm = "",
                    Value = cfg.inn.newItem(),
                };
            }
        }

        private void SetExportInof(TConfig cfg, Item itmReturn)
        {
            var sql = @"SELECT in_title, in_battle_repechage, in_uniform_color FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'";
            cfg.itmMeeting = cfg.inn.applySQL(sql);
            if (cfg.itmMeeting.isError() || cfg.itmMeeting.getResult() == "")
            {
                throw new Exception("找無該場賽事資料!");
            }

            cfg.mt_title = cfg.itmMeeting.getProperty("in_title", "");
            cfg.mt_battle_repechage = cfg.itmMeeting.getProperty("in_battle_repechage", "").ToLower();
            cfg.in_uniform_color = cfg.itmMeeting.getProperty("in_uniform_color", "");

            var target_name = "competition_nation";
            var itmXls = cfg.inn.applyMethod("In_Meeting_Excel_Path", "<in_name>" + target_name + "</in_name>");
            cfg.export_path = itmXls.getProperty("export_path", "");
            cfg.template_path = itmXls.getProperty("template_path", "");
            if (cfg.template_path == "")
            {
                throw new Exception("查無報表樣板!");
            }

            //道服顏色資訊
            cfg.itmColor = cfg.inn.newItem("In_Meeting");
            cfg.itmColor.setProperty("in_uniform_color", cfg.in_uniform_color);
            cfg.itmColor = cfg.itmColor.apply("in_meeting_uniform_color");

            cfg.export_type = itmReturn.getProperty("export_type", "").ToLower();
        }

        private Item GetProgramItems(TConfig cfg, Item itmReturn)
        {
            var program_id = itmReturn.getProperty("program_id", "");

            var sql = "SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + program_id + "'";

            //var sql = "SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK)"
            //    + " WHERE in_meeting = '" + cfg.meeting_id + "'"
            //    + " AND in_team_count IN (32, 16, 8, 5, 4) ORDER BY in_team_count DESC";

            var items = cfg.inn.applySQL(sql);

            if (items.isError() || items.getResult() == "")
            {
                throw new Exception("查無組別資料!");
            }

            return items;
        }

        private void CopyItemValue(Item itmA, Item itmB, string propA, string propB = "")
        {
            if (propB == "")
            {
                itmA.setProperty(propA, itmB.getProperty(propA, ""));
            }
            else
            {
                itmA.setProperty(propA, itmB.getProperty(propB, ""));
            }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }

            public string meeting_id { get; set; }
            public string scene { get; set; }

            public Item itmMeeting { get; set; }
            public Item itmColor { get; set; }


            public string mt_title { get; set; }
            public string mt_battle_repechage { get; set; }
            public string in_uniform_color { get; set; }

            public string export_path { get; set; }
            public string template_path { get; set; }
            public string export_type { get; set; }
        }

        private class TProgram
        {
            public string id { get; set; }
            public string in_name { get; set; }
            public string in_short_name { get; set; }
            public string in_weight { get; set; }
            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }
            public string in_fight_day { get; set; }
            public string in_battle_type { get; set; }
            public string in_round_code { get; set; }
            public string in_team_count { get; set; }
            public string in_sign_time { get; set; }

            public string in_place { get; set; }

            public Item Value { get; set; }

            public string sheetKey { get; set; }
            public int teamCount { get; set; }
            public bool isRobin { get; set; }
            public string signTime { get; set; }

            public Dictionary<string, TEvt> map { get; set; }
            public Dictionary<string, TDtl> players { get; set; }

            public int LeftColumnOffset { get; set; }
            public int TopRowOffset { get; set; }

        }

        private TProgram MapOneProgram(TConfig cfg, Item itmProgram)
        {
            var result = new TProgram
            {
                id = itmProgram.getProperty("id", ""),
                in_name = itmProgram.getProperty("in_name", ""),
                in_short_name = itmProgram.getProperty("in_short_name", ""),
                in_weight = itmProgram.getProperty("in_weight", ""),
                in_l1 = itmProgram.getProperty("in_l1", ""),
                in_l2 = itmProgram.getProperty("in_l2", ""),
                in_l3 = itmProgram.getProperty("in_l3", ""),
                in_fight_day = itmProgram.getProperty("in_fight_day", ""),
                in_battle_type = itmProgram.getProperty("in_battle_type", ""),
                in_round_code = itmProgram.getProperty("in_round_code", "0"),
                in_team_count = itmProgram.getProperty("in_team_count", "0"),
                in_sign_time = itmProgram.getProperty("in_sign_time", ""),
                Value = itmProgram,
            };

            result.in_place = GetProgramPlace(cfg, result.id);
            if (result.in_short_name == "")
            {
                result.in_short_name = result.id;
            }

            result.teamCount = GetInt(result.in_team_count);
            result.signTime = GetDtmStr(result.in_sign_time, 8, "dd.MM.yyyy");

            var btype = result.in_battle_type;

            if (btype.Contains("SingleRoundRobin"))
            {
                result.isRobin = true;
                result.sheetKey = result.in_team_count;
            }
            else if (btype == "Cross")
            {
                result.isRobin = true;
                result.sheetKey = result.in_team_count + "_Cross";
            }
            else
            {
                result.sheetKey = result.in_round_code;
            }

            return result;
        }

        private string GetProgramPlace(TConfig cfg, string program_id)
        {
            var item = cfg.inn.applySQL("SELECT TOP 1 in_place FROM IN_MEETING_ALLOCATION WITH(NOLOCK) WHERE in_program = '" + program_id + "'");
            if (item.isError() || item.getResult() == "")
            {
                return "";
            }
            else
            {
                return item.getProperty("in_place", "");
            }
        }

        private class TEvt
        {
            public string tno { get; set; }
            public TDtl f1 { get; set; }
            public TDtl f2 { get; set; }
            public Item Value { get; set; }
        }

        private class TDtl
        {
            public string no { get; set; }
            public string og { get; set; }
            public string nm { get; set; }
            public string sno { get; set; }
            public Item Value { get; set; }
        }

        private Dictionary<string, TEvt> MapEventList(TConfig cfg, TProgram program)
        {
            var result = new Dictionary<string, TEvt>();
            var itmDetails = GetMPEventDetails(cfg, program);
            var count = itmDetails.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var itmDetail = itmDetails.getItemByIndex(i);
                var in_fight_id = itmDetail.getProperty("in_fight_id", "");
                var in_tree_no = itmDetail.getProperty("in_tree_no", "");
                var in_sign_foot = itmDetail.getProperty("in_sign_foot", "");
                if (in_fight_id == "") continue;

                var evt = default(TEvt);
                if (result.ContainsKey(in_fight_id))
                {
                    evt = result[in_fight_id];
                }
                else
                {
                    evt = new TEvt
                    {
                        tno = in_tree_no,
                        Value = itmDetail,
                    };
                    result.Add(in_fight_id, evt);
                }

                if (in_sign_foot == "1")
                {
                    evt.f1 = NewDetail(cfg, itmDetail);
                }
                else if (in_sign_foot == "2")
                {
                    evt.f2 = NewDetail(cfg, itmDetail);
                }
            }
            return result;
        }

        private TDtl NewDetail(TConfig cfg, Item itmDetail)
        {
            var result = new TDtl
            {
                no = itmDetail.getProperty("in_section_no", ""),
                og = itmDetail.getProperty("map_short_org", ""),
                nm = itmDetail.getProperty("in_name", ""),
                Value = itmDetail,
            };

            return result;
        }

        private Item GetMPEventDetails(TConfig cfg, TProgram program)
        {
            string sql = @"
                SELECT
                    t1.id
                    , t1.in_tree_name
                    , t1.in_tree_id
                    , t1.in_fight_id
                    , t1.in_tree_no
                    , t1.in_win_time
                    , t1.in_win_sign_no
                    , t1.in_win_local_time
                    , t2.in_sign_foot
                    , t2.in_sign_no
                    , t2.in_sign_bypass
                    , t2.in_points
                    , t2.in_correct_count
                    , t2.in_status
                    , t3.in_name AS 'site_name'
                    , t3.in_code AS 'site_code'
                    , t11.in_section_no
                    , t11.in_name
                    , t11.in_team
                    , t11.map_short_org
                    , t11.in_sign_time
                    , t11.in_weight_value
                    , t11.in_weight_message
                    , t11.in_final_rank
                    , t11.in_final_count
                    , t11.in_final_points
                FROM
                    IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
                    IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK)
                    ON t2.source_id = t1.id
                LEFT OUTER JOIN
                    IN_MEETING_SITE t3 WITH(NOLOCK)
                    ON t3.id = t1.in_site
                LEFT OUTER JOIN
                    VU_MEETING_PTEAM  t11 WITH(NOLOCK)
                    ON t11.source_id = t1.source_id
                    AND t11.in_sign_no = t2.in_sign_no
                WHERE
                    t1.source_id = '{#program_id}'
                ORDER BY
                      t1.in_tree_sort
                    , t1.in_tree_id
                    , t1.in_tree_no
                    , t2.in_sign_foot
            ";

            sql = sql.Replace("{#program_id}", program.id);

            return cfg.inn.applySQL(sql);
        }

        //分組交叉參數判斷
        private bool NeedCrossTop(TConfig cfg, string in_key)
        {
            var sql = "SELECT TOP 1 in_value FROM IN_MEETING_VARIABLE WITH(NOLOCK) WHERE source_id = '" + cfg.meeting_id + "' AND in_key = '" + in_key + "'";
            var itmV = cfg.inn.applySQL(sql);
            if (itmV.isError() || itmV.getResult() == "")
            {
                return false;
            }
            else
            {
                return itmV.getProperty("in_value", "") == "cross";
            }
        }

        private Dictionary<string, TDtl> MapPlayerList(TConfig cfg, TProgram program)
        {
            var result = new Dictionary<string, TDtl>();
            var items = GetPlayerItems(cfg, program);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var in_sign_no = item.getProperty("in_sign_no", "");
                var in_judo_no = item.getProperty("in_judo_no", "");
                var in_section_no = item.getProperty("in_section_no", "");
                var in_short_org = item.getProperty("in_short_org", "");
                var in_names = item.getProperty("in_names", "");

                if (!result.ContainsKey(in_sign_no))
                {
                    result.Add(in_sign_no, new TDtl
                    {
                        no = in_sign_no,
                        sno = in_judo_no,
                        og = in_short_org,
                        nm = in_names,
                        Value = item
                    });
                }
            }
            return result;
        }

        private Item GetPlayerItems(TConfig cfg, TProgram program)
        {
            string sql = @"
                SELECT
	                in_sign_no
	                , in_judo_no
                    , in_section_no
	                , in_short_org
	                , in_names
                FROM
	                IN_MEETING_PTEAM WITH(NOLOCK)
                WHERE
	                source_id = '{#program_id}'
	                AND ISNULL(in_sign_no, '') NOT IN ('', '0')
                ORDER BY
	                in_sign_no

            ";

            sql = sql.Replace("{#program_id}", program.id);

            return cfg.inn.applySQL(sql);
        }

        private string GetDtmStr(string value, int hours, string format)
        {
            if (value == "") return "";

            var result = GetDtm(value, hours);
            if (result == DateTime.MinValue) return "";

            return result.ToString(format);
        }

        private DateTime GetDtm(string value, int hours)
        {
            if (value == "") return DateTime.MinValue;

            DateTime result = DateTime.MinValue;
            if (DateTime.TryParse(value, out result))
            {
                return result.AddHours(hours);
            }
            return DateTime.MinValue;
        }

        private int GetInt(string value, int defV = 0)
        {
            if (value == "") return 0;
            int result = defV;
            if (int.TryParse(value, out result))
            {
                return result;
            }
            return defV;
        }
    }
}