﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Judo.Fight
{
    public class in_meeting_pevent_sub : Item
    {
        public in_meeting_pevent_sub(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 建立子場次
                日期: 
                    - 2021-10-11 創建 (Lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]in_meeting_pevent_sub";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, this.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                InnH = _InnH,

                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                program_id = itmR.getProperty("program_id", ""),
                event_id = itmR.getProperty("event_id", ""),
                scene = itmR.getProperty("scene", ""),
            };

            cfg.scene = "fix_represent";
            cfg.meeting_id = "3C26E884604A457A9CFFB30450EBF7CF";

            switch (cfg.scene)
            {
                case "fix_represent"://修復代表戰
                    FixRepresent(cfg, itmR);
                    break;

                case "remove":
                    RemovePEvent(cfg, itmR);
                    break;

                default:
                    CreateSubEvent(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void FixRepresent(TConfig cfg, Item itmReturn)
        {
            string sql = @"
                SELECT
	                *
                FROM
	                IN_MEETING_PROGRAM WITH(NOLOCK)
                WHERE
	                in_meeting = '{#meeting_id}'
	                AND in_l1 = '團體組'
	                AND in_team_count > 0
                ORDER BY 
	                in_sort_order
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            Item itmPrograms = cfg.inn.applySQL(sql);
            int count = itmPrograms.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item itmProgram = itmPrograms.getItemByIndex(i);
                FixRepresentProgram(cfg, itmProgram);
            }
        }

        private void FixRepresentProgram(TConfig cfg, Item itmProgram)
        {
            string program_id = itmProgram.getProperty("id", "");
            string in_sub_event = itmProgram.getProperty("in_sub_event", "0");
            int sub_event = GetIntVal(in_sub_event);

            if (sub_event <= 0)
            {
                throw new Exception("量級數量異常 #1");
            }

            var lstSect = GetMeetingSection(cfg, program_id);
            if (sub_event > lstSect.Count)
            {
                throw new Exception("量級數量異常 #2");
            }

            string sql = "SELECT * FROM IN_MEETING_PEVENT WITH(NOLOCK)"
                + " WHERE source_id = '" + program_id + "'"
                + " AND ISNULL(in_tree_no, 0) > 0"
                + " AND ISNULL(in_type, '') = 'p'"
                + " ORDER BY in_tree_sort, in_tree_id";

            Item itmEvents = cfg.inn.applySQL(sql);
            int count = itmEvents.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item itmEvent = itmEvents.getItemByIndex(i);
                TEvent evt = GetEvent(cfg, itmProgram, itmEvent, sub_event, lstSect);

                //建立代表戰
                GenerateEndEvent(cfg, evt);
            }
        }

        private void RemovePEvent(TConfig cfg, Item itmReturn)
        {
            string sql = "";
            Item itmDelete = null;

            sql = "DELETE FROM IN_MEETING_PEVENT_DETAIL WHERE source_id = '" + cfg.event_id + "'";
            itmDelete = cfg.inn.applySQL(sql);
            if (itmDelete.isError())
            {
                throw new Exception("刪除場次明細失敗");
            }

            sql = "DELETE FROM IN_MEETING_PEVENT WHERE id = '" + cfg.event_id + "'";
            itmDelete = cfg.inn.applySQL(sql);
            if (itmDelete.isError())
            {
                throw new Exception("刪除場次失敗");
            }
        }

        private void CreateSubEvent(TConfig cfg, Item itmReturn)
        {
            string sql = "";

            sql = "SELECT in_l1, in_sub_event FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + cfg.program_id + "'";
            Item itmProgram = cfg.inn.applySQL(sql);

            if (itmProgram.isError())
            {
                throw new Exception("賽程組別發生錯誤");
            }

            string in_l1 = itmProgram.getProperty("in_l1", "");
            if (in_l1 != "團體組")
            {
                return;
            }

            int sub_event = GetIntVal(itmProgram.getProperty("in_sub_event", "0"));
            if (sub_event <= 0)
            {
                return;
                //throw new Exception("賽程組別 子場次數量 發生錯誤");
            }

            var lstSect = GetMeetingSection(cfg, cfg.program_id);

            if (sub_event > lstSect.Count)
            {
                return;
                //throw new Exception("量級數量異常");
            }

            if (cfg.event_id != "")
            {
                sql = "SELECT * FROM IN_MEETING_PEVENT WITH(NOLOCK) WHERE id = '" + cfg.event_id + "'"
                    + " ORDER BY in_tree_sort, in_tree_id";
            }
            else
            {
                sql = "SELECT * FROM IN_MEETING_PEVENT WITH(NOLOCK) WHERE source_id = '" + cfg.program_id + "'"
                    + " ORDER BY in_tree_sort, in_tree_id";
            }

            Item itmEvents = cfg.inn.applySQL(sql);

            int count = itmEvents.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item itmEvent = itmEvents.getItemByIndex(i);

                TEvent evt = GetEvent(cfg, itmProgram, itmEvent, sub_event, lstSect);

                //建立子場次
                GenerateSub(cfg, evt);

                //建立代表戰
                GenerateEndEvent(cfg, evt);

                //更新主場次資料
                string sql_upd = "UPDATE IN_MEETING_PEVENT SET in_type = 'p' WHERE id = '" + itmEvent.getProperty("id", "") + "'";
                cfg.inn.applySQL(sql_upd);
            }
        }

        //建立子場次
        private void GenerateSub(TConfig cfg, TEvent evt)
        {
            for (int i = 0; i < evt.sub_event; i++)
            {
                int no = i + 1;
                string sno = no.ToString();

                string s_tree_id = evt.in_tree_id + "-" + sno.PadLeft(2, '0');
                int s_tree_no = evt.base_no + no;

                Item item = cfg.inn.newItem("In_Meeting_PEvent", "merge");
                item.setAttribute("where", "in_parent='" + evt.id + "' AND in_tree_id = '" + s_tree_id + "'");

                item.setProperty("in_meeting", cfg.meeting_id);
                item.setProperty("source_id", evt.program_id);
                item.setProperty("in_parent", evt.id);

                item.setProperty("in_date_key", evt.in_date_key);
                item.setProperty("in_period", evt.in_period);
                item.setProperty("in_site", evt.in_site);
                item.setProperty("in_site_code", evt.in_site_code);

                item.setProperty("in_tree_name", "sub");
                item.setProperty("in_tree_sort", "9000");
                item.setProperty("in_tree_id", s_tree_id);
                item.setProperty("in_tree_no", s_tree_no.ToString());
                item.setProperty("in_tree_rank", "");
                item.setProperty("in_tree_rank_ns", "");
                item.setProperty("in_tree_rank_nss", "");
                item.setProperty("in_tree_alias", "第" + sno + "場");

                item.setProperty("in_round", evt.in_round);
                item.setProperty("in_round_id", sno.ToString());
                item.setProperty("in_round_code", evt.sub_event.ToString());

                item.setProperty("in_sign_code", "");
                item.setProperty("in_sign_no", "");

                item.setProperty("in_surface_id", "0");
                item.setProperty("in_surface_name", "");

                item.setProperty("in_type", "s");
                item.setProperty("in_sub_id", sno);
                item.setProperty("in_sub_sect", evt.lstSect[i].getProperty("in_name", ""));

                item = item.apply();

                if (item.isError())
                {
                    throw new Exception("建立單場賽事組別場次發生失敗");
                }

                AppendDetail(cfg, evt, item, no, "1");
                AppendDetail(cfg, evt, item, no, "2");
            }
        }

        //建立代表戰
        private void GenerateEndEvent(TConfig cfg, TEvent evt)
        {
            //if (evt.sub_event % 2 > 0) return;

            int no = evt.sub_event + 1;
            string sno = no.ToString();

            string s_tree_id = evt.in_tree_id + "-" + sno.PadLeft(2, '0');
            int s_tree_no = evt.base_no + no;

            Item item = cfg.inn.newItem("In_Meeting_PEvent", "merge");
            item.setAttribute("where", "in_parent='" + evt.id + "' AND in_tree_id = '" + s_tree_id + "'");

            item.setProperty("in_meeting", cfg.meeting_id);
            item.setProperty("source_id", evt.program_id);
            item.setProperty("in_parent", evt.id);

            item.setProperty("in_date_key", evt.in_date_key);
            item.setProperty("in_period", evt.in_period);
            item.setProperty("in_site", evt.in_site);
            item.setProperty("in_site_code", evt.in_site_code);

            item.setProperty("in_tree_name", "sub");
            item.setProperty("in_tree_sort", "9000");
            item.setProperty("in_tree_id", s_tree_id);
            item.setProperty("in_tree_no", s_tree_no.ToString());
            item.setProperty("in_tree_rank", "");
            item.setProperty("in_tree_rank_ns", "");
            item.setProperty("in_tree_rank_nss", "");
            item.setProperty("in_tree_alias", "代表戰");

            item.setProperty("in_round", evt.in_round);
            item.setProperty("in_round_id", sno.ToString());
            item.setProperty("in_round_code", evt.sub_event.ToString());

            item.setProperty("in_sign_code", "");
            item.setProperty("in_sign_no", "");

            item.setProperty("in_surface_id", "0");
            item.setProperty("in_surface_name", "");

            item.setProperty("in_type", "s");
            item.setProperty("in_sub_id", sno);
            item.setProperty("in_sub_sect", "代表戰");

            item.setProperty("in_team_serial", sno);

            item = item.apply();

            if (item.isError())
            {
                throw new Exception("建立單場賽事組別場次發生失敗");
            }

            AppendDetail(cfg, evt, item, no, "1");
            AppendDetail(cfg, evt, item, no, "2");

        }

        private void AppendDetail(TConfig cfg, TEvent evt, Item itmSub, int sub_no, string foot)
        {
            string event_id = itmSub.getProperty("id", "");
            string name = "第" + sub_no + "位";
            string target_foot = foot == "1" ? "2" : "1";

            Item itmDetail = cfg.inn.newItem("In_Meeting_PEvent_Detail", "merge");
            itmDetail.setAttribute("where", "source_id='" + event_id + "' AND in_sign_foot = '" + foot + "'");

            itmDetail.setProperty("source_id", event_id);
            itmDetail.setProperty("in_sign_foot", foot);
            itmDetail.setProperty("in_target_foot", target_foot);
            itmDetail.setProperty("in_player_name", name);

            //int sign_no = 0;

            //if (evt.is_main_round_1)
            //{
            //    sign_no = foot == "1"
            //        ? evt.left_base_sign + sub_no
            //        : evt.right_base_sign + sub_no;
            //}
            //else
            //{
            //    sign_no = foot == "1"
            //        ? (10 + sub_no) * -1
            //        : (20 + sub_no) * -1;
            //}

            //if (sign_no != 0)
            //{
            //    itmDetail.setProperty("in_sign_no", sign_no.ToString());
            //}

            itmDetail = itmDetail.apply();
        }

        private TEvent GetEvent(TConfig cfg, Item itmProgram, Item itmEvent, int sub_event, List<Item> lstSect)
        {
            var evt = new TEvent
            {
                id = itmEvent.getProperty("id", ""),
                program_id = itmEvent.getProperty("source_id", ""),
                in_tree_name = itmEvent.getProperty("in_tree_name", ""),
                in_tree_id = itmEvent.getProperty("in_tree_id", ""),
                in_tree_no = itmEvent.getProperty("in_tree_no", ""),
                in_round = itmEvent.getProperty("in_round", ""),
                in_round_code = itmEvent.getProperty("in_round_code", ""),
                left_base_sign = 0,
                right_base_sign = 0,
                is_main_round_1 = false,
                in_date_key = itmEvent.getProperty("in_date_key", ""),
                in_period = itmEvent.getProperty("in_period", ""),
                in_site = itmEvent.getProperty("in_site", ""),
                in_site_code = itmEvent.getProperty("in_site", ""),
            };

            evt.sub_event = sub_event;
            evt.tree_no = GetIntVal(evt.in_tree_no);
            evt.base_no = evt.tree_no * 100;

            evt.itmProgram = itmProgram;
            evt.Value = itmEvent;
            evt.lstSect = lstSect;

            //if (evt.in_tree_name == "main" && evt.in_round == "1")
            //{
            //    evt.is_main_round_1 = true;
            //    string sql = "SELECT * FROM IN_MEETING_PEVENT_DETAIL WITH(NOLOCK) WHERE source_id = '" + evt.id + "'";
            //    Item itmDetails = cfg.inn.applySQL(sql);
            //    int count = itmDetails.getItemCount();
            //    for (int i = 0; i < count; i++)
            //    {
            //        Item itmDetail = itmDetails.getItemByIndex(i);
            //        string in_sign_foot = itmDetail.getProperty("in_sign_foot", "");
            //        string in_sign_no = itmDetail.getProperty("in_sign_no", "0");
            //        int sign_no = GetIntVal(in_sign_no);
            //        if (sign_no <= 0)
            //        {
            //            continue;
            //        }

            //        if (in_sign_foot == "1")
            //        {
            //            evt.left_base_sign = sign_no * 100;
            //        }
            //        else if (in_sign_foot == "2")
            //        {
            //            evt.right_base_sign = sign_no * 100;
            //        }
            //    }
            //}

            return evt;
        }

        //取得團體賽量級清單
        private List<Item> GetMeetingSection(TConfig cfg, string program_id)
        {
            List<Item> result = new List<Item>();

            string sql = "SELECT * FROM IN_MEETING_PSECT WITH(NOLOCK)"
                + " WHERE in_program = '" + program_id + "'"
                + " ORDER BY in_sub_id";

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                result.Add(items.getItemByIndex(i));
            }
            return result;
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public Innosoft.InnovatorHelper InnH { get; set; }

            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }

            public string meeting_id { get; set; }
            public string program_id { get; set; }
            public string event_id { get; set; }
            public string scene { get; set; }
        }

        private class TEvent
        {
            public string id { get; set; }
            public string program_id { get; set; }
            public string in_tree_name { get; set; }
            public string in_tree_id { get; set; }
            public string in_tree_no { get; set; }
            public string in_round { get; set; }
            public string in_round_code { get; set; }

            public string in_date_key { get; set; }
            public string in_period { get; set; }
            public string in_site { get; set; }
            public string in_site_code { get; set; }
            
            public int sub_event { get; set; }
            public int tree_no { get; set; }
            public int base_no { get; set; }

            public bool is_main_round_1 { get; set; }
            public int left_base_sign { get; set; }
            public int right_base_sign { get; set; }

            public Item Value { get; set; }
            public Item itmProgram { get; set; }

            public List<Item> lstSect { get; set; }
        }

        private int GetIntVal(string value, int def = 0)
        {
            int result = def;
            Int32.TryParse(value, out result);
            return result;
        }
    }
}