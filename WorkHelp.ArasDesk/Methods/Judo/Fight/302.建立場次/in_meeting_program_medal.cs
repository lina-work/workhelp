﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Judo.Fight
{
    public class in_meeting_program_medal : Item
    {
        public in_meeting_program_medal(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 重算獎牌戰類型、賽別時程、RankCount
                日誌: 
                    - 2022-05-18: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]in_meeting_program_medal";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                program_id = itmR.getProperty("program_id", ""),
                fight_day = itmR.getProperty("in_date", ""),
            };

            if (cfg.fight_day == "")
            {
                cfg.fight_day = itmR.getProperty("in_fight_day", "");
            }

            //cfg.meeting_id = "23FEFC6A2DE04C33A0A5513D61478FBA";

            cfg.itmMeeting = cfg.inn.applySQL("SELECT id, in_battle_type FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            if (cfg.itmMeeting.isEmpty() || cfg.itmMeeting.getResult() == "")
            {
                throw new Exception("查無賽事資料");
            }

            cfg.mt_battle_type = cfg.itmMeeting.getProperty("in_battle_type", "");

            //重算出賽人數
            ResetRankCount(cfg);

            //取得組別清單
            var rows = GetPrograms(cfg);

            foreach (var row in rows)
            {
                //重算相關參數
                ResetMedal(cfg, row);
            }

            return this;
        }

        //重算相關參數
        private void ResetMedal(TConfig cfg, TProgram row)
        {
            switch (row.in_battle_type)
            {
                case "Format":
                    break;
                case "SingleRoundRobin"://循環戰
                    SingleRoundRobin(cfg, row);
                    break;
                case "GroupSRoundRobin":
                    break;
                case "TopTwo"://單淘賽
                    TopTwo(cfg, row);
                    break;
                case "JudoTopFour"://四柱復活賽
                    JudoTopFour(cfg, row);
                    break;
                case "Challenge"://四柱復活挑戰賽
                    Challenge(cfg, row);
                    break;
            }
        }

        //循環戰
        private void SingleRoundRobin(TConfig cfg, TProgram row)
        {
            if (row.team_count == 2)
            {
                if (cfg.mt_battle_type == "JudoTopFour")
                {
                    //三戰兩勝-第 1 場
                    UpdateEventRobin(cfg, row, new TEvt { tree = TreeEnum.main, treeid = "M101", period = PeriodEnum.final, medal = MedalEnum.copper });
                    //三戰兩勝-第 2 場
                    UpdateEventRobin(cfg, row, new TEvt { tree = TreeEnum.main, treeid = "M102", period = PeriodEnum.final, medal = MedalEnum.golden });
                    //三戰兩勝-第 3 場
                    UpdateEventRobin(cfg, row, new TEvt { tree = TreeEnum.main, treeid = "M103", period = PeriodEnum.more, medal = MedalEnum.golden });
                }
                else if (cfg.mt_battle_type == "Challenge")
                {
                    //三戰兩勝-第 1 場
                    UpdateEventRobin(cfg, row, new TEvt { tree = TreeEnum.main, treeid = "M101", period = PeriodEnum.final, medal = MedalEnum.challenge1 });
                    //三戰兩勝-第 2 場
                    UpdateEventRobin(cfg, row, new TEvt { tree = TreeEnum.main, treeid = "M102", period = PeriodEnum.final, medal = MedalEnum.challenge2 });
                    //三戰兩勝-第 3 場
                    UpdateEventRobin(cfg, row, new TEvt { tree = TreeEnum.main, treeid = "M103", period = PeriodEnum.more, medal = MedalEnum.challenge3 });

                    // - 將[盟主挑戰1]變為[決賽]，獎牌戰 = 1 vs 1
                    UpdateEvent(cfg, row, new TEvt { tree = TreeEnum.ka01, round = "2", period = PeriodEnum.final, medal = MedalEnum.lord1 });
                    // - 將[盟主挑戰2]變為[決賽]，獎牌戰 = 1 vs 1
                    UpdateEvent(cfg, row, new TEvt { tree = TreeEnum.kb01, round = "2", period = PeriodEnum.final, medal = MedalEnum.lord2 });
                }
            }
            else
            {
                if (cfg.mt_battle_type == "Challenge")
                {
                    // - 將[盟主挑戰1]變為[決賽]，獎牌戰 = 1 vs 1
                    UpdateEvent(cfg, row, new TEvt { tree = TreeEnum.ka01, round = "2", period = PeriodEnum.final, medal = MedalEnum.lord1 });
                    // - 將[盟主挑戰2]變為[決賽]，獎牌戰 = 1 vs 1
                    UpdateEvent(cfg, row, new TEvt { tree = TreeEnum.kb01, round = "2", period = PeriodEnum.final, medal = MedalEnum.lord2 });
                }
                else
                {
                    UpdateEventRobin(cfg, row, new TEvt { tree = TreeEnum.main, treeid = "", period = PeriodEnum.pre, medal = MedalEnum.none });
                }
            }
        }

        //單淘賽
        private void TopTwo(TConfig cfg, TProgram row)
        {
            // - 將[銅牌戰]變為[預賽]，獎牌戰 = 無
            UpdateEvent(cfg, row, new TEvt { tree = TreeEnum.rpc, round = "4", period = PeriodEnum.pre, medal = MedalEnum.none });
            // - 將[34名戰]變為[預賽]，獎牌戰 = 無
            UpdateEvent(cfg, row, new TEvt { tree = TreeEnum.rpc, round = "2", period = PeriodEnum.pre, medal = MedalEnum.none });
            // - 將[金牌戰]變為[預賽]，獎牌戰 = 無
            UpdateEvent(cfg, row, new TEvt { tree = TreeEnum.main, round = "2", period = PeriodEnum.pre, medal = MedalEnum.none });
        }

        //四柱復活賽
        private void JudoTopFour(TConfig cfg, TProgram row)
        {
            if (row.rank_count < 5)
            {

            }
            else if (row.rank_count == 5)
            {
                //五取三，要打三四名
                // - 將[銅牌戰]變為[預賽]，獎牌戰 = 無
                UpdateEvent(cfg, row, new TEvt { tree = TreeEnum.rpc, round = "4", period = PeriodEnum.pre, medal = MedalEnum.none });
                // - 將[34名戰]變為[決賽]，獎牌戰 = 銅牌
                UpdateEvent(cfg, row, new TEvt { tree = TreeEnum.rpc, round = "2", period = PeriodEnum.final, medal = MedalEnum.copper });
            }
            else if (row.rank_count == 7)
            {
                //七取五，要打五六名
                // - 將[56名戰]變為[決賽]，獎牌戰 = 銅牌 (需等銅牌戰打完，最後一場)
                UpdateEvent(cfg, row, new TEvt { tree = TreeEnum.rank56, round = "2", period = PeriodEnum.final, medal = MedalEnum.copper });

                // - 將[銅牌戰]變為[決賽]，獎牌戰 = 銅牌
                UpdateEvent(cfg, row, new TEvt { tree = TreeEnum.rpc, round = "4", period = PeriodEnum.final, medal = MedalEnum.copper });
            }
            else
            {
                // - 將[銅牌戰]變為[決賽]，獎牌戰 = 銅牌
                UpdateEvent(cfg, row, new TEvt { tree = TreeEnum.rpc, round = "4", period = PeriodEnum.final, medal = MedalEnum.copper });
            }

            // - 將[金牌戰]變為[決賽]，獎牌戰 = 金牌
            UpdateEvent(cfg, row, new TEvt { tree = TreeEnum.main, round = "2", period = PeriodEnum.final, medal = MedalEnum.golden });
        }

        //四柱復活挑戰賽
        private void Challenge(TConfig cfg, TProgram row)
        {
            // - 將[銅牌戰]變為[預賽]，獎牌戰 = 無
            UpdateEvent(cfg, row, new TEvt { tree = TreeEnum.rpc, round = "4", period = PeriodEnum.pre, medal = MedalEnum.none });
            // - 將[34名戰]變為[預賽]，獎牌戰 = 無
            UpdateEvent(cfg, row, new TEvt { tree = TreeEnum.rpc, round = "2", period = PeriodEnum.pre, medal = MedalEnum.none });
            // - 將[金牌戰]變為[決賽]，獎牌戰 = 無
            UpdateEvent(cfg, row, new TEvt { tree = TreeEnum.main, round = "2", period = PeriodEnum.pre, medal = MedalEnum.none });

            // - 將[3挑戰2]變為[決賽]，獎牌戰 = 3 vs 2
            UpdateEvent(cfg, row, new TEvt { tree = TreeEnum.cha, round = "4", period = PeriodEnum.final, medal = MedalEnum.challenge1 });
            // - 將[2挑戰1]變為[決賽]，獎牌戰 = 2 vs 1
            UpdateEvent(cfg, row, new TEvt { tree = TreeEnum.cha, round = "2", period = PeriodEnum.final, medal = MedalEnum.challenge2 });
            // - 將[1挑戰1]變為[決賽]，獎牌戰 = 1 vs 1
            UpdateEvent(cfg, row, new TEvt { tree = TreeEnum.chb, round = "2", period = PeriodEnum.final, medal = MedalEnum.challenge3 });

            // - 將[盟主挑戰1]變為[決賽]，獎牌戰 = 1 vs 1
            UpdateEvent(cfg, row, new TEvt { tree = TreeEnum.ka01, round = "2", period = PeriodEnum.final, medal = MedalEnum.lord1 });
            // - 將[盟主挑戰2]變為[決賽]，獎牌戰 = 1 vs 1
            UpdateEvent(cfg, row, new TEvt { tree = TreeEnum.kb01, round = "2", period = PeriodEnum.final, medal = MedalEnum.lord2 });
        }

        private void UpdateEvent(TConfig cfg, TProgram row, TEvt evt)
        {
            string sql = @"
                UPDATE IN_MEETING_PEVENT SET 
	                in_period = {#period}
	                , in_medal = {#medal}
                WHERE 
	                source_id = '{#program_id}' 
	                AND in_tree_name = '{#tree}' 
	                AND in_round_code = {#round}
            ";

            sql = sql.Replace("{#program_id}", row.id)
                .Replace("{#tree}", TreeName(evt.tree))
                .Replace("{#round}", evt.round)
                .Replace("{#period}", ((int)evt.period).ToString())
                .Replace("{#medal}", ((int)evt.medal).ToString());

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "UpdateEvent _# sql: " + sql);

            Item itmResult = cfg.inn.applySQL(sql);

            if (itmResult.isError())
            {
                throw new Exception(row.in_name + " 發生錯誤 _# sql: " + sql);
            }
        }

        private void UpdateEventRobin(TConfig cfg, TProgram row, TEvt evt)
        {
            string filter = string.IsNullOrWhiteSpace(evt.treeid)
                ? ""
                : "AND in_tree_id = '" + evt.treeid + "'";

            string sql = @"
                UPDATE IN_MEETING_PEVENT SET 
	                in_period = {#period}
	                , in_medal = {#medal}
                WHERE 
	                source_id = '{#program_id}' 
	                AND in_tree_name = '{#tree}' 
	                {#filter}
            ";

            sql = sql.Replace("{#program_id}", row.id)
                .Replace("{#tree}", TreeName(evt.tree))
                .Replace("{#filter}", filter)
                .Replace("{#period}", ((int)evt.period).ToString())
                .Replace("{#medal}", ((int)evt.medal).ToString());

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "UpdateEventRobin _# sql: " + sql);

            Item itmResult = cfg.inn.applySQL(sql);

            if (itmResult.isError())
            {
                throw new Exception(row.in_name + " 發生錯誤 _# sql: " + sql);
            }
        }

        private string TreeName(TreeEnum e)
        {
            switch (e)
            {
                case TreeEnum.main: return "main";
                case TreeEnum.rpc: return "repechage";
                case TreeEnum.cha: return "challenge-a";
                case TreeEnum.chb: return "challenge-b";
                case TreeEnum.ka01: return "ka01";
                case TreeEnum.kb01: return "kb01";
                case TreeEnum.rank34: return "rank34";
                case TreeEnum.rank56: return "rank56";
                case TreeEnum.rank78: return "rank78";
                case TreeEnum.sub: return "sub";
                default: return "";
            }
        }

        private List<TProgram> GetPrograms(TConfig cfg)
        {
            List<TProgram> result = new List<TProgram>();

            List<string> filters = new List<string>();
            filters.Add("in_meeting = '" + cfg.meeting_id + "'");
            if (cfg.fight_day != "") filters.Add("in_fight_day = '" + cfg.fight_day + "'");
            if (cfg.program_id != "") filters.Add("id = '" + cfg.program_id + "'");

            string sql = @"
                SELECT 
	                id
                    , in_name
	                , in_battle_type
	                , in_team_count
	                , in_rank_count
                FROM
	                IN_MEETING_PROGRAM WITH(NOLOCK)
                WHERE
	                {#filter}
                ORDER BY
                    in_sort_order
            ";

            sql = sql.Replace("{#filter}", string.Join(" AND ", filters));

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "GetPrograms _# sql: " + sql);

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                var obj = new TProgram
                {
                    id = item.getProperty("id", ""),
                    in_name = item.getProperty("in_name", ""),
                    in_battle_type = item.getProperty("in_battle_type", ""),
                    in_team_count = item.getProperty("in_team_count", "0"),
                    in_rank_count = item.getProperty("in_rank_count", "0"),
                };

                obj.team_count = GetInt(obj.in_team_count);
                obj.rank_count = GetInt(obj.in_rank_count);

                result.Add(obj);
            }

            return result;
        }

        private void ResetRankCount(TConfig cfg)
        {
            List<string> filters = new List<string>();
            filters.Add("t101.in_meeting = '" + cfg.meeting_id + "'");
            if (cfg.fight_day != "") filters.Add("t101.in_fight_day = '" + cfg.fight_day + "'");
            if (cfg.program_id != "") filters.Add("t101.id = '" + cfg.program_id + "'");

            string sql = @"
                UPDATE t1 SET 
	                t1.in_rank_count = t2.rank_count
                FROM 
                    IN_MEETING_PROGRAM t1
                INNER JOIN
                (
	                SELECT 
		                t101.id, count(*) AS 'rank_count'
	                FROM
		                IN_MEETING_PROGRAM t101 WITH(NOLOCK)
	                INNER JOIN
		                IN_MEETING_PTEAM t102 WITH(NOLOCK)
		                ON t102.source_id = t101.id
	                WHERE 
		                {#filter}
		                AND ISNULL(t102.in_weight_message, '') NOT IN (N'(請假)', N'(未到)', N'(居隔)', N'(DQ)')
	                GROUP BY
		                t101.id
                )t2 ON t2.id = t1.id
            ";

            sql = sql.Replace("{#filter}", string.Join(" AND ", filters));

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "ResetRankCount _# sql: " + sql);

            cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }

            public string meeting_id { get; set; }
            public string program_id { get; set; }
            public string fight_day { get; set; }

            public Item itmMeeting { get; set; }
            public string mt_battle_type { get; set; }
        }

        private class TProgram
        {
            public string id { get; set; }
            public string in_name { get; set; }
            public string in_battle_type { get; set; }
            public string in_team_count { get; set; }
            public string in_rank_count { get; set; }

            public int team_count { get; set; }
            public int rank_count { get; set; }
        }

        private class TEvt
        {
            public TreeEnum tree { get; set; }
            public string treeid { get; set; }
            public string round { get; set; }
            public PeriodEnum period { get; set; }
            public MedalEnum medal { get; set; }
        }

        private enum TreeEnum
        {
            main = 100,
            rpc = 200,
            cha = 300,
            chb = 400,
            ka01 = 500,
            kb01 = 600,
            rank34 = 3000,
            rank56 = 5000,
            rank78 = 7000,
            sub = 9000,
        }

        private enum PeriodEnum
        {
            /// <summary>
            /// 預賽
            /// </summary>
            pre = 1,
            /// <summary>
            /// 決賽
            /// </summary>
            final = 4,
            /// <summary>
            /// 加賽
            /// </summary>
            more = 8,
        }

        private enum MedalEnum
        {
            /// <summary>
            /// 未設定
            /// </summary>
            none = 0,
            /// <summary>
            /// 銅牌戰
            /// </summary>
            copper = 1,
            /// <summary>
            /// 金牌戰
            /// </summary>
            golden = 2,
            /// <summary>
            /// 三挑戰二
            /// </summary>
            challenge1 = 11,
            /// <summary>
            /// 二挑戰一
            /// </summary>
            challenge2 = 12,
            /// <summary>
            /// 一落敗再挑戰
            /// </summary>
            challenge3 = 13,
            /// <summary>
            /// 盟主戰
            /// </summary>
            lord1 = 21,
            /// <summary>
            /// 盟主落敗再挑戰
            /// </summary>
            lord2 = 22,
        }

        private int GetInt(string value, int def = 0)
        {
            int result = def;
            Int32.TryParse(value, out result);
            return result;
        }
    }
}