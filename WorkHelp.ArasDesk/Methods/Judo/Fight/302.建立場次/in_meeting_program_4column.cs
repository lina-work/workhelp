﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Judo.Fight
{
    public class in_meeting_program_4column : Item
    {
        public in_meeting_program_4column(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 單淘 or 四柱復活賽 or 挑戰賽
                日誌: 
                    - 2022-03-01: 加入賽別類型 (lina)
                    - 2020-11-20: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]in_meeting_program_4column";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,

                meeting_id = itmR.getProperty("meeting_id", ""),
                program_id = itmR.getProperty("program_id", ""),
                battle_type = itmR.getProperty("battle_type", ""),
                scene = itmR.getProperty("scene", ""),
            };

            if (cfg.meeting_id == "" || cfg.program_id == "" || cfg.battle_type == "")
            {
                throw new Exception("參數錯誤");
            }

            //檢查頁面權限
            var itmPermit = inn.applyMethod("In_CheckIdentity", "<method>" + strMethodName + "</method><code>" + "ALL" + "</code>");
            if (itmPermit.getProperty("isMeetingAdmin", "") != "1")
            {
                itmR.setProperty("error_message", "您無權限瀏覽此頁面");
                return itmR;
            }

            cfg.itmMeeting = GetMeeting(cfg);
            if (cfg.itmMeeting.isError())
            {
                throw new Exception("賽事資料錯誤");
            }

            cfg.in_site_mode = cfg.itmMeeting.getProperty("in_site_mode", "");
            cfg.in_battle_repechage = cfg.itmMeeting.getProperty("in_battle_repechage", "");

            cfg.itmProgram = GetProgram(cfg);
            if (cfg.itmProgram.isError())
            {
                throw new Exception("賽程組別資料錯誤");
            }

            cfg.in_rank_type = cfg.itmProgram.getProperty("in_rank_type", "");
            cfg.in_team_count = cfg.itmProgram.getProperty("in_team_count", "0");
            cfg.in_round_count = cfg.itmProgram.getProperty("in_round_count", "0");
            cfg.in_fight_day = cfg.itmProgram.getProperty("in_fight_day", "");
            cfg.in_site = cfg.itmProgram.getProperty("in_site", "");
            cfg.in_l1 = cfg.itmProgram.getProperty("in_l1", "");
            cfg.in_sub_event = cfg.itmProgram.getProperty("in_sub_event", "0");

            cfg.team_count = GetInt(cfg.in_team_count);
            cfg.round_count = GetInt(cfg.in_round_count);
            cfg.sub_event = GetInt(cfg.in_sub_event);

            if (cfg.scene == "run_sub_event")
            {
                RunSubEvents(cfg, itmR);
            }
            else
            {
                RunAllEvents(cfg, itmR);
            }

            return itmR;
        }

        //跑子場次
        private void RunSubEvents(TConfig cfg, Item itmReturn)
        {
            if (cfg.in_l1 != "團體組" || cfg.sub_event <= 0) return;

            //建立子場次
            GenerateSubEvents(cfg);

            if (cfg.battle_type == "JudoTopFour")
            {
                //四柱復活: 將 勝部金牌戰(一場)、敗部銅牌戰(兩場)變更為決賽
                FinalPeriod(cfg);
            }

            //子場次全部重編
            ResetSubTreeNo(cfg);

            //子場次量級重設
            ResetSubSects(cfg);
        }

        private void RunAllEvents(TConfig cfg, Item itmReturn)
        {
            //更新組別賽制
            UpdateProgram(cfg);

            //清除場次資料
            RemoveEvents(cfg);

            //新增主場次資料
            AddMainEvents(cfg);

            switch (cfg.battle_type)
            {
                case "TopTwo":
                    //新增單淘三四名戰
                    AddRank34Event(cfg);
                    break;

                case "JudoTopFour": //四柱復活
                    if (cfg.in_battle_repechage == "quarterfinals")
                    {
                        //只有前八強可進敗部
                        AddRepechageEvents2(cfg);
                    }
                    else
                    {
                        //和四強對打的選手可進敗部
                        AddRepechageEvents1(cfg);
                    }
                    break;

                case "Challenge"://挑戰賽
                    //新增四柱復活場次資料
                    AddRepechageEvents1(cfg);
                    //新增挑戰賽場次資料
                    AddChallengeEvents(cfg);
                    break;
            }

            //新增場次明細
            AddEventDetails(cfg);

            if (cfg.in_l1 == "團體組" && cfg.sub_event > 0)
            {
                //建立子場次
                GenerateSubEvents(cfg);
            }

            //重設場次編號
            RestEventNos(cfg);

            if (cfg.battle_type == "JudoTopFour")
            {
                //四柱復活: 將 勝部金牌戰(一場)、敗部銅牌戰(兩場)變更為決賽
                FinalPeriod(cfg);
            }

            //子場次全部重編
            ResetSubTreeNo(cfg);

            //子場次量級重設
            ResetSubSects(cfg);

            //重設東西側
            ResetWestEast(cfg);
        }

        //重設東西側
        private void ResetWestEast(TConfig cfg)
        {
            var sql = "";

            //勝部西側
            sql = @"
                UPDATE IN_MEETING_PEVENT SET
	                in_site_we = 'W'
                WHERE 
	                source_id = '{#program_id}'
	                AND in_tree_name = 'main'
	                AND in_round_id <= (in_round_code / 4)
					AND in_fight_id <> 'M002-01'
            ";

            sql = sql.Replace("{#program_id}", cfg.program_id);

            cfg.inn.applySQL(sql);

            //勝部東側

            sql = @"
                UPDATE IN_MEETING_PEVENT SET
	                in_site_we = 'E'
                WHERE 
	                source_id = '{#program_id}'
	                AND in_tree_name = 'main'
	                AND in_round_id > (in_round_code / 4)
					AND in_fight_id <> 'M002-01'
            ";

            sql = sql.Replace("{#program_id}", cfg.program_id);

            cfg.inn.applySQL(sql);

            //敗部西側

            sql = @"
                UPDATE IN_MEETING_PEVENT SET
	                in_site_we = 'W'
                WHERE 
	                source_id = '{#program_id}'
	                AND in_tree_name = 'repechage'
	                AND in_fight_id IN ('R064-01', 'R064-02', 'R032-01', 'R032-02', 'R016-01', 'R016-02', 'R008-01', 'R004-01')
            ";

            sql = sql.Replace("{#program_id}", cfg.program_id);

            cfg.inn.applySQL(sql);

            //敗部東側

            sql = @"
                UPDATE IN_MEETING_PEVENT SET
	                in_site_we = 'E'
                WHERE 
	                source_id = '{#program_id}'
	                AND in_tree_name = 'repechage'
	                AND in_fight_id IN ('R064-03', 'R064-04', 'R032-03', 'R032-04', 'R016-03', 'R016-04', 'R008-02', 'R004-02')
            ";

            sql = sql.Replace("{#program_id}", cfg.program_id);

            cfg.inn.applySQL(sql);
        }

        //子場次量級重設
        private void ResetSubSects(TConfig cfg)
        {
            var sql = @"
                UPDATE t1 SET
                	t1.in_sub_sect = t2.in_name
                FROM
                	IN_MEETING_PEVENT t1
                INNER JOIN
                	IN_MEETING_PSECT t2
                	ON t2.IN_PROGRAM = t1.source_id
                	AND t1.in_tree_name = 'sub'
                	AND t2.in_sub_id = t1.in_sub_id
                WHERE
                	t1.source_id = '{#program_id}'
            ";

            sql = sql.Replace("{#program_id}", cfg.program_id);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            cfg.inn.applySQL(sql);
        }

        //子場次全部重編
        private void ResetSubTreeNo(TConfig cfg)
        {
            var sql = @"
                UPDATE t1 SET
	                t1.in_tree_no = t2.in_tree_no * 100 + t1.in_sub_id
	                , t1.in_period = t2.in_period
	                , t1.in_medal = t2.in_medal
	                , t1.in_show_site = t2.in_show_site
	                , t1.in_show_serial = t2.in_tree_no * 100 + t1.in_sub_id
                FROM 
	                IN_MEETING_PEVENT t1 WITH(NOLOCK) 
                INNER JOIN
	                IN_MEETING_PEVENT t2 WITH(NOLOCK) 
	                ON t2.id = t1.in_parent
                WHERE
	                t1.source_id = '{#program_id}'
	                AND t1.in_tree_name = 'sub'
	                AND ISNULL(t2.in_tree_no, 0) > 0
            ";

            sql = sql.Replace("{#program_id}", cfg.program_id);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            cfg.inn.applySQL(sql);

            sql = @"
                UPDATE t1 SET
	                t1.in_tree_no = NULL
	                , t1.in_period = t2.in_period
	                , t1.in_medal = t2.in_medal
	                , t1.in_show_site = t2.in_show_site
	                , t1.in_show_serial = NULL
                FROM 
	                IN_MEETING_PEVENT t1 WITH(NOLOCK) 
                INNER JOIN
	                IN_MEETING_PEVENT t2 WITH(NOLOCK) 
	                ON t2.id = t1.in_parent
                WHERE
	                t1.source_id = '{#program_id}'
	                AND t1.in_tree_name = 'sub'
	                AND ISNULL(t2.in_tree_no, 0) <= 0
            ";

            sql = sql.Replace("{#program_id}", cfg.program_id);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            cfg.inn.applySQL(sql);
        }

        //四柱復活: 將 12名(一場)、34名(兩場)變更為決賽
        private void FinalPeriod(TConfig cfg)
        {
            string sql = "UPDATE IN_MEETING_PEVENT SET"
                + " in_period = 4"
                + " WHERE source_id = '{#program_id}'"
                + " AND in_tree_name = '{#in_tree_name}'"
                + " AND in_round_code = {#in_round_code}";

            //勝部金牌戰 x 1
            string sql_upd_1 = sql.Replace("{#program_id}", cfg.program_id)
                .Replace("{#in_tree_name}", "main")
                .Replace("{#in_round_code}", "2");

            cfg.inn.applySQL(sql_upd_1);

            //敗部銅牌戰 x 2
            string sql_upd_2 = sql.Replace("{#program_id}", cfg.program_id)
                .Replace("{#in_tree_name}", "repechage")
                .Replace("{#in_round_code}", "4");

            cfg.inn.applySQL(sql_upd_2);
        }

        //建立子場次
        private void GenerateSubEvents(TConfig cfg)
        {
            Item item = cfg.inn.newItem();
            item.setType("In_Meeting_Program");
            item.setProperty("meeting_id", cfg.meeting_id);
            item.setProperty("program_id", cfg.program_id);
            item.apply("in_meeting_pevent_sub");
        }

        //重設場次編號
        private void RestEventNos(TConfig cfg)
        {
            if (cfg.in_site_mode == "")
            {
                Item itmData = cfg.inn.newItem("In_Meeting_Program");
                itmData.setProperty("meeting_id", cfg.meeting_id);
                itmData.setProperty("program_id", cfg.program_id);
                itmData.setProperty("is_build", "1");
                itmData.apply("in_meeting_pevent_reset");
            }
            else
            {
                Item itmData = cfg.inn.newItem("In_Meeting_Program");
                itmData.setProperty("meeting_id", cfg.meeting_id);
                itmData.setProperty("program_id", cfg.program_id);
                itmData.setProperty("is_build", "1");
                itmData.apply("in_meeting_pevent_reset");
            }
        }

        /// <summary>
        /// 更新組別賽制
        /// </summary>
        private void UpdateProgram(TConfig cfg)
        {
            string sql = "UPDATE IN_MEETING_PROGRAM SET"
                + " in_battle_type = N'" + cfg.battle_type + "'"
                + " , in_tiebreaker = N''"
                + " , in_challenge = N''"
                + " WHERE id = '" + cfg.program_id + "'";

            Item itmSQL = cfg.inn.applySQL(sql);

            if (itmSQL.isError())
            {
                throw new Exception("更新組別賽制發生錯誤");
            }
        }

        /// <summary>
        /// 清除場次資料
        /// </summary>
        private void RemoveEvents(TConfig cfg)
        {
            Item itmData = cfg.inn.newItem("In_Meeting_Program");
            itmData.setProperty("program_id", cfg.program_id);
            itmData.apply("in_meeting_program_remove_all");
        }

        #region 主場次演算

        /// <summary>
        /// 建立比賽場次
        /// </summary>
        private void AddMainEvents(TConfig cfg)
        {
            if (cfg.team_count <= 0) return;

            int[] round_arr = GetRoundAndSum(cfg.team_count, 2, 2, 1);

            TTree tree = new TTree
            {
                MeetingId = cfg.meeting_id,
                ProgramId = cfg.program_id,
                TeamCount = cfg.team_count,

                TreeCode = "M",
                TreeName = "main",
                TreeSort = 100,
                MaxRound = round_arr[0],
                MaxCode = round_arr[1],

                FightDay = cfg.in_fight_day,
                SiteId = cfg.in_site,
            };

            //當前輪數 (ex: 6)
            int current_round = round_arr[0];
            //當前輪數代碼 (ex: 2)
            int current_code = 2;
            //當前輪數序號
            int current_round_id = 1;
            //當前場次籤數
            int sign_code = 1;
            //當前場次籤號
            int sign_no = 1;

            //樹圖序號(空間)
            string in_tree_id = GetTreeId(tree.TreeCode, current_round, current_round_id);

            TEventStatus status = GetEventStatus(sign_no, sign_code, tree);

            TEvent root = new TEvent
            {
                in_meeting = tree.MeetingId,
                source_id = tree.ProgramId,
                in_date_key = tree.FightDay,
                in_site = tree.SiteId,

                in_tree_name = tree.TreeName,
                in_tree_sort = tree.TreeSort,
                in_tree_id = in_tree_id,
                in_tree_no = "", //最後算 樹圖序號(時間)
                in_tree_rank = "rank12",
                in_tree_rank_ns = "1,2",
                in_tree_rank_nss = "1,2",
                in_tree_alias = GetTreeAlias(current_code),

                in_bypass_foot = "",
                in_bypass_status = "",

                in_round = current_round,
                in_round_code = current_code,
                in_round_id = current_round_id,

                in_sign_code = sign_code,
                in_sign_no = sign_no,

                in_next_win = "",
                in_next_foot_win = "",

                Signs = status.Signs,
                in_period = 1,
            };

            ApplyEvent(cfg, root);

            List<TEvent> list = new List<TEvent>() { root };

            AppendMainRecursive(cfg, tree, list, current_round - 1, current_code * 2);
        }

        //新增賽事組別場次(遞迴)
        private void AppendMainRecursive(TConfig cfg, TTree tree, List<TEvent> parents, int current_round, int current_code)
        {
            if (current_round <= 0)
            {
                return;
            }

            List<TEvent> list = new List<TEvent>();

            int current_round_id = 1;

            for (int i = 0; i < parents.Count; i++)
            {
                var parent = parents[i];
                var psno = parent.in_sign_no.ToString();
                var pscd = parent.in_sign_code;

                var current_sign_code = pscd * 2;

                for (int j = 0; j < parent.Signs.Count; j++)
                {
                    var sign = parent.Signs[j];

                    var current_sign_no = sign.SignNo;
                    var current_tree_id = GetTreeId(tree.TreeCode, current_round, current_round_id);

                    TEventStatus current_status = GetEventStatus(current_sign_no, current_sign_code, tree);

                    TEvent evt = new TEvent
                    {
                        in_meeting = tree.MeetingId,
                        source_id = tree.ProgramId,
                        in_date_key = tree.FightDay,
                        in_site = tree.SiteId,

                        in_tree_name = tree.TreeName,
                        in_tree_sort = tree.TreeSort,
                        in_tree_id = current_tree_id,
                        in_tree_no = "", //最後算 樹圖序號(時間)
                        in_tree_rank = "",
                        in_tree_rank_ns = "",
                        in_tree_rank_nss = "",
                        in_tree_alias = GetTreeAlias(current_code),

                        in_bypass_foot = "",
                        in_bypass_status = "",

                        in_round = current_round,
                        in_round_code = current_code,
                        in_round_id = current_round_id,

                        in_sign_code = current_sign_code,
                        in_sign_no = current_sign_no,

                        in_next_win = parent.id,
                        in_next_foot_win = current_sign_no % 2 > 0 ? "1" : "2",

                        Signs = current_status.Signs,
                        in_period = 1,
                    };

                    list.Add(evt);

                    current_round_id++;
                }
            }

            for (int i = 0; i < list.Count; i++)
            {
                TEvent evt = list[i];
                ApplyEvent(cfg, evt);
            }

            AppendMainRecursive(cfg, tree, list, current_round - 1, current_code * 2);
        }

        #endregion 主場次演算

        #region 單淘

        private void AddRank34Event(TConfig cfg)
        {
            if (cfg.team_count < 4) return;

            //增加三四名場次
            Item itmEvent = AppendSingle(cfg
                , in_tree_name: "rank34"
                , in_tree_sort: 200
                , in_tree_id: "rank34"
                , in_fight_id: "RNK34-01"
                , in_round: "1"
                , in_round_id: "1"
                , in_tree_rank: "rank34"
                , in_tree_rank_ns: "3,4"
                , in_tree_alias: "三四名"
                , in_period: 1);

            Rank34LinkMainSemiFinal_NoSwitch(cfg, itmEvent);

        }

        //建立 L 下一場 id 連結 (無對調上下籤腳)
        private void Rank34LinkMainSemiFinal_NoSwitch(TConfig cfg, Item itmEvent)
        {
            string sql = "";
            string sql_update = "";

            string program_id = itmEvent.getProperty("source_id", "");
            string next_event_id = itmEvent.getID();

            sql = @"
                UPDATE 
                    IN_MEETING_PEVENT 
                SET
                    in_next_lose = '{#next_id}'
                    , in_next_foot_lose = '{#next_foot}'
                WHERE
                    source_id = '{#program_id}'
                    AND in_tree_name = N'main'
                    AND in_round_code = '{#in_round_code}'
                    AND in_round_id = '{#in_round_id}'
             ";

            // 主線-準決賽-第 1 場 loss 連結到 三四名場次-籤腳 1
            sql_update = sql.Replace("{#program_id}", program_id)
                .Replace("{#in_round_code}", "4")//準決賽
                .Replace("{#in_round_id}", "1")//第 1 場
                .Replace("{#next_id}", next_event_id)
                .Replace("{#next_foot}", "1");

            cfg.inn.applySQL(sql_update);


            // 主線-準決賽-第 2 場 loss 連結到 三四名場次-籤腳 2
            sql_update = sql.Replace("{#program_id}", program_id)
                .Replace("{#in_round_code}", "4")//準決賽
                .Replace("{#in_round_id}", "2")//第 2 場
                .Replace("{#next_id}", next_event_id)
                .Replace("{#next_foot}", "2");

            cfg.inn.applySQL(sql_update);
        }
        #endregion 單淘

        #region 四柱復活演算

        private void AddRepechageEvents1(TConfig cfg)
        {
            if (cfg.team_count > 0 && cfg.team_count < 3)
            {
                //兩隊以下，不處理
            }
            else if (cfg.team_count >= 3 && cfg.team_count <= 4)
            {
                //增加三四名場次
                Item itmEvent = AppendSingle(cfg
                    , in_tree_name: "repechage"
                    , in_tree_sort: 200
                    , in_tree_id: "rank34"
                    , in_fight_id: "RNK34-01"
                    , in_round: "1"
                    , in_round_id: "1"
                    , in_tree_rank: "rank34"
                    , in_tree_rank_ns: "3,4"
                    , in_tree_alias: "三四名"
                    , in_period: 1);

                Rank34LinkMainSemiFinal(cfg, itmEvent);
            }
            else
            {
                //新增復活表場次

                AppendRepechage(cfg, cfg.team_count, cfg.round_count);

                RpcSemiFinalLinkMainSemiFinal(cfg);

                Item itmEvent56 = AppendSingle(cfg
                    , in_tree_name: "rank56"
                    , in_tree_sort: 5000
                    , in_tree_id: "rank56"
                    , in_fight_id: "RNK56-01"
                    , in_round: "1"
                    , in_round_id: "1"
                    , in_tree_rank: "rank56"
                    , in_tree_rank_ns: "5,6"
                    , in_tree_alias: "五六名"
                    , in_period: 1);

                Rank56LinkRpcSemiFinal(cfg, itmEvent56);

                bool need_rank78 = cfg.team_count >= 8;

                if (need_rank78)
                {
                    Item itmEvent78 = AppendSingle(cfg
                        , in_tree_name: "rank78"
                        , in_tree_sort: 7000
                        , in_tree_id: "rank78"
                        , in_fight_id: "RNK78-01"
                        , in_round: "1"
                        , in_round_id: "1"
                        , in_tree_rank: "rank78"
                        , in_tree_rank_ns: "7,8"
                        , in_tree_alias: "七八名"
                        , in_period: 1);

                    Rank78LinkRpcSemiSemi(cfg, itmEvent78);
                }
            }
        }

        //前八強
        private void AddRepechageEvents2(TConfig cfg)
        {
            if (cfg.team_count > 0 && cfg.team_count < 3)
            {
                //兩隊以下，不處理
            }
            else if (cfg.team_count >= 3 && cfg.team_count <= 4)
            {
                //增加三四名場次
                Item itmEvent = AppendSingle(cfg
                    , in_tree_name: "repechage"
                    , in_tree_sort: 200
                    , in_tree_id: "rank34"
                    , in_fight_id: "RNK34-01"
                    , in_round: "1"
                    , in_round_id: "1"
                    , in_tree_rank: "rank34"
                    , in_tree_rank_ns: "3,4"
                    , in_tree_alias: "三四名"
                    , in_period: 1);

                Rank34LinkMainSemiFinal(cfg, itmEvent);
            }
            else
            {
                //新增復活表場次
                int team_count = 8;
                int round_count = 3;

                AppendRepechage(cfg, team_count, round_count);

                RpcSemiFinalLinkMainSemiFinal(cfg);

                Item itmEvent56 = AppendSingle(cfg
                    , in_tree_name: "rank56"
                    , in_tree_sort: 5000
                    , in_tree_id: "rank56"
                    , in_fight_id: "RNK56-01"
                    , in_round: "1"
                    , in_round_id: "1"
                    , in_tree_rank: "rank56"
                    , in_tree_rank_ns: "5,6"
                    , in_tree_alias: "五六名"
                    , in_period: 1);

                Rank56LinkRpcSemiFinal(cfg, itmEvent56);

                bool need_rank78 = team_count >= 8;

                if (need_rank78)
                {
                    Item itmEvent78 = AppendSingle(cfg
                        , in_tree_name: "rank78"
                        , in_tree_sort: 7000
                        , in_tree_id: "rank78"
                        , in_fight_id: "RNK78-01"
                        , in_round: "1"
                        , in_round_id: "1"
                        , in_tree_rank: "rank78"
                        , in_tree_rank_ns: "7,8"
                        , in_tree_alias: "七八名"
                        , in_period: 1);

                    Rank78LinkRpcSemiSemi(cfg, itmEvent78);
                }
            }
        }

        //新增賽事組別場次
        private void AppendRepechage(TConfig cfg, int team_count, int round_count)
        {
            if (cfg.round_count <= 0) return;

            TTree tree = new TTree
            {
                MeetingId = cfg.meeting_id,
                ProgramId = cfg.program_id,
                TeamCount = team_count,
                RpcTeamCount = team_count - 2,

                TreeCode = "R",
                TreeName = "repechage",
                TreeSort = 200,
                MaxRound = round_count,

                FightDay = cfg.in_fight_day,
                SiteId = cfg.in_site,
            };

            //當前輪數 (ex: 6)
            int current_round = round_count;
            //當前輪數代碼 (ex: 2)
            int current_code = 2;
            //當前輪數序號
            int current_round_id = 1;
            //當前場次籤數
            int sign_code = 1;
            //當前場次籤號
            int sign_no = 1;

            //樹圖序號(空間)
            string in_tree_id = GetTreeId(tree.TreeCode, current_round, current_round_id);

            TEventStatus status = GetEventStatus(sign_no, sign_code, tree);

            string rank34_nss = "3,4";
            switch (cfg.in_rank_type)
            {
                case "SameRank":
                    rank34_nss = "3,3";
                    break;
            }

            TEvent root = new TEvent
            {
                in_meeting = tree.MeetingId,
                source_id = tree.ProgramId,

                in_date_key = tree.FightDay,
                in_site = tree.SiteId,

                in_tree_name = tree.TreeName,
                in_tree_sort = tree.TreeSort,
                in_tree_id = in_tree_id,
                in_tree_no = "", //最後算 樹圖序號(時間)
                in_tree_rank = "rank34",
                in_tree_rank_ns = "3,4",
                in_tree_rank_nss = rank34_nss,
                in_tree_alias = "復活賽-決賽",

                in_bypass_foot = "",
                in_bypass_status = "",

                in_round = current_round,
                in_round_code = current_code,
                in_round_id = current_round_id,

                in_sign_code = sign_code,
                in_sign_no = sign_no,

                in_next_win = "",
                in_next_foot_win = "",

                Signs = status.Signs,
                in_period = 1,
            };

            ApplyEvent(cfg, root);

            List<TEvent> list = new List<TEvent>() { root };

            AppendRepechageRecursive(cfg, tree, list, current_round - 1);
        }

        //新增賽事組別場次(遞迴)
        private void AppendRepechageRecursive(TConfig cfg, TTree tree, List<TEvent> parents, int current_round)
        {
            if (current_round <= 0)
            {
                return;
            }

            List<int> arr = new List<int>
            {
                tree.MaxRound - 1,
                tree.MaxRound - 3,
            };

            if (tree.TeamCount <= 8)
            {
                arr = new List<int>
                {
                    tree.MaxRound - 1,
                };
            }

            List<TEvent> list = new List<TEvent>();

            int current_round_id = 1;

            for (int i = 0; i < parents.Count; i++)
            {
                var parent = parents[i];
                var psno = parent.in_sign_no.ToString();
                var pscd = parent.in_sign_code;

                var current_sign_code = pscd * 2;

                int event_count = 1; //只生一個子場次

                if (arr.Contains(current_round))
                {
                    event_count = parent.Signs.Count;
                }

                for (int j = 0; j < event_count; j++)
                {
                    var sign = parent.Signs[j];

                    var current_sign_no = sign.SignNo;
                    var current_tree_id = GetTreeId(tree.TreeCode, current_round, current_round_id);
                    var current_round_code = current_sign_code * 2;

                    var in_tree_rank = "";
                    var in_tree_rank_ns = "";
                    var in_tree_rank_nss = "";

                    if (current_round_code == 4)
                    {
                        in_tree_rank = "rank3";
                        in_tree_rank_ns = "3,0";
                        in_tree_rank_nss = "3,0";
                    }

                    TEventStatus current_status = GetEventStatus(current_sign_no, current_sign_code, tree);

                    TEvent evt = new TEvent
                    {
                        in_meeting = tree.MeetingId,
                        source_id = tree.ProgramId,
                        in_date_key = tree.FightDay,
                        in_site = tree.SiteId,

                        in_tree_name = tree.TreeName,
                        in_tree_sort = tree.TreeSort,
                        in_tree_id = current_tree_id,
                        in_tree_no = "", //最後算 樹圖序號(時間)
                        in_tree_rank = in_tree_rank,
                        in_tree_rank_ns = in_tree_rank_ns,
                        in_tree_rank_nss = in_tree_rank_nss,
                        in_tree_alias = "復活賽",

                        in_bypass_foot = "",
                        in_bypass_status = "",

                        in_round = current_round,
                        in_round_code = current_round_code,
                        in_round_id = current_round_id,

                        in_sign_code = current_sign_code,
                        in_sign_no = current_sign_no,

                        in_next_win = parent.id,
                        in_next_foot_win = current_sign_no % 2 > 0 ? "1" : "2",

                        Signs = current_status.Signs,
                        in_period = 1,
                    };

                    list.Add(evt);

                    current_round_id++;
                }
            }

            for (int i = 0; i < list.Count; i++)
            {
                TEvent evt = list[i];
                ApplyEvent(cfg, evt);
            }

            AppendRepechageRecursive(cfg, tree, list, current_round - 1);
        }


        //建立單場賽事組別場次
        private Item AppendSingle(TConfig cfg
            , string in_tree_name
            , int in_tree_sort
            , string in_tree_id
            , string in_fight_id
            , string in_round
            , string in_round_id
            , string in_tree_rank
            , string in_tree_rank_ns
            , string in_tree_alias
            , int in_period)
        {

            string in_tree_rank_nss = in_tree_rank_ns;
            switch (cfg.in_rank_type)
            {
                case "SameRank"://冠亞三三五五
                    if (in_tree_rank_ns == "3,4")
                    {
                        in_tree_rank_nss = "3,3";
                    }
                    else if (in_tree_rank_ns == "5,6")
                    {
                        in_tree_rank_nss = "5,5";
                    }
                    else if (in_tree_rank_ns == "7,8")
                    {
                        in_tree_rank_nss = "7,7";
                    }
                    else
                    {
                        in_tree_rank_nss = in_tree_rank_ns;
                    }
                    break;

                case "Challenge"://冠亞三四五五
                    if (in_tree_rank_ns == "5,6")
                    {
                        in_tree_rank_nss = "5,5";
                    }
                    else if (in_tree_rank_ns == "7,8")
                    {
                        in_tree_rank_nss = "7,7";
                    }
                    else
                    {
                        in_tree_rank_nss = in_tree_rank_ns;
                    }
                    break;

                default:
                    break;
            }

            Item item = cfg.inn.newItem("In_Meeting_PEvent", "add");
            item.setProperty("in_meeting", cfg.meeting_id);
            item.setProperty("source_id", cfg.program_id);
            item.setProperty("in_date_key", cfg.in_fight_day);
            item.setProperty("in_site", cfg.in_site);

            item.setProperty("in_tree_name", in_tree_name);
            item.setProperty("in_tree_sort", in_tree_sort.ToString());
            item.setProperty("in_tree_id", in_tree_id);
            item.setProperty("in_tree_no", "1");
            item.setProperty("in_tree_rank", in_tree_rank);
            item.setProperty("in_tree_rank_ns", in_tree_rank_ns);
            item.setProperty("in_tree_rank_nss", in_tree_rank_nss);
            item.setProperty("in_tree_alias", in_tree_alias);

            item.setProperty("in_round", in_round);
            item.setProperty("in_round_id", in_round_id);
            item.setProperty("in_round_code", "2");

            item.setProperty("in_sign_code", "");
            item.setProperty("in_sign_no", "");

            item.setProperty("in_period", in_period.ToString());
            item.setProperty("in_fight_id", in_fight_id);

            item = item.apply();

            if (item.isError())
            {
                throw new Exception("建立單場賽事組別場次發生失敗");
            }

            return item;
        }

        //建立 L 下一場 id 連結
        private void Rank34LinkMainSemiFinal(TConfig cfg, Item itmEvent)
        {
            string sql = "";
            string sql_update = "";
            Item itmSQL = null;

            string program_id = itmEvent.getProperty("source_id", "");
            string next_event_id = itmEvent.getID();

            sql = @"
                UPDATE 
                    IN_MEETING_PEVENT 
                SET
                    in_next_lose = '{#next_id}'
                    , in_next_foot_lose = '{#next_foot}'
                WHERE
                    source_id = '{#program_id}'
                    AND in_tree_name = N'main'
                    AND in_round_code = '4'
                    AND in_round_id = '{#in_round_id}'
             ";

            // 三四名場-籤腳 1 連結到 主線-準決賽-第 2 場
            sql_update = sql.Replace("{#program_id}", program_id)
                .Replace("{#in_round_id}", "2")
                .Replace("{#next_id}", next_event_id)
                .Replace("{#next_foot}", "1");

            itmSQL = cfg.inn.applySQL(sql_update);


            // 三四名場-籤腳 2 連結到 主線-準決賽-第 1 場
            sql_update = sql.Replace("{#program_id}", program_id)
                .Replace("{#in_round_id}", "1")
                .Replace("{#next_id}", next_event_id)
                .Replace("{#next_foot}", "2");

            itmSQL = cfg.inn.applySQL(sql_update);
        }

        //[主線準決賽]與[復活線準決賽]連結
        private void RpcSemiFinalLinkMainSemiFinal(TConfig cfg)
        {
            //復活線準決賽-第 1 場
            Item itmEvent_1 = cfg.inn.applySQL("SELECT * FROM IN_MEETING_PEVENT WITH(NOLOCK) WHERE source_id = '" + cfg.program_id + "' AND in_tree_name = 'repechage' AND in_round_code = '4' AND in_round_id = '1'");
            //復活線準決賽-第 2 場
            Item itmEvent_2 = cfg.inn.applySQL("SELECT * FROM IN_MEETING_PEVENT WITH(NOLOCK) WHERE source_id = '" + cfg.program_id + "' AND in_tree_name = 'repechage' AND in_round_code = '4' AND in_round_id = '2'");

            if (itmEvent_1.isError() || itmEvent_2.isError())
            {
                return;
            }

            string sql = "";
            string sql_update = "";
            Item itmSQL = null;

            sql = @"
                UPDATE 
                    IN_MEETING_PEVENT 
                SET
                    in_next_lose = '{#next_id}'
                    , in_next_foot_lose = '{#next_foot}'
                WHERE
                    source_id = '{#program_id}'
                    AND in_tree_name = N'main'
                    AND in_round_code = '4'
                    AND in_round_id = '{#in_round_id}'
            ";

            // 復活線準決賽-第 1 場 連結到 主線-準決賽-第 2 場
            sql_update = sql.Replace("{#program_id}", cfg.program_id)
                .Replace("{#in_round_id}", "2")
                .Replace("{#next_id}", itmEvent_1.getProperty("id", ""))
                .Replace("{#next_foot}", "2");

            itmSQL = cfg.inn.applySQL(sql_update);


            // 復活線準決賽-第 2 場 連結到 主線-準決賽-第 1 場
            sql_update = sql.Replace("{#program_id}", cfg.program_id)
                .Replace("{#in_round_id}", "1")
                .Replace("{#next_id}", itmEvent_2.getProperty("id", ""))
                .Replace("{#next_foot}", "2");

            itmSQL = cfg.inn.applySQL(sql_update);
        }

        //建立 L 下一場 id 連結
        private void Rank56LinkRpcSemiFinal(TConfig cfg, Item itmEvent)
        {
            string sql = "";
            string sql_update = "";
            Item itmSQL = null;

            string program_id = itmEvent.getProperty("source_id", "");
            string next_event_id = itmEvent.getID();

            sql = @"
                UPDATE 
                    IN_MEETING_PEVENT 
                SET
                    in_next_lose = '{#next_id}'
                    , in_next_foot_lose = '{#next_foot}'
                    , in_tree_rank = 'rank35'
                    , in_tree_rank_ns = '3,5'
                    , in_tree_rank_nss = '3,5'
                WHERE
                    source_id = '{#program_id}'
                    AND in_tree_name = N'repechage'
                    AND in_round_code = '4'
                    AND in_round_id = '{#in_round_id}'
            ";

            // 五六名場-籤腳 1 連結到 復活線-準決賽-第 1 場
            sql_update = sql.Replace("{#program_id}", program_id)
                .Replace("{#in_round_id}", "1")
                .Replace("{#next_id}", next_event_id)
                .Replace("{#next_foot}", "1");

            itmSQL = cfg.inn.applySQL(sql_update);


            // 五六名場-籤腳 2 連結到 復活線-準決賽-第 2 場
            sql_update = sql.Replace("{#program_id}", program_id)
                .Replace("{#in_round_id}", "2")
                .Replace("{#next_id}", next_event_id)
                .Replace("{#next_foot}", "2");

            itmSQL = cfg.inn.applySQL(sql_update);
        }


        //建立 L 下一場 id 連結
        private void Rank78LinkRpcSemiSemi(TConfig cfg, Item itmEvent)
        {
            string sql = "";
            string sql_update = "";
            Item itmSQL = null;

            string program_id = itmEvent.getProperty("source_id", "");
            string next_event_id = itmEvent.getID();

            sql = @"
                UPDATE 
                    IN_MEETING_PEVENT 
                SET
                    in_next_lose = '{#next_id}'
                    , in_next_foot_lose = '{#next_foot}'
                    , in_tree_rank = 'rank57'
                    , in_tree_rank_ns = '0,7'
                    , in_tree_rank_nss = '0,7'
                WHERE
                    source_id = '{#program_id}'
                    AND in_tree_name = 'repechage'
                    AND in_round_code = '8'
                    AND in_round_id = '{#in_round_id}'
            ";

            // 七八名場-籤腳 1 連結到 復活線-準決賽前一場-第 1 場
            sql_update = sql.Replace("{#program_id}", program_id)
                .Replace("{#in_round_id}", "1")
                .Replace("{#next_id}", next_event_id)
                .Replace("{#next_foot}", "1");

            itmSQL = cfg.inn.applySQL(sql_update);


            // 七八名場-籤腳 2 連結到 復活線-準決賽前一場-第 2 場
            sql_update = sql.Replace("{#program_id}", program_id)
                .Replace("{#in_round_id}", "2")
                .Replace("{#next_id}", next_event_id)
                .Replace("{#next_foot}", "2");

            itmSQL = cfg.inn.applySQL(sql_update);
        }

        #endregion 四柱復活演算

        #region 挑戰賽
        private void AddChallengeEvents(TConfig cfg)
        {
            //最終決賽(如果決賽的第一名輸了，要多打這一場)
            TEvent evt_real_final = new TEvent
            {
                in_meeting = cfg.meeting_id,
                source_id = cfg.program_id,
                in_date_key = cfg.in_fight_day,
                in_site = cfg.in_site,

                in_tree_name = "challenge-b",
                in_tree_sort = 400,
                in_tree_id = "CB01",
                in_tree_no = "1",
                in_tree_rank = "rank12",
                in_tree_rank_ns = "1,2",
                in_tree_rank_nss = "1,2",
                in_tree_alias = "挑戰賽-最終決賽",
                in_round = 1,
                in_round_code = 2,
                in_round_id = 1,
                in_sign_code = 2,
                in_sign_no = 1,
                in_next_win = "",
                in_next_foot_win = "",
                in_next_lose = "",
                in_next_foot_lose = "",
                in_period = 8,
            };
            ApplyEvent(cfg, evt_real_final);

            //決賽
            TEvent evt_final = new TEvent
            {
                in_meeting = cfg.meeting_id,
                source_id = cfg.program_id,
                in_date_key = cfg.in_fight_day,
                in_site = cfg.in_site,

                in_tree_name = "challenge-a",
                in_tree_sort = 300,
                in_tree_id = "CA02",
                in_tree_no = "2",
                in_tree_rank = "rank12",
                in_tree_rank_ns = "1,2",
                in_tree_rank_nss = "1,2",
                in_tree_alias = "挑戰賽-決賽",
                in_round = 2,
                in_round_code = 2,
                in_round_id = 1,
                in_sign_code = 2,
                in_sign_no = 1,
                in_next_win = "",
                in_next_foot_win = "",
                in_next_lose = "",
                in_next_foot_lose = "",
                in_period = 4,
            };
            ApplyEvent(cfg, evt_final);

            //挑戰賽
            TEvent evt_challenge = new TEvent
            {
                in_meeting = cfg.meeting_id,
                source_id = cfg.program_id,
                in_date_key = cfg.in_fight_day,
                in_site = cfg.in_site,

                in_tree_name = "challenge-a",
                in_tree_sort = 300,
                in_tree_id = "CA01",
                in_tree_no = "1",
                in_tree_rank = "rank23",
                in_tree_rank_ns = "2,3",
                in_tree_rank_nss = "2,3",
                in_tree_alias = "挑戰賽",
                in_round = 1,
                in_round_code = 4,
                in_round_id = 1,
                in_sign_code = 2,
                in_sign_no = 1,
                in_next_win = evt_final.id,
                in_next_foot_win = "2",
                in_period = 4,
            };
            ApplyEvent(cfg, evt_challenge);

            string sql_upd = "";
            Item itmUpd = null;

            //第一名到 CA02 白方 (勝部決賽)
            //第二名到 CA01 白方 (勝部決賽)
            //第三名到 CA01 藍方 (敗部決賽)

            sql_upd = "UPDATE IN_MEETING_PEVENT SET"
                + " in_next_win = '" + evt_final.id + "'"
                + ", in_next_foot_win = '1'"
                + ", in_next_lose = '" + evt_challenge.id + "'"
                + ", in_next_foot_lose = '1' "
                + " WHERE source_id = '" + cfg.program_id + "'"
                + " AND in_tree_name = 'main'"
                + " AND in_round_code = 2";

            itmUpd = cfg.inn.applySQL(sql_upd);

            sql_upd = "UPDATE IN_MEETING_PEVENT SET"
                + " in_next_win = '" + evt_challenge.id + "'"
                + ", in_next_foot_win = '2'"
                + " WHERE source_id = '" + cfg.program_id + "'"
                + " AND (in_tree_name = 'repechage' OR in_tree_name = 'rank34')"
                + " AND in_round_code = 2";

            itmUpd = cfg.inn.applySQL(sql_upd);

        }
        #endregion 挑戰賽

        #region 場次共用

        //新增賽程場次
        private void ApplyEvent(TConfig cfg, TEvent evt)
        {
            Item itmEvent = cfg.inn.newItem("In_Meeting_PEvent", "add");
            itmEvent.setProperty("in_meeting", evt.in_meeting);
            itmEvent.setProperty("source_id", evt.source_id);
            itmEvent.setProperty("in_date_key", evt.in_date_key);
            itmEvent.setProperty("in_site", evt.in_site);

            itmEvent.setProperty("in_tree_name", evt.in_tree_name);
            itmEvent.setProperty("in_tree_sort", evt.in_tree_sort.ToString());
            itmEvent.setProperty("in_tree_id", evt.in_tree_id);
            itmEvent.setProperty("in_tree_no", evt.in_tree_no);
            itmEvent.setProperty("in_tree_rank", evt.in_tree_rank);
            itmEvent.setProperty("in_tree_rank_ns", evt.in_tree_rank_ns);
            itmEvent.setProperty("in_tree_rank_nss", evt.in_tree_rank_nss);
            itmEvent.setProperty("in_tree_alias", evt.in_tree_alias);

            itmEvent.setProperty("in_bypass_foot", evt.in_bypass_foot);
            itmEvent.setProperty("in_bypass_status", evt.in_bypass_status);

            itmEvent.setProperty("in_round", evt.in_round.ToString());
            itmEvent.setProperty("in_round_code", evt.in_round_code.ToString());
            itmEvent.setProperty("in_round_id", evt.in_round_id.ToString());

            itmEvent.setProperty("in_sign_code", evt.in_sign_code.ToString());
            itmEvent.setProperty("in_sign_no", evt.in_sign_no.ToString());

            itmEvent.setProperty("in_next_win", evt.in_next_win);
            itmEvent.setProperty("in_next_foot_win", evt.in_next_foot_win);

            itmEvent.setProperty("in_period", evt.in_period.ToString());

            itmEvent.setProperty("in_fight_id", GetFightId(cfg, evt));

            itmEvent = itmEvent.apply();

            if (itmEvent.isError())
            {
                throw new Exception("建立賽事組別場次發生失敗");
            }

            evt.id = itmEvent.getID();
        }

        private string GetFightId(TConfig cfg, TEvent evt)
        {
            var result = "";
            var a = evt.in_round_code.ToString().PadLeft(3, '0');
            var b = evt.in_tree_id.Substring(evt.in_tree_id.Length - 2, 2).PadLeft(2, '0');
            var code = a + "-" + b;

            switch (evt.in_tree_name)
            {
                case "main": result = "M" + code; break;
                case "repechage": result = "R" + code; break;
                case "challenge-a": result = evt.in_tree_id; break;
                case "challenge-b": result = evt.in_tree_id; break;
                case "ka01": result = "KA01"; break;
                case "kb01": result = "KB01"; break;
            }

            return result;
        }

        //取得場次狀態
        private TEventStatus GetEventStatus(int event_sign, int event_code, TTree tree)
        {
            TStatus event_status = GetSignStatus(event_sign, event_code, tree);
            TStatus n1_status = GetSignStatus(event_status.N1, event_code * 2, tree);
            TStatus n2_status = GetSignStatus(event_status.N2, event_code * 2, tree);

            TEventStatus result = new TEventStatus
            {
                Signs = new List<TSign>
        {
            new TSign{ FootNo = 1, SignNo = event_status.N1 },
            new TSign{ FootNo = 2, SignNo = event_status.N2 },
        }
            };

            if (event_status.IsByPass)
            {
                result.ByPassFoot = event_status.ByPassFoot.ToString();
            }
            else
            {
                result.ByPassFoot = "";
            }

            if (n1_status.IsByPass && n2_status.IsByPass)
            {
                result.ByPassStatus = "4";
            }
            else if (n1_status.IsByPass)
            {
                result.ByPassStatus = "2";
            }
            else if (n2_status.IsByPass)
            {
                result.ByPassStatus = "2";
            }
            else
            {
                result.ByPassStatus = "1";
            }
            return result;
        }

        //取得籤位狀態
        private TStatus GetSignStatus(int no, int code, TTree tree)
        {
            int sum = code * 2 + 1;
            int n1 = no % 2 > 0 ? no : (sum - no);
            int n2 = sum - n1;

            TStatus result = new TStatus();
            result.N1 = n1;
            result.N2 = n2;

            if (n1 > tree.TeamCount && n2 > tree.TeamCount)
            {
                //兩腳輪空
                result.IsByPass = false;
                result.ByPassFoot = 3;
            }
            else if (n1 > tree.TeamCount)
            {
                //籤腳 1 為輪空
                result.IsByPass = true;
                result.ByPassFoot = 1;
            }
            else if (n2 > tree.TeamCount)
            {
                //籤腳 2 為輪空
                result.IsByPass = true;
                result.ByPassFoot = 2;
            }
            else
            {
                //無輪空
                result.IsByPass = false;
                result.ByPassFoot = 0;
            }

            return result;
        }

        /// <summary>
        /// 取得樹圖序號
        /// </summary>
        /// <param name="code">輪次代碼</param>
        /// <param name="round">回合</param>
        /// <param name="id">序號</param>
        /// <returns></returns>
        private string GetTreeId(string code, int round, int id)
        {
            return code + round + id.ToString().PadLeft(2, '0');
        }

        /// <summary>
        /// 取得場次別名
        /// </summary>
        private string GetTreeAlias(int value)
        {
            if (value == 2)
            {
                return "決賽"; //Final
            }
            else if (value == 4)
            {
                return "準決賽"; //Semi-Final
            }
            else if (value == 8)
            {
                return "半準決賽"; //Quarter-Final
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// 取得組面名稱
        /// </summary>
        private string GetSurfaceName(int value, string type = "")
        {
            string[] arr;

            switch (type)
            {
                default:
                    arr = new string[] { "", "A面", "B面", "C面", "D面", "E面", "F面" };
                    break;
            }

            if (value > -1 && value < (arr.Length))
            {
                return arr[value];
            }
            else
            {
                return string.Empty;
            }
        }

        #endregion 場次共用

        #region 場次明細

        //新增場次明細
        private void AddEventDetails(TConfig cfg)
        {
            var team_count = GetInt(cfg.itmProgram.getProperty("in_team_count", ""));

            var items = cfg.inn.applySQL("SELECT * FROM IN_MEETING_PEVENT WITH(NOLOCK) WHERE source_id = '" + cfg.program_id + "' ");

            int count = items.getItemCount();
            int code = 2;
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string id = item.getProperty("id", "");
                string[] ns = GetSignNos(item);

                for (int j = 1; j <= code; j++)
                {
                    string in_sign_no = "";
                    string in_sign_foot = "";
                    string in_target_no = "";
                    string in_target_foot = "";

                    if (j % 2 != 0)
                    {
                        in_sign_no = ns[0];
                        in_sign_foot = "1";
                        in_target_no = ns[1];
                        in_target_foot = "2";
                    }
                    else
                    {
                        in_sign_no = ns[1];
                        in_sign_foot = "2";
                        in_target_no = ns[0];
                        in_target_foot = "1";
                    }


                    int sign_no = GetInt(in_sign_no);
                    string in_sign_bypass = sign_no > team_count ? "1" : "0";

                    Item itmDetail = cfg.inn.newItem("In_Meeting_PEvent_Detail", "add");
                    itmDetail.setProperty("source_id", id);
                    itmDetail.setProperty("in_sign_foot", in_sign_foot);
                    itmDetail.setProperty("in_sign_no", in_sign_no);
                    itmDetail.setProperty("in_sign_bypass", in_sign_bypass);

                    itmDetail.setProperty("in_target_no", in_target_no);
                    itmDetail.setProperty("in_target_foot", in_target_foot);
                    itmDetail.setProperty("in_meeting", cfg.meeting_id);

                    itmDetail.apply();
                }
            }
        }

        //取得對應籤號
        private string[] GetSignNos(Item item)
        {
            string[] result = new string[] { "", "" };

            string in_tree_name = item.getProperty("in_tree_name", "");

            if (in_tree_name != "main")
            {
                return result;
            }

            string in_round = item.getProperty("in_round", "");
            if (in_round != "1")
            {
                return result;
            }

            //__人對戰(回合代碼) = 籤數 * 2
            string in_round_code = item.getProperty("in_round_code", "");
            //回合序號 (排序)
            string in_round_id = item.getProperty("in_round_id", "");
            //籤數
            string in_sign_code = item.getProperty("in_sign_code", "");
            //籤號
            string in_sign_no = item.getProperty("in_sign_no", "");

            int round_code = 0;
            Int32.TryParse(in_round_code, out round_code);

            int sign_no = 0;
            Int32.TryParse(in_sign_no, out sign_no);

            int round_sum = round_code + 1;

            int target_no = round_sum - sign_no;
            if (sign_no % 2 == 0)
            {
                return new string[] { target_no.ToString(), sign_no.ToString() };
            }
            else
            {
                return new string[] { sign_no.ToString(), target_no.ToString() };
            }
        }
        #endregion 場次明細

        #region 資料結構

        /// <summary>
        /// 場次資料結構
        /// </summary>
        private class TEvent
        {
            /// <summary>
            /// 賽程 id
            /// </summary>
            public string in_meeting { get; set; }

            /// <summary>
            /// 賽程組別 id
            /// </summary>
            public string source_id { get; set; }

            /// <summary>
            /// 賽程場次 id
            /// </summary>
            public string id { get; set; }

            /// <summary>
            /// 樹圖名稱
            /// </summary>
            public string in_tree_name { get; set; }

            /// <summary>
            /// 樹圖排序
            /// </summary>
            public int in_tree_sort { get; set; }

            /// <summary>
            /// 樹圖序號(空間)
            /// </summary>
            public string in_tree_id { get; set; }

            /// <summary>
            /// 樹圖序號(時間)
            /// </summary>
            public string in_tree_no { get; set; }

            /// <summary>
            /// 樹圖名次
            /// </summary>
            public string in_tree_rank { get; set; }

            /// <summary>
            /// 樹圖名次清單 (排序)
            /// </summary>
            public string in_tree_rank_ns { get; set; }

            /// <summary>
            /// 樹圖名次清單 (show)
            /// </summary>
            public string in_tree_rank_nss { get; set; }

            /// <summary>
            /// 樹圖別名
            /// </summary>
            public string in_tree_alias { get; set; }

            /// <summary>
            /// 輪空籤腳
            /// </summary>
            public string in_bypass_foot { get; set; }

            /// <summary>
            /// 輪空狀態
            /// </summary>
            public string in_bypass_status { get; set; }

            /// <summary>
            /// 回合
            /// </summary>
            public int in_round { get; set; }

            /// <summary>
            /// 回合代碼
            /// </summary>
            public int in_round_code { get; set; }

            /// <summary>
            /// 回合序號
            /// </summary>
            public int in_round_id { get; set; }

            /// <summary>
            /// 場次籤數
            /// </summary>
            public int in_sign_code { get; set; }

            /// <summary>
            /// 場次籤號
            /// </summary>
            public int in_sign_no { get; set; }

            /// <summary>
            /// w 下一場 id (父場次)
            /// </summary>
            public string in_next_win { get; set; }

            /// <summary>
            /// w 下一場籤腳 (父場次)
            /// </summary>
            public string in_next_foot_win { get; set; }

            /// <summary>
            /// l 下一場 id (父場次)
            /// </summary>
            public string in_next_lose { get; set; }

            /// <summary>
            /// l 下一場籤腳 (父場次)
            /// </summary>
            public string in_next_foot_lose { get; set; }

            /// <summary>
            /// 子籤號清單
            /// </summary>
            public List<TSign> Signs { get; set; }

            /// <summary>
            /// 比賽日期
            /// </summary>
            public string in_date_key { get; set; }

            /// <summary>
            /// 比賽場地 id
            /// </summary>
            public string in_site { get; set; }

            /// <summary>
            /// 賽別類型(1: 預賽、4: 決賽、8: 加賽)
            /// </summary>
            public int in_period { get; set; }

        }

        /// <summary>
        /// 場次狀態
        /// </summary>
        private class TEventStatus
        {
            /// <summary>
            /// 輪空籤腳
            /// </summary>
            public string ByPassFoot { get; set; }

            /// <summary>
            /// 輪空狀態
            /// </summary>
            public string ByPassStatus { get; set; }

            /// <summary>
            /// 子籤號清單
            /// </summary>
            public List<TSign> Signs { get; set; }
        }

        /// <summary>
        /// 籤位狀態
        /// </summary>
        private class TSign
        {
            public int FootNo { get; set; }

            public int SignNo { get; set; }
        }

        /// <summary>
        /// 狀態
        /// </summary>
        private class TStatus
        {
            /// <summary>
            /// 腳 1 籤號
            /// </summary>
            public int N1 { get; set; }

            /// <summary>
            /// 腳 2 籤號
            /// </summary>
            public int N2 { get; set; }

            /// <summary>
            /// 是否單腳輪空
            /// </summary>
            public bool IsByPass { get; set; }

            /// <summary>
            /// 輪空的籤腳 (0: 無輪空、3: 兩腳輪空)
            /// </summary>
            public int ByPassFoot { get; set; }
        }

        /// <summary>
        /// 樹圖結構
        /// </summary>
        private class TTree
        {
            /// <summary>
            /// 賽程 id
            /// </summary>
            public string MeetingId { get; set; }

            /// <summary>
            /// 賽程組別 id
            /// </summary>
            public string ProgramId { get; set; }

            /// <summary>
            /// 隊伍數
            /// </summary>
            public int TeamCount { get; set; }

            /// <summary>
            /// Repechage 隊伍數
            /// </summary>
            public int RpcTeamCount { get; set; }

            /// <summary>
            /// 樹圖代碼
            /// </summary>
            public string TreeCode { get; set; }

            /// <summary>
            /// 樹圖名稱
            /// </summary>
            public string TreeName { get; set; }

            /// <summary>
            /// 樹圖排序
            /// </summary>
            public int TreeSort { get; set; }

            /// <summary>
            /// 取得最大輪次
            /// </summary>
            public int MaxRound { get; set; }

            /// <summary>
            /// 取得最大輪次代碼
            /// </summary>
            public int MaxCode { get; set; }

            /// <summary>
            /// 比賽日期
            /// </summary>
            public string FightDay { get; set; }

            /// <summary>
            /// 比賽場地 id
            /// </summary>
            public string SiteId { get; set; }
        }

        #endregion 資料結構

        #region 籤號算法

        /// <summary>
        /// 取得最大輪次代碼
        /// </summary>
        private static int[] GetRoundAndSum(int value, int code, int sum, int round)
        {
            if (value == 0)
            {
                return new int[] { 0, 0 };
            }
            else if (value > sum)
            {
                return GetRoundAndSum(value, code, code * sum, round + 1);
            }
            else
            {
                return new int[] { round, sum };
            }
        }

        /// <summary>
        /// 取得籤號陣列
        /// </summary>
        private int[] GetTkdNoArray(int code)
        {
            List<TKdNode> nodes = new List<TKdNode>();

            nodes.Add(new TKdNode { No = 1, IsEven = false });

            for (int i = 2; i <= code; i++)
            {
                TKdNode node = new TKdNode
                {
                    No = i,
                    IsEven = (i % 2) == 0
                };
                AppendTreeNode(nodes, node);
            }

            return nodes.Select(x => x.No).ToArray();
        }

        /// <summary>
        /// 附加節點
        /// </summary>
        private void AppendTreeNode(List<TKdNode> tree, TKdNode node)
        {
            //當前節點的落點
            int[] rs = GetRoundAndSum(node.No, 2, 2, 1);
            int code = rs[1];
            int sum = code + 1;

            //尋找對手
            int target_no = sum - node.No;
            int target_pos = -1;

            for (int i = 0; i < tree.Count; i++)
            {
                TKdNode x = tree[i];
                if (x.No == target_no)
                {
                    target_pos = i;
                }
            }

            //找不到對手節點
            if (target_pos == -1)
            {
                return;
            }
            else
            {
                if (node.IsEven)
                {
                    tree.Insert(target_pos + 1, node);
                }
                else
                {
                    tree.Insert(target_pos, node);
                }
            }
        }

        /// <summary>
        /// 節點結構
        /// </summary>
        private class TKdNode
        {
            /// <summary>
            /// 籤號
            /// </summary>
            public int No { get; set; }
            /// <summary>
            /// 是否為偶數
            /// </summary>
            public bool IsEven { get; set; }
        }

        #endregion 籤號算法


        //取得賽事資訊
        private Item GetMeeting(TConfig cfg)
        {
            var list = new List<string>
            {
                "in_title",
                "in_battle_type",
                "in_battle_repechage",
                "in_rank_type",
                "in_surface_code",
                "in_site_mode",
            };

            string aml = "<AML><Item type='In_Meeting' action='get' id='{#meeting_id}' select='{#cols}'></Item></AML>";

            aml = aml.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#cols}", string.Join(",", list));

            return cfg.inn.applyAML(aml);
        }

        //取得賽程組別資訊
        private Item GetProgram(TConfig cfg)
        {
            string sql = @"SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '{#program_id}'";
            sql = sql.Replace("{#program_id}", cfg.program_id);
            return cfg.inn.applySQL(sql);
        }

        //取得賽程日期資訊
        private Item GetAllocation(TConfig cfg)
        {
            string sql = @"SELECT * FROM IN_MEETING_ALLOCATION WITH(NOLOCK) WHERE in_program = '{#program_id}'";
            sql = sql.Replace("{#program_id}", cfg.program_id);
            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }

            public string meeting_id { get; set; }
            public string program_id { get; set; }
            public string battle_type { get; set; }
            public string scene { get; set; }

            public Item itmMeeting { get; set; }
            public Item itmProgram { get; set; }

            public string in_site_mode { get; set; }
            public string in_battle_repechage { get; set; }

            public string in_rank_type { get; set; }
            public string in_team_count { get; set; }
            public string in_round_count { get; set; }
            public string in_fight_day { get; set; }
            public string in_site { get; set; }
            public string in_l1 { get; set; }
            public string in_sub_event { get; set; }

            public int team_count { get; set; }
            public int round_count { get; set; }
            public int sub_event { get; set; }
        }

        private int GetInt(string value, int def = 0)
        {
            int result = def;
            int.TryParse(value, out result);
            return result;
        }
    }
}