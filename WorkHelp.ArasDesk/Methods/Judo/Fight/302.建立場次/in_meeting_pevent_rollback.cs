﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Judo.Fight
{
    public class in_meeting_pevent_rollback : Item
    {
        public in_meeting_pevent_rollback(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
    目的: 清除勝出狀態
    日誌: 
        - 2023-04-10: 創建 (lina)
*/

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "in_meeting_pevent_rollback";

            var itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                strIdentityId = inn.getUserAliases(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                program_id = itmR.getProperty("program_id", ""),
                scene = itmR.getProperty("scene", ""),
            };

            // cfg.meeting_id = "61EBAE60D44A4E9C99C0789CB2C5BC40";
            // cfg.program_id = "EAAEAF9EE7684A92A3CB6E8257E3BF38";

            Rollback(cfg, itmR);

            return itmR;
        }

        private void Rollback(TConfig cfg, Item itmReturn)
        {
            ClearPlayerRank(cfg);
            ClearNonMainEventDetail(cfg);
            ClearNonMainEvents(cfg);

            ClearNonRound1MainEventDetail(cfg);
            ClearRound1MainEventDetail(cfg);
            ClearMainEvents(cfg);

            FixMainRound2(cfg);
            FixFightId(cfg);
        }

        private void FixFightId(TConfig cfg)
        {
            var itmProgram = cfg.inn.applySQL("SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + cfg.program_id + "'");
            var in_battle_type = itmProgram.getProperty("in_battle_type", "");
            var is_robin = in_battle_type.Contains("Robin");

            var sql = @"
                SELECT 
	                *
                FROM 
	                IN_MEETING_PEVENT WITH(NOLOCK) 
                WHERE 
	                source_id = '{#program_id}'
                ORDER BY 
	                in_tree_sort
                    , in_tree_id
            ";

            sql = sql.Replace("{#program_id}", cfg.program_id);

            var itmEvents = cfg.inn.applySQL(sql);
            var count = itmEvents.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var itmEvent = itmEvents.getItemByIndex(i);
                var eid = itmEvent.getProperty("id", "");

                var in_fight_id = is_robin
                    ? GetFightIdRobin(cfg, itmProgram, itmEvent)
                    : GetFightIdTopTwo(cfg, itmProgram, itmEvent);

                var sql_upd = "UPDATE IN_MEETING_PEVENT SET"
                    + " in_fight_id = '" + in_fight_id + "'"
                    + " WHERE id = '" + eid + "'";

                cfg.inn.applySQL(sql_upd);
            }
        }

        private string GetFightIdTopTwo(TConfig cfg, Item itmProgram, Item itmEvent)
        {
            string in_tree_name = itmEvent.getProperty("in_tree_name", "");
            string in_tree_id = itmEvent.getProperty("in_tree_id", "");
            string in_round_code = itmEvent.getProperty("in_round_code", "");

            var result = "";
            var a = in_round_code.ToString().PadLeft(3, '0');
            var b = in_tree_id.Substring(in_tree_id.Length - 2, 2).PadLeft(2, '0');
            var code = a + "-" + b;

            switch (in_tree_name)
            {
                case "main": result = "M" + code; break;
                case "repechage": result = "R" + code; break;
                case "challenge-a": result = in_tree_id.ToUpper(); break;
                case "challenge-b": result = in_tree_id.ToUpper(); break;
                case "ka01": result = "KA01"; break;
                case "kb01": result = "KB01"; break;
                case "rank34": result = "RNK34-01"; break;
                case "rank56": result = "RNK56-01"; break;
                case "rank78": result = "RNK78-01"; break;
            }

            return result;
        }

        private string GetFightIdRobin(TConfig cfg, Item itmProgram, Item itmEvent)
        {
            string in_team_count = itmProgram.getProperty("in_team_count", "");
            string in_tree_id = itmEvent.getProperty("in_tree_id", "");
            var a = "RBN" + in_team_count;
            var b = in_tree_id.Substring(in_tree_id.Length - 2, 2).PadLeft(2, '0');
            return a + "-" + b;
        }

        private void FixMainRound2(TConfig cfg)
        {
            var sql = @"
                SELECT 
	                *
                FROM 
	                IN_MEETING_PEVENT WITH(NOLOCK) 
                WHERE 
	                source_id = '{#program_id}'
	                AND in_tree_name = 'main'
	                AND in_round = 1
	                AND ISNULL(in_tree_no, 0) = 0
                ORDER BY 
	                in_tree_id
            ";

            sql = sql.Replace("{#program_id}", cfg.program_id);

            var itmEvents = cfg.inn.applySQL(sql);
            var count = itmEvents.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var itmEvent = itmEvents.getItemByIndex(i);
                FixMainRound2(cfg, itmEvent);
            }
        }

        private void FixMainRound2(TConfig cfg, Item itmEvent)
        {
            var eid = itmEvent.getProperty("id", "");
            var itmDetails = cfg.inn.applySQL("SELECT * FROM IN_MEETING_PEVENT_DETAIL WITH(NOLOCK) WHERE source_id = '" + eid + "' ORDER BY in_sign_foot");
            if (itmDetails.isError() || itmDetails.getResult() == "") return;

            var count = itmDetails.getItemCount();
            if (count != 2) return;
            var itmFoot1 = default(Item);
            var itmFoot2 = default(Item);
            for (var i = 0; i < count; i++)
            {
                var itmDetail = itmDetails.getItemByIndex(i);
                var foot = itmDetail.getProperty("in_sign_foot", "");
                if (foot == "1") itmFoot1 = itmDetail;
                else if (foot == "2") itmFoot2 = itmDetail;
            }
            if (itmFoot1 == null) itmFoot1 = cfg.inn.newItem();
            if (itmFoot2 == null) itmFoot2 = cfg.inn.newItem();

            AnalysisRepechageEvent(cfg, itmEvent, itmFoot1, itmFoot2);

        }

        private void AnalysisRepechageEvent(TConfig cfg, Item itmEvent, Item itmFoot1, Item itmFoot2)
        {
            var f1_sign_no = itmFoot1.getProperty("in_sign_no", "");
            var f2_sign_no = itmFoot2.getProperty("in_sign_no", "");
            var f1_team_id = GetTeamId(cfg, cfg.program_id, f1_sign_no);
            var f2_team_id = GetTeamId(cfg, cfg.program_id, f2_sign_no);

            var in_next_win = itmEvent.getProperty("in_next_win", "");
            var in_next_foot_win = itmEvent.getProperty("in_next_foot_win", "");

            if (f1_team_id != "" && f2_team_id != "")
            {
                //不處理
            }
            else if (f1_team_id != "")
            {
                ResetNextEventLink(cfg, in_next_win, in_next_foot_win, f1_sign_no);
            }
            else if (f2_team_id != "")
            {
                ResetNextEventLink(cfg, in_next_win, in_next_foot_win, f2_sign_no);
            }
            else
            {
                //Cancel ?
            }
        }

        //連結到下一場
        private void ResetNextEventLink(TConfig cfg, string eid, string foot, string no)
        {
            if (eid == "" || foot == "") return;

            var sql = "";

            var sign_no = no == "" || no == "0" ? "NULL" : "'" + no + "'";
            var assigned = no == "" || no == "0" ? "NULL" : "'1'";
            var oppt_foot = foot == "1" ? "2" : "1";

            sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET"
                + "  in_sign_no = " + sign_no
                + ", in_assigned = " + assigned
                + " WHERE source_id = '" + eid + "'"
                + " AND in_sign_foot = " + foot;

            cfg.inn.applySQL(sql);

            sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET"
                + "  in_target_no = " + sign_no
                + " WHERE source_id = '" + eid + "'"
                + " AND in_sign_foot = " + oppt_foot;

            cfg.inn.applySQL(sql);
        }

        private string GetTeamId(TConfig cfg, string program_id, string in_sign_no)
        {
            if (in_sign_no == "" || in_sign_no == "0") return "";

            var sql = "SELECT id FROM IN_MEETING_PTEAM WITH(NOLOCK)"
                + " WHERE source_id = '" + program_id + "'"
                + " AND in_sign_no = '" + in_sign_no + "'";

            var itemTeam = cfg.inn.applySQL(sql);
            if (itemTeam.isError() || itemTeam.getResult() == "")
            {
                return "";
            }
            else
            {
                return itemTeam.getProperty("id", "");
            }
        }

        private void ClearMainEvents(TConfig cfg)
        {
            var sql = @"
                UPDATE IN_MEETING_PEVENT SET 
	                  in_win_creator = NULL
	                , in_win_creator_sno = NULL
	                , in_win_status = NULL
	                , in_win_sign_no = NULL
	                , in_win_time = NULL
	                , in_win_local = NULL
	                , in_win_local_time = NULL
	                , in_cancel = NULL
                WHERE source_id = '{#program_id}'
                AND in_tree_name IN ('main')
                AND ISNULL(in_tree_no, 0) > 0
            ";

            sql = sql.Replace("{#program_id}", cfg.program_id);

            cfg.inn.applySQL(sql);
        }

        private void ClearRound1MainEventDetail(TConfig cfg)
        {
            var sql = @"
                UPDATE IN_MEETING_PEVENT_DETAIL SET
	                 in_sign_action = NULL
	                , in_status = NULL
	                , in_points = NULL
	                , in_points_type = NULL
	                , in_points_text = NULL
	                , in_correct_count = NULL
	                , in_player_org = NULL
	                , in_player_name = NULL
	                , in_player_sno = NULL
	                , in_player_status = NULL
	                , in_player_team = NULL
	                , in_assigned = NULL
                WHERE source_id IN (
	                SELECT id FROM IN_MEETING_PEVENT WITH(NOLOCK) 
	                WHERE source_id = '{#program_id}'
	                AND in_tree_name IN ('main')
                    AND ISNULL(in_tree_no, 0) > 0
                    AND in_round = 1
                )
            ";

            sql = sql.Replace("{#program_id}", cfg.program_id);

            cfg.inn.applySQL(sql);
        }

        private void ClearNonRound1MainEventDetail(TConfig cfg)
        {
            var sql = @"
                UPDATE IN_MEETING_PEVENT_DETAIL SET
	                  in_sign_no = NULL
	                , in_target_no = NULL
	                , in_sign_bypass = NULL
	                , in_target_bypass = NULL
	                , in_sign_action = NULL
	                , in_status = NULL
	                , in_points = NULL
	                , in_points_type = NULL
	                , in_points_text = NULL
	                , in_correct_count = NULL
	                , in_player_org = NULL
	                , in_player_name = NULL
	                , in_player_sno = NULL
	                , in_player_status = NULL
	                , in_player_team = NULL
	                , in_assigned = NULL
                WHERE source_id IN (
	                SELECT id FROM IN_MEETING_PEVENT WITH(NOLOCK) 
	                WHERE source_id = '{#program_id}'
	                AND in_tree_name IN ('main')
                    AND ISNULL(in_tree_no, 0) > 0
                    AND in_round > 1
                )
            ";

            sql = sql.Replace("{#program_id}", cfg.program_id);

            cfg.inn.applySQL(sql);
        }

        private void ClearNonMainEvents(TConfig cfg)
        {
            var sql = @"
                UPDATE IN_MEETING_PEVENT SET 
	                  in_win_creator = NULL
	                , in_win_creator_sno = NULL
	                , in_win_status = NULL
	                , in_win_sign_no = NULL
	                , in_win_time = NULL
	                , in_win_local = NULL
	                , in_win_local_time = NULL
	                , in_cancel = NULL
                WHERE source_id = '{#program_id}'
                AND in_tree_name NOT IN ('main')
            ";

            sql = sql.Replace("{#program_id}", cfg.program_id);

            cfg.inn.applySQL(sql);
        }

        private void ClearNonMainEventDetail(TConfig cfg)
        {
            var sql = @"
                UPDATE IN_MEETING_PEVENT_DETAIL SET
	                  in_sign_no = NULL
	                , in_target_no = NULL
	                , in_sign_bypass = NULL
	                , in_target_bypass = NULL
	                , in_sign_action = NULL
	                , in_status = NULL
	                , in_points = NULL
	                , in_points_type = NULL
	                , in_points_text = NULL
	                , in_correct_count = NULL
	                , in_player_org = NULL
	                , in_player_name = NULL
	                , in_player_sno = NULL
	                , in_player_status = NULL
	                , in_player_team = NULL
	                , in_assigned = NULL
                WHERE source_id IN (
	                SELECT id FROM IN_MEETING_PEVENT WITH(NOLOCK) 
	                WHERE source_id = '{#program_id}'
	                AND in_tree_name NOT IN ('main')
                )
            ";

            sql = sql.Replace("{#program_id}", cfg.program_id);

            cfg.inn.applySQL(sql);
        }

        private void ClearPlayerRank(TConfig cfg)
        {
            var sql = "UPDATE IN_MEETING_PTEAM SET"
                + "  in_final_rank = NULL"
                + ", in_show_rank = NULL"
                + ", in_final_count = NULL"
                + ", in_final_points = NULL"
                + ", in_event = NULL"
                + ", in_foot = NULL"
                + " WHERE source_id = '" + cfg.program_id + "'";

            cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }

            public string meeting_id { get; set; }
            public string program_id { get; set; }
            public string scene { get; set; }
        }
    }
}