﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Judo.Fight
{
    public class in_meeting_pevent_reset : Item
    {
        public in_meeting_pevent_reset(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 重設所有場次
                輸入: 
                    - meeting_id
                    - program_id
                日期: 
                    - 2021-12-01: 循環賽清資料 (lina)
                    - 2021-02-04: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]in_meeting_pevent_reset";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "input dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                RequestState = RequestState,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                inn = inn,
                meeting_id = itmR.getProperty("meeting_id", ""),
                program_id = itmR.getProperty("program_id", ""),
                in_date = itmR.getProperty("in_date", ""),
                scene = itmR.getProperty("scene", ""),
                is_run = itmR.getProperty("is_run", ""),
                is_build = itmR.getProperty("is_build", ""),
            };

            if (cfg.in_date != "")
            {
                cfg.in_date = cfg.in_date.Replace("/", "-");
            }

            if (cfg.scene == "")
            {
                cfg.scene = "run";
            }

            if (cfg.meeting_id == "")
            {
                itmR.setProperty("error_message", "賽事 id 不得為空白");
                return itmR;
            }

            switch(cfg.scene)
            {
                case "run":
                    RunResetAllStatus(cfg, itmR);
                    break;

                case "reset_event_count":
                    RunResetEventCount(cfg, itmR);
                    break;


            }

            return itmR;
        }

        private void RunResetEventCount(TConfig cfg, Item itmReturn)
        {
            var itmPrograms = GetProgramItems(cfg);
            if (itmPrograms.isError() || itmPrograms.getItemCount() <= 0)
            {
                throw new Exception("組別量級錯誤");
            }

            var count = itmPrograms.getItemCount();

            for (int i = 0; i < count; i++)
            {
                var itmProgram = itmPrograms.getItemByIndex(i);
                var program_id = itmProgram.getProperty("id", "");
                var in_team_yn = itmProgram.getProperty("in_team_yn", "");

                if (in_team_yn == "1")
                {
                    RecountEventsTeam(cfg, program_id);
                }
                else
                {
                    RecountEventsSolo(cfg, program_id);
                }
            }
        }

        private void RunResetAllStatus(TConfig cfg, Item itmReturn)
        {
            var itmDrawMode = cfg.inn.applySQL("SELECT in_value FROM IN_VARIABLE WITH(NOLOCK) WHERE in_name = 'draw_mode'");
            cfg.draw_mode = itmDrawMode.getProperty("in_value", "").ToLower();

            if (cfg.draw_mode == "judo")
            {
                cfg.is_tkd_mode = false;
                cfg.Service = new InnSport.Core.Services.Impl.JudoSportService();
            }
            else
            {
                cfg.is_tkd_mode = true;
                cfg.Service = new InnSport.Core.Services.Impl.TkdSportService();
            }

            //賽會參數
            cfg.variable_map = MapVariable(cfg);

            //重設所有場次
            ResetAllEvents(cfg, itmReturn);
        }

        //重設所有場次
        private void ResetAllEvents(TConfig cfg, Item itmReturn)
        {
            var itmPrograms = GetProgramItems(cfg);
            if (itmPrograms.isError() || itmPrograms.getItemCount() <= 0)
            {
                throw new Exception("組別量級錯誤");
            }

            var count = itmPrograms.getItemCount();

            for (int i = 0; i < count; i++)
            {
                var itmProgram = itmPrograms.getItemByIndex(i);
                var entProgram = new TProgram
                {
                    Id = itmProgram.getProperty("id", ""),
                    InName = itmProgram.getProperty("in_name", ""),
                    InL1 = itmProgram.getProperty("in_l1", ""),
                    InL2 = itmProgram.getProperty("in_l2", ""),
                    InL3 = itmProgram.getProperty("in_l3", ""),
                    BattleType = itmProgram.getProperty("in_battle_type", ""),
                    TeamCount = GetInt(itmProgram.getProperty("in_team_count", "0")),
                    RankCount = GetInt(itmProgram.getProperty("in_rank_count", "0")),
                    RoundCode = GetInt(itmProgram.getProperty("in_round_code", "0")),
                    RoundCount = GetInt(itmProgram.getProperty("in_round_count", "0")),

                    Value = itmProgram,
                };

                entProgram.IsRoundRobin = entProgram.BattleType.Contains("RoundRobin");
                entProgram.IsTeamFight = itmProgram.getProperty("in_team_yn", "") == "1";
                
                ResetOneProgramEvents(cfg, entProgram);
            }
        }

        //重設單一組別所有場次
        private void ResetOneProgramEvents(TConfig cfg, TProgram entProgram)
        {
            //重算報名人數
            UpdateProgramRegisterCount(cfg, entProgram);

            if (entProgram.TeamCount <= 1 || entProgram.IsRoundRobin)
            {
                //重設場次(非重建)
                ResetRobinEvents(cfg, entProgram);

                //重算場次數
                RecountEvents(cfg, entProgram);

                return;
            }

            entProgram.NoList = cfg.Service.TkdNoListByPosition(entProgram.TeamCount);
            entProgram.ByPassNoList = entProgram.NoList.FindAll(x => x.IsByPass).Select(x => x.TkdNo).ToList();

            //重設場次(非重建)
            ResetEvents(cfg, entProgram);

            //勝部
            var map = GetMPEDTs(cfg, entProgram);

            foreach (var kv in map)
            {
                var evt = kv.Value;
                FixEventBypass(cfg, entProgram, evt);
            }

            //更新場次序號(時間)
            UpdateEventTreeNo(cfg, entProgram);

            //更新場次序號(時間) (敗部)
            UpdateRpcEventTreeNo(cfg, entProgram);

            //修正場次序號(時間) (敗部)
            FixRpcEventTreeNo(cfg, entProgram);

            //復原 3, 4 名場次序號
            var need_rank34 = GetNeedRank34(cfg, entProgram);
            if (need_rank34)
            {
                if (entProgram.TeamCount == 4)
                {
                    AssignedRank34TreeNo(cfg, entProgram, "4");
                }
                else if (entProgram.TeamCount == 5)
                {
                    AssignedRank34TreeNo(cfg, entProgram, "5");
                }
                else
                {
                    AssignedRank34TreeNo(cfg, entProgram, "1");
                }
            }

            //復原 5, 6 名場次序號
            var need_rank56 = GetNeedRank56(cfg, entProgram);
            if (need_rank56)
            {
                AssignedRankTreeNo(cfg, entProgram, "rank56", "11");
            }

            //復原 7, 8 名場次序號
            var need_rank78 = GetNeedRank78(cfg, entProgram);
            if (need_rank78)
            {
                AssignedRankTreeNo(cfg, entProgram, "rank78", "14");
            }

            //重算場次數
            RecountEvents(cfg, entProgram);

            //自動晉級(輪空)
            AutoUpgrade(cfg, entProgram.Value);

            // //自動晉級(DQ)
            // AutoUpgradeDQ(cfg, itmProgram);

            if (cfg.is_build != "1")
            {
                //修補日期
                FixDate(cfg, entProgram.Value);
            }
        }

        //重算報名人數
        private void UpdateProgramRegisterCount(TConfig cfg, TProgram entProgram)
        {
            var sql = @"
                SELECT 
	                COUNT(*) AS 'cnt'
                FROM 
	                IN_MEETING_USER WITH(NOLOCK)
                WHERE
	                source_id = '{#meeting_id}'
	                AND ISNULL(in_l1, '') = N'{#in_l1}'
	                AND ISNULL(in_l2, '') = N'{#in_l2}'
	                AND ISNULL(in_l3, '') = N'{#in_l3}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_l1}", entProgram.InL1)
                .Replace("{#in_l2}", entProgram.InL2)
                .Replace("{#in_l3}", entProgram.InL3);

            var itmCount = cfg.inn.applySQL(sql);
            var new_count = "0";
            if (!itmCount.isError() && itmCount.getResult() != "")
            {
                new_count = itmCount.getProperty("cnt", "");
            }

            sql = "UPDATE IN_MEETING_PROGRAM SET"
                + " in_register_count = " + new_count
                + " WHERE id = '" + entProgram.Id + "'";

            cfg.inn.applySQL(sql);
        }

        //重算場次數
        private void RecountEvents(TConfig cfg, TProgram entProgram)
        {
            if (entProgram.IsTeamFight)
            {
                RecountEventsTeam(cfg, entProgram.Id);
            }
            else
            {
                RecountEventsSolo(cfg, entProgram.Id);
            }
        }

        //重算場次數(團體賽)
        private void RecountEventsTeam(TConfig cfg, string program_id)
        {
            var sql = @"
                UPDATE t1 SET
	                t1.in_event_count = t2.cnt
                FROM
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
                (
	                SELECT
		                source_id
		                , count(*) AS 'cnt'
	                FROM
		                IN_MEETING_PEVENT WITH(NOLOCK)
	                WHERE
		                source_id = '{#program_id}'
		                AND ISNULL(in_tree_no, 0) > 0
		                AND ISNULL(in_win_status, '') NOT IN ('cancel', 'nofight', 'bypass')
		                AND ISNULL(in_type, '') = 's'
	                GROUP BY
		                source_id
                ) t2
                ON t2.source_id = t1.id
            ";

            sql = sql.Replace("{#program_id}", program_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            cfg.inn.applySQL(sql);
        }

        //重算場次數(個人賽)
        private void RecountEventsSolo(TConfig cfg, string program_id)
        {
            var sql = @"
                UPDATE t1 SET
	                t1.in_event_count = t2.cnt
                FROM
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
                (
	                SELECT
		                source_id
		                , count(*) AS 'cnt'
	                FROM
		                IN_MEETING_PEVENT WITH(NOLOCK)
	                WHERE
		                source_id = '{#program_id}'
		                AND ISNULL(in_tree_no, 0) > 0
		                AND ISNULL(in_win_status, '') NOT IN ('cancel', 'nofight', 'bypass')
	                GROUP BY
		                source_id
                ) t2
                ON t2.source_id = t1.id
            ";

            sql = sql.Replace("{#program_id}", program_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            cfg.inn.applySQL(sql);
        }

        //復原 3, 4 名場次序號
        private void AssignedRank34TreeNo(TConfig cfg, TProgram entProgram, string in_tree_no)
        {
            string sql = @"
                UPDATE IN_MEETING_PEVENT SET
                	in_tree_no = {#in_tree_no}
                WHERE 
                	source_id = '{#program_id}' 
                	AND ( (in_tree_name = 'rank34') OR (in_tree_name = 'repechage' AND in_round_code = 2) )
            ";

            sql = sql.Replace("{#program_id}", entProgram.Id)
                .Replace("{#in_tree_no}", in_tree_no);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            cfg.inn.applySQL(sql);
        }

        //復原名次場次序號
        private void AssignedRankTreeNo(TConfig cfg, TProgram entProgram, string in_tree_name, string in_tree_no)
        {
            string sql = @"
                UPDATE IN_MEETING_PEVENT SET
                	in_tree_no = {#in_tree_no}
                WHERE 
                	source_id = '{#program_id}' 
                	AND in_tree_name = '{#in_tree_name}'
            ";

            sql = sql.Replace("{#program_id}", entProgram.Id)
                .Replace("{#in_tree_name}", in_tree_name)
                .Replace("{#in_tree_no}", in_tree_no);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            cfg.inn.applySQL(sql);
        }

        //清空敗部 3, 4 名場次序號
        private void ClearRpcFinalTreeNo(TConfig cfg, TProgram entProgram)
        {
            string sql = "";
            Item itmSQL = null;

            string program_id = entProgram.Id;

            sql = @"
                UPDATE IN_MEETING_PEVENT SET
                	in_tree_no = NULL
                WHERE 
                	source_id = '{#program_id}' 
                	AND in_tree_name = 'repechage' 
                	AND in_round_code = 2
            ";

            sql = sql.Replace("{#program_id}", program_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            itmSQL = cfg.inn.applySQL(sql);

            if (itmSQL.isError()) throw new Exception("發生錯誤: 清空敗部 3, 4 名場次序號");

        }

        /// <summary>
        /// 重設場次(非重建)
        /// </summary>
        private void ResetRobinEvents(TConfig cfg, TProgram entProgram)
        {
            string sql = "";
            Item itmSQL = null;

            string program_id = entProgram.Id;

            //清除對戰
            sql = @"
                UPDATE t1 SET
                    in_status = NULL
	                , in_sign_bypass = NULL
                    , in_sign_status = NULL
                    , in_sign_action = NULL
	                , in_target_bypass = NULL
                    , in_target_status = NULL
                    , in_points = NULL
                    , in_points_type = NULL
                    , in_points_text = NULL
                    , in_correct_count = NULL
                FROM
                    IN_MEETING_PEVENT_DETAIL t1
                INNER JOIN
                    IN_MEETING_PEVENT t2
                    ON t2.id = t1.source_id
                WHERE
                    t2.source_id = '{#program_id}'
            ";

            sql = sql.Replace("{#program_id}", program_id);
            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);
            itmSQL = cfg.inn.applySQL(sql);
            if (itmSQL.isError()) throw new Exception("發生錯誤: 重設循環賽【場次明細】資料");


            //【場次】資料
            sql = @"
                UPDATE t1 SET
	                in_bypass_foot = NULL
	                , in_bypass_status = NULL
	                , in_win_status = NULL
	                , in_win_sign_no = NULL
	                , in_win_time = NULL
	                , in_win_creator = NULL
	                , in_win_creator_sno = NULL
                    , in_win_local = NULL
                FROM
	                IN_MEETING_PEVENT t1
                WHERE
	                t1.source_id = '{#program_id}'
            ";

            sql = sql.Replace("{#program_id}", program_id);
            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);
            itmSQL = cfg.inn.applySQL(sql);
            if (itmSQL.isError()) throw new Exception("發生錯誤: 重設循環賽【場次】資料");
        }

        /// <summary>
        /// 重設場次(非重建)
        /// </summary>
        private void ResetEvents(TConfig cfg, TProgram entProgram)
        {
            string sql = "";
            string program_id = entProgram.Id;

            //勝部第一回合場次【對戰】資料 (不能清籤號)
            sql = @"
                UPDATE t1 SET
                    in_status = NULL
	                , in_sign_bypass = NULL
                    , in_sign_status = NULL
                    , in_sign_action = NULL
	                , in_target_bypass = NULL
                    , in_target_status = NULL
                    , in_points = NULL
                    , in_points_type = NULL
                    , in_points_text = NULL
                    , in_correct_count = NULL
                FROM
                    IN_MEETING_PEVENT_DETAIL t1
                INNER JOIN
                    IN_MEETING_PEVENT t2
                    ON t2.id = t1.source_id
                WHERE
                    t2.source_id = '{#program_id}'
                    AND t2.in_tree_name IN (N'main', N'sub')
                    AND t2.in_round = 1
            ";

            sql = sql.Replace("{#program_id}", program_id);
            cfg.inn.applySQL(sql);

            //勝部第二回合以上場次【對戰】資料
            sql = @"
                UPDATE t1 SET
                    in_status = NULL
                    , in_sign_no = NULL
	                , in_sign_bypass = NULL
                    , in_sign_status = NULL
                    , in_sign_action = NULL
                    , in_target_no = NULL
	                , in_target_bypass = NULL
                    , in_target_status = NULL
                    , in_points = NULL
                    , in_points_type = NULL
                    , in_points_text = NULL
                    , in_correct_count = NULL
	                , in_assigned = NULL
                FROM
                    IN_MEETING_PEVENT_DETAIL t1
                INNER JOIN
                    IN_MEETING_PEVENT t2
                    ON t2.id = t1.source_id
                WHERE
                    t2.source_id = '{#program_id}'
                    AND t2.in_tree_name IN (N'main', N'sub')
                    AND t2.in_round >= 2
            ";

            sql = sql.Replace("{#program_id}", program_id);
            cfg.inn.applySQL(sql);

            //勝部以外的場次【對戰】資料
            sql = @"
                UPDATE t1 SET
                    in_status = NULL
                    , in_sign_no = NULL
	                , in_sign_bypass = NULL
                    , in_sign_status = NULL
                    , in_sign_action = NULL
                    , in_target_no = NULL
	                , in_target_bypass = NULL
                    , in_target_status = NULL
                    , in_points = NULL
                    , in_points_type = NULL
                    , in_points_text = NULL
                    , in_correct_count = NULL
	                , in_assigned = NULL
                FROM
                    IN_MEETING_PEVENT_DETAIL t1
                INNER JOIN
                    IN_MEETING_PEVENT t2
                    ON t2.id = t1.source_id
                WHERE
                    t2.source_id = '{#program_id}'
                    AND t2.in_tree_name NOT IN (N'main', N'sub')
            ";

            sql = sql.Replace("{#program_id}", program_id);
            cfg.inn.applySQL(sql);

            //【場次】資料
            sql = @"
                UPDATE t1 SET
	                in_tree_no = NULL
	                , in_tree_sno = NULL
	                , in_bypass_foot = NULL
	                , in_bypass_status = NULL
	                , in_win_status = NULL
	                , in_win_sign_no = NULL
	                , in_win_time = NULL
	                , in_win_creator = NULL
	                , in_win_creator_sno = NULL
                    , in_win_local = NULL
	                , in_win_local_time = NULL
	                , in_cancel = NULL
                FROM
	                IN_MEETING_PEVENT t1
                WHERE
	                t1.source_id = '{#program_id}'
            ";

            sql = sql.Replace("{#program_id}", program_id);
            cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// 取得場次對戰與隊伍資料
        /// </summary>
        private Dictionary<string, TEvent> GetMPEDTs(TConfig cfg, TProgram entProgram, string next_win_id = "")
        {
            string sql = "";

            string program_id = entProgram.Id;
            string next_condition = next_win_id == ""
                ? ""
                : "AND t2.in_next_win = '" + next_win_id + "'";

            sql = @"
                SELECT
	                t1.id       AS 'program_id'
	                , t1.in_l1
	                , t1.in_l2
	                , t1.in_l3
	                , t2.id     AS 'event_id'
                    , t2.in_tree_id
                    , t2.in_round
                    , t2.in_bypass_foot
                    , t2.in_bypass_status
	                , t3.id     AS 'detail_id'
	                , t3.in_sign_foot
	                , t3.in_sign_no
                FROM
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PEVENT t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t3 WITH(NOLOCK)
	                ON t3.source_id = t2.id
                WHERE
	                t1.id = '{#program_id}'
	                AND t2.in_tree_name = N'main'
                    {#next_condition}
                ORDER BY
	                t2.in_tree_sort
	                , t2.in_round_code DESC
	                , t2.in_tree_id
	                , t3.in_sign_foot
	                , t3.in_sign_no
            ";

            sql = sql.Replace("{#program_id}", program_id)
                .Replace("{#next_condition}", next_condition);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            var items = cfg.inn.applySQL(sql);

            if (items.isError() || items.getItemCount() <= 0)
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "查詢資料異常 sql: " + sql);
                throw new Exception("場次對戰資料查詢發生錯誤");
            }

            Dictionary<string, TEvent> map = new Dictionary<string, TEvent>();

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string key = item.getProperty("event_id", "");
                string in_tree_id = item.getProperty("in_tree_id", "");
                string in_bypass_foot = item.getProperty("in_bypass_foot", "");
                string in_sign_foot = item.getProperty("in_sign_foot", "");
                string in_round = item.getProperty("in_round", "");

                TEvent evt = null;
                if (map.ContainsKey(key))
                {
                    evt = map[key];
                }
                else
                {
                    evt = new TEvent
                    {
                        Id = key,
                        TreeId = in_tree_id,
                        Round = in_round,
                        ByPassFoot = in_bypass_foot,
                        Foot1 = new TDetail(),
                        Foot2 = new TDetail(),
                    };
                    map.Add(key, evt);
                }

                if (in_sign_foot == "1")
                {
                    SetTEDetial(entProgram, evt.Foot1, item);
                }
                else
                {
                    SetTEDetial(entProgram, evt.Foot2, item);
                }
            }

            foreach (var kv in map)
            {
                var evt = kv.Value;
                FixTEDetial(evt.Foot1);
            }

            return map;
        }

        /// <summary>
        /// 修正場次輪空狀態
        /// </summary>
        private void FixEventBypass(TConfig cfg, TProgram entProgram, TEvent evt)
        {
            string sql = "";
            Item itmSQL = null;

            if (evt.Foot1.IsError || evt.Foot2.IsError)
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "場次對戰資料異常 event id: " + evt.Id);
                throw new Exception("場次對戰資料異常");
            }

            string in_bypass_foot = "";
            string in_bypass_status = "1"; //整數，0 = 不排, 1 = 無輪空, 2 = 單腳輪空(上場次), 8 兩腳皆輪空(上場次)

            if (evt.Round == "1")
            {
                if (evt.Foot1.IsSignBypass)
                {
                    in_bypass_foot = "1";
                    in_bypass_status = "0";
                }
                else if (evt.Foot2.IsSignBypass)
                {
                    in_bypass_foot = "2";
                    in_bypass_status = "0";
                }
                else
                {
                    in_bypass_status = "1";
                }

                string detail_id_1 = evt.Foot1.Id;
                string in_sign_bypass_1 = evt.Foot1.IsSignBypass ? "1" : "0";
                sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET in_sign_bypass = '" + in_sign_bypass_1 + "' WHERE id = '" + detail_id_1 + "'";
                itmSQL = cfg.inn.applySQL(sql);
                if (itmSQL.isError()) throw new Exception("error");

                string detail_id_2 = evt.Foot2.Id;
                string in_sign_bypass_2 = evt.Foot2.IsSignBypass ? "1" : "0";
                sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET in_sign_bypass = '" + in_sign_bypass_2 + "' WHERE id = '" + detail_id_2 + "'";
                itmSQL = cfg.inn.applySQL(sql);
                if (itmSQL.isError()) throw new Exception("error");

                sql = "UPDATE IN_MEETING_PEVENT SET in_bypass_foot = '" + in_bypass_foot + "', in_bypass_status = '" + in_bypass_status + "' WHERE id = '" + evt.Id + "'";
                itmSQL = cfg.inn.applySQL(sql);
                if (itmSQL.isError()) throw new Exception("error");
            }
            else if (evt.Round == "2")
            {
                if (cfg.is_tkd_mode)
                {
                    Dictionary<string, TEvent> sub = GetMPEDTs(cfg, entProgram, next_win_id: evt.Id);
                    if (sub == null || sub.Count != 2) throw new Exception("error");

                    var son1 = sub.First().Value;
                    var son2 = sub.Last().Value;

                    if (son1.ByPassFoot != "" && son2.ByPassFoot != "")
                    {
                        in_bypass_status = "8";
                    }
                    else if (son1.ByPassFoot != "" || son2.ByPassFoot != "")
                    {
                        in_bypass_status = "4";
                    }
                    else
                    {
                        in_bypass_status = "1";
                    }
                }
                else
                {
                    in_bypass_status = "1";
                }

                sql = "UPDATE IN_MEETING_PEVENT SET in_bypass_status = '" + in_bypass_status + "' WHERE id = '" + evt.Id + "'";
                itmSQL = cfg.inn.applySQL(sql);
                if (itmSQL.isError()) throw new Exception("error");
            }
            else
            {
                sql = "UPDATE IN_MEETING_PEVENT SET in_bypass_status = '1' WHERE id = '" + evt.Id + "'";
                itmSQL = cfg.inn.applySQL(sql);
                if (itmSQL.isError()) throw new Exception("error");
            }
        }

        /// <summary>
        /// 更新場次序號(時序)
        /// </summary>
        private void UpdateEventTreeNo(TConfig cfg, TProgram entProgram)
        {
            string program_id = entProgram.Id;

            string sql = @"
                UPDATE t1 SET
                    t1.in_tree_no = t2.rno
                    , t1.in_tree_sno = t2.rno
                FROM 
                    IN_MEETING_PEVENT t1
                INNER JOIN
                (
                    SELECT 
                        ROW_NUMBER() OVER (ORDER BY in_round, in_bypass_status DESC, in_round_id) AS rno
                        , id
                        , in_round
                        , in_bypass_status
                        , in_round_id
                    FROM
                        IN_MEETING_PEVENT WITH(NOLOCK)
                    WHERE
                        source_id = '{#program_id}'
                        AND ISNULL(in_bypass_foot, '') = ''
                        AND in_tree_name = 'main'
                ) t2
                    ON t2.id = t1.id
                WHERE
                    t1.source_id = '{#program_id}'
                    AND t1.in_tree_name = 'main'
            ";

            sql = sql.Replace("{#program_id}", program_id);

            cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// 更新場次序號(時序)
        /// </summary>
        private void UpdateRpcEventTreeNo(TConfig cfg, TProgram entProgram)
        {
            string program_id = entProgram.Id;

            string sql = @"
                UPDATE t1 SET
                    t1.in_tree_no = t2.rno
                    , t1.in_tree_sno = t2.rno
                FROM 
                    IN_MEETING_PEVENT t1
                INNER JOIN
                (
                    SELECT 
                        ROW_NUMBER() OVER (ORDER BY in_round, in_round_id) AS rno
                        , id
                        , in_round
                        , in_round_id
                    FROM
                        IN_MEETING_PEVENT WITH(NOLOCK)
                    WHERE
                        source_id = '{#program_id}'
                        AND in_tree_name = N'repechage'
                ) t2
                    ON t2.id = t1.id
                WHERE
                    t1.source_id = '{#program_id}'
                    AND t1.in_tree_name = N'repechage'
            ";

            sql = sql.Replace("{#program_id}", program_id);

            cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// 修正場次序號(時序)
        /// </summary>
        private void FixRpcEventTreeNo(TConfig cfg, TProgram entProgram)
        {
            var rpc = GetRpc(cfg, entProgram);
            int w_cnt = rpc.C1.Players.Count + rpc.C2.Players.Count;
            int e_cnt = rpc.C3.Players.Count + rpc.C4.Players.Count;

            switch (rpc.real_player_count)
            {
                case 1:
                case 2:
                case 3:
                case 4:
                case 8:
                case 16:
                case 32:
                case 64:
                case 128:
                    break;

                case 5:
                    CancelRpcEvent(cfg, rpc, "R101");
                    CancelRpcEvent(cfg, rpc, "R102");
                    if (w_cnt < 3) ClearRpcTreeNo(cfg, rpc, "R201");
                    if (e_cnt < 3) ClearRpcTreeNo(cfg, rpc, "R202");
                    break;

                case 6:
                    ClearRpcTreeNo(cfg, rpc, "R101");
                    ClearRpcTreeNo(cfg, rpc, "R102");
                    break;

                case 7:
                    if (w_cnt < 4) ClearRpcTreeNo(cfg, rpc, "R101");
                    if (e_cnt < 4) ClearRpcTreeNo(cfg, rpc, "R102");
                    break;

                default:
                    int code = rpc.col_code / 2;
                    if (rpc.C1.Players.Count <= code) ClearRpcTreeNo(cfg, rpc, "R101");
                    if (rpc.C2.Players.Count <= code) ClearRpcTreeNo(cfg, rpc, "R102");
                    if (rpc.C3.Players.Count <= code) ClearRpcTreeNo(cfg, rpc, "R103");
                    if (rpc.C4.Players.Count <= code) ClearRpcTreeNo(cfg, rpc, "R104");
                    break;
            }
        }

        /// <summary>
        /// 清除場次序號
        /// </summary>
        private void ClearRpcTreeNo(TConfig cfg, TRpc rpc, string in_tree_id)
        {
            string sql = "UPDATE IN_MEETING_PEVENT SET in_tree_no = NULL, in_tree_sno = NULL WHERE source_id = '" + rpc.program_id + "' AND in_tree_id = '" + in_tree_id + "'";
            cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// 取消場次
        /// </summary>
        private void CancelRpcEvent(TConfig cfg, TRpc rpc, string in_tree_id)
        {
            string sql = "";

            sql = "UPDATE IN_MEETING_PEVENT SET in_tree_no = NULL, in_tree_sno = NULL, in_win_status = 'cancel' WHERE source_id = '" + rpc.program_id + "' AND in_tree_id = '" + in_tree_id + "'";
            cfg.inn.applySQL(sql);
            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "dom: " + sql);

            sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET in_sign_status = N'cancel' WHERE source_id IN "
                + "("
                + "     SELECT id FROM IN_MEETING_PEVENT WHERE source_id = '" + rpc.program_id + "' AND in_tree_id = '" + in_tree_id + "'"
                + ")";
            cfg.inn.applySQL(sql);
            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "dom: " + sql);
        }

        /// <summary>
        /// 取得敗部資料
        /// </summary>
        private TRpc GetRpc(TConfig cfg, TProgram entProgram)
        {
            string program_id = entProgram.Id;
            int team_count = entProgram.TeamCount;
            int round_code = entProgram.RoundCode;
            int round_count = entProgram.RoundCount;

            int col_code = round_code / 4;
            TRpc rpc = new TRpc
            {
                program_id = program_id,
                tree_name = "repechage",
                real_player_count = team_count,
                plan_player_count = round_code,
                plan_round_count = round_count,
                col_code = col_code,
                C1 = new TColumn { Suffix = "1", Players = new List<InnSport.Core.Models.Output.TNoStatus> { } },
                C2 = new TColumn { Suffix = "2", Players = new List<InnSport.Core.Models.Output.TNoStatus> { } },
                C3 = new TColumn { Suffix = "3", Players = new List<InnSport.Core.Models.Output.TNoStatus> { } },
                C4 = new TColumn { Suffix = "4", Players = new List<InnSport.Core.Models.Output.TNoStatus> { } },
            };

            var list = entProgram.NoList;
            for (int i = 0; i < list.Count; i++)
            {
                var item = list[i];

                if (item.IsByPass || col_code <= 0)
                {
                    continue;
                }

                int id = i / col_code;

                switch (id)
                {
                    case 0:
                        rpc.C1.Players.Add(item);
                        break;
                    case 1:
                        rpc.C2.Players.Add(item);
                        break;
                    case 2:
                        rpc.C3.Players.Add(item);
                        break;
                    case 3:
                        rpc.C4.Players.Add(item);
                        break;
                }
            }

            return rpc;
        }

        /// <summary>
        /// 修補隊伍的量級序號
        /// </summary>
        private void FixTeamSectionNo(TConfig cfg, Item itmProgram)
        {
            Item item = cfg.inn.newItem();
            item.setType("In_Meeting_Program");
            item.setProperty("meeting_id", itmProgram.getProperty("in_meeting", ""));
            item.setProperty("program_id", itmProgram.getProperty("id", ""));
            item.apply("in_meeting_program_team_fix2");
        }

        /// <summary>
        /// 自動晉級
        /// </summary>
        private void AutoUpgrade(TConfig cfg, Item itmProgram)
        {
            Item item = cfg.inn.newItem();
            item.setType("In_Meeting_Program");
            item.setProperty("meeting_id", itmProgram.getProperty("in_meeting", ""));
            item.setProperty("program_id", itmProgram.getProperty("id", ""));
            item.setProperty("mode", "upgrade");
            item.apply("in_meeting_program_score");
        }

        /// <summary>
        /// 自動晉級(DQ)
        /// </summary>
        private void AutoUpgradeDQ(TConfig cfg, Item itmProgram)
        {
            Item item = cfg.inn.newItem();
            item.setType("In_Meeting_Program");
            item.setProperty("meeting_id", itmProgram.getProperty("in_meeting", ""));
            item.setProperty("program_id", itmProgram.getProperty("id", ""));
            item.setProperty("mode", "dq_upgrade");
            item.apply("in_meeting_program_score");
        }

        //修正日期
        private void FixDate(TConfig cfg, Item itmProgram)
        {
            string sql = "";
            Item itmSQL = null;

            string program_id = itmProgram.getProperty("id", "");

            sql = @"
                 UPDATE t1 SET
             	    t1.in_date_key = t3.in_date_key
             	    , t1.in_site = t4.id
             	    , t1.in_site_code = t4.in_code
             	    , t1.in_tree_state = 0
                 FROM
             	    IN_MEETING_PEVENT t1
                 INNER JOIN
                    IN_MEETING_PROGRAM t2
                    ON t2.id = t1.source_id
                 INNER JOIN
             	    IN_MEETING_ALLOCATION t3
             	    ON t3.in_program = t2.id
                 INNER JOIN
             	    IN_MEETING_SITE t4
             	    ON t4.id = t3.in_site
                 WHERE
                    t2.id = '{#program_id}'
            ";

            sql = sql.Replace("{#program_id}", program_id);

            itmSQL = cfg.inn.applySQL(sql);
        }

        private void FixTEDetial(TDetail foot)
        {
            if (foot.Value == null)
            {
                foot.Id = "";
                foot.IsSignBypass = true;
                foot.IsError = true;
            }
        }

        private void SetTEDetial(TProgram entProgram, TDetail foot, Item item)
        {
            //跆拳道籤號
            string in_sign_no = item.getProperty("in_sign_no", "0");
            int sign_no = GetInt(in_sign_no);
            bool is_bypass = entProgram.ByPassNoList.Contains(sign_no);

            foot.Id = item.getProperty("detail_id", "");
            foot.SignFoot = item.getProperty("in_sign_foot", "");
            foot.SignNo = in_sign_no;
            foot.Value = item;

            if (is_bypass)
            {
                foot.IsSignBypass = true;
            }

            if (foot.SignNo == "")
            {
                foot.UnSigned = true;
            }

            foot.IsError = false;
        }


        //取得級組
        private Item GetProgramItems(TConfig cfg)
        {
            var sql = "";

            if (cfg.in_date != "")
            {
                sql = @"
                    SELECT
                    	t1.*
                    FROM
                    	IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                    INNER JOIN
                    	IN_MEETING_ALLOCATION t2 WITH(NOLOCK)
                    	ON t2.in_program = t1.id
                    WHERE
                    	t1.in_meeting = '{#meeting_id}'
                    	AND t2.in_date_key = '{#in_date_key}'
                    ORDER BY
                    	t2.created_on
                ";

                sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                    .Replace("{#in_date_key}", cfg.in_date);
            }
            else if (cfg.program_id != "")
            {
                sql = "SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + cfg.program_id + "'";
            }
            else
            {
                sql = "SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE in_meeting = '" + cfg.meeting_id + "'";
            }

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        //三四名參數
        private bool GetNeedRank34(TConfig cfg, TProgram pg)
        {
            //不論人數或賽制，必打
            var v = cfg.variable_map.need_rank34;
            if (v == "1") return true;

            //挑戰賽才打
            if (v == "" && pg.BattleType == "Challenge") return true;

            return false;
        }

        //五六名參數
        private bool GetNeedRank56(TConfig cfg, TProgram pg)
        {
            //不論人數或賽制，必打
            var v = cfg.variable_map.need_rank56;
            if (v == "1") return true;

            //7人才打
            if (v == "2" && pg.RankCount == 7) return true;

            return false;
        }

        //七八名參數
        private bool GetNeedRank78(TConfig cfg, TProgram pg)
        {
            //不論人數或賽制，必打
            var v = cfg.variable_map.need_rank78;
            if (v == "1") return true;

            //9人才打
            if (v == "2" && pg.RankCount == 9) return true;

            return false;
        }

        private TVariable MapVariable(TConfig cfg)
        {
            var map = new TVariable
            {
                need_rank34 = "0",
                need_rank56 = "0",
                need_rank78 = "0",
            };

            var items = GetMtVariableItems(cfg);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var in_key = item.getProperty("in_key", "");
                var in_value = item.getProperty("in_value", "");
                switch (in_key)
                {
                    case "need_rank34":
                        map.need_rank34 = in_value;
                        break;

                    case "need_rank56":
                        map.need_rank56 = in_value;
                        break;

                    case "need_rank78":
                        map.need_rank78 = in_value;
                        break;
                }
            }
            return map;
        }

        private Item GetMtVariableItems(TConfig cfg)
        {
            var sql = @"
                SELECT 
	                in_key
	                , in_value
                FROM 
	                IN_MEETING_VARIABLE WITH(NOLOCK) 
                WHERE
	                source_id = '{#meeting_id}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            return cfg.inn.applySQL(sql);
        }

        private class TVariable
        {
            public string need_rank34 { get; set; }
            public string need_rank56 { get; set; }
            public string need_rank78 { get; set; }
        }

        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Aras.Server.Core.IContextState RequestState { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public Innovator inn { get; set; }

            public string draw_mode { get; set; }
            public bool is_tkd_mode { get; set; }

            public string meeting_id { get; set; }
            public string program_id { get; set; }
            public string in_date { get; set; }
            public string scene { get; set; }
            public string is_run { get; set; }
            public string is_build { get; set; }

            public InnSport.Core.Services.ISportService Service { get; set; }
            public TVariable variable_map { get; set; }
        }

        private class TProgram
        {
            public string Id { get; set; }
            public string InName { get; set; }
            public string InL1 { get; set; }
            public string InL2 { get; set; }
            public string InL3 { get; set; }
            public string BattleType { get; set; }
            public int TeamCount { get; set; }
            public int RankCount { get; set; }
            public int RoundCode { get; set; }
            public int RoundCount { get; set; }
            public bool IsRoundRobin { get; set; }
            public bool IsTeamFight { get; set; }
            public Item Value { get; set; }

            public List<InnSport.Core.Models.Output.TNoStatus> NoList { get; set; }
            public List<int> ByPassNoList { get; set; }
        }

        private class TEvent
        {
            public string Id { get; set; }
            public string TreeId { get; set; }
            public string Round { get; set; }
            public string ByPassFoot { get; set; }
            public Item Value { get; set; }
            public TDetail Foot1 { get; set; }
            public TDetail Foot2 { get; set; }
        }

        private class TDetail
        {
            public string Id { get; set; }
            public string SignFoot { get; set; }
            public string SignNo { get; set; }
            public Item Value { get; set; }
            public bool IsError { get; set; }
            public bool IsSignBypass { get; set; }
            public bool UnSigned { get; set; }
        }

        private class TRpc
        {
            public string program_id { get; set; }
            public string tree_name { get; set; }
            public int real_player_count { get; set; }
            public int plan_player_count { get; set; }
            public int plan_round_count { get; set; }
            public int col_code { get; set; }
            public TColumn C1 { get; set; }
            public TColumn C2 { get; set; }
            public TColumn C3 { get; set; }
            public TColumn C4 { get; set; }
        }

        private class TColumn
        {
            public string Suffix { get; set; }
            public List<InnSport.Core.Models.Output.TNoStatus> Players { get; set; }
        }

        private int GetInt(string value, int def = 0)
        {
            int result = def;
            int.TryParse(value, out result);
            return result;
        }
    }
}