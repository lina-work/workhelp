﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;
using System.Net;

namespace WorkHelp.ArasDesk.Methods.Judo.Fight
{
    public class in_qrcode_url : Item
    {
        public in_qrcode_url(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
    目的: 產生短網址與 QrCode 圖片
    輸入: 
        - meeting_id
        - scene
    日期: 
        - 2021-12-01: 創建 (lina)
        - 2021-05-21: 創建 (lina)
*/

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]in_qrcode_url";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            string sql = "";

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                meeting_id = itmR.getProperty("meeting_id", ""),
                scene = itmR.getProperty("scene", ""),
                exe_type = itmR.getProperty("exe_type", ""),
                url = itmR.getProperty("url", ""),
                token = itmR.getProperty("token", ""),
                need_icon = itmR.getProperty("need_icon", ""),
            };

            if (cfg.meeting_id != "")
            {
                sql = @"SELECT in_title FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'";
                Item itmMeeting = cfg.inn.applySQL(sql);
                itmR.setProperty("in_title", itmMeeting.getProperty("in_title", ""));
            }

            itmR.setProperty("new_url", "#");
            itmR.setProperty("img_url", "../Images/empty.png");

            //http://act.innosoft.com.tw/judo/
            sql = @"SELECT in_value FROM In_Variable WITH(NOLOCK) WHERE in_name = 'app_url'";
            Item itmPLM = cfg.inn.applySQL(sql);

            //https://act.innosoft.com.tw/api/
            sql = @"SELECT in_value FROM In_Variable WITH(NOLOCK) WHERE in_name = 'api_url'";
            Item itmApi = cfg.inn.applySQL(sql);

            if (itmPLM.isError() || itmPLM.getResult() == "")
            {
                itmR.setProperty("err_msg", "[原創設定參數]app_url 未設定");
                return itmR;
            }

            //C:\site\judo\tempvault\meeting_qrcode
            sql = "SELECT in_value FROM In_Variable WITH(NOLOCK) WHERE in_name = 'meeting_qrcode'";
            Item itmPath = cfg.inn.applySQL(sql);

            if (itmPath.isError() || itmPath.getResult() == "")
            {
                itmR.setProperty("err_msg", "[原創設定參數]meeting_qrcode 未設定");
                return itmR;
            }

            cfg.plm_url = itmPLM.getProperty("in_value", "");
            cfg.api_url = itmApi.getProperty("in_value", "").Trim('/');

            switch (cfg.scene)
            {
                case "PublicCompetition":
                    cfg.url = cfg.plm_url + "pages/b.aspx?page=PublicCompetition.html&method=in_meeting_program_preview&meeting_id=" + cfg.meeting_id;
                    break;

                case "PublicScore":
                    cfg.url = cfg.plm_url + "pages/b.aspx?page=PublicScoreSearch.html&method=in_meeting_program_score_s&meeting_id=" + cfg.meeting_id + "&open=1";
                    break;

                default:
                    break;
            }

            //CCO.Utilities.WriteDebug(strMethodName, "url: " + cfg.url);

            if (cfg.url != "")
            {
                TApi api = GetShortUrlApi(cfg);

                if (api.err_msg != "")
                {
                    itmR.setProperty("err_msg", api.err_msg);
                }
                else
                {
                    string[] arr = null;
                    if (cfg.need_icon == "1")
                    {
                        arr = GenerateQrCodeIcon(itmPath, api.new_id, api.new_url);
                    }
                    else
                    {
                        arr = GenerateQrCode(itmPath, api.new_id, api.new_url);
                    }

                    //CCO.Utilities.WriteDebug(strMethodName, "img_file: " + arr[0]);
                    //CCO.Utilities.WriteDebug(strMethodName, "img_url: " + arr[1]);

                    itmR.setProperty("new_url", api.new_url);
                    itmR.setProperty("img_url", arr[1] + "?t=" + DateTime.Now.ToString("HHmmss"));
                }

                if (cfg.exe_type == "box")
                {
                    Box(cfg, api, itmR);
                }
            }

            return itmR;
        }

        private void Box(TConfig cfg, TApi api, Item itmReturn)
        {
            string new_url = itmReturn.getProperty("new_url", "");
            string img_url = itmReturn.getProperty("img_url", "");

            StringBuilder builder = new StringBuilder();

            if (new_url == "" || new_url == "#")
            {
                builder.Append("<div class='row' >");
                builder.Append("    <p style='color: red'>無法產生短網址" + api.err_msg + "</p>");
                builder.Append("</div>");
            }
            else
            {
                builder.Append("<div class='row'>");
                builder.Append("    <a href='" + new_url + "' target='_blank'>");
                builder.Append("        <img src='" + img_url + "' style='height: 200px; width:auto'>");
                builder.Append("    </a>");
                builder.Append("</div>");

                builder.Append("<div class='row' >");
                builder.Append("    <a href='" + new_url + "' target='_blank'>" + new_url + "</a>");
                builder.Append("</div>");
            }
            itmReturn.setProperty("inn_box", builder.ToString());
        }

        //生成 QrCode
        private string[] GenerateQrCodeIcon(Item itmPath, string name, string contents)
        {
            string imgIconPath = @"C:\site\judo\favicon-48x48.png";

            string folder = itmPath.getProperty("in_value", "");
            if (!System.IO.Directory.Exists(folder))
            {
                System.IO.Directory.CreateDirectory(folder);
            }

            string file = name + ".jpg";
            string img_file = System.IO.Path.Combine(folder, file);
            string img_url = "../ltsvault/meeting_qrcode/" + file;

            if (System.IO.File.Exists(img_file))
            {
                System.IO.File.Delete(img_file);
            }

            if (!System.IO.File.Exists(img_file))
            {

                var writer = new ZXing.BarcodeWriter  //dll裡面可以看到屬性
                {
                    Format = ZXing.BarcodeFormat.QR_CODE,
                    Options = new ZXing.QrCode.QrCodeEncodingOptions //設定大小
                    {
                        CharacterSet = "UTF-8",
                        ErrorCorrection = ZXing.QrCode.Internal.ErrorCorrectionLevel.H,
                        Height = 500,
                        Width = 500,
                        Margin = 0,
                    },
                };

                var imgQrCode = writer.Write(contents);
                var imgIcon = new System.Drawing.Bitmap(imgIconPath);

                var deltaH = imgQrCode.Height - imgIcon.Height;
                var deltaW = imgQrCode.Width - imgIcon.Width;

                var g = System.Drawing.Graphics.FromImage(imgQrCode);

                g.DrawImage(imgIcon, new System.Drawing.Point(deltaW / 2, deltaH / 2));

                imgQrCode.Save(img_file, System.Drawing.Imaging.ImageFormat.Jpeg);
                imgQrCode.Dispose();
                imgIcon.Dispose();
            }

            return new string[] { img_file, img_url };
        }

        //生成 QrCode
        private string[] GenerateQrCode(Item itmPath, string name, string contents)
        {
            string folder = itmPath.getProperty("in_value", "");
            if (!System.IO.Directory.Exists(folder))
            {
                System.IO.Directory.CreateDirectory(folder);
            }

            string file = name + ".jpg";
            string img_file = System.IO.Path.Combine(folder, file);
            string img_url = "../ltsvault/meeting_qrcode/" + file;

            if (System.IO.File.Exists(img_file))
            {
                System.IO.File.Delete(img_file);
            }

            if (!System.IO.File.Exists(img_file))
            {
                var writer = new ZXing.BarcodeWriter  //dll裡面可以看到屬性
                {
                    Format = ZXing.BarcodeFormat.QR_CODE,
                    Options = new ZXing.QrCode.QrCodeEncodingOptions //設定大小
                    {
                        Height = 500,
                        Width = 500,
                        Margin = 0,
                        //CharacterSet = "UTF-8",
                        //ErrorCorrection = ZXing.QrCode.Internal.ErrorCorrectionLevel.M
                    },
                };

                var img = writer.Write(contents);
                var myBitmap = new System.Drawing.Bitmap(img);

                myBitmap.Save(img_file, System.Drawing.Imaging.ImageFormat.Jpeg);
            }

            return new string[] { img_file, img_url };
        }

        //生成短網址
        private TApi GetShortUrlApi(TConfig cfg)
        {
            if (cfg.token == "")
            {
                return CreateShortUrlApi(cfg);
            }
            else
            {
                return AppointShortUrlApi(cfg);
            }
        }

        //生成短網址
        private TApi CreateShortUrlApi(TConfig cfg)
        {
            TApi api = new TApi
            {
                new_url = "",
                new_id = "",
                err_msg = "",
            };

            try
            {
                string tgturl = System.Web.HttpUtility.UrlEncode(cfg.url);
                string fullurl = cfg.api_url + "/create?url=" + tgturl;
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, fullurl);

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(fullurl);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                StreamReader tReader = new StreamReader(response.GetResponseStream());

                //api.new_id = tReader.ReadToEnd().Replace("\"", "");
                //api.new_url = api.host_url + "/r/" + api.new_id;

                var rst = tReader.ReadToEnd().Replace("\"", "");
                if (rst != null && rst != "")
                {
                    api.new_url = rst;
                    api.new_id = rst.Split('/').Last();
                }
                else
                {
                    api.new_url = "";
                    api.new_id = "";
                }

                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, api.new_url);
            }
            catch (Exception ex)
            {
                api.err_msg = ex.Message;
            }

            return api;
        }

        //指定短網址
        private TApi AppointShortUrlApi(TConfig cfg)
        {
            TApi api = new TApi
            {
                new_url = "",
                new_id = "",
                err_msg = "",
            };

            try
            {
                string tgturl = System.Web.HttpUtility.UrlEncode(cfg.url);
                string fullurl = cfg.api_url + "/appoint?url=" + tgturl + "&token=" + cfg.token;
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, fullurl);

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(fullurl);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                StreamReader tReader = new StreamReader(response.GetResponseStream());

                //api.new_id = tReader.ReadToEnd().Replace("\"", "");
                //api.new_url = api.host_url + "/r/" + api.new_id;

                var rst = tReader.ReadToEnd().Replace("\"", "");
                if (rst != null && rst != "")
                {
                    if (rst.StartsWith("err"))
                    {
                        api.err_msg = rst.Replace("err", "");
                    }
                    else
                    {
                        api.new_url = rst;
                        api.new_id = rst.Split('/').Last();
                    }
                }
                else
                {
                    api.new_url = "";
                    api.new_id = "";
                }

                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, api.new_url);
            }
            catch (Exception ex)
            {
                api.err_msg = ex.Message;
            }

            return api;
        }

        private class TApi
        {
            public string new_url { get; set; }
            public string new_id { get; set; }
            public string err_msg { get; set; }
        }

        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }

            public string meeting_id { get; set; }
            public string scene { get; set; }
            public string exe_type { get; set; }
            public string url { get; set; }
            public string token { get; set; }

            public string plm_url { get; set; }
            public string api_url { get; set; }
            public string need_icon { get; set; }
        }
    }
}