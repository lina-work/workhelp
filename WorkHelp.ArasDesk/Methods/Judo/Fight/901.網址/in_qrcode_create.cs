﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;
using System.Net;

namespace WorkHelp.ArasDesk.Methods.Judo.Fight
{
    public class in_qrcode_create : Item
    {
        public in_qrcode_create(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 競賽連結中心-建立連結
                輸入: meeting_id
                日期: 
                    2021-12-09: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]in_qrcode_create";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, this.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                meeting_id = itmR.getProperty("meeting_id", ""),
            };

            if (cfg.meeting_id == "")
            {
                throw new Exception("賽事 id 不可為空值");
            }

            //賽事
            Item itmMeeting = cfg.inn.applySQL("SELECT in_title, in_language FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            if (itmMeeting.isError() || itmMeeting.getResult() == "")
            {
                throw new Exception("查無 賽事資料");
            }
            cfg.mt_title = itmMeeting.getProperty("in_title", "");
            cfg.mt_language = itmMeeting.getProperty("in_language", "");
            cfg.is_english = cfg.mt_language == "en";

            //iplm 站台網址
            cfg.plm_url = GetVariable(cfg, "app_url");

            //Api 網址
            cfg.api_url = GetVariable(cfg, "api_url");

            //開放資料
            CreateOpenPage(cfg);

            //競管平台
            CreateAdminPage(cfg);

            //修正英文標籤
            FixLanguageLabel(cfg);

            //刷新連結排序
            RefreshSortOrder(cfg, "function");
            RefreshSortOrder(cfg, "admin-func");

            return itmR;
        }

        private void FixLanguageLabel(TConfig cfg)
        {
            if (cfg.meeting_id == "") return;
            UpdLanguageLabel(cfg, "rtm-player", "Athlete Profile");
            UpdLanguageLabel(cfg, "checkin", "Contest Order");
            UpdLanguageLabel(cfg, "rtm-tree", "Draw Sheet");
            UpdLanguageLabel(cfg, "rtm-score-list", "Last Records");
            UpdLanguageLabel(cfg, "allocate-site", "Mat Division");
            UpdLanguageLabel(cfg, "spotcheck-announce", "Random Weigh-In List");
            UpdLanguageLabel(cfg, "rtm-score3", "Real-Time Records");
            UpdLanguageLabel(cfg, "rank-list", "Results");
            UpdLanguageLabel(cfg, "rtm-site", "Running Order");
            UpdLanguageLabel(cfg, "tidbits-collection", "Photos");
            UpdLanguageLabel(cfg, "meeting-information", "Information");
        }

        private void UpdLanguageLabel(TConfig cfg, string in_value, string in_label_en)
        {
            string sql = "";
            sql = "UPDATE IN_MEETING_SHORTURL SET in_label_en = '" + in_label_en + "' WHERE in_meeting = '" + cfg.meeting_id + "' AND in_value = '" + in_value + "'";
            cfg.inn.applySQL(sql);
        }

        #region 競管平台

        private void CreateAdminPage(TConfig cfg)
        {
            Item itmDays = cfg.inn.applySQL("SELECT DISTINCT in_date_key FROM IN_MEETING_ALLOCATION WITH(NOLOCK) WHERE in_meeting = '" + cfg.meeting_id + "' ORDER BY in_date_key");

            //首頁
            var lnkIndex = new TLink { val = "admin", lbl = "首頁", sort = "1", page = "AdminIndex.html" };
            Item itmIndex = GetMShortUrl(cfg, "", lnkIndex);
            //itmIndex = IndexPage(cfg, lnkIndex);

            //功能頁
            var lnkFunc = new TLink { val = "admin-func", lbl = cfg.mt_title, sort = "1", page = "AdminFunc.html" };
            Item itmFunc = GetMShortUrl(cfg, cfg.meeting_id, lnkFunc);
            if (itmFunc.isError() || itmFunc.getResult() == "")
            {
                lnkFunc.sort = GetFuncSort(cfg, lnkFunc);
                itmFunc = CommonPage(cfg, itmIndex, lnkFunc);
            }
            else
            {
                lnkFunc.sort = itmFunc.getProperty("in_sort_order", "");
                itmFunc = CommonPage(cfg, itmIndex, lnkFunc);
            }

            //過磅
            var lnkWeight = new TLink { val = "admin-weight", lbl = "過磅", sort = "1", page = "AdminWeight.html" };
            Item itmWeight = GetMShortUrl(cfg, cfg.meeting_id, lnkWeight);
            itmWeight = CommonFunc(cfg, itmFunc, lnkWeight, WeighInUrl);

            ////過磅-比賽日期
            //AddFightDayFuncs(cfg, itmWeight, lnkWeight, itmDays, true);

            //抽籤
            var lnkDraw = new TLink { val = "admin-draw", lbl = "抽籤", sort = "2" };
            Item itmDraw = GetMShortUrl(cfg, cfg.meeting_id, lnkDraw);
            itmDraw = CommonFunc(cfg, itmFunc, lnkDraw, DrawUrl);

            //對戰表
            var lnkBattleTree = new TLink { val = "admin-tree", lbl = "對戰表", sort = "3" };
            Item itmBattleTree = GetMShortUrl(cfg, cfg.meeting_id, lnkBattleTree);
            itmBattleTree = CommonFunc(cfg, itmFunc, lnkBattleTree, TreeUrl);

            //場次編號
            var lnkTreeNo = new TLink { val = "admin-treeno", lbl = "場次編號", sort = "4", page = "AdminSubFunc.html" };
            Item itmTreeNo = GetMShortUrl(cfg, cfg.meeting_id, lnkTreeNo);
            itmTreeNo = CommonPage(cfg, itmFunc, lnkTreeNo);

            //場次編號編輯
            AddTreeNoFuncs(cfg, itmTreeNo);

            //抽磅
            var lnkSpot = new TLink { val = "admin-spotcheck", lbl = "抽磅", sort = "5", page = "AdminWeight.html" };
            Item itmSpot = GetMShortUrl(cfg, cfg.meeting_id, lnkSpot);
            //itmSpot = CommonPage(cfg, itmFunc, lnkSpot);
            itmSpot = CommonFunc(cfg, itmFunc, lnkSpot, SpotWeighInUrl);

            //抽磅-比賽日期
            AddFightDayFuncs(cfg, itmSpot, lnkSpot, itmDays, false);

            //戰情中心
            var lnkCentral = new TLink { val = "admin-central", lbl = "戰情中心", sort = "6" };
            Item itmCentral = GetMShortUrl(cfg, cfg.meeting_id, lnkCentral);
            itmCentral = CommonFunc(cfg, itmFunc, lnkCentral, RtmCentralUrl22);

            //即時成績列表
            var lnkRtmScore = new TLink { val = "admin-rtmscore", lbl = "即時成績列表", sort = "7" };
            Item itmRtmScore = GetMShortUrl(cfg, cfg.meeting_id, lnkRtmScore);
            itmRtmScore = CommonFunc(cfg, itmFunc, lnkRtmScore, RealTimeScoreUrl);

            //場次統計與調場
            var lnkSummary = new TLink { val = "admin-summary", lbl = "場次統計與調場", sort = "8" };
            Item itmSummary = GetMShortUrl(cfg, cfg.meeting_id, lnkSummary);
            itmSummary = CommonFunc(cfg, itmFunc, lnkSummary, SummaryUrl);

            //比賽進度表(檢錄)
            var lnkCheckIn = new TLink { val = "admin-checkin", lbl = "比賽進度表(檢錄)", sort = "9" };
            Item itmCheckIn = GetMShortUrl(cfg, cfg.meeting_id, lnkCheckIn);
            itmCheckIn = CommonFunc(cfg, itmFunc, lnkCheckIn, CheckInUrl);

            //獎牌戰
            var lnkMedal = new TLink { val = "admin-medal", lbl = "獎牌戰(挑戰賽)", sort = "10" };
            Item itmMedal = GetMShortUrl(cfg, cfg.meeting_id, lnkMedal);
            itmMedal = CommonFunc(cfg, itmFunc, lnkMedal, MedalUrl);

            //名次公告
            var lnkRank = new TLink { val = "admin-rank", lbl = "名次公告", sort = "11" };
            Item itmRank = GetMShortUrl(cfg, cfg.meeting_id, lnkRank);
            itmRank = CommonFunc(cfg, itmFunc, lnkRank, RankUrl);


            //賽程管理
            var lnkRule = new TLink { val = "admin-rule", lbl = "賽程管理", sort = "12", page = "AdminSubFunc.html" };
            Item itmRule = GetMShortUrl(cfg, cfg.meeting_id, lnkRule);
            itmRule = CommonPage(cfg, itmFunc, lnkRule);

            //賽程管理-細項功能
            AddRuleFuncs(cfg, itmRule);

            //團體賽
            var lnkTeamBattle = new TLink { val = "admin-team", lbl = "團體賽", sort = "13", page = "AdminSubFunc.html" };
            Item itmTeamBattle = GetMShortUrl(cfg, cfg.meeting_id, lnkTeamBattle);
            itmTeamBattle = CommonPage(cfg, itmFunc, lnkTeamBattle);

            //團體賽-細項功能
            AddTeamFuncs(cfg, itmTeamBattle);

            //選手資訊
            var lnkRtmPlayer = new TLink { val = "admin-player", lbl = "選手資訊", sort = "14" };
            Item itmRtmPlayer = GetMShortUrl(cfg, cfg.meeting_id, lnkRtmPlayer);
            itmRtmPlayer = CommonFunc(cfg, itmFunc, lnkRtmPlayer, RtmPlayerUrl);
        }

        //賽程管理-細項功能
        private void AddRuleFuncs(TConfig cfg, Item itmParent)
        {
            //賽程管理-賽事後台
            var lnkManager = new TLink { val = "admin-rule-manager", lbl = "賽事後台", sort = "2" };
            Item itmManager = GetMShortUrl(cfg, cfg.meeting_id, lnkManager);
            itmManager = CommonFunc(cfg, itmParent, lnkManager, ManagerSetting);

            //賽程管理-賽制設定
            var lnkSetting = new TLink { val = "admin-rule-setting", lbl = "賽制設定", sort = "2" };
            Item itmSetting = GetMShortUrl(cfg, cfg.meeting_id, lnkSetting);
            itmSetting = CommonFunc(cfg, itmParent, lnkSetting, RuleSetting);

            //賽程管理-賽會參數設定
            var lnkVariable = new TLink { val = "admin-rule-variable", lbl = "過磅體重設定", sort = "3" };
            Item itmVariable = GetMShortUrl(cfg, cfg.meeting_id, lnkVariable);
            itmVariable = CommonFunc(cfg, itmParent, lnkVariable, VariableUrl);

            //賽程管理-過磅體重設定
            var lnkWeight = new TLink { val = "admin-rule-weight", lbl = "過磅體重設定", sort = "4" };
            Item itmWeight = GetMShortUrl(cfg, cfg.meeting_id, lnkWeight);
            itmWeight = CommonFunc(cfg, itmParent, lnkWeight, RuleWeight);

            //賽程管理-場地分配
            var lnkSite = new TLink { val = "admin-rule-site", lbl = "場地分配", sort = "5" };
            Item itmSite = GetMShortUrl(cfg, cfg.meeting_id, lnkSite);
            itmSite = CommonFunc(cfg, itmParent, lnkSite, RuleSite);

            ////賽程管理-團體賽設定
            //var lnkTeams = new TLink { val = "admin-rule-team", lbl = "團體賽設定", sort = "6" };
            //Item itmTeams = GetMShortUrl(cfg, cfg.meeting_id, lnkTeams);
            //itmTeams = CommonFunc(cfg, itmParent, lnkTeams, RuleTeams);

            //賽程管理-參賽者管理
            var lnkPlayer = new TLink { val = "admin-rule-player", lbl = "參賽者管理", sort = "7" };
            Item itmPlayer = GetMShortUrl(cfg, cfg.meeting_id, lnkPlayer);
            itmPlayer = CommonFunc(cfg, itmParent, lnkPlayer, RulePlayer);

            //賽程管理-上傳賽程資料
            var lnkUpload = new TLink { val = "admin-rule-upload", lbl = "上傳賽程資料", sort = "8" };
            Item itmUpload = GetMShortUrl(cfg, cfg.meeting_id, lnkUpload);
            itmUpload = CommonFunc(cfg, itmParent, lnkUpload, RuleUploadNew);
        }

        //賽程管理-賽制設定
        private string RuleSetting(TConfig cfg)
        {
            return "/pages/c.aspx"
                + "?page=In_MeetingProgram.html"
                + "&method=in_meeting_program"
                + "&meeting_id=" + cfg.meeting_id
                + "&scene=";
        }

        //賽程管理-賽會參數設定
        private string VariableUrl(TConfig cfg)
        {
            return "/pages/c.aspx"
                + "?page=In_MeetingVariable.html"
                + "&method=in_meeting_variable"
                + "&meeting_id=" + cfg.meeting_id
                + "&scene=";
        }

        //賽程管理-賽事後台
        private string ManagerSetting(TConfig cfg)
        {
            return "/pages/c.aspx"
                + "?page=In_MeetingView.html"
                + "&method=In_Get_SingleItemInfoGeneral"
                + "&itemtype=In_Meeting"
                + "&itemid=" + cfg.meeting_id
                + "&mode=latest";
        }

        //賽程管理-過磅體重設定
        private string RuleWeight(TConfig cfg)
        {
            return "/pages/c.aspx"
                + "?page=In_MeetingWeight.html"
                + "&method=in_meeting_weight"
                + "&meeting_id=" + cfg.meeting_id;
        }

        //賽程管理-場地設定
        private string RuleSite(TConfig cfg)
        {
            return "/pages/c.aspx"
                + "?page=In_MeetingSite_Allocate.html"
                + "&method=In_meeting_allocation"
                + "&meeting_id=" + cfg.meeting_id
                + "&mode=edit"
                + "&link=weight";
        }

        //賽程管理-團體賽設定
        private string RuleTeams(TConfig cfg)
        {
            return "/pages/c.aspx"
                + "?page=in_team_battle_setting.html"
                + "&method=in_team_battle_setting"
                + "&meeting_id=" + cfg.meeting_id;
        }

        //賽程管理-參賽者管理
        private string RulePlayer(TConfig cfg)
        {
            return "/pages/c.aspx"
                + "?page=In_MeetingProgram_Player.html"
                + "&method=in_meeting_pteam"
                + "&meeting_id=" + cfg.meeting_id;
        }

        //賽程管理-上傳賽程資料
        private string RuleUploadOld(TConfig cfg)
        {
            return "/pages/c.aspx"
                + "?page=In_MeetingSite_Upload.html"
                + "&method=In_meeting_allocation"
                + "&meeting_id=" + cfg.meeting_id
                + "&scene=upload";
        }

        //賽程管理-上傳賽程資料(New)
        private string RuleUploadNew(TConfig cfg)
        {
            return "/pages/c.aspx"
                + "?page=In_MeetingSite_UploadNew.html"
                + "&method=In_Meeting_Program_Upload"
                + "&meeting_id=" + cfg.meeting_id
                + "&scene=site_page";
        }


        //團體賽-細項功能
        private void AddTeamFuncs(TConfig cfg, Item itmParent)
        {
            //團體賽-註冊
            var lnkRegister = new TLink { val = "admin-team-register", lbl = "註冊", sort = "1" };
            Item itmRegister = GetMShortUrl(cfg, cfg.meeting_id, lnkRegister);
            itmRegister = CommonFunc(cfg, itmParent, lnkRegister, TeamBattleRegisterUrl);

            //團體賽-過磅
            var lnkWeight = new TLink { val = "admin-team-weight", lbl = "過磅", sort = "2" };
            Item itmWeight = GetMShortUrl(cfg, cfg.meeting_id, lnkWeight);
            itmWeight = CommonFunc(cfg, itmParent, lnkWeight, TeamBattleWeightUrl);

            //團體賽-出賽
            var lnkFight = new TLink { val = "admin-team-fight", lbl = "出賽", sort = "3" };
            Item itmFight = GetMShortUrl(cfg, cfg.meeting_id, lnkFight);
            itmFight = CommonFunc(cfg, itmParent, lnkFight, TeamBattleFightUrl);

            ////團體賽-成績總表
            //var lnkScore = new TLink { val = "admin-team-score", lbl = "成績總表", sort = "4" };
            //Item itmScore = GetMShortUrl(cfg, cfg.meeting_id, lnkScore);
            //itmScore = CommonFunc(cfg, itmParent, lnkScore, TeamBattleScoreUrl);

            //賽程管理-團體賽設定
            var lnkTeams = new TLink { val = "admin-rule-team", lbl = "量級設定", sort = "5" };
            Item itmTeams = GetMShortUrl(cfg, cfg.meeting_id, lnkTeams);
            itmTeams = CommonFunc(cfg, itmParent, lnkTeams, RuleTeams);
        }

        //團體戰-註冊
        private string TeamBattleRegisterUrl(TConfig cfg)
        {
            return "/pages/c.aspx"
                + "?page=in_team_battle_register.html"
                + "&method=in_team_battle_register"
                + "&meeting_id=" + cfg.meeting_id
                + "&scene=";
        }

        //團體戰-過磅
        private string TeamBattleWeightUrl(TConfig cfg)
        {
            return "/pages/c.aspx"
                + "?page=in_team_battle_weight.html"
                + "&method=in_team_battle_register"
                + "&meeting_id=" + cfg.meeting_id
                + "&scene=weight";
        }

        //團體戰-出賽
        private string TeamBattleFightUrl(TConfig cfg)
        {
            return "/pages/c.aspx"
                + "?page=in_team_battle_fight.html"
                + "&method=in_team_battle_fight"
                + "&meeting_id=" + cfg.meeting_id
                + "&scene=";
        }

        //團體戰-成績總表
        private string TeamBattleScoreUrl(TConfig cfg)
        {
            return "/pages/c.aspx"
                + "?page=In_Team_Battle_Score.html"
                + "&method=in_team_battle_fight"
                + "&meeting_id=" + cfg.meeting_id
                + "&scene=score";
        }

        //名次公告
        private string RankUrl(TConfig cfg)
        {
            return "/pages/c.aspx"
                + "?page=In_Competition_Bulletin_N1.html"
                + "&method=in_meeting_program_bulletin_print"
                + "&meeting_id=" + cfg.meeting_id;
        }

        //獎牌戰
        private string MedalUrl(TConfig cfg)
        {
            return "/pages/c.aspx"
                + "?page=In_Meeting_Medal.html"
                + "&method=in_meeting_medal"
                + "&meeting_id=" + cfg.meeting_id;
        }

        //Overview Page
        private string OverviewUrl(TConfig cfg)
        {
            return "/pages/PublicFuncS.html"
                + "?meeting_id=" + cfg.meeting_id;
        }

        //比賽進度表(檢錄)
        private string CheckInUrl(TConfig cfg)
        {
            string resource = "/pages/public/NextEvent.html"
                + "?meeting_id=" + cfg.meeting_id;

            if (cfg.is_english)
            {
                resource = resource.Replace("NextEvent.html", "NextEvent_En.html");
            }

            return resource;
        }

        //場次統計與調場
        private string SummaryUrl(TConfig cfg)
        {
            return "/pages/c.aspx"
                + "?page=In_Competition_Summary.html"
                + "&method=in_meeting_pevent_sum"
                + "&meeting_id=" + cfg.meeting_id;
        }

        //即時成績列表
        private string RealTimeScoreUrl(TConfig cfg)
        {
            string resource = "/pages/public/NewScore.html"
                + "?meeting_id=" + cfg.meeting_id;

            if (cfg.is_english)
            {
                resource = resource.Replace("NewScore.html", "NewScore_En.html");
            }

            return resource;
        }

        //調整場次順序(列表編輯)
        private string TreeNoEdit1Url(TConfig cfg)
        {
            return "/pages/c.aspx"
                + "?page=TableRowDrag.html"
                + "&method=in_meeting_allocation_drag"
                + "&meeting_id=" + cfg.meeting_id;
        }

        //調整場次順序(樹狀編輯)
        private string TreeNoEdit2Url(TConfig cfg)
        {
            return "/pages/c.aspx"
                + "?page=in_competition_preview.html"
                + "&method=in_meeting_program_preview"
                + "&meeting_id=" + cfg.meeting_id
                + "&eno=edit";
        }

        //對戰表(含單位數量)
        private string TreeUrl(TConfig cfg)
        {
            return "/pages/c.aspx"
                + "?page=In_Competition_Preview.html"
                + "&method=in_meeting_program_preview"
                + "&meeting_id=" + cfg.meeting_id
                + "&mode=draw";
        }

        //過磅
        private string WeighInUrl(TConfig cfg)
        {
            return "/pages/c.aspx"
                + "?page=Inspection_check_group.html"
                + "&method=In_Meeting_GetCheckByGroup"
                + "&meeting_id=" + cfg.meeting_id;
        }

        //抽籤
        private string DrawUrl(TConfig cfg)
        {
            return "/pages/c.aspx"
                + "?page=In_MeetingDraw.html"
                + "&method=in_meeting_draw"
                + "&meeting_id=" + cfg.meeting_id;
        }

        //抽磅
        private string SpotWeighInUrl(TConfig cfg)
        {
            return "/pages/c.aspx"
                + "?page=Inspection_spotcheck_group.html"
                + "&method=in_meeting_program_spotcheck"
                + "&meeting_id=" + cfg.meeting_id;
        }

        private void AddFightDayFuncs(TConfig cfg, Item itmParent, TLink lnkParent, Item items, bool is_weight)
        {
            if (items.isError() || items.getResult() == "")
            {
                return;
            }

            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string code = (i + 1).ToString();
                string value = lnkParent.val + "-site-" + code.PadLeft(2, '0');
                string label = "比賽日期 " + item.getProperty("in_date_key", "");
                string key = item.getProperty("in_date_key", "");

                var link = new TLink { val = value, lbl = label, sort = code, key = key };

                WeightDayFunc(cfg, itmParent, link, is_weight);
            }
        }

        //場次編號編輯
        private void AddTreeNoFuncs(TConfig cfg, Item itmTreeNo)
        {
            //場次編號
            var lnkEdit1 = new TLink { val = "admin-treeno-edit-01", lbl = "拖曳編輯", sort = "1" };
            Item itmEdit1 = GetMShortUrl(cfg, cfg.meeting_id, lnkEdit1);
            itmEdit1 = CommonFunc(cfg, itmTreeNo, lnkEdit1, TreeNoEdit1Url);

            //場次編號
            var lnkEdit2 = new TLink { val = "admin-treeno-edit-02", lbl = "樹狀編輯", sort = "2" };
            Item itmEdit2 = GetMShortUrl(cfg, cfg.meeting_id, lnkEdit2);
            itmEdit2 = CommonFunc(cfg, itmTreeNo, lnkEdit2, TreeNoEdit2Url);
        }

        //過磅與抽磅-比賽日期
        private Item WeightDayFunc(TConfig cfg, Item itmParent, TLink link, bool is_weight)
        {
            string parent_id = itmParent.getProperty("id", "");

            string resource = "";

            if (is_weight)
            {
                resource = "/pages/c.aspx"
                + "?page=Inspection_check_group.html"
                + "&method=In_Meeting_GetCheckByGroup"
                + "&meeting_id=" + cfg.meeting_id
                + "&in_date=" + link.key
                + "";
            }
            else
            {
                resource = "/pages/c.aspx"
                + "?page=Inspection_spotcheck_group.html"
                + "&method=in_meeting_program_spotcheck"
                + "&meeting_id=" + cfg.meeting_id
                + "&in_date=" + link.key
                + "";
            }

            //Aras
            var api_entity = GetUrlEntity(cfg, resource);

            Item item = cfg.inn.newItem("IN_MEETING_SHORTURL", "merge");
            item.setAttribute("where", "in_meeting = '" + cfg.meeting_id + "' AND in_value = '" + link.val + "'");
            item.setProperty("in_meeting", cfg.meeting_id);
            item.setProperty("in_parent", parent_id);

            SetProperties(item, link.val, link.lbl, link.sort, api_entity);

            item = item.apply();

            if (item.isError() || item.getResult() == "")
            {
                throw new Exception("競管平台-" + link.lbl + ": 建立失敗");
            }

            return item;
        }

        #endregion 競管平台

        private void CreateOpenPage(TConfig cfg)
        {
            //首頁
            var lnkIndex = new TLink { val = "index", lbl = "首頁", sort = "1", page = "PublicIndex.html" };
            Item itmIndex = GetMShortUrl(cfg, "", lnkIndex);
            itmIndex = IndexPage(cfg, lnkIndex);

            //if (itmIndex.isError() || itmIndex.getResult() == "")
            //{
            //    itmIndex = IndexPage(cfg, lnkIndex);
            //}

            // //功能頁
            // var lnkFunc = new TLink { val = "function", lbl = cfg.mt_title, sort = "1", page = "PublicFunc.html" };
            // Item itmFunc = GetMShortUrl(cfg, cfg.meeting_id, lnkFunc);
            // if (itmFunc.isError() || itmFunc.getResult() == "")
            // {
            //     lnkFunc.sort = GetFuncSort(cfg, lnkFunc);
            //     itmFunc = CommonPage(cfg, itmIndex, lnkFunc);
            // }
            // else
            // {
            //     lnkFunc.sort = itmFunc.getProperty("in_sort_order", "");
            //     itmFunc = CommonPage(cfg, itmIndex, lnkFunc);
            // }

            //功能頁
            var lnkFunc = new TLink { val = "function", lbl = cfg.mt_title, sort = "1" };
            Item itmFunc = GetMShortUrl(cfg, cfg.meeting_id, lnkFunc);
            if (itmFunc.isError() || itmFunc.getResult() == "")
            {
                lnkFunc.sort = GetFuncSort(cfg, lnkFunc);
                itmFunc = CommonFunc(cfg, itmIndex, lnkFunc, OverviewUrl, in_is_closed: "0");
            }
            else
            {
                lnkFunc.sort = itmFunc.getProperty("in_sort_order", "");
                itmFunc = CommonFunc(cfg, itmIndex, lnkFunc, OverviewUrl, in_is_closed: "0");
            }

            //活動資訊
            var lnkMtInfo = new TLink { val = "meeting-information", lbl = "活動資訊", sort = "100" };
            Item itmMtInfo = GetMShortUrl(cfg, cfg.meeting_id, lnkMtInfo);
            itmMtInfo = CommonFunc(cfg, itmFunc, lnkMtInfo, MtInfoUrl);

            //比賽進度表(檢錄)
            var lnkCheckIn = new TLink { val = "checkin", lbl = "比賽進度表(檢錄)", sort = "200" };
            Item itmCheckIn = GetMShortUrl(cfg, cfg.meeting_id, lnkCheckIn);
            itmCheckIn = CommonFunc(cfg, itmFunc, lnkCheckIn, CheckInUrl, in_is_closed: "0");

            ////檢錄頁-各場地

            //最新成績(戰情中心)-列表版
            var lnkRtmScore = new TLink { val = "rtm-score-list", lbl = "最新成績(戰情中心)", sort = "300" };
            Item itmRtmScore = GetMShortUrl(cfg, cfg.meeting_id, lnkRtmScore);
            itmRtmScore = CommonFunc(cfg, itmFunc, lnkRtmScore, RtmScoreUrl, in_is_closed: "1");

            // //戰情中心(檢錄)-手機版
            // var lnkRtmScore2 = new TLink { val = "rtm-score", lbl = "戰情中心(檢錄)", sort = "2" };
            // Item itmRtmScore2 = GetMShortUrl(cfg, cfg.meeting_id, lnkRtmScore2);
            // itmRtmScore2 = CommonFunc(cfg, itmFunc, lnkRtmScore2, RtmCentralUrl04, in_is_closed: "0");

            //戰情中心
            var lnkRtmScore3 = new TLink { val = "rtm-score3", lbl = "戰情中心", sort = "400" };
            Item itmRtmScore3 = GetMShortUrl(cfg, cfg.meeting_id, lnkRtmScore3);
            itmRtmScore3 = CommonFunc(cfg, itmFunc, lnkRtmScore3, RtmCentralUrl22, in_is_closed: "0");

            //對戰表
            var lnkRtmTree = new TLink { val = "rtm-tree", lbl = "對戰表", sort = "500" };
            Item itmRtmTree = GetMShortUrl(cfg, cfg.meeting_id, lnkRtmTree);
            itmRtmTree = CommonFunc(cfg, itmFunc, lnkRtmTree, RtmTreeUrl);

            //選手資訊
            var lnkRtmPlayer = new TLink { val = "rtm-player", lbl = "選手資訊", sort = "600" };
            Item itmRtmPlayer = GetMShortUrl(cfg, cfg.meeting_id, lnkRtmPlayer);
            itmRtmPlayer = CommonFunc(cfg, itmFunc, lnkRtmPlayer, RtmPlayerUrl);

            //場次查詢(選手場地資訊)
            var lnkRtmSite = new TLink { val = "rtm-site", lbl = "場次查詢", sort = "700" };
            Item itmRtmSite = GetMShortUrl(cfg, cfg.meeting_id, lnkRtmSite);
            itmRtmPlayer = CommonFunc(cfg, itmFunc, lnkRtmSite, RtmPlayerSiteUrl);

            //場地分配表
            var lnkAllocate = new TLink { val = "allocate-site", lbl = "場地分配表", sort = "800" };
            Item itmAllocate = GetMShortUrl(cfg, cfg.meeting_id, lnkAllocate);
            itmAllocate = CommonFunc(cfg, itmFunc, lnkAllocate, AllocateSiteUrl);

            //名次查詢
            var lnkRank = new TLink { val = "rank-list", lbl = "名次查詢", sort = "900" };
            Item itmRank = GetMShortUrl(cfg, cfg.meeting_id, lnkRank);
            itmRank = CommonFunc(cfg, itmFunc, lnkRank, RankListUrl);

            //抽磅公告
            var lnkSpotCheck = new TLink { val = "spotcheck-announce", lbl = "抽磅公告", sort = "1000" };
            Item itmSpotCheck = GetMShortUrl(cfg, cfg.meeting_id, lnkSpotCheck);
            itmSpotCheck = CommonFunc(cfg, itmFunc, lnkSpotCheck, SpotcheckAnnounceUrl);

            //花絮集錦
            var lnkTidbits = new TLink { val = "tidbits-collection", lbl = "花絮集錦", sort = "1100" };
            Item itmTidbits = GetMShortUrl(cfg, cfg.meeting_id, lnkTidbits);
            itmTidbits = CommonFunc(cfg, itmFunc, lnkTidbits, TitbitsUrl);
        }

        private string GetVariable(TConfig cfg, string in_name)
        {
            Item item = cfg.inn.applySQL("SELECT TOP 1 in_value FROM IN_VARIABLE WITH(NOLOCK) WHERE in_name = '" + in_name + "'");
            if (item.isError() || item.getResult() == "")
            {
                throw new Exception("查無 " + in_name + " 站台網址");
            }
            return item.getProperty("in_value", "").TrimEnd('/');
        }

        private Item GetMShortUrl(TConfig cfg, string meeting_id, TLink link)
        {
            string sql = "SELECT TOP 1 * FROM IN_MEETING_SHORTURL WITH(NOLOCK)"
                + " WHERE ISNULL(in_meeting, '') = '" + meeting_id + "'"
                + " AND in_value = '" + link.val + "'";

            return cfg.inn.applySQL(sql);
        }

        //首頁
        private Item IndexPage(TConfig cfg, TLink link)
        {
            string resource = "/pages/b.aspx"
                + "?page=" + link.page
                + "&method=in_qrcode_query1"
                + "&meeting_id="
                + "&func_id=" + link.val;

            //Aras
            var api_entity = GetUrlEntity(cfg, resource);

            Item item = cfg.inn.newItem("IN_MEETING_SHORTURL", "merge");
            item.setAttribute("where", "in_value = '" + link.val + "'");

            SetProperties(item, link.val, link.lbl, link.sort, api_entity);

            item = item.apply();

            if (item.isError() || item.getResult() == "")
            {
                throw new Exception("競賽連結中心-首頁: 建立失敗");
            }

            return item;
        }

        //功能頁
        private Item CommonPage(TConfig cfg, Item itmParent, TLink link)
        {
            string parent_id = itmParent.getProperty("id", "");

            string resource = "/pages/b.aspx"
                + "?page=" + link.page
                + "&method=in_qrcode_query1"
                + "&meeting_id=" + cfg.meeting_id
                + "&func_id=" + link.val;

            //Aras
            var api_entity = GetUrlEntity(cfg, resource);

            Item item = cfg.inn.newItem("IN_MEETING_SHORTURL", "merge");
            item.setAttribute("where", "in_meeting = '" + cfg.meeting_id + "' AND in_value = '" + link.val + "'");
            item.setProperty("in_meeting", cfg.meeting_id);
            item.setProperty("in_parent", parent_id);

            SetProperties(item, link.val, link.lbl, link.sort, api_entity);

            item = item.apply();

            if (item.isError() || item.getResult() == "")
            {
                throw new Exception("競賽連結中心-" + link.lbl + ": 建立失敗");
            }

            return item;
        }

        //通用 Func
        private Item CommonFunc(TConfig cfg, Item itmParent, TLink link, Func<TConfig, string> getUrl, string in_is_closed = "0")
        {
            string parent_id = itmParent.getProperty("id", "");

            string resource = getUrl(cfg);

            //Aras
            var api_entity = GetUrlEntity(cfg, resource);

            Item item = cfg.inn.newItem("IN_MEETING_SHORTURL", "merge");
            item.setAttribute("where", "in_meeting = '" + cfg.meeting_id + "' AND in_value = '" + link.val + "'");
            item.setProperty("in_meeting", cfg.meeting_id);
            item.setProperty("in_parent", parent_id);

            SetProperties(item, link.val, link.lbl, link.sort, api_entity);
            item.setProperty("in_is_closed", in_is_closed);

            item = item.apply();

            if (item.isError() || item.getResult() == "")
            {
                throw new Exception("競賽連結中心-" + link.lbl + ": 建立失敗");
            }

            return item;
        }

        //最新成績(戰情中心)
        private string RtmScoreUrl(TConfig cfg)
        {
            return "/pages/b.aspx"
                + "?page=PublicScoreSearch.html"
                + "&method=in_meeting_program_score_s"
                + "&meeting_id=" + cfg.meeting_id
                + "&open=1";
        }

        //戰情中心(檢錄)
        private string RtmCentralUrl04(TConfig cfg)
        {
            return RtmCentralUrl(cfg, "0", "4");
        }

        //戰情中心(最新成績)
        private string RtmCentralUrl22(TConfig cfg)
        {
            return RtmCentralUrl(cfg, "2", "2");
        }

        //最新成績(戰情中心)
        private string RtmCentralUrl(TConfig cfg, string fought, string waiting)
        {
            string resource = "/pages/public/Monitor.html"
                + "?meeting_id=" + cfg.meeting_id
                + "&fought=" + fought
                + "&waiting=" + waiting;

            if (cfg.is_english)
            {
                resource = resource.Replace("Monitor.html", "Monitor_En.html");
            }

            return resource;
        }

        //對戰表
        private string RtmTreeUrl(TConfig cfg)
        {
            string resource = "/pages/public/FightTreeV1.html"
                + "?meeting_id=" + cfg.meeting_id;

            if (cfg.is_english)
            {
                resource = resource.Replace("FightTreeV1.html", "FightTreeV1_En.html");
            }

            return resource;
        }

        //選手資訊
        private string RtmPlayerUrl(TConfig cfg)
        {
            string resource = "/pages/public/PlayerInfo.html"
                + "?meeting_id=" + cfg.meeting_id;

            if (cfg.is_english)
            {
                resource = resource.Replace("PlayerInfo.html", "PlayerInfo_En.html");
            }

            return resource;
        }

        //選手場地資訊
        private string RtmPlayerSiteUrl(TConfig cfg)
        {
            string resource = "/pages/public/PlayerSite.html"
                + "?meeting_id=" + cfg.meeting_id;

            if (cfg.is_english)
            {
                resource = resource.Replace("PlayerSite.html", "PlayerSite_En.html");
            }

            return resource;
        }

        //場地分配表
        private string AllocateSiteUrl(TConfig cfg)
        {
            string resource = "/pages/public/SectSite.html"
                + "?meeting_id=" + cfg.meeting_id;

            if (cfg.is_english)
            {
                resource = resource.Replace("SectSite.html", "SectSite_En.html");
            }

            return resource;
        }

        //名次查詢
        private string RankListUrl(TConfig cfg)
        {
            string resource = "/pages/public/RankList.html"
                + "?meeting_id=" + cfg.meeting_id;

            if (cfg.is_english)
            {
                resource = resource.Replace("RankList.html", "RankList_En.html");
            }

            return resource;
        }

        //抽磅公告
        private string SpotcheckAnnounceUrl(TConfig cfg)
        {
            string resource = "/pages/public/RandomWeighIn.html"
                + "?meeting_id=" + cfg.meeting_id;

            if (cfg.is_english)
            {
                resource = resource.Replace("RandomWeighIn.html", "RandomWeighIn_En.html");
            }

            return resource;
        }

        //活動資訊
        private string MtInfoUrl(TConfig cfg)
        {
            string resource = "/pages/public/MeetingInfo.html"
                + "?meeting_id=" + cfg.meeting_id;

            if (cfg.is_english)
            {
                resource = resource.Replace("MeetingInfo.html", "MeetingInfo_En.html");
            }

            return resource;
        }

        //活動花絮
        private string TitbitsUrl(TConfig cfg)
        {
            string resource = "/pages/public/PhotoTreeV1.html"
                + "?meeting_id=" + cfg.meeting_id;

            if (cfg.is_english)
            {
                resource = resource.Replace("PhotoTreeV1.html", "PhotoTreeV1_En.html");
            }

            return resource;
        }

        private void SetProperties(Item item, string in_value, string in_label, string in_sort, TUrl api_entity)
        {
            item.setProperty("in_value", in_value);
            item.setProperty("in_label", in_label);
            item.setProperty("in_sort_order", in_sort);

            item.setProperty("in_resource", api_entity.Resource);
            item.setProperty("in_api_srcurl", api_entity.FullUrl);
            item.setProperty("in_api_shorturl", api_entity.ShortUrl);
            item.setProperty("in_api_token", api_entity.Token);
            item.setProperty("in_api_qrcode", "");
        }

        private TUrl GetUrlEntity(TConfig cfg, string resource)
        {
            TUrl result = new TUrl
            {
                Resource = resource,
                FullUrl = cfg.plm_url.TrimEnd('/') + "/" + resource.TrimStart('/'),
                ShortUrl = "",
                Token = "",
            };

            result.ShortUrl = GetApiShortUrl(cfg, result.FullUrl);

            if (result.ShortUrl == "")
            {
                throw new Exception("建立短網址發生錯誤 _# " + result.FullUrl);
            }
            else
            {
                result.Token = result.ShortUrl.Split('/').Last();
            }

            return result;
        }

        private string GetPlmMtUrl(TConfig cfg, string page, string func_id)
        {
            return "/pages/b.aspx"
                + "?page=" + page
                + "&method=in_qrcode_query1"
                + "&meeting_id=" + cfg.meeting_id
                + "&func_id=" + func_id
                + "";
        }

        //通報 WebSocket
        private void CallSportHub(TConfig cfg)
        {
            try
            {
                var rootUrl = GetVariable(cfg);
                var rsrcUrl = rootUrl + "/Judo/ScheduleMoveSite?token=0225585561";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(rsrcUrl);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                StreamReader tReader = new StreamReader(response.GetResponseStream());

                string rst = tReader.ReadToEnd();
                if (!string.IsNullOrWhiteSpace(rst))
                {
                    var result = rst.Replace("\"", "");
                }
            }
            catch (Exception ex)
            {
                
            }
        }

        private string GetVariable(TConfig cfg)
        {
            Item item = cfg.inn.applySQL("SELECT TOP 1 in_value FROM IN_VARIABLE WITH(NOLOCK) WHERE in_name = 'api_url'");
            if (item.isError() || item.getResult() == "")
            {
                return "";
            }
            else
            {
                return item.getProperty("in_value", "").TrimEnd('/');
            }
        }

        //生成短網址
        private string GetApiShortUrl(TConfig cfg, string srcUrl)
        {
            string result = "";

            try
            {
                string tgturl = System.Web.HttpUtility.UrlEncode(srcUrl);
                string fullurl = cfg.api_url + "/create?url=" + tgturl;
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, fullurl);

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(fullurl);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                StreamReader tReader = new StreamReader(response.GetResponseStream());

                string rst = tReader.ReadToEnd();
                if (!string.IsNullOrWhiteSpace(rst))
                {
                    result = rst.Replace("\"", "");
                }
            }
            catch (Exception ex)
            {
                result = "";
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, ex.Message);
            }

            return result;
        }

        // //生成 QrCode
        // private string[] GenerateQrCode(TConfig cfg, string name, string contents)
        // {
        //     try
        //     {
        //         string folder = cfg.iplm_qrcode_folder;
        //         if (!System.IO.Directory.Exists(folder))
        //         {
        //             System.IO.Directory.CreateDirectory(folder);
        //         }

        //         string file = name + ".jpg";
        //         string img_file = System.IO.Path.Combine(folder, file);
        //         string img_url = cfg.iplm_qrcode_url + "/" + file;

        //         if (!System.IO.File.Exists(img_file))
        //         {
        //             var writer = new ZXing.BarcodeWriter  //dll裡面可以看到屬性
        //             {
        //                 Format = ZXing.BarcodeFormat.QR_CODE,
        //                 Options = new ZXing.QrCode.QrCodeEncodingOptions //設定大小
        //                 {
        //                     Height = 500,
        //                     Width = 500,
        //                     Margin = 0,
        //                     //CharacterSet = "UTF-8",
        //                     //ErrorCorrection = ZXing.QrCode.Internal.ErrorCorrectionLevel.M
        //                 },
        //             };

        //             var img = writer.Write(contents);
        //             var myBitmap = new System.Drawing.Bitmap(img);

        //             myBitmap.Save(img_file, System.Drawing.Imaging.ImageFormat.Jpeg);
        //         }

        //         return new string[] { img_file, img_url };
        //     }
        //     catch (Exception ex)
        //     {
        //         cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, ex.Message);
        //         return new string[] { "", "" };
        //     }
        // }

        private string GetFuncSort(TConfig cfg, TLink link)
        {
            string sql = "SELECT count(*) AS 'cnt' FROM IN_MEETING_SHORTURL WITH(NOLOCK) WHERE in_value = '" + link.val + "'";
            Item itmData = cfg.inn.applySQL(sql);
            if (itmData.isError() || itmData.getResult() == "")
            {
                return "1";
            }

            string cnt = itmData.getProperty("cnt", "0");
            if (cnt == "" || cnt == "0")
            {
                return "1";
            }

            int old_sort = GetIntValue(cnt);
            if (old_sort == 0)
            {
                return "1";
            }
            else
            {
                return (old_sort + 1).ToString();
            }
        }

        private void RefreshSortOrder(TConfig cfg, string in_value)
        {
            //function
            //admin-func
            var sql = @"
                UPDATE t11 SET
	                t11.in_sort_order = t12.rn * 100
                FROM
	                IN_MEETING_SHORTURL t11 WITH(NOLOCK)
                INNER JOIN
                (
	                SELECT
		                t1.id
		                , ROW_NUMBER() OVER (ORDER BY t2.in_date_s DESC) AS rn
	                FROM 
		                IN_MEETING_SHORTURL t1 WITH(NOLOCK)
	                INNER JOIN
		                IN_MEETING t2 WITH(NOLOCK) 
		                ON t2.id = t1.in_meeting
	                WHERE
		                t1.in_value = 'function'
                ) t12 oN t12.id = t11.id
            ";

            sql = sql.Replace("{#in_value}", in_value);

            cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }

            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }

            public string meeting_id { get; set; }
            public string mt_title { get; set; }
            public string mt_language { get; set; }
            public bool is_english { get; set; }

            public string plm_url { get; set; }
            public string api_url { get; set; }

            public int site_count { get; set; }
        }

        private class TLink
        {
            public string val { get; set; }
            public string lbl { get; set; }
            public string sort { get; set; }
            public string page { get; set; }

            public string key { get; set; }
        }

        private class TUrl
        {
            public string Resource { get; set; }
            public string FullUrl { get; set; }
            public string ShortUrl { get; set; }
            public string Token { get; set; }
        }

        private int GetIntValue(string value)
        {
            int result = 0;
            if (int.TryParse(value, out result))
            {
                return result;
            }
            return 0;
        }
    }
}