﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Judo.Common
{
    public class in_qrcode_query : Item
    {
        public in_qrcode_query(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 競賽連結中心-取得連結
                輸入: meeting_id
                日期: 
                    2021-12-09: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]in_qrcode_query";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, this.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                InnH = _InnH,

                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                func_id = itmR.getProperty("func_id", ""),
                is_api = itmR.getProperty("is_api", ""),
            };

            // if (cfg.func_id == "function")
            // {
            //     CCO.Utilities.WriteDebug(strMethodName, "in");
            // }

            string sql = "";

            Item itmSite = cfg.inn.applySQL("SELECT TOP 1 * FROM In_Site WITH(NOLOCK)");
            if (itmSite.getResult() != "")
            {
                string ov_img_pc = "../DownloadMeetingFile.aspx?fileid=" + itmSite.getProperty("in_overview_banner_pc", "");
                string ov_img_mb = "../DownloadMeetingFile.aspx?fileid=" + itmSite.getProperty("in_overview_banner_mb", "");
                string ov_img_2k = "../DownloadMeetingFile.aspx?fileid=" + itmSite.getProperty("in_overview_banner_2k", "");
                itmR.setProperty("ov_img_pc", ov_img_pc);
                itmR.setProperty("ov_img_mb", ov_img_mb);
                itmR.setProperty("ov_img_2k", ov_img_2k);
            }

            //iplm 站台網址
            Item itmPlm = cfg.inn.applySQL("SELECT TOP 1 in_value FROM IN_VARIABLE WITH(NOLOCK) WHERE in_name = 'app_url'");
            if (itmPlm.isError() || itmPlm.getResult() == "")
            {
                throw new Exception("查無 iplm 站台網址");
            }
            cfg.iplm_url = itmPlm.getProperty("in_value", "").TrimEnd('/');

            cfg.need_more = cfg.meeting_id != "" && cfg.func_id == "function";

            //賽事資訊
            AppendMeetingInfo(cfg, itmR);

            //樣式
            AppendBoxCss(cfg, itmR);

            //統計資訊
            AppendStatistics(cfg, itmR);

            //量級組別選單
            AppendWeightMenu(cfg, itmR);

            //對戰表版本
            itmR.setProperty("fight_tree_mode", "2");

            return itmR;
        }

        //統計資訊
        private void AppendWeightMenu(TConfig cfg, Item itmReturn)
        {
            if (!cfg.need_more) return;

            var nodes = new List<TNode>();
            AppendSoloMenu(cfg, nodes);
            AppendTeamMenu(cfg, nodes);

            itmReturn.setProperty("sect_json", Newtonsoft.Json.JsonConvert.SerializeObject(nodes));
        }

        private void AppendTeamMenu(TConfig cfg, List<TNode> nodes)
        {
            var items = GetTeamItems(cfg);
            if (items.getItemCount() <= 0) return;

            var list = GetTeamMenu(cfg, items);

            var key = "團體組";
            var node = new TNode
            {
                Val = key,
                Lbl = key,
                MNodes = new List<TNode>(),
                WNodes = new List<TNode>(),
            };
            nodes.Add(node);

            foreach (var row in list)
            {
                var m = row.MNode;
                var w = row.WNode;
                node.MNodes.Add(m);
                node.WNodes.Add(w);
            }
        }

        private List<TTeamNode> GetTeamMenu(TConfig cfg, Item items)
        {
            var list = new List<TTeamNode>();

            list.Add(new TTeamNode { Age = AgeType.S4, Sect = "社[g]+41歲", MNode = new TNode(), WNode = new TNode() });
            list.Add(new TTeamNode { Age = AgeType.S3, Sect = "社[g]-40歲", MNode = new TNode(), WNode = new TNode() });
            list.Add(new TTeamNode { Age = AgeType.S2, Sect = "社[g]甲", MNode = new TNode(), WNode = new TNode() });
            list.Add(new TTeamNode { Age = AgeType.S1, Sect = "社[g]乙", MNode = new TNode(), WNode = new TNode() });
            list.Add(new TTeamNode { Age = AgeType.C2, Sect = "大[g]甲", MNode = new TNode(), WNode = new TNode() });
            list.Add(new TTeamNode { Age = AgeType.C1, Sect = "大[g]乙", MNode = new TNode(), WNode = new TNode() });
            list.Add(new TTeamNode { Age = AgeType.H1, Sect = "高[g]", MNode = new TNode(), WNode = new TNode() });
            list.Add(new TTeamNode { Age = AgeType.J1, Sect = "國[g]", MNode = new TNode(), WNode = new TNode() });
            list.Add(new TTeamNode { Age = AgeType.E1, Sect = "小[g]", MNode = new TNode(), WNode = new TNode() });


            var count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var id = item.getProperty("id");
                var in_l2 = item.getProperty("in_l2");
                var in_l3 = item.getProperty("in_l3");
                var in_sort_ = item.getProperty("in_l3");

                var is_m = in_l3.Contains("男");
                var is_w = in_l3.Contains("女");

                var e = GetAgeType(cfg, in_l3);
                if (e == AgeType.NN)
                {
                    continue;
                }

                var row = list.Find(x => x.Age == e);
                if (row == null)
                {
                    continue;
                }

                var node = default(TNode);
                var gndr = "";

                if (is_m)
                {
                    node = row.MNode;
                    gndr = "男";
                }
                if (is_w)
                {
                    node = row.WNode;
                    gndr = "女";
                }

                if (node == null)
                {
                    continue;
                }

                node.Val = id;
                node.Lbl = row.Sect.Replace("[g]", gndr);
                node.Ext = in_l3;
            }

            return list;
        }

        private Item GetTeamItems(TConfig cfg)
        {
            string sql = @"
                SELECT 
	                t1.*
                FROM
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                WHERE
	                t1.in_meeting = '{#meeting_id}'
	                AND t1.in_team_count > 1
	                AND t1.in_l1 = N'團體組'
                ORDER BY 
	                t1.in_sort_order
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            var items = cfg.inn.applySQL(sql);

            return items;
        }

        private AgeType GetAgeType(TConfig cfg, string in_l3)
        {
            var e = AgeType.NN;
            if (in_l3.Contains("特別組"))
            {
                if (in_l3.Contains("41"))
                {
                    e = AgeType.S4;
                }
                else if (in_l3.Contains("30-40"))
                {
                    e = AgeType.S3;
                }
            }
            else if (in_l3.Contains("社會"))
            {
                if (in_l3.Contains("甲"))
                {
                    e = AgeType.S2;
                }
                else if (in_l3.Contains("乙"))
                {
                    e = AgeType.S1;
                }
            }
            else if (in_l3.Contains("大專"))
            {
                if (in_l3.Contains("甲"))
                {
                    e = AgeType.C2;
                }
                else if (in_l3.Contains("乙"))
                {
                    e = AgeType.C1;
                }
            }
            else if (in_l3.Contains("高中"))
            {
                e = AgeType.H1;
            }
            else if (in_l3.Contains("國中"))
            {
                e = AgeType.J1;
            }
            else if (in_l3.Contains("國小"))
            {
                e = AgeType.E1;
            }

            return e;
        }

        private void AppendSoloMenu(TConfig cfg, List<TNode> nodes)
        {
            string sql = @"
                SELECT 
	                t1.*
                FROM
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                WHERE
	                t1.in_meeting = '{#meeting_id}'
	                AND t1.in_team_count > 1
	                AND t1.in_l1 NOT IN (N'格式組', N'團體組')
                ORDER BY 
	                t1.in_sort_order
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);
            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                AppendNodeSolo(cfg, nodes, item);
            }
        }

        //個人組
        private void AppendNodeSolo(TConfig cfg, List<TNode> nodes, Item item)
        {
            var id = item.getProperty("id");
            var in_l2 = item.getProperty("in_l2");
            var in_l3 = item.getProperty("in_l3");
            var in_weight = item.getProperty("in_weight");

            var key = in_l2.Replace("生", "子")
                .Replace("男", "")
                .Replace("女", "")
                .Replace("個-", "")
                .Replace("子", "")
                .Replace("組", "");

            var node = nodes.Find(x => x.Val == key);
            if (node == null)
            {
                node = new TNode
                {
                    Val = key,
                    Lbl = key,
                    MNodes = new List<TNode>(),
                    WNodes = new List<TNode>(),
                };
                nodes.Add(node);
            }

            var scd = new TNode
            {
                Val = id,
                Lbl = in_weight,
                Ext = in_l3,
            };

            if (in_l2.Contains("男"))
            {
                node.MNodes.Add(scd);
            }
            else if (in_l2.Contains("女"))
            {
                node.WNodes.Add(scd);
            }
        }

        //統計資訊
        private void AppendStatistics(TConfig cfg, Item itmReturn)
        {
            itmReturn.setProperty("content_unit", "CATEGORIES");
            itmReturn.setProperty("content_count", "--");

            itmReturn.setProperty("org_unit", "CONTRIES");
            itmReturn.setProperty("org_count", "--");

            itmReturn.setProperty("competitors_unit", "COMPETITORS");
            itmReturn.setProperty("competitors_count", "--");

            itmReturn.setProperty("men_unit", "MEN");
            itmReturn.setProperty("men_count", "--");

            itmReturn.setProperty("women_unit", "WOMEN");
            itmReturn.setProperty("women_count", "--");

            if (!cfg.need_more) return;

            string in_org_unit = cfg.itmMeeting.getProperty("in_org_unit", "").ToUpper();
            if (in_org_unit != "")
            {
                itmReturn.setProperty("org_unit", in_org_unit);
            }

            string sql = "";
            Item itmResult = null;

            sql = @"
                SELECT 
	                COUNT(*) AS 'cnt'
                FROM
	                IN_MEETING_PROGRAM WITH(NOLOCK)
                WHERE
	                in_meeting = '{#meeting_id}'
	                AND in_team_count > 1
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);
            itmResult = cfg.inn.applySQL(sql);
            if (!itmResult.isError() && itmResult.getResult() != "")
            {
                string content_count = itmResult.getProperty("cnt", "0");
                if (content_count != "0")
                {
                    itmReturn.setProperty("content_count", content_count);
                }
            }

            sql = @"
                SELECT 
	                DISTINCT t2.in_short_org
                FROM
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PTEAM t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                WHERE
	                t1.in_meeting = '{#meeting_id}'
	                AND t1.in_team_count > 1
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);
            itmResult = cfg.inn.applySQL(sql);
            if (!itmResult.isError() && itmResult.getResult() != "")
            {
                int org_count = itmResult.getItemCount();
                if (org_count >= 0)
                {
                    itmReturn.setProperty("org_count", org_count.ToString());
                }
            }

            sql = @"
                SELECT 
	                t1.in_l1
	                , t2.in_name
	                , t2.in_sno
	                , t2.in_gender
                FROM
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_USER t2 WITH(NOLOCK)
	                ON t2.source_id = t1.in_meeting
	                AND t2.in_l1 = t1.in_l1
	                AND t2.in_l2 = t1.in_l2
	                AND t2.in_l3 = t1.in_l3
                WHERE
	                t1.in_meeting = '{#meeting_id}'
	                AND t1.in_team_count > 1
                ORDER BY 
	                t2.in_gender
	                , t2.in_sno
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);
            itmResult = cfg.inn.applySQL(sql);
            if (!itmResult.isError() && itmResult.getResult() != "")
            {
                int cmpt_count = itmResult.getItemCount();
                if (cmpt_count >= 0)
                {
                    itmReturn.setProperty("competitors_count", cmpt_count.ToString());

                    var lstM = new List<Item>();
                    var lstW = new List<Item>();
                    for (int i = 0; i < cmpt_count; i++)
                    {
                        Item item = itmResult.getItemByIndex(i);
                        string in_gender = item.getProperty("in_gender", "");
                        switch (in_gender)
                        {
                            case "男": lstM.Add(item); break;
                            case "女": lstW.Add(item); break;
                        }
                    }
                    itmReturn.setProperty("men_count", lstM.Count.ToString());
                    itmReturn.setProperty("women_count", lstW.Count.ToString());
                }
            }
        }

        //賽事資訊
        private void AppendMeetingInfo(TConfig cfg, Item itmReturn)
        {
            if (cfg.meeting_id == "") return;

            cfg.itmMeeting = cfg.inn.applySQL("SELECT * FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            if (cfg.itmMeeting.isError() || cfg.itmMeeting.getResult() == "")
            {
                throw new Exception("查無 賽事資料");
            }

            cfg.in_call_url = cfg.itmMeeting.getProperty("in_call_url", "");
            cfg.in_call_title = cfg.itmMeeting.getProperty("in_call_title", "");
            cfg.in_call_note = cfg.itmMeeting.getProperty("in_call_note", "");
            cfg.in_video_photo = cfg.itmMeeting.getProperty("in_video_photo", "");

            cfg.in_score_url = cfg.itmMeeting.getProperty("in_score_url", "");
            cfg.in_score_title = cfg.itmMeeting.getProperty("in_score_title", "");

            string in_title = cfg.itmMeeting.getProperty("in_title", "");
            string in_propaganda_3 = cfg.itmMeeting.getProperty("in_propaganda_3", "");
            if (in_propaganda_3 == "") in_propaganda_3 = in_title;

            itmReturn.setProperty("in_title", in_title);
            itmReturn.setProperty("in_propaganda_3", in_propaganda_3);
            itmReturn.setProperty("banner_file", cfg.itmMeeting.getProperty("in_banner_photo", ""));
            itmReturn.setProperty("banner_file2", cfg.itmMeeting.getProperty("in_banner_photo2", ""));
            itmReturn.setProperty("in_video_url1", cfg.itmMeeting.getProperty("in_video_url1", ""));

            var in_video_list = cfg.itmMeeting.getProperty("in_video_list", "");
            if (in_video_list != "")
            {
                var video_list_code = "<a target='_blank' href='" + in_video_list + "'>合輯</a>";
                itmReturn.setProperty("video_list_code", video_list_code);
            }
            else
            {
                itmReturn.setProperty("video_list_code", " ");
            }

            string sql = "SELECT in_label FROM IN_MEETING_SHORTURL WITH(NOLOCK)"
                + " WHERE in_meeting = '" + cfg.meeting_id + "'"
                + " AND in_value = '" + cfg.func_id + "'";

            Item itmPage = cfg.inn.applySQL(sql);
            if (!itmPage.isError() && itmPage.getResult() != "")
            {
                itmReturn.setProperty("page_title", itmPage.getProperty("in_label", ""));
            }
        }

        //樣式
        private void AppendBoxCss(TConfig cfg, Item itmReturn)
        {
            string sql = "";

            string short_prop = "in_mvc_shorturl";
            string qrcode_prop = "in_mvc_qrcode";

            if (cfg.is_api == "1")
            {
                short_prop = "in_api_shorturl";
                qrcode_prop = "in_api_qrcode";
            }

            if (cfg.meeting_id == "")
            {
                sql = "SELECT t1.id, t1.in_label, t1." + short_prop + " AS 'short_url', t1." + qrcode_prop + " AS 'qrcode_url'"
                    + " FROM IN_MEETING_SHORTURL t1 WITH(NOLOCK)"
                    + " INNER JOIN IN_MEETING_SHORTURL t2 WITH(NOLOCK) ON t2.id = t1.in_parent"
                    + " WHERE t2.in_value = '" + cfg.func_id + "'"
                    + " AND t1.in_is_closed = 0"
                    + " ORDER BY t1.in_sort_order";
            }
            else
            {
                sql = "SELECT t1.id, t1.in_label, t1." + short_prop + " AS 'short_url', t1." + qrcode_prop + " AS 'qrcode_url'"
                    + " FROM IN_MEETING_SHORTURL t1 WITH(NOLOCK)"
                    + " INNER JOIN IN_MEETING_SHORTURL t2 WITH(NOLOCK) ON t2.id = t1.in_parent"
                    + " WHERE t1.in_meeting = '" + cfg.meeting_id + "'"
                    + " AND t2.in_value = '" + cfg.func_id + "'"
                    + " AND t1.in_is_closed = 0"
                    + " ORDER BY t1.in_sort_order";
            }

            Item items = cfg.inn.applySQL(sql);

            var list = ResetQrCodeList(cfg, items);
            int count = list.Count;

            int code = count % 15;
            string col_md = "col-md-3";

            string style = "";
            string style1 = ".flex-container { display: flex; justify-content: left; align-items: flex-start; flex-wrap: wrap; }";
            string style2 = ".flex-container { display: flex; justify-content: center; align-items: center; flex-wrap: wrap; }";

            switch (code)
            {
                case 0:
                    col_md = "col-md-3";
                    style = style1;
                    break;
                case 1:
                    col_md = "col-md-4";
                    style = style2;
                    break;
                case 2:
                    col_md = "col-md-4";
                    style = style2;
                    break;
                case 3:
                    col_md = "col-md-4";
                    style = style1;
                    break;
                case 4:
                    col_md = "col-md-3";
                    style = style1;
                    break;
                case 5:
                    col_md = "col-md-4";
                    style = style1;
                    break;
                case 6:
                    col_md = "col-md-4";
                    style = style1;
                    break;
                case 7:
                    col_md = "col-md-3";
                    style = style1;
                    break;
                case 8:
                    col_md = "col-md-3";
                    style = style1;
                    break;
                case 9:
                    col_md = "col-md-3";
                    style = style1;
                    break;
                case 10:
                    col_md = "col-md-3";
                    style = style1;
                    break;
                case 11:
                    col_md = "col-md-3";
                    style = style1;
                    break;
                case 12:
                    col_md = "col-md-3";
                    style = style1;
                    break;
                case 13:
                    col_md = "col-md-3";
                    style = style1;
                    break;
                case 14:
                    col_md = "col-md-3";
                    style = style1;
                    break;
                default:
                    break;
            }

            for (int i = 0; i < count; i++)
            {
                Item item = list[i];//items.getItemByIndex(i);
                string qrcode_url = item.getProperty("qrcode_url", "");

                item.setType("inn_url");
                item.setProperty("no", (i + 1).ToString());
                item.setProperty("col_md", col_md);
                item.setProperty("iplm_url", cfg.iplm_url);
                item.setProperty("qrcode_url", qrcode_url);
                //item.setProperty("qrcode_url", qrcode_url + "?t="+DateTime.Now.ToString("HHmmss"));
                itmReturn.addRelationship(item);
            }

            itmReturn.setProperty("flex_container", style);
        }

        private List<Item> ResetQrCodeList(TConfig cfg, Item items)
        {
            var list = new List<Item>();

            if (cfg.func_id == "function" && cfg.in_call_url != "")
            {
                Item itmCall = cfg.inn.newItem();
                itmCall.setProperty("short_url", cfg.in_call_url);
                itmCall.setProperty("in_label", cfg.in_call_title);
                list.Add(itmCall);
            }

            int count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                list.Add(item);
            }

            //成績查詢
            if (cfg.func_id == "function" && cfg.in_score_url != "")
            {
                Item itmScore = cfg.inn.newItem();
                itmScore.setProperty("short_url", cfg.in_score_url);
                itmScore.setProperty("in_label", cfg.in_score_title);
                list.Add(itmScore);
            }

            //花絮集錦
            if (cfg.func_id == "function" && cfg.in_video_photo != "")
            {
                Item itmCall = cfg.inn.newItem();
                itmCall.setProperty("short_url", cfg.in_video_photo);
                itmCall.setProperty("in_label", "花絮連結");
                list.Add(itmCall);
            }

            // //附加檔案連結
            // AppendFileQrCode(cfg, list);

            return list;
        }

        //附加檔案連結
        private void AppendFileQrCode(TConfig cfg, List<Item> list)
        {
            string sql = @"
                SELECT 
                	t2.id
                	, t2.filename
                	, t2.mimetype
                FROM 
                	IN_MEETING_FILE t1 WITH(NOLOCK) 
                INNER JOIN
                	[FILE] t2 WITH(NOLOCK)
                	ON t2.id = t1.related_id
                WHERE
                    t1.source_id = '{#meeting_id}'
                ORDER BY 
                	t1.created_on
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);
            Item items = cfg.inn.applySQL(sql);
            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string fileid = item.getProperty("id", "");
                string filename = item.getProperty("filename", "");
                string mimetype = item.getProperty("mimetype", "");

                string url = "../DownloadMeetingFile.aspx?fileid=" + fileid + "&t=20220918";
                item.setProperty("short_url", url);
                item.setProperty("in_label", filename);

                list.Add(item);
            }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public Innosoft.InnovatorHelper InnH { get; set; }

            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }

            public string meeting_id { get; set; }
            public string func_id { get; set; }
            public string is_api { get; set; }
            public string iplm_url { get; set; }
            public bool need_more { get; set; }

            public Item itmMeeting { get; set; }

            public string in_call_url { get; set; }
            public string in_call_title { get; set; }
            public string in_call_note { get; set; }
            public string in_video_photo { get; set; }

            public string in_score_url { get; set; }
            public string in_score_title { get; set; }
        }

        private class TNode
        {
            public string Val { get; set; }
            public string Lbl { get; set; }
            public string Ext { get; set; }
            public string Cnt { get; set; }
            public List<TNode> MNodes { get; set; }
            public List<TNode> WNodes { get; set; }
        }

        private enum AgeType
        {
            NN = 0,
            S4 = 100,
            S3 = 200,
            S2 = 300,
            S1 = 400,
            C2 = 500,
            C1 = 600,
            H1 = 700,
            J1 = 800,
            E1 = 910,
            E2 = 920,
            E3 = 930,
        }

        private class TTeamNode
        {
            public AgeType Age { get; set; }
            public string Sect { get; set; }
            public TNode MNode { get; set; }
            public TNode WNode { get; set; }
        }
    }
}