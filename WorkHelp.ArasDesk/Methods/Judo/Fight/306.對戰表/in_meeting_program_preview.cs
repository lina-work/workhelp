﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.PLMCTA.Common
{
    public class in_meeting_program_preview : Item
    {
        public in_meeting_program_preview(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
    目的: 賽程圖預覽
    日期: 
        2022-12-09: 格式組調整 (lina)
        2020-09-30: 創建 (lina)
*/

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]in_meeting_program_preview";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, this.dom.InnerXml);

            string meeting_id = itmR.getProperty("meeting_id", "");
            string mode = itmR.getProperty("mode", "");
            string in_exchange = itmR.getProperty("in_exchange", "");

            if (meeting_id == "")
            {
                itmR.setProperty("error_message", "賽事 id 不得為空白");
                return itmR;
            }

            //檢查頁面權限
            Item itmPermit = inn.applyMethod("In_CheckIdentity", "<method>" + strMethodName + "</method><code>" + "ALL" + "</code>");
            bool isMeetingAdmin = itmPermit.getProperty("isMeetingAdmin", "") == "1";
            bool isMeetingLeader = itmPermit.getProperty("isMeetingLeader", "") == "1";
            bool can_view = isMeetingAdmin || isMeetingLeader;
            if (!can_view)
            {
                itmR.setProperty("error_message", "您無權限瀏覽此頁面");
                return itmR;
            }

            Query(CCO, strMethodName, inn, itmR);

            if (in_exchange == "1")
            {
                AppendMoreCtrl(itmR);
            }

            return itmR;
        }

        private void AppendMoreCtrl(Item itmReturn)
        {
            string btn_exchange = "<button class='btn btn-sm btn-danger' onclick='ExchangeFootPlayer()'> <i class='fa fa-exchange'></i> </button>";

            itmReturn.setProperty("inn_more_ctrl", btn_exchange);
        }

        private void AppendSiteOptions(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string meeting_id, Item itmReturn)
        {
            string sql = "SELECT * FROM IN_MEETING_SITE WITH(NOLOCK) WHERE in_meeting = '" + meeting_id + "' ORDER BY in_code";
            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item items = inn.applySQL(sql);

            int count = items.getItemCount();

            Item itmEmpty = inn.newItem();
            itmEmpty.setType("inn_site");
            itmEmpty.setProperty("value", "");
            itmEmpty.setProperty("text", "設定場地");
            itmEmpty.setProperty("selected", "selected");
            itmReturn.addRelationship(itmEmpty);

            for (int i = 0; i < count; i++)
            {
                Item source = items.getItemByIndex(i);

                Item item = inn.newItem();
                item.setType("inn_site");
                item.setProperty("value", source.getProperty("id", ""));
                item.setProperty("text", source.getProperty("in_name", ""));
                item.setProperty("selected", "");
                itmReturn.addRelationship(item);
            }
        }

        private string GetProgramId(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            string meeting_id = itmReturn.getProperty("meeting_id", "");
            string program_id = itmReturn.getProperty("program_id", "");
            string in_l1 = itmReturn.getProperty("l1", "");
            string in_l2 = itmReturn.getProperty("l2", "");
            string in_l3 = itmReturn.getProperty("l3", "");

            string result = program_id;
            string sql = "";
            Item itmTemp = null;

            if (program_id == "")
            {
                if (in_l1 != "" && in_l2 != "" && in_l3 != "")
                {
                    sql = "SELECT TOP 1 id FROM IN_MEETING_PROGRAM WITH(NOLOCK)"
                        + " WHERE in_meeting = '" + meeting_id + "' AND in_l1 = N'" + in_l1 + "' AND in_l2 = N'" + in_l2 + "' AND in_l3 = N'" + in_l3 + "'";

                    itmTemp = inn.applySQL(sql);

                    if (!itmTemp.isError() && itmTemp.getResult() != "")
                    {
                        result = itmTemp.getProperty("id", "");
                        itmReturn.setProperty("program_id", result);
                    }
                }
                else
                {
                    sql = "SELECT TOP 1 * FROM IN_MEETING_PROGRAM WITH(NOLOCK)"
                        + " WHERE in_meeting = '" + meeting_id + "' ORDER BY in_sort_order";

                    itmTemp = inn.applySQL(sql);

                    if (!itmTemp.isError() && itmTemp.getResult() != "")
                    {
                        result = itmTemp.getProperty("id", "");
                        itmReturn.setProperty("program_id", result);
                    }
                }
            }

            return result;
        }

        //查詢
        private void Query(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            string meeting_id = itmReturn.getProperty("meeting_id", "");
            string program_id = GetProgramId(CCO, strMethodName, inn, itmReturn);

            Item itmMeeting = GetMeeting(CCO, strMethodName, inn, meeting_id);
            if (itmMeeting.isError())
            {
                itmReturn.setProperty("error_message", "取得賽事資料發生錯誤");
                return;
            }

            //附加賽事資訊
            AppendMeeting(itmMeeting, itmReturn);

            //附加選單資訊
            Item itmJson = inn.newItem("In_Meeting");
            itmJson.setProperty("meeting_id", meeting_id);
            itmJson.setProperty("need_id", "1");
            itmJson = itmJson.apply("in_meeting_program_options");
            itmReturn.setProperty("in_level_json", itmJson.getProperty("json", ""));
            itmReturn.setProperty("in_level_count", itmJson.getProperty("total", ""));

            //附加選單資訊
            Item itmJson2 = inn.newItem("In_Meeting");
            itmJson2.setProperty("meeting_id", meeting_id);
            itmJson2.setProperty("need_id", "1");
            itmJson2 = itmJson2.apply("in_meeting_day_options");
            itmReturn.setProperty("in_day_json", itmJson2.getProperty("json", ""));

            if (program_id == "")
            {
                //itmReturn.setProperty("error_message", "組別 id 不得為空白");

                itmReturn.setProperty("hide_repechage", "item_show_0");
                itmReturn.setProperty("hide_ranking", "item_show_0");
                return;
            }

            Item itmProgram = GetProgram(CCO, strMethodName, inn, program_id);
            if (itmProgram.isError() || itmProgram.getResult() == "")
            {
                itmReturn.setProperty("error_message", "取得組別資料發生錯誤");
                return;
            }

            //附加組別資訊
            AppendProgram(itmProgram, itmReturn);

            //附加樣式資訊
            AppendCss(CCO, strMethodName, inn, itmMeeting, itmProgram, itmReturn);

            // //附加場次選單
            // AppendEventMenu(CCO, strMethodName, inn, itmProgram, itmReturn);

            //附加對戰表資訊
            AppendMatchList(CCO, strMethodName, inn, itmMeeting, itmProgram, itmReturn);

            //附加比賽名次
            AppendRanks(CCO, strMethodName, inn, itmProgram, itmReturn);

            //附加場地選單
            AppendSiteOptions(CCO, strMethodName, inn, meeting_id, itmReturn);
        }

        #region 附加場次選單

        //附加場次選單
        private void AppendEventMenu(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmProgram, Item itmReturn)
        {
            string program_id = itmProgram.getProperty("id", "");

            Dictionary<string, List<Item>> map = new Dictionary<string, List<Item>>();
            AddItems(map, GetEventsMenuItems(CCO, strMethodName, inn, program_id, query_childre: false));
            AddItems(map, GetEventsMenuItems(CCO, strMethodName, inn, program_id, query_childre: true));

            List<TNode> nodes = new List<TNode>();

            foreach (var kv in map)
            {
                var key = kv.Key;
                var list = kv.Value;

                TNode n1 = new TNode
                {
                    Val = key,
                    Nodes = new List<TNode>(),
                };

                int count = list.Count;
                for (int i = 0; i < count; i++)
                {
                    Item itemLast = i == 0 ? inn.newItem() : list[i - 1];
                    Item item = list[i];
                    Item itemNext = i == (count - 1) ? inn.newItem() : list[i + 1];

                    TNode evt = new TNode
                    {
                        Lbl = item.getProperty("in_tree_name", ""),
                        Ext = item.getProperty("in_tree_no", ""),

                        Val = item.getProperty("in_tree_id", ""),
                        Last = itemLast.getProperty("in_tree_id", ""),
                        Next = itemNext.getProperty("in_tree_id", ""),
                    };
                    n1.Nodes.Add(evt);
                }

                nodes.Add(n1);
            }

            itmReturn.setProperty("in_event_json", Newtonsoft.Json.JsonConvert.SerializeObject(nodes));
        }

        private void AddItems(Dictionary<string, List<Item>> map, Item items)
        {
            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string source_id = item.getProperty("source_id", "");

                List<Item> list = null;
                if (map.ContainsKey(source_id))
                {
                    list = map[source_id];
                }
                else
                {
                    list = new List<Item>();
                    map.Add(source_id, list);
                }
                list.Add(item);
            }
        }

        private Item GetEventsMenuItems(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string program_id, bool query_childre = false)
        {
            string program_condition = query_childre
                ? "source_id IN (SELECT id FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE in_program = '" + program_id + "')"
                : "source_id = '" + program_id + "'";

            string sql = @"
        SELECT
	        source_id
	        , in_tree_name
	        , in_tree_id
	        , in_tree_no
        FROM 
	        IN_MEETING_PEVENT WITH(NOLOCK)
        WHERE 
	        {#program_condition}
	        AND ISNULL(in_tree_no, '') NOT IN ('', '0')
	        AND ISNULL(in_tree_rank, '') NOT IN (N'rank78')
	        AND ISNULL(in_win_status, '') NOT IN (N'bypass')
        ORDER BY
	        in_tree_sort
	        , in_tree_no
        ";

            sql = sql.Replace("{#program_condition}", program_condition);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return inn.applySQL(sql);
        }

        private class TNode
        {
            public string Val { get; set; }
            public string Lbl { get; set; }
            public string Ext { get; set; }
            public string Last { get; set; }
            public string Next { get; set; }
            public List<TNode> Nodes { get; set; }
        }


        #endregion 附加場次選單

        //附加對戰表資訊
        private void AppendMatchList(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmMeeting, Item itmProgram, Item itmReturn)
        {
            string mt_battle_repechage = itmMeeting.getProperty("in_battle_repechage", "").ToLower();

            int team_count = GetIntVal(itmProgram.getProperty("in_team_count", ""));

            itmProgram.setType("In_Meeting_Program");
            itmProgram.setProperty("meeting_id", itmProgram.getProperty("in_meeting", ""));
            itmProgram.setProperty("program_id", itmProgram.getProperty("id", ""));
            itmProgram.setProperty("mode", itmReturn.getProperty("mode", ""));
            itmProgram.setProperty("eno", itmReturn.getProperty("eno", ""));
            itmProgram.setProperty("pdf", itmReturn.getProperty("pdf", ""));
            itmProgram.setProperty("page_language", itmReturn.getProperty("page_language", ""));

            string in_battle_type = itmProgram.getProperty("in_battle_type", "");
            string in_l1 = itmProgram.getProperty("in_l1", "");
            bool is_challenge = in_battle_type == "Challenge";

            switch (in_battle_type)
            {
                case "Kata": //格式組
                    Item itmResult1 = itmProgram.apply("in_meeting_program_preview_kata");
                    itmReturn.setProperty("bracket_title", "格式組");
                    itmReturn.setProperty("hide_repechage", "item_show_0");
                    itmReturn.setProperty("hide_rank56", "item_show_0");
                    itmReturn.setProperty("hide_rank78", "item_show_0");
                    itmReturn.setProperty("bracket_table", itmResult1.getProperty("bracket_table", ""));
                    itmReturn.setProperty("hide_ranking", "item_show_0");
                    break;

                case "TopTwo": //單淘
                    Item itmResult2 = itmProgram.apply("in_meeting_program_preview_main");

                    itmReturn.setProperty("bracket_title", "Match List 對戰表");
                    itmReturn.setProperty("bracket_table", itmResult2.getProperty("bracket_table", ""));

                    if (team_count > 4)
                    {
                        Item itmResult2R = itmProgram.apply("in_meeting_program_preview_repechage");

                        itmReturn.setProperty("repechage_title", "三四名對戰");
                        itmReturn.setProperty("repechage_hr", " <hr />");
                        itmReturn.setProperty("repechage_table", itmResult2R.getProperty("rank34_table", ""));
                    }
                    else
                    {
                        itmReturn.setProperty("hide_repechage", "item_show_0");
                    }
                    break;

                case "JudoTopFour": //四柱復活賽
                case "Challenge":   //挑戰賽
                    Item itmResult3 = itmProgram.apply("in_meeting_program_preview_main");

                    itmReturn.setProperty("bracket_title", "Match List 對戰表");
                    itmReturn.setProperty("bracket_table", itmResult3.getProperty("bracket_table", ""));

                    if (team_count <= 2)
                    {
                        itmReturn.setProperty("hide_repechage", "item_show_0");
                    }
                    else if (team_count < 4)
                    {
                        itmReturn.setProperty("hide_repechage", "item_show_0");
                    }
                    else if (team_count == 4)
                    {
                        Item itmResult3R = itmProgram.apply("in_meeting_program_preview_repechage");

                        itmReturn.setProperty("repechage_title", "三四名對戰");
                        itmReturn.setProperty("repechage_hr", " <hr />");
                        itmReturn.setProperty("repechage_table", itmResult3R.getProperty("repechage_table", ""));
                    }
                    else
                    {
                        string result3r_method = mt_battle_repechage == "quarterfinals" //前八強才可進復活賽
                            ? "in_meeting_program_preview_repechage2"
                            : "in_meeting_program_preview_repechage";

                        Item itmResult3R = itmProgram.apply(result3r_method);

                        itmReturn.setProperty("repechage_title", "Repechage 復活表");
                        itmReturn.setProperty("repechage_hr", " <hr />");
                        itmReturn.setProperty("repechage_table", itmResult3R.getProperty("repechage_table", ""));
                    }
                    if (is_challenge)
                    {
                        Item itmResult3C = itmProgram.apply("in_meeting_program_preview_challenge");
                        itmReturn.setProperty("challenge_section", itmResult3C.getProperty("challenge_section", ""));
                    }
                    break;

                case "SingleRoundRobin": //單循環賽
                case "DoubleRoundRobin": //雙循環賽

                    if (in_battle_type == "SingleRoundRobin")
                    {
                        itmReturn.setProperty("bracket_title", "Single Round-robin Competition 單循環賽");
                    }
                    else if (in_battle_type == "DoubleRoundRobin")
                    {
                        itmReturn.setProperty("bracket_title", "Double Round-robin Competition 雙循環賽");
                    }

                    Item itmResult4 = null;

                    if (team_count == 2)
                    {
                        itmResult4 = itmProgram.apply("in_meeting_program_preview_robin2");

                        // if (in_l1.Contains("團體"))
                        // {
                        //     itmResult4 = itmProgram.apply("in_meeting_program_preview_main");
                        //     itmReturn.setProperty("bracket_title", "Match List 對戰表");
                        // }
                        // else
                        // {
                        //     itmResult4 = itmProgram.apply("in_meeting_program_preview_robin2");
                        // }
                    }
                    else
                    {
                        itmResult4 = itmProgram.apply("in_meeting_program_preview_robin");
                    }

                    itmReturn.setProperty("hide_repechage", "item_show_0");
                    itmReturn.setProperty("hide_rank56", "item_show_0");
                    itmReturn.setProperty("hide_rank78", "item_show_0");
                    itmReturn.setProperty("bracket_table", itmResult4.getProperty("bracket_table", ""));
                    break;

                case "GroupSRoundRobin": //分組-單循環賽
                case "GroupDRoundRobin": //分組-雙循環賽
                case "GroupTopTwo":      //分組-單淘汰賽
                case "GroupJudoTopFour": //分組-四柱復活賽
                    if (in_battle_type == "GroupSRoundRobin")
                    {
                        itmReturn.setProperty("bracket_title", "分組-單循環賽");
                    }
                    else if (in_battle_type == "GroupDRoundRobin")
                    {
                        itmReturn.setProperty("bracket_title", "分組-雙循環賽");
                    }
                    else if (in_battle_type == "GroupTopTwo")
                    {
                        itmReturn.setProperty("bracket_title", "分組-單淘汰賽");
                    }
                    else if (in_battle_type == "GroupJudoTopFour")
                    {
                        itmReturn.setProperty("bracket_title", "分組-四柱復活賽");
                    }

                    itmReturn.setProperty("hide_repechage", "item_show_0");
                    itmReturn.setProperty("hide_rank56", "item_show_0");
                    itmReturn.setProperty("hide_rank78", "item_show_0");
                    itmReturn.setProperty("container_class", "container-table");
                    AppendChildrenPrograms(CCO, strMethodName, inn, itmProgram, itmReturn);
                    break;

                default:
                    break;
            }

            if (in_battle_type != "Kata" && in_battle_type != "Cross")
            {
                Item itmResultOther = itmProgram.apply("in_meeting_program_preview_other");
                itmReturn.setProperty("other_box", itmResultOther.getProperty("other_box", ""));
            }
        }

        //附加 Children 組別
        private void AppendChildrenPrograms(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmProgram, Item itmReturn)
        {
            string meeting_id = itmProgram.getProperty("in_meeting", "");
            string program_id = itmProgram.getProperty("id", "");
            string program_name = itmProgram.getProperty("in_name", "");
            string program_name2 = itmProgram.getProperty("in_name2", "");
            string program_name3 = itmProgram.getProperty("in_name3", "");
            string program_display = itmProgram.getProperty("in_display", "");

            Item itmChildrenPrograms = GetChildrenPrograms(CCO, strMethodName, inn, program_id);

            int count = itmChildrenPrograms.getItemCount();

            if (count <= 0)
            {
                return;
            }

            StringBuilder builder = new StringBuilder();

            for (int i = 0; i < count; i++)
            {
                Item itmChildrenProgram = itmChildrenPrograms.getItemByIndex(i);
                string in_group_display = itmChildrenProgram.getProperty("in_group_display", "");
                string in_battle_type = itmChildrenProgram.getProperty("in_battle_type", "");
                string in_group_tag = itmChildrenProgram.getProperty("in_group_tag", "");

                itmChildrenProgram.setType("In_Meeting_Program");
                itmChildrenProgram.setProperty("meeting_id", meeting_id);
                itmChildrenProgram.setProperty("program_id", itmChildrenProgram.getProperty("id", ""));
                itmChildrenProgram.setProperty("mode", itmReturn.getProperty("mode", ""));
                itmChildrenProgram.setProperty("eno", itmReturn.getProperty("eno", ""));

                string export_match_btn = "";
                if (in_group_tag == "tiebreaker")
                {
                    export_match_btn = "<";
                }
                switch (in_battle_type)
                {
                    case "SingleRoundRobin":
                    case "DoubleRoundRobin":
                        Item itmResult = itmChildrenProgram.apply("in_meeting_program_preview_robin");

                        builder.AppendLine("<div class='box-body row container-row' >");
                        builder.AppendLine("  <div class='col-lg-12 col-md-9 col-sm-12 col-xs-12'>");
                        builder.AppendLine("    <div class='box'>");
                        builder.AppendLine("      <div class='box-header with-border'>");
                        builder.AppendLine("        <h3 class='box-title'>");
                        builder.AppendLine(program_name3 + " - <b>" + in_group_display + "</b>");
                        builder.AppendLine("        </h3>");
                        builder.AppendLine("      </div>");

                        builder.AppendLine("      <div class='box-body'>");
                        builder.AppendLine(itmResult.getProperty("bracket_table", ""));
                        builder.AppendLine("      </div>");
                        builder.AppendLine("    </div>");
                        builder.AppendLine("  </div>");
                        builder.AppendLine("</div>");
                        break;

                    case "TopTwo":
                        Item itmResult2 = itmChildrenProgram.apply("in_meeting_program_preview_main");

                        builder.AppendLine("<div class='box-body row container-row' >");
                        builder.AppendLine("  <div class='col-lg-12 col-md-9 col-sm-12 col-xs-12'>");
                        builder.AppendLine("    <div class='box'>");
                        builder.AppendLine("      <div class='box-header with-border'>");
                        builder.AppendLine("        <h3 class='box-title'>");
                        builder.AppendLine(program_name3 + " - <b>" + in_group_display + BtnExportMatch(itmChildrenProgram));
                        builder.AppendLine("        </h3>");
                        builder.AppendLine("      </div>");

                        builder.AppendLine("      <div class='box-body'>");
                        builder.AppendLine(itmResult2.getProperty("bracket_table", ""));
                        builder.AppendLine("      </div>");
                        builder.AppendLine("    </div>");
                        builder.AppendLine("  </div>");
                        builder.AppendLine("</div>");
                        break;

                    default:
                        break;
                }
            }

            itmReturn.setProperty("bracket_table", builder.ToString());

        }

        private string BtnExportMatch(Item itmProgram)
        {
            string btn = "&emsp;<button class='btn btn-success' onclick=\"ExportMatchList('{#meeting_id}', '{#program_id}', '{#in_l1}', '{#export_type}')\">匯出對戰表</button>";

            btn = btn.Replace("{#meeting_id}", itmProgram.getProperty("in_meeting", ""))
                .Replace("{#program_id}", itmProgram.getProperty("id", ""))
                .Replace("{#in_l1}", "")
                .Replace("{#export_type}", "XLSX");

            return btn;
        }

        //附加賽事資訊
        private void AppendMeeting(Item itmMeeting, Item itmReturn)
        {
            itmReturn.setProperty("in_title", itmMeeting.getProperty("in_title", ""));
            itmReturn.setProperty("in_battle_type", itmMeeting.getProperty("in_battle_type", ""));
            itmReturn.setProperty("in_surface_code", itmMeeting.getProperty("in_surface_code", ""));
            itmReturn.setProperty("banner_file", itmMeeting.getProperty("in_banner_photo", ""));
            itmReturn.setProperty("banner_file2", itmMeeting.getProperty("in_banner_photo2", ""));
            itmReturn.setProperty("in_language", itmMeeting.getProperty("in_language", ""));
        }

        //附加組別資訊
        private void AppendProgram(Item itmProgram, Item itmReturn)
        {
            itmReturn.setProperty("in_name", itmProgram.getProperty("in_name", ""));
            itmReturn.setProperty("in_name2", itmProgram.getProperty("in_name2", ""));
            itmReturn.setProperty("in_name3", itmProgram.getProperty("in_name3", ""));
            itmReturn.setProperty("in_display", itmProgram.getProperty("in_display", ""));
            itmReturn.setProperty("in_team_count", itmProgram.getProperty("in_team_count", ""));
            itmReturn.setProperty("in_fight_day", itmProgram.getProperty("in_fight_day", ""));
            itmReturn.setProperty("pg_battle_type", itmProgram.getProperty("in_battle_type", ""));

            string in_site_mat = itmProgram.getProperty("in_site_mat", "");
            string in_site_code = in_site_mat.Contains("-")
                ? in_site_mat.Split('-').First().Replace("MAT ", "")
                : "";

            itmReturn.setProperty("in_site_mat", in_site_mat);
            itmReturn.setProperty("in_site_code", in_site_code);

            string in_sign_time = itmProgram.getProperty("in_sign_time", "");
            itmReturn.setProperty("sign_time", GetDateTimeVal(in_sign_time, "yyyy-MM-dd"));

            itmReturn.setProperty("in_l1", itmProgram.getProperty("in_l1", ""));
            itmReturn.setProperty("in_l2", itmProgram.getProperty("in_l2", ""));
            itmReturn.setProperty("in_l3", itmProgram.getProperty("in_l3", ""));

            itmReturn.setProperty("program_file2", FileDownLink(itmProgram, "in_file2", "fa-list", "賽程表"));
            itmReturn.setProperty("program_file3", FileDownLink(itmProgram, "in_file3", "fa-list-ol", "比賽結果"));

        }

        private void AppendCss(Aras.Server.Core.CallContext CCO
            , string strMethodName
            , Innovator inn
            , Item itmMeeting
            , Item itmProgram
            , Item itmReturn)
        {
            string meeting_id = itmReturn.getProperty("meeting_id", "");

            //附加賽會參數
            Item itmMtVrl = inn.applyMethod("In_Meeting_Variable"
                    , "<meeting_id>" + meeting_id + "</meeting_id>"
                    + "<name>line_color</name>"
                    + "<scene>get</scene>");

            string line_color = itmMtVrl.getProperty("inn_result", "");

            string css_name = "tournament_sign_red";

            if (line_color == "multicolor")
            {
                css_name = "tournament_sign_" + itmProgram.getProperty("in_round_code", "");
            }

            itmReturn.setProperty("tournament_sign", css_name);
        }

        private string FileDownLink(Item item, string prop, string icon, string message)
        {
            string file_id = item.getProperty(prop, "");
            string file_ext = item.getProperty(prop + "_ext", "");

            string file_mode = file_ext.Contains("pdf")
                ? "file"
                : "";

            if (file_id == "") return "";

            return "<a onclick='openPhotoSwipe3(this)', data-fid='" + file_id + "' data-fmode='" + file_mode + "'  >"
                + "<i class='fa " + icon + "' ></i>"
                + "<label class='box-title'>&nbsp;" + message + "</label>"
                + "</a>";
        }

        //附加比賽名次
        private void AppendRanks(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmProgram, Item itmReturn)
        {
            string meeting_id = itmProgram.getProperty("in_meeting", "");
            string program_id = itmProgram.getProperty("id", "");

            string in_rank_type = itmProgram.getProperty("in_rank_type", "");
            string in_team_count = itmProgram.getProperty("in_team_count", "0");

            Item itmMaxRank = inn.applyMethod("in_meeting_program_rank", "<in_team_count>" + in_team_count + "</in_team_count>");
            string max_rank = itmMaxRank.getProperty("max_rank", "0");

            Item items = GetRanks(CCO, strMethodName, inn, meeting_id, program_id, max_rank);
            int count = items.getItemCount();

            string[] rank_arr = { "", "一", "二", "三", "四", "五", "六", "七", "八", "九" };

            switch (in_rank_type)
            {
                case "SameRank":
                    rank_arr = new string[] { "", "一", "二", "三", "三", "五", "五", "七", "七", "" };
                    break;

                case "Challenge":
                    rank_arr = new string[] { "", "一", "二", "三", "四", "五", "五", "七", "七", "" };
                    break;
            }

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_name = item.getProperty("in_name", "");
                string in_weight_message = item.getProperty("in_weight_message", "");
                if (in_weight_message != "") in_name += "<label style='color: red'>" + in_weight_message + "</label>";
                item.setProperty("in_name", in_name);

                string in_stuff_a1 = item.getProperty("in_stuff_a1", "");
                string in_show_org = item.getProperty("in_show_org", "");
                if (in_show_org != "")
                {
                    item.setProperty("in_stuff_a1", in_show_org + " / " + in_stuff_a1);
                }

                string in_final_rank = item.getProperty("in_final_rank", "");
                string rank_display = GetRankDisplay(in_final_rank, rank_arr);

                string mtName = item.getProperty("program_display", "");
                string[] nameSplit = mtName.Split('-');
                mtName = mtName.Replace(nameSplit[0] + "-", "");
                item.setProperty("program_display", mtName);

                if (in_final_rank == "1")
                {
                    string icon = "<i class='fa fa-trophy' style='color: gold;'></i>";
                    rank_display = icon + rank_display + icon;
                }

                item.setType("inn_rank");
                item.setProperty("rank_display", rank_display);
                itmReturn.addRelationship(item);
            }
        }

        //取得賽事資訊
        private Item GetMeeting(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string meeting_id)
        {
            var sql = "SELECT in_title, in_battle_type, in_battle_repechage, in_surface_code, in_banner_photo, in_banner_photo2, in_language FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + meeting_id + "'";
            return inn.applySQL(sql);
            // string aml = @"
            //     <AML>
            //         <Item type='In_Meeting' action='get' id='{#meeting_id}' select='{#cols}'>
            //         </Item>
            //     </AML>
            // ".Replace("{#meeting_id}", meeting_id)
            // .Replace("{#cols}", string.Join(", ", cols));

            // return inn.applyAML(aml);
        }

        //取得組別資訊
        private Item GetProgram(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string program_id)
        {
            string sql = @"
                SELECT	
                	t1.*
                	, ISNULL(t11.mimetype, '') AS 'in_file1_ext'
                	, ISNULL(t12.mimetype, '') AS 'in_file2_ext'
                	, ISNULL(t13.mimetype, '') AS 'in_file3_ext'
                FROM 
                	IN_MEETING_PROGRAM t1 WITH(NOLOCK) 
                LEFT OUTER JOIN
                	[File] t11 WITH(NOLOCK)
                	ON t11.id = t1.in_file1
                LEFT OUTER JOIN
                	[File] t12 WITH(NOLOCK)
                	ON t12.id = t1.in_file2
                LEFT OUTER JOIN
                	[File] t13 WITH(NOLOCK)
                	ON t13.id = t1.in_file3
                WHERE 
                	t1.id = '{#program_id}'
            ";

            sql = sql.Replace("{#program_id}", program_id);

            return inn.applySQL(sql);
        }

        //取得賽事組別名次
        private Item GetRanks(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string meeting_id, string program_id, string max_rank)
        {
            string sql = @"
                SELECT
                    t1.in_current_org
                    , t1.in_name
                    , t1.in_sno
                    , t1.in_final_rank
                    , t1.in_show_rank
                    , t1.in_sign_no
                    , t1.in_section_no
                    , t1.in_weight_message
                    , t1.map_short_org
                    , t1.map_org_name
                    , t1.in_show_org
                    , t2.in_name     AS 'program_name'
                    , t2.in_name2    AS 'program_name2'
                    , t2.in_name3    AS 'program_name3'
                    , t2.in_display  AS 'program_display'
                    , t1.in_stuff_a1
                FROM 
                    VU_MEETING_PTEAM t1 WITH(NOLOCK)
                INNER JOIN
                    IN_MEETING_PROGRAM t2 WITH(NOLOCK)
                    ON t2.id = t1.source_id
                LEFT OUTER JOIN
                    IN_MEETING_PEVENT t3 WITH(NOLOCK)
                    ON t3.id = t1.in_event
                WHERE
                    t1.in_meeting = '{#meeting_id}'
                    AND t1.source_id = '{#program_id}'
                    AND ISNULL(t1.in_final_rank, 0) > 0
                    AND t1.in_final_rank <= {#max_rank}
                ORDER BY
                    t1.in_final_rank
                    , t3.in_tree_id
                    , t1.in_foot
            ";

            sql = sql.Replace("{#meeting_id}", meeting_id)
                .Replace("{#program_id}", program_id)
                .Replace("{#max_rank}", max_rank);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return inn.applySQL(sql);
        }

        //取得 Children 組別資訊
        private Item GetChildrenPrograms(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string program_id, string in_group_tag = "")
        {
            string group_condition = in_group_tag == "" ? "" : "AND t1.in_group_tag = N'" + in_group_tag + "'";

            string sql = @"
        SELECT
            t1.*
            , t2.label AS 'program_battle_type'
        FROM
            IN_MEETING_PROGRAM t1 WITH(NOLOCK)
        LEFT OUTER JOIN
        (
            SELECT
                t12.value AS 'value'
                , t12.label_zt AS 'label'
            FROM
                [LIST] t11 WITH(NOLOCK)
            INNER JOIN
                [VALUE] t12 WITH(NOLOCK)
                ON t12.source_id = t11.id
            WHERE
                t11.name = N'In_Meeting_BattleType'
        ) t2
            ON t1.in_battle_type = t2.value
        WHERE
            t1.in_program = '{#program_id}'
            {#group_condition}
        ORDER BY
            t1.in_group_type
            , t1.in_group_id
        ";

            sql = sql.Replace("{#program_id}", program_id)
                .Replace("{#group_condition}", group_condition);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return inn.applySQL(sql);
        }

        private int GetIntVal(string value, int def = 0)
        {
            int result = def;
            Int32.TryParse(value, out result);
            return result;
        }

        private string GetRankDisplay(string in_final_rank, string[] rank_arr)
        {
            string rank_display = "";

            int numRank = GetIntVal(in_final_rank);

            if (numRank > rank_arr.Length)
            {
                rank_display = "";
            }
            else
            {
                if (rank_arr[numRank] != "")
                {
                    rank_display = "第 " + rank_arr[numRank] + " 名"; ;
                }
            }
            return rank_display;
        }

        private string GetDateTimeVal(string value, string format)
        {
            DateTime result = DateTime.MinValue;
            DateTime.TryParse(value, out result);
            if (result != DateTime.MinValue)
            {
                return result.ToString(format);
            }
            return "";
        }
    }
}