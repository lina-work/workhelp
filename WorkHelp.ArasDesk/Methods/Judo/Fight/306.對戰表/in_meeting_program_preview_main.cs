﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Judo.Fight
{
    public class in_meeting_program_preview_main : Item
    {
        public in_meeting_program_preview_main(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                 目的: 單淘預覽
                 日期: 
                     2020-11-24: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]in_meeting_program_preview_main";

            Item itmR = inn.newItem();
            //CCO.Utilities.WriteDebug(strMethodName, this.dom.InnerXml);

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,

                meeting_id = this.getProperty("meeting_id", ""),
                program_id = this.getProperty("program_id", ""),
                mode = this.getProperty("mode", ""),
                eno = this.getProperty("eno", ""),
                in_sign_time = this.getProperty("in_sign_time", ""),
                page_language = this.getProperty("page_language", ""),

                itmProgram = this,
            };

            cfg.showSignNo = cfg.mode == "draw";
            cfg.showOrgCount = cfg.mode == "draw" && cfg.in_sign_time != "";
            cfg.isEdit = cfg.eno == "edit";
            cfg.isRecover = cfg.eno == "recover";

            //比賽選手
            Item itmEventPlayers = GetEventPlayers(cfg);

            if (!itmEventPlayers.isError() && itmEventPlayers.getResult() != "")
            {
                cfg.EventMap = ItemsToDictionary(itmEventPlayers);
                AppendEvents(cfg, itmEventPlayers, itmR);
            }

            return itmR;
        }

        private Dictionary<string, Item> ItemsToDictionary(Item items)
        {
            var result = new Dictionary<string, Item>();
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var key = item.getProperty("in_tree_id", "");
                if (!result.ContainsKey(key))
                {
                    result.Add(key, item);
                }
            }
            return result;
        }


        //附加比賽選手
        private void AppendEvents(TConfig cfg, Item itmEventPlayers, Item itmReturn)
        {
            TMap map = MapEvents(cfg, itmEventPlayers);

            BindMap(cfg, map);

            string contents = GetBracketTableContents(cfg, map);

            itmReturn.setProperty("bracket_table", contents);
        }
        #region 賽程 Table

        /// <summary>
        /// 繪製賽程表資料表
        /// </summary>
        private string GetBracketTableContents(TConfig cfg, TMap map)
        {
            var objs = GetMapRows(cfg, map, map.Mains);

            var body = new StringBuilder();

            body.AppendLine("<tbody>");

            for (int i = 1; i <= map.rows; i++)
            {
                body.AppendLine("<tr>");

                var obj = objs[i - 1];

                for (int j = 1; j <= map.cols; j++)
                {
                    var field = obj.Cols[j - 1];

                    if (field.IsRemove)
                    {
                        continue;
                    }

                    body.Append("<td");
                    body.Append(field.RowSpan);
                    body.Append(GetClasses(field.Classes));
                    body.Append(" >");
                    body.Append(field.Value);
                    body.Append("</td>");
                }
                body.AppendLine("</tr>");
            }
            body.AppendLine("</tbody>");

            var builder = new StringBuilder();

            builder.AppendLine("<table class='tb-box inno-main'>");
            builder.Append(GetHeadBuilder(cfg, map));
            builder.Append(body);
            builder.AppendLine("</table>");

            return builder.ToString();
        }

        private List<TRow> GetMapRows(TConfig cfg, TMap map, Dictionary<string, List<Item>> dictionary)
        {
            List<TRow> rows = GetMapRowsData(cfg, map, dictionary);

            List<TSignGroup> signs = map.real_team_count == 2 ? GetSigns2(map) : GetSigns(map);

            AppendMapLines(cfg, map, dictionary, rows, signs);

            return rows;
        }

        private List<TSignGroup> GetSigns2(TMap map)
        {
            List<TSignGroup> result = new List<TSignGroup>();

            int center_cidx = 6;

            result.Add(new TSignGroup { cidx = center_cidx, ys = 0, ye = 0, val = "1", id = "M101", is_end = true });

            return result;
        }

        private List<TSignGroup> GetSigns(TMap map)
        {
            List<TSignGroup> result = new List<TSignGroup>();

            int xs = 0;
            int xcount = map.tree_team_count / 2;
            int yspan = map.team_rows;

            int last_cidx = 0;
            for (int i = 0; i < map.round - 1; i++)
            {
                int round = i + 1;
                int l_cidx = map.team_col_end + i;
                int r_cidx = map.cols - l_cidx - 1;
                List<int> cidxs = new List<int> { l_cidx, r_cidx };

                AppendSignGroup(map, result, round, cidxs, xs, xcount, yspan);

                xs += yspan / 2;
                xcount = xcount / 2;
                yspan = yspan * 2;
                last_cidx = l_cidx;
            }

            var m_cidx = last_cidx + 2;
            var m_ridx = map.rows / 2 - 1;
            var m_tree_id = GetRowId(map.round, 1);

            result.Add(new TSignGroup { cidx = m_cidx, ys = m_ridx, ye = m_ridx, val = m_tree_id, id = m_tree_id, is_end = true });

            return result;
        }

        private void AppendSignGroup(TMap map, List<TSignGroup> groups, int round, List<int> cidxs, int xs, int xcount, int yspan)
        {
            int round_id = 1;
            for (int i = 0; i < cidxs.Count; i++)
            {
                int cidx = cidxs[i];
                bool is_right = i > 0;

                for (int j = 0; j < xcount; j++)
                {
                    if ((j % 2) == 1) continue;

                    int ridx = j * yspan;
                    int ys = ridx + xs;
                    int ye = ys + yspan;
                    string in_tree_id = GetRowId(round, round_id);

                    var group = new TSignGroup
                    {
                        cidx = cidx,
                        ys = ys,
                        ye = ye,
                        is_right = is_right,
                        val = in_tree_id,
                        id = in_tree_id,
                    };

                    groups.Add(group);
                    round_id++;
                }
            }
        }

        private string GetRowId(int round, int seq)
        {
            return "M" + round + seq.ToString().PadLeft(2, '0');
        }

        private void AppendMapLines(TConfig cfg, TMap map, Dictionary<string, List<Item>> dictionary, List<TRow> rows, List<TSignGroup> signs)
        {
            for (int i = 0; i < signs.Count; i++)
            {
                var sg = signs[i];
                var cidx = sg.cidx;

                if (sg.is_long)
                {
                    rows[sg.ridx].Cols[cidx].Classes.Add("line-bottom");
                }
                else
                {
                    int ys = sg.ys;
                    int ye = sg.ye;
                    int ym = (ys + ye) / 2;

                    rows[ys].Cols[cidx].Classes.Add("line-bottom");
                    rows[ye + 1].Cols[cidx].Classes.Add("line-top");

                    Item itmSign1 = null;
                    Item itmSign2 = null;

                    if (sg.id != null && sg.id != "")
                    {
                        itmSign1 = GetTeam(cfg, dictionary, sg.id, "1");
                        itmSign2 = GetTeam(cfg, dictionary, sg.id, "2");
                    }
                    else
                    {
                        itmSign1 = cfg.inn.newItem();
                        itmSign2 = cfg.inn.newItem();
                    }

                    string in_tree_no = itmSign1.getProperty("in_tree_no", "");
                    string win_sign_no = itmSign1.getProperty("in_win_sign_no", "");
                    string f1_sign_no = itmSign1.getProperty("in_sign_no", "");
                    string f2_sign_no = itmSign2.getProperty("in_sign_no", "");

                    bool f1_win = f1_sign_no == win_sign_no;
                    bool f2_win = f2_sign_no == win_sign_no;

                    if (sg.is_end)
                    {
                        rows[ym].Cols[cidx - 1].Classes.Add("line-bottom");
                        rows[ym].Cols[cidx + 1].Classes.Add("line-bottom");
                        rows[ym].Cols[cidx - 1].Classes.Add(GetSignClass(itmSign1));
                        rows[ym].Cols[cidx + 1].Classes.Add(GetSignClass(itmSign2));

                        rows[ym].Cols[cidx - 1].Value = GetSignNoInfo(itmSign1);
                        rows[ym].Cols[cidx].Value = GetRowIdLink(cfg, sg.id, in_tree_no, "");
                        rows[ym].Cols[cidx + 1].Value = GetSignNoInfo(itmSign2);

                        rows[ym + 1].Cols[cidx - 1].Value = GetStatusDisplay(itmSign1);
                        rows[ym + 1].Cols[cidx + 1].Value = GetStatusDisplay(itmSign2);

                        if (itmSign1.getProperty("in_status") == "1")
                        {
                            rows[ym].Cols[cidx].Classes.Add(GetSignClass(itmSign1));
                        }
                        else if (itmSign2.getProperty("in_status") == "1")
                        {
                            rows[ym].Cols[cidx].Classes.Add(GetSignClass(itmSign2));
                        }
                    }
                    else
                    {
                        rows[ys].Cols[cidx].Value = GetSignNoInfo(itmSign1);
                        rows[ym].Cols[cidx].Value = GetRowIdLink(cfg, sg.id, in_tree_no, "");
                        rows[ye + 1].Cols[cidx].Value = GetSignNoInfo(itmSign2);

                        rows[ys].Cols[cidx].Classes.Add(GetSignClass(itmSign1));
                        rows[ye + 1].Cols[cidx].Classes.Add(GetSignClass(itmSign2));

                    }

                    for (int y = ys + 1; y <= ye; y++)
                    {
                        string in_win_time = itmSign1.getProperty("in_win_time", "");

                        if (sg.is_right)
                        {
                            rows[y].Cols[cidx].Classes.Add("line-left");

                        }
                        else
                        {
                            rows[y].Cols[cidx].Classes.Add("line-right");
                        }

                        if (y <= ym)
                        {
                            if (f1_win)
                            {
                                rows[y].Cols[cidx].Classes.Add(GetSignClass(itmSign1));
                            }
                        }
                        else
                        {
                            if (f2_win)
                            {
                                rows[y].Cols[cidx].Classes.Add(GetSignClass(itmSign2));
                            }
                        }
                    }
                }
            }
        }

        private string GetSignClass(Item itmSign)
        {
            string org_name = itmSign.getProperty("map_short_org", "");
            if (org_name == "")
            {
                return "";
            }

            if (itmSign.getProperty("in_sign_bypass", "") == "1")
            {
                return "";
            }

            string in_section_no = itmSign.getProperty("in_section_no", "");
            return "sgn-" + in_section_no.PadLeft(3, '0');
        }

        private Item GetTeam(TConfig cfg, Dictionary<string, List<Item>> dictionary, string key, string foot)
        {
            Item result = null;
            if (!string.IsNullOrEmpty(key) && dictionary.ContainsKey(key))
            {
                List<Item> list = dictionary[key];

                for (int i = 0; i < list.Count; i++)
                {
                    Item item = list[i];
                    string in_sign_foot = item.getProperty("in_sign_foot", "");
                    if (in_sign_foot == foot)
                    {
                        result = item;
                    }
                }
            }

            if (result == null)
            {
                result = cfg.inn.newItem();
            }

            return result;
        }

        private string GetRowIdLink(TConfig cfg, string rid, string tno, string btn_class)
        {
            if (cfg.isEdit)
            {
                return GetRowIdLinkEdit(cfg, rid, tno, btn_class);
            }
            else
            {
                return GetRowIdLinkView(cfg, rid, tno, btn_class);
            }
        }

        private string GetRowIdLinkEdit(TConfig cfg, string rid, string tno, string btn_class)
        {
            string tree_no = tno == "" ? "NA" : tno;

            return "<input type='text' value='" + tno + "' class='event-input'"
            + " data-pid='" + cfg.program_id + "' data-rid='" + rid + "' data-old='" + tno + "'"
            + " onkeyup='Event_KeyUp(this)'"
            + " ondblclick='Event_Click(this)'"
            + " >";
        }

        private string GetRowIdLinkView(TConfig cfg, string rid, string tno, string btn_class)
        {
            var itmEvt = cfg.EventMap.ContainsKey(rid) ? cfg.EventMap[rid] : cfg.inn.newItem();
            var event_id = itmEvt.getProperty("event_id", "");
            var in_site_code = itmEvt.getProperty("in_site_code", "");
            var current_class = btn_class == "" ? "event-btn" : btn_class;
            var val = "　";

            if (tno != "" && tno != "0")
            {
                val = "&nbsp;" + tno + "&nbsp;";
            }
            else
            {
                if (cfg.isRecover)
                {
                    val = "NA";
                }
                else
                {
                    val = "　";
                    current_class = "no-event-btn";
                }
            }

            return "<a class='" + current_class + "' href='javascript:void(0)' onclick='Event_Click(this)'"
                + " data-pid='" + cfg.program_id + "'"
                + " data-eid='" + event_id + "'"
                + " data-scd='" + in_site_code + "'"
                + " data-rid='" + rid + "'"
                + " >" + val + "</a>";
        }

        private List<TRow> GetMapRowsData(TConfig cfg, TMap map, Dictionary<string, List<Item>> dictionary)
        {
            List<TRow> result = new List<TRow>();

            var body_merge = new List<int>();
            body_merge.Add(1);
            body_merge.Add(3);
            body_merge.Add(5);
            body_merge.Add(map.cols - 0);
            body_merge.Add(map.cols - 2);
            body_merge.Add(map.cols - 4);

            for (int i = 1; i <= map.rows; i++)
            {
                int rno = i % 4 == 0 ? i / 4 : i / 4 + 1;

                TRow row = new TRow
                {
                    RNo = rno,
                    Cols = new List<TCol>(),
                };

                row.IsTeamRow = i % 4 == 1;
                row.IsMergeRow = i % 4 == 2;

                for (int j = 1; j <= map.cols; j++)
                {
                    TCol col = new TCol
                    {
                        Classes = new List<string>()
                    };
                    row.Cols.Add(col);
                }

                if (row.IsTeamRow)
                {
                    row.Cols[1 - 1].Classes.Add("rpc-number");
                    row.Cols[3 - 1].Classes.Add("rpc-org inn-left");
                    row.Cols[5 - 1].Classes.Add("rpc-name");
                    row.Cols[map.cols - 1].Classes.Add("rpc-number");
                    row.Cols[map.cols - 3].Classes.Add("rpc-org inn-right");
                    row.Cols[map.cols - 5].Classes.Add("rpc-name");
                }

                for (int x = 0; x < body_merge.Count; x++)
                {
                    var idx = body_merge[x];
                    var field = row.Cols[idx - 1];

                    if (row.IsTeamRow)
                    {
                        field.RowSpan = " rowspan='2'";
                    }
                    else if (row.IsMergeRow)
                    {
                        field.IsRemove = true;
                    }
                }

                if (row.IsTeamRow)
                {
                    int xs = map.tree_team_count / 4;
                    int rs = row.RNo % 2 == 0 ? row.RNo / 2 : row.RNo / 2 + 1;

                    string l_tree_id = GetRowId(1, rs);
                    string r_tree_id = GetRowId(1, rs + xs);
                    string sign_foot = rno % 2 != 0 ? "1" : "2";

                    var itmLeft = default(Item);
                    var itmRight = default(Item);

                    if (map.real_team_count == 2)
                    {
                        itmLeft = GetTeam(cfg, dictionary, l_tree_id, "1");
                        itmRight = GetTeam(cfg, dictionary, r_tree_id, "2");
                    }
                    else
                    {
                        itmLeft = GetTeam(cfg, dictionary, l_tree_id, sign_foot);
                        itmRight = GetTeam(cfg, dictionary, r_tree_id, sign_foot);
                    }

                    row.Cols[1 - 1].Value = GetSignNoInfo(itmLeft);
                    row.Cols[3 - 1].Value = GetOrgInfo(cfg, itmLeft);
                    row.Cols[5 - 1].Value = GetNameInfo(cfg, itmLeft);

                    row.Cols[map.cols - 1].Value = GetSignNoInfo(itmRight);
                    row.Cols[map.cols - 3].Value = GetOrgInfo(cfg, itmRight);
                    row.Cols[map.cols - 5].Value = GetNameInfo(cfg, itmRight);
                }

                result.Add(row);
            }
            return result;
        }

        /// <summary>
        /// 取得標題內文
        /// </summary>
        private StringBuilder GetHeadBuilder(TConfig cfg, TMap map)
        {
            var hdr_num = "籤號";
            var hdr_org = "單位";
            var hdr_nam = "姓名";
            var head_titles = GetClearArr(map.arrs);
            if (cfg.page_language == "en")
            {
                hdr_num = "No.";
                hdr_org = "Org.";
                hdr_nam = "Name";
            }

            head_titles[1] = hdr_num;
            head_titles[3] = hdr_org;
            head_titles[5] = hdr_nam;
            head_titles[map.cols - 0] = hdr_num;
            head_titles[map.cols - 2] = hdr_org;
            head_titles[map.cols - 4] = hdr_nam;

            for (int i = 1; i < map.round; i++)
            {
                var s = map.team_col_end;
                var e = map.cols;
                head_titles[s + i] = "R" + i;
                head_titles[e - 4 - i] = "R" + i;
            }
            head_titles[map.cols_half] = "R" + map.round;

            var head_widths = GetClearArr(map.arrs, "width='50px' ");
            head_widths[1] = "";
            head_widths[2] = "width='1px' ";
            head_widths[3] = "";
            head_widths[4] = "width='1px' ";
            head_widths[5] = "";
            head_widths[map.cols - 0] = "";
            head_widths[map.cols - 1] = "width='1px' ";
            head_widths[map.cols - 2] = "";
            head_widths[map.cols - 3] = "width='1px' ";
            head_widths[map.cols - 4] = "";

            var head = new StringBuilder();
            head.AppendLine("<thead>");
            head.AppendLine("<tr>");
            for (int j = 1; j <= map.cols; j++)
            {
                head.Append("<th ");
                head.Append(head_widths[j]);
                head.Append(" >");
                head.Append(head_titles[j]);
                head.Append("</th>");
                head.AppendLine();
            }
            head.AppendLine("</tr>");
            head.AppendLine("</thead>");

            return head;
        }

        /// <summary>
        /// 資料列
        /// </summary>
        private class TRow
        {
            /// <summary>
            /// 列序號
            /// </summary>
            public int RNo { get; set; }

            /// <summary>
            /// 是否為隊伍資料列
            /// </summary>
            public bool IsTeamRow { get; set; }

            /// <summary>
            /// 是否為合併列
            /// </summary>
            public bool IsMergeRow { get; set; }

            /// <summary>
            /// 資料欄
            /// </summary>
            public List<TCol> Cols { get; set; }
        }

        /// <summary>
        /// 資料欄
        /// </summary>
        private class TCol
        {
            /// <summary>
            /// 是否移除 (不繪製該 td)
            /// </summary>
            public bool IsRemove { get; set; }

            /// <summary>
            /// 跨列合併數
            /// </summary>
            public string RowSpan { get; set; }

            /// <summary>
            /// 樣式集
            /// </summary>
            public List<string> Classes { get; set; }

            /// <summary>
            /// 值
            /// </summary>
            public string Value { get; set; }
        }

        private class TRowTeam
        {
            public TTeam WTeam { get; set; }
            public TTeam ETeam { get; set; }
        }

        private class TTeam
        {
            public string in_tree_id { get; set; }

            public string in_sign_foot { get; set; }
        }

        private class TSignGroup
        {
            public bool is_right { get; set; }
            public bool is_end { get; set; }
            public bool is_long { get; set; }
            public int cidx { get; set; }
            public int ridx { get; set; }
            public int ys { get; set; }
            public int ye { get; set; }
            public string id { get; set; }
            public string val { get; set; }
        }

        /// <summary>
        /// 繫結圖面資料
        /// </summary>
        /// <param name="map"></param>
        private void BindMap(TConfig cfg, TMap map)
        {
            int team_col_end = 5;

            map.round = GetRounds(map.tree_team_count);

            map.team_rows = 4;
            map.group_rows = map.team_rows * 2;

            map.surface_teams = map.tree_team_count / 2;
            map.surface_events = map.surface_teams / 2;

            map.team_cols = (3 + 2) * 2;
            map.round_cols = map.round * 2 + 1;

            map.rows = map.surface_teams * map.team_rows - 2;//最後一列不需 4 列
            map.cols = map.team_cols + map.round_cols;

            map.rows_half = map.rows / 2;
            map.cols_half = map.cols / 2 + 1;

            map.arrs = map.cols + 1;

            map.m_idxs = new List<int>();
            map.m_idxs.Add(map.cols_half - 1);
            map.m_idxs.Add(map.cols_half);
            map.m_idxs.Add(map.cols_half + 1);


            map.team_col_end = team_col_end;

            map.l_idxs = new List<int>();
            map.r_idxs = new List<int>();

            for (int i = 1; i < map.round; i++)
            {
                var s = map.team_col_end;
                var e = map.cols;

                map.l_idxs.Add(s + i);
                map.r_idxs.Add(e - 4 - i);
            }
        }

        /// <summary>
        /// 圖面資料模型
        /// </summary>
        private class TMap
        {
            /// <summary>
            /// 賽事 id
            /// </summary>
            public string meeting_id { get; set; }

            /// <summary>
            /// 組別 id
            /// </summary>
            public string program_id { get; set; }

            /// <summary>
            /// 組別隊伍數
            /// </summary>
            public int real_team_count { get; set; }

            /// <summary>
            /// 賽程表隊伍數
            /// </summary>
            public int tree_team_count { get; set; }

            /// <summary>
            /// 回合數
            /// </summary>
            public int round { get; set; }

            /// <summary>
            /// 隊-資料列數
            /// </summary>
            public int team_rows { get; set; }

            /// <summary>
            /// 競賽群組-資料列數
            /// </summary>
            public int group_rows { get; set; }

            /// <summary>
            /// 組面-隊伍數
            /// </summary>
            public int surface_teams { get; set; }

            /// <summary>
            /// 組面-場次數
            /// </summary>
            public int surface_events { get; set; }

            /// <summary>
            /// 隊資-欄位數
            /// </summary>
            public int team_cols { get; set; }

            /// <summary>
            /// 回合-欄位數
            /// </summary>
            public int round_cols { get; set; }

            /// <summary>
            /// 列數
            /// </summary>
            public int rows { get; set; }

            /// <summary>
            /// 欄數
            /// </summary>
            public int cols { get; set; }

            /// <summary>
            /// y軸 中數 (rows 除以 2)
            /// </summary>
            public int rows_half { get; set; }

            /// <summary>
            /// x軸 中數 (cols 除以 2)
            /// </summary>
            public int cols_half { get; set; }

            /// <summary>
            /// 隊資欄位-結束位置
            /// </summary>
            public int team_col_end { get; set; }

            /// <summary>
            /// 字串陣列(由於從 1 取值，因此長度為 cols + 1)
            /// </summary>
            public int arrs { get; set; }

            /// <summary>
            /// 回合-左面欄位索引集
            /// </summary>
            public List<int> l_idxs { get; set; }

            /// <summary>
            /// 回合-中間欄位索引集
            /// </summary>
            public List<int> m_idxs { get; set; }

            /// <summary>
            /// 回合-右間欄位索引集
            /// </summary>
            public List<int> r_idxs { get; set; }

            /// <summary>
            /// 主線資料集
            /// </summary>
            public Dictionary<string, List<Item>> Mains { get; set; }

            /// <summary>
            /// 隊資料集
            /// </summary>
            public Dictionary<string, Item> Teams { get; set; }

            /// <summary>
            /// 場次資料集
            /// </summary>
            public Dictionary<string, Dictionary<string, Item>> Events { get; set; }
        }

        #endregion 賽程 Table

        //轉換比賽分組
        private TMap MapEvents(TConfig cfg, Item itmEventPlayers)
        {
            TMap result = new TMap
            {
                meeting_id = cfg.meeting_id,
                program_id = cfg.program_id,
                real_team_count = GetIntVal(cfg.itmProgram.getProperty("in_team_count", "0")),
                tree_team_count = GetIntVal(cfg.itmProgram.getProperty("in_round_code", "0")),

                Mains = new Dictionary<string, List<Item>>(),

                Teams = new Dictionary<string, Item>(),
                Events = new Dictionary<string, Dictionary<string, Item>>(),

            };

            int count = itmEventPlayers.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = itmEventPlayers.getItemByIndex(i);
                string round = item.getProperty("in_round", "");
                string in_tree_id = item.getProperty("in_tree_id", "");

                AppendDetail(result.Mains, item, in_tree_id);
            }

            return result;
        }

        private void AppendEvents(Dictionary<string, Dictionary<string, Item>> dictionary, Item item)
        {
            string in_tree_id = item.getProperty("in_tree_id", "");
            string in_sign_foot = item.getProperty("in_sign_foot", "");

            //M101
            string key = in_tree_id;
            string sub_key = in_sign_foot;

            Dictionary<string, Item> sub = null;
            if (dictionary.ContainsKey(key))
            {
                sub = dictionary[key];
            }
            else
            {
                sub = new Dictionary<string, Item>();
                dictionary.Add(key, sub);
            }

            if (sub.ContainsKey(sub_key))
            {
                //異常
            }
            else
            {
                sub.Add(sub_key, item);
            }
        }

        private void AppendTeams(Dictionary<string, Item> dictionary, Item item)
        {
            string mdid = item.getProperty("in_sign_no", "");
            string in_tree_id = item.getProperty("in_tree_id", "");
            string in_sign_foot = item.getProperty("in_sign_foot", "");

            //M101-1
            string key = in_tree_id + "-" + in_sign_foot;

            if (dictionary.ContainsKey(key))
            {
                //異常
            }
            else
            {
                if (mdid == "")
                {
                    item.setProperty("no_team", "1");
                }
                dictionary.Add(key, item);
            }
        }

        private void AppendDetail(Dictionary<string, List<Item>> dictionary, Item item, string key)
        {
            if (dictionary.ContainsKey(key))
            {
                dictionary[key].Add(item);
            }
            else
            {
                List<Item> list = new List<Item>();
                list.Add(item);
                dictionary.Add(key, list);
            }
        }

        //取得勝敗呈現(LINE)
        private string GetStatusDisplay(Item item)
        {
            string in_status = item.getProperty("in_status", "");
            string in_points = item.getProperty("in_points", "0");

            switch (in_status)
            {
                case "1": return "<span class='team_score_b'>" + in_points + "</span>";
                case "0": return "<span class='team_score_c'>" + in_points + "</span>";
                default: return "<span class='team_score_a'>&nbsp;</span>";
            }
        }

        private string GetSignNoInfo(Item item)
        {
            string in_sign_bypass = item.getProperty("in_sign_bypass", "");
            if (in_sign_bypass == "1")
            {
                return "";
            }

            string in_current_org = item.getProperty("in_current_org", "");
            if (in_current_org == "")
            {
                return "";
            }

            return item.getProperty("in_section_no", "");
        }

        private string GetNameInfo(TConfig cfg, Item item)
        {
            string in_name = item.getProperty("in_name", "");
            string in_sign_no = item.getProperty("in_sign_no", "");
            string in_check_result = item.getProperty("in_check_result", "");
            string in_weight_message = item.getProperty("in_weight_message", "");

            string display = in_name.Replace("(", "<br>(") + in_weight_message;

            if (in_name == "")
            {
                display = in_weight_message;
            }

            switch (in_check_result)
            {
                case "1": return "<span class='player_on'>" + display + "</span>";
                case "0": return "<span class='player_off'>" + display + "</span>";
                default: return "<span  class='player_default'>" + display + "</span>";
            }
        }

        private string GetOrgInfo(TConfig cfg, Item item)
        {
            string org_name = item.getProperty("map_short_org", "");
            if (org_name == "")
            {
                return "";
            }

            if (cfg.showOrgCount)
            {
                string in_org_teams = item.getProperty("in_org_teams", "");
                if (in_org_teams != "" && in_org_teams != "0" && in_org_teams != "1")
                {
                    org_name += " (" + in_org_teams + ")";
                }
            }

            return org_name;
        }

        //取得賽事組別場次
        private Item GetEventPlayers(TConfig cfg)
        {
            string sql = @"
                SELECT
                    t1.id AS 'event_id'
                    , t1.in_tree_name
                    , t1.in_tree_id
                    , t1.in_tree_no
                    , t1.in_round
                    , t1.in_round_id
                    , t1.in_round_code
                    , t1.in_site_code
                    , t1.in_site_no
                    , t1.in_site_id
                    , t1.in_site_allocate
                    , t1.in_next_win
                    , t1.in_next_foot_win
                    , t1.in_next_lose
                    , t1.in_next_foot_lose
                    , t1.in_detail_ns
                    , t1.in_win_sign_no
                    , t1.in_win_time
                    , t2.id AS 'detail_id'
                    , t2.in_sign_foot
                    , t2.in_sign_no
                    , t2.in_sign_bypass
                    , t2.in_target_no
                    , t2.in_points
                    , t2.in_status
                    , t3.id AS 'mdid'
                    , ISNULL(t3.in_current_org, t2.in_player_org) AS 'in_current_org'
                    , ISNULL(t3.in_short_org, t2.in_player_org)   AS 'in_short_org'
                    , ISNULL(t3.map_short_org, t2.in_player_org)  AS 'map_short_org'
                    , ISNULL(t3.in_name, t2.in_player_name)       AS 'in_name'
                    , ISNULL(t3.in_sno, t2.in_player_sno)         AS 'in_sno'
                    , t3.in_names
                    , t3.in_org_teams
                    , t3.in_section_no
                    , t3.in_judo_no
                    , t3.in_seeds
                    , t3.in_check_result
                    , t3.in_weight_message
                FROM 
                    IN_MEETING_PEVENT t1 WITH(NOLOCK)
                LEFT OUTER JOIN
                    IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK)
                    ON t2.source_id = t1.id
                LEFT OUTER JOIN
                    VU_MEETING_PTEAM t3 WITH(NOLOCK)
                    ON t3.source_id = t1.source_id
                    AND ISNULL(t3.in_sign_no, '') <> ''
                    AND t3.in_sign_no = t2.in_sign_no
                WHERE
                    t1.source_id = '{#program_id}'
                    AND t1.in_tree_name = 'main'
                ORDER BY
                    t1.in_tree_name
                    , t1.in_round
                    , t1.in_round_id
                    , t2.in_sign_foot
             ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#program_id}", cfg.program_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// 取得樣式集
        /// </summary>
        private string GetClasses(List<string> list)
        {
            if (list.Count == 0)
            {
                return " class='td_empty'";
            }
            else
            {
                return " class='" + string.Join(" ", list) + "'";
            }
        }

        /// <summary>
        /// 取得回合數
        /// </summary>
        private int GetRounds(int value, int code = 2, int count = 0)
        {
            while (value > 1)
            {
                value = value / code;
                count++;
            }
            return count;
        }

        /// <summary>
        /// 取得空字串陣列
        /// </summary>
        private string[] GetClearArr(int len, string def = "")
        {
            string[] result = new string[len];
            for (int i = 0; i < len; i++)
            {
                result[i] = def;
            }
            return result;
        }

        private int GetIntVal(string value, int def = 0)
        {
            int result = def;
            Int32.TryParse(value, out result);
            return result;
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }

            public string meeting_id { get; set; }
            public string program_id { get; set; }
            public string mode { get; set; }
            public string eno { get; set; }
            public string in_sign_time { get; set; }
            public string page_language { get; set; }

            public Item itmProgram { get; set; }

            /// <summary>
            /// 是否秀出籤號
            /// </summary>
            public bool showSignNo { get; set; }
            /// <summary>
            /// 是否秀出同隊數量
            /// </summary>
            public bool showOrgCount { get; set; }
            /// <summary>
            /// 是否場次編號可編輯模式
            /// </summary>
            public bool isEdit { get; set; }
            /// <summary>
            /// 是否場次全秀 (NA 可點擊)
            /// </summary>
            public bool isRecover { get; set; }

            public Dictionary<string, Item> EventMap { get; set; }
        }
    }
}