﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Judo.Fight
{
    public class in_meeting_program_preview_robin2 : Item
    {
        public in_meeting_program_preview_robin2(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 循環賽預覽(三戰兩勝)
                日期: 
                    - 2021-10-09: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]in_meeting_program_preview_robin2";

            Item itmProgram = this;
            Item itmR = inn.newItem();
            //CCO.Utilities.WriteDebug(strMethodName, this.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,

                meeting_id = itmProgram.getProperty("meeting_id", ""),
                program_id = itmProgram.getProperty("program_id", ""),
                eno = itmProgram.getProperty("eno", ""),
            };

            this.GLOBAL_EVENT_EDIT = cfg.eno == "edit";

            //賽事資訊
            Item itmMeeting = inn.applySQL("SELECT id, in_title, in_uniform_color, in_language FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            cfg.is_english = itmMeeting.getProperty("in_language", "") == "en";

            //道服顏色資訊
            Item itmColor = inn.newItem("In_Meeting");
            itmColor.setProperty("in_uniform_color", itmMeeting.getProperty("in_uniform_color", ""));
            itmColor = itmColor.apply("in_meeting_uniform_color");

            //附加比賽選手
            Item itmPlayers = GetPlayers(cfg);

            //附加場次
            Item itPlayerEvents = GetEventPlayers(cfg);

            //統計積分與勝場
            Dictionary<string, TStatistics> map = MapTeam(itPlayerEvents);

            var evt_list = GetEventMap(cfg);

            if (!itmPlayers.isError())
            {
                AppendCycleEvent(cfg, map, evt_list, itmProgram, itmPlayers, itmColor, itmR);
            }

            return itmR;
        }

        //是否場次編號可編輯模式
        private bool GLOBAL_EVENT_EDIT = false;

        /// <summary>
        /// 統計隊伍積分與勝場
        /// </summary>
        private Dictionary<string, TStatistics> MapTeam(Item itmEvents)
        {
            Dictionary<string, TStatistics> map = new Dictionary<string, TStatistics>();
            int count = itmEvents.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item itmEvent = itmEvents.getItemByIndex(i);
                string in_sign_no = itmEvent.getProperty("in_sign_no", "");
                string in_status = itmEvent.getProperty("in_status", "");
                string in_points = itmEvent.getProperty("in_points", "0");
                int points = GetIntVal(in_points);

                TStatistics entity = null;
                if (map.ContainsKey(in_sign_no))
                {
                    entity = map[in_sign_no];
                }
                else
                {
                    entity = new TStatistics
                    {
                        in_sign_no = in_sign_no,
                        total_points = 0,
                        total_wins = 0,
                    };
                    map.Add(in_sign_no, entity);
                }

                if (in_status == "1")
                {
                    entity.total_points += points;
                    entity.total_wins++;
                }

            }
            return map;
        }

        //循環賽
        private void AppendCycleEvent(TConfig cfg
            , Dictionary<string, TStatistics> map
            , List<TEvent> evt_list
            , Item itmProgram
            , Item itmPlayers
            , Item itmColor
            , Item itmReturn)
        {


            var builder = new StringBuilder();

            //循環賽表格
            AppendCycelTable(cfg, map, evt_list, itmProgram, itmPlayers, builder);

            string is_draw = itmProgram.getProperty("is_draw", "");
            if (is_draw != "1")
            {
                //對戰表格
                AppendFightTable(cfg, itmProgram, evt_list, builder, itmColor);
            }

            itmReturn.setProperty("bracket_table", builder.ToString());
        }

        private void AppendFightTable(TConfig cfg, Item itmProgram, List<TEvent> evt_list, StringBuilder builder, Item itmColor)
        {
            string f1n = itmColor.getProperty("f1_name", "");
            string f1c = itmColor.getProperty("f1_css", "");
            string f2n = itmColor.getProperty("f2_name", "");
            string f2c = itmColor.getProperty("f2_css", "");

            var program_id = itmProgram.getProperty("id", "");

            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            head.AppendLine("<thead>");
            head.AppendLine("  <tr>");
            head.AppendLine("    <td class='text-center' rowspan='2'>場地</td>");
            head.AppendLine("    <td class='text-center' rowspan='2'>場次</td>");
            head.AppendLine("    <td class='text-center " + f1c + "' rowspan='2'>" + f1n + "<br>籤號</td>");
            head.AppendLine("    <td class='text-center " + f1c + "' rowspan='2'>" + f1n + "選手</td>");
            head.AppendLine("    <td class='text-center " + f1c + "' rowspan='1' colspan='3'>得分</td>");
            head.AppendLine("    <td class='text-center " + f1c + "' rowspan='2'>得分</td>");
            head.AppendLine("    <td class='text-center' rowspan='2'>比賽時間</td>");
            head.AppendLine("    <td class='text-center " + f2c + "' rowspan='2'>得分</td>");
            head.AppendLine("    <td class='text-center " + f2c + "' rowspan='1' colspan='3'>得分</td>");
            head.AppendLine("    <td class='text-center " + f2c + "' rowspan='2'>" + f2n + "選手</td>");
            head.AppendLine("    <td class='text-center " + f2c + "' rowspan='2'>" + f2n + "<br>籤號</td>");
            head.AppendLine("  </tr>");
            head.AppendLine("  <tr>");
            head.AppendLine("    <td class='text-center " + f1c + "'>I</td>");
            head.AppendLine("    <td class='text-center " + f1c + "'>W</td>");
            head.AppendLine("    <td class='text-center " + f1c + "'>S</td>");
            head.AppendLine("    <td class='text-center " + f2c + "'>I</td>");
            head.AppendLine("    <td class='text-center " + f2c + "'>W</td>");
            head.AppendLine("    <td class='text-center " + f2c + "'>S</td>");
            head.AppendLine("  </tr>");
            head.AppendLine("</thead>");


            body.AppendLine("<tbody>");

            for (int i = 0; i < evt_list.Count; i++)
            {
                var evt = evt_list[i];

                string site_name = evt.Value.getProperty("site_name", "");
                string in_tree_no = evt.Value.getProperty("in_tree_no", "");
                string in_win_status = evt.Value.getProperty("in_win_status", "");
                string in_win_local_time = evt.Value.getProperty("in_win_local_time", "");

                site_name = site_name.Replace("第", "").Replace("場地", "");
                if (in_win_status == "cancel") in_tree_no += "(C)";


                body.AppendLine("<tr>");
                body.AppendLine("    <td class='text-center'>" + site_name + "</td>");
                body.AppendLine("    <td class='text-center'>" + in_tree_no + "</td>");

                body.AppendLine("    <td class='text-center " + f1c + "'>" + evt.Foot1.display_no + "</td>");
                body.AppendLine("    <td class='text-center " + f1c + "'>" + evt.Foot1.display_name + "</td>");
                body.AppendLine("    <td class='text-center " + f1c + "'>" + evt.Foot1.score_i + "</td>");
                body.AppendLine("    <td class='text-center " + f1c + "'>" + evt.Foot1.score_w + "</td>");
                body.AppendLine("    <td class='text-center " + f1c + "'>" + evt.Foot1.score_s + "</td>");
                body.AppendLine("    <td class='text-center " + f1c + "'>" + evt.Foot1.score_points + "</td>");

                body.AppendLine("    <td class='text-center'>" + in_win_local_time + "</td>");

                body.AppendLine("    <td class='text-center " + f2c + "'>" + evt.Foot2.score_points + "</td>");
                body.AppendLine("    <td class='text-center " + f2c + "'>" + evt.Foot2.score_i + "</td>");
                body.AppendLine("    <td class='text-center " + f2c + "'>" + evt.Foot2.score_w + "</td>");
                body.AppendLine("    <td class='text-center " + f2c + "'>" + evt.Foot2.score_s + "</td>");
                body.AppendLine("    <td class='text-center " + f2c + "'>" + evt.Foot2.display_name + "</td>");
                body.AppendLine("    <td class='text-center " + f2c + "'>" + evt.Foot2.display_no + "</td>");

                body.AppendLine("</tr>");
            }

            body.AppendLine("</tbody>");

            var table_name = "table_fight_" + program_id;

            builder.AppendLine("</div>");

            builder.AppendLine("<hr style='margin-top: 30px;' />");
            builder.AppendLine("<div class='main-header' style='margin-bottom: 10px; z-index: 100;'>");
            builder.AppendLine("  <h4>循環賽紀錄表 &emsp; <small><button class='btn btn-sm btn-primary' onclick='ShowRecordMessage()'><i class='fa fa-info'></i></button></small> </h4>");
            builder.AppendLine("</div>");

            builder.AppendLine("<div class='container' style='margin-top: 30px;'>");
            builder.AppendLine("<table id='" + table_name + "' class='table table-hover table-bordered table-rwd rwd rwdtable match-table'>");
            builder.Append(head);
            builder.Append(body);
            builder.AppendLine("</table>");
        }

        private void AppendCycelTable(TConfig cfg
            , Dictionary<string, TStatistics> map
            , List<TEvent> evt_list
            , Item itmProgram
            , Item itmPlayers
            , StringBuilder builder)
        {

            var program_id = itmProgram.getProperty("id", "");
            var player_map = ConvertMap(cfg.CCO, cfg.strMethodName, cfg.inn, itmPlayers, "in_sign_no");

            if (player_map.Count == 0 || evt_list.Count == 0)
            {
                return;
            }

            var itmPlayer1 = FindItem(cfg.inn, player_map, "1");
            var itmPlayer2 = FindItem(cfg.inn, player_map, "2");

            string f1_sign_no = itmPlayer1.getProperty("detail_sign_no", "");
            string f1_final_rank = itmPlayer1.getProperty("in_final_rank", "");
            TStatistics f1_statistics = FindStatistics(map, f1_sign_no);

            string f2_sign_no = itmPlayer2.getProperty("detail_sign_no", "");
            string f2_final_rank = itmPlayer2.getProperty("in_final_rank", "");
            TStatistics f2_statistics = FindStatistics(map, f2_sign_no);

            string m101 = GetEventLink(cfg, evt_list, "M101");
            string m201 = GetEventLink(cfg, evt_list, "M102");
            string m301 = GetEventLink(cfg, evt_list, "M103");


            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();


            head.AppendLine("<thead>");
            head.AppendLine("<tr>");
            head.AppendLine("<th class='text-center'>Draw</th>");
            head.AppendLine("<th class='text-center'>Player</th>");

            head.AppendLine("<th class='text-center'>" + GetCycleNameInfo(cfg, itmPlayer1, isAddNo: true) + "</th>");
            head.AppendLine("<th class='text-center'>" + GetCycleNameInfo(cfg, itmPlayer2, isAddNo: true) + "</th>");

            head.AppendLine("<th class='text-center'>One<BR>More</th>");
            head.AppendLine("<th class='text-center'>Win</th>");
            head.AppendLine("<th class='text-center'>Points</th>");
            head.AppendLine("<th class='text-center'>Results</th>");
            head.AppendLine("</tr>");
            head.AppendLine("</thead>");



            body.AppendLine("<tbody>");

            body.AppendLine("<tr>");
            body.AppendLine("<td class='text-center'>" + GetCycleSignNoInfo(itmPlayer1) + "</td>");
            body.AppendLine("<td class='text-center " + GetCheckClass(itmPlayer1) + "'>" + GetCycleNameInfo(cfg, itmPlayer1) + "</td>");
            body.AppendLine("<td class='text-center disable-evet'>&nbsp;</td>");
            body.AppendLine("<td class='text-center' style='vertical-align: middle;'>" + m101 + "</td>");
            body.AppendLine("<td class='text-center' data-title='加賽' rowspan='2' style='vertical-align: middle;'> " + m301 + " </td>");
            body.AppendLine("<td class='text-center' data-title='勝場'> " + f1_statistics.total_wins + " </td>");
            body.AppendLine("<td class='text-center' data-title='積分'> " + f1_statistics.total_points + " </td>");
            body.AppendLine("<td class='text-center' data-title='名次'> " + f1_final_rank + " </td>");
            body.AppendLine("</tr>");

            body.AppendLine("<tr>");
            body.AppendLine("<td class='text-center'>" + GetCycleSignNoInfo(itmPlayer2) + "</td>");
            body.AppendLine("<td class='text-center " + GetCheckClass(itmPlayer2) + "'>" + GetCycleNameInfo(cfg, itmPlayer2) + "</td>");
            body.AppendLine("<td class='text-center' style='vertical-align: middle;'>" + m201 + "</td>");
            body.AppendLine("<td class='text-center disable-evet'>&nbsp;</td>");
            body.AppendLine("<td class='text-center' data-title='勝場'> " + f2_statistics.total_wins + " </td>");
            body.AppendLine("<td class='text-center' data-title='積分'> " + f2_statistics.total_points + " </td>");
            body.AppendLine("<td class='text-center' data-title='名次'> " + f2_final_rank + " </td>");
            body.AppendLine("</tr>");


            body.AppendLine("</tbody>");


            var table_name = "table_" + program_id;

            builder.AppendLine("<table id='" + table_name + "' class='table table-hover table-bordered table-rwd rwd rwdtable match-table'>");
            builder.Append(head);
            builder.Append(body);
            builder.AppendLine("</table>");

            // builder.AppendLine("<script>");
            // builder.AppendLine("$('#" + table_name + "').bootstrapTable({});");

            // builder.AppendLine("if($(window).width() <= 768) {");
            // builder.AppendLine("    $('#" + table_name + "').bootstrapTable('toggleView');");
            // builder.AppendLine("}");

            // builder.AppendLine("</script>");

        }

        private string GetEventLink(TConfig cfg, List<TEvent> evt_list, string in_tree_id)
        {
            var evt = evt_list.Find(x => x.in_tree_id == in_tree_id);

            if (evt == null)
            {
                return "[&nbsp;]";
            }

            var entity = default(TDetail);
            if (evt.in_win_sign_no != "")
            {
                entity = GetBattleResult(evt);
            }
            else
            {
                entity = GetBattleInitial(evt);
            }

            var event_link = GetRowIdLink(cfg, evt, entity);

            return entity.event_div + "<br>" + event_link;
        }

        private TDetail GetBattleResult(TEvent evt)
        {
            TDetail result = new TDetail
            {
                event_div = "",
                score_div = "",
                btn_class = "",
            };

            string f1_sign_no = evt.itmFoot1.getProperty("detail_sign_no", "");
            string f2_sign_no = evt.itmFoot2.getProperty("detail_sign_no", "");

            if (f1_sign_no == evt.in_win_sign_no) f1_sign_no += "(win)";
            if (f2_sign_no == evt.in_win_sign_no) f2_sign_no += "(win)";

            result.btn_class = "event-btn2";
            result.event_div = "<span class='event-btn'>&nbsp;" + evt.in_tree_no + " </span>&nbsp;" + f1_sign_no + " vs " + f2_sign_no;
            result.score_div = evt.itmFoot1.getProperty("in_points", "0") + ":" + evt.itmFoot2.getProperty("in_points", "0");

            return result;
        }

        private TDetail GetBattleInitial(TEvent evt)
        {
            TDetail result = new TDetail
            {
                event_div = "",
                score_div = "",
                btn_class = "",
            };

            string f1_sign_no = evt.itmFoot1.getProperty("in_sign_no", "");
            string f2_sign_no = evt.itmFoot2.getProperty("in_sign_no", "");

            result.event_div = f1_sign_no + " vs " + f2_sign_no;
            result.score_div = evt.in_tree_no;

            return result;
        }

        private TStatistics FindStatistics(Dictionary<string, TStatistics> map, string in_sign_no)
        {
            if (map.ContainsKey(in_sign_no))
            {
                return map[in_sign_no];
            }
            else
            {
                return new TStatistics
                {
                    in_sign_no = in_sign_no,
                    total_points = 0,
                    total_wins = 0
                };
            }
        }


        private string GetRowIdLink(TConfig cfg, TEvent evt, TDetail detail)
        {
            if (GLOBAL_EVENT_EDIT)
            {
                return GetRowIdLinkEdit(cfg, evt, detail);
            }
            else
            {
                return GetRowIdLinkView(cfg, evt, detail);
            }
        }

        private string GetRowIdLinkView(TConfig cfg, TEvent evt, TDetail detail)
        {
            var current_class = detail.btn_class == "" ? "event-btn" : detail.btn_class;
            var val = "　";

            if (evt.TreeNo > 0)
            {
                val = "&nbsp;" + evt.in_tree_no + "&nbsp;";
            }
            else
            {
                current_class = "no-event-btn";
            }

            return "<a class='" + current_class + "' href='javascript:void(0)' onclick='Event_Click(this)'"
                    + " data-pid='" + cfg.program_id + "'"
                    + " data-eid='" + evt.Id + "'"
                    + " data-rid='" + evt.in_tree_id + "'"
                    + " data-scd='" + evt.site_code + "'"
                    + " >" + val + "</a>";
        }

        private string GetRowIdLinkEdit(TConfig cfg, TEvent evt, TDetail detail)
        {
            if (evt.TreeNo > 0)
            {
                return "<input type='text' value='" + evt.in_tree_no + "' class='event-input'"
                + " data-pid='" + cfg.program_id + "' data-rid='" + evt.in_tree_id + "' data-old='" + evt.in_tree_no + "'"
                + " onkeyup='Event_KeyUp(this)'"
                + " ondblclick='Event_Click(this)'"
                + " >";
            }
            else
            {
                return "<input type='text' value='NA' class='event-input'"
                + " data-pid='" + cfg.program_id + "' data-rid='" + evt.in_tree_id + "' data-old='" + evt.in_tree_no + "'"
                + " onkeyup='Event_KeyUp(this)'"
                + " ondblclick='Event_Click(this)'"
                + " >";
            }
        }

        private string GetCheckClass(Item itmPlayer)
        {
            string in_check_result = itmPlayer.getProperty("in_check_result", "");

            switch (in_check_result)
            {
                case "1": return "bg-success";
                case "0": return "bg-danger";
                default: return "";
            }
        }

        private string GetCycleSignNoInfo(Item item)
        {
            return item.getProperty("in_sign_no", "");
        }

        private string GetCycleNameInfo(TConfig cfg, Item item, bool isAddNo = false)
        {
            string in_l1 = item.getProperty("in_l1", "");
            string in_name = item.getProperty("in_name", "");
            string no = item.getProperty("detail_sign_no", "");
            string map_short_org = item.getProperty("map_short_org", "");

            string org = "<span class='rank-org'>" + map_short_org + "</span>";

            if (in_l1 == "團體組")
            {
                if (in_name == "")
                {
                    return org;
                }
                else
                {
                    return org + "(" + in_name + ")";
                }
            }
            else if (isAddNo)
            {
                if (cfg.is_english)
                {
                    return no + "<br>" + in_name + "<br>" + org;
                }
                else
                {
                    return no + "<br>" + org + "<br>" + in_name;
                }

            }
            else
            {
                if (cfg.is_english)
                {
                    return in_name + "<br>" + org;
                }
                else
                {
                    return org + "<br>" + in_name;
                }
            }
        }

        /// <summary>
        /// 轉換為字典
        /// </summary>
        private Dictionary<string, Item> ConvertMap(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item items, string property)
        {
            Dictionary<string, Item> map = new Dictionary<string, Item>();

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string key = item.getProperty(property, "");

                if (map.ContainsKey(key))
                {
                    //throw new Exception("鍵值重覆");
                }
                else
                {
                    map.Add(key, item);
                }
            }
            return map;
        }
        /// <summary>
        /// 轉換為字典
        /// </summary>
        private Dictionary<string, List<Item>> ConvertMap2(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item items, string property)
        {
            Dictionary<string, List<Item>> map = new Dictionary<string, List<Item>>();

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string key = item.getProperty(property, "");

                List<Item> list = null;
                if (map.ContainsKey(key))
                {
                    list = map[key];
                }
                else
                {
                    list = new List<Item>();
                    map.Add(key, list);
                }
                list.Add(item);
            }
            return map;
        }

        /// <summary>
        /// 找出項目
        /// </summary>
        private Item FindItem(Innovator inn, Dictionary<string, Item> map, string key)
        {
            if (map.ContainsKey(key))
            {
                return map[key];
            }
            else
            {
                return inn.newItem();
            }
        }

        /// <summary>
        /// 找出項目清單
        /// </summary>
        private List<Item> FindItems(Innovator inn, Dictionary<string, List<Item>> map, string key)
        {
            if (map.ContainsKey(key))
            {
                return map[key];
            }
            else
            {
                List<Item> list = new List<Item>();
                list.Add(inn.newItem());
                list.Add(inn.newItem());
                return list;
            }
        }

        /// <summary>
        /// 取得單循環對戰鍵值
        /// </summary>
        private string GetSingleCycleKey(int y, int x)
        {
            if (y < x)
            {
                return y + "," + x;
            }
            else
            {
                return x + "," + y;
            }
        }

        /// <summary>
        /// 取得雙循環對戰鍵值
        /// </summary>
        private string GetDoubleCycleKey(int y, int x)
        {
            return y + "," + x;
        }

        private List<TEvent> GetEventMap(TConfig cfg)
        {
            string condition = "t1.source_id = '" + cfg.program_id + "' AND t1.in_tree_name = 'main'";

            string sql = @"
                SELECT 
                    t1.id
                    , t1.in_tree_name
                    , t1.in_tree_id
                    , t1.in_tree_no
                    , t1.in_site_id
                    , t1.in_win_sign_no
                    , t1.in_win_status
                    , t1.in_win_time
                    , t1.in_win_local_time
                    , t2.in_sign_foot
                    , t2.in_sign_no         AS 'detail_sign_no'
                    , t2.in_status
                    , t2.in_points
                    , t2.in_correct_count
                    , t11.map_short_org     AS 'player_org'
                    , t11.in_name           AS 'player_name'
                    , t21.in_name           AS 'site_name'
                    , t21.in_code           AS 'site_code'
                FROM 
                    IN_MEETING_PEVENT t1 WITH(NOLOCK)
                LEFT OUTER JOIN
                    IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK)
                    ON t2.source_id = t1.id
                LEFT OUTER JOIN
                    VU_MEETING_PTEAM t11 WITH(NOLOCK)
                    ON t11.source_id = t1.source_id
                    AND t11.in_sign_no = t2.in_sign_no
                LEFT OUTER JOIN
                    IN_MEETING_SITE t21 WITH(NOLOCK)
                    ON t21.id = t1.in_site
                WHERE 
                    {#condition}
                ORDER BY
                    t1.in_tree_sort
                    , t1.in_tree_id
            ";

            sql = sql.Replace("{#condition}", condition);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item itmEventDetails = cfg.inn.applySQL(sql);

            List<TEvent> evts = MapEvent(cfg, itmEventDetails);

            return evts;
        }

        private List<TEvent> MapEvent(TConfig cfg, Item items)
        {
            List<TEvent> list = new List<TEvent>();
            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string id = item.getProperty("id", "");
                string in_sign_foot = item.getProperty("in_sign_foot", "");

                TEvent evt = list.Find(x => x.Id == id);

                if (evt == null)
                {
                    evt = new TEvent
                    {
                        Id = id,
                        Value = item,
                        in_tree_id = item.getProperty("in_tree_id", ""),
                        in_tree_no = item.getProperty("in_tree_no", ""),
                        in_win_sign_no = item.getProperty("in_win_sign_no", ""),
                        site_code = item.getProperty("site_code", "0"),
                    };

                    evt.TreeNo = GetIntVal(evt.in_tree_no);

                    list.Add(evt);
                }

                if (in_sign_foot == "1")
                {
                    evt.itmFoot1 = item;
                }
                else if (in_sign_foot == "2")
                {
                    evt.itmFoot2 = item;
                }
            }

            foreach (var evt in list)
            {
                if (evt.itmFoot1 == null) evt.itmFoot1 = cfg.inn.newItem();
                if (evt.itmFoot2 == null) evt.itmFoot2 = cfg.inn.newItem();

                evt.Foot1 = MapFoot(cfg, evt.itmFoot1);
                evt.Foot2 = MapFoot(cfg, evt.itmFoot2, true);
            }

            return list;
        }

        //取得選手
        private Item GetPlayers(TConfig cfg)
        {
            string sql = @"
                SELECT
                    t1.in_sign_no
                    , t1.in_section_no    AS 'detail_sign_no'
                    , t1.in_check_result
                    , t1.in_current_org
                    , t1.map_short_org
                    , t1.in_name
                    , t1.in_sno
                    , t1.in_final_rank
                    , t2.in_l1
                FROM
                    VU_MEETING_PTEAM t1 WITH(NOLOCK)
                INNER JOIN
                    IN_MEETING_PROGRAM t2 WITH(NOLOCK)
                    ON t2.id = t1.source_id
                WHERE
                    t1.source_id = '{#program_id}'
                    --AND ISNULL(t1.in_sign_no, '') <> ''
                ORDER BY
                    CAST(ISNULL(t1.in_sign_no, '0') AS INT)
            ";

            sql = sql.Replace("{#program_id}", cfg.program_id);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        //取得賽事組別場次
        private Item GetEventPlayers(TConfig cfg)
        {
            string sql = @"
                SELECT
                    t1.id                       AS 'event_id'
                    , t1.in_tree_name
                    , t1.in_tree_id
                    , t1.in_tree_no
                    , t1.in_round
                    , t1.in_round_id
                    , t1.in_round_code
                    , t1.in_next_win
                    , t1.in_next_foot_win
                    , t1.in_next_lose
                    , t1.in_next_foot_lose
                    , t1.in_detail_ns
                    , t1.in_win_sign_no
                    , t1.in_win_status
                    , t1.in_win_local_time
                    , t2.id                     AS 'detail_id'
                    , t2.in_sign_foot
                    , t2.in_sign_no
                    , t2.in_sign_bypass
                    , t2.in_target_no
                    , t2.in_status
                    , t2.in_points
                    , t2.in_points_type
                    , t2.in_correct_count
                    , t3.in_name                AS 'site_name'
                    , t11.id                    AS 'mdid'
                    , t11.in_current_org
                    , t11.in_short_org
                    , t11.map_short_org
                    , t11.in_name
                    , t11.in_names
                    , t11.in_sno
                    , t11.in_org_teams
                    , t11.in_section_no         AS 'detail_sign_no'
                    , t11.in_seeds
                    , t11.in_check_result
                FROM 
                    IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
                    IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK)
                    ON t2.source_id = t1.id
                LEFT OUTER JOIN
                    IN_MEETING_SITE t3 WITH(NOLOCK)
                    ON t3.id = t1.in_site
                LEFT OUTER JOIN
                    VU_MEETING_PTEAM t11 WITH(NOLOCK)
                    ON t11.source_id = t1.source_id
                    AND ISNULL(t11.in_sign_no, '') <> ''
                    AND t11.in_sign_no = t2.in_sign_no
                WHERE
                    t1.source_id = '{#program_id}'
                    AND t1.in_tree_name = 'main'
                ORDER BY
                    t1.in_tree_sort
                    , t1.in_round
                    , t1.in_round_id
                    , t2.in_sign_foot
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#program_id}", cfg.program_id);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        private class TStatistics
        {
            /// <summary>
            /// 籤號
            /// </summary>
            public string in_sign_no { get; set; }

            /// <summary>
            /// 總積分
            /// </summary>
            public int total_points { get; set; }

            /// <summary>
            /// 總勝場
            /// </summary>
            public int total_wins { get; set; }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public Innosoft.InnovatorHelper InnH { get; set; }

            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }

            public string meeting_id { get; set; }
            public string program_id { get; set; }
            public string eno { get; set; }
            public bool is_english { get; set; }
        }

        private class TEvent
        {
            public string Id { get; set; }
            public Item Value { get; set; }
            public Item itmFoot1 { get; set; }
            public Item itmFoot2 { get; set; }
            public string in_tree_id { get; set; }
            public string in_tree_no { get; set; }
            public string in_win_sign_no { get; set; }
            public string site_code { get; set; }

            public int TreeNo { get; set; }
            public TFoot Foot1 { get; set; }
            public TFoot Foot2 { get; set; }
        }

        private class TDetail
        {
            public string event_div { get; set; }
            public string score_div { get; set; }
            public string btn_class { get; set; }
        }


        private class TCycleModel
        {
            public string program_id { get; set; }
            public string in_battle_type { get; set; }
            public bool is_double { get; set; }
            public Dictionary<string, Item> player_map { get; set; }
            public Dictionary<string, List<Item>> event_map { get; set; }
            public int team_count { get; set; }
            public string row_title { get; set; }
            public Item itmPlayers { get; set; }
            public Item itmEvents { get; set; }
        }

        private TFoot MapFoot(TConfig cfg, Item item, bool is_foot2 = false)
        {
            var result = new TFoot();

            string no = item.getProperty("detail_sign_no", "");
            string org = item.getProperty("player_org", "");
            string name = item.getProperty("player_name", "");
            string in_status = item.getProperty("in_status", "");
            string in_points = item.getProperty("in_points", "");
            string in_correct_count = item.getProperty("in_correct_count", "0");
            string in_win_time = item.getProperty("in_win_time", "");

            if (in_points.Length == 2)
            {
                result.score_i = in_points[0].ToString();
                result.score_w = in_points[1].ToString();
            }
            else if (in_points.Length == 1)
            {
                result.score_i = "";
                result.score_w = in_points;
            }
            else
            {
                result.score_i = "";
                result.score_w = "";
            }

            result.display_no = no;

            result.display_name = is_foot2
                ? name + "<br><span class='rank-org inn-right'>" + org + "</span>"
                : name + "<br><span class='rank-org'>" + org + "</span>";

            if (in_status == "1")
            {
                result.display_name = "<i class='fa fa-check winner_icon'></i>" + result.display_name;
            }

            result.score_points = in_points;
            result.score_s = in_correct_count;

            if (in_win_time == "")
            {
                result.score_i = "";
                result.score_w = "";
                result.score_s = "";
                result.score_points = "";
            }

            return result;
        }

        private class TFoot
        {
            public string display_no { get; set; }
            public string display_name { get; set; }
            public string score_i { get; set; }
            public string score_w { get; set; }
            public string score_s { get; set; }
            public string score_points { get; set; }
        }


        private int GetIntVal(string value, int def = 0)
        {
            if (value == "") return def;
            int result = def;
            int.TryParse(value, out result);
            return result;
        }
    }
}