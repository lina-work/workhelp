﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Judo.Fight
{
    public class In_Meeting_Program_Upload : Item
    {
        public In_Meeting_Program_Upload(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 上傳賽程資料
                日誌: 
                    - 2022-11-15: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]" + "In_Meeting_Program_Upload";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                strIdentityId = inn.getUserAliases(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                program_id = itmR.getProperty("program_id", ""),
                in_fight_day = itmR.getProperty("in_fight_day", ""),
                scene = itmR.getProperty("scene", ""),
            };

            SetFileCtrl(cfg);

            switch (cfg.scene)
            {
                case "site_page":
                    cfg.is_list_mode = false;
                    TabPage(cfg, itmR);
                    break;

                case "list_page":
                    cfg.is_list_mode = true;
                    TabPage(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void AppendMeetingInfo(TConfig cfg, Item itmReturn)
        {
            Item itmMeeting = cfg.inn.applySQL("SELECT in_title FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            itmReturn.setProperty("in_title", itmMeeting.getProperty("in_title", ""));
        }

        //設定頁面
        private void TabPage(TConfig cfg, Item itmReturn)
        {
            AppendMeetingInfo(cfg, itmReturn);
            AppendFightDayMenu(cfg, itmReturn);

            var itmPrograms = FightProgramItems(cfg);
            var rows = GetPrograms(cfg, itmPrograms);

            var page = MapPageModel(cfg, rows);

            var contens = new StringBuilder();
            contens.Append(SiteTabHead(cfg, page));
            contens.Append(SiteTabBody(cfg, page));
            itmReturn.setProperty("inn_tabs", contens.ToString());
        }

        private StringBuilder SiteTabHead(TConfig cfg, TPage page)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("<ul class='nav nav-pills' style='justify-content: start;' id='pills-tab' role='tablist'>");
            foreach (var tab in page.tabs)
            {
                builder.Append("<li id='" + tab.li_id + "' class='nav-item " + tab.active1 + "' >");
                builder.Append(TabAnchor(cfg, tab));
                builder.Append("</li>");
            }
            builder.Append("</ul>");
            return builder;
        }

        private StringBuilder SiteTabBody(TConfig cfg, TPage page)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("<div class='tab-content' id='pills-tabContent' style='margin-top: -9px'>");
            foreach (var tab in page.tabs)
            {
                builder.Append("  <div class='tab-pane " + tab.active2 + "' id='" + tab.content_id + "' role='tabpanel'>");
                builder.Append("    <div class='showsheet_ clearfix'>");
                builder.Append("      <div id='" + tab.folder_id + "'>");
                builder.Append(SiteTabTable(cfg, page, tab));
                builder.Append("      </div>");
                builder.Append("    </div>");
                builder.Append("  </div>");
            }
            builder.Append("</div>");
            return builder;
        }

        private StringBuilder SiteTabTable(TConfig cfg, TPage page, TTab tab)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(TabTable(cfg, tab.table_id));
            builder.Append("<thead>");
            builder.Append("  <tr>");
            builder.Append("    <th class='text-center' data-width='5%'>No</th>");
            builder.Append("    <th class='text-center' data-width='15%'>量級</th>");
            builder.Append("    <th class='text-center' data-width='10%'>場地</th>");
            builder.Append("    <th class='text-center' data-width='20%'>過磅單</th>");
            builder.Append("    <th class='text-center' data-width='20%'>賽前賽程</th>");
            builder.Append("    <th class='text-center' data-width='20%'>賽後賽程</th>");
            builder.Append("    <th class='text-center' data-width='10%'>功能</th>");
            builder.Append("  </tr>");
            builder.Append("</thead>");

            builder.Append("<tbody>");
            int no = 0;
            foreach (var row in tab.rows)
            {
                no++;
                var nm = ProgramInfo(cfg, row);
                builder.Append("<tr data-id='" + row.id + "' data-nm='" + nm + "'>");
                builder.Append("    <td class='text-center'>" + no + "</th>");
                builder.Append("    <td class='text-left'>" + ProgramLink(cfg, row.id, nm) + "</th>");
                builder.Append("    <td class='text-center'>" + SiteMat(cfg, row) + "</th>");
                builder.Append("    <td class='text-center'>" + FileInfo(cfg, row, "in_file1") + "</th>");
                builder.Append("    <td class='text-center'>" + FileInfo(cfg, row, "in_file2") + "</th>");
                builder.Append("    <td class='text-center'>" + FileInfo(cfg, row, "in_file3") + "</th>");
                builder.Append("    <td class='text-center'>" + FileFunc(cfg, row) + "</th>");
                builder.Append("</tr>");
            }
            builder.Append("</tbody>");

            builder.Append("</table>");
            return builder;
        }

        private string FileInfo(TConfig cfg, TProgram row, string prop)
        {
            var dic = new Dictionary<string, string>();
            dic.Add("{#pid}", row.id);
            dic.Add("{#prop_name}", prop);
            dic.Add("{#prop_value}", row.value.getProperty(prop, ""));
            return GetFileCtrl(cfg.FileCtrl, dic);
        }

        private string SiteMat(TConfig cfg, TProgram row)
        {
            return row.value.getProperty("in_site_mat", "");
        }

        private string ProgramLink(TConfig cfg, string id, string name)
        {
            string url = "c.aspx?page=in_competition_preview.html"
                + "&method=in_meeting_program_preview"
                + "&meeting_id=" + cfg.meeting_id
                + "&program_id=" + id
                ;
            return "<a target='_blank' href='" + url + "'>" + name + "</a>";
        }

        private string FileFunc(TConfig cfg, TProgram row)
        {
            return "<button class='btn btn-primary' onclick='SaveFiles_Click(this)'>儲　存</th>";
        }

        private string ProgramInfo(TConfig cfg, TProgram row)
        {
            var itmProgram = row.value;
            string in_l1 = itmProgram.getProperty("in_l1", "");
            string in_name2 = itmProgram.getProperty("in_name2", "");
            string in_weight = itmProgram.getProperty("in_weight", "");
            string in_team_count = itmProgram.getProperty("in_team_count", "");

            if (in_l1 == "團體賽")
            {
                return in_name2 + "(" + in_team_count + ")";
            }
            else
            {
                return in_name2 + " " + in_weight + "(" + in_team_count + ")";
            }
        }

        private string TabTable(TConfig cfg, string table_id)
        {
            return "<table id='" + table_id + "'"
                + " class='table table-hover table-bordered table-rwd rwd rwdtable'"
                + " style='background-color: #fff;' data-toggle='table'"
                + " data-click-to-select='true' data-striped='false'"
                + " data-search='false' data-pagination='false'>";
        }

        private string TabAnchor(TConfig cfg, TTab tab)
        {
            return "<a id='" + tab.a_id + "'"
                + " href='#" + tab.content_id + "'"
                + " class='nav-link inn_nav'"
                + " role='tab'"
                + " data-toggle='pill'"
                + " aria-controls='pills-home'"
                + " aria-selected='true'"
                + " onclick='StoreTab_Click(this)'>"
                + tab.title
                + "</a>";
        }

        private TPage MapPageModel(TConfig cfg, List<TProgram> rows)
        {
            var result = new TPage
            {
                tabs = new List<TTab>(),
            };

            for (var i = 0; i < rows.Count; i++)
            {
                var row = rows[i];
                var tab = cfg.is_list_mode
                    ? result.tabs.Find(x => x.key == row.key_list)
                    : result.tabs.Find(x => x.code == row.key_site);

                if (tab == null)
                {
                    tab = cfg.is_list_mode
                        ? NewTab_List(cfg, row, result.tabs.Count + 1)
                        : NewTab_Site(cfg, row, result.tabs.Count + 1);

                    if (result.tabs.Count == 0)
                    {
                        tab.active1 = "active";
                        tab.active2 = "fade in active";
                    }
                    result.tabs.Add(tab);
                }
                tab.rows.Add(row);
            }

            return result;
        }

        private TTab NewTab_Site(TConfig cfg, TProgram row, int idx)
        {
            var id = "site_tab_" + idx;

            return new TTab
            {
                code = row.key_site,
                key = row.key_list,
                site_code = row.key_site.ToString(),
                active1 = "",
                active2 = "",
                title = "第" + GetSiteName(row.key_site) + "場地",
                li_id = "li_" + id,
                a_id = "a_" + id,
                content_id = "box_" + id,
                folder_id = "folder_" + id,
                toggle_id = "toggle_" + id,
                table_id = "table_" + id,
                rows = new List<TProgram>(),
            };
        }

        private TTab NewTab_List(TConfig cfg, TProgram row, int idx)
        {
            var id = "site_tab_" + idx;

            return new TTab
            {
                code = idx,
                key = row.key_list,
                active1 = "",
                active2 = "",
                title = row.key_list,
                li_id = "li_" + id,
                a_id = "a_" + id,
                content_id = "box_" + id,
                folder_id = "folder_" + id,
                toggle_id = "toggle_" + id,
                table_id = "table_" + id,
                rows = new List<TProgram>(),
            };
        }

        private string GetSiteName(int code)
        {
            return GetSiteName(code.ToString());
        }

        private string GetSiteName(string code)
        {
            switch (code)
            {
                case "1": return "一";
                case "2": return "二";
                case "3": return "三";
                case "4": return "四";
                case "5": return "五";
                case "6": return "六";
                case "7": return "七";
                case "8": return "八";
                case "9": return "九";
                case "10": return "十";
                default: return "";
            }
        }

        private List<TProgram> GetPrograms(TConfig cfg, Item items)
        {
            var result = new List<TProgram>();
            var count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string id = item.getProperty("id", "");
                string in_l1 = item.getProperty("in_l1", "");
                string in_l2 = item.getProperty("in_l2", "");
                string in_short_name = item.getProperty("in_short_name", "");
                string in_battle_type = item.getProperty("in_battle_type", "");
                string site_code = item.getProperty("site_code", "");
                string in_end_tree_no = item.getProperty("in_end_tree_no", "");

                var row = new TProgram
                {
                    id = id,
                    in_l1 = in_l1,
                    in_l2 = in_l2,
                    in_short_name = in_short_name,
                    value = item,
                };

                row.is_team = in_l1 == "團體組";
                row.is_robin = in_battle_type == "SingleRoundRobin";

                row.key_site = GetIntVal(site_code);
                row.key_list = GetAgeGender(cfg, row);

                result.Add(row);
            }
            return result;
        }

        private string GetAgeGender(TConfig cfg, TProgram row)
        {
            if (row.is_team)
            {
                return row.in_l1;
            }
            else
            {
                return row.in_short_name.Length == 4
                    ? row.in_short_name.Substring(0, 3)
                    : row.in_short_name.Substring(0, 2);
            }
        }

        private Dictionary<string, List<Item>> GetEventMap(TConfig cfg, List<Item> items)
        {
            var result = new Dictionary<string, List<Item>>();

            for (int i = 0; i < items.Count; i++)
            {
                var item = items[i];
                var key = item.getProperty("source_id", "");

                var list = default(List<Item>);
                if (result.ContainsKey(key))
                {
                    list = result[key];
                }
                else
                {
                    list = new List<Item>();
                    result.Add(key, list);
                }
                list.Add(item);
            }
            return result;
        }

        private void AppendFightDayMenu(TConfig cfg, Item itmReturn)
        {
            var items = FightDayItems(cfg);
            var count = items.getItemCount();

            var first = "";
            var today = DateTime.Now.ToString("yyyy-MM-dd");
            var has_exist = false;
            var has_today = false;

            var itmEmpty = cfg.inn.newItem();
            itmEmpty.setType("inn_day");
            itmEmpty.setProperty("value", "");
            itmEmpty.setProperty("text", "請選擇");
            itmReturn.addRelationship(itmEmpty);

            for (int i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var in_fight_day = item.getProperty("in_fight_day", "");

                if (i == 0)
                {
                    first = in_fight_day;
                }

                if (today == in_fight_day)
                {
                    has_today = true;
                }

                if (cfg.in_fight_day == in_fight_day)
                {
                    has_exist = true;
                }

                item.setType("inn_day");
                item.setProperty("value", in_fight_day);
                item.setProperty("text", in_fight_day);
                itmReturn.addRelationship(item);
            }

            if (cfg.is_list_mode)
            {
                //允許無比賽日期
                return;
            }

            if (count > 0 && (cfg.in_fight_day == "" || !has_exist))
            {
                if (has_today)
                {
                    cfg.in_fight_day = today;
                }
                else
                {
                    cfg.in_fight_day = first;
                }
                itmReturn.setProperty("in_fight_day", cfg.in_fight_day);
            }
        }

        private Item FightProgramItems(TConfig cfg)
        {
            string dayCond = cfg.in_fight_day == ""
                ? ""
                : "AND t1.in_fight_day = '" + cfg.in_fight_day + "'";

            string orderBy = cfg.is_list_mode
                ? "t1.in_sort_order"
                : "t3.in_code, t2.created_on";

            string sql = @"
                SELECT
	                t1.id
	                , t1.in_l1
	                , t1.in_l2
	                , t1.in_l3
	                , t1.in_name2
	                , t1.in_short_name
	                , t1.in_weight
	                , t1.in_team_count
	                , t1.in_battle_type
	                , t1.in_site_mat
                    , t1.in_file1
                    , t1.in_file2
                    , t1.in_file3
	                , t3.in_code AS 'site_code'
	                , t3.in_name AS 'site_name'
                FROM
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_ALLOCATION t2 WITH(NOLOCK)
	                ON t2.in_program = t1.id
                INNER JOIN
	                IN_MEETING_SITE t3 WITH(NOLOCK)
	                ON t3.id = t2.in_site
                WHERE
	                t1.in_meeting = '{#meeting_id}'
	                AND t1.in_l1 NOT IN (N'格式組')
	                {#dayCond}
	                AND t1.in_team_count > 1
                ORDER BY
	                {#orderBy}
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#dayCond}", dayCond)
                .Replace("{#orderBy}", orderBy);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        private Item FightDayItems(TConfig cfg)
        {
            string sql = @"
                SELECT DISTINCT
	                in_fight_day 
                FROM 
	                IN_MEETING_PROGRAM WITH(NOLOCK) 
                WHERE 
	                in_meeting = '{#meeting_id}' 
	                AND ISNULL(in_fight_day, '') <> ''
                ORDER BY 
	                in_fight_day
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }

            public string meeting_id { get; set; }
            public string program_id { get; set; }
            public string in_fight_day { get; set; }
            public string scene { get; set; }

            public bool is_list_mode { get; set; }

            public string FileCtrl { get; set; }
        }

        private void SetFileCtrl(TConfig cfg)
        {
            var builder = new StringBuilder();

            builder.Append("<div class='inn_func_col'>");
            builder.Append("<div class='portrait in_input' ");
            builder.Append("	data-item='In_Meeting_Program'");
            builder.Append("	data-id='{#pid}'");
            builder.Append("	data-prop='{#prop_name}'");
            builder.Append("	id='{#pid}_{#prop_name}'>");
            builder.Append("	<img class='in_thumbnail' onclick='openPhotoSwipe2(this)' />");
            builder.Append("</div>");
            builder.Append("<button class='btn btn-default' onclick='SelectFile(this)'");
            builder.Append("	data-prop='{#prop_name}'>");
            builder.Append("	選擇");
            builder.Append("</button>");
            builder.Append("</div>");
            cfg.FileCtrl = builder.ToString();
        }

        private string GetFileCtrl(string template, Dictionary<string, string> values)
        {
            var builder = new StringBuilder(template, template.Length * 2);

            foreach (string k in values.Keys)
            {
                builder.Replace(k, values[k]);
            }

            return builder.ToString();
        }

        private class TPage
        {
            public List<TTab> tabs { get; set; }
        }

        private class TTab
        {
            public int code { get; set; }
            public string key { get; set; }
            public string site_code { get; set; }

            public string id { get; set; }
            public string active1 { get; set; }
            public string active2 { get; set; }
            public string title { get; set; }
            public string groups { get; set; }

            public string li_id { get; set; }
            public string a_id { get; set; }
            public string content_id { get; set; }
            public string folder_id { get; set; }
            public string toggle_id { get; set; }
            public string table_id { get; set; }

            public List<TProgram> rows { get; set; }
        }

        private class TProgram
        {
            public string id { get; set; }
            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_short_name { get; set; }

            public bool is_team { get; set; }
            public bool is_robin { get; set; }
            public int key_site { get; set; }
            public string key_list { get; set; }

            public Item value { get; set; }
        }

        private string DtmStr(string value, string format = "HH:mm")
        {
            if (value == "") return "";

            var dt = GetDtm(value);
            if (dt == DateTime.MinValue) return "";

            return dt.ToString(format);
        }

        private DateTime GetDtm(string value)
        {
            if (value == "") return DateTime.MinValue;

            DateTime result = DateTime.MinValue;
            if (DateTime.TryParse(value, out result))
            {
                return result;
            }
            return DateTime.MinValue;
        }

        private int GetIntVal(string value, int def = 0)
        {
            int result = def;
            Int32.TryParse(value, out result);
            return result;
        }
    }
}