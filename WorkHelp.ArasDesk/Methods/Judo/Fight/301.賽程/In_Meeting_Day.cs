﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Judo.Fight
{
    public class In_Meeting_Day : Item
    {
        public In_Meeting_Day(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 比賽日期
                日誌: 
                    - 2023-03-24: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]" + "In_Meeting_Day";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                strIdentityId = inn.getUserAliases(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                scene = itmR.getProperty("scene", ""),
            };

            switch (cfg.scene)
            {
                case "page":
                    Page(cfg, itmR);
                    break;

                case "edit_day":
                    EditDay(cfg, itmR);
                    break;

                case "batch_save":
                    BatchSave(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void BatchSave(TConfig cfg, Item itmReturn)
        {
            var data = itmReturn.getProperty("data", "");
            var rows = GetDayRowList(cfg, data);
            if (rows == null || rows.Count == 0)
            {
                throw new Exception("無資料");
            }

            var sql_del = "DELETE FROM In_Meeting_Day_Weight WHERE source_id IN (SELECT id FROM In_Meeting_Day WHERE in_meeting = '" + cfg.meeting_id + "')";
            cfg.inn.applySQL(sql_del);

            sql_del = "DELETE FROM IN_MEETING_ALLOCATION WHERE in_meeting = '" + cfg.meeting_id + "' AND in_type = 'category'";
            cfg.inn.applySQL(sql_del);

            foreach (var row in rows)
            {
                var itmMDay = GetMeetingDayItem(cfg, row.code);
                var itmMUser = GetMeetingUserItem(cfg, row.key);

                var itmNew = cfg.inn.newItem("In_Meeting_Day_Weight", "add");
                itmNew.setProperty("source_id", itmMDay.getProperty("id", ""));
                itmNew.setProperty("in_l1", itmMUser.getProperty("in_l1", ""));
                itmNew.setProperty("in_l2", itmMUser.getProperty("in_l2", ""));
                itmNew.setProperty("in_l3", itmMUser.getProperty("in_l3", ""));
                itmNew.setProperty("in_meeting", cfg.meeting_id);
                itmNew.apply();

                var in_day = itmMDay.getProperty("in_day", "");
                var fday = GetDtm(in_day);

                var itmNew2 = cfg.inn.newItem("IN_MEETING_ALLOCATION", "add");
                itmNew2.setProperty("in_meeting", cfg.meeting_id);
                itmNew2.setProperty("in_date_key", in_day);
                itmNew2.setProperty("in_date", fday.AddHours(-8).ToString("yyyy-MM-dd HH:mm:ss"));
                itmNew2.setProperty("in_type", "category");
                itmNew2.setProperty("in_l1", itmMUser.getProperty("in_l1", ""));
                itmNew2.setProperty("in_l2", itmMUser.getProperty("in_l2", ""));
                itmNew2.apply();
            }

        }

        private Item GetMeetingDayItem(TConfig cfg, string code)
        {
            var sql = "SELECT * FROM In_Meeting_Day WITH(NOLOCK) WHERE in_meeting = '" + cfg.meeting_id + "' AND in_code = '" + code + "'";
            return cfg.inn.applySQL(sql);
        }

        private Item GetMeetingUserItem(TConfig cfg, string key)
        {
            var sql = "SELECT DISTINCT in_l1, in_l2 FROM IN_MEETING_USER WITH(NOLOCK) WHERE source_id = '" + cfg.meeting_id + "' AND (in_l1 + '-' + in_l2) = '" + key + "'";
            return cfg.inn.applySQL(sql);
        }

        private List<TDayRow> GetDayRowList(TConfig cfg, string json)
        {
            try
            {
                return Newtonsoft.Json.JsonConvert.DeserializeObject<List<TDayRow>>(json);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private class TDayRow
        {
            public string code { get; set; }
            public string key { get; set; }
        }

        private void EditDay(TConfig cfg, Item itmReturn)
        {
            string code = itmReturn.getProperty("code", "");
            string text = itmReturn.getProperty("text", "");

            string sql = "UPDATE In_Meeting_Day SET"
                + "   in_day = '" + text + "'"
                + " WHERE in_meeting = '" + cfg.meeting_id + "'"
                + " AND in_code = '" + code + "'";

            cfg.inn.applySQL(sql);
        }

        //頁面
        private void Page(TConfig cfg, Item itmReturn)
        {
            var sql = "SELECT in_title FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'";
            var itmMeeting = cfg.inn.applySQL(sql);

            var in_days_json = Newtonsoft.Json.JsonConvert.SerializeObject(MapDayList(cfg));

            itmReturn.setProperty("in_title", itmMeeting.getProperty("in_title", ""));
            itmReturn.setProperty("in_days_json", in_days_json);
        }

        private List<TDay> MapDayList(TConfig cfg)
        {
            var result = new List<TDay>();

            //未設定名單
            AppendNoSelectWeight(cfg, result);

            var items = GetDayWeightItems(cfg);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var in_code = item.getProperty("in_code", "");

                var dobj = result.Find(x => x.in_code == in_code);
                if (dobj == null)
                {
                    dobj = new TDay
                    {
                        id = item.getProperty("id", ""),
                        in_code = item.getProperty("in_code", ""),
                        in_day = item.getProperty("in_day", ""),
                        in_name = item.getProperty("in_name", ""),
                        rows = new List<TRow>(),
                    };
                    result.Add(dobj);
                }

                dobj.rows.Add(MapRow(cfg, item));
            }

            return result;
        }

        private void AppendNoSelectWeight(TConfig cfg, List<TDay> list)
        {
            var eobj = new TDay { id = "", in_code = "0", in_day = "未設定", in_name = "未設定", rows = new List<TRow>() };
            var items = GetMUserWeightItems(cfg);

            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                eobj.rows.Add(MapRow(cfg, item));
            }

            list.Add(eobj);
        }

        private TRow MapRow(TConfig cfg, Item item)
        {
            var result = new TRow
            {
                in_l1 = item.getProperty("in_l1", ""),
                in_l2 = item.getProperty("in_l2", ""),
            };

            result.key = result.in_l1 + "-" + result.in_l2;

            return result;
        }

        private Item GetMUserWeightItems(TConfig cfg)
        {
            var sql = @"
                SELECT DISTINCT 
	                t1.in_l1
					, t1.in_l2
                FROM
	                IN_MEETING_USER t1 WITH(NOLOCK)
                WHERE
	                t1.source_id = '{#meeting_id}'
					AND in_l1 + '-' + in_l2 NOT IN (
						SELECT in_l1 + '-' + in_l2 FROM IN_MEETING_DAY_WEIGHT WITH(NOLOCK) WHERE in_meeting = '{#meeting_id}'
					)
				ORDER BY
					t1.in_l1
					, t1.in_l2
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);



            return cfg.inn.applySQL(sql);
        }

        private Item GetDayWeightItems(TConfig cfg)
        {
            var sql = @"
                SELECT
	                t1.id
	                , t1.in_code
	                , t1.in_name
	                , t1.in_day
	                , t2.in_l1
	                , t2.in_l2
                FROM
	                IN_MEETING_DAY t1 WITH(NOLOCK)
                LEFT OUTER JOIN
	                IN_MEETING_DAY_WEIGHT t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                WHERE
	                t1.in_meeting = '{#meeting_id}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            return cfg.inn.applySQL(sql);
        }

        private class TDay
        {
            public string id { get; set; }
            public string in_code { get; set; }
            public string in_name { get; set; }
            public string in_day { get; set; }
            public List<TRow> rows { get; set; }
        }

        private class TRow
        {
            public string key { get; set; }
            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }

            public string meeting_id { get; set; }
            public string scene { get; set; }
        }

        private DateTime GetDtm(string value)
        {
            var result = DateTime.MinValue;
            if (value == "") return result;
            DateTime.TryParse(value, out result);
            return result;
        }
    }
}