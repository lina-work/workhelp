﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Judo.Fight
{
    public class In_Meeting_Program_Mapping : Item
    {
        public In_Meeting_Program_Mapping(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 多重賽制
                日誌: 
                    - 2022-09-29: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]" + "In_Meeting_Program_Mapping";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                scene = itmR.getProperty("scene", ""),
            };

            switch (cfg.scene)
            {
                case "page":
                    Page(cfg, itmR);
                    break;

                case "some":
                    SomeSave(cfg, itmR);

                    //附加賽事組別資訊
                    Item itmSomePrograms = GetSomePrograms(cfg);
                    AppendPrograms(cfg, itmSomePrograms, itmR);
                    break;

                case "save":
                    Save(cfg, itmR);

                    //附加賽事組別資訊
                    Item itmPrograms = GetPrograms(cfg);
                    AppendPrograms(cfg, itmPrograms, itmR);
                    break;

                case "fix_name_and_weight":
                    FixNameAndWeight(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void FixNameAndWeight(TConfig cfg, Item itmReturn)
        {
            var in_l3_id = GetSurveyId(cfg, "in_l3");

            var sql = @"
                UPDATE t2 SET
                	in_short_name = t1.in_n1
                	, in_weight = t1.in_weight
                FROM 
                	IN_SURVEY_OPTION t1 WITH(NOLOCK)
                INNER JOIN
                	IN_MEETING_PROGRAM t2 WITH(NOLOCK)
                	ON t2.in_meeting = '{#meeting_id}'
                	AND t2.in_l1 = t1.in_grand_filter
                	AND t2.in_l2 = t1.in_filter
                	AND t2.in_l3 = t1.in_value
                WHERE 
                	t1.source_id = '{#svy_id}'
                	AND t1.in_grand_filter NOT IN (N'隊職員', N'格式組')

            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#svy_id}", in_l3_id);

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            cfg.inn.applySQL(sql);
        }

        //附加賽事組別資訊
        private void AppendPrograms(TConfig cfg, Item items, Item itmReturn)
        {
            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);

                string meeting_id = item.getProperty("in_meeting", "");
                string program_id = item.getProperty("id", "");
                string in_l1 = item.getProperty("in_l1", "");
                string team_link = " ";

                string in_battle_type = item.getProperty("in_battle_type", "");
                string battle_label = item.getProperty("battle_label", "");

                item.setType("inn_program");
                item.setProperty("no", (i + 1).ToString());
                itmReturn.addRelationship(item);
            }
        }

        //局部提交
        private void SomeSave(TConfig cfg, Item itmReturn)
        {
            Item itmMeeting = cfg.inn.applySQL("SELECT id, in_robin_player FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            if (itmMeeting.isError() || itmMeeting.getResult() == "")
            {
                throw new Exception("賽事資料錯誤");
            }

            cfg.in_l1_cond = "";
            cfg.in_l2_cond = "";

            string in_robin_player = itmMeeting.getProperty("in_robin_player", "3");
            string in_l1 = itmReturn.getProperty("in_l1", "");
            string values = itmReturn.getProperty("values", "");
            string battle_type = itmReturn.getProperty("battle_type", "");

            if (values == "")
            {
                throw new Exception("請設定組別資料");
            }
            if (battle_type == "")
            {
                throw new Exception("請設定賽制資料");
            }

            var arr = values.Replace("<BR>", ",")
                .Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            if (arr == null || arr.Length == 0)
            {
                throw new Exception("請設定組別資料");
            }

            var l2_list = arr.Select(x => "N'" + x + "'").ToList();
            var in_l2 = string.Join(", ", l2_list);

            cfg.in_l1_cond = in_l1;
            cfg.in_l2_cond = in_l2;

            //變更賽制
            string sql_update = "UPDATE IN_MEETING_PROGRAM SET"
                + " in_battle_type = '" + battle_type + "'"
                + " WHERE in_meeting = '" + cfg.meeting_id + "'"
                + " AND in_l1 = N'" + in_l1 + "'"
                + " AND in_l2 IN (" + in_l2 + ")"
                + " AND ISNULL(in_program, '') IN ('', 'p')"
                ;

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql_update);
            cfg.inn.applySQL(sql_update);

            //循環賽
            string sql_update2 = "UPDATE IN_MEETING_PROGRAM SET"
                + " in_battle_type = 'SingleRoundRobin'"
                + " WHERE in_meeting = '" + cfg.meeting_id + "'"
                + " AND in_l1 = N'" + in_l1 + "'"
                + " AND in_l2 IN (" + in_l2 + ")"
                + " AND ISNULL(in_program, '') IN ('', 'p')"
                + " AND ISNULL(in_team_count, 0) <= " + in_robin_player
                ;

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql_update2);
            cfg.inn.applySQL(sql_update2);
        }

        //提交
        private void Save(TConfig cfg, Item itmReturn)
        {
            string values = itmReturn.getProperty("values", "");
            if (values == "")
            {
                throw new Exception("請設定組別資料");
            }

            var ui_rows = MapRowsFromJson(cfg, values);
            if (ui_rows == null || ui_rows.Count == 0)
            {
                throw new Exception("請設定組別資料");
            }

            cfg.inn.applySQL("DELETE FROM In_Meeting_Program_Mapping WHERE in_meeting = '" + cfg.meeting_id + "'");

            var in_l2_id = GetSurveyId(cfg, "in_l2");
            var db_rows = MapRowsFromItem(cfg, GetSectionItems(cfg, in_l2_id));
            if (db_rows == null || db_rows.Count == 0)
            {
                throw new Exception("查無組別資料");
            }

            foreach (var row in db_rows)
            {
                var ui = ui_rows.Find(x => x.in_key == row.in_key);
                if (ui == null || string.IsNullOrWhiteSpace(ui.in_battle_type))
                {
                    continue;
                }

                row.in_battle_type = ui.in_battle_type;
                Item itmOld = cfg.inn.newItem("In_Meeting_Program_Mapping", "merge");
                itmOld.setAttribute("where", "in_meeting = '" + cfg.meeting_id + "' AND in_key = '" + row.in_key + "'");
                itmOld.setProperty("in_meeting", cfg.meeting_id);
                itmOld.setProperty("in_key", row.in_key);
                itmOld.setProperty("in_l1", row.in_l1);
                itmOld.setProperty("in_age", row.in_age);
                itmOld.setProperty("in_l2_list", string.Join(",", row.in_l2_list));
                itmOld.setProperty("in_l2_values", string.Join(",", row.in_l2_values));
                itmOld.setProperty("in_sort_order", row.in_sort_order.ToString());
                itmOld.setProperty("in_battle_type", row.in_battle_type);
                itmOld = itmOld.apply();
            }

            foreach (var row in db_rows)
            {
                if (row.in_l2_list == null || row.in_l2_list.Count == 0)
                {
                    continue;
                }

                var l2_list = row.in_l2_list.Select(x => "N'" + x + "'").ToList();
                var in_l2 = string.Join(", ", l2_list);

                string sql_update = "UPDATE IN_MEETING_PROGRAM SET"
                    + " in_battle_type = '" + row.in_battle_type + "'"
                    + " WHERE in_meeting = '" + cfg.meeting_id + "'"
                    + " AND in_l1 = N'" + row.in_l1 + "'"
                    + " AND in_l2 IN (" + in_l2 + ")"
                    + " AND ISNULL(in_program, '') = ''"
                    ;

                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql_update);

                cfg.inn.applySQL(sql_update);
            }

            //循環賽
            string sql_update2 = "UPDATE IN_MEETING_PROGRAM SET"
                + " in_battle_type = 'SingleRoundRobin'"
                + " WHERE in_meeting = '" + cfg.meeting_id + "'"
                + " AND ISNULL(in_program, '') = ''"
                + " AND ISNULL(in_team_count, 0) <= ISNULL(in_robin_player, 0)"
                ;

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql_update2);

            cfg.inn.applySQL(sql_update2);

        }

        //取得賽事組別資訊
        private Item GetSomePrograms(TConfig cfg)
        {
            string sql = @"
                SELECT 
                    t1.*
                    , t2.label AS 'battle_label' 
                FROM 
                    IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                LEFT OUTER JOIN
                    VU_Mt_Battle t2
                    ON t1.in_battle_type = t2.value
                WHERE 
                    t1.in_meeting = '{#meeting_id}'
                    AND t1.in_l1 = N'{#in_l1}'
                    AND t1.in_l2 IN ({#in_l2s})
                    AND ISNULL(t1.in_program, '') IN ('', 'p')
                    AND ISNULL(t1.in_battle_type, '') NOT LIKE 'Robin'
                ORDER BY
                    t1.in_sort_order
                ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_l1}", cfg.in_l1_cond)
                .Replace("{#in_l2s}", cfg.in_l2_cond);

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        //取得賽事組別資訊
        private Item GetPrograms(TConfig cfg)
        {
            string sql = @"
                SELECT 
                    t1.*
                    , t2.label AS 'battle_label' 
                FROM 
                    IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                LEFT OUTER JOIN
                    VU_Mt_Battle t2
                    ON t1.in_battle_type = t2.value
                WHERE 
                    t1.in_meeting = '{#meeting_id}'
                    AND ISNULL(t1.in_program, '') = ''
                ORDER BY
                    t1.in_sort_order
                ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        //頁面
        private void Page(TConfig cfg, Item itmReturn)
        {
            Item itmMeeting = cfg.inn.applySQL("SELECT in_title FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            itmReturn.setProperty("in_title", itmMeeting.getProperty("in_title", ""));

            //附加年齡分級清單
            string in_l2_id = GetSurveyId(cfg, "in_l2");
            Item itmSections = GetSectionItems(cfg, in_l2_id);
            AppendSections(cfg, itmSections, itmReturn);
        }

        //附加年齡分級清單
        private void AppendSections(TConfig cfg, Item items, Item itmReturn)
        {
            var rows = MapRowsFromItem(cfg, items);

            foreach (var row in rows)
            {
                Item item = cfg.inn.newItem();
                item.setType("inn_sect");
                item.setProperty("in_key", row.in_key);
                item.setProperty("in_l1", row.in_l1);
                item.setProperty("in_age", row.in_age);
                item.setProperty("in_l2_list", string.Join("<BR>", row.in_l2_list));
                item.setProperty("in_l2_values", string.Join("<BR>", row.in_l2_values));
                item.setProperty("in_sort_order", row.in_sort_order.ToString());
                itmReturn.addRelationship(item);
            }
        }

        private List<TRow> MapRowsFromItem(TConfig cfg, Item items)
        {
            var rows = new List<TRow>();
            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_filter = item.getProperty("in_filter", "");
                string in_value = item.getProperty("in_value", "");

                string value = in_value.Replace("團-", "")
                    .Replace("個-", "");

                string age = GetAge(cfg, value);
                string key = in_filter + "-" + age;

                var row = rows.Find(x => x.in_key == key);
                if (row == null)
                {
                    row = new TRow
                    {
                        in_key = key,
                        in_l1 = in_filter,
                        in_age = age,
                        in_l2_list = new List<string>(),
                        in_l2_values = new List<string>(),
                        in_sort_order = (rows.Count + 1) * 100,
                    };
                    rows.Add(row);
                }

                row.in_l2_list.Add(in_value);
                row.in_l2_values.Add(value);
            }

            return rows;
        }

        private string GetAge(TConfig cfg, string value)
        {
            if (value.Contains("特別組")) return "特別組";
            if (value.Contains("社會")) return "社會";
            if (value.Contains("大專")) return "大專";
            if (value.Contains("高中")) return "高中";
            if (value.Contains("國中")) return "國中";
            if (value.Contains("國小")) return "國小";
            return "ERROR";
        }

        private Item GetSectionItems(TConfig cfg, string survey_id)
        {
            string sql = @"
                SELECT 
	                in_filter
	                , in_label
	                , in_value 
                FROM 
	                IN_SURVEY_OPTION WITH(NOLOCK) 
                WHERE 
	                source_id = '{#survey_id}'
	                AND in_filter NOT IN (N'隊職員', N'格式組')
                ORDER BY
	                sort_order
            ";

            sql = sql.Replace("{#survey_id}", survey_id);

            Item items = cfg.inn.applySQL(sql);

            if (items.isError() || items.getResult() == "")
            {
                throw new Exception("查無競賽組別清單");
            }

            return items;
        }

        private string GetSurveyId(TConfig cfg, string in_property)
        {
            string sql = @"
                SELECT 
	                t2.id 
	                , t2.in_questions
                FROM 
	                IN_MEETING_SURVEYS t1 WITH(NOLOCK)
                INNER JOIN
	                IN_SURVEY t2 WITH(NOLOCK)
	                ON t2.id = t1.related_id
                WHERE
	                t1.source_id = '{#meeting_id}'
	                AND t2.in_property = '{#in_property}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_property}", in_property);

            Item item = cfg.inn.applySQL(sql);

            if (item.isError() || item.getResult() == "")
            {
                throw new Exception("查無競賽組別資料");
            }

            return item.getProperty("id", "");
        }

        private List<TRow> MapRowsFromJson(TConfig cfg, string json)
        {
            try
            {
                return Newtonsoft.Json.JsonConvert.DeserializeObject<List<TRow>>(json);
            }
            catch (Exception ex)
            {
                return new List<TRow>();
            }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }

            public string meeting_id { get; set; }
            public string scene { get; set; }

            public string in_l1_cond { get; set; }
            public string in_l2_cond { get; set; }
        }

        private class TRow
        {
            public string in_key { get; set; }
            public string in_l1 { get; set; }
            public string in_age { get; set; }
            public List<string> in_l2_list { get; set; }
            public List<string> in_l2_values { get; set; }
            public int in_sort_order { get; set; }

            public string in_battle_type { get; set; }
        }
    }
}