﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Judo.Fight
{
    public class In_Meeting_Variable : Item
    {
        public In_Meeting_Variable(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
               目的: 賽會參數設定
               日期: 
                   - 2022-03-23 創建 (Lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]In_Meeting_Variable";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, this.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                meeting_id = itmR.getProperty("meeting_id", ""),
                scene = itmR.getProperty("scene", ""),
            };

            switch (cfg.scene)
            {
                case "get":
                    Scalar(cfg, itmR);
                    break;

                case "fight_site":
                    ResetFightSite(cfg, itmR);
                    break;

                case "update":
                    Update(cfg, itmR);
                    break;

                default:
                    Page(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void Scalar(TConfig cfg, Item itmReturn)
        {
            string result = "";

            string name = itmReturn.getProperty("name", "");

            string sql = "SELECT in_value FROM In_Meeting_Variable WITH(NOLOCK)"
                + " WHERE source_id = '" + cfg.meeting_id + "'"
                + " AND in_key = '" + name + "'";

            Item itmData = cfg.inn.applySQL(sql);

            if (!itmData.isError() && itmData.getResult() != "")
            {
                result = itmData.getProperty("in_value", "");
            }

            itmReturn.setProperty("inn_result", result);
        }

        private void ResetFightSite(TConfig cfg, Item itmReturn)
        {
            string name = cfg.scene;

            Item itmData = cfg.inn.newItem("In_Meeting_Variable", "get");
            itmData.setProperty("source_id", cfg.meeting_id);
            itmData.setProperty("in_key", name);
            itmData = itmData.apply();

            if (itmData.isError() || itmData.getResult() == "")
            {
                throw new Exception("賽會參數取值發生錯誤");
            }

            string in_value = itmData.getProperty("in_value", "");

            string sql = "";
            Item itmSQL = null;

            sql = "UPDATE IN_MEETING_ALLOCATION SET in_place = N'" + in_value + "' WHERE in_meeting = '" + cfg.meeting_id + "'";
            itmSQL = cfg.inn.applySQL(sql);
            if (itmSQL.isError())
            {
                throw new Exception("執行發生錯誤 #1: ResetFightSite");
            }


            sql = "UPDATE IN_MEETING_PROGRAM SET in_fight_site = N'" + in_value + "' WHERE in_meeting = '" + cfg.meeting_id + "'";
            itmSQL = cfg.inn.applySQL(sql);
            if (itmSQL.isError())
            {
                throw new Exception("執行發生錯誤 #2: ResetFightSite");
            }
        }

        private void Update(TConfig cfg, Item itmReturn)
        {
            string name = itmReturn.getProperty("name", "");
            string value = itmReturn.getProperty("value", "");

            Item itmData = cfg.inn.newItem("In_Meeting_Variable", "merge");
            itmData.setAttribute("where", "source_id = '" + cfg.meeting_id + "' AND in_key = '" + name + "'");
            itmData.setProperty("source_id", cfg.meeting_id);
            itmData.setProperty("in_key", name);
            itmData.setProperty("in_value", value);

            Item itmResult = itmData.apply();
            if (itmResult.isError())
            {
                throw new Exception("賽會參數更新時發生錯誤");
            }

            switch (name)
            {
                case "fight_day_count":
                    RebuildMeetingDays(cfg, value);
                    break;

                case "fight_site_count":
                    RebuildMeetingSites(cfg, value);
                    break;

                case "fight_battle_type":
                    UpdateMeetingVariable(cfg, "in_battle_type", value);
                    break;

                case "fight_robin_player":
                    UpdateMeetingVariable(cfg, "in_robin_player", value);
                    break;

            }
        }

        private void UpdateMeetingVariable(TConfig cfg, string property, string value)
        {
            var sql = "UPDATE IN_MEETING SET " + property + " = '" + value + "' WHERE id = '" + cfg.meeting_id + "'";
            cfg.inn.applySQL(sql);
        }

        private void RebuildMeetingDays(TConfig cfg, string value)
        {
            var max = GetInt(value);
            if (max <= 0) throw new Exception("比賽日期數量錯誤");

            var sql = "SELECT DATEADD(HOUR, 8, in_date_s) AS 'mt_day' FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'";
            var itmMeeting = cfg.inn.applySQL(sql);

            //技術會議日期
            var mt_day = GetDtm(itmMeeting.getProperty("mt_day", "")).Date;
            //比賽第一天
            var ft_day = mt_day.AddDays(1);

            //刪除比賽日期
            sql = "DELETE FROM IN_MEETING_DAY WHERE in_meeting = '" + cfg.meeting_id + "'";
            cfg.inn.applySQL(sql);

            for (var i = 1; i <= max; i++)
            {
                var code = i.ToString();

                var itmNew = cfg.inn.newItem("In_Meeting_Day", "add");
                itmNew.setProperty("in_meeting", cfg.meeting_id);
                itmNew.setProperty("in_key", code);
                itmNew.setProperty("in_name", "Day " + code);
                itmNew.setProperty("in_day", ft_day.ToString("yyyy-MM-dd"));
                itmNew.apply();

                ft_day = ft_day.AddDays(1);
            }
        }

        private void RebuildMeetingSites(TConfig cfg, string value)
        {
            var max = GetInt(value);
            if (max <= 0) throw new Exception("場地數量錯誤");

            //清除所有場地資料
            Item itmData = cfg.inn.newItem();
            itmData.setType("In_Meeting");
            itmData.setProperty("meeting_id", cfg.meeting_id);
            itmData.setProperty("mode", "remove");
            itmData.apply("in_meeting_site");

            for (var i = 1; i <= max; i++)
            {
                var code = i.ToString();
                var row = SiteCodeRow(i);

                var itmNew = cfg.inn.newItem("In_Meeting_Site", "add");
                itmNew.setProperty("in_meeting", cfg.meeting_id);
                itmNew.setProperty("in_code", code);
                itmNew.setProperty("in_code_en", row.w1);
                itmNew.setProperty("in_name", row.w2);
                itmNew.setProperty("in_rows", "1");
                itmNew.setProperty("in_cols", value);
                itmNew.setProperty("in_row_index", "0");
                itmNew.setProperty("in_col_index", (i - 1).ToString());
                itmNew.apply();
            }
        }

        private TSP SiteCodeRow(int code)
        {
            switch (code)
            {
                case 1: return new TSP { w1 = "A", w2 = "一" };
                case 2: return new TSP { w1 = "B", w2 = "二" };
                case 3: return new TSP { w1 = "C", w2 = "三" };
                case 4: return new TSP { w1 = "D", w2 = "四" };
                case 5: return new TSP { w1 = "E", w2 = "五" };
                case 6: return new TSP { w1 = "F", w2 = "六" };
                case 7: return new TSP { w1 = "G", w2 = "七" };
                case 8: return new TSP { w1 = "H", w2 = "八" };
                case 9: return new TSP { w1 = "I", w2 = "九" };
                case 10: return new TSP { w1 = "J", w2 = "十" };
                case 11: return new TSP { w1 = "K", w2 = "十一" };
                case 12: return new TSP { w1 = "L", w2 = "十二" };
                default: return new TSP { w1 = "ERR", w2 = "ERR" };
            }
        }

        private class TSP
        {
            public string w1 { get; set; }
            public string w2 { get; set; }
        }


        private void Page(TConfig cfg, Item itmReturn)
        {
            Item itmMeeting = cfg.inn.applySQL("SELECT in_title FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            itmReturn.setProperty("in_title", itmMeeting.getProperty("in_title", ""));

            Item items = GetMeetingVariables(cfg);
            if (items.isError() || items.getResult() == "")
            {
                throw new Exception("查無賽會參數表");
            }

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string name = item.getProperty("name", "");
                string in_value = item.getProperty("in_value", "");
                string inn_input = " ";
                string inn_btn = " ";

                switch (name)
                {
                    case "fight_site_address":
                        inn_input = "<input type='text' class='form-control inn_ctrl inn_text' value='" + in_value + "' placeholder='輸入後請按 Enter'>";
                        inn_btn = "<button class='btn btn-danger inn_button'>更新至全部組別</button>";
                        break;

                    case "fight_day_count":
                    case "fight_site_count":
                        inn_input = "<select class='form-control inn_ctrl inn_select' data-value='" + in_value + "'>"
                            + "<option value=''>請選擇</option>"
                            + "<option value='1'>1</option>"
                            + "<option value='2'>2</option>"
                            + "<option value='3'>3</option>"
                            + "<option value='4'>4</option>"
                            + "<option value='5'>5</option>"
                            + "<option value='6'>6</option>"
                            + "<option value='7'>7</option>"
                            + "<option value='8'>8</option>"
                            + "<option value='9'>9</option>"
                            + "<option value='10'>10</option>"
                            + "<option value='11'>11</option>"
                            + "<option value='12'>12</option>"
                            + "</select>";
                        break;

                    case "fight_battle_type":
                        inn_input = "<select class='form-control inn_ctrl inn_select' data-value='" + in_value + "'>"
                            + "<option value=''>請選擇</option>"
                            + "<option value='TopTwo'>單淘汰制</option>"
                            + "<option value='JudoTopFour'>四柱復活賽</option>"
                            + "<option value='Challenge'>四柱復活賽挑戰賽</option>"
                            + "<option value='QuarterFinals'>八強復活</option>"
                            + "<option value='SingleRoundRobin'>單循環賽</option>"
                            + "</select>";
                        break;

                    case "fight_robin_player":
                        inn_input = "<select class='form-control inn_ctrl inn_select' data-value='" + in_value + "'>"
                            + "<option value=''>請選擇</option>"
                            + "<option value='3'>3人/隊以下</option>"
                            + "<option value='4'>4人/隊以下</option>"
                            + "<option value='5'>5人/隊以下</option>"
                            + "</select>";
                        break;

                    case "allocate_mode":
                        inn_input = "<select class='form-control inn_ctrl inn_select' data-value='" + in_value + "'>"
                            + "<option value=''>請選擇</option>"
                            + "<option value='robin'>多循環戰(64強-32強-16強-8強)</option>"
                            + "<option value='allocate08' >該組打完  8強 再換下一組</option>"
                            + "<option value='allocate16'>該組打完 16強 再換下一組</option>"
                            + "<option value='allocateST'>該組打完再換下一組(單淘-全柔錦)</option>"
                            + "</select>";
                        break;

                    case "line_color":
                        inn_input = "<select class='form-control inn_ctrl inn_select' data-value='" + in_value + "'>"
                            + "<option value=''>請選擇</option>"
                            + "<option value='red'>紅色</option>"
                            + "<option value='multicolor'>彩色</option>"
                            + "</select>";
                        break;

                    case "need_rank34":
                        inn_input = "<select class='form-control inn_ctrl inn_select' data-value='" + in_value + "'>"
                            + "<option value=''>挑戰賽才打</option>"
                            + "<option value='1'>不論賽制，必打</option>"
                            + "<option value='0'>絕對不打</option>"
                            + "</select>";
                        break;

                    case "need_rank55":
                        inn_input = "<select class='form-control inn_ctrl inn_select' data-value='" + in_value + "'>"
                            + "<option value=''>需要55名</option>"
                            + "<option value='0'>不取55名</option>"
                            + "</select>";
                        break;

                    case "need_rank56":
                        inn_input = "<select class='form-control inn_ctrl inn_select' data-value='" + in_value + "'>"
                            + "<option value=''>請選擇</option>"
                            + "<option value='1'>不論人數，必打</option>"
                            + "<option value='0'>絕對不打</option>"
                            + "<option value='2'>7人才打</option>"
                            + "</select>";
                        break;

                    case "need_rank77":
                        inn_input = "<select class='form-control inn_ctrl inn_select' data-value='" + in_value + "'>"
                            + "<option value=''>需要77名</option>"
                            + "<option value='0'>不取77名</option>"
                            + "</select>";
                        break;

                    case "need_rank78":
                        inn_input = "<select class='form-control inn_ctrl inn_select' data-value='" + in_value + "'>"
                            + "<option value=''>請選擇</option>"
                            + "<option value='1'>不論人數，必打</option>"
                            + "<option value='0'>絕對不打</option>"
                            + "<option value='2'>9人才打</option>"
                            + "</select>";
                        break;

                    case "team_battle_only_two":
                        inn_input = "<select class='form-control inn_ctrl inn_select' data-value='" + in_value + "'>"
                            + "<option value=''>打一場</option>"
                            + "<option value='3'>三戰兩勝</option>"
                            + "</select>";
                        break;

                    case "seed_14_same_face":
                        inn_input = "<select class='form-control inn_ctrl inn_select' data-value='" + in_value + "'>"
                            + "<option value=''>不同邊</option>"
                            + "<option value='1'>同邊</option>"
                            + "</select>";
                        break;

                    case "today_is_fight_day":
                        inn_input = "<select class='form-control inn_ctrl inn_select' data-value='" + in_value + "'>"
                            + "<option value=''>不鎖定</option>"
                            + "<option value='1'>鎖定</option>"
                            + "</select>";
                        break;

                    case "challenge_end_no_swap":
                        inn_input = "<select class='form-control inn_ctrl inn_select' data-value='" + in_value + "'>"
                            + "<option value=''>不換</option>"
                            + "<option value='1'>換</option>"
                            + "</select>";
                        break;

                    case "has_setted":
                        inn_input = "<select class='form-control inn_ctrl inn_select' data-value='" + in_value + "'>"
                            + "<option value=''>未設置</option>"
                            + "<option value='1'>已設置</option>"
                            + "</select>";
                        break;

                    default:
                        inn_input = "<select class='form-control inn_ctrl inn_select' data-value='" + in_value + "'>"
                            + "</select>";
                        break;

                }

                item.setType("In_Meeting_Variable");
                item.setProperty("inn_input", inn_input);
                item.setProperty("inn_btn", inn_btn);
                itmReturn.addRelationship(item);

            }
        }

        private Item GetMeetingVariables(TConfig cfg)
        {
            string sql = @"
                SELECT
	                t1.value       AS 'name'
	                , t1.label_zt  AS 'label'
	                , t2.id
	                , t2.in_value
                FROM 
	                VU_Mt_Variable t1
                LEFT OUTER JOIN
	                IN_MEETING_VARIABLE t2 WITH(NOLOCK)
	                ON t2.in_key = t1.value
	                AND t2.source_id = '{#meeting_id}'
                ORDER BY
	                t1.sort_order
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public Innosoft.InnovatorHelper InnH { get; set; }

            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }

            public string meeting_id { get; set; }
            public string scene { get; set; }

            public Item itmMeeting { get; set; }
        }

        private int GetInt(string value, int def = 0)
        {
            if (value == "") return 0;

            int result = def;
            int.TryParse(value, out result);
            return result;
        }

        private DateTime GetDtm(string value)
        {
            if (value == "") return DateTime.MinValue;

            DateTime result = DateTime.MinValue;
            DateTime.TryParse(value, out result);
            return result;
        }
    }
}
