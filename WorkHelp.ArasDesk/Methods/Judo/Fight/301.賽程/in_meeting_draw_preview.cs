﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Judo.Fight
{
    public class in_meeting_draw_preview : Item
    {
        public in_meeting_draw_preview(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 
                    預覽對戰 (XLS)
                輸入: 
                    meeting_id
                    in_draw_group
                輸出: 
                    XLS
                重點: 
                    - 需 System.Draw.dll (建議一併引用 System.Drawing.Common.dll)
                    - 需 Spire.Xls 系統 dll
                    - Spire.Xls 需 license
                    - 請於 method-config.xml 引用 DLL
                日期: 
                    2020-12-25: 加入循環賽s (lina)
                    2020-10-21: 改為 Spire.Xls (lina)
                    2020-07-28: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]in_meeting_draw_preview";

            Item itmR = this;

            string aml = "";
            string sql = "";

            string meeting_id = itmR.getProperty("meeting_id", "").Trim();
            string export_type = itmR.getProperty("export_type", "").Trim();

            if (meeting_id == "")
            {
                throw new Exception("賽事 id 不得為空白");
            }

            aml = @"
                <AML>
                    <Item type='In_Meeting' action='get' id='{#meeting_id}' select='keyed_name,in_title,in_date_s,in_date_e,in_address,in_draw_file'>
                    </Item>
                </AML>
            ".Replace("{#meeting_id}", meeting_id);

            Item itmMeeting = inn.applyAML(aml);

            if (itmMeeting.isError())
            {
                throw new Exception("取得賽事資料發生錯誤");
            }

            Item itmPrograms = GetPrograms(CCO, strMethodName, inn, itmR);

            if (itmPrograms.isError() || itmPrograms.getItemCount() <= 0)
            {
                throw new Exception("取得賽事組別資料發生錯誤");
            }

            //比賽日期
            string in_date_display = GetMeetingDateSE(itmMeeting);
            itmMeeting.setProperty("in_date_display", in_date_display);

            TXls xinfo = GetExcelInfo(CCO, strMethodName, inn, itmMeeting, itmR);

            Spire.Xls.Workbook workbook = new Spire.Xls.Workbook();
            workbook.LoadFromFile(xinfo.xls_source);
            Spire.Xls.Worksheet sheetMatchTemplate = workbook.Worksheets[0];
            Spire.Xls.Worksheet sheetFormatTemplate = workbook.Worksheets[1];

            int count = itmPrograms.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item itmProgram = itmPrograms.getItemByIndex(i);
                string in_battle_type = itmProgram.getProperty("in_battle_type", "");
                string in_team_count = itmProgram.getProperty("in_team_count", "0");
                string in_l1 = itmProgram.getProperty("in_l1", "");

                // //同組不分頁
                // AppendSheets(CCO, strMethodName, inn, workbook, sheetMatchTemplate, itmMeeting, itmProgram);
                if (in_battle_type == "SingleRoundRobin")
                {
                    if (in_team_count == "2")
                    {
                        //單循環賽
                        RoundSheets2(CCO, strMethodName, inn, workbook, sheetFormatTemplate, itmMeeting, itmProgram);
                    }
                    else
                    {
                        //單循環賽
                        RoundSheets(CCO, strMethodName, inn, workbook, sheetFormatTemplate, itmMeeting, itmProgram);
                    }
                }
                else if (in_battle_type == "DoubleRoundRobin")
                {
                    //雙循環賽
                    RoundSheets(CCO, strMethodName, inn, workbook, sheetFormatTemplate, itmMeeting, itmProgram, is_double: true);
                }
                else if (in_l1 == "格式組")
                {
                    //格式組
                    FormatSheets(CCO, strMethodName, inn, workbook, sheetFormatTemplate, itmMeeting, itmProgram);
                }
                else
                {
                    itmProgram.setProperty("name_max", GetNameMax(CCO, strMethodName, inn, itmMeeting, itmProgram));
                    //同組分頁
                    SplitSheets(CCO, strMethodName, inn, workbook, sheetMatchTemplate, itmMeeting, itmProgram);
                }
            }

            //移除樣板 sheet
            sheetMatchTemplate.Remove();
            sheetFormatTemplate.Remove();

            if (export_type == "PDF")
            {
                workbook.SaveToFile(xinfo.xls_file.Replace("xlsx", "pdf"), Spire.Xls.FileFormat.PDF);
                itmR.setProperty("xls_name", xinfo.xls_url.Replace("xlsx", "pdf"));
            }
            else
            {
                workbook.SaveToFile(xinfo.xls_file, Spire.Xls.ExcelVersion.Version2010);
                itmR.setProperty("xls_name", xinfo.xls_url);
            }

            return itmR;
        }
        
        private string GetNameMax(Aras.Server.Core.CallContext CCO
            , string strMethodName
            , Innovator inn
            , Item itmMeeting
            , Item itmProgram)

        {

            string meeting_id = itmProgram.getProperty("in_meeting", "");
            string program_id = itmProgram.getProperty("id", "");
            string in_l1 = itmProgram.getProperty("in_l1", "");
            string in_l2 = itmProgram.getProperty("in_l2", "");
            string in_l3 = itmProgram.getProperty("in_l3", "");

            string sql = @"
                SELECT 
	                MAX(LEN(in_name)) AS 'name_max' 
                FROM 
	                IN_MEETING_USER WITH(NOLOCK)
                WHERE
	                source_id = '{#meeting_id}'
	                AND in_l1 = N'{#in_l1}'
	                AND in_l2 = N'{#in_l2}'
	                AND in_l3 = N'{#in_l3}'
            ";

            sql = sql.Replace("{#meeting_id}", meeting_id)
                .Replace("{#in_l1}", in_l1)
                .Replace("{#in_l2}", in_l2)
                .Replace("{#in_l3}", in_l3);

            Item item = inn.applySQL(sql);

            if (!item.isError() && item.getResult() != "")
            {
                return item.getProperty("name_max", "9");
            }
            else
            {
                return "9";
            }
        }

        //循環賽
        private void RoundSheets(Aras.Server.Core.CallContext CCO
            , string strMethodName
            , Innovator inn
            , Spire.Xls.Workbook workbook
            , Spire.Xls.Worksheet sheetTemplate
            , Item itmMeeting
            , Item itmProgram
            , bool is_double = false)
        {

            //單循環-隊伍資料
            List<Item> itmTeams = GetMTeamList3(CCO, strMethodName, inn, itmProgram);

            if (itmTeams.Count == 0)
            {
                return;
            }

            string program_name = itmProgram.getProperty("in_name", "");
            string program_name2 = itmProgram.getProperty("in_name2", "");
            string program_name3 = itmProgram.getProperty("in_name3", "");
            string program_display = itmProgram.getProperty("in_display", "");
            string program_short_name = itmProgram.getProperty("in_short_name", "");
            string in_team_count = itmTeams.Count.ToString();

            if (in_team_count == "" || in_team_count == "0")
            {
                return;
            }

            //檢查籤號
            CheckSignNos(itmTeams, in_team_count, program_name3);

            TSheet cfg = new TSheet
            {
                WsRow = 4,
                WsCol = 0,
                LineRow = 3,
                LineCol = 3,
                Heads = new string[] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" },
                Font = "標楷體",
                Sheets = new List<string>(),
            };

            string sheetName = GetSheetName(cfg, program_short_name, program_name2);

            Spire.Xls.Worksheet sheet = workbook.CreateEmptySheet();
            sheet.CopyFrom(sheetTemplate);
            sheet.Name = sheetName;

            sheet.Range["A1"].Text = "組別量級: " + program_name3 + " (" + in_team_count + ")";
            sheet.Range["A1"].Style.Font.FontName = cfg.Font;

            //設定隊別 or 姓名寬度
            sheet.SetColumnWidth(2, 15);

            int wsRow = cfg.WsRow;
            int wsCol = cfg.WsCol;
            int count = itmTeams.Count;

            int row_height = 35;
            int col_wide = 15;

            //標題列
            if (is_double)
            {
                SetItemCell2(sheet, cfg, wsRow, wsCol + 0, "No.", "");
                SetItemCell2(sheet, cfg, wsRow, wsCol + 1, "", "", need_split_line: true);
            }
            else
            {
                SetItemCell2(sheet, cfg, wsRow, wsCol + 0, "No.", "");
                SetItemCell2(sheet, cfg, wsRow, wsCol + 1, "", "", need_split_line: true);
            }

            for (int i = 0; i < count; i++)
            {
                Item itmTeam = itmTeams[i];
                string map_short_org = itmTeam.getProperty("map_short_org", "");
                string in_name = itmTeam.getProperty("in_name", "");
                string inn_display = map_short_org + System.Environment.NewLine + in_name;

                int currentCol = wsCol + 2 + i;

                SetItemCell2(sheet, cfg, wsRow, currentCol, inn_display, "");

                sheet.SetColumnWidth(currentCol, col_wide);
            }

            sheet.SetRowHeight(wsRow, row_height);

            wsRow++;

            //資料容器
            Item itmContainer = inn.newItem();

            Dictionary<string, string> map = new Dictionary<string, string>();

            for (int i = 0; i < count; i++)
            {
                Item itmTeam = itmTeams[i];
                string map_short_org = itmTeam.getProperty("map_short_org", "");
                string in_name = itmTeam.getProperty("in_name", "");
                string in_sign_no = itmTeam.getProperty("in_sign_no", "");
                string inn_display = map_short_org + System.Environment.NewLine + in_name;

                SetItemCell2(sheet, cfg, wsRow, wsCol + 0, in_sign_no, "");
                SetItemCell2(sheet, cfg, wsRow, wsCol + 1, inn_display, "");

                for (int j = 0; j < count; j++)
                {
                    Item itmTeam2 = itmTeams[j];
                    string in_sign_no2 = itmTeam2.getProperty("in_sign_no", "");
                    string key = is_double ? GetSRKey2(itmTeam, itmTeam2) : GetSRKey(itmTeam, itmTeam2);

                    bool need_gray_color = false;
                    bool need_split_line = false;

                    string value = "";

                    if (in_sign_no == in_sign_no2)
                    {
                        need_split_line = false;
                    }
                    else if (map.ContainsKey(key))
                    {
                        need_gray_color = true;
                    }
                    else
                    {
                        map.Add(key, key);
                        value = map.Count.ToString();
                    }

                    SetItemCell2(sheet, cfg, wsRow, wsCol + 2 + j, value, "", need_gray_color: need_gray_color, need_split_line: need_split_line);
                }

                sheet.SetRowHeight(wsRow, row_height);

                wsRow = wsRow + 1;
            }


            var pos = "A4:E7";
            if (count == 2)
            {
                pos = "A3:D6";
                sheet.Range["C5:C5"].Borders[Spire.Xls.BordersLineType.DiagonalDown].LineStyle = Spire.Xls.LineStyleType.Thin;
                sheet.Range["D6:D6"].Borders[Spire.Xls.BordersLineType.DiagonalDown].LineStyle = Spire.Xls.LineStyleType.Thin;
                sheet.Range["E7:E7"].Borders[Spire.Xls.BordersLineType.DiagonalDown].LineStyle = Spire.Xls.LineStyleType.Thin;
            }
            else if (count == 3)
            {
                sheet.Range["D5"].Text = "1";
                sheet.Range["E5"].Text = "3";
                sheet.Range["E6"].Text = "2";
                sheet.Range["C5:C5"].Borders[Spire.Xls.BordersLineType.DiagonalDown].LineStyle = Spire.Xls.LineStyleType.Thin;
                sheet.Range["D6:D6"].Borders[Spire.Xls.BordersLineType.DiagonalDown].LineStyle = Spire.Xls.LineStyleType.Thin;
                sheet.Range["E7:E7"].Borders[Spire.Xls.BordersLineType.DiagonalDown].LineStyle = Spire.Xls.LineStyleType.Thin;
            }
            else if (count == 4)
            {
                pos = "A4:F8";
                sheet.SetColumnWidth(2, 13);
                sheet.SetColumnWidth(3, 13);
                sheet.SetColumnWidth(4, 13);
                sheet.SetColumnWidth(5, 13);
                sheet.SetColumnWidth(6, 13);

                sheet.Range["D5"].Text = "1";
                sheet.Range["E5"].Text = "3";
                sheet.Range["F5"].Text = "5";
                sheet.Range["E6"].Text = "6";
                sheet.Range["F6"].Text = "4";
                sheet.Range["F7"].Text = "2";

                sheet.Range["C5:C5"].Borders[Spire.Xls.BordersLineType.DiagonalDown].LineStyle = Spire.Xls.LineStyleType.Thin;
                sheet.Range["D6:D6"].Borders[Spire.Xls.BordersLineType.DiagonalDown].LineStyle = Spire.Xls.LineStyleType.Thin;
                sheet.Range["E7:E7"].Borders[Spire.Xls.BordersLineType.DiagonalDown].LineStyle = Spire.Xls.LineStyleType.Thin;
                sheet.Range["F8:F8"].Borders[Spire.Xls.BordersLineType.DiagonalDown].LineStyle = Spire.Xls.LineStyleType.Thin;
            }
            else if (count == 5)
            {
                pos = "A4:G9";
                sheet.SetColumnWidth(2, 13);
                sheet.SetColumnWidth(3, 13);
                sheet.SetColumnWidth(4, 13);
                sheet.SetColumnWidth(5, 13);
                sheet.SetColumnWidth(6, 13);
                sheet.SetColumnWidth(7, 13);

                sheet.Range["D5"].Text = "1";
                sheet.Range["E5"].Text = "6";
                sheet.Range["F5"].Text = "9";
                sheet.Range["G5"].Text = "3";

                sheet.Range["E6"].Text = "4";
                sheet.Range["F6"].Text = "7";
                sheet.Range["G6"].Text = "10";

                sheet.Range["F7"].Text = "2";
                sheet.Range["G7"].Text = "8";

                sheet.Range["G8"].Text = "5";

                sheet.Range["C5:C5"].Borders[Spire.Xls.BordersLineType.DiagonalDown].LineStyle = Spire.Xls.LineStyleType.Thin;
                sheet.Range["D6:D6"].Borders[Spire.Xls.BordersLineType.DiagonalDown].LineStyle = Spire.Xls.LineStyleType.Thin;
                sheet.Range["E7:E7"].Borders[Spire.Xls.BordersLineType.DiagonalDown].LineStyle = Spire.Xls.LineStyleType.Thin;
                sheet.Range["F8:F8"].Borders[Spire.Xls.BordersLineType.DiagonalDown].LineStyle = Spire.Xls.LineStyleType.Thin;
                sheet.Range["G9:G9"].Borders[Spire.Xls.BordersLineType.DiagonalDown].LineStyle = Spire.Xls.LineStyleType.Thin;
            }

            var range = sheet.Range[pos];
            range.BorderInside(Spire.Xls.LineStyleType.Thin, System.Drawing.Color.Black);
            range.BorderAround(Spire.Xls.LineStyleType.Thin, System.Drawing.Color.Black);
        }

        //循環賽
        private void RoundSheets2(Aras.Server.Core.CallContext CCO
            , string strMethodName
            , Innovator inn
            , Spire.Xls.Workbook workbook
            , Spire.Xls.Worksheet sheetTemplate
            , Item itmMeeting
            , Item itmProgram
            , bool is_double = false)
        {
            string program_id = itmProgram.getProperty("id", "").Trim();

            string sql = @"
                SELECT 
	                id
                FROM 
	                IN_MEETING_PEVENT WITH(NOLOCK)
                WHERE 
	                source_id = '{#program_id}'
	                AND ISNULL(in_type, '') IN ('', 'p')
            ";

            sql = sql.Replace("{#program_id}", program_id);

            Item itmEvents = inn.applySQL(sql);

            int count = itmEvents.getItemCount();

            if (count == 1)
            {
                RoundSheets2_1Event(CCO, strMethodName, inn, workbook, sheetTemplate, itmMeeting, itmProgram, is_double);

            }
            else
            {
                RoundSheets2_3Events(CCO, strMethodName, inn, workbook, sheetTemplate, itmMeeting, itmProgram, is_double);
            }
        }

        //循環賽(只打一場)
        private void RoundSheets2_1Event(Aras.Server.Core.CallContext CCO
            , string strMethodName
            , Innovator inn
            , Spire.Xls.Workbook workbook
            , Spire.Xls.Worksheet sheetTemplate
            , Item itmMeeting
            , Item itmProgram
            , bool is_double = false)
        {

            //單循環-隊伍資料
            List<Item> itmTeams = GetMTeamList3(CCO, strMethodName, inn, itmProgram);

            if (itmTeams.Count == 0)
            {
                return;
            }

            string program_name = itmProgram.getProperty("in_name", "");
            string program_name2 = itmProgram.getProperty("in_name2", "");
            string program_name3 = itmProgram.getProperty("in_name3", "");
            string program_display = itmProgram.getProperty("in_display", "");
            string program_short_name = itmProgram.getProperty("in_short_name", "");
            string in_team_count = itmTeams.Count.ToString();

            if (in_team_count == "" || in_team_count == "0")
            {
                return;
            }

            //檢查籤號
            CheckSignNos(itmTeams, in_team_count, program_name3);

            TSheet cfg = new TSheet
            {
                WsRow = 4,
                WsCol = 0,
                LineRow = 3,
                LineCol = 3,
                Heads = new string[] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" },
                Font = "標楷體",
                Sheets = new List<string>(),
            };

            string sheetName = GetSheetName(cfg, program_short_name, program_name2);

            Spire.Xls.Worksheet sheet = workbook.CreateEmptySheet();
            sheet.CopyFrom(sheetTemplate);
            sheet.Name = sheetName;

            sheet.Range["A1"].Text = "組別量級: " + program_name3 + " (" + in_team_count + ")";
            sheet.Range["A1"].Style.Font.FontName = cfg.Font;

            //設定隊別 or 姓名寬度
            sheet.SetColumnWidth(2, 12);

            int wsRow = cfg.WsRow;
            int wsCol = cfg.WsCol;
            int count = itmTeams.Count;

            int row_height = 35;
            int col_wide = 15;

            SetItemCell2(sheet, cfg, wsRow, wsCol + 0, "No.", "");
            SetItemCell2(sheet, cfg, wsRow, wsCol + 1, "隊別", "", need_split_line: false);
            SetItemCell2(sheet, cfg, wsRow, wsCol + 2, "", "", need_split_line: false);
            sheet.SetRowHeight(wsRow, 35);
            wsRow++;

            for (int i = 0; i < count; i++)
            {
                Item itmTeam = itmTeams[i];
                string map_short_org = itmTeam.getProperty("map_short_org", "");
                string in_name = itmTeam.getProperty("in_name", "");
                string inn_display = map_short_org + System.Environment.NewLine + in_name;

                SetItemCell2(sheet, cfg, wsRow, wsCol + 0, (i + 1).ToString(), "");
                SetItemCell2(sheet, cfg, wsRow, wsCol + 1, inn_display, "");
                sheet.SetRowHeight(wsRow, 35);
                wsRow++;
            }

            sheet.SetColumnWidth(2, 34);
            sheet.SetColumnWidth(3, 17);

            var evt_rng = sheet.Range["C5:C6"];
            evt_rng.Merge();
            evt_rng.Text = "1";
            evt_rng.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;

            var range = sheet.Range["A4:C6"];
            range.BorderInside(Spire.Xls.LineStyleType.Thin, System.Drawing.Color.Black);
            range.BorderAround(Spire.Xls.LineStyleType.Thin, System.Drawing.Color.Black);


        }

        //循環賽(三戰兩勝)
        private void RoundSheets2_3Events(Aras.Server.Core.CallContext CCO
            , string strMethodName
            , Innovator inn
            , Spire.Xls.Workbook workbook
            , Spire.Xls.Worksheet sheetTemplate
            , Item itmMeeting
            , Item itmProgram
            , bool is_double = false)
        {

            //單循環-隊伍資料
            List<Item> itmTeams = GetMTeamList3(CCO, strMethodName, inn, itmProgram);

            if (itmTeams.Count == 0)
            {
                return;
            }

            string program_name = itmProgram.getProperty("in_name", "");
            string program_name2 = itmProgram.getProperty("in_name2", "");
            string program_name3 = itmProgram.getProperty("in_name3", "");
            string program_display = itmProgram.getProperty("in_display", "");
            string program_short_name = itmProgram.getProperty("in_short_name", "");
            string in_team_count = itmTeams.Count.ToString();

            if (in_team_count == "" || in_team_count == "0")
            {
                return;
            }

            //檢查籤號
            CheckSignNos(itmTeams, in_team_count, program_name3);

            TSheet cfg = new TSheet
            {
                WsRow = 4,
                WsCol = 0,
                LineRow = 3,
                LineCol = 3,
                Heads = new string[] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" },
                Font = "標楷體",
                Sheets = new List<string>(),
            };

            string sheetName = GetSheetName(cfg, program_short_name, program_name2);

            Spire.Xls.Worksheet sheet = workbook.CreateEmptySheet();
            sheet.CopyFrom(sheetTemplate);
            sheet.Name = sheetName;

            sheet.Range["A1"].Text = "組別量級: " + program_name3 + " (" + in_team_count + ")";
            sheet.Range["A1"].Style.Font.FontName = cfg.Font;

            //設定隊別 or 姓名寬度
            sheet.SetColumnWidth(2, 12);

            int wsRow = cfg.WsRow;
            int wsCol = cfg.WsCol;
            int count = itmTeams.Count;

            int row_height = 35;
            int col_wide = 15;

            //標題列
            if (is_double)
            {
                SetItemCell2(sheet, cfg, wsRow, wsCol + 0, "No.", "");
                SetItemCell2(sheet, cfg, wsRow, wsCol + 1, "", "", need_split_line: true);
            }
            else
            {
                SetItemCell2(sheet, cfg, wsRow, wsCol + 0, "No.", "");
                SetItemCell2(sheet, cfg, wsRow, wsCol + 1, "", "", need_split_line: true);
            }

            for (int i = 0; i < count; i++)
            {
                Item itmTeam = itmTeams[i];
                string map_short_org = itmTeam.getProperty("map_short_org", "");
                string in_name = itmTeam.getProperty("in_name", "");
                string inn_display = map_short_org + System.Environment.NewLine + in_name;

                int currentCol = wsCol + 2 + i;

                SetItemCell2(sheet, cfg, wsRow, currentCol, inn_display, "");

                sheet.SetColumnWidth(currentCol, col_wide);
            }

            sheet.SetRowHeight(wsRow, row_height);

            wsRow++;

            //資料容器
            Item itmContainer = inn.newItem();

            Dictionary<string, string> map = new Dictionary<string, string>();

            for (int i = 0; i < count; i++)
            {

                Item itmTeam = itmTeams[i];
                string map_short_org = itmTeam.getProperty("map_short_org", "");
                string in_name = itmTeam.getProperty("in_name", "");
                string in_sign_no = itmTeam.getProperty("in_sign_no", "");
                string inn_display = map_short_org + System.Environment.NewLine + in_name;

                SetItemCell2(sheet, cfg, wsRow, wsCol + 0, in_sign_no, "");
                SetItemCell2(sheet, cfg, wsRow, wsCol + 1, inn_display, "");

                for (int j = 0; j < count; j++)
                {
                    Item itmTeam2 = itmTeams[j];
                    string in_sign_no2 = itmTeam2.getProperty("in_sign_no", "");
                    string key = is_double ? GetSRKey2(itmTeam, itmTeam2) : GetSRKey(itmTeam, itmTeam2);

                    bool need_gray_color = false;
                    bool need_split_line = false;

                    string value = "";

                    if (in_sign_no == in_sign_no2)
                    {
                        need_split_line = true;
                    }
                    else if (map.ContainsKey(key))
                    {
                        need_gray_color = true;
                    }
                    else
                    {
                        map.Add(key, key);
                        value = map.Count.ToString();
                    }

                    SetItemCell2(sheet, cfg, wsRow, wsCol + 2 + j, value, "", need_gray_color: false, need_split_line: need_split_line);
                }

                sheet.SetRowHeight(wsRow, row_height);

                wsRow = wsRow + 1;
            }

            SetItemCell2(sheet, cfg, 4, 4, "加賽", "", need_gray_color: false, need_split_line: false);
            SetItemCell2(sheet, cfg, 5, 4, "3", "", need_gray_color: false, need_split_line: false);
            SetItemCell2(sheet, cfg, 6, 4, " ", "", need_gray_color: false, need_split_line: false);

            SetItemCell2(sheet, cfg, 6, 2, "2", "", need_gray_color: false, need_split_line: false);

            sheet.SetColumnWidth(2, 17);
            sheet.SetColumnWidth(3, 17);
            sheet.SetColumnWidth(4, 17);
            sheet.SetColumnWidth(5, 10);

            sheet.Range["E5:E6"].Merge();

            var range = sheet.Range["A4:E6"];
            range.BorderInside(Spire.Xls.LineStyleType.Thin, System.Drawing.Color.Black);
            range.BorderAround(Spire.Xls.LineStyleType.Thin, System.Drawing.Color.Black);

        }

        private string GetSRKey(Item itmTeam, Item itmTeam2)
        {
            int sign_no1 = GetIntVal(itmTeam.getProperty("in_sign_no", ""));
            int sign_no2 = GetIntVal(itmTeam2.getProperty("in_sign_no", ""));
            if (sign_no2 > sign_no1)
            {
                return sign_no1 + "," + sign_no2;
            }
            else
            {
                return sign_no2 + "," + sign_no1;

            }
        }

        private string GetSRKey2(Item itmTeam, Item itmTeam2)
        {
            return itmTeam.getProperty("in_sign_no", "") + "," + itmTeam2.getProperty("in_sign_no", "");
        }

        //格式組
        private void FormatSheets(Aras.Server.Core.CallContext CCO
            , string strMethodName
            , Innovator inn
            , Spire.Xls.Workbook workbook
            , Spire.Xls.Worksheet sheetTemplate
            , Item itmMeeting
            , Item itmProgram)
        {

            //格式組-隊伍資料
            List<Item> itmTeams = GetMTeamList2(CCO, strMethodName, inn, itmProgram);

            if (itmTeams.Count == 0)
            {
                return;
            }

            string program_name = itmProgram.getProperty("in_name", "");
            string program_name2 = itmProgram.getProperty("in_name2", "");
            string program_name3 = itmProgram.getProperty("in_name3", "");
            string program_display = itmProgram.getProperty("in_display", "");
            string program_short_name = itmProgram.getProperty("in_short_name", "");
            string in_team_count = itmTeams.Count.ToString();

            if (in_team_count == "" || in_team_count == "0")
            {
                return;
            }

            //檢查籤號
            CheckSignNos(itmTeams, in_team_count, program_name3);

            TSheet cfg = new TSheet
            {
                WsRow = 4,
                WsCol = 0,
                LineRow = 3,
                LineCol = 3,
                Heads = new string[] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" },
                Font = "標楷體",
                Sheets = new List<string>(),
            };

            string sheetName = GetSheetName(cfg, program_short_name, program_name2);

            Spire.Xls.Worksheet sheet = workbook.CreateEmptySheet();
            sheet.CopyFrom(sheetTemplate);
            sheet.Name = sheetName;

            string[] titles = new string[]
            {
                "No",
                "單位",
                "姓名",
                "被施術者單位",
                "被施術者姓名",
            };

            string[] fields = new string[]
            {
                "in_judo_no",
                "map_short_org",
                "in_name",
                "map_short_org",
                "in_exe_a1",
            };

            string[] formats = new string[]
            {
                "center",
                "",
                "",
                "",
                "",
            };

            sheet.Range["A1"].Text = "組別量級: " + program_name3 + " (" + in_team_count + ")";
            sheet.Range["A1"].Style.Font.FontName = cfg.Font;

            //設定隊別 or 姓名寬度
            sheet.SetColumnWidth(2, 15);
            sheet.SetColumnWidth(3, 18);
            sheet.SetColumnWidth(4, 15);
            sheet.SetColumnWidth(5, 18);


            int wsRow = cfg.WsRow;
            int wsCol = cfg.WsCol;
            int count = itmTeams.Count;

            //資料容器
            Item itmContainer = inn.newItem();

            for (int i = 0; i < count; i++)
            {
                Item itmTeam = itmTeams[i];
                itmContainer.setProperty("in_sign_no", itmTeam.getProperty("in_sign_no", ""));
                itmContainer.setProperty("map_short_org", itmTeam.getProperty("map_short_org", ""));
                itmContainer.setProperty("in_name", itmTeam.getProperty("in_name", ""));
                itmContainer.setProperty("in_exe_a1", itmTeam.getProperty("in_exe_a1", ""));
                itmContainer.setProperty("in_judo_no", itmTeam.getProperty("in_judo_no", ""));

                SetItemCell(sheet, cfg, wsRow, wsCol, itmContainer, fields, formats);

                wsRow = wsRow + 1;
            }
        }

        //同組不分頁
        private void AppendSheets(Aras.Server.Core.CallContext CCO
            , string strMethodName
            , Innovator inn
            , Spire.Xls.Workbook workbook
            , Spire.Xls.Worksheet sheetTemplate
            , Item itmMeeting
            , Item itmProgram)
        {

            //隊伍資料
            List<Item> itmTeams = GetMTeamList(CCO, strMethodName, inn, itmProgram);

            if (itmTeams.Count == 0)
            {
                return;
            }

            string program_name = itmProgram.getProperty("in_name", "");
            string program_name2 = itmProgram.getProperty("in_name2", "");
            string program_name3 = itmProgram.getProperty("in_name3", "");
            string program_display = itmProgram.getProperty("in_display", "");
            string in_team_count = itmProgram.getProperty("in_team_count", "");

            if (in_team_count == "" || in_team_count == "0")
            {
                return;
            }

            //檢查籤號
            CheckSignNos(itmTeams, in_team_count, program_name3);

            for (int i = 0; i < itmTeams.Count; i++)
            {
                Item item = itmTeams[i];
                item.setProperty("no", (i + 1).ToString());
            }

            TSheet cfg = new TSheet
            {
                WsRow = 4,
                WsCol = 0,
                LineRow = 3,
                LineCol = 3,
                Heads = new string[] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" },
                Font = "標楷體",
                SheetCount = 1,
                Sheets = new List<string>(),
            };

            AppendSheet(CCO
                , strMethodName
                , inn
                , workbook
                , sheetTemplate
                , itmMeeting
                , itmProgram
                , cfg
                , 1
                , itmTeams);

        }

        //同組分頁
        private void SplitSheets(Aras.Server.Core.CallContext CCO
            , string strMethodName
            , Innovator inn
            , Spire.Xls.Workbook workbook
            , Spire.Xls.Worksheet sheetTemplate
            , Item itmMeeting
            , Item itmProgram)
        {

            //隊伍資料
            List<Item> itmTeams = GetMTeamList(CCO, strMethodName, inn, itmProgram);

            if (itmTeams.Count == 0)
            {
                return;
            }

            string program_name = itmProgram.getProperty("in_name", "");
            string program_name2 = itmProgram.getProperty("in_name2", "");
            string program_name3 = itmProgram.getProperty("in_name3", "");
            string program_display = itmProgram.getProperty("in_display", "");
            string in_team_count = itmProgram.getProperty("in_team_count", "");

            if (in_team_count == "" || in_team_count == "0")
            {
                return;
            }

            //檢查籤號
            CheckSignNos(itmTeams, in_team_count, program_name3);

            TSheet cfg = new TSheet
            {
                WsRow = 4,
                WsCol = 0,
                LineRow = 3,
                LineCol = 3,
                Heads = new string[] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" },
                Font = "標楷體",
                Sheets = new List<string>(),
            };

            //每頁最大筆數 (max_page_count)
            int mpc = 32;
            //實際總隊數，ex: 40
            int real_team_count = GetIntVal(in_team_count);

            //實際分組數，ex: 2，只能二進分割
            int real_sheet_count = real_team_count % mpc == 0 ? real_team_count / mpc : real_team_count / mpc + 1;
            if (real_sheet_count == 3)
            {
                real_sheet_count = 4;
            }
            else if (real_sheet_count > 4)
            {
                throw new Exception("隊數過多發生錯誤");
            }

            if (real_team_count >= 65 && real_team_count <= 68)
            {
                real_sheet_count = 2;
            }

            //二進總隊數，ex: 64
            int plan_team_count = itmTeams.Count;
            //二進每頁隊數，ex: 32 (plan_page_count)
            int ppc = plan_team_count / real_sheet_count;

            Dictionary<int, List<Item>> sheets = new Dictionary<int, List<Item>>();

            int no = 1;

            //分割資料
            for (int i = 0; i < itmTeams.Count; i++)
            {
                Item item = itmTeams[i];
                string in_sign_id = item.getProperty("in_sign_id", "");

                //設定總體序號
                if (in_sign_id != "")
                {
                    item.setProperty("no", no.ToString());
                    no++;
                }

                int serial = i + 1;
                int sheet_no = serial % ppc == 0 ? serial / ppc : serial / ppc + 1;

                List<Item> rows = null;

                if (sheets.ContainsKey(sheet_no))
                {
                    rows = sheets[sheet_no];
                }
                else
                {
                    rows = new List<Item>();
                    sheets.Add(sheet_no, rows);
                }
                rows.Add(item);
            }

            cfg.SheetCount = real_sheet_count;

            if (cfg.SheetCount > 1)
            {
                //計算全場次序號
                List<TSignGroup> signs = GetSigns(cfg, itmTeams, cfg.LineRow, cfg.LineCol);
                cfg.EventNoMap = GetEventNoMap(signs, cfg.SheetCount);
                cfg.TFEventNo = signs.Max(x => x.EventNo);
                cfg.MaxEventNo = cfg.TFEventNo - 1;
            }

            foreach (KeyValuePair<int, List<Item>> pair in sheets)
            {
                var sheetNo = pair.Key; //from 1
                var rows = pair.Value;

                AppendSheet(CCO
                    , strMethodName
                    , inn
                    , workbook
                    , sheetTemplate
                    , itmMeeting
                    , itmProgram
                    , cfg
                    , sheetNo
                    , rows);
            }
        }

        private void AppendSheet(Aras.Server.Core.CallContext CCO
            , string strMethodName
            , Innovator inn
            , Spire.Xls.Workbook workbook
            , Spire.Xls.Worksheet sheetTemplate
            , Item itmMeeting
            , Item itmProgram
            , TSheet cfg
            , int sheetNo
            , List<Item> teams)
        {
            string program_name = itmProgram.getProperty("in_name", "");
            string program_name2 = itmProgram.getProperty("in_name2", "");
            string program_name3 = itmProgram.getProperty("in_name3", "");
            string program_display = itmProgram.getProperty("in_display", "");
            string program_short_name = itmProgram.getProperty("in_short_name", "");
            string in_team_count = itmProgram.getProperty("in_team_count", "");
            string in_l1 = itmProgram.getProperty("in_l1", "");
            string in_l2 = itmProgram.getProperty("in_l2", "");

            if (teams.Count == 0)
            {
                return;
            }

            bool is_multiPlayers = IsMultiPlayers(in_l1);
            string team_unit = is_multiPlayers ? "比賽隊數" : "比賽人數";
            string team_count = in_team_count.ToString() + (is_multiPlayers ? " 隊" : " 人");

            string sheetName = GetSheetName(cfg, program_short_name, program_name2, sheetNo.ToString());

            Spire.Xls.Worksheet sheet = workbook.CreateEmptySheet();
            sheet.CopyFrom(sheetTemplate);
            sheet.Name = sheetName;

            string[] titles = new string[]
            {
                "No",
                "單位",
                "姓名",
                "",
            };

            string[] fields = new string[]
            {
                "in_judo_no",
                "map_short_org",
                "in_display",
                ""
            };

            string[] formats = new string[]
            {
                "center",
                "center",
                "center",
                "center",
            };

            sheet.Range["A1"].Text = "組別量級: " + program_name3 + " (" + team_count + ")";
            sheet.Range["A1"].Style.Font.FontName = cfg.Font;

            if (in_l1 == "團體組")
            {
                //設定隊別 or 姓名寬度
                sheet.SetColumnWidth(2, 15);
                sheet.SetColumnWidth(3, 4);
            }
            else
            {
                int name_max = GetIntVal(itmProgram.getProperty("name_max", "9"));
                int ncw = name_max * 2 - 2;
                sheet.SetColumnWidth(2, 15);
                sheet.SetColumnWidth(3, ncw);
            }

            // sheet.Range["A2"].Text = sheetName;
            // sheet.Range["A2"].Style.Font.FontName = cfg.Font;

            if (cfg.SheetCount > 1)
            {
                sheet.Range["K1"].Text = GetCodeName(sheetNo + "/" + cfg.SheetCount);
                sheet.Range["K1"].Style.Font.FontName = cfg.Font;
            }

            int wsRow = cfg.WsRow;
            int wsCol = cfg.WsCol;
            int count = teams.Count;

            //資料容器
            Item itmContainer = inn.newItem();

            for (int i = 0; i < count; i++)
            {
                Item itmTeam = teams[i];

                string in_sign_id = itmTeam.getProperty("in_sign_id", "");

                if (in_sign_id == "")
                {
                    //我是輪空
                    continue;
                }

                itmContainer.setProperty("no", itmTeam.getProperty("no", ""));
                itmContainer.setProperty("in_current_org", itmTeam.getProperty("in_current_org", ""));
                itmContainer.setProperty("map_short_org", itmTeam.getProperty("map_short_org", ""));
                itmContainer.setProperty("in_name", itmTeam.getProperty("in_name", ""));
                itmContainer.setProperty("in_sign_no", itmTeam.getProperty("in_sign_no", ""));
                itmContainer.setProperty("in_judo_no", itmTeam.getProperty("in_judo_no", ""));

                string in_display = itmTeam.getProperty("in_name", "");

                if (is_multiPlayers)
                {
                    in_display = itmTeam.getProperty("in_team", "");
                }

                itmContainer.setProperty("in_display", in_display);

                SetItemCell(sheet, cfg, wsRow, wsCol, itmContainer, fields, formats);
                SetItemCell(sheet, cfg, wsRow + 1, wsCol, itmContainer, fields, formats);

                sheet.Range["B" + wsRow].HorizontalAlignment = Spire.Xls.HorizontalAlignType.Right;
                sheet.Range["C" + wsRow].HorizontalAlignment = Spire.Xls.HorizontalAlignType.Right;

                wsRow = wsRow + 2;
            }

            List<TSignGroup> signs = GetSigns(cfg, teams, cfg.LineRow, cfg.LineCol);

            if (cfg.SheetCount > 1)
            {
                //分取全場次序號
                BindEventNos(signs, cfg);
            }

            DrawLine(sheet, cfg, sheetNo, signs);
        }

        private void DrawLine(Spire.Xls.Worksheet sheet, TSheet cfg, int sheetNo, List<TSignGroup> signs)
        {
            ClosedXML.Excel.XLBorderStyleValues line = ClosedXML.Excel.XLBorderStyleValues.Thin;

            int event_no = 1;

            for (int i = 0; i < signs.Count; i++)
            {
                TSignGroup sg = signs[i];

                int cidx = sg.ColIndex;

                if (sg.IsLong || sg.IsEnd)
                {
                    if (sg.RowIndex > cfg.LineRow)
                    {
                        var cell = sheet.Range[sg.RowIndex, cidx];
                        cell.Style.Borders[Spire.Xls.BordersLineType.EdgeBottom].LineStyle = Spire.Xls.LineStyleType.Thin;
                        //cell.Style.Borders.Color = System.Drawing.Color.Red;

                        if (sg.IsEnd)
                        {
                            if (cfg.SheetCount == 4)
                            {
                                int row_min = sg.RowIndex;
                                int row_max = sg.RowIndex + 6;
                                //偶數頁往上畫，單數頁往下畫
                                if (sheetNo % 2 == 0)
                                {
                                    row_min = sg.RowIndex - 6;
                                    row_max = sg.RowIndex;
                                }

                                for (int j = row_min + 1; j <= row_max; j++)
                                {
                                    var cell2 = sheet.Range[j, cidx];
                                    cell2.Style.Borders[Spire.Xls.BordersLineType.EdgeRight].LineStyle = Spire.Xls.LineStyleType.Thin;
                                    //cell2.Style.Borders.Color = System.Drawing.Color.Red;
                                }

                                var cell3 = sheet.Range[row_min + 3, cidx + 1];
                                cell3.Style.Borders[Spire.Xls.BordersLineType.EdgeBottom].LineStyle = Spire.Xls.LineStyleType.Thin;
                                //cell3.Style.Borders.Color = System.Drawing.Color.Red;

                                //準決賽
                                var cell_semi = sheet.Range[row_min + 3, cidx];
                                cell_semi.NumberValue = sg.EventNo;

                                //決賽
                                var cell_final = sheet.Range[row_min + 3, cidx + 2];
                                cell_final.NumberValue = cfg.MaxEventNo;
                                cell_final.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Left;
                            }
                            else if (cfg.SheetCount == 2)
                            {
                                int row_min = sg.RowIndex;
                                int row_max = sg.RowIndex + 6;
                                //偶數頁往上畫，單數頁往下畫
                                if (sheetNo % 2 == 0)
                                {
                                    row_min = sg.RowIndex - 6;
                                    row_max = sg.RowIndex;
                                }

                                for (int j = row_min + 1; j <= row_max; j++)
                                {
                                    var cell2 = sheet.Range[j, cidx];
                                    cell2.Style.Borders[Spire.Xls.BordersLineType.EdgeRight].LineStyle = Spire.Xls.LineStyleType.Thin;
                                    //cell2.Style.Borders.Color = System.Drawing.Color.Red;
                                }

                                var cell3 = sheet.Range[row_min + 3, cidx + 1];
                                cell3.Style.Borders[Spire.Xls.BordersLineType.EdgeBottom].LineStyle = Spire.Xls.LineStyleType.Thin;

                                // //決賽
                                var cell_final = sheet.Range[row_min + 3, cidx];
                                cell_final.NumberValue = sg.EventNo;
                            }
                            else
                            {
                                // var cellEnd = sheet.Range[sg.RowIndex, cidx  + 1];
                                // cellEnd.NumberValue = sg.EventNo;
                                // cellEnd.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Left;

                                // string pos_s = cfg.Heads[cidx] + (sg.RowMin + 1);
                                // string pos_e = cfg.Heads[cidx] + sg.RowMax;
                                // string pos = pos_s + ":" + pos_e;
                                // sheet.Range[pos].Merge();
                                // sheet.Range[pos].NumberValue = sg.EventNo;
                                // sheet.Range[pos].HorizontalAlignment = Spire.Xls.HorizontalAlignType.Left;
                            }
                        }
                    }
                }
                else
                {
                    var cellUp = sheet.Range[sg.RowMin, cidx];
                    cellUp.Style.Borders[Spire.Xls.BordersLineType.EdgeBottom].LineStyle = Spire.Xls.LineStyleType.Thin;
                    //cellUp.Style.Borders.Color = System.Drawing.Color.Red;

                    var cellBtm = sheet.Range[sg.RowMax, cidx];
                    cellBtm.Style.Borders[Spire.Xls.BordersLineType.EdgeBottom].LineStyle = Spire.Xls.LineStyleType.Thin;
                    //cellBtm.Style.Borders.Color = System.Drawing.Color.Red;

                    for (int j = sg.RowMin + 1; j <= sg.RowMax; j++)
                    {
                        var cell = sheet.Range[j, cidx];
                        cell.Style.Borders[Spire.Xls.BordersLineType.EdgeRight].LineStyle = Spire.Xls.LineStyleType.Thin;
                        //cell.Style.Borders.Color = System.Drawing.Color.Red;
                    }

                    int rcntr = sg.RowMin + sg.RowMax;
                    var rmidx = (rcntr % 2 == 0) ? rcntr / 2 : rcntr / 2 + 1;

                    var cellEvent = sheet.Range[rmidx, cidx];
                    cellEvent.NumberValue = sg.EventNo;

                    string pos_s = cfg.Heads[cidx - 1] + (sg.RowMin + 1);
                    string pos_e = cfg.Heads[cidx - 1] + sg.RowMax;
                    string pos = pos_s + ":" + pos_e;
                    sheet.Range[pos].Merge();
                    sheet.Range[pos].NumberValue = sg.EventNo;

                }
            }
        }

        private List<TSignGroup> GetSigns(TSheet cfg, List<Item> items, int row_start, int col_start)
        {
            List<TSignGroup> source = new List<TSignGroup>();

            int round = 1;

            int span = 2;

            int rno = 1;

            int count = items.Count;

            int max_event_no = 0;

            for (int i = 0; i < count; i++)
            {
                Item item = items[i];
                string in_sign_id = item.getProperty("in_sign_id", "");
                string in_sign_no = item.getProperty("in_sign_no", "");
                string in_sign_foot = item.getProperty("in_sign_foot", "");

                string target_id = item.getProperty("target_id", "");
                string target_sign_no = item.getProperty("target_sign_no", "");
                string target_sign_foot = in_sign_foot == "1" ? "2" : "1";

                if (in_sign_id == "")
                {
                    //我是輪空
                    continue;
                }

                int gno = GetGroupNo(i);

                TSignGroup sg = GetAndAddGroup(source, round, gno);

                sg.ColIndex = col_start + round;

                if (in_sign_foot == "1")
                {
                    sg.RowMin = row_start + rno * span - 1;
                }
                else
                {
                    sg.RowMax = row_start + rno * span - 1;
                }

                if (target_id == "")
                {
                    sg.IsLong = true;
                    sg.RowIndex = row_start + rno * span - 1;
                }
                else if (sg.EventNo == 0)
                {
                    sg.EventNo = max_event_no + 1;
                    max_event_no++;
                }

                rno++;
            }

            if (source.Count > 0)
            {
                AppendNextRound(cfg, source, round, span, max_event_no);
            }

            return source;
        }

        private void AppendNextRound(TSheet cfg, List<TSignGroup> source, int last_round, int last_span, int last_event_no)
        {
            List<TSignGroup> entities = source.FindAll(x => x.Round == last_round);

            if (entities == null)
            {
                return;
            }

            if (entities.Count == 1)
            {
                TSignGroup semi_end_stage = source.Last();
                TSignGroup end_stage = GetAndAddGroup(source, last_round + 1, 1);
                end_stage.ColIndex = semi_end_stage.ColIndex + 1;
                end_stage.RowIndex = (semi_end_stage.RowMin + semi_end_stage.RowMax) / 2;
                end_stage.RowMin = semi_end_stage.RowMin;
                end_stage.RowMax = semi_end_stage.RowMax;
                end_stage.IsEnd = true;
                end_stage.EventNo = last_event_no + 1;
                return;
                // if (cfg.SheetCount == 2|| cfg.SheetCount == 4)
                // {
                //     TSignGroup semi_end_stage = source.Last();
                //     TSignGroup end_stage = GetAndAddGroup(source, last_round + 1, 1);
                //     end_stage.ColIndex = semi_end_stage.ColIndex + 1;
                //     end_stage.RowIndex = (semi_end_stage.RowMin + semi_end_stage.RowMax) / 2;
                //     end_stage.RowMin = semi_end_stage.RowMin;
                //     end_stage.RowMax = semi_end_stage.RowMax;
                //     end_stage.IsEnd = true;
                //     end_stage.EventNo = last_event_no + 1;
                //     return;
                // }
                // else
                // {
                //     return;
                // }
            }

            int count = entities.Count;
            int current_round = last_round + 1;
            int current_span = last_span * 2;

            int max_event_no = last_event_no;

            List<TSignGroup> currents = new List<TSignGroup>();

            for (int i = 0; i < count; i++)
            {
                TSignGroup last_stage = entities[i];

                int current_gno = GetGroupNo(i);
                int current_foot = i % 2 == 0 ? 1 : 2;

                TSignGroup sg = GetAndAddGroup(currents, current_round, current_gno);
                sg.ColIndex = last_stage.ColIndex + 1;

                if (last_stage.IsLong)
                {
                    if (current_foot == 1)
                    {
                        sg.RowMin = last_stage.RowIndex;
                        sg.UpIsLong = true;
                    }
                    else
                    {
                        sg.RowMax = last_stage.RowIndex;
                        sg.BtmIsLong = true;
                    }
                }
                else
                {
                    int RowCenter = (last_stage.RowMin + last_stage.RowMax) / 2;
                    if (current_foot == 1)
                    {
                        sg.RowMin = RowCenter;
                    }
                    else
                    {
                        sg.RowMax = RowCenter;
                    }
                }
            }

            //上輪皆為長籤腳
            for (int i = 0; i < currents.Count; i++)
            {
                TSignGroup sg = currents[i];
                if (sg.UpIsLong && sg.BtmIsLong)
                {
                    sg.EventNo = max_event_no + 1;
                    max_event_no++;
                }
            }

            //上輪其一為長籤腳
            for (int i = 0; i < currents.Count; i++)
            {
                TSignGroup sg = currents[i];
                if (sg.EventNo <= 0 && (sg.UpIsLong || sg.BtmIsLong))
                {
                    sg.EventNo = max_event_no + 1;
                    max_event_no++;
                }
            }

            //上輪無長籤腳
            for (int i = 0; i < currents.Count; i++)
            {
                TSignGroup sg = currents[i];
                if (sg.EventNo <= 0)
                {
                    sg.EventNo = max_event_no + 1;
                    max_event_no++;
                }
            }

            for (int i = 0; i < currents.Count; i++)
            {
                TSignGroup sg = currents[i];
                source.Add(sg);
            }

            AppendNextRound(cfg, source, current_round, current_span, max_event_no);
        }

        private TSignGroup GetAndAddGroup(List<TSignGroup> list, int round, int gno)
        {
            TSignGroup search = list.Find(x => x.Round == round && x.GNo == gno);

            if (search == null)
            {
                search = new TSignGroup
                {
                    Round = round,
                    GNo = gno,
                };

                list.Add(search);
            }

            return search;
        }

        private int GetGroupNo(int idx)
        {
            int val = idx + 1;
            if (val % 2 == 0)
            {
                return val / 2;
            }
            else
            {
                return val / 2 + 1;
            }
        }

        private class TSheet
        {
            public int WsRow { get; set; }

            public int WsCol { get; set; }

            public int LineRow { get; set; }

            public int LineCol { get; set; }

            public string[] Heads { get; set; }

            public string Font { get; set; }

            public Dictionary<int, List<int>> EventNoMap { get; set; }

            public int SheetCount { get; set; }

            public int MaxEventNo { get; set; }

            //三四名場次
            public int TFEventNo { get; set; }

            public List<string> Sheets { get; set; }
        }

        private class TSignGroup
        {
            /// <summary>
            /// 場次代碼
            /// </summary>
            public int EventNo { get; set; }

            /// <summary>
            /// 回合數
            /// </summary>
            public int Round { get; set; }

            /// <summary>
            /// 群組序號
            /// </summary>
            public int GNo { get; set; }

            /// <summary>
            /// 下籤腳
            /// </summary>
            public int BNo { get; set; }

            /// <summary>
            /// 是否為長籤腳
            /// </summary>
            public bool IsLong { get; set; }

            /// <summary>
            /// 是否為結束節點
            /// </summary>
            public bool IsEnd { get; set; }

            public int RowMin { get; set; }

            public int RowMax { get; set; }

            public int RowIndex { get; set; }

            public int ColIndex { get; set; }

            /// <summary>
            /// 上節點為長籤腳
            /// </summary>
            public bool UpIsLong { get; set; }

            /// <summary>
            /// 下節點為長籤腳
            /// </summary>
            public bool BtmIsLong { get; set; }
        }

        /// <summary>
        /// 取得比賽日期
        /// </summary>
        private string GetMeetingDateSE(Item itmMeeting)
        {
            string in_date_s = itmMeeting.getProperty("in_date_s", "");
            string in_date_e = itmMeeting.getProperty("in_date_e", "");

            if (in_date_s != "" && in_date_e != "")
            {
                DateTime dtS = Convert.ToDateTime(in_date_s).AddHours(-8);
                DateTime dtE = Convert.ToDateTime(in_date_e).AddHours(-8);
                if (dtS.Year == dtE.Year)
                {
                    int chinese_year = dtS.Year - 1911;
                    if (dtS.Month == dtE.Month)
                    {
                        return chinese_year + "年" + dtS.Month + "月" + dtS.Day + "~" + dtE.Day;
                    }
                    else
                    {

                        return chinese_year + "年" + dtS.ToString("MM/dd") + "~" + dtE.ToString("MM/dd");
                    }
                }
                else
                {
                    int chinese_year1 = dtS.Year - 1911;
                    int chinese_year2 = dtE.Year - 1911;
                    return chinese_year1 + "/" + dtS.ToString("MM/dd") + "~" + chinese_year2 + "/" + dtE.ToString("MM/dd");
                }
            }
            else if (in_date_s != "")
            {
                return Convert.ToDateTime(in_date_s).AddHours(8).ToString("yyyy/MM/dd");
            }
            else if (in_date_e != "")
            {
                return "~" + Convert.ToDateTime(in_date_e).AddHours(8).ToString("yyyy/MM/dd");
            }
            else
            {
                return "";
            }
        }

        #region 查詢資料

        private string ClearOption(string value)
        {
            if (value == "全部" || value == "請選擇" || value == "ALL")
            {
                return "";
            }
            else
            {
                return value.Trim();
            }
        }

        /// <summary>
        /// 取得賽事組別資料
        /// </summary>
        private Item GetPrograms(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            string meeting_id = itmReturn.getProperty("meeting_id", "");
            string in_l1 = ClearOption(itmReturn.getProperty("in_l1", ""));
            string in_l2 = ClearOption(itmReturn.getProperty("in_l2", ""));
            string in_l3 = ClearOption(itmReturn.getProperty("in_l3", ""));
            string program_id = itmReturn.getProperty("program_id", "");

            string l1_condition = in_l1 != "" ? "AND in_l1 = N'" + in_l1 + "'" : "";
            string l2_condition = in_l2 != "" ? "AND in_l2 = N'" + in_l2 + "'" : "";
            string l3_condition = in_l3 != "" ? "AND in_l3 = N'" + in_l3 + "'" : "";
            string program_condition = program_id != "" ? "AND id = '" + program_id + "'" : "AND ISNULL(in_program, '') = ''";

            string sql = @"
                SELECT
                    *
                FROM
                    IN_MEETING_PROGRAM WITH(NOLOCK)
                WHERE
                    in_meeting = '{#meeting_id}'
                    {#l1_condition}
                    {#l2_condition}
                    {#l3_condition}
                    {#program_condition}
                    AND ISNULL(in_team_count, 0) > 1
                ORDER BY
                    in_sort_order
                ";

            sql = sql.Replace("{#meeting_id}", meeting_id)
                .Replace("{#l1_condition}", l1_condition)
                .Replace("{#l2_condition}", l2_condition)
                .Replace("{#l3_condition}", l3_condition)
                .Replace("{#program_condition}", program_condition);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return inn.applySQL(sql);
        }

        /// <summary>
        /// 取得選手資料
        /// </summary>
        private List<Item> GetMTeamList(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmProgram)
        {
            string meeting_id = itmProgram.getProperty("in_meeting", "").Trim();
            string program_id = itmProgram.getProperty("id", "").Trim();

            string sql = @"
                SELECT
                    t1.in_tree_id
                    , t1.in_round_id
                    , t2.in_sign_foot
                    , t2.in_sign_no
                    , t3.id AS 'in_sign_id'
                    , t3.in_current_org
                    , t3.in_short_org
                    , t3.map_short_org
                    , t3.in_team
                    , t3.in_name
                    , t3.in_sno
                    , t3.in_names
                    , t3.in_judo_no
                    , t2.in_target_no AS 'target_sign_no'
                    , t4.id           AS 'target_id'
                FROM
                    IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
                    IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK)
                    ON t2.source_id = t1.id
                LEFT OUTER JOIN
                    VU_MEETING_PTEAM t3 WITH(NOLOCK)
                    ON t3.source_id = t1.source_id
                    AND t3.in_sign_no = t2.in_sign_no
                    AND ISNULL(t3.in_type, '') <> 's'
                LEFT OUTER JOIN
                    VU_MEETING_PTEAM t4 WITH(NOLOCK)
                    ON t4.source_id = t1.source_id
                    AND t4.in_sign_no = t2.in_target_no
                    AND ISNULL(t4.in_type, '') <> 's'
                WHERE
                    t1.source_id = '{#program_id}'
                    AND t1.in_tree_name = N'main'
                    AND t1.in_round = 1
                    AND ISNULL(t1.in_type, '') <> 's'
                ORDER BY
                    t1.in_round_id
                    , t2.in_sign_foot
                    , t2.in_sign_no
                ";

            sql = sql.Replace("{#program_id}", program_id);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item items = inn.applySQL(sql);

            List<Item> list = new List<Item>();

            for (int i = 0; i < items.getItemCount(); i++)
            {
                Item item = items.getItemByIndex(i);
                list.Add(item);
            }

            return list;
        }

        /// <summary>
        /// 取得選手資料(格式組)
        /// </summary>
        private List<Item> GetMTeamList2(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmProgram)
        {
            string meeting_id = itmProgram.getProperty("in_meeting", "").Trim();
            string program_id = itmProgram.getProperty("id", "").Trim();

            string sql = @"
                SELECT
                    t1.in_sign_no
                    , t1.in_sign_no AS 'in_sign_id'
                    , t1.map_short_org
                    , t1.in_name
                    , t1.in_judo_no
                    , t3.in_exe_a1
                    , t3.in_exe_a2
                    , t3.in_exe_a3
                FROM
                    VU_MEETING_PTEAM t1 WITH(NOLOCK)
                INNER JOIN
                    IN_MEETING_PROGRAM t2 WITH(NOLOCK)
                    ON t2.id = t1.source_id
                INNER JOIN
                    IN_MEETING_USER t3 WITH(NOLOCK)
                    ON t3.source_id = t2.in_meeting
                    AND t3.in_l1 = t2.in_l1
                    AND t3.in_l2 = t2.in_l2
                    AND t3.in_l3 = t2.in_l3
                    AND t3.in_index = t1.in_index
                WHERE
                    t1.source_id = '{#program_id}'
                ORDER BY
                    CAST(ISNULL(t1.in_judo_no, '0') AS INT)
                ";

            sql = sql.Replace("{#program_id}", program_id);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item items = inn.applySQL(sql);

            List<Item> list = new List<Item>();

            for (int i = 0; i < items.getItemCount(); i++)
            {
                Item item = items.getItemByIndex(i);
                list.Add(item);
            }

            return list;
        }

        /// <summary>
        /// 取得選手資料(循環賽)
        /// </summary>
        private List<Item> GetMTeamList3(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmProgram)
        {
            string meeting_id = itmProgram.getProperty("in_meeting", "").Trim();
            string program_id = itmProgram.getProperty("id", "").Trim();

            string sql = @"
                SELECT
                    t1.in_sign_no
                    , t1.in_sign_no AS 'in_sign_id'
                    , t1.map_short_org
                    , t1.in_name
                    , t1.in_judo_no
                FROM
                    VU_MEETING_PTEAM t1 WITH(NOLOCK)
                INNER JOIN
                    IN_MEETING_PROGRAM t2 WITH(NOLOCK)
                    ON t2.id = t1.source_id
                WHERE
                    t1.source_id = '{#program_id}'
                    AND ISNULL(t1.in_type, '') <> 's'
                ORDER BY
                    CAST(ISNULL(t1.in_judo_no, '0') AS INT)
                ";

            sql = sql.Replace("{#program_id}", program_id);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item items = inn.applySQL(sql);

            List<Item> list = new List<Item>();

            for (int i = 0; i < items.getItemCount(); i++)
            {
                Item item = items.getItemByIndex(i);
                list.Add(item);
            }

            return list;
        }

        /// <summary>
        /// 取得匯出路徑
        /// </summary>
        private Item GetXlsPaths(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string in_name)
        {
            Item itmResult = inn.newItem();

            //Excel 匯出路徑 (from 原創.設定參數)
            string sql = @"
                SELECT 
                    t2.in_name AS 'variable', t1.in_name, t1.in_value 
                FROM 
                    In_Variable_Detail t1 WITH(NOLOCK)
                INNER JOIN 
                    In_Variable t2 WITH(NOLOCK) ON t2.ID = t1.SOURCE_ID 
                WHERE 
                    t2.in_name = N'meeting_excel'  
                    AND t1.in_name IN (N'export_path', N'{#in_name}')
                ";

            sql = sql.Replace("{#in_name}", in_name);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item items = inn.applySQL(sql);

            if (items.isError() || items.getItemCount() < 1)
            {
                throw new Exception("未設定匯出路徑 _# " + in_name);
            }

            for (int i = 0; i < items.getItemCount(); i++)
            {
                Item item = items.getItemByIndex(i);
                string xls_name = item.getProperty("in_name", "");
                string xls_value = item.getProperty("in_value", "");

                if (xls_name == "export_path")
                {
                    itmResult.setProperty("export_path", xls_value);
                }
                else
                {
                    itmResult.setProperty("template_path", xls_value);
                }
            }

            return itmResult;
        }

        private TXls GetExcelInfo(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmMeeting, Item itmReturn)
        {
            string in_l1 = itmReturn.getProperty("in_l1", "");

            Item itmPath = GetXlsPaths(CCO, strMethodName, inn, "match_list_path");

            //比賽名稱
            string in_title = itmMeeting.getProperty("keyed_name", "");
            string export_path = itmPath.getProperty("export_path", "").TrimEnd('\\');
            string ext_name = ".xlsx";
            string xls_name = in_title + "_對戰表_" + DateTime.Now.ToString("yyyyMMdd_HHmmss");

            if (in_l1 == "ALL" || in_l1 == "全部" || in_l1 == "請選擇")
            {
                xls_name = in_title + "_對戰表(全)_" + DateTime.Now.ToString("MMdd_HHmmss");
            }
            else if (in_l1 != "")
            {
                xls_name = in_title + "_" + in_l1 + "_對戰表_" + DateTime.Now.ToString("MMdd_HHmmss");
            }

            string xls_file = export_path + "\\" + xls_name + ext_name;
            string xls_url = xls_name + ext_name;

            return new TXls
            {
                xls_source = itmPath.getProperty("template_path", ""),
                xls_file = xls_file,
                xls_url = xls_url,
            };

        }

        #endregion 查詢資料

        #region Excel

        //設定物件與資料列
        private void SetItemCell(Spire.Xls.Worksheet sheet, TSheet cfg, int wsRow, int wsCol, Item item, string[] fields, string[] formats)
        {
            int field_count = fields.Length;
            for (int i = 0; i < field_count; i++)
            {
                string field_name = fields[i];
                string field_format = formats[i];
                string filed_pos = cfg.Heads[wsCol + i] + wsRow;

                Spire.Xls.CellRange range = sheet.Range[filed_pos];

                if (field_name == "")
                {
                    //SetBodyCell(range, cfg, "", field_format);
                }
                else
                {
                    string value = item.getProperty(field_name, "");
                    if (value != "")
                    {
                        SetBodyCell(range, cfg, value, field_format);
                    }
                }
            }
        }

        //設定物件與資料列
        private void SetItemCell2(Spire.Xls.Worksheet sheet, TSheet cfg, int wsRow, int wsCol, string value, string format, bool need_gray_color = false, bool need_split_line = false)
        {
            string filed_pos = cfg.Heads[wsCol] + wsRow;
            Spire.Xls.CellRange range = sheet.Range[filed_pos];

            if (value != "")
            {
                SetBodyCell(range, cfg, value, format);
            }

            if (need_gray_color)
            {
                range.Style.Color = System.Drawing.Color.Gray;

            }

            if (need_split_line)
            {
                //左上右下斜線
                range.Borders[Spire.Xls.BordersLineType.DiagonalDown].LineStyle = Spire.Xls.LineStyleType.Thin;
            }

            // range.Borders[Spire.Xls.BordersLineType.EdgeLeft].LineStyle = Spire.Xls.LineStyleType.Thin;
            // range.Borders[Spire.Xls.BordersLineType.EdgeRight].LineStyle = Spire.Xls.LineStyleType.Thin;
            // range.Borders[Spire.Xls.BordersLineType.EdgeTop].LineStyle = Spire.Xls.LineStyleType.Thin;
            // range.Borders[Spire.Xls.BordersLineType.EdgeBottom].LineStyle = Spire.Xls.LineStyleType.Thin;

            range.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
        }

        //設定資料列
        private void SetBodyCell(Spire.Xls.CellRange range, TSheet cfg, string value = "", string format = "")
        {
            switch (format)
            {
                case "center":
                    range.Text = value;
                    range.Style.Font.FontName = cfg.Font;
                    break;

                case "yyyy/MM/dd":
                case "yyyy/MM/dd HH:mm":
                    range.Text = GetDateTimeValue(value, format);
                    range.Style.Font.FontName = cfg.Font;
                    break;

                case "$ #,##0":
                    range.Text = value;
                    range.Style.Font.FontName = cfg.Font;
                    break;

                case "text":
                    range.Text = value;
                    range.Style.Font.FontName = cfg.Font;
                    break;

                default:
                    range.Text = value;
                    range.Style.Font.FontName = cfg.Font;
                    break;
            }
        }

        //設定空白欄 (有格線)
        private void SetLineCell(ClosedXML.Excel.IXLCell cell)
        {
            // 	cell.Style.Border.SetRightBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            // 	cell.Style.Border.SetTopBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            // 	cell.Style.Border.SetBottomBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            // 	cell.Style.Border.SetLeftBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
        }

        //轉換日期
        private string GetDateTimeValue(string value, string format)
        {
            if (value == "") return "";
            DateTime dt = Convert.ToDateTime(value);
            return dt.ToString(format);
        }

        //設定標題列
        private void SetHeadCell(ClosedXML.Excel.IXLCell cell, string title)
        {
            //     cell.Value = title;

            //     cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
            //     cell.Style.Font.Bold = true;
            //     cell.DataType = ClosedXML.Excel.XLDataType.Text;
            //     cell.Style.Fill.BackgroundColor = head_bg_color;
            //     cell.Style.Font.FontColor = ClosedXML.Excel.XLColor.White;

            // 	cell.Style.Border.SetRightBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            // 	cell.Style.Border.SetTopBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            // 	cell.Style.Border.SetBottomBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            // 	cell.Style.Border.SetLeftBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
        }

        //資料 Cell 上色
        private void SetBodyCellColor(ClosedXML.Excel.IXLWorksheet sheet, int wsRow, int wsCol_s, int wsCol_e, ClosedXML.Excel.XLColor color)
        {
            // for (int i = wsCol_s; i <= wsCol_e; i++)
            // {
            //     ClosedXML.Excel.IXLCell cell = sheet.Cell(wsRow, i);
            //     cell.Style.Fill.BackgroundColor = color;
            // }
        }

        #endregion

        private class TXls
        {
            public string xls_source { get; set; }

            public string xls_file { get; set; }

            public string xls_url { get; set; }
        }

        //是否為多人對打
        private bool IsMultiPlayers(string value)
        {
            if (value.Contains("團體")) return true;
            if (value.Contains("混雙")) return true;
            if (value.Contains("雙人")) return true;
            if (value.Contains("三人")) return true;
            if (value.Contains("五人")) return true;
            if (value.Contains("雙打")) return true;
            if (value.Contains("混合雙打")) return true;

            return false;
        }

        private int GetIntVal(string value, int def = 0)
        {
            int result = def;
            Int32.TryParse(value, out result);
            return result;
        }

        private Dictionary<int, List<int>> GetEventNoMap(List<TSignGroup> sgs, int sheetCount)
        {
            Dictionary<int, List<int>> result = new Dictionary<int, List<int>>();

            int r_cidx1 = sgs.Max(x => x.ColIndex);
            int r_cidx2 = r_cidx1 - 1;
            int r_cidx3 = r_cidx1 - 2;

            for (int i = 0; i < sgs.Count; i++)
            {
                TSignGroup sg = sgs[i];
                if (sg.EventNo <= 0 || sg.ColIndex <= 0)
                {
                    continue;
                }

                List<int> codes = null;
                if (result.ContainsKey(sg.ColIndex))
                {
                    codes = result[sg.ColIndex];
                }
                else
                {
                    codes = new List<int>();
                    result.Add(sg.ColIndex, codes);
                }

                if (!codes.Contains(sg.EventNo))
                {
                    codes.Add(sg.EventNo);

                    if (sheetCount == 4)
                    {
                        if (sg.ColIndex == r_cidx1)
                        {
                        }
                        else if (sg.ColIndex == r_cidx2)
                        {
                            //要個四個面取值
                            codes.Add(sg.EventNo);
                            codes.Add(sg.EventNo);
                            codes.Add(sg.EventNo);
                        }
                        else if (sg.ColIndex == r_cidx3)
                        {
                            //要個兩個面取值
                            codes.Add(sg.EventNo);
                        }
                    }
                    else if (sheetCount == 2)
                    {
                        if (sg.ColIndex == r_cidx2)
                        {
                            codes.Add(sg.EventNo);
                        }
                    }
                }
            }
            return result;
        }

        private void BindEventNos(List<TSignGroup> sgs, TSheet cfg)
        {
            for (int i = 0; i < sgs.Count; i++)
            {
                TSignGroup sg = sgs[i];
                if (sg.EventNo > 0)
                {
                    sg.EventNo = PopEventNo(cfg.EventNoMap, sg.ColIndex);
                }
            }
        }

        private int PopEventNo(Dictionary<int, List<int>> codes, int cidx)
        {
            int result = 0;

            if (codes.ContainsKey(cidx))
            {
                List<int> list = codes[cidx];
                if (list.Count > 0)
                {
                    result = list[0];
                    list.RemoveAt(0);
                }
            }
            return result;
        }

        //檢查籤號
        private void CheckSignNos(List<Item> itmTeams, string in_team_count, string program_display)
        {
            // List<string> SignNos = GetSignNos(in_team_count);

            // for (int i = 0; i < itmTeams.Count; i++)
            // {
            //     Item item = itmTeams[i];
            //     string in_sign_id = item.getProperty("in_sign_id", "");
            //     string in_sign_no = item.getProperty("in_sign_no", "");
            //     if (in_sign_id != "")
            //     {
            //         SignNos.Remove(in_sign_no);
            //     }
            // }

            // if (SignNos.Count > 0)
            // {
            //     throw new Exception(program_display + " 選手籤號有誤: " + string.Join("、", SignNos));
            // }
        }

        //取得籤號表
        private List<string> GetSignNos(string in_team_count)
        {
            List<string> result = new List<string>();

            int team_count = GetIntVal(in_team_count);

            for (int i = 1; i <= team_count; i++)
            {
                result.Add(i.ToString());
            }

            return result;
        }

        private string GetCodeName(string value)
        {
            switch (value)
            {
                case "1/2": return "A";
                case "2/2": return "B";

                case "1/4": return "A";
                case "2/4": return "B";
                case "3/4": return "C";
                case "4/4": return "D";

                default: return "";
            }
        }

        private string GetSheetName(TSheet cfg, string program_short_name, string program_display, string sheetNo = "")
        {
            string sheetName = program_short_name;

            if (sheetName == "")
            {
                sheetName = ClearSheetName(program_display);
            }

            if (cfg.SheetCount > 1)
            {
                sheetName = sheetName + "_" + sheetNo;
            }

            if (cfg.Sheets.Contains(sheetName))
            {
                throw new Exception(program_display + " sheet name 重複");
            }
            else
            {
                cfg.Sheets.Add(sheetName);
            }

            return sheetName;
        }

        private string ClearSheetName(string value)
        {
            return value.Replace("/", "_")
                .Replace("\\", "_")
                .Replace("?", "_")
                .Replace("[", "【")
                .Replace("]", "】")
                .Replace("-", "_")
                .Replace(" ", "_")
                .Replace("：", "_")
                .Replace(":", "_");
        }
    }
}