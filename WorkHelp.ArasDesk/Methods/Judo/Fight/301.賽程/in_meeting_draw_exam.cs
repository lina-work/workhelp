﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Judo.Fight
{
    public class in_meeting_draw_exam : Item
    {
        public in_meeting_draw_exam(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
    目的: 報名異常檢查
    日誌: 
        - 2023-10-26: 創建 (lina)
*/

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "in_meeting_draw_exam";

            var itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                meeting_id = itmR.getProperty("meeting_id", ""),
                scene = itmR.getProperty("scene", ""),
            };

            switch (cfg.scene)
            {
                case "page":
                    Examination(cfg, itmR);
                    break;
            }

            return itmR;
        }

        //健康檢查
        private void Examination(TConfig cfg, Item itmReturn)
        {
            Item itmMeeting = cfg.inn.applySQL("SELECT in_title, in_robin_player FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            itmReturn.setProperty("in_title", itmMeeting.getProperty("in_title", ""));

            cfg.robin_player = GetInt(itmMeeting.getProperty("in_robin_player", "0"));

            var rows = new List<TRow>();

            //與會者有，Program 沒有
            InMtUserNotInMtProgram(cfg, rows);
            //Program 有，與會者 沒有
            InMtProgramNotInMtUser(cfg, rows);
            //檢查參賽人數/隊伍數
            CheckTeamCount(cfg, rows);
            //只有一組
            OnlyOneTeam(cfg, rows);
            //檢查種子籤
            CheckSeedDrawNo(cfg, rows);
            //檢查組別是否未抽籤
            CheckProgramDrawStatus(cfg, rows);
            //檢查無籤號
            CheckNoDrawNoSetting(cfg, rows);
            //檢查籤號重複
            CheckRepeatDrawNoSetting(cfg, rows);
            //檢查籤號異常
            CheckUnusuallyDrawNo(cfg, rows);
            //循環賽異常
            CheckRobinStatus(cfg, rows);

            var body = new StringBuilder();
            body.Append("<ul>");
            foreach (var row in rows)
            {
                body.Append("<li class='inno-title'>");
                body.Append(row.desc);
                body.Append("  <ul>");
                body.Append(string.Join(" ", row.messages.Select(x => "<li class='" + x.css + "'> - " + x.message + "</li>")));
                body.Append("  </ul>");
                body.Append("</li>");
            }
            body.Append("</ul>");

            itmReturn.setProperty("inn_check_result", body.ToString());
        }

        //與會者有，Program 沒有
        private void InMtUserNotInMtProgram(TConfig cfg, List<TRow> rows)
        {
            var no = (rows.Count + 1);
            var row = new TRow { desc = no + ". [與會者]有資料，[籤表組別檔]沒有資料: 請[檢查異動並重建]", messages = new List<TMessage>(), isOK = false };
            rows.Add(row);

            var sql = @"
                SELECT 
	                t1.item_name
	                , t1.item_count
	                , t2.in_name
	                , t2.in_team_count
                FROM
	                VU_MEETING_USER_TEAMCOUNT t1
                LEFT OUTER JOIN
	                IN_MEETING_PROGRAM t2 WITH(NOLOCK)
	                ON t2.in_meeting = t1.source_id
	                AND t2.in_l1 = t1.in_l1
	                AND t2.in_l2 = t1.in_l2
	                AND t2.in_l3 = t1.in_l3
                WHERE
	                t1.source_id = '{#meeting_id}'
	                AND t2.id IS NULL
	            ORDER BY
                    t1.in_l1
                    , t1.in_l2
                    , t1.in_l3
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();

            if (count <= 0)
            {
                row.isOK = true;
                AddMsgOK(row);
                return;
            }

            if (count > 10)
            {
                AddMsgError(row, "檢查結果: 共有 " + count + " 組異常");
                return;
            }

            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var name = item.getProperty("item_name");
                var item_count = item.getProperty("item_count");
                var message = name + " (" + item_count + ")";
                AddMsgError(row, message);
            }
        }

        //Program 有，與會者 沒有
        private void InMtProgramNotInMtUser(TConfig cfg, List<TRow> rows)
        {
            var no = (rows.Count + 1);
            var row = new TRow { desc = no + ". [籤表組別檔]有資料，[與會者]沒有資料: 請刪除[籤表組別]", messages = new List<TMessage>(), isOK = false };
            rows.Add(row);

            var sql = @"
                SELECT 
	                t1.id
	                , t1.in_name
	                , t1.in_team_count
	                , t2.item_count
	                , t2.item_name
                FROM
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                LEFT OUTER JOIN
	                VU_MEETING_USER_TEAMCOUNT t2 
	                ON t2.source_id = t1.in_meeting
	                AND t2.in_l1 = t1.in_l1
	                AND t2.in_l2 = t1.in_l2
	                AND t2.in_l3 = t1.in_l3
                WHERE
	                t1.in_meeting = '{#meeting_id}'
	                AND t2.item_name IS NULL
                ORDER BY
	                t1.in_sort_order
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();

            if (count <= 0)
            {
                row.isOK = true;
                AddMsgOK(row);
                return;
            }

            //if (count > 10)
            //{
            //    AddMsgError(row, "檢查結果: 共有 " + count + " 組異常");
            //    return;
            //}

            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var name = item.getProperty("in_name");
                var item_count = item.getProperty("in_team_count");
                var message = name + " (" + item_count + ")";
                AddMsgError(row, message);
            }
        }

        //檢查參賽人數/隊伍數
        private void CheckTeamCount(TConfig cfg, List<TRow> rows)
        {
            var no = (rows.Count + 1);
            var row = new TRow { desc = no + ". 比對參賽人數/隊伍數: 請[檢查異動並重建]", messages = new List<TMessage>(), isOK = false };
            rows.Add(row);

            var sql = @"
                SELECT 
	                t1.id
	                , t1.in_name
	                , t1.in_team_count
	                , t2.item_count
	                , t2.item_name
                FROM
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
	                VU_MEETING_USER_TEAMCOUNT t2 
	                ON t2.source_id = t1.in_meeting
	                AND t2.in_l1 = t1.in_l1
	                AND t2.in_l2 = t1.in_l2
	                AND t2.in_l3 = t1.in_l3
                WHERE
	                t1.in_meeting = '{#meeting_id}'
	                AND ISNULL(t1.in_team_count, 0) <> t2.item_count
                ORDER BY
	                t1.in_sort_order
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();

            if (count <= 0)
            {
                row.isOK = true;
                AddMsgOK(row);
                return;
            }

            //if (count > 20)
            //{
            //    AddMsgError(row, "檢查結果: 共有 " + count + " 組異常");
            //    return;
            //}

            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var name = item.getProperty("in_name");
                var c1 = item.getProperty("in_team_count");
                var c2 = item.getProperty("item_count");
                var message = name + " (與會者 = " + c2 + ", 籤組檔 = " + c1 + ")";
                AddMsgError(row, message);
            }
        }

        //只有一組
        private void OnlyOneTeam(TConfig cfg, List<TRow> rows)
        {
            var no = (rows.Count + 1);
            var row = new TRow { desc = no + ". 只有一組: 請確認[進行合併或取消]", messages = new List<TMessage>(), isOK = false };
            rows.Add(row);

            var sql = @"
                SELECT 
	                t1.item_name
	                , t1.item_count
	                , t2.in_name
	                , t2.in_team_count
                FROM
	                VU_MEETING_USER_TEAMCOUNT t1
                LEFT OUTER JOIN
	                IN_MEETING_PROGRAM t2 WITH(NOLOCK)
	                ON t2.in_meeting = t1.source_id
	                AND t2.in_l1 = t1.in_l1
	                AND t2.in_l2 = t1.in_l2
	                AND t2.in_l3 = t1.in_l3
                WHERE
	                t1.source_id = '{#meeting_id}'
	                AND t1.item_count = 1
                ORDER BY
	                t2.in_sort_order
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();

            if (count <= 0)
            {
                row.isOK = true;
                AddMsgOK(row);
                return;
            }

            //if (count > 20)
            //{
            //    AddMsgError(row, "檢查結果: 共有 " + count + " 組異常");
            //    return;
            //}

            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var name = item.getProperty("in_name");
                var item_count = item.getProperty("item_count");
                var message = name + " (" + item_count + ")";
                AddMsgError(row, message);
            }
        }

        //檢查種子籤
        private void CheckSeedDrawNo(TConfig cfg, List<TRow> rows)
        {
            var no = (rows.Count + 1);
            var row = new TRow { desc = no + ". 檢查種子籤，請確認本次賽制是否設定種子籤", messages = new List<TMessage>(), isOK = false };
            rows.Add(row);

            row.isOK = true;
            AddMsgOK(row);
            return;
        }

        //檢查組別是否未抽籤
        private void CheckProgramDrawStatus(TConfig cfg, List<TRow> rows)
        {
            var no = (rows.Count + 1);
            var row = new TRow { desc = no + ". 檢查組別是否未抽籤", messages = new List<TMessage>(), isOK = false };
            rows.Add(row);

            var sql = @"
                SELECT
	                t1.id
	                , t1.in_name
	                , t1.in_team_count
                FROM
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                WHERE
	                t1.in_meeting = '{#meeting_id}'
	                AND ISNULL(t1.in_team_count, 0) > 0
	                AND t1.in_sign_time IS NULL
                ORDER BY
	                t1.in_sort_order
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();

            if (count <= 0)
            {
                row.isOK = true;
                AddMsgOK(row);
                return;
            }

            if (count > 30)
            {
                AddMsgError(row, "檢查結果: 共有 " + count + " 組尚未抽籤");
                return;
            }

            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var name = item.getProperty("in_name");
                var item_count = item.getProperty("in_team_count");
                var message = name + " (" + item_count + ")";
                AddMsgError(row, message);
            }
        }

        //檢查無籤號
        private void CheckNoDrawNoSetting(TConfig cfg, List<TRow> rows)
        {
            var no = (rows.Count + 1);
            var row = new TRow { desc = no + ". 選手/隊伍無籤號 (已完成抽籤)", messages = new List<TMessage>(), isOK = false };
            rows.Add(row);

            var sql = @"
                SELECT
	                t1.in_name
	                , t1.in_team_count
	                , t2.in_current_org
	                , t2.in_team
	                , t2.in_names
	                , t2.in_sign_no
                FROM
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PTEAM t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                WHERE
	                t1.in_meeting = '{#meeting_id}'
	                AND t1.in_sign_time IS NOT NULL
	                AND ISNULL(t2.in_sign_no, '') IN ('', '0')
					AND ISNULL(t2.in_type, '') IN ('', 'p')
                ORDER BY
	                t1.in_sort_order
	                , t2.in_city_no
	                , t2.in_stuff_b1
	                , t2.in_current_org
	                , t2.in_team
	                , t2.in_names
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();

            if (count <= 0)
            {
                row.isOK = true;
                AddMsgOK(row);
                return;
            }

            //if (count > 20)
            //{
            //    AddMsgError(row, "檢查結果: 共有 " + count + " 組異常");
            //    return;
            //}

            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var name = item.getProperty("in_name");
                var item_count = item.getProperty("in_team_count");
                var in_current_org = item.getProperty("in_current_org");
                var in_team = item.getProperty("in_team");
                var in_names = item.getProperty("in_names");

                var message = name + " (" + item_count + ")"
                    + ": " + in_current_org + " - " + in_names + in_team;

                AddMsgError2(row, message);
            }
        }

        //檢查籤號重複
        private void CheckRepeatDrawNoSetting(TConfig cfg, List<TRow> rows)
        {
            var no = (rows.Count + 1);
            var row = new TRow { desc = no + ". 選手/隊伍籤號重複 (已完成抽籤)", messages = new List<TMessage>(), isOK = false };
            rows.Add(row);

            var sql = @"
                SELECT
	                t1.in_name
	                , t1.in_team_count
	                , t2.in_current_org
	                , t2.in_team
	                , t2.in_names
	                , t2.in_sign_no
	                , t2.in_judo_no
                FROM
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PTEAM t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
				INNER JOIN
					[VU_MEETING_DRAWNO_REPEAT] t3
					ON t3.in_meeting = t2.in_meeting
					AND t3.id = t2.source_id
					AND t3.in_judo_no = t2.in_judo_no
					AND ISNULL(t2.in_type, '') IN ('', 'p')
                WHERE
	                t1.in_meeting = '{#meeting_id}'
	                AND t1.in_sign_time IS NOT NULL
					AND ISNULL(t2.in_type, '') IN ('', 'p')
                ORDER BY
	                t1.in_sort_order
	                , t2.in_judo_no
	                , t2.in_city_no
	                , t2.in_stuff_b1
	                , t2.in_current_org
	                , t2.in_team
	                , t2.in_names
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();

            if (count <= 0)
            {
                row.isOK = true;
                AddMsgOK(row);
                return;
            }

            //if (count > 20)
            //{
            //    AddMsgError(row, "檢查結果: 共有 " + count + " 組異常");
            //    return;
            //}

            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var name = item.getProperty("in_name");
                var item_count = item.getProperty("in_team_count");
                var in_current_org = item.getProperty("in_current_org");
                var in_team = item.getProperty("in_team");
                var in_names = item.getProperty("in_names");
                var in_judo_no = item.getProperty("in_judo_no");

                var message = name + " (籤號 " + in_judo_no + ")"
                    + ": " + in_current_org + " - " + in_names + in_team;

                AddMsgError2(row, message);
            }
        }

        private List<int> SortNoArray(int value)
        {
            var result = new List<int>();
            for (int i = 1; i <= value; i++)
            {
                result.Add(i);
            }
            return result;
        }

        //檢查籤號異常
        private void CheckUnusuallyDrawNo(TConfig cfg, List<TRow> rows)
        {
            var no = (rows.Count + 1);
            var row = new TRow { desc = no + ". 籤號異常 (已完成抽籤)", messages = new List<TMessage>(), isOK = false };
            rows.Add(row);

            var programs = ResetProgramTeams(cfg);

            for (var i = 0; i < programs.Count; i++)
            {
                var program = programs[i];
                var svc = new InnSport.Core.Services.Impl.JudoSportService();
                var cnt = program.NumList.Count;

                var ns1 = program.NumList.OrderBy(x => x).ToList();
                var ns2 = program.isSpecial || cnt <= program.robinCount
                    ? SortNoArray(cnt)
                    : svc.NoListByOrder(cnt).ToList().GetRange(0, cnt);

                for (var j = ns1.Count - 1; j >= 0; j--)
                {
                    var n = ns1[j];
                    var existed = ns2.Find(x => x == n);
                    if (existed >= 1)
                    {
                        ns1.RemoveAt(j);
                        ns2.Remove(existed);
                    }
                }

                if (ns1.Count <= 0 && ns2.Count <= 0) continue;
                foreach (var n in ns2)
                {
                    var message = program.name + " 缺少籤號: " + n;
                    AddMsgError2(row, message);
                }

                foreach (var n in ns1)
                {
                    var item = program.map[n];

                    var name = item.getProperty("in_name");
                    var item_count = item.getProperty("in_team_count");
                    var in_current_org = item.getProperty("in_current_org");
                    var in_team = item.getProperty("in_team");
                    var in_names = item.getProperty("in_names");
                    var in_judo_no = item.getProperty("in_judo_no");

                    var message = name + " (籤號 " + in_judo_no + ")"
                        + ": " + in_current_org + " - " + in_names + in_team;

                    AddMsgError2(row, message);
                }
            }
        }

        private List<TProgram> ResetProgramTeams(TConfig cfg)
        {
            var sql = @"
                SELECT
	                t1.id
	                , t1.in_name
	                , t1.in_l1
	                , t1.in_team_count
	                , t2.in_current_org
	                , t2.in_team
	                , t2.in_names
	                , t2.in_sign_no
	                , t2.in_judo_no
	                , t3.in_robin_player
                FROM
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PTEAM t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                INNER JOIN
	                IN_MEETING t3 WITH(NOLOCK)
	                ON t3.id = t1.in_meeting
                WHERE
	                t1.in_meeting = '{#meeting_id}'
	                AND ISNULL(t1.in_team_count, 0) > 1
	                AND t1.in_sign_time IS NOT NULL
					AND ISNULL(t2.in_type, '') IN ('', 'p')
                ORDER BY
	                t1.in_sort_order
	                , t2.in_judo_no
	                , t2.in_city_no
	                , t2.in_stuff_b1
	                , t2.in_current_org
	                , t2.in_team
	                , t2.in_names
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            var result = new List<TProgram>();
            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var key = item.getProperty("id", "");
                var pg = result.Find(x => x.key == key);
                if (pg == null)
                {
                    pg = new TProgram
                    {
                        key = key,
                        name = item.getProperty("in_name", ""),
                        l1 = item.getProperty("in_l1", ""),
                        teamCount = GetInt(item.getProperty("in_team_count", "0")),
                        robinCount = GetInt(item.getProperty("in_robin_player", "0")),
                        NumList = new List<int>(),
                        map = new Dictionary<int, Item>(),
                    };
                    pg.isSpecial = pg.l1 == "格式組";
                    result.Add(pg);
                }

                var judoNo = GetInt(item.getProperty("in_judo_no", "0"));

                if (judoNo <= 0) continue;
                if (pg.map.ContainsKey(judoNo)) continue;

                pg.map.Add(judoNo, item);
                pg.NumList.Add(judoNo);
            }
            return result;
        }

        //循環賽異常
        private void CheckRobinStatus(TConfig cfg, List<TRow> rows)
        {
            var no = (rows.Count + 1);
            var row = new TRow { desc = no + ". 以下組別應為循環賽", messages = new List<TMessage>(), isOK = false };
            rows.Add(row);

            var sql = @"
                SELECT 
	                in_name 
	                , in_team_count 
	                , in_battle_type 
                FROM 
	                IN_MEETING_PROGRAM WITH(NOLOCK) 
                WHERE 
	                in_meeting = '{#meeting_id}'
	                AND in_l1 IN ('個人組', '團體組')
	                AND in_team_count > 1
	                AND in_team_count <= {#robin_player}
	                AND in_battle_type <> 'SingleRoundRobin'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#robin_player}", cfg.robin_player.ToString());

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();

            if (count <= 0)
            {
                row.isOK = true;
                AddMsgOK(row);
                return;
            }

            //if (count > 20)
            //{
            //    AddMsgError(row, "檢查結果: 共有 " + count + " 組異常");
            //    return;
            //}

            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var name = item.getProperty("in_name");
                var item_count = item.getProperty("in_team_count");

                var message = name + " ( " + item_count + ")";

                AddMsgError(row, message);
            }
        }

        private void AddMsgOK(TRow row)
        {
            row.messages.Add(new TMessage { isOK = true, message = "檢查結果: 正常", css = "inno-item-success" });
        }

        private void AddMsgError(TRow row, string message)
        {
            var no = row.messages.Count + 1;
            row.messages.Add(new TMessage { isOK = true, message = "(" + no + "). " + message, css = "inno-item-error" });
        }

        private void AddMsgError2(TRow row, string message)
        {
            var no = row.messages.Count + 1;
            row.messages.Add(new TMessage { isOK = true, message = "(" + no + "). " + message, css = "inno-item-warning" });
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }

            public string meeting_id { get; set; }
            public string scene { get; set; }
            public int robin_player { get; set; }

        }

        public class TRow
        {
            public string desc { get; set; }
            public List<TMessage> messages { get; set; }
            public bool isOK { get; set; }
        }

        public class TMessage
        {
            public bool isOK { get; set; }
            public string message { get; set; }
            public string css { get; set; }
        }

        private class TProgram
        {
            public string key { get; set; }
            public string name { get; set; }
            public string l1 { get; set; }
            public int teamCount { get; set; }
            public int robinCount { get; set; }
            public Dictionary<int, Item> map { get; set; }
            public List<int> NumList { get; set; }
            public bool isSpecial { get; set; }
        }

        private int GetInt(string value)
        {
            if (string.IsNullOrWhiteSpace(value)) return 0;
            int result = int.MinValue;
            if (int.TryParse(value, out result)) return result;
            return 0;
        }
    }
}