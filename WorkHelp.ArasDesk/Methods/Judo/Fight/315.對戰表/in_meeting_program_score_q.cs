﻿using Aras.IOM;
using System;
using System.Collections.Generic;
using System.Linq;

namespace WorkHelp.ArasDesk.Methods.Judo.Fight
{
    public class In_Meeting_Score_Modal : Item
    {
        public In_Meeting_Score_Modal(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
    目的: 成績跳窗
    日期: 
        2025-02-08: 創建 (lina)
*/

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]In_Meeting_Score_Modal";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, this.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,

                meeting_id = itmR.getProperty("meeting_id", ""),
                program_id = itmR.getProperty("program_id", ""),
                event_id = itmR.getProperty("event_id", ""),
                tree_id = itmR.getProperty("id", ""),
                mode = itmR.getProperty("mode", ""),
            };

            if (cfg.meeting_id == "")
            {
                itmR.setProperty("error_message", "賽事 id 不得為空白");
                return itmR;
            }

            ModalJudo(cfg, itmR);

            return itmR;
        }

        private void ModalJudo(TConfig cfg, Item itmReturn)
        {
            List<string> err = new List<string>();
            if (cfg.program_id == "") err.Add("組別 id 不得為空白");
            if (cfg.tree_id == "" && cfg.event_id == "") err.Add("場次 id 不得為空白");

            if (err.Count > 0)
            {
                itmReturn.setProperty("error_message", string.Join(" <br> ", err));
                return;
            }

            //主成績框
            MainModal_Judo(cfg, cfg.program_id, cfg.tree_id, itmReturn);

            //子場次選單
            SubEventMenu_Judo(cfg, cfg.program_id, cfg.tree_id, itmReturn);
        }

        //子場次選單
        private void SubEventMenu_Judo(TConfig cfg, string program_id, string tree_id, Item itmReturn)
        {
            string sql = "";

            string p_tree_id = tree_id.Substring(0, 4);

            sql = @"
                SELECT
                    in_tree_id
                    , in_sub_id
                    , in_tree_alias
                FROM
                    IN_MEETING_PEVENT WITH(NOLOCK)
                WHERE
                    source_id = '{#program_id}'
                    AND in_tree_id LIKE '{#p_tree_id}%'
                    AND in_type = 's'
                ORDER BY
                    in_tree_no
            ";

            sql = sql.Replace("{#program_id}", program_id)
                .Replace("{#p_tree_id}", p_tree_id);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item items = cfg.inn.applySQL(sql);

            if (items.isError() || items.getResult() == "")
            {
                itmReturn.setProperty("sub_json", "[]");
                return;
            }

            List<TNode> nodes = new List<TNode>();

            var main = new TNode
            {
                Lbl = "主場",
                Val = p_tree_id,
            };
            nodes.Add(main);

            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string value = item.getProperty("in_tree_id", "");
                //string name = item.getProperty("in_tree_alias", "");
                string name = "第" + (i + 1) + "場";

                var node = new TNode
                {
                    Lbl = name,
                    Val = value,
                };

                nodes.Add(node);
            }

            itmReturn.setProperty("sub_json", Newtonsoft.Json.JsonConvert.SerializeObject(nodes));
        }

        private void MainModal_Judo(TConfig cfg, string program_id, string tree_id, Item itmReturn)
        {
            var pack = GetEventModalData(cfg);

            //上一場
            SetLastEvent(cfg, pack.evt, itmReturn);

            //下一場
            SetNextEvent(cfg, pack.evt, itmReturn);

            itmReturn.setProperty("inn_pack_json", Newtonsoft.Json.JsonConvert.SerializeObject(pack));
        }

        private void SetLastEvent(TConfig cfg, TEvent evt, Item itmReturn)
        {
            if (evt.in_tree_no == "" || evt.in_tree_no == "0")
            {
                itmReturn.setProperty("last_event_title", "無上一場資料");
                itmReturn.setProperty("last_event_message", "");
                return;
            }

            if (evt.in_tree_no == "1")
            {
                itmReturn.setProperty("last_event_title", "已無上一場資料");
                itmReturn.setProperty("last_event_message", "");
                return;
            }

            string sql = @"
                SELECT
                    MAX(in_tree_no) AS 'target_tree_no'
                FROM 
                    IN_MEETING_PEVENT WITH(NOLOCK)
                WHERE
                    ISNULL(in_site, '') = '{#in_site}'
                    AND ISNULL(in_date_key, '') = '{#in_date_key}'
                    AND ISNULL(in_tree_no, 0) <> 0
                    AND in_tree_no < {#in_tree_no}
                    --AND in_tree_name NOT IN ('rank56', 'rank78')
            ";

            sql = sql.Replace("{#in_site}", evt.in_site)
                .Replace("{#in_date_key}", evt.in_date_key)
                .Replace("{#in_tree_no}", evt.in_tree_no);

            Item itmTreeNo = cfg.inn.applySQL(sql);
            string target_tree_no = itmTreeNo.getProperty("target_tree_no", "");

            if (target_tree_no == "")
            {
                itmReturn.setProperty("last_event_title", "已無上一場資料");
                itmReturn.setProperty("last_event_message", "");
                return;
            }

            Item itmTargetEvent = GetTargetEvent(cfg, evt, target_tree_no);
            int event_count = itmTargetEvent.getItemCount();

            if (event_count <= 0)
            {
                itmReturn.setProperty("last_event_title", "已無上一場資料");
                itmReturn.setProperty("last_event_message", "");
            }
            else if (event_count > 1)
            {
                string event_title = "[" + evt.site_name + "]場次編號(" + target_tree_no + ")有重複資料";
                string event_message = "";
                for (int i = 0; i < event_count; i++)
                {
                    Item item = itmTargetEvent.getItemByIndex(i);
                    string _program_id = item.getProperty("program_id", "");
                    string _program_name3 = item.getProperty("program_name3", "");
                    string _in_tree_id = item.getProperty("in_tree_id", "");
                    event_message += "<a data-pid='" + _program_id + "' data-rid='" + _in_tree_id + "' onclick='MEvent_click(this)'>" + _program_name3 + "</a><br>";
                }

                var link = new TEvtLink();
                link.in_tree_no = target_tree_no;
                link.title = event_title;
                link.message = event_message;
                evt.last = link;
            }
            else
            {
                var link = new TEvtLink();
                link.program_id = itmTargetEvent.getProperty("program_id", "");
                link.program_name = itmTargetEvent.getProperty("program_name3", "");
                link.in_tree_id = itmTargetEvent.getProperty("in_tree_id", "");
                link.in_tree_no = target_tree_no;
                link.title = "";
                link.message = "";
                evt.last = link;
            }
        }

        private void SetNextEvent(TConfig cfg, TEvent evt, Item itmReturn)
        {
            if (evt.in_tree_no == "" || evt.in_tree_no == "0")
            {
                itmReturn.setProperty("last_event_title", "無下一場資料");
                itmReturn.setProperty("last_event_message", "");
                return;
            }

            string sql = @"
                SELECT
                    MIN(in_tree_no) AS 'target_tree_no'
                FROM 
                    IN_MEETING_PEVENT WITH(NOLOCK)
                WHERE
                    ISNULL(in_site, '') = '{#in_site}'
                    AND ISNULL(in_date_key, '') = '{#in_date_key}'
                    AND ISNULL(in_tree_no, 0) <> 0
                    AND in_tree_no > {#in_tree_no}
                    --AND in_tree_name NOT IN ('rank56', 'rank78')
            ";

            sql = sql.Replace("{#in_site}", evt.in_site)
                .Replace("{#in_date_key}", evt.in_date_key)
                .Replace("{#in_tree_no}", evt.in_tree_no);

            Item itmTreeNo = cfg.inn.applySQL(sql);
            string target_tree_no = itmTreeNo.getProperty("target_tree_no", "");

            if (target_tree_no == "")
            {
                itmReturn.setProperty("next_event_title", "已無下一場資料");
                itmReturn.setProperty("next_event_message", "");
                return;
            }

            Item itmTargetEvent = GetTargetEvent(cfg, evt, target_tree_no);
            int event_count = itmTargetEvent.getItemCount();

            if (event_count <= 0)
            {
                itmReturn.setProperty("next_event_title", "已無下一場資料");
                itmReturn.setProperty("next_event_message", "");
            }
            else if (event_count > 1)
            {
                string event_title = "[" + evt.site_name + "]場次編號(" + target_tree_no + ")有重複資料";
                string event_message = "";

                for (int i = 0; i < event_count; i++)
                {
                    Item item = itmTargetEvent.getItemByIndex(i);
                    string _program_id = item.getProperty("program_id", "");
                    string _program_name3 = item.getProperty("program_name3", "");
                    string _in_tree_id = item.getProperty("in_tree_id", "");
                    event_message += "<a data-pid='" + _program_id + "' data-rid='" + _in_tree_id + "' onclick='MEvent_click(this)'>" + _program_name3 + "</a><br>";
                }

                var link = new TEvtLink();
                link.in_tree_no = target_tree_no;
                link.title = event_title;
                link.message = event_message;
                evt.next = link;
            }
            else
            {
                var link = new TEvtLink();
                link.program_id = itmTargetEvent.getProperty("program_id", "");
                link.program_name = itmTargetEvent.getProperty("program_name3", "");
                link.in_tree_id = itmTargetEvent.getProperty("in_tree_id", "");
                link.in_tree_no = target_tree_no;
                link.title = "";
                link.message = "";
                evt.next = link;
            }
        }

        //可能多筆
        private Item GetTargetEvent(TConfig cfg, TEvent evt, string in_tree_no)
        {
            string sql = @"
                SELECT 
                    t1.id            AS 'program_id'
                    , t1.in_name3    AS 'program_name3'
                    , t2.id            AS 'event_id'
                    , t2.in_tree_id
                    , t2.in_tree_no
                FROM
                    IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN 
                    IN_MEETING_PEVENT t2 WITH(NOLOCK)
                    ON t2.source_id = t1.id
                WHERE
                    t1.in_meeting = '{#meeting_id}'
                    AND ISNULL(t2.in_site, '') = '{#in_site}'
                    AND ISNULL(t2.in_date_key, '') = '{#in_date_key}'
                    AND t2.in_tree_no = {#in_tree_no}
                    --AND t2.in_tree_name NOT IN ('rank56', 'rank78')
                ORDER BY
                    t1.in_sort_order
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_site}", evt.in_site)
                .Replace("{#in_date_key}", evt.in_date_key)
                .Replace("{#in_tree_no}", in_tree_no);

            return cfg.inn.applySQL(sql);
        }

        private void AppendLevelOptions(TConfig cfg, Item itmReturn)
        {
            Item itmJson = cfg.inn.newItem("In_Meeting");
            itmJson.setProperty("meeting_id", cfg.meeting_id);
            itmJson.setProperty("need_id", "1");
            itmJson = itmJson.apply("in_meeting_program_options");
            itmReturn.setProperty("in_level_json", itmJson.getProperty("json", ""));
            itmReturn.setProperty("in_level_count", itmJson.getProperty("total", ""));
        }

        private void AppendEventOptions(TConfig cfg, Item itmReturn)
        {
            string sql = @"
                SELECT 
                    DISTINCT 
                    in_tree_sort
                    , in_tree_name
                    , in_round
                    , in_tree_id
                    , in_tree_no
                    , in_tree_rank
                    , in_tree_alias
                    , in_win_status
                FROM
                    IN_MEETING_PEVENT WITH(NOLOCK)
                WHERE
                    source_id = '{#program_id}'
                    AND ISNULL(in_tree_no, '') <> '' 
                ORDER BY
                    in_tree_sort
                    , in_round
                    , in_tree_no
             ";

            sql = sql.Replace("{#program_id}", cfg.program_id);

            //CCO.Utilities.WriteDebug(strMethodName, sql);

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();

            List<TNode> nodes = new List<TNode>();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);

                TNode l1 = AddAndGetNode(nodes, item, "in_tree_name");

                if (string.IsNullOrEmpty(l1.Lbl))
                {
                    switch (l1.Val)
                    {
                        case "main":
                            l1.Lbl = "對戰表";
                            l1.Sort = GetIntVal(item.getProperty("in_tree_sort", ""));
                            break;

                        case "repechage":
                            l1.Lbl = "復活表";
                            l1.Sort = GetIntVal(item.getProperty("in_tree_sort", ""));
                            break;

                        case "challenge-a":
                            l1.Lbl = "挑戰賽";
                            l1.Sort = GetIntVal(item.getProperty("in_tree_sort", ""));
                            break;

                        case "challenge-b":
                            l1.Lbl = "挑戰賽-最終賽";
                            l1.Sort = GetIntVal(item.getProperty("in_tree_sort", ""));
                            break;

                        case "rank56":
                            l1.Lbl = "五六名對戰";
                            l1.Sort = GetIntVal(item.getProperty("in_tree_sort", ""));
                            break;

                        case "rank78":
                            l1.Lbl = "七八名對戰";
                            l1.Sort = GetIntVal(item.getProperty("in_tree_sort", ""));
                            break;

                        default:
                            l1.Lbl = l1.Val;
                            l1.Sort = 99999;
                            break;
                    }
                }

                TNode l2 = AddAndGetNode(l1.Nodes, item, "in_round");

                if (string.IsNullOrEmpty(l2.Lbl))
                {
                    l2.Lbl = "R" + l2.Val;
                }

                TNode l3 = AddAndGetNode(l2.Nodes, item, "in_tree_id");
                string in_tree_no = item.getProperty("in_tree_no", "");
                string in_win_status = item.getProperty("in_win_status", "");

                l3.Lbl = "第 " + in_tree_no + " 場";
                if (in_win_status != "")
                {
                    l3.Lbl += "(已登錄)";
                }
            }

            itmReturn.setProperty("in_event_json", Newtonsoft.Json.JsonConvert.SerializeObject(nodes.OrderBy(x => x.Sort)));
        }

        private TNode AddAndGetNode(List<TNode> nodes, Item item, string key)
        {
            string value = item.getProperty(key, "");

            TNode search = nodes.Find(x => x.Val == value);

            if (search == null)
            {
                search = new TNode
                {
                    Val = value,
                    Nodes = new List<TNode>()
                };

                nodes.Add(search);
            }

            return search;
        }

        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }

            public string meeting_id { get; set; }
            public string program_id { get; set; }
            public string event_id { get; set; }
            public string tree_id { get; set; }
            public string mode { get; set; }
        }

        private class TNode
        {
            public string Val { get; set; }
            public string Lbl { get; set; }
            public int Sort { get; set; }
            public List<TNode> Nodes { get; set; }
        }

        private class TPack
        {
            public TMeeting meeting { set; get; }
            public TSect sect { set; get; }
            public TEvent evt { set; get; }
            public TFoot foot1 { set; get; }
            public TFoot foot2 { set; get; }
        }

        private class TMeeting
        {
            public string in_title { get; set; }
            public string in_uniform_color { get; set; }
        }

        private class TSect
        {
            public string id { get; set; }
            public string in_name { get; set; }
            public string in_name2 { get; set; }
            public string in_name3 { get; set; }
            public string in_display { get; set; }
            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }
            public string in_fight_day { get; set; }
            public string in_fight_time { get; set; }
            public string in_battle_type { get; set; }
            public string in_event_count { get; set; }
            public string in_team_count { get; set; }
            public string in_site_mat { get; set; }
        }

        private class TEvent
        {
            public string in_meeting { get; set; }
            public string source_id { get; set; }
            public string id { get; set; }
            public string in_type { get; set; }
            public string in_tree_name { get; set; }
            public string in_tree_id { get; set; }
            public string in_tree_no { get; set; }
            public string in_tree_alias { get; set; }
            public string in_round { get; set; }
            public string in_round_code { get; set; }
            public string in_fight_id { get; set; }

            public string in_date_key { get; set; }
            public string in_win_status { get; set; }
            public string in_win_time { get; set; }
            public string in_win_local { get; set; }//現場勝方 W/B
            public string in_win_local_time { get; set; }//現場計時結果
            public string in_win_local_note { get; set; }//現場勝出時間

            public string in_sub_sect { get; set; }
            public string in_suspend { get; set; }

            public string in_site { get; set; }
            public string site_name { get; set; }
            public string site_code { get; set; }
            public string site_code_en { get; set; }

            public TEvtLink last { get; set; }
            public TEvtLink next { get; set; }
        }

        private class TFoot
        {
            public string id { get; set; }
            public string in_sign_foot { get; set; }
            public string in_sign_no { get; set; }
            public string in_sign_bypass { get; set; }
            public string in_target_no { get; set; }

            public string in_status { get; set; }
            public string in_score { get; set; }
            public string in_points { get; set; }
            public string in_points_type { get; set; }
            public string in_points_text { get; set; }

            public string in_score1 { get; set; }
            public string in_score2 { get; set; }
            public string in_correct_count { get; set; }
            public string in_suspend { get; set; }
            public string in_yuko { get; set; }

            public string team_id { get; set; }
            public string player_org { get; set; }
            public string player_team { get; set; }
            public string player_name { get; set; }
            public string player_sno { get; set; }
            public string player_count { get; set; }

            public string in_show_org { get; set; }
            public string in_check_result { get; set; }
            public string in_check_status { get; set; }
            public string in_section_no { get; set; }

            public string player_photo { get; set; }
            public string org_photo { get; set; }
        }

        private class TEvtLink
        {
            public string program_id { get; set; }
            public string program_name { get; set; }
            public string event_id { get; set; }
            public string in_tree_id { get; set; }
            public string in_tree_no { get; set; }
            public string title { get; set; }
            public string message { get; set; }
        }

        private TPack GetEventModalData(TConfig cfg)
        {
            var pack = new TPack();
            pack.evt = GetEventData(cfg);
            pack.foot1 = GetFootData(cfg, pack.evt, "1");
            pack.foot2 = GetFootData(cfg, pack.evt, "2");
            pack.sect = GetSectData(cfg, pack.evt.source_id);
            pack.meeting = GetMeetingData(cfg, pack.evt.in_meeting);
            return pack;
        }

        private TMeeting GetMeetingData(TConfig cfg, string id)
        {
            var sql = @"
                SELECT
	                t1.id
                    , t1.in_uniform_color
                FROM
	                IN_MEETING t1 WITH(NOLOCK)
                WHERE
	                t1.id = '{#id}'
            ";
            sql = sql.Replace("{#id}", id);
            var item = cfg.inn.applySQL(sql);
            if (item.getResult() == "") item = cfg.inn.newItem();

            var meeting = new TMeeting();
            meeting.in_uniform_color = item.getProperty("in_uniform_color", "");
            return meeting;
        }

        private TSect GetSectData(TConfig cfg, string id)
        {
            var sql = @"
                SELECT
	                t1.*
                FROM
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                WHERE
	                t1.id = '{#id}'
            ";

            sql = sql.Replace("{#id}", id);
            var item = cfg.inn.applySQL(sql);
            if (item.getResult() == "") item = cfg.inn.newItem();

            var sect = new TSect();
            sect.id = item.getProperty("id", "");
            sect.in_name = item.getProperty("in_name", "");
            sect.in_name2 = item.getProperty("in_name2", "");
            sect.in_name3 = item.getProperty("in_name3", "");
            sect.in_display = item.getProperty("in_display", "");
            sect.in_l1 = item.getProperty("in_l1", "");
            sect.in_l2 = item.getProperty("in_l2", "");
            sect.in_l3 = item.getProperty("in_l3", "");
            sect.in_fight_day = item.getProperty("in_fight_day", "");
            sect.in_fight_time = item.getProperty("in_fight_time", "");
            sect.in_battle_type = item.getProperty("in_battle_type", "");
            sect.in_team_count = item.getProperty("in_team_count", "");
            sect.in_event_count = item.getProperty("in_event_count", "");
            sect.in_site_mat = item.getProperty("in_site_mat", "");
            return sect;
        }

        private TEvent GetEventData(TConfig cfg)
        {
            var cond = cfg.event_id == ""
                ? "AND t1.in_tree_id = '" + cfg.tree_id + "'"
                : "AND t1.id = '" + cfg.event_id + "'";

            var sql = @"
                SELECT TOP 1
	                t1.*
	                , t2.in_name		AS 'site_name'
	                , t2.in_code		AS 'site_code'
	                , t2.in_code_en		AS 'site_code_en'
                FROM
	                IN_MEETING_PEVENT t1 WITH(NOLOCK)
                LEFT JOIN
	                IN_MEETING_SITE t2 WITH(NOLOCK)
	                ON t2.id = t1.in_site
                WHERE
	                t1.source_id = '{#program_id}'
	                {#cond}
            ";

            sql = sql.Replace("{#program_id}", cfg.program_id)
                .Replace("{#cond}", cond);
            
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);
            
            var item = cfg.inn.applySQL(sql);
            if (item.getResult() == "") item = cfg.inn.newItem();

            var evt = new TEvent();
            evt.in_meeting = item.getProperty("in_meeting", "");
            evt.source_id = item.getProperty("source_id", "");

            evt.id = item.getProperty("id", "");
            evt.in_type = item.getProperty("in_type", "");
            evt.in_tree_name = item.getProperty("in_tree_name", "");
            evt.in_tree_id = item.getProperty("in_tree_id", "");
            evt.in_tree_no = item.getProperty("in_tree_no", "");
            evt.in_tree_alias = item.getProperty("in_tree_alias", "");
            evt.in_round = item.getProperty("in_round", "");
            evt.in_round_code = item.getProperty("in_round_code", "");
            evt.in_fight_id = item.getProperty("in_fight_id", "");

            evt.in_date_key = item.getProperty("in_date_key", "");
            evt.in_win_status = item.getProperty("in_win_status", "");

            evt.in_win_time = item.getProperty("in_win_time", "");
            evt.in_win_local = item.getProperty("in_win_local", "");
            evt.in_win_local_time = item.getProperty("in_win_local_time", "");
            evt.in_win_local_note = item.getProperty("in_win_local_note", "");

            evt.in_sub_sect = item.getProperty("in_sub_sect", "");
            evt.in_suspend = item.getProperty("in_suspend", "");

            evt.in_site = item.getProperty("in_site", "");
            evt.site_name = item.getProperty("site_name", "");
            evt.site_code = item.getProperty("site_code", "");
            evt.site_code_en = item.getProperty("site_code_en", "");

            if (cfg.tree_id == "")
            {
                cfg.tree_id = evt.in_tree_id;
            }

            return evt;
        }

        private TFoot GetFootData(TConfig cfg, TEvent evt, string in_sign_foot)
        {
            var sql = @"
                SELECT TOP 1
	                t1.id
	                , t1.in_sign_foot
	                , t1.in_sign_no
	                , t1.in_sign_bypass
	                , t1.in_status
	                , t1.in_score
	                , t1.in_score1
	                , t1.in_score2
	                , t1.in_points
	                , t1.in_correct_count
	                , t1.in_yuko
	                , t1.in_suspend
	                , t2.id AS 'team_id'
                    , ISNULL(t2.map_short_org, t1.in_player_org) AS 'player_org'
                    , ISNULL(t2.in_team, t1.in_player_team)      AS 'player_team'
                    , ISNULL(t2.in_name, t1.in_player_name)      AS 'player_name'
                    , ISNULL(t2.in_sno, t1.in_player_sno)        AS 'player_sno'
                    , ISNULL(t2.in_team_players, '1')            AS 'player_count'
                    , t2.in_show_org
                    , t2.in_check_result
                    , t2.in_check_status
                    , t2.in_section_no
	                , t3.in_photo AS 'player_photo'
                FROM
	                IN_MEETING_PEVENT_DETAIL t1 WITH(NOLOCK)
                LEFT JOIN
	                VU_MEETING_PTEAM t2 WITH(NOLOCK)
	                ON t2.source_id = '{#program_id}'
	                AND t2.in_sign_no = t1.in_sign_no
                LEFT JOIN
	                IN_RESUME t3 WITH(NOLOCK)
	                ON t3.login_name = t2.in_sno
                WHERE
	                t1.source_id = '{#event_id}'
	                AND t1.in_sign_foot = {#in_sign_foot}
            ";

            sql = sql.Replace("{#program_id}", evt.source_id)
                .Replace("{#event_id}", evt.id)
                .Replace("{#in_sign_foot}", in_sign_foot);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            var item = cfg.inn.applySQL(sql);
            if (item.getResult() == "") item = cfg.inn.newItem();

            var isFought = evt.in_win_time == "" ? false : true;

            var foot = new TFoot();
            foot.id = item.getProperty("id", "");
            foot.in_sign_foot = item.getProperty("in_sign_foot", "");
            foot.in_sign_no = item.getProperty("in_sign_no", "");
            foot.in_sign_bypass = item.getProperty("in_sign_bypass", "");
            foot.in_status = item.getProperty("in_status", "");
            foot.in_score = item.getProperty("in_score", "");
            foot.in_points = item.getProperty("in_points", "");
            foot.in_score1 = item.getProperty("in_score1", "");//一勝
            foot.in_score2 = item.getProperty("in_score2", "");//半勝
            foot.in_yuko = item.getProperty("in_yuko", "");//有效
            foot.in_correct_count = item.getProperty("in_correct_count", "");//指導
            foot.in_suspend = item.getProperty("in_suspend", "");

            if (isFought)
            {
                if (foot.in_score1 == "") foot.in_score1 = "0";
                if (foot.in_score2 == "") foot.in_score2 = "0";
                if (foot.in_yuko == "") foot.in_yuko = "0";
                if (foot.in_correct_count == "") foot.in_correct_count = "0";
            }
            else
            {
                if (foot.in_score1 == "") foot.in_score1 = "&nbsp;";
                if (foot.in_score2 == "") foot.in_score2 = "&nbsp;";
                if (foot.in_yuko == "") foot.in_yuko = "&nbsp;";
                if (foot.in_correct_count == "") foot.in_correct_count = "&nbsp;";
            }

            foot.team_id = item.getProperty("team_id", "");
            foot.player_org = item.getProperty("player_org", "");
            foot.player_team = item.getProperty("player_team", "");
            foot.player_name = item.getProperty("player_name", "");
            foot.player_sno = item.getProperty("player_sno", "");
            foot.player_count = item.getProperty("player_count", "");
            foot.in_show_org = item.getProperty("in_show_org", "");
            foot.in_check_result = item.getProperty("in_check_result", "");
            foot.in_check_status = item.getProperty("in_check_status", "");
            foot.in_section_no = item.getProperty("in_section_no", "");
            foot.player_photo = item.getProperty("player_photo", "");

            return foot;
        }

        private int GetIntVal(string value, int def = 0)
        {
            int result = def;
            int.TryParse(value, out result);
            return result;
        }
    }
}