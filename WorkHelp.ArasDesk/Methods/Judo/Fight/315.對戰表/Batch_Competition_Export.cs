﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Judo.Fight
{
    public class Batch_Competition_Export : Item
    {
        public Batch_Competition_Export(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]In_Meeting_Weight_Xls";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, this.dom.InnerXml);
            
            string sql = "";
            string meeting_id = itmR.getProperty("meeting_id", "");
            string in_date = itmR.getProperty("in_date", "");

            Item itmMeeting = inn.applySQL("SELECT in_title FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + meeting_id + "'");
            if (itmMeeting.isError())
            {
                throw new Exception("賽事資料異常");
            }

            Item itmXls = inn.applyMethod("In_Meeting_Excel_Path", "<in_name>competition_path</in_name>");
            string Export_Path = itmXls.getProperty("export_path", "");
            string Template_Path = itmXls.getProperty("template_path", "");

            Export_Path = Export_Path.Replace("meeting_excel", "meeting_fight");

            //生成檔案在該賽事名稱資料夾下
            var in_title = itmMeeting.getProperty("in_title", "");
            var target_folder = Export_Path.Trim('\\');
            var zip_name = in_title + "_對戰表_" + in_date + ".zip";
            var zip_file = target_folder + "\\" + zip_name;
            Export_Path = target_folder + "\\" + in_title;

            //清除檔案
            FixDirAndClearFile(Export_Path);

            sql = @"
                SELECT 
                    * 
                FROM
                    IN_MEETING_PROGRAM WITH(NOLOCK) 
                WHERE 
                    in_meeting = '{#meeting_id}'
                    AND in_l1 <> N'格式組'
					AND ISNULL(in_team_count, '') NOT IN ('', '0', '1')
					AND in_fight_day = '{#in_fight_day}'
                ORDER BY 
                    in_sort_order
            ";

            sql = sql.Replace("{#meeting_id}", meeting_id)
                .Replace("{#in_fight_day}", in_date);

            Item items = inn.applySQL(sql);

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                item.setType("IN_MEETING_PROGRAM");
                item.setProperty("meeting_id", item.getProperty("in_meeting", ""));
                item.setProperty("program_id", item.getProperty("id", ""));
                item.setProperty("mode", "competition");
                item.setProperty("is_batch", "1");
                item.setProperty("batch_export", Export_Path);
                item.apply("In_Meeting_Competition_Export");
            }

            //打包
            Zip(zip_file, Export_Path);

            //輸出
            itmR.setProperty("xls_name", zip_name);

            return itmR;
        }

        private void FixDirAndClearFile(string path)
        {
            if (System.IO.Directory.Exists(path))
            {
                var dir = new System.IO.DirectoryInfo(path);
                dir.Delete(true);
            }

            if (!System.IO.Directory.Exists(path))
            {
                System.IO.Directory.CreateDirectory(path);
            }
        }

        private void Zip(string zip_file, string zip_folder)
        {
            //刪除既有檔案
            if (System.IO.File.Exists(zip_file))
            {
                System.IO.File.Delete(zip_file);
            }

            //壓縮資料庫
            using (var zip = new Ionic.Zip.ZipFile(zip_file, System.Text.Encoding.UTF8))
            {
                zip.AddDirectory(zip_folder);
                zip.Comment = "This zip was created at " + System.DateTime.Now.ToString("G");
                zip.Save();
            }


        }
    }
}