﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Judo.Fight
{
    public class in_meeting_program_preview_other : Item
    {
        public in_meeting_program_preview_other(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                 目的: Other Tree 預覽
                 日期: 
                     2020-11-24: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]in_meeting_program_preview_other";

            Item itmProgram = this;
            Item itmR = inn.newItem();
            //CCO.Utilities.WriteDebug(strMethodName, this.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                inn = inn,

                itmProgram = itmProgram,
                meeting_id = itmProgram.getProperty("meeting_id", ""),
                program_id = itmProgram.getProperty("program_id", ""),
                mode = itmProgram.getProperty("mode", ""),
                eno = itmProgram.getProperty("eno", ""),
                in_sign_time = itmProgram.getProperty("in_sign_time", ""),
                in_team_count = itmProgram.getProperty("in_team_count", ""),
                in_round_code = itmProgram.getProperty("in_round_code", ""),
                in_battle_type = itmProgram.getProperty("in_battle_type", ""),
                pdf = itmProgram.getProperty("pdf", ""),
            };
            cfg.team_count = GetIntVal(cfg.in_team_count);

            cfg.GLOBAL_SIGN_NO_SHOW = cfg.mode == "draw";
            cfg.GLOBAL_ORG_TEAM_SHOW = cfg.mode == "draw" && cfg.in_sign_time != "";
            cfg.GLOBAL_EVENT_EDIT = cfg.eno == "edit";
            cfg.GLOBAL_EVENT_ALL = cfg.eno == "recover";

            //取得比賽選手
            cfg.itmEventPlayers = GetEventPlayers(cfg);

            if (!cfg.itmEventPlayers.isError() && cfg.itmEventPlayers.getItemCount() > 0)
            {
                cfg.EventMap = ItemsToDictionary(cfg.itmEventPlayers);
                var map = AppendEvents(cfg);
                if (cfg.pdf == "")
                {
                    itmR.setProperty("other_box", GetOtherBox(cfg, map).ToString());
                }
                else
                {
                    itmR.setProperty("other_box", GetOtherPdf(cfg, map).ToString());
                }
            }
            else
            {
                itmR.setProperty("other_box", " ");
            }

            return itmR;
        }

        //附加比賽選手
        private StringBuilder GetOtherBox(TConfig cfg, TMap map)
        {
            string in_name2 = cfg.itmProgram.getProperty("in_name2", "");

            StringBuilder builder = new StringBuilder();

            // if (cfg.team_count != 7)
            // {
            //     map.Trees.RemoveAll(x => x.name == "rank56");
            // }
            // if (cfg.team_count != 9)
            // {
            //     map.Trees.RemoveAll(x => x.name == "rank78");
            // }

            foreach (var tree in map.Trees)
            {
                if (builder.Length > 0)
                {
                    builder.AppendLine(" <hr />");
                }

                builder.AppendLine("<div class='main-header r78' style='margin-bottom: 10px; z-index: 100;'>");
                builder.AppendLine("    <h4>");
                builder.AppendLine("        <div class='form-inline'>");
                builder.AppendLine("            " + in_name2 + " &emsp;");
                builder.AppendLine("        </div>");
                builder.AppendLine("    </h4>");
                builder.AppendLine("    <h2>");
                builder.AppendLine("        <div class='form-inline'>");
                builder.AppendLine("            " + tree.title);
                builder.AppendLine("        </div>");
                builder.AppendLine("    </h2>");
                builder.AppendLine("</div>");

                builder.AppendLine("<div class='container '>");
                builder.AppendLine("    <div class='container ' style='margin-bottom: 30px;'>");
                builder.AppendLine(tree.contents);
                builder.AppendLine("    </div>");
                builder.AppendLine("</div>");
            }

            return builder;
        }

        //附加比賽選手
        private StringBuilder GetOtherPdf(TConfig cfg, TMap map)
        {
            string in_name2 = cfg.itmProgram.getProperty("in_name2", "");

            StringBuilder builder = new StringBuilder();

            foreach (var tree in map.Trees)
            {
                builder.AppendLine("<div class='text-center'>");
                builder.AppendLine("    <h3>" + tree.title + "</h3>");
                builder.AppendLine("</div>");


                builder.AppendLine("<div class='container '>");
                builder.AppendLine(tree.contents);
                builder.AppendLine("</div>");
            }

            return builder;
        }

        //附加比賽選手
        private TMap AppendEvents(TConfig cfg)
        {
            TMap map = MapEvents(cfg);

            BindMap(cfg, map);

            foreach (var tree in map.Trees)
            {
                var rpcCfg = GetRepechageCfg(4, tree.name.ToUpper());
                var rpcData = tree.List;
                tree.contents = GetRepechageTableContents(cfg, map, rpcData, rpcCfg);
            }

            return map;
        }

        //轉換比賽分組
        private TMap MapEvents(TConfig cfg)
        {
            TMap result = new TMap
            {
                Trees = new List<TTree>(),

                Teams = new Dictionary<string, Item>(),
                Events = new Dictionary<string, Dictionary<string, Item>>(),

                real_team_count = GetIntVal(cfg.in_team_count),
                tree_team_count = GetIntVal(cfg.in_round_code),
            };

            int count = cfg.itmEventPlayers.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = cfg.itmEventPlayers.getItemByIndex(i);
                string in_tree_name = item.getProperty("in_tree_name", "");
                string in_tree_id = item.getProperty("in_tree_id", "").ToLower();
                string round = item.getProperty("in_round", "");
                string key = item.getProperty("event_id", "");

                var tree = result.Trees.Find(x => x.name == in_tree_name);
                if (tree == null)
                {
                    tree = new TTree
                    {
                        name = in_tree_name,
                        title = GetTreeTitle(in_tree_name),
                        contents = "",
                        List = new Dictionary<string, List<Item>>(),
                    };

                    result.Trees.Add(tree);
                }

                AppendDetail(tree.List, item, in_tree_id);
            }

            return result;
        }

        private string GetTreeTitle(string value)
        {
            switch (value.ToLower())
            {
                case "rank56": return "五六名";
                case "rank78": return "七八名";
                case "ka01": return "盟主賽";
                case "kb01": return "盟主賽-加賽";
                default: return "";
            }
        }

        private void AppendEvents(Dictionary<string, Dictionary<string, Item>> dictionary, Item item)
        {
            string in_tree_id = item.getProperty("in_tree_id", "");
            string in_sign_foot = item.getProperty("in_sign_foot", "");

            //M101
            string key = in_tree_id;
            string sub_key = in_sign_foot;

            Dictionary<string, Item> sub = null;
            if (dictionary.ContainsKey(key))
            {
                sub = dictionary[key];
            }
            else
            {
                sub = new Dictionary<string, Item>();
                dictionary.Add(key, sub);
            }

            if (sub.ContainsKey(sub_key))
            {
                //異常
            }
            else
            {
                sub.Add(sub_key, item);
            }
        }

        private void AppendTeams(Dictionary<string, Item> dictionary, Item item)
        {
            string mdid = item.getProperty("in_sign_no", "");
            string in_tree_id = item.getProperty("in_tree_id", "");
            string in_sign_foot = item.getProperty("in_sign_foot", "");

            //M101-1
            string key = in_tree_id + "-" + in_sign_foot;

            if (dictionary.ContainsKey(key))
            {
                //異常
            }
            else
            {
                if (mdid == "")
                {
                    item.setProperty("no_team", "1");
                }
                dictionary.Add(key, item);
            }
        }

        private void AppendDetail(Dictionary<string, List<Item>> dictionary, Item item, string key)
        {
            if (dictionary.ContainsKey(key))
            {
                dictionary[key].Add(item);
            }
            else
            {
                List<Item> list = new List<Item>();
                list.Add(item);
                dictionary.Add(key, list);
            }
        }

        private string GetSignClass(Item itmSign)
        {
            if (itmSign.getProperty("in_sign_bypass", "") == "1")
            {
                return "";
            }

            return "sgn-" + itmSign.getProperty("in_section_no", "").PadLeft(3, '0');
        }

        //取得勝敗呈現(LINE)
        private string GetStatusDisplay(Item item)
        {
            string in_status = item.getProperty("in_status", "");
            string in_points = item.getProperty("in_points", "0");

            switch (in_status)
            {
                case "1": return "<span class='team_score_b'>" + in_points + "</span>";
                case "0": return "<span class='team_score_c'>" + in_points + "</span>";
                default: return "<span class='team_score_a'>&nbsp;</span>";
            }
        }

        #region 賽程 Table

        private string GetSignNoInfo(Item item)
        {
            string in_sign_bypass = item.getProperty("in_sign_bypass", "");
            if (in_sign_bypass == "1")
            {
                return "";
            }

            string in_current_org = item.getProperty("in_current_org", "");
            if (in_current_org == "")
            {
                return "";
            }

            return item.getProperty("in_section_no", "");
            //return item.getProperty("in_sign_no", "");
        }

        private string GetNameInfo(Item item)
        {
            string in_name = item.getProperty("in_name", "");
            string in_check_result = item.getProperty("in_check_result", "");
            string in_weight_message = item.getProperty("in_weight_message", "");

            //string display = in_name;
            string display = in_name.Replace("(", "<br>(") + in_weight_message;

            if (in_name == "")
            {
                display = "&nbsp;";
            }

            switch (in_check_result)
            {
                // case "1": return "<p class='text-muted text-center btn-success'>" + display + "</p>";
                // case "0": return "<p class='text-muted text-center btn-danger'>" + display + "</p>";
                // default: return "<p class='text-muted text-center btn-default'>" + display + "</p>";

                case "1": return "<span class='player_on'>" + display + "</span>";
                case "0": return "<span class='player_off'>" + display + "</span>";
                default: return "<span  class='player_default'>" + display + "</span>";
            }

            // string in_name = item.getProperty("in_name", "");
            // if (in_name =="")
            // {
            //     return "";
            // }

            // string in_seeds = item.getProperty("in_seeds", "");
            // string in_check_result = item.getProperty("in_check_result", "");

            // return in_name + GetCheckDisplay(in_check_result);

            // if (in_seeds == "" || in_seeds == "0")
            // {
            //     return in_name;
            // }
            // else
            // {
            //     return in_name + "<span style='color: red'>#" + in_seeds + "</span>";
            // }
        }

        private string GetOrgInfo(Item item)
        {
            //string org_name = item.getProperty("in_current_org", "");
            string org_name = item.getProperty("map_short_org", "");
            if (org_name == "")
            {
                return "";
            }
            return org_name;

            // string in_org_teams = item.getProperty("in_org_teams", "");
            // if (in_org_teams == "" || in_org_teams == "0")
            // {
            //     return org_name;
            // }
            // else
            // {
            //     return org_name + "<span style='color: red'>(" + in_org_teams + ")</span>";
            // }
        }


        /// <summary>
        /// 繫結圖面資料
        /// </summary>
        /// <param name="map"></param>
        private void BindMap(TConfig cfg, TMap map)
        {
            int team_col_end = 5;

            map.round_count = GetRounds(map.tree_team_count);

            map.team_rows = 4;
            map.group_rows = map.team_rows * 2;

            map.surface_teams = map.tree_team_count / 2;
            map.surface_events = map.surface_teams / 2;

            map.team_cols = (3 + 2) * 2;
            map.round_cols = map.round_count * 2 + 1;

            map.rows = map.surface_teams * map.team_rows - 2;//最後一列不需 4 列
            map.cols = map.team_cols + map.round_cols;

            map.rows_half = map.rows / 2;
            map.cols_half = map.cols / 2 + 1;

            map.arrs = map.cols + 1;

            map.m_idxs = new List<int>();
            map.m_idxs.Add(map.cols_half - 1);
            map.m_idxs.Add(map.cols_half);
            map.m_idxs.Add(map.cols_half + 1);


            map.team_col_end = team_col_end;

            map.l_idxs = new List<int>();
            map.r_idxs = new List<int>();

            for (int i = 1; i < map.round_count; i++)
            {
                var s = map.team_col_end;
                var e = map.cols;

                map.l_idxs.Add(s + i);
                map.r_idxs.Add(e - 4 - i);
            }
        }

        private string GetRowIdLink(TConfig cfg, string is_allocate, string rid, string sid, string tno, string btn_class = "")
        {
            if (cfg.GLOBAL_EVENT_EDIT)
            {
                return GetRowIdLinkEdit(cfg, cfg.program_id, is_allocate, rid, sid, tno, btn_class);
            }
            else
            {
                return GetRowIdLinkView(cfg, cfg.program_id, is_allocate, rid, sid, tno, btn_class);
            }
        }

        private string GetRowIdLinkView(TConfig cfg, string pid, string is_allocate, string tid, string sid, string tno, string btn_class = "")
        {
            var rid = tid == null ? "" : tid;
            var itmEvt = rid != "" && cfg.EventMap.ContainsKey(rid) ? cfg.EventMap[rid] : cfg.inn.newItem();
            var event_id = itmEvt.getProperty("event_id", "");
            var in_site_code = itmEvt.getProperty("in_site_code", "");
            var current_class = btn_class == "" ? "event-btn" : btn_class;
            var val = "　";

            if (is_allocate == "1")
            {
                val = sid;
            }
            else if (tno == "0")
            {
                if (cfg.GLOBAL_EVENT_ALL)
                {
                    val = "NA";
                }
                else
                {
                    current_class = "no-event-btn";
                }
            }
            else if (tno != "")
            {
                val = "&nbsp;" + tno + "&nbsp;";
            }
            else
            {
                if (cfg.GLOBAL_EVENT_ALL)
                {
                    val = "NA";
                }
                else
                {
                    current_class = "no-event-btn";
                }
            }

            return "<a class='" + current_class + "' href='javascript:void(0)' onclick='Event_Click(this)'"
                + " data-pid='" + cfg.program_id + "'"
                + " data-eid='" + event_id + "'"
                + " data-scd='" + in_site_code + "'"
                + " data-rid='" + rid + "'"
                + " >" + val + "</a>";
        }

        private string GetRowIdLinkEdit(TConfig cfg, string pid, string is_allocate, string rid, string sid, string tno, string btn_class = "")
        {
            if (is_allocate == "1")
            {
                return "<input type='text' value='" + sid + "' class='event-input'"
                + " data-pid='" + pid + "' data-rid='" + rid + "' data-old='" + sid + "'"
                + " onkeyup='Event_KeyUp(this)'"
                + " ondblclick='Event_Click(this)'"
                + " >";
            }
            else if (tno != "")
            {
                return "<input type='text' value='" + tno + "' class='event-input'"
                + " data-pid='" + pid + "' data-rid='" + rid + "' data-old='" + tno + "'"
                + " onkeyup='Event_KeyUp(this)'"
                + " ondblclick='Event_Click(this)'"
                + " >";
            }
            else
            {
                return "<input type='text' value='NA' class='event-input'"
                + " onkeyup='Event_KeyUp(this)'"
                + " data-pid='" + pid + "' data-rid='" + rid + "' data-old='" + tno + "'"
                + " ondblclick='Event_Click(this)'"
                + " >";
            }
        }

        /// <summary>
        /// 圖面資料模型
        /// </summary>
        private class TMap
        {
            /// <summary>
            /// 組別隊伍數
            /// </summary>
            public int real_team_count { get; set; }

            /// <summary>
            /// 賽程表隊伍數
            /// </summary>
            public int tree_team_count { get; set; }

            /// <summary>
            /// 回合數
            /// </summary>
            public int round_count { get; set; }

            /// <summary>
            /// 隊-資料列數
            /// </summary>
            public int team_rows { get; set; }

            /// <summary>
            /// 競賽群組-資料列數
            /// </summary>
            public int group_rows { get; set; }

            /// <summary>
            /// 組面-隊伍數
            /// </summary>
            public int surface_teams { get; set; }

            /// <summary>
            /// 組面-場次數
            /// </summary>
            public int surface_events { get; set; }

            /// <summary>
            /// 隊資-欄位數
            /// </summary>
            public int team_cols { get; set; }

            /// <summary>
            /// 回合-欄位數
            /// </summary>
            public int round_cols { get; set; }

            /// <summary>
            /// 列數
            /// </summary>
            public int rows { get; set; }

            /// <summary>
            /// 欄數
            /// </summary>
            public int cols { get; set; }

            /// <summary>
            /// y軸 中數 (rows 除以 2)
            /// </summary>
            public int rows_half { get; set; }

            /// <summary>
            /// x軸 中數 (cols 除以 2)
            /// </summary>
            public int cols_half { get; set; }

            /// <summary>
            /// 隊資欄位-結束位置
            /// </summary>
            public int team_col_end { get; set; }

            /// <summary>
            /// 字串陣列(由於從 1 取值，因此長度為 cols + 1)
            /// </summary>
            public int arrs { get; set; }

            /// <summary>
            /// 回合-左面欄位索引集
            /// </summary>
            public List<int> l_idxs { get; set; }

            /// <summary>
            /// 回合-中間欄位索引集
            /// </summary>
            public List<int> m_idxs { get; set; }

            /// <summary>
            /// 回合-右間欄位索引集
            /// </summary>
            public List<int> r_idxs { get; set; }

            /// <summary>
            /// 隊資料集
            /// </summary>
            public Dictionary<string, Item> Teams { get; set; }

            /// <summary>
            /// 場次資料集
            /// </summary>
            public Dictionary<string, Dictionary<string, Item>> Events { get; set; }

            public List<TTree> Trees { get; set; }

        }

        private class TTree
        {
            public string name { get; set; }
            public string title { get; set; }
            public string contents { get; set; }
            public Dictionary<string, List<Item>> List { get; set; }
        }

        #endregion 賽程 Table
        #region Repechage Table

        private TRepechage GetRepechageCfg(int team_count, string in_tree_id)
        {
            TRepechage rpc = new TRepechage
            {
                sfc_team_rows = 4,
                sfc_team_cols = (3 + 2), //三個資料格 + 兩個分隔格
            };

            rpc.round = 1;
            rpc.sfc_team_count = 1;
            rpc.in_tree_id = in_tree_id;
            rpc.GetRpcSignsFunc = GetRpcSigns2;
            rpc.GetRpcEventsFunc = GetRpcEvents2;
            rpc.in_tree_id = in_tree_id;

            rpc.sfc_team_col_end = rpc.sfc_team_cols;

            rpc.all_team_cols = rpc.sfc_team_cols * 2;
            rpc.round_cols = rpc.round * 2 + 1;

            rpc.rows = rpc.sfc_team_count * rpc.sfc_team_rows;
            rpc.cols = rpc.all_team_cols + rpc.round_cols;
            rpc.cols_half = rpc.cols / 2 + 1;
            rpc.arrs = rpc.cols + 1;

            rpc.column_team_count = rpc.round - 2;
            rpc.column_seed_team = rpc.column_team_count * 2 + 1;
            if (rpc.column_team_count == 0)
            {
                rpc.column_team_count = 1;
            }

            return rpc;
        }
        private List<TSignGroup> GetRpcSigns2(TRepechage rpc)
        {
            List<TSignGroup> result = new List<TSignGroup>();

            List<int> r1_cidxes = new List<int> { 5, rpc.cols - 5 - 1 };
            int re_cidxes = 6;

            //Round: 1
            for (int i = 0; i < r1_cidxes.Count; i++)
            {
                bool is_right = i > 0;

                int x = r1_cidxes[i];

                result.Add(new TSignGroup { cidx = x, ys = 0, ye = 0, is_right = is_right });
            }

            result.Add(new TSignGroup { cidx = re_cidxes, ys = 0, ye = 0, val = "1", id = rpc.in_tree_id, is_end = true });

            return result;
        }

        private List<TRpcEvent> GetRpcEvents2(TRepechage rpc)
        {
            List<TRpcEvent> result = new List<TRpcEvent>();

            result.Add(new TRpcEvent
            {
                WTeam = new TRpcTeam { in_tree_id = rpc.in_tree_id, in_sign_foot = "1" },
                ETeam = new TRpcTeam { in_tree_id = rpc.in_tree_id, in_sign_foot = "2" },
            });

            return result;
        }

        /// <summary>
        /// 繪製 Repechage 資料表
        /// </summary>
        private string GetRepechageTableContents(TConfig cfg, TMap map, Dictionary<string, List<Item>> dictionary, TRepechage rpc)
        {
            var objs = GetRpcRows(cfg, map, dictionary, rpc);

            var body = new StringBuilder();

            body.AppendLine("<tbody>");

            for (int i = 1; i <= rpc.rows; i++)
            {
                body.AppendLine("<tr>");

                var obj = objs[i - 1];

                for (int j = 1; j <= rpc.cols; j++)
                {
                    var field = obj.Cols[j - 1];

                    if (field.IsRemove)
                    {
                        continue;
                    }

                    body.Append("<td");
                    body.Append(field.RowSpan);
                    body.Append(GetClasses(field.Classes));
                    body.Append(" >");
                    body.Append(field.Value);
                    body.Append("</td>");
                }
                body.AppendLine("</tr>");
            }
            body.AppendLine("</tbody>");

            var builder = new StringBuilder();

            builder.AppendLine("<table class='tb-box inno-other'>");
            builder.Append(GetHeadBuilder(rpc));
            builder.Append(body);
            builder.AppendLine("</table>");

            return builder.ToString();
        }

        private List<TRow> GetRpcRows(TConfig cfg, TMap map, Dictionary<string, List<Item>> dictionary, TRepechage rpc)
        {
            List<TRow> rows = GetRpcRowsData(cfg, map, dictionary, rpc);

            List<TSignGroup> signs = rpc.GetRpcSignsFunc(rpc);

            AppendRpcLines(cfg, map, dictionary, rpc, rows, signs);

            return rows;
        }

        private List<TRow> GetRpcRowsData(TConfig cfg, TMap map, Dictionary<string, List<Item>> dictionary, TRepechage rpc)
        {
            List<TRow> result = new List<TRow>();

            List<TRpcEvent> rpcEvents = rpc.GetRpcEventsFunc(rpc);

            var body_merge = new List<int>();
            body_merge.Add(1);
            body_merge.Add(3);
            body_merge.Add(5);
            body_merge.Add(rpc.cols - 0);
            body_merge.Add(rpc.cols - 2);
            body_merge.Add(rpc.cols - 4);

            for (int i = 1; i <= rpc.rows; i++)
            {
                int rno = i % 4 == 0 ? i / 4 : i / 4 + 1;

                TRow row = new TRow
                {
                    RNo = rno,
                    RpcEvent = rpcEvents[rno - 1],
                    Cols = new List<TCol>(),
                };

                row.IsTeamRow = i % 4 == 1;
                row.IsMergeRow = i % 4 == 2;

                for (int j = 1; j <= rpc.cols; j++)
                {
                    TCol col = new TCol
                    {
                        Classes = new List<string>()
                    };
                    row.Cols.Add(col);
                }

                if (row.IsTeamRow)
                {
                    row.Cols[1 - 1].Classes.Add("rpc-number");
                    row.Cols[3 - 1].Classes.Add("rpc-org inn-left");
                    row.Cols[5 - 1].Classes.Add("rpc-name");
                    row.Cols[rpc.cols - 1].Classes.Add("rpc-number");
                    row.Cols[rpc.cols - 3].Classes.Add("rpc-org inn-right");
                    row.Cols[rpc.cols - 5].Classes.Add("rpc-name");
                }

                for (int x = 0; x < body_merge.Count; x++)
                {
                    var idx = body_merge[x];
                    var field = row.Cols[idx - 1];

                    if (row.IsTeamRow)
                    {
                        field.RowSpan = " rowspan='2'";
                    }
                    else if (row.IsMergeRow)
                    {
                        field.IsRemove = true;
                    }
                }

                if (row.IsTeamRow)
                {
                    var wteam = row.RpcEvent.WTeam;
                    var eteam = row.RpcEvent.ETeam;
                    // row.Cols[3 - 1].Value = wteam.in_tree_id + ":" + wteam.in_sign_foot;
                    // row.Cols[rpc.cols - 3].Value = eteam.in_tree_id + ":" + eteam.in_sign_foot;

                    Item itmLeft = GetTeam(cfg, dictionary, wteam.in_tree_id, wteam.in_sign_foot);
                    Item itmRight = GetTeam(cfg, dictionary, eteam.in_tree_id, eteam.in_sign_foot);

                    string left_check_result = itmLeft.getProperty("in_check_result", "");
                    string right_check_result = itmRight.getProperty("in_check_result", "");

                    // if (left_check_result != "0")
                    // {
                    //     row.Cols[1 - 1].Value = GetSignNoInfo(itmLeft);
                    //     row.Cols[3 - 1].Value = GetOrgInfo(itmLeft);
                    //     row.Cols[5 - 1].Value = GetNameInfo(itmLeft);
                    // }

                    // if (right_check_result != "0")
                    // {
                    //     row.Cols[rpc.cols - 1].Value = GetSignNoInfo(itmRight);
                    //     row.Cols[rpc.cols - 3].Value = GetOrgInfo(itmRight);
                    //     row.Cols[rpc.cols - 5].Value = GetNameInfo(itmRight);
                    // }

                    row.Cols[1 - 1].Value = GetSignNoInfo(itmLeft);
                    row.Cols[3 - 1].Value = GetOrgInfo(itmLeft);
                    row.Cols[5 - 1].Value = GetNameInfo(itmLeft);

                    row.Cols[rpc.cols - 1].Value = GetSignNoInfo(itmRight);
                    row.Cols[rpc.cols - 3].Value = GetOrgInfo(itmRight);
                    row.Cols[rpc.cols - 5].Value = GetNameInfo(itmRight);
                }
                result.Add(row);
            }

            if (!string.IsNullOrEmpty(rpc.in_tree_id))
            {
                Item itmLeft = GetTeam(cfg, dictionary, rpc.in_tree_id, "1");
                Item itmRight = GetTeam(cfg, dictionary, rpc.in_tree_id, "2");

                if (itmLeft != null && itmRight != null)
                {
                    string left_check_result = itmLeft.getProperty("in_check_result", "");
                    string right_check_result = itmRight.getProperty("in_check_result", "");

                    // if (left_check_result != "0")
                    // {
                    //     result[0].Cols[1 - 1].Value = GetSignNoInfo(itmLeft);
                    //     result[0].Cols[3 - 1].Value = GetOrgInfo(itmLeft);
                    //     result[0].Cols[5 - 1].Value = GetNameInfo(itmLeft);
                    // }

                    // if (right_check_result != "0")
                    // {
                    //     result[0].Cols[rpc.cols - 1].Value = GetSignNoInfo(itmRight);
                    //     result[0].Cols[rpc.cols - 3].Value = GetOrgInfo(itmRight);
                    //     result[0].Cols[rpc.cols - 5].Value = GetNameInfo(itmRight);
                    // }

                    result[0].Cols[1 - 1].Value = GetSignNoInfo(itmLeft);
                    result[0].Cols[3 - 1].Value = GetOrgInfo(itmLeft);
                    result[0].Cols[5 - 1].Value = GetNameInfo(itmLeft);

                    result[0].Cols[rpc.cols - 1].Value = GetSignNoInfo(itmRight);
                    result[0].Cols[rpc.cols - 3].Value = GetOrgInfo(itmRight);
                    result[0].Cols[rpc.cols - 5].Value = GetNameInfo(itmRight);

                }
            }
            return result;
        }


        private Item GetTeam(TConfig cfg, Dictionary<string, List<Item>> dictionary, string key, string foot)
        {
            Item result = null;

            if (string.IsNullOrEmpty(key))
            {
                result = cfg.inn.newItem();
                return result;
            }

            string l_key = key.ToLower();
            if (!dictionary.ContainsKey(l_key))
            {
                result = cfg.inn.newItem();
                return result;
            }

            List<Item> list = dictionary[l_key];

            for (int i = 0; i < list.Count; i++)
            {
                Item item = list[i];
                string in_sign_foot = item.getProperty("in_sign_foot", "");
                if (in_sign_foot == foot)
                {
                    result = item;
                }
            }

            return result;
        }

        private void AppendRpcLines(TConfig cfg, TMap map, Dictionary<string, List<Item>> dictionary, TRepechage rpc, List<TRow> rows, List<TSignGroup> signs)
        {
            for (int i = 0; i < signs.Count; i++)
            {
                var sg = signs[i];
                var cidx = sg.cidx;

                Item itmSign1 = null;
                Item itmSign2 = null;

                string in_tree_id = sg.id;
                if (!string.IsNullOrEmpty(rpc.in_tree_id))
                {
                    in_tree_id = rpc.in_tree_id;
                }

                if (!string.IsNullOrEmpty(in_tree_id))
                {
                    itmSign1 = GetTeam(cfg, dictionary, in_tree_id, "1");
                    itmSign2 = GetTeam(cfg, dictionary, in_tree_id, "2");
                }
                else
                {
                    itmSign1 = cfg.inn.newItem();
                    itmSign2 = cfg.inn.newItem();
                }

                string f1_section_no = itmSign1.getProperty("in_section_no", "");
                string f2_section_no = itmSign2.getProperty("in_section_no", "");

                if (sg.is_long)
                {
                    rows[sg.ridx].Cols[cidx].Classes.Add("line-bottom");
                    if (!sg.not_color)
                    {
                        rows[sg.ridx].Cols[cidx].Classes.Add(GetSignClass(itmSign2));
                    }
                }
                else
                {
                    int ys = sg.ys;
                    int ye = sg.ye;
                    int ym = (ys + ye) / 2;

                    rows[ys].Cols[cidx].Classes.Add("line-bottom");
                    rows[ye].Cols[cidx].Classes.Add("line-bottom");

                    string in_tree_no = itmSign1.getProperty("in_tree_no", "");

                    if (sg.is_end)
                    {
                        rows[ym].Cols[cidx - 1].Value = GetSignNoInfo(itmSign1);
                        rows[ym].Cols[cidx].Value = GetRowIdLink(cfg, "", in_tree_id, "", in_tree_no);
                        rows[ym].Cols[cidx + 1].Value = GetSignNoInfo(itmSign2);

                        rows[ym + 1].Cols[cidx - 1].Value = GetStatusDisplay(itmSign1);
                        rows[ym + 1].Cols[cidx + 1].Value = GetStatusDisplay(itmSign2);

                        rows[ym].Cols[cidx - 1].Classes.Add(GetSignClass(itmSign1));
                        rows[ym].Cols[cidx + 1].Classes.Add(GetSignClass(itmSign2));

                        if (itmSign1.getProperty("in_status") == "1")
                        {
                            rows[ym].Cols[cidx].Classes.Add(GetSignClass(itmSign1));
                        }
                        else if (itmSign2.getProperty("in_status") == "1")
                        {
                            rows[ym].Cols[cidx].Classes.Add(GetSignClass(itmSign2));
                        }
                    }
                    else
                    {
                        rows[ys].Cols[cidx].Value = GetSignNoInfo(itmSign1);
                        rows[ym].Cols[cidx].Value = GetRowIdLink(cfg, "", sg.id, "", in_tree_no);
                        rows[ye + 1].Cols[cidx].Value = GetSignNoInfo(itmSign2);

                        if (ys != ye)
                        {
                            rows[ys].Cols[cidx].Classes.Add(GetSignClass(itmSign1));
                            rows[ye + 1].Cols[cidx].Classes.Add(GetSignClass(itmSign2));
                        }

                        for (int y = ys + 1; y <= ye; y++)
                        {
                            if (sg.is_right)
                            {
                                rows[y].Cols[cidx].Classes.Add("line-left");
                            }
                            else
                            {
                                rows[y].Cols[cidx].Classes.Add("line-right");
                            }

                            if (y <= ym)
                            {
                                rows[y].Cols[cidx].Classes.Add(GetSignClass(itmSign1));
                            }
                            else
                            {
                                rows[y].Cols[cidx].Classes.Add(GetSignClass(itmSign2));
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 取得標題內文
        /// </summary>
        private StringBuilder GetHeadBuilder(TRepechage rpc)
        {
            var head_titles = GetClearArr(rpc.arrs);
            head_titles[1] = "籤號";
            head_titles[3] = "單位";
            head_titles[5] = "姓名";
            head_titles[rpc.cols - 0] = "籤號";
            head_titles[rpc.cols - 2] = "單位";
            head_titles[rpc.cols - 4] = "姓名";

            for (int i = 1; i < rpc.round; i++)
            {
                var s = rpc.sfc_team_col_end;
                var e = rpc.cols;
                head_titles[s + i] = "R" + i;
                head_titles[e - 4 - i] = "R" + i;
            }
            head_titles[rpc.cols_half] = "R" + rpc.round;

            var head_widths = GetClearArr(rpc.arrs, "width='50px' ");
            head_widths[1] = "";
            head_widths[2] = "width='1px' ";
            head_widths[3] = "";
            head_widths[4] = "width='1px' ";
            head_widths[5] = "";
            head_widths[rpc.cols - 0] = "";
            head_widths[rpc.cols - 1] = "width='1px' ";
            head_widths[rpc.cols - 2] = "";
            head_widths[rpc.cols - 3] = "width='1px' ";
            head_widths[rpc.cols - 4] = "";

            var head = new StringBuilder();
            head.AppendLine("<thead>");
            head.AppendLine("<tr>");
            for (int j = 1; j <= rpc.cols; j++)
            {
                head.Append("<th ");
                head.Append(head_widths[j]);
                head.Append(" >");
                head.Append(head_titles[j]);
                head.Append("</th>");
                head.AppendLine();
            }
            head.AppendLine("</tr>");
            head.AppendLine("</thead>");

            return head;
        }

        /// <summary>
        /// 取得樣式集
        /// </summary>
        private string GetClasses(List<string> list)
        {
            if (list.Count == 0)
            {
                return " class='td_empty'";
            }
            else
            {
                return " class='" + string.Join(" ", list) + "'";
            }
        }

        #region 復活賽資料結構

        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public Innovator inn { get; set; }

            public string meeting_id { get; set; }
            public string program_id { get; set; }
            public string mode { get; set; }
            public string eno { get; set; }
            public string in_sign_time { get; set; }
            public string in_team_count { get; set; }
            public string in_round_code { get; set; }
            public string in_battle_type { get; set; }
            public int team_count { get; set; }

            public bool GLOBAL_SIGN_NO_SHOW { get; set; }
            public bool GLOBAL_ORG_TEAM_SHOW { get; set; }
            public bool GLOBAL_EVENT_EDIT { get; set; }
            public bool GLOBAL_EVENT_ALL { get; set; }

            public Item itmProgram { get; set; }
            public Item itmEventPlayers { get; set; }
            public string pdf { get; set; }
            public Dictionary<string, Item> EventMap { get; set; }
        }

        private class TSignGroup
        {
            public bool is_right { get; set; }
            public bool is_end { get; set; }
            public bool is_long { get; set; }
            public bool not_color { get; set; }
            public int cidx { get; set; }
            public int ridx { get; set; }
            public int ys { get; set; }
            public int ye { get; set; }
            public string id { get; set; }
            public string val { get; set; }
        }

        private class TRepechage
        {
            /// <summary>
            /// 回合數
            /// </summary>
            public int round { get; set; }
            /// <summary>
            /// 回合欄數
            /// </summary>
            public int round_cols { get; set; }

            /// <summary>
            /// 單邊比賽隊伍數
            /// </summary>
            public int sfc_team_count { get; set; }
            /// <summary>
            /// 單邊隊伍所占列數(預設為 4)
            /// </summary>
            public int sfc_team_rows { get; set; }
            /// <summary>
            /// 單邊隊伍所占欄數
            /// </summary>
            public int sfc_team_cols { get; set; }
            /// <summary>
            /// 單邊隊伍所占欄結束位址
            /// </summary>
            public int sfc_team_col_end { get; set; }

            /// <summary>
            /// 雙邊隊伍所占欄數
            /// </summary>
            public int all_team_cols { get; set; }

            /// <summary>
            /// 總列數
            /// </summary>
            public int rows { get; set; }
            /// <summary>
            /// 總欄數
            /// </summary>
            public int cols { get; set; }
            /// <summary>
            /// 中間欄索引值
            /// </summary>
            public int cols_half { get; set; }

            /// <summary>
            /// 總欄數+1
            /// </summary>
            public int arrs { get; set; }

            /// <summary>
            /// 單柱比賽隊伍數 (1/4) 每柱選手數  (尚未乘以 4)
            /// </summary>
            public int column_team_count { get; set; }

            /// <summary>
            /// 單柱準決賽敗出隊伍位址 (尚未乘以 4)
            /// </summary>
            public int column_seed_team { get; set; }

            /// <summary>
            /// 
            /// </summary>
            public string in_tree_id { get; set; }

            /// <summary>
            /// 繪製線條 Func
            /// </summary>
            public Func<TRepechage, List<TSignGroup>> GetRpcSignsFunc { get; set; }

            /// <summary>
            /// 
            /// </summary>
            public Func<TRepechage, List<TRpcEvent>> GetRpcEventsFunc { get; set; }
        }

        /// <summary>
        /// 資料列
        /// </summary>
        private class TRow
        {
            /// <summary>
            /// 列序號
            /// </summary>
            public int RNo { get; set; }

            /// <summary>
            /// 是否為隊伍資料列
            /// </summary>
            public bool IsTeamRow { get; set; }

            /// <summary>
            /// 是否為合併列
            /// </summary>
            public bool IsMergeRow { get; set; }

            /// <summary>
            /// 資料欄
            /// </summary>
            public List<TCol> Cols { get; set; }

            public TRpcEvent RpcEvent { get; set; }
        }

        /// <summary>
        /// 資料欄
        /// </summary>
        private class TCol
        {
            /// <summary>
            /// 是否移除 (不繪製該 td)
            /// </summary>
            public bool IsRemove { get; set; }

            /// <summary>
            /// 跨列合併數
            /// </summary>
            public string RowSpan { get; set; }

            /// <summary>
            /// 樣式集
            /// </summary>
            public List<string> Classes { get; set; }

            /// <summary>
            /// 值
            /// </summary>
            public string Value { get; set; }
        }

        private class TRpcEvent
        {
            public TRpcTeam WTeam { get; set; }
            public TRpcTeam ETeam { get; set; }
        }

        private class TRpcTeam
        {
            public string in_tree_id { get; set; }

            public string in_sign_foot { get; set; }
        }

        #endregion 復活賽資料結構

        #endregion Repechage Table

        //取得賽事組別場次
        private Item GetEventPlayers(TConfig cfg)
        {
            string sql = @"
                SELECT
                    t1.id AS 'event_id'
                    , t1.in_tree_name
                    , t1.in_tree_id
                    , t1.in_tree_no
                    , t1.in_round
                    , t1.in_round_id
                    , t1.in_round_code
                    , t1.in_site_code
                    , t1.in_site_no
                    , t1.in_site_id
                    , t1.in_site_allocate
                    , t1.in_next_win
                    , t1.in_next_foot_win
                    , t1.in_next_lose
                    , t1.in_next_foot_lose
                    , t1.in_detail_ns
                    , t1.in_win_sign_no
                    , t2.id AS 'detail_id'
                    , t2.in_sign_foot
                    , t2.in_sign_no
                    , t2.in_sign_bypass
                    , t2.in_target_no
                    , t2.in_points
                    , t2.in_status
                    , t3.id AS 'mdid'
                    , ISNULL(t3.map_short_org, t2.in_player_org) AS 'map_short_org'
                    , ISNULL(t3.in_team, t2.in_player_team) AS 'in_team'
                    , ISNULL(t3.in_name, t2.in_player_name) AS 'in_name'
                    , ISNULL(t3.in_sno, t2.in_player_name) AS 'in_sno'
                    , t3.in_section_no
                    , t3.in_org_teams
                    , t3.in_seeds
                    , t3.in_check_result
                    , t3.in_weight_message
                FROM 
                    IN_MEETING_PEVENT t1 WITH(NOLOCK)
                LEFT OUTER JOIN
                    IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK)
                    ON t2.source_id = t1.id
                LEFT OUTER JOIN
                    VU_MEETING_PTEAM t3 WITH(NOLOCK)
                    ON t3.source_id = t1.source_id
                    AND ISNULL(t3.in_sign_no, '') <> ''
                    AND t3.in_sign_no = t2.in_sign_no
                WHERE
                    t1.source_id = '{#program_id}'
                    AND t1.in_tree_name NOT IN ('main', 'repechage', 'rank34', 'challenge-a', 'challenge-b')
                    AND ISNULL(t1.in_type, '') <> 's'
                ORDER BY
                    t1.in_tree_name
                    , t1.in_round
                    , t1.in_round_id
                    , t2.in_sign_foot
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#program_id}", cfg.program_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        private Dictionary<string, Item> ItemsToDictionary(Item items)
        {
            var result = new Dictionary<string, Item>();
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var key = item.getProperty("in_tree_id", "");
                if (!result.ContainsKey(key))
                {
                    result.Add(key, item);
                }
            }
            return result;
        }

        /// <summary>
        /// 取得回合數
        /// </summary>
        private int GetRounds(int value, int code = 2, int count = 0)
        {
            while (value > 1)
            {
                value = value / code;
                count++;
            }
            return count;
        }

        /// <summary>
        /// 取得空字串陣列
        /// </summary>
        private string[] GetClearArr(int len, string def = "")
        {
            string[] result = new string[len];
            for (int i = 0; i < len; i++)
            {
                result[i] = def;
            }
            return result;
        }

        private int GetIntVal(string value, int def = 0)
        {
            int result = def;
            int.TryParse(value, out result);
            return result;
        }
    }
}