﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Judo.Fight
{
    public class in_meeting_org_xls : Item
    {
        public in_meeting_org_xls(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 單位報表
                日誌: 
                    - 2022-09-18: 單位請假 (lina)
                    - 2022-05-03: 新增入場管控表 (lina)
                    - 2022-03-11: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "in_meeting_org_xls";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,

                meeting_id = itmR.getProperty("meeting_id", ""),
                report_date = itmR.getProperty("report_date", ""),
                section_name = itmR.getProperty("section_name", ""),
                scene = itmR.getProperty("scene", ""),
                page = itmR.getProperty("page", ""),

                mode = itmR.getProperty("mode", ""),
                group = itmR.getProperty("group", ""),
                org = itmR.getProperty("org", ""),

                CharSet = GetCharSet(),
            };

            cfg.itmMeeting = cfg.inn.applySQL("SELECT in_title FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            if (cfg.itmMeeting.isError() || cfg.itmMeeting.getResult() == "")
            {
                throw new Exception("賽事資料錯誤");
            }

            cfg.mt_title = cfg.itmMeeting.getProperty("in_title", "");
            cfg.fight_site = GetFightSite(cfg);

            if (cfg.report_date != "")
            {
                cfg.hasDay = true;
                cfg.fight_day = cfg.report_date.Replace("/", "");
            }

            switch (cfg.scene)
            {
                case "player"://參賽選手確認單
                    PlayerXls(cfg, "", itmR);
                    break;

                case "player-solo"://參賽選手確認單-個人
                    PlayerXls(cfg, "solo", itmR);
                    break;

                case "player-team"://參賽選手確認單-團體
                    PlayerXls(cfg, "team", itmR);
                    break;

                case "signin"://各單位簽到表
                    SignInXls(cfg, itmR);
                    break;

                case "fight"://當日參賽單位
                    FightOrgsXls(cfg, itmR);
                    break;

                case "medal_signin"://獎牌單位簽名表
                    MedalSignInXls(cfg, itmR);
                    break;

                case "entrance"://入場管控表
                    EntranceXls(cfg, itmR);
                    break;

                case "org_rank_page"://單位名次
                    OrgRankPage(cfg, itmR);
                    break;

                case "org_rank_xls"://單位名次
                    OrgRankXls(cfg, itmR);
                    break;

                case "org_leave_page"://單位請假
                    OrgLeavePage(cfg, itmR);
                    break;

                case "org_leave_table"://單位請假
                    OrgLeavePage(cfg, itmR);
                    OrgLeaveTable(cfg, itmR);
                    break;

                case "org_leave_list"://單位請假
                    OrgLeaveTable(cfg, itmR);
                    break;

                case "coach"://單位教練表
                    OrgCoachXls(cfg, itmR);
                    break;
            }

            return itmR;
        }

        //單位教練表
        private void OrgCoachXls(TConfig cfg, Item itmReturn)
        {
            Item itmOrgs = OrgCoachItems(cfg);
            int org_cnt = itmOrgs.getItemCount();
            if (org_cnt <= 0)
            {
                throw new Exception("查無資料");
            }

            cfg.CharSet = GetCharSet();

            Item itmXls = cfg.inn.applyMethod("In_Meeting_Excel_Path", "<in_name></in_name>");
            string export_Path = itmXls.getProperty("export_path", "");

            //試算表
            Spire.Xls.Workbook workbook = new Spire.Xls.Workbook();
            Spire.Xls.Worksheet sheet = workbook.CreateEmptySheet();
            sheet.Name = "單位教練表";

            //將所有欄放入單一頁面
            sheet.PageSetup.FitToPagesWide = 1;
            sheet.PageSetup.FitToPagesTall = 0;

            sheet.PageSetup.TopMargin = 0.3;
            sheet.PageSetup.LeftMargin = 0.3;
            sheet.PageSetup.RightMargin = 0;
            sheet.PageSetup.BottomMargin = 0;
            
            ////列印標題
            //sheet.PageSetup.PrintTitleRows = "$1:$4";
            //適合頁面
            //workbook.ConverterSetting.SheetFitToPage = true;

            List<TField> fields = new List<TField>();
            fields.Add(new TField { title = "團會編號", property = "in_stuff_b1", css = TCss.Center, width = 9 });
            fields.Add(new TField { title = "單位簡稱", property = "in_short_org", css = TCss.Center, width = 16 });
            fields.Add(new TField { title = "選手人數", property = "player_count", css = TCss.Center, width = 9 });
            fields.Add(new TField { title = "競賽項目", property = "in_l1", css = TCss.Center, width = 12 });
            fields.Add(new TField { title = "競賽組別", property = "sect_name", css = TCss.Center, width = 24 });
            fields.Add(new TField { title = "教練", property = "in_stuff_c1", css = TCss.Center, width = 12 });
            fields.Add(new TField { title = "隊別", property = "in_team", css = TCss.Center, width = 12 });
            //fields.Add(new TField { title = "單位名稱", property = "in_current_org", css = TCss.Center, width = 16 });
            fields.Add(new TField { title = "聯絡人", property = "in_creator", css = TCss.Center, width = 12 });
            fields.Add(new TField { title = "聯絡電話", property = "in_tel", css = TCss.Center, width = 12 });

            MapCharSetAndIndex(cfg, fields, fontName: "標楷體", fontSize: 12, isBold: false);

            var list = GetOrgCoachList(cfg, itmOrgs);

            //分頁
            //List<TPage> pages = MapPage(cfg, list, 30);
            //AppendOrgCoachRows2(cfg, fields, list, pages, sheet);

            //不分頁
            AppendOrgCoachRows1(cfg, fields, list, sheet);

            //設定欄寬
            SetColumnWidth(sheet, fields);

            //移除空白 Sheet
            workbook.Worksheets[0].Remove();
            workbook.Worksheets[0].Remove();
            workbook.Worksheets[0].Remove();

            string guid = System.Guid.NewGuid().ToString().Substring(0, 6).ToUpper();
            string ext_name = ".xlsx";
            string xls_name = cfg.mt_title
                + "_" + "單位教練表"
                + "_" + guid;

            string xls_file = export_Path + "\\" + xls_name + ext_name;
            string xls_url = xls_name + ext_name;

            workbook.SaveToFile(xls_file, Spire.Xls.ExcelVersion.Version2010);

            itmReturn.setProperty("xls_name", xls_url);
        }
        private void AppendOrgCoachRows1(TConfig cfg, List<TField> fields, List<Item> list, Spire.Xls.Worksheet sheet)
        {
            var fs = fields[0];
            var fe = fields.Last();

            int wsRow = 1;

            //活動標題
            var meeting_text = cfg.mt_title + " 單位教練清單";
            var meeting_pos = fs.cs + wsRow + ":" + fe.cs + wsRow;
            var meeting_mr = sheet.Range[meeting_pos];
            meeting_mr.Merge();
            SetMainCell(sheet, meeting_mr, meeting_text, 20);
            sheet.SetRowHeight(wsRow, 30);
            wsRow++;

            //間隔列
            sheet.SetRowHeight(wsRow, 15);
            wsRow++;

            //時間
            var time_text = "報表日期：" + cfg.report_date;
            var time_pos = fs.cs + wsRow;
            var time_mr = sheet.Range[time_pos];
            SetMainCell(sheet, time_mr, time_text, 16);
            time_mr.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Left;

            //地點
            var place_text = "地點：" + cfg.fight_site;
            var place_pos = fields[4].cs + wsRow;
            var place_mr = sheet.Range[place_pos];
            SetMainCell(sheet, place_mr, place_text, 16);
            place_mr.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Left;

            sheet.SetRowHeight(wsRow, 30);
            wsRow++;

            int bdr_row_s = wsRow;

            //標題列
            SetHeadRow(sheet, wsRow, fields, new TField { fn = "標楷體", fs = 12, fb = true });
            sheet.SetRowHeight(wsRow, 24);
            wsRow++;

            foreach (var item in list)
            {
                SetItemCell(cfg, sheet, wsRow, item, fields);
                sheet.SetRowHeight(wsRow, 24);

                wsRow++;
            }

            //設定格線
            SetRangeBorder(sheet, fields, bdr_row_s, wsRow);

            sheet.HPageBreaks.Add(sheet.Range[fs.cs + (wsRow)]);
        }

        private void AppendOrgCoachRows2(TConfig cfg, List<TField> fields, List<Item> list, List<TPage> pages, Spire.Xls.Worksheet sheet)
        {
            var fs = fields[0];
            var fe = fields.Last();

            int wsRow = 1;
            int pageTotal = pages.Count;
            var total = list.Count.ToString();
            foreach (var page in pages)
            {
                //活動標題
                var meeting_text = cfg.mt_title + " 單位教練 " + page.no + "/" + pageTotal;
                var meeting_pos = fs.cs + wsRow + ":" + fe.cs + wsRow;
                var meeting_mr = sheet.Range[meeting_pos];
                meeting_mr.Merge();
                SetMainCell(sheet, meeting_mr, meeting_text, 20);
                sheet.SetRowHeight(wsRow, 30);
                wsRow++;

                //間隔列
                sheet.SetRowHeight(wsRow, 15);
                wsRow++;

                //時間
                var time_text = "比賽日期：" + cfg.report_date;
                var time_pos = fs.cs + wsRow;
                var time_mr = sheet.Range[time_pos];
                SetMainCell(sheet, time_mr, time_text, 16);
                time_mr.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Left;

                //地點
                var place_text = "地點：" + cfg.fight_site;
                var place_pos = fields[4].cs + wsRow;
                var place_mr = sheet.Range[place_pos];
                SetMainCell(sheet, place_mr, place_text, 16);
                place_mr.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Left;

                sheet.SetRowHeight(wsRow, 30);
                wsRow++;

                int bdr_row_s = wsRow;

                //標題列
                SetHeadRow(sheet, wsRow, fields, new TField { fn = "標楷體", fs = 12, fb = true });
                sheet.SetRowHeight(wsRow, 24);
                wsRow++;

                foreach (var item in page.items)
                {
                    SetItemCell(cfg, sheet, wsRow, item, fields);
                    sheet.SetRowHeight(wsRow, 24);

                    wsRow++;
                }

                //設定格線
                SetRangeBorder(sheet, fields, bdr_row_s, wsRow);

                sheet.HPageBreaks.Add(sheet.Range[fs.cs + (wsRow)]);
            }
        }

        private List<Item> GetOrgCoachList(TConfig cfg, Item itmOrgs)
        {
            var itmCoaches = CoachItems(cfg);
            var map = CoachMap(cfg, itmCoaches);

            var result = new List<Item>();
            var org_cnt = itmOrgs.getItemCount();
            for (int i = 0; i < org_cnt; i++)
            {
                Item itmOrg = itmOrgs.getItemByIndex(i);
                string in_stuff_b1 = itmOrg.getProperty("in_stuff_b1", "");
                itmOrg.setProperty("player_count", OrgCoachPlayerCount(cfg, itmOrg));

                if (!map.ContainsKey(in_stuff_b1))
                {
                    continue;
                }

                var list = map[in_stuff_b1];
                for (int j = 0; j < list.Count; j++)
                {
                    Item itmCoache = list[j];
                    if (j == 0)
                    {
                        itmCoache.setProperty("in_stuff_b1", itmOrg.getProperty("in_stuff_b1", ""));
                        itmCoache.setProperty("in_short_org", itmOrg.getProperty("in_short_org", ""));
                        itmCoache.setProperty("in_current_org", itmOrg.getProperty("in_current_org", ""));
                        itmCoache.setProperty("in_creator", itmOrg.getProperty("in_creator", ""));
                        itmCoache.setProperty("in_tel", itmOrg.getProperty("in_tel", ""));
                        itmCoache.setProperty("player_count", itmOrg.getProperty("player_count", "0"));
                    }
                    result.Add(itmCoache);
                }
            }

            return result;
        }

        private Dictionary<string, List<Item>> CoachMap(TConfig cfg, Item items)
        {
            var result = new Dictionary<string, List<Item>>();

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_stuff_b1 = item.getProperty("in_stuff_b1", "");
                if (!result.ContainsKey(in_stuff_b1))
                {
                    result.Add(in_stuff_b1, new List<Item>());
                }
                result[in_stuff_b1].Add(item);
            }

            return result;
        }

        private Item CoachItems(TConfig cfg)
        {
            string sql = @"
                SELECT t11.*
                FROM
                (
                    SELECT DISTINCT
						in_stuff_b1
						, in_l1
						, in_l2
    	                , REPLACE(REPLACE(in_l2, N'團-', ''), N'個-', '') AS 'sect_name'
						, ISNULL(in_team, '') AS 'in_team'
    	                , in_stuff_c1
                    FROM 
    	                IN_MEETING_USER t1 WITH(NOLOCK)
                    WHERE 
    	                t1.source_id = '{#meeting_id}'
    	                AND t1.in_l1 NOT IN (N'隊職員', N'格式組')
    	        ) t11
    	        ORDER BY in_stuff_b1
					, CASE t11.in_l1  
						WHEN '團體組' THEN 1100
					ELSE 9999 END
					, CASE t11.sect_name 
					WHEN '41歲以上特別組' THEN 1100
					WHEN '30-40歲特別組' THEN 1200
					WHEN '社會男子甲組' THEN 2100
					WHEN '社會女子甲組' THEN 2200
					WHEN '社會男子乙組' THEN 2300
					WHEN '社會女子乙組' THEN 2400
					WHEN '大專男子甲組' THEN 3100
					WHEN '大專女子甲組' THEN 3200
					WHEN '大專男子乙組' THEN 3300
					WHEN '大專女子乙組' THEN 3400
					WHEN '高中男子組' THEN 4100
					WHEN '高中女子組' THEN 4200
					WHEN '國中男子組' THEN 5100
					WHEN '國中女子組' THEN 5200
					WHEN '國小男生組' THEN 6100
					WHEN '國小女生組' THEN 6200
					WHEN '國小男生高年級組' THEN 7100
					WHEN '國小男生中年級組' THEN 7200
					WHEN '國小男生低年級組' THEN 7300
					WHEN '國小女生高年級組' THEN 8100
					WHEN '國小女生中年級組' THEN 8200
					WHEN '國小女生低年級組' THEN 8300
					ELSE 9999 END
					, t11.in_team
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            return cfg.inn.applySQL(sql);
        }

        private string OrgCoachPlayerCount(TConfig cfg, Item itmOrg)
        {
            string in_stuff_b1 = itmOrg.getProperty("in_stuff_b1", "");

            string sql = @"
                SELECT 
	                COUNT(*) AS 'player_count'
                FROM
                (
	                SELECT 
		                DISTINCT in_sno
	                FROM 
		                IN_MEETING_USER t1 WITH(NOLOCK)
	                WHERE 
		                t1.source_id = '{#meeting_id}'
		                AND t1.in_l1 NOT IN (N'隊職員', N'格式組')
		                AND t1.in_stuff_b1 = '{#in_stuff_b1}'
		                AND ISNULL(t1.in_sno, '') <> ''
                ) t11
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_stuff_b1}", in_stuff_b1);

            Item item = cfg.inn.applySQL(sql);

            if (item.isError() || item.getResult() == "")
            {
                return "0";
            }
            else
            {
                return item.getProperty("player_count", "0");
            }
        }

        private Item OrgCoachItems(TConfig cfg)
        {
            string sql = @"
                SELECT DISTINCT 
	                t1.in_stuff_b1
	                , t1.in_short_org
	                , t1.in_current_org
	                , t1.in_creator
	                , t2.in_tel
                FROM 
	                IN_MEETING_USER t1 WITH(NOLOCK)
                INNER JOIN
	                IN_RESUME t2 WITH(NOLOCK)
	                ON t2.in_sno = t1.in_creator_sno
                WHERE 
	                t1.source_id = '{#meeting_id}'
	                AND t1.in_l1 NOT IN (N'隊職員', N'格式組')
                ORDER BY 
	                t1.in_stuff_b1
	                , t1.in_short_org
	                , t1.in_current_org
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            return cfg.inn.applySQL(sql);
        }

        #region 單位請假

        private void OrgLeaveTable(TConfig cfg, Item itmReturn)
        {
            string sect_cond = cfg.section_name == ""
                ? ""
                : "AND t2.in_name LIKE N'%" + cfg.section_name + "%'";

            string sql = @"
                SELECT
	                t1.id
	                , t1.in_name
	                , t1.in_sno
	                , t1.in_gender
	                , CONVERT(VARCHAR, t1.in_birth, 111) AS 'in_birth'
	                , t1.in_seed_lottery
	                , t1.in_weight_status
	                , t2.in_l1
	                , t2.in_l2
	                , t2.in_l3
	                , t2.in_fight_day
                FROM
	                IN_MEETING_USER t1 WITH(NOLOCK)
                LEFT OUTER JOIN
	                IN_MEETING_PROGRAM t2 WITH(NOLOCK)
	                ON t2.in_meeting = t1. source_id
	                AND t2.in_l1 = t1.in_l1
	                AND t2.in_l2 = t1.in_l2
	                AND t2.in_l3 = t1.in_l3
                WHERE
	                t1.source_id = '{#meeting_id}'
	                AND t1.in_l1 NOT IN (N'隊職員', N'格式組')
	                AND t1.in_current_org = N'{#in_current_org}'
	                {#sect_cond}
                ORDER BY 
	                t1.in_city_no
	                , t1.in_stuff_b1
                    , t2.in_sort_order
                    , t1.in_team
                    , t1.in_gender
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_current_org}", cfg.org)
                .Replace("{#sect_cond}", sect_cond);

            Item items = cfg.inn.applySQL(sql);
            int count = items.getItemCount();

            var lsW = new List<Item>();
            var lsM = new List<Item>();
            var lsN = new List<Item>();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_gender = item.getProperty("in_gender", "");
                switch (in_gender)
                {
                    case "女": lsW.Add(item); break;
                    case "男": lsM.Add(item); break;
                    default: lsN.Add(item); break;
                }

            }

            var rows = new List<Item>();
            rows.AddRange(lsW);
            rows.AddRange(lsM);
            rows.AddRange(lsN);

            string contents = GetOrgTableContents(cfg, rows);
            itmReturn.setProperty("inn_table", contents);
        }

        private string GetOrgTableContents(TConfig cfg, List<Item> rows)
        {
            var head = new StringBuilder();
            var body = new StringBuilder();

            head.Append("<thead>");
            head.Append("<tr>");
            head.Append("  <th class='mailbox-subject text-center' data-width='10%' >比賽日期</th>");
            head.Append("  <th class='mailbox-subject text-center' data-width='30%' >參加組別</th>");
            head.Append("  <th class='mailbox-subject text-center' data-width='20%' >選手姓名</th>");
            head.Append("  <th class='mailbox-subject text-center' data-width='10%' >性別</th>");
            head.Append("  <th class='mailbox-subject text-center' data-width='10%' >種子</th>");
            head.Append("  <th class='mailbox-subject text-center' data-width='20%' >功能</th>");
            head.Append("</tr>");
            head.Append("</thead>");


            body.Append("<tbody>");
            for (int i = 0; i < rows.Count; i++)
            {
                Item item = rows[i];
                string in_name = item.getProperty("in_name", "");
                string in_gender = item.getProperty("in_gender", "");
                string in_seed_lottery = item.getProperty("in_seed_lottery", "");
                string in_l2 = item.getProperty("in_l2", "");
                string in_l3 = item.getProperty("in_l3", "");
                string in_fight_day = item.getProperty("in_fight_day", "");

                string sect_name = in_l2.Replace("個-", "").Replace("團-", "").Replace("格-", "")
                    + "-"
                    + in_l3;

                body.Append("<tr class='show_all'>");
                body.Append("  <td class='text-left'> " + in_fight_day + " </td>");
                body.Append("  <td class='text-left'> " + sect_name + " </td>");
                body.Append("  <td class='text-left'> " + in_name + " </td>");
                body.Append("  <td class='text-center'> " + in_gender + " </td>");
                body.Append("  <td class='text-center'> " + in_seed_lottery + " </td>");
                body.Append("  <td class='text-center'> " + WeightStatusMenu(cfg, item) + " </td>");
                body.Append("</tr>");
            }
            body.Append("</tbody>");

            string table_name = "data_table";

            StringBuilder builder = new StringBuilder();
            // builder.Append("<div class='box-body tablenobg' style='position: relative'>");
            builder.AppendLine(GetTableAttribute(table_name));
            builder.Append(head);
            builder.Append(body);
            builder.AppendLine("</table>");
            // builder.Append("</div>");
            builder.AppendLine("<script>");
            builder.AppendLine("$('#" + table_name + "').bootstrapTable();");
            builder.AppendLine("if ($(window).width() <= 768) { $('#" + table_name + "').bootstrapTable('toggleView'); }");
            builder.AppendLine("</script>");

            return builder.ToString();
        }

        private string WeightStatusMenu(TConfig cfg, Item item)
        {
            string muid = item.getProperty("id", "");
            string in_name = item.getProperty("in_name", "");
            string in_weight_status = item.getProperty("in_weight_status", "");

            var sb = new StringBuilder();
            sb.Append("<select class='form-control weight-status' onchange='WeightStatus_Change(this)'"
                + " data-id='" + muid + "'"
                + " data-name='" + in_name + "'"
                + " data-status='" + in_weight_status + "'>");
            sb.Append("  <option value=''>--</option>");
            sb.Append("  <option value='leave'>請假</option>");
            sb.Append("  <option value='rollback'>正常出賽</option>");
            sb.Append("  <option value='on' disabled>過磅合格</option>");
            sb.Append("  <option value='off' disabled>DNS</option>");
            sb.Append("  <option value='dq' disabled>體重不符</option>");
            sb.Append("</select>");
            return sb.ToString();
        }

        private void OrgLeavePage(TConfig cfg, Item itmReturn)
        {
            itmReturn.setProperty("in_title", cfg.mt_title);

            string sql = @"  
                SELECT DISTINCT
	                t1.in_city_no
	                , t1.in_stuff_b1
	                , t1.in_current_org
	                , t1.in_short_org
                FROM
	                IN_MEETING_USER t1 WITH(NOLOCK)
                LEFT OUTER JOIN
	                IN_MEETING_PROGRAM t2 WITH(NOLOCK)
	                ON t2.in_meeting = t1. source_id
	                AND t2.in_l1 = t1.in_l1
	                AND t2.in_l2 = t1.in_l2
	                AND t2.in_l3 = t1.in_l3
                WHERE
	                t1.source_id = '{#meeting_id}'
	                AND t1.in_l1 NOT IN (N'隊職員', N'格式組')
                ORDER BY 
	                t1.in_city_no
	                , t1.in_stuff_b1
	                , t1.in_current_org
	                , t1.in_short_org
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);
            Item items = cfg.inn.applySQL(sql);
            int count = items.getItemCount();

            Item itmEmpty = cfg.inn.newItem();
            itmEmpty.setType("inn_org");
            itmEmpty.setProperty("value", "");
            itmEmpty.setProperty("label", "請選擇");
            itmReturn.addRelationship(itmEmpty);

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_stuff_b1 = item.getProperty("in_stuff_b1", "");
                string in_current_org = item.getProperty("in_current_org", "");
                string in_short_org = item.getProperty("in_short_org", "");

                string label = in_stuff_b1 == ""
                    ? in_short_org
                    : in_stuff_b1 + " " + in_short_org;

                item.setType("inn_org");
                item.setProperty("value", in_current_org);
                item.setProperty("label", label);
                itmReturn.addRelationship(item);
            }

            sql = @"
                SELECT 
                	DISTINCT in_fight_day 
                FROM 
                	IN_MEETING_PROGRAM WITH(NOLOCK) 
                WHERE 
                	in_meeting = '{#meeting_id}' 
                	AND ISNULL(in_fight_day, '') <> ''
                ORDER BY
                	in_fight_day
            ";

            //附加比賽日期
            AppendDayMenu(cfg, itmReturn);
        }
        #endregion 單位請假
        #region 單位名次

        private void OrgRankXls(TConfig cfg, Item itmReturn)
        {
            cfg.section_name = "";

            string fight_day = cfg.report_date.Replace("/", "-");
            Item items = null;

            if (cfg.group == "weight")
            {
                items = WeightRankPlayers(cfg);
            }
            else
            {
                items = OrgRankPlayers(cfg);
            }

            int count = items.getItemCount();
            if (count <= 0)
            {
                throw new Exception("查無資料");
            }

            cfg.CharSet = GetCharSet();

            Item itmXls = cfg.inn.applyMethod("In_Meeting_Excel_Path", "<in_name></in_name>");
            string export_Path = itmXls.getProperty("export_path", "");

            //試算表
            Spire.Xls.Workbook workbook = new Spire.Xls.Workbook();

            Spire.Xls.Worksheet sheet = workbook.CreateEmptySheet();
            sheet.Name = fight_day;

            //將所有欄放入單一頁面
            sheet.PageSetup.FitToPagesWide = 1;
            sheet.PageSetup.FitToPagesTall = 0;

            sheet.PageSetup.TopMargin = 0.3;
            sheet.PageSetup.LeftMargin = 0.3;
            sheet.PageSetup.RightMargin = 0;
            sheet.PageSetup.BottomMargin = 0;

            ////列印標題
            //sheet.PageSetup.PrintTitleRows = "$1:$4";

            //適合頁面
            //workbook.ConverterSetting.SheetFitToPage = true;

            List<TField> fields = new List<TField>();
            fields.Add(new TField { title = "編號", property = "no", css = TCss.Center, width = 8 });
            fields.Add(new TField { title = "會員編號", property = "in_stuff_b1", css = TCss.Center, width = 10 });
            fields.Add(new TField { title = "單位簡稱", property = "in_short_org", css = TCss.Center, width = 16 });
            fields.Add(new TField { title = "組別", property = "program_short", css = TCss.Center, width = 12 });
            fields.Add(new TField { title = "量級", property = "program_weight", css = TCss.Center, width = 12 });
            fields.Add(new TField { title = "選手姓名", property = "in_names", css = TCss.Center, width = 20, need_fit = true });
            fields.Add(new TField { title = "名次", property = "in_show_rank", css = TCss.Center, width = 8 });
            fields.Add(new TField { title = "備註", property = "", css = TCss.Center, width = 16 });

            MapCharSetAndIndex(cfg, fields, fontName: "標楷體", fontSize: 12, isBold: false);

            //分頁
            var list = ToList(cfg, items);
            List<TPage> pages = MapPage(cfg, list, 30);
            AppendOrgRankRows(cfg, fields, list, pages, sheet);

            //設定欄寬
            SetColumnWidth(sheet, fields);

            //移除空白 Sheet
            workbook.Worksheets[0].Remove();
            workbook.Worksheets[0].Remove();
            workbook.Worksheets[0].Remove();

            string guid = System.Guid.NewGuid().ToString().Substring(0, 6).ToUpper();
            string ext_name = ".xlsx";
            string xls_name = cfg.mt_title
                + "_" + "當日單位名次"
                + "_" + fight_day
                + "_" + guid;

            string xls_file = export_Path + "\\" + xls_name + ext_name;
            string xls_url = xls_name + ext_name;

            workbook.SaveToFile(xls_file, Spire.Xls.ExcelVersion.Version2010);

            itmReturn.setProperty("xls_name", xls_url);
        }

        private void AppendOrgRankRows(TConfig cfg, List<TField> fields, List<Item> list, List<TPage> pages, Spire.Xls.Worksheet sheet)
        {
            var fs = fields[0];
            var fe = fields.Last();

            int wsRow = 1;
            int pageTotal = pages.Count;
            var total = list.Count.ToString();
            foreach (var page in pages)
            {
                //活動標題
                var meeting_text = cfg.mt_title + " 單位名次(" + total + ") " + page.no + "/" + pageTotal;
                var meeting_pos = fs.cs + wsRow + ":" + fe.cs + wsRow;
                var meeting_mr = sheet.Range[meeting_pos];
                meeting_mr.Merge();
                SetMainCell(sheet, meeting_mr, meeting_text, 20);
                sheet.SetRowHeight(wsRow, 30);
                wsRow++;

                //間隔列
                sheet.SetRowHeight(wsRow, 15);
                wsRow++;

                //時間
                var time_text = "比賽日期：" + cfg.report_date;
                var time_pos = fs.cs + wsRow;
                var time_mr = sheet.Range[time_pos];
                SetMainCell(sheet, time_mr, time_text, 16);
                time_mr.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Left;

                //地點
                var place_text = "地點：" + cfg.fight_site;
                var place_pos = fields[4].cs + wsRow;
                var place_mr = sheet.Range[place_pos];
                SetMainCell(sheet, place_mr, place_text, 16);
                place_mr.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Left;

                sheet.SetRowHeight(wsRow, 30);
                wsRow++;

                int bdr_row_s = wsRow;

                //標題列
                SetHeadRow(sheet, wsRow, fields, new TField { fn = "標楷體", fs = 12, fb = true });
                sheet.SetRowHeight(wsRow, 24);
                wsRow++;

                foreach (var item in page.items)
                {
                    SetItemCell(cfg, sheet, wsRow, item, fields);
                    sheet.SetRowHeight(wsRow, 24);

                    wsRow++;
                }

                //設定格線
                SetRangeBorder(sheet, fields, bdr_row_s, wsRow);

                sheet.HPageBreaks.Add(sheet.Range[fs.cs + (wsRow)]);
            }
        }

        private void OrgRankPage(TConfig cfg, Item itmReturn)
        {
            //活動資訊
            itmReturn.setProperty("in_title", cfg.mt_title);

            //群組模式
            if (cfg.group == "")
            {
                cfg.group = "org";
                itmReturn.setProperty("group", cfg.group);
            }

            //日期選單
            AppendDayMenu(cfg, itmReturn);

            //單位選單
            AppendOrgMenu(cfg, itmReturn);

            //資料表
            Item items = null;
            string contents = "";

            if (cfg.group == "weight")
            {
                items = WeightRankPlayers(cfg);
                contents = WeightRankPlayersContent(cfg, items);
            }
            else
            {
                items = OrgRankPlayers(cfg);
                contents = OrgRankPlayersContent(cfg, items);
            }

            itmReturn.setProperty("inn_tabs", contents);
        }

        private string WeightRankPlayersContent(TConfig cfg, Item items)
        {
            int count = items.getItemCount();

            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            head.Append("<thead>");
            head.Append("<tr>");
            head.Append("  <th class='mailbox-subject text-center' data-width='6%' >No.</th>");

            head.Append("  <th class='mailbox-subject text-center' data-width='8%' >組</th>");
            head.Append("  <th class='mailbox-subject text-center' data-width='5%' >級</th>");
            head.Append("  <th class='mailbox-subject text-center' data-width='8%' >量</th>");
            head.Append("  <th class='mailbox-subject text-center' data-width='8%' >選手數</th>");

            head.Append("  <th class='mailbox-subject text-center' data-width='7%' >會員編號</th>");
            head.Append("  <th class='mailbox-subject text-center' data-width='16%' >單位簡稱</th>");

            head.Append("  <th class='mailbox-subject text-center' data-width='16%' >選手姓名</th>");

            head.Append("  <th class='mailbox-subject text-center' data-width='8%' data-sortable='true'>名次</th>");
            head.Append("  <th class='mailbox-subject text-center' data-width='8%' >比賽日期</th>");
            head.Append("  <th class='mailbox-subject text-center' data-width='8%' >比賽場地</th>");
            head.Append("</tr>");
            head.Append("</thead>");


            body.Append("<tbody>");
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                var obj = MapTP(cfg, item);

                body.Append("<tr class='show_all'>");

                body.Append("  <td class='text-center'> " + (i + 1) + " </td>");
                body.Append("  <td class='text-center'> " + obj.n1 + " </td>");
                body.Append("  <td class='text-center'> " + obj.n2 + " </td>");
                body.Append("  <td class='text-center'> " + obj.n3 + " </td>");
                body.Append("  <td class='text-center'> " + obj.n4 + " </td>");
                body.Append("  <td class='text-center'> " + obj.o1 + " </td>");
                body.Append("  <td class='text-center'> " + obj.o2 + " </td>");
                body.Append("  <td class='text-center'> " + item.getProperty("in_names", "") + " </td>");
                body.Append("  <td class='text-center'> " + obj.f3 + " </td>");
                body.Append("  <td class='text-center'> " + obj.f1 + " </td>");
                body.Append("  <td class='text-center'> " + obj.f2 + " </td>");
                body.Append("</tr>");
            }
            body.Append("</tbody>");

            return ContactTable(head, body);
        }

        private string OrgRankPlayersContent(TConfig cfg, Item items)
        {
            int count = items.getItemCount();

            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            head.Append("<thead>");
            head.Append("<tr>");
            head.Append("  <th class='mailbox-subject text-center' data-width='6%' >No.</th>");

            head.Append("  <th class='mailbox-subject text-center' data-width='7%' >會員編號</th>");
            head.Append("  <th class='mailbox-subject text-center' data-width='16%' >單位簡稱</th>");

            head.Append("  <th class='mailbox-subject text-center' data-width='8%' >組</th>");
            head.Append("  <th class='mailbox-subject text-center' data-width='5%' >級</th>");
            head.Append("  <th class='mailbox-subject text-center' data-width='8%' >量</th>");
            head.Append("  <th class='mailbox-subject text-center' data-width='8%' >選手數</th>");

            head.Append("  <th class='mailbox-subject text-center' data-width='16%' >選手姓名</th>");
            head.Append("  <th class='mailbox-subject text-center' data-width='8%' data-sortable='true'>名次</th>");
            head.Append("  <th class='mailbox-subject text-center' data-width='8%' >比賽日期</th>");
            head.Append("  <th class='mailbox-subject text-center' data-width='8%' >比賽場地</th>");
            head.Append("</tr>");
            head.Append("</thead>");


            body.Append("<tbody>");
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                var obj = MapTP(cfg, item);

                body.Append("<tr class='show_all'>");

                body.Append("  <td class='text-center'> " + (i + 1) + " </td>");
                body.Append("  <td class='text-center'> " + obj.o1 + " </td>");
                body.Append("  <td class='text-center'> " + obj.o2 + " </td>");
                body.Append("  <td class='text-center'> " + obj.n1 + " </td>");
                body.Append("  <td class='text-center'> " + obj.n2 + " </td>");
                body.Append("  <td class='text-center'> " + obj.n3 + " </td>");
                body.Append("  <td class='text-center'> " + obj.n4 + " </td>");
                body.Append("  <td class='text-center'> " + item.getProperty("in_names", "") + " </td>");
                body.Append("  <td class='text-center'> " + obj.f3 + " </td>");
                body.Append("  <td class='text-center'> " + obj.f1 + " </td>");
                body.Append("  <td class='text-center'> " + obj.f2 + " </td>");
                body.Append("</tr>");
            }
            body.Append("</tbody>");

            return ContactTable(head, body);
        }

        private string ContactTable(StringBuilder head, StringBuilder body)
        {
            string table_name = "data_table";

            StringBuilder builder = new StringBuilder();
            // builder.Append("<div class='box-body tablenobg' style='position: relative'>");
            builder.AppendLine(GetTableAttribute(table_name));
            builder.Append(head);
            builder.Append(body);
            builder.AppendLine("</table>");
            // builder.Append("</div>");
            builder.AppendLine("<script>");
            builder.AppendLine("$('#" + table_name + "').bootstrapTable();");
            builder.AppendLine("if ($(window).width() <= 768) { $('#" + table_name + "').bootstrapTable('toggleView'); }");
            builder.AppendLine("</script>");

            return builder.ToString();
        }

        private string GetTableAttribute(string table_name)
        {
            return "<table id='" + table_name + "'"
                + " class='table table-hover table-striped'"
                + " data-toggle='table'"
                + " data-show-toggle='false'"
                + " data-show-columns='false'"
                + " data-sortable='true'"
                + " data-sort-stable='true'"
                + " data-search='true'"
                + " data-search-align='left'"
                + ">";
        }

        private Item OrgRankPlayers(TConfig cfg)
        {
            string day_cond = cfg.hasDay
                ? "AND t1.in_fight_day = '" + cfg.fight_day + "'"
                : "";

            string org_cond = cfg.org != ""
                ? "AND t2.in_short_org = N'" + cfg.org + "'"
                : "";

            string sql = @"
                SELECT
	                t1.id              AS 'program_id'
	                , t1.in_name       AS 'program_name'
	                , t1.in_short_name AS 'program_short'
	                , t1.in_weight     AS 'program_weight'
	                , t1.in_fight_day
	                , t1.in_team_count
	                , t1.in_rank_count
	                , t1.in_site_mat
	                , t2.in_stuff_b1
	                , t2.in_short_org
	                , t2.in_name
	                , t2.in_names
	                , t2.in_final_rank
	                , t2.in_show_rank
                FROM
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
	                VU_MEETING_PTEAM t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                WHERE
	                t1.in_meeting = '{#meeting_id}'
	                AND ISNULL(t2.in_final_rank, 0) > 0
	                {#day_cond}
	                {#org_cond}
                ORDER BY
	                t2.in_stuff_b1
	                , t1.in_sort_order
	                , t2.in_final_rank
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#day_cond}", day_cond)
                .Replace("{#org_cond}", org_cond);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        private Item WeightRankPlayers(TConfig cfg)
        {
            string day_cond = cfg.hasDay
                ? "AND t1.in_fight_day = '" + cfg.fight_day + "'"
                : "";

            string org_cond = cfg.org != ""
                ? "AND t2.in_short_org = N'" + cfg.org + "'"
                : "";

            string sql = @"
                SELECT
	                t1.id              AS 'program_id'
	                , t1.in_name       AS 'program_name'
	                , t1.in_short_name AS 'program_short'
	                , t1.in_weight     AS 'program_weight'
	                , t1.in_fight_day
	                , t1.in_team_count
	                , t1.in_rank_count
	                , t1.in_site_mat
	                , t2.in_stuff_b1
	                , t2.in_short_org
	                , t2.in_name
	                , t2.in_names
	                , t2.in_final_rank
	                , t2.in_show_rank
                FROM
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
	                VU_MEETING_PTEAM t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                WHERE
	                t1.in_meeting = '{#meeting_id}'
	                AND ISNULL(t2.in_final_rank, 0) > 0
	                {#day_cond}
	                {#org_cond}
                ORDER BY
	                t1.in_sort_order
	                , t2.in_final_rank
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#day_cond}", day_cond)
                .Replace("{#org_cond}", org_cond);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        //日期選單
        private void AppendDayMenu(TConfig cfg, Item itmReturn)
        {
            var today = System.DateTime.Now.ToString("yyyy-MM-dd");
            var itmDays = FightDayItems(cfg);
            var hasToday = false;
            var fstDay = "";

            var itmEmpty = cfg.inn.newItem();
            itmEmpty.setType("inn_date");
            itmEmpty.setProperty("label", "全部");
            itmEmpty.setProperty("value", "all");
            itmReturn.addRelationship(itmEmpty);

            if (!itmDays.isError() && itmDays.getResult() != "")
            {
                int count = itmDays.getItemCount();
                for (int i = 0; i < count; i++)
                {
                    Item itmDay = itmDays.getItemByIndex(i);
                    string in_fight_day = itmDay.getProperty("in_fight_day", "");

                    itmDay.setType("inn_date");
                    itmDay.setProperty("label", in_fight_day);
                    itmDay.setProperty("value", in_fight_day);
                    itmReturn.addRelationship(itmDay);

                    if (i == 0)
                    {
                        fstDay = in_fight_day;
                    }

                    if (today == in_fight_day)
                    {
                        hasToday = true;
                    }
                }
            }

            if (!cfg.hasDay)
            {
                if (cfg.mode == "all")
                {

                }
                else if (hasToday)
                {
                    cfg.hasDay = true;
                    cfg.report_date = today;
                    cfg.fight_day = today;
                    itmReturn.setProperty("report_date", cfg.report_date);
                }
                else
                {
                    cfg.hasDay = true;
                    cfg.report_date = fstDay;
                    cfg.fight_day = fstDay;
                    itmReturn.setProperty("report_date", cfg.report_date);
                }
            }
        }

        private Item FightDayItems(TConfig cfg)
        {
            string sql = @"
                SELECT 
	                DISTINCT in_fight_day 
                FROM 
	                IN_MEETING_PROGRAM WITH(NOLOCK) 
                WHERE 
	                in_meeting = '{#meeting_id}' 
	                AND ISNULL(in_fight_day, '') <> ''
                ORDER BY
	                in_fight_day
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            return cfg.inn.applySQL(sql);
        }

        //單位選單
        private void AppendOrgMenu(TConfig cfg, Item itmReturn)
        {
            var itmEmpty = cfg.inn.newItem();
            itmEmpty.setType("inn_org");
            itmEmpty.setProperty("label", "請選擇");
            itmEmpty.setProperty("value", "");
            itmReturn.addRelationship(itmEmpty);

            Item items = FightOrgItems(cfg);
            if (!items.isError() && items.getResult() != "")
            {
                int count = items.getItemCount();
                for (int i = 0; i < count; i++)
                {
                    Item item = items.getItemByIndex(i);
                    string in_stuff_b1 = item.getProperty("in_stuff_b1", "");
                    string in_short_org = item.getProperty("in_short_org", "");
                    string label = in_stuff_b1 + " " + in_short_org;

                    item.setType("inn_org");
                    item.setProperty("label", label);
                    item.setProperty("value", in_short_org);
                    itmReturn.addRelationship(item);
                }
            }
        }

        private Item FightOrgItems(TConfig cfg)
        {
            string day_cond = cfg.hasDay
                ? "AND t1.in_fight_day = '" + cfg.fight_day + "'"
                : "";

            string sql = @"
                SELECT DISTINCT
                    t2.in_city_no
                    , t2.in_stuff_b1
	                , t2.in_short_org
                FROM
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_USER t2 WITH(NOLOCK)
	                ON t2.source_id = t1.in_meeting
					AND t2.in_l1 = t1.in_l1
					AND t2.in_l2 = t1.in_l2
					AND t2.in_l3 = t1.in_l3
                WHERE
	                t1.in_meeting = '{#meeting_id}'
                    {#day_cond}
                ORDER BY
	                t2.in_city_no
	                , t2.in_stuff_b1
	                , t2.in_short_org
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#day_cond}", day_cond);

            return cfg.inn.applySQL(sql);
        }

        #endregion 單位名次

        #region 入場管控表

        private void EntranceXls(TConfig cfg, Item itmReturn)
        {
            var musers = FightDayMUsers(cfg);

            if (musers.Count == 0)
            {
                throw new Exception("查無資料");
            }

            cfg.itmFightDay = GetDayInfo(cfg);
            cfg.CharSet = GetCharSet();

            Item itmXls = cfg.inn.applyMethod("In_Meeting_Excel_Path", "<in_name>org_entrance</in_name>");
            string export_Path = itmXls.getProperty("export_path", "");
            string template_path = itmXls.getProperty("template_path", "");

            if (cfg.page == "A3")
            {
                template_path = template_path.Replace("A4", "A3");
            }

            //試算表
            Spire.Xls.Workbook workbook = new Spire.Xls.Workbook();
            workbook.LoadFromFile(template_path);

            //樣板
            Spire.Xls.Worksheet sheetTemplate = workbook.Worksheets[0];

            if (cfg.page == "A3")
            {
                AppendEntranceSheetA3(cfg, musers, "男", workbook, sheetTemplate);
                AppendEntranceSheetA3(cfg, musers, "女", workbook, sheetTemplate);
            }
            else
            {
                AppendEntranceSheetA4(cfg, musers, "男", workbook, sheetTemplate);
                AppendEntranceSheetA4(cfg, musers, "女", workbook, sheetTemplate);
            }

            //移除空白 Sheet
            workbook.Worksheets[0].Remove();


            string ext_name = ".xlsx";
            string xls_name = cfg.mt_title
                + "_" + cfg.report_date.Replace("_", "")
                + "_" + "入場管控表"
                + "_" + DateTime.Now.ToString("MMdd_HHmmss");

            string xls_file = export_Path + "\\" + xls_name + ext_name;
            string xls_url = xls_name + ext_name;

            workbook.SaveToFile(xls_file, Spire.Xls.ExcelVersion.Version2010);

            itmReturn.setProperty("xls_name", xls_url);
        }

        private void AppendEntranceSheetA3(TConfig cfg, List<TMUser> source, string in_gender, Spire.Xls.Workbook workbook, Spire.Xls.Worksheet sheetTemplate)
        {
            var list = source.FindAll(x => x.in_gender == in_gender);
            if (list == null || list.Count == 0)
            {
                return;
            }

            //工作表
            Spire.Xls.Worksheet sheet = workbook.CreateEmptySheet();
            sheet.CopyFrom(sheetTemplate);
            sheet.Name = in_gender;

            //將所有欄放入單一頁面
            sheet.PageSetup.FitToPagesWide = 1;
            sheet.PageSetup.FitToPagesTall = 0;

            sheet.PageSetup.TopMargin = 0.3;
            sheet.PageSetup.LeftMargin = 0.3;
            sheet.PageSetup.RightMargin = 0.3;
            sheet.PageSetup.BottomMargin = 0.3;
            sheet.PageSetup.FooterMarginInch = 0.2;
            //sheet.PageSetup.CenterFooter = "&\"Calibri\"&B&10&K4253E2Page &P / &N";

            //列印標題
            sheet.PageSetup.PrintTitleRows = "$1:$5";

            //適合頁面
            //workbook.ConverterSetting.SheetFitToPage = true;

            List<TField> fields_l = new List<TField>();
            fields_l.Add(new TField { title = "No.", property = "no", css = TCss.Center, width = 6, rh = 21, fs = 12, fn = "Times New Roman" });
            fields_l.Add(new TField { title = "姓名", property = "in_name", css = TCss.Center, width = 15, rh = 21, fs = 16, fn = "新細明體" });
            fields_l.Add(new TField { title = "參賽單位", property = "in_short_org", css = TCss.Center, width = 16.8, rh = 21, fs = 16, fn = "新細明體" });
            fields_l.Add(new TField { title = "量級", property = "program_name", css = TCss.Center, width = 20, rh = 21, fs = 12, fn = "新細明體" });

            List<TField> fields_r = new List<TField>();
            fields_r.Add(new TField { title = "No.", property = "no", css = TCss.Center, width = 6, rh = 21, fs = 12, fn = "Times New Roman" });
            fields_r.Add(new TField { title = "姓名", property = "in_name", css = TCss.Center, width = 15, rh = 21, fs = 16, fn = "新細明體" });
            fields_r.Add(new TField { title = "參賽單位", property = "in_short_org", css = TCss.Center, width = 17, rh = 21, fs = 16, fn = "新細明體" });
            fields_r.Add(new TField { title = "量級", property = "program_name", css = TCss.Center, width = 20, rh = 21, fs = 12, fn = "新細明體" });

            MapCharSetAndIndex(cfg, fields_l, null, 0, false);
            MapCharSetAndIndex(cfg, fields_r, null, 0, false, 5);

            double rh = 21.8;
            if (list.Count > 100)
            {
                rh = 18.6;
                fields_l[1].fs = 14;
                fields_l[2].fs = 14;
                fields_r[1].fs = 14;
                fields_r[2].fs = 14;
            }

            var fs = fields_l[0];
            var fe = fields_r.Last();

            //活動標題
            var fight_day = "Day " + cfg.itmFightDay.getProperty("day_no", "");
            var meeting_text = cfg.mt_title + " " + fight_day + " " + in_gender;
            var meeting_pos = fs.cs + "1";
            var meeting_mr = sheet.Range[meeting_pos];
            meeting_mr.Text = meeting_text;

            //入場管控表
            var time_text = cfg.report_date.Replace("-", "/") + " " + "入場管控表";
            var time_pos = fs.cs + "3";
            var time_mr = sheet.Range[time_pos];
            time_mr.Text = time_text;

            int wsRow = 6;
            int mnRow = 6;

            var fields = fields_l;
            var half = list.Count / 2;
            if (list.Count % 2 > 0) half = half + 1;
            if (half < 50) half = 50;

            Item item = cfg.inn.newItem();
            for (int i = 0; i < list.Count; i++)
            {
                var no = i + 1;
                var obj = list[i];

                item.setProperty("no", no.ToString()); ;
                item.setProperty("in_name", obj.in_name); ;
                item.setProperty("in_short_org", obj.in_short_org); ;
                item.setProperty("program_name", obj.program_name); ;

                SetItemCell(cfg, sheet, wsRow, item, fields);
                sheet.SetRowHeight(wsRow, rh);
                wsRow++;

                if (no == half)
                {
                    //設定格線
                    SetRangeBorder(sheet, fields, mnRow, wsRow);

                    fields = fields_r;
                    wsRow = mnRow;
                }
            }

            //設定格線
            SetRangeBorder(sheet, fields, mnRow, wsRow);

            //設定欄寬
            SetColumnWidth(sheet, fields);
        }

        private void AppendEntranceSheetA4(TConfig cfg, List<TMUser> source, string in_gender, Spire.Xls.Workbook workbook, Spire.Xls.Worksheet sheetTemplate)
        {
            var list = source.FindAll(x => x.in_gender == in_gender);
            if (list == null || list.Count == 0)
            {
                return;
            }

            //工作表
            Spire.Xls.Worksheet sheet = workbook.CreateEmptySheet();
            sheet.CopyFrom(sheetTemplate);
            sheet.Name = in_gender;

            //將所有欄放入單一頁面
            //sheet.PageSetup.FitToPagesWide = 1;
            //sheet.PageSetup.FitToPagesTall = 0;

            //sheet.PageSetup.TopMargin = 0.3;
            //sheet.PageSetup.LeftMargin = 0.3;
            //sheet.PageSetup.RightMargin = 0.3;
            //sheet.PageSetup.BottomMargin = 0.3;
            //sheet.PageSetup.FooterMarginInch = 0.2;
            sheet.PageSetup.CenterFooter = "&\"Calibri\"&B&10&K4253E2Page &P / &N";

            //列印標題
            sheet.PageSetup.PrintTitleRows = "$1:$5";

            //適合頁面
            //workbook.ConverterSetting.SheetFitToPage = true;

            List<TField> fields = new List<TField>();
            fields.Add(new TField { title = "No.", property = "no", css = TCss.Center, width = 6, rh = 21, fs = 12, fn = "Times New Roman" });
            fields.Add(new TField { title = "姓名", property = "in_name", css = TCss.Center, width = 15, rh = 21, fs = 16, fn = "新細明體" });
            fields.Add(new TField { title = "參賽單位", property = "in_short_org", css = TCss.Center, width = 16.8, rh = 21, fs = 16, fn = "新細明體" });
            fields.Add(new TField { title = "量級", property = "program_name", css = TCss.Center, width = 27, rh = 21, fs = 12, fn = "新細明體" });
            fields.Add(new TField { title = "備註", property = "in_memo", css = TCss.Center, width = 18, rh = 21, fs = 10, fn = "新細明體" });

            MapCharSetAndIndex(cfg, fields, null, 0, false);

            var fs = fields[0];
            var fe = fields.Last();

            //活動標題
            var fight_day = "Day " + cfg.itmFightDay.getProperty("day_no", "");
            var meeting_text = cfg.mt_title + " " + fight_day + " " + in_gender;
            var meeting_pos = fs.cs + "1";
            var meeting_mr = sheet.Range[meeting_pos];
            meeting_mr.Text = meeting_text;

            //入場管控表
            var time_text = cfg.report_date.Replace("-", "/") + " " + "入場管控表";
            var time_pos = fs.cs + "3";
            var time_mr = sheet.Range[time_pos];
            time_mr.Text = time_text;

            int wsRow = 6;
            int mnRow = 6;

            Item item = cfg.inn.newItem();
            for (int i = 0; i < list.Count; i++)
            {
                var no = i + 1;
                var obj = list[i];

                item.setProperty("no", no.ToString()); ;
                item.setProperty("in_name", obj.in_name); ;
                item.setProperty("in_short_org", obj.in_short_org); ;
                item.setProperty("program_name", obj.program_name); ;
                item.setProperty("in_memo", "");

                SetItemCell(cfg, sheet, wsRow, item, fields);
                wsRow++;
            }

            //設定格線
            SetRangeBorder(sheet, fields, mnRow, wsRow);

            //設定欄寬
            SetColumnWidth(sheet, fields);
        }

        //取得日期資料
        private Item GetDayInfo(TConfig cfg)
        {
            string sql = @"
                SELECT t11.* FROM
                (
	                SELECT in_date_key, ROW_NUMBER() OVER (ORDER BY in_date_key) AS 'day_no' FROM
	                (
		                SELECT DISTINCT in_date_key FROM IN_MEETING_PEVENT WITH(NOLOCK) 
		                WHERE in_meeting = '{#meeting_id}'
						AND ISNULL(in_date_key, '') <> ''
	                ) t1
                ) t11
                WHERE t11.in_date_key = '{#in_date_key}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_date_key}", cfg.report_date);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item itmDay = cfg.inn.applySQL(sql);

            if (itmDay.isError() || itmDay.getResult() == "")
            {
                throw new Exception("查無比賽日期資料");
            }

            return itmDay;
        }

        private List<TMUser> FightDayMUsers(TConfig cfg)
        {
            List<TMUser> result = new List<TMUser>();

            string sql = @"
                SELECT 
                    t1.in_name
                    , t1.in_gender
                    , t1.in_current_org
                    , t1.in_stuff_b1
                    , t1.in_short_org
                    , t1.in_weight_status
                    , t1.in_seed_lottery
                    , t1.in_l1
                    , t2.id                 AS 'program_id'
                    , t2.in_name2           AS 'program_name'
                    , t2.in_sort_order      AS 'program_sort'
                FROM 
                    IN_MEETING_USER t1 WITH(NOLOCK)
                INNER JOIN 
                    IN_MEETING_PROGRAM t2 WITH(NOLOCK)
                    ON t2.in_meeting = t1.source_id
                    AND t2.in_l1 = t1.in_l1 
                    AND t2.in_l2 = t1.in_l2 
                    AND t2.in_l3 = t1.in_l3
                WHERE 
                    t1.source_id = '{#meeting_id}'
                    AND t2.in_fight_day = '{#in_fight_day}'
                    AND ISNULL(t1.IN_WEIGHT_STATUS, '') NOT IN ('leave', 'dq', 'off')
                ORDER BY
	                t1.in_city_no
	                , t1.in_stuff_b1
	                , t1.in_short_org
	                , t2.in_sort_order
	                , t1.in_index
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_fight_day}", cfg.report_date);

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                var obj = MapMUser(cfg, item);
                result.Add(obj);
            }

            return result;
        }
        #endregion 入場管控表

        #region 獎牌單位簽名表

        private void MedalSignInXls(TConfig cfg, Item itmReturn)
        {
            var items = MedalSignInItems(cfg);
            var sects = MapSectRankList(cfg, items);

            if (sects.Count == 0)
            {
                throw new Exception("查無資料");
            }

            cfg.CharSet = GetCharSet();

            Item itmXls = cfg.inn.applyMethod("In_Meeting_Excel_Path", "<in_name></in_name>");
            string export_Path = itmXls.getProperty("export_path", "");

            //試算表
            Spire.Xls.Workbook workbook = new Spire.Xls.Workbook();

            Spire.Xls.Worksheet sheet = workbook.CreateEmptySheet();
            sheet.Name = "獎狀領取簽名表";

            //將所有欄放入單一頁面
            sheet.PageSetup.FitToPagesWide = 1;
            sheet.PageSetup.FitToPagesTall = 0;

            sheet.PageSetup.TopMargin = 0.3;
            sheet.PageSetup.LeftMargin = 0.3;
            sheet.PageSetup.RightMargin = 0.3;
            sheet.PageSetup.BottomMargin = 0.3;
            sheet.PageSetup.FooterMarginInch = 0.2;
            sheet.PageSetup.CenterFooter = "&\"Calibri\"&B&10&K4253E2Page &P / &N";

            ////列印標題
            //sheet.PageSetup.PrintTitleRows = "$1:$4";

            //適合頁面
            //workbook.ConverterSetting.SheetFitToPage = true;

            List<TField> fields = new List<TField>();
            fields.Add(new TField { title = "組別(人數)", property = "program_short", css = TCss.Center, width = 15 });
            fields.Add(new TField { title = "量級", property = "weight", css = TCss.Center, width = 10 });
            fields.Add(new TField { title = "獎牌", property = "rank_title", css = TCss.Center, width = 12 });
            fields.Add(new TField { title = "單位", property = "map_short_org", css = TCss.Center, width = 15, need_fit = true });
            fields.Add(new TField { title = "選手姓名", property = "in_name", css = TCss.Center, width = 16, need_fit = true });
            fields.Add(new TField { title = "簽名", property = "", css = TCss.Center, width = 16 });
            fields.Add(new TField { title = "連絡電話", property = "", css = TCss.Center, width = 16 });

            MapCharSetAndIndex(cfg, fields, "標楷體", 12, false);

            var fs = fields[0];
            var f2 = fields[1];
            var fe = fields.Last();

            int wsRow = 1;

            //活動標題
            var meeting_text = cfg.mt_title;
            var meeting_pos = fs.cs + wsRow + ":" + fe.cs + wsRow;
            var meeting_mr = sheet.Range[meeting_pos];
            meeting_mr.Merge();
            SetMainCell(sheet, meeting_mr, meeting_text, 20);
            sheet.SetRowHeight(wsRow, 30);
            wsRow++;

            //時間
            var time_text = "列印日期：" + DateTime.Now.ToString("yyyy-MM-dd");
            if (cfg.report_date != "") time_text = "比賽日期：" + cfg.report_date;

            var time_pos = fs.cs + wsRow;
            var time_mr = sheet.Range[time_pos];
            SetMainCell(sheet, time_mr, time_text, 16);
            time_mr.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Left;

            //地點
            var place_text = "地點：" + cfg.fight_site;
            var place_pos = fields[3].cs + wsRow;
            var place_mr = sheet.Range[place_pos];
            SetMainCell(sheet, place_mr, place_text, 16);
            place_mr.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Left;

            sheet.SetRowHeight(wsRow, 30);
            wsRow++;

            int bdr_row_s = wsRow;

            //標題列
            SetHeadRow(sheet, wsRow, fields, new TField { fn = "標楷體", fs = 12, fb = false });
            sheet.SetRowHeight(wsRow, 30);
            wsRow++;

            Item item = cfg.inn.newItem();
            foreach (var sect in sects)
            {
                item.setProperty("program_name", sect.program_name + "(" + sect.rank_count + ")");
                item.setProperty("program_short", sect.program_short + "(" + sect.rank_count + ")");
                item.setProperty("in_l1", sect.in_l1);
                item.setProperty("in_l2", sect.in_l2);
                item.setProperty("in_l3", sect.in_l3);
                item.setProperty("weight", sect.weight);

                int minRow = wsRow;
                foreach (var rank in sect.RankList)
                {
                    item.setProperty("rank_title", RankTitle(rank.in_show_rank));
                    item.setProperty("in_current_org", rank.in_current_org);
                    item.setProperty("map_short_org", rank.map_short_org);
                    item.setProperty("in_name", rank.in_name);

                    SetItemCell(cfg, sheet, wsRow, item, fields);
                    sheet.SetRowHeight(wsRow, 30);
                    wsRow++;
                }

                var sect_range = sheet.Range[fs.cs + minRow + ":" + fs.cs + (wsRow - 1)];
                sect_range.Merge();
                sect_range.Style.VerticalAlignment = Spire.Xls.VerticalAlignType.Top;

                var weight_range = sheet.Range[f2.cs + minRow + ":" + f2.cs + (wsRow - 1)];
                weight_range.Merge();
                weight_range.Style.VerticalAlignment = Spire.Xls.VerticalAlignType.Top;
            }

            //設定格線
            SetRangeBorder(sheet, fields, bdr_row_s, wsRow);

            //設定欄寬
            SetColumnWidth(sheet, fields);

            //移除空白 Sheet
            workbook.Worksheets[0].Remove();
            workbook.Worksheets[0].Remove();
            workbook.Worksheets[0].Remove();


            string ext_name = ".xlsx";
            string xls_name = cfg.mt_title
                + "_" + "獎狀領取簽名表"
                + "_" + DateTime.Now.ToString("MMdd_HHmmss");

            string xls_file = export_Path + "\\" + xls_name + ext_name;
            string xls_url = xls_name + ext_name;

            workbook.SaveToFile(xls_file, Spire.Xls.ExcelVersion.Version2010);

            itmReturn.setProperty("xls_name", xls_url);
        }

        private string RankTitle(string rank)
        {
            switch (rank)
            {
                case "1": return "金牌";
                case "2": return "銀牌";
                case "3": return "銅牌";
                case "4": return "第四名";
                case "5": return "第五名";
                case "6": return "第六名";
                case "7": return "第七名";
                case "8": return "第八名";
                default: return "";
            }
        }


        private Item MedalSignInItems(TConfig cfg)
        {
            string day_condition = cfg.report_date == ""
                ? ""
                : "AND t1.in_fight_day = '" + cfg.report_date + "'";

            string sql = @"
                SELECT 
	                t1.id              AS 'program_id'
	                , t1.in_name       AS 'program_name'
	                , t1.in_short_name AS 'program_short'
	                , t1.in_weight     AS 'program_weight'
	                , t1.in_l1
	                , t1.in_l2
	                , t1.in_l3
                    , t1.in_team_count
                    , t1.in_rank_count
	                , t2.in_final_rank
	                , t2.in_show_rank
	                , t2.map_short_org
	                , t2.in_current_org
	                , t2.in_team
	                , t2.in_name
                FROM
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
	                VU_MEETING_PTEAM t2 
	                ON t2.source_id = t1.id
	            LEFT OUTER JOIN
	                IN_MEETING_PEVENT t3 WITH(NOLOCK)
	                ON t3.id = t2.in_event
                WHERE
	                t1.in_meeting = '{#meeting_id}'
	                AND ISNULL(in_final_rank, 0) > 0
	                {#day_condition}
                ORDER BY
	                t1.in_sort_order
	                , t2.in_final_rank
	                , t3.in_tree_id
	                , t2.in_foot
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#day_condition}", day_condition);

            return cfg.inn.applySQL(sql);

        }

        private List<TSect> MapSectRankList(TConfig cfg, Item items)
        {
            List<TSect> list = new List<TSect>();
            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                var rank = NewRank(cfg, item);
                var sect = list.Find(x => x.program_id == rank.program_id);
                if (sect == null)
                {
                    sect = NewSect(cfg, rank);
                    list.Add(sect);
                }
                sect.RankList.Add(rank);
            }
            return list;
        }

        private TSect NewSect(TConfig cfg, TRank obj)
        {
            var result = new TSect
            {
                program_id = obj.program_id,
                program_name = obj.program_name,
                program_short = obj.program_short,
                program_weight = obj.program_weight,
                in_l1 = obj.in_l1,
                in_l2 = obj.in_l2,
                in_l3 = obj.in_l3,
                team_count = GetIntVal(obj.in_team_count),
                rank_count = GetIntVal(obj.in_rank_count),
                RankList = new List<TRank>(),
                weight = "",
            };

            // var arr = result.in_l3.Split(new char[] { '：' }, StringSplitOptions.RemoveEmptyEntries);
            // result.weight = arr == null ? result.in_l3 : arr.Last();
            result.weight = result.program_weight;

            return result;
        }

        private TRank NewRank(TConfig cfg, Item item)
        {
            var result = new TRank
            {
                program_id = item.getProperty("program_id", ""),
                program_name = item.getProperty("program_name", ""),
                program_short = item.getProperty("program_short", ""),
                program_weight = item.getProperty("program_weight", ""),
                in_l1 = item.getProperty("in_l1", ""),
                in_l2 = item.getProperty("in_l2", ""),
                in_l3 = item.getProperty("in_l3", ""),
                in_team_count = item.getProperty("in_team_count", "0"),
                in_rank_count = item.getProperty("in_rank_count", "0"),
                in_final_rank = item.getProperty("in_final_rank", ""),
                in_show_rank = item.getProperty("in_show_rank", ""),
                map_short_org = item.getProperty("map_short_org", ""),
                in_current_org = item.getProperty("in_current_org", ""),
                in_team = item.getProperty("in_team", ""),
                in_name = item.getProperty("in_name", ""),
                Value = item,
            };

            return result;
        }
        private class TSect
        {
            public string program_id { get; set; }
            public string program_name { get; set; }
            public string program_short { get; set; }
            public string program_weight { get; set; }
            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }
            public string weight { get; set; }
            public int team_count { get; set; }
            public int rank_count { get; set; }
            public List<TRank> RankList { get; set; }
        }

        private class TRank
        {
            public string program_id { get; set; }
            public string program_name { get; set; }
            public string program_short { get; set; }
            public string program_weight { get; set; }
            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }
            public string in_team_count { get; set; }
            public string in_rank_count { get; set; }
            public string in_final_rank { get; set; }
            public string in_show_rank { get; set; }
            public string map_short_org { get; set; }
            public string in_current_org { get; set; }
            public string in_team { get; set; }
            public string in_name { get; set; }
            public Item Value { get; set; }
        }
        #endregion 獎牌單位簽名表

        #region 當日參賽單位

        private void FightOrgsXls(TConfig cfg, Item itmReturn)
        {
            cfg.section_name = "";

            string fight_day = cfg.report_date.Replace("/", "-");
            Item items = GetMUsers(cfg, fight_day, "");
            var orgs = MapOrgList(cfg, items);
            if (orgs.Count == 0)
            {
                throw new Exception("查無資料");
            }

            cfg.CharSet = GetCharSet();

            Item itmXls = cfg.inn.applyMethod("In_Meeting_Excel_Path", "<in_name></in_name>");
            string export_Path = itmXls.getProperty("export_path", "");

            //試算表
            Spire.Xls.Workbook workbook = new Spire.Xls.Workbook();

            Spire.Xls.Worksheet sheet = workbook.CreateEmptySheet();
            sheet.Name = fight_day;

            //將所有欄放入單一頁面
            sheet.PageSetup.FitToPagesWide = 1;
            sheet.PageSetup.FitToPagesTall = 0;

            sheet.PageSetup.TopMargin = 0.3;
            sheet.PageSetup.LeftMargin = 0.3;
            sheet.PageSetup.RightMargin = 0;
            sheet.PageSetup.BottomMargin = 0;

            ////列印標題
            //sheet.PageSetup.PrintTitleRows = "$1:$4";

            //適合頁面
            //workbook.ConverterSetting.SheetFitToPage = true;

            List<TField> fields = new List<TField>();
            fields.Add(new TField { title = "編號", property = "no", css = TCss.Center, width = 8 });
            fields.Add(new TField { title = "參賽單位", property = "short_name", css = TCss.Center, width = 21 });
            fields.Add(new TField { title = "當日參賽", property = "player_count", css = TCss.Center, width = 14 });
            fields.Add(new TField { title = "領取項目", property = "", css = TCss.Center, width = 28 });
            fields.Add(new TField { title = "簽名", property = "", css = TCss.Center, width = 13 });
            fields.Add(new TField { title = "手機號碼", property = "", css = TCss.Center, width = 13 });

            MapCharSetAndIndex(cfg, fields, fontName: "標楷體", fontSize: 16, isBold: true);

            //分頁
            List<TPage> pages = MapPage(cfg, orgs);
            AppendOrgFightRows(cfg, fields, orgs, pages, sheet);

            //設定欄寬
            SetColumnWidth(sheet, fields);

            //移除空白 Sheet
            workbook.Worksheets[0].Remove();
            workbook.Worksheets[0].Remove();
            workbook.Worksheets[0].Remove();

            string guid = System.Guid.NewGuid().ToString().Substring(0, 6).ToUpper();
            string ext_name = ".xlsx";
            string xls_name = cfg.mt_title
                + "_" + "當日參賽單位"
                + "_" + fight_day
                + "_" + guid;

            string xls_file = export_Path + "\\" + xls_name + ext_name;
            string xls_url = xls_name + ext_name;

            workbook.SaveToFile(xls_file, Spire.Xls.ExcelVersion.Version2010);

            itmReturn.setProperty("xls_name", xls_url);
        }

        private void AppendOrgFightRows(TConfig cfg, List<TField> fields, List<TOrg> orgs, List<TPage> pages, Spire.Xls.Worksheet sheet)
        {
            var fs = fields[0];
            var fe = fields.Last();

            int wsRow = 1;
            int pageTotal = pages.Count;
            var total = orgs.Count.ToString();
            foreach (var page in pages)
            {
                //活動標題
                var meeting_text = page.title.Replace("{#total}", total) + " " + page.no + "/" + pageTotal;
                var meeting_pos = fs.cs + wsRow + ":" + fe.cs + wsRow;
                var meeting_mr = sheet.Range[meeting_pos];
                meeting_mr.Merge();
                SetMainCell(sheet, meeting_mr, meeting_text, 20);
                sheet.SetRowHeight(wsRow, 30);
                wsRow++;

                //間隔列
                sheet.SetRowHeight(wsRow, 15);
                wsRow++;

                //時間
                var time_text = "比賽日期：" + cfg.report_date;
                var time_pos = fs.cs + wsRow;
                var time_mr = sheet.Range[time_pos];
                SetMainCell(sheet, time_mr, time_text, 16);
                time_mr.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Left;

                //地點
                var place_text = "地點：" + cfg.fight_site;
                var place_pos = fields[2].cs + wsRow;
                var place_mr = sheet.Range[place_pos];
                SetMainCell(sheet, place_mr, place_text, 16);
                place_mr.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Left;

                sheet.SetRowHeight(wsRow, 30);
                wsRow++;

                int bdr_row_s = wsRow;

                //標題列
                SetHeadRow(sheet, wsRow, fields, new TField { fn = "標楷體", fs = 16, fb = true });
                sheet.SetRowHeight(wsRow, 30);
                wsRow++;

                Item item = cfg.inn.newItem();
                foreach (var org in page.orgs)
                {
                    item.setProperty("no", org.no.ToString());
                    item.setProperty("short_name", org.sheet_name);
                    item.setProperty("player_count", org.player_count.ToString());

                    SetItemCell(cfg, sheet, wsRow, item, fields);
                    sheet.SetRowHeight(wsRow, 32);

                    wsRow++;
                }

                //設定格線
                SetRangeBorder(sheet, fields, bdr_row_s, wsRow);

                sheet.HPageBreaks.Add(sheet.Range[fs.cs + (wsRow)]);

            }
        }

        #endregion 當日參賽單位

        #region 各單位簽到表

        private void SignInXls(TConfig cfg, Item itmReturn)
        {
            Item items = GetMUsers(cfg, "", "");
            var orgs = MapOrgList(cfg, items);
            if (orgs.Count == 0)
            {
                throw new Exception("查無資料");
            }

            cfg.CharSet = GetCharSet();

            Item itmXls = cfg.inn.applyMethod("In_Meeting_Excel_Path", "<in_name></in_name>");
            string export_Path = itmXls.getProperty("export_path", "");

            //試算表
            Spire.Xls.Workbook workbook = new Spire.Xls.Workbook();

            Spire.Xls.Worksheet sheet = workbook.CreateEmptySheet();
            sheet.Name = cfg.section_name == "" ? "全部組別" : cfg.section_name;

            //將所有欄放入單一頁面
            sheet.PageSetup.FitToPagesWide = 1;
            sheet.PageSetup.FitToPagesTall = 0;

            sheet.PageSetup.TopMargin = 0.3;
            sheet.PageSetup.LeftMargin = 0.3;
            sheet.PageSetup.RightMargin = 0;
            sheet.PageSetup.BottomMargin = 0;

            ////列印標題
            //sheet.PageSetup.PrintTitleRows = "$1:$4";

            //適合頁面
            //workbook.ConverterSetting.SheetFitToPage = true;

            List<TField> fields = new List<TField>();
            fields.Add(new TField { title = "編號", property = "no", css = TCss.Center, width = 8 });
            fields.Add(new TField { title = "參賽單位", property = "short_name", css = TCss.Center, width = 22 });
            fields.Add(new TField { title = "參賽人數", property = "player_count", css = TCss.Center, width = 16 });
            fields.Add(new TField { title = "職稱", property = "", css = TCss.Center, width = 17 });
            fields.Add(new TField { title = "簽名", property = "", css = TCss.Center, width = 17 });
            fields.Add(new TField { title = "交回", property = "", css = TCss.Center, width = 17 });

            MapCharSetAndIndex(cfg, fields, fontName: "標楷體", fontSize: 16, isBold: true);

            //分頁
            List<TPage> pages = MapPage(cfg, orgs);
            AppendOrgRows(cfg, fields, pages, sheet);

            //設定欄寬
            SetColumnWidth(sheet, fields);

            //移除空白 Sheet
            workbook.Worksheets[0].Remove();
            workbook.Worksheets[0].Remove();
            workbook.Worksheets[0].Remove();

            string sct_name = cfg.section_name == "" ? "(全部)" : "(" + cfg.section_name + "組)";

            string ext_name = ".xlsx";
            string xls_name = cfg.mt_title
                + sct_name
                + "_" + "各單位簽到表"
                + "_" + DateTime.Now.ToString("MMdd_HHmmss");

            string xls_file = export_Path + "\\" + xls_name + ext_name;
            string xls_url = xls_name + ext_name;

            workbook.SaveToFile(xls_file, Spire.Xls.ExcelVersion.Version2010);

            itmReturn.setProperty("xls_name", xls_url);
        }

        private void AppendOrgRows(TConfig cfg, List<TField> fields, List<TPage> pages, Spire.Xls.Worksheet sheet)
        {
            var fs = fields[0];
            var fe = fields.Last();

            int wsRow = 1;
            int pageTotal = pages.Count;
            foreach (var page in pages)
            {
                //活動標題
                var meeting_text = page.title + " " + page.no + "/" + pageTotal;
                var meeting_pos = fs.cs + wsRow + ":" + fe.cs + wsRow;
                var meeting_mr = sheet.Range[meeting_pos];
                meeting_mr.Merge();
                SetMainCell(sheet, meeting_mr, meeting_text, 20);
                sheet.SetRowHeight(wsRow, 30);
                wsRow++;

                //間隔列
                sheet.SetRowHeight(wsRow, 15);
                wsRow++;

                //時間
                var time_text = "時間：" + cfg.report_date;
                var time_pos = fs.cs + wsRow;
                var time_mr = sheet.Range[time_pos];
                SetMainCell(sheet, time_mr, time_text, 16);
                time_mr.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Left;

                //地點
                var place_text = "地點：" + cfg.fight_site;
                var place_pos = fields[2].cs + wsRow;
                var place_mr = sheet.Range[place_pos];
                SetMainCell(sheet, place_mr, place_text, 16);
                place_mr.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Left;

                sheet.SetRowHeight(wsRow, 30);
                wsRow++;

                int bdr_row_s = wsRow;

                //標題列
                SetHeadRow(sheet, wsRow, fields, new TField { fn = "標楷體", fs = 16, fb = true });
                sheet.SetRowHeight(wsRow, 30);
                wsRow++;

                Item item = cfg.inn.newItem();
                foreach (var org in page.orgs)
                {
                    item.setProperty("no", org.no.ToString());
                    item.setProperty("short_name", org.sheet_name);
                    item.setProperty("player_count", org.player_count.ToString());

                    SetItemCell(cfg, sheet, wsRow, item, fields);
                    sheet.SetRowHeight(wsRow, 32);

                    wsRow++;
                }

                //設定格線
                SetRangeBorder(sheet, fields, bdr_row_s, wsRow);

                sheet.HPageBreaks.Add(sheet.Range[fs.cs + (wsRow)]);

            }
        }

        private void SetMainCell(Spire.Xls.Worksheet sheet, Spire.Xls.CellRange range, string text, int fontSize)
        {
            range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
            range.Style.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;
            range.Style.Font.FontName = "新細明體";
            range.Style.Font.IsBold = false;

            range.Text = text;
            range.Style.Font.Size = fontSize;
        }

        private List<TPage> MapPage(TConfig cfg, List<TOrg> orgs)
        {
            List<TPage> pages = new List<TPage>();

            string title = "";

            switch (cfg.scene)
            {
                case "fight":
                    title = cfg.mt_title + "-" + "當日單位({#total})";
                    break;

                default:
                    title = GetSignTitle(cfg);
                    break;
            }

            int r_cnt = 21;
            int p_cnt = orgs.Count / r_cnt;
            if (orgs.Count % r_cnt > 0) p_cnt += 1;

            int max_idx = orgs.Count - 1;
            for (int i = 0; i < p_cnt; i++)
            {
                var page = new TPage
                {
                    no = (i + 1),
                    orgs = new List<TOrg>(),
                    title = title,
                };

                int idx_s = i * r_cnt;
                int idx_e = idx_s + r_cnt;

                for (int j = idx_s; j < idx_e; j++)
                {
                    if (j <= max_idx)
                    {
                        page.orgs.Add(orgs[j]);
                    }
                }

                pages.Add(page);
            }

            return pages;
        }

        private List<Item> ToList(TConfig cfg, Item items)
        {
            List<Item> list = new List<Item>();
            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                item.setProperty("no", (i + 1).ToString());
                list.Add(item);
            }
            return list;
        }

        private List<TPage> MapPage(TConfig cfg, List<Item> list, int pageRow)
        {
            List<TPage> pages = new List<TPage>();

            string title = "";

            int r_cnt = pageRow;
            int p_cnt = list.Count / r_cnt;
            if (list.Count % r_cnt > 0) p_cnt += 1;

            int max_idx = list.Count - 1;
            for (int i = 0; i < p_cnt; i++)
            {
                var page = new TPage
                {
                    no = (i + 1),
                    items = new List<Item>(),
                    title = title,
                };

                int idx_s = i * r_cnt;
                int idx_e = idx_s + r_cnt;

                for (int j = idx_s; j < idx_e; j++)
                {
                    if (j <= max_idx)
                    {
                        page.items.Add(list[j]);
                    }
                }

                pages.Add(page);
            }

            return pages;
        }

        private class TPage
        {
            public int no { get; set; }
            public List<TOrg> orgs { get; set; }
            public string title { get; set; }
            public List<Item> items { get; set; }
        }

        private string GetSignTitle(TConfig cfg)
        {
            string result = cfg.mt_title;

            if (cfg.section_name != "")
            {
                result += "(" + cfg.section_name + "組)" + "簽到表";
            }
            else
            {
                result += "簽到表";
            }

            return result;
        }

        #endregion 各單位簽到表

        #region 參賽選手確認單

        private void PlayerXls(TConfig cfg, string l1_opt, Item itmReturn)
        {
            Item items = GetMUsers(cfg, "", l1_opt);
            var orgs = MapOrgList(cfg, items);
            if (orgs.Count == 0)
            {
                throw new Exception("查無資料");
            }

            cfg.CharSet = GetCharSet();

            Item itmXls = cfg.inn.applyMethod("In_Meeting_Excel_Path", "<in_name>org_player</in_name>");
            string export_Path = itmXls.getProperty("export_path", "");
            string template_Path = itmXls.getProperty("template_path", "");

            //試算表
            Spire.Xls.Workbook workbook = new Spire.Xls.Workbook();
            workbook.LoadFromFile(template_Path);

            //樣板
            Spire.Xls.Worksheet sheetTemplate = workbook.Worksheets[0];

            List<TKVC> soloMap = new List<TKVC>();
            if (l1_opt == "team") soloMap = GetSoloMap(cfg);

            foreach (var org in orgs)
            {
                //組別排序
                var sorted_sects = org.Sections.OrderBy(x => x.code)
                    .ThenBy(x => x.sort_order)
                    .ToList();

                AppendOrgPlayerSheet(cfg, org, sorted_sects, workbook, sheetTemplate, soloMap);
            }

            //移除樣板 sheet
            sheetTemplate.Remove();
            workbook.Worksheets[0].Activate();

            ////移除空白 Sheet
            //workbook.Worksheets[0].Remove();
            //workbook.Worksheets[0].Remove();
            //workbook.Worksheets[0].Remove();

            string l1_name = "";
            switch (l1_opt)
            {
                case "solo": l1_name = "_個人組"; break;
                case "team": l1_name = "_團體組"; break;

            }
            string ext_name = ".xlsx";

            string xls_name = cfg.mt_title
                + "_" + "參賽選手確認單"
                + l1_name
                + "_" + DateTime.Now.ToString("MMdd_HHmmss");

            string xls_file = export_Path + "\\" + xls_name + ext_name;
            string xls_url = xls_name + ext_name;

            workbook.SaveToFile(xls_file, Spire.Xls.ExcelVersion.Version2010);

            itmReturn.setProperty("xls_name", xls_url);
        }

        private List<TKVC> GetSoloMap(TConfig cfg)
        {
            var result = new List<TKVC>();

            Item items = GetMUsers(cfg, "", "solo");
            int count = items.getItemCount();

            var rMap = GetMeetingWeightMap(cfg);
            var wMap = GetGroupWeightMap(cfg, "社女甲");
            var mMap = GetGroupWeightMap(cfg, "社男甲");
            var gMap = new List<TKVC>();

            if (rMap.Count == 0) return result;
            if (wMap.Count == 0) return result;
            if (mMap.Count == 0) return result;

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string key = item.getProperty("in_sno", "");
                string in_l3 = item.getProperty("in_l3", "");
                string in_gender = item.getProperty("in_gender", "");

                if (key == "") continue;
                if (in_l3 == "") continue;

                var existed = result.Find(x => x.k == key);
                if (existed != null) continue;

                //第一級：60.0公斤以下
                //第二級：60.1公斤至66.0公斤
                //第七級：100.1公斤以上
                string[] args = in_l3.Split(new char[] { '：' }, StringSplitOptions.RemoveEmptyEntries);
                if (args == null || args.Length <= 0) continue;

                var map = gMap;
                var score = 0;
                if (in_gender == "女")
                {
                    map = wMap;
                    score = 100;
                }
                else if (in_gender == "男")
                {
                    map = mMap;
                    score = 200;
                }

                if (map.Count <= 0) continue;

                var lv = args[0];//第一級;
                var wv = map.Find(x => x.k == lv);
                if (wv == null) continue;

                //var w_existed = rMap.Find(x => x.v.Contains(wv));
                //if (w_existed == null) continue;

                result.Add(new TKVC { k = key, v = wv.v, c = wv.c + score });
            }

            return result;
        }

        private List<TKVC> GetGroupWeightMap(TConfig cfg, string in_group)
        {
            var result = new List<TKVC>();
            string sql = @"SELECT in_no, in_level, in_weight FROM IN_GROUP_WEIGHT WITH(NOLOCK) WHERE in_group = N'" + in_group + "'";
            Item items = cfg.inn.applySQL(sql);
            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string key = item.getProperty("in_level", "");
                string value = item.getProperty("in_weight", "");
                string tag = value.ToLower().Replace("kg", "");
                int code = GetIntVal(item.getProperty("in_no", "0"));
                result.Add(new TKVC
                {
                    k = key,
                    v = value,
                    t = tag,
                    c = code,
                });
            }
            return result;
        }

        private List<TKVC> GetMeetingWeightMap(TConfig cfg)
        {
            List<TKVC> result = new List<TKVC>();
            string sql = @"SELECT in_sub_id, in_name, in_ranges, in_weight FROM IN_MEETING_PSECT WHERE in_meeting = '" + cfg.meeting_id + "'";
            Item items = cfg.inn.applySQL(sql);
            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string key = item.getProperty("in_name", "").Replace("女子", "W").Replace("男子", "M");
                string value = item.getProperty("in_ranges", "");
                int code = GetIntVal(item.getProperty("in_sub_id", "0"));
                result.Add(new TKVC { k = key, v = value, c = code });
            }
            return result;
        }

        private void AppendOrgPlayerSheet(TConfig cfg
            , TOrg org
            , List<TSection> sects
            , Spire.Xls.Workbook workbook
            , Spire.Xls.Worksheet sheetTemplate
            , List<TKVC> soloMap)
        {
            Spire.Xls.Worksheet sheet = workbook.CreateEmptySheet();
            sheet.CopyFrom(sheetTemplate);
            sheet.Name = org.sheet_name;

            string title = cfg.mt_title;
            if (cfg.section_name != "") title += "(" + cfg.section_name + "組)";

            //活動標題
            var meeting_pos = "A1";
            var meeting_mr = sheet.Range[meeting_pos];
            meeting_mr.Text = title;
            meeting_mr.Style.Font.Size = 22;

            //單位名稱
            var org_pos = "B7";
            var org_mr = sheet.Range[org_pos];
            org_mr.Text = org.in_stuff_b1 == ""
                ? org.short_name
                : org.in_stuff_b1 + " " + org.short_name;

            //抽籤日期
            var date_pos = "G4";
            var date_mr = sheet.Range[date_pos];
            date_mr.Text = cfg.report_date;

            int wsRow = 11;
            foreach (var sect in sects)
            {
                var musers = ResortMUsers(sect, soloMap);
                foreach (var muser in musers)
                {
                    sheet.Range["B" + wsRow].Text = sect.isTeam ? muser.weight_sect : sect.sect_name;
                    sheet.Range["C" + wsRow].Text = muser.in_name;
                    sheet.Range["D" + wsRow].Text = muser.in_gender;
                    sheet.Range["E" + wsRow].Text = muser.seed;
                    sheet.Range["F" + wsRow].Text = "□";
                    wsRow++;
                }
            }
            wsRow++;
            wsRow++;

            var coach_pos = "C" + wsRow + ":" + "F" + wsRow;
            var coach_mr = sheet.Range[coach_pos];
            coach_mr.Merge();
            coach_mr.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Left;
            coach_mr.Style.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;
            coach_mr.Borders[Spire.Xls.BordersLineType.EdgeBottom].LineStyle = Spire.Xls.LineStyleType.Thick;
            coach_mr.Text = "領隊(教練)簽名: ";
            wsRow++;

            sheet.SetRowHeight(wsRow, 3);
            wsRow++;

            var memo_pos = "C" + wsRow;
            var memo_mr = sheet.Range[memo_pos];
            memo_mr.Text = "請各單位確認無誤後簽名";
        }

        private List<TMUser> ResortMUsers(TSection sect, List<TKVC> soloMap)
        {
            if (!sect.isTeam || soloMap.Count == 0) return sect.MUsers;
            List<TMUser> list = new List<TMUser>();
            foreach (var muser in sect.MUsers)
            {
                var existed = soloMap.Find(x => x.k == muser.in_sno);
                if (existed == null)
                {
                    muser.weight_sect = sect.in_l3;
                }
                else
                {
                    muser.weight_id = existed.c;
                    muser.weight_sect = sect.in_l3 + " (" + existed.v + ")";
                    list.Add(muser);
                }
            }

            if (list.Count == 0) return sect.MUsers;
            return list.OrderBy(x => x.weight_id).ToList();
        }

        //private string GetPlayerWeightText(TSection sect, TMUser muser, Dictionary<string, string> soloMap)
        //{
        //    if (soloMap.Count > 0 && soloMap.ContainsKey(muser.in_sno))
        //    {
        //        return sect.in_l3 + " (" + soloMap[muser.in_sno] + ")";
        //    }
        //    return sect.in_l3;
        //}

        #endregion 參賽選手確認單


        private List<TOrg> MapOrgList(TConfig cfg, Item items)
        {
            List<TOrg> result = new List<TOrg>();
            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                var muser = MapMUser(cfg, item);
                var org = result.Find(x => x.sheet_name == muser.sheet_name);
                if (org == null)
                {
                    org = MapOrg(cfg, muser);
                    org.no = result.Count + 1;
                    result.Add(org);
                }
                org.player_count++;

                var sect = org.Sections.Find(x => x.program_id == muser.program_id);
                if (sect == null)
                {
                    sect = MapSection(cfg, muser);
                    org.Sections.Add(sect);
                }
                sect.MUsers.Add(muser);
            }
            return result;
        }

        private Item GetMUsers(TConfig cfg, string fight_day, string l1_opt)
        {
            string sect_cond = cfg.section_name == ""
                ? ""
                : "AND t1.in_name LIKE N'%" + cfg.section_name + "%'";

            string day_cond = string.IsNullOrWhiteSpace(fight_day)
                ? ""
                : "AND t1.in_fight_day = '" + fight_day + "'";

            string l1_cond = "";
            switch (l1_opt)
            {
                case "solo": l1_cond = "AND t1.in_l1 <> N'團體組'"; break;
                case "team": l1_cond = "AND t1.in_l1 = N'團體組'"; break;
            }

            string sql = @"
                SELECT 
	                t1.id              AS 'program_id'
	                , t1.in_name       AS 'program_name'
                    , t1.in_sort_order AS 'program_sort'
	                , t2.in_current_org
	                , t2.in_stuff_b1
	                , t2.in_short_org
	                , t2.in_name
	                , t2.in_gender
	                , t2.in_seed_lottery
	                , t2.in_l1
	                , t2.in_l2
	                , t2.in_l3
	                , t2.in_sno
                FROM 
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_USER t2 WITH(NOLOCK)
	                ON t2.source_id = t1.in_meeting
	                AND t2.in_l1 = t1.in_l1
	                AND t2.in_l2 = t1.in_l2
	                AND t2.in_l3 = t1.in_l3
                WHERE 
	                t1.in_meeting = '{#meeting_id}'
	                AND t2.in_l1 NOT IN (N'隊職員', N'常年會費')
	                {#sect_cond}
	                {#day_cond}
                    {#l1_cond}
                ORDER BY
	                t2.in_city_no
	                , t2.in_stuff_b1
                    , t1.in_sort_order
                    , t2.in_team
                    , t2.in_gender
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#sect_cond}", sect_cond)
                .Replace("{#day_cond}", day_cond)
                .Replace("{#l1_cond}", l1_cond);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        private class TOrg
        {
            public int no { get; set; }
            public string sheet_name { get; set; }
            public string in_stuff_b1 { get; set; }
            public string long_name { get; set; }
            public string short_name { get; set; }
            public List<TSection> Sections { get; set; }
            public int player_count { get; set; }
        }

        private class TSection
        {
            public string program_id { get; set; }
            public string program_name { get; set; }
            public string in_gender { get; set; }
            public int code { get; set; }
            public int sort_order { get; set; }

            public string sect_name { get; set; }
            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }
            public List<TMUser> MUsers { get; set; }

            public bool isTeam { get; set; }
        }

        private class TMUser
        {
            public string sheet_name { get; set; }
            public string in_current_org { get; set; }
            public string in_stuff_b1 { get; set; }
            public string in_short_org { get; set; }

            public string program_id { get; set; }
            public string program_name { get; set; }
            public string program_sort { get; set; }

            public string in_name { get; set; }
            public string in_gender { get; set; }
            public string in_sno { get; set; }
            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }

            public string in_seed_lottery { get; set; }
            public string seed { get; set; }

            public int weight_id { get; set; }
            public string weight_sect { get; set; }
        }

        private TOrg MapOrg(TConfig cfg, TMUser obj)
        {
            var result = new TOrg
            {
                in_stuff_b1 = obj.in_stuff_b1,
                sheet_name = obj.sheet_name,
                long_name = obj.in_current_org,
                short_name = obj.in_short_org,
                Sections = new List<TSection>(),
                player_count = 0,
            };


            return result;
        }

        private TSection MapSection(TConfig cfg, TMUser obj)
        {
            var result = new TSection
            {
                program_id = obj.program_id,
                program_name = obj.program_name,
                sect_name = obj.program_name,
                in_gender = obj.in_gender,
                in_l1 = obj.in_l1,
                in_l2 = obj.in_l2,
                in_l3 = obj.in_l3,
                MUsers = new List<TMUser>(),
                isTeam = obj.in_l1 == "團體組",
            };

            result.sect_name = result.sect_name.Replace("個-", "")
                .Replace(obj.in_l1 + "-", "");

            //預設女排在先
            if (obj.in_gender == "女")
            {
                result.code = 100;
            }
            else if (obj.in_gender == "男")
            {
                result.code = 200;
            }
            else
            {
                result.code = 300;
            }

            result.sort_order = result.code * 10000 + GetIntVal(obj.program_sort);

            return result;
        }

        private TMUser MapMUser(TConfig cfg, Item item)
        {
            var result = new TMUser
            {
                in_current_org = item.getProperty("in_current_org", ""),
                in_stuff_b1 = item.getProperty("in_stuff_b1", ""),
                in_short_org = item.getProperty("in_short_org", ""),

                program_id = item.getProperty("program_id", ""),
                program_name = item.getProperty("program_name", ""),
                program_sort = item.getProperty("program_sort", "0"),

                in_name = item.getProperty("in_name", ""),
                in_gender = item.getProperty("in_gender", ""),
                in_sno = item.getProperty("in_sno", ""),
                in_seed_lottery = item.getProperty("in_seed_lottery", ""),

                in_l1 = item.getProperty("in_l1", ""),
                in_l2 = item.getProperty("in_l2", ""),
                in_l3 = item.getProperty("in_l3", ""),
            };

            if (string.IsNullOrWhiteSpace(result.in_short_org))
            {
                result.sheet_name = result.in_current_org;
            }
            else
            {
                result.sheet_name = result.in_short_org;
            }

            if (result.in_seed_lottery != "" && result.in_seed_lottery != "0")
            {
                result.seed = "R";
            }
            else
            {
                result.seed = "";
            }

            return result;
        }


        #region Spire.Xls

        //設定資料列
        private void SetItemCell(TConfig cfg, Spire.Xls.Worksheet sheet, int wsRow, Item item, List<TField> fields)
        {
            foreach (var field in fields)
            {
                var cr = field.cs + wsRow;
                var va = "";

                if (field.getValue != null)
                {
                    va = field.getValue(cfg, field, item);
                }
                else if (field.property != "")
                {
                    va = item.getProperty(field.property, "");
                }

                SetCell(sheet, cr, va, field);
            }
        }

        //設定資料格
        private void SetCell(Spire.Xls.Worksheet sheet, string cr, string value, TField field)
        {
            var range = sheet.Range[cr];
            range.Style.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;
            range.Style.Font.FontName = field.fn;
            range.Style.Font.Size = field.fs;
            range.Style.Font.IsBold = field.fb;

            switch (field.css)
            {
                case TCss.None:
                    range.Text = value;
                    break;
                case TCss.Center:
                    range.Text = value;
                    range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                    break;
                case TCss.Text:
                    range.Text = "'" + value;
                    range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                    break;
                case TCss.Number:
                    range.NumberValue = GetIntVal(value);
                    range.NumberFormat = "0";
                    range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Right;
                    break;
            }

            if (field.need_fit)
            {
                range.Style.ShrinkToFit = true;
            }
        }

        //設定格線
        private void SetRangeBorder(Spire.Xls.Worksheet sheet, string pos)
        {
            var range = sheet.Range[pos];
            range.BorderInside(Spire.Xls.LineStyleType.Thin, System.Drawing.Color.Black);
            range.BorderAround(Spire.Xls.LineStyleType.Thin, System.Drawing.Color.Black);
        }

        //設定格線
        private void SetRangeBorder(Spire.Xls.Worksheet sheet, List<TField> fields, int mnRow, int wsRow)
        {
            var tbl_ps = fields.First().cs + mnRow;
            var tbl_pe = fields.Last().cs + (wsRow - 1);
            SetRangeBorder(sheet, tbl_ps + ":" + tbl_pe);
        }

        private void MapCharSet(TConfig cfg, List<TField> fields)
        {
            foreach (var field in fields)
            {
                field.cs = cfg.CharSet[field.ci];
            }
        }

        private void MapCharSetAndIndex(TConfig cfg, List<TField> fields, string fontName, int fontSize, bool isBold, int start = 0)
        {
            int no = start;

            foreach (var field in fields)
            {
                field.ci = no + 1;
                field.cs = cfg.CharSet[field.ci];
                field.fb = isBold;
                if (fontSize > 0)
                {
                    field.fs = fontSize;
                }
                if (!string.IsNullOrEmpty(fontName))
                {
                    field.fn = fontName;
                }

                no++;
            }
        }

        private void SetHeadRow(Spire.Xls.Worksheet sheet, int wsRow, List<TField> fields, TField head_field)
        {
            foreach (var field in fields)
            {
                string cr = field.cs + wsRow;
                SetHead(sheet, cr, field.title, head_field);
            }
        }

        //設定標題列
        private void SetHead(Spire.Xls.Worksheet sheet, string cr, string value, TField field)
        {
            var range = sheet.Range[cr];
            range.Text = value;
            //range.Style.Font.Color = System.Drawing.Color.White;
            //range.Style.Color = System.Drawing.Color.FromArgb(41, 92, 144);//#295C90
            range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
            range.Style.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;
            range.Style.Font.FontName = field.fn;
            range.Style.Font.Size = field.fs;
            range.Style.Font.IsBold = field.fb;
        }

        private void SetColumnWidth(Spire.Xls.Worksheet sheet, List<TField> fields)
        {
            foreach (var field in fields)
            {
                sheet.Columns[field.ci - 1].ColumnWidth = field.width;
            }
        }

        private void AutoFit(Spire.Xls.Worksheet sheet, List<TField> fields)
        {
            foreach (var field in fields)
            {
                sheet.Columns[field.ci - 1].AutoFitColumns();
            }
        }

        private class TField
        {
            public string title { get; set; }
            public string property { get; set; }
            public TCss css { get; set; }
            public int ci { get; set; }
            public string cs { get; set; }
            public double rh { get; set; }
            public double width { get; set; }
            public Func<TConfig, TField, Item, string> getValue { get; set; }

            public bool is_merge { get; set; }

            public string fn { get; set; }
            public int fs { get; set; }
            public bool fb { get; set; }

            public bool need_fit { get; set; }
        }

        private enum TCss
        {
            None = 0,
            Center = 10,
            Text = 100,
            Number = 200,
            Money = 300,
            Day = 400,
        }

        private string[] GetCharSet()
        {
            return new string[] { "", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
        }

        #endregion Spire.Xls

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }

            public string meeting_id { get; set; }
            public string report_date { get; set; }
            public string section_name { get; set; }
            public string scene { get; set; }
            public string page { get; set; }
            public string group { get; set; }
            public string org { get; set; }
            public string mode { get; set; }

            public bool hasDay { get; set; }
            public string fight_day { get; set; }

            public Item itmMeeting { get; set; }
            public string mt_title { get; set; }
            public string fight_site { get; set; }


            public Item itmFightDay { get; set; }


            public string[] CharSet { get; set; }
        }

        private string GetFightSite(TConfig cfg)
        {
            string sql = @"
                SELECT TOP 1 
                    id
                    , in_place 
                FROM 
                    IN_MEETING_ALLOCATION WITH(NOLOCK)
                WHERE 
                    in_meeting = '{#meeting_id}'
                    AND ISNULL(in_place, '') <> ''
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            Item itmAllocation = cfg.inn.applySQL(sql);

            if (itmAllocation.isError() || itmAllocation.getResult() == "")
            {
                return "";
            }
            else
            {
                return itmAllocation.getProperty("in_place", "");
            }
        }

        private TP MapTP(TConfig cfg, Item item)
        {
            string program_short = item.getProperty("program_short", "");
            string program_weight = item.getProperty("program_weight", "");

            string in_team_count = item.getProperty("in_team_count", "0");
            string in_rank_count = item.getProperty("in_rank_count", "0");

            string in_fight_day = item.getProperty("in_fight_day", "");
            string in_site_mat = item.getProperty("in_site_mat", "");
            string in_show_rank = item.getProperty("in_show_rank", "0");

            string in_stuff_b1 = item.getProperty("in_stuff_b1", "");
            string in_short_org = item.getProperty("in_short_org", "");

            var result = new TP();

            if (program_short.Contains("團"))
            {
                result.n1 = program_short;
                result.n2 = "";
                result.n3 = "";
                result.n4 = in_team_count;
            }
            else if (program_short.Length == 4)
            {
                result.n1 = program_short.Substring(0, 3);
                result.n2 = program_short[3].ToString();
                result.n3 = program_weight;
                result.n4 = in_rank_count;
            }
            else if (program_short.Length == 3)
            {
                result.n1 = program_short.Substring(0, 2);
                result.n2 = program_short[2].ToString();
                result.n3 = program_weight;
                result.n4 = in_rank_count;
            }
            else
            {
                result.n1 = program_short;
                result.n2 = "";
                result.n3 = program_weight;
                result.n4 = in_rank_count;
            }

            result.f1 = in_fight_day;
            result.f2 = in_site_mat;
            result.f3 = in_show_rank;

            var dt = GetDtmVal(in_fight_day);
            if (dt != DateTime.MinValue)
            {
                result.f1 = dt.ToString("MM-dd");
            }
            else
            {
                result.f1 = "";
            }
            result.f3 = "<span style='color: red; font-weight: bold'>" + in_show_rank + "</span>";

            result.o1 = "";
            result.o2 = in_short_org;
            if (in_stuff_b1.Length == 3)
            {
                result.o1 = in_stuff_b1;
            }

            return result;
        }

        private class TP
        {
            public string value { get; set; }

            public string n1 { get; set; }
            public string n2 { get; set; }
            public string n3 { get; set; }
            public string n4 { get; set; }

            public string f1 { get; set; }
            public string f2 { get; set; }
            public string f3 { get; set; }

            public string o1 { get; set; }
            public string o2 { get; set; }
        }

        public class TKVC
        {
            public string k { get; set; }
            public string v { get; set; }
            public string t { get; set; }
            public int c { get; set; }
        }

        private int GetIntVal(string value, int def = 0)
        {
            if (value == "") return def;
            int result = def;
            int.TryParse(value, out result);
            return result;
        }

        private DateTime GetDtmVal(string value, int hours = 0)
        {
            if (value == "") return DateTime.MinValue;
            DateTime result = DateTime.MinValue;
            if (DateTime.TryParse(value, out result))
            {
                return result.AddHours(hours);
            }
            return DateTime.MinValue;
        }
    }
}