﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Judo.Fight
{
    public class in_meeting_draw : Item
    {
        public in_meeting_draw(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 組別抽籤
                輸入: meeting_id
                輸出: 
                    1. 組別
                    2. 籤表
                人員: lina
                日誌: 
                    2020-07-20: 改為 team 版本 (lina)
                    2020-06-22: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]in_meeting_draw";

            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "input dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,

                meeting_id = itmR.getProperty("meeting_id", "").Trim(),
                program_id = itmR.getProperty("program_id", "").Trim(),
                exe_type = itmR.getProperty("exe_type", "").Trim(),
                in_draw_group = itmR.getProperty("in_draw_group", "").Trim(),
            };

            if (cfg.meeting_id == "")
            {
                throw new Exception("賽事 id 不得為空白");
            }

            string aml = "";
            string sql = "";

            var cols = new List<string>();
            cols.Add("in_title");
            cols.Add("in_draw_status");
            cols.Add("in_draw_file");
            cols.Add("in_battle_type");
            cols.Add("in_banner_photo");
            cols.Add("in_banner_photo2");
            cols.Add("in_language");

            aml = @"<AML><Item type='In_Meeting' action='get' id='{#meeting_id}' select='" + string.Join(", ", cols) + "'></Item></AML>";
            aml = aml.Replace("{#meeting_id}", cfg.meeting_id);

            Item itmMeeting = inn.applyAML(aml);
            if (itmMeeting.isError())
            {
                throw new Exception("取得賽事資料發生錯誤");
            }

            cfg.in_language = itmMeeting.getProperty("in_language", "");
            cfg.is_english = cfg.in_language == "en";

            SetButtonStatus(itmMeeting, itmR);

            itmR.setProperty("id", itmMeeting.getProperty("id", ""));
            itmR.setProperty("in_title", itmMeeting.getProperty("in_title", ""));
            itmR.setProperty("in_draw_status", itmMeeting.getProperty("in_draw_status", ""));
            itmR.setProperty("banner_file", itmMeeting.getProperty("in_banner_photo", ""));
            itmR.setProperty("banner_file2", itmMeeting.getProperty("in_banner_photo2", ""));
            itmR.setProperty("in_language", itmMeeting.getProperty("in_language", ""));
            itmR.setProperty("en_app5_js", cfg.is_english ? "<script src='../Scripts/in_app5_en.js?t=20230202'></script>" : "");
            //itmR.setProperty("level_array", level_array);

            AppendPrograms(cfg, itmR);

            //抽籤演算法
            Item itmDrawMode = inn.applySQL("SELECT in_value FROM IN_VARIABLE WITH(NOLOCK) WHERE in_name = 'draw_mode'");
            cfg.draw_mode = itmDrawMode.getProperty("in_value", "").ToLower();
            cfg.is_tkd_mode = cfg.draw_mode == "tkd";

            //抽籤時同隊不同邊
            Item itmSameOrgMode = inn.applySQL("SELECT in_value FROM IN_MEETING_VARIABLE WITH(NOLOCK) WHERE source_id = '" + cfg.meeting_id + "' AND in_key = 'same_org_diff_course'");
            if (itmSameOrgMode.getResult() == "") itmSameOrgMode = inn.newItem();
            var same_org_diff_course = itmSameOrgMode.getProperty("in_value", "");
            itmR.setProperty("same_org_diff_course", same_org_diff_course == "" ? "O" : same_org_diff_course);

            Item itmTeams = null;
            switch (cfg.exe_type)
            {
                case "players":
                    itmTeams = GetPTeams(cfg);
                    if (cfg.is_english)
                    {
                        AppendPTeamsEN(cfg, itmMeeting, itmTeams, itmR);
                    }
                    else
                    {
                        AppendPTeamsTW(cfg, itmMeeting, itmTeams, itmR);
                    }
                    break;

                case "xls":
                    itmTeams = GetPTeams(cfg);

                    //建立籤表 XLS
                    GenerateExcel(cfg, itmMeeting, itmTeams, itmR);

                    //更新賽程狀態
                    string in_draw_file = itmR.getProperty("in_draw_file", "");
                    sql = "UPDATE IN_MEETING SET in_draw_status = '1', in_draw_file = N'" + in_draw_file + "' WHERE id = '" + cfg.meeting_id + "'";
                    inn.applySQL(sql);
                    break;

                case "save":
                    //儲存籤號
                    sql = "UPDATE IN_MEETING_PTEAM SET in_sign_no_glacier = in_sign_no WHERE in_meeting = '" + cfg.meeting_id + "'";
                    inn.applySQL(sql);

                    //更新賽程狀態
                    sql = "UPDATE IN_MEETING SET in_draw_status = N'2' WHERE id = '" + cfg.meeting_id + "'";
                    inn.applySQL(sql);

                    //自動晉級
                    // AutoUpgrade(CCO, strMethodName, inn, itmMeeting, itmR);
                    break;

            }

            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

            return itmR;
        }

        // //自動晉級
        // private void AutoUpgrade(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmMeeting, Item itmReturn)
        // {
        //     string sql = "";

        //     string meeting_id = itmMeeting.getProperty("id", "");

        //     if (meeting_id == "")
        //     {
        //         return;
        //     }

        //     sql = "SELECT * FROM In_Meeting_Program WITH(NOLOCK) WHERE in_meeting = '" + meeting_id + "'";

        //     Item itmPrograms = inn.applySQL(sql);

        //     int count = itmPrograms.getItemCount();

        //     for (int i = 0; i < count; i++)
        //     {
        //         Item itmProgram = itmPrograms.getItemByIndex(i);

        //         //自動勝出
        //         itmProgram.setType("In_Meeting_Program");
        //         itmProgram.setProperty("meeting_id", itmMeeting.getProperty("id", ""));
        //         itmProgram.setProperty("program_id", itmProgram.getProperty("id", ""));
        //         itmProgram.setProperty("mode", "upgrade");

        //         Item itmMethodResult = itmProgram.apply("in_meeting_program_preview");
        //     }
        // }

        private void SetButtonStatus(Item itmMeeting, Item itmReturn)
        {
            string in_draw_status = itmMeeting.getProperty("in_draw_status", "");

            if (in_draw_status == "" || in_draw_status == "0")
            {
                itmReturn.setProperty("can_generate_class", "item_show_1");
                itmReturn.setProperty("can_run_class", "item_show_0");
                itmReturn.setProperty("can_battle_class", "item_show_0");
            }
            else if (in_draw_status == "1")
            {
                //已產生過籤號檔，隱藏產生 XLS 按鈕
                itmReturn.setProperty("can_generate_class", "item_show_1");
                itmReturn.setProperty("can_run_class", "item_show_1");
                itmReturn.setProperty("can_battle_class", "item_show_0");
            }
            else
            {
                //已儲存籤表，不再異動籤號
                itmReturn.setProperty("can_generate_class", "item_show_0");
                itmReturn.setProperty("can_run_class", "item_show_0");
                itmReturn.setProperty("can_battle_class", "item_show_1");
            }
        }

        //附加賽事組別
        private void AppendPrograms(TConfig cfg, Item itmReturn)
        {
            string meeting_id = itmReturn.getProperty("meeting_id", "");
            string url_in_l1 = itmReturn.getProperty("l1", "");
            string url_in_l2 = itmReturn.getProperty("l2", "");
            string url_in_l3 = itmReturn.getProperty("l3", "");
            bool has_program = url_in_l1 != "";

            string sql = @"
                SELECT
                   *
                FROM
                    IN_MEETING_PROGRAM WITH(NOLOCK)
                WHERE
                    in_meeting = '{#meeting_id}'
                    AND ISNULL(in_program, '') <> 's'
                ORDER BY
                    in_sort_order
            ";

            sql = sql.Replace("{#meeting_id}", meeting_id);

            Item items = cfg.inn.applySQL(sql);
            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                if (has_program)
                {
                    string id = item.getProperty("id", "");
                    string in_l1 = item.getProperty("in_l1", "");
                    string in_l2 = item.getProperty("in_l2", "");
                    string in_l3 = item.getProperty("in_l3", "");
                    if (in_l1 == url_in_l1 && in_l2 == url_in_l2 && in_l3 == url_in_l3)
                    {
                        itmReturn.setProperty("selected_program", id);
                        itmReturn.setProperty("selected_l1", in_l1);
                        itmReturn.setProperty("selected_l2", in_l2);
                        itmReturn.setProperty("selected_l3", in_l3);
                    }
                }
                item.setType("inn_program");
                itmReturn.addRelationship(item);
            }
        }

        //附加賽事組別隊伍
        private Item GetPTeams(TConfig cfg)
        {
            string sql = @"
                SELECT
                   t2.id            AS 'program_id'
                   , t2.in_name     AS 'program_name'
                   , t2.in_name2    AS 'program_name2'
                   , t2.in_name3    AS 'program_name3'
                   , t2.in_display  AS 'program_display'
                   , t1.*
                FROM
                    VU_MEETING_PTEAM t1 WITH(NOLOCK)
                INNER JOIN
                    IN_MEETING_PROGRAM t2 WITH(NOLOCK)
                    ON t2.id = t1.source_id
                WHERE
                    t2.in_meeting = '{#meeting_id}'
                    {#program_condition}
                    AND ISNULL(t1.in_type, '') IN ('', 'p')
                ORDER BY
                    CASE WHEN ISNULL(t1.in_seeds, '') = '' THEN 1 ELSE 0 END ASC
                    , t1.in_seeds ASC
                    , t1.in_city_no
                    , t1.in_stuff_b1
                    , t1.in_current_org
                    , t1.in_team
                    , t1.in_sno
                ";

            string pg_cond = cfg.program_id == ""
                    ? ""
                    : "AND t1.source_id = '" + cfg.program_id + "'";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#program_condition}", pg_cond);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        //附加賽事組別隊伍
        private void AppendPTeamsTW(TConfig cfg, Item itmMeeting, Item items, Item itmReturn)
        {
            string in_draw_status = itmMeeting.getProperty("in_draw_status", "");
            string inn_readonly = in_draw_status == "2" ? "readonly" : "";
            string inn_readonly_bg = in_draw_status == "2" ? "background-color: #DDDDDD;" : "";

            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            head.Append("<tr>");
            head.Append("  <th>No.</th>");
            head.Append("  <th>參賽單位</th>");
            head.Append("  <th data-field='in_label'>組別量級</th>");
            head.Append("  <th>姓名(隊別)</th>");

            if (cfg.is_tkd_mode)
            {
                head.Append("  <th data-field='in_sign_no'><span style='color: blue;'>籤號</span></th>");
            }
            else
            {
                head.Append("  <th data-field='in_judo_no' {#vsb_juso_no}><span style='color: red;'>籤號</span></th>");
            }

            head.Append("  <th>種子籤</th>");
            head.Append("</tr>");

            string inputSignNo = "<input type='text'"
                + " class='text ctrl_sign_no'"
                + " style='text-align: right; width: 60px; color: red; '"
                + " value='{#value}'"
                + " onkeyup=\"doUpdateTkdNumber('{#source_id}', '{#id}', this)\" />";

            string inputJudoNo = "<input type='text'"
                + " class='text ctrl_judo_no'"
                + " style='text-align: right; width: 60px; color: red; '"
                + " value='{#value}'"
                + " onkeyup=\"doUpdateJudoNumber('{#source_id}', '{#id}', this)\" />";

            string inputSeedNo = "<input type='text'"
                + " class='text'"
                + " style='text-align: right; width: 60px; " + inn_readonly_bg + "'"
                + " value='{#value}'"
                + " onkeyup=\"doUpdateSeed('{#source_id}', '{#id}', this)\" " + inn_readonly + " />";

            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string id = item.getProperty("id", "");
                string source_id = item.getProperty("source_id", "");
                string in_team_index = item.getProperty("in_team_index", "");
                string map_org_name = item.getProperty("map_org_name", "");
                string program_name3 = item.getProperty("program_name3", "");
                string in_names = item.getProperty("in_names", "");
                string in_weight_message = item.getProperty("in_weight_message", "");
                string in_sign_no = item.getProperty("in_sign_no", "");
                string in_judo_no = item.getProperty("in_judo_no", "");
                string in_seeds = item.getProperty("in_seeds", "");

                if (in_weight_message != "")
                {
                    in_names = "(DQ)" + in_names;
                }

                string signNoCtrl = inputSignNo.Replace("{#value}", in_sign_no)
                    .Replace("{#source_id}", source_id)
                    .Replace("{#id}", id);

                string judoNoCtrl = inputJudoNo.Replace("{#value}", in_judo_no)
                    .Replace("{#source_id}", source_id)
                    .Replace("{#id}", id);

                string seedNoCtrl = inputSeedNo.Replace("{#value}", in_seeds)
                    .Replace("{#source_id}", source_id)
                    .Replace("{#id}", id);

                body.Append("<tr>");
                //body.Append("  <td class='inn_player_no'>" + in_team_index + "</td>");
                body.Append("  <td class='inn_player_no'>" + (i + 1) + "</td>");
                body.Append("  <td>" + map_org_name + "</td>");
                body.Append("  <td>" + program_name3 + "</td>");
                body.Append("  <td>" + in_names + "</td>");

                if (cfg.is_tkd_mode)
                {
                    body.Append("  <td>" + signNoCtrl + "</td>");
                }
                else
                {
                    body.Append("  <td>" + judoNoCtrl + "</td>");
                }

                body.Append("  <td>" + seedNoCtrl + "</td>");
                body.Append("</tr>");
            }

            StringBuilder table = new StringBuilder();
            table.Append("<table id='draw_player'");
            table.Append("  class='table table-bordered'");
            table.Append("  data-toggle='table'");
            table.Append("  data-fixed-columns='false'");
            table.Append("  data-fixed-number='0'");
            table.Append("  data-virtual-scroll='true'>");
            table.Append("<thead>");
            table.Append(head);
            table.Append("</thead>");
            table.Append("<tbody>");
            table.Append(body);
            table.Append("</tbody>");
            table.Append("</table>");

            string teamCountShow = "";
            if (count >= 0)
            {
                teamCountShow = items.getItemByIndex(0).getProperty("program_name2", "") + " (" + count + ")";
            }

            table.Append("<div style='display:none; padding: 0; margin: 0'>");
            table.Append("  <p id='SubPageTeamCount'>" + teamCountShow + "</p>");
            table.Append("</div>");

            itmReturn.setProperty("inn_table", table.ToString());
        }

        //附加賽事組別隊伍
        private void AppendPTeamsEN(TConfig cfg, Item itmMeeting, Item items, Item itmReturn)
        {
            string in_draw_status = itmMeeting.getProperty("in_draw_status", "");
            string inn_readonly = in_draw_status == "2" ? "readonly" : "";
            string inn_readonly_bg = in_draw_status == "2" ? "background-color: #DDDDDD;" : "";

            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            head.Append("<tr>");
            head.Append("  <th data-field='in_label'>Categories</th>");
            head.Append("  <th>Serial</th>");
            head.Append("  <th>Org.</th>");
            head.Append("  <th>Name</th>");

            if (cfg.is_tkd_mode)
            {
                head.Append("  <th data-field='in_sign_no'><span style='color: blue;'>Draw</span></th>");
            }
            else
            {
                head.Append("  <th data-field='in_judo_no' {#vsb_juso_no}><span style='color: red;'>Draw</span></th>");
            }

            head.Append("  <th>lottery</th>");
            head.Append("</tr>");

            string inputSignNo = "<input type='text'"
                + " class='text ctrl_sign_no'"
                + " style='text-align: right; width: 60px; color: red; '"
                + " value='{#value}'"
                + " onkeyup=\"doUpdateTkdNumber('{#source_id}', '{#id}', this)\" />";

            string inputJudoNo = "<input type='text'"
                + " class='text ctrl_judo_no'"
                + " style='text-align: right; width: 60px; color: red; '"
                + " value='{#value}'"
                + " onkeyup=\"doUpdateJudoNumber('{#source_id}', '{#id}', this)\" />";

            string inputSeedNo = "<input type='text'"
                + " class='text'"
                + " style='text-align: right; width: 60px; " + inn_readonly_bg + "'"
                + " value='{#value}'"
                + " onkeyup=\"doUpdateSeed('{#source_id}', '{#id}', this)\" " + inn_readonly + " />";

            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string id = item.getProperty("id", "");
                string source_id = item.getProperty("source_id", "");
                string in_team_index = item.getProperty("in_team_index", "");
                string map_org_name = item.getProperty("map_org_name", "");
                string program_name3 = item.getProperty("program_name3", "");
                string in_names = item.getProperty("in_names", "");
                string in_weight_message = item.getProperty("in_weight_message", "");
                string in_sign_no = item.getProperty("in_sign_no", "");
                string in_judo_no = item.getProperty("in_judo_no", "");
                string in_seeds = item.getProperty("in_seeds", "");

                if (in_weight_message != "")
                {
                    in_names = "(DQ)" + in_names;
                }

                string signNoCtrl = inputSignNo.Replace("{#value}", in_sign_no)
                    .Replace("{#source_id}", source_id)
                    .Replace("{#id}", id);

                string judoNoCtrl = inputJudoNo.Replace("{#value}", in_judo_no)
                    .Replace("{#source_id}", source_id)
                    .Replace("{#id}", id);

                string seedNoCtrl = inputSeedNo.Replace("{#value}", in_seeds)
                    .Replace("{#source_id}", source_id)
                    .Replace("{#id}", id);

                map_org_name = "<img src='../images/flags/" + map_org_name + ".png' style='padding: 3px; width: 30px'>"
                    + map_org_name;

                body.Append("<tr>");
                body.Append("  <td>" + program_name3 + "</td>");
                body.Append("  <td class='inn_player_no'>" + (i + 1) + "</td>");
                body.Append("  <td>" + map_org_name + "</td>");
                body.Append("  <td>" + in_names + "</td>");

                if (cfg.is_tkd_mode)
                {
                    body.Append("  <td>" + signNoCtrl + "</td>");
                }
                else
                {
                    body.Append("  <td>" + judoNoCtrl + "</td>");
                }

                body.Append("  <td>" + seedNoCtrl + "</td>");
                body.Append("</tr>");
            }

            StringBuilder table = new StringBuilder();
            table.Append("<table id='draw_player'");
            table.Append("  class='table table-bordered'");
            table.Append("  data-toggle='table'");
            table.Append("  data-fixed-columns='false'");
            table.Append("  data-fixed-number='0'");
            table.Append("  data-virtual-scroll='true'>");
            table.Append("<thead>");
            table.Append(head);
            table.Append("</thead>");
            table.Append("<tbody>");
            table.Append(body);
            table.Append("</tbody>");
            table.Append("</table>");

            itmReturn.setProperty("inn_table", table.ToString());
        }

        ////附加賽事組別隊伍
        //private void AppendPTeams(Item itmMeeting, Item items, Item itmReturn)
        //{
        //    string in_draw_status = itmMeeting.getProperty("in_draw_status", "");
        //    string inn_readonly = in_draw_status == "2" ? "readonly" : "";
        //    string inn_readonly_bg = in_draw_status == "2" ? "background-color: #DDDDDD;" : "";

        //    int count = items.getItemCount();

        //    for (int i = 0; i < count; i++)
        //    {
        //        Item item = items.getItemByIndex(i);
        //        string in_names = item.getProperty("in_names", "");
        //        string in_weight_message = item.getProperty("in_weight_message", "");
        //        if (in_weight_message != "")
        //        {
        //            item.setProperty("in_names", "(DQ)" + in_names);
        //        }

        //        item.setType("inn_team");
        //        item.setProperty("inn_readonly", inn_readonly);
        //        item.setProperty("inn_readonly_bg", inn_readonly_bg);

        //        itmReturn.addRelationship(item);
        //    }
        //}

        /// <summary>
        /// 建立籤表 XLS (xlsx)
        /// </summary>
        private void GenerateExcel(TConfig cfg, Item itmMeeting, Item itmRows, Item itmReturn)
        {
            Item itmPath = GetExcelPath(cfg, "draw_path");

            string export_path = itmPath.getProperty("export_path", "");
            string folder_path = export_path.TrimEnd('\\') + "\\" + cfg.meeting_id + "\\";
            if (!System.IO.Directory.Exists(folder_path))
            {
                System.IO.Directory.CreateDirectory(folder_path);
            }

            string ext_name = ".xlsx";
            string xls_name = DateTime.Now.ToString("yyyyMMdd_HHmmss_fff");
            string xls_file = folder_path + xls_name + ext_name;
            string xls_url = cfg.meeting_id + "/" + xls_name + ext_name;
            string sheet_name = "sheet1";

            string in_draw_file = xls_name + ext_name;

            ClosedXML.Excel.XLWorkbook workbook = new ClosedXML.Excel.XLWorkbook();
            ClosedXML.Excel.IXLWorksheet sheet = workbook.Worksheets.Add(sheet_name);

            //lina 2020.08.04: 與 亂數抽籤 DLL 串接，標題列為欄位名稱，不得改簡體中文
            string[] titles = new string[]
            {
        "選手編號",
        "單位",
        "代號",
        "組別",
        "姓名",
        "籤號",
        "順序",
        "積分",
            };

            string[] fields = new string[]
            {
        "in_team_index",
        "in_current_org",
        "in_alias",
        "program_name3",
        "in_names",
        "in_sign_no",
        "in_sort_order",
        "in_points"
            };

            string[] formats = new string[]
            {
        "", //"選手編號",
        "", //"單位",
        "", //"代號",
        "", //"組別",
        "", //"姓名",
        "int", //"籤號",
        "int", //"順序",
        "int", //"積分",
            };

            int wsRow = 1;
            int wsCol = 1;
            int field_count = fields.Length;
            int raw_count = itmRows.getItemCount();
            int last_index = raw_count - 1;

            for (int i = 0; i < field_count; i++)
            {
                string title = titles[i];

                ClosedXML.Excel.IXLCell cell = sheet.Cell(wsRow, wsCol);
                cell.Value = title;
                cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                cell.Style.Font.Bold = true;
                cell.DataType = ClosedXML.Excel.XLDataType.Text;
                wsCol++;
            }

            for (int i = 0; i < raw_count; i++)
            {
                wsRow++;
                wsCol = 1;

                Item itmRow = itmRows.getItemByIndex(i);

                for (int j = 0; j < field_count; j++)
                {
                    string field_name = fields[j];
                    string field_format = formats[j];

                    string value = itmRow.getProperty(field_name, "0");

                    switch (field_name)
                    {
                        case "in_sign_no":
                        case "in_points":
                        case "in_sort_order":
                            if (value == "")
                            {
                                value = "0";
                            }
                            break;

                        default:
                            break;
                    }

                    ClosedXML.Excel.IXLCell cell = sheet.Cell(wsRow, wsCol);

                    if (value != "")
                    {
                        switch (field_format)
                        {
                            case "short_date":
                                cell.Value = value;
                                cell.Style.NumberFormat.Format = "yyyy/mm/dd";
                                cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                                break;
                            case "short_date_time":
                                cell.Value = value;
                                cell.Style.NumberFormat.Format = "yyyy/mm/dd HH:mm";
                                cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                                break;
                            case "int":
                                cell.Value = value;
                                break;
                            default:
                                SetCellTextValue(ref cell, value);
                                break;
                        }
                    }
                    else
                    {
                        cell.Value = value;
                    }

                    //自動伸縮欄寬
                    if (i == last_index)
                    {
                        sheet.Column(wsCol).AdjustToContents();
                        //sheet.Column(wsCol).Width += 1;
                    }

                    wsCol++;
                }
            }

            //寫入檔案
            workbook.SaveAs(xls_file);

            //輸出檔案路徑
            itmReturn.setProperty("xls_name", xls_url);
            itmReturn.setProperty("in_draw_file", in_draw_file);
        }

        private Item GetExcelPath(TConfig cfg, string in_name)
        {
            Item itmResult = cfg.inn.newItem();

            //Excel 匯出路徑 (from 原創.設定參數)
            string sql = @"
                SELECT TOP 1 
                    t2.in_name AS 'variable'
                    , t1.in_name, t1.in_value 
                FROM 
                    In_Variable_Detail t1 WITH(NOLOCK)
                INNER JOIN 
                    In_Variable t2 WITH(NOLOCK) ON t2.ID = t1.SOURCE_ID 
                WHERE 
                    t2.in_name = N'meeting_excel'  
                    AND t1.in_name = N'{#in_name}'
            ";

            sql = sql.Replace("{#in_name}", in_name);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item item = cfg.inn.applySQL(sql);

            if (item.isError())
            {
                throw new Exception("未設定匯出路徑 _# " + in_name);
            }

            itmResult.setProperty("export_path", item.getProperty("in_value", ""));

            return itmResult;
        }

        private void SetCellTextValue(ref ClosedXML.Excel.IXLCell cell, string textValue)
        {
            DateTime dtTmp;
            double dblTmp;

            if (DateTime.TryParse(textValue, out dtTmp))
            {
                cell.Value = "'" + textValue;
            }
            else if (double.TryParse(textValue, out dblTmp))
            {
                cell.Value = "'" + textValue;
            }
            else
            {
                cell.Value = textValue;
            }
            cell.DataType = ClosedXML.Excel.XLDataType.Text;
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }

            public string meeting_id { get; set; }
            public string program_id { get; set; }

            public string exe_type { get; set; }
            public string in_draw_group { get; set; }
            public string draw_mode { get; set; }
            public bool is_tkd_mode { get; set; }

            public string in_language { get; set; }
            public bool is_english { get; set; }
        }
    }
}