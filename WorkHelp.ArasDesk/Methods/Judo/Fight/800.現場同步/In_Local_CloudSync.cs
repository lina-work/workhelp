﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.PLMCTA.Common
{
    public class In_Local_CloudSync : Item
    {
        public In_Local_CloudSync(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 地雲同步
                日誌: 
                    - 2023-02-15: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "In_Local_CloudSync";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                strIdentityId = inn.getUserAliases(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                in_fight_day = itmR.getProperty("in_fight_day", ""),
                scene = itmR.getProperty("scene", ""),
            };

            switch (cfg.scene)
            {
                case "page":
                    Page(cfg, itmR);
                    break;

                case "upload":
                    Upload(cfg, itmR);
                    break;

                case "backup":
                    BackupScripts(cfg, itmR);
                    break;
            }

            return itmR;
        }

        //備份
        private void BackupScripts(TConfig cfg, Item itmReturn)
        {
            var settings = new TSettings
            {
                Source = new TApi
                {
                    BaseUrl = itmReturn.getProperty("in_target_url", ""),
                    Resource = "/Judo/GetScriptsCompress",
                },
                MeetingId = cfg.meeting_id,
                FightDay = cfg.in_fight_day,
            };

            var postData = GetUpdScriptList(settings, settings.MeetingId, settings.FightDay);
            if (postData.IsError || postData.Value == null || postData.Value.Count == 0)
            {
                throw new Exception(postData.Message);
            }

            try
            {
                var head = "來源: " + settings.Source.BaseUrl + "\r\n";
                var rows = postData.Value;
                InnSport.Core.Logging.TLog.Text(head + Newtonsoft.Json.JsonConvert.SerializeObject(rows));

                System.Threading.Thread.Sleep(1000);

                foreach (var row in rows)
                {
                    var json = Decompress(row.compress_cmd);
                    row.cmd_list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<string>>(json);
                }

                var contents = new StringBuilder();
                contents.AppendLine(head);
                contents.AppendLine();
                foreach (var row in rows)
                {
                    foreach (var cmd in row.cmd_list)
                    {
                        contents.AppendLine(cmd);
                    }
                    contents.AppendLine();
                }
                InnSport.Core.Logging.TLog.Text(contents.ToString());
            }
            catch (Exception ex)
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "BackupScripts: " + ex.Message);
            }
        }

        //從地端上傳到雲端
        private void Upload(TConfig cfg, Item itmReturn)
        {
            var settings = new TSettings
            {
                Source = new TApi
                {
                    BaseUrl = itmReturn.getProperty("in_local_url", ""),
                    Resource = "/Judo/GetScriptsCompress",
                },
                Destination = new TApi
                {
                    BaseUrl = itmReturn.getProperty("in_cloud_url", ""),
                    Resource = "/Judo/ExeScriptsCompress",
                },
                MeetingId = cfg.meeting_id,
                FightDay = cfg.in_fight_day,
            };

            if (settings.MeetingId == "") throw new Exception("活動 id 為必填");
            if (settings.FightDay == "") throw new Exception("比賽日期 為必填");
            if (settings.Source.BaseUrl == "") throw new Exception("資料來源網址 為必填");
            if (settings.Destination.BaseUrl == "") throw new Exception("目的地網址 為必填");

            var postData = GetUpdScriptList(settings, settings.MeetingId, settings.FightDay);

            if (postData.IsError)
            {
                throw new Exception(postData.Message);
            }

            var respData = ExeUpdScriptList(settings, postData.Value);

            if (respData.IsError)
            {
                throw new Exception(respData.Message);
            }
            else
            {
                itmReturn.setProperty("execute_message", respData.Message);
            }
        }

        private TWrapper<string> ExeUpdScriptList(TSettings settings, List<TItemType> entities)
        {
            var result = new TWrapper<string> { Message = string.Empty, IsError = true };

            try
            {
                var baseUrl = settings.Destination.BaseUrl;
                var resource = settings.Destination.Resource;

                var client = new RestSharp.RestClient(baseUrl);
                //client.Timeout = 30 * 1000; // 30秒為逾期

                var request = new RestSharp.RestRequest(resource
                    , RestSharp.Method.POST
                    , RestSharp.DataFormat.Json);

                string json = Newtonsoft.Json.JsonConvert.SerializeObject(new
                {
                    Token = "0225585561",
                    Value = entities
                });

                //InnSport.Cloud.Sync.Logging.TLog.Text(json);

                request.AddJsonBody(json);

                var response = client.ExecuteAsync(request);
                var rsp_data = response.Result.Content;
                if (!string.IsNullOrWhiteSpace(response.Result.ErrorMessage))
                {
                    result.Message = response.Result.ErrorMessage + " (目的)";
                }
                else if (rsp_data == null)
                {
                    result.Message = "no response data" + " (目的)";
                }
                else
                {
                    result.Message = rsp_data;
                    result.IsError = false;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.Message + " (目的)";
            }
            return result;
        }

        private TWrapper<List<TItemType>> GetUpdScriptList(TSettings settings, string meeting_id, string fight_day)
        {
            var result = new TWrapper<List<TItemType>> { Message = string.Empty, IsError = true, };

            try
            {
                var baseUrl = settings.Source.BaseUrl;
                var resource = settings.Source.Resource;

                var client = new RestSharp.RestClient(baseUrl);
                //client.Timeout = 30 * 1000; // 30秒為逾期

                var request = new RestSharp.RestRequest(resource
                    , RestSharp.Method.POST
                    , RestSharp.DataFormat.Json);

                var json = Newtonsoft.Json.JsonConvert.SerializeObject(new
                {
                    Token = "0225585561",
                    Value = new
                    {
                        meeting_id = meeting_id,
                        fight_day = fight_day,
                    }
                });

                request.AddJsonBody(json);

                var response = client.ExecuteAsync<List<TItemType>>(request);
                var rsp_data = response.Result.Data;
                if (!string.IsNullOrWhiteSpace(response.Result.ErrorMessage))
                {
                    result.Message = response.Result.ErrorMessage + " (來源)";
                }
                else if (rsp_data == null || rsp_data.Count == 0)
                {
                    result.Message = "no response data" + " (來源)";
                }
                else
                {
                    result.IsError = false;
                    result.Value = rsp_data;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.Message + " (來源)";
            }
            return result;
        }

        //頁面
        private void Page(TConfig cfg, Item itmReturn)
        {
            AppendMeetingInfo(cfg, itmReturn);
            AppendFightDayMenu(cfg, itmReturn);
            itmReturn.setProperty("inn_cloud_url", GetCloudIP(cfg));
            itmReturn.setProperty("inn_local_url", GetLocalIP(cfg));
        }

        private void AppendMeetingInfo(TConfig cfg, Item itmReturn)
        {
            var sql = @"
                SELECT 
	                id
	                , item_number
	                , in_title
                FROM 
	                IN_MEETING WITH(NOLOCK) 
                WHERE 
                    id = '{#meeting_id}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            var itmMeeting = cfg.inn.applySQL(sql);

            itmReturn.setProperty("in_title", itmMeeting.getProperty("in_title", ""));
        }

        private void AppendFightDayMenu(TConfig cfg, Item itmReturn)
        {
            var sql = @"
                SELECT DISTINCT
	                in_fight_day
                FROM
	                IN_MEETING_PROGRAM WITH(NOLOCK)
                WHERE
	                in_meeting = '{#meeting_id}'
	                AND ISNULL(in_fight_day, '') <> ''
                ORDER BY
	                in_fight_day
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            var itmEmpty = cfg.inn.newItem();
            itmEmpty.setType("inn_day");
            itmEmpty.setProperty("value", "");
            itmEmpty.setProperty("text", "請選擇");
            itmReturn.addRelationship(itmEmpty);

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();

            var is_exists = false;
            var is_today = false;
            var day1 = "";
            var today = DateTime.Now.ToString("yyyy-MM-dd");

            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                string in_fight_day = item.getProperty("in_fight_day", "");
                if (i == 0)
                {
                    day1 = in_fight_day;
                }

                if (in_fight_day == cfg.in_fight_day)
                {
                    is_exists = true;
                }

                if (in_fight_day == today)
                {
                    is_today = true;
                }

                item.setType("inn_day");
                item.setProperty("value", in_fight_day);
                item.setProperty("text", in_fight_day);
                itmReturn.addRelationship(item);
            }

            if (!is_exists)
            {
                if (is_today)
                {
                    cfg.in_fight_day = today;
                    itmReturn.setProperty("in_fight_day", today);
                }
                else
                {
                    cfg.in_fight_day = day1;
                    itmReturn.setProperty("in_fight_day", day1);
                }
            }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }

            public string meeting_id { get; set; }
            public string in_fight_day { get; set; }
            public string scene { get; set; }
        }

        /// <summary>
        /// 封裝
        /// </summary>
        private class TWrapper
        {
            /// <summary>
            /// 訊息
            /// </summary>
            public string Message { get; set; }

            /// <summary>
            /// 是否錯誤
            /// </summary>
            public bool IsError { get; set; }
        }

        /// <summary>
        /// 封裝
        /// </summary>
        private class TWrapper<T> : TWrapper
        {
            /// <summary>
            /// 值
            /// </summary>
            public T Value { get; set; }
        }

        private class TSettings
        {
            /// <summary>
            /// 資料來源
            /// </summary>
            public TApi Source { get; set; }

            /// <summary>
            /// 目的地
            /// </summary>
            public TApi Destination { get; set; }

            /// <summary>
            /// 活動 id
            /// </summary>
            public string MeetingId { get; set; }

            /// <summary>
            /// 活動日期
            /// </summary>
            public string FightDay { get; set; }
        }

        /// <summary>
        /// Api 模型
        /// </summary>
        internal class TApi
        {
            /// <summary>
            /// 網址
            /// </summary>
            public string BaseUrl { get; set; }

            /// <summary>
            /// 接口
            /// </summary>
            public string Resource { get; set; }
        }

        /// <summary>
        /// 物件類型模型
        /// </summary>
        private class TItemType
        {
            /// <summary>
            /// 活動 id
            /// </summary>
            public string meeting_id { get; set; }

            /// <summary>
            /// 比賽日期
            /// </summary>
            public string fight_day { get; set; }

            /// <summary>
            /// 資料表
            /// </summary>
            public string table { get; set; }

            /// <summary>
            /// 參數
            /// </summary>
            public string variable { get; set; }

            /// <summary>
            /// 查詢指令
            /// </summary>
            public string qry_sql { get; set; }

            /// <summary>
            /// 生成指令
            /// </summary>
            public string crt_sql { get; set; }

            /// <summary>
            /// 更新指令
            /// </summary>
            public List<string> cmd_list { get; set; }

            /// <summary>
            /// 屬性清單
            /// </summary>
            public List<TProperty> PropList { get; set; }

            /// <summary>
            /// 生成指令經壓縮
            /// </summary>
            public string compress_cmd { get; set; }
        }

        /// <summary>
        /// 屬性模型
        /// </summary>
        private class TProperty
        {
            /// <summary>
            /// 名稱
            /// </summary>
            public string name { get; set; }

            /// <summary>
            /// 資料型別
            /// </summary>
            public string data_type { get; set; }
        }

        /// <summary>
        /// IP 位址
        /// </summary>
        private string GetCloudIP(TConfig cfg)
        {
            string result = string.Empty;

            try
            {
                //return "https://act.innosoft.com.tw/api";
                //return "https://app3.innosoft.com.tw/api";
                return "https://act2.innosoft.com.tw/api";
            }
            catch (Exception ex)
            {
                result = "IP error";
            }

            return result;
        }

        /// <summary>
        /// IP 位址
        /// </summary>
        private string GetLocalIP(TConfig cfg)
        {
            string result = string.Empty;

            try
            {
                // 取得本機名稱
                string strHostName = System.Net.Dns.GetHostName();

                // 取得本機的IpHostEntry類別實體，MSDN建議新的用法
                var iphostentry = System.Net.Dns.GetHostEntry(strHostName);

                foreach (var ipaddress in iphostentry.AddressList)
                {
                    // 只取得 IP V4 的Address
                    if (ipaddress.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                    {
                        result = ipaddress.ToString();
                    }
                }

                if (!string.IsNullOrWhiteSpace(result))
                {
                    result = "http://" + result.Trim('/') + "/" + "api";
                }
            }
            catch (Exception ex)
            {
                result = "IP error";
            }

            return result;
        }

        /// <summary>
        /// 解壓縮(將傳入的二進位字串資料以GZip演算法)
        /// </summary>
        /// <param name="value">傳入經 GZip 壓縮後的二進位字串資料</param>
        /// <returns>傳回原後的未壓縮原始字串資料</returns>
        private string Decompress(string value)
        {
            string result = string.Empty;

            if (!string.IsNullOrEmpty(value))
            {
                var zippedData = Convert.FromBase64String(value);

                if (zippedData != null && zippedData.Length > 0)
                {
                    var bytes = Decompress(zippedData);
                    result = DecompressBytes(bytes);
                }
            }

            return result;
        }

        /// <summary>
        /// GZip解壓縮
        /// </summary>
        /// <param name="zippedData">要解壓縮的byte[]</param>
        /// <returns></returns>
        private byte[] Decompress(byte[] zippedData)
        {
            byte[] result = null;

            using (var memoryStream = new MemoryStream(zippedData))
            {
                using (var gzipstream = new System.IO.Compression.GZipStream(memoryStream, System.IO.Compression.CompressionMode.Decompress))
                {
                    using (var resultStream = new MemoryStream())
                    {
                        gzipstream.CopyTo(resultStream);
                        result = resultStream.ToArray();
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// byte[] 轉換為 string
        /// </summary>
        /// <param name="bytes">要轉換之byte[]</param>
        /// <returns></returns>
        private string DecompressBytes(byte[] bytes)
        {
            if (bytes == null || bytes.Length == 0)
            {
                return string.Empty;
            }
            else
            {
                return System.Text.Encoding.UTF8.GetString(bytes);
            }
        }
    }
}