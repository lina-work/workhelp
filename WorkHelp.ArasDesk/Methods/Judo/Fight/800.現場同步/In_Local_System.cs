﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Judo.Fight
{
    public class In_Local_System : Item
    {
        public In_Local_System(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;            /*
                目的: 現場系統資訊
                輸入: meeting_id
                日期: 
                    2022-11-10: 創建 (lina)
             */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]In_Local_System";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "input dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                strIdentityId = inn.getUserAliases(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                scene = itmR.getProperty("scene", ""),
            };

            switch (cfg.scene)
            {
                case "export":
                    Export(cfg, itmR);
                    break;

                default:
                    Page(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void Export(TConfig cfg, Item itmReturn)
        {
            //匯出資訊
            Item itmXls = cfg.inn.applyMethod("In_Meeting_Excel_Path", "<in_name></in_name>");
            string export_path = itmXls.getProperty("export_path", "");

            //活動資訊
            Item itmMeeting = cfg.inn.applySQL("SELECT id, in_title FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            string mt_title = itmMeeting.getProperty("in_title", "");
            string pg_func = "現場系統資訊";
            string pg_title = mt_title + " " + pg_func;

            //資料來源
            var rows = new List<TRow>();
            AppendApiUrlInfo(cfg, rows, Environment.NewLine);
            AppendAdminCheckin(cfg, rows, Environment.NewLine);
            AppendAdminCentral(cfg, rows, Environment.NewLine);
            AppendRtmTree(cfg, rows, Environment.NewLine);

            //試算表
            var book = new Spire.Xls.Workbook();
            var sheet = book.CreateEmptySheet();
            sheet.Name = pg_func;

            //將所有欄放入單一頁面
            sheet.PageSetup.FitToPagesWide = 1;
            sheet.PageSetup.FitToPagesTall = 0;

            //邊界
            sheet.PageSetup.TopMargin = 0.3;
            sheet.PageSetup.LeftMargin = 0.3;
            sheet.PageSetup.RightMargin = 0;
            sheet.PageSetup.BottomMargin = 0;

            //活動列
            SetTitle(sheet, "A1:C1", 1, pg_title);
            sheet.SetRowHeight(1, 50);

            //標題列
            SetHead(sheet, "A2", "項目");
            SetHead(sheet, "B2", "連結網址");
            SetHead(sheet, "C2", "標的");
            sheet.SetRowHeight(2, 50);

            var wsRow = 3;
            for (int i = 0; i < rows.Count; i++)
            {
                var row = rows[i];
                SetCell(sheet, "A" + wsRow, row.title, TCss.Center);
                SetCell(sheet, "B" + wsRow, row.url.ToLower(), TCss.Left);
                SetCell(sheet, "C" + wsRow, row.note, TCss.Left);
                sheet.SetRowHeight(wsRow, 70);
                wsRow++;
            }

            var tbl_ps = "A2";
            var tbl_pe = "C" + (wsRow - 1);
            SetRangeBorder(sheet, tbl_ps + ":" + tbl_pe);

            sheet.Columns[0].ColumnWidth = 20;
            sheet.Columns[1].ColumnWidth = 60;
            sheet.Columns[2].ColumnWidth = 30;

            //移除空白 Sheet
            book.Worksheets[0].Remove();
            book.Worksheets[0].Remove();
            book.Worksheets[0].Remove();

            string guid = System.Guid.NewGuid().ToString().Substring(0, 4).ToUpper();

            string ext_name = ".xlsx";//".pdf";
            string pdfName = pg_title + guid;
            string pdfFile = export_path + pdfName + ext_name;

            //book.SaveToFile(pdfFile, Spire.Xls.FileFormat.PDF);
            book.SaveToFile(pdfFile, Spire.Xls.ExcelVersion.Version2013);

            itmReturn.setProperty("xls_name", pdfName + ext_name);
        }

        //設定格線
        private void SetRangeBorder(Spire.Xls.Worksheet sheet, string pos)
        {
            var range = sheet.Range[pos];
            range.BorderInside(Spire.Xls.LineStyleType.Thin, System.Drawing.Color.Black);
            range.BorderAround(Spire.Xls.LineStyleType.Thin, System.Drawing.Color.Black);
        }

        private void SetCell(Spire.Xls.Worksheet sheet, string cr, string text, TCss css)
        {
            var range = sheet.Range[cr];
            range.Text = text;
            switch(css)
            {
                case TCss.Center:
                    range.Style.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;
                    break;

                case TCss.Left:
                    range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Left;
                    break;
            }

            range.Style.Font.FontName = "Courier New";
            range.Style.Font.Size = 14;
            range.Style.Font.IsBold = false;
        }

        private void SetHead(Spire.Xls.Worksheet sheet, string cr, string text)
        {
            var range = sheet.Range[cr];
            range.Text = text;
            range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
            range.Style.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;
            range.Style.Font.FontName = "Courier New";
            range.Style.Font.Size = 14;
            range.Style.Font.IsBold = true;
        }

        private void SetTitle(Spire.Xls.Worksheet sheet, string cr, int wsRow, string text)
        {
            var range = sheet.Range[cr];
            range.Merge();

            range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
            range.Style.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;
            range.Style.Font.FontName = "標楷體";
            range.Style.Font.IsBold = false;

            range.Text = text;
            range.Style.Font.Size = 16;
        }

        private void Page(TConfig cfg, Item itmReturn)
        {
            var rows = new List<TRow>();
            AppendApiUrlInfo(cfg, rows, "<br>");
            AppendAdminCheckin(cfg, rows, "<br>");
            AppendAdminCentral(cfg, rows, "<br>");
            AppendRtmTree(cfg, rows, "<br>");

            foreach (var row in rows)
            {
                Item item = cfg.inn.newItem();
                item.setType("inn_url");
                item.setProperty("title", row.title);
                item.setProperty("url", row.url.ToLower());
                item.setProperty("func", row.func);
                itmReturn.addRelationship(item);
            }
        }

        //Api
        private void AppendApiUrlInfo(TConfig cfg, List<TRow> rows, string ln)
        {
            Item itmData = GetVariable(cfg, "api_url");
            if (itmData.isError() || itmData.getResult() == "") return;

            var row = new TRow
            {
                title = "Api 網址" + ln + "(計分程式)",
                url = itmData.getProperty("in_value", "").TrimEnd('/'),
            };

            row.link = row.url;
            row.func = ButtonCtrlLink("local_sync_api.png");
            row.note = "計分筆電";
            
            rows.Add(row);
        }

        //比賽進度表
        private void AppendAdminCheckin(TConfig cfg, List<TRow> rows, string ln)
        {
            Item itmData = GetMtShortUrl(cfg, "admin-checkin");
            if (itmData.isError() || itmData.getResult() == "") return;

            var row = new TRow
            {
                title = "比賽進度表" + ln + "(檢錄)",
                url = itmData.getProperty("in_api_shorturl", "").TrimEnd('/'),
            };

            row.link = "<a target='_blank' href='" + row.url + "'>" + row.url + "</a>";
            row.func = ButtonCtrlQrCode(row);
            row.note = "檢錄平板";
            rows.Add(row);
        }

        //戰情中心(競賽組)
        private void AppendAdminCentral(TConfig cfg, List<TRow> rows, string ln)
        {
            Item itmData = GetMtShortUrl(cfg, "admin-central");
            if (itmData.isError() || itmData.getResult() == "") return;

            var row = new TRow
            {
                title = "戰情中心" + ln + "(競賽組)",
                url = itmData.getProperty("in_api_shorturl", "").TrimEnd('/'),
                func = "",
            };

            row.link = "<a target='_blank' href='" + row.url + "'>" + row.url + "</a>";
            row.func = ButtonCtrlQrCode(row);
            row.note = "檢錄平板" + ln + "計分台筆電" + ln + "(非計分筆電)";
            rows.Add(row);
        }

        //對戰表(籤表)
        private void AppendRtmTree(TConfig cfg, List<TRow> rows, string ln)
        {
            Item itmData = GetMtShortUrl(cfg, "rtm-tree");
            if (itmData.isError() || itmData.getResult() == "") return;

            var row = new TRow
            {
                title = "對戰表" + ln + "(籤表)",
                url = itmData.getProperty("in_api_shorturl", "").TrimEnd('/'),
            };

            row.link = "<a target='_blank' href='" + row.url + "'>" + row.url + "</a>";
            row.func = ButtonCtrlQrCode(row);
            row.note = "檢錄平板" + ln + "計分台筆電" + ln + "(非計分筆電)";

            rows.Add(row);
        }

        private Item GetMtShortUrl(TConfig cfg, string in_value)
        {
            return cfg.inn.applySQL("SELECT TOP 1 * FROM IN_MEETING_SHORTURL WITH(NOLOCK) WHERE in_meeting = '" + cfg.meeting_id + "' AND in_value = '" + in_value + "'");
        }

        private Item GetVariable(TConfig cfg, string in_name)
        {
            return cfg.inn.applySQL("SELECT TOP 1 in_value FROM IN_VARIABLE WITH(NOLOCK) WHERE in_name = '" + in_name + "'");
        }

        private string ButtonCtrlLink(string gimg)
        {
            return "<button class='btn btn-sm btn-success' style='margin-left: 10px;'"
                + " data-gimg='" + gimg + "'"
                + " onclick='ShowGuideline(this)'> "
                + " <i class='fa fa-external-link key-info-btn'></i>設定導引"
                + "</button>";
        }

        private string ButtonCtrlQrCode(TRow row)
        {
            return "<button class='btn btn-sm btn-success' style='margin-left: 10px;'"
                + " data-title='" + row.title.Replace("<br>", " ") + "'"
                + " data-url='" + row.url + "'"
                + " onclick='ShowQrCode(this)'> "
                + " <i class='fa fa-qrcode key-info-btn'></i>"
                + "</button>";
        }

        private enum TCss
        {
            None = 0,
            Center = 10,
            Left = 20,
            Text = 100,
            Number = 200,
            Money = 300,
            Day = 400,
        }

        private class TRow
        {
            public string title { get; set; }
            public string url { get; set; }
            public string func { get; set; }
            public string link { get; set; }
            public string note { get; set; }
        }


        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }

            public string meeting_id { get; set; }
            public string scene { get; set; }
        }
    }
}