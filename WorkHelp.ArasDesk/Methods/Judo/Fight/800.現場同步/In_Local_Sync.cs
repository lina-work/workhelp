﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Judo.Fight
{
    public class In_Local_Sync : Item
    {
        public In_Local_Sync(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 現場系統整合(成績同步)
                輸入: meeting_id
                日期: 
                    2021-09-28: 創建 (lina)
             */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]In_Local_Sync";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "input dom: " + itmR.dom.InnerXml);
            CCO.Utilities.WriteDebug(strMethodName, "in");

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,

                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
            };


            //新同步資料
            Item itmSchedules = GetLocalSchedules(cfg);
            RefreshScore(cfg, itmSchedules);

            //成績修正
            Item itmChanges = GetChangeScores(cfg);
            RefreshScore(cfg, itmChanges);

            return itmR;
        }

        private void RefreshScore(TConfig cfg, Item itmSchedules)
        {
            if (itmSchedules.isError() || itmSchedules.getResult() == "" || itmSchedules.getItemCount() <= 0)
            {
                return;
            }

            int count = itmSchedules.getItemCount();

            for (int i = 0; i < count; i++)
            {

                var itmSchedule = itmSchedules.getItemByIndex(i);
                var row = MapRow(cfg, itmSchedule);

                //CCO.Utilities.WriteDebug(strMethodName, "step: 1");

                if (row.MId == "")
                {
                    //更新現場 Schedule
                    UpdateSchedule(cfg, row, "2");
                    continue;
                }

                var itmDetails = GetEventDetail(cfg, row);
                if (itmDetails.isError() || itmDetails.getResult() == "" || itmDetails.getItemCount() <= 0)
                {
                    UpdateSchedule(cfg, row, "3");
                    continue;
                }

                var evt = MapEvent(cfg, itmDetails);
                var is_f1_win = row.Win == "W";
                var is_f2_win = row.Win == "B";

                //代表戰
                //  白方單位與選手
                evt.Foot1.setProperty("in_player_org", row.WhiteD);
                evt.Foot1.setProperty("in_player_name", row.WhiteN);
                //  藍方單位與選手
                evt.Foot2.setProperty("in_player_org", row.BlueD);
                evt.Foot2.setProperty("in_player_name", row.BlueN);

                if (is_f1_win)
                {
                    //籤腳 1 勝出
                    SetScore(evt.Foot1, "勝出", row, row.WhiteI, row.WhiteW, row.WhiteS, row.BlueS);
                    SetScore(evt.Foot2, "", row, row.BlueI, row.BlueW, row.BlueS, row.WhiteS);
                }

                if (is_f2_win)
                {
                    //籤腳 2 勝出
                    evt.Foot2.setProperty("in_sign_action", "勝出");
                    SetScore(evt.Foot2, "勝出", row, row.BlueI, row.BlueW, row.BlueS, row.WhiteS);
                    SetScore(evt.Foot1, "", row, row.WhiteI, row.WhiteW, row.WhiteS, row.BlueS);
                }

                //更新明細: 籤腳1
                UpdateDetail(cfg, row, evt.Foot1);
                //更新明細: 籤腳2
                UpdateDetail(cfg, row, evt.Foot2);
                //更新場次
                UpdateEvent(cfg, row, evt);

                //遞迴勝出
                if (is_f1_win)
                {
                    ApplySync(cfg, row, evt.Value, evt.Foot1, evt.Foot2);

                    //更新現場 Schedule
                    UpdateSchedule(cfg, row, "1");
                }

                if (is_f2_win)
                {
                    ApplySync(cfg, row, evt.Value, evt.Foot2, evt.Foot1);

                    //更新現場 Schedule
                    UpdateSchedule(cfg, row, "1");
                }
            }
        }

        /// <summary>
        /// 更新現場 Schedule
        /// </summary>
        private void UpdateSchedule(TConfig cfg, TRow row, string sync_flag)
        {
            var keys = new List<string>();
            keys.Add(row.item.getProperty("whitei", ""));
            keys.Add(row.item.getProperty("whitew", ""));
            keys.Add(row.item.getProperty("whites", ""));
            keys.Add(row.item.getProperty("bluei", ""));
            keys.Add(row.item.getProperty("bluew", ""));
            keys.Add(row.item.getProperty("blues", ""));
            keys.Add(row.item.getProperty("win", ""));

            string upd_result = string.Join("-", keys);

            string sql = "UPDATE In_Local_Schedule SET"
                + "  SyncFlag = '" + sync_flag + "'"
                + ", UpdResult = '" + upd_result + "'"
                + " WHERE enumber = '" + row.ENumber + "'";

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            cfg.inn.applySQL(sql);
        }

        private void ApplySync(TConfig cfg, TRow row, Item itmEvent, Item itmWinner, Item itmLoser)
        {
            Item itmData = cfg.inn.newItem();
            itmData.setType("In_Meeting_PEvent");
            itmData.setProperty("meeting_id", row.MId);
            itmData.setProperty("program_id", itmEvent.getProperty("program_id", ""));
            itmData.setProperty("event_id", itmEvent.getProperty("event_id", ""));
            itmData.setProperty("detail_id", itmWinner.getProperty("detail_id", ""));
            itmData.setProperty("target_detail_id", itmLoser.getProperty("detail_id", ""));
            itmData.setProperty("mode", "sync");

            itmData.apply("in_meeting_program_score");
        }

        private void UpdateEvent(TConfig cfg, TRow row, TEvent evt)
        {
            string sql = "";

            if (row.WhiteS == "3" || row.BlueS == "3")
            {
                sql = "UPDATE IN_MEETING_PEVENT SET"
                    + "  in_win_local = '" + row.Win + "'"
                    + ", in_win_local_time = '" + row.Tme + "'"
                    + ", in_note = '31'"
                    + " WHERE id = '" + evt.Id + "'";
            }
            else
            {
                sql = "UPDATE IN_MEETING_PEVENT SET"
                    + "  in_win_local = '" + row.Win + "'"
                    + ", in_win_local_time = '" + row.Tme + "'"
                    + " WHERE id = '" + evt.Id + "'";
            }

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item itmSQL = cfg.inn.applySQL(sql);

            if (itmSQL.isError())
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "更新 IN_MEETING_PEVENT 失敗: " + sql);
            }
        }

        private void UpdateDetail(TConfig cfg, TRow row, Item item)
        {
            string sql = "";

            if (row.EWeight.Contains("代表戰"))
            {
                sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET"
                    + " in_sign_action = N'" + item.getProperty("in_sign_action", "") + "'"
                    + " , in_points = N'" + item.getProperty("in_points", "") + "'"
                    + " , in_points_text = N'" + item.getProperty("in_points_text", "") + "'"
                    + " , in_correct_count = N'" + item.getProperty("in_correct_count", "") + "'"
                    + " , in_player_org = N'" + item.getProperty("in_player_org", "") + "'"
                    + " , in_player_name = N'" + item.getProperty("in_player_name", "") + "'"
                    + " WHERE id = '" + item.getProperty("detail_id", "") + "'";

            }
            else
            {
                sql = "UPDATE IN_MEETING_PEVENT_DETAIL SET"
                    + " in_sign_action = N'" + item.getProperty("in_sign_action", "") + "'"
                    + " , in_points = N'" + item.getProperty("in_points", "") + "'"
                    + " , in_points_text = N'" + item.getProperty("in_points_text", "") + "'"
                    + " , in_correct_count = N'" + item.getProperty("in_correct_count", "") + "'"
                    + " WHERE id = '" + item.getProperty("detail_id", "") + "'";
            }

            Item itmSQL = cfg.inn.applySQL(sql);

            if (itmSQL.isError())
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "更新 IN_MEETING_PEVENT_DETAIL 失敗: " + sql);
            }
        }

        private void SetScore(Item item, string action, TRow row, string ippon, string waza_ari, string shido, string tgt_shido)
        {
            item.setProperty("in_sign_action", action);

            if (ippon == "1")
            {
                if (waza_ari == "1")
                {
                    item.setProperty("in_points", "11");
                    item.setProperty("in_points_text", "半勝+全勝");
                    item.setProperty("in_note", "12");
                }
                else
                {
                    item.setProperty("in_points", "10");
                    item.setProperty("in_points_text", "全勝");
                    item.setProperty("in_note", "11");
                }
            }
            else if (waza_ari == "1")
            {
                item.setProperty("in_points", "1");
            }

            //指導次數
            item.setProperty("in_correct_count", shido);

            if (shido == "3")
            {
                item.setProperty("in_points_text", "因指導落敗");
                item.setProperty("in_note", "31");
            }
            else if (tgt_shido == "3")
            {
                item.setProperty("in_points_text", "因指導勝出");
            }

            //主場次 (後面不會有括號)
            if (row.EWeight.EndsWith("團體賽"))
            {
                item.setProperty("in_points", ippon);
            }
        }

        private TRow MapRow(TConfig cfg, Item itmSchedule)
        {
            TRow result = new TRow
            {
                No = itmSchedule.getProperty("no", ""),
                Site = itmSchedule.getProperty("site", ""),
                ERound = itmSchedule.getProperty("eround", ""),
                EWeight = itmSchedule.getProperty("eweight", ""),
                WhiteD = itmSchedule.getProperty("whited", ""),
                WhiteN = itmSchedule.getProperty("whiten", ""),
                BlueD = itmSchedule.getProperty("blued", ""),
                BlueN = itmSchedule.getProperty("bluen", ""),
                WhiteI = itmSchedule.getProperty("whitei", ""),
                BlueI = itmSchedule.getProperty("bluei", ""),
                WhiteW = itmSchedule.getProperty("whitew", ""),
                BlueW = itmSchedule.getProperty("bluew", ""),
                WhiteS = itmSchedule.getProperty("whites", ""),
                BlueS = itmSchedule.getProperty("blues", ""),
                Win = itmSchedule.getProperty("win", ""),
                Tme = itmSchedule.getProperty("tme", ""),
                MNumber = itmSchedule.getProperty("mnumber", ""),
                MDay = itmSchedule.getProperty("mday", ""),
                ENumber = itmSchedule.getProperty("enumber", ""),
                MId = itmSchedule.getProperty("mid", ""),
                item = itmSchedule,
            };

            result.InSiteCode = GetSiteCodeNum(result.Site);

            return result;
        }

        private TEvent MapEvent(TConfig cfg, Item items)
        {
            var itmEvent = items.getItemByIndex(0);

            var evt = new TEvent
            {
                Id = itmEvent.getProperty("event_id", ""),
                Value = itmEvent,
            };

            var count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_sign_foot = item.getProperty("in_sign_foot", "");
                if (in_sign_foot == "1")
                {
                    evt.Foot1 = item;
                }
                else
                {
                    evt.Foot2 = item;
                }
            }

            if (evt.Foot1 == null) evt.Foot1 = cfg.inn.newItem();
            if (evt.Foot2 == null) evt.Foot2 = cfg.inn.newItem();

            return evt;
        }

        private Item GetLocalSchedules(TConfig cfg)
        {
            string sql = @"
                SELECT 
	                t1.*
	                , t2.id AS 'MId'
                FROM 
	                In_Local_Schedule t1 WITH(NOLOCK) 
                INNER JOIN 
	                IN_MEETING t2 WITH(NOLOCK) 
	                ON t2.item_number = t1.MNumber 
	            LEFT OUTER JOIN
	                IN_MEETING_PEVENT t3 WITH(NOLOCK)
	                ON t3.item_number = t1.enumber
                WHERE
                    t1.win <> ISNULL(t3.in_win_local, '')
            ";

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }


        private Item GetChangeScores(TConfig cfg)
        {
            string sql = @"
                SELECT 
	                t1.*
                FROM 
	                In_Local_Schedule t1 WITH(NOLOCK) 
				WHERE
                    ISNULL(SyncFlag, '') = 1
					AND (
                        ISNULL(t1.WhiteI, '') 
                        + '-' + ISNULL(t1.WhiteW, '') 
                        + '-' + ISNULL(t1.WhiteS, '') 
                        + '-' + ISNULL(t1.BlueI, '') 
                        + '-' + ISNULL(t1.BlueW, '') 
                        + '-' + ISNULL(t1.BlueS, '') 
                        + '-' + ISNULL(t1.Win, '')
                        <> 
                        ISNULL(UpdResult, '')
                    )
            ";

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        private Item GetEventDetail(TConfig cfg, TRow row)
        {
            string sql = @"
                SELECT 
	                t1.source_id    AS 'program_id'
	                , t1.id         AS 'event_id'
	                , t2.id         AS 'detail_id'
	                , t2.in_sign_foot
	                , t2.in_sign_no
                FROM 
	                IN_MEETING_PEVENT t1 WITH(NOLOCK) 
                INNER JOIN 
	                IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                WHERE
	                t1.item_number = '{#item_number}'
                ORDER BY
	                t2.in_sign_foot
            ";

            sql = sql.Replace("{#item_number}", row.ENumber);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        private string GetSiteCodeNum(string value)
        {
            switch (value)
            {
                case "A": return "1";
                case "B": return "2";
                case "C": return "3";
                case "D": return "4";
                case "E": return "5";
                case "F": return "6";
                case "G": return "7";
                case "H": return "8";
                case "I": return "9";
                case "J": return "10";
                case "K": return "11";
                case "L": return "12";
                case "M": return "13";
                case "N": return "14";
                case "O": return "15";
                case "P": return "16";
                case "Q": return "17";
                case "R": return "18";
                case "S": return "19";
                case "T": return "20";
                case "U": return "21";
                case "V": return "22";
                case "W": return "23";
                case "X": return "24";
                case "Y": return "25";
                case "Z": return "26";
                default: return "";
            }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }

            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
        }

        private class TEvent
        {
            /// <summary>
            /// 編號
            /// </summary>
            public string Id { get; set; }

            /// <summary>
            /// 場次資料
            /// </summary>
            public Item Value { get; set; }

            /// <summary>
            /// 白方
            /// </summary>
            public Item Foot1 { get; set; }

            /// <summary>
            /// 藍方
            /// </summary>
            public Item Foot2 { get; set; }
        }

        private class TRow
        {
            /// <summary>
            /// 編號
            /// </summary>
            public string No { get; set; }

            /// <summary>
            /// 場地
            /// </summary>
            public string Site { get; set; }

            /// <summary>
            /// 場次
            /// </summary>
            public string ERound { get; set; }

            /// <summary>
            /// 量級
            /// </summary>
            public string EWeight { get; set; }

            /// <summary>
            /// 白單位
            /// </summary>
            public string WhiteD { get; set; }

            /// <summary>
            /// 白姓名
            /// </summary>
            public string WhiteN { get; set; }

            /// <summary>
            /// 藍單位
            /// </summary>
            public string BlueD { get; set; }

            /// <summary>
            /// 藍姓名
            /// </summary>
            public string BlueN { get; set; }

            /// <summary>
            /// 白Ippon(一勝)
            /// </summary>
            public string WhiteI { get; set; }

            /// <summary>
            /// 藍Ippon(一勝)
            /// </summary>
            public string BlueI { get; set; }

            /// <summary>
            /// 白Waza-ari(半勝)
            /// </summary>
            public string WhiteW { get; set; }

            /// <summary>
            /// 藍Waza-ari(半勝)
            /// </summary>
            public string BlueW { get; set; }

            /// <summary>
            /// 白Shido(指導)
            /// </summary>
            public string WhiteS { get; set; }

            /// <summary>
            /// 藍Shido(指導)
            /// </summary>
            public string BlueS { get; set; }

            /// <summary>
            /// 勝負
            /// </summary>
            public string Win { get; set; }

            /// <summary>
            /// 時間
            /// </summary>
            public string Tme { get; set; }

            /// <summary>
            /// 賽事編號
            /// </summary>
            public string MNumber { get; set; }

            /// <summary>
            /// 賽事日期
            /// </summary>
            public string MDay { get; set; }

            /// <summary>
            /// 場次編號
            /// </summary>
            public string ENumber { get; set; }

            /// <summary>
            /// 賽事 Id
            /// </summary>
            public string MId { get; set; }

            /// <summary>
            /// 場地代碼
            /// </summary>
            public string InSiteCode { get; set; }

            public Item item { get; set; }
        }
    }
}