﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.PLMCTA.Common
{
    public class In_Local_Schedule_Clear : Item
    {
        public In_Local_Schedule_Clear(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            Innovator inn = this.getInnovator();

            Item itmR = this;

            string sql = "SELECT DISTINCT MNumber, MDay FROM In_Local_Schedule WITH(NOLOCK)";
            Item items = inn.applySQL(sql);
            if (items.isError())
            {
                return itmR;
            }

            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string mnumber = item.getProperty("mnumber", "");
                string mday = item.getProperty("mday", "");

                string code = mnumber.Substring(mnumber.Length - 3 - 1, 3);
                string day = mday.Replace("-", "");

                string table = "In_Local_Schedule_" + code + "_" + day;


                sql = "SELECT TOP 1 * FROM " + table + " WITH(NOLOCK)";
                Item itmOlds = inn.applySQL(sql);
                if (itmOlds.isError()) continue;

                if (itmOlds.getResult() == "")
                {
                    sql = "SELECT * INTO " + table + " FROM In_Local_Schedule WITH(NOLOCK)"
                        + " WHERE mnumber = '" + mnumber + "' "
                        + " AND mday = '" + mday + "' ";
                    Item itmClone = inn.applySQL(sql);
                }
            }

            sql = "TRUNCATE TABLE In_Local_Schedule";
            Item itmSQL = inn.applySQL(sql);

            return itmR;
        }

    }
}
