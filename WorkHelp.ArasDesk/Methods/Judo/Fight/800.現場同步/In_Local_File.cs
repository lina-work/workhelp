﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Judo.Fight
{
    public class In_Local_File : Item
    {
        public In_Local_File(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
    目的: 下載活動相關檔案
    輸入: meeting_id
    日期: 
        2022-11-10: 創建 (lina)
 */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]In_Local_System";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "input dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                strIdentityId = inn.getUserAliases(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                scene = itmR.getProperty("scene", ""),
            };

            cfg.aras_folder = @"C:\Aras\Vault\" + strDatabaseName;
            cfg.cloud_url = "https://act.innosoft.com.tw/judo/DownloadMeetingFile.aspx?fileid=";
            cfg.local_url = "../DownloadMeetingFile.aspx?fileid=";

            Item itmPlm = cfg.inn.applySQL("SELECT in_value FROM IN_VARIABLE WITH(NOLOCK) WHERE in_name = 'site_path'");
            cfg.iplm_folder = itmPlm.getProperty("in_value", "").Trim('\\')
                + "\\tempvault";

            switch (cfg.scene)
            {
                case "download":
                    Download(cfg, itmR);
                    break;

                case "page":
                    Page(cfg, itmR);
                    break;

                default:
                    break;
            }

            return itmR;
        }

        private void Download(TConfig cfg, Item itmReturn)
        {
            var id = itmReturn.getProperty("fileid", "");
            var nm = itmReturn.getProperty("filename", "");

            if (id == "")
            {
                throw new Exception("id 不可為空值");
            }

            var row = new TRow { id = id };
            var item = cfg.inn.applySQL("SELECT id, mimetype, filename, file_size FROM [FILE] WITH(NOLOCK) WHERE id = '" + id + "'");
            if (item.isError() || item.getResult() == "")
            {
                throw new Exception("查無 File");
            }

            MatchRowFile(cfg, row, item, true);
            if (row.is_error)
            {
                throw new Exception(row.message);
            }

            if (row.is_exists)
            {
                return;
            }

            try
            {
                var wc = new System.Net.WebClient();
                wc.DownloadFile(row.cloud_url, row.iplm_full);

                if (!System.IO.File.Exists(row.iplm_full))
                {
                    throw new Exception("下載失敗");
                }
            }
            catch (Exception ex)
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, ex.Message);
            }
        }

        private void Page(TConfig cfg, Item itmReturn)
        {
            var rows = new List<TRow>();
            AppendSiteFile(cfg, rows);

            var map = GetFileMap(cfg, rows);
            foreach (var row in rows)
            {
                if (!map.ContainsKey(row.id))
                {
                    row.is_error = true;
                    row.message = "查無 File";
                    continue;
                }

                MatchRowFile(cfg, row, map[row.id], false);
            }

            itmReturn.setProperty("inn_table", TableContents(cfg, rows));
        }

        private string TableContents(TConfig cfg, List<TRow> rows)
        {
            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            head.AppendLine("<th class='text-center'>檔案名稱</th>");
            head.AppendLine("<th class='text-center'>說明</th>");
            head.AppendLine("<th class='text-center'>狀態</th>");
            head.AppendLine("<th class='text-center'>線上圖片</th>");
            head.AppendLine("<th class='text-center'>本機圖片</th>");
            head.AppendLine("<th class='text-center'>功能</th>");

            for (int i = 0; i < rows.Count; i++)
            {
                var row = rows[i];
                body.AppendLine("<tr>");
                body.AppendLine("  <td class='text-left'>" + row.name + "</td>");
                body.AppendLine("  <td class='text-left'>" + row.desc + "</td>");
                body.AppendLine("  <td class='text-left'>" + row.message + "</td>");
                body.AppendLine("  <td class='text-left'>" + GetFileIcon(cfg, row.cloud_url, true) + "</td>");
                body.AppendLine("  <td class='text-left'>" + GetFileIcon(cfg, row.local_url, row.is_exists) + "</td>");
                body.AppendLine("  <td class='text-center'>" + GetButton(cfg, row) + "</td>");
                body.AppendLine("</tr>");
            }

            string table_name = "data_table";

            StringBuilder builder = new StringBuilder();
            builder.AppendLine(GetTableAttribute(table_name));

            builder.AppendLine("<thead>");
            builder.Append(head);
            builder.AppendLine("</thead>");

            builder.AppendLine("<tbody>");
            builder.Append(body);
            builder.AppendLine("</tbody>");

            builder.AppendLine("</table>");
            builder.AppendLine("<script>");
            builder.AppendLine("$('#" + table_name + "').bootstrapTable();");
            builder.AppendLine("</script>");
            return builder.ToString();
        }

        private string GetButton(TConfig cfg, TRow row)
        {
            return "<button class='btn btn-danger inn_download_btn' style='color: white'"
                + " onclick='DownloadFile(this)'"
                + " data-id='" + row.id + "'"
                + " data-nm='" + row.name + "'"
                + "> 下載 </button>";
        }

        private string GetFileIcon(TConfig cfg, string url, bool is_exists)
        {
            var img = is_exists ? "paperClip2_finished.png" : "paperClip2.png";

            return "<img src='../images/" + img + "' style='width:30px; height:30px'"
                + " onclick='openPhotoSwipe5(this)'"
                + " data-fsrc='" + url + "'"
                + " title='" + url + "' />";
        }

        private string GetTableAttribute(string table_name)
        {
            return "<table id='" + table_name + "' class='table table-hover table-bordered table-rwd rwd rwdtable' "
                + " data-toggle='table' "
                + " data-click-to-select='true' "
                + " data-striped='false' "
                + " data-search='false' "
                // + " data-pagination='true' "
                // + " data-show-pagination-switch='false'"
                + ">";
        }
        private Dictionary<string, Item> GetFileMap(TConfig cfg, List<TRow> rows)
        {
            var result = new Dictionary<string, Item>();
            var ids = string.Join(", ", rows.Select(x => "'" + x.id + "'"));
            var sql = "SELECT id, mimetype, filename, file_size FROM [FILE] WITH(NOLOCK) WHERE id IN (" + ids + ")";
            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var id = item.getProperty("id", "");
                result.Add(id, item);
            }
            return result;
        }

        private void AppendSiteFile(TConfig cfg, List<TRow> rows)
        {
            string sql = @"SELECT TOP 1 * FROM In_Site WITH(NOLOCK)";
            Item item = cfg.inn.applySQL(sql);
            if (item.isError() || item.getResult() == "")
            {
                return;
            }

            item.setType("In_Site");
            AddRow(cfg, rows, item, "in_overview_banner_pc", "Overview頁 桌機版 Banner");
            AddRow(cfg, rows, item, "in_overview_banner_mb", "Overview頁 手機版 Banner");
            AddRow(cfg, rows, item, "in_overview_banner_2k", "Overview頁 2000w Banner");
        }

        private void AddRow(TConfig cfg, List<TRow> rows, Item item, string prop, string desc)
        {
            var fileid = item.getProperty(prop, "");
            if (fileid == "") return;

            var row = new TRow
            {
                id = fileid,
                prop = prop,
                desc = desc,
                itemtype = item.getType(),
            };

            rows.Add(row);
        }

        private void MatchRowFile(TConfig cfg, TRow row, Item item, bool need_create_dir = false)
        {
            row.is_exists = false;
            row.is_error = false;
            row.size = string.Empty;
            row.name = string.Empty;
            row.message = string.Empty;

            row.item = item;
            row.name = item.getProperty("filename", "");
            row.size = item.getProperty("file_size", "0");
            row.cloud_url = cfg.cloud_url + row.id;
            row.local_url = cfg.local_url + row.id;

            string f1 = row.id.Substring(0, 1);
            string f2 = row.id.Substring(1, 2);
            string f3 = row.id.Substring(3, 29);

            if (row.name == "" || row.size == "0")
            {
                row.is_error = true;
                row.message = "檔案異常";
                row.local_url = string.Empty;
                return;
            }

            row.aras_path = cfg.aras_folder
                + @"\" + f1
                + @"\" + f2
                + @"\" + f3;

            row.aras_full = row.aras_path
                + @"\" + row.name;

            row.iplm_path = cfg.iplm_folder
                + @"\" + row.id;

            row.iplm_full = row.iplm_path
                + @"\" + row.name;

            if (System.IO.File.Exists(row.iplm_full))
            {
                //檔案已存在
                row.is_exists = true;
                row.message = "檔案已存在";
                return;
            }

            row.is_exists = false;
            row.message = "本機無檔案";
            row.local_url = string.Empty;

            if (need_create_dir)
            {
                if (!System.IO.Directory.Exists(row.iplm_path))
                {
                    System.IO.Directory.CreateDirectory(row.iplm_path);
                }
            }
        }

        private class TRow
        {
            public string id { get; set; }
            public string prop { get; set; }
            public string desc { get; set; }
            public string itemtype { get; set; }

            public Item item { get; set; }
            public string name { get; set; }
            public string size { get; set; }
            public string message { get; set; }

            public string aras_path { get; set; }
            public string aras_full { get; set; }

            public string iplm_path { get; set; }
            public string iplm_full { get; set; }

            public bool is_error { get; set; }
            public bool is_exists { get; set; }

            public string cloud_url { get; set; }
            public string local_url { get; set; }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }

            public string meeting_id { get; set; }
            public string scene { get; set; }

            public string aras_folder { get; set; }
            public string iplm_folder { get; set; }
            public string cloud_url { get; set; }
            public string local_url { get; set; }
        }
    }
}