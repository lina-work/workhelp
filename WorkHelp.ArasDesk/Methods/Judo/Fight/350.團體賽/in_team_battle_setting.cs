﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Judo.Fight
{
    public class in_team_battle_setting : Item
    {
        public in_team_battle_setting(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
    目的: 團體戰-量級設定
    日期: 
        - 2022-08-11 改版 (Lina)
        - 2021-12-17 創建 (Lina)
*/

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]in_team_battle_setting";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, this.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                meeting_id = itmR.getProperty("meeting_id", ""),
                program_id = itmR.getProperty("program_id", ""),
                scene = itmR.getProperty("scene", ""),
                in_property = itmR.getProperty("in_property", ""),
                ages = itmR.getProperty("ages", ""),
            };

            switch (cfg.scene)
            {
                case "modal":
                    Modal(cfg, itmR);
                    break;

                case "modal_save":
                    ModalSave(cfg, itmR);
                    break;

                case "update":
                    Update(cfg, itmR);
                    break;

                case "create":
                    Create(cfg, itmR);
                    break;

                case "rebuild":
                    Rebuild(cfg, itmR);
                    break;

                case "draw_page":
                    DrawPage(cfg, itmR);
                    break;

                case "draw_save":
                    DrawSave(cfg, itmR);
                    break;

                case "draw_random":
                    DrawRandom(cfg, itmR);
                    break;

                case "draw_report":
                    DrawReport(cfg, itmR);
                    break;

                case "clear_start":
                    ClearStart(cfg, itmR);
                    break;

                case "weight_check":
                    TeamWeightCheck(cfg, itmR);
                    break;

                default:
                    Page(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void TeamWeightCheck(TConfig cfg, Item itmReturn)
        {
            var in_team_weight_check = itmReturn.getProperty("in_team_weight_check", "");
            var sql = "";

            sql = @" 
                UPDATE IN_MEETING_PROGRAM SET 
                    in_team_weight_check = '{#in_team_weight_check}' 
                WHERE 
                    id = '{#program_id}'";

            sql = sql.Replace("{#program_id}", cfg.program_id)
                .Replace("{#in_team_weight_check}", in_team_weight_check);

            cfg.inn.applySQL(sql);
        }

        private void ClearStart(TConfig cfg, Item itmReturn)
        {
            string sql = "";

            //清除組別上的起始量級
            sql = @" 
                UPDATE IN_MEETING_PROGRAM SET 
                    in_sect_start = NULL 
                WHERE 
                    in_meeting = '{#meeting_id}'";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            cfg.inn.applySQL(sql);

            //復歸子場次的場次編號
            //以及清除子場次的動態輪序
            sql = @"
                UPDATE t1 SET
	                t1.in_tree_no = t2.in_tree_no * 100 + t1.in_sub_id
	                , t1.in_period = t2.in_period
	                , t1.in_medal = t2.in_medal
	                , t1.in_team_serial = t1.in_sub_id
                FROM 
	                IN_MEETING_PEVENT t1 WITH(NOLOCK) 
                INNER JOIN
	                IN_MEETING_PEVENT t2 WITH(NOLOCK) 
	                ON t2.id = t1.in_parent
                WHERE
	                t1.in_meeting = '{#meeting_id}'
	                AND t1.in_tree_name = 'sub'
	                AND ISNULL(t2.in_tree_no, 0) > 0
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            cfg.inn.applySQL(sql);

            sql = @"
				UPDATE t12 SET
	                t12.in_player_org = ISNULL(t3.in_short_org, '')
	                , t12.in_player_team = ISNULL(t3.in_team, '')
	                , t12.in_player_name = N'第' + CONVERT(VARCHAR, t11.in_team_serial) + N'位'
	                , t12.in_parent = t2.id
                FROM 
	                IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                LEFT OUTER JOIN
	                IN_MEETING_PTEAM t3 WITH(NOLOCK)
	                ON t3.source_id = t1.source_id
	                AND t3.in_sign_no = t2.in_sign_no
				INNER JOIN
					IN_MEETING_PEVENT t11 WITH(NOLOCK) 
					ON t11.in_parent = t1.id
				INNER JOIN
					IN_MEETING_PEVENT_DETAIL t12 WITH(NOLOCK) 
					ON t12.source_id = t11.id
					AND t12.in_sign_foot = t2.in_sign_foot
                WHERE 
	                t1.in_meeting = '{#meeting_id}'
	                AND ISNULL(t1.in_type, '') IN ('', 'p')
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            cfg.inn.applySQL(sql);
        }

        #region 輪次報表

        private void DrawReport(TConfig cfg, Item itmReturn)
        {
            var in_sub_event = itmReturn.getProperty("in_sub_event", "0");

            var rows = MapPgSect2(cfg);
            var row = rows.Find(x => x.count == in_sub_event);
            if (row == null || row.kvs == null || row.kvs.Count == 0) throw new Exception("查無該團體賽設定");

            cfg.CharSet = GetCharSet();

            Item itmMt = cfg.inn.applySQL("SELECT id, in_title FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            string title = itmMt.getProperty("in_title", "");


            Spire.Xls.Workbook book = new Spire.Xls.Workbook();
            Spire.Xls.Worksheet sheet = book.CreateEmptySheet();

            var sect_cnt = row.kvs.Count;
            var sect_min = GetIntVal(row.draw) - 1;


            //第一列
            var ttl_ps_s = "A1";
            var ttl_ps_e = cfg.CharSet[sect_cnt + 1] + "1";
            var rng_title = sheet.Range[ttl_ps_s + ":" + ttl_ps_e];
            rng_title.Merge();
            rng_title.Text = title;
            rng_title.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
            rng_title.Style.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;
            rng_title.Style.Font.Size = 24;
            rng_title.RowHeight = 30;

            var dfCol = 1;
            var wsRow = 2;
            var wsCol = dfCol;

            //第二列
            SetBodyCell(cfg, sheet, ref wsCol, wsRow, row.name);
            SetBodyCell(cfg, sheet, ref wsCol, wsRow, "起始量級");
            SetBodyCell(cfg, sheet, ref wsCol, wsRow, row.draw);
            sheet.Rows[wsRow - 1].RowHeight = 36;
            wsRow++;
            wsCol = dfCol;

            SetHeadCell(cfg, sheet, ref wsCol, wsRow, "輪次");
            for (int i = 1; i <= sect_cnt; i++)
            {
                SetHeadCell(cfg, sheet, ref wsCol, wsRow, "第" + i + "場");
            }
            sheet.Rows[wsRow - 1].RowHeight = 30;
            wsRow++;

            var idx_min = 0;
            var idx_max = 8;

            for (int i = idx_min; i < idx_max; i++)
            {
                wsCol = dfCol;
                SetBodyCell(cfg, sheet, ref wsCol, wsRow, "第" + (i + 1) + "輪");

                var sect_idx = sect_min + i;
                if (sect_idx >= sect_cnt) sect_idx = sect_idx - sect_cnt;

                for (int j = 1; j <= sect_cnt; j++)
                {
                    if (sect_idx >= sect_cnt) sect_idx = sect_idx - sect_cnt;
                    SetBodyCell(cfg, sheet, ref wsCol, wsRow, row.kvs[sect_idx].name);
                    sect_idx++;
                }
                sheet.Rows[wsRow - 1].RowHeight = 30;
                wsRow++;
            }

            sheet.Columns[0].ColumnWidth = 16;
            for (int i = 1; i <= sect_cnt; i++)
            {
                sheet.Columns[i].ColumnWidth = 16;
            }

            var pos_s = "A3";
            var pos_e = cfg.CharSet[sect_cnt + 1] + (wsRow - 1);
            var range = sheet.Range[pos_s + ":" + pos_e];
            range.BorderInside(Spire.Xls.LineStyleType.Thin, System.Drawing.Color.Black);
            range.BorderAround(Spire.Xls.LineStyleType.Thin, System.Drawing.Color.Black);

            sheet.PageSetup.FitToPagesWide = 1;
            sheet.PageSetup.FitToPagesTall = 1;
            sheet.PageSetup.TopMargin = 0.3;
            sheet.PageSetup.LeftMargin = 0.5;
            sheet.PageSetup.RightMargin = 0.5;
            sheet.PageSetup.BottomMargin = 0.5;

            ////移除空 sheet
            //workbook.Worksheets[0].Remove();
            //workbook.Worksheets[0].Remove();
            //workbook.Worksheets[0].Remove();

            Item itmXls = cfg.inn.applyMethod("In_Variable_File", "<in_name></in_name>");
            string export_path = itmXls.getProperty("export_path", "");
            string guid = System.Guid.NewGuid().ToString().Substring(0, 6).ToUpper()
                + "_" + DateTime.Now.ToString("yyyyMMddHHmmssfff");

            string ext_name = "." + "pdf";
            string xls_name = title + "_" + row.name + "_" + guid;
            string xls_file = export_path + "\\" + xls_name + ext_name;

            book.SaveToFile(xls_file, Spire.Xls.FileFormat.PDF);
            itmReturn.setProperty("xls_name", xls_name + ext_name);
        }

        private void SetHeadCell(TConfig cfg, Spire.Xls.Worksheet sheet, ref int cidx, int ridx, string value)
        {
            var pos = cfg.CharSet[cidx] + ridx;
            var range = sheet.Range[pos];
            range.Text = value;
            range.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
            range.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;
            range.Style.Font.IsBold = true;
            range.Style.Font.Size = 16;
            cidx++;
        }

        private void SetBodyCell(TConfig cfg, Spire.Xls.Worksheet sheet, ref int cidx, int ridx, string value)
        {
            var pos = cfg.CharSet[cidx] + ridx;
            var range = sheet.Range[pos];
            range.Text = value;
            range.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
            range.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;
            range.Style.Font.IsBold = false;
            range.Style.Font.Size = 14;
            cidx++;
        }

        private string[] GetCharSet()
        {
            return new string[] { "", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
        }
        #endregion 輪次報表

        private void DrawSave(TConfig cfg, Item itmReturn)
        {
            var ages = itmReturn.getProperty("ages", "");
            var in_sub_event = itmReturn.getProperty("in_sub_event", "0");
            var in_sub_id = itmReturn.getProperty("in_sub_id", "");
            if (in_sub_id == "" || in_sub_id == "0" || in_sub_id == "請選擇")
            {
                in_sub_id = "NULL";
            }

            //更新起始量級
            UpdStartSects(cfg, in_sub_event, in_sub_id, ages);
            //更新場次序號量級序號與子場次
            UpdEventSerial(cfg, in_sub_event, itmReturn);
        }

        private void DrawRandom(TConfig cfg, Item itmReturn)
        {
            var ages = itmReturn.getProperty("ages", "");
            var in_sub_event = itmReturn.getProperty("in_sub_event", "0");
            var num = GetIntVal(in_sub_event);

            if (num <= 0)
            {
                throw new Exception("參數錯誤");
            }

            var draw = InnSport.Core.Utilities.TRNG.Next(1, num);
            var in_sect_start = draw.ToString();

            //更新起始量級
            UpdStartSects(cfg, in_sub_event, in_sect_start, ages);
            //更新場次序號量級序號與子場次
            UpdEventSerial(cfg, in_sub_event, itmReturn);

            itmReturn.setProperty("inn_draw", in_sect_start);
        }

        private void UpdEventSerial(TConfig cfg, string in_sub_event, Item itmReturn)
        {
            Item itmData = cfg.inn.newItem("In_Meeting_Program");
            itmData.setProperty("meeting_id", cfg.meeting_id);
            itmData.setProperty("in_sub_event", in_sub_event);
            itmData.setProperty("ages", itmReturn.getProperty("ages", ""));
            itmData.setProperty("scene", "team_serial");
            itmData.setProperty("in_property", cfg.in_property);
            itmData.apply("in_team_battle_serial");
        }

        private void UpdStartSects(TConfig cfg, string in_sub_event, string in_sect_start, string ages)
        {
            if (ages == "")
            {
                UpdL2StartSect(cfg, in_sub_event, in_sect_start, "");
            }
            else
            {
                var l2s = ages.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                if (l2s == null || l2s.Length == 0)
                {
                    UpdL2StartSect(cfg, in_sub_event, in_sect_start, "");
                }
                else
                {
                    foreach (var l2 in l2s)
                    {
                        UpdL2StartSect(cfg, in_sub_event, in_sect_start, l2);
                    }
                }
            }
        }

        private void UpdL2StartSect(TConfig cfg, string in_sub_event, string in_sect_start, string group_name)
        {
            //112年全柔改到第三階
            string cond_lv = group_name == ""
                ? ""
                : "AND " + cfg.in_property + " LIKE N'%" + group_name + "%'";

            string sql = @"
                UPDATE IN_MEETING_PROGRAM SET 
                    in_sect_start = {#in_sect_start}
                WHERE 
                    in_meeting = '{#meeting_id}'
                    AND in_l1 = N'團體組'
                    AND ISNULL(in_sub_event, 0) = {#in_sub_event}
                    {#cond_lv}
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_sub_event}", in_sub_event)
                .Replace("{#in_sect_start}", in_sect_start)
                .Replace("{#cond_lv}", cond_lv);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            Item itmResult = cfg.inn.applySQL(sql);

            if (itmResult.isError())
            {
                throw new Exception("執行發生錯誤");
            }
        }

        private void DrawPage(TConfig cfg, Item itmReturn)
        {
            Item itmMeeting = cfg.inn.applySQL("SELECT in_title FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            itmReturn.setProperty("in_title", itmMeeting.getProperty("in_title", ""));

            var rows = MapPgSect1(cfg);
            foreach (var row in rows)
            {
                Item item = cfg.inn.newItem();
                item.setType("inn_row");
                item.setProperty("name", row.name);
                item.setProperty("count", row.count);
                item.setProperty("ctrl", WeightSelect(cfg, row));
                itmReturn.addRelationship(item);
            }
        }

        private string WeightSelect(TConfig cfg, TPgSect row)
        {
            var sb = new StringBuilder();
            sb.Append("<select class='form-control btn btn-default' >");
            sb.Append("<option value=''>請選擇</option>");
            foreach (var kv in row.kvs)
            {
                if (kv.id == row.draw)
                {
                    sb.Append("<option value='" + kv.id + "' selected>" + kv.name + "</option>");
                }
                else
                {
                    sb.Append("<option value='" + kv.id + "'>" + kv.name + "</option>");
                }
            }
            sb.Append("</select");
            return sb.ToString();
        }

        private List<TPgSect> MapPgSect1(TConfig cfg)
        {
            var rows = new List<TPgSect>();

            string sql = @"
                SELECT
                	DISTINCT t2.in_sub_event
                	, t1.in_sub_id
                	, t1.in_name
                FROM 
                	IN_MEETING_PSECT t1 WITH(NOLOCK) 
                INNER JOIN
                	IN_MEETING_PROGRAM t2 WITH(NOLOCK)
                	ON t2.id = t1.in_program
                WHERE 
                	t1.in_meeting = '{#meeting_id}'
                	AND t2.in_sub_event IN (5, 6)
                ORDER BY
                    t2.in_sub_event
                    , t1.in_sub_id
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_sub_event = item.getProperty("in_sub_event", "");
                string in_sect_start = item.getProperty("in_sect_start", "");
                string sect_id = item.getProperty("in_sub_id", "");
                string sect_name = item.getProperty("in_name", "");

                string name = "";
                if (in_sub_event == "5")
                {
                    name = "五人團體賽";
                }
                if (in_sub_event == "6")
                {
                    name = "男女混合團體賽(六人)";
                }

                var row = rows.Find(x => x.name == name);

                if (row == null)
                {
                    row = new TPgSect
                    {
                        name = name,
                        count = in_sub_event,
                        draw = in_sect_start,
                        kvs = new List<TKV>(),
                    };
                    rows.Add(row);
                }

                row.kvs.Add(new TKV
                {
                    id = sect_id,
                    name = sect_name,
                });

            }
            return rows;
        }

        private List<TPgSect> MapPgSect2(TConfig cfg)
        {
            var rows = new List<TPgSect>();

            string sql = @"
                SELECT
                	DISTINCT t2.in_sect_start
                	, t2.in_sub_event
                	, t1.in_sub_id
                	, t1.in_name
                FROM 
                	IN_MEETING_PSECT t1 WITH(NOLOCK) 
                INNER JOIN
                	IN_MEETING_PROGRAM t2 WITH(NOLOCK)
                	ON t2.id = t1.in_program
                WHERE 
                	t1.in_meeting = '{#meeting_id}'
                	AND t2.in_sub_event IN (5, 6)
                	AND t2.{#property} LIKE '%' + N'{#ages}' + '%'
					AND ISNULL(in_sect_start, 0) > 0
                ORDER BY
                    t2.in_sub_event
                    , t1.in_sub_id
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#property}", cfg.in_property)
                .Replace("{#ages}", cfg.ages);

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_sub_event = item.getProperty("in_sub_event", "");
                string in_sect_start = item.getProperty("in_sect_start", "");
                string sect_id = item.getProperty("in_sub_id", "");
                string sect_name = item.getProperty("in_name", "");

                string name = "";
                if (in_sub_event == "5")
                {
                    name = "五人團體賽";
                }
                if (in_sub_event == "6")
                {
                    name = "男女混合團體賽(六人)";
                }

                var row = rows.Find(x => x.name == name);

                if (row == null)
                {
                    row = new TPgSect
                    {
                        name = name,
                        count = in_sub_event,
                        draw = in_sect_start,
                        kvs = new List<TKV>(),
                    };
                    rows.Add(row);
                }

                row.kvs.Add(new TKV
                {
                    id = sect_id,
                    name = sect_name,
                });

            }
            return rows;
        }

        private class TPgSect
        {
            public string name { get; set; }
            public string count { get; set; }
            public string draw { get; set; }
            public List<TKV> kvs { get; set; }
        }

        private class TKV
        {
            public string id { get; set; }
            public string name { get; set; }
        }

        private void ModalSave(TConfig cfg, Item itmReturn)
        {
            string sect_id = itmReturn.getProperty("sect_id", "");
            string in_ranges = itmReturn.getProperty("in_ranges", "");

            Item itmProgram = cfg.inn.applySQL("SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + cfg.program_id + "'");
            Item itmOld = cfg.inn.applySQL("SELECT * FROM IN_MEETING_PSECT WITH(NOLOCK) WHERE id = '" + sect_id + "'");

            string pg_short_name = itmProgram.getProperty("in_short_name", "");

            string sect_name = itmOld.getProperty("in_name", "");
            string sect_gender = itmOld.getProperty("in_gender", "");

            string in_weight_min = "";
            string in_weight_val = "";
            string in_weight_txt = "";
            if (in_ranges != "")
            {
                var arr = in_ranges.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                in_weight_min = arr.First();
                in_weight_val = arr.Last();
                in_weight_txt = in_weight_val + "kg";
            }

            if (in_weight_val.Contains("-"))
            {
                if (pg_short_name.Contains("混"))
                {
                    sect_name = (sect_gender == "W" ? "女子" : "男子")
                        + in_weight_txt;
                }
            }
            else
            {
                if (pg_short_name.Contains("混"))
                {
                    var pkg = GetWeightPackage(cfg, pg_short_name + sect_name);
                    if (pkg != null)
                    {
                        var idx = pkg.weights.FindIndex(x => x == in_weight_min);
                        if (idx > 0)
                        {
                            in_weight_txt = pkg.weights[idx - 1]
                                 .Replace("-", "+") + "kg";

                            sect_name = (sect_gender == "W" ? "女子" : "男子")
                                + in_weight_txt;
                        }
                    }
                }
            }

            string sql = "UPDATE IN_MEETING_PSECT SET"
                + " in_name = '" + sect_name + "'"
                + ", in_weight = '" + in_weight_txt + "'"
                + ", in_ranges = '" + in_ranges + "'"
                + " WHERE id = '" + sect_id + "'";

            Item itmUpd = cfg.inn.applySQL(sql);

            if (itmUpd.isError())
            {
                throw new Exception("更新發生錯誤");
            }

        }

        private void Modal(TConfig cfg, Item itmReturn)
        {
            string sect_id = itmReturn.getProperty("sect_id", "");

            string sql = @"
                SELECT 
                    t1.in_name       AS 'sect_name'
                    , t1.in_ranges   AS 'sect_ranges'
                    , t1.in_note     AS 'sect_note'
                	, t2.in_name2
                	, t2.in_short_name
                FROM 
                	IN_MEETING_PSECT t1 WITH(NOLOCK)
                INNER JOIN
                	IN_MEETING_PROGRAM t2 WITH(NOLOCK)
                	ON t2.id = t1.in_program
                WHERE
                	t1.id = '{#sect_id}'
                ORDER BY
                	t2.in_sub_event
            ";

            sql = sql.Replace("{#sect_id}", sect_id);

            Item item = cfg.inn.applySQL(sql);

            if (item.isError() || item.getResult() == "")
            {
                return;
            }


            string sect_name = item.getProperty("sect_name", "");
            string sect_ranges = item.getProperty("sect_ranges", "");
            string sect_note = item.getProperty("sect_note", "");
            string in_name2 = item.getProperty("in_name2", "");
            string in_short_name = item.getProperty("in_short_name", "");

            itmReturn.setProperty("sect_name", sect_name);
            itmReturn.setProperty("sect_ranges", sect_ranges);
            itmReturn.setProperty("sect_note", sect_note);
            itmReturn.setProperty("in_name2", in_name2);
            itmReturn.setProperty("in_short_name", in_short_name);

            if (in_short_name.Contains("混"))
            {
                in_short_name += sect_name;
            }

            var pkg = GetWeightPackage(cfg, in_short_name);

            if (pkg != null)
            {
                var sb = new StringBuilder();
                for (int i = 0; i < pkg.weights.Count; i++)
                {
                    var no = i + 1;
                    var wt = pkg.weights[i];
                    var text = "第" + no + "級: " + wt;

                    sb.Append("<span data-weight='" + wt + "' class='span-btn' style='margin-right: 6px' onclick='SetWeight(this)' >" + text + "</span>");
                }
                itmReturn.setProperty("inn_title", pkg.title);
                itmReturn.setProperty("inn_weight", sb.ToString());
            }
        }

        private TWPkg GetWeightPackage(TConfig cfg, string in_short_name)
        {
            var pkg = default(TWPkg);

            if (in_short_name.Contains("特"))
            {
                if (in_short_name.Contains("女"))
                {
                    pkg = AgeWeights_Society_W(cfg);
                }
                else
                {
                    pkg = AgeWeights_Society_M(cfg);
                }
            }
            else if (in_short_name.Contains("社"))
            {
                if (in_short_name.Contains("女"))
                {
                    pkg = AgeWeights_Society_W(cfg);
                }
                else
                {
                    pkg = AgeWeights_Society_M(cfg);
                }
            }
            else if (in_short_name.Contains("大"))
            {
                if (in_short_name.Contains("女"))
                {
                    pkg = AgeWeights_Society_W(cfg);
                }
                else
                {
                    pkg = AgeWeights_Society_M(cfg);
                }
            }
            else if (in_short_name.Contains("高"))
            {
                if (in_short_name.Contains("女"))
                {
                    pkg = AgeWeights_High_W(cfg);
                }
                else
                {
                    pkg = AgeWeights_High_M(cfg);
                }
            }
            else if (in_short_name.Contains("國"))
            {
                if (in_short_name.Contains("女"))
                {
                    pkg = AgeWeights_Senior_W(cfg);
                }
                else
                {
                    pkg = AgeWeights_Senior_M(cfg);
                }
            }
            else if (in_short_name.Contains("小"))
            {
                if (in_short_name.Contains("A"))
                {
                    if (in_short_name.Contains("女"))
                    {
                        pkg = AgeWeights_Element_A_W(cfg);
                    }
                    else
                    {
                        pkg = AgeWeights_Element_A_M(cfg);
                    }
                }
                else if (in_short_name.Contains("B"))
                {
                    if (in_short_name.Contains("女"))
                    {
                        pkg = AgeWeights_Element_B_W(cfg);
                    }
                    else
                    {
                        pkg = AgeWeights_Element_B_M(cfg);
                    }
                }
                else if (in_short_name.Contains("C"))
                {
                    if (in_short_name.Contains("女"))
                    {
                        pkg = AgeWeights_Element_C_W(cfg);
                    }
                    else
                    {
                        pkg = AgeWeights_Element_C_M(cfg);
                    }
                }
                else
                {
                    if (in_short_name.Contains("女"))
                    {
                        pkg = AgeWeights_Element_A_W(cfg);
                    }
                    else
                    {
                        pkg = AgeWeights_Element_A_M(cfg);
                    }
                }
            }
            return pkg;
        }

        //社會男、大專男、特別男
        private TWPkg AgeWeights_Society_M(TConfig cfg)
        {
            return new TWPkg { title = "社男", weights = GetGroupWeightList(cfg, "社男甲") };
        }

        //社會女、大專女、特別女
        private TWPkg AgeWeights_Society_W(TConfig cfg)
        {
            return new TWPkg { title = "社女", weights = GetGroupWeightList(cfg, "社女甲") };
        }

        //高中男
        private TWPkg AgeWeights_High_M(TConfig cfg)
        {
            return new TWPkg { title = "高男", weights = GetGroupWeightList(cfg, "高男") };
        }

        //高中女
        private TWPkg AgeWeights_High_W(TConfig cfg)
        {
            return new TWPkg { title = "高女", weights = GetGroupWeightList(cfg, "高女") };
        }

        //國中男
        private TWPkg AgeWeights_Senior_M(TConfig cfg)
        {
            return new TWPkg { title = "國男", weights = GetGroupWeightList(cfg, "國男") };
        }

        //國中女
        private TWPkg AgeWeights_Senior_W(TConfig cfg)
        {
            return new TWPkg { title = "國女", weights = GetGroupWeightList(cfg, "國女") };
        }

        //國小A男(高年級)
        private TWPkg AgeWeights_Element_A_M(TConfig cfg)
        {
            return new TWPkg { title = "國小A男", weights = GetGroupWeightList(cfg, "小男A") };
        }

        //國小A女(高年級)
        private TWPkg AgeWeights_Element_A_W(TConfig cfg)
        {
            return new TWPkg { title = "國小A女", weights = GetGroupWeightList(cfg, "小女A") };
        }

        //國小B男(中年級)
        private TWPkg AgeWeights_Element_B_M(TConfig cfg)
        {
            return new TWPkg { title = "國小B男", weights = GetGroupWeightList(cfg, "小男B") };
        }

        //國小B女(中年級)
        private TWPkg AgeWeights_Element_B_W(TConfig cfg)
        {
            return new TWPkg { title = "國小B女", weights = GetGroupWeightList(cfg, "小女B") };
        }

        //國小C男(低年級)
        private TWPkg AgeWeights_Element_C_M(TConfig cfg)
        {
            return new TWPkg { title = "國小C男", weights = GetGroupWeightList(cfg, "小男C") };
        }

        //國小C女(低年級)
        private TWPkg AgeWeights_Element_C_W(TConfig cfg)
        {
            return new TWPkg { title = "國小C女", weights = GetGroupWeightList(cfg, "小女C") };
        }

        private List<string> GetGroupWeightList(TConfig cfg, string in_group)
        {
            var list = new List<string>();
            var sql = @"
                SELECT 
	                REPLACE(in_weight, 'Kg', '') AS 'in_weight'
                FROM 
	                IN_GROUP_WEIGHT WITH(NOLOCK) 
                WHERE 
	                in_group = N'{#in_group}'
                ORDER BY
	                in_index
            ";

            sql = sql.Replace("{#in_group}", in_group);
            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var w = item.getProperty("in_weight", "");
                list.Add(w);
            }
            return list;
        }

        //重建
        private void Rebuild(TConfig cfg, Item itmReturn)
        {
            string sql = "SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK)"
                + " WHERE id = '" + cfg.program_id + "'";

            Item itmPrograms = cfg.inn.applySQL(sql);

            if (itmPrograms.isError() || itmPrograms.getResult() == "")
            {
                return;
            }

            int count = itmPrograms.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item itmProgram = itmPrograms.getItemByIndex(i);
                string program_id = itmProgram.getProperty("id", "");
                string in_name = itmProgram.getProperty("in_name", "");

                sql = "SELECT TOP 1 * FROM IN_MEETING_PSECT WITH(NOLOCK) WHERE in_program = '" + program_id + "'";
                Item itmPSect = cfg.inn.applySQL(sql);
                if (itmPSect.isError() || itmPSect.getResult() == "")
                {
                    throw new Exception(in_name + " 未設定團體賽量級");
                }

                string in_sub_event = "";
                string in_team_battle = itmPSect.getProperty("in_team_battle", "");

                switch (in_team_battle)
                {
                    case "weight_3":
                        in_sub_event = "3";
                        break;

                    case "weight_5":
                        in_sub_event = "5";
                        break;

                    case "mix_6":
                        in_sub_event = "6";
                        break;

                    default:
                        throw new Exception(in_name + " 未設定團體賽量級");
                }

                sql = "UPDATE IN_MEETING_PROGRAM SET"
                    + " in_sub_event = '" + in_sub_event + "'"
                    + ", in_team_battle = '" + in_team_battle + "'"
                    + " WHERE id = '" + program_id + "'";

                Item itmUpd = cfg.inn.applySQL(sql);

                if (itmUpd.isError())
                {
                    throw new Exception("比賽模式變更失敗");
                }

                //重建子場次
                GenerateEvents(cfg, itmProgram, itmReturn);

                //更新場次輪次
                UpdateRoundSerial(cfg, itmProgram);
            }
        }

        private void Create(TConfig cfg, Item itmReturn)
        {
            cfg.fight_mode = itmReturn.getProperty("fight_mode", "");

            string sql = "SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK)"
                + " WHERE id = '" + cfg.program_id + "'"
                + " ORDER BY in_sort_order";

            Item itmPrograms = cfg.inn.applySQL(sql);

            if (itmPrograms.isError() || itmPrograms.getResult() == "")
            {
                return;
            }

            //清空資料
            sql = "DELETE FROM IN_MEETING_PSECT WHERE in_program = '" + cfg.program_id + "'";
            Item itmDelete = cfg.inn.applySQL(sql);
            if (itmDelete.isError())
            {
                throw new Exception("清空舊設定發生錯誤");
            }

            string in_sub_event = "0";
            switch (cfg.fight_mode)
            {
                case "weight_3":
                    in_sub_event = "3";
                    CreateSects_Weight3(cfg, itmPrograms, itmReturn);
                    break;

                case "weight_5":
                    in_sub_event = "5";
                    CreateSects_Weight5(cfg, itmPrograms, itmReturn);
                    break;

                case "mix_6":
                    in_sub_event = "6";
                    CreateSects_Mix6(cfg, itmPrograms, itmReturn);
                    break;
            }


            int count = itmPrograms.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item itmProgram = itmPrograms.getItemByIndex(i);
                string program_id = itmProgram.getProperty("id", "");

                sql = "UPDATE IN_MEETING_PROGRAM SET"
                    + " in_sub_event = '" + in_sub_event + "'"
                    + ", in_team_battle = '" + cfg.fight_mode + "'"
                    + " WHERE id = '" + program_id + "'";

                Item itmUpd = cfg.inn.applySQL(sql);

                if (itmUpd.isError())
                {
                    throw new Exception("比賽模式變更失敗");
                }

                //重建子場次
                GenerateEvents(cfg, itmProgram, itmReturn);

                //更新場次輪次
                UpdateRoundSerial(cfg, itmProgram);
            }
        }


        //更新場次輪次
        private void UpdateRoundSerial(TConfig cfg, Item itmProgram)
        {
            Item itmData = cfg.inn.newItem();
            itmData.setType("In_Meeting_Program");
            itmData.setProperty("meeting_id", itmProgram.getProperty("in_meeting", ""));
            itmData.setProperty("program_id", itmProgram.getProperty("id", ""));
            itmData.setProperty("scene", "round_serial");
            itmData.apply("in_team_battle_serial");
        }

        //修補與會者量級對照鍵值
        private void FixMUserTeamWeightN1(TConfig cfg, Item itmProgram, Item itmReturn)
        {
            string meeting_id = itmProgram.getProperty("in_meeting", "");
            string sql = "";

            //修補與會者量級對照鍵值
            sql = @"
                UPDATE t1 SET
                	t1.in_team_weight_n1 = REPLACE(t2.in_short_name, '團', N'')
                FROM
                	IN_MEETING_USER t1 WITH(NOLOCK)
                INNER JOIN
                	IN_MEETING_PROGRAM t2 WITH(NOLOCK)
                	ON t2.in_meeting = t1.source_id
                	AND t2.in_l1 = t1.in_l1
                	AND t2.in_l2 = t1.in_l2
                	AND t2.in_l3 = t1.in_l3
                WHERE
                	t1.source_id = '{#meeting_id}'
                	AND t1.in_l1 = N'團體組'
                	AND t1.in_l2 NOT LIKE '%特別%'
            ";

            sql = sql.Replace("{#meeting_id}", meeting_id);

            cfg.inn.applySQL(sql);

            //修補小男A
            sql = @"
                UPDATE t1 SET
                	t1.in_team_weight_n1 = N'小男A'
                FROM
                	IN_MEETING_USER t1 WITH(NOLOCK)
                WHERE
                	t1.source_id = '{#meeting_id}'
                	AND t1.in_l1 = N'團體組'
                	AND ISNULL(t1.in_team_weight_n1, '') = N'小男'
            ";

            sql = sql.Replace("{#meeting_id}", meeting_id);

            cfg.inn.applySQL(sql);

            //修補小女A
            sql = @"
                UPDATE t1 SET
                	t1.in_team_weight_n1 = N'小女A'
                FROM
                	IN_MEETING_USER t1 WITH(NOLOCK)
                WHERE
                	t1.source_id = '{#meeting_id}'
                	AND t1.in_l1 = N'團體組'
                	AND ISNULL(t1.in_team_weight_n1, '') = N'小女'
            ";

            sql = sql.Replace("{#meeting_id}", meeting_id);

            cfg.inn.applySQL(sql);
        }

        //建立場次
        private void GenerateEvents(TConfig cfg, Item itmProgram, Item itmReturn)
        {
            string battle_type = itmProgram.getProperty("in_battle_type", "");

            string method = "";
            string mode = "";

            Item item = cfg.inn.newItem();
            item.setType("In_Meeting_Program");
            item.setProperty("meeting_id", itmProgram.getProperty("in_meeting", ""));
            item.setProperty("program_id", itmProgram.getProperty("id", ""));
            item.setProperty("battle_type", battle_type);
            item.setProperty("scene", "run_sub_event");

            switch (battle_type)
            {
                case "TopTwo"://單淘
                    method = "in_meeting_program_4column";
                    break;

                case "JudoTopFour"://四柱復活
                    method = "in_meeting_program_4column";
                    break;

                case "Challenge"://挑戰賽
                    method = "in_meeting_program_4column";
                    break;

                case "SingleRoundRobin"://單循環
                    method = "in_meeting_program_robin";
                    break;

                case "DoubleRoundRobin"://雙循環
                    method = "in_meeting_program_robin";
                    break;

                default:
                    break;
            }

            if (method == "")
            {
                throw new Exception("賽制錯誤");
            }

            item.apply(method);

        }

        private void CreateSects_Weight3(TConfig cfg, Item itmPrograms, Item itmReturn)
        {
            int count = itmPrograms.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item itmProgram = itmPrograms.getItemByIndex(i);
                string in_name2 = itmProgram.getProperty("in_name2", "");
                string in_short_name = itmProgram.getProperty("in_short_name", "");

                switch (in_short_name)
                {
                    case "特別組團體賽":
                    case "團男30特三":
                    case "團男40特三":
                        MergeSects(cfg, itmProgram, GetSects_Weight3_Special_M);
                        break;

                    case "團女30特三":
                    case "團女40特三":
                        MergeSects(cfg, itmProgram, GetSects_Weight3_Special_W);
                        break;

                    case "團社男甲":
                    case "團社男乙":
                    case "團大男甲":
                    case "團大男乙":
                    case "團社男甲三":
                    case "團社男乙三":
                    case "團大男甲三":
                    case "團大男乙三":
                        MergeSects(cfg, itmProgram, GetSects_Weight3_Social_M);
                        break;

                    case "團社女甲":
                    case "團社女乙":
                    case "團大女甲":
                    case "團大女乙":
                    case "團社女甲三":
                    case "團社女乙三":
                    case "團大女甲三":
                    case "團大女乙三":
                        MergeSects(cfg, itmProgram, GetSects_Weight3_Social_W);
                        break;


                    case "團高男":
                    case "團高男三":
                        MergeSects(cfg, itmProgram, GetSects_Weight3_High_M);
                        break;

                    case "團高女":
                    case "團高女三":
                        MergeSects(cfg, itmProgram, GetSects_Weight3_High_W);
                        break;

                    case "團國男":
                    case "團國男三":
                        MergeSects(cfg, itmProgram, GetSects_Weight3_Senior_M);
                        break;

                    case "團國女":
                    case "團國女三":
                        MergeSects(cfg, itmProgram, GetSects_Weight3_Senior_W);
                        break;

                    case "團小男":
                    case "團小男三":
                        MergeSects(cfg, itmProgram, GetSects_Weight3_Element_A_M);
                        break;

                    case "團小女":
                    case "團小女三":
                        MergeSects(cfg, itmProgram, GetSects_Weight3_Element_A_W);
                        break;

                    default:
                        throw new Exception("團體簡稱查無對應量級表");
                }
            }
        }

        //三人團體賽：特別組團體賽
        private List<TSect> GetSects_Weight3_Special_M()
        {
            return GetSects_Weight3_Special("M");
        }

        //三人團體賽：特別組團體賽
        private List<TSect> GetSects_Weight3_Special_W()
        {
            return GetSects_Weight3_Special("W");
        }

        //三人團體賽：特別組團體賽
        private List<TSect> GetSects_Weight3_Special(string in_gender = "")
        {
            var sects = new List<TSect>();
            sects.Add(new TSect { T1 = "1", T2 = "先鋒", T3 = in_gender, T4 = "不分級", T5 = "", T6 = "" });
            sects.Add(new TSect { T1 = "2", T2 = "中堅", T3 = in_gender, T4 = "不分級", T5 = "", T6 = "" });
            sects.Add(new TSect { T1 = "3", T2 = "主將", T3 = in_gender, T4 = "不分級", T5 = "", T6 = "" });
            return sects;
        }

        //三人團體賽(男)：社會、大專
        private List<TSect> GetSects_Weight3_Social_M()
        {
            var sects = new List<TSect>();
            sects.Add(new TSect { T1 = "1", T2 = "先鋒", T3 = "M", T4 = "-66kg", T5 = "-60,-66", T6 = "先鋒限第 1-2 級" });
            sects.Add(new TSect { T1 = "2", T2 = "中堅", T3 = "M", T4 = "-81kg", T5 = "-73,-81", T6 = "中堅限第 3-4 級" });
            sects.Add(new TSect { T1 = "3", T2 = "主將", T3 = "M", T4 = "-100kg", T5 = "-90,-100", T6 = "主將限第 5-6 級" });
            return sects;
        }

        //三人團體賽(女)：社會、大專、特別
        private List<TSect> GetSects_Weight3_Social_W()
        {
            var sects = new List<TSect>();
            sects.Add(new TSect { T1 = "1", T2 = "先鋒", T3 = "W", T4 = "-52kg", T5 = "-48,-52", T6 = "先鋒限第 1-2 級" });
            sects.Add(new TSect { T1 = "2", T2 = "中堅", T3 = "W", T4 = "-63kg", T5 = "-57,-63", T6 = "中堅限第 3-4 級" });
            sects.Add(new TSect { T1 = "3", T2 = "主將", T3 = "W", T4 = "-78kg", T5 = "-70,-78", T6 = "主將限第 5-6 級" });
            return sects;
        }

        //三人團體賽(男)：高中
        private List<TSect> GetSects_Weight3_High_M()
        {
            var sects = new List<TSect>();
            sects.Add(new TSect { T1 = "1", T2 = "先鋒", T3 = "M", T4 = "-66kg", T5 = "-55,-60,-66", T6 = "先鋒限第 1-3 級" });
            sects.Add(new TSect { T1 = "2", T2 = "中堅", T3 = "M", T4 = "-81kg", T5 = "-73,-81", T6 = "中堅限第 4-5 級" });
            sects.Add(new TSect { T1 = "3", T2 = "主將", T3 = "M", T4 = "-100kg", T5 = "-90,-100", T6 = "主將限第 6-7 級" });
            return sects;
        }

        //三人團體賽(女)：高中
        private List<TSect> GetSects_Weight3_High_W()
        {
            var sects = new List<TSect>();
            sects.Add(new TSect { T1 = "1", T2 = "先鋒", T3 = "W", T4 = "-52kg", T5 = "-44,-48,-52", T6 = "先鋒限第 1-3 級" });
            sects.Add(new TSect { T1 = "2", T2 = "中堅", T3 = "W", T4 = "-63kg", T5 = "-57,-63", T6 = "中堅限第 4-5 級" });
            sects.Add(new TSect { T1 = "3", T2 = "主將", T3 = "W", T4 = "-78kg", T5 = "-70,-78", T6 = "主將限第 6-7 級" });
            return sects;
        }

        //三人團體賽(男)：國中
        private List<TSect> GetSects_Weight3_Senior_M()
        {
            var sects = new List<TSect>();
            sects.Add(new TSect { T1 = "1", T2 = "先鋒", T3 = "M", T4 = "-46kg", T5 = "-38,-42,-46", T6 = "先鋒限第 1-3 級" });
            sects.Add(new TSect { T1 = "2", T2 = "中堅", T3 = "M", T4 = "-60kg", T5 = "-50,-55,-60", T6 = "中堅限第 4-6 級" });
            sects.Add(new TSect { T1 = "3", T2 = "主將", T3 = "M", T4 = "-81kg", T5 = "-66,-73,-81", T6 = "主將限第 7-9 級" });
            return sects;
        }

        //三人團體賽(女)：國中
        private List<TSect> GetSects_Weight3_Senior_W()
        {
            var sects = new List<TSect>();
            sects.Add(new TSect { T1 = "1", T2 = "先鋒", T3 = "W", T4 = "-44kg", T5 = "-36,-40,-44", T6 = "先鋒限第 1-3 級" });
            sects.Add(new TSect { T1 = "2", T2 = "中堅", T3 = "W", T4 = "-57kg", T5 = "-48,-52,-57", T6 = "中堅限第 4-6 級" });
            sects.Add(new TSect { T1 = "3", T2 = "主將", T3 = "W", T4 = "-70kg", T5 = "-63,-70", T6 = "主將限第 7-8 級" });
            return sects;
        }

        //三人團體賽(男)：國小
        private List<TSect> GetSects_Weight3_Element_A_M()
        {
            var sects = new List<TSect>();
            sects.Add(new TSect { T1 = "1", T2 = "先鋒", T3 = "M", T4 = "-33kg", T5 = "-30,-33", T6 = "先鋒限第 1-2 級" });
            sects.Add(new TSect { T1 = "2", T2 = "中堅", T3 = "M", T4 = "-41kg", T5 = "-37,-41", T6 = "中堅限第 3-4 級" });
            sects.Add(new TSect { T1 = "3", T2 = "主將", T3 = "M", T4 = "-50kg", T5 = "-45,-50", T6 = "主將限第 5-6 級" });
            return sects;
        }

        //三人團體賽(女)：國小
        private List<TSect> GetSects_Weight3_Element_A_W()
        {
            var sects = new List<TSect>();
            sects.Add(new TSect { T1 = "1", T2 = "先鋒", T3 = "W", T4 = "-33kg", T5 = "-30,-33", T6 = "先鋒限第 1-2 級" });
            sects.Add(new TSect { T1 = "2", T2 = "中堅", T3 = "W", T4 = "-41kg", T5 = "-37,-41", T6 = "中堅限第 3-4 級" });
            sects.Add(new TSect { T1 = "3", T2 = "主將", T3 = "W", T4 = "-50kg", T5 = "-45,-50", T6 = "主將限第 5-6 級" });
            return sects;
        }

        private void CreateSects_Mix6(TConfig cfg, Item itmPrograms, Item itmReturn)
        {
            int count = itmPrograms.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item itmProgram = itmPrograms.getItemByIndex(i);
                string in_name2 = itmProgram.getProperty("in_name2", "");
                string in_short_name = itmProgram.getProperty("in_short_name", "");

                switch (in_short_name)
                {
                    case "混團":
                    case "團混":
                    case "團社甲混":
                    case "團社乙混":
                    case "團大甲混":
                    case "團大乙混":
                        MergeSects(cfg, itmProgram, GetSects_Mix6_Social);
                        break;

                    case "團高混":
                        MergeSects(cfg, itmProgram, GetSects_Mix6_High);
                        break;

                    case "團國混":
                        MergeSects(cfg, itmProgram, GetSects_Mix6_Senior);
                        break;

                    case "團小混":
                        MergeSects(cfg, itmProgram, GetSects_Mix6_Element);
                        break;

                    default:
                        throw new Exception("團體簡稱查無對應量級表");
                }
            }
        }

        //混合組團體賽-社會、大專
        private List<TSect> GetSects_Mix6_Social()
        {
            var sects = new List<TSect>();
            sects.Add(new TSect { T1 = "1", T2 = "女子-57kg", T3 = "W", T4 = "-57kg", T5 = "-48,-52,-57", T6 = "限57.0公斤以下" });
            sects.Add(new TSect { T1 = "2", T2 = "男子-73kg", T3 = "M", T4 = "-73kg", T5 = "-60,-66,-73", T6 = "限73.0公斤以下" });
            sects.Add(new TSect { T1 = "3", T2 = "女子-70kg", T3 = "W", T4 = "-70kg", T5 = "-63,-70", T6 = "限57.1-70.0公斤以下" });
            sects.Add(new TSect { T1 = "4", T2 = "男子-90kg", T3 = "M", T4 = "-90kg", T5 = "-81,-90", T6 = "限73.1-90.0公斤以下" });
            sects.Add(new TSect { T1 = "5", T2 = "女子+70kg", T3 = "W", T4 = "+70kg", T5 = "-78,+78", T6 = "限70.1公斤以上" });
            sects.Add(new TSect { T1 = "6", T2 = "男子+90kg", T3 = "M", T4 = "+90kg", T5 = "-100,+100", T6 = "限90.1公斤以上" });
            return sects;
        }

        //混合組團體賽-高中
        private List<TSect> GetSects_Mix6_High()
        {
            var sects = new List<TSect>();
            sects.Add(new TSect { T1 = "1", T2 = "女子-57kg", T3 = "W", T4 = "-57kg", T5 = "-48,-52,-57", T6 = "限57.0公斤以下" });
            sects.Add(new TSect { T1 = "2", T2 = "男子-73kg", T3 = "M", T4 = "-73kg", T5 = "-60,-66,-73", T6 = "限73.0公斤以下" });
            sects.Add(new TSect { T1 = "3", T2 = "女子-70kg", T3 = "W", T4 = "-70kg", T5 = "-63,-70", T6 = "限57.1-70.0公斤以下" });
            sects.Add(new TSect { T1 = "4", T2 = "男子-90kg", T3 = "M", T4 = "-90kg", T5 = "-81,-90", T6 = "限73.1-90.0公斤以下" });
            sects.Add(new TSect { T1 = "5", T2 = "女子+70kg", T3 = "W", T4 = "+70kg", T5 = "-78,+78", T6 = "限70.1公斤以上" });
            sects.Add(new TSect { T1 = "6", T2 = "男子+90kg", T3 = "M", T4 = "+90kg", T5 = "-100,+100", T6 = "限90.1公斤以上" });
            return sects;
        }

        //混合組團體賽-國中
        private List<TSect> GetSects_Mix6_Senior()
        {
            var sects = new List<TSect>();
            sects.Add(new TSect { T1 = "1", T2 = "女子-48kg", T3 = "W", T4 = "-48kg", T5 = "-36,-40,-44,-48", T6 = "限48.0公斤以下" });
            sects.Add(new TSect { T1 = "2", T2 = "男子-50kg", T3 = "M", T4 = "-50kg", T5 = "-38,-42,-46,-50", T6 = "限50.0公斤以下" });
            sects.Add(new TSect { T1 = "3", T2 = "女子-63kg", T3 = "W", T4 = "-63kg", T5 = "-52,-57,-63", T6 = "限48.1-63.0公斤以下" });
            sects.Add(new TSect { T1 = "4", T2 = "男子-73kg", T3 = "M", T4 = "-73kg", T5 = "-55,-60,-66,-73", T6 = "限50.1-73.0公斤以下" });
            sects.Add(new TSect { T1 = "5", T2 = "女子+63kg", T3 = "W", T4 = "+63kg", T5 = "-70,+70", T6 = "限63.1公斤以上" });
            sects.Add(new TSect { T1 = "6", T2 = "男子+73kg", T3 = "M", T4 = "+73kg", T5 = "-81,+81", T6 = "限73.1公斤以上" });
            return sects;
        }

        //混合組團體賽-國小
        private List<TSect> GetSects_Mix6_Element()
        {
            var sects = new List<TSect>();
            sects.Add(new TSect { T1 = "1", T2 = "女子-37kg", T3 = "W", T4 = "-37kg", T5 = "-30,-33,-37", T6 = "限37.0公斤以下" });
            sects.Add(new TSect { T1 = "2", T2 = "男子-37kg", T3 = "M", T4 = "-37kg", T5 = "-30,-33,-37", T6 = "限37.0公斤以下" });
            sects.Add(new TSect { T1 = "3", T2 = "女子-50kg", T3 = "W", T4 = "-50kg", T5 = "-41,-45,-50", T6 = "限37.1-50.0公斤以下" });
            sects.Add(new TSect { T1 = "4", T2 = "男子-50kg", T3 = "M", T4 = "-50kg", T5 = "-41,-45,-50", T6 = "限37.1-50.0公斤以下" });
            sects.Add(new TSect { T1 = "5", T2 = "女子+50kg", T3 = "W", T4 = "+50kg", T5 = "-55,+55", T6 = "限50.1-60.0公斤以下" });
            sects.Add(new TSect { T1 = "6", T2 = "男子+50kg", T3 = "M", T4 = "+50kg", T5 = "-55,+55", T6 = "限50.1-60.0公斤以下" });
            return sects;
        }

        private void CreateSects_Weight5(TConfig cfg, Item itmPrograms, Item itmReturn)
        {
            int count = itmPrograms.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item itmProgram = itmPrograms.getItemByIndex(i);
                string in_name2 = itmProgram.getProperty("in_name2", "");
                string in_short_name = itmProgram.getProperty("in_short_name", "");

                switch (in_short_name)
                {
                    case "特別組團體賽":
                    case "團男特30":
                    case "團男特41":
                    case "團男30特五":
                    case "團男40特五":
                    case "團男特五-40":
                    case "團男特五+40":
                    case "團特別男+40五":
                        MergeSects(cfg, itmProgram, GetSects_Weight5_Special_M);
                        break;

                    case "團女特30":
                    case "團女特41":
                    case "團女30特五":
                    case "團女40特五":
                    case "團女特五-40":
                    case "團女特五+40":
                    case "團特別女+40五":
                        MergeSects(cfg, itmProgram, GetSects_Weight5_Special_W);
                        break;

                    case "團社男甲":
                    case "團大男甲":
                    case "團社男甲五":
                    case "團大男甲五":
                        MergeSects(cfg, itmProgram, GetSects_Weight5_C_MA);
                        break;

                    case "團社男乙":
                    case "團大男乙":
                    case "團社男乙五":
                    case "團大男乙五":
                        MergeSects(cfg, itmProgram, GetSects_Weight5_C_MB);
                        break;

                    case "團社女甲":
                    case "團大女甲":
                    case "團社女甲五":
                    case "團大女甲五":
                        MergeSects(cfg, itmProgram, GetSects_Weight5_C_WA);
                        break;

                    case "團社女乙":
                    case "團大女乙":
                    case "團社女乙五":
                    case "團大女乙五":
                        MergeSects(cfg, itmProgram, GetSects_Weight5_C_WB);
                        break;


                    case "團高男":
                    case "團高男五":
                        MergeSects(cfg, itmProgram, GetSects_Weight5_H_M);
                        break;

                    case "團高女":
                    case "團高女五":
                        MergeSects(cfg, itmProgram, GetSects_Weight5_H_W);
                        break;

                    case "團國男":
                    case "團國男五":
                        MergeSects(cfg, itmProgram, GetSects_Weight5_J_M);
                        break;

                    case "團國女":
                    case "團國女五":
                        MergeSects(cfg, itmProgram, GetSects_Weight5_J_W);
                        break;

                    case "團小男":
                    case "團小男五":
                        MergeSects(cfg, itmProgram, GetSects_Weight5_E_M);
                        break;

                    case "團小女":
                    case "團小女五":
                        MergeSects(cfg, itmProgram, GetSects_Weight5_E_W);
                        break;

                    default:
                        throw new Exception("團體簡稱查無對應量級表");
                }
            }
        }

        private void MergeSects(TConfig cfg, Item itmProgram, Func<List<TSect>> getSects)
        {
            var in_program = itmProgram.getProperty("id", "");
            var in_program_name = itmProgram.getProperty("in_name", "");

            var sects = getSects();

            for (int i = 0; i < sects.Count; i++)
            {
                var sect = sects[i];

                Item itmPSect = cfg.inn.newItem("IN_MEETING_PSECT");

                itmPSect.setAttribute("where", "in_program = '" + in_program + "'"
                    + " AND in_sub_id = '" + sect.T1 + "'");

                itmPSect.setProperty("in_meeting", cfg.meeting_id);
                itmPSect.setProperty("in_program", in_program);
                itmPSect.setProperty("in_program_name", in_program_name);
                itmPSect.setProperty("in_team_battle", cfg.fight_mode);
                itmPSect.setProperty("in_sub_id", sect.T1);

                itmPSect.setProperty("in_name", sect.T2);
                itmPSect.setProperty("in_gender", sect.T3);
                itmPSect.setProperty("in_weight", sect.T4);
                itmPSect.setProperty("in_ranges", sect.T5);
                itmPSect.setProperty("in_note", sect.T6);

                Item itmResult = itmPSect.apply("merge");

                if (itmResult.isError())
                {
                    throw new Exception("新增團體賽組別失敗");
                }
            }
        }

        //特別組團體賽
        private List<TSect> GetSects_Weight5_Special_M()
        {
            return GetSects_Weight5_Special("M");
        }

        //特別組團體賽
        private List<TSect> GetSects_Weight5_Special_W()
        {
            return GetSects_Weight5_Special("W");
        }

        //特別組團體賽
        private List<TSect> GetSects_Weight5_Special(string in_gender = "")
        {
            var sects = new List<TSect>();
            sects.Add(new TSect { T1 = "1", T2 = "先鋒", T3 = in_gender, T4 = "不分級", T5 = "", T6 = "" });
            sects.Add(new TSect { T1 = "2", T2 = "次鋒", T3 = in_gender, T4 = "不分級", T5 = "", T6 = "" });
            sects.Add(new TSect { T1 = "3", T2 = "中堅", T3 = in_gender, T4 = "不分級", T5 = "", T6 = "" });
            sects.Add(new TSect { T1 = "4", T2 = "副將", T3 = in_gender, T4 = "不分級", T5 = "", T6 = "" });
            sects.Add(new TSect { T1 = "5", T2 = "主將", T3 = in_gender, T4 = "不分級", T5 = "", T6 = "" });
            return sects;
        }

        //大專男子組團體賽(甲組)
        private List<TSect> GetSects_Weight5_C_MA()
        {
            var sects = new List<TSect>();
            sects.Add(new TSect { T1 = "1", T2 = "先鋒", T3 = "M", T4 = "-90kg", T5 = "-60,-66,-73,-81,-90", T6 = "先鋒限第 1-5 級" });
            sects.Add(new TSect { T1 = "2", T2 = "次鋒", T3 = "M", T4 = "-90kg", T5 = "-60,-66,-73,-81,-90", T6 = "次鋒限第 1-5 級" });
            sects.Add(new TSect { T1 = "3", T2 = "中堅", T3 = "M", T4 = "-90kg", T5 = "-60,-66,-73,-81,-90", T6 = "中堅限第 1-5 級" });
            sects.Add(new TSect { T1 = "4", T2 = "副將", T3 = "M", T4 = "+81kg", T5 = "-90,-100,+100", T6 = "副將限第 5-7 級" });
            sects.Add(new TSect { T1 = "5", T2 = "主將", T3 = "M", T4 = "+81kg", T5 = "-90,-100,+100", T6 = "主將限第 5-7 級" });
            return sects;
        }

        //大專男子組團體賽(乙組)
        private List<TSect> GetSects_Weight5_C_MB()
        {
            var sects = new List<TSect>();
            sects.Add(new TSect { T1 = "1", T2 = "先鋒", T3 = "M", T4 = "-66kg", T5 = "-60,-66", T6 = "先鋒限第 1-2 級" });
            sects.Add(new TSect { T1 = "2", T2 = "次鋒", T3 = "M", T4 = "-73kg", T5 = "-60,-66,-73", T6 = "次鋒限第 1-3 級" });
            sects.Add(new TSect { T1 = "3", T2 = "中堅", T3 = "M", T4 = "-81kg", T5 = "-60,-66,-73,-81", T6 = "中堅限第 1-4 級" });
            sects.Add(new TSect { T1 = "4", T2 = "副將", T3 = "M", T4 = "-90kg", T5 = "-60,-66,-73,-81,-90", T6 = "副將限第 1-5 級" });
            sects.Add(new TSect { T1 = "5", T2 = "主將", T3 = "M", T4 = "+81kg", T5 = "-90,-100,+100", T6 = "主將限第 5-7 級" });
            return sects;
        }

        //大專女子組團體賽(甲組)
        private List<TSect> GetSects_Weight5_C_WA()
        {
            var sects = new List<TSect>();
            sects.Add(new TSect { T1 = "1", T2 = "先鋒", T3 = "W", T4 = "-70kg", T5 = "-48,-52,-57,-63,-70", T6 = "先鋒限第 1-5 級" });
            sects.Add(new TSect { T1 = "2", T2 = "次鋒", T3 = "W", T4 = "-70kg", T5 = "-48,-52,-57,-63,-70", T6 = "次鋒限第 1-5 級" });
            sects.Add(new TSect { T1 = "3", T2 = "中堅", T3 = "W", T4 = "-70kg", T5 = "-48,-52,-57,-63,-70", T6 = "中堅限第 1-5 級" });
            sects.Add(new TSect { T1 = "4", T2 = "副將", T3 = "W", T4 = "+63kg", T5 = "-70,-78,+78", T6 = "副將限第 5-7 級" });
            sects.Add(new TSect { T1 = "5", T2 = "主將", T3 = "W", T4 = "+63kg", T5 = "-70,-78,+78", T6 = "主將限第 5-7 級" });
            return sects;
        }

        //大專女子組團體賽(乙組)
        private List<TSect> GetSects_Weight5_C_WB()
        {
            var sects = new List<TSect>();
            sects.Add(new TSect { T1 = "1", T2 = "先鋒", T3 = "W", T4 = "-52kg", T5 = "-48,-52", T6 = "先鋒限第 1-2 級" });
            sects.Add(new TSect { T1 = "2", T2 = "次鋒", T3 = "W", T4 = "-57kg", T5 = "-48,-52,-57", T6 = "次鋒限第 1-3 級" });
            sects.Add(new TSect { T1 = "3", T2 = "中堅", T3 = "W", T4 = "-63kg", T5 = "-48,-52,-57,-63", T6 = "中堅限第 1-4 級" });
            sects.Add(new TSect { T1 = "4", T2 = "副將", T3 = "W", T4 = "-70kg", T5 = "-48,-52,-57,-63,-70", T6 = "副將限第 1-5 級" });
            sects.Add(new TSect { T1 = "5", T2 = "主將", T3 = "W", T4 = "+63kg", T5 = "-70,-78,+78", T6 = "主將限第 5-7 級" });
            return sects;
        }

        //高中男子組團體賽
        private List<TSect> GetSects_Weight5_H_M()
        {
            var sects = new List<TSect>();
            sects.Add(new TSect { T1 = "1", T2 = "先鋒", T3 = "M", T4 = "-90kg", T5 = "-55,-60,-66,-73,-81,-90", T6 = "先鋒限第 1-6 級" });
            sects.Add(new TSect { T1 = "2", T2 = "次鋒", T3 = "M", T4 = "-90kg", T5 = "-55,-60,-66,-73,-81,-90", T6 = "次鋒限第 1-6 級" });
            sects.Add(new TSect { T1 = "3", T2 = "中堅", T3 = "M", T4 = "-90kg", T5 = "-55,-60,-66,-73,-81,-90", T6 = "中堅限第 1-6 級" });
            sects.Add(new TSect { T1 = "4", T2 = "副將", T3 = "M", T4 = "+81kg", T5 = "-90,-100,+100", T6 = "副將限第 6-8 級" });
            sects.Add(new TSect { T1 = "5", T2 = "主將", T3 = "M", T4 = "+81kg", T5 = "-90,-100,+100", T6 = "主將限第 6-8 級" });
            return sects;
        }

        //高中女子組團體賽
        private List<TSect> GetSects_Weight5_H_W()
        {
            var sects = new List<TSect>();
            sects.Add(new TSect { T1 = "1", T2 = "先鋒", T3 = "W", T4 = "-70kg", T5 = "-48,-52,-57,-63,-70", T6 = "先鋒限第 1-5 級" });
            sects.Add(new TSect { T1 = "2", T2 = "次鋒", T3 = "W", T4 = "-70kg", T5 = "-48,-52,-57,-63,-70", T6 = "次鋒限第 1-5 級" });
            sects.Add(new TSect { T1 = "3", T2 = "中堅", T3 = "W", T4 = "-70kg", T5 = "-48,-52,-57,-63,-70", T6 = "中堅限第 1-5 級" });
            sects.Add(new TSect { T1 = "4", T2 = "副將", T3 = "W", T4 = "+63kg", T5 = "-70,-78,+78", T6 = "副將限第 5-7 級" });
            sects.Add(new TSect { T1 = "5", T2 = "主將", T3 = "W", T4 = "+63kg", T5 = "-70,-78,+78", T6 = "主將限第 5-7 級" });
            return sects;
        }

        //國中男子組團體賽
        private List<TSect> GetSects_Weight5_J_M()
        {
            var sects = new List<TSect>();
            sects.Add(new TSect { T1 = "1", T2 = "先鋒", T3 = "M", T4 = "-46kg", T5 = "-38,-42,-46", T6 = "先鋒限 1-3 級" });
            sects.Add(new TSect { T1 = "2", T2 = "次鋒", T3 = "M", T4 = "-55kg", T5 = "-46,-50,-55", T6 = "次鋒限 3-5 級" });
            sects.Add(new TSect { T1 = "3", T2 = "中堅", T3 = "M", T4 = "-66kg", T5 = "-55,-60,-66", T6 = "中堅限 5-7 級" });
            sects.Add(new TSect { T1 = "4", T2 = "副將", T3 = "M", T4 = "-73kg", T5 = "-60,-66,-73", T6 = "副將限 6-8 級" });
            sects.Add(new TSect { T1 = "5", T2 = "主將", T3 = "M", T4 = "+66kg", T5 = "-73,-81,+81", T6 = "主將限 8-10 級" });
            return sects;
        }

        //國中女子組團體賽
        private List<TSect> GetSects_Weight5_J_W()
        {
            var sects = new List<TSect>();
            sects.Add(new TSect { T1 = "1", T2 = "先鋒", T3 = "W", T4 = "-44kg", T5 = "-36,-40,-44", T6 = "先鋒限 1-3 級" });
            sects.Add(new TSect { T1 = "2", T2 = "次鋒", T3 = "W", T4 = "-52kg", T5 = "-44,-48,-52", T6 = "次鋒限 3-5 級" });
            sects.Add(new TSect { T1 = "3", T2 = "中堅", T3 = "W", T4 = "-63kg", T5 = "-52,-57,-63", T6 = "中堅限 5-7 級" });
            sects.Add(new TSect { T1 = "4", T2 = "副將", T3 = "W", T4 = "-70kg", T5 = "-57,-63,-70", T6 = "副將限 6-8 級" });
            sects.Add(new TSect { T1 = "5", T2 = "主將", T3 = "W", T4 = "+63kg", T5 = "-70,+70", T6 = "主將限第 8-9 級" });
            return sects;
        }

        //國小男子組團體賽
        private List<TSect> GetSects_Weight5_E_M()
        {
            var sects = new List<TSect>();
            sects.Add(new TSect { T1 = "1", T2 = "先鋒", T3 = "M", T4 = "-37kg", T5 = "-30,-33,-37", T6 = "先鋒限第 1-3 級" });
            sects.Add(new TSect { T1 = "2", T2 = "次鋒", T3 = "M", T4 = "-45kg", T5 = "-30,-33,-37,-41,-45", T6 = "次鋒限第 1-5 級" });
            sects.Add(new TSect { T1 = "3", T2 = "中堅", T3 = "M", T4 = "-50kg", T5 = "-30,-33,-37,-41,-45,-50", T6 = "中堅限第 1-6 級" });
            sects.Add(new TSect { T1 = "4", T2 = "副將", T3 = "M", T4 = "-60kg", T5 = "-30,-33,-37,-41,-45,-50,-55,-60", T6 = "副將限第 1-8 級" });
            sects.Add(new TSect { T1 = "5", T2 = "主將", T3 = "M", T4 = "-66kg", T5 = "-30,-33,-37,-41,-45,-50,-55,-60,-66", T6 = "主將限第 1-9 級" });
            return sects;
        }

        //國小女子組團體賽
        private List<TSect> GetSects_Weight5_E_W()
        {
            var sects = new List<TSect>();
            sects.Add(new TSect { T1 = "1", T2 = "先鋒", T3 = "W", T4 = "-37kg", T5 = "-30,-33,-37", T6 = "先鋒限第 1-3 級" });
            sects.Add(new TSect { T1 = "2", T2 = "次鋒", T3 = "W", T4 = "-45kg", T5 = "-30,-33,-37,-41,-45", T6 = "次鋒限第 1-5 級" });
            sects.Add(new TSect { T1 = "3", T2 = "中堅", T3 = "W", T4 = "-50kg", T5 = "-30,-33,-37,-41,-45,-50", T6 = "中堅限第 1-6 級" });
            sects.Add(new TSect { T1 = "4", T2 = "副將", T3 = "W", T4 = "-60kg", T5 = "-30,-33,-37,-41,-45,-50,-55,-60", T6 = "副將限第 1-8 級" });
            sects.Add(new TSect { T1 = "5", T2 = "主將", T3 = "W", T4 = "-66kg", T5 = "-30,-33,-37,-41,-45,-50,-55,-60,-66", T6 = "主將限第 1-9 級" });
            return sects;
        }

        private class TSect
        {
            /// <summary>
            /// in_sub_id
            /// </summary>
            public string T1 { get; set; }
            /// <summary>
            /// in_name
            /// </summary>
            public string T2 { get; set; }
            /// <summary>
            /// in_gender
            /// </summary>
            public string T3 { get; set; }
            /// <summary>
            /// in_weight
            /// </summary>
            public string T4 { get; set; }
            /// <summary>
            /// in_ranges
            /// </summary>
            public string T5 { get; set; }
            /// <summary>
            /// in_note
            /// </summary>
            public string T6 { get; set; }
        }


        private void Update(TConfig cfg, Item itmReturn)
        {
            string id = itmReturn.getProperty("sect_id", "");
            string property = itmReturn.getProperty("property", "");
            string value = itmReturn.getProperty("value", "");
            RunUpdate(cfg, id, property, value);
        }

        private void RunUpdate(TConfig cfg, string id, string in_property, string value)
        {
            string sql = "UPDATE IN_MEETING_PSECT SET " + in_property + " = N'" + value + "' WHERE id = '" + id + "'";

            Item itmSQL = cfg.inn.applySQL(sql);

            if (itmSQL.isError())
            {
                throw new Exception("更新失敗");
            }
        }

        private void Page(TConfig cfg, Item itmReturn)
        {
            Item itmMeeting = cfg.inn.applySQL("SELECT in_title FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            itmReturn.setProperty("in_title", itmMeeting.getProperty("in_title", ""));

            Item items = GetMSects(cfg);

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                item.setType("inn_sect");
                itmReturn.addRelationship(item);
            }

            if (cfg.program_id != "")
            {
                Item itmProgram = cfg.inn.applySQL("SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + cfg.program_id + "'");
                if (!itmProgram.isError() && itmProgram.getResult() != "")
                {
                    itmReturn.setProperty("in_team_battle", itmProgram.getProperty("in_team_battle", ""));
                    itmReturn.setProperty("in_team_weight_check", itmProgram.getProperty("in_team_weight_check", ""));
                }
            }

            //團體組別
            AppendProgramMenu(cfg, itmReturn);
            //團體戰比賽模式
            AppendTeamBattleMenu(cfg, itmReturn);
        }

        private void AppendProgramMenu(TConfig cfg, Item itmReturn)
        {
            string sql = @"
                SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK)
                WHERE in_meeting = '{#meeting_id}'
                AND in_l1 = N'團體組'
                AND ISNULL(in_team_count, 0) > 0
                ORDER BY in_sort_order, in_sub_event
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);
            Item items = cfg.inn.applySQL(sql);

            var itmEmpty = cfg.inn.newItem();
            itmEmpty.setType("inn_program");
            itmEmpty.setProperty("id", "");
            itmEmpty.setProperty("label", "請選擇");
            itmEmpty.setProperty("mode", "");
            itmReturn.addRelationship(itmEmpty);

            if (!items.isError() && items.getResult() != "")
            {
                int count = items.getItemCount();
                for (int i = 0; i < count; i++)
                {
                    var item = items.getItemByIndex(i);
                    string in_name2 = item.getProperty("in_name2", "");
                    string in_team_count = item.getProperty("in_team_count", "0");
                    string in_team_battle = item.getProperty("in_team_battle", "");
                    string label = in_name2 + " (" + in_team_count + ")";

                    item.setType("inn_program");
                    item.setProperty("label", label);
                    item.setProperty("mode", in_team_battle);
                    itmReturn.addRelationship(item);
                }
            }
        }

        private void AppendTeamBattleMenu(TConfig cfg, Item itmReturn)
        {
            string sql = @"
                SELECT t1.value, t1.label_zt AS 'label' FROM [VALUE] t1 WITH(NOLOCK)
				INNER JOIN [LIST] t2 WITH(NOLOCK) ON t2.id = t1.source_id
                WHERE t2.name = 'In_Team_Battle'
                ORDER BY t1.sort_order
            ";

            Item items = cfg.inn.applySQL(sql);

            var itmEmpty = cfg.inn.newItem();
            itmEmpty.setType("inn_battle");
            itmEmpty.setProperty("value", "");
            itmEmpty.setProperty("label", "請選擇");
            itmReturn.addRelationship(itmEmpty);

            if (!items.isError() && items.getResult() != "")
            {
                int count = items.getItemCount();
                for (int i = 0; i < count; i++)
                {
                    var item = items.getItemByIndex(i);
                    item.setType("inn_battle");
                    itmReturn.addRelationship(item);
                }
            }
        }

        private Item GetMSects(TConfig cfg)
        {
            string program_condition = cfg.program_id == ""
                ? ""
                : "AND t1.in_program = '" + cfg.program_id + "'";

            string sql = @"
                SELECT 
	                t2.id            AS 'pg_id'
	                , t2.in_name2    AS 'pg_name'
	                , t1.*
                FROM 
	                IN_MEETING_PSECT t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PROGRAM t2 WITH(NOLOCK)
	                ON t2.id = t1.in_program
                WHERE
	                t1.in_meeting = '{#meeting_id}'
	                {#program_condition}
                ORDER BY
                    t2.in_sort_order
	                , t2.in_sub_event
	                , t1.in_sub_id
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#program_condition}", program_condition);

            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public Innosoft.InnovatorHelper InnH { get; set; }

            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }

            public string meeting_id { get; set; }
            public string program_id { get; set; }
            public string scene { get; set; }
            public string fight_mode { get; set; }
            public string[] CharSet { get; set; }

            public string in_property { get; set; }
            public string ages { get; set; }

        }

        private class TWPkg
        {
            public string title { get; set; }
            public List<string> weights { get; set; }
        }

        private int GetIntVal(string value, int def = 0)
        {
            int result = def;
            Int32.TryParse(value, out result);
            return result;
        }
    }
}