﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Judo.Fight
{
    public class in_team_battle_fight_setting : Item
    {
        public in_team_battle_fight_setting(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
    目的: 單位出賽單設定
    日誌: 
        - 2022-11-03: 創建 (lina)
*/

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]" + "in_team_battle_fight_setting";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                strIdentityId = inn.getUserAliases(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                program_id = itmR.getProperty("pid", ""),
                event_id = itmR.getProperty("eid", ""),
                team_id = itmR.getProperty("tid", ""),
                scene = itmR.getProperty("scene", ""),
            };

            switch (cfg.scene)
            {
                case "org_change":
                    OrgChange(cfg, itmR);
                    break;

                case "event_change":
                    EventChange(cfg, itmR);
                    break;

                case "page":
                    Page(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void OrgChange(TConfig cfg, Item itmReturn)
        {
            //組別資訊
            AppendProgramInfo(cfg, itmReturn);

            AppendTable(cfg, itmReturn);
        }

        private void EventChange(TConfig cfg, Item itmReturn)
        {
            var items = GetProgramEventItems(cfg, cfg.event_id);
            var evts = MapProgramEventItems(cfg, items);
            if (evts != null && evts.Count == 1)
            {
                var evt = evts[0];
                var list = new List<string>();
                AppendEvetnOrg(cfg, evt, evt.Foot1, list);
                AppendEvetnOrg(cfg, evt, evt.Foot2, list);
                var data = string.Join("", list);
                itmReturn.setProperty("inn_orgs", data);
            }
        }

        //頁面
        private void Page(TConfig cfg, Item itmReturn)
        {
            //活動資訊
            AppendMeetingInfo(cfg, itmReturn);
            //組別資訊
            AppendProgramInfo(cfg, itmReturn);
            //場次資訊
            AppendEventInfo(cfg, itmReturn);
            //隊伍資訊
            AppendTeamInfo(cfg, itmReturn);
            //場次明細資訊
            AppendEventDetail(cfg, itmReturn);

            //組別選單
            AppendProgramMenu(cfg, itmReturn);
            //場次選單
            AppendEventMenu(cfg, itmReturn);

            if (cfg.team_id != "")
            {
                AppendTable(cfg, itmReturn);
            }
        }

        private void AppendTable(TConfig cfg, Item itmReturn)
        {
            var itmSects = GetSectItems(cfg);
            var sects = MapSectItems(cfg, itmSects);

            var itmPlayers = GetTeamItems(cfg);
            var corps = MapTeamItems(cfg, itmPlayers);

            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            head.Append("<thead>");
            head.Append("  <tr>");
            head.Append("    <th class='text-center' rowspan='1' colspan='1'>子場次</th>");
            head.Append("    <th class='text-center' rowspan='1' colspan='1'>組別</th>");
            head.Append("    <th class='text-center' rowspan='1' colspan='1'>量級</th>");
            head.Append("    <th class='text-center' rowspan='1' colspan='1'>勾選</th>");
            head.Append("    <th class='text-center' rowspan='1' colspan='1'>選手姓名</th>");
            head.Append("    <th class='text-center' rowspan='1' colspan='1'>範圍</th>");
            head.Append("  </tr>");
            head.Append("</thead>");

            body.Append("<tbody>");
            for (int i = 0; i < sects.Count; i++)
            {
                var sect = sects[i];
                var players = GetPlayers(cfg, sect, corps);

                var rs = players.Count;
                var mx = rs - 1;

                for (int j = 0; j < rs; j++)
                {
                    var player = players[j];
                    var sct = SectInfo(cfg, sect);
                    var name = NameInfo(cfg, sect, player);
                    var ranges = player.Value.getProperty("ranges", "");

                    if (j == 0)
                    {
                        body.Append("  <tr>");
                        body.Append("      <td rowspan='" + rs + "' colspan='1'>" + sect.Value.getProperty("in_tree_no", "") + "</td>");
                        body.Append("      <td rowspan='" + rs + "' colspan='1'>" + sct + "</td>");
                        body.Append("      <td rowspan='" + rs + "' colspan='1'>" + sect.Value.getProperty("sect_weight", "") + "</td>");
                        body.Append("      <td rowspan='1' colspan='1'>" + CheckBoxCtrl(cfg, sect, player) + "</td>");
                        body.Append("      <td rowspan='1' colspan='1'>" + name + "</td>");
                        body.Append("      <td rowspan='1' colspan='1' class='col-md-4'>" + ranges + "</td>");
                        body.Append("  </tr>");
                    }
                    else
                    {
                        if (j == mx)
                        {
                            body.Append("  <tr class='tree_row'>");
                        }
                        else
                        {
                            body.Append("  <tr>");
                        }
                        body.Append("      <td rowspan='1' colspan='1'>" + CheckBoxCtrl(cfg, sect, player) + "</td>");
                        body.Append("      <td rowspan='1' colspan='1'>" + name + "</td>");
                        body.Append("      <td rowspan='1' colspan='1' class='col-md-4'>" + ranges + "</td>");
                        body.Append("  </tr>");
                    }
                }
            }
            body.Append("</tbody>");

            string tb_id = "data_table";

            StringBuilder builder = new StringBuilder();

            builder.Append("<table id='" + tb_id + "'"
                + " class='table table-hover table-bordered table-rwd rwd'"
                + " style='background-color: #fff;' "
                + " data-toggle='table' "
                + " data-search='false'"
                + " data-search-align='left'"
                + ">");

            builder.Append(head);
            builder.Append(body);
            builder.Append("</table>");

            itmReturn.setProperty("inn_table", builder.ToString());
        }

        private string SectInfo(TConfig cfg, TSect sect)
        {
            return "<label class='inn_sect'>"
                + sect.sect_name
                + "</label>"
                ;
        }

        private string NameInfo(TConfig cfg, TSect sect, TPlayer player)
        {
            string name = player.Value.getProperty("show_name", "");
            string sno = player.Value.getProperty("in_sno", "");
            string weight = player.Value.getProperty("in_weight", "");
            string lv = player.Value.getProperty("in_team_weight_n2", "");
            string message = player.Value.getProperty("in_weight_message", "");

            if (sno == "" || sno == "-")
            {

            }
            else if (message != "")
            {
                name += message;
            }
            else
            {
                if (!cfg.is_special_sect)
                {
                    name += "(" + weight + ")";
                }
            }

            string result = "<label onclick='LinkCheckBox(this)'>"
                + name
                + "</label>";

            return result;
        }

        private string CheckBoxCtrl(TConfig cfg, TSect sect, TPlayer player)
        {
            var sub = sect.sect_id;
            var did = sect.Value.getProperty("did", "");
            var old_name = sect.Value.getProperty("old_name", "");
            var old_sno = sect.Value.getProperty("old_sno", "");

            var muid = player.Value.getProperty("muid", "");
            var weight = player.Value.getProperty("in_weight", "");
            var new_name = player.Value.getProperty("in_name", "");
            var new_sno = player.Value.getProperty("in_sno", "");

            if (new_sno == "-")
            {
                return "";
            }

            var nm = "sect_" + sect.sect_id;
            var state = old_sno == new_sno ? "checked" : "";
            if (old_name != "沒有選手" && old_sno == "") state = "";

            var ctrl = "<label class='checkbox'>"
                + "<input type='checkbox' name='" + nm + "' class='checkbox inn_player'"
                + " " + state
                + " data-did='" + did + "'"
                + " data-muid='" + muid + "'"
                + " data-onm='" + old_name + "'"
                + " data-nnm='" + new_name + "'"
                + " data-sub='" + sub + "'"
                + " data-weight='" + weight + "'"
                + " data-sno='" + new_sno + "'"
                + " data-sect='" + sect.sect_name + "'"
                + " onclick='SelectedPlayer(this)' />"
                + "</label>";

            return ctrl;
        }

        private List<TPlayer> GetPlayers(TConfig cfg, TSect sect, List<TSect> corps)
        {
            var corp = corps.Find(x => x.sect_id == sect.sect_id);
            if (corp == null) corp = new TSect();

            var players = corp.Players;
            if (players == null) players = new List<TPlayer>();

            var list = players;
            if (!cfg.is_special_sect)
            {
                list = players.OrderBy(x => x.weight).ToList();
            }
            foreach (var p in list)
            {
                var in_name = p.Value.getProperty("in_name", "");
                var in_sub_weight = p.Value.getProperty("in_sub_weight", "");
                p.Value.setProperty("show_name", in_name);
                p.Value.setProperty("ranges", GetPlayerSect(cfg, sect, in_sub_weight));
            }


            //沒有選手
            var itmNoplayer = cfg.inn.newItem();
            itmNoplayer.setProperty("in_name", "沒有選手");
            itmNoplayer.setProperty("show_name", "<label style='color: red'>沒有選手</label>");
            itmNoplayer.setProperty("ranges", "-");

            //空白
            var itmEmpty = cfg.inn.newItem();
            itmEmpty.setProperty("in_name", "-");
            itmEmpty.setProperty("in_sno", "-");
            itmEmpty.setProperty("show_name", "-");
            itmEmpty.setProperty("ranges", "-");

            list.Insert(0, new TPlayer { Value = itmNoplayer, weight = 0m });

            var d = cfg.max_player_count - list.Count;
            for (int i = 0; i < d; i++)
            {
                list.Add(new TPlayer { Value = itmEmpty, weight = 0m });
            }

            return list;
        }

        private string GetPlayerSect(TConfig cfg, TSect sect, string weight)
        {
            if (sect.ranges == null || sect.ranges.Length == 0)
            {
                return "-";
            }

            List<string> list = new List<string>();

            foreach (var v in sect.ranges)
            {
                if (v == weight)
                {
                    list.Add("■" + v);
                }
                else
                {
                    list.Add("□" + v);
                }
            }

            return string.Join(",", list);
        }

        //活動資訊
        private void AppendMeetingInfo(TConfig cfg, Item itmReturn)
        {
            string sql = "SELECT id, in_title, in_uniform_color FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'";
            cfg.itmMeeting = cfg.inn.applySQL(sql);

            if (cfg.itmMeeting.isError() || cfg.itmMeeting.getResult() == "")
            {
                cfg.itmMeeting = cfg.inn.newItem();
            }

            itmReturn.setProperty("in_title", cfg.itmMeeting.getProperty("in_title", ""));
            itmReturn.setProperty("in_uniform_color", cfg.itmMeeting.getProperty("in_uniform_color", ""));
        }

        //組別資訊
        private void AppendProgramInfo(TConfig cfg, Item itmReturn)
        {
            string sql = "SELECT id, in_sub_event, in_name2 FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + cfg.program_id + "'";
            cfg.itmProgram = cfg.inn.applySQL(sql);

            if (cfg.itmProgram.isError() || cfg.itmProgram.getResult() == "")
            {
                cfg.itmProgram = cfg.inn.newItem();
            }
        }

        //場次資訊
        private void AppendEventInfo(TConfig cfg, Item itmReturn)
        {
            string sql = "SELECT id, in_tree_id, in_tree_no FROM IN_MEETING_PEVENT WITH(NOLOCK) WHERE id = '" + cfg.event_id + "'";
            cfg.itmEvt = cfg.inn.applySQL(sql);

            if (cfg.itmEvt.isError() || cfg.itmEvt.getResult() == "")
            {
                cfg.itmEvt = cfg.inn.newItem();
            }
        }

        //隊伍資訊
        private void AppendTeamInfo(TConfig cfg, Item itmReturn)
        {
            string sql = "SELECT id, in_sign_no FROM IN_MEETING_PTEAM WITH(NOLOCK) WHERE id = '" + cfg.team_id + "'";
            cfg.itmTeam = cfg.inn.applySQL(sql);

            if (cfg.itmTeam.isError() || cfg.itmTeam.getResult() == "")
            {
                cfg.itmTeam = cfg.inn.newItem();
            }
        }

        //場次明細資訊
        private void AppendEventDetail(TConfig cfg, Item itmReturn)
        {
            string sql = @"
                SELECT
	                id
	                , in_sign_foot
	                , in_sign_no
                FROM
	                IN_MEETING_PEVENT_DETAIL WITH(NOLOCK)
                WHERE
	                source_id = '{#event_id}'
	                AND in_sign_no = '{#sign_no}'
            ";

            sql = sql.Replace("{#event_id}", cfg.event_id)
                .Replace("{#sign_no}", cfg.itmTeam.getProperty("in_sign_no", ""));

            cfg.itmDetail = cfg.inn.applySQL(sql);

            if (cfg.itmDetail.isError() || cfg.itmDetail.getResult() == "")
            {
                cfg.itmDetail = cfg.inn.newItem();
            }

            string detail_id = cfg.itmDetail.getProperty("id", "");
            string in_sign_foot = cfg.itmDetail.getProperty("in_sign_foot", "");

            itmReturn.setProperty("did", detail_id);
            itmReturn.setProperty("team_sign_foot", in_sign_foot);

            cfg.detail_id = detail_id;
            cfg.team_sign_foot = in_sign_foot;
        }

        //附加組別選單
        private void AppendProgramMenu(TConfig cfg, Item itmReturn)
        {
            var list = GetProgramDictionary(cfg);

            var builder = new StringBuilder();
            builder.Append("<select id='program_menu' class='form-control' style='width: 240px;' onchange='ProgramChange()'>");
            foreach (var kv in list)
            {
                var day = kv.Key;
                builder.Append("<optgroup label='" + day + "'>");
                foreach (var item in kv.Value)
                {
                    string id = item.getProperty("id", "");
                    string in_name2 = item.getProperty("in_name2", "");
                    string in_team_count = item.getProperty("in_team_count", "0");
                    string text = in_name2 + " (" + in_team_count + ")";

                    builder.Append("<option data-day='" + day + "' value='" + id + "'>" + text + "</option>");
                }
                builder.Append("</optgroup>");
            }
            builder.Append("</select>");

            var contents = builder.ToString();

            itmReturn.setProperty("program_menu", contents);
        }

        //附加組別選單
        private Dictionary<string, List<Item>> GetProgramDictionary(TConfig cfg)
        {
            var list = new Dictionary<string, List<Item>>();

            string sql = @"
                SELECT 
	                id
                    , in_name2
                    , in_fight_day
                    , in_team_count
                FROM 
	                IN_MEETING_PROGRAM WITH(NOLOCK) 
                WHERE 
	                in_meeting = '{#meeting_id}'
	                AND in_l1 = N'團體組'
                ORDER BY 
                    in_fight_day
	                , in_sort_order
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);
            Item items = cfg.inn.applySQL(sql);
            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string key = item.getProperty("in_fight_day", "");
                if (!list.ContainsKey(key))
                {
                    list.Add(key, new List<Item>());
                }
                list[key].Add(item);
            }

            return list;
        }

        //附加場次選單
        private void AppendEventMenu(TConfig cfg, Item itmReturn)
        {
            var items = GetProgramEventItems(cfg, "");
            var list = MapProgramEventItems(cfg, items);

            var builder = new StringBuilder();
            builder.Append("<select id='event_menu' class='form-control' style='width: 170px' onchange='EventChange()'>");
            builder.Append("<option data-orgs='' value=''>請選擇</option>");
            foreach (var row in list)
            {
                builder.Append("<option data-orgs='" + EventOrgs(cfg, row) + "' value='" + row.id + "'>" + EventText(cfg, row) + "</option>");
            }
            builder.Append("</select>");

            var contents = builder.ToString();

            itmReturn.setProperty("event_menu", contents);
        }

        private string EventOrgs(TConfig cfg, TEvt evt)
        {
            var list = new List<string>();
            AppendEvetnOrg(cfg, evt, evt.Foot1, list);
            AppendEvetnOrg(cfg, evt, evt.Foot2, list);
            return string.Join("", list);
        }

        private void AppendEvetnOrg(TConfig cfg, TEvt evt, Item item, List<string> list)
        {
            string team_id = item.getProperty("team_id");
            string in_stuff_b1 = item.getProperty("in_stuff_b1");
            string in_short_org = item.getProperty("in_short_org");
            string in_sign_foot = item.getProperty("in_sign_foot");
            string in_sign_no = item.getProperty("in_sign_no");

            if (team_id == "") return;

            string data = "@" + in_stuff_b1
                + "|" + in_short_org
                + "|" + team_id
                + "|" + in_sign_foot
                + "|" + in_sign_no
                ;

            list.Add(data);
        }

        private string EventText(TConfig cfg, TEvt evt)
        {
            string in_tree_no = evt.Value.getProperty("in_tree_no", "");
            string in_round_serial = evt.Value.getProperty("in_round_serial", "");
            string site_name = evt.Value.getProperty("site_name", "");

            string text = in_round_serial == "" || in_round_serial == "0"
                ? in_tree_no
                : in_tree_no + "  (第" + in_round_serial + "輪)";

            text += "  " + site_name;

            return text;
        }
        private List<TEvt> MapProgramEventItems(TConfig cfg, Item items)
        {
            var result = new List<TEvt>();
            var itmEmpty = cfg.inn.newItem();
            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string id = item.getProperty("id", "");
                string in_sign_foot = item.getProperty("in_sign_foot", "");
                var row = result.Find(x => x.id == id);
                if (row == null)
                {
                    row = new TEvt
                    {
                        id = id,
                        Value = item,
                        Foot1 = itmEmpty,
                        Foot2 = itmEmpty,
                    };
                    result.Add(row);
                }

                if (in_sign_foot == "1")
                {
                    row.Foot1 = item;
                }
                if (in_sign_foot == "2")
                {
                    row.Foot2 = item;
                }
            }
            return result;
        }

        private Item GetProgramEventItems(TConfig cfg, string event_id)
        {
            string evt_cond = event_id == ""
                ? ""
                : "AND t1.id = '" + event_id + "'";

            string sql = @"
                SELECT 
	                t1.id
	                , t1.in_tree_id
	                , t1.in_tree_no
	                , t1.in_round_serial
	                , t2.in_sign_foot
	                , t2.in_sign_no
	                , t3.id       AS 'team_id'
	                , t3.in_stuff_b1
	                , t3.in_short_org
	                , t3.in_team
	                , t11.id      AS 'site_id'
	                , t11.in_code AS 'site_code'
	                , t11.in_name AS 'site_name'
                FROM
	                IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                LEFT OUTER JOIN
	                VU_MEETING_PTEAM t3 WITH(NOLOCK)
	                ON t3.source_id = t1.source_id
	                AND t3.in_sign_no = t2.in_sign_no
	                AND ISNULL(t3.in_type, '') IN ('', 'p')
                INNER JOIN
	                IN_MEETING_SITE t11 WITH(NOLOCK)
	                ON t11.id = t1.in_site
                WHERE
	                t1.source_id = '{#program_id}'
	                AND ISNULL(t1.in_type, '') IN ('', 'p')
	                AND ISNULL(t1.in_tree_no, 0) > 0
	                {#evt_cond}
                ORDER BY
	                t1.in_tree_no
	                , t2.in_sign_foot
	                , t2.in_sign_no
            ";

            sql = sql.Replace("{#program_id}", cfg.program_id)
                .Replace("{#evt_cond}", evt_cond);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        private List<TSect> MapSectItems(TConfig cfg, Item items)
        {
            var result = new List<TSect>();
            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var sect_id = GetIntVal(item.getProperty("sect_id", ""));
                var sect_name = item.getProperty("sect_name", "");
                var sect_ranges = item.getProperty("sect_ranges", "");

                var row = result.Find(x => x.sect_id == sect_id);
                if (row == null)
                {
                    row = new TSect
                    {
                        sect_id = sect_id,
                        sect_name = sect_name,
                        ranges = GetSectRanges(cfg, sect_ranges),
                        Value = item,
                    };
                    result.Add(row);
                }
            }
            return result;
        }

        private string[] GetSectRanges(TConfig cfg, string in_ranges)
        {
            if (in_ranges == "") return null;
            var values = in_ranges;
            return values.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
        }

        private Item GetSectItems(TConfig cfg)
        {
            string sql = @"
                SELECT 
	                t2.in_sub_id   AS 'sect_id'
	                , t2.in_name   AS 'sect_name'
	                , t2.in_weight AS 'sect_weight'
	                , t2.in_ranges AS 'sect_ranges'
	                , t1.id
	                , t1.in_tree_id
	                , t1.in_tree_no
                    , t3.id             AS 'did'
                    , t3.in_player_name AS 'old_name'
                    , t3.in_player_sno  AS 'old_sno'
                FROM 
	                IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PSECT t2 WITH(NOLOCK)
	                ON t2.in_program = t1.source_id
	                AND t2.in_sub_id = t1.in_sub_id
	            INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t3 WITH(NOLOCK)
                    ON t3.source_id = t1.id
                WHERE 
	                t1.in_parent = '{#event_id}'
	                AND t3.in_parent = '{#detail_id}'
                ORDER BY 
	                ISNULL(t1.in_team_serial, t1.in_sub_id)
            ";

            sql = sql.Replace("{#event_id}", cfg.event_id)
                .Replace("{#detail_id}", cfg.detail_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        private List<TSect> MapTeamItems(TConfig cfg, Item items)
        {
            var result = new List<TSect>();
            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var sect_id = GetIntVal(item.getProperty("sect_id", ""));
                var in_weight = item.getProperty("in_weight", "0");

                var row = result.Find(x => x.sect_id == sect_id);
                if (row == null)
                {
                    row = new TSect
                    {
                        sect_id = sect_id,
                        Players = new List<TPlayer>(),
                    };
                    result.Add(row);
                }

                decimal w = cfg.is_special_sect
                    ? 0m
                    : GetDcmVal(in_weight);

                row.Players.Add(new TPlayer
                {
                    weight = w,
                    Value = item
                });
            }
            return result;
        }

        private Item GetTeamItems(TConfig cfg)
        {
            string sql = @"
                SELECT
	                t1.id
	                , t1.in_sub_id    AS 'sect_id'
	                , t1.in_sub_sect  AS 'sect_name'
                    , t1.in_sub_weight
                    , t1.in_weight_message
	                , t2.id           AS 'muid'
	                , t2.in_name
	                , t2.in_sno
	                , t2.in_gender
	                , ISNULL(t2.in_weight, 0) AS 'in_weight'
	                , REPLACE(REPLACE(t2.in_team_weight_n2, N'第', ''), N'級', '') AS 'in_team_weight_n2'
	                , t2.in_team_weight_n3
                FROM
	                IN_MEETING_PTEAM t1 WITH(NOLOCK)
                LEFT OUTER JOIN
	                IN_MEETING_USER t2 WITH(NOLOCK)
	                ON t2.id = t1.in_muser
                WHERE
	                t1.in_parent = '{#team_id}'
                ORDER BY
	                t1.in_sub_id
	                , t2.in_sno
            ";

            sql = sql.Replace("{#team_id}", cfg.team_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        private class TSect
        {
            public int sect_id { get; set; }
            public string sect_name { get; set; }
            public Item Value { get; set; }
            public List<TPlayer> Players { get; set; }
            public string[] ranges { get; set; }
        }

        private class TPlayer
        {
            public decimal weight { get; set; }
            public Item Value { get; set; }
        }


        private class TEvt
        {
            public string id { get; set; }
            public Item Value { get; set; }
            public Item Foot1 { get; set; }
            public Item Foot2 { get; set; }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }

            public Item itmMeeting { get; set; }
            public Item itmProgram { get; set; }
            public Item itmEvt { get; set; }
            public Item itmTeam { get; set; }
            public Item itmDetail { get; set; }

            public string meeting_id { get; set; }
            public string program_id { get; set; }
            public string event_id { get; set; }
            public string detail_id { get; set; }
            public string team_id { get; set; }
            public string scene { get; set; }

            public string pg_name2 { get; set; }
            public string in_sub_event { get; set; }
            public int sub_event { get; set; }
            public int max_player_count { get; set; }
            public bool is_special_sect { get; set; }

            public string team_sign_foot { get; set; }
            public string team_sign_no { get; set; }

        }

        private int GetMaxPlayerCount(int sub_event)
        {
            switch (sub_event)
            {
                case 3: return 5;
                case 5: return 7;
                case 6: return 5;
                default: return 5;
            }
        }

        private decimal GetDcmVal(string value, decimal def = 0)
        {
            decimal result = def;
            decimal.TryParse(value, out result);
            return result;
        }

        private int GetIntVal(string value, int def = 0)
        {
            if (value == null || value == "") return 0;

            int result = def;
            Int32.TryParse(value, out result);
            return result;
        }
    }
}