﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Judo.Fight
{
    public class in_team_battle_fight_xlsx_site : Item
    {
        public in_team_battle_fight_xlsx_site(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 團體賽-出賽單(New)
                日誌: 
                    - 2022-12-16: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]" + "in_team_battle_fight_xlsx_site";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                strIdentityId = inn.getUserAliases(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                file_extension = itmR.getProperty("file_extension", ""),
                scene = itmR.getProperty("scene", ""),
            };

            if (cfg.file_extension == "")
            {
                cfg.file_extension = "xlsx";
            }

            //賽事資訊
            AppendMeetingInfo(cfg, itmR);

            //XLSX 資訊
            AppendXlsxInfo(cfg, itmR);

            //組別資訊
            var itmPrograms = GetProgramItems(cfg);

            return itmR;
        }

        private List<TProgram> MapPrograms(TConfig cfg, Item itmPrograms)
        {
            var result = new List<TProgram>();

            var count = itmPrograms.getItemCount();

            for (var i = 0; i < count; i++)
            {
                var itmProgram = itmPrograms.getItemByIndex(i);
                var row = new TProgram
                {
                    item = itmProgram,
                    in_name2 = itmProgram.getProperty("in_name2", ""),
                    in_short_name = itmProgram.getProperty("in_short_name", ""),
                    in_weight = itmProgram.getProperty("in_weight", ""),
                    in_l1 = itmProgram.getProperty("in_l1", ""),
                    in_l2 = itmProgram.getProperty("in_l2", ""),
                    in_l3 = itmProgram.getProperty("in_l3", ""),
                    in_battle_type = itmProgram.getProperty("in_battle_type", ""),
                    in_sub_event = itmProgram.getProperty("in_sub_event", ""),
                    in_sub_event = itmProgram.getProperty("in_sub_event", ""),
                };
            }
            return result;
        }

        private class TProgram
        {
            public Item item { get; set; }

            public List<TSect> sects { get; set; }
            public Item itmParents { get; set; }
            public Item itmChildren { get; set; }
            public Item itmTeams { get; set; }

            public string in_name2 { get; set; }
            public string in_short_name { get; set; }
            public string in_weight { get; set; }
            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }
            public string in_battle_type { get; set; }
            public string in_sub_event { get; set; }

            public bool is_robin { get; set; }
            public int sub_event { get; set; }
            public int sect_start { get; set; }
            public int max_rcount { get; set; }

            public bool is_special_sect { get; set; }
        }

        private Item GetProgramItems(TConfig cfg)
        {
            string sql = @"
                SELECT
	                t1.id
	                , t1.in_name
	                , t1.in_name2
	                , t1.in_short_name
	                , t1.in_weight
	                , t1.in_l1
	                , t1.in_l2
	                , t1.in_l3
	                , t1.in_battle_type
	                , t1.in_team_count
	                , t1.in_rank_count
	                , t1.in_sub_event
	                , t1.in_sect_start
                FROM
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                WHERE
	                t1.in_meeting = '{#meeting_id}'
	                AND t1.in_fight_day = '{#fight_day}'
	                AND t1.in_l1 = N'團體組'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#fight_day}", cfg.fight_day);

            Item items = cfg.inn.applySQL(sql);

            if (items.isError() || items.getResult() == "")
            {
                throw new Exception("組別資料錯誤資料錯誤");
            }

            return items;
        }

        private void AppendMeetingInfo(TConfig cfg, Item itmReturn)
        {
            string sql = "SELECT id, in_title FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'";
            cfg.itmMeeting = cfg.inn.applySQL(sql);
            if (cfg.itmMeeting.isError() || cfg.itmMeeting.getResult() == "")
            {
                throw new Exception("賽事資料錯誤");
            }

            cfg.mt_title = cfg.itmMeeting.getProperty("in_title", "");
        }

        private void AppendXlsxInfo(TConfig cfg, Item itmReturn)
        {
            cfg.itmXlsx = cfg.inn.applyMethod("In_Variable_File", "<in_name>team_fight</in_name>");
            if (cfg.itmXlsx.isError() || cfg.itmXlsx.getResult() == "")
            {
                throw new Exception("賽程 XLSX 資料錯誤");
            }

            cfg.template_path = cfg.itmXlsx.getProperty("template_path", "");
            cfg.export_path = cfg.itmXlsx.getProperty("export_path", "");
            if (cfg.export_path == "")
            {
                throw new Exception("賽程 XLSX export path 錯誤");
            }
        }

        private class TRow
        {
            public string id { get; set; }
            public Item value { get; set; }
            public TOrg foot1 { get; set; }
            public TOrg foot2 { get; set; }
            public List<TRow> Children { get; set; }

            public int no { get; set; }
            public decimal weight { get; set; }
        }

        private class TOrg
        {
            public int sign_no { get; set; }

            public string org_code { get; set; }
            public string org_name { get; set; }
            public string org_short { get; set; }
            public string org_show { get; set; }

            public string team { get; set; }
            public string team_status { get; set; }

            public string player_name { get; set; }
            public string player_names { get; set; }
            public string player_sno { get; set; }
            public string player_status { get; set; }

            public Item value { get; set; }
        }

        private class TSect
        {
            public int sub_id { get; set; }
            public string name { get; set; }
            public string gender { get; set; }
            public string weight { get; set; }

            public Item Value { get; set; }
        }

        private class TSectRow
        {
            public string sub_tree_no { get; set; }

            public int id { get; set; }
            public string name { get; set; }
            public string weight { get; set; }
            public string ranges { get; set; }

            public List<TRow> players { get; set; }
        }

        private class TSectPlayer
        {
            public string name { get; set; }
            public string ranges { get; set; }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }

            public string meeting_id { get; set; }
            public string fight_day { get; set; }
            public string file_extension { get; set; }
            public string scene { get; set; }

            public Item itmMeeting { get; set; }
            public Item itmXlsx { get; set; }

            public string mt_title { get; set; }

            public string template_path { get; set; }
            public string export_path { get; set; }
        }

        private int GetMaxPlayerCount(int sub_event)
        {
            switch (sub_event)
            {
                case 3: return 5;
                case 5: return 7;
                case 6: return 5;
                default: return 5;
            }
        }

        private decimal GetDcmVal(string value, decimal def = 0)
        {
            decimal result = def;
            decimal.TryParse(value, out result);
            return result;
        }

        private int GetIntVal(string value, int def = 0)
        {
            if (value == null || value == "") return 0;

            int result = def;
            Int32.TryParse(value, out result);
            return result;
        }
    }
}