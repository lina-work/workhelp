﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Judo.Fight.Team
{
    public class in_team_battle_weight_match : Item
    {
        public in_team_battle_weight_match(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 使用個人組量級匹配鋒將位
                日誌: 
                    - 2024-05-17: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "in_team_battle_weight_match";

            var itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                meeting_id = itmR.getProperty("meeting_id", ""),
                muid = itmR.getProperty("muid", ""),
                scene = itmR.getProperty("scene", ""),
            };

            switch (cfg.scene)
            {
                case "run":
                    Run(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void Run(TConfig cfg, Item itmReturn)
        {
            var subMap = MapSubList(cfg);

            var items = GetMeetingUserItem(cfg);
            var count = items.getItemCount();
            InnSport.Core.Logging.TLog.Watch(message: "共 " + count + " 筆");

            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var muid = item.getProperty("id", "");
                var in_name = item.getProperty("in_name", "");
                var in_current_org = item.getProperty("in_current_org", "");
                var in_l1 = item.getProperty("in_l1", "");
                var in_l2 = item.getProperty("in_l2", "");
                var in_l3 = item.getProperty("in_l3", "");
                var in_index = item.getProperty("in_index", "");
                var in_team_n3 = item.getProperty("in_team_n3", "");
                var in_creator_sno = item.getProperty("in_creator_sno", "").ToUpper();
                var key = string.Join("-", new List<string> { in_l1, in_l2, in_l3, in_index, in_creator_sno });
                var key2 = string.Join("-", new List<string> { in_l1, in_l2, in_l3, in_index, in_current_org, in_name });

                if (in_team_n3 == "") continue;

                in_team_n3 = in_team_n3.Replace("Kg", "");

                var itmParent = FindParent(cfg, key);
                if (itmParent.isError() || itmParent.getResult() == "")
                {
                    throw new Exception("組別隊伍資料異常: " + key);
                }

                var program_id = itmParent.getProperty("source_id", "");
                if (!subMap.ContainsKey(program_id))
                {
                    throw new Exception("團體賽量級查無匹配表: " + key + "-" + program_id);
                }

                var subs = subMap[program_id];

                var selectors = subs.FindAll(x => x.ranges.Contains(in_team_n3));
                if (!selectors.Any()) continue;

                var sub = selectors.First();
                InnSport.Core.Logging.TLog.Watch(message: (i + 1) + ". " + key2);
                AppendSubTeam(cfg, item, itmParent, sub, in_team_n3);
            }
        }

        private void AppendSubTeam(TConfig cfg, Item itmMUser, Item itmParent, TSub sub, string in_team_n3)
        {
            Item itmData = cfg.inn.newItem("In_Meeting_PTeam");
            itmData.setProperty("meeting_id", cfg.meeting_id);
            itmData.setProperty("program_id", itmParent.getProperty("source_id", ""));
            itmData.setProperty("in_index", itmParent.getProperty("in_index", ""));
            itmData.setProperty("muid", itmMUser.getProperty("id", ""));
            itmData.setProperty("pid", itmParent.getProperty("id", ""));
            itmData.setProperty("cid", "");
            itmData.setProperty("sub", sub.in_sub_id);
            itmData.setProperty("name", sub.in_name);
            itmData.setProperty("weight", in_team_n3);
            itmData.setProperty("need_remove", "0");
            itmData.setProperty("scene", "update");
            itmData.setProperty("sub_scene", "");
            itmData.apply("in_team_battle_register_org");
        }

        private Dictionary<string, List<TSub>> MapSubList(TConfig cfg)
        {
            var map = new Dictionary<string, List<TSub>>();
            var items = FindMeetingSectItems(cfg);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var row = new TSub
                {
                    program_id = item.getProperty("in_program", ""),
                    in_sub_id = item.getProperty("in_sub_id", ""),
                    in_name = item.getProperty("in_name", ""),
                    in_ranges = item.getProperty("in_ranges", ""),
                };
                row.ranges = row.in_ranges.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                if (map.ContainsKey(row.program_id))
                {
                    map[row.program_id].Add(row);
                }
                else
                {
                    var list = new List<TSub> { row };
                    map.Add(row.program_id, list);
                }
            }
            return map;
        }

        private class TSub
        {
            public string program_id { get; set; }
            public string in_sub_id { get; set; }
            public string in_name { get; set; }
            public string in_ranges { get; set; }
            public string[] ranges { get; set; }
        }

        private Item FindMeetingSectItems(TConfig cfg)
        {
            var sql = @"
                SELECT 
	                * 
                FROM 
	                IN_MEETING_PSECT WITH(NOLOCK)
                WHERE
                    in_meeting = '{#meeting_id}'
                ORDER BY
	                in_program
	                , in_sub_id
            ";
            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);
            return cfg.inn.applySQL(sql);
        }

        private Item FindParent(TConfig cfg, string in_team_key)
        {
            var sql = @"
                SELECT TOP 1
	                *
                FROM 
	                IN_MEETING_PTEAM WITH(NOLOCK)
                WHERE
	                in_meeting = '{#meeting_id}'
	                AND in_team_key = N'{#in_team_key}'
	                AND ISNULL(in_type, '') IN ('', 'p')
            ";
            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_team_key}", in_team_key);
            return cfg.inn.applySQL(sql);
        }

        //private Item FindChildren(TConfig cfg, string in_team_key)
        //{
        //    var sql = @"
        //        SELECT TOP 1
        //         *
        //        FROM 
        //         IN_MEETING_PTEAM WITH(NOLOCK)
        //        WHERE
        //         in_meeting = '{#meeting_id}'
        //         AND in_team_key = N'{#in_team_key}'
        //         AND ISNULL(in_type, '') IN ('', 'p')
        //    ";
        //    sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
        //        .Replace("{#in_team_key}", in_team_key);
        //    return cfg.inn.applySQL(sql);
        //}

        private Item GetMeetingUserItem(TConfig cfg)
        {
            var cond = cfg.muid == "" ? "" : "AND id = '" + cfg.muid + "'";

            var sql = @"
                SELECT 
	                * 
                FROM 
	                VU_MEETING_TEAM_LIST 
                WHERE
	                source_id = '{#meeting_id}'
	                {#cond}
                ORDER BY 
	                in_l1
	                , in_l2
	                , in_l3
	                , in_stuff_b1
	                , in_current_org
	                , in_team
	                , in_index
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#cond}", cond);

            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string meeting_id { get; set; }
            public string muid { get; set; }
            public string scene { get; set; }
        }
    }
}