﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Judo.Fight
{
    public class in_team_battle_fight : Item
    {
        public in_team_battle_fight(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 團體戰-出賽、紀錄表
                日期: 
                    - 2022-08-23 紀錄表依競賽組要求改為動態輪次 (Lina)
                    - 2021-10-13 創建 (Lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]in_team_battle_fight";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, this.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                meeting_id = itmR.getProperty("meeting_id", ""),
                program_id = itmR.getProperty("program_id", ""),
                scene = itmR.getProperty("scene", ""),
            };

            switch (cfg.scene)
            {
                case "represent_save":
                    Represent(cfg, itmR, RepresentSave);
                    break;

                case "represent_modal":
                    Represent(cfg, itmR, RepresentModal);
                    break;

                case "save":
                    Save(cfg, itmR);
                    break;

                case "record_xlsx":
                    RecordXlsx(cfg, itmR);
                    break;

                default:
                    BattlePage(cfg, itmR);
                    break;
            }

            return itmR;
        }

        #region 代表戰

        private void Represent(TConfig cfg, Item itmReturn, Action<TConfig, TRepre, Item> execute)
        {
            string in_parent = itmReturn.getProperty("eid", "");

            string sql = "";

            sql = "SELECT * FROM IN_MEETING_PEVENT WHERE id = '" + in_parent + "'";
            Item itmParent = cfg.inn.applySQL(sql);
            if (itmParent.isError() || itmParent.getResult() == "")
            {
                throw new Exception("查無場次資料");
            }

            string program_id = itmParent.getProperty("source_id", "");
            sql = "SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + program_id + "'";
            Item itmProgram = cfg.inn.applySQL(sql);
            if (itmProgram.isError() || itmProgram.getResult() == "")
            {
                throw new Exception("查無組別資料");
            }

            int sub_event = GetIntVal(itmProgram.getProperty("in_sub_event", "0"));

            var entity = new TRepre
            {
                in_parent = in_parent,

                itmParent = itmParent,
                itmProgram = itmProgram,

                sub_event = sub_event,
                next_event = sub_event + 1
            };

            entity.program_id = entity.itmProgram.getProperty("id", "");

            execute(cfg, entity, itmReturn);

            itmReturn.setProperty("meeting_id", cfg.meeting_id);
            itmReturn.setProperty("program_id", entity.program_id);
            itmReturn.setProperty("event_id", in_parent);
        }

        private void RepresentModal(TConfig cfg, TRepre entity, Item itmReturn)
        {
            Item itmEmpty = cfg.inn.newItem();
            itmEmpty.setType("inn_sect");
            itmEmpty.setProperty("value", "");
            itmEmpty.setProperty("label", "請選擇");
            itmReturn.addRelationship(itmEmpty);

            string sql = "SELECT * FROM IN_MEETING_PSECT WITH(NOLOCK)"
                + " WHERE in_program = '" + entity.program_id + "'"
                + " AND in_name <> N'代表戰'"
                + " ORDER BY in_sub_id";

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                item.setType("inn_sect");
                item.setProperty("value", item.getProperty("in_sub_id", ""));
                item.setProperty("label", item.getProperty("in_name", ""));
                itmReturn.addRelationship(item);
            }
        }

        //代表戰量級設定
        private void RepresentSave(TConfig cfg, TRepre entity, Item itmReturn)
        {
            string sql = "";
            string in_sub_id = itmReturn.getProperty("in_sub_id", "");

            //要複製的子場次
            sql = "SELECT id, in_sub_sect FROM IN_MEETING_PEVENT WITH(NOLOCK)"
                + " WHERE in_parent = '" + entity.in_parent + "'"
                + " AND in_sub_id = '" + in_sub_id + "'";

            Item itmSubEvent = cfg.inn.applySQL(sql);
            if (itmSubEvent.isError() || itmSubEvent.getResult() == "")
            {
                throw new Exception("查無子場次資料");
            }

            //代表戰場次
            sql = "SELECT id, in_sub_sect FROM IN_MEETING_PEVENT WITH(NOLOCK)"
                + " WHERE in_parent = '" + entity.in_parent + "'"
                + " AND in_sub_id = '" + entity.next_event + "'";

            Item itmRepEvent = cfg.inn.applySQL(sql);
            if (itmRepEvent.isError() || itmRepEvent.getResult() == "")
            {
                throw new Exception("查無代表戰資料");
            }


            string sub_sect = itmSubEvent.getProperty("in_sub_sect", "");

            //更新代表戰-量級
            sql = "UPDATE IN_MEETING_PEVENT SET"
                + " in_sub_sect = N'代表戰-" + sub_sect + "'"
                + " WHERE in_parent = '" + entity.in_parent + "'"
                + " AND in_sub_id = '" + entity.next_event + "'";

            Item itmUpd = cfg.inn.applySQL(sql);

            if (itmUpd.isError())
            {
                throw new Exception("代表戰量級設定發生錯誤");
            }

            CloneEventDetail(cfg, itmSubEvent, itmRepEvent, "1");
            CloneEventDetail(cfg, itmSubEvent, itmRepEvent, "2");
        }

        private void CloneEventDetail(TConfig cfg, Item itmSubEvent, Item itmRepEvent, string in_sign_foot)
        {
            string sql = "";
            string sub_id = itmSubEvent.getProperty("id", "");
            string rep_id = itmRepEvent.getProperty("id", "");

            sql = "SELECT * FROM IN_MEETING_PEVENT_DETAIL WITH(NOLOCK)"
                + " WHERE source_id = '" + sub_id + "'"
                + " AND in_sign_foot = '" + in_sign_foot + "'";

            Item itmSubFoot = cfg.inn.applySQL(sql);
            if (itmSubFoot.isError() || itmSubFoot.getResult() == "")
            {
                throw new Exception("查無代表戰資料");
            }

            string[] props = new string[]
            {
                "in_player_org",
                "in_player_team",
                "in_player_name",
                "in_player_sno",
                "in_player_status",
            };

            Item itmOld = cfg.inn.newItem("IN_MEETING_PEVENT_DETAIL", "merge");
            itmOld.setAttribute("where", "source_id = '" + rep_id + "' AND in_sign_foot = '" + in_sign_foot + "'");
            itmOld.setProperty("in_meeting", cfg.meeting_id);
            foreach (var prop in props)
            {
                itmOld.setProperty(prop, itmSubFoot.getProperty(prop, ""));
            }
            Item itmResult = itmOld.apply();
            if (itmResult.isError())
            {
                throw new Exception("查無代表戰設定出賽者發生錯誤");
            }
        }

        private class TRepre
        {
            public string in_parent { get; set; }

            public Item itmProgram { get; set; }
            public Item itmParent { get; set; }

            public int sub_event { get; set; }
            public int next_event { get; set; }

            public string program_id { get; set; }
        }
        #endregion 代表戰

        #region 團體出賽單

        //團體紀錄表 Excel
        private void RecordXlsx(TConfig cfg, Item itmReturn)
        {
            string sql = "";

            sql = "SELECT id, in_title FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'";
            cfg.itmMeeting = cfg.inn.applySQL(sql);

            sql = "SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + cfg.program_id + "'";
            cfg.itmProgram = cfg.inn.applySQL(sql);

            if (cfg.itmMeeting.isError() || cfg.itmMeeting.getResult() == ""
                || cfg.itmProgram.isError() || cfg.itmProgram.getResult() == "")
            {
                throw new Exception("賽程組別資料錯誤");
            }

            cfg.sect_start = GetIntVal(cfg.itmProgram.getProperty("in_sect_start", "0"));
            cfg.sub_event = GetIntVal(cfg.itmProgram.getProperty("in_sub_event", "0"));

            //場次資料
            var match = GetMatch(cfg, TMode.SubEvents, itmReturn);
            if (match == null || match.OrgList == null || match.Children == null)
            {
                throw new Exception("場次資料錯誤");
            }

            //子場次量級
            var sects = GetSectList(cfg);
            //匯出資料
            var xls_parm_name = "team_record_" + cfg.sub_event;
            var export = ExportInfo(cfg, xls_parm_name);

            var workbook = new ClosedXML.Excel.XLWorkbook(export.template_Path);

            //團體出賽單
            var sheetEmpty = workbook.Worksheet(1);
            sheetEmpty.CopyTo(match.MatName);

            var sheet = workbook.Worksheet(workbook.Worksheets.Count);
            AppendRecordSheet(cfg, match, sects, sheet, itmReturn);

            workbook.Worksheets.Delete(1);

            string mt_title = cfg.itmMeeting.getProperty("in_title", "");
            string pg_title = cfg.itmProgram.getProperty("in_name2", "");
            string xlsName = mt_title + "_" + pg_title + "_紀錄表" + "_" + match.MatName;

            workbook.SaveAs(export.export_Path + xlsName + ".xlsx");

            itmReturn.setProperty("xls_name", xlsName + ".xlsx");
        }

        //團體紀錄表 Xlsx
        private void AppendRecordSheet(TConfig cfg, TMatch match, List<TSect> sects, ClosedXML.Excel.IXLWorksheet sheet, Item itmReturn)
        {
            string mt_title = cfg.itmMeeting.getProperty("in_title", "");
            string pg_title = cfg.itmProgram.getProperty("in_name2", "");

            var rndSerial = "";
            if (match.InRoundSerial != "" && match.InRoundSerial != "0")
            {
                rndSerial = "輪次：" + match.InRoundSerial;
            }

            SetHead(sheet.Cell(1, 1), mt_title, fontSize: 20);
            SetHead(sheet.Cell(3, 1), pg_title, fontSize: 20);
            SetNumHead(sheet.Cell(4, 4), match.SiteCode, fontSize: 20);
            SetNumHead(sheet.Cell(4, 8), match.TreeNo, fontSize: 20);
            SetNumHead(sheet.Cell(4, 14), rndSerial, fontSize: 20);

            int wsRow = 6;
            int wsCol = 0;

            string[] fight_arr = GetFightArr(sects.Count, cfg.sect_start, match.RoundSerial);

            var rows = new List<TSectRow>();
            for (int i = 0; i < sects.Count; i++)
            {
                var sect = sects[i];
                var sect_evt = match.Children.Find(x => x.SubId == sect.sub_id);
                if (sect_evt == null) continue;

                var fight_no = fight_arr[i];

                var f1 = sect_evt.Map["1"];
                var f2 = sect_evt.Map["2"];

                bool ns1 = f1.PlayerName.Length >= 8;
                bool ns2 = f2.PlayerName.Length >= 8;

                var s1 = ns1
                    ? f1.PlayerName + Environment.NewLine + f1.PlayerStatus
                    : f1.PlayerName + f1.PlayerStatus;

                var s2 = ns2
                    ? f2.PlayerName + Environment.NewLine + f2.PlayerStatus
                    : f2.PlayerName + f2.PlayerStatus;

                var n1 = f1.PlayerName == "" ? "沒有選手" : s1;
                var n2 = f2.PlayerName == "" ? "沒有選手" : s2;

                var row = new TSectRow
                {
                    in_team_serial = fight_no,
                    team_serial = GetIntVal(fight_no),
                    tree_no = sect_evt.TreeNo,
                    sect_name = sect.name,
                    player1 = f1.PlayerOrg + f1.PlayerTeam + Environment.NewLine + n1,
                    player2 = f2.PlayerOrg + f2.PlayerTeam + Environment.NewLine + n2,
                    ns1 = ns1,
                    ns2 = ns2,
                };

                rows.Add(row);
            }

            var end_no = rows.Count + 1;
            var end_sub_id = end_no.ToString();
            var end_tree_no = match.TreeNo + end_sub_id.PadLeft(2, '0');

            rows.Add(new TSectRow
            {
                in_team_serial = (end_no).ToString(),
                team_serial = end_no,
                tree_no = end_tree_no,
                sect_name = "代表戰",
                player1 = "",
                player2 = "",
            });

            //用序號做排序
            var sorted_rows = rows.OrderBy(x => x.team_serial).ToList();
            //var sorted_rows = rows;

            for (int i = 0; i < sorted_rows.Count; i++)
            {
                var row = sorted_rows[i];
                SetCell(sheet.Cell(wsRow, wsCol + 1), row.in_team_serial);
                SetTreeNoCell(sheet.Cell(wsRow, wsCol + 2), row.tree_no);
                SetCell(sheet.Cell(wsRow, wsCol + 3), row.sect_name, isBold: true);
                SetNameCell(sheet.Cell(wsRow, wsCol + 4), row.player1, needSmaller: row.ns1);
                SetNameCell(sheet.Cell(wsRow, wsCol + 15), row.player2, needSmaller: row.ns2);

                wsRow++;
                wsRow++;
            }
        }

        private string[] GetFightArr(int count, int start, int serial)
        {
            string[] result = new string[count + 1];

            if (start > 0)
            {
                int rno = start + (serial - 1);
                int sidx = rno - 1;
                int eidx = sidx + count;

                int no = 1;
                for (int i = sidx; i < eidx; i++)
                {
                    var idx = i % count;
                    result[idx] = no.ToString();
                    no++;
                }
            }
            else
            {
                for (int i = 0; i < count; i++)
                {
                    result[i] = "";
                }
            }

            result[count] = "";

            return result;
        }

        //設定標題列
        private void SetHead(ClosedXML.Excel.IXLCell cell, string title, int fontSize = 14)
        {
            cell.Value = title;

            cell.Style.Font.Bold = true;
            cell.DataType = ClosedXML.Excel.XLDataType.Text;
            cell.Style.Font.FontName = "標楷體";
            cell.Style.Font.SetFontSize(fontSize);
        }

        //設定標題列
        private void SetOrgHead(ClosedXML.Excel.IXLCell cell, string title, int fontSize = 16)
        {
            cell.Value = title;

            cell.Style.Font.Bold = true;
            cell.DataType = ClosedXML.Excel.XLDataType.Text;
            cell.Style.Font.FontName = "標楷體";
            cell.Style.Font.SetFontSize(fontSize);
        }

        //設定標題列
        private void SetNumHead(ClosedXML.Excel.IXLCell cell, string title, int fontSize = 14)
        {
            cell.Value = title;

            cell.Style.Font.Bold = false;
            cell.DataType = ClosedXML.Excel.XLDataType.Text;
            cell.Style.Font.FontName = "標楷體";
            cell.Style.Font.SetFontSize(fontSize);
        }

        //設定標題列
        private void SetCell(ClosedXML.Excel.IXLCell cell, string title, bool isBold = false)
        {
            cell.Value = title;

            cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
            cell.Style.Font.Bold = isBold;
            cell.DataType = ClosedXML.Excel.XLDataType.Text;
            cell.Style.Font.FontName = "標楷體";
        }

        //設定標題列
        private void SetNameCell(ClosedXML.Excel.IXLCell cell, string title, bool isBold = false, bool needSmaller = false)
        {
            cell.Value = title;

            cell.Style.Font.Bold = isBold;
            cell.DataType = ClosedXML.Excel.XLDataType.Text;
            cell.Style.Font.FontName = "標楷體";
            cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Left;
            if (needSmaller)
            {
                cell.Style.Font.SetFontSize(14);
            }
            else
            {
                cell.Style.Font.SetFontSize(15);
            }
        }

        //設定標題列
        private void SetCellLeft(ClosedXML.Excel.IXLCell cell, string title, bool isBold = false)
        {
            cell.Value = title;

            cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Left;
            cell.Style.Font.Bold = isBold;
            cell.DataType = ClosedXML.Excel.XLDataType.Text;
            cell.Style.Font.FontName = "標楷體";
        }

        //設定標題列
        private void SetTreeNoCell(ClosedXML.Excel.IXLCell cell, string title, bool isBold = false)
        {
            cell.Value = title;

            cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
            cell.Style.Font.Bold = isBold;
            cell.DataType = ClosedXML.Excel.XLDataType.Text;
            cell.Style.Font.FontName = "Times New Roman";

            if (title.Length == 4)
            {
                cell.Style.Font.SetFontSize(16);
            }
            else if (title.Length == 5)
            {
                cell.Style.Font.SetFontSize(14);
            }
            else if (title.Length >= 6)
            {
                cell.Style.Font.SetFontSize(12);
            }
        }

        //設定標題列
        private void SetNumCell(ClosedXML.Excel.IXLCell cell, string title, bool isBold = false)
        {
            cell.Value = title;

            cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
            cell.Style.Font.Bold = isBold;
            cell.DataType = ClosedXML.Excel.XLDataType.Text;
            cell.Style.Font.FontName = "Times New Roman";
        }

        private TExport ExportInfo(TConfig cfg, string in_name)
        {
            TExport result = new TExport
            {
                template_Path = "",
                export_Path = "",
            };

            string aml = "<AML>" +
                "<Item type='In_Variable' action='get'>" +
                "<in_name>meeting_excel</in_name>" +
                "<Relationships>" +
                "<Item type='In_Variable_Detail' action='get'/>" +
                "</Relationships>" +
                "</Item></AML>";

            Item Vairable = cfg.inn.applyAML(aml);

            for (int i = 0; i < Vairable.getRelationships("In_Variable_Detail").getItemCount(); i++)
            {
                Item In_Variable_Detail = Vairable.getRelationships("In_Variable_Detail").getItemByIndex(i);
                if (In_Variable_Detail.getProperty("in_name", "") == in_name)
                    result.template_Path = In_Variable_Detail.getProperty("in_value", "");

                if (In_Variable_Detail.getProperty("in_name", "") == "export_path")
                    result.export_Path = In_Variable_Detail.getProperty("in_value", "");
                if (!result.export_Path.EndsWith(@"\"))
                    result.export_Path = result.export_Path + @"\";
            }

            return result;
        }

        private Item GetEventDetails(TConfig cfg, string event_id)
        {
            string sql = @"
                SELECT
	                t1.id               AS 'event_id'
	                , t1.in_tree_id
	                , t1.in_tree_no
	                , t1.in_round
	                , t1.in_round_serial
	                , t1.in_round_name
	                , t2.in_sign_foot
	                , t2.in_sign_no
	                , t11.id AS         'team_id'
	                , t11.in_short_org
	                , t11.in_team
	                , t11.in_name
	                , t11.in_sno
	                , t12.in_code       AS 'site_code'
	                , t12.in_name       AS 'site_name'
	                , t12.in_code_en    AS 'site_code_en'
                FROM
	                IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                LEFT OUTER JOIN
	                VU_MEETING_PTEAM t11 WITH(NOLOCK)
	                ON t11.source_id = t1.source_id
	                AND t11.in_sign_no = t2.in_sign_no
                INNER JOIN
	                IN_MEETING_SITE t12 WITH(NOLOCK)
	                ON t12.id = t1.in_site
                WHERE
	                t1.id = '{#event_id}'
            ";

            sql = sql.Replace("{#event_id}", event_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        private TMatch GetMatch(TConfig cfg, TMode mode, Item itmReturn)
        {
            var result = new TMatch
            {
                EventId = itmReturn.getProperty("event_id", ""),
                OrgList = new List<TOrg>(),
                Children = new List<TSub>(),
            };

            Item items = GetEventDetails(cfg, result.EventId);
            if (items.isError() || items.getItemCount() <= 1)
            {
                throw new Exception("場次明細資料錯誤");
            }

            Item itmFirst = items.getItemByIndex(0);
            result.SiteCode = itmFirst.getProperty("site_code", "");
            result.SiteName = itmFirst.getProperty("site_name", "");
            result.SiteCodeEn = itmFirst.getProperty("site_code_en", "");
            result.TreeNo = itmFirst.getProperty("in_tree_no", "");
            result.InRoundSerial = itmFirst.getProperty("in_round_serial", "");
            result.RoundSerial = GetIntVal(result.InRoundSerial);
            result.MatName = result.SiteCodeEn + result.TreeNo;

            int count = items.getItemCount();

            var f1 = new TOrg { SignFoot = "1" };
            var f2 = new TOrg { SignFoot = "2" };

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_sign_foot = item.getProperty("in_sign_foot", "");

                if (in_sign_foot == "1")
                {
                    f1.Value = item;
                }
                else if (in_sign_foot == "2")
                {
                    f2.Value = item;
                }
            }

            if (f1.Value == null) f1.Value = cfg.inn.newItem();
            if (f2.Value == null) f2.Value = cfg.inn.newItem();

            SetOrgValue(cfg, f1, f2, mode);
            SetOrgValue(cfg, f2, f1, mode);

            result.OrgList.Add(f1);
            result.OrgList.Add(f2);

            if (mode == TMode.SubEvents)
            {
                result.Children = GetSubEvents(cfg, result);
            }

            return result;
        }

        private List<TSub> GetSubEvents(TConfig cfg, TMatch match)
        {
            var result = new List<TSub>();
            var sql = @"
                SELECT
	                t1.id		AS 'event_id'
	                , t1.in_tree_id
	                , t1.in_tree_no
	                , t1.in_sub_id
	                , t1.in_win_time
	                , t2.id		AS 'detail_id'
	                , t2.in_sign_foot
	                , t2.in_sign_no
	                , t2.in_player_org
	                , t2.in_player_team
	                , t2.in_player_name
	                , t2.in_player_sno
	                , t2.in_player_status
                FROM
	                IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                INNER JOIN
	                IN_MEETING_SITE t12 WITH(NOLOCK)
	                ON t12.id = t1.in_site
                WHERE
	                t1.in_parent = '{#event_id}'
				ORDER BY
					t1.in_sub_id
            ";

            sql = sql.Replace("{#event_id}", match.EventId);
            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);
            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var sub_id = GetIntVal(item.getProperty("in_sub_id", "0"));
                var in_sign_foot = item.getProperty("in_sign_foot", "");

                var child = result.Find(x => x.SubId == sub_id);
                if (child == null)
                {
                    child = new TSub
                    {
                        EventId = item.getProperty("event_id", ""),
                        TreeId = item.getProperty("in_tree_id", ""),
                        TreeNo = item.getProperty("in_tree_no", ""),
                        SubId = sub_id,
                        Value = item,
                        Map = new Dictionary<string, TPlayer>(),
                    };
                    result.Add(child);
                }

                var player = new TPlayer
                {
                    DetailId = item.getProperty("detail_id", ""),
                    SignFoot = in_sign_foot,
                    SignNo = item.getProperty("in_sign_no", ""),
                    PlayerOrg = item.getProperty("in_player_org", ""),
                    PlayerTeam = item.getProperty("in_player_team", ""),
                    PlayerName = item.getProperty("in_player_name", ""),
                    PlayerSno = item.getProperty("in_player_sno", ""),
                    PlayerStatus = item.getProperty("in_player_status", ""),
                };

                switch (player.PlayerStatus)
                {
                    case "0":
                    case ":":
                    case ":0":
                        player.PlayerStatus = "";
                        break;
                }


                if (player.PlayerStatus != "")
                {
                    player.PlayerStatus = "(" + player.PlayerStatus + ")";
                }

                child.Map.Add(in_sign_foot, player);
            }

            return result;
        }

        private void SetOrgValue(TConfig cfg, TOrg a, TOrg b, TMode mode)
        {
            var a_short_org = a.Value.getProperty("in_short_org", "");
            var a_show_org = a.Value.getProperty("in_show_org", "");
            var b_short_org = b.Value.getProperty("in_short_org", "");
            var b_show_org = b.Value.getProperty("in_show_org", "");

            if (a_show_org != "") a_short_org = a_show_org;
            if (b_show_org != "") b_short_org = b_show_org;

            a.OrgName = a_short_org;
            a.SignNo = a.Value.getProperty("in_sign_no", "");
            a.TeamId = a.Value.getProperty("team_id", "");
            a.TeamName = a.Value.getProperty("in_team", "");

            a.Enemy = b;
            a.EnemyName = b_short_org;

            if (mode == TMode.OrgPlayers && a.TeamId != "")
            {
                a.Players = GetPlayerList(cfg, a.TeamId);
            }
            else
            {
                a.Players = new List<TMTeam>();
            }
        }

        private List<TMTeam> GetPlayerList(TConfig cfg, string team_id)
        {
            List<TMTeam> list = new List<TMTeam>();

            string sql = "SELECT * FROM IN_MEETING_PTEAM WITH(NOLOCK)"
                    + " WHERE in_type = 's'"
                    + " AND in_parent = '" + team_id + "'"
                    + " AND ISNULL(in_weight_message, '') = ''"
                    + " AND ISNULL(in_name, '') <> ''"
                    + " ORDER BY in_sub_id, in_sub_serial";

            Item itmSubs = cfg.inn.applySQL(sql);

            int count = itmSubs.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = itmSubs.getItemByIndex(i);
                list.Add(GetMTeam(cfg, item));
            }

            return list;
        }

        //空隊伍
        private TMTeam GetEmptyPlayer(TConfig cfg)
        {
            return new TMTeam
            {
                in_name = "",
                in_sub_weight = "",
                in_weight_message = "",
                Value = cfg.inn.newItem(),
            };
        }

        private class TExport
        {
            public string template_Path { get; set; }
            public string export_Path { get; set; }
        }

        private enum TMode
        {
            None = 0,
            OrgPlayers = 1,
            SubEvents = 2,
        }

        private class TMatch
        {
            public string EventId { get; set; }
            public string SiteName { get; set; }
            public string SiteCode { get; set; }
            public string SiteCodeEn { get; set; }
            public string MatName { get; set; }
            public string TreeId { get; set; }
            public string TreeNo { get; set; }
            public string InRoundSerial { get; set; }
            public int RoundSerial { get; set; }

            public Item Value { get; set; }
            public List<TOrg> OrgList { get; set; }
            public List<TSub> Children { get; set; }
        }

        private class TSub
        {
            public string EventId { get; set; }
            public string TreeId { get; set; }
            public string TreeNo { get; set; }
            public int SubId { get; set; }
            public Item Value { get; set; }
            public Dictionary<string, TPlayer> Map { get; set; }
        }

        private class TPlayer
        {
            public string DetailId { get; set; }
            public string SignFoot { get; set; }
            public string SignNo { get; set; }
            public string PlayerOrg { get; set; }
            public string PlayerTeam { get; set; }
            public string PlayerName { get; set; }
            public string PlayerSno { get; set; }
            public string PlayerStatus { get; set; }
        }

        private class TOrg
        {
            public string OrgName { get; set; }
            public string TeamId { get; set; }
            public string TeamName { get; set; }
            public string SignFoot { get; set; }
            public string SignNo { get; set; }

            public string EnemyName { get; set; }

            /// <summary>
            /// 我方
            /// </summary>
            public Item Value { get; set; }

            /// <summary>
            /// 選手
            /// </summary>
            public List<TMTeam> Players { get; set; }

            /// <summary>
            /// 對手
            /// </summary>
            public TOrg Enemy { get; set; }
        }

        #endregion 團體過磅單

        //儲存
        private void Save(TConfig cfg, Item itmReturn)
        {
            string did = itmReturn.getProperty("did", "");
            string muid = itmReturn.getProperty("muid", "");
            string weight = itmReturn.getProperty("weight", "");

            string sql = "SELECT in_name, in_sno, in_team, in_short_org, in_show_org, in_team_weight_n2, in_team_weight_n3 FROM IN_MEETING_USER WITH(NOLOCK) WHERE id = '" + muid + "'";
            Item itmMuser = cfg.inn.applySQL(sql);
            if (itmMuser.isError() || itmMuser.getResult() == "")
            {
                itmMuser = cfg.inn.newItem();
                //throw new Exception("查無與會者資料");
            }

            string old_name = itmReturn.getProperty("old_name", "");
            string new_name = itmReturn.getProperty("new_name", "");
            string new_sno = itmMuser.getProperty("in_sno", "");

            string new_n2 = itmMuser.getProperty("in_team_weight_n2", "")
                .Replace("第", "")
                .Replace("級", "");

            Item itmChild = GetEvtDtlById(cfg, did);
            if (itmChild.isError() || itmChild.getResult() == "")
            {
                throw new Exception("查無子場次明細資料");
            }

            string detail_id = itmChild.getProperty("detail_id", "");
            string in_parent = itmChild.getProperty("in_parent", "");
            string in_sub_id = itmChild.getProperty("in_sub_id", "");
            string in_sign_foot = itmChild.getProperty("in_sign_foot", "");

            Item itmParent = GetEvtDtlByFoot(cfg, in_parent, in_sign_foot);
            if (itmParent.isError() || itmParent.getResult() == "")
            {
                itmParent = cfg.inn.newItem();
                //throw new Exception("查無主場次明細資料");
            }

            int sub_id = GetIntVal(in_sub_id);

            int sign_no = in_sign_foot == "1"
                    ? (10 + sub_id) * -1
                    : (20 + sub_id) * -1;

            //檢查上一場次是否包含沒有選手
            CheckLastEventPlayer(cfg, itmParent, new_name, in_sub_id, itmReturn);

            string in_player_org = itmParent.getProperty("player_org", "");
            string in_player_team = itmParent.getProperty("player_team", "");

            string in_show_org = itmParent.getProperty("in_show_org", "");
            if (in_show_org != "") in_player_org = in_show_org;

            string player_status = new_n2 + ":" + weight;
            if (new_n2 == "") player_status = "";

            string sql_upd1 = "UPDATE IN_MEETING_PEVENT_DETAIL SET "
                + " in_sign_no = '" + sign_no + "'"
                + " , in_player_org = N'" + in_player_org + "'"
                + " , in_player_team = N'" + in_player_team + "'"
                + " , in_player_name = N'" + new_name + "'"
                + " , in_player_sno = N'" + new_sno + "'"
                + " , in_player_status = '" + player_status + "'"
                + " WHERE id = '" + detail_id + "'";

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "變更選手 from: " + old_name + " to: " + new_name
                + "，sql: " + sql_upd1);

            Item itmSQL1 = cfg.inn.applySQL(sql_upd1);

            if (itmSQL1.isError())
            {
                throw new Exception("出賽選手變更失敗");
            }
        }

        private void CheckLastEventPlayer(TConfig cfg, Item itmParent, string new_name, string in_sub_id, Item itmReturn)
        {
            if (new_name == "" || new_name == "沒有選手") return;

            string need_check_no_player = itmReturn.getProperty("need_check_no_player", "");
            if (need_check_no_player != "1") return;

            string sql = @"
                SELECT TOP 1
	                t1.id              AS 'p_evt_id'
	                , t1.in_tree_no    AS 'p_tree_no'
	                , t1.in_win_time   AS 'p_win_time'
	                , t2.in_sign_foot  AS 'p_sign_foot'
	                , t2.in_sign_no    AS 'p_sign_no'
	                , t3.in_stuff_b1   AS 'p_org_code'
	                , t3.in_short_org  AS 'p_org_name'
	                , t3.in_team       AS 'p_team'
	                , t11.id           AS 's_evt_id'
	                , t11.in_tree_no   AS 's_tree_no'
	                , t11.in_sub_sect
	                , t12.id           AS 's_detail_id'
	                , t12.in_player_org
	                , t12.in_player_team
	                , t12.in_player_name
                FROM 
	                IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                INNER JOIN
	                IN_MEETING_PTEAM t3 WITH(NOLOCK)
	                ON t3.source_id = t1.source_id
	                AND t3.in_sign_no = t2.in_sign_no
                INNER JOIN
	                IN_MEETING_PEVENT t11 WITH(NOLOCK)
	                ON t11.in_parent = t1.id
                INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t12 WITH(NOLOCK)
	                ON t12.source_id = t11.id
	                AND t12.in_parent = t2.id
                WHERE
	                t1.source_id = '{#program_id}'
	                AND ISNULL(t1.in_tree_no, 0) < {#tree_no}
	                AND t3.in_sign_no = '{#sign_no}'
	                AND t11.in_sub_id = {#sub_id}
                ORDER BY
	                t1.in_tree_no DESC
            ";

            sql = sql.Replace("{#program_id}", itmParent.getProperty("source_id", ""))
                .Replace("{#tree_no}", itmParent.getProperty("in_tree_no", "0"))
                .Replace("{#sign_no}", itmParent.getProperty("in_sign_no", "0"))
                .Replace("{#sub_id}", in_sub_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            Item item = cfg.inn.applySQL(sql);

            if (item.isError() || item.getResult() == "")
            {
                return;
            }

            string org = itmParent.getProperty("player_org", "");

            string s_tree_no = item.getProperty("s_tree_no", "");
            string in_sub_sect = item.getProperty("in_sub_sect", "");
            string in_player_name = item.getProperty("in_player_name", "");

            if (in_player_name == "沒有選手")
            {
                throw new Exception("[" + org + "]於" + s_tree_no + "場次[" + in_sub_sect + "]已設定為[沒有選手]，因此不能指派選手");
            }
        }

        private Item GetEvtDtlById(TConfig cfg, string did)
        {
            string sql = @"
                SELECT
	                t1.id           AS 'event_id'
                    , t1.in_type
                    , t1.in_parent
                    , t1.in_sub_id
	                , t2.id         AS 'detail_id'
                    , t2.in_sign_foot
                    , t2.in_sign_no
                FROM
	                IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                WHERE
	                t2.id = '{#did}'
            ";

            sql = sql.Replace("{#did}", did);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);

        }

        private Item GetEvtDtlByFoot(TConfig cfg, string eid, string foot)
        {
            string sql = @"
                SELECT
                    t1.source_id
                    , t1.in_tree_no
                    , t11.in_sign_no
	                , ISNULL(t11.map_short_org, t2.in_player_org) AS 'player_org'
	                , ISNULL(t11.in_team, t2.in_player_team)    AS 'player_team'
	                , ISNULL(t11.in_name, t2.in_player_name)    AS 'player_name'
	                , ISNULL(t11.in_sno, t2.in_player_sno)      AS 'player_sno'
	                , t11.in_show_org
                FROM
	                IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                LEFT OUTER JOIN
	                VU_MEETING_PTEAM t11 WITH(NOLOCK)
	                ON t11.source_id = t1.source_id
	                AND t11.in_sign_no = t2.in_sign_no
                WHERE
	                t1.id = '{#eid}'
	                AND t2.in_sign_foot = '{#foot}'
            ";

            sql = sql.Replace("{#eid}", eid)
                .Replace("{#foot}", foot);

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        //頁面
        private void BattlePage(TConfig cfg, Item itmReturn)
        {
            string sql = "";

            sql = "SELECT id, in_title, in_uniform_color FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'";
            cfg.itmMeeting = cfg.inn.applySQL(sql);

            if (cfg.itmMeeting.isError() || cfg.itmMeeting.getResult() == "")
            {
                throw new Exception("賽程組別資料錯誤");
            }
            itmReturn.setProperty("in_title", cfg.itmMeeting.getProperty("in_title", ""));
            itmReturn.setProperty("in_uniform_color", cfg.itmMeeting.getProperty("in_uniform_color", ""));

            AppendProgramMenu(cfg, itmReturn);
            itmReturn.setProperty("program_id", cfg.program_id);

            if (cfg.program_id != "")
            {
                cfg.itmProgram = cfg.inn.applySQL("SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + cfg.program_id + "'");

                string program_id = cfg.itmProgram.getProperty("id", "");
                itmReturn.setProperty("program_id", program_id);

                cfg.sub_event = GetIntVal(cfg.itmProgram.getProperty("in_sub_event", "0"));
                cfg.need_represent = true;

                //隊伍
                var rows = RowsFromMTeam(cfg);
                //場次
                var evts = GetEvents(cfg);
                //繫結場次與隊伍
                BindEventRows(cfg, evts, rows);
                //對戰 Table
                BattleTable(cfg, evts, itmReturn);
            }
        }

        //附加組別選單
        private void AppendProgramMenu(TConfig cfg, Item itmReturn)
        {
            var list = GetProgramDictionary(cfg);

            var builder = new StringBuilder();
            builder.Append("<select id='program_menu' class='form-control' style='width: 240px;' onchange='Program_Change(this)'>");
            foreach (var kv in list)
            {
                var day = kv.Key;
                builder.Append("<optgroup label='" + day + "'>");
                foreach (var item in kv.Value)
                {
                    string id = item.getProperty("id", "");
                    string in_name2 = item.getProperty("in_name2", "");
                    string in_team_count = item.getProperty("in_team_count", "0");
                    string text = in_name2 + " (" + in_team_count + ")";

                    builder.Append("<option data-day='" + day + "' value='" + id + "'>" + text + "</option>");
                }
                builder.Append("</optgroup>");
            }
            builder.Append("</select>");

            var contents = builder.ToString();

            itmReturn.setProperty("program_menu", contents);
        }

        //附加組別選單
        private Dictionary<string, List<Item>> GetProgramDictionary(TConfig cfg)
        {
            var list = new Dictionary<string, List<Item>>();

            string sql = @"
                SELECT 
	                id
                    , in_name2
                    , in_fight_day
                    , in_team_count
                FROM 
	                IN_MEETING_PROGRAM WITH(NOLOCK) 
                WHERE 
	                in_meeting = '{#meeting_id}'
	                AND in_l1 = N'團體組'
            	    AND ISNULL(in_team_count, 0) > 0
                ORDER BY
                    in_fight_day
	                , in_sort_order
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);
            Item items = cfg.inn.applySQL(sql);
            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string key = item.getProperty("in_fight_day", "");
                if (!list.ContainsKey(key))
                {
                    list.Add(key, new List<Item>());
                }
                list[key].Add(item);
            }

            if (cfg.program_id == "" && count >= 1)
            {
                cfg.program_id = items.getItemByIndex(0).getProperty("id", "");
            }
            return list;
        }

        //與會者隊伍
        private List<TRow> RowsFromMTeam(TConfig cfg)
        {
            //主隊伍資料
            string sql = @"SELECT * FROM IN_MEETING_PTEAM WITH(NOLOCK)"
                + " WHERE source_id = '" + cfg.itmProgram.getProperty("id", "")
                + "' ORDER BY in_stuff_b1, in_team, in_type, in_sub_id, in_sub_serial";

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            var itmMTeams = cfg.inn.applySQL(sql);

            List<TRow> rows = new List<TRow>();

            int count = itmMTeams.getItemCount();
            if (count <= 0) return rows;

            for (int i = 0; i < count; i++)
            {
                Item item = itmMTeams.getItemByIndex(i);
                string team_id = item.getProperty("id", "");
                string in_type = item.getProperty("in_type", "");
                string in_parent = item.getProperty("in_parent", "");
                string in_name = item.getProperty("in_name", "");

                var row = default(TRow);

                if (in_type == "p")
                {
                    var in_short_org = item.getProperty("in_short_org", "");
                    var in_show_org = item.getProperty("in_show_org", "");
                    if (in_show_org != "") in_short_org = in_show_org;

                    row = new TRow
                    {
                        TeamId = team_id,
                        SortNo = item.getProperty("in_stuff_b1", ""),
                        OrgName = in_short_org,
                        TeamName = item.getProperty("in_team", ""),
                        InSignNo = item.getProperty("in_sign_no", ""),
                        InJudoNo = item.getProperty("in_judo_no", ""),
                        InWeightMessage = item.getProperty("in_weight_message", ""),
                        Parent = GetMTeam(cfg, item),
                        Children = new List<TMTeam>(),
                    };

                    row.SignNo = GetIntVal(row.InSignNo);

                    rows.Add(row);
                }
                else if (in_type == "s" && in_parent != "" && in_name != "")
                {
                    row = rows.Find(x => x.TeamId == in_parent);
                    if (row == null)
                    {
                        //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "隊伍資料異常(parent) dom: " + item.dom.InnerXml);
                    }
                    else
                    {
                        row.Children.Add(GetMTeam(cfg, item));
                    }
                }
                else
                {
                    //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "隊伍資料異常 dom: " + item.dom.InnerXml);
                }
            }

            return rows;
        }

        private TMTeam GetMTeam(TConfig cfg, Item item)
        {
            var result = new TMTeam
            {
                team_id = item.getProperty("id", ""),
                in_short_org = item.getProperty("in_short_org", ""),
                in_team = item.getProperty("in_team", ""),
                in_name = item.getProperty("in_name", ""),
                in_sno = item.getProperty("in_sno", ""),
                in_current_org = item.getProperty("in_current_org", ""),

                in_type = item.getProperty("in_type", ""),
                in_parent = item.getProperty("in_parent", ""),
                in_sub_sect = item.getProperty("in_sub_sect", ""),
                in_sub_id = item.getProperty("in_sub_id", ""),
                in_sub_serial = item.getProperty("in_sub_serial", ""),
                in_sub_weight = item.getProperty("in_sub_weight", ""),
                in_muser = item.getProperty("in_muser", ""),

                in_sign_no = item.getProperty("in_sign_no", ""),
                in_judo_no = item.getProperty("in_judo_no", ""),

                in_weight_message = item.getProperty("in_weight_message", ""),
                in_weight_value = item.getProperty("in_weight_value", "0"),

                Value = item,
            };

            result.sign_no = GetIntVal(result.in_sign_no);
            result.sub_id = GetIntVal(result.in_sub_id);
            result.sub_serial = GetIntVal(result.in_sub_serial);
            result.weight_value = GetDcmVal(result.in_weight_value);

            return result;
        }

        //繫結場次與隊伍
        private void BindEventRows(TConfig cfg, List<TEvent> mevents, List<TRow> teams)
        {
            foreach (var mevent in mevents)
            {
                BindFootRow(cfg, mevent.Foots, "1", teams);
                BindFootRow(cfg, mevent.Foots, "2", teams);
            }
        }

        //繫結場次腳位與隊伍
        private void BindFootRow(TConfig cfg, Dictionary<string, TFoot> foots, string sign_foot, List<TRow> teams)
        {
            if (foots.ContainsKey(sign_foot))
            {
                var foot = foots[sign_foot];
                if (foot.sign_no > 0)
                {
                    foot.OrgTeam = teams.Find(x => x.SignNo == foot.sign_no);
                }

                if (foot.OrgTeam == null)
                {
                    foot.OrgTeam = new TRow
                    {
                        OrgName = "",
                        Parent = new TMTeam { },
                        Children = new List<TMTeam>(),
                    };
                }
            }
        }

        //表格箱
        private void BattleTable(TConfig cfg, List<TEvent> list, Item itmReturn)
        {
            StringBuilder builder = new StringBuilder();

            builder.Append("<div class='showsheet_ clearfix'>");
            builder.Append("    <div>");
            builder.Append("        <div class='float-btn clearfix'>");
            builder.Append("        </div>");

            if (cfg.scene == "score")
            {
                AppendScoreTable(cfg, list, builder);
            }
            else
            {
                AppendBattleTable(cfg, list, builder);
            }

            builder.Append("    </div>");

            builder.Append("</div>");

            itmReturn.setProperty("inn_tabs", builder.ToString());
        }

        private void AppendScoreTable(TConfig cfg, List<TEvent> list, StringBuilder builder)
        {
            var sects = GetSectList(cfg);
            AppendRepresentSect(cfg, sects);

            List<TField> fields = new List<TField>();
            fields.Add(new TField { title = "場次", format = TFType.None, getValue = EventName, css = "treeStyle" });
            fields.Add(new TField { title = "單位", format = TFType.None, getValue = OrgName, css = "cellStyle" });
            //fields.Add(new TField { title = "隊別", format = TFType.None, getValue = TeamName, css = "cellStyle" });
            //fields.Add(new TField { title = "籤號", format = TFType.None, getValue = SignNo, css = "cellStyle" });
            fields.Add(new TField { title = "比數", format = TFType.None, getValue = SumVal, css = "cellStyle" });
            for (int i = 0; i < sects.Count; i++)
            {
                var sect = sects[i];
                fields.Add(new TField { title = sect.name, format = TFType.None, getValue = ScoreVal, sect = sect, css = "cellStyle" });
            }

            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            head.Append("<thead>");
            head.Append("  <tr>");
            for (int i = 0; i < fields.Count; i++)
            {
                var field = fields[i];
                head.Append("    <th class='text-center' rowspan='1' data-cell-style='" + field.css + "'>");
                head.Append(field.title);
                head.Append("    </th>");
            }
            head.Append("  </tr>");
            head.Append("</thead>");


            int count = list.Count;
            body.Append("<tbody>");
            for (int i = 0; i < count; i++)
            {
                var evt = list[i];

                var evt_field = fields[0];

                body.Append("  <tr>");
                body.Append("      <td rowspan='2' >");
                body.Append(evt_field.getValue(cfg, evt_field, evt, "", null));
                body.Append("      </td>");
                AppendTeamCols(cfg, fields, evt, "1", body);
                body.Append("  </tr>");

                body.Append("  <tr class='tree_row'>");
                AppendTeamCols(cfg, fields, evt, "2", body);
                body.Append("  </tr>");
            }
            body.Append("</tbody>");



            string tb_id = "team_battle_table";

            builder.Append("<table id='" + tb_id + "'"
                + " class='table table-hover table-bordered table-rwd rwd'"
                + " style='background-color: #fff;' "
                + " data-toggle='table' "
                + " data-search='false'"
                + " data-search-align='left'"
                + ">");

            builder.Append(head);
            builder.Append(body);
            builder.Append("</table>");

            //builder.Append("<script>");
            //builder.Append("    $('#" + tb_id + "').bootstrapTable({});");
            //builder.Append("    if($(window).width() <= 768) { $('#" + tb_id + "').bootstrapTable('toggleView'); }");
            //builder.Append("</script>");
        }

        private void AppendBattleTable(TConfig cfg, List<TEvent> list, StringBuilder builder)
        {
            var sects = GetSectList(cfg);
            AppendRepresentSect(cfg, sects);

            List<TField> fields = new List<TField>();
            fields.Add(new TField { title = "場次", format = TFType.None, getValue = EventName, css = "treeStyle" });
            fields.Add(new TField { title = "單位", format = TFType.None, getValue = OrgName, css = "cellStyle" });
            for (int i = 0; i < sects.Count; i++)
            {
                var sect = sects[i];
                fields.Add(new TField { title = sect.name, format = TFType.None, getValue = CtrlVal, sect = sect, css = "cellStyle" });
            }
            fields.Add(new TField { title = "出賽", format = TFType.None, getValue = BtnValue, css = "cellStyle" });
            fields.Add(new TField { title = "紀錄", format = TFType.None, getValue = NextEventFunc, css = "treeStyle" });

            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            head.Append("<thead>");
            head.Append("  <tr>");
            for (int i = 0; i < fields.Count; i++)
            {
                var field = fields[i];
                head.Append("    <th class='text-center' rowspan='1' data-cell-style='" + field.css + "'>");
                head.Append(field.title);
                head.Append("    </th>");
            }
            head.Append("  </tr>");
            head.Append("</thead>");


            var evt_field = fields[0];
            var nxt_field = fields.Last();
            int count = list.Count;
            body.Append("<tbody>");
            for (int i = 0; i < count; i++)
            {
                var evt = list[i];
                body.Append("  <tr>");
                body.Append("      <td rowspan='2' >");
                body.Append(evt_field.getValue(cfg, evt_field, evt, "", null));
                body.Append("      </td>");
                AppendTeamCols(cfg, fields, evt, "1", body);
                body.Append("      <td rowspan='2' >");
                body.Append(nxt_field.getValue(cfg, nxt_field, evt, "", null));
                body.Append("      </td>");
                body.Append("  </tr>");

                body.Append("  <tr class='tree_row'>");
                AppendTeamCols(cfg, fields, evt, "2", body);
                body.Append("  </tr>");
            }
            body.Append("</tbody>");



            string tb_id = "team_battle_table";

            builder.Append("<table id='" + tb_id + "'"
                + " class='table table-hover table-bordered table-rwd rwd'"
                + " style='background-color: #fff;' "
                + " data-toggle='table' "
                + " data-search='false'"
                + " data-search-align='left'"
                + ">");

            builder.Append(head);
            builder.Append(body);
            builder.Append("</table>");

            //builder.Append("<script>");
            //builder.Append("    $('#" + tb_id + "').bootstrapTable({});");
            //builder.Append("    if($(window).width() <= 768) { $('#" + tb_id + "').bootstrapTable('toggleView'); }");
            //builder.Append("</script>");
        }

        private void AppendTeamCols(TConfig cfg, List<TField> fields, TEvent evt, string foot, StringBuilder body)
        {
            var team = evt.Foots[foot].OrgTeam;
            var mn = 1;
            var mx = fields.Count - 1;
            for (int i = mn; i < mx; i++)
            {
                var field = fields[i];
                body.Append("      <td >");
                if (team == null)
                {
                    body.Append("&nbsp;");
                }
                else
                {
                    body.Append(field.getValue(cfg, field, evt, foot, team));
                }
                body.Append("      </td>");
            }
        }

        private string NextEventFunc(TConfig cfg, TField field, TEvent evt, string foot, TRow team)
        {
            string btn1 = "<button class='btn btn-sm btn-primary'"
                + " data-eid='" + evt.event_id + "'"
                + " onclick='ExportRecord(this)'>"
                + "紀錄表"
                + "</button>";

            string btn2 = "<button class='btn btn-sm btn-default'"
                + " data-eid='" + evt.event_id + "'"
                + " onclick='ExportNextFightTable(this)'>"
                + "下一場出賽"
                + "</button>";

            return "<div style='display: flex;flex-direction: column; gap: 15px 20px;'>"
                + btn1
                + btn2
                + "</div>";
        }

        private string EventName(TConfig cfg, TField field, TEvent evt, string foot, TRow team)
        {
            var text = "<span>" + evt.site_code_en + evt.in_tree_no + " <a href='javascript:;' data-eid='" + evt.event_id + "' onclick='GoFightTreePage()' ><i class='fa fa-link'></i></span></a>";
            var btn = "";
            if (cfg.need_represent)
            {
                // btn = "<i class='fa fa-gavel inn_represent' title='代表戰量級設定'"
                //     + " data-eid='" + evt.event_id + "'"
                //     + "></i>";

                btn = "<button class='btn btn-sm btn-default' title='代表戰量級設定'"
                    + " data-eid='" + evt.event_id + "'"
                    + " onclick='ShowRepresentModal(this)'>"
                    + "代表戰"
                    + "</button>";
            }
            return "<div style='display: flex;flex-direction: column; gap: 15px 20px;'>"
                + text
                + btn
                + "</div>";
        }

        private string OrgName(TConfig cfg, TField field, TEvent evt, string foot, TRow team)
        {
            if (team.TeamName != "")
            {
                return team.InWeightMessage + team.OrgName + " " + team.TeamName;
            }
            else
            {
                return team.InWeightMessage + team.OrgName;
            }
        }

        private string SignNo(TConfig cfg, TField field, TEvent evt, string foot, TRow team)
        {
            return team.InJudoNo;
        }

        private string TeamName(TConfig cfg, TField field, TEvent evt, string foot, TRow team)
        {
            return team.TeamName;
        }

        private string ScoreVal(TConfig cfg, TField field, TEvent evt, string foot, TRow team)
        {
            string points = "";
            var sect = field.sect;
            var child = evt.Children.Find(x => x.sub_id == sect.sub_id);
            if (child != null && child.Foots.ContainsKey(foot))
            {
                var f = child.Foots[foot];
                string in_status = f.Value.getProperty("in_status", "");
                string in_points = f.Value.getProperty("in_points", "0");
                string in_correct_count = f.Value.getProperty("in_correct_count", "0");
                string in_player_name = f.Value.getProperty("in_player_name", "");

                switch (in_status)
                {
                    case "1": return "<span class='team_score_b'><i class='fa fa-check-square'></i> " + in_points + "</span>";
                    case "0":
                        if (in_correct_count == "3")
                        {
                            return "<span class='team_score_c'><i class='fa fa-square'></i> " + in_points + " (S3)</span>";
                        }
                        else if (in_player_name == "")
                        {
                            return "<span class='team_score_c'><i class='fa fa-square'></i> " + in_points + " (無)</span>";
                        }
                        else
                        {
                            return "<span class='team_score_c'><i class='fa fa-square'></i> " + in_points + "</span>";
                        }
                    default: return "<span class='team_score_a'>&nbsp;</span>";
                }
            }
            return points;
        }

        private string SumVal(TConfig cfg, TField field, TEvent evt, string foot, TRow team)
        {
            var cnt = 0;

            foreach (var child in evt.Children)
            {
                if (child.Foots.ContainsKey(foot))
                {
                    var itmDetail = child.Foots[foot].Value;
                    var in_status = itmDetail.getProperty("in_status", "");
                    if (in_status == "1")
                    {
                        cnt++;
                    }
                }
            }

            return "<p class='bg-green color-palette'>" + cnt.ToString() + "</p>";
        }

        private string BtnValue(TConfig cfg, TField field, TEvent evt, string foot, TRow team)
        {
            string pid = team.Parent.team_id;
            string eid = evt.event_id;
            string sign = team.InSignNo;

            if (team.SignNo <= 0)
            {
                return "&nbsp;";
            }

            var btn1 = "<button class='btn btn-sm btn-primary'"
                + " data-eid='" + eid + "'"
                + " data-pid='" + pid + "'"
                + " data-sign='" + sign + "'"
                + " onclick='ExportXlsx(this)'>"
                + "出賽單"
                + "</button>";

            var btn2 = "<button class='btn btn-sm btn-success'"
                + " data-eid='" + eid + "'"
                + " data-pid='" + pid + "'"
                + " data-sign='" + sign + "'"
                + " onclick='TeamFightSetting(this)'>"
                + "設　定"
                + "</button>";

            return btn1 + " " + btn2;
        }

        private string CtrlVal(TConfig cfg, TField field, TEvent evt, string foot, TRow team)
        {
            var sect = field.sect;

            string sub = sect.sub_id.ToString();
            string pid = team.Parent.team_id;
            string sign = team.Parent.in_sign_no;
            string eid = evt.event_id;
            string did = "";
            string name = "";
            string sno = "";
            string wtime = "";
            string wstat = "";

            var child = evt.Children.Find(x => x.sub_id == sect.sub_id);
            if (child != null && child.Foots.ContainsKey(foot))
            {
                var f = child.Foots[foot];
                did = f.detail_id;
                name = f.Value.getProperty("in_player_name", "");
                sno = f.Value.getProperty("in_player_sno", "");
                wtime = f.Value.getProperty("in_win_time", "");
                wstat = f.Value.getProperty("in_player_status", "");
            }

            if (sect.is_represent)
            {
                return name;
            }

            StringBuilder builder = new StringBuilder();
            builder.Append("<select class='form-control btn btn-default team-select'");
            builder.Append(" data-did='" + did + "'");
            builder.Append(" data-sub='" + sub + "'");
            builder.Append(" data-name='" + name + "'");
            builder.Append(" data-sign='" + sign + "'");
            builder.Append(" data-time='" + wtime + "'");
            builder.Append(" data-state='" + wstat + "'");
            builder.Append(" onchange='BattlePlayer_Change(this)' >");

            builder.Append("    <option value=''>請選擇</option>");
            builder.Append("    <option value='沒有選手'>沒有選手</option>");

            var players = team.Children.FindAll(x => x.sub_id == sect.sub_id);
            // var players = default(List<TMTeam>);
            // if (sect.is_represent)
            // {
            //     players = team.Children;
            // }
            // else
            // {
            //     players = team.Children.FindAll(x => x.sub_id == sect.sub_id);
            // }

            if (players != null)
            {
                players = players.OrderBy(x => x.weight_value).ToList();

                foreach (var player in players)
                {
                    var obj = MapMUserOpt(player);

                    var opt = "<option value='" + obj.value + "'"
                        + " data-muid='" + player.in_muser + "'"
                        + " data-weight='" + obj.weight + "'"
                        + ">"
                        + obj.label
                        + "</option>";

                    builder.Append(opt);
                }
            }

            builder.Append("</select>");

            return builder.ToString();
        }

        private TMUserOpt MapMUserOpt(TMTeam player)
        {
            var result = new TMUserOpt
            {
                value = player.in_name,
                weight = player.in_weight_value,
                label = player.in_name,
                disabled = "",
            };

            if (player.in_weight_message != "")
            {
                result.value = player.in_name + player.in_weight_message;
                result.label = player.in_name + player.in_weight_message;
                result.disabled = "disabled";
            }
            else if (player.weight_value > 0)
            {
                result.label = player.in_name + "(" + player.in_weight_value + ")";
            }

            return result;
        }

        private class TMUserOpt
        {
            public string value { get; set; }
            public string label { get; set; }
            public string weight { get; set; }
            public string disabled { get; set; }
        }

        private List<TEvent> GetEvents(TConfig cfg)
        {
            List<TEvent> main_evts = GetMainEvents(cfg);
            AppendSubEvents(cfg, main_evts);
            return main_evts;
        }

        private void AppendSubEvents(TConfig cfg, List<TEvent> main)
        {
            string sql = @"
                SELECT
	                t1.id AS 'event_id'
	                , t1.in_tree_name
	                , t1.in_tree_id
	                , t1.in_tree_no
	                , t1.in_tree_alias
                    , t1.in_win_sign_no
                    , CONVERT(VARCHAR, t1.in_win_time, 121) AS 'in_win_time'
                    , t1.in_parent
                    , t1.in_sub_id
                    , t2.id AS 'detail_id'
                    , t2.in_sign_foot
	                , t2.in_sign_no
	                , t11.id                                        AS 'team_id'
	                , ISNULL(t11.map_short_org, t2.in_player_org)   AS 'in_player_org'
	                , ISNULL(t11.in_team, t2.in_player_team)        AS 'in_player_team'
	                , ISNULL(t11.in_name, t2.in_player_name)        AS 'in_player_name'
	                , ISNULL(t11.in_sno, t2.in_player_sno)          AS 'in_player_sno'
	                , t2.in_player_status
                    , t2.in_status
                    , t2.in_points
                    , t2.in_correct_count
                FROM
	                IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                LEFT OUTER JOIN
                    VU_MEETING_PTEAM t11
                    ON t11.source_id = t1.source_id
                    AND t11.in_sign_no = t2.in_sign_no
                WHERE
	                t1.source_id = '{#program_id}'
	                AND t1.in_tree_name = 'sub'
					AND ISNULL(t1.in_tree_no, 0) <> 0
                ORDER BY
	                t1.in_tree_no
	                , t2.in_sign_foot
            ";

            sql = sql.Replace("{#program_id}", cfg.itmProgram.getProperty("id", ""));

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            Item items = cfg.inn.applySQL(sql);
            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_parent = item.getProperty("in_parent", "");
                string event_id = item.getProperty("event_id", "");

                //父場次
                TEvent parent = main.Find(x => x.event_id == in_parent);
                if (parent == null)
                {
                    //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "error sql: " + sql);
                    continue;
                }

                TEvent evt = parent.Children.Find(x => x.event_id == event_id);

                if (evt == null)
                {
                    evt = new TEvent
                    {
                        event_id = item.getProperty("event_id", ""),
                        in_tree_name = item.getProperty("in_tree_name", ""),
                        in_tree_id = item.getProperty("in_tree_id", ""),
                        in_tree_no = item.getProperty("in_tree_no", ""),
                        in_sub_id = item.getProperty("in_sub_id", "0"),
                        in_win_sign_no = item.getProperty("in_win_sign_no", "0"),
                        Value = item,
                        Foots = new Dictionary<string, TFoot>(),
                        Children = new List<TEvent>(),
                    };

                    evt.sub_id = GetIntVal(evt.in_sub_id);

                    parent.Children.Add(evt);
                }

                string detail_id = item.getProperty("detail_id", "");
                string in_sign_foot = item.getProperty("in_sign_foot", "");
                string in_sign_no = item.getProperty("in_sign_no", "0");
                int sign_no = GetIntVal(in_sign_no);

                var foot = new TFoot
                {
                    detail_id = detail_id,
                    in_sign_foot = in_sign_foot,
                    in_sign_no = in_sign_no,
                    sign_no = sign_no,
                    Value = item,
                };

                evt.Foots.Add(in_sign_foot, foot);
            }
        }

        private List<TEvent> GetMainEvents(TConfig cfg)
        {
            List<TEvent> result = new List<TEvent>();

            string sql = @"
                SELECT
	                t1.id AS 'event_id'
	                , t1.in_tree_name
	                , t1.in_tree_id
	                , t1.in_tree_no
	                , t1.in_tree_alias
                    , t1.in_win_sign_no
                    , t2.in_sign_foot
	                , t2.in_sign_no
                    , t2.in_status
                    , t2.in_points
                    , t2.in_correct_count
                    , t12.in_name    AS 'site_name'
                    , t12.in_code    AS 'site_code'
                    , t12.in_code_en AS 'site_code_en'
                FROM
	                IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                LEFT OUTER JOIN
                    IN_MEETING_SITE t12
                    ON t12.id = t1.in_site
                WHERE
	                t1.source_id = '{#program_id}'
	                AND t1.in_tree_name NOT IN (N'sub')
	                AND ISNULL(t1.in_tree_no, 0) <> 0
                ORDER BY
	                t1.in_tree_no
	                , t12.in_code
	                , t2.in_sign_foot
            ";

            sql = sql.Replace("{#program_id}", cfg.itmProgram.getProperty("id", ""));

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item items = cfg.inn.applySQL(sql);
            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string event_id = item.getProperty("event_id", "");

                TEvent evt = result.Find(x => x.event_id == event_id);

                if (evt == null)
                {
                    evt = new TEvent
                    {
                        event_id = item.getProperty("event_id", ""),
                        in_tree_name = item.getProperty("in_tree_name", ""),
                        in_tree_id = item.getProperty("in_tree_id", ""),
                        in_tree_no = item.getProperty("in_tree_no", ""),
                        site_code = item.getProperty("site_code", ""),
                        site_name = item.getProperty("site_name", ""),
                        site_code_en = item.getProperty("site_code_en", ""),
                        Value = item,
                        Foots = new Dictionary<string, TFoot>(),
                        Children = new List<TEvent>(),
                    };

                    result.Add(evt);
                }

                string in_sign_foot = item.getProperty("in_sign_foot", "");
                string in_sign_no = item.getProperty("in_sign_no", "0");
                int sign_no = GetIntVal(in_sign_no);

                var foot = new TFoot
                {
                    in_sign_foot = in_sign_foot,
                    in_sign_no = in_sign_no,
                    sign_no = sign_no,
                    Value = item,
                };

                evt.Foots.Add(in_sign_foot, foot);
            }
            return result;
        }


        //取得組別量級選單
        private List<TSect> GetSectList(TConfig cfg)
        {
            var lstSect = GetMeetingSection(cfg);
            var sects = new List<TSect>();
            for (int i = 0; i < lstSect.Count; i++)
            {
                Item itmSect = lstSect[i];
                sects.Add(new TSect
                {
                    sub_id = i + 1,
                    name = itmSect.getProperty("in_name", ""),
                    gender = itmSect.getProperty("in_gender", ""),
                    weight = itmSect.getProperty("in_weight", ""),
                    Value = itmSect,
                });
            }
            //sects.Add(new TSect { No = 7, Name = "代表戰" });
            return sects;
        }

        private void AppendRepresentSect(TConfig cfg, List<TSect> sects)
        {
            if (sects == null || sects.Count == 0) return;

            var endSect = sects[sects.Count - 1];

            var sub_id = sects.Count + 1;
            var sub_nm = "代表戰";

            var item = cfg.inn.newItem();
            item.setProperty("in_sub_id", sub_id.ToString());
            item.setProperty("in_name", sub_nm);
            item.setProperty("in_program", endSect.Value.getProperty("in_program", ""));
            item.setProperty("in_program_name", endSect.Value.getProperty("in_program_name", ""));
            item.setProperty("in_team_battel", endSect.Value.getProperty("in_team_battel", ""));

            var rptSect = new TSect
            {
                sub_id = sub_id,
                name = sub_nm,
                gender = "",
                weight = "",
                is_represent = true,
                Value = item,
            };

            sects.Add(rptSect);
        }

        //取得團體賽量級清單
        private List<Item> GetMeetingSection(TConfig cfg)
        {
            var program_id = cfg.program_id;
            var result = new List<Item>();
            var sql = @"
                SELECT 
                    * 
                FROM 
                    IN_MEETING_PSECT WITH(NOLOCK) 
                WHERE 
                    in_program = '{#program_id}'
                     AND in_name <> N'代表戰'
                ORDER BY 
                    in_sub_id
            ";

            sql = sql.Replace("{#program_id}", program_id);
            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                result.Add(items.getItemByIndex(i));
            }
            return result;
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }

            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }

            public string meeting_id { get; set; }
            public string program_id { get; set; }
            public string scene { get; set; }

            public Item itmMeeting { get; set; }
            public Item itmProgram { get; set; }
            public string in_battle_type { get; set; }
            public bool is_robin { get; set; }
            public int sect_start { get; set; }
            public int sub_event { get; set; }
            public bool need_represent { get; set; }
        }

        private class TField
        {
            public string title { get; set; }
            public string css { get; set; }
            public TFType format { get; set; }
            public Func<TConfig, TField, TEvent, string, TRow, string> getValue { get; set; }
            public TSect sect { get; set; }
        }

        private class TSect
        {
            public int sub_id { get; set; }
            public string name { get; set; }
            public string gender { get; set; }
            public string weight { get; set; }
            public bool is_represent { get; set; }

            public Item Value { get; set; }
        }

        private enum TFType
        {
            None = 0,
            Center = 1,
        }

        private class TEvent
        {
            public string event_id { get; set; }
            public string in_tree_name { get; set; }
            public string in_tree_id { get; set; }
            public string in_tree_no { get; set; }
            public string in_win_sign_no { get; set; }
            public string in_sub_id { get; set; }

            public string site_code { get; set; }
            public string site_name { get; set; }
            public string site_code_en { get; set; }

            public Item Value { get; set; }

            public Dictionary<string, TFoot> Foots { get; set; }
            public List<TEvent> Children { get; set; }

            public int sub_id { get; set; }
        }

        private class TFoot
        {
            public string detail_id { get; set; }
            public string in_sign_foot { get; set; }
            public string in_sign_no { get; set; }
            public string in_judo_no { get; set; }
            public int sign_no { get; set; }
            public TRow OrgTeam { get; set; }
            public Item Value { get; set; }
        }

        private class TRow
        {
            public string TeamId { get; set; }
            public string SortNo { get; set; }
            public string OrgName { get; set; }
            public string TeamName { get; set; }
            public string InSignNo { get; set; }
            public string InJudoNo { get; set; }
            public string InWeightMessage { get; set; }

            public TMTeam Parent { get; set; }
            public List<TMTeam> Children { get; set; }

            public int SignNo { get; set; }
        }

        private class TMTeam
        {
            public string team_id { get; set; }
            public string in_short_org { get; set; }
            public string in_team { get; set; }

            public string in_name { get; set; }
            public string in_sno { get; set; }
            public string in_current_org { get; set; }

            public string in_type { get; set; }
            public string in_parent { get; set; }
            public string in_sub_sect { get; set; }
            public string in_sub_id { get; set; }
            public string in_sub_serial { get; set; }
            public string in_sub_weight { get; set; }
            public string in_muser { get; set; }

            public string in_sign_no { get; set; }
            public string in_judo_no { get; set; }
            public string in_weight_message { get; set; }
            public string in_weight_value { get; set; }
            public decimal weight_value { get; set; }

            public int sign_no { get; set; }
            public int sub_id { get; set; }
            public int sub_serial { get; set; }

            public Item Value { get; set; }
        }

        private class TSectRow
        {
            public string in_team_serial { get; set; }
            public int team_serial { get; set; }

            public string tree_no { get; set; }
            public string sect_name { get; set; }
            public string player1 { get; set; }
            public string player2 { get; set; }

            public bool ns1 { get; set; }
            public bool ns2 { get; set; }
        }

        private string MapSiteName(string site_code)
        {
            switch (site_code)
            {
                case "1": return "A";
                case "2": return "B";
                case "3": return "C";
                case "4": return "D";
                case "5": return "E";
                case "6": return "F";
                case "7": return "G";
                case "8": return "H";
                case "9": return "I";
                case "10": return "J";
                case "11": return "K";
                case "12": return "L";
                default: return "?";
            }
        }

        private int GetMaxPlayerCount(int sub_event)
        {
            switch (sub_event)
            {
                case 3: return 5;
                case 5: return 7;
                case 6: return 5;
                default: return 5;
            }
        }

        private decimal GetDcmVal(string value, decimal def = 0)
        {
            decimal result = def;
            decimal.TryParse(value, out result);
            return result;
        }

        private int GetIntVal(string value, int def = 0)
        {
            if (value == null || value == "") return 0;
            int result = def;
            int.TryParse(value, out result);
            return result;
        }
    }
}