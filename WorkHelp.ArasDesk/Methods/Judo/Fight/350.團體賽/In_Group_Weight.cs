﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Judo.Fight
{
    public class In_Group_Weight : Item
    {
        public In_Group_Weight(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 量級體重表
                日誌: 
                    - 2023-05-15: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "In_Group_Weight";

            var itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,

                meeting_id = itmR.getProperty("meeting_id", ""),
                in_group = itmR.getProperty("in_group", ""),
                scene = itmR.getProperty("scene", ""),
            };

            switch (cfg.scene)
            {
                case "page":
                    Page(cfg, itmR);
                    break;
            }

            return itmR;
        }

        //頁面
        private void Page(TConfig cfg, Item itmReturn)
        {
            AppendGroupItems(cfg, itmReturn);

            if (cfg.in_group == "") return;

            var sql = @"
                SELECT 
                    *
                FROM 
	                In_Group_Weight WITH(NOLOCK) 
                WHERE 
	                in_group = N'{#in_group}'
                ORDER BY 
	                in_index
            ";
            sql = sql.Replace("{#in_group}", cfg.in_group);
            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                item.setType("inn_weight");
                itmReturn.addRelationship(item);
            }
        }

        private void AppendGroupItems(TConfig cfg, Item itmReturn)
        {
            var sql = @"
                SELECT DISTINCT
	                in_group_index
	                , in_group
                FROM 
	                In_Group_Weight WITH(NOLOCK) 
                ORDER BY 
	                in_group_index
            ";

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                item.setType("inn_group");
                itmReturn.addRelationship(item);
            }
        }

        private void CopyItemValue(Item itmA, Item itmB, string propA, string propB = "")
        {
            if (propB == "")
            {
                itmA.setProperty(propA, itmB.getProperty(propA, ""));
            }
            else
            {
                itmA.setProperty(propA, itmB.getProperty(propB, ""));
            }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string meeting_id { get; set; }
            public string in_group { get; set; }
            public string scene { get; set; }
        }

    }
}
