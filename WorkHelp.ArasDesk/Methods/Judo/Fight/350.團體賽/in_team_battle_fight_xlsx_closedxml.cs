﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Judo.Fight
{
    public class in_team_battle_fight_xlsx_closedxml : Item
    {
        public in_team_battle_fight_xlsx_closedxml(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 團體賽-出賽單(New)
                日誌: 
                    - 2022-10-27: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]" + "in_team_battle_fight_xlsx";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                strIdentityId = inn.getUserAliases(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                program_id = itmR.getProperty("program_id", ""),
                team_sign_no = itmR.getProperty("team_sign_no", ""),
                scene = itmR.getProperty("scene", ""),
            };


            string sql = "";

            sql = "SELECT id, in_title FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'";
            cfg.itmMeeting = cfg.inn.applySQL(sql);
            if (cfg.itmMeeting.isError() || cfg.itmMeeting.getResult() == "")
            {
                throw new Exception("賽事資料錯誤");
            }

            sql = "SELECT TOP 1 * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + cfg.program_id + "'";
            cfg.itmProgram = cfg.inn.applySQL(sql);
            if (cfg.itmProgram.isError() || cfg.itmProgram.getResult() == "")
            {
                throw new Exception("賽程 組別 資料錯誤");
            }

            cfg.in_title = cfg.itmMeeting.getProperty("in_title", "");

            cfg.pg_name2 = cfg.itmProgram.getProperty("in_name2", "");
            cfg.in_battle_type = cfg.itmProgram.getProperty("in_battle_type", "");
            cfg.is_robin = cfg.in_battle_type == "SingleRoundRobin";
            cfg.sub_event = GetIntVal(cfg.itmProgram.getProperty("in_sub_event", "0"));
            cfg.sect_start = GetIntVal(cfg.itmProgram.getProperty("in_sect_start", "0"));

            var xls_parm_name = "team_fight_" + cfg.sub_event;
            cfg.itmXlsx = cfg.inn.applyMethod("In_Variable_File", "<in_name>" + xls_parm_name + "</in_name>");
            // if (cfg.itmXlsx.isError() || cfg.itmXlsx.getResult() == "")
            // {
            //     throw new Exception("賽程 XLSX 資料錯誤");
            // }

            cfg.template_path = cfg.itmXlsx.getProperty("template_path", "");
            cfg.export_path = cfg.itmXlsx.getProperty("export_path", "");
            if (cfg.export_path == "")
            {
                throw new Exception("賽程 XLSX 資料錯誤");
            }

            cfg.sects = GetSectList(cfg, true);
            if (cfg.sects == null || cfg.sects.Count == 0)
            {
                throw new Exception("賽程 團體賽量級 資料錯誤");
            }

            cfg.itmParents = GetParentEvents(cfg);
            cfg.itmChildren = GetChildrenEvents(cfg);
            cfg.itmTeams = GetSubTeams(cfg);

            var evts = MapMainEvents(cfg, cfg.itmParents);
            AppendChildren(cfg, evts, cfg.itmChildren);
            if (evts.Count == 0)
            {
                throw new Exception("賽程 場次 資料錯誤");
            }

            var teams = MapMainTeams(cfg, cfg.itmTeams);
            if (teams.Count == 0)
            {
                throw new Exception("賽程 隊伍 資料錯誤");
            }

            cfg.max_rcount = GetMaxPlayerCount(cfg.sub_event);
            cfg.sheets = new List<string>();

            Export(cfg, evts, teams, itmR);

            return itmR;
        }

        private void Export(TConfig cfg, List<TRow> evts, List<TRow> teams, Item itmReturn)
        {
            var book = new ClosedXML.Excel.XLWorkbook(cfg.template_path);

            for (int i = 0; i < evts.Count; i++)
            {
                var evt = evts[i];
                AppendSheet(cfg, evts, evt, evt.foot1, teams, book);
                AppendSheet(cfg, evts, evt, evt.foot2, teams, book);
            }

            book.Worksheets.Delete(1);

            string pg_title = cfg.itmProgram.getProperty("in_name2", "");
            string xlsName = cfg.in_title + "_" + pg_title + "_出賽單";

            book.SaveAs(cfg.export_path + xlsName + ".xlsx");

            itmReturn.setProperty("xls_name", xlsName + ".xlsx");
        }

        private void AppendSheet(TConfig cfg, List<TRow> evts, TRow evt, TOrg org, List<TRow> teams, ClosedXML.Excel.IXLWorkbook book)
        {
            var in_tree_no = evt.value.getProperty("in_tree_no", "");
            var rnd = "R" + evt.value.getProperty("in_round", "");
            var key = org.org_short + org.team + "_" + in_tree_no;
            //var key = org.org_short + org.team + "_" + rnd;

            //該單位隊伍清單 (org > sect > player)
            var org_team_page = teams.Find(x => x.no == org.sign_no);
            if (org_team_page == null) org_team_page = new TRow { Children = new List<TRow>() };


            if (in_tree_no == "" || in_tree_no == "0") return;
            if (org.org_short == "") return;
            //if (org.team_status != "") return;

            if (cfg.sheets.Contains(key))
            {
                //該單位該場次已存在，結束
                return;
            }
            else
            {
                cfg.sheets.Add(key);
            }

            //複製
            var sheetEmpty = book.Worksheet(1);
            sheetEmpty.CopyTo(key);

            var sheet = book.Worksheet(cfg.sheets.Count + 1);
            AppendFightSheet(cfg, evt, org, org_team_page, sheet);

            var in_next_win = evt.value.getProperty("in_next_win", "");
            if (in_next_win != "")
            {
                var next_evt = evts.Find(x => x.id == in_next_win);
                if (next_evt != null)
                {
                    AppendSheet(cfg, evts, next_evt, org, teams, book);
                }
            }
        }

        private void AppendFightSheet(TConfig cfg, TRow evt, TOrg org, TRow org_team_page, ClosedXML.Excel.IXLWorksheet sheet)
        {
            SetHead(sheet.Cell(1, 1), cfg.in_title, 18);
            SetHead(sheet.Cell(3, 1), cfg.pg_name2, 16);

            var site_code = evt.value.getProperty("site_code", "");
            var in_tree_no = evt.value.getProperty("in_tree_no", "");
            var in_round_serial = evt.value.getProperty("in_round_serial", "");
            if (in_round_serial != "" && in_round_serial != "0")
            {
                in_round_serial = "輪次：" + in_round_serial;
            }

            SetOrgHead(sheet.Cell(4, 1), "場地：" + site_code);
            SetOrgHead(sheet.Cell(4, 3), "場次：" + in_tree_no);
            SetOrgHead(sheet.Cell(4, 5), in_round_serial);

            SetHead(sheet.Cell(4, 8), "單位：" + org.org_show, 16);

            int wsRow = 7;
            int wsCol = 0;

            var org_sects = new List<TSectRow>();
            for (int i = 0; i < cfg.sects.Count; i++)
            {
                var sect = cfg.sects[i];

                var sub_evt = evt.Children.Find(x => x.no == sect.sub_id);
                if (sub_evt == null) sub_evt = new TRow { value = cfg.inn.newItem() };
                var sub_tree_no = sub_evt.value.getProperty("in_tree_no", "");

                var org_sect_team_box = org_team_page.Children.Find(x => x.no == sect.sub_id);
                if (org_sect_team_box == null) org_sect_team_box = new TRow { Children = new List<TRow>() };

                var org_sect = new TSectRow 
                {
                    sub_tree_no = sub_tree_no,

                    id = sect.sub_id,
                    name = sect.Value.getProperty("in_name", ""),
                    weight = sect.Value.getProperty("in_weight", ""),
                    ranges = sect.Value.getProperty("in_ranges", ""),
                    players = ResetSectPlayers(cfg, sect, org_sect_team_box.Children),
                };
                org_sects.Add(org_sect);
            }

            //sorted_org_sects = org_sects;
            var sorted_org_sects = org_sects.OrderBy(x => x.sub_tree_no).ToList();

            foreach(var sect in sorted_org_sects)
            {
                SetNumCell(sheet.Cell(wsRow, wsCol + 1), sect.sub_tree_no);
                SetCell(sheet.Cell(wsRow, wsCol + 2), sect.name, isBold: true);
                SetCell(sheet.Cell(wsRow, wsCol + 3), sect.weight, isBold: true);

                foreach(var p in sect.players)
                {
                    var inn_display = p.value.getProperty("inn_display", "");
                    var inn_ranges = p.value.getProperty("inn_ranges", "");

                    SetCell(sheet.Cell(wsRow, wsCol + 5), inn_display);
                    SetCellLeft(sheet.Cell(wsRow, wsCol + 9), inn_ranges);
                    wsRow++;
                }
                wsRow++;
            }
        }

        private List<TRow> ResetSectPlayers(TConfig cfg, TSect sect, List<TRow> source)
        {
            //用體重排序
            var list = source.OrderBy(x => x.weight).ToList();
            foreach(var row in list)
            {
                string in_name = row.value.getProperty("in_name", "");
                string in_weight_message = row.value.getProperty("in_weight_message", "");
                string in_sub_weight = row.value.getProperty("in_sub_weight", "");

                string display = in_name;
                if (in_weight_message != "")
                {
                    display += in_weight_message;
                }
                else if (row.weight > 0)
                {
                    display += "(" + row.weight + ")";
                }

                string ranges= GetPlayerSect(cfg, sect, in_sub_weight);

                row.value.setProperty("inn_display", display);
                row.value.setProperty("inn_ranges", ranges);
            }

            //空資料列
            var d = cfg.max_rcount - list.Count;
            for (int i = 0; i < d; i++)
            {
                var itmEmpty = cfg.inn.newItem();
                itmEmpty.setProperty("inn_display", "沒有選手");
                itmEmpty.setProperty("inn_ranges", "");
                list.Add(new TRow { value = itmEmpty });
            }
            return list;
        }

        //設定標題列
        private void SetHead(ClosedXML.Excel.IXLCell cell, string title, int fontSize = 14)
        {
            cell.Value = title;

            cell.Style.Font.Bold = true;
            cell.DataType = ClosedXML.Excel.XLDataType.Text;
            cell.Style.Font.FontName = "標楷體";
            cell.Style.Font.SetFontSize(fontSize);
        }

        //設定資料列
        private void SetCell(ClosedXML.Excel.IXLCell cell, string title, bool isBold = false)
        {
            cell.Value = title;

            cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
            cell.Style.Font.Bold = isBold;
            cell.DataType = ClosedXML.Excel.XLDataType.Text;
            cell.Style.Font.FontName = "標楷體";
        }


        //設定資料列
        private void SetCellLeft(ClosedXML.Excel.IXLCell cell, string title, bool isBold = false)
        {
            cell.Value = title;

            cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Left;
            cell.Style.Font.Bold = isBold;
            cell.DataType = ClosedXML.Excel.XLDataType.Text;
            cell.Style.Font.FontName = "標楷體";
        }

        private string GetPlayerSect(TConfig cfg, TSect sect, string weight)
        {
            var in_ranges = sect.Value.getProperty("in_ranges", "");
            if (in_ranges == "") return "";

            //var values = in_ranges + ",無";
            var values = in_ranges;
            var arr = values.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            if (arr == null || arr.Length == 0) return "";

            List<string> list = new List<string>();
            foreach (var v in arr)
            {
                if (v == weight)
                {
                    list.Add("■" + v);
                }
                else
                {
                    list.Add("□" + v);
                }
            }

            return string.Join(",", list);
        }

        //設定標題列
        private void SetOrgHead(ClosedXML.Excel.IXLCell cell, string title, int fontSize = 16)
        {
            cell.Value = title;

            cell.Style.Font.Bold = true;
            cell.DataType = ClosedXML.Excel.XLDataType.Text;
            cell.Style.Font.FontName = "標楷體";
            cell.Style.Font.SetFontSize(fontSize);
        }

        //設定標題列
        private void SetNumCell(ClosedXML.Excel.IXLCell cell, string title, bool isBold = false)
        {
            cell.Value = title;

            cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
            cell.Style.Font.Bold = isBold;
            cell.DataType = ClosedXML.Excel.XLDataType.Text;
            cell.Style.Font.FontName = "Times New Roman";
        }

        //取得組別量級選單
        private List<TSect> GetSectList(TConfig cfg, bool is_filter)
        {
            var lstSect = GetMeetingSection(cfg, is_filter);

            var sects = new List<TSect>();

            for (int i = 0; i < lstSect.Count; i++)
            {
                Item itmSect = lstSect[i];
                sects.Add(new TSect
                {
                    sub_id = i + 1,
                    name = itmSect.getProperty("in_name", ""),
                    gender = itmSect.getProperty("in_gender", ""),
                    weight = itmSect.getProperty("in_weight", ""),
                    Value = itmSect,
                });
            }

            //sects.Add(new TSect { No = 7, Name = "代表戰" });

            return sects;
        }

        //取得團體賽量級清單
        private List<Item> GetMeetingSection(TConfig cfg, bool is_filter)
        {
            string program_id = cfg.program_id;

            List<Item> result = new List<Item>();

            string sql = "SELECT * FROM In_Meeting_PSect WITH(NOLOCK) WHERE in_program = '" + program_id + "'"
                + " ORDER BY in_sub_id";

            if (is_filter)
            {
                sql = "SELECT * FROM In_Meeting_PSect WITH(NOLOCK) WHERE in_program = '" + program_id + "'"
                + " AND in_name <> N'代表戰'"
                + " ORDER BY in_sub_id";
            }

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item items = cfg.inn.applySQL(sql);
            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                result.Add(items.getItemByIndex(i));
            }

            return result;
        }

        private List<TRow> MapMainTeams(TConfig cfg, Item items)
        {
            var rows = new List<TRow>();
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                var sign_no = GetIntVal(item.getProperty("in_sign_no", "0"));
                var sub_id = GetIntVal(item.getProperty("in_sub_id", "0"));

                var row = rows.Find(x => x.no == sign_no);
                if (row == null)
                {
                    row = new TRow
                    {
                        no = sign_no,
                        Children = new List<TRow>(),
                    };
                    rows.Add(row);
                }

                var sect = row.Children.Find(x => x.no == sub_id);
                if (sect == null)
                {
                    sect = new TRow
                    {
                        no = sub_id,
                        Children = new List<TRow>(),
                    };
                    row.Children.Add(sect);
                }

                sect.Children.Add(new TRow
                {
                    value = item,
                    weight = GetDcmVal(item.getProperty("in_weight", "0")),
                });
            }
            return rows;
        }

        private List<TRow> MapMainEvents(TConfig cfg, Item items)
        {
            var rows = new List<TRow>();
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                rows.Add(new TRow
                {
                    id = item.getProperty("id", ""),
                    value = item,
                    foot1 = MapOrg(cfg, item, "f1_"),
                    foot2 = MapOrg(cfg, item, "f2_"),
                    Children = new List<TRow>(),
                });
            }
            return rows;
        }

        private void AppendChildren(TConfig cfg, List<TRow> rows, Item items)
        {
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_parent = item.getProperty("in_parent", "");
                var row = rows.Find(x => x.id == in_parent);
                if (row == null) continue;
                row.Children.Add(new TRow
                {
                    id = item.getProperty("id", ""),
                    value = item,
                    no = GetIntVal(item.getProperty("in_sub_id", "")),
                });
            }
        }

        private TOrg MapOrg(TConfig cfg, Item item, string prefix)
        {
            var result = new TOrg
            {
                sign_no = GetIntVal(item.getProperty(prefix + "sign_no", "0")),
                org_code = item.getProperty(prefix + "org_code", ""),
                org_name = item.getProperty(prefix + "org_name", ""),
                org_short = item.getProperty(prefix + "org_short", ""),
                team = item.getProperty(prefix + "team", ""),
                team_status = item.getProperty(prefix + "team_status", ""),
                player_name = item.getProperty(prefix + "player_name", ""),
                player_names = item.getProperty(prefix + "player_names", ""),
            };

            result.org_show = result.org_short + result.team;
            if (result.team_status != "") result.org_show += result.team_status;

            return result;
        }

        private Item GetChildrenEvents(TConfig cfg)
        {
            string sql = @"
                SELECT
	                t1.id
	                , t1.in_parent
	                , t1.in_tree_id
	                , t1.in_tree_no
                    , t1.in_sub_id
                    , t1.in_round
	                , t1.in_round_code
	                , t1.in_round_serial
	                , t1.in_next_win
	                , t1.in_next_foot_win
	                , t11.id                AS 'f1_did'
	                , t11.in_sign_foot      AS 'f1_foot'
	                , t11.in_player_org     AS 'f1_org_name'
	                , t11.in_player_team    AS 'f1_team'
	                , t11.in_player_name    AS 'f1_player_name'
	                , t11.in_player_sno     AS 'f1_player_sno'
	                , t11.in_player_status  AS 'f1_player_status'
	                , t12.id                AS 'f2_did'
	                , t12.in_sign_foot      AS 'f2_foot'
	                , t12.in_player_org     AS 'f2_org_name'
	                , t12.in_player_team    AS 'f2_team'
	                , t12.in_player_name    AS 'f2_player_name'
	                , t12.in_player_sno     AS 'f2_player_sno'
	                , t12.in_player_status  AS 'f2_player_status'
                FROM
	                IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t11 WITH(NOLOCK)
	                ON t11.source_id = t1.id
	                AND t11.in_sign_foot = 1
                INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t12 WITH(NOLOCK)
	                ON t12.source_id = t1.id
	                AND t12.in_sign_foot = 2
                WHERE
	                t1.source_id = '{#program_id}'
	                AND ISNULL(t1.in_tree_no, 0) > 0
	                AND t1.in_type = 's'
                ORDER BY
	                t1.in_tree_id
            ";

            sql = sql.Replace("{#program_id}", cfg.program_id);

            return cfg.inn.applySQL(sql);
        }

        private Item GetParentEvents(TConfig cfg)
        {
            string sql = @"

                SELECT
	                t1.id
	                , t1.in_tree_id
	                , t1.in_tree_no
                    , t1.in_round
	                , t1.in_round_code
	                , t1.in_round_serial
	                , t1.in_next_win
	                , t1.in_next_foot_win
	                , t11.id                AS 'f1_did'
	                , t11.in_sign_foot      AS 'f1_foot'
	                , t11.in_sign_no        AS 'f1_sign_no'
	                , t21.id                AS 'f1_tid'
	                , t21.in_stuff_b1       AS 'f1_org_code'
	                , t21.in_current_org    AS 'f1_org_name'
	                , t21.in_short_org      AS 'f1_org_short'
	                , t21.in_name           AS 'f1_plary_name'
	                , t21.in_names          AS 'f1_plary_names'
	                , t21.in_team           AS 'f1_team'
	                , t21.in_weight_message AS 'f1_team_status'
	                , t12.id                AS 'f2_did'
	                , t12.in_sign_foot      AS 'f2_foot'
	                , t12.in_sign_no        AS 'f2_sign_no'
	                , t22.id                AS 'f2_tid'
	                , t22.in_stuff_b1       AS 'f2_org_code'
	                , t22.in_current_org    AS 'f2_org_name'
	                , t22.in_short_org      AS 'f2_org_short'
	                , t22.in_name           AS 'f2_player_name'
	                , t22.in_names          AS 'f2_player_names'
	                , t22.in_team           AS 'f2_team'
	                , t22.in_weight_message AS 'f2_team_status'
	                , t31.id                AS 'site_id'
	                , t31.in_name           AS 'site_name'
	                , t31.in_code           AS 'site_code'
                FROM
	                IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t11 WITH(NOLOCK)
	                ON t11.source_id = t1.id
	                AND t11.in_sign_foot = 1
                INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t12 WITH(NOLOCK)
	                ON t12.source_id = t1.id
	                AND t12.in_sign_foot = 2
                LEFT OUTER JOIN
	                VU_MEETING_PTEAM t21 WITH(NOLOCK)
	                ON t21.source_id = t1.source_id
	                AND t21.in_sign_no = t11.in_sign_no
                    AND ISNULL(t21.in_type, '') <> 's'
                LEFT OUTER JOIN
	                VU_MEETING_PTEAM t22 WITH(NOLOCK)
	                ON t22.source_id = t1.source_id
	                AND t22.in_sign_no = t12.in_sign_no
                    AND ISNULL(t22.in_type, '') <> 's'
                INNER JOIN
	                IN_MEETING_SITE t31 WITH(NOLOCK)
	                ON t31.id = t1.IN_SITE
                WHERE
	                t1.source_id = '{#program_id}'
	                AND ISNULL(t1.in_tree_no, 0) > 0
	                AND t1.in_type = 'p'
                ORDER BY
	                t1.in_tree_id
            ";

            sql = sql.Replace("{#program_id}", cfg.program_id);

            return cfg.inn.applySQL(sql);
        }


        private Item GetSubTeams(TConfig cfg)
        {
            string sql = @"
                SELECT 
	                t1.in_parent
	                , t1.in_sub_id
	                , t1.in_weight_message
                    , t1.in_sub_weight
	                , t2.in_name
	                , t2.in_sno
	                , t2.in_gender
	                , t2.in_weight
	                , t2.in_team_weight_n2
	                , t2.in_team_weight_n3
	                , t3.in_sign_no
                FROM 
	                VU_MEETING_PTEAM t1 
                INNER JOIN
	                IN_MEETING_USER t2 WITH(NOLOCK)
	                ON t2.id = t1.in_muser
                INNER JOIN
	                IN_MEETING_PTEAM t3 WITH(NOLOCK)
	                ON t3.id = t1.in_parent
                WHERE
	                t1.source_id = '{#program_id}'
	                AND ISNULL(t1.in_type, '') = 's'
                ORDER BY
	                t1.in_parent
	                , t1.in_sub_id
            ";

            sql = sql.Replace("{#program_id}", cfg.program_id);

            return cfg.inn.applySQL(sql);
        }

        private class TRow
        {
            public string id { get; set; }
            public Item value { get; set; }
            public TOrg foot1 { get; set; }
            public TOrg foot2 { get; set; }
            public List<TRow> Children { get; set; }

            public int no { get; set; }
            public decimal weight { get; set; }

        }

        private class TOrg
        {
            public int sign_no { get; set; }

            public string org_code { get; set; }
            public string org_name { get; set; }
            public string org_short { get; set; }
            public string org_show { get; set; }
            
            public string team { get; set; }
            public string team_status { get; set; }
            
            public string player_name { get; set; }
            public string player_names { get; set; }
            public string player_sno { get; set; }
            public string player_status { get; set; }

            public Item value { get; set; }
        }

        private class TSect
        {
            public int sub_id { get; set; }
            public string name { get; set; }
            public string gender { get; set; }
            public string weight { get; set; }

            public Item Value { get; set; }
        }

        private class TSectRow
        {
            public string sub_tree_no { get; set; }

            public int id { get; set; }
            public string name { get; set; }
            public string weight { get; set; }
            public string ranges { get; set; }

            public List<TRow> players { get; set; }
        }

        private class TSectPlayer
        {
            public string name { get; set; }
            public string ranges { get; set; }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }

            public string meeting_id { get; set; }
            public string program_id { get; set; }
            public string scene { get; set; }
            public string team_sign_no { get; set; }

            public Item itmMeeting { get; set; }
            public Item itmProgram { get; set; }
            public Item itmXlsx { get; set; }

            public string in_title { get; set; }

            public string pg_name2 { get; set; }
            public string in_battle_type { get; set; }
            public bool is_robin { get; set; }
            public int sub_event { get; set; }
            public int sect_start { get; set; }
            public int max_rcount { get; set; }

            public string template_path { get; set; }
            public string export_path { get; set; }

            public List<TSect> sects { get; set; }

            public Item itmParents { get; set; }
            public Item itmChildren { get; set; }
            public Item itmTeams { get; set; }

            public List<string> sheets { get; set; }
        }

        private int GetMaxPlayerCount(int sub_event)
        {
            switch (sub_event)
            {
                case 3: return 5;
                case 5: return 7;
                case 6: return 5;
                default: return 5;
            }
        }

        private decimal GetDcmVal(string value, decimal def = 0)
        {
            decimal result = def;
            decimal.TryParse(value, out result);
            return result;
        }

        private int GetIntVal(string value, int def = 0)
        {
            if (value == null || value == "") return 0;

            int result = def;
            Int32.TryParse(value, out result);
            return result;
        }
    }
}