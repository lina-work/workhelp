﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Judo.Fight
{
    public class in_team_battle_register : Item
    {
        public in_team_battle_register(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 團體戰-註冊、過磅
                日期: 
                    - 2021-12-13 領取簽名總表 (Lina)
                    - 2021-10-13 創建 (Lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]in_team_battle_register";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, this.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                meeting_id = itmR.getProperty("meeting_id", ""),
                program_id = itmR.getProperty("program_id", ""),
                team_id = itmR.getProperty("team_id", ""),
                scene = itmR.getProperty("scene", ""),
            };

            Item itmCurrentUser = inn.getItemById("User", cfg.strUserId);
            cfg.strUserName = itmCurrentUser.getProperty("last_name", "");

            switch (cfg.scene)
            {
                case "save":
                    Save(cfg, itmR);
                    break;

                case "remove":
                    Remove(cfg, itmR);
                    break;

                case "weight":
                    WeightPage(cfg, itmR);
                    break;

                case "weight_table":
                    cfg.is_weight_table = true;
                    WeightTable(cfg, itmR);
                    break;

                case "weight_xlsx":
                    WeightXlsx(cfg, itmR);
                    break;

                case "sect_update":
                    UpdateSect(cfg, itmR);
                    break;

                case "state_update":
                    UpdateState(cfg, itmR);
                    break;

                case "weight_update":
                    UpdateWeight(cfg, itmR);
                    break;

                case "sign_xlsx":
                    SignNameXlsx(cfg, itmR);
                    break;

                case "org_register_xlsx":
                    RegisterXlsx(cfg, itmR, is_empty: false);
                    break;

                case "empty_register_xlsx":
                    RegisterXlsx(cfg, itmR, is_empty: true);
                    break;

                case "special_section":
                    SpecialSection(cfg, itmR);
                    break;

                default:
                    OrgPage(cfg, itmR);
                    break;
            }

            return itmR;
        }

        //特別組
        private void SpecialSection(TConfig cfg, Item itmReturn)
        {
            string sql = @"
                SELECT
	                id
	                , in_l1
	                , in_l2
	                , in_l3
                FROM
	                IN_MEETING_PROGRAM WITH(NOLOCK)
                WHERE
	                in_meeting = '{#meeting_id}'
	                AND in_l1 = '團體組'
	                AND in_l2 LIKE '%特別組%'
                ORDER BY
	                in_sort_order
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);
            Item itmPrograms = cfg.inn.applySQL(sql);
            int count = itmPrograms.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item itmProgram = itmPrograms.getItemByIndex(i);
                Item itmSects = SpecialSectionSects(cfg, itmProgram);
                Item itmTeams = SpecialSectionTeams(cfg, itmProgram);

                int team_count = itmTeams.getItemCount();
                for (int j = 0; j < team_count; j++)
                {
                    Item itmTeam = itmTeams.getItemByIndex(j);
                    Item itmUsers = SpecialSectionUsers(cfg, itmProgram, itmTeam);
                    MergeSubTeam(cfg, itmProgram, itmSects, itmTeam, itmUsers);
                }
            }
        }

        private void MergeSubTeam(TConfig cfg, Item itmProgram, Item itmSects, Item itmParent, Item itmUsers)
        {
            string pid = itmParent.getProperty("id", "");

            int sect_count = itmSects.getItemCount();
            int user_count = itmUsers.getItemCount();
            for (int i = 0; i < sect_count; i++)
            {
                Item itmSect = itmSects.getItemByIndex(i);
                string sub = itmSect.getProperty("in_sub_id", "");
                string sect_name = itmSect.getProperty("in_name", "");

                for (int j = 0; j < user_count; j++)
                {
                    Item itmUser = itmUsers.getItemByIndex(j);
                    string muid = itmUser.getProperty("id", "");
                    int serial = j + 1;

                    Item itmPTeam = cfg.inn.newItem("In_Meeting_Pteam");

                    itmPTeam.setAttribute("where", "in_parent = '" + pid + "'"
                        + " AND in_sub_id = '" + sub + "'"
                        + " AND in_sub_serial = '" + serial + "'");

                    itmPTeam.setProperty("in_type", "s");
                    itmPTeam.setProperty("in_parent", pid);
                    itmPTeam.setProperty("in_sub_sect", sect_name);
                    itmPTeam.setProperty("in_sub_id", sub);
                    itmPTeam.setProperty("in_sub_serial", serial.ToString());
                    itmPTeam.setProperty("in_muser", muid);


                    itmPTeam.setProperty("in_meeting", cfg.meeting_id);
                    itmPTeam.setProperty("source_id", itmParent.getProperty("source_id", ""));
                    itmPTeam.setProperty("in_team_index", "");
                    itmPTeam.setProperty("in_team_key", itmParent.getProperty("in_team_key", ""));

                    itmPTeam.setProperty("in_index", itmParent.getProperty("in_index", ""));
                    itmPTeam.setProperty("in_group", itmParent.getProperty("in_group", ""));
                    itmPTeam.setProperty("in_current_org", itmParent.getProperty("in_current_org", ""));
                    itmPTeam.setProperty("in_short_org", itmParent.getProperty("in_short_org", ""));
                    itmPTeam.setProperty("in_stuff_b1", itmParent.getProperty("in_stuff_b1", ""));
                    itmPTeam.setProperty("in_team", itmParent.getProperty("in_team", ""));
                    itmPTeam.setProperty("in_team_players", "");
                    itmPTeam.setProperty("in_creator_sno", itmParent.getProperty("in_creator_sno", ""));
                    itmPTeam.setProperty("in_city_no", itmParent.getProperty("in_city_no", ""));

                    itmPTeam.setProperty("in_name", itmUser.getProperty("in_name", ""));
                    itmPTeam.setProperty("in_sno", itmUser.getProperty("in_sno", ""));
                    itmPTeam.setProperty("in_names", "");
                    itmPTeam.setProperty("in_sub_weight", "");

                    Item itmChild = itmPTeam.apply("merge");
                }
            }
        }

        private Item SpecialSectionUsers(TConfig cfg, Item itmProgram, Item itmTeam)
        {
            string in_l1 = itmProgram.getProperty("in_l1", "");
            string in_l2 = itmProgram.getProperty("in_l2", "");
            string in_l3 = itmProgram.getProperty("in_l3", "");
            string in_index = itmTeam.getProperty("in_index", "");
            string in_creator_sno = itmTeam.getProperty("in_creator_sno", "");

            string sql = @"
                SELECT
	                id
                    , in_name
                    , in_sno
                    , in_gender
                FROM
	                IN_MEETING_USER WITH(NOLOCK)
                WHERE
	                source_id = '{#meeting_id}'
	                AND in_l1 = N'{#in_l1}'
	                AND in_l2 = N'{#in_l2}'
	                AND in_l3 = N'{#in_l3}'
	                AND in_index = '{#in_index}'
	                AND in_creator_sno = N'{#in_creator_sno}'
                ORDER BY
	                in_gender
	                , in_sno
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_l1}", in_l1)
                .Replace("{#in_l2}", in_l2)
                .Replace("{#in_l3}", in_l3)
                .Replace("{#in_index}", in_index)
                .Replace("{#in_creator_sno}", in_creator_sno);

            return cfg.inn.applySQL(sql);
        }

        private Item SpecialSectionSects(TConfig cfg, Item itmProgram)
        {
            string program_id = itmProgram.getProperty("id", "");

            string sql = @"
                SELECT
	                *
                FROM
	                IN_MEETING_PSECT WITH(NOLOCK)
                WHERE
	                in_program = '{#program_id}'
	                AND ISNULL(in_name, '') <> N'代表戰'
                ORDER BY
	                in_sub_id
            ";

            sql = sql.Replace("{#program_id}", program_id);

            return cfg.inn.applySQL(sql);
        }

        private Item SpecialSectionTeams(TConfig cfg, Item itmProgram)
        {
            string program_id = itmProgram.getProperty("id", "");

            string sql = @"
                SELECT
	                *
                FROM
	                IN_MEETING_PTEAM WITH(NOLOCK)
                WHERE
	                source_id = '{#program_id}'
	                AND ISNULL(in_sign_no, 0) > 0
	                AND ISNULL(in_type, '') IN ('', 'p')
                ORDER BY
	                in_sign_no
            ";

            sql = sql.Replace("{#program_id}", program_id);

            return cfg.inn.applySQL(sql);
        }

        #region 領取簽名總表

        //單位註冊單(報名單)
        private void RegisterXlsx(TConfig cfg, Item itmReturn, bool is_empty = false)
        {
            //賽事資訊
            MeetingInfo(cfg);

            //組別資訊
            ProgramInfo(cfg);

            //子場次數量
            ResetSubEvent(cfg, cfg.itmProgram);

            //團體戰子量級資訊
            var sects = GetSectList(cfg, cfg.program_id);

            //匯出資訊
            var in_name = "team_register_" + cfg.in_sub_event;
            var exp = ExportInfo(cfg, in_name);

            //試算表
            Spire.Xls.Workbook workbook = new Spire.Xls.Workbook();
            workbook.LoadFromFile(exp.template_Path);

            //取得樣板 sheet
            Spire.Xls.Worksheet sheetTemplate = workbook.Worksheets[0];

            string sub_xls_name = "";

            if (is_empty)
            {
                sub_xls_name = "空白註冊單";
                AppendRegisterSheet(cfg, sects, cfg.inn.newItem(), "註冊單", workbook, sheetTemplate);
            }
            else
            {
                sub_xls_name = "單位註冊單";

                //單位資料
                var list = GetRegisters(cfg);

                if (list == null || list.Count <= 0 || list.Count > 1)
                {
                    throw new Exception("組別單位資料錯誤");
                }

                var no = 1;
                var itmTeams = list[0].Teams;
                foreach (var itmTeam in itmTeams)
                {
                    string sheet_name = no.ToString().PadLeft(2, '0');
                    AppendRegisterSheet(cfg, sects, itmTeam, sheet_name, workbook, sheetTemplate);
                    no++;
                }
            }

            //移除樣板 sheet
            sheetTemplate.Remove();
            workbook.Worksheets[0].Activate();

            string ext_name = ".xlsx";

            string xls_name = cfg.mt_title
                + "_" + "團體戰"
                + "_" + sub_xls_name
                + "_" + cfg.itmProgram.getProperty("in_name2", "")
                + "_" + DateTime.Now.ToString("MMdd_HHmmss");

            string xls_file = exp.export_Path + "\\" + xls_name + ext_name;
            string xls_url = xls_name + ext_name;

            workbook.SaveToFile(xls_file, Spire.Xls.ExcelVersion.Version2010);

            itmReturn.setProperty("xls_name", xls_url);
        }

        private void AppendRegisterSheet(TConfig cfg, List<TSect> sects, Item itmTeam, string sheet_name, Spire.Xls.Workbook workbook, Spire.Xls.Worksheet sheetTemplate)
        {
            Spire.Xls.Worksheet sheet = workbook.CreateEmptySheet();
            sheet.CopyFrom(sheetTemplate);
            sheet.Name = sheet_name;

            string fontName1 = "標楷體";
            string fontName2 = "Times New Roman";

            string map_short_org = itmTeam.getProperty("map_short_org", "");
            string in_names = itmTeam.getProperty("in_names", "");
            string in_team = itmTeam.getProperty("in_team", "");
            if (in_team != "")
            {
                map_short_org += " " + in_team;
            }

            //賽事標題
            sheet.Range["A1"].Text = cfg.mt_title;
            sheet.Range["A1"].Style.Font.FontName = fontName1;

            //報表標題
            sheet.Range["A2"].Text = "註冊單";
            sheet.Range["A2"].Style.Font.FontName = fontName1;

            sheet.Range["A3"].Text = cfg.itmProgram.getProperty("in_name2");

            //單位
            sheet.Range["C4"].Text = map_short_org;
            sheet.Range["C4"].Style.Font.FontName = fontName1;
            //日期
            sheet.Range["H4"].Text = "";
            //報名人員
            sheet.Range["A5"].Text = in_names;

            int wsRow = 7;

            int count = sects.Count;

            for (int i = 0; i < count; i++)
            {
                var sect = sects[i];
                string in_name = sect.Value.getProperty("in_name", "");
                string in_weight = sect.Value.getProperty("in_weight", "");
                string in_ranges = sect.Value.getProperty("in_ranges", "");
                string ranges = WeightValues(in_ranges);

                //子量級
                sheet.Range["A" + wsRow].Text = in_name;
                sheet.Range["A" + wsRow].Style.Font.FontName = fontName1;

                var weight_memo = string.IsNullOrWhiteSpace(in_ranges)
                    ? in_weight
                    : in_weight + Environment.NewLine + "(" + in_ranges + ")";

                sheet.Range["B" + wsRow].Text = weight_memo;

                for (int j = 0; j < cfg.sub_event_rows; j++)
                {
                    sheet.Range["G" + wsRow].Text = ranges;
                    sheet.Range["G" + wsRow].Style.Font.FontName = fontName2;
                    wsRow++;
                }
                wsRow++;
            }

            //SetRangeBorder(sheet, "A" + wsRowS + ":" + "E" + wsRowC);
        }

        private string WeightValues(string value)
        {
            string[] arr = value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            if (arr != null && arr.Length > 0)
            {
                return string.Join(", ", arr.ToList().Select(x => "□" + x));
            }
            else
            {
                return value;
            }
        }

        //領取簽名總表
        private void SignNameXlsx(TConfig cfg, Item itmReturn)
        {
            //賽事資訊
            MeetingInfo(cfg);

            //組別單位資料
            var list = GetRegisters(cfg);

            //匯出資訊
            var exp = ExportInfo(cfg, "team_sign");

            //試算表
            Spire.Xls.Workbook workbook = new Spire.Xls.Workbook();
            workbook.LoadFromFile(exp.template_Path);

            //取得樣板 sheet
            Spire.Xls.Worksheet sheetTemplate = workbook.Worksheets[0];

            foreach (var entity in list)
            {
                AppendSignNameSheet(cfg, entity, workbook, sheetTemplate);
            }

            //移除樣板 sheet
            sheetTemplate.Remove();

            string ext_name = ".xlsx";

            string xls_name = cfg.mt_title
                + "_" + "團體戰"
                + "_" + "報名單"
                + "_" + "領取簽名總表"
                + "_" + DateTime.Now.ToString("MMdd_HHmmss");

            string xls_file = exp.export_Path + "\\" + xls_name + ext_name;
            string xls_url = xls_name + ext_name;

            workbook.SaveToFile(xls_file, Spire.Xls.ExcelVersion.Version2010);

            itmReturn.setProperty("xls_name", xls_url);
        }

        //附加領取簽名總表
        private void AppendSignNameSheet(TConfig cfg, TProgram program, Spire.Xls.Workbook workbook, Spire.Xls.Worksheet sheetTemplate)
        {
            Spire.Xls.Worksheet sheet = workbook.CreateEmptySheet();
            sheet.CopyFrom(sheetTemplate);
            sheet.Name = program.ShortName;

            sheet.Range["A1"].Text = cfg.mt_title;
            sheet.Range["A2"].Text = "註冊單領取簽名表";

            int wsRow = 4;

            int count = program.Teams.Count;

            for (int i = 0; i < count; i++)
            {
                Item item = program.Teams[i];
                string map_short_org = item.getProperty("map_short_org", "");
                string in_team = item.getProperty("in_team", "");
                if (in_team != "")
                {
                    map_short_org += " " + in_team;
                }

                sheet.Range["A" + (wsRow + i)].Text = (i + 1).ToString();
                sheet.Range["B" + (wsRow + i)].Text = program.ProgramName;
                sheet.Range["C" + (wsRow + i)].Text = map_short_org;
                sheet.SetRowHeight(wsRow + i, 30);
            }

            SetRangeBorder(sheet, "A" + wsRow + ":" + "E" + (wsRow + count - 1));
        }

        private void SetRangeBorder(Spire.Xls.Worksheet sheet, string pos)
        {
            var range = sheet.Range[pos];
            range.BorderInside(Spire.Xls.LineStyleType.Thin, System.Drawing.Color.Black);
            range.BorderAround(Spire.Xls.LineStyleType.Thin, System.Drawing.Color.Black);
        }

        //賽事資訊
        private void MeetingInfo(TConfig cfg)
        {
            string sql = "SELECT id, in_title FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'";
            cfg.itmMeeting = cfg.inn.applySQL(sql);
            if (cfg.itmMeeting.isError() || cfg.itmMeeting.getResult() == "")
            {
                throw new Exception("賽事資料錯誤");
            }

            cfg.mt_title = cfg.itmMeeting.getProperty("in_title", "");
        }

        //組別資訊
        private void ProgramInfo(TConfig cfg)
        {
            string sql = "SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + cfg.program_id + "'";
            cfg.itmProgram = cfg.inn.applySQL(sql);
            if (cfg.itmProgram.isError() || cfg.itmProgram.getResult() == "")
            {
                throw new Exception("組別資料錯誤");
            }
        }

        //取得團體戰-註冊資料
        private List<TProgram> GetRegisters(TConfig cfg)
        {
            string condition = cfg.scene == "sign_xlsx"
                ? ""
                : "AND t1.id = '" + cfg.program_id + "'";

            string sql = @"
                SELECT 
	                t1.id				AS 'pg_id'
	                , t1.in_name2		AS 'pg_name'
	                , t1.in_short_name	AS 'pg_short_name'
	                , t1.in_fight_day	AS 'pg_fight_day'
	                , t2.map_short_org
	                , t2.in_team
	                , t2.in_names
                FROM 
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK) 
                INNER JOIN
	                VU_MEETING_PTEAM t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                WHERE 
	                t1.in_meeting = '{#meeting_id}' 
	                AND t1.in_l1 = N'團體組' 
	                AND ISNULL(t2.in_type, '') IN ('', 'p')
                    {#condition}
                ORDER BY 
	                t1.in_fight_day
	                , t1.in_sort_order
	                , t2.in_city_no
	                , t2.in_stuff_b1
	                , t2.in_team
	                , t2.in_sno
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#condition}", condition);

            Item itmPTeams = cfg.inn.applySQL(sql);
            if (itmPTeams.isError() || itmPTeams.getResult() == "")
            {
                throw new Exception("賽程隊伍資料錯誤");
            }

            //轉換型別
            return MapGroupProgram(cfg, itmPTeams);
        }

        private List<TProgram> MapGroupProgram(TConfig cfg, Item items)
        {
            string mt_title = cfg.itmMeeting.getProperty("in_title", "");

            var list = new List<TProgram>();

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string pg_id = item.getProperty("pg_id", "");
                string pg_name = item.getProperty("pg_name", "");
                string pg_short_name = item.getProperty("pg_short_name", "");
                string pg_fight_day = item.getProperty("pg_fight_day", "");
                var entity = list.Find(x => x.ProgramId == pg_id);

                if (entity == null)
                {
                    entity = new TProgram
                    {
                        ProgramId = pg_id,
                        ProgramName = pg_name,
                        ShortName = pg_short_name,
                        FightDay = pg_fight_day,
                        Teams = new List<Item>(),
                    };
                    list.Add(entity);
                }

                entity.Teams.Add(item);
            }

            return list;
        }

        private class TProgram
        {
            public string ProgramId { get; set; }
            public string ProgramName { get; set; }
            public string ShortName { get; set; }
            public string FightDay { get; set; }
            public List<Item> Teams { get; set; }
        }


        #endregion 領取簽名總表

        //體重
        private void UpdateWeight(TConfig cfg, Item itmReturn)
        {
            string muid = itmReturn.getProperty("muid", "");//in_meeting_user id
            string pid = itmReturn.getProperty("pid", "");//team parent id
            string sub = itmReturn.getProperty("sub", "");
            string serial = itmReturn.getProperty("serial", "");

            bool need_qry = false;
            string value = itmReturn.getProperty("value", "");
            string value_d = itmReturn.getProperty("value", "");
            string value_s = itmReturn.getProperty("value", "");

            if (value_d == "")
            {
                value_d = "NULL";
            }

            if (value_s == "")
            {
                value_s = "NULL";
            }
            else
            {
                need_qry = true;
                value_s = "'" + value_s + "'";
            }

            Item itmMUser = cfg.inn.applySQL("SELECT id, in_team_weight_n1 FROM IN_MEETING_USER (NOLOCK) WHERE id = '" + muid + "'");
            if (itmMUser.isError() || itmMUser.getResult() == "")
            {
                throw new Exception("查無與會者");
            }

            string in_team_weight_n1 = itmMUser.getProperty("in_team_weight_n1", "");
            string in_team_weight_n2 = "";
            string in_team_weight_n3 = "";
            if (need_qry)
            {
                string sql_qry = "SELECT TOP 1 * FROM In_Group_Weight (NOLOCK)"
                    + " WHERE in_group = '" + in_team_weight_n1 + "'"
                    + " AND in_min <= " + value
                    + " AND in_max >= " + value
                    ;
                Item itmWeight = cfg.inn.applySQL(sql_qry);
                if (!itmWeight.isError() && itmWeight.getResult() != "")
                {
                    in_team_weight_n2 = itmWeight.getProperty("in_level", "");//級
                    in_team_weight_n3 = itmWeight.getProperty("in_weight", "");//量
                }
            }

            string in_weight_time = System.DateTime.Now.AddHours(-8).ToString("yyyy/MM/dd HH:mm:ss");

            string sql = "UPDATE IN_MEETING_PTEAM SET"
                + " in_weight_value = " + value_s
                + " WHERE in_parent = '" + pid + "'"
                + " AND in_sub_id = '" + sub + "'"
                + " AND in_sub_serial = '" + serial + "'";

            Item itmSQL = cfg.inn.applySQL(sql);

            if (itmSQL.isError())
            {
                throw new Exception("體重 更新失敗");
            }

            sql = "UPDATE IN_MEETING_USER SET"
                + " in_weight = " + value_d
                + ", in_weight_createid = '" + cfg.strUserId + "'"
                + ", in_weight_createname = N'" + cfg.strUserName + "'"
                + ", in_weight_time = N'" + in_weight_time + "'"
                + ", in_team_weight_n2 = N'" + in_team_weight_n2 + "'"
                + ", in_team_weight_n3 = N'" + in_team_weight_n3 + "'"
                + " WHERE id = '" + muid + "'";

            itmSQL = cfg.inn.applySQL(sql);

            if (itmSQL.isError())
            {
                throw new Exception("體重 更新失敗(與會者)");
            }
        }

        //過磅狀態
        private void UpdateState(TConfig cfg, Item itmReturn)
        {
            string pid = itmReturn.getProperty("pid", "");//team parent id
            string sub = itmReturn.getProperty("sub", "");
            string serial = itmReturn.getProperty("serial", "");
            string state = itmReturn.getProperty("state", "");

            string sql = "UPDATE IN_MEETING_PTEAM SET"
                + " in_weight_message = N'" + state + "'"
                + " WHERE in_parent = '" + pid + "'"
                + " AND in_sub_id = '" + sub + "'"
                + " AND in_sub_serial = '" + serial + "'";

            Item itmSQL = cfg.inn.applySQL(sql);

            if (itmSQL.isError())
            {
                throw new Exception("過磅狀態 更新失敗");
            }
        }

        //原個人參賽量級
        private void UpdateSect(TConfig cfg, Item itmReturn)
        {
            string pid = itmReturn.getProperty("pid", "");//team parent id
            string sub = itmReturn.getProperty("sub", "");
            string serial = itmReturn.getProperty("serial", "");
            string weight = itmReturn.getProperty("weight", "");

            string sql = "UPDATE IN_MEETING_PTEAM SET"
                + " in_sub_weight = N'" + weight + "'"
                + " WHERE in_parent = '" + pid + "'"
                + " AND in_sub_id = '" + sub + "'"
                + " AND in_sub_serial = '" + serial + "'";

            Item itmSQL = cfg.inn.applySQL(sql);

            if (itmSQL.isError())
            {
                throw new Exception("原個人參賽量級 更新失敗");
            }
        }

        #region 團體過磅單

        //過磅單 Excel
        private void WeightXlsx(TConfig cfg, Item itmReturn)
        {
            string sql = "";

            sql = "SELECT id, in_title FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'";
            cfg.itmMeeting = cfg.inn.applySQL(sql);

            sql = "SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + cfg.program_id + "'";
            cfg.itmProgram = cfg.inn.applySQL(sql);

            if (cfg.itmMeeting.isError() || cfg.itmMeeting.getResult() == ""
                || cfg.itmProgram.isError() || cfg.itmProgram.getResult() == "")
            {
                throw new Exception("賽程組別資料錯誤");
            }

            //子場次數量
            ResetSubEvent(cfg, cfg.itmProgram);

            itmReturn.setProperty("program_id", cfg.itmProgram.getProperty("id", ""));

            var itmMUsers = default(Item);
            var itmMTeams = default(Item);
            var is_one_team = cfg.team_id != "";

            //主隊伍資料
            if (is_one_team)
            {
                itmMTeams = GetMTeams(cfg, cfg.team_id);
                if (itmMTeams.isError() || itmMTeams.getItemCount() < 1)
                {
                    throw new Exception("單位資料錯誤");
                }

                var itmMain = itmMTeams.getItemByIndex(0);
                var in_index = itmMain.getProperty("in_index", "");
                var in_creator_sno = itmMain.getProperty("in_creator_sno", "");

                //與會者資料
                itmMUsers = GetMUsers(cfg, in_index, in_creator_sno);
            }
            else
            {
                //隊伍資料
                itmMTeams = GetMTeams(cfg, "");
                if (itmMTeams.isError() || itmMTeams.getItemCount() < 1)
                {
                    throw new Exception("單位資料錯誤");
                }

                //與會者資料
                itmMUsers = GetMUsers(cfg, "", "");
            }

            if (itmMUsers.isError() || itmMUsers.getItemCount() < 1)
            {
                throw new Exception("與會者資料錯誤");
            }

            //與會者隊伍
            var rows = RowsFromMUser(cfg, itmMUsers);

            if (rows.Count <= 0)
            {
                throw new Exception("隊伍資料錯誤");
            }

            //隊伍查找表
            var map = GetTeamMap(cfg, itmMTeams);

            //隊伍匹配
            MapPTeam(cfg, rows, map);

            //生成控件
            GenerateCtrls(cfg, rows);

            //子場次量級
            var sects = GetSectList(cfg, cfg.program_id);

            //匯出資訊
            var xls_name = "team_weight_" + cfg.in_sub_event;
            var exp = ExportInfo(cfg, xls_name);

            //試算表
            Spire.Xls.Workbook workbook = new Spire.Xls.Workbook();
            workbook.LoadFromFile(exp.template_Path);

            //取得樣板 sheet
            Spire.Xls.Worksheet sheetTemplate = workbook.Worksheets[0];

            //團體過磅單
            for (int i = 0; i < rows.Count; i++)
            {
                var row = rows[i];

                Spire.Xls.Worksheet sheet = workbook.CreateEmptySheet();
                sheet.CopyFrom(sheetTemplate);

                if (row.TeamName != "")
                {
                    sheet.Name = row.OrgName + row.TeamName;
                }
                else
                {
                    sheet.Name = row.OrgName;
                }

                WeightXlsx(cfg, row, sects, sheet, cfg.sub_event_rows, itmReturn);
            }

            workbook.Worksheets.RemoveAt(0);

            string mt_title = cfg.itmMeeting.getProperty("in_title", "");
            string pg_title = cfg.itmMeeting.getProperty("in_name2", "");

            string xlsName = "";

            if (cfg.team_id == "")
            {
                xlsName = mt_title + "_" + pg_title + "_" + "全部單位";
            }
            else
            {
                xlsName = mt_title + "_" + pg_title + "_" + rows[0].OrgName + "_" + rows[0].TeamName;
            }

            string xls_file = exp.export_Path + xlsName + ".xlsx";

            workbook.SaveToFile(xls_file, Spire.Xls.ExcelVersion.Version2010);
            itmReturn.setProperty("xls_name", xlsName + ".xlsx");
        }

        //團體過磅單 Xlsx
        private void WeightXlsx(TConfig cfg, TRow row, List<TSect> sects, Spire.Xls.Worksheet sheet, int dfCount, Item itmReturn)
        {
            var players = row.Children;

            sheet.Range["A1"].Text = cfg.itmMeeting.getProperty("in_title", "");//18
            sheet.Range["A3"].Text = cfg.itmProgram.getProperty("in_name2", "");//16

            sheet.Range["C4"].Text = row.OrgName + row.TeamName;
            sheet.Range["H4"].Text = "";//cfg.itmProgram.getProperty("in_weight_day", "");

            int wsRow = 7;

            for (int i = 0; i < sects.Count; i++)
            {
                var sect = sects[i];
                var sect_players = players.FindAll(x => x.sub_id == sect.sub_id);

                string sect_name = sect.Value.getProperty("in_name", "");
                string in_weight = sect.Value.getProperty("in_weight", "");
                string in_ranges = sect.Value.getProperty("in_ranges", "");

                sheet.Range["A" + wsRow].Text = sect_name;

                if (string.IsNullOrWhiteSpace(in_ranges))
                {
                    sheet.Range["B" + wsRow].Text = in_weight;
                }
                else
                {
                    sheet.Range["B" + wsRow].Text = in_weight + Environment.NewLine + "(" + in_ranges + ")";
                }

                int mxCount = dfCount;
                for (int j = 0; j < sect_players.Count; j++)
                {
                    var p = sect_players[j];
                    var v = GetPlayerDisplay(p);
                    sheet.Range["D" + wsRow].Text = v;
                    wsRow++;
                    mxCount--;
                }

                for (int j = 0; j < mxCount; j++)
                {
                    var p = GetEmptyPlayer(cfg);
                    var v = GetPlayerDisplay(p);
                    sheet.Range["D" + wsRow].Text = v;
                    wsRow++;
                }

                wsRow++;
            }
        }

        private string GetPlayerDisplay(TMTeam p)
        {
            string result = "";
            if (p.in_name != "")
            {
                if (p.in_sub_weight != "")
                {
                    result = p.in_name + "(" + p.in_sub_weight + ")";
                }
                else
                {
                    result = p.in_name;
                }
            }
            return result;
        }

        ClosedXML.Excel.XLColor head_bg_color = ClosedXML.Excel.XLColor.FromHtml("#295C90");

        private void SetCell(ClosedXML.Excel.IXLCell cell, string title, int fontSize = 14)
        {
            cell.Value = title;

            //cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
            cell.Style.Font.Bold = true;
            cell.DataType = ClosedXML.Excel.XLDataType.Text;
            cell.Style.Font.SetFontSize(fontSize);
            cell.Style.Font.FontName = "標楷體";

            //cell.Style.Fill.BackgroundColor = head_bg_color;
            //cell.Style.Font.FontColor = ClosedXML.Excel.XLColor.White;
            // cell.Style.Border.SetRightBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            // cell.Style.Border.SetTopBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            // cell.Style.Border.SetBottomBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            // cell.Style.Border.SetLeftBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
        }

        private void SetNumCell(ClosedXML.Excel.IXLCell cell, string title, int fontSize = 14)
        {
            cell.Value = title;

            //cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
            cell.Style.Font.Bold = true;
            cell.DataType = ClosedXML.Excel.XLDataType.Text;
            cell.Style.Font.SetFontSize(fontSize);
            cell.Style.Font.FontName = "Times New Roman";

            //cell.Style.Fill.BackgroundColor = head_bg_color;
            //cell.Style.Font.FontColor = ClosedXML.Excel.XLColor.White;
            // cell.Style.Border.SetRightBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            // cell.Style.Border.SetTopBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            // cell.Style.Border.SetBottomBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            // cell.Style.Border.SetLeftBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
        }

        private TExport ExportInfo(TConfig cfg, string in_name)
        {
            Item itmXls = cfg.inn.applyMethod("In_Meeting_Excel_Path", "<in_name>" + in_name + "</in_name>");
            string Export_Path = itmXls.getProperty("export_path", "");
            string Template_Path = itmXls.getProperty("template_path", "");

            TExport result = new TExport
            {
                template_Path = Template_Path,
                export_Path = Export_Path,
            };

            return result;
        }

        private class TExport
        {
            public string template_Path { get; set; }
            public string export_Path { get; set; }
        }
        #endregion 團體過磅單

        //過磅單 Table
        private void WeightTable(TConfig cfg, Item itmReturn)
        {
            string sql = "";

            sql = "SELECT id, in_title FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'";
            cfg.itmMeeting = cfg.inn.applySQL(sql);

            sql = "SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + cfg.program_id + "'";
            cfg.itmProgram = cfg.inn.applySQL(sql);

            if (cfg.itmMeeting.isError() || cfg.itmMeeting.getResult() == ""
                || cfg.itmProgram.isError() || cfg.itmProgram.getResult() == "")
            {
                throw new Exception("賽程組別資料錯誤");
            }

            itmReturn.setProperty("program_id", cfg.itmProgram.getProperty("id", ""));

            //主隊伍資料
            var itmMTeams = GetMTeams(cfg, cfg.team_id);
            if (itmMTeams.isError() || itmMTeams.getItemCount() < 1)
            {
                throw new Exception("單位資料錯誤");
            }

            var itmMain = itmMTeams.getItemByIndex(0);
            string in_index = itmMain.getProperty("in_index", "");
            string in_creator_sno = itmMain.getProperty("in_creator_sno", "");

            //與會者資料
            var itmMUsers = GetMUsers(cfg, in_index, in_creator_sno);
            //與會者隊伍
            var rows = RowsFromMUser(cfg, itmMUsers);

            if (rows.Count != 1)
            {
                throw new Exception("隊伍資料錯誤");
            }

            //隊伍查找表
            var map = GetTeamMap(cfg, itmMTeams);

            //隊伍匹配
            MapPTeam(cfg, rows, map);

            //生成控件
            GenerateCtrls(cfg, rows);

            //子場次量級
            var sects = GetSectList(cfg, cfg.program_id);

            WeightTable(cfg, rows[0], sects, itmReturn);
        }

        //過磅單 Table
        private void WeightTable(TConfig cfg, TRow row, List<TSect> sects, Item itmReturn)
        {
            StringBuilder builder = new StringBuilder();

            builder.Append("<div class='showsheet_ clearfix'>");
            builder.Append("    <div>");
            builder.Append("        <div class='float-btn clearfix'>");
            builder.Append("        </div>");

            List<TField> fields = new List<TField>();
            fields.Add(new TField { title = "級組", colspan = 2 });
            fields.Add(new TField { title = "選手姓名", colspan = 1 });
            fields.Add(new TField { title = "參賽量級", colspan = 1 });
            fields.Add(new TField { title = "實際體重", colspan = 1 });
            fields.Add(new TField { title = "過磅狀態", colspan = 1 });
            fields.Add(new TField { title = "選手簽名", colspan = 1 });
            fields.Add(new TField { title = "備註", colspan = 1 });

            StringBuilder body = new StringBuilder();

            body.Append("<thead>");
            body.Append("  <tr>");
            for (int i = 0; i < fields.Count; i++)
            {
                var field = fields[i];
                body.Append("    <th class='default-td' colspan='" + field.colspan + "' >");
                body.Append(field.title);
                body.Append("    </th>");
            }
            body.Append("  </tr>");
            body.Append("</thead>");

            var players = row.Children;

            body.Append("<tbody>");
            for (int i = 0; i < sects.Count; i++)
            {
                var sect = sects[i];
                var sect_players = players.FindAll(x => x.sub_id == sect.sub_id);
                if (sect_players == null) continue;

                if (sect_players.Count > 7)
                {
                    throw new Exception("超過系統預設數量");
                }

                var sect_name = sect.Value.getProperty("in_name", "");
                var sect_weight = sect.Value.getProperty("in_weight", "");
                var sect_ranges = sect.Value.getProperty("in_ranges", "");

                var name = sect_name;
                var display = "";

                if (sect.gender_display != "")
                {
                    if (string.IsNullOrWhiteSpace(sect_ranges))
                    {
                        display = sect_weight;
                    }
                    else
                    {
                        display = sect_weight
                            + "<br>"
                            + "(" + sect_ranges + ")";
                    }
                }

                body.Append("  <tr>");

                body.Append("    <td rowspan='7' class='gender-td'>");
                body.Append(name);
                body.Append("    </td>");

                body.Append("    <td rowspan='7' class='weight-td'>");
                body.Append(display);
                body.Append("    </td>");

                var p1 = sect_players.Find(x => x.sub_serial == 1);
                var p2 = sect_players.Find(x => x.sub_serial == 2);
                var p3 = sect_players.Find(x => x.sub_serial == 3);
                var p4 = sect_players.Find(x => x.sub_serial == 4);
                var p5 = sect_players.Find(x => x.sub_serial == 5);
                var p6 = sect_players.Find(x => x.sub_serial == 6);
                var p7 = sect_players.Find(x => x.sub_serial == 7);

                if (p1 == null) p1 = GetEmptyPlayer(cfg);
                if (p2 == null) p2 = GetEmptyPlayer(cfg);
                if (p3 == null) p3 = GetEmptyPlayer(cfg);
                if (p4 == null) p4 = GetEmptyPlayer(cfg);
                if (p5 == null) p5 = GetEmptyPlayer(cfg);
                if (p6 == null) p6 = GetEmptyPlayer(cfg);
                if (p7 == null) p7 = GetEmptyPlayer(cfg);

                var select1 = GetSelectCtrl(cfg, sect, row, 1);
                var select2 = GetSelectCtrl(cfg, sect, row, 2);
                var select3 = GetSelectCtrl(cfg, sect, row, 3);
                var select4 = GetSelectCtrl(cfg, sect, row, 4);
                var select5 = GetSelectCtrl(cfg, sect, row, 5);
                var select6 = GetSelectCtrl(cfg, sect, row, 6);
                var select7 = GetSelectCtrl(cfg, sect, row, 7);

                body.Append("    <td rowspan='1' class='default-td'>");
                body.Append(select1);
                body.Append("    </td>");
                body.Append("    <td rowspan='1' class='default-td'>" + GetCheckBoxList(sect_ranges, sect.sub_id, 1, p1.in_sub_weight) + "</td>");
                body.Append("    <td rowspan='1' class='weight-td'>" + GetWeightInput(p1.in_weight_value) + "</td>");
                body.Append("    <td rowspan='1' class='default-td'>" + GetStateMenu(p1.in_weight_message) + "</td>");
                body.Append("    <td rowspan='1' class='default-td'>&nbsp;</td>");
                body.Append("    <td rowspan='1' class='default-td'>&nbsp;</td>");
                body.Append("  </tr>");

                body.Append("  <tr class=''>");
                body.Append("    <td rowspan='1' class='default-td'>");
                body.Append(select2);
                body.Append("    </td>");
                body.Append("    <td rowspan='1' class='default-td'>" + GetCheckBoxList(sect_ranges, sect.sub_id, 2, p2.in_sub_weight) + "</td>");
                body.Append("    <td rowspan='1' class='weight-td'>" + GetWeightInput(p2.in_weight_value) + "</td>");
                body.Append("    <td rowspan='1' class='default-td'>" + GetStateMenu(p2.in_weight_message) + "</td>");
                body.Append("    <td rowspan='1' class='default-td'>&nbsp;</td>");
                body.Append("    <td rowspan='1' class='default-td'>&nbsp;</td>");
                body.Append("  </tr>");

                body.Append("  <tr class=''>");
                body.Append("    <td rowspan='1' class='default-td'>");
                body.Append(select3);
                body.Append("    </td>");
                body.Append("    <td rowspan='1' class='default-td'>" + GetCheckBoxList(sect_ranges, sect.sub_id, 3, p3.in_sub_weight) + "</td>");
                body.Append("    <td rowspan='1' class='weight-td'>" + GetWeightInput(p3.in_weight_value) + "</td>");
                body.Append("    <td rowspan='1' class='default-td'>" + GetStateMenu(p3.in_weight_message) + "</td>");
                body.Append("    <td rowspan='1' class='default-td'>&nbsp;</td>");
                body.Append("    <td rowspan='1' class='default-td'>&nbsp;</td>");
                body.Append("  </tr>");

                body.Append("  <tr class=''>");
                body.Append("    <td rowspan='1' class='default-td'>");
                body.Append(select4);
                body.Append("    </td>");
                body.Append("    <td rowspan='1' class='default-td'>" + GetCheckBoxList(sect_ranges, sect.sub_id, 4, p4.in_sub_weight) + "</td>");
                body.Append("    <td rowspan='1' class='weight-td'>" + GetWeightInput(p4.in_weight_value) + "</td>");
                body.Append("    <td rowspan='1' class='default-td'>" + GetStateMenu(p4.in_weight_message) + "</td>");
                body.Append("    <td rowspan='1' class='default-td'>&nbsp;</td>");
                body.Append("    <td rowspan='1' class='default-td'>&nbsp;</td>");
                body.Append("  </tr>");

                body.Append("  <tr class=''>");
                body.Append("    <td rowspan='1' class='default-td'>");
                body.Append(select5);
                body.Append("    </td>");
                body.Append("    <td rowspan='1' class='default-td'>" + GetCheckBoxList(sect_ranges, sect.sub_id, 5, p5.in_sub_weight) + "</td>");
                body.Append("    <td rowspan='1' class='weight-td'>" + GetWeightInput(p5.in_weight_value) + "</td>");
                body.Append("    <td rowspan='1' class='default-td'>" + GetStateMenu(p5.in_weight_message) + "</td>");
                body.Append("    <td rowspan='1' class='default-td'>&nbsp;</td>");
                body.Append("    <td rowspan='1' class='default-td'>&nbsp;</td>");
                body.Append("  </tr>");

                body.Append("  <tr class=''>");
                body.Append("    <td rowspan='1' class='default-td'>");
                body.Append(select6);
                body.Append("    </td>");
                body.Append("    <td rowspan='1' class='default-td'>" + GetCheckBoxList(sect_ranges, sect.sub_id, 6, p6.in_sub_weight) + "</td>");
                body.Append("    <td rowspan='1' class='weight-td'>" + GetWeightInput(p6.in_weight_value) + "</td>");
                body.Append("    <td rowspan='1' class='default-td'>" + GetStateMenu(p6.in_weight_message) + "</td>");
                body.Append("    <td rowspan='1' class='default-td'>&nbsp;</td>");
                body.Append("    <td rowspan='1' class='default-td'>&nbsp;</td>");
                body.Append("  </tr>");

                body.Append("  <tr class='weight_row'>");
                body.Append("    <td rowspan='1' class='default-td'>");
                body.Append(select7);
                body.Append("    </td>");
                body.Append("    <td rowspan='1' class='default-td'>" + GetCheckBoxList(sect_ranges, sect.sub_id, 7, p7.in_sub_weight) + "</td>");
                body.Append("    <td rowspan='1' class='weight-td'>" + GetWeightInput(p7.in_weight_value) + "</td>");
                body.Append("    <td rowspan='1' class='default-td'>" + GetStateMenu(p7.in_weight_message) + "</td>");
                body.Append("    <td rowspan='1' class='default-td'>&nbsp;</td>");
                body.Append("    <td rowspan='1' class='default-td'>&nbsp;</td>");
                body.Append("  </tr>");
            }

            body.Append("</tbody>");


            string tb_id = "team_weight_table";

            builder.Append("<table id='" + tb_id + "'"
                + " class='table table-bordered table-rwd rwd'"
                + " style='background-color: #fff;' "
                + " data-toggle='table' "
                + " data-search='false'"
                + " data-search-align='left'"
                + ">");

            builder.Append(body);
            builder.Append("</table>");

            //builder.Append("<script>");
            //builder.Append("    $('#" + tb_id + "').bootstrapTable({});");
            //builder.Append("    if($(window).width() <= 768) { $('#" + tb_id + "').bootstrapTable('toggleView'); }");
            //builder.Append("</script>");

            builder.Append("    </div>");

            builder.Append("</div>");

            itmReturn.setProperty("inn_tabs", builder.ToString());
        }

        private string GetWeightInput(string value)
        {
            return "<input data-value='' style='width:100%' class='eventKD' type='number' value='" + value + "'>";
        }

        private string GetStateMenu(string value)
        {
            return "<select class='form-control btn btn-default state-select'"
                + " data-wv='" + value + "'"
                + " onchange='PlayerState_Change(this)' >"
                + "  <option value='請選擇'>--</option>"
                + "  <option value='(請假)'>請假</option>"
                + "  <option value='(未到)'>未到</option>"
                + "  <option value='(DQ)'>DQ</option>"
                + "  <option value=''>過磅合格</option>"
                + "<select>";
        }

        private string GetCheckBoxList(string value, int sub_id, int serial, string wv)
        {
            string result = "";
            //var ws = value + ",無,未";
            var ws = value;
            var arr = ws.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            if (arr == null || arr.Length == 0) return "&nbsp;";

            string weight_id = "wt" + "_" + sub_id + "_" + serial;

            result += "<div class='row'>";
            foreach (var weight in arr)
            {
                // result += "<label class='cbx-ctrl' >"
                //     + "<input type='checkbox' class='checkbox cbx-ctrl' checked /> "
                //     + weight
                //     + "</label>";

                result += "<span class='span-btn' onclick='SetWeight(this)' style='margin-right: 5px'"
                + " data-weight='" + weight + "'"
                + " data-wv='" + wv + "'"
                + " data-tid='" + weight_id + "'>" + weight + "</span>";
            }

            result += "</div>";
            return result;
        }


        //過磅單 Page
        private void WeightPage(TConfig cfg, Item itmReturn)
        {
            string sql = "";

            sql = "SELECT id, in_title FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'";
            cfg.itmMeeting = cfg.inn.applySQL(sql);

            if (cfg.itmMeeting.isError() || cfg.itmMeeting.getResult() == "")
            {
                throw new Exception("賽程組別資料錯誤");
            }
            itmReturn.setProperty("in_title", cfg.itmMeeting.getProperty("in_title", ""));

            AppendProgramMenu(cfg, itmReturn);

            if (cfg.program_id != "")
            {
                cfg.itmProgram = cfg.inn.applySQL("SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + cfg.program_id + "'");

                string program_id = cfg.itmProgram.getProperty("id", "");
                itmReturn.setProperty("program_id", program_id);

                sql = "SELECT id, map_short_org, in_team FROM VU_MEETING_PTEAM WITH(NOLOCK)"
                     + " WHERE source_id = '" + program_id + "'"
                     + " AND ISNULL(in_type, '') IN ('', 'p')"
                     + " ORDER BY in_stuff_b1, in_team, in_sno";

                Item itmTeams = cfg.inn.applySQL(sql);
                int team_count = itmTeams.getItemCount();

                Item itmEmpty = cfg.inn.newItem();
                itmEmpty.setType("inn_team");
                itmEmpty.setProperty("value", "");
                itmEmpty.setProperty("text", "請選擇");
                itmReturn.addRelationship(itmEmpty);

                for (int i = 0; i < team_count; i++)
                {
                    Item itmTeam = itmTeams.getItemByIndex(i);
                    string map_short_org = itmTeam.getProperty("map_short_org", "");
                    string in_team = itmTeam.getProperty("in_team", "");

                    itmTeam.setType("inn_team");
                    itmTeam.setProperty("value", itmTeam.getProperty("id", ""));

                    if (in_team != "")
                    {
                        itmTeam.setProperty("text", map_short_org + " " + in_team + "隊");
                    }
                    else
                    {
                        itmTeam.setProperty("text", map_short_org);
                    }

                    itmReturn.addRelationship(itmTeam);
                }

                itmReturn.setProperty("hide_team", "");
            }
            else
            {
                itmReturn.setProperty("hide_team", "item_show_0");
            }
        }

        //清空名單
        private void Remove(TConfig cfg, Item itmReturn)
        {
            if (cfg.program_id == "")
            {
                throw new Exception("組別 id 不可為空白");
            }

            string sql = "DELETE FROM IN_MEETING_PTEAM WHERE source_id = '" + cfg.program_id + "' AND in_type = 's'";
            Item itmDelete = cfg.inn.applySQL(sql);
            if (itmDelete.isError())
            {
                throw new Exception("清空團體賽名單 發生錯誤");
            }

            sql = "UPDATE t1 SET"
                + " in_player_org = NULL"
                + " , in_player_team = NULL"
                + " , in_player_name = NULL"
                + " , in_player_sno = NULL"
                + " FROM In_Meeting_PEvent_Detail t1 WITH(NOLOCK)"
                + " INNER JOIN In_Meeting_PEvent t2 WITH(NOLOCK)"
                + "     ON t2.id = t1.source_id"
                + " WHERE t2.source_id = '" + cfg.program_id + "' AND t2.in_type = 's'";

            itmDelete = cfg.inn.applySQL(sql);
            if (itmDelete.isError())
            {
                throw new Exception("清空團體賽名單(2) 發生錯誤");
            }
        }

        //儲存
        private void Save(TConfig cfg, Item itmReturn)
        {
            string meeting_id = itmReturn.getProperty("meeting_id", "");
            string program_id = itmReturn.getProperty("program_id", "");
            string muid = itmReturn.getProperty("muid", "");
            string pid = itmReturn.getProperty("pid", "");//parent id
            string cid = itmReturn.getProperty("cid", "");//child id
            string sub = itmReturn.getProperty("sub", "");
            string serial = itmReturn.getProperty("serial", "");

            string sql = "";

            sql = "SELECT in_current_org, in_team, in_name, in_sno, in_index FROM IN_MEETING_USER WITH(NOLOCK) WHERE id = '" + muid + "'";
            Item itmMUser = null;

            if (muid != "")
            {
                itmMUser = cfg.inn.applySQL(sql);
            }

            if (itmMUser == null || itmMUser.isError() || itmMUser.getResult() == "")
            {
                //throw new Exception("查無與會者資料");
                itmMUser = cfg.inn.newItem();
            }

            sql = "SELECT * FROM IN_MEETING_PSECT WITH(NOLOCK)"
                + " WHERE in_program = '" + program_id + "'"
                + " AND in_sub_id = '" + sub + "'";

            Item itmSect = cfg.inn.applySQL(sql);
            if (itmSect.isError() || itmSect.getResult() == "")
            {
                //throw new Exception("查無量級資料");
                itmSect = cfg.inn.newItem();
            }

            string sect_name = itmSect.getProperty("in_name", "");


            sql = "SELECT * FROM IN_MEETING_PTEAM WITH(NOLOCK) WHERE id = '" + pid + "'";
            Item itmParent = cfg.inn.applySQL(sql);
            if (itmParent.isError() || itmParent.getResult() == "")
            {
                throw new Exception("查無隊伍資料");
            }
            if (muid == "")
            {
                string sql_delete = "DELETE FROM In_Meeting_Pteam"
                    + " WHERE in_parent = '" + pid + "'"
                    + " AND in_type = 's'"
                    + " AND in_sub_id = '" + sub + "'"
                    + " AND in_sub_serial = '" + serial + "'"
                    + "";

                Item itmDelete = cfg.inn.applySQL(sql_delete);

                if (itmDelete.isError())
                {
                    throw new Exception("更新發生錯誤");
                }
            }
            else
            {
                Item itmPTeam = cfg.inn.newItem("In_Meeting_Pteam");

                itmPTeam.setAttribute("where", "in_parent = '" + pid + "'"
                    + " AND in_sub_id = '" + sub + "'"
                    + " AND in_sub_serial = '" + serial + "'");

                itmPTeam.setProperty("in_type", "s");
                itmPTeam.setProperty("in_parent", pid);
                itmPTeam.setProperty("in_sub_sect", sect_name);
                itmPTeam.setProperty("in_sub_id", sub);
                itmPTeam.setProperty("in_sub_serial", serial);
                itmPTeam.setProperty("in_muser", muid);


                itmPTeam.setProperty("in_meeting", cfg.meeting_id);
                itmPTeam.setProperty("source_id", itmParent.getProperty("source_id", ""));
                itmPTeam.setProperty("in_team_index", "");
                itmPTeam.setProperty("in_team_key", itmParent.getProperty("in_team_key", ""));

                itmPTeam.setProperty("in_index", itmParent.getProperty("in_index", ""));
                itmPTeam.setProperty("in_group", itmParent.getProperty("in_group", ""));
                itmPTeam.setProperty("in_current_org", itmParent.getProperty("in_current_org", ""));
                itmPTeam.setProperty("in_short_org", itmParent.getProperty("in_short_org", ""));
                itmPTeam.setProperty("in_stuff_b1", itmParent.getProperty("in_stuff_b1", ""));
                itmPTeam.setProperty("in_team", itmParent.getProperty("in_team", ""));
                itmPTeam.setProperty("in_team_players", "");
                itmPTeam.setProperty("in_creator_sno", itmParent.getProperty("in_creator_sno", ""));

                itmPTeam.setProperty("in_name", itmMUser.getProperty("in_name", ""));
                itmPTeam.setProperty("in_sno", itmMUser.getProperty("in_sno", ""));
                itmPTeam.setProperty("in_names", "");
                itmPTeam.setProperty("in_sub_weight", "");

                Item itmChild = itmPTeam.apply("merge");

                if (itmChild.isError())
                {
                    throw new Exception("更新發生錯誤");
                }

                ////修正子籤號
                //FixSignNo(cfg, itmParent, itmChild);

            }
        }

        ////修正子籤號
        //private void FixSignNo(TConfig cfg, Item itmParent, Item itmChild)
        //{
        //    var parent_id = itmParent.getProperty("id", "");
        //    var parent_sign_no = GetIntVal(itmParent.getProperty("in_sign_no", "0"));
        //    var parent_judo_no = GetIntVal(itmParent.getProperty("in_judo_no", "0"));
        //    if (parent_sign_no <= 0 || parent_judo_no <= 0) return;

        //    string sql = "";
        //    Item itmSQL = null;

        //    var in_sub_id = itmChild.getProperty("in_sub_id", "");
        //    var sub_id = GetIntVal(in_sub_id);

        //    var child_id = itmChild.getProperty("id", "");
        //    var child_sign_no = parent_sign_no * 100 + sub_id;
        //    var child_judo_no = parent_judo_no * 100 + sub_id;

        //    sql = "UPDATE IN_MEETING_PTEAM SET in_sign_no = NULL, in_judo_no = NULL WHERE in_parent = '" + parent_id + "' AND ISNULL(in_sign_no, '') = '" + child_sign_no + "'";
        //    itmSQL = cfg.inn.applySQL(sql);

        //    sql = "UPDATE IN_MEETING_PTEAM SET in_sign_no = '" + child_sign_no + "', in_judo_no = '" + child_judo_no + "' WHERE id = '" + child_id + "'";
        //    itmSQL = cfg.inn.applySQL(sql);
        //}

        //頁面
        private void OrgPage(TConfig cfg, Item itmReturn)
        {
            string sql = "";

            sql = "SELECT id, in_title FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'";
            cfg.itmMeeting = cfg.inn.applySQL(sql);

            if (cfg.itmMeeting.isError() || cfg.itmMeeting.getResult() == "")
            {
                throw new Exception("賽程組別資料錯誤");
            }
            itmReturn.setProperty("in_title", cfg.itmMeeting.getProperty("in_title", ""));

            AppendProgramMenu(cfg, itmReturn);

            if (cfg.program_id != "")
            {
                cfg.itmProgram = cfg.inn.applySQL("SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + cfg.program_id + "'");
                itmReturn.setProperty("program_id", cfg.itmProgram.getProperty("id", ""));

                //子場次數量
                ResetSubEvent(cfg, cfg.itmProgram);

                //與會者資料
                var itmMUsers = GetMUsers(cfg, "", "");
                //與會者隊伍
                var rows = RowsFromMUser(cfg, itmMUsers);

                //主隊伍資料
                var itmMTeams = GetMTeams(cfg, "");
                //隊伍查找表
                var map = GetTeamMap(cfg, itmMTeams);

                //隊伍匹配
                MapPTeam(cfg, rows, map);

                //生成控件
                GenerateCtrls(cfg, rows);

                TableBox(cfg, rows, itmReturn);
            }
        }

        //附加組別選單
        private void AppendProgramMenu(TConfig cfg, Item itmReturn)
        {
            string sql = @"
                SELECT
            	    *
                FROM
            	    IN_MEETING_PROGRAM WITH(NOLOCK)
                WHERE
            	    in_meeting = '{#meeting_id}'
            	    AND in_l1 = N'團體組'
            	    AND ISNULL(in_team_count, 0) > 0
                ORDER BY
            	    in_sort_order
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            Item items = cfg.inn.applySQL(sql);

            string in_type = "inn_program";

            Item itmEmpty = cfg.inn.newItem(in_type);
            itmEmpty.setProperty("value", "");
            itmEmpty.setProperty("text", "請選擇");
            itmReturn.addRelationship(itmEmpty);

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string id = item.getProperty("id", "");
                string in_name2 = item.getProperty("in_name2", "");
                string in_team_count = item.getProperty("in_team_count", "0");

                item.setType(in_type);
                item.setProperty("value", id);
                item.setProperty("text", in_name2 + " (" + in_team_count + ")");
                itmReturn.addRelationship(item);

                if (count == 1 && i == 0)
                {
                    cfg.program_id = id;
                }
            }
        }


        //表格箱
        private void TableBox(TConfig cfg, List<TRow> list, Item itmReturn)
        {
            StringBuilder builder = new StringBuilder();

            builder.Append("<div class='showsheet_ clearfix'>");
            builder.Append("    <div>");
            builder.Append("        <div class='float-btn clearfix'>");
            builder.Append("        </div>");

            AppendTable(cfg, list, builder);

            builder.Append("    </div>");

            builder.Append("</div>");

            itmReturn.setProperty("inn_tabs", builder.ToString());
        }

        private void AppendTable(TConfig cfg, List<TRow> list, StringBuilder builder)
        {
            var sects = GetSectList(cfg, cfg.program_id);

            List<TField> fields = new List<TField>();
            fields.Add(new TField { title = "單位", getValue = OrgName, css = "org-td" });
            fields.Add(new TField { title = "隊別", getValue = TeamName, css = "sign-no-td" });
            fields.Add(new TField { title = "籤號", getValue = SignNo, css = "sign-no-td" });
            for (int i = 0; i < sects.Count; i++)
            {
                var sect = sects[i];
                fields.Add(new TField { title = sect.name, getValue = CtrlVal, sect = sect, css = "select-td" });
            }

            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            head.Append("<thead>");
            head.Append("  <tr>");
            for (int i = 0; i < fields.Count; i++)
            {
                var field = fields[i];
                head.Append("    <th class='" + field.css + " text-center'>");
                head.Append(field.title);
                head.Append("    </th>");
            }
            head.Append("  </tr>");
            head.Append("</thead>");


            int count = list.Count;
            body.Append("<tbody>");
            for (int i = 0; i < count; i++)
            {
                var entity = list[i];
                body.Append("  <tr class='weight_row'>");
                for (int j = 0; j < fields.Count; j++)
                {
                    var field = fields[j];
                    body.Append("    <td>");
                    body.Append(field.getValue(cfg, field, entity));
                    body.Append("    </td>");
                }
                body.Append("  </tr>");
            }
            body.Append("</tbody>");



            string tb_id = "team_battle_table";

            builder.Append("<table id='" + tb_id + "'"
                + " class='table table-hover table-bordered table-rwd rwd'"
                + " style='background-color: #fff;' "
                + " data-toggle='table' "
                + " data-search='false'"
                + " data-search-align='left'"
                + ">");

            builder.Append(head);
            builder.Append(body);
            builder.Append("</table>");

            //builder.Append("<script>");
            //builder.Append("    $('#" + tb_id + "').bootstrapTable({});");
            //builder.Append("    if($(window).width() <= 768) { $('#" + tb_id + "').bootstrapTable('toggleView'); }");
            //builder.Append("</script>");
        }

        private string OrgName(TConfig cfg, TField field, TRow row)
        {
            return row.OrgName;
        }

        private string TeamName(TConfig cfg, TField field, TRow row)
        {
            return row.TeamName;
        }

        private string SignNo(TConfig cfg, TField field, TRow row)
        {
            return row.InJudoNo;
        }

        private string CtrlVal(TConfig cfg, TField field, TRow row)
        {
            StringBuilder container = new StringBuilder();
            for (int i = 1; i <= cfg.sub_event_rows; i++)
            {
                AppendSelect(cfg, field.sect, row, i, container);
            }
            return container.ToString();
        }

        private void AppendSelect(TConfig cfg, TSect sect, TRow row, int serial, StringBuilder container)
        {
            StringBuilder ctrl = GetSelectCtrl(cfg, sect, row, serial);
            container.Append(ctrl);
        }

        private StringBuilder GetSelectCtrl(TConfig cfg, TSect sect, TRow row, int serial)
        {
            StringBuilder builder = null;
            if (sect.gender == "W")
            {
                builder = new StringBuilder(row.SelectCtrlW.ToString());
            }
            else if (sect.gender == "M")
            {
                builder = new StringBuilder(row.SelectCtrlM.ToString());
            }
            else
            {
                builder = new StringBuilder(row.SelectCtrlALL.ToString());
            }
            //new StringBuilder(team.SelectCtrl);

            string muid = "";
            string pid = row.Parent.team_id;
            string cid = "";
            string sub = sect.sub_id.ToString();

            if (row.Children.Count > 0)
            {
                var child = row.Children.Find(x => x.sub_id == sect.sub_id && x.sub_serial == serial);

                if (child != null)
                {
                    muid = child.in_muser;
                    cid = child.team_id;
                }
            }

            builder.Replace("{#muid}", muid);
            builder.Replace("{#pid}", pid);
            builder.Replace("{#cid}", cid);
            builder.Replace("{#sub}", sub);
            builder.Replace("{#serial}", serial.ToString());

            return builder;
        }

        //生成控件
        private void GenerateCtrls(TConfig cfg, List<TRow> list)
        {
            foreach (var entity in list)
            {
                entity.SelectCtrlALL = GetSelectCtrl(cfg, entity.GenderALL);
                entity.SelectCtrlW = GetSelectCtrl(cfg, entity.GenderW);
                entity.SelectCtrlM = GetSelectCtrl(cfg, entity.GenderM);
            }
        }

        private StringBuilder GetSelectCtrl(TConfig cfg, List<TMUser> musers)
        {
            StringBuilder builder = new StringBuilder();

            builder.Append("<select class='form-control btn btn-default team-select'");
            builder.Append(" data-muid='{#muid}'");
            builder.Append(" data-pid='{#pid}'");//team parent id
            builder.Append(" data-cid='{#cid}'");//team child id
            builder.Append(" data-sub='{#sub}'");
            builder.Append(" data-serial='{#serial}'");
            builder.Append(" onchange='BattlePlayer_Change(this)' >");

            builder.Append("    <option value=''>請選擇</option>");
            foreach (var muser in musers)
            {
                builder.Append("    <option value='" + muser.muid + "'>" + muser.display + "</option>");
            }

            builder.Append("</select>");

            return builder;
        }

        //與會者隊伍
        private List<TRow> RowsFromMUser(TConfig cfg, Item itmMUsers)
        {
            List<TRow> list = new List<TRow>();

            int count = itmMUsers.getItemCount();
            if (count <= 0) return list;

            for (int i = 0; i < count; i++)
            {
                Item item = itmMUsers.getItemByIndex(i);

                var muser = new TMUser
                {
                    muid = item.getProperty("muid", ""),
                    in_name = item.getProperty("in_name", ""),
                    in_sno = item.getProperty("in_sno", ""),
                    in_gender = item.getProperty("in_gender", ""),
                    in_current_org = item.getProperty("in_current_org", ""),
                    in_short_org = item.getProperty("in_short_org", ""),
                    in_stuff_b1 = item.getProperty("in_stuff_b1", ""),
                    in_team = item.getProperty("in_team", ""),
                    in_index = item.getProperty("in_index", ""),
                    display = "",
                    Value = item,
                };

                muser.display = muser.in_name + "(" + muser.in_gender + ")";

                string key = muser.in_short_org + "-" + muser.in_index;

                var entity = list.Find(x => x.Key == key);
                if (entity == null)
                {
                    entity = new TRow
                    {
                        Key = key,
                        SortNo = muser.in_stuff_b1,
                        OrgName = muser.in_short_org,
                        TeamName = muser.in_team,
                        TeamIndex = muser.in_index,
                        ParentKey = key + "-" + "p",
                        SonKey = key + "-" + "s",
                        GenderALL = new List<TMUser>(),
                        GenderW = new List<TMUser>(),
                        GenderM = new List<TMUser>(),
                    };

                    list.Add(entity);
                }

                if (cfg.is_weight_table)
                {
                    muser.display = (entity.GenderALL.Count + 1)
                        + ". "
                        + muser.display;
                }

                entity.GenderALL.Add(muser);

                if (muser.in_gender == "女")
                {
                    entity.GenderW.Add(muser);
                }
                else if (muser.in_gender == "男")
                {
                    entity.GenderM.Add(muser);
                }
            }
            return list;
        }

        //隊伍查找表
        private Dictionary<string, List<TMTeam>> GetTeamMap(TConfig cfg, Item itmMTeams)
        {
            Dictionary<string, List<TMTeam>> map = new Dictionary<string, List<TMTeam>>();

            int count = itmMTeams.getItemCount();
            if (count <= 0) return map;

            for (int i = 0; i < count; i++)
            {
                Item item = itmMTeams.getItemByIndex(i);
                string in_short_org = item.getProperty("in_short_org", "");
                string in_team = item.getProperty("in_team", "");
                string in_type = item.getProperty("in_type", "");
                string in_index = item.getProperty("in_index", "");
                string key = in_short_org + "-" + in_index + "-" + in_type;

                var selected = default(List<TMTeam>);
                if (map.ContainsKey(key))
                {
                    selected = map[key];
                }
                else
                {
                    selected = new List<TMTeam>();
                    map.Add(key, selected);
                }

                var player = new TMTeam
                {
                    team_id = item.getProperty("id", ""),
                    in_short_org = in_short_org,
                    in_team = in_team,
                    in_index = in_index,

                    in_name = item.getProperty("in_name", ""),
                    in_sno = item.getProperty("in_sno", ""),
                    in_current_org = item.getProperty("in_current_org", ""),

                    in_type = in_type,
                    in_parent = item.getProperty("in_parent", ""),
                    in_sub_sect = item.getProperty("in_sub_sect", ""),
                    in_sub_id = item.getProperty("in_sub_id", ""),
                    in_sub_serial = item.getProperty("in_sub_serial", ""),
                    in_sub_weight = item.getProperty("in_sub_weight", ""),
                    in_muser = item.getProperty("in_muser", ""),

                    in_sign_no = item.getProperty("in_sign_no", "0"),
                    in_judo_no = item.getProperty("in_judo_no", "0"),
                    in_weight_message = item.getProperty("in_weight_message", ""),
                    in_weight_value = item.getProperty("in_weight_value", ""),

                    Value = item,
                };

                player.sub_id = GetIntVal(player.in_sub_id);
                player.sub_serial = GetIntVal(player.in_sub_serial);

                selected.Add(player);
            }

            return map;
        }

        //隊伍匹配
        private void MapPTeam(TConfig cfg, List<TRow> rows, Dictionary<string, List<TMTeam>> map)
        {
            foreach (var row in rows)
            {
                if (map.ContainsKey(row.ParentKey))
                {
                    var plist = map[row.ParentKey];
                    if (plist.Count > 0)
                    {
                        row.Parent = plist[0];
                        row.InJudoNo = row.Parent.in_judo_no;
                    }
                }

                if (map.ContainsKey(row.SonKey))
                {
                    row.Children = map[row.SonKey];
                }

                if (row.Parent == null) row.Parent = new TMTeam();
                if (row.Children == null) row.Children = new List<TMTeam>();
            }
        }

        private Item GetMUsers(TConfig cfg, string in_index, string in_creator_sno)
        {
            string condition = in_index == ""
                ? ""
                : "AND t1.in_index = '" + in_index + "' AND in_creator_sno = '" + in_creator_sno + "'";

            string sql = @"
                SELECT
	                t1.id					AS 'muid'
	                , t1.in_short_org
	                , t1.in_team
	                , t1.in_index
	                , t1.in_gender
	                , t1.in_name
	                , t1.in_sno
	                , t2.in_stuff_b1
                FROM
	                IN_MEETING_USER t1 WITH(NOLOCK)
                LEFT OUTER JOIN
	                IN_ORG_MAP t2 WITH(NOLOCK)
	                ON t2.in_stuff_b1 = t1.in_stuff_b1
                WHERE
	                t1.source_id = '{#meeting_id}'
	                AND t1.in_l1 = N'{#in_l1}'
	                AND t1.in_l2 = N'{#in_l2}'
	                AND t1.in_l3 = N'{#in_l3}'
                    {#condition}
                ORDER BY
	                t1.in_stuff_b1
	                , t1.in_team
	                , t1.in_index
	                , t1.in_gender
	                , t1.in_sno
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_l1}", cfg.itmProgram.getProperty("in_l1", ""))
                .Replace("{#in_l2}", cfg.itmProgram.getProperty("in_l2", ""))
                .Replace("{#in_l3}", cfg.itmProgram.getProperty("in_l3", ""))
                .Replace("{#condition}", condition);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        //取得隊伍資料
        private Item GetMTeams(TConfig cfg, string team_id)
        {
            string sql = @"SELECT * FROM IN_MEETING_PTEAM WITH(NOLOCK)";

            if (team_id == "")
            {
                sql += " WHERE source_id = '" + cfg.itmProgram.getProperty("id", "") + "' ORDER BY in_stuff_b1, in_team, in_type, in_sub_id, in_sub_serial";
            }
            else
            {
                sql += " WHERE id = '" + team_id + "' OR in_parent = '" + team_id + "' ORDER BY in_type, in_sub_id, in_sub_serial";
            }

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        //取得組別量級選單
        private List<TSect> GetSectList(TConfig cfg, string program_id)
        {
            var lstSect = GetMSectList(cfg, program_id);

            var sects = new List<TSect>();

            for (int i = 0; i < lstSect.Count; i++)
            {
                Item itmSect = lstSect[i];

                var sect = new TSect
                {
                    sub_id = i + 1,
                    name = itmSect.getProperty("in_name", ""),
                    gender = itmSect.getProperty("in_gender", ""),
                    Value = itmSect,
                };

                switch (sect.gender)
                {
                    case "W":
                        sect.gender_display = "女子";
                        break;

                    case "M":
                        sect.gender_display = "男子";
                        break;

                    default:
                        sect.gender_display = "";
                        break;
                }

                sects.Add(sect);
            }

            return sects;
        }

        //取得團體賽量級清單
        private List<Item> GetMSectList(TConfig cfg, string program_id)
        {
            List<Item> result = new List<Item>();

            string sql = "SELECT * FROM In_Meeting_PSect WITH(NOLOCK)"
                + " WHERE in_program = '" + program_id + "'"
                + " AND in_name <> N'代表戰'"
                + " ORDER BY in_sub_id";

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item items = cfg.inn.applySQL(sql);
            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                result.Add(items.getItemByIndex(i));
            }

            return result;
        }

        private List<TMTeam> GetMTeamChildren(TConfig cfg)
        {
            List<TMTeam> result = new List<TMTeam>();

            string sql = "SELECT * FROM IN_MEETING_PTEAM WITH(NOLOCK)"
                + " WHERE in_parent = '" + cfg.team_id + "' AND in_type = 's'"
                + " ORDER BY in_sub_id, in_sub_serial";

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);

                TMTeam player = new TMTeam
                {
                    in_sub_id = item.getProperty("in_sub_id", ""),
                    in_sub_serial = item.getProperty("in_sub_serial", ""),
                    Value = item,
                };

                player.sub_id = GetIntVal(player.in_sub_id);
                player.sub_serial = GetIntVal(player.in_sub_serial);

                result.Add(player);
            }

            return result;
        }

        //空隊伍
        private TMTeam GetEmptyPlayer(TConfig cfg)
        {
            return new TMTeam
            {
                in_name = "",
                in_sub_weight = "",
                Value = cfg.inn.newItem(),
            };
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }

            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strUserName { get; set; }

            public string meeting_id { get; set; }
            public string program_id { get; set; }
            public string team_id { get; set; }
            public string scene { get; set; }
            public string subtype { get; set; }

            public Item itmMeeting { get; set; }
            public Item itmProgram { get; set; }

            public string mt_title { get; set; }
            public string in_sub_event { get; set; }
            public bool is_weight_table { get; set; }
            public int sub_event { get; set; }
            public int sub_event_rows { get; set; }
        }

        private class TField
        {
            public string title { get; set; }
            public string css { get; set; }
            public int colspan { get; set; }
            public TSect sect { get; set; }
            public Func<TConfig, TField, TRow, string> getValue { get; set; }
        }

        private class TSect
        {
            public int sub_id { get; set; }
            public string name { get; set; }
            public string gender { get; set; }
            public string gender_display { get; set; }

            public Item Value { get; set; }
        }

        private enum TFType
        {
            None = 0,
            Center = 1,
        }

        private class TRow
        {
            public string Key { get; set; }
            public string SortNo { get; set; }
            public string OrgName { get; set; }
            public string TeamName { get; set; }
            public string TeamIndex { get; set; }
            public List<TMUser> GenderALL { get; set; }
            public List<TMUser> GenderW { get; set; }
            public List<TMUser> GenderM { get; set; }

            public StringBuilder SelectCtrlALL { get; set; }
            public StringBuilder SelectCtrlW { get; set; }
            public StringBuilder SelectCtrlM { get; set; }

            public TMTeam Parent { get; set; }
            public List<TMTeam> Children { get; set; }
            public string ParentKey { get; set; }
            public string SonKey { get; set; }

            public int SignNo { get; set; }
            public string InJudoNo { get; set; }
        }

        private class TMUser
        {
            public string muid { get; set; }
            public string in_name { get; set; }
            public string in_sno { get; set; }
            public string in_gender { get; set; }
            public string in_current_org { get; set; }
            public string in_short_org { get; set; }
            public string in_stuff_b1 { get; set; }
            public string in_team { get; set; }
            public string in_index { get; set; }

            public string display { get; set; }

            public Item Value { get; set; }
        }

        private class TMTeam
        {
            public string team_id { get; set; }
            public string in_short_org { get; set; }
            public string in_team { get; set; }
            public string in_index { get; set; }

            public string in_name { get; set; }
            public string in_sno { get; set; }
            public string in_current_org { get; set; }

            public string in_type { get; set; }
            public string in_parent { get; set; }
            public string in_sub_sect { get; set; }
            public string in_sub_id { get; set; }
            public string in_sub_serial { get; set; }
            public string in_sub_weight { get; set; }
            public string in_muser { get; set; }

            public string in_sign_no { get; set; }
            public string in_judo_no { get; set; }
            public string in_weight_value { get; set; }
            public string in_weight_message { get; set; }

            public int sign_no { get; set; }
            public int sub_id { get; set; }
            public int sub_serial { get; set; }

            public Item Value { get; set; }
        }

        //子場次數量
        private void ResetSubEvent(TConfig cfg, Item itmProgram)
        {
            cfg.in_sub_event = itmProgram.getProperty("in_sub_event", "0");

            cfg.sub_event = GetIntVal(cfg.in_sub_event);

            switch (cfg.sub_event)
            {
                case 3:
                    cfg.sub_event_rows = 5;
                    break;
                case 5:
                    cfg.sub_event_rows = 7;
                    break;
                case 6:
                    cfg.sub_event_rows = 5;
                    break;
            }
        }

        private int GetIntVal(string value, int def = 0)
        {
            int result = def;
            Int32.TryParse(value, out result);
            return result;
        }
    }
}