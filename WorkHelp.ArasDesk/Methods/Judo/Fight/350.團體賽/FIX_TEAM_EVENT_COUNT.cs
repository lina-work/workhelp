﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Judo.Fight
{
    public class FIX_TEAM_EVENT_COUNT : Item
    {
        public FIX_TEAM_EVENT_COUNT(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            
            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "FIX_TEAM_EVENT_COUNT";

            var itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                meeting_id = itmR.getProperty("meeting_id", ""),
                scene = itmR.getProperty("scene", ""),
            };

            cfg.meeting_id = "2FC72AD431CE495B9AB5944035599341";

            switch (cfg.scene)
            {
                case "recount_event":
                    RecountEvents(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void RecountEvents(TConfig cfg, Item itmReturn)
        {
            var itmPrograms = cfg.inn.applySQL("SELECT id FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE in_meeting = '" + cfg.meeting_id + "' AND in_l1 = N'團體組' ORDER BY in_sort_order");
            var count = itmPrograms.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var itmProgram = itmPrograms.getItemByIndex(i);
                var program_id = itmProgram.getProperty("id", "");
                RecountEvents(cfg, program_id);
            }
        }

        private void RecountEvents(TConfig cfg, string program_id)
        {
            var sql = @"
                UPDATE t1 SET
	                t1.in_event_count = t2.cnt
                FROM
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
                (
	                SELECT
		                source_id
		                , count(*) AS 'cnt'
	                FROM
		                IN_MEETING_PEVENT WITH(NOLOCK)
	                WHERE
		                source_id = '{#program_id}'
		                AND ISNULL(in_tree_no, 0) > 0
		                AND ISNULL(in_win_status, '') NOT IN ('cancel', 'nofight', 'bypass')
		                AND ISNULL(in_type, '') = 's'
						AND in_sub_sect <> N'代表戰'
	                GROUP BY
		                source_id
                ) t2
                ON t2.source_id = t1.id
            ";

            sql = sql.Replace("{#program_id}", program_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            cfg.inn.applySQL(sql);
        }


        private void CopyItemValue(Item itmA, Item itmB, string propA, string propB = "")
        {
            if (propB == "")
            {
                itmA.setProperty(propA, itmB.getProperty(propA, ""));
            }
            else
            {
                itmA.setProperty(propA, itmB.getProperty(propB, ""));
            }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }

            public string meeting_id { get; set; }
            public string scene { get; set; }
        }

    }
}
