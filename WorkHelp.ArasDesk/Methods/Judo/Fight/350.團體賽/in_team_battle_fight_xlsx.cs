﻿using Aras.IOM;
using Spire.Xls;
using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;

namespace WorkHelp.ArasDesk.Methods.Judo.Fight
{
    public class in_team_battle_fight_xlsx : Item
    {
        public in_team_battle_fight_xlsx(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 團體賽-出賽單(New)
                日誌: 
                    - 2022-10-27: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "in_team_battle_fight_xlsx";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                meeting_id = itmR.getProperty("meeting_id", ""),
                program_id = itmR.getProperty("program_id", ""),
                event_id = itmR.getProperty("event_id", ""),
                team_id = itmR.getProperty("team_id", ""),
                team_sign_no = itmR.getProperty("team_sign_no", ""),
                file_extension = itmR.getProperty("file_extension", ""),
                round_serial = itmR.getProperty("round_serial", ""),
                scene = itmR.getProperty("scene", ""),
            };

            if (cfg.file_extension == "")
            {
                cfg.file_extension = "xlsx";
            }

            cfg.need_all = cfg.round_serial == "" || cfg.round_serial == "ALL";
            cfg.need_assigned = cfg.round_serial == "ASSIGNED";
            cfg.eventIdList = cfg.need_assigned ? GetAssignedEventIdList(cfg) : new List<string>();
            cfg.need_next = cfg.round_serial == "NEXT-EVENT";
            cfg.roundEventId = itmR.getProperty("round_event_id", "");
            cfg.itmNextEvent = cfg.need_next ? GetNextEventItem(cfg) : cfg.inn.newItem();
            if (cfg.need_next && cfg.itmNextEvent.getProperty("in_next_win", "") == "")
            {
                throw new Exception("無下一場資料");
            }

            string sql = "";

            sql = "SELECT id, in_title FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'";
            cfg.itmMeeting = cfg.inn.applySQL(sql);
            if (cfg.itmMeeting.isError() || cfg.itmMeeting.getResult() == "")
            {
                throw new Exception("賽事資料錯誤");
            }

            sql = "SELECT TOP 1 * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + cfg.program_id + "'";
            cfg.itmProgram = cfg.inn.applySQL(sql);
            if (cfg.itmProgram.isError() || cfg.itmProgram.getResult() == "")
            {
                throw new Exception("賽程 組別 資料錯誤");
            }

            cfg.in_title = cfg.itmMeeting.getProperty("in_title", "");

            cfg.pg_name2 = cfg.itmProgram.getProperty("in_name2", "");
            cfg.in_battle_type = cfg.itmProgram.getProperty("in_battle_type", "");
            cfg.is_robin = cfg.in_battle_type == "SingleRoundRobin";
            cfg.sub_event = GetIntVal(cfg.itmProgram.getProperty("in_sub_event", "0"));
            cfg.sect_start = GetIntVal(cfg.itmProgram.getProperty("in_sect_start", "0"));
            cfg.is_special_sect = cfg.pg_name2.Contains("特別組");

            var xls_parm_name = "team_fight_" + cfg.sub_event;
            cfg.itmXlsx = cfg.inn.applyMethod("In_Variable_File", "<in_name>" + xls_parm_name + "</in_name>");
            // if (cfg.itmXlsx.isError() || cfg.itmXlsx.getResult() == "")
            // {
            //     throw new Exception("賽程 XLSX 資料錯誤");
            // }

            cfg.template_path = cfg.itmXlsx.getProperty("template_path", "");
            cfg.export_path = cfg.itmXlsx.getProperty("export_path", "");
            if (cfg.export_path == "")
            {
                throw new Exception("賽程 XLSX 資料錯誤");
            }

            cfg.sects = GetSectList(cfg, true);
            if (cfg.sects == null || cfg.sects.Count == 0)
            {
                throw new Exception("賽程 團體賽量級 資料錯誤");
            }

            cfg.itmParents = GetParentEvents(cfg, cfg.event_id);
            cfg.itmChildren = GetChildrenEvents(cfg, cfg.event_id);
            cfg.itmTeams = GetSubTeams(cfg, cfg.team_id);

            var evts = MapMainEvents(cfg, cfg.itmParents);
            AppendChildren(cfg, evts, cfg.itmChildren);

            if (evts.Count == 0)
            {
                throw new Exception("賽程 場次 資料錯誤");
            }

            var teams = MapMainTeams(cfg, cfg.itmTeams);
            cfg.max_rcount = GetMaxPlayerCount(cfg.sub_event);
            cfg.sheets = new List<string>();
            cfg.head_color = System.Drawing.Color.FromArgb(237, 237, 237);

            if (cfg.team_id == "")
            {
                var entities = GetSheetList1(cfg, evts, teams);
                RunExportFightReport(cfg, entities, GetOneProgramXlsName, itmR);
            }
            else
            {
                var entities = GetSheetList2(cfg, evts[0], teams);
                RunExportFightReport(cfg, entities, GetOrgTeamXlsName, itmR);
            }

            return itmR;
        }

        private void RunExportFightReport(TConfig cfg, List<TSheet> entities, Func<TConfig, string> getXlsName, Item itmReturn)
        {
            var book = new Spire.Xls.Workbook();
            book.LoadFromFile(cfg.template_path);

            var sheetTemplate = book.Worksheets[0];
            var assignedList = new List<TSheet>();

            for (int i = 0; i < entities.Count; i++)
            {
                var entity = entities[i];
                var hasEventId = entity.evt != null && entity.evt.id != null;

                if (cfg.need_all)
                {
                    AppendSheetMain(cfg, entity, book, sheetTemplate);
                }
                else if (cfg.need_assigned && hasEventId)
                {
                    if (cfg.eventIdList.Contains(entity.evt.id))
                    {
                        assignedList.Add(entity);
                        AppendSheetMain(cfg, entity, book, sheetTemplate);
                    }
                }
                else if (cfg.need_next && hasEventId)
                {
                    if (IsMatchNextEvent(cfg, entity))
                    {
                        AppendSheetMain(cfg, entity, book, sheetTemplate);
                    }
                }
                else if (cfg.round_serial == entity.in_round_serial)
                {
                    AppendSheetMain(cfg, entity, book, sheetTemplate);
                }
            }

            if (cfg.need_assigned)
            {
                AppendAssignedSheet(cfg, book, assignedList);
            }


            if (book.Worksheets.Count > 1)
            {
                sheetTemplate.Remove();
                if (cfg.need_next)
                {
                    book.Worksheets[0].SelectTab();
                    book.Worksheets[1].SelectTab();
                }
                else
                {
                    book.ActiveSheetIndex = 0;
                }
            }

            string xlsName = getXlsName(cfg);

            string extName = "." + cfg.file_extension;
            string xls_file = cfg.export_path + xlsName + extName;
            string xls_name = xlsName + extName;

            if (cfg.file_extension == "pdf")
            {
                book.SaveToFile(xls_file, Spire.Xls.FileFormat.PDF);
            }
            else
            {
                book.SaveToFile(xls_file, Spire.Xls.ExcelVersion.Version2013);
            }

            itmReturn.setProperty("xls_name", xls_name);
        }

        private void AppendAssignedSheet(TConfig cfg, Spire.Xls.Workbook workbook, List<TSheet> entities)
        {
            if (entities.Count == 0) return;

            var sheet = workbook.CreateEmptySheet();
            sheet.Name = "對照表";
            sheet.MoveSheet(0);

            sheet.PageSetup.CenterHorizontally = true;
            sheet.PageSetup.TopMargin = 0.8;
            sheet.PageSetup.LeftMargin = 0.8;
            sheet.PageSetup.RightMargin = 0.8;
            sheet.PageSetup.BottomMargin = 0.8;

            sheet.PageSetup.PrintTitleRows = "$1:$2";

            var ri = 1;
            var posM = "A" + ri + ":" + "C" + ri;
            sheet.Range[posM].Merge();
            sheet.Range[posM].RowHeight = 42;
            SetSummaryCell(sheet, "A" + ri, cfg.in_title, 20);
            ri++;

            var posX = "A" + ri + ":" + "C" + ri;
            sheet.Range[posX].Merge();
            sheet.Range[posX].RowHeight = 40;
            SetSummaryCell(sheet, posX, cfg.pg_name2, 16);
            sheet.Range[posX].Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Right;
            ri++;

            var rs = ri;
            SetSummaryCell(sheet, "A" + ri, "No.", 16);
            SetSummaryCell(sheet, "B" + ri, "單位", 16);
            SetSummaryCell(sheet, "C" + ri, "場次", 16);
            ri++;

            var no = 0;
            for (var i = 0; i < entities.Count; i++)
            {
                var entity = entities[i];
                if (entity.org.org_display == "") continue;

                SetSummaryCell(sheet, "A" + ri, (no + 1).ToString(), 16);
                SetSummaryCell(sheet, "B" + ri, entity.org.org_display, 16);
                SetSummaryCell(sheet, "C" + ri, entity.alias2, 16);

                if (no % 2 > 0)
                {
                    SetSummaryColor(cfg, sheet, "A" + ri);
                    SetSummaryColor(cfg, sheet, "B" + ri);
                    SetSummaryColor(cfg, sheet, "C" + ri);
                }

                ri++;
                no++;
            }

            var posB = "A" + rs + ":" + "C" + (ri - 1);
            var rngB = sheet.Range[posB];
            rngB.BorderInside(Spire.Xls.LineStyleType.Thin, System.Drawing.Color.Black);
            rngB.BorderAround(Spire.Xls.LineStyleType.Thin, System.Drawing.Color.Black);

            sheet.Columns[0].ColumnWidth = 18;
            sheet.Columns[1].ColumnWidth = 34;
            sheet.Columns[2].ColumnWidth = 24;
        }

        private void SetSummaryColor(TConfig cfg, Spire.Xls.Worksheet sheet, string pos)
        {
            var range = sheet.Range[pos];
            range.Style.Color = cfg.head_color;
        }

        private void SetSummaryCell(Spire.Xls.Worksheet sheet, string pos, string value, int fontSize)
        {
            var range = sheet.Range[pos];
            range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
            range.Style.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;
            range.Text = value;
            range.Style.Font.Size = fontSize;
            range.Style.Font.FontName = "標楷體";
        }

        private bool IsMatchNextEvent(TConfig cfg, TSheet sheet)
        {
            var f1_no = GetIntVal(cfg.itmNextEvent.getProperty("in_sign_no", "0"));
            var f2_no = GetIntVal(cfg.itmNextEvent.getProperty("in_target_no", "0"));
            var alias = cfg.itmNextEvent.getProperty("next_site_code_en", "") + cfg.itmNextEvent.getProperty("next_tree_no", "");
            var b1 = sheet.sign_no == f1_no || sheet.sign_no == f2_no;
            var b2 = sheet.alias == alias;
            return b1 && b2;
        }

        private List<TSheet> GetSheetList2(TConfig cfg, TRow evt, List<TRow> teams)
        {
            var list = new List<TSheet>();
            var target_sign_no = GetIntVal(cfg.team_sign_no);

            if (evt.foot1.sign_no == target_sign_no)
            {
                AppendTeamSheetEntity(cfg, list, evt, evt.foot1, teams);
            }
            else if (evt.foot2.sign_no == target_sign_no)
            {
                AppendTeamSheetEntity(cfg, list, evt, evt.foot2, teams);
            }

            return list;
        }

        //遞迴
        private void AppendTeamSheetEntity(TConfig cfg
            , List<TSheet> entities
            , TRow evt
            , TOrg org
            , List<TRow> teams)
        {
            var entity = new TSheet();
            entity.org = org;
            entity.evt = evt;
            entity.in_round_serial = evt.value.getProperty("in_round_serial", "");
            entity.site_code = evt.value.getProperty("site_code", "");
            entity.site_code_en = evt.value.getProperty("site_code_en", "");
            entity.in_tree_no = evt.value.getProperty("in_tree_no", "");
            entity.rnd = "R" + evt.value.getProperty("in_round", "");
            entity.org_name = org.org_short + org.team;
            entity.sign_no = org.sign_no;
            entity.alias = entity.site_code_en + entity.in_tree_no;

            entity.key = "輪" + entity.in_round_serial
                + "_" + entity.org_name
                + "_" + entity.site_code_en + entity.in_tree_no;

            //該單位隊伍清單 (org > sect > player)
            var org_team_page = teams.Find(x => x.no == org.sign_no);
            if (org_team_page == null) org_team_page = new TRow { Children = new List<TRow>() };
            entity.team_page = org_team_page;

            AppendSheetEntitySub(cfg, entity);
            entities.Add(entity);

            var in_next_win = "";
            if (cfg.is_robin)
            {
                in_next_win = GetRobinNextEvent(cfg, org, evt);
            }
            else
            {
                in_next_win = evt.value.getProperty("in_next_win", "");
            }

            if (in_next_win != "")
            {
                var next_evt = GetNextEvent(cfg, in_next_win);
                if (next_evt != null)
                {
                    AppendTeamSheetEntity(cfg, entities, next_evt, org, teams);
                }
            }
        }

        private string GetRobinNextEvent(TConfig cfg, TOrg org, TRow evt)
        {
            string in_tree_no = evt.value.getProperty("in_tree_no", "0");

            string sql = @"
                SELECT TOP 1
	                t1.id 
                FROM
	                IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                INNER JOIN
	                IN_MEETING_PTEAM t3 WITH(NOLOCK)
	                ON t3.source_id = t1.source_id
	                AND t3.in_sign_no = t2.in_sign_no
	                AND ISNULL(t3.in_type, '') = 'p'
                WHERE
	                t1.source_id = '{#program_id}'
	                AND t3.id = '{#team_id}'
	                AND ISNULL(t1.in_tree_no, 0) > {#in_tree_no}
            ";

            sql = sql.Replace("{#program_id}", cfg.program_id)
                .Replace("{#team_id}", cfg.team_id)
                .Replace("{#in_tree_no}", in_tree_no);

            Item item = cfg.inn.applySQL(sql);

            if (!item.isError() && item.getResult() != "")
            {
                return item.getProperty("id", "");
            }
            else
            {
                return "";
            }
        }

        private TRow GetNextEvent(TConfig cfg, string event_id)
        {
            var itmParents = GetParentEvents(cfg, event_id);
            var itmChildren = GetChildrenEvents(cfg, event_id);

            var evts = MapMainEvents(cfg, itmParents);
            AppendChildren(cfg, evts, itmChildren);

            if (evts.Count > 0)
            {
                return evts[0];
            }
            else
            {
                return null;
            }
        }

        private List<TSheet> GetSheetList1(TConfig cfg, List<TRow> evts, List<TRow> teams)
        {
            var list = new List<TSheet>();
            for (int i = 0; i < evts.Count; i++)
            {
                var evt = evts[i];
                AppendSheetEntity(cfg, list, evts, evt, evt.foot1, teams);
                AppendSheetEntity(cfg, list, evts, evt, evt.foot2, teams);
            }
            return list;
        }

        //遞迴
        private void AppendSheetEntity(TConfig cfg
            , List<TSheet> entities
            , List<TRow> evts
            , TRow evt
            , TOrg org
            , List<TRow> teams)
        {
            var entity = new TSheet();
            entity.org = org;
            entity.evt = evt;
            entity.in_round_serial = evt.value.getProperty("in_round_serial", "");
            entity.site_code = evt.value.getProperty("site_code", "");
            entity.site_code_en = evt.value.getProperty("site_code_en", "");
            entity.in_tree_no = evt.value.getProperty("in_tree_no", "");
            entity.in_fight_id = evt.value.getProperty("in_fight_id", "");
            entity.in_fight_note = evt.value.getProperty("in_fight_note", "");
            entity.rnd = "R" + evt.value.getProperty("in_round", "");
            entity.org_name = org.org_short + org.team;
            entity.sign_no = org.sign_no;
            entity.sign_foot = org.foot;
            entity.alias = entity.site_code_en + entity.in_tree_no;
            entity.alias2 = entity.alias + "-" + entity.sign_foot;

            entity.key = "輪" + entity.in_round_serial
                + "_" + entity.org_name
                + "_" + entity.site_code_en + entity.in_tree_no;

            var org_team_page = teams.Find(x => x.no == org.sign_no);
            if (org_team_page == null) org_team_page = new TRow { Children = new List<TRow>() };
            entity.team_page = org_team_page;

            AppendSheetEntitySub(cfg, entity);
            entities.Add(entity);

            //只下載有籤號的，中斷
            if (cfg.need_assigned) return;

            var in_next_win = evt.value.getProperty("in_next_win", "");
            if (in_next_win != "")
            {
                var next_evt = evts.Find(x => x.id == in_next_win);
                if (next_evt != null)
                {
                    AppendSheetEntity(cfg, entities, evts, next_evt, org, teams);
                }
            }
        }

        private void AppendSheetEntitySub(TConfig cfg, TSheet entity)
        {
            var evt = entity.evt;

            var site_code = evt.value.getProperty("site_code", "");
            var in_tree_no = evt.value.getProperty("in_tree_no", "");
            var in_round_serial = evt.value.getProperty("in_round_serial", "");
            if (in_round_serial != "" && in_round_serial != "0")
            {
                in_round_serial = "輪次：" + in_round_serial;
            }

            entity.A1 = cfg.in_title;
            entity.A2 = cfg.pg_name2 + " - 出賽單";
            entity.A3 = cfg.pg_name2;
            entity.A4 = "場地：" + site_code;
            entity.C4 = "場次：" + in_tree_no;
            entity.E4 = in_round_serial;
            entity.H4 = "單位：" + entity.org.org_display;

            var org_sects = new List<TSectRow>();
            for (int i = 0; i < cfg.sects.Count; i++)
            {
                var sect = cfg.sects[i];

                var sub_evt = evt.Children.Find(x => x.no == sect.sub_id);
                if (sub_evt == null) sub_evt = new TRow { value = cfg.inn.newItem() };
                var sub_tree_no = sub_evt.value.getProperty("in_tree_no", "");

                var org_sect_team_box = entity.team_page.Children.Find(x => x.no == sect.sub_id);
                if (org_sect_team_box == null) org_sect_team_box = new TRow { Children = new List<TRow>() };

                var org_sect = new TSectRow
                {
                    sub_tree_no = sub_tree_no,

                    id = sect.sub_id,
                    name = sect.Value.getProperty("in_name", ""),
                    weight = sect.Value.getProperty("in_weight", ""),
                    ranges = sect.Value.getProperty("in_ranges", ""),
                    note = sect.Value.getProperty("in_note", ""),
                    players = ResetSectPlayers(cfg, sect, org_sect_team_box.Children),
                };

                org_sects.Add(org_sect);
            }

            //sorted_org_sects = org_sects;
            entity.sorted_org_sects = org_sects.OrderBy(x => x.sub_tree_no).ToList();
        }

        //遞迴
        private void AppendSheetMain(TConfig cfg, TSheet entity, Spire.Xls.Workbook book, Spire.Xls.Worksheet sheetTemplate)
        {
            if (entity.in_tree_no == "" || entity.in_tree_no == "0") return;
            if (entity.org.org_short == "") return;
            if (cfg.sheets.Contains(entity.key)) return;
            //if (org.team_status != "") return;

            cfg.sheets.Add(entity.key);

            var sheet = book.CreateEmptySheet();
            sheet.CopyFrom(sheetTemplate);
            sheet.Name = entity.key;

            AppendFightSheet(cfg, entity, sheet);

            //教練獎狀 - 8強
            var b1 = entity.in_fight_id.StartsWith("M002");
            var b2 = entity.in_fight_id.StartsWith("M004");
            var b3 = entity.in_fight_id.StartsWith("M008");
            var b = b1 || b2 || b3;
            if (b3 && cfg.sub_event == 5)
            {
                var coachPos = "A51";
                sheet.Range[coachPos].Text = "教練獎狀：";
                sheet.Range[coachPos].Style.Font.FontName = "標楷體";
                sheet.Range[coachPos].Style.Font.Size = 12;

                var edgeRange = sheet.Range["A51:J51"];
                var edgeBottom = edgeRange.Borders[Spire.Xls.BordersLineType.EdgeBottom];
                edgeBottom.LineStyle = Spire.Xls.LineStyleType.Thin;
                edgeBottom.Color = System.Drawing.Color.Black;
            }

            var pg_name = cfg.itmProgram.getProperty("in_short_name", "").Replace("團", "");
            var headText = "&\"Times New Roman\"&12&B " + entity.alias;
            var footText = "&\"標楷體\"&12&B " + pg_name + " - " + entity.org.org_display;

            if (cfg.need_assigned)
            {
                headText = "&\"Times New Roman\"&12&B " + entity.alias2;
            }

            sheet.PageSetup.RightHeader = headText;
            sheet.PageSetup.RightFooter = footText;

        }

        private void AppendFightSheet(TConfig cfg, TSheet entity, Spire.Xls.Worksheet sheet)
        {
            SetHead(cfg, sheet, "A1", entity.A1, 18);
            SetHead(cfg, sheet, "A2", entity.A2, 16);
            SetHead(cfg, sheet, "A4", entity.A4, 16);
            SetHead(cfg, sheet, "C4", entity.C4, 16);
            SetHead(cfg, sheet, "E4", entity.E4, 16);
            SetHead(cfg, sheet, "H4", entity.H4, 16);

            if (entity.org.team_status != "")
            {
                SetFontColor(cfg, sheet, "H4");
            }

            var wsRow = 7;

            var sorted_org_sects = entity.sorted_org_sects;
            foreach (var sect in sorted_org_sects)
            {
                //var nm = sect.name;
                var nm = sect.name + Environment.NewLine + sect.note.Replace(sect.name + "限", "").Replace("級", "").Replace("第 ", "").Trim();
                SetNumCell(cfg, sheet, "A" + wsRow, sect.sub_tree_no, isBold: false);
                SetCell(cfg, sheet, "B" + wsRow, nm, isBold: true);
                SetCell(cfg, sheet, "C" + wsRow, sect.weight, isBold: true);

                foreach (var p in sect.players)
                {
                    var inn_display = p.value.getProperty("inn_display", "");
                    var inn_ranges = p.value.getProperty("inn_ranges", "");

                    SetCell(cfg, sheet, "E" + wsRow, inn_display, isBold: false);
                    SetCell(cfg, sheet, "I" + wsRow, inn_ranges, isBold: false);
                    wsRow++;
                }
                wsRow++;
            }
        }

        private List<TRow> ResetSectPlayers(TConfig cfg, TSect sect, List<TRow> source)
        {
            //用體重排序
            var list = cfg.is_special_sect
                ? source
                : source.OrderBy(x => x.weight).ToList();

            foreach (var row in list)
            {
                string in_name = row.value.getProperty("in_name", "");
                string in_weight_message = row.value.getProperty("in_weight_message", "");
                string in_sub_weight = row.value.getProperty("in_sub_weight", "");

                string display = in_name;
                if (in_weight_message != "")
                {
                    display += in_weight_message;
                }
                else if (row.weight > 0)
                {
                    display += "(" + row.word + row.weight + ")";
                    //display += "(" + row.weight + ")";
                }

                string ranges = GetPlayerSect(cfg, sect, in_sub_weight);

                row.value.setProperty("inn_display", display);
                row.value.setProperty("inn_ranges", ranges);
            }

            //空資料列
            var d = cfg.max_rcount - list.Count;
            for (int i = 0; i < d; i++)
            {
                var itmEmpty = cfg.inn.newItem();
                itmEmpty.setProperty("inn_display", "沒有選手");
                itmEmpty.setProperty("inn_ranges", "");
                list.Add(new TRow { value = itmEmpty });
            }
            return list;
        }

        //設定顏色
        private void SetFontColor(TConfig cfg, Spire.Xls.Worksheet sheet, string pos)
        {
            var range = sheet.Range[pos];
            range.Style.Font.Color = System.Drawing.Color.Red;
        }

        //設定標題列
        private void SetHead(TConfig cfg, Spire.Xls.Worksheet sheet, string pos, string value, int fontSize = 14)
        {
            var range = sheet.Range[pos];
            range.Text = value;

            range.Style.Font.FontName = "標楷體";
            range.Style.Font.IsBold = true;
            range.Style.Font.Size = fontSize;
            //range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
            range.Style.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;
        }

        //設定資料列
        private void SetCell(TConfig cfg, Spire.Xls.Worksheet sheet, string pos, string value, bool isBold = false)
        {
            var range = sheet.Range[pos];
            range.Text = value;

            range.Style.Font.FontName = "標楷體";
            range.Style.Font.IsBold = isBold;
            //range.Style.Font.Size = fontSize;
            range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
            range.Style.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;
        }


        //設定資料列
        private void SetCellLeft(TConfig cfg, Spire.Xls.Worksheet sheet, string pos, string value, bool isBold = false)
        {
            var range = sheet.Range[pos];
            range.Text = value;

            range.Style.Font.FontName = "標楷體";
            range.Style.Font.IsBold = isBold;
            //range.Style.Font.Size = fontSize;
            range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Left;
            range.Style.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;
        }

        //設定標題列
        private void SetNumCell(TConfig cfg, Spire.Xls.Worksheet sheet, string pos, string value, bool isBold = false)
        {
            var range = sheet.Range[pos];
            range.Text = value;

            range.NumberValue = GetIntVal(value);
            range.NumberFormat = "0";

            range.Style.Font.FontName = "Times New Roman";
            range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
            range.Style.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;
        }

        private string GetPlayerSect(TConfig cfg, TSect sect, string weight)
        {
            var in_ranges = sect.Value.getProperty("in_ranges", "");
            if (in_ranges == "") return "";

            //var values = in_ranges + ",無";
            var values = in_ranges;
            var arr = values.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            if (arr == null || arr.Length == 0) return "";

            List<string> list = new List<string>();
            foreach (var v in arr)
            {
                if (v == weight)
                {
                    list.Add("■" + v);
                }
                else
                {
                    list.Add("□" + v);
                }
            }

            return string.Join(",", list);
        }

        //取得組別量級選單
        private List<TSect> GetSectList(TConfig cfg, bool is_filter)
        {
            var lstSect = GetMeetingSection(cfg, is_filter);

            var sects = new List<TSect>();

            for (int i = 0; i < lstSect.Count; i++)
            {
                Item itmSect = lstSect[i];
                sects.Add(new TSect
                {
                    sub_id = i + 1,
                    name = itmSect.getProperty("in_name", ""),
                    gender = itmSect.getProperty("in_gender", ""),
                    weight = itmSect.getProperty("in_weight", ""),
                    note = itmSect.getProperty("in_note", ""),
                    Value = itmSect,
                });
            }

            //sects.Add(new TSect { No = 7, Name = "代表戰" });

            return sects;
        }

        //取得團體賽量級清單
        private List<Item> GetMeetingSection(TConfig cfg, bool is_filter)
        {
            string program_id = cfg.program_id;

            List<Item> result = new List<Item>();

            string sql = "SELECT * FROM In_Meeting_PSect WITH(NOLOCK) WHERE in_program = '" + program_id + "'"
                + " ORDER BY in_sub_id";

            if (is_filter)
            {
                sql = "SELECT * FROM In_Meeting_PSect WITH(NOLOCK) WHERE in_program = '" + program_id + "'"
                + " AND in_name <> N'代表戰'"
                + " ORDER BY in_sub_id";
            }

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item items = cfg.inn.applySQL(sql);
            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                result.Add(items.getItemByIndex(i));
            }

            return result;
        }

        private List<TRow> MapMainTeams(TConfig cfg, Item items)
        {
            var rows = new List<TRow>();
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                var sign_no = GetIntVal(item.getProperty("in_sign_no", "0"));
                var sub_id = GetIntVal(item.getProperty("in_sub_id", "0"));

                var row = rows.Find(x => x.no == sign_no);
                if (row == null)
                {
                    row = new TRow
                    {
                        no = sign_no,
                        Children = new List<TRow>(),
                    };
                    rows.Add(row);
                }

                var sect = row.Children.Find(x => x.no == sub_id);
                if (sect == null)
                {
                    sect = new TRow
                    {
                        no = sub_id,
                        Children = new List<TRow>(),
                    };
                    row.Children.Add(sect);
                }

                sect.Children.Add(new TRow
                {
                    value = item,
                    word = item.getProperty("in_sub_word", ""),
                    weight = GetDcmVal(item.getProperty("in_weight", "0")),
                });
            }
            return rows;
        }

        private List<TRow> MapMainEvents(TConfig cfg, Item items)
        {
            var rows = new List<TRow>();
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                rows.Add(new TRow
                {
                    id = item.getProperty("id", ""),
                    value = item,
                    foot1 = MapOrg(cfg, item, "f1_"),
                    foot2 = MapOrg(cfg, item, "f2_"),
                    Children = new List<TRow>(),
                });
            }
            return rows;
        }

        private void AppendChildren(TConfig cfg, List<TRow> rows, Item items)
        {
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_parent = item.getProperty("in_parent", "");
                var row = rows.Find(x => x.id == in_parent);
                if (row == null) continue;
                row.Children.Add(new TRow
                {
                    id = item.getProperty("id", ""),
                    value = item,
                    no = GetIntVal(item.getProperty("in_sub_id", "")),
                });
            }
        }

        private TOrg MapOrg(TConfig cfg, Item item, string prefix)
        {
            var result = new TOrg
            {
                foot = GetIntVal(item.getProperty(prefix + "foot", "0")),
                sign_no = GetIntVal(item.getProperty(prefix + "sign_no", "0")),
                org_code = item.getProperty(prefix + "org_code", ""),
                org_name = item.getProperty(prefix + "org_name", ""),
                org_short = item.getProperty(prefix + "org_short", ""),
                org_show = item.getProperty(prefix + "org_show", ""),
                team = item.getProperty(prefix + "team", ""),
                team_status = item.getProperty(prefix + "team_status", ""),
                player_name = item.getProperty(prefix + "player_name", ""),
                player_names = item.getProperty(prefix + "player_names", ""),
            };

            if (result.org_show != "") result.org_short = result.org_show;

            result.org_display = result.org_short + result.team;
            if (result.team_status != "") result.org_display += result.team_status;

            return result;
        }

        private Item GetChildrenEvents(TConfig cfg, string parent_id)
        {
            string event_cond = parent_id == ""
                ? ""
                : "AND t1.in_parent = '" + parent_id + "'";

            string sql = @"
                SELECT
	                t1.id
	                , t1.in_parent
	                , t1.in_tree_id
	                , t1.in_tree_no
                    , t1.in_sub_id
                    , t1.in_round
	                , t1.in_round_code
	                , t1.in_round_serial
	                , t1.in_next_win
	                , t1.in_next_foot_win
	                , t11.id                AS 'f1_did'
	                , t11.in_sign_foot      AS 'f1_foot'
	                , t11.in_player_org     AS 'f1_org_name'
	                , t11.in_player_team    AS 'f1_team'
	                , t11.in_player_name    AS 'f1_player_name'
	                , t11.in_player_sno     AS 'f1_player_sno'
	                , t11.in_player_status  AS 'f1_player_status'
	                , t12.id                AS 'f2_did'
	                , t12.in_sign_foot      AS 'f2_foot'
	                , t12.in_player_org     AS 'f2_org_name'
	                , t12.in_player_team    AS 'f2_team'
	                , t12.in_player_name    AS 'f2_player_name'
	                , t12.in_player_sno     AS 'f2_player_sno'
	                , t12.in_player_status  AS 'f2_player_status'
                FROM
	                IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t11 WITH(NOLOCK)
	                ON t11.source_id = t1.id
	                AND t11.in_sign_foot = 1
                INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t12 WITH(NOLOCK)
	                ON t12.source_id = t1.id
	                AND t12.in_sign_foot = 2
                WHERE
	                t1.source_id = '{#program_id}'
	                AND ISNULL(t1.in_tree_no, 0) > 0
	                AND t1.in_type = 's'
                    {#event_cond}
                ORDER BY
	                t1.in_tree_no
            ";

            sql = sql.Replace("{#program_id}", cfg.program_id)
                .Replace("{#event_cond}", event_cond);

            return cfg.inn.applySQL(sql);
        }

        private Item GetParentEvents(TConfig cfg, string event_id)
        {
            string event_cond = event_id == ""
                ? ""
                : "AND t1.id = '" + event_id + "'";

            string sql = @"
                SELECT
	                t1.id
	                , t1.in_tree_id
	                , t1.in_tree_no
                    , t1.in_round
	                , t1.in_round_code
	                , t1.in_round_serial
	                , t1.in_next_win
	                , t1.in_next_foot_win
	                , t1.in_fight_id
	                , t1.in_fight_note
	                , t11.id                AS 'f1_did'
	                , t11.in_sign_foot      AS 'f1_foot'
	                , t11.in_sign_no        AS 'f1_sign_no'
	                , t21.id                AS 'f1_tid'
	                , t21.in_stuff_b1       AS 'f1_org_code'
	                , t21.in_current_org    AS 'f1_org_name'
	                , t21.in_short_org      AS 'f1_org_short'
	                , t21.in_show_org       AS 'f1_org_show'
	                , t21.in_name           AS 'f1_plary_name'
	                , t21.in_names          AS 'f1_plary_names'
	                , t21.in_team           AS 'f1_team'
	                , t21.in_weight_message AS 'f1_team_status'
	                , t12.id                AS 'f2_did'
	                , t12.in_sign_foot      AS 'f2_foot'
	                , t12.in_sign_no        AS 'f2_sign_no'
	                , t22.id                AS 'f2_tid'
	                , t22.in_stuff_b1       AS 'f2_org_code'
	                , t22.in_current_org    AS 'f2_org_name'
	                , t22.in_short_org      AS 'f2_org_short'
	                , t22.in_show_org       AS 'f2_org_show'
	                , t22.in_name           AS 'f2_player_name'
	                , t22.in_names          AS 'f2_player_names'
	                , t22.in_team           AS 'f2_team'
	                , t22.in_weight_message AS 'f2_team_status'
	                , t31.id                AS 'site_id'
	                , t31.in_name           AS 'site_name'
	                , t31.in_code           AS 'site_code'
	                , t31.in_code_en        AS 'site_code_en'
                FROM
	                IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t11 WITH(NOLOCK)
	                ON t11.source_id = t1.id
	                AND t11.in_sign_foot = 1
                INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t12 WITH(NOLOCK)
	                ON t12.source_id = t1.id
	                AND t12.in_sign_foot = 2
                LEFT OUTER JOIN
	                VU_MEETING_PTEAM t21 WITH(NOLOCK)
	                ON t21.source_id = t1.source_id
	                AND t21.in_sign_no = t11.in_sign_no
                    AND ISNULL(t21.in_type, '') <> 's'
                LEFT OUTER JOIN
	                VU_MEETING_PTEAM t22 WITH(NOLOCK)
	                ON t22.source_id = t1.source_id
	                AND t22.in_sign_no = t12.in_sign_no
                    AND ISNULL(t22.in_type, '') <> 's'
                INNER JOIN
	                IN_MEETING_SITE t31 WITH(NOLOCK)
	                ON t31.id = t1.IN_SITE
                WHERE
	                t1.source_id = '{#program_id}'
	                AND ISNULL(t1.in_tree_no, 0) > 0
	                AND t1.in_type = 'p'
                    {#event_cond}
                ORDER BY
	                t1.in_tree_id
            ";

            sql = sql.Replace("{#program_id}", cfg.program_id)
                .Replace("{#event_cond}", event_cond);

            return cfg.inn.applySQL(sql);
        }

        private Item GetSubTeams(TConfig cfg, string team_id)
        {
            string team_cond = team_id == ""
                ? ""
                : "AND t3.id = '" + team_id + "'";

            string sql = @"
                SELECT 
	                t1.in_parent
	                , t1.in_sub_id
	                , t1.in_weight_message
                    , t1.in_sub_weight
                    , t1.in_sub_word
	                , t2.in_name
	                , t2.in_sno
	                , t2.in_gender
	                , t2.in_weight
	                , t2.in_team_weight_n2
	                , t2.in_team_weight_n3
	                , t3.in_sign_no
                FROM 
	                VU_MEETING_PTEAM t1 
                INNER JOIN
	                IN_MEETING_USER t2 WITH(NOLOCK)
	                ON t2.id = t1.in_muser
                INNER JOIN
	                IN_MEETING_PTEAM t3 WITH(NOLOCK)
	                ON t3.id = t1.in_parent
                WHERE
	                t1.source_id = '{#program_id}'
	                AND ISNULL(t1.in_type, '') = 's'
                    {#team_cond}
                ORDER BY
	                t1.in_parent
	                , t1.in_sub_id
	                , t1.in_sno
            ";

            sql = sql.Replace("{#program_id}", cfg.program_id)
                .Replace("{#team_cond}", team_cond);

            return cfg.inn.applySQL(sql);
        }

        private class TRow
        {
            public string id { get; set; }
            public Item value { get; set; }
            public TOrg foot1 { get; set; }
            public TOrg foot2 { get; set; }
            public List<TRow> Children { get; set; }

            public int no { get; set; }
            public decimal weight { get; set; }
            public string word { get; set; }

        }

        private class TOrg
        {
            public int foot { get; set; }
            public int sign_no { get; set; }

            public string org_code { get; set; }
            public string org_name { get; set; }
            public string org_short { get; set; }
            public string org_show { get; set; }
            public string org_display { get; set; }

            public string team { get; set; }
            public string team_status { get; set; }

            public string player_name { get; set; }
            public string player_names { get; set; }
            public string player_sno { get; set; }
            public string player_status { get; set; }

            public Item value { get; set; }
        }

        private class TSect
        {
            public int sub_id { get; set; }
            public string name { get; set; }
            public string gender { get; set; }
            public string weight { get; set; }
            public string note { get; set; }

            public Item Value { get; set; }
        }

        private class TSectRow
        {
            public string sub_tree_no { get; set; }

            public int id { get; set; }
            public string name { get; set; }
            public string weight { get; set; }
            public string ranges { get; set; }
            public string note { get; set; }

            public List<TRow> players { get; set; }
        }

        private class TSectPlayer
        {
            public string name { get; set; }
            public string ranges { get; set; }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }

            public string meeting_id { get; set; }
            public string program_id { get; set; }
            public string event_id { get; set; }
            public string team_id { get; set; }
            public string team_sign_no { get; set; }
            public string file_extension { get; set; }
            public string round_serial { get; set; }
            public string scene { get; set; }

            public bool need_all { get; set; }
            public bool need_assigned { get; set; }
            public bool need_next { get; set; }

            public Item itmMeeting { get; set; }
            public Item itmProgram { get; set; }
            public Item itmXlsx { get; set; }

            public string in_title { get; set; }

            public string pg_name2 { get; set; }
            public string in_battle_type { get; set; }
            public bool is_robin { get; set; }
            public int sub_event { get; set; }
            public int sect_start { get; set; }
            public int max_rcount { get; set; }

            public string template_path { get; set; }
            public string export_path { get; set; }

            public List<TSect> sects { get; set; }

            public Item itmParents { get; set; }
            public Item itmChildren { get; set; }
            public Item itmTeams { get; set; }

            public List<string> sheets { get; set; }
            public System.Drawing.Color head_color { get; set; }

            public bool is_special_sect { get; set; }

            public List<string> eventIdList { get; set; }
            public string roundEventId { get; set; }
            public Item itmNextEvent { get; set; }
        }

        private class TSheet
        {
            public TOrg org { get; set; }
            public TRow evt { get; set; }
            public string in_round_serial { get; set; }
            public string org_name { get; set; }
            public int sign_no { get; set; }
            public int sign_foot { get; set; }
            public string site_code { get; set; }
            public string site_code_en { get; set; }
            public string in_tree_no { get; set; }
            public string in_fight_id { get; set; }
            public string in_fight_note { get; set; }
            
            public string rnd { get; set; }
            public string key { get; set; }
            public string alias { get; set; }
            public string alias2 { get; set; }
            public TRow team_page { get; set; }

            public string A1 { get; set; }
            public string A2 { get; set; }
            public string A3 { get; set; }
            public string A4 { get; set; }
            public string C4 { get; set; }
            public string E4 { get; set; }
            public string H4 { get; set; }

            public List<TSectRow> sorted_org_sects { get; set; }
        }

        private string GetOneProgramXlsName(TConfig cfg)
        {
            string pg_title = cfg.itmProgram.getProperty("in_name2", "");
            string xlsName = cfg.in_title + "_" + pg_title + "_出賽單";

            string guid = System.Guid.NewGuid().ToString().Substring(0, 4).ToUpper()
                + "_" + DateTime.Now.ToString("yyyyMMddHHmmss");

            return xlsName + "_" + guid;
        }

        private string GetOrgTeamXlsName(TConfig cfg)
        {
            var sql = "SELECT in_short_org, in_show_org FROM IN_MEETING_PTEAM WITH(NOLOCK) WHERE id = '" + cfg.team_id + "'";
            var itmOrg = cfg.inn.applySQL(sql);
            if (itmOrg.isError() || itmOrg.getResult() == "")
            {
                itmOrg = cfg.inn.newItem();
            }

            var in_short_org = itmOrg.getProperty("in_short_org", "");
            var in_show_org = itmOrg.getProperty("in_show_org", "");
            if (in_show_org != "") in_short_org = in_show_org;

            string pg_title = cfg.itmProgram.getProperty("in_short_name", "");
            string xlsName = cfg.in_title + "_" + pg_title + "_" + in_short_org + "_出賽單";
            return xlsName;
        }

        private List<string> GetAssignedEventIdList(TConfig cfg)
        {
            var list = new List<string>();
            var sql = @"
                SELECT DISTINCT
	                t1.id
	                , t1.in_win_status
                FROM
	                IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                WHERE
	                t1.source_id = '{#program_id}'
	                AND ISNULL(t1.in_tree_no, 0) > 0
	                AND ISNULL(t1.in_type, '') IN ('', 'p')
	                AND ISNULL(t2.in_sign_no, '') NOT IN ('', '0')
	                AND ISNULL(t1.in_fight_note, '') = 'sign'
            ";
            sql = sql.Replace("{#program_id}", cfg.program_id);
            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var id = item.getProperty("id", "");
                list.Add(id);
            }
            return list;
        }

        private Item GetNextEventItem(TConfig cfg)
        {
            var sql = @"
                SELECT 
	                t1.id
	                , t1.in_next_win
					, t2.in_tree_no AS 'next_tree_no'
					, t3.in_code	AS 'next_site_code'
					, t3.in_code_en AS 'next_site_code_en'
					, t4.in_sign_no
					, t4.in_target_no
                FROM
	                IN_MEETING_PEVENT t1 WITH(NOLOCK)
				INNER JOIN
					IN_MEETING_PEVENT t2 WITH(NOLOCK)
					ON t2.id = t1.in_next_win
				INNER JOIN
					IN_MEETING_SITE t3 WITH(NOLOCK)
					ON t3.id = t2.in_site
				INNER JOIN
					IN_MEETING_PEVENT_DETAIL t4 WITH(NOLOCK)
					ON t4.source_id = t1.id
					AND t4.in_sign_foot = 1
                WHERE
	                t1.id = '{#event_id}'
            ";
            sql = sql.Replace("{#event_id}", cfg.roundEventId);
            var item = cfg.inn.applySQL(sql);
            if (item.isError() || item.getResult() == "") return cfg.inn.newItem();
            return item;
        }

        private int GetMaxPlayerCount(int sub_event)
        {
            switch (sub_event)
            {
                case 3: return 5;
                case 5: return 7;
                case 6: return 6;
                //case 6: return 5;
                default: return 5;
            }
        }

        private decimal GetDcmVal(string value, decimal def = 0)
        {
            decimal result = def;
            decimal.TryParse(value, out result);
            return result;
        }

        private int GetIntVal(string value, int def = 0)
        {
            if (value == null || value == "") return 0;
            int result = def;
            int.TryParse(value, out result);
            return result;
        }
    }
}