﻿using Aras.IOM;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WorkHelp.ArasDesk.Methods.Judo.Fight
{
    public class in_team_battle_register_org : Item
    {
        public in_team_battle_register_org(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 團體戰-單位註冊
                日期: 
                    - 2021-12-13 領取簽名總表 (Lina)
                    - 2021-10-13 創建 (Lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]in_team_battle_register_org";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, this.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,

                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                ssn_loginid = itmR.getProperty("ssn_loginid", ""),//b.aspx 附加參數

                meeting_id = itmR.getProperty("meeting_id", ""),
                program_id = itmR.getProperty("program_id", ""),
                team_id = itmR.getProperty("team_id", ""),
                in_index = itmR.getProperty("in_index", ""),
                scene = itmR.getProperty("scene", ""),
                sub_scene = itmR.getProperty("sub_scene", ""),
                unfinished = itmR.getProperty("unfinished", ""),
            };

            if (cfg.scene == "weight_link")
            {
                cfg.isMeetingAdmin = true;
                cfg.strUserName = "lwu001";
                cfg.in_creator_sno = "lwu001";
                cfg.itmResume = cfg.inn.newItem();
            }
            else
            {
                if (cfg.ssn_loginid != "")
                {
                    CCO.Utilities.WriteDebug(strMethodName, cfg.ssn_loginid);
                    throw new Exception("不開放該功能");
                }

                //檢查頁面權限
                Item itmPermit = inn.applyMethod("In_CheckIdentity", "<method>" + strMethodName + "</method><code>" + "ALL" + "</code>");
                cfg.isMeetingAdmin = itmPermit.getProperty("isMeetingAdmin", "") == "1";

                Item itmCurrentUser = inn.getItemById("User", cfg.strUserId);
                cfg.strUserName = itmCurrentUser.getProperty("last_name", "");
                cfg.in_creator_sno = itmCurrentUser.getProperty("login_name", "");

                cfg.itmResume = inn.applySQL("SELECT in_current_org FROM IN_RESUME WITH(NOLOCK) WHERE login_name = '" + cfg.in_creator_sno + "'");
            }

            switch (cfg.scene)
            {
                case "team_mode_update":
                    if (cfg.isMeetingAdmin)
                    {
                        UpdateTeamMode(cfg, itmR);
                    }
                    break;

                case "state_update":
                    if (cfg.isMeetingAdmin)
                    {
                        UpdateState(cfg, itmR);
                    }
                    break;

                case "weight_link":
                    LinkWeight(cfg, itmR);
                    break;

                case "weight_update":
                    if (cfg.isMeetingAdmin)
                    {
                        UpdateWeight(cfg, itmR);
                        WeightTables(cfg, itmR);
                    }
                    break;

                case "update"://單位註冊-選手量級修改
                    UpdateTeamWeight(cfg, itmR);
                    if (cfg.program_id != "")
                    {
                        if (cfg.sub_scene == "weight_page")
                        {
                            WeightTables(cfg, itmR);
                        }
                        else
                        {
                            OrgTables(cfg, itmR);
                        }
                    }
                    break;

                case "status_page"://單位註冊狀態
                    StatusPage(cfg, itmR);
                    break;

                case "weight_page"://單位過磅
                    OrgPage(cfg, itmR);
                    if (cfg.isMeetingAdmin && cfg.program_id != "")
                    {
                        //資料表
                        WeightTables(cfg, itmR);
                    }
                    break;

                default:
                    OrgPage(cfg, itmR);
                    if (cfg.program_id != "")
                    {
                        OrgTables(cfg, itmR);
                    }

                    if (cfg.isMeetingAdmin)
                    {
                        itmR.setProperty("func_box_css", "item_show_1");
                    }
                    else
                    {
                        itmR.setProperty("func_box_css", "item_show_0");
                    }
                    break;
            }

            return itmR;
        }

        private void UpdateTeamMode(TConfig cfg, Item itmReturn)
        {
            var muid = itmReturn.getProperty("muid", "");
            var in_team_mode = itmReturn.getProperty("in_team_mode", "");
            var sql = "UPDATE IN_MEETING_USER SET in_team_mode = '" + in_team_mode + "' WHERE id = '" + muid + "'";
            cfg.inn.applySQL(sql);
        }

        //更新體重 (from 過磅單)
        private void LinkWeight(TConfig cfg, Item itmReturn)
        {
            string meeting_id = itmReturn.getProperty("meeting_id", "");
            string muid = itmReturn.getProperty("muid", "");
            string in_l1 = itmReturn.getProperty("in_l1", "");
            string in_l2 = itmReturn.getProperty("in_l2", "");
            string in_l3 = itmReturn.getProperty("in_l3", "");
            string in_index = itmReturn.getProperty("in_index", "");
            string in_creator_sno = itmReturn.getProperty("in_creator_sno", "");
            string in_weight = itmReturn.getProperty("in_weight", "");

            string in_team_key = string.Join("-", new List<string>
            {
                in_l1,
                in_l2,
                in_l3,
                in_index,
                in_creator_sno,
            });

            string sql = "SELECT TOP 1 source_id, id FROM IN_MEETING_PTEAM WITH(NOLOCK)"
                + " WHERE in_meeting = '" + meeting_id + "'"
                + " AND in_team_key = '" + in_team_key + "'"
                + " AND ISNULL(in_type, '') IN ('', 'p')"
                ;

            Item itmPTeam = cfg.inn.applySQL(sql);
            if (itmPTeam.isError() || itmPTeam.getResult() == "")
            {
                return;
            }

            cfg.program_id = itmPTeam.getProperty("source_id", "");

            Item itmData = cfg.inn.newItem();
            itmData.setProperty("muid", muid);
            itmData.setProperty("pid", itmPTeam.getProperty("id", ""));
            itmData.setProperty("value", in_weight);

            var view = MapWeightView(cfg, itmData);

            if (view.need_auto_match)
            {
                var model = new TEditModel
                {
                    muid = view.muid,
                    pid = view.pid,
                    cid = "",
                    sub = "",
                    name = "",
                    need_remove = "",
                    in_team_v1 = view.in_team_v1,
                    in_team_v2 = view.in_team_v2,
                };

                //移除子隊伍量級資料
                if (view.canRemoveChild)
                {
                    RemoveChildTeam(cfg, model);
                }
                //自動匹配相同量級位置
                AutoMapSectWeight(cfg, model, view.itmMUser, view.itmParent);
                //重設子隊伍流水序號
                ResetInSubSerial(cfg, model);
            }

            string sql_upd1 = "UPDATE IN_MEETING_PTEAM SET"
                + "  in_check_result = " + view.tm_check_result
                + ", in_check_status = " + view.tm_check_status
                + ", in_weight_result = " + view.tm_weight_result
                + ", in_weight_message = " + view.tm_weight_message
                + ", in_weight_value = " + view.value_s
                + " WHERE in_parent = '" + view.pid + "'"
                + " AND in_muser = '" + view.muid + "'"
                ;

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql_upd1);
            cfg.inn.applySQL(sql_upd1);
            
            string sql_upd2 = "UPDATE IN_MEETING_USER SET"
                + "  in_team_weight = N'" + view.in_team_weight + "'"
                + ", in_team_r1 = N'" + view.in_team_r1 + "'"
                + ", in_team_r2 = N'" + view.in_team_r2 + "'"
                + ", in_team_v1 = N'" + view.in_team_v1 + "'"
                + ", in_team_v2 = N'" + view.in_team_v2 + "'"
                + " WHERE id = '" + view.muid + "'";

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql_upd2);
            cfg.inn.applySQL(sql_upd2);
        }

        //更新體重 (from 單位過磅)
        private void UpdateWeight(TConfig cfg, Item itmReturn)
        {
            var view = MapWeightView(cfg, itmReturn);

            if (view.need_auto_match)
            {
                var model = new TEditModel
                {
                    muid = view.muid,
                    pid = view.pid,
                    cid = "",
                    sub = "",
                    name = "",
                    need_remove = "",
                    in_team_v1 = view.in_team_v1,
                    in_team_v2 = view.in_team_v2,
                };

                //移除子隊伍量級資料
                if (view.canRemoveChild)
                {
                    RemoveChildTeam(cfg, model);
                }
                //自動匹配相同量級位置
                AutoMapSectWeight(cfg, model, view.itmMUser, view.itmParent);
                //重設子隊伍流水序號
                ResetInSubSerial(cfg, model);
            }

            string sql = "UPDATE IN_MEETING_PTEAM SET"
                + "  in_check_result = " + view.tm_check_result
                + ", in_check_status = " + view.tm_check_status
                + ", in_weight_result = " + view.tm_weight_result
                + ", in_weight_message = " + view.tm_weight_message
                + ", in_weight_value = " + view.value_s
                + " WHERE in_parent = '" + view.pid + "'"
                + " AND in_muser = '" + view.muid + "'"
                ;

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);
            cfg.inn.applySQL(sql);

            sql = "UPDATE IN_MEETING_USER SET"
                + "  in_weight = " + view.value_d
                + ", in_weight_status = " + view.mu_weight_status
                + ", in_weight_createid = '" + cfg.strUserId + "'"
                + ", in_weight_createname = N'" + cfg.strUserName + "'"
                + ", in_weight_time = N'" + view.in_weight_time + "'"
                + ", in_team_weight = N'" + view.in_team_weight + "'"
                + ", in_team_r1 = '" + view.in_team_r1 + "'"
                + ", in_team_r2 = '" + view.in_team_r2 + "'"
                + ", in_team_v1 = '" + view.in_team_v1 + "'"
                + ", in_team_v2 = '" + view.in_team_v2 + "'"
                + " WHERE id = '" + view.muid + "'";

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);
            cfg.inn.applySQL(sql);
        }

        private void SetSectWeightResult(TConfig cfg, TWeightView view)
        {
            string sql = @"
                SELECT
	                t1.id          AS 'cid' --Child Id
	                , t1.in_name
	                , t2.in_name   AS 'sect_name'
	                , t2.in_weight AS 'sect_weight'
	                , t2.in_ranges AS 'sect_ranges'
	                , t2.in_note   AS 'sect_note'
	                , t2.in_min    AS 'sect_min'
	                , t2.in_max    AS 'sect_max'
                FROM 
	                IN_MEETING_PTEAM t1 WITH(NOLOCK) 
                INNER JOIN
	                IN_MEETING_PSECT t2 WITH(NOLOCK) 
	                ON t2.in_program = t1.source_id
	                AND t2.in_sub_id = t1.in_sub_id
                WHERE
	                t1.in_parent = '{#pid}'
	                AND ISNULL(t1.in_type, '') = 's'
	                AND t1.in_muser = '{#muid}'
            ";

            sql = sql.Replace("{#pid}", view.pid)
                .Replace("{#muid}", view.muid);

            Item items = cfg.inn.applySQL(sql);
            int count = items.getItemCount();
            if (count <= 0)
            {
                //未設定，視為不合格
                SetWeightDQ(view);
                return;
            }

            SetWeightDQ(view);
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                var min = GetDcmVal(item.getProperty("sect_min", "0"));
                var max = GetDcmVal(item.getProperty("sect_max", "0"));
                if (view.dcmValue >= min && view.dcmValue <= max)
                {
                    SetWeightOK(view);
                    break;
                }
            }
        }

        private void SetWeightDQ(TWeightView view)
        {
            view.mu_weight_status = "'dq'";
            view.mu_weight_result = "'0'";
            view.tm_check_result = "'0'";
            view.tm_check_status = "N'不通過'";
            view.tm_weight_result = "'0'";
            view.tm_weight_message = "'(DQ)'";
        }

        private void SetWeightOK(TWeightView model)
        {
            if (model.value == "" || model.value == "0")
            {
                model.mu_weight_status = "NULL";//已過磅
                model.mu_weight_result = "NULL";//過磅合格

                model.tm_check_result = "NULL";
                model.tm_check_status = "NULL";
                model.tm_weight_result = "NULL";
                model.tm_weight_message = "NULL";//不需註記特殊狀態
            }
            else
            {
                model.mu_weight_status = "'on'";//已過磅
                model.mu_weight_result = "'1'";//過磅合格

                model.tm_check_result = "'1'";
                model.tm_check_status = "'通過'";
                model.tm_weight_result = "'1'";
                model.tm_weight_message = "''";//不需註記特殊狀態
            }
        }

        private TWeightView MapWeightView(TConfig cfg, Item itmReturn)
        {
            string muid = itmReturn.getProperty("muid", "");//in_meeting_user id
            string pid = itmReturn.getProperty("pid", "");//team parent id
            string value = itmReturn.getProperty("value", "");

            bool need_qry = false;
            string value_d = itmReturn.getProperty("value", "");
            string value_s = itmReturn.getProperty("value", "");

            if (value_d == "")
            {
                value_d = "NULL";
            }

            if (value_s == "")
            {
                value_s = "NULL";
            }
            else
            {
                need_qry = true;
                value_s = "'" + value_s + "'";
            }

            string sql_meeting = "SELECT id, in_title, in_weight_mode FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'";
            Item itmMeeting = cfg.inn.applySQL(sql_meeting);
            if (itmMeeting.isError() || itmMeeting.getResult() == "")
            {
                throw new Exception("查無活動");
            }

            string sql_muser = @"SELECT * FROM IN_MEETING_USER (NOLOCK) WHERE id = '" + muid + "'";
            Item itmMUser = cfg.inn.applySQL(sql_muser);
            if (itmMUser.isError() || itmMUser.getResult() == "")
            {
                throw new Exception("查無與會者");
            }

            Item itmParent = cfg.inn.applySQL("SELECT * FROM IN_MEETING_PTEAM WITH(NOLOCK) WHERE id = '" + pid + "'");
            if (itmParent.isError() || itmParent.getResult() == "")
            {
                throw new Exception("查無隊伍資料");
            }

            string program_id = itmParent.getProperty("source_id", "");
            Item itmProgram = cfg.inn.applySQL("SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + program_id + "'");
            if (itmProgram.isError() || itmProgram.getResult() == "")
            {
                throw new Exception("查無組別資料");
            }

            string in_team_weight = itmMUser.getProperty("in_team_weight", "");
            string in_team_sect = GetTeamSect(itmMUser);
            string in_team_r1 = "";
            string in_team_r2 = "";

            if (need_qry)
            {
                string sql_weight = "SELECT TOP 1 * FROM IN_GROUP_WEIGHT (NOLOCK)"
                    + " WHERE in_group = '" + in_team_sect + "'"
                    + " AND in_min <= " + value
                    + " AND in_max >= " + value
                    + " ORDER BY in_no"
                    ;
                Item itmWeight = cfg.inn.applySQL(sql_weight);
                if (!itmWeight.isError() && itmWeight.getResult() != "")
                {
                    in_team_r1 = itmWeight.getProperty("in_level", "");//級
                    in_team_r2 = itmWeight.getProperty("in_weight", "");//量
                }
            }

            var view = new TWeightView
            {
                muid = muid,
                pid = pid,
                value = value,
                value_d = value_d,
                value_s = value_s,
                dcmValue = GetDcmVal(value),

                itmMeeting = itmMeeting,
                itmMUser = itmMUser,
                itmParent = itmParent,
                itmProgram = itmProgram,

                in_sno = itmMUser.getProperty("in_sno", ""),
                in_team_mode = itmMUser.getProperty("in_team_mode", ""),
                in_team_weight = in_team_weight,
                in_team_sect = in_team_sect,
                in_team_r1 = in_team_r1,//實際-級
                in_team_r2 = in_team_r2,//實際-量
                in_team_n1 = itmMUser.getProperty("in_team_n1", ""),//註冊-級
                in_team_n2 = itmMUser.getProperty("in_team_n2", ""),//註冊-量

                in_weight_time = System.DateTime.Now.AddHours(-8).ToString("yyyy/MM/dd HH:mm:ss"),

                mt_auto_match = GetMeetingVariable(cfg, "team_battle_auto_match"),
                pg_auto_match = itmProgram.getProperty("in_team_weight_check", ""),
                noSoloRegister = false,
            };

            if (view.in_team_n1 == "")
            {
                view.in_team_n1 = view.in_team_r1;
            }

            if (view.in_team_n2 == "")
            {
                view.in_team_n2 = view.in_team_r2;
                view.noSoloRegister = true;
            }

            view.range = TWeightRange.TrueWeight;
            view.in_team_v1 = view.in_team_r1;
            view.in_team_v2 = view.in_team_r2;

            view.canRemoveChild = true;
            if (view.in_team_mode == "2")
            {
                //允許多級
                view.canRemoveChild = false;
            }

            if (view.pg_auto_match == "1")
            {
                //不需自動匹配，依註冊
                view.need_auto_match = false;
                view.in_team_v1 = view.in_team_n1;
                view.in_team_v2 = view.in_team_n2;
                //需檢查體重起迄
                SetSectWeightResult(cfg, view);
            }
            else if (view.noSoloRegister)
            {
                //需自動匹配，依實際(未報個人)
                view.need_auto_match = true;
                SetWeightOK(view);
            }
            else
            {
                //需自動匹配，有報個人，需進行分析
                view.need_auto_match = true;
                AutoAnalysisWeight(cfg, view);
                SetWeightOK(view);
            }

            return view;
        }

        private string GetMeetingVariable(TConfig cfg, string in_key)
        {
            var sql = "SELECT TOP 1 id, in_value FROM IN_MEETING_VARIABLE WITH(NOLOCK)"
                + " WHERE source_id = '" + cfg.meeting_id + "'"
                + " AND in_key = '" + in_key + "'";
            var itmV = cfg.inn.applySQL(sql);
            if (itmV.isError() || itmV.getResult() == "") return "";
            return itmV.getProperty("in_value", "");
        }

        private void AutoAnalysisWeight(TConfig cfg, TWeightView view)
        {
            string sql = "SELECT TOP 1 * FROM IN_GROUP_WEIGHT (NOLOCK)"
                + " WHERE in_group = '" + view.in_team_sect + "'" //社男甲
                + " AND in_weight = '" + view.in_team_n2 + "'" //-60kg(小寫)
                + " ORDER BY in_no"
                ;

            Item itmWeight = cfg.inn.applySQL(sql);
            if (itmWeight.isError() || itmWeight.getResult() == "")
            {
                //查無量級表，依實際體重
                return;
            }

            var min = GetDcmVal(itmWeight.getProperty("in_min", "0"));
            var max1 = GetDcmVal(itmWeight.getProperty("in_max", "0"));
            var max2 = GetDcmVal(itmWeight.getProperty("in_max2", "0"));//+5%

            if (view.dcmValue <= 0)
            {
                view.range = TWeightRange.None;
            }
            else if (view.dcmValue < min)
            {
                //過輕，依實際體重
            }
            else if (view.dcmValue <= max1)
            {
                //範圍內，依實際體重
            }
            else if (view.dcmValue <= max2)
            {
                var status = MapWeightStatus(cfg, view);
                if (status.w1)
                {
                    //昨日有過磅，依實際體重
                }
                else if (status.w2)
                {
                    //小於等於 5%，依註冊體重，隔了一天過磅
                    view.range = TWeightRange.AsEqual;
                    view.in_team_v1 = view.in_team_n1;
                    view.in_team_v2 = view.in_team_n2;
                }
            }
            else
            {
                //超過 5%，依實際體重
            }
        }

        private TWStatus MapWeightStatus(TConfig cfg, TWeightView view)
        {
            var model = new TWStatus { w1 = false, w2 = false, w3 = false };

            var in_fight_day = view.itmProgram.getProperty("in_fight_day", "");
            var in_weight_mode = view.itmMeeting.getProperty("in_weight_mode", "");
            var wd = in_weight_mode == "fight-day" ? GetDateTime(in_fight_day) : GetDateTime(in_fight_day).AddDays(-1);

            var wd1 = wd.AddDays(-1).Date;
            var wd2 = wd.AddDays(-2).Date;
            var wd3 = wd.AddDays(-3).Date;

            var sql = "SELECT id, in_l1, in_l2, in_l3, in_weight, in_weight_time FROM IN_MEETING_USER WITH(NOLOCK)"
                + " WHERE source_id = '" + cfg.meeting_id + "'"
                + " AND in_sno = '" + view.in_sno + "'"
                + " AND id <> '" + view.muid + "'"
                + " ORDER BY in_weight_time"
                ;

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var in_weight_time = item.getProperty("in_weight_time", "");
                var wt = GetDateTime(in_weight_time).AddHours(8).Date;
                if (wt == wd1) model.w1 = true;
                else if (wt == wd2) model.w2 = true;
                else if (wt == wd3) model.w3 = true;
            }

            return model;
        }

        private string GetTeamSect(Item itmMUser)
        {
            string in_team_sect = itmMUser.getProperty("in_team_sect", "");
            if (in_team_sect != "") return in_team_sect;

            string in_gender = itmMUser.getProperty("in_gender", "");
            string in_l2 = itmMUser.getProperty("in_l2", "");
            string in_l3 = itmMUser.getProperty("in_l3", "");
            string v = in_l2 + "-" + in_l3;

            if (v.Contains("特別"))
            {
                return "";
            }

            if (v.Contains("社會"))
            {
                if (v.Contains("甲"))
                {
                    if (in_gender == "女")
                    {
                        in_team_sect = "社女甲";
                    }
                    else if (in_gender == "男")
                    {
                        in_team_sect = "社男甲";
                    }
                }
                else if (v.Contains("乙"))
                {
                    if (in_gender == "女")
                    {
                        in_team_sect = "社女乙";
                    }
                    else if (in_gender == "男")
                    {
                        in_team_sect = "社男乙";
                    }
                }
            }
            else if (v.Contains("大專"))
            {
                if (v.Contains("甲"))
                {
                    if (in_gender == "女")
                    {
                        in_team_sect = "大女甲";
                    }
                    else if (in_gender == "男")
                    {
                        in_team_sect = "大男甲";
                    }
                }
                else if (v.Contains("乙"))
                {
                    if (in_gender == "女")
                    {
                        in_team_sect = "大女乙";
                    }
                    else if (in_gender == "男")
                    {
                        in_team_sect = "大男乙";
                    }
                }
            }
            else if (v.Contains("高中"))
            {
                if (in_gender == "女")
                {
                    in_team_sect = "高女";
                }
                else if (in_gender == "男")
                {
                    in_team_sect = "高男";
                }
            }
            else if (v.Contains("國中"))
            {
                if (in_gender == "女")
                {
                    in_team_sect = "國女";
                }
                else if (in_gender == "男")
                {
                    in_team_sect = "國男";
                }
            }
            else if (v.Contains("國小"))
            {
                if (in_gender == "女")
                {
                    in_team_sect = "小女A";
                }
                else if (in_gender == "男")
                {
                    in_team_sect = "小男A";
                }
            }
            else
            {
                if (in_gender == "女")
                {
                    in_team_sect = "社女甲";
                }
                else if (in_gender == "男")
                {
                    in_team_sect = "社男甲";
                }
            }

            return in_team_sect;
        }

        //過磅狀態
        private void UpdateState(TConfig cfg, Item itmReturn)
        {
            var muid = itmReturn.getProperty("muid", "");//in_meeting_user id
            var pid = itmReturn.getProperty("pid", "");//team parent id
            var value = itmReturn.getProperty("value", "");
            if (value == "ok") value = "";

            var sql = "UPDATE IN_MEETING_PTEAM SET"
                + " in_weight_message = N'" + value + "'"
                + " WHERE in_parent = '" + pid + "'"
                + " AND in_muser = '" + muid + "'";

            var stats = new string[] { "(請假)", "(DNS)", "(DQ)", "(停賽)" };
            var needRemove = stats.Contains(value);
            if (needRemove)
            {
                sql = "DELETE FROM IN_MEETING_PTEAM WHERE in_parent = '" + pid + "' AND in_muser = '" + muid + "'";
            }

            var itmSQL = cfg.inn.applySQL(sql);

            if (itmSQL.isError())
            {
                throw new Exception("過磅狀態 更新失敗");
            }

            sql = "UPDATE IN_MEETING_USER SET"
                + " in_weight_status = N'" + MapStatus2(cfg, value) + "'"
                + " WHERE id = '" + muid + "'"
                ;

            itmSQL = cfg.inn.applySQL(sql);

            if (itmSQL.isError())
            {
                throw new Exception("過磅狀態 更新失敗");
            }
        }

        private void UpdateTeamWeight(TConfig cfg, Item itmReturn)
        {
            var model = new TEditModel
            {
                muid = itmReturn.getProperty("muid", ""),
                pid = itmReturn.getProperty("pid", ""),
                cid = itmReturn.getProperty("cid", ""),
                sub = itmReturn.getProperty("sub", ""),
                name = itmReturn.getProperty("name", ""),
                need_remove = itmReturn.getProperty("need_remove", ""),
                in_team_v1 = "",
                in_team_v2 = itmReturn.getProperty("weight", "") + "kg",
            };
            UpdateTeamWeightBySetting(cfg, model);
        }

        private void UpdateTeamWeightBySetting(TConfig cfg, TEditModel model)
        {
            string sql = "";

            sql = "SELECT * FROM IN_MEETING_USER WITH(NOLOCK) WHERE id = '" + model.muid + "'";
            Item itmMUser = cfg.inn.applySQL(sql);
            if (itmMUser == null || itmMUser.isError() || itmMUser.getResult() == "")
            {
                throw new Exception("查無與會者資料");
            }

            sql = "SELECT * FROM IN_MEETING_PTEAM WITH(NOLOCK) WHERE id = '" + model.pid + "'";
            Item itmParent = cfg.inn.applySQL(sql);
            if (itmParent.isError() || itmParent.getResult() == "")
            {
                throw new Exception("查無隊伍資料");
            }

            var canRemoveChild = true;
            
            var in_team_mode = itmMUser.getProperty("in_team_mode", "");
            model.in_team_v1 = itmMUser.getProperty("in_team_v1", "");
            model.in_team_v2 = itmMUser.getProperty("in_team_v2", "");

            if (in_team_mode == "2")
            {
                sql = "SELECT id FROM IN_MEETING_PTEAM WITH(NOLOCK) WHERE in_muser = '" + model.muid + "' AND in_sub_weight = '" + model.in_team_v2.Replace("kg", "") + "'";
                var itmOld = cfg.inn.applySQL(sql);
                if (!itmOld.isError() && itmOld.getResult() != "")
                {
                    canRemoveChild = true;
                }
                else
                {
                    canRemoveChild = false;
                }
            }

            if (model.need_remove == "1" && model.cid != "")
            {
                //移除子隊伍量級資料
                if (canRemoveChild) RemoveChildTeamByWeight(cfg, model);
            }
            else
            {
                sql = "SELECT * FROM IN_MEETING_PSECT WITH(NOLOCK)"
                    + " WHERE in_program = '" + cfg.program_id + "'"
                    + " AND in_sub_id = '" + model.sub + "'";

                Item itmSect = cfg.inn.applySQL(sql);
                if (itmSect.isError() || itmSect.getResult() == "")
                {
                    throw new Exception("查無量級資料");
                }

                //移除子隊伍量級資料
                if (canRemoveChild) RemoveChildTeam(cfg, model);
                //建立子隊伍量級資料
                MergeSubTeam(cfg, model, itmMUser, itmParent, itmSect);
                //自動匹配相同量級位置
                AutoMapSectWeight(cfg, model, itmMUser, itmParent);
                //重設子隊伍流水序號
                ResetInSubSerial(cfg, model);
            }
        }

        //移除子隊伍量級資料
        private void RemoveChildTeam(TConfig cfg, TEditModel model)
        {
            var sql = "DELETE FROM IN_MEETING_PTEAM WHERE in_muser = '" + model.muid + "'";
            cfg.inn.applySQL(sql);
        }

        //移除子隊伍量級資料
        private void RemoveChildTeamByWeight(TConfig cfg, TEditModel model)
        {
            string sql = "SELECT * FROM IN_MEETING_PTEAM WHERE id = '" + model.cid + "'";
            Item itmOld = cfg.inn.applySQL(sql);
            if (itmOld.isError() || itmOld.getResult() == "")
            {
                throw new Exception("更新發生錯誤");
            }

            string in_parent = itmOld.getProperty("in_parent", "");
            string in_muser = itmOld.getProperty("in_muser", "");
            string in_sub_weight = itmOld.getProperty("in_sub_weight", "");

            sql = "DELETE FROM IN_MEETING_PTEAM WHERE in_parent = '" + in_parent + "'"
                + " AND in_muser = '" + in_muser + "'"
                + " AND in_sub_weight = '" + in_sub_weight + "'"
                ;

            Item itmDelete = cfg.inn.applySQL(sql);

            if (itmDelete.isError())
            {
                throw new Exception("更新發生錯誤");
            }
        }

        //自動匹配
        private void AutoMapSectWeight(TConfig cfg, TEditModel src, Item itmMUser, Item itmParent)
        {
            var sects = GetSectList(cfg, cfg.program_id);

            foreach (var sect in sects)
            {
                if (sect.in_sub_id == src.sub)
                {
                    continue;
                }

                var code = src.in_team_v2.Replace("kg", "");
                var weight = sect.rangs.Find(x => x == code);
                if (!string.IsNullOrWhiteSpace(weight))
                {
                    var model = new TEditModel
                    {
                        muid = src.muid,
                        pid = src.pid,
                        cid = "",
                        sub = sect.in_sub_id,
                        need_remove = "",
                        in_team_v1 = src.in_team_v1,
                        in_team_v2 = src.in_team_v2,
                    };

                    MergeSubTeam(cfg, model, itmMUser, itmParent, sect.Value);
                }
            }
        }

        //建立子隊伍量級資料
        private void MergeSubTeam(TConfig cfg, TEditModel model, Item itmMUser, Item itmParent, Item itmSect)
        {
            string sect_name = itmSect.getProperty("in_name", "");

            Item itmPTeam = cfg.inn.newItem("In_Meeting_PTeam");

            itmPTeam.setAttribute("where", "in_parent = '" + model.pid + "'"
                + " AND in_sub_id = '" + model.sub + "'"
                + " AND in_muser = '" + model.muid + "'");

            itmPTeam.setProperty("in_type", "s");
            itmPTeam.setProperty("in_parent", model.pid);
            itmPTeam.setProperty("in_sub_sect", sect_name);
            itmPTeam.setProperty("in_sub_id", model.sub);
            //itmPTeam.setProperty("in_sub_serial", serial);
            itmPTeam.setProperty("in_muser", model.muid);


            itmPTeam.setProperty("in_meeting", cfg.meeting_id);
            itmPTeam.setProperty("source_id", itmParent.getProperty("source_id", ""));
            itmPTeam.setProperty("in_team_index", "");
            itmPTeam.setProperty("in_team_key", itmParent.getProperty("in_team_key", ""));

            itmPTeam.setProperty("in_index", itmParent.getProperty("in_index", ""));
            itmPTeam.setProperty("in_group", itmParent.getProperty("in_group", ""));
            itmPTeam.setProperty("in_current_org", itmParent.getProperty("in_current_org", ""));
            itmPTeam.setProperty("in_short_org", itmParent.getProperty("in_short_org", ""));
            itmPTeam.setProperty("in_stuff_b1", itmParent.getProperty("in_stuff_b1", ""));
            itmPTeam.setProperty("in_show_org", itmParent.getProperty("in_show_org", ""));

            itmPTeam.setProperty("in_team", itmParent.getProperty("in_team", ""));
            itmPTeam.setProperty("in_team_players", "");
            itmPTeam.setProperty("in_creator_sno", itmParent.getProperty("in_creator_sno", ""));
            itmPTeam.setProperty("in_city_no", itmParent.getProperty("in_city_no", ""));

            itmPTeam.setProperty("in_name", itmMUser.getProperty("in_name", ""));
            itmPTeam.setProperty("in_sno", itmMUser.getProperty("in_sno", ""));
            itmPTeam.setProperty("in_names", "");

            itmPTeam.setProperty("in_sub_word", model.in_team_v1.Replace("第", "").Replace("級", ""));
            itmPTeam.setProperty("in_sub_weight", model.in_team_v2.Replace("kg", ""));

            itmPTeam.setProperty("in_weight_value", itmMUser.getProperty("in_weight", ""));

            Item itmChild = itmPTeam.apply("merge");

            if (itmChild.isError())
            {
                throw new Exception("更新發生錯誤");
            }
        }

        //重設子隊伍流水序號
        private void ResetInSubSerial(TConfig cfg, TEditModel model)
        {
            string sql = @"
                UPDATE t1 SET
                	t1.in_sub_serial = t1.rno
                FROM
                (
                	SELECT 
                		id
                		, in_sub_sect
                		, in_sub_weight
                		, in_sub_serial
                		, ROW_NUMBER() OVER (PARTITION BY in_sub_id ORDER BY in_sub_weight) AS 'rno'  
                	FROM 
                		IN_MEETING_PTEAM WITH(NOLOCK)
                	WHERE 
                		in_parent = '{#pid}'
                		AND ISNULL(in_type, '') = 's'
                ) t1
            ";

            sql = sql.Replace("{#pid}", model.pid);

            Item itmUpdRNo = cfg.inn.applySQL(sql);

            if (itmUpdRNo.isError())
            {
                throw new Exception("更新發生錯誤");
            }
        }

        //註冊狀態頁面
        private void StatusPage(TConfig cfg, Item itmReturn)
        {
            string sql = "";

            sql = "SELECT id, in_title, in_url FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'";
            cfg.itmMeeting = cfg.inn.applySQL(sql);

            if (cfg.itmMeeting.isError() || cfg.itmMeeting.getResult() == "")
            {
                throw new Exception("賽程組別資料錯誤");
            }

            //活動資訊
            itmReturn.setProperty("in_title", cfg.itmMeeting.getProperty("in_title", ""));
            itmReturn.setProperty("in_url", cfg.itmMeeting.getProperty("in_url", ""));

            //登入者資訊
            itmReturn.setProperty("in_current_org", cfg.itmResume.getProperty("in_current_org", ""));

            AppendProgramMenu2(cfg, itmReturn);
            itmReturn.setProperty("program_id", cfg.program_id);

            if (cfg.isMeetingAdmin)
            {
                Item items = GetMUsersFromAdmin(cfg, cfg.program_id);

                List<TPgRow> programs = MapProgramRows(cfg, items);

                foreach (var program in programs)
                {
                    program.IsFinished = true;

                    foreach (var team in program.teams)
                    {
                        team.IsFinished = true;
                        if (program.IsSpecial)
                        {
                            continue;
                        }

                        if (team.playersW.Count > 0)
                        {
                            team.IsFinished = false;
                            program.IsFinished = false;
                        }
                    }
                }

                string contents = StatusBox(cfg, programs);

                itmReturn.setProperty("inn_tabs", contents);

            }
        }

        //附加組別選單
        private void AppendProgramMenu2(TConfig cfg, Item itmReturn)
        {
            var list = GetProgramDictionary(cfg);

            var builder = new StringBuilder();
            builder.Append("<select id='program_menu' class='form-control' style='width: 240px;' onchange='Program_Change(this)'>");
            foreach (var kv in list)
            {
                var day = kv.Key;
                builder.Append("<optgroup label='" + day + "'>");
                foreach (var item in kv.Value)
                {
                    string id = item.getProperty("id", "");
                    string in_name2 = item.getProperty("in_name2", "");
                    string in_team_count = item.getProperty("in_team_count", "0");
                    string text = in_name2 + " (" + in_team_count + ")";

                    builder.Append("<option data-day='" + day + "' value='" + id + "'>" + text + "</option>");
                }
                builder.Append("</optgroup>");
            }
            builder.Append("</select>");

            var contents = builder.ToString();

            itmReturn.setProperty("program_menu", contents);
        }

        //附加組別選單
        private Dictionary<string, List<Item>> GetProgramDictionary(TConfig cfg)
        {
            var list = new Dictionary<string, List<Item>>();

            string sql = @"
                SELECT 
	                id
                    , in_name2
                    , in_fight_day
                    , in_team_count
                FROM 
	                IN_MEETING_PROGRAM WITH(NOLOCK) 
                WHERE 
	                in_meeting = '{#meeting_id}'
	                AND in_l1 = N'團體組'
                ORDER BY 
                    in_fight_day
	                , in_sort_order
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);
            Item items = cfg.inn.applySQL(sql);
            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string key = item.getProperty("in_fight_day", "");
                if (!list.ContainsKey(key))
                {
                    list.Add(key, new List<Item>());
                }
                list[key].Add(item);
            }

            if (cfg.program_id == "" && count >= 1)
            {
                cfg.program_id = items.getItemByIndex(0).getProperty("id", "");
            }

            return list;
        }

        //表格箱
        private string StatusBox(TConfig cfg, List<TPgRow> programs)
        {
            StringBuilder builder = new StringBuilder();

            builder.Append("<div id='data_box' class='showsheet_ clearfix'>");
            builder.Append("    <div>");

            builder.Append("<div class='float-btn clearfix'>");
            builder.Append("</div>");
            AppendStatusTable(cfg, programs, builder);
            builder.Append("    </div>");

            builder.Append("</div>");

            return builder.ToString();
        }

        private void AppendStatusTable(TConfig cfg, List<TPgRow> src, StringBuilder builder)
        {
            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            head.Append("<tr>");
            head.Append("  <th class='text-center'>級組</th>");
            head.Append("  <th class='text-center'>單位隊伍</th>");
            head.Append("  <th class='text-center'>選手人數</th>");
            head.Append("  <th class='text-center'>未註冊</th>");
            head.Append("  <th class='text-center'>已註冊</th>");
            head.Append("  <th class='text-center'>狀態</th>");
            head.Append("  <th class='text-center'>協助報名者</th>");
            head.Append("  <th class='text-center'>聯絡電話</th>");
            head.Append("</tr>");

            var need_filter = cfg.unfinished == "1";
            var programs = src;
            if (need_filter)
            {
                programs = src.FindAll(x => !x.IsFinished);
            }
            if (programs == null)
            {
                programs = new List<TPgRow>();
            }

            int count = programs.Count;
            for (int i = 0; i < programs.Count; i++)
            {
                var program = programs[i];
                var itmProgram = program.Value;


                var teams = program.teams;
                if (need_filter)
                {
                    teams = program.teams.FindAll(x => !x.IsFinished);
                }
                if (teams == null)
                {
                    teams = new List<TTmRow>();
                }

                var end_idx = teams.Count - 1;
                for (int j = 0; j < teams.Count; j++)
                {
                    var team = teams[j];

                    if (j == end_idx)
                    {
                        body.Append("<tr class='weight_row'>");
                    }
                    else
                    {
                        body.Append("<tr>");
                    }

                    if (j == 0)
                    {
                        body.Append("<td class='text-left col-md-3'>" + ProgramLink(cfg, program) + "</td>");
                    }
                    else
                    {
                        body.Append("<td class='text-left col-md-3'>" + "&nbsp;" + "</td>");
                    }

                    body.Append("<td class='text-left'>" + TeamLink(cfg, program, team) + "</td>");
                    body.Append("<td class='text-left'>" + team.players.Count + "</td>");
                    body.Append("<td class='text-left'>" + team.playersW.Count + "</td>");
                    body.Append("<td class='text-left'>" + team.playersF.Count + "</td>");
                    body.Append("<td class='text-left'>" + StatusBadge(cfg, program, team) + "</td>");
                    body.Append("<td class='text-left'>" + team.Value.getProperty("in_creator", "") + "</td>");
                    body.Append("<td class='text-left'>" + team.Value.getProperty("in_creator_tel", "") + "</td>");

                    body.Append("</tr>");
                }
            }

            builder.Append("<table"
                + " class='table table-hover table-bordered table-rwd rwd'"
                + " style='background-color: #fff;' "
                + " data-toggle='table' "
                + " data-search='false'"
                + " data-search-align='left'"
                + ">");

            builder.Append("<thead>");
            builder.Append(head);
            builder.Append("</thead>");
            builder.Append("<tbody>");
            builder.Append(body);
            builder.Append("</tbody>");
            builder.Append("</table>");
        }

        private string ProgramLink(TConfig cfg, TPgRow program)
        {
            var pg_name2 = program.Value.getProperty("pg_name2", "");

            var url = "c.aspx?page=in_team_battle_register_org.html"
                + "&method=in_team_battle_register_org"
                + "&meeting_id=" + cfg.meeting_id
                + "&program_id=" + program.pg_id
                ;

            return "<a target='_blank' href='" + url + "'>" + pg_name2 + "</a>";
        }

        private string TeamLink(TConfig cfg, TPgRow program, TTmRow team)
        {
            var itmTeam = team.Value;

            var in_short_org = itmTeam.getProperty("in_short_org", "");
            var in_team = itmTeam.getProperty("in_team", "");
            var in_index = itmTeam.getProperty("in_index", "");

            var suffix = in_team == "" ? "" : " " + in_team + " 隊";
            var org = in_short_org + suffix;

            var url = "c.aspx?page=in_team_battle_register_org.html"
                + "&method=in_team_battle_register_org"
                + "&meeting_id=" + cfg.meeting_id
                + "&program_id=" + program.pg_id
                + "&in_index=" + in_index
                ;

            return "<a target='_blank' href='" + url + "'>" + org + "</a>";
        }

        private string StatusBadge(TConfig cfg, TPgRow program, TTmRow team)
        {
            var status = "<small class='label bg-red'>未填寫</small>";
            if (team.playersF.Count == team.players.Count)
            {
                status = "<small class='label bg-green'>完　成</small>";
            }
            else if (team.playersF.Count > 0)
            {
                status = "<small class='label bg-blue'>未完成</small>";
            }

            if (program.IsSpecial)
            {
                status = "不需填寫";
            }

            return status;
        }

        private List<TPgRow> MapProgramRows(TConfig cfg, Item items)
        {
            List<TPgRow> rows = new List<TPgRow>();

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string pg_id = item.getProperty("pg_id", "");
                string in_l1 = item.getProperty("in_l1", "");
                string in_l2 = item.getProperty("in_l2", "");
                string in_l3 = item.getProperty("in_l3", "");
                string in_index = item.getProperty("in_index", "");
                string in_creator_sno = item.getProperty("in_creator_sno", "");
                string in_team_weight = item.getProperty("in_team_weight", "");

                string team_key = string.Join("-", new List<string>
                {
                    in_l1,
                    in_l2,
                    in_l3,
                    in_index,
                    in_creator_sno,
                });

                var pg = rows.Find(x => x.pg_id == pg_id);
                if (pg == null)
                {
                    pg = new TPgRow
                    {
                        pg_id = pg_id,
                        teams = new List<TTmRow>(),
                        Value = item,
                        IsSpecial = false,
                        IsFinished = false,
                    };
                    if (in_l2.Contains("特別"))
                    {
                        pg.IsSpecial = true;
                    }
                    rows.Add(pg);
                }

                var team = pg.teams.Find(x => x.team_key == team_key);
                if (team == null)
                {
                    team = new TTmRow
                    {
                        team_key = team_key,
                        players = new List<Item>(),
                        playersW = new List<Item>(),
                        playersF = new List<Item>(),
                        Value = item,
                    };
                    pg.teams.Add(team);
                }

                team.players.Add(item);

                if (in_team_weight == "")
                {
                    team.playersW.Add(item);
                }
                else
                {
                    team.playersF.Add(item);
                }
            }

            return rows;
        }


        private class TPgRow
        {
            public string pg_id { get; set; }
            public Item Value { get; set; }
            public List<TTmRow> teams { get; set; }
            public bool IsFinished { get; set; }
            public bool IsSpecial { get; set; }
        }

        private class TTmRow
        {
            public string team_key { get; set; }
            public Item Value { get; set; }
            public bool IsFinished { get; set; }

            /// <summary>
            /// 全部選手
            /// </summary>
            public List<Item> players { get; set; }

            /// <summary>
            /// 未設定團體賽量級的選手
            /// </summary>
            public List<Item> playersW { get; set; }

            /// <summary>
            /// 已設定團體賽量級的選手
            /// </summary>
            public List<Item> playersF { get; set; }
        }

        //頁面
        private void OrgPage(TConfig cfg, Item itmReturn, bool show_org_team_menu = true)
        {
            string sql = "";

            sql = "SELECT id, in_title, in_url FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'";
            cfg.itmMeeting = cfg.inn.applySQL(sql);

            if (cfg.itmMeeting.isError() || cfg.itmMeeting.getResult() == "")
            {
                throw new Exception("賽程組別資料錯誤");
            }

            //活動資訊
            itmReturn.setProperty("in_title", cfg.itmMeeting.getProperty("in_title", ""));
            itmReturn.setProperty("in_url", cfg.itmMeeting.getProperty("in_url", ""));

            //登入者資訊
            itmReturn.setProperty("in_current_org", cfg.itmResume.getProperty("in_current_org", ""));

            //競賽組別選單
            AppendProgramMenu(cfg, itmReturn);

            //隊伍選單
            AppendTeamMenu(cfg, itmReturn);
        }

        private void WeightTables(TConfig cfg, Item itmReturn)
        {
            if (cfg.in_index == "")
            {
                itmReturn.setProperty("inn_tabs", "");
                return;
            }

            var itmProgram = cfg.inn.applySQL("SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + cfg.program_id + "'");

            //與會者資料
            var itmMUsers = GetMUsers(cfg, itmProgram);

            //與會者隊伍
            var teams = MapTeams(cfg, itmProgram, itmMUsers);

            //團體量級
            var sects = GetSectList(cfg, cfg.program_id);

            //表格 HTML
            var contents = WeightBox(cfg, teams, sects, itmReturn);

            itmReturn.setProperty("inn_tabs", contents);
        }

        //表格箱
        private string WeightBox(TConfig cfg, List<TTeam> teams, List<TSect> sects, Item itmReturn)
        {
            StringBuilder builder = new StringBuilder();

            builder.Append("<div id='data_box' class='showsheet_ clearfix'>");
            builder.Append("    <div>");

            foreach (var team in teams)
            {
                builder.Append("<div class='float-btn clearfix'>");
                builder.Append("  <h3>" + team.name + "</h3>");
                builder.Append("</div>");
                AppendWeightTable(cfg, team, team.musers, sects, builder);
            }

            builder.Append("    </div>");

            builder.Append("</div>");

            return builder.ToString();
        }

        private void AppendWeightTable(TConfig cfg, TTeam team, List<TMUser> rows, List<TSect> sects, StringBuilder builder)
        {
            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            head.Append("<tr>");
            head.Append("  <th class='text-center'>選手</th>");
            head.Append("  <th class='text-center'>實際體重</th>");
            head.Append("  <th class='text-center'>功能</th>");
            head.Append("  <th class='text-center'>過磅狀態</th>");
            head.Append("  <th class='text-center'>實際/註冊</th>");
            head.Append("  <th class='text-center'>位置</th>");
            head.Append("  <th class='text-center'>級組</th>");
            head.Append("  <th class='text-center'>參賽量級</th>");
            head.Append("  <th class='text-center'>備註</th>");
            head.Append("  <th class='text-center'>雙級</th>");
            head.Append("</tr>");


            int count = rows.Count;
            for (int i = 0; i < count; i++)
            {
                var own_sects = sects;

                var row = rows[i];
                if (team.name.Contains("混合"))
                {
                    if (row.in_gender == "女")
                    {
                        own_sects = sects.FindAll(x => x.in_gender == "W");
                    }
                    else
                    {
                        own_sects = sects.FindAll(x => x.in_gender == "M");
                    }
                }

                if (own_sects == null || own_sects.Count <= 0)
                {
                    continue;
                }

                var input = "<input id='{#id}' data-value='' style='width:100%' class='eventKD' type='number' value='{#value}'>";
                var button = "<button class='btn btn-sm btn-info' onclick='SaveWeight(this)'>儲存</button>";
                var select = "<select class='form-control btn btn-default state-select' onchange='PlayerState_Change(this)' data-val='" + MapStatus(cfg, row) + "'>"
                    + "  <option value='請選擇'>--</option>"
                    + "  <option value='(請假)'>請假</option> "
                    + "  <option value='(DNS)'>DNS</option> "
                    + "  <option value='(DQ)'>DQ</option>"
                    + "  <option value='ok'>合格</option>"
                    + "  <option value='(停賽)'>停賽</option>"
                    + "</select>";

                var sect_end_idx = own_sects.Count - 1;
                for (int j = 0; j < own_sects.Count; j++)
                {
                    var sect = own_sects[j];
                    if (j == sect_end_idx)
                    {
                        body.Append("<tr class='weight_row' data-pid='" + team.team_id + "' data-muid='" + row.muid + "'>");
                    }
                    else
                    {
                        body.Append("<tr data-pid='" + team.team_id + "' data-muid='" + row.muid + "'>");
                    }

                    if (j == 0)
                    {
                        var myName = "<label style='color: blue; font-size: 1.2em'> " + row.in_name + " </label>";
                        var myAge = "";// row.reg_birth + "=" + row.in_age;
                        var myDisplay = myName + " " + myAge;

                        var myInput = input.Replace("{#id}", "txt_" + row.muid)
                            .Replace("{#value}", row.in_weight);

                        body.Append("<td class='text-left col-md-2'>" + myDisplay + "</td>");
                        body.Append("<td class='text-left col-md-1'>" + myInput + "</td>");
                        body.Append("<td class='text-left'>" + button + "</td>");
                        body.Append("<td class='text-left col-md-1'>" + select + "</td>");
                        body.Append("<td class='text-left team-weight-sys'>" + MatchWeight(cfg, team, row) + "</td>");
                    }
                    else
                    {
                        if (j == 1)
                        {
                            string org = row.in_stuff_b1 == "" ? "" : row.in_stuff_b1 + " ";
                            org += row.in_short_org;
                            if (row.in_team != "") org += " " + row.in_team;
                            if (row.in_show_org != "")
                            {
                                org += "<BR>" + row.in_show_org;
                            }
                            body.Append("<td class='text-left'>" + org + "</td>");
                        }
                        else if (j == 2)
                        {
                            if (row.in_team_sect != "")
                            {
                                body.Append("<td class='text-left'>" + row.in_team_sect
                                    + " " + row.in_team_n1
                                    + " " + row.in_team_n2 + "</td>");
                            }
                            else
                            {
                                body.Append("<td class='text-left'>&nbsp;</td>");
                            }
                        }
                        else
                        {
                            body.Append("<td class='text-left'>&nbsp;</td>");
                        }
                        body.Append("<td class='text-left'>&nbsp;</td>");
                        body.Append("<td class='text-left'>&nbsp;</td>");
                        body.Append("<td class='text-left'>&nbsp;</td>");
                        body.Append("<td class='text-left'>&nbsp;</td>");
                    }

                    body.Append("<td class='text-left'>" + sect.in_name + "</td>");
                    body.Append("<td class='text-left'>" + sect.in_weight.Replace("Kg", "").Replace("kg", "") + "</td>");
                    body.Append("<td class='text-left'>" + MapSectOptions(cfg, team, row, sect) + "</td>");
                    body.Append("<td class='text-left'>" + sect.in_note + "</td>");

                    if (j == 0)
                    {
                        body.Append("<td class='text-left'>" + TeamModeCtrl(cfg, row) + "</td>");
                    }
                    else
                    {
                        body.Append("<td class='text-left'>&nbsp;</td>");
                    }
                    body.Append("</tr>");
                }
            }

            builder.Append("<table"
                + " class='table table-hover table-bordered table-rwd rwd'"
                + " style='background-color: #fff;' "
                + " data-toggle='table' "
                + " data-search='false'"
                + " data-search-align='left'"
                + ">");

            builder.Append("<thead>");
            builder.Append(head);
            builder.Append("</thead>");
            builder.Append("<tbody>");
            builder.Append(body);
            builder.Append("</tbody>");
            builder.Append("</table>");
        }

        private string TeamModeCtrl(TConfig cfg, TMUser row)
        {
            var opts = "";
            switch (row.in_team_mode)
            {
                case "2": opts = "<option value=''>否</option><option value='2' selected>是</option>"; break;
                default: opts = "<option value=''>否</option><option value='2'>是</option>"; break;
            }
            return "<select class='form-control' onchange='MUserTeamModeChange(this)' data-muid='" + row.muid + "'>" + opts + "</select>";
        }

        private string MapStatus(TConfig cfg, TMUser row)
        {
            var hasWeight = row.in_weight != "" && row.in_weight != "0";
            switch (row.in_weight_status)
            {
                case "on": return "ok";
                case "off": return "(DNS)";
                //case "off": return "(未到)";
                case "leave": return "(請假)";
                case "dq": return "(DQ)";
                case "stop": return "(停賽)";
                default: return hasWeight ? "ok" : "請選擇";
            }
        }

        private string MapStatus2(TConfig cfg, string value)
        {
            switch (value)
            {
                case "": return "";
                case "(未到)": return "off";
                case "(DNS)": return "off";
                case "(請假)": return "leave";
                case "(DQ)": return "dq";
                case "(停賽)": return "stop";
                default: return "";
            }
        }

        private void OrgTables(TConfig cfg, Item itmReturn)
        {
            var itmProgram = cfg.inn.applySQL("SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + cfg.program_id + "'");

            //與會者資料
            var itmMUsers = GetMUsers(cfg, itmProgram);

            //與會者隊伍
            var teams = MapTeams(cfg, itmProgram, itmMUsers);

            //團體量級
            var sects = GetSectList(cfg, cfg.program_id);

            //表格 HTML
            var contents = TableBox(cfg, teams, sects, itmReturn);

            itmReturn.setProperty("inn_tabs", contents);

        }

        //表格箱
        private string TableBox(TConfig cfg, List<TTeam> teams, List<TSect> sects, Item itmReturn)
        {
            StringBuilder builder = new StringBuilder();

            builder.Append("<div id='data_box' class='showsheet_ clearfix'>");
            builder.Append("    <div>");

            foreach (var team in teams)
            {
                builder.Append("<div class='float-btn clearfix'>");
                builder.Append("  <h3>" + team.name + "</h3>");
                builder.Append("</div>");
                AppendTable(cfg, team, team.musers, sects, builder);
            }

            builder.Append("    </div>");

            builder.Append("</div>");

            return builder.ToString();
        }

        private void AppendTable(TConfig cfg, TTeam team, List<TMUser> rows, List<TSect> sects, StringBuilder builder)
        {
            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            head.Append("<tr>");
            head.Append("  <th class='text-center'>選手</th>");
            head.Append("  <th class='text-center'>位置</th>");
            head.Append("  <th class='text-center'>級組</th>");
            head.Append("  <th class='text-center'>參賽量級</th>");
            head.Append("  <th class='text-center'>備註</th>");
            head.Append("</tr>");


            int count = rows.Count;
            for (int i = 0; i < count; i++)
            {
                var own_sects = sects;

                var row = rows[i];
                if (team.name.Contains("混合"))
                {
                    if (row.in_gender == "女")
                    {
                        own_sects = sects.FindAll(x => x.in_gender == "W");
                    }
                    else
                    {
                        own_sects = sects.FindAll(x => x.in_gender == "M");
                    }
                }

                if (own_sects == null || own_sects.Count <= 0)
                {
                    continue;
                }

                var sect_end_idx = own_sects.Count - 1;
                for (int j = 0; j < own_sects.Count; j++)
                {
                    var sect = own_sects[j];
                    if (j == sect_end_idx)
                    {
                        body.Append("<tr class='weight_row'>");
                    }
                    else
                    {
                        body.Append("<tr>");
                    }

                    if (j == 0)
                    {
                        body.Append("<td class='text-left col-md-2'>" + row.in_name + "</td>");
                    }
                    else
                    {
                        body.Append("<td class='text-left'>&nbsp;</td>");
                    }

                    body.Append("<td class='text-left'>" + sect.in_name + "</td>");
                    body.Append("<td class='text-left'>" + sect.in_weight + "</td>");
                    body.Append("<td class='text-left'>" + MapSectOptions(cfg, team, row, sect) + "</td>");
                    body.Append("<td class='text-left'>" + sect.in_note + "</td>");
                    body.Append("</tr>");
                }
            }

            builder.Append("<table"
                + " class='table table-hover table-bordered table-rwd rwd'"
                + " style='background-color: #fff;' "
                + " data-toggle='table' "
                + " data-search='false'"
                + " data-search-align='left'"
                + ">");

            builder.Append("<thead>");
            builder.Append(head);
            builder.Append("</thead>");
            builder.Append("<tbody>");
            builder.Append(body);
            builder.Append("</tbody>");
            builder.Append("</table>");
        }

        private string MatchWeight(TConfig cfg, TTeam team, TMUser muser)
        {
            if (muser.in_team_r1 == "") return "";
            if (muser.in_team_n1 == "") return muser.in_team_r1 + " " + muser.in_team_r2 + "<br />未註冊";

            var w1 = muser.in_team_r1 + " " + muser.in_team_r2;
            var w2 = muser.in_team_n1 + " " + muser.in_team_n2;
            if (w1 == w2) return w1 + "<br />" + "實際&註冊";

            var w3 = w1 + "(實際)" + "<br />" + w2 + "(註冊)";
            w3 = w3.Replace("Kg", "").Replace("kg", "");
            return w3;
        }

        private string MapSectOptions(TConfig cfg, TTeam team, TMUser muser, TSect sect)
        {
            var sb = new StringBuilder();

            //個人在該隊伍出賽的量級(EX: 先鋒、中鋒)
            var child_sects = team.Children.FindAll(x => x.muid == muser.muid);

            foreach (var r in sect.rangs)
            {
                var cid = "";
                var css = "span-btn";

                if (child_sects != null)
                {
                    var weight = child_sects.Find(x => x.in_sub_weight == r);
                    if (weight != null)
                    {
                        cid = weight.team_id;
                        css = "span-btn span-btn-selected";
                    }
                }

                sb.Append("<span class='" + css + "'"
                    + " onclick='SetWeight(this)'"
                    + " style='margin-right: 5px'"
                    + " data-pid='" + team.team_id + "'"
                    + " data-cid='" + cid + "'"
                    + " data-muid='" + muser.muid + "'"
                    + " data-name='" + sect.in_name + "'"
                    + " data-sub='" + sect.in_sub_id + "'"
                    + " data-weight='" + r + "'"
                    + ">"
                    + r
                    + "</span>");
            }

            return sb.ToString();
        }

        private List<TTeam> MapTeams(TConfig cfg, Item itmProgram, Item items)
        {
            var list = new List<TTeam>();

            int count = items.getItemCount();
            if (count <= 0) return list;

            string in_l1 = itmProgram.getProperty("in_l1", "");
            string in_l2 = itmProgram.getProperty("in_l2", "");
            string in_l3 = itmProgram.getProperty("in_l3", "");
            string in_name2 = itmProgram.getProperty("in_name2", "");
            string in_name3 = itmProgram.getProperty("in_name3", "");

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);

                var muser = new TMUser
                {
                    muid = item.getProperty("muid", ""),
                    in_name = item.getProperty("in_name", ""),
                    in_sno = item.getProperty("in_sno", ""),
                    in_gender = item.getProperty("in_gender", ""),
                    reg_birth = item.getProperty("reg_birth", ""),
                    in_age = item.getProperty("in_age", ""),
                    in_current_org = item.getProperty("in_current_org", ""),
                    in_short_org = item.getProperty("in_short_org", ""),
                    in_show_org = item.getProperty("in_show_org", ""),
                    in_stuff_b1 = item.getProperty("in_stuff_b1", ""),
                    in_team = item.getProperty("in_team", ""),
                    in_index = item.getProperty("in_index", ""),
                    in_weight = item.getProperty("in_weight", ""),
                    in_weight_status = item.getProperty("in_weight_status", ""),
                    in_team_mode = item.getProperty("in_team_mode", ""),
                    in_team_weight = item.getProperty("in_team_weight", ""),
                    in_team_sect = item.getProperty("in_team_sect", ""),
                    in_team_r1 = item.getProperty("in_team_r1", ""),
                    in_team_r2 = item.getProperty("in_team_r2", ""),
                    in_team_n1 = item.getProperty("in_team_n1", ""),
                    in_team_n2 = item.getProperty("in_team_n2", ""),
                    in_team_v1 = item.getProperty("in_team_v1", ""),
                    in_team_v2 = item.getProperty("in_team_v2", ""),
                    display = "",
                    Value = item,
                };

                string in_index = item.getProperty("in_index", "");
                string in_team = item.getProperty("in_team", "");
                string team_key = in_l1 + "-" + in_l2 + "-" + in_l3 + "-" + in_index;
                string suffix = in_team == "" ? "" : " " + in_team + " 隊";

                var row = list.Find(x => x.key == team_key);
                if (row == null)
                {
                    row = new TTeam
                    {
                        key = team_key,
                        name = in_name2 + suffix,
                        musers = new List<TMUser>(),
                        team_id = GetTeamId(cfg, in_index),
                    };

                    if (cfg.isMeetingAdmin)
                    {
                        string no = (list.Count + 1).ToString();
                        string org = muser.in_show_org == "" ? muser.in_short_org : muser.in_show_org;
                        row.name = in_name2
                            + " (" + no + ")"
                            + " <span style='color: blue'>" + org + suffix + "</span>";
                    }

                    row.Children = GetTeamChildren(cfg, row);

                    list.Add(row);
                }
                row.musers.Add(muser);
            }
            return list;
        }

        private string GetTeamId(TConfig cfg, string in_index)
        {
            string cond = cfg.isMeetingAdmin
                ? ""
                : "AND in_creator_sno = '" + cfg.in_creator_sno + "' ";

            string sql = @"
                SELECT 
	                id 
                FROM 
	                IN_MEETING_PTEAM WITH(NOLOCK) 
                WHERE 
	                source_id = '{#program_id}' 
	                AND in_index = '{#in_index}' 
	                AND ISNULL(in_type, '') <> 's'
	                {#cond}
            ";

            sql = sql.Replace("{#program_id}", cfg.program_id)
                .Replace("{#in_index}", in_index)
                .Replace("{#cond}", cond);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            var item = cfg.inn.applySQL(sql);

            if (item.isError() || item.getResult() == "")
            {
                return "";
            }
            else
            {
                return item.getProperty("id", "");
            }
        }

        private List<TChild> GetTeamChildren(TConfig cfg, TTeam team)
        {
            var result = new List<TChild>();

            string sql = @"
                SELECT 
	                *
                FROM 
	                IN_MEETING_PTEAM WITH(NOLOCK) 
                WHERE 
	                in_parent = '{#team_id}' 
	                AND ISNULL(in_type, '') = 's'
            ";

            sql = sql.Replace("{#team_id}", team.team_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                result.Add(new TChild
                {
                    team_id = item.getProperty("id", ""),
                    muid = item.getProperty("in_muser", ""),
                    in_sub_weight = item.getProperty("in_sub_weight", ""),
                    Value = item,
                });
            }

            return result;
        }

        //附加組別選單
        private void AppendProgramMenu(TConfig cfg, Item itmReturn)
        {
            if (cfg.isMeetingAdmin)
            {
                AppendProgramMenuAdmin(cfg, itmReturn);

            }
            else
            {
                AppendProgramMenuOrg(cfg, itmReturn);
            }
        }

        //附加組別選單
        private void AppendProgramMenuAdmin(TConfig cfg, Item itmReturn)
        {
            string sql = @"
                SELECT
            	    *
                FROM
            	    IN_MEETING_PROGRAM WITH(NOLOCK)
                WHERE
            	    in_meeting = '{#meeting_id}'
            	    AND in_l1 = N'團體組'
            	    AND ISNULL(in_team_count, 0) > 0
                ORDER BY
            	    in_sort_order
            	    , in_sub_event
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_creator_sno}", cfg.in_creator_sno);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item items = cfg.inn.applySQL(sql);

            string in_type = "inn_program";

            AppendProgramMenu(cfg, items, in_type, itmReturn);

            int count = items.getItemCount();
            if (count > 0 && cfg.program_id == "")
            {
                cfg.program_id = items.getItemByIndex(0).getProperty("id", "");
                itmReturn.setProperty("program_id", cfg.program_id);
            }
        }

        //附加組別選單
        private void AppendProgramMenuOrg(TConfig cfg, Item itmReturn)
        {
            string sql = @"
                SELECT
            	    *
                FROM
            	    IN_MEETING_PROGRAM WITH(NOLOCK)
                WHERE
            	    in_meeting = '{#meeting_id}'
            	    AND in_l1 = N'團體組'
            	    AND in_name IN 
                    (
            		    SELECT DISTINCT 
            			    ISNULL(in_l1, '')
            			    + '-' + ISNULL(in_l2, '')
            			    + '-' + ISNULL(in_l3, '')
            		    FROM
            			    IN_MEETING_USER WITH(NOLOCK)
            		    WHERE 
            			    source_id = '{#meeting_id}'
            			    AND in_creator_sno = '{#in_creator_sno}'
            			    AND in_l1 = N'團體組'
            	    )
                ORDER BY
            	    in_sort_order
            	    , in_sub_event
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_creator_sno}", cfg.in_creator_sno);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item items = cfg.inn.applySQL(sql);

            string in_type = "inn_program";

            AppendProgramMenu(cfg, items, in_type, itmReturn);

            int count = items.getItemCount();
            if (count > 0 && cfg.program_id == "")
            {
                cfg.program_id = items.getItemByIndex(0).getProperty("id", "");
                itmReturn.setProperty("program_id", cfg.program_id);
            }
        }

        private void AppendProgramMenu(TConfig cfg, Item items, string in_type, Item itmReturn)
        {
            Item itmEmpty = cfg.inn.newItem(in_type);
            itmEmpty.setProperty("value", "");
            itmEmpty.setProperty("text", "請選擇");
            itmReturn.addRelationship(itmEmpty);

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string id = item.getProperty("id", "");
                string in_name2 = item.getProperty("in_name2", "");
                string in_team_count = item.getProperty("in_team_count", "0");

                item.setType(in_type);
                item.setProperty("value", id);
                if (cfg.isMeetingAdmin)
                {
                    item.setProperty("text", in_name2 + " (" + in_team_count + ")");
                }
                else
                {
                    item.setProperty("text", in_name2);
                }
                itmReturn.addRelationship(item);

            }
        }

        //附加組別選單
        private void AppendTeamMenu(TConfig cfg, Item itmReturn)
        {
            itmReturn.setProperty("hide_team_menu", "item_show_0");

            string cond = cfg.isMeetingAdmin
                ? ""
                : "AND in_creator_sno = '" + cfg.in_creator_sno + "'";

            string sql = @"
                SELECT DISTINCT
                	in_city_no
                	, in_stuff_b1
                	, in_short_org
                	, in_show_org
                	, in_team
                	, in_index 
                FROM 
                	IN_MEETING_PTEAM WITH(NOLOCK) 
                WHERE 
                	source_id = '{#program_id}'
                	AND ISNULL(in_type, '') IN ('', 'p')
                    {#cond}
                ORDER BY
                    in_city_no
                    , in_stuff_b1
                	, in_short_org
                	, in_show_org
                	, in_team
                	, in_index
            ";

            sql = sql.Replace("{#program_id}", cfg.program_id)
                .Replace("{#cond}", cond);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            Item items = cfg.inn.applySQL(sql);

            string in_type = "inn_team";

            Item itmEmpty = cfg.inn.newItem(in_type);
            itmEmpty.setProperty("value", "");
            itmEmpty.setProperty("text", "請選擇");
            itmReturn.addRelationship(itmEmpty);

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_stuff_b1 = item.getProperty("in_stuff_b1", "");
                string in_short_org = item.getProperty("in_short_org", "");
                string in_show_org = item.getProperty("in_show_org", "");
                string in_team = item.getProperty("in_team", "");
                string in_index = item.getProperty("in_index", "");

                if (in_stuff_b1 != "") in_stuff_b1 += " ";

                string value = in_index;

                string label = in_show_org == ""
                    ? in_stuff_b1 + in_short_org
                    : in_show_org + " / " + in_stuff_b1 + in_short_org;

                if (in_team != "") label += " " + in_team + " 隊";

                item.setType(in_type);
                item.setProperty("value", value);
                item.setProperty("text", label);
                itmReturn.addRelationship(item);
            }

            if (count > 1)
            {
                itmReturn.setProperty("hide_team_menu", "item_show_1");
            }
        }

        private Item GetMUsers(TConfig cfg, Item itmProgram)
        {
            var conds = new List<string>();
            if (!cfg.isMeetingAdmin)
            {
                conds.Add("AND t1.in_creator_sno = '" + cfg.in_creator_sno + "'");
            }

            if (cfg.in_index != "")
            {
                conds.Add("AND t1.in_index = '" + cfg.in_index + "'");
            }

            string cond = string.Join(" ", conds);

            string sql = @"
                SELECT
	                t1.id					AS 'muid'
	                , t1.in_name
	                , t1.in_sno
	                , t1.in_gender
	                , t1.in_l1
	                , t1.in_l2
	                , t1.in_l3
	                , t1.in_team
	                , t1.in_index
	                , t1.in_short_org
	                , t1.in_current_org
	                , t1.in_show_org
	                , t1.in_stuff_b1
	                , t1.in_weight
	                , t1.in_weight_status
	                , t1.in_team_weight
	                , t1.in_team_sect
	                , t1.in_team_mode
	                , t1.in_team_r1
	                , t1.in_team_r2
	                , t1.in_team_n1
	                , t1.in_team_n2
	                , t1.in_team_v1
	                , t1.in_team_v2
	                , CONVERT(VARCHAR, DATEADD(HOUR, 8, t1.in_birth), 111) AS 'reg_birth'
	                , t1.in_age
                FROM
	                IN_MEETING_USER t1 WITH(NOLOCK)
                LEFT OUTER JOIN
	                IN_ORG_MAP t2 WITH(NOLOCK)
	                ON t2.in_stuff_b1 = t1.in_stuff_b1
                WHERE
	                t1.source_id = '{#meeting_id}'
	                AND t1.in_l1 = N'{#in_l1}'
	                AND t1.in_l2 = N'{#in_l2}'
	                AND t1.in_l3 = N'{#in_l3}'
	                {#conds}
                ORDER BY
	                t1.in_stuff_b1
	                , t1.in_team
	                , t1.in_index
	                , t1.in_gender
	                , t1.in_sno
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_l1}", itmProgram.getProperty("in_l1", ""))
                .Replace("{#in_l2}", itmProgram.getProperty("in_l2", ""))
                .Replace("{#in_l3}", itmProgram.getProperty("in_l3", ""))
                .Replace("{#conds}", cond);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }


        private Item GetMUsersFromAdmin(TConfig cfg, string program_id)
        {
            string program_cond = program_id == ""
                ? ""
                : "AND t11.id = '" + program_id + "'";

            string sql = @"
                SELECT
	                t1.id					AS 'muid'
	                , t1.in_name
	                , t1.in_sno
	                , t1.in_gender
	                , t1.in_l1
	                , t1.in_l2
	                , t1.in_l3
	                , t1.in_team
	                , t1.in_index
	                , t1.in_short_org
	                , t1.in_current_org
	                , t1.in_stuff_b1 
	                , t1.in_creator
	                , t1.in_creator_sno
	                , t1.in_weight
	                , t1.in_weight_status
	                , t1.in_team_mode
	                , t1.in_team_weight
	                , t1.in_team_sect
	                , t1.in_team_r1
	                , t1.in_team_r2
	                , t1.in_team_n1
	                , t1.in_team_n2
	                , t1.in_team_v1
	                , t1.in_team_v2
	                , t11.id             AS 'pg_id'
	                , t11.in_name        AS 'pg_name1'
	                , t11.in_name2       AS 'pg_name2'
	                , t11.in_name2       AS 'pg_name3'
	                , t11.in_short_name  AS 'pg_short'
					, t21.in_tel         AS 'in_creator_tel'
                FROM
	                IN_MEETING_USER t1 WITH(NOLOCK)
                LEFT OUTER JOIN
	                IN_ORG_MAP t2 WITH(NOLOCK)
	                ON t2.in_stuff_b1 = t1.in_stuff_b1
                INNER JOIN
                    IN_MEETING_PROGRAM t11 WITH(NOLOCK)
                    ON t11.in_meeting = t1.source_id
                    AND t11.in_l1 = t1.in_l1
                    AND t11.in_l2 = t1.in_l2
                    AND t11.in_l3 = t1.in_l3
                INNER JOIN
                    IN_RESUME t21 WITH(NOLOCK)
                    ON t21.in_sno = t1.in_creator_sno
                WHERE
	                t11.in_meeting = '{#meeting_id}'
                    AND t11.in_l1 = N'團體組'
	                AND ISNULL(t11.in_team_count, 0) > 1
                    {#program_cond}
                ORDER BY
	                t11.in_sort_order
                    , t11.in_sub_event
					, t1.in_stuff_b1
					, t1.in_team
					, t1.in_index
					, t1.in_gender
					, t1.in_sno
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#program_cond}", program_cond);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        //取得隊伍資料
        private Item GetMTeams(TConfig cfg, string team_id)
        {
            string program_id = cfg.program_id;

            string sql = @"
                SELECT 
                    * 
                FROM 
                    IN_MEETING_PTEAM WITH(NOLOCK)
                WHERE
                    source_id = '{#program_id}'
                    AND in_creator_sno = '{#in_creator_sno}'
                ORDER BY 
                    in_stuff_b1
                    , in_team
                    , in_type
                    , in_sub_id
                    , in_sub_serial
            ";

            if (cfg.isMeetingAdmin)
            {
                sql = @"
                    SELECT 
                        * 
                    FROM 
                        IN_MEETING_PTEAM WITH(NOLOCK)
                    WHERE
                        source_id = '{#program_id}'
                    ORDER BY 
                        in_stuff_b1
                        , in_team
                        , in_type
                        , in_sub_id
                        , in_sub_serial
                ";
            }

            if (team_id != "")
            {
                sql = @"
                    SELECT 
                        * 
                    FROM 
                        IN_MEETING_PTEAM WITH(NOLOCK)
                    WHERE
                        (id = '{#team_id}' OR in_parent = '{#team_id}')
                    ORDER BY 
                        in_type
                        , in_sub_id
                        , in_sub_serial
                ";
            }

            sql = sql.Replace("{#program_id}", program_id)
                .Replace("{#in_creator_sno}", cfg.in_creator_sno)
                .Replace("{#team_id}", team_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        //取得組別量級選單
        private List<TSect> GetSectList(TConfig cfg, string program_id)
        {
            var lstSect = GetMSectList(cfg, program_id);

            var sects = new List<TSect>();

            for (int i = 0; i < lstSect.Count; i++)
            {
                Item itmSect = lstSect[i];

                var sect = new TSect
                {
                    no = i + 1,
                    in_sub_id = itmSect.getProperty("in_sub_id", ""),
                    in_name = itmSect.getProperty("in_name", ""),
                    in_gender = itmSect.getProperty("in_gender", ""),
                    in_ranges = itmSect.getProperty("in_ranges", ""),
                    in_weight = itmSect.getProperty("in_weight", ""),
                    in_note = itmSect.getProperty("in_note", ""),
                    Value = itmSect,
                };

                switch (sect.in_gender)
                {
                    case "W":
                        sect.gender_display = "女子";
                        break;

                    case "M":
                        sect.gender_display = "男子";
                        break;

                    default:
                        sect.gender_display = "";
                        break;
                }

                if (sect.in_ranges != "")
                {
                    var arr = sect.in_ranges.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    if (arr != null && arr.Length > 0)
                    {
                        sect.rangs = arr.ToList();
                    }
                }

                if (sect.rangs == null)
                {
                    sect.rangs = new List<string>();
                }

                sects.Add(sect);
            }

            return sects;
        }

        //取得團體賽量級清單
        private List<Item> GetMSectList(TConfig cfg, string program_id)
        {
            List<Item> result = new List<Item>();

            string sql = @"
                SELECT 
	                * 
                FROM 
	                IN_MEETING_PSECT WITH(NOLOCK)
                WHERE 
	                in_program = '{#program_id}'
	                AND in_name <> N'代表戰'
                ORDER BY 
	                in_sub_id
            ";

            sql = sql.Replace("{#program_id}", program_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item items = cfg.inn.applySQL(sql);
            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                result.Add(items.getItemByIndex(i));
            }
            return result;
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public Innosoft.InnovatorHelper InnH { get; set; }

            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strUserName { get; set; }
            public string ssn_loginid { get; set; }
            public bool isMeetingAdmin { get; set; }

            public string meeting_id { get; set; }
            public string program_id { get; set; }
            public string team_id { get; set; }
            public string scene { get; set; }
            public string subtype { get; set; }

            public Item itmMeeting { get; set; }
            public Item itmResume { get; set; }

            public string mt_title { get; set; }
            public string in_creator_sno { get; set; }
            public string in_index { get; set; }
            public string unfinished { get; set; }
            public string sub_scene { get; set; }
        }

        private class TMUser
        {
            public string muid { get; set; }
            public string in_name { get; set; }
            public string in_sno { get; set; }
            public string in_gender { get; set; }
            public string reg_birth { get; set; }
            public string in_age { get; set; }
            public string in_current_org { get; set; }
            public string in_short_org { get; set; }
            public string in_stuff_b1 { get; set; }
            public string in_show_org { get; set; }
            public string in_team { get; set; }
            public string in_index { get; set; }
            public string in_weight { get; set; }
            public string in_weight_status { get; set; }

            public string in_team_mode { get; set; }
            public string in_team_weight { get; set; }
            public string in_team_sect { get; set; }

            public string in_team_n1 { get; set; }
            public string in_team_n2 { get; set; }
            public string in_team_r1 { get; set; }
            public string in_team_r2 { get; set; }
            public string in_team_v1 { get; set; }
            public string in_team_v2 { get; set; }

            public string display { get; set; }

            public Item Value { get; set; }
        }

        private class TSect
        {
            public int no { get; set; }
            public string in_sub_id { get; set; }
            public string in_name { get; set; }
            public string in_gender { get; set; }
            public string in_ranges { get; set; }
            public string in_weight { get; set; }
            public string in_note { get; set; }
            public string gender_display { get; set; }

            public List<string> rangs { get; set; }

            public Item Value { get; set; }
        }

        private class TTeam
        {
            public string key { get; set; }
            public string name { get; set; }
            public List<TMUser> musers { get; set; }

            public string team_id { get; set; }
            public List<TChild> Children { get; set; }
        }

        private class TChild
        {
            public string team_id { get; set; }
            public string muid { get; set; }
            public string in_sub_weight { get; set; }
            public Item Value { get; set; }
        }

        private class TWeightView
        {
            public string muid { get; set; }
            public string pid { get; set; }
            public string value { get; set; }
            public string value_d { get; set; }
            public string value_s { get; set; }
            public decimal dcmValue { get; set; }

            public Item itmMeeting { get; set; }
            public Item itmMUser { get; set; }
            public Item itmParent { get; set; }
            public Item itmProgram { get; set; }

            public string in_sno { get; set; }
            public string in_team_mode { get; set; }
            public string in_team_weight { get; set; }
            public string in_team_sect { get; set; }

            public string in_team_r1 { get; set; }
            public string in_team_r2 { get; set; }

            public string in_team_n1 { get; set; }
            public string in_team_n2 { get; set; }

            public string in_team_v1 { get; set; }
            public string in_team_v2 { get; set; }

            public TWeightRange range { get; set; }

            public string in_weight_time { get; set; }

            /// <summary>
            /// 團體賽自動匹配鋒將位(賽會參數): 已停用，1: 需註冊，預設=自動匹配
            /// </summary>
            public string mt_auto_match { get; set; }
            /// <summary>
            /// 團體賽體重模式(0: 自動匹配、1: 依註冊、2: 雙級制)
            /// </summary>
            public string pg_auto_match { get; set; }
            /// <summary>
            /// 是否未報個人組
            /// </summary>
            public bool noSoloRegister { get; set; }

            public bool canRemoveChild { get; set; }

            /// <summary>
            /// on、off、leave、dq
            /// </summary>
            public string mu_weight_status { get; set; }

            /// <summary>
            /// 1: 過磅合格
            /// </summary>
            public string mu_weight_result { get; set; }

            /// <summary>
            /// 1: 通過
            /// </summary>
            public string tm_check_result { get; set; }

            /// <summary>
            /// 通過
            /// </summary>
            public string tm_check_status { get; set; }

            /// <summary>
            /// 過磅結果
            /// </summary>
            public string tm_weight_result { get; set; }

            /// <summary>
            /// 訊息
            /// </summary>
            public string tm_weight_message { get; set; }

            /// <summary>
            /// 需自動匹配
            /// </summary>
            public bool need_auto_match { get; set; }

        }

        private enum TWeightRange
        {
            None = 0,
            TrueWeight = 100,
            AsEqual = 200,
        }

        private class TEditModel
        {
            public string muid { get; set; }

            /// <summary>
            /// In_Meeting_PTeam parent id
            /// </summary>
            public string pid { get; set; }

            /// <summary>
            /// In_Meeting_PTeam child id
            /// </summary>
            public string cid { get; set; }

            /// <summary>
            /// In_Meeting_PSect in_sub_id
            /// </summary>
            public string sub { get; set; }

            /// <summary>
            /// In_Meeting_PSect in_name
            /// </summary>
            public string name { get; set; }

            /// <summary>
            /// 匹配-級
            /// </summary>
            public string in_team_v1 { get; set; }

            /// <summary>
            /// 匹配-量
            /// </summary>
            public string in_team_v2 { get; set; }

            /// <summary>
            /// 是否需要移除
            /// </summary>
            public string need_remove { get; set; }
        }

        private class TWStatus
        {
            public bool w1 { get; set; }
            public bool w2 { get; set; }
            public bool w3 { get; set; }
        }

        private DateTime GetDateTime(string value)
        {
            if (value == "") return DateTime.MinValue;
            DateTime result = DateTime.MinValue;
            DateTime.TryParse(value, out result);
            return result;
        }

        private decimal GetDcmVal(string value, decimal def = 0)
        {
            decimal result = def;
            decimal.TryParse(value, out result);
            return result;
        }

        private int GetIntVal(string value, int def = 0)
        {
            int result = def;
            int.TryParse(value, out result);
            return result;
        }
    }
}