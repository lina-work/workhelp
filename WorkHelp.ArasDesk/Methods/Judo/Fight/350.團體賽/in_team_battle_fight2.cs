﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Judo.Fight
{
    public class in_team_battle_fight2 : Item
    {
        public in_team_battle_fight2(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 出賽狀態
                日期: 
                    - 2023-05-22 創建 (Lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]in_team_battle_fight2";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, this.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,

                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,

                meeting_id = itmR.getProperty("meeting_id", ""),
                fight_day = itmR.getProperty("fight_day", ""),
                site_code = itmR.getProperty("site_code", ""),
                scene = itmR.getProperty("scene", ""),
            };

            switch (cfg.scene)
            {
                default:
                    AppendMeeting(cfg, itmR);
                    AppendDayMenu(cfg, itmR);
                    BattlePage(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void AppendDayMenu(TConfig cfg, Item itmReturn)
        {
            Item itmDays = GetDays(cfg);
            int count = itmDays.getItemCount();
            if (count <= 0)
            {
                throw new Exception("查無比賽日期");
            }

            if (cfg.fight_day == "")
            {
                var today = DateTime.Now.ToString("yyyy-MM-dd");
                var day1 = itmDays.getItemByIndex(0).getProperty("in_date_key", today);
                cfg.fight_day = day1;
                itmReturn.setProperty("fight_day", day1);
            }

            for (int i = 0; i < count; i++)
            {
                var itmDay = itmDays.getItemByIndex(i);
                string in_date_key = itmDay.getProperty("in_date_key", "");

                itmDay.setType("inn_date");
                itmDay.setProperty("value", in_date_key);
                itmDay.setProperty("label", in_date_key);
                itmReturn.addRelationship(itmDay);
            }
        }

        private Item GetDays(TConfig cfg)
        {
            string sql = @"
                SELECT DISTINCT 
	                t2.in_date_key 
                FROM 
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PEVENT t2 WITH(NOLOCK) 
	                ON t2.source_id = t1.id
                WHERE 
	                t1.in_meeting = '{#meeting_id}'
	                AND t1.in_l1 = '團體組'
                ORDER BY 
	                t2.in_date_key
            ";
            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);
            return cfg.inn.applySQL(sql);
        }

        private void AppendMeeting(TConfig cfg, Item itmReturn)
        {
            var sql = "SELECT id, in_title, in_uniform_color FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'";
            cfg.itmMeeting = cfg.inn.applySQL(sql);

            if (cfg.itmMeeting.isError() || cfg.itmMeeting.getResult() == "")
            {
                throw new Exception("賽程組別資料錯誤");
            }

            var in_uniform_color = cfg.itmMeeting.getProperty("in_uniform_color", "");
            var foot2_color = in_uniform_color == "WhiteBlue" ? "藍方" : "紅方";
            itmReturn.setProperty("in_title", cfg.itmMeeting.getProperty("in_title", ""));
            itmReturn.setProperty("in_uniform_color", in_uniform_color);
            itmReturn.setProperty("foot2_color", foot2_color);

            cfg.in_uniform_color = in_uniform_color;
            cfg.foot2_color = foot2_color;
        }

        //頁面
        private void BattlePage(TConfig cfg, Item itmReturn)
        {
            if (cfg.fight_day == "") return;

            cfg.df1 = "<span style='color: red'>未設定</span>";
            cfg.df2 = "<span style='color: red'>異常</span>";
            cfg.df3 = "<span style='color: blue'>V</span>";
            cfg.df4 = "X";

            var items = GetDayEventItems(cfg);
            var sites = MapSites(cfg, items);
            cfg.subs = GetSubStatus(cfg);

            var builder = new StringBuilder();
            for (var i = 0; i < sites.Count; i++)
            {
                var site = sites[i];
                AppendSiteTable(cfg, site, builder);
            }
        }

        private void AppendSiteTable(TConfig cfg, TSite site, StringBuilder builder )
        {
            var headCss = "text-center";

            var head = new StringBuilder();
            head.Append("<th class='" + headCss + "'>場次</th>");
            head.Append("<th class='" + headCss + "'>組別</th>");
            head.Append("<th class='" + headCss + "'>勝方</th>");
            head.Append("<th class='" + headCss + "'>白方單位</th>");
            head.Append("<th class='" + headCss + "'>白方狀態</th>");
            head.Append("<th class='" + headCss + "'>"+ cfg.foot2_color +"狀態</th>");
            head.Append("<th class='" + headCss + "'>"+ cfg.foot2_color +"單位</th>");

            var link1 = "c.aspx?page=In_Team_Battle_Fight.html&method=in_team_battle_fight"
                + "&meeting_id=" + cfg.meeting_id
                + "&program_id={#pg_id}";

            var link2 = "c.aspx?page=in_competition_preview.html&method=in_meeting_program_preview"
                + "&meeting_id=" + cfg.meeting_id
                + "&program_id={#pg_id}";

            var bodyCss = "text-center";
            var body = new StringBuilder();
            for (var i = 0; i < site.evts.Count; i++)
            {
                var evt = site.evts[i];
                if (evt.itmFoot1 == null) evt.itmFoot1 = cfg.inn.newItem();
                if (evt.itmFoot2 == null) evt.itmFoot2 = cfg.inn.newItem();

                var f1 = MapFoot(cfg, evt.itmFoot1);
                var f2 = MapFoot(cfg, evt.itmFoot2);
                var winResult = GetEvtWinStatus(cfg, f1, f2);

                var pg_id = evt.Value.getProperty("pg_id", "");
                var pg_short = evt.Value.getProperty("pg_short", "");
                var pg_url1 = link1.Replace("{#pg_id}", pg_id);
                var pg_url2 = link2.Replace("{#pg_id}", pg_id);

                var pg_link = pg_short
                    + " <a target='_blank' href='" + pg_url1 + "'><i class='fa fa-list-ol'></i></a>"
                    + " <a target='_blank' href='" + pg_url2 + "'><i class='fa fa-sitemap'></i></a>"
                    ;

                body.Append("<tr class='inno-evt-row' data-id='" + evt.id + "'>");
                body.Append("<td class='" + bodyCss + "'>" + evt.alias + "</td>");
                body.Append("<td class='" + bodyCss + "'>" + pg_link + "</td>");
                body.Append("<td class='" + bodyCss + "'>" + winResult + "</td>");
                body.Append("<td class='" + bodyCss + "'>" + f1.display_org + "</td>");
                body.Append("<td class='" + bodyCss + "'>" + f1.text + "</td>");
                body.Append("<td class='" + bodyCss + "'>" + f2.text + "</td>");
                body.Append("<td class='" + bodyCss + "'>" + f2.display_org + "</td>");
                body.Append("</tr>");
            }

            builder.Append("<div class='col-12 col-lg-6'>");
            builder.Append("<h3>" + site.name + "</h3>");
            builder.Append(GetTableAttribute("data-table-" + site.code));
            builder.Append("<thead>");
            builder.Append("  <tr>");
            builder.Append(head);
            builder.Append("  </tr>");
            builder.Append("</thead>");
            builder.Append("<tbody class='inno-sort-container'>");
            builder.Append(body);
            builder.Append("</tbody>");
            builder.Append("</table>");
            builder.Append("</div>");
        }

        private string GetTableAttribute(string table_name)
        {
            return "<table id='" + table_name + "'"
                + " class='table table-hover table-bordered table-rwd rwd rwdtable'"
                + " data-toggle='table'"
                + " data-show-toggle='false'"
                + " data-show-columns='false'"
                + " data-sort-stable='false'"
                + " data-search='false'"
                + ">";
        }

        private string GetEvtWinStatus(TConfig cfg, TFoot f1, TFoot f2)
        {
            //var in_win_local = itmFoot1.getProperty("in_win_local", "");
            //var in_win_sign_no = itmFoot1.getProperty("in_win_sign_no", "");
            //var f1_sign_no = itmFoot1.getProperty("in_sign_no", "");
            //var f2_sign_no = itmFoot2.getProperty("in_sign_no", "");

            if (f1.in_win_local != "") return f1.in_win_local;
            if (f1.in_win_sign_no != "" && f1.in_win_sign_no != "0")
            {
                if (f1.in_win_sign_no == f1.in_sign_no) return "W";
                if (f1.in_win_sign_no == f2.in_sign_no) return "B";
            }
            return "";
        }

        private List<TSite> MapSites(TConfig cfg, Item items)
        {
            var map = new Dictionary<int, TSite>();
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var code = GetIntVal(item.getProperty("site_code", "0"));
                if (!map.ContainsKey(code))
                {
                    map.Add(code, new TSite
                    {
                        code = code,
                        name = item.getProperty("site_name", ""),
                        evts = new List<TEvt>(),
                    });
                }

                var site = map[code];
                var key = item.getProperty("event_id", "");
                var site_code_en = item.getProperty("site_code_en", "");
                var in_tree_no = item.getProperty("in_tree_no", "");
                var foot = GetIntVal(item.getProperty("in_sign_foot", "0"));

                var old = site.evts.Find(x => x.id == key);
                if (old == null)
                {
                    old = new TEvt
                    {
                        id = key,
                        alias = site_code_en + in_tree_no,
                        Value = item
                    };
                    site.evts.Add(old);
                }
                if (foot == 1)
                {
                    old.itmFoot1 = item;
                }
                else if (foot == 2)
                {
                    old.itmFoot2 = item;
                }
            }
            return map.Values.ToList();
        }

        private class TSite
        {
            public string name { get; set; }
            public int code { get; set; }
            public List<TEvt> evts { get; set; }
        }

        private class TEvt
        {
            public string id { get; set; }
            public string alias { get; set; }
            public Item Value { get; set; }
            public Item itmFoot1 { get; set; }
            public Item itmFoot2 { get; set; }
        }

        private class TFoot
        {
            public string event_id { get; set; }
            public string in_sign_foot { get; set; }
            public string in_sign_no { get; set; }
            public string in_weight_message { get; set; }
            public string in_suspend { get; set; }

            public string in_win_status { get; set; }
            public string in_win_local { get; set; }
            public string in_win_sign_no { get; set; }
            
            public string sub_key { get; set; }
            public string text { get; set; }

            public string in_current_org { get; set; }
            public string display_org { get; set; }
        }

        private TFoot MapFoot(TConfig cfg, Item itmFoot)
        {
            var x = new TFoot();
            x.event_id = itmFoot.getProperty("event_id", "");
            x.in_sign_foot = itmFoot.getProperty("in_sign_foot", "");
            x.in_sign_no = itmFoot.getProperty("in_sign_no", "");
            x.in_current_org = itmFoot.getProperty("in_current_org", "");
            x.in_weight_message = itmFoot.getProperty("in_weight_message", "");
            x.in_suspend = itmFoot.getProperty("in_suspend", "");

            x.in_win_status = itmFoot.getProperty("in_win_status", "");
            x.in_win_local = itmFoot.getProperty("in_win_local", "");
            x.in_win_sign_no = itmFoot.getProperty("in_win_sign_no", "");

            x.sub_key = x.event_id + "-" + x.in_sign_foot;

            ResetFootStatus(cfg, x);

            var in_short_org = itmFoot.getProperty("in_short_org", "");
            var in_show_org = itmFoot.getProperty("in_show_org", "");
            var in_team = itmFoot.getProperty("in_team", "");

            if (in_show_org != "") in_short_org = in_show_org;
            if (in_team != "") in_short_org += " " + in_team;
            x.display_org = in_short_org;

            return x;
        }

        private void ResetFootStatus(TConfig cfg, TFoot x)
        {
            x.text = "";
            if (x.in_win_status != "" && x.in_current_org == "")
            {
                x.text = cfg.df4;
            }
            else if (x.in_win_status == "cancel")
            {
                x.text = "取消";
            }
            else if (x.in_win_status == "bypass")
            {
                x.text = "輪空";
            }
            else if (x.in_win_local != "")
            {
                x.text = "已完賽";
            }
            else if (x.in_win_sign_no != "" && x.in_win_sign_no != "0")
            {
                //非計分程式回送結果
                x.text = cfg.df3;
            }
            else if (x.in_weight_message != "")
            {
                x.text = x.in_weight_message;
            }
            else if (x.in_suspend != "")
            {
                x.text = x.in_suspend;
            }
            else if (x.event_id == "")
            {
                x.text = cfg.df2;
            }
            else
            {
                if (!cfg.subs.ContainsKey(x.sub_key))
                {
                    x.text = "ERROR";
                }
                else
                {
                    var itmSub = cfg.subs[x.sub_key];
                    var in_player_name = itmSub.getProperty("in_player_name", "");
                    if (in_player_name == "")
                    {
                        x.text = cfg.df1;
                    }
                    else if (in_player_name.Contains("第") && in_player_name.Contains("位"))
                    {
                        x.text = cfg.df1;
                    }
                    else
                    {
                        x.text = "已設定";
                    }
                }
            }
        }

        private Dictionary<string, Item> GetSubStatus(TConfig cfg)
        {
            var map = new Dictionary<string, Item>();

            var sql = @"
                SELECT 
					t1.source_id
					, t1.in_parent
	                , t1.id
	                , t1.in_tree_no
	                , t2.in_sign_foot
	                , t1.in_win_status
	                , t1.in_sub_id
	                , t1.in_sub_sect
	                , t2.in_player_name
                FROM 
	                IN_MEETING_PEVENT t1 WITH(NOLOCK) 
                INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                WHERE 
	                t1.in_meeting = '{#meeting_id}'
	                AND t1.in_date_key = '{#fight_day}'
	                AND t1.in_sub_id = 1
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#fight_day}", cfg.fight_day);

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++) 
            {
                var item = items.getItemByIndex(i);
                var in_parent = item.getProperty("in_parent", "");
                var in_sign_foot = item.getProperty("in_sign_foot", "");
                var key = in_parent + "-" + in_sign_foot;
                if (!map.ContainsKey(key)) map.Add(key, item);
            }
            return map;
        }

        private Item GetDayEventItems(TConfig cfg)
        {
            var siteCond = cfg.site_code == "" || cfg.site_code == "ALL"
                ? ""
                : "AND t12.in_code = " + cfg.site_code;

            var sql = @"
                SELECT
	                t11.id				AS 'pg_id'
					, t11.in_name		AS 'pg_name'
					, t11.in_short_name AS 'pg_short'
	                , t1.id             AS 'event_id'
	                , t1.in_tree_name
	                , t1.in_tree_id
	                , t1.in_tree_no
	                , t1.in_tree_alias
                    , t1.in_win_sign_no
                    , t1.in_win_local
                    , t2.id				AS 'detail_id'
                    , t2.in_sign_foot
	                , t2.in_sign_no
                    , t2.in_status
                    , t2.in_points
                    , t2.in_correct_count
                    , t2.in_suspend
					, t21.in_current_org
					, t21.in_short_org
					, t21.in_show_org
					, t21.in_team
					, t21.in_weight_message
					, t12.in_name AS 'site_name'
					, t12.in_code AS 'site_code'
					, t12.in_code_en AS 'site_code_en'
                FROM
	                IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                INNER JOIN
	                IN_MEETING_PROGRAM t11 WITH(NOLOCK)
	                ON t11.id = t1.source_id
                INNER JOIN
	                IN_MEETING_SITE t12 WITH(NOLOCK)
	                ON t12.id = t1.in_site
				LEFT OUTER JOIN
					IN_MEETING_PTEAM t21 WITH(NOLOCK)
					ON t21.source_id = t1.source_id
					AND t21.in_sign_no = t2.in_sign_no
                WHERE
	                t1.in_meeting = '{#meeting_id}'
	                AND t1.in_date_key = '{#fight_day}'
					{#siteCond}
	                AND ISNULL(t1.in_tree_no, 0) > 0
	                AND ISNULL(t1.in_type, '') = 'p'
                ORDER BY
	                t12.in_code
	                , t1.in_tree_no
					, t2.in_sign_foot
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#fight_day}", cfg.fight_day)
                .Replace("{#siteCond}", siteCond);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }

            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }

            public string meeting_id { get; set; }
            public string fight_day { get; set; }
            public string site_code { get; set; }
            public string scene { get; set; }

            public Item itmMeeting { get; set; }
            public string in_uniform_color { get; set; }
            public string foot2_color { get; set; }

            public Dictionary<string, Item> subs { get; set; }

            public string df1 { get; set; }
            public string df2 { get; set; }
            public string df3 { get; set; }
            public string df4 { get; set; }
        }

        private decimal GetDcmVal(string value, decimal def = 0)
        {
            decimal result = def;
            decimal.TryParse(value, out result);
            return result;
        }

        private int GetIntVal(string value, int def = 0)
        {
            if (value == null || value == "") return 0;

            int result = def;
            int.TryParse(value, out result);
            return result;
        }
    }
}