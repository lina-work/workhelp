﻿using Aras.IOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WorkHelp.ArasDesk.Methods.Judo.Fight
{
    public class in_team_battle_no_solo : Item
    {
        public in_team_battle_no_solo(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 團體組-未報個人
                日誌: 
                    - 2024-05-16: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "in_team_battle_no_solo";

            var itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                meeting_id = itmR.getProperty("meeting_id", ""),
                scene = itmR.getProperty("scene", ""),
            };

            cfg.meeting_id = "95C1607DDA434EFD9BE95FDC042A49D2";
            cfg.scene = "one_team_many_solo";

            switch (cfg.scene)
            {
                case "one_team_many_solo":
                    OneTeamManySolo(cfg, itmR);
                    break;

                case "analysis":
                    Analysis(cfg, itmR);
                    break;

                case "report":
                    RegisterWeightReport(cfg, itmR);
                    break;

                case "clear":
                    ClearWeight(cfg, itmR);
                    break;

                case "edit":
                    EditWeight(cfg, itmR);
                    break;

                case "page":
                    Page(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void OneTeamManySolo(TConfig cfg, Item itmReturn)
        {
            var teams = GetTeamPlayerList(cfg);
            var soloMap = GetSoloPlayerMap(cfg);
            var builder = new StringBuilder();
            for (var i = 0; i < teams.Count; i++)
            {
                var team = teams[i];
                if (soloMap.ContainsKey(team.in_sno))
                {
                    team.solos = soloMap[team.in_sno];
                }
                else
                {
                    team.solos = new List<TTeamPlayer>();
                }

                if (team.solos.Count > 1)
                {
                    var words = string.Join(",", team.solos.Select(x => x.in_team_solo));

                    var line = team.id
                        + "\t" + team.in_name
                        + "\t" + team.in_sno
                        + "\t" + team.in_l2
                        + "\t" + team.in_l3
                        + "\t" + words
                        + "\t" + team.in_team_solo
                        ;
                    builder.AppendLine(line);

                    //for (var j = 0; j < team.solos.Count; j++)
                    //{
                    //    var solo = team.solos[j];

                    //    var line = team.id
                    //        + "\t" + team.in_name
                    //        + "\t" + team.in_sno
                    //        + "\t" + team.in_l2
                    //        + "\t" + team.in_l3
                    //        + "\t" + solo.in_l2
                    //        + "\t" + solo.in_l3
                    //        + "\t" + team.in_team_solo
                    //        ;
                    //    builder.AppendLine(line);

                    //}
                }
            }

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, builder.ToString());
        }


        private class TTeamPlayer
        {
            public string id { get; set; }
            public string in_sno { get; set; }
            public string in_name { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }
            public string in_team_solo { get; set; }
            public string in_team_n1 { get; set; }
            public string in_team_n2 { get; set; }
            public string in_team_n3 { get; set; }
            public List<TTeamPlayer> solos { get; set; }
        }

        private Dictionary<string, List<TTeamPlayer>> GetSoloPlayerMap(TConfig cfg)
        {
            var map = new Dictionary<string, List<TTeamPlayer>>();
            var items = GetSoloPlayerItems(cfg);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var key = item.getProperty("in_sno", "").ToUpper();
                if (!map.ContainsKey(key)) map.Add(key, new List<TTeamPlayer>());
                var rows = map[key];
                var x = new TTeamPlayer
                {
                    in_sno = key,
                    in_name = item.getProperty("in_name", ""),
                    in_l2 = item.getProperty("in_l2", ""),
                    in_l3 = item.getProperty("in_l3", ""),
                    in_team_n1 = item.getProperty("in_team_n1", ""),
                    in_team_n2 = item.getProperty("in_team_n2", ""),
                    in_team_n3 = item.getProperty("in_team_n3", ""),
                };
                x.in_team_solo = x.in_team_n1 + x.in_team_n3.Replace("Kg", "");
                rows.Add(x);
            }
            return map;
        }
        private List<TTeamPlayer> GetTeamPlayerList(TConfig cfg)
        {
            var rows = new List<TTeamPlayer>();
            var items = GetTeamPlayerItems(cfg);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                rows.Add(new TTeamPlayer
                {
                    id = item.getProperty("id", ""),
                    in_sno = item.getProperty("in_sno", "").ToUpper(),
                    in_name = item.getProperty("in_name", ""),
                    in_l2 = item.getProperty("in_l2", ""),
                    in_l3 = item.getProperty("in_l3", ""),
                    in_team_solo = item.getProperty("in_team_solo", ""),
                });
            }
            return rows;
        }

        private Item GetSoloPlayerItems(TConfig cfg)
        {
            var sql = @"
                SELECT 
	                t1.id
	                , t1.in_name
	                , t1.in_sno
	                , t1.in_l2
	                , t1.in_l3
                FROM 
	                IN_MEETING_USER t1 WITH(NOLOCK)
                LEFT OUTER JOIN
	                VU_MEETING_SVY_L1 t11
	                ON t11.source_id = t1.source_id
	                AND t11.in_value = t1.in_l1
                LEFT OUTER JOIN
	                VU_MEETING_SVY_L2 t12
	                ON t12.source_id = t1.source_id
	                AND t12.in_filter = t1.in_l1
	                AND t12.in_value = t1.in_l2
                LEFT OUTER JOIN
	                VU_MEETING_SVY_L3 t13
	                ON t13.source_id = t1.source_id
	                AND t13.in_grand_filter = t1.in_l1
	                AND t13.in_filter = t1.in_l2
	                AND t13.in_value = t1.in_l3
                INNER JOIN
                (
                	SELECT DISTINCT
                		in_sno
                	FROM 
                		IN_MEETING_USER WITH(NOLOCK)
                	WHERE 
                		source_id = '{#meeting_id}'
                		AND in_l1 = '團體組'
                ) t21 ON t21.in_sno = t1.in_sno
                WHERE 
	                t1.source_id = '{#meeting_id}'
	                AND t1.in_l1 = '個人組'
                ORDER BY
	                t11.sort_order
	                , t12.sort_order
	                , t13.sort_order
	                , t1.in_sno
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            return cfg.inn.applySQL(sql);
        }

        private Item GetTeamPlayerItems(TConfig cfg)
        {
            var sql = @"
                SELECT 
	                t1.id
	                , t1.in_name
	                , t1.in_sno
	                , t1.in_l2
	                , t1.in_l3
	                , t1.in_team_solo
                FROM 
	                IN_MEETING_USER t1 WITH(NOLOCK)
                LEFT OUTER JOIN
	                VU_MEETING_SVY_L1 t11
	                ON t11.source_id = t1.source_id
	                AND t11.in_value = t1.in_l1
                LEFT OUTER JOIN
	                VU_MEETING_SVY_L2 t12
	                ON t12.source_id = t1.source_id
	                AND t12.in_filter = t1.in_l1
	                AND t12.in_value = t1.in_l2
                LEFT OUTER JOIN
	                VU_MEETING_SVY_L3 t13
	                ON t13.source_id = t1.source_id
	                AND t13.in_grand_filter = t1.in_l1
	                AND t13.in_filter = t1.in_l2
	                AND t13.in_value = t1.in_l3
                WHERE 
	                t1.source_id = '{#meeting_id}'
	                AND t1.in_l1 = '團體組'
	                AND t1.in_l2 NOT LIKE '特別'
                ORDER BY
	                t11.sort_order
	                , t12.sort_order
	                , t13.sort_order
	                , t1.in_sno
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            return cfg.inn.applySQL(sql);
        }

        private void Analysis(TConfig cfg, Item itmReturn)
        {
            FixTeamN1(cfg, "女");
            FixTeamN1(cfg, "男");
            FixTeamN2(cfg, "");
            FixTeamN3(cfg, "");

            FixTeamWeight(cfg, "");
            MatchTeamWeightFromSolo(cfg, "");
            ResetTeamWeightRegisterStatus(cfg, "");
            //ResetTeamSectWeightCheckStatus(cfg, "");

            //FixMeetingSectTeamWeightTable(cfg, "女");
            //FixMeetingSectTeamWeightTable(cfg, "男");

            //FixMeetingSectTeamWeightRanges(cfg);

            AnalysisSoloSects(cfg, "");
        }

        private void AnalysisSoloSects(TConfig cfg, string gender)
        {
            var sql = "";

            //先清欄位
            sql = "UPDATE IN_MEETING_USER SET"
                + " in_team_solo = NULL"
                + " WHERE source_id = '@id' AND in_l1 = N'團體組'";
            sql = sql.Replace("@id", cfg.meeting_id).Replace("@g", gender);
            cfg.inn.applySQL(sql);

            var soloMap = GetSoloMap(cfg);

            var items = GetTeamList(cfg);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var id = item.getProperty("id", "");
                var key = item.getProperty("in_sno", "").ToUpper().Trim();
                if (soloMap.ContainsKey(key))
                {
                    var list = soloMap[key];
                    var values = GetSoloSectText(cfg, list);
                    sql = "UPDATE IN_MEETING_USER SET in_team_solo = N'" + values + "' WHERE id = '" + id + "'";
                    cfg.inn.applySQL(sql);
                }
            }
        }

        private string GetSoloSectText(TConfig cfg, List<Item> list)
        {
            var lines = new List<string>();
            for (var i = 0; i < list.Count; i++)
            {
                var item = list[i];
                var in_team_n1 = item.getProperty("in_team_n1", "");
                var in_team_nv = item.getProperty("in_team_nv", "");
                lines.Add(in_team_n1 + in_team_nv); //大男乙-81
            }
            return string.Join(",", lines);
        }

        private Item GetTeamList(TConfig cfg)
        {
            var sql = @"
                SELECT
	                *
                FROM
	                VU_MEETING_TEAM_LIST
                WHERE
	                source_id = '@id'
                ORDER BY
	                in_stuff_b1
	                , in_l1
	                , in_l2
	                , in_l3
	                , in_team
            ";
            sql = sql.Replace("@id", cfg.meeting_id);
            return cfg.inn.applySQL(sql);
        }

        private Dictionary<string, List<Item>> GetSoloMap(TConfig cfg)
        {
            var map = new Dictionary<string, List<Item>>();

            var sql = @"
                SELECT
	                *
                FROM
	                VU_MEETING_SOLO_LIST
                WHERE
	                source_id = '@id'
                ORDER BY
	                in_stuff_b1
	                , in_l1
	                , in_l2
	                , in_l3
	                , in_team
            ";

            sql = sql.Replace("@id", cfg.meeting_id);

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var key = item.getProperty("in_sno", "");
                if (map.ContainsKey(key))
                {
                    map[key].Add(item);
                }
                else
                {
                    var list = new List<Item>();
                    list.Add(item);
                    map.Add(key, list);
                }
            }
            return map;
        }

        private void FixTeamWeight(TConfig cfg, string gender)
        {
            var sql = @"UPDATE IN_MEETING_USER SET in_team_nv = REPLACE(in_team_n3, 'Kg', '') WHERE source_id = '@id' AND in_l1 = '個人組'";
            sql = sql.Replace("@id", cfg.meeting_id).Replace("@g", gender);
            cfg.inn.applySQL(sql);
        }

        private void FixMeetingSectTeamWeightRanges(TConfig cfg)
        {
            var map = GetWeightRangeMap(cfg);

            var sql = @"
                SELECT 
	                id
	                , in_program_name
	                , in_group
	                , in_ranges
	                , in_min
	                , in_max 
                FROM 
	                IN_MEETING_PSECT WITH(NOLOCK)
                WHERE 
	                in_meeting = '@id'
	                AND ISNULL(in_ranges, '') <> ''
                ORDER BY 
	                in_program
	                , in_sub_id
            ";
            sql = sql.Replace("@id", cfg.meeting_id);
            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var id = item.getProperty("id", "");
                var in_group = item.getProperty("in_group", "");
                var in_ranges = item.getProperty("in_ranges", "").Replace(" ", "");
                var range = FindGroupWeightRow(cfg, map, in_group, in_ranges);
                if (range == null)
                {
                    throw new Exception(in_group + " > " + in_ranges + " 異常");
                }

                sql = "UPDATE IN_MEETING_PSECT SET"
                    + "  in_min = '" + range.in_min + "'"
                    + ", in_max = '" + range.in_max + "'"
                    + " WHERE id = '" + id + "'";

                cfg.inn.applySQL(sql);
            }
        }

        private TWeightRange FindGroupWeightRow(TConfig cfg, Dictionary<string, TWeightRange> map, string in_group, string in_ranges)
        {
            var row = new TWeightRange();

            var arr = in_ranges.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            if (arr == null || arr.Length == 0) return null;

            var keyMin = in_group + arr.First();
            var keyMax = in_group + arr.Last();

            if (map.ContainsKey(keyMin))
            {
                row.in_min = map[keyMin].in_min;
                row.min_ok = true;
            }
            if (map.ContainsKey(keyMax))
            {
                row.in_max = map[keyMax].in_max;
                row.max_ok = true;
            }

            if (row.min_ok && row.max_ok)
            {
                return row;
            }
            else
            {
                return null;
            }
        }

        private Dictionary<string, TWeightRange> GetWeightRangeMap(TConfig cfg)
        {
            var map = new Dictionary<string, TWeightRange>();
            var sql = "SELECT * FROM IN_GROUP_WEIGHT WITH(NOLOCK) ORDER BY in_no";
            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var in_group = item.getProperty("in_group", "");
                var in_weight = item.getProperty("in_weight", "");
                var in_value = in_weight.Replace("Kg", "");
                var key = in_group + in_value;

                map.Add(key, new TWeightRange
                {
                    key = key,
                    in_group = in_group,
                    in_weight = in_weight,
                    in_value = in_value,
                    in_min = item.getProperty("in_min", ""),
                    in_max = item.getProperty("in_max", ""),
                });
            }
            return map;
        }

        private class TWeightRange
        {
            public string key { get; set; }
            public string in_group { get; set; }
            public string in_weight { get; set; }
            public string in_value { get; set; }
            public string in_min { get; set; }
            public string in_max { get; set; }

            public bool min_ok { get; set; }
            public bool max_ok { get; set; }
        }

        private void FixMeetingSectTeamWeightTable(TConfig cfg, string gender)
        {
            var sql = "";

            sql = "UPDATE IN_MEETING_PSECT SET in_group = '社@g甲' WHERE in_meeting = '@id' AND in_program_name LIKE '%社會@g子甲%'";
            sql = sql.Replace("@id", cfg.meeting_id).Replace("@g", gender);
            cfg.inn.applySQL(sql);

            sql = "UPDATE IN_MEETING_PSECT SET in_group = '社@g乙' WHERE in_meeting = '@id' AND in_program_name LIKE '%社會@g子乙%'";
            sql = sql.Replace("@id", cfg.meeting_id).Replace("@g", gender);
            cfg.inn.applySQL(sql);

            sql = "UPDATE IN_MEETING_PSECT SET in_group = '大@g甲' WHERE in_meeting = '@id' AND in_program_name LIKE '%大專@g子甲%'";
            sql = sql.Replace("@id", cfg.meeting_id).Replace("@g", gender);
            cfg.inn.applySQL(sql);

            sql = "UPDATE IN_MEETING_PSECT SET in_group = '大@g乙' WHERE in_meeting = '@id' AND in_program_name LIKE '%大專@g子乙%'";
            sql = sql.Replace("@id", cfg.meeting_id).Replace("@g", gender);
            cfg.inn.applySQL(sql);

            sql = "UPDATE IN_MEETING_PSECT SET in_group = '國@g' WHERE in_meeting = '@id' AND in_program_name LIKE '%國中@g子%'";
            sql = sql.Replace("@id", cfg.meeting_id).Replace("@g", gender);
            cfg.inn.applySQL(sql);

            sql = "UPDATE IN_MEETING_PSECT SET in_group = '高@g' WHERE in_meeting = '@id' AND in_program_name LIKE '%高中@g子%'";
            sql = sql.Replace("@id", cfg.meeting_id).Replace("@g", gender);
            cfg.inn.applySQL(sql);

            sql = "UPDATE IN_MEETING_PSECT SET in_group = '小@gA' WHERE in_meeting = '@id' AND in_program_name LIKE '%國小@g子%'";
            sql = sql.Replace("@id", cfg.meeting_id).Replace("@g", gender);
            cfg.inn.applySQL(sql);
        }

        //private void ResetTeamSectWeightCheckStatus(TConfig cfg, string gender)
        //{
        //    var sql = "";

        //    sql = "UPDATE IN_MEETING_PROGRAM SET in_team_weight_check = 0 WHERE in_meeting = '@id' AND in_l1 = '團體組'";
        //    sql = sql.Replace("@id", cfg.meeting_id).Replace("@g", gender);
        //    cfg.inn.applySQL(sql);

        //    sql = "UPDATE IN_MEETING_PROGRAM SET in_team_weight_check = 1 WHERE in_meeting = '@id' AND in_l1 = '團體組' AND in_l3 NOT LIKE '%特別%'";
        //    sql = sql.Replace("@id", cfg.meeting_id).Replace("@g", gender);
        //    cfg.inn.applySQL(sql);
        //}

        private void ResetTeamWeightRegisterStatus(TConfig cfg, string gender)
        {
            var sql = "";

            sql = "UPDATE IN_MEETING_USER SET in_team_register = 0 WHERE source_id = '@id' AND in_l1 = '團體組'";
            sql = sql.Replace("@id", cfg.meeting_id).Replace("@g", gender);
            cfg.inn.applySQL(sql);

            sql = "UPDATE IN_MEETING_USER SET in_team_register = 1 WHERE source_id = '@id' AND in_l1 = '團體組' AND in_l3 NOT LIKE '%特別%' AND ISNULL(in_team_n3, '') = ''";
            sql = sql.Replace("@id", cfg.meeting_id).Replace("@g", gender);
            cfg.inn.applySQL(sql);
        }

        private void MatchTeamWeightFromSolo(TConfig cfg, string gender)
        {
            var sql = @"
                UPDATE t1 SET
	                  t1.in_team_nv = t2.in_team_nv
	                , t1.in_team_n2 = t2.in_team_n2
	                , t1.in_team_n3 = t2.in_team_n3
                FROM 
	                VU_MEETING_TEAM_LIST t1
                INNER JOIN
	                VU_MEETING_SOLO_LIST t2
	                ON t2.source_id = t1.source_id
	                AND t2.in_sno = t1.in_sno
	                AND t2.in_team_n1 = t1.in_team_n1
                WHERE 
	                t1.source_id = '@id'
            ";
            sql = sql.Replace("@id", cfg.meeting_id).Replace("@g", gender);
            cfg.inn.applySQL(sql);
        }

        private void FixTeamN3(TConfig cfg, string gender)
        {
            var sql = "UPDATE t1 SET t1.in_team_n3 = t2.in_weight, t1.in_team_nv = REPLACE(t2.in_weight, 'Kg', '')"
                + " FROM IN_MEETING_USER t1 WITH(NOLOCK)"
                + " INNER JOIN IN_GROUP_WEIGHT t2 ON t2.in_group = t1.in_team_n1 AND t2.in_level = t1.in_team_n2"
                + " WHERE source_id = '@id' AND in_l1 = '個人組'";

            sql = sql.Replace("@id", cfg.meeting_id).Replace("@g", gender);
            cfg.inn.applySQL(sql);
        }

        private void FixTeamN2(TConfig cfg, string gender)
        {
            var sql = "UPDATE IN_MEETING_USER SET in_team_n2 = LEFT(in_l3, 3) WHERE source_id = '@id' AND in_l1 = '個人組' AND in_l3 LIKE '第%'";
            sql = sql.Replace("@id", cfg.meeting_id).Replace("@g", gender);
            cfg.inn.applySQL(sql);
        }

        private void FixTeamN1(TConfig cfg, string gender)
        {
            var sql = "";

            sql = "UPDATE IN_MEETING_USER SET in_team_n1 = '社@g乙', in_team_grade = '社會組' WHERE in_l2 = '個-社會@g子乙組' AND in_l1 = '個人組' AND source_id = '@id'";
            sql = sql.Replace("@id", cfg.meeting_id).Replace("@g", gender);
            cfg.inn.applySQL(sql);

            sql = "UPDATE IN_MEETING_USER SET in_team_n1 = '社@g甲', in_team_grade = '社會組' WHERE in_l2 = '個-社會@g子甲組' AND in_l1 = '個人組' AND source_id = '@id'";
            sql = sql.Replace("@id", cfg.meeting_id).Replace("@g", gender);
            cfg.inn.applySQL(sql);

            sql = "UPDATE IN_MEETING_USER SET in_team_n1 = '大@g乙', in_team_grade = '大專組' WHERE in_l2 = '個-大專@g子乙組' AND in_l1 = '個人組' AND source_id = '@id'";
            sql = sql.Replace("@id", cfg.meeting_id).Replace("@g", gender);
            cfg.inn.applySQL(sql);

            sql = "UPDATE IN_MEETING_USER SET in_team_n1 = '大@g甲', in_team_grade = '大專組' WHERE in_l2 = '個-大專@g子甲組' AND in_l1 = '個人組' AND source_id = '@id'";
            sql = sql.Replace("@id", cfg.meeting_id).Replace("@g", gender);
            cfg.inn.applySQL(sql);

            sql = "UPDATE IN_MEETING_USER SET in_team_n1 = '高@g',   in_team_grade = '高中組' WHERE in_l2 = '個-高中@g子組' AND in_l1 = '個人組' AND source_id = '@id'";
            sql = sql.Replace("@id", cfg.meeting_id).Replace("@g", gender);
            cfg.inn.applySQL(sql);

            sql = "UPDATE IN_MEETING_USER SET in_team_n1 = '國@g',   in_team_grade = '國中組' WHERE in_l2 = '個-國中@g子組' AND in_l1 = '個人組' AND source_id = '@id'";
            sql = sql.Replace("@id", cfg.meeting_id).Replace("@g", gender);
            cfg.inn.applySQL(sql);

            sql = "UPDATE IN_MEETING_USER SET in_team_n1 = '小@gA',  in_team_grade = '國小組' WHERE in_l2 = '個-國小@g生高年級組' AND in_l1 = '個人組' AND source_id = '@id'";
            sql = sql.Replace("@id", cfg.meeting_id).Replace("@g", gender);
            cfg.inn.applySQL(sql);

            sql = "UPDATE IN_MEETING_USER SET in_team_n1 = '小@gB',  in_team_grade = '國小組' WHERE in_l2 = '個-國小@g生中年級組' AND in_l1 = '個人組' AND source_id = '@id'";
            sql = sql.Replace("@id", cfg.meeting_id).Replace("@g", gender);
            cfg.inn.applySQL(sql);

            sql = "UPDATE IN_MEETING_USER SET in_team_n1 = '小@gC',  in_team_grade = '國小組' WHERE in_l2 = '個-國小@g生低年級組' AND in_l1 = '個人組' AND source_id = '@id'";
            sql = sql.Replace("@id", cfg.meeting_id).Replace("@g", gender);
            cfg.inn.applySQL(sql);



            sql = "UPDATE IN_MEETING_USER  SET in_team_n1 = '社@g甲',  in_team_grade = '社會組'  WHERE in_l3 LIKE '社會@g子甲%' AND in_l1 = '團體組' AND source_id = '@id'";
            sql = sql.Replace("@id", cfg.meeting_id).Replace("@g", gender);
            cfg.inn.applySQL(sql);

            sql = "UPDATE IN_MEETING_USER  SET in_team_n1 = '社@g乙',  in_team_grade = '社會組'  WHERE in_l3 LIKE '社會@g子乙%' AND in_l1 = '團體組' AND source_id = '@id'";
            sql = sql.Replace("@id", cfg.meeting_id).Replace("@g", gender);
            cfg.inn.applySQL(sql);

            sql = "UPDATE IN_MEETING_USER  SET in_team_n1 = '大@g甲',  in_team_grade = '大專組'  WHERE in_l3 LIKE '大專@g子甲%' AND in_l1 = '團體組' AND source_id = '@id'";
            sql = sql.Replace("@id", cfg.meeting_id).Replace("@g", gender);
            cfg.inn.applySQL(sql);

            sql = "UPDATE IN_MEETING_USER  SET in_team_n1 = '大@g乙',  in_team_grade = '大專組'  WHERE in_l3 LIKE '大專@g子乙%' AND in_l1 = '團體組' AND source_id = '@id'";
            sql = sql.Replace("@id", cfg.meeting_id).Replace("@g", gender);
            cfg.inn.applySQL(sql);

            sql = "UPDATE IN_MEETING_USER  SET in_team_n1 = '高@g',  in_team_grade = '高中組'  WHERE in_l3 LIKE '高中@g子組%' AND in_l1 = '團體組' AND source_id = '@id'";
            sql = sql.Replace("@id", cfg.meeting_id).Replace("@g", gender);
            cfg.inn.applySQL(sql);

            sql = "UPDATE IN_MEETING_USER  SET in_team_n1 = '國@g',  in_team_grade = '國中組'  WHERE in_l3 LIKE '國中@g子組%' AND in_l1 = '團體組' AND source_id = '@id'";
            sql = sql.Replace("@id", cfg.meeting_id).Replace("@g", gender);
            cfg.inn.applySQL(sql);

            sql = "UPDATE IN_MEETING_USER  SET in_team_n1 = '小@gA',  in_team_grade = '國小組'  WHERE in_l3 LIKE '國小@g子%' AND in_l1 = '團體組' AND source_id = '@id'";
            sql = sql.Replace("@id", cfg.meeting_id).Replace("@g", gender);
            cfg.inn.applySQL(sql);
        }

        private void RegisterWeightReport(TConfig cfg, Item itmReturn)
        {
            var sql = "SELECT id, in_title FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'";
            cfg.itmMeeting = cfg.inn.applySQL(sql);
            if (cfg.itmMeeting.isError() || cfg.itmMeeting.getResult() == "")
            {
                throw new Exception("賽事資料錯誤");
            }
            cfg.mt_title = cfg.itmMeeting.getProperty("in_title", "");
            var exp = ExportInfo(cfg, "rank_path");
            var map = new Dictionary<string, TWeightGroup>();
            var items = GetNoSoloRegister(cfg);
            var orgs = MapOrgs(cfg, items);
            //試算表
            var book = new Spire.Xls.Workbook();
            for (var i = 0; i < orgs.Count; i++)
            {
                var org = orgs[i];
                AppendRegisterSheet(cfg, book, org, map);
            }

            book.Worksheets[0].Remove();
            book.Worksheets[0].Remove();
            book.Worksheets[0].Remove();

            var ext_name = ".xlsx";
            var xls_name = cfg.mt_title + "_團體賽-量級註冊單_" + DateTime.Now.ToString("MMdd_HHmmss");
            var xls_file = exp.export_Path + "\\" + xls_name + ext_name;
            var xls_url = xls_name + ext_name;

            book.SaveToFile(xls_file, Spire.Xls.ExcelVersion.Version2013);

            itmReturn.setProperty("xls_name", xls_name + ".xlsx");
        }

        private void AppendRegisterSheet(TConfig cfg, Spire.Xls.Workbook book, TOrg org, Dictionary<string, TWeightGroup> map)
        {
            var sheet = book.CreateEmptySheet();
            sheet.Name = org.in_short_org;
            sheet.PageSetup.PaperSize = Spire.Xls.PaperSizeType.PaperA4;

            //橫式列印
            //sheet.PageSetup.Orientation = Spire.Xls.PageOrientationType.Landscape;

            //將所有欄放入單一頁面
            sheet.PageSetup.FitToPagesWide = 1;

            //頁面配置-邊界
            sheet.PageSetup.TopMargin = 0.5;
            sheet.PageSetup.BottomMargin = 0.5;
            sheet.PageSetup.LeftMargin = 0.5;
            sheet.PageSetup.RightMargin = 0.5;

            SetSheetTitle(cfg, sheet, "A1:L1", cfg.mt_title, true, 28, 42, true);
            SetSheetTitle(cfg, sheet, "A2:L2", "團體賽-量級註冊單(未報個人組)", true, 24, 30, false);
            sheet["A1:L1"].Style.ShrinkToFit = true;

            SetSheetTitle(cfg, sheet, "A4", "單位：", false, 14, 25, false, 2);
            SetSheetTitle(cfg, sheet, "B4", org.in_current_org, false, 14, 25, true, 1);
            //SetSheetTitle(cfg, sheet, "I4", "日期：", false, 14, 25, false, 2);

            var ridx = 6;

            for (var i = 0; i < org.ages.Count; i++)
            {
                var age = org.ages[i];

                var rstart = ridx;

                SetSheetTitle(cfg, sheet, "A" + ridx + ":" + "B" + ridx, "競賽組別", true, 14, 25, true);
                SetSheetTitle(cfg, sheet, "C" + ridx + ":" + "D" + ridx, "選手姓名", true, 14, 25, true);
                SetSheetTitle(cfg, sheet, "E" + ridx + ":" + "L" + ridx, "參賽量級", true, 14, 25, true);
                ridx++;

                for (var j = 0; j < age.items.Count; j++)
                {
                    var item = age.items[j];
                    var in_l3 = item.getProperty("in_l3", "");
                    var in_name = item.getProperty("in_name", "");
                    var weights = MatchAgeWeightList2(cfg, map, item);

                    SetSheetTitle(cfg, sheet, "A" + ridx + ":" + "B" + ridx, in_l3, true, 12, 24, false, 1);
                    SetSheetTitle(cfg, sheet, "C" + ridx + ":" + "D" + ridx, in_name, true, 12, 24, false, 1);
                    SetSheetTitle(cfg, sheet, "E" + ridx + ":" + "L" + ridx, weights, true, 11, 24, false, 1);
                    ridx++;

                }

                SetRangeBorder(sheet, "A" + rstart + ":" + "L" + (ridx - 1));

                ridx++;
            }

            var rend = ridx + 2;
            SetSheetTitle(cfg, sheet, "D" + rend, "教練親簽：", false, 16, 28, false, 2);
            SetSheetTitle(cfg, sheet, "H" + rend, "日期：", false, 16, 28, false, 2);

            sheet.Range["E" + rend].Borders[Spire.Xls.BordersLineType.EdgeBottom].LineStyle = Spire.Xls.LineStyleType.Thin;
            sheet.Range["F" + rend].Borders[Spire.Xls.BordersLineType.EdgeBottom].LineStyle = Spire.Xls.LineStyleType.Thin;
            sheet.Range["G" + rend].Borders[Spire.Xls.BordersLineType.EdgeBottom].LineStyle = Spire.Xls.LineStyleType.Thin;
            sheet.Range["I" + rend].Borders[Spire.Xls.BordersLineType.EdgeBottom].LineStyle = Spire.Xls.LineStyleType.Thin;
            sheet.Range["J" + rend].Borders[Spire.Xls.BordersLineType.EdgeBottom].LineStyle = Spire.Xls.LineStyleType.Thin;
        }

        //設定格線
        private void SetRangeBorder(Spire.Xls.Worksheet sheet, string pos)
        {
            var range = sheet.Range[pos];
            range.BorderInside(Spire.Xls.LineStyleType.Thin, System.Drawing.Color.Black);
            range.BorderAround(Spire.Xls.LineStyleType.Thin, System.Drawing.Color.Black);
        }

        private void SetSheetTitle(TConfig cfg, Spire.Xls.Worksheet sheet, string pos, string value, bool isMerge, int fontSize, int height, bool isBold, int ha = 0)
        {
            var range = sheet.Range[pos];

            if (isMerge) range.Merge();

            switch (ha)
            {
                case 1:
                    range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Left;
                    break;
                case 2:
                    range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Right;
                    break;
                default:
                    range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                    break;
            }

            range.Style.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;
            range.Text = value;
            range.Style.Font.Size = fontSize;
            range.Style.Font.IsBold = isBold;
            range.RowHeight = height;
            range.Style.Font.FontName = "標楷體";
        }

        private void SetSheetHead(TConfig cfg, Spire.Xls.Worksheet sheet, string pos, string value)
        {
            var range = sheet.Range[pos];
            range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
            range.Style.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;
            range.Text = value;
            range.Style.Font.Size = 14;
            range.Style.Font.FontName = "標楷體";
            range.Style.Font.IsBold = true;
        }

        private TExport ExportInfo(TConfig cfg, string in_name)
        {
            var itmXls = cfg.inn.applyMethod("In_Meeting_Excel_Path", "<in_name>" + in_name + "</in_name>");
            var result = new TExport
            {
                template_Path = itmXls.getProperty("template_path", ""),
                export_Path = itmXls.getProperty("export_path", ""),
            };
            return result;
        }

        private class TExport
        {
            public string template_Path { get; set; }
            public string export_Path { get; set; }
        }

        private class TField
        {
            public string title { get; set; }
            public string property { get; set; }
            public TCss css { get; set; }
            public int ci { get; set; }
            public string cs { get; set; }
            public int width { get; set; }
            public Func<TConfig, TField, Item, string> getValue { get; set; }

            public bool is_merge { get; set; }
        }

        private enum TCss
        {
            None = 0,
            Center = 10,
            Text = 100,
            Double = 200,
        }

        private List<TOrg> MapOrgs(TConfig cfg, Item items)
        {
            var orgs = new List<TOrg>();
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var key1 = item.getProperty("in_stuff_b1", "");
                var key2 = item.getProperty("in_short_name", "") + item.getProperty("in_team", "");
                var org = orgs.Find(x => x.key == key1);
                if (org == null)
                {
                    org = new TOrg
                    {
                        key = key1,
                        in_stuff_b1 = item.getProperty("in_stuff_b1", ""),
                        in_short_org = item.getProperty("in_short_org", ""),
                        in_current_org = item.getProperty("in_current_org", ""),
                        ages = new List<TAge>(),
                    };
                    orgs.Add(org);
                }

                var age = org.ages.Find(x => x.key == key2);
                if (age == null)
                {
                    age = new TAge
                    {
                        key = key2,
                        items = new List<Item>(),
                    };
                    org.ages.Add(age);
                }
                age.items.Add(item);
            }
            return orgs;
        }

        private class TOrg
        {
            public string key { get; set; }
            public string in_stuff_b1 { get; set; }
            public string in_short_org { get; set; }
            public string in_current_org { get; set; }
            public List<TAge> ages { get; set; }
        }

        private class TAge
        {
            public string key { get; set; }
            public List<Item> items { get; set; }
        }

        private void ClearWeight(TConfig cfg, Item itmReturn)
        {
            var muid = itmReturn.getProperty("muid", "");
            var in_team_nv = "";
            var in_team_n2 = "";
            var in_team_n3 = "";

            var sql = "UPDATE IN_MEETING_USER SET"
                + "  in_team_nv = '" + in_team_nv + "'"
                + ", in_team_n2 = '" + in_team_n2 + "'"
                + ", in_team_n3 = '" + in_team_n3 + "'"
                + " WHERE id = '" + muid + "'";

            cfg.inn.applySQL(sql);


            sql = "DELETE FROM IN_MEETING_PTEAM WHERE in_muser = '" + muid + "'";
            cfg.inn.applySQL(sql);
        }

        private void EditWeight(TConfig cfg, Item itmReturn)
        {
            var muid = itmReturn.getProperty("muid", "");
            var in_team_nv = itmReturn.getProperty("in_team_nv", "");
            var in_team_n2 = itmReturn.getProperty("in_team_n2", "");
            var in_team_n3 = itmReturn.getProperty("in_team_n3", "");

            var sql = "UPDATE IN_MEETING_USER SET"
                + "  in_team_nv = '" + in_team_nv + "'"
                + ", in_team_n2 = '" + in_team_n2 + "'"
                + ", in_team_n3 = '" + in_team_n3 + "'"
                + " WHERE id = '" + muid + "'";

            cfg.inn.applySQL(sql);

            Item itmData = cfg.inn.newItem("In_Meeting_User");
            itmData.setProperty("meeting_id", cfg.meeting_id);
            itmData.setProperty("muid", muid);
            itmData.setProperty("scene", "run");
            itmData.apply("in_team_battle_weight_match");
        }

        private void Page(TConfig cfg, Item itmReturn)
        {
            var sql = "SELECT id, in_title FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'";
            cfg.itmMeeting = cfg.inn.applySQL(sql);
            if (cfg.itmMeeting.isError() || cfg.itmMeeting.getResult() == "")
            {
                throw new Exception("賽事資料錯誤");
            }

            var items = GetNoSoloRegister(cfg);
            var count = items.getItemCount();
            var builder = new StringBuilder();
            if (count <= 0) count = 0;

            AppendPlayerTable(cfg, items, builder);

            itmReturn.setProperty("in_title", cfg.itmMeeting.getProperty("in_title", ""));
            itmReturn.setProperty("inn_table", builder.ToString());
            itmReturn.setProperty("inn_count", "(共 " + count + " 筆)");

        }

        private void AppendPlayerTable(TConfig cfg, Item items, StringBuilder builder)
        {
            var tb_id = "data-table";
            var head = new StringBuilder();
            var body = new StringBuilder();
            var count = items.getItemCount();
            var map = new Dictionary<string, TWeightGroup>();
            var clearButton = "<button class='btn btn-default' onclick='ResetTeamWeightStatus(this)'>清空</button>";

            head.Append("<thead>");
            head.Append("<tr>");
            head.Append("  <th class='text-center'>No.</th>");
            head.Append("  <th class='text-center'>報名者</th>");
            head.Append("  <th class='text-center'>所屬單位</th>");
            head.Append("  <th class='text-center'>競賽組別</th>");
            head.Append("  <th class='text-center'>選手</th>");
            head.Append("  <th class='text-center'>參賽量級</th>");
            head.Append("  <th class='text-center'>功能</th>");
            head.Append("</tr>");
            head.Append("</thead>");

            body.Append("<tbody>");
            for (int i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var id = item.getProperty("id", "");
                var in_stuff_b1 = item.getProperty("in_stuff_b1", "");
                var in_short_org = item.getProperty("in_short_org", "");
                var in_team = item.getProperty("in_team", "");

                var org = in_stuff_b1
                    + " " + in_short_org
                    + " " + in_team;

                body.Append("<tr>");
                body.Append("<td class='text-center'>" + (i + 1) + "</td>");
                body.Append("<td class='text-left'>" + item.getProperty("in_creator", "") + "</td>");
                body.Append("<td class='text-left'>" + org + "</td>");
                body.Append("<td class='text-left'>" + item.getProperty("in_l3", "") + "</td>");
                body.Append("<td class='text-left'>" + item.getProperty("in_name", "") + "</td>");
                body.Append("<td class='text-left' data-muid='" + id + "'>" + MatchAgeWeightList(cfg, map, item) + "</td>");
                body.Append("<td class='text-left' data-muid='" + id + "'>" + clearButton + "</td>");
                body.Append("</tr>");
            }
            body.Append("</tbody>");

            builder.Append("<table id='" + tb_id + "'"
                + " class='table table-hover table-bordered table-rwd rwd'"
                + " style='background-color: #fff;' "
                + " data-toggle='table' "
                + " data-search='false'"
                + " data-search-align='left'"
                + ">");

            builder.Append(head);
            builder.Append(body);
            builder.Append("</table>");
        }

        private string MatchAgeWeightList(TConfig cfg, Dictionary<string, TWeightGroup> map, Item itmMUser)
        {
            var group = default(TWeightGroup);
            var key = itmMUser.getProperty("in_team_n1", "");//大女乙
            var in_level = itmMUser.getProperty("in_team_n2", "");//第一級
            var in_weight = itmMUser.getProperty("in_team_n3", "");//-48Kg
            var setted = in_weight != "";
            var css = "span-btn-selected";

            if (map.ContainsKey(key))
            {
                group = map[key];
            }
            else
            {
                group = NewWeightGroup(cfg, key);
                map.Add(key, group);
            }

            var builder = new StringBuilder();
            for (var i = 0; i < group.weights.Count; i++)
            {
                var w = group.weights[i];
                if (setted && w.in_level == in_level)
                {
                    builder.Append(WeightButton(cfg, w, css));
                }
                else
                {
                    builder.Append(WeightButton(cfg, w, ""));
                }

            }
            return builder.ToString();
        }

        private string MatchAgeWeightList2(TConfig cfg, Dictionary<string, TWeightGroup> map, Item itmMUser)
        {
            var group = default(TWeightGroup);
            var key = itmMUser.getProperty("in_team_n1", "");//大女乙
            var in_level = itmMUser.getProperty("in_team_n2", "");//第一級
            var in_weight = itmMUser.getProperty("in_team_n3", "");//-48Kg
            var setted = in_weight != "";

            if (map.ContainsKey(key))
            {
                group = map[key];
            }
            else
            {
                group = NewWeightGroup(cfg, key);
                map.Add(key, group);
            }

            var builder = new StringBuilder();
            for (var i = 0; i < group.weights.Count; i++)
            {
                var w = group.weights[i];
                var c = "□";
                //if (setted && w.in_level == in_level)
                //{
                //    c = "■";
                //}
                if (builder.Length > 0) builder.Append(", ");
                builder.Append(c + w.in_value);

            }
            return builder.ToString();
        }

        private TWeightGroup NewWeightGroup(TConfig cfg, string in_group)
        {
            var model = new TWeightGroup { in_group = in_group, weights = new List<TWeightRow>() }; ;
            if (in_group == "") return model;

            var sql = @"
                SELECT
	                *
                FROM 
	                IN_GROUP_WEIGHT WITH(NOLOCK)
                WHERE
	                in_group = '{#in_group}'
                ORDER BY
	                in_index
            ";

            sql = sql.Replace("{#in_group}", in_group);

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var weight = new TWeightRow
                {
                    in_level = item.getProperty("in_level", ""),
                    in_weight = item.getProperty("in_weight", ""),
                };
                weight.in_value = weight.in_weight.Replace("Kg", "");
                model.weights.Add(weight);
            }
            return model;
        }

        private string WeightButton(TConfig cfg, TWeightRow w, string css)
        {
            return "<span class='span-btn " + css + "' onclick='SetWeight(this)' style='margin-right: 5px'"
                + " data-level='" + w.in_level + "'"
                + " data-weight='" + w.in_weight + "'"
                + " data-value='" + w.in_value + "'"
                + ">" + w.in_value + "</span>";
        }

        private class TWeightGroup
        {
            public string in_group { get; set; }
            public List<TWeightRow> weights { get; set; }
        }

        private class TWeightRow
        {
            public string in_level { get; set; }
            public string in_weight { get; set; }
            public string in_value { get; set; }
        }

        private Item GetNoSoloRegister(TConfig cfg)
        {
            var sql = @"
                SELECT
	                t1.source_id
	                , t1.id
	                , t1.in_stuff_b1
	                , t1.in_current_org
	                , t1.in_short_org
	                , t1.in_name
	                , t1.in_sno
	                , t1.in_l1
	                , t1.in_l2
	                , t1.in_l3
	                , t1.in_index
	                , t1.in_team
	                , t1.in_creator
	                , t1.in_creator_sno
	                , t1.in_team_nv
	                , t1.in_team_n1
	                , t1.in_team_n2
	                , t1.in_team_n3
	                , t1.in_team_grade
	                , t2.id AS 'program_id'
	                , t2.in_fight_day
	                , t2.in_short_name
                FROM 
	                IN_MEETING_USER t1 WITH(NOLOCK)
                LEFT OUTER JOIN
	                IN_MEETING_PROGRAM t2
	                ON t2.in_meeting = t1.source_id
	                AND t2.in_l1 = t1.in_l1
	                AND t2.in_l2 = t1.in_l2
	                AND t2.in_l3 = t1.in_l3
                WHERE 
	                t1.source_id = '{#meeting_id}'
	                AND t1.in_l1 = N'團體組'
	                AND t1.in_l2 NOT LIKE '%特別%'
	                AND t1.in_team_register = 1
                ORDER BY
	                t1.in_city_no
	                , t1.in_stuff_b1
	                , t1.in_current_org
	                , t2.in_sort_order
	                , t1.in_team
	                , t1.in_name
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string meeting_id { get; set; }
            public string scene { get; set; }

            public Item itmMeeting { get; set; }
            public string mt_title { get; set; }
        }
    }
}