﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Judo.Fight
{
    public class in_team_battle_serial : Item
    {
        public in_team_battle_serial(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
    目的: 團體賽-重設子場次出賽動態序號
    日期: 
        - 2022-08-22 創建 (lina)
*/

            System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]in_team_battle_serial";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, this.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,

                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,

                meeting_id = itmR.getProperty("meeting_id", ""),
                program_id = itmR.getProperty("program_id", ""),
                in_sub_event = itmR.getProperty("in_sub_event", ""),
                ages = itmR.getProperty("ages", ""),
                scene = itmR.getProperty("scene", ""),
            };

            switch (cfg.scene)
            {
                case "round_serial":
                    Reset_Parant_Event_RoundSerial(cfg);
                    break;

                case "team_serial":
                    Reset_Children_Event_RoundSerial(cfg);
                    break;
            }

            return itmR;
        }

        private void Reset_Parant_Event_RoundSerial(TConfig cfg)
        {
            var itmProgram = cfg.inn.applySQL("SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + cfg.program_id + "' AND in_l1 = N'團體組'");
            UpdateRoundSerial(cfg, itmProgram);
        }

        private void Reset_Children_Event_RoundSerial(TConfig cfg)
        {
            if (cfg.ages == "")
            {
                Run_Reset_Children_Event_RoundSerial(cfg, "");
            }
            else
            {
                var l2s = cfg.ages.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                if (l2s == null || l2s.Length == 0)
                {
                    Run_Reset_Children_Event_RoundSerial(cfg, "");
                }
                else
                {
                    foreach (var l2 in l2s)
                    {
                        Run_Reset_Children_Event_RoundSerial(cfg, l2);
                    }
                }
            }
        }
        private void Run_Reset_Children_Event_RoundSerial(TConfig cfg, string in_l2)
        {
            string cond_l2 = in_l2 == ""
                ? ""
                : "AND in_l3 LIKE N'%" + in_l2 + "%'";

            string sql = @"
                SELECT
	                *
                FROM
	                IN_MEETING_PROGRAM WITH(NOLOCK)
                WHERE
	                in_meeting = '{#meeting_id}'
                    AND in_l1 = N'團體組'
	                AND ISNULL(in_sub_event, 0) = {#in_sub_event}
                    AND in_fight_day NOT IN ('2022-11-12', '2022-11-13')
	                {#cond_l2}
                ORDER BY
	                in_sort_order
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_sub_event}", cfg.in_sub_event)
                .Replace("{#cond_l2}", cond_l2);

            Item itmPrograms = cfg.inn.applySQL(sql);

            int count = itmPrograms.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item itmProgram = itmPrograms.getItemByIndex(i);
                //主場次輪次序號重編
                UpdateRoundSerial(cfg, itmProgram);
                //子場次輪次序號重編
                ResetSubEventSerail(cfg, itmProgram);
                //子場次全部重編
                ResetSubTreeNo(cfg, itmProgram);
                //子場次單位名稱校正
                ResetSubOrgName(cfg, itmProgram);
            }
        }

        //子場次單位名稱校正
        private void ResetSubOrgName(TConfig cfg, Item itmProgram)
        {
            string program_id = itmProgram.getProperty("id", "");

            string sql = @"
				UPDATE t12 SET
	                t12.in_player_org = ISNULL(t3.in_short_org, '')
	                , t12.in_player_team = ISNULL(t3.in_team, '')
	                , t12.in_player_name = N'第' + CONVERT(VARCHAR, t11.in_team_serial) + N'位'
	                , t12.in_parent = t2.id
                FROM 
	                IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                LEFT OUTER JOIN
	                IN_MEETING_PTEAM t3 WITH(NOLOCK)
	                ON t3.source_id = t1.source_id
	                AND t3.in_sign_no = t2.in_sign_no
				INNER JOIN
					IN_MEETING_PEVENT t11 WITH(NOLOCK) 
					ON t11.in_parent = t1.id
				INNER JOIN
					IN_MEETING_PEVENT_DETAIL t12 WITH(NOLOCK) 
					ON t12.source_id = t11.id
					AND t12.in_sign_foot = t2.in_sign_foot
                WHERE 
	                t1.source_id = '{#program_id}'
	                AND ISNULL(t1.in_type, '') IN ('', 'p')
            ";

            sql = sql.Replace("{#program_id}", program_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            cfg.inn.applySQL(sql);
        }

        //子場次全部重編
        private void ResetSubTreeNo(TConfig cfg, Item itmProgram)
        {
            string program_id = itmProgram.getProperty("id", "");

            string sql = @"
                UPDATE t1 SET
                    t1.in_tree_no = t2.in_tree_no * 100 + t1.in_team_serial
	                , t1.in_period = t2.in_period
	                , t1.in_medal = t2.in_medal
	                , t1.in_show_site = t2.in_show_site
	                , t1.in_show_serial = t2.in_tree_no * 100 + t1.in_team_serial
                FROM 
                    IN_MEETING_PEVENT t1 WITH(NOLOCK) 
                INNER JOIN
                    IN_MEETING_PEVENT t2 WITH(NOLOCK) 
                    ON t2.id = t1.in_parent
                WHERE
                    t2.source_id = '{#program_id}'
                    AND ISNULL(t2.in_type, '') = 'p'
                    AND ISNULL(t2.in_tree_no, 0) > 0
                    AND ISNULL(t1.in_team_serial, 0) > 0
            ";

            sql = sql.Replace("{#program_id}", program_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            cfg.inn.applySQL(sql);
        }

        //子場次輪次序號重編
        private void ResetSubEventSerail(TConfig cfg, Item itmProgram)
        {
            string program_id = itmProgram.getProperty("id", "");
            string in_sub_event = itmProgram.getProperty("in_sub_event", "0");
            string in_sect_start = itmProgram.getProperty("in_sect_start", "0");

            int sub_event = GetIntVal(in_sub_event);
            int sect_start = GetIntVal(in_sect_start);

            if (sub_event <= 0) throw new Exception("子場次錯誤");
            if (sect_start <= 0) throw new Exception("起始場次錯誤");


            string sql = @"
                SELECT 
                    t1.id
                    , t1.in_tree_no
                    , t1.in_sub_id
                    , t1.in_sub_sect
                    , t2.in_round_serial
                    , t3.in_sect_start
                FROM 
                    IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN 
                    IN_MEETING_PEVENT t2 
                    ON t2.id = t1.in_parent
                INNER JOIN 
                    IN_MEETING_PROGRAM t3 
                    ON t3.id = t2.source_id
                WHERE 
                    t1.source_id = '{#program_id}'
                    AND ISNULL(t1.in_tree_no, 0) > 0
                    AND ISNULL(t1.in_type, '') = 's'
                ORDER BY 
                    t1.in_tree_no
                    , t1.in_sub_id
            ";

            sql = sql.Replace("{#program_id}", program_id);

            Item items = cfg.inn.applySQL(sql);
            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string id = item.getProperty("id", "");
                string in_round_serial = item.getProperty("in_round_serial", "0");
                string in_sub_id = item.getProperty("in_sub_id", "0");
                string in_sub_sect = item.getProperty("in_sub_sect", "");
                int round_serial = GetIntVal(in_round_serial);
                int sub_id = GetIntVal(in_sub_id);

                if (round_serial < 0) throw new Exception("輪次錯誤");
                if (sub_id <= 0) throw new Exception("子場次序號錯誤");

                string[] fight_arr = GetFightArr(sub_event, sect_start, round_serial);
                string fight_no = fight_arr[sub_id - 1];

                string sql_upd = "UPDATE IN_MEETING_PEVENT SET in_team_serial = " + fight_no + " WHERE id = '" + id + "'";
                // if (in_sub_sect == "代表戰")
                // {
                //     sql_upd = "UPDATE IN_MEETING_PEVENT SET in_team_serial = " + (sub_event + 1) + " WHERE id = '" + id + "'";
                // }

                Item itmResult = cfg.inn.applySQL(sql_upd);
            }
        }

        //更新場次輪次
        private void UpdateRoundSerial(TConfig cfg, Item itmProgram)
        {
            string program_id = itmProgram.getProperty("id", "");
            string in_battle_type = itmProgram.getProperty("in_battle_type", "");
            string in_team_count = itmProgram.getProperty("in_team_count", "0");
            string prcode = itmProgram.getProperty("in_round_code", "0");

            if (in_battle_type.Contains("RoundRobin"))
            {
                //循環戰，都從抽到的那個量級開打
                switch (in_team_count)
                {
                    case "5":
                        UpdERound_Robin(cfg, program_id, "1, 2", "1", "第1輪");
                        UpdERound_Robin(cfg, program_id, "3, 4", "2", "第2輪");
                        UpdERound_Robin(cfg, program_id, "5, 6", "3", "第3輪");
                        UpdERound_Robin(cfg, program_id, "7, 8", "4", "第4輪");
                        UpdERound_Robin(cfg, program_id, "9, 10", "5", "第5輪");
                        break;

                    case "4":
                        UpdERound_Robin(cfg, program_id, "1, 2", "1", "第1輪");
                        UpdERound_Robin(cfg, program_id, "3, 4", "2", "第2輪");
                        UpdERound_Robin(cfg, program_id, "5, 6", "3", "第3輪");
                        break;

                    case "3":
                        UpdERound_Robin(cfg, program_id, "1", "1", "第1輪");
                        UpdERound_Robin(cfg, program_id, "2", "2", "第2輪");
                        UpdERound_Robin(cfg, program_id, "3", "3", "第3輪");
                        break;

                    case "2":
                        UpdERound_Robin(cfg, program_id, "1", "1", "第1輪");
                        break;
                }
            }
            else
            {
                //全柔錦採單淘(3 隊以下採循環賽)
                switch (prcode)
                {
                    case "64": //64強對戰
                        UpdERound(cfg, program_id, tname: "main", rcode: "64", serial: "1", rname: "第1輪");
                        UpdERound(cfg, program_id, tname: "main", rcode: "32", serial: "2", rname: "第2輪");
                        UpdERound(cfg, program_id, tname: "main", rcode: "16", serial: "3", rname: "第3輪");
                        UpdERound(cfg, program_id, tname: "main", rcode: "8", serial: "4", rname: "第4輪");
                        UpdERound(cfg, program_id, tname: "main", rcode: "4", serial: "5", rname: "第5輪");
                        UpdERound(cfg, program_id, tname: "main", rcode: "2", serial: "6", rname: "第6輪");
                        break;
                    case "32": //32強對戰
                        UpdERound(cfg, program_id, tname: "main", rcode: "32", serial: "1", rname: "第1輪");
                        UpdERound(cfg, program_id, tname: "main", rcode: "16", serial: "2", rname: "第2輪");
                        UpdERound(cfg, program_id, tname: "main", rcode: "8", serial: "3", rname: "第3輪");
                        UpdERound(cfg, program_id, tname: "main", rcode: "4", serial: "4", rname: "第4輪");
                        UpdERound(cfg, program_id, tname: "main", rcode: "2", serial: "5", rname: "第5輪");
                        break;
                    case "16": //16強對戰
                        UpdERound(cfg, program_id, tname: "main", rcode: "16", serial: "1", rname: "第1輪");
                        UpdERound(cfg, program_id, tname: "main", rcode: "8", serial: "2", rname: "第2輪");
                        UpdERound(cfg, program_id, tname: "main", rcode: "4", serial: "3", rname: "第3輪");
                        UpdERound(cfg, program_id, tname: "main", rcode: "2", serial: "4", rname: "第4輪");
                        break;
                    case "8": //8強對戰
                        UpdERound(cfg, program_id, tname: "main", rcode: "8", serial: "1", rname: "第1輪");
                        UpdERound(cfg, program_id, tname: "main", rcode: "4", serial: "2", rname: "第2輪");
                        UpdERound(cfg, program_id, tname: "main", rcode: "2", serial: "3", rname: "第3輪");
                        break;
                    case "4": //8強對戰
                        UpdERound(cfg, program_id, tname: "main", rcode: "4", serial: "1", rname: "第1輪");
                        UpdERound(cfg, program_id, tname: "main", rcode: "2", serial: "2", rname: "第2輪");
                        break;
                }
            }
        }

        //循環賽
        private void UpdERound_Robin(TConfig cfg, string program_id, string ns, string serial, string rname)
        {
            string sql = @"
                UPDATE IN_MEETING_PEVENT SET
	                in_round_serial = {#serial}
	                , in_round_name = N'{#rname}' 
                WHERE 
	                source_id = '{#program_id}'
                    AND ISNULL(in_type, '') = 'p'
                    AND ISNULL(in_tree_sno, 0) IN ({#ns})
            ";

            sql = sql.Replace("{#program_id}", program_id)
                .Replace("{#ns}", ns)
                .Replace("{#serial}", serial)
                .Replace("{#rname}", rname);

            Item itmUpd = cfg.inn.applySQL(sql);

            if (itmUpd.isError())
            {
                throw new Exception("更新場次的輪次編號發生錯誤");
            }
        }

        //更新場次的輪次編號
        private void UpdERound(TConfig cfg, string program_id, string tname, string rcode, string serial, string rname)
        {
            string sql = @"
                UPDATE IN_MEETING_PEVENT SET 
                    in_round_serial = {#serial}
                    , in_round_name = N'{#rname}' 
                WHERE 
	                source_id = '{#program_id}'
	                AND in_tree_name = '{#tname}'
	                AND in_round_code = {#rcode}
	                AND ISNULL(in_type, '') = 'p'
            ";

            sql = sql.Replace("{#program_id}", program_id)
                .Replace("{#tname}", tname)
                .Replace("{#rcode}", rcode)
                .Replace("{#serial}", serial)
                .Replace("{#rname}", rname);

            Item itmUpd = cfg.inn.applySQL(sql);

            if (itmUpd.isError())
            {
                throw new Exception("更新場次的輪次編號發生錯誤");
            }
        }

        private string[] GetFightArr(int count, int start, int serial)
        {
            string[] result = new string[count + 1];

            if (start > 0)
            {
                int rno = start + (serial - 1);
                int sidx = rno - 1;
                int eidx = sidx + count;

                int no = 1;
                for (int i = sidx; i < eidx; i++)
                {
                    var idx = i % count;
                    result[idx] = no.ToString();
                    no++;
                }
            }
            else
            {
                for (int i = 0; i < count; i++)
                {
                    result[i] = "";
                }
            }

            result[count] = (count + 1) + "";

            return result;
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public Innosoft.InnovatorHelper InnH { get; set; }

            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }

            public string meeting_id { get; set; }
            public string program_id { get; set; }
            public string in_sub_event { get; set; }
            public string ages { get; set; }
            public string scene { get; set; }

            public Item itmMeeting { get; set; }
            public Item itmProgram { get; set; }
        }

        private int GetIntVal(string value, int def = 0)
        {
            int result = def;
            int.TryParse(value, out result);
            return result;
        }
    }
}