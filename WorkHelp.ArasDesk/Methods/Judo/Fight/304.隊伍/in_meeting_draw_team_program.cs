﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Judo.Fight
{
    public class in_meeting_draw_team_program : Item
    {
        public in_meeting_draw_team_program(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 另建賽事組別隊伍資料(單組)
                輸入: meeting_id、program_id
                日期: 
                    2022-10-12: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]in_meeting_draw_team_program";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "input dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                strIdentityId = inn.getUserAliases(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                program_id = itmR.getProperty("program_id", ""),
                scene = itmR.getProperty("scene", ""),
            };

            if (cfg.meeting_id == "")
            {
                throw new Exception("賽事 id 不得為空白");
            }
            if (cfg.program_id == "")
            {
                throw new Exception("組別 id 不得為空白");
            }

            switch (cfg.scene)
            {
                case "clear_sign_no":
                    ClearSignNo(cfg, itmR);
                    break;

                case "rebuild":
                    Rebuild(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void ClearSignNo(TConfig cfg, Item itmReturn)
        {
            string sql = @"
                UPDATE IN_MEETING_PTEAM SET
	                  in_final_rank = NULL
	                , in_show_rank = NULL
	                , in_sign_no = NULL
	                , in_sign_time = NULL
	                , in_judo_no = NULL
	                , in_show_no = NULL
                    , in_not_draw = 0
                WHERE
	                source_id = '{#program_id}'
            ";

            sql = sql.Replace("{#program_id}", cfg.program_id);
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);
            cfg.inn.applySQL(sql);

            sql = @"
                UPDATE t1 SET
                    t1.in_show_no = t2.rno
                FROM
                    IN_MEETING_PTEAM t1
                INNER JOIN
                (
                    SELECT
                        id
                        , ROW_NUMBER() OVER(PARTITION BY source_id ORDER BY in_stuff_b1, in_team, in_sno) AS 'rno'
                    FROM
                        IN_MEETING_PTEAM WITH(NOLOCK)
                    WHERE
                        source_id = '{#program_id}'
                        AND ISNULL(in_type, '') in ('', 'p')
                        AND ISNULL(in_not_draw, 0) = 0
                ) t2
                    ON t2.id = t1.id
                WHERE
                    t1.source_id = '{#program_id}'
            ";

            sql = sql.Replace("{#program_id}", cfg.program_id);
            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);
            cfg.inn.applySQL(sql);

            sql = @"
                UPDATE t1 SET
        	        t1.in_sign_no = t2.in_sign_no
        	        , t1.in_section_no = t2.in_section_no
                FROM
        	        IN_MEETING_USER t1 WITH(NOLOCK)
                INNER JOIN
        	        IN_MEETING_PTEAM t2 WITH(NOLOCK)
        	        ON t2.in_meeting = t1.source_id
        	        AND t2.in_team_key = ISNULL(t1.in_l1, '') 
        		        + '-' + ISNULL(t1.in_l2, '') 
        		        + '-' + ISNULL(t1.in_l3, '') 
        		        + '-' + ISNULL(t1.in_index, '')
        		        + '-' + ISNULL(t1.in_creator_sno, '')
                WHERE
        	        t2.source_id = '{#program_id}'
                    AND ISNULL(t2.in_not_draw, 0) = 0
            ";

            sql = sql.Replace("{#program_id}", cfg.program_id);
            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);
            cfg.inn.applySQL(sql);

            sql = @"
                UPDATE
                    IN_MEETING_PROGRAM
                SET
                    in_sign_time = NULL
                WHERE
                    id = '{#program_id}'
            ";

            sql = sql.Replace("{#program_id}", cfg.program_id);
            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);
            cfg.inn.applySQL(sql);
        }

        private void Rebuild(TConfig cfg, Item itmReturn)
        {
            MergeTeam(cfg, itmReturn);
            UpdateOrgTeams(cfg, itmReturn);
            UpdateShowNo(cfg, itmReturn);
            RebuildOneProgram(cfg, itmReturn);
        }

        private void RebuildOneProgram(TConfig cfg, Item itmReturn)
        {
            //取得賽事組別
            string sql = "SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + cfg.program_id + "'";
            Item itmPrograms = cfg.inn.applySQL(sql);
            int program_count = itmPrograms.getItemCount();
            for (int i = 0; i < program_count; i++)
            {
                Item itmProgram = itmPrograms.getItemByIndex(i);
                itmProgram.setType("In_Meeting_Program");
                itmProgram.setProperty("scene", "");
                itmProgram.setProperty("meeting_id", cfg.meeting_id);
                itmProgram.setProperty("l1_value", itmProgram.getProperty("in_l1", ""));
                itmProgram.setProperty("l2_value", itmProgram.getProperty("in_l2", ""));
                itmProgram.setProperty("l3_value", itmProgram.getProperty("in_l3", ""));
                itmProgram.setProperty("muid", "");
                itmProgram.setProperty("no_straw", "1");
                itmProgram.apply("in_meeting_straw");
            }
        }

        private void MergeTeam(TConfig cfg, Item itmReturn)
        {
            string sql = "";

            //同步中
            sql = "UPDATE IN_MEETING_PTEAM SET in_is_sync = '1' WHERE source_id = '" + cfg.program_id + "'";
            cfg.inn.applySQL(sql);

            //取得賽事組別
            sql = "SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + cfg.program_id + "'";
            Item itmPrograms = cfg.inn.applySQL(sql);

            //取得單位簡稱對照表
            List<TOrgMap> entities = GetOrgMap(cfg);

            //取得既有隊伍清單
            sql = "SELECT t1.* FROM IN_MEETING_PTEAM t1 WITH(NOLOCK)"
                + " INNER JOIN IN_MEETING_PROGRAM t2 WITH(NOLOCK) ON t2.id = t1.source_id"
                + " WHERE t1.source_id = '" + cfg.program_id + "'"
                + " AND ISNULL(t1.in_type, '') IN ('', 'p')";
            Item itmTeams = cfg.inn.applySQL(sql);

            Dictionary<string, Item> team_map = new Dictionary<string, Item>();

            int team_count = itmTeams.getItemCount();

            for (int i = 0; i < team_count; i++)
            {
                Item itmTeam = itmTeams.getItemByIndex(i);
                string in_team_key = itmTeam.getProperty("in_team_key", "");

                if (!team_map.ContainsKey(in_team_key))
                {
                    team_map.Add(in_team_key, itmTeam);
                }
            }

            int program_count = itmPrograms.getItemCount();

            int no = 1;

            for (int i = 0; i < program_count; i++)
            {
                Item itmProgram = itmPrograms.getItemByIndex(i);

                string program_id = itmProgram.getProperty("id", "");

                Dictionary<string, List<Item>> dictionary = GetProgramTeams(cfg, itmProgram);

                foreach (KeyValuePair<string, List<Item>> kv in dictionary)
                {
                    string in_team_key = kv.Key;

                    List<Item> list = kv.Value;
                    Item itmTeam = list[0];

                    string in_index = itmTeam.getProperty("in_index", "");

                    string in_group = itmTeam.getProperty("in_group", "");
                    string in_current_org = itmTeam.getProperty("in_current_org", "");
                    string in_short_org = itmTeam.getProperty("in_short_org", "");
                    string in_team = itmTeam.getProperty("in_team", "");
                    string in_creator_sno = itmTeam.getProperty("in_creator_sno", "");
                    //string in_stuff_b1 = itmTeam.getProperty("in_stuff_b1", "");
                    //string in_stuff_b1 = in_current_org.Substring(0, 3);
                    string in_stuff_b1 = itmTeam.getProperty("in_stuff_b1", "");
                    string in_seed_lottery = itmTeam.getProperty("in_seed_lottery", "");

                    string in_team_players = list.Count.ToString();

                    string in_name = "";
                    string in_sno = "";
                    string in_names = "";
                    string in_team_index = GetTeamIndex((no).ToString());

                    //修補單位簡稱
                    in_short_org = GetShortOrg(entities, in_current_org, in_short_org);

                    if (list.Count == 1)
                    {
                        in_name = itmTeam.getProperty("in_name", "");
                        in_sno = itmTeam.getProperty("in_sno", "");
                        in_names = in_name;
                    }
                    else
                    {
                        in_names = GetNames(list);

                        if (in_team != "")
                        {
                            in_name = in_team;
                            in_names = in_names + " (" + in_team + "隊)";
                        }
                        else
                        {
                            in_name = "";
                        }
                    }

                    Item itmPTeam = cfg.inn.newItem("In_Meeting_PTeam");
                    itmPTeam.setProperty("in_meeting", cfg.meeting_id);
                    itmPTeam.setProperty("source_id", program_id);
                    itmPTeam.setProperty("in_team_index", in_team_index);
                    itmPTeam.setProperty("in_team_key", in_team_key);

                    itmPTeam.setProperty("in_index", in_index);
                    itmPTeam.setProperty("in_group", in_group);
                    itmPTeam.setProperty("in_current_org", in_current_org);
                    itmPTeam.setProperty("in_short_org", in_short_org);
                    itmPTeam.setProperty("in_team", in_team);
                    itmPTeam.setProperty("in_team_players", in_team_players);
                    itmPTeam.setProperty("in_creator_sno", in_creator_sno);
                    itmPTeam.setProperty("in_stuff_b1", in_stuff_b1);

                    itmPTeam.setProperty("in_name", in_name);
                    itmPTeam.setProperty("in_sno", in_sno);
                    itmPTeam.setProperty("in_names", in_names);

                    itmPTeam.setProperty("in_names", in_names);

                    itmPTeam.setProperty("in_type", "p");

                    if (in_seed_lottery != "" && in_seed_lottery != "0")
                    {
                        int seed_lottery = GetIntVal(in_seed_lottery);
                        int base_lottery = 10000;
                        string in_points = (base_lottery - seed_lottery).ToString(); // 積分，由種子籤倒扣

                        itmPTeam.setProperty("in_seeds", in_seed_lottery);
                        itmPTeam.setProperty("in_points", in_points);
                    }

                    string action = "add";

                    if (team_map.ContainsKey(in_team_key))
                    {
                        action = "merge";

                        Item itmOld = team_map[in_team_key];
                        itmPTeam.setAttribute("where", "id='" + itmOld.getProperty("id", "") + "'");
                        itmPTeam.setProperty("id", itmOld.getProperty("id", ""));

                        //打上存在標記
                        itmOld.setProperty("is_exist", "1");
                    }

                    itmPTeam = itmPTeam.apply(action);

                    no++;
                }
            }

            for (int i = 0; i < team_count; i++)
            {
                Item itmTeam = itmTeams.getItemByIndex(i);
                string id = itmTeam.getProperty("id", "");
                string is_exist = itmTeam.getProperty("is_exist", "");

                if (is_exist == "1")
                {
                    continue;
                }

                //刪除該隊伍
                cfg.inn.applySQL("DELETE FROM IN_MEETING_PTEAM WHERE id = '" + id + "'");
            }

            //刪除子隊伍
            cfg.inn.applySQL("DELETE FROM IN_MEETING_PTEAM WHERE source_id = '" + cfg.program_id + "' AND in_type = 's'");
        }

        //更新單位隊伍數
        private void UpdateOrgTeams(TConfig cfg, Item itmReturn)
        {
            string sql = "";

            sql = @"
                UPDATE
                    t1
                SET
                    t1.in_org_teams = t2.in_org_teams
                FROM
                    IN_MEETING_PTEAM t1
                LEFT OUTER JOIN
                (
                    SELECT 
                        source_id
                        , in_current_org
                        , count(id) AS 'in_org_teams'
                    FROM
                        IN_MEETING_PTEAM WITH(NOLOCK)
                    WHERE
                        in_meeting = '{#meeting_id}'
                        AND in_type = 'p'
                    GROUP BY
                        source_id
                        , in_current_org
                ) t2 
                    ON t2.source_id = t1.source_id
                    AND t2.in_current_org = t1.in_current_org
                WHERE
                    t1.in_meeting = '{#meeting_id}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            cfg.inn.applySQL(sql);
        }

        //更新單位隊伍 No
        private void UpdateShowNo(TConfig cfg, Item itmReturn)
        {
            string sql = "";

            sql = @"
                UPDATE t1 SET
	                t1.in_show_no = t2.rno
                FROM 
	                IN_MEETING_PTEAM t1
                INNER JOIN
                (
	                SELECT
		                id
		                , ROW_NUMBER() OVER (PARTITION BY source_id ORDER BY in_stuff_b1, in_team, in_sno) AS 'rno'
	                FROM
		                IN_MEETING_PTEAM WITH(NOLOCK)
	                WHERE
		                in_meeting = '{#meeting_id}'
		                AND in_type = 'p'
                ) t2
                ON t2.id = t1.id
                WHERE
	                t1.in_meeting = '{#meeting_id}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            cfg.inn.applySQL(sql);
        }

        //修補與會者單位簡稱
        private Dictionary<string, List<Item>> GetProgramTeams(TConfig cfg, Item itmProgram)
        {
            string sql = "";

            string in_l1 = itmProgram.getProperty("in_l1", "");
            string in_l2 = itmProgram.getProperty("in_l2", "");
            string in_l3 = itmProgram.getProperty("in_l3", "");

            sql = @"
                SELECT
                    *
                FROM
                    IN_MEETING_USER t1 WITH(NOLOCK)
                WHERE
                    t1.source_id = '{#meeting_id}'
                    AND ISNULL(t1.in_l1, '') = N'{#in_l1}'
                    AND ISNULL(t1.in_l2, '') = N'{#in_l2}'
                    AND ISNULL(t1.in_l3, '') = N'{#in_l3}'
                ORDER BY
                    t1.in_stuff_b1
                    , t1.in_team
                    , t1.in_sno
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_l1}", in_l1)
                .Replace("{#in_l2}", in_l2)
                .Replace("{#in_l3}", in_l3);

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();

            Dictionary<string, List<Item>> dictionary = new Dictionary<string, List<Item>>();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_index = item.getProperty("in_index", "");
                string in_creator_sno = item.getProperty("in_creator_sno", "");

                string in_team_key = in_l1 + "-" + in_l2 + "-" + in_l3 + "-" + in_index + "-" + in_creator_sno;

                List<Item> list = null;
                if (dictionary.ContainsKey(in_team_key))
                {
                    list = dictionary[in_team_key];
                }
                else
                {
                    list = new List<Item>();
                    dictionary.Add(in_team_key, list);
                }
                list.Add(item);
            }

            return dictionary;
        }

        #region 單位簡稱處理

        /// <summary>
        /// 取得單位簡稱
        /// </summary>
        private string GetShortOrg(List<TOrgMap> entities, string in_current_org, string in_short_org)
        {
            if (in_short_org == "" || in_short_org.Length > 6)
            {
                var comparison = StringComparison.InvariantCultureIgnoreCase;

                var item = entities.FirstOrDefault(x => x.in_current_org.Equals(in_current_org, comparison));

                if (item == null) item = entities.FirstOrDefault(x => x.in_alias1.Equals(in_current_org, comparison));

                if (item == null) item = entities.FirstOrDefault(x => x.in_alias2.Equals(in_current_org, comparison));

                if (item == null) item = entities.FirstOrDefault(x => x.in_group.Equals(in_current_org, comparison));

                if (item == null)
                {
                    return in_short_org == "" ? in_current_org : in_short_org;
                }
                else
                {
                    return item.in_short_org;
                }
            }
            else
            {
                return in_short_org;
            }
        }

        /// <summary>
        /// 取得單位名稱對照表
        /// </summary>
        private List<TOrgMap> GetOrgMap(TConfig cfg)
        {
            List<TOrgMap> entities = new List<TOrgMap>();

            string sql = "SELECT * FROM IN_ORG_MAP WITH(NOLOCK)";

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);

                TOrgMap entity = new TOrgMap
                {
                    in_stuff_b1 = item.getProperty("in_stuff_b1", ""),
                    in_stuff_a1 = item.getProperty("in_stuff_a1", ""),
                    in_group = item.getProperty("in_group", ""),
                    in_current_org = item.getProperty("in_current_org", ""),
                    in_short_org = item.getProperty("in_short_org", ""),
                    in_alias1 = item.getProperty("in_alias1", ""),
                    in_alias2 = item.getProperty("in_alias2", ""),
                    in_alias3 = item.getProperty("in_alias3", ""),
                };
                entities.Add(entity);
            }

            return entities;
        }

        private class TOrgMap
        {
            /// <summary>
            /// 團體會員編號
            /// </summary>
            public string in_stuff_b1 { get; set; }

            /// <summary>
            /// 團體會員名稱
            /// </summary>
            public string in_stuff_a1 { get; set; }

            /// <summary>
            /// 所屬群組
            /// </summary>
            public string in_group { get; set; }

            /// <summary>
            /// 所屬單位
            /// </summary>
            public string in_current_org { get; set; }

            /// <summary>
            /// 團體會員編號
            /// </summary>
            public string in_short_org { get; set; }

            /// <summary>
            /// 所屬群組別名
            /// </summary>
            public string in_alias1 { get; set; }

            /// <summary>
            /// 所屬單位別名
            /// </summary>
            public string in_alias2 { get; set; }

            /// <summary>
            /// 別名3
            /// </summary>
            public string in_alias3 { get; set; }
        }

        #endregion 單位簡稱處理

        private string GetNames(List<Item> list)
        {
            List<string> names = new List<string>();
            List<string> names_w = new List<string>();


            for (int i = 0; i < list.Count; i++)
            {
                Item item = list[i];
                string in_name = item.getProperty("in_name", "");
                string in_waiting_list = item.getProperty("in_waiting_list", "");

                if (in_waiting_list.Contains("備"))
                {
                    names_w.Add(in_name + "[備]");
                }
                else
                {
                    names.Add(in_name);
                }
            }

            string split = ", ";
            string in_names = string.Join(split, names);

            if (names_w.Count > 0)
            {
                if (in_names != "")
                {
                    in_names += split;
                }
                in_names += string.Join(split, names_w);
            }

            return in_names;
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }

            public string meeting_id { get; set; }
            public string program_id { get; set; }
            public string scene { get; set; }
        }

        private int GetIntVal(string value, int def = 0)
        {
            int result = def;
            int.TryParse(value, out result);
            return result;
        }

        private string GetTeamIndex(string value)
        {
            return value.PadLeft(5, '0');
        }
    }
}