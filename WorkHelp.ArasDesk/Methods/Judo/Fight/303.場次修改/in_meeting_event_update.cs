﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Judo.Fight
{
    public class in_meeting_event_update : Item
    {
        public in_meeting_event_update(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
   目的: 更新場次資料
   日誌: 
       - 2022-05-19: 創建 (lina)
       - 2022-02-10: 創建 (lina)
*/

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "in_meeting_event_update";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,

                meeting_id = itmR.getProperty("meeting_id", ""),
                program_id = itmR.getProperty("program_id", ""),
                event_id = itmR.getProperty("event_id", ""),
                scene = itmR.getProperty("scene", ""),
            };

            Item itmPermit = inn.applyMethod("In_CheckIdentity", "<method>" + strMethodName + "</method><code>" + "ALL" + "</code>");
            bool isMeetingAdmin = itmPermit.getProperty("isMeetingAdmin", "") == "1";
            if (!isMeetingAdmin)
            {
                throw new Exception("無權限");
            }

            switch (cfg.scene)
            {
                case "edit_time":
                    EditTime(cfg, itmR);
                    break;

                case "change":
                    ChangeInfo(cfg, itmR);
                    break;

                case "tree_no":
                    SetTreeNo(cfg, cfg.event_id, itmR.getProperty("tree_no", ""));
                    break;

                case "site_id":
                    SetSiteId(cfg, itmR);
                    break;

                case "site_item":
                    SetSiteItem(cfg, itmR);
                    break;

                case "site_item2":
                    string in_site = itmR.getProperty("in_site", "");
                    SetSiteItem(cfg, cfg.event_id, in_site);
                    break;

                case "edit_site":
                    EditSiteCode(cfg, itmR);
                    break;

                case "batch_move":
                    CallSportHub(cfg);
                    BatchMove(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void EditSiteCode(TConfig cfg, Item itmReturn)
        {
            string sql = "";
            string in_tree_id = itmReturn.getProperty("in_tree_id", "");
            string new_site_code = itmReturn.getProperty("new_site_code", "");

            sql = "SELECT id FROM IN_MEETING_PEVENT WITH(NOLOCK)"
                + " WHERE source_id = '" + cfg.program_id + "'"
                + " AND in_tree_id = '" + in_tree_id + "'";

            Item itmEvent = cfg.inn.applySQL(sql);
            if (itmEvent.isError() || itmEvent.getResult() == "")
            {
                throw new Exception("查無場次資料");
            }

            sql = "SELECT * FROM IN_MEETING_SITE WITH(NOLOCK)"
                + " WHERE in_meeting = '" + cfg.meeting_id + "'"
                + " AND in_code = '" + new_site_code + "'";

            Item itmSite = cfg.inn.applySQL(sql);
            if (itmSite.isError() || itmSite.getResult() == "")
            {
                throw new Exception("查無場地資料");
            }

            string event_id = itmEvent.getProperty("id", "");
            SetSiteFromItem(cfg, event_id, itmSite);
        }

        private void BatchMove(TConfig cfg, Item itmReturn)
        {
            var data = itmReturn.getProperty("data", "");
            var rows = GetEvtShowList(cfg, data);
            if (rows == null || rows.Count == 0)
            {
                throw new Exception("無資料");
            }

            foreach (var row in rows)
            {
                if (row.new_show_site == "") continue;
                if (row.new_show_serial == "") continue;
                if (row.old_site_code == row.new_show_site)
                {
                    RollbackSiteMove(cfg, row);
                }
                else
                {
                    CommitSiteMove(cfg, row);
                }
            }
        }

        private void CommitSiteMove(TConfig cfg, TEvtShow row)
        {
            var sql = "UPDATE IN_MEETING_PEVENT SET"
                + "  in_show_site = '" + row.new_show_site + "'"
                + ", in_show_serial = '" + row.new_show_serial + "'"
                + ", in_site_move = '" + row.new_show_site + "'"
                + ", in_site_time = getutcdate()"
                + " WHERE id = '" + row.id + "'";

            cfg.inn.applySQL(sql);

            //子場次
            sql = @"
                UPDATE t1 SET
	                  t1.in_show_site = t2.in_show_site
	                , t1.in_show_serial = t2.in_show_serial * 100 + t1.in_sub_id
	                , t1.in_site_move = t2.in_site_move
	                , t1.in_site_time = t2.in_site_time
                FROM 
                    IN_MEETING_PEVENT t1 WITH(NOLOCK) 
                INNER JOIN
                    IN_MEETING_PEVENT t2 WITH(NOLOCK) 
                    ON t2.id = t1.in_parent
                WHERE
                    t1.in_parent = '{#event_id}'
                    AND ISNULL(t2.in_tree_no, 0) > 0
            ";

            sql = sql.Replace("{#event_id}", row.id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            cfg.inn.applySQL(sql);
        }

        private void RollbackSiteMove(TConfig cfg, TEvtShow row)
        {
            var sql = "UPDATE IN_MEETING_PEVENT SET"
                + "  in_show_site = '" + row.new_show_site + "'"
                + ", in_show_serial = '" + row.new_show_serial + "'"
                + ", in_site_move = NULL"
                + ", in_site_time = NULL"
                + " WHERE id = '" + row.id + "'";

            cfg.inn.applySQL(sql);

            //子場次
            sql = @"
                UPDATE t1 SET
	                  t1.in_show_site = t2.in_show_site
	                , t1.in_show_serial = t2.in_show_serial * 100 + t1.in_sub_id
	                , t1.in_site_move = NULL
	                , t1.in_site_time = NULL
                FROM 
                    IN_MEETING_PEVENT t1 WITH(NOLOCK) 
                INNER JOIN
                    IN_MEETING_PEVENT t2 WITH(NOLOCK) 
                    ON t2.id = t1.in_parent
                WHERE
                    t1.in_parent = '{#event_id}'
                    AND ISNULL(t2.in_tree_no, 0) > 0
            ";

            sql = sql.Replace("{#event_id}", row.id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            cfg.inn.applySQL(sql);
        }

        private List<TEvtShow> GetEvtShowList(TConfig cfg, string json)
        {
            try
            {
                return Newtonsoft.Json.JsonConvert.DeserializeObject<List<TEvtShow>>(json);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private class TEvtShow
        {
            public string id { get; set; }
            public string new_show_site { get; set; }
            public string new_show_serial { get; set; }
            public string old_site_code { get; set; }
            public string old_show_serial { get; set; }
        }

        private void EditTime(TConfig cfg, Item itmReturn)
        {
            string old_time = itmReturn.getProperty("old_time", "");
            string new_time = itmReturn.getProperty("new_time", "");

            //string sql_qry = "SELECT * FROM IN_MEETING_PEVENT WITH(NOLOCK) WHERE id = '" + cfg.event_id + "'";

            string sql_upd_old = "UPDATE IN_MEETING_PEVENT SET in_win_local_time = '" + old_time + "' WHERE id = '" + cfg.event_id + "'";
            string sql_upd_new = "UPDATE IN_MEETING_PEVENT SET in_win_local_time = '" + new_time + "' WHERE id = '" + cfg.event_id + "'";

            // cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "變更競賽時間 old: " + sql_upd_old);
            // cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "變更競賽時間 new: " + sql_upd_new);

            Item itmUpd = cfg.inn.applySQL(sql_upd_new);

            if (itmUpd.isError())
            {
                throw new Exception("時間更新失敗");
            }
        }

        /// <summary>
        /// 場次資訊變更
        /// </summary>
        private void ChangeInfo(TConfig cfg, Item itmReturn)
        {
            string sql = "";
            Item itmResult = null;

            string info = itmReturn.getProperty("info", "");
            string func = itmReturn.getProperty("func", "");
            string text = itmReturn.getProperty("text", "");
            string value = itmReturn.getProperty("value", "");

            switch (func)
            {
                case "site":
                    Item itmSite = cfg.inn.applySQL("SELECT TOP 1 * FROM IN_MEETING_SITE WITH(NOLOCK) WHERE in_meeting = '" + cfg.meeting_id + "' AND in_code = '" + value + "'");
                    if (itmSite.isError() || itmSite.getResult() == "")
                    {
                        throw new Exception("查無場地資料: " + text);
                    }

                    SetSiteFromItem(cfg, cfg.event_id, itmSite);
                    break;

                case "period":
                    sql = "UPDATE IN_MEETING_PEVENT SET in_period = '" + value + "' WHERE id = '" + cfg.event_id + "'";
                    itmResult = cfg.inn.applySQL(sql);
                    if (itmResult.isError())
                    {
                        throw new Exception(info + "失敗 sql: " + sql);
                    }
                    break;

                case "medal":
                    sql = "UPDATE IN_MEETING_PEVENT SET in_medal = '" + value + "' WHERE id = '" + cfg.event_id + "'";
                    itmResult = cfg.inn.applySQL(sql);
                    if (itmResult.isError())
                    {
                        throw new Exception(info + "失敗 sql: " + sql);
                    }
                    break;

                case "tno":
                    SetTreeNo(cfg, cfg.event_id, value);
                    break;

                case "tno_all":
                    SetAllTreeNo(cfg, value);
                    break;
            }
        }

        /// <summary>
        /// 變更全部場次
        /// </summary>
        private void SetAllTreeNo(TConfig cfg, string value)
        {
            var list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<TEvt>>(value);
            if (list == null || list.Count == 0)
            {
                throw new Exception("場次資料不可為空值");
            }

            foreach (var row in list)
            {
                if (row.tno != row.ono)
                {
                    SetTreeNo(cfg, row.eid, row.tno);
                }
            }
        }


        /// <summary>
        /// 設定場次
        /// </summary>
        private void SetTreeNo(TConfig cfg, string event_id, string tree_no)
        {
            if (tree_no != "" && tree_no != "NA")
            {
                int temp_no = 0;
                if (!Int32.TryParse(tree_no, out temp_no))
                {
                    throw new Exception("場次編號必須為整數");
                }
            }

            var sql = @"
                SELECT
	                t1.id
	                , t1.in_tree_no
	                , t2.in_code
	                , t2.in_code_en
                FROM
	                IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_SITE t2 WITH(NOLOCK)
	                ON t2.id = t1.in_site
                WHERE
	                t1.id = '{#event_id}'
            ";

            sql = sql.Replace("{#event_id}", event_id);

            var itmEvent = cfg.inn.applySQL(sql);
            if (itmEvent.isError() || itmEvent.getResult() == "")
            {
                throw new Exception("查無場次資料");
            }

            var in_show_site = itmEvent.getProperty("in_code", "");
            var in_show_serial = tree_no;

            sql = "UPDATE IN_MEETING_PEVENT SET"
                + "  in_tree_no = '" + tree_no + "'"
                + ", in_show_site = '" + in_show_site + "'"
                + ", in_show_serial = '" + in_show_serial + "'"
                + " WHERE id = '" + event_id + "'";

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            cfg.inn.applySQL(sql);

            //子場次全部重編
            ResetSubTreeNo(cfg, event_id);
        }

        /// <summary>
        /// 設定場地
        /// </summary>
        private void SetSiteId(TConfig cfg, Item itmReturn)
        {
            string in_site_id = itmReturn.getProperty("in_site_id", "");
            SetSiteId(cfg, cfg.event_id, in_site_id);
        }

        /// <summary>
        /// 設定場地
        /// </summary>
        private void SetSiteFromItem(TConfig cfg, string event_id, Item itmSite)
        {
            var site_id = itmSite.getProperty("id", "");
            var site_code = itmSite.getProperty("in_code", "");

            string sql = "UPDATE IN_MEETING_PEVENT SET"
                + "  in_site = '" + site_id + "'"
                + ", in_site_code = '" + site_code + "'"
                + ", in_show_site = '" + site_code + "'"
                + " WHERE id = '" + event_id + "'";

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item itmResult = cfg.inn.applySQL(sql);

            if (itmResult.isError())
            {
                throw new Exception("更新場地失敗 sql: " + sql);
            }

            //刷新子場次所屬場地
            ResetSubSite(cfg, event_id);
        }

        /// <summary>
        /// 設定場地
        /// </summary>
        private void SetSiteItem(TConfig cfg, Item itmReturn)
        {
            string tree_id = itmReturn.getProperty("tree_id", "");
            string site_id = itmReturn.getProperty("site_id", "");

            Item itmEvent = cfg.inn.applySQL("SELECT id FROM IN_MEETING_PEVENT WITH(NOLOCK) WHERE source_id = '" + cfg.program_id + "' AND in_tree_id = '" + tree_id + "'");

            if (itmEvent.isError() || itmEvent.getItemCount() != 1)
            {
                throw new Exception("場次資料錯誤");
            }

            string event_id = itmEvent.getProperty("id", "");

            SetSiteItem(cfg, event_id, site_id);
        }

        /// <summary>
        /// 設定場地
        /// </summary>
        private void SetSiteItem(TConfig cfg, string event_id, string site_id)
        {
            string sql = "UPDATE IN_MEETING_PEVENT SET in_site = '" + site_id + "' WHERE id = '" + event_id + "'";

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item itmResult = cfg.inn.applySQL(sql);

            if (itmResult.isError())
            {
                throw new Exception("更新場地失敗 sql: " + sql);
            }

            //刷新子場次所屬場地
            ResetSubSite(cfg, event_id);
        }

        //子場次全部重編
        private void ResetSubTreeNo(TConfig cfg, string event_id)
        {
            string sql = "";
            Item itmSQL = null;

            sql = @"
                UPDATE t1 SET
                    t1.in_tree_no = t2.in_tree_no * 100 + t1.in_sub_id
	                , t1.in_period = t2.in_period
	                , t1.in_medal = t2.in_medal
	                , t1.in_show_site = t2.in_show_site
	                , t1.in_show_serial = t2.in_show_serial * 100 + t1.in_sub_id
                FROM 
                    IN_MEETING_PEVENT t1 WITH(NOLOCK) 
                INNER JOIN
                    IN_MEETING_PEVENT t2 WITH(NOLOCK) 
                    ON t2.id = t1.in_parent
                WHERE
                    t1.in_parent = '{#event_id}'
                    AND ISNULL(t2.in_tree_no, 0) > 0
            ";

            sql = sql.Replace("{#event_id}", event_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            itmSQL = cfg.inn.applySQL(sql);
        }

        //刷新子場次所屬場地(必須與主場次一致)
        private void ResetSubSite(TConfig cfg, string event_id)
        {
            string sql = "";
            Item itmSQL = null;

            sql = @"
                UPDATE t1 SET
                    t1.in_site = t2.in_site
                FROM 
                    IN_MEETING_PEVENT t1 WITH(NOLOCK) 
                INNER JOIN
                    IN_MEETING_PEVENT t2 WITH(NOLOCK) 
                    ON t2.id = t1.in_parent
                WHERE
                    t1.in_parent = '{#event_id}'
            ";

            sql = sql.Replace("{#event_id}", event_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            itmSQL = cfg.inn.applySQL(sql);
        }

        //通報 WebSocket
        private void CallSportHub(TConfig cfg)
        {
            try
            {
                var rootUrl = GetVariable(cfg);
                var rsrcUrl = rootUrl + "/Judo/ScheduleMoveSite?token=0225585561";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(rsrcUrl);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                StreamReader tReader = new StreamReader(response.GetResponseStream());

                string rst = tReader.ReadToEnd();
                if (!string.IsNullOrWhiteSpace(rst))
                {
                    var result = rst.Replace("\"", "");
                }
            }
            catch (Exception ex)
            {

            }
        }

        private string GetVariable(TConfig cfg)
        {
            Item item = cfg.inn.applySQL("SELECT TOP 1 in_value FROM IN_VARIABLE WITH(NOLOCK) WHERE in_name = 'api_url'");
            if (item.isError() || item.getResult() == "")
            {
                return "";
            }
            else
            {
                return item.getProperty("in_value", "").TrimEnd('/');
            }
        }

        /// <summary>
        /// 設定場地編號(TKD自動配號使用)
        /// </summary>
        private void SetSiteId(TConfig cfg, string event_id, string in_site_id)
        {
            string sql = "";
            Item itmSQL = null;

            sql = "UPDATE IN_MEETING_PEVENT SET in_site_id = '" + in_site_id + "', in_site_allocate = '1' WHERE id = '" + event_id + "'";
            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            itmSQL = cfg.inn.applySQL(sql);

            if (itmSQL.isError())
            {
                throw new Exception("更新場地編號失敗 sql: " + sql);
            }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }

            public string meeting_id { get; set; }
            public string program_id { get; set; }
            public string event_id { get; set; }
            public string scene { get; set; }
        }

        private class TEvt
        {
            public string eid { get; set; }
            public string ono { get; set; }
            public string tno { get; set; }
        }
    }
}