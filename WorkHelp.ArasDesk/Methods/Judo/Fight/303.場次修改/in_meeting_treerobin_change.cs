﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Judo.Fight
{
    public class in_meeting_treerobin_change : Item
    {
        public in_meeting_treerobin_change(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 單淘改為循環賽
                日誌: 
                    - 2023-05-24: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "in_meeting_treerobin_change";

            var itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                meeting_id = itmR.getProperty("meeting_id", ""),
                program_id = itmR.getProperty("program_id", ""),
                robin_count = itmR.getProperty("robin_count", ""),
                scene = itmR.getProperty("scene", ""),
            };

            switch (cfg.scene)
            {
                case "change":
                    ChangeTree(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void ChangeTree(TConfig cfg, Item itmReturn)
        {
            var list = GetCurrentEventItems(cfg, itmReturn);
            var cnt = GetRobinCount(cfg, itmReturn);
            if (cnt != list.Count)
            {
                throw new Exception("有編號的場次必須為 " + cnt + "場");
            }

            //變更組別資料
            ChangeTeamCount(cfg, itmReturn);

            switch(cfg.robin_count)
            {
                case "3": ChangeRobinEvent_3(cfg, list); break;
            }

        }

        private void ChangeRobinEvent_3(TConfig cfg, List<TEvt> list)
        {
            //變更場次暨明細資料
            for (int i = 0; i < list.Count; i++)
            {
                var row = list[i];
                switch (i)
                {
                    case 0: ChangeRobinEvent_3_1(cfg, row.evt_id); break;
                    case 1: ChangeRobinEvent_3_2(cfg, row.evt_id); break;
                    case 2: ChangeRobinEvent_3_3(cfg, row.evt_id); break;
                }
            }
        }

        //變更場次暨明細資料(三人)
        private void ChangeRobinEvent_3_1(TConfig cfg, string evt_id)
        {
            var sql = @"
                UPDATE IN_MEETING_PEVENT SET
	                KEYED_NAME = 'M101'
	                , IN_TREE_ID = 'M101'
	                , IN_TREE_RANK = NULL
	                , IN_TREE_RANK_NS = NULL
	                , IN_TREE_RANK_NSS = NULL
	                , IN_ROUND = '1'
	                , IN_ROUND_CODE = 3
	                , IN_ROUND_ID = 1
	                , IN_SIGN_CODE = 3
	                , IN_SIGN_NO = 1
	                , IN_TREE_ALIAS = NULL
	                , IN_BYPASS_STATUS = NULL
	                , IN_NEXT_WIN = NULL
	                , IN_NEXT_LOSE = NULL
	                , IN_NEXT_FOOT_WIN = NULL
	                , IN_NEXT_FOOT_LOSE = NULL
	                , IN_TREE_SNO = 1
	                , IN_DETAIL_NS = '2,1'
	                , IN_ROBIN_KEY = '1,2'
	                , IN_FIGHT_ID = 'RBN3-01'
                WHERE ID = '{#evt_id}'
            ";

            sql = sql.Replace("{#evt_id}", evt_id);
            cfg.inn.applySQL(sql);


            sql = @"
                UPDATE IN_MEETING_PEVENT_DETAIL SET 
	                IN_SIGN_NO = 2, IN_TARGET_NO = 1
                WHERE SOURCE_ID = '{#evt_id}' AND IN_SIGN_FOOT = 1
            ";

            sql = sql.Replace("{#evt_id}", evt_id);
            cfg.inn.applySQL(sql);


            sql = @"
                UPDATE IN_MEETING_PEVENT_DETAIL SET 
	                IN_SIGN_NO = 1, IN_TARGET_NO = 2
                WHERE SOURCE_ID = '{#evt_id}' AND IN_SIGN_FOOT = 2
            ";

            sql = sql.Replace("{#evt_id}", evt_id);
            cfg.inn.applySQL(sql);
        }

        //變更場次暨明細資料(三人)
        private void ChangeRobinEvent_3_2(TConfig cfg, string evt_id)
        {
            var sql = @"
                UPDATE IN_MEETING_PEVENT SET
	                KEYED_NAME = 'M103'
	                , IN_TREE_ID = 'M103'
	                , IN_TREE_RANK = NULL
	                , IN_TREE_RANK_NS = NULL
	                , IN_TREE_RANK_NSS = NULL
	                , IN_ROUND = '1'
	                , IN_ROUND_CODE = 3
	                , IN_ROUND_ID = 3
	                , IN_SIGN_CODE = 3
	                , IN_SIGN_NO = 3
	                , IN_TREE_ALIAS = NULL
	                , IN_BYPASS_STATUS = NULL
	                , IN_NEXT_WIN = NULL
	                , IN_NEXT_LOSE = NULL
	                , IN_NEXT_FOOT_WIN = NULL
	                , IN_NEXT_FOOT_LOSE = NULL
	                , IN_TREE_SNO = 2
	                , IN_DETAIL_NS = '3,2'
	                , IN_ROBIN_KEY = '2,3'
	                , IN_FIGHT_ID = 'RBN3-03'
                WHERE ID = '{#evt_id}'
            ";

            sql = sql.Replace("{#evt_id}", evt_id);
            cfg.inn.applySQL(sql);


            sql = @"
                UPDATE IN_MEETING_PEVENT_DETAIL SET 
	                IN_SIGN_NO = 3, IN_TARGET_NO = 2
                WHERE SOURCE_ID = '{#evt_id}' AND IN_SIGN_FOOT = 1
            ";

            sql = sql.Replace("{#evt_id}", evt_id);
            cfg.inn.applySQL(sql);


            sql = @"
                UPDATE IN_MEETING_PEVENT_DETAIL SET 
	                IN_SIGN_NO = 2, IN_TARGET_NO = 3
                WHERE SOURCE_ID = '{#evt_id}' AND IN_SIGN_FOOT = 2
            ";

            sql = sql.Replace("{#evt_id}", evt_id);
            cfg.inn.applySQL(sql);
        }

        //變更場次暨明細資料(三人)
        private void ChangeRobinEvent_3_3(TConfig cfg, string evt_id)
        {
            var sql = @"
                UPDATE IN_MEETING_PEVENT SET
	                KEYED_NAME = 'M102'
	                , IN_TREE_ID = 'M102'
	                , IN_TREE_RANK = NULL
	                , IN_TREE_RANK_NS = NULL
	                , IN_TREE_RANK_NSS = NULL
	                , IN_ROUND = '1'
	                , IN_ROUND_CODE = 3
	                , IN_ROUND_ID = 2
	                , IN_SIGN_CODE = 3
	                , IN_SIGN_NO = 2
	                , IN_TREE_ALIAS = NULL
	                , IN_BYPASS_STATUS = NULL
	                , IN_NEXT_WIN = NULL
	                , IN_NEXT_LOSE = NULL
	                , IN_NEXT_FOOT_WIN = NULL
	                , IN_NEXT_FOOT_LOSE = NULL
	                , IN_TREE_SNO = 3
	                , IN_DETAIL_NS = '1,3'
	                , IN_ROBIN_KEY = '1,3'
	                , IN_FIGHT_ID = 'RBN3-02'
                WHERE ID = '{#evt_id}'
            ";

            sql = sql.Replace("{#evt_id}", evt_id);
            cfg.inn.applySQL(sql);


            sql = @"
                UPDATE IN_MEETING_PEVENT_DETAIL SET 
	                IN_SIGN_NO = 1, IN_TARGET_NO = 3
                WHERE SOURCE_ID = '{#evt_id}' AND IN_SIGN_FOOT = 1
            ";

            sql = sql.Replace("{#evt_id}", evt_id);
            cfg.inn.applySQL(sql);


            sql = @"
                UPDATE IN_MEETING_PEVENT_DETAIL SET 
	                IN_SIGN_NO = 3, IN_TARGET_NO = 1
                WHERE SOURCE_ID = '{#evt_id}' AND IN_SIGN_FOOT = 2
            ";

            sql = sql.Replace("{#evt_id}", evt_id);
            cfg.inn.applySQL(sql);
        }


        //變更組別資料
        private void ChangeTeamCount(TConfig cfg, Item itmReturn)
        {
            var sql = @"
                UPDATE IN_MEETING_PROGRAM SET 
	                IN_BATTLE_TYPE = 'SingleRoundRobin'
	                , IN_TEAM_COUNT = {#cnt}
	                , IN_RANK_COUNT = {#cnt}
                WHERE ID = '{#program_id}'
            ";

            sql = sql.Replace("{#program_id}", cfg.program_id)
                .Replace("{#cnt}", cfg.robin_count);

            cfg.inn.applySQL(sql);
        }

        private List<TEvt> GetCurrentEventItems(TConfig cfg, Item itmReturn)
        {
            var sql = @"
                SELECT 
	                id
	                , in_tree_no
                FROM 
	                IN_MEETING_PEVENT WITH(NOLOCK) 
                WHERE 
	                source_id = '{#program_id}'
	                AND ISNULL(in_tree_no, 0) > 0
                ORDER BY
	                in_tree_no
            ";

            sql = sql.Replace("{#program_id}", cfg.program_id);

            var result = new List<TEvt>();
            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (var i = 0; i< count; i++)
            {
                var item = items.getItemByIndex(i);
                result.Add(new TEvt 
                {
                    evt_id = item.getProperty("id", ""),
                    Value = item
                });
            }
            return result;
        }

        private int GetRobinCount(TConfig cfg, Item itmReturn)
        {
            switch (cfg.robin_count)
            {
                case "1": return 1;
                case "2": return 3;
                case "3": return 3;
                case "4": return 6;
                case "5": return 10;
                default: return 0;
            }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }

            public string meeting_id { get; set; }
            public string program_id { get; set; }
            public string robin_count { get; set; }
            public string scene { get; set; }
        }

        private class TEvt
        {
            public string evt_id { get; set; }
            public Item Value { get; set; }
        }
    }
}
