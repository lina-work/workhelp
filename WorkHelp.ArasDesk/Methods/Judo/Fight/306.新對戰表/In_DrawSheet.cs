﻿using Aras.IOM;
using Aras.Server.Core;
using System;
using System.Web.WebSockets;

namespace WorkHelp.ArasDesk.Methods.Judo.Fight._306.新對戰表
{
    internal class In_DrawSheet : Item
    {
        public In_DrawSheet(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 新對戰表
                日誌: 
                    - 2025-02-12: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "In_DrawSheet";

            var itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                meeting_id = itmR.getProperty("meeting_id", ""),
                program_id = itmR.getProperty("program_id", ""),
                in_l1 = itmR.getProperty("in_l1", ""),
                in_l2 = itmR.getProperty("in_l2", ""),
                in_l3 = itmR.getProperty("in_l3", ""),
                scene = itmR.getProperty("scene", ""),
            };

            if (cfg.scene == "")
            {
                cfg.scene = "page";
            }

            switch (cfg.scene)
            {
                case "page":
                    Page(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void Page(TConfig cfg, Item itmReturn)
        {
            cfg.site_path = GetVariable(cfg, "site_path").TrimEnd('\\');
            cfg.template_path = cfg.site_path + @"\scripts\pages\templates";
            AppendMeeting(cfg, itmReturn);
            SetProgramId(cfg, itmReturn);
            AppendProgram(cfg, itmReturn);
            MatchTemplate(cfg, itmReturn);
            ReadTemplate(cfg, itmReturn);
        }

        private void MatchTemplate(TConfig cfg, Item itmReturn)
        {
            switch (cfg.in_battle_type)
            {
                case "Challenge":
                    cfg.in_battle_desc = "挑戰賽";
                    cfg.file_code = cfg.round_code.ToString().PadLeft(3, '0');
                    cfg.main_template = NewTemplate(cfg, "top-four", "top-four-" + cfg.file_code + ".html");
                    cfg.repechage_template = NewTemplate(cfg, "repechage", "repechage-" + cfg.file_code + ".html");
                    cfg.challenge_template = NewTemplate(cfg, "challenge", "challenge-003.html");
                    break;

                case "JudoTopFour":
                    cfg.in_battle_desc = "四柱復活賽";
                    cfg.file_code = cfg.round_code.ToString().PadLeft(3, '0');
                    cfg.main_template = NewTemplate(cfg, "top-four", "top-four-" + cfg.file_code + ".html");
                    cfg.repechage_template = NewTemplate(cfg, "repechage", "repechage-" + cfg.file_code + ".html");
                    cfg.challenge_template = NewTemplate(cfg, "", "");
                    break;

                case "TopTwo":
                    cfg.in_battle_desc = "單淘汰";
                    cfg.file_code = cfg.round_code.ToString().PadLeft(3, '0');
                    cfg.main_template = NewTemplate(cfg, "top-four", "top-four-" + cfg.file_code + ".html");
                    cfg.repechage_template = NewTemplate(cfg, "", "");
                    cfg.challenge_template = NewTemplate(cfg, "", "");
                    break;

                case "SingleRoundRobin":
                    cfg.in_battle_desc = "循環賽";
                    cfg.file_code = cfg.team_count.ToString().PadLeft(3, '0');
                    cfg.main_template = NewTemplate(cfg, "robin", "robin-" + cfg.file_code + ".html");
                    cfg.repechage_template = NewTemplate(cfg, "", "");
                    cfg.challenge_template = NewTemplate(cfg, "", "");
                    break;
            }
        }

        private TTemplate NewTemplate(TConfig cfg, string folder, string file)
        {
            var x = new TTemplate { folder = "", file = "", full = "", contents = "" };
            if (folder == "") return x;

            x.folder = folder;
            x.file = file;
            x.full = cfg.template_path + @"\" + folder + @"\" + file;

            if (System.IO.File.Exists(x.full))
            {
                lock (InnSport.Core.Utilities.TUtility.ArasFileLock)
                {
                    x.contents = System.IO.File.ReadAllText(x.full, System.Text.Encoding.UTF8);
                }
            }

            return x;
        }

        private void ReadTemplate(TConfig cfg, Item itmReturn)
        {
            itmReturn.setProperty("in_battle_desc", cfg.in_battle_desc);

            itmReturn.setProperty("inn_main_file", cfg.main_template.full);
            itmReturn.setProperty("inn_main_template", cfg.main_template.contents);

            itmReturn.setProperty("inn_repechage_file", cfg.repechage_template.full);
            itmReturn.setProperty("inn_repechage_template", cfg.repechage_template.contents);

            itmReturn.setProperty("inn_challenge_file", cfg.challenge_template.full);
            itmReturn.setProperty("inn_challenge_template", cfg.challenge_template.contents);
        }

        private void AppendProgram(TConfig cfg, Item itmReturn)
        {
            var sql = @"
                SELECT 
                    * 
                FROM 
                    IN_MEETING_PROGRAM WITH(NOLOCK) 
                WHERE 
                    id = '{#program_id}'
            ";

            sql = sql.Replace("{#program_id}", cfg.program_id);
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);
            cfg.itmProgram = cfg.inn.applySQL(sql);

            if (cfg.itmProgram.getResult() == "") cfg.itmProgram = cfg.inn.newItem();

            cfg.in_battle_type = cfg.itmProgram.getProperty("in_battle_type", "");
            cfg.team_count = GetInt(cfg.itmProgram.getProperty("in_team_count", "0"));
            cfg.round_code = GetInt(cfg.itmProgram.getProperty("in_round_code", "0"));

            itmReturn.setProperty("pg_battle_type", cfg.itmProgram.getProperty("in_battle_type", ""));
            itmReturn.setProperty("in_l1", cfg.itmProgram.getProperty("in_l1", ""));
            itmReturn.setProperty("in_l2", cfg.itmProgram.getProperty("in_l2", ""));
            itmReturn.setProperty("in_l3", cfg.itmProgram.getProperty("in_l3", ""));
            itmReturn.setProperty("in_fight_day", cfg.itmProgram.getProperty("in_fight_day", ""));
        }

        private void AppendMeeting(TConfig cfg, Item itmReturn)
        {
            var sql = @"
                SELECT 
                    id
                    , in_title 
                FROM 
                    IN_MEETING WITH(NOLOCK) 
                WHERE 
                    id = '{#meeting_id}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            cfg.itmMeeting = cfg.inn.applySQL(sql);

            if (cfg.itmMeeting.getResult() == "") cfg.itmMeeting = cfg.inn.newItem();

            itmReturn.setProperty("mt_title", cfg.itmMeeting.getProperty("in_title", ""));
        }

        private void SetProgramId(TConfig cfg, Item itmReturn)
        {
            if (cfg.program_id != "")
            {
                var sql = "SELECT TOP 1 id FROM IN_MEETING_PROGRAM WITH(NOLOCK)"
                    + " WHERE in_meeting = '" + cfg.meeting_id + "'"
                    + " AND id = '" + cfg.program_id + "'";

                var item = cfg.inn.applySQL(sql);
                if (item.getResult() != "")
                {
                    return;
                }
                else
                {
                    cfg.program_id = "";
                }
            }

            if (cfg.in_l1 != "" && cfg.in_l2 != "" && cfg.in_l3 != "")
            {
                var sql1 = "SELECT TOP 1 id FROM IN_MEETING_PROGRAM WITH(NOLOCK)"
                    + " WHERE in_meeting = '" + cfg.meeting_id + "'"
                    + " AND in_l1 = N'" + cfg.in_l1 + "'"
                    + " AND in_l2 = N'" + cfg.in_l2 + "'"
                    + " AND in_l3 = N'" + cfg.in_l3 + "'";

                var item = cfg.inn.applySQL(sql1);

                if (item.getResult() != "")
                {
                    cfg.program_id = item.getProperty("id", "");
                }
            }
            else
            {
                var sql2 = "SELECT TOP 1 id FROM IN_MEETING_PROGRAM WITH(NOLOCK)"
                    + " WHERE in_meeting = '" + cfg.meeting_id + "'"
                    + " ORDER BY in_sort_order";

                var item = cfg.inn.applySQL(sql2);

                if (item.getResult() != "")
                {
                    cfg.program_id = item.getProperty("id", "");
                }
            }

            itmReturn.setProperty("program_id", cfg.program_id);
        }

        private string GetVariable(TConfig cfg, string in_name)
        {
            var sql = "SELECT TOP 1 in_value FROM IN_VARIABLE WITH(NOLOCK)"
                + " WHERE in_name = '" + in_name + "'";
            var item = cfg.inn.applySQL(sql);
            if (item.getResult() != "") return item.getProperty("in_value", "");
            return "";
        }

        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }

            public string meeting_id { get; set; }
            public string program_id { get; set; }
            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }
            public string scene { get; set; }

            public Item itmMeeting { get; set; }
            public Item itmProgram { get; set; }

            public string in_battle_type { get; set; }
            public int team_count { get; set; }
            public int round_code { get; set; }
            public string file_code { get; set; }

            //IN_VARIABLE
            public string site_path { get; set; }
            public string template_path { get; set; }

            public string in_battle_desc { get; set; }

            public TTemplate main_template { get; set; }
            public TTemplate repechage_template { get; set; }
            public TTemplate challenge_template { get; set; }
        }

        private class TTemplate
        {
            public string folder { get; set; }
            public string file { get; set; }
            public string full { get; set; }
            public string contents { get; set; }
        }

        private int GetInt(string value, int def = 0)
        {
            int result = def;
            int.TryParse(value, out result);
            return result;
        }
    }
}