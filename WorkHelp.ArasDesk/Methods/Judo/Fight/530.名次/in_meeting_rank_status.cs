﻿using Aras.IOM;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace WorkHelp.ArasDesk.Methods.Judo.Fight
{
    public class in_meeting_rank_status : Item
    {
        public in_meeting_rank_status(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 賽程進度設定
                日誌: 
                    - 2022-11-15: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]" + "in_meeting_rank_status";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                strIdentityId = inn.getUserAliases(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                program_id = itmR.getProperty("program_id", ""),
                in_fight_day = itmR.getProperty("in_fight_day", ""),
                scene = itmR.getProperty("scene", ""),
            };

            switch (cfg.scene)
            {
                case "site_page":
                    cfg.is_list_mode = false;
                    TabPage(cfg, itmR);
                    break;

                case "list_page":
                    cfg.is_list_mode = true;
                    TabPage(cfg, itmR);
                    break;

                case "medal_page":
                    MedalPage(cfg, itmR);
                    break;

                case "change_status":
                    ChangeStatus(cfg, itmR);
                    break;

                case "modal":
                    Modal(cfg, itmR);
                    break;

                case "dns":
                    PlayerDNS(cfg, itmR);
                    break;
            }

            return itmR;
        }

        //未到
        private void PlayerDNS(TConfig cfg, Item itmReturn)
        {
            string team_id = itmReturn.getProperty("team_id", "");
            string name = itmReturn.getProperty("name", "");
            string is_dns = itmReturn.getProperty("is_dns", "");

            var itmTeam = cfg.inn.applySQL("SELECT id, in_medal_dns FROM IN_MEETING_PTEAM WITH(NOLOCK) WHERE id = '" + team_id + "'");
            if (itmTeam.isError() || itmTeam.getResult() == "")
            {
                throw new Exception("查無選手/隊伍資料");
            }

            string old_medal_dns = itmTeam.getProperty("in_medal_dns", "");
            
            var dns_list = GetDNSList(cfg, old_medal_dns);

            if (is_dns == "1")
            {
                if (dns_list.Count > 0 && dns_list.Contains(name))
                {
                    dns_list.Remove(name);
                }
            }
            else
            {
                if (dns_list.Count <= 0)
                {
                    dns_list.Add(name);
                }
                else if (!dns_list.Contains(name))
                {
                    dns_list.Add(name);
                }
            }

            string new_medal_dns = string.Join(",", dns_list);

            string sql = "UPDATE IN_MEETING_PTEAM SET"
                + " in_medal_dns = N'" + new_medal_dns + "'"
                + " WHERE id = '" + team_id + "'"
                ;

            Item itmUpd = cfg.inn.applySQL(sql);
            if (itmUpd.isError())
            {
                throw new Exception("更新失敗");
            }
        }

        private List<string> GetDNSList(TConfig cfg, string value)
        {
            if (value == "") return new List<string>();

            var arr = value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            if (arr == null || arr.Length == 0) return new List<string>();

            return arr.ToList();
        }

        //名單跳窗
        private void Modal(TConfig cfg, Item itmReturn)
        {
            AppendMeetingInfo(cfg, itmReturn);

            var items = RankItems(cfg);
            var count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                item.setType("inn_team");
                item.setProperty("inn_rank", RankInfo(cfg, item));
                item.setProperty("inn_name", Players(cfg, item));
                itmReturn.addRelationship(item);
            }
        }

        private string Players(TConfig cfg, Item item)
        {
            string result = "";

            string in_l1 = item.getProperty("in_l1", "");
            string org = item.getProperty("map_short_org", "");
            string in_names = item.getProperty("in_names", "");
            string in_team = item.getProperty("in_team", "");
            string in_medal_dns = item.getProperty("in_medal_dns", "");
            var dns_list = GetDNSList(cfg, in_medal_dns);

            result = "<p style='color: blue'>" + org + "</p>";

            if (in_l1 == "團體組")
            {
                var key = "(" + in_team + "隊)";

                var players = in_names.Replace(key, "").Trim()
                    .Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                if (players != null && players.Length != 0)
                {
                    foreach (var player in players)
                    {
                        result += GetNameInfo(cfg, item, player, dns_list);
                    }
                }
            }
            else
            {
                result += GetNameInfo(cfg, item, in_names, dns_list);
            }

            return result;
        }

        private string GetNameInfo(TConfig cfg, Item item, string name, List<string> dns_list)
        {
            string team_id = item.getProperty("team_id", "");

            string css = dns_list.Count > 0 && dns_list.Contains(name)
                ? "btn-danger"
                : "btn-default";

            string func = " &nbsp; <button class='btn btn-sm btn-default' data-id='" + team_id + "' data-nm='" + name + "' onclick='MedalDNS_Click(this)'> 未到 </button>";
            string line = "<p class='form-inline'> - " + name + func + " </p>";
            return line;
        }

        private string RankInfo(TConfig cfg, Item item)
        {
            string in_final_rank = item.getProperty("in_final_rank", "");
            return "第 " + in_final_rank + " 名"; ;
        }

        //頒獎頁面
        private void MedalPage(TConfig cfg, Item itmReturn)
        {
            AppendMeetingInfo(cfg, itmReturn);
            AppendFightDayMenu(cfg, itmReturn);

            var itmPrograms = MedalItems(cfg);
            itmReturn.setProperty("inn_table", MedalTable(cfg, itmPrograms).ToString());
        }

        private StringBuilder MedalTable(TConfig cfg, Item items)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(TabTable(cfg, "data_table"));
            builder.Append("<thead>");
            builder.Append("  <tr>");
            builder.Append("    <th class='text-center' data-width='50%'>量級</th>");
            builder.Append("    <th class='text-center' data-width='25%'>名單</th>");
            builder.Append("    <th class='text-center' data-width='25%'>功能</th>");
            builder.Append("  </tr>");
            builder.Append("</thead>");

            builder.Append("<tbody>");
            var count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var id = item.getProperty("id", "");

                var row = new TProgram
                {
                    id = id,
                    value = item,
                };

                var name = ProgramInfo(cfg, row);

                builder.Append("<tr data-id='" + row.id + "' data-nm='" + name + "'>");
                builder.Append("    <td class='text-left'>" + ProgramLink(cfg, id, name) + "</th>");
                builder.Append("    <td class='text-center'><button class='btn btn-primary' onclick='RankList_Click(this)'>名　單</th>");
                builder.Append("    <td class='text-center'><button class='btn btn-danger' onclick='Awarded_Click(this)'>已頒獎</th>");
                builder.Append("</tr>");
            }
            builder.Append("</tbody>");

            builder.Append("</table>");
            return builder;
        }

        private void ChangeStatus(TConfig cfg, Item itmReturn)
        {
            var now = DateTime.Now;

            string program_id = itmReturn.getProperty("program_id", "");
            string prop = itmReturn.getProperty("prop", "");

            string value = itmReturn.getProperty("value", "");
            string value_status = value;
            string value_time = now.AddHours(-8).ToString("yyyy-MM-dd HH:mm:ss");
            var show_time = "(" + now.ToString("HH:mm") + ")";

            string prop_status = "in_" + prop + "_status";
            string prop_time = "in_" + prop + "_time";

            if (value_status == "")
            {
                value_status = "NULL";
                value_time = "NULL";
                show_time = "";
            }
            else
            {
                value_status = "N'" + value_status + "'";
                value_time = "'" + value_time + "'";
            }

            string sql = "UPDATE IN_MEETING_PROGRAM SET"
                + "  " + prop_status + " = " + value_status
                + ", " + prop_time + " = " + value_time
                + " WHERE id = '" + program_id + "'"
                ;

            cfg.inn.applySQL(sql);

            if (value == "已印製")
            {
                sql = "UPDATE IN_MEETING_PROGRAM SET"
                    + "  in_medal_status = '可頒獎'"
                    + " WHERE id = '" + program_id + "' AND ISNULL(in_medal_status, '') = ''"
                    ;

                cfg.inn.applySQL(sql);
            }

            itmReturn.setProperty("inn_status", value);
            itmReturn.setProperty("inn_time", show_time);
        }

        private void AppendMeetingInfo(TConfig cfg, Item itmReturn)
        {
            Item itmMeeting = cfg.inn.applySQL("SELECT in_title FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            itmReturn.setProperty("in_title", itmMeeting.getProperty("in_title", ""));
        }


        //設定頁面
        private void TabPage(TConfig cfg, Item itmReturn)
        {
            AppendMeetingInfo(cfg, itmReturn);
            AppendFightDayMenu(cfg, itmReturn);

            var itmPrograms = FightProgramItems(cfg);

            var itmRank12Events = FightRank12EventItems(cfg);
            var itmRank34Events = FightRank34EventItems(cfg);

            var rnk12_evt_map = GetEventMap(cfg, itmRank12Events);
            var rnk34_evt_map = GetEventMap(cfg, itmRank34Events);
            var rows = GetPrograms(cfg, itmPrograms, rnk12_evt_map, rnk34_evt_map);

            var page = MapPageModel(cfg, rows);

            var contens = new StringBuilder();
            contens.Append(SiteTabHead(cfg, page));
            contens.Append(SiteTabBody(cfg, page));
            itmReturn.setProperty("inn_tabs", contens.ToString());
        }

        private StringBuilder SiteTabHead(TConfig cfg, TPage page)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("<ul class='nav nav-pills' style='justify-content: start;' id='pills-tab' role='tablist'>");
            foreach (var tab in page.tabs)
            {
                builder.Append("<li id='" + tab.li_id + "' class='nav-item " + tab.active1 + "' >");
                builder.Append(TabAnchor(cfg, tab));
                builder.Append("</li>");
            }
            builder.Append("</ul>");
            return builder;
        }

        private StringBuilder SiteTabBody(TConfig cfg, TPage page)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("<div class='tab-content' id='pills-tabContent' style='margin-top: -9px'>");
            foreach (var tab in page.tabs)
            {
                builder.Append("  <div class='tab-pane " + tab.active2 + "' id='" + tab.content_id + "' role='tabpanel'>");
                builder.Append("    <div class='showsheet_ clearfix'>");
                builder.Append("      <div id='" + tab.folder_id + "'>");
                builder.Append(SiteTabTable(cfg, page, tab));
                builder.Append("      </div>");
                builder.Append("    </div>");
                builder.Append("  </div>");
            }
            builder.Append("</div>");
            return builder;
        }

        private StringBuilder SiteTabTable(TConfig cfg, TPage page, TTab tab)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(TabTable(cfg, tab.table_id));
            builder.Append("<thead>");
            builder.Append("  <tr>");
            builder.Append("    <th class='text-center' data-width='3%'>No</th>");
            builder.Append("    <th class='text-center' data-width='18%'>量級</th>");
            builder.Append("    <th class='text-center' data-width='14%'>冠亞軍</th>");
            builder.Append("    <th class='text-center' data-width='14%'>三四名</th>");
            builder.Append("    <th class='text-center' data-width='18%'>競賽組確認</th>");
            builder.Append("    <th class='text-center' data-width='18%'>獎狀狀態</th>");
            builder.Append("    <th class='text-center' data-width='18%'>頒獎狀態</th>");
            builder.Append("    <th class='text-center' data-width='6%'>功能</th>");
            builder.Append("  </tr>");
            builder.Append("</thead>");

            builder.Append("<tbody>");
            int no = 0;
            foreach (var row in tab.rows)
            {
                no++;
                var nm = ProgramInfo(cfg, row);
                builder.Append("<tr data-id='" + row.id + "' data-nm='" + nm + "'>");
                builder.Append("    <td class='text-center'>" + no + "</th>");
                builder.Append("    <td class='text-left'>" + ProgramLink(cfg, row.id, nm) + "</th>");
                builder.Append("    <td class='text-center'>" + EventInfo(cfg, tab, row, row.rk12_events, 12) + "</th>");
                builder.Append("    <td class='text-center'>" + EventInfo(cfg, tab, row, row.rk34_events, 34) + "</th>");
                builder.Append("    <td class='text-center'>" + DirectorInfo(cfg, row) + "</th>");
                builder.Append("    <td class='text-center'>" + CertificateInfo(cfg, row) + "</th>");
                builder.Append("    <td class='text-center'>" + MedalInfo(cfg, row) + "</th>");
                builder.Append("    <td class='text-center'>" + RankFunc(cfg, row) + "</th>");
                builder.Append("</tr>");
            }
            builder.Append("</tbody>");

            builder.Append("</table>");
            return builder;
        }

        private string ProgramLink(TConfig cfg, string id, string name)
        {
            string url = "c.aspx?page=in_competition_preview.html"
                + "&method=in_meeting_program_preview"
                + "&meeting_id=" + cfg.meeting_id
                + "&program_id=" + id
                ;
            return "<a target='_blank' href='" + url + "'>" + name + "</a>";
        }

        private string RankFunc(TConfig cfg, TProgram row)
        {
            return "<button class='btn btn-primary' onclick='RankList_Click(this)'>名　單</th>";
        }

        private string MedalInfo(TConfig cfg, TProgram row)
        {
            var itmProgram = row.value;
            string value = itmProgram.getProperty("in_medal_status", "");
            string time = itmProgram.getProperty("in_medal_time", "");
            string prop = "medal";

            if (time != "") time = "(" + DtmStr(time) + ")";

            return "<div class='col-md-8' style='padding-right: 0px;'>"
                + " <select class='form-control inn_select' data-status='" + value + "' data-prop='" + prop + "' onchange='StatusChange(this)'>"
                + "   <option value=''>未確認</option>"
                + "   <option value='可頒獎'>可頒獎</option>"
                + "   <option value='不頒獎'>不頒獎</option>"
                + "   <option value='已頒獎'>已頒獎</option>"
                + " </select>"
                + "</div>"
                + "<div class='col-md-2'>"
                + "  <label class='inn_time'>"
                + time
                + "  </label>"
                + "</div>";
        }

        private string CertificateInfo(TConfig cfg, TProgram row)
        {
            var itmProgram = row.value;
            string value = itmProgram.getProperty("in_certificate_status", "");
            string time = itmProgram.getProperty("in_certificate_time", "");
            string prop = "certificate";

            if (time != "") time = "(" + DtmStr(time) + ")";

            return "<div class='col-md-8' style='padding-right: 0px;'>"
                + " <select class='form-control inn_select' data-status='" + value + "' data-prop='" + prop + "' onchange='StatusChange(this)'>"
                + "   <option value=''>未確認</option>"
                + "   <option value='印製中'>印製中</option>"
                + "   <option value='已印製'>已印製</option>"
                + "   <option value='異常'>異常</option>"
                + " </select>"
                + "</div>"
                + "<div class='col-md-2'>"
                + "  <label class='inn_time'>"
                + time
                + "  </label>"
                + "</div>";
        }

        private string DirectorInfo(TConfig cfg, TProgram row)
        {
            var itmProgram = row.value;
            string value = itmProgram.getProperty("in_director_status", "");
            string time = itmProgram.getProperty("in_director_time", "");
            string prop = "director";

            if (time != "") time = "(" + DtmStr(time) + ")";

            return "<div class='col-md-8' style='padding-right: 0px;'>"
                + " <select class='form-control inn_select' data-status='" + value + "' data-prop='" + prop + "' onchange='StatusChange(this)'>"
                + "   <option value=''>未確認</option>"
                + "   <option value='已確認'>已確認</option>"
                + "   <option value='異常'>異常</option>"
                + " </select>"
                + "</div>"
                + "<div class='col-md-2'>"
                + "  <label class='inn_time'>"
                + time
                + "  </label>"
                + "</div>";
        }

        private string EventInfo(TConfig cfg, TTab tab, TProgram row, List<Item> list, int type)
        {
            if (type == 34 && row.is_robin) return "循環賽";
            if (list == null || list.Count == 0) return "";

            var sb = new StringBuilder();
            foreach (var itmEvt in list)
            {
                string in_tree_no = itmEvt.getProperty("in_tree_no", "");
                string in_win_time = itmEvt.getProperty("in_win_time", "");

                string old_site_code = itmEvt.getProperty("site_code", "");
                string old_site_name = itmEvt.getProperty("site_name", "");
                string new_site_code = itmEvt.getProperty("in_site_move", "");

                if (new_site_code != "")
                {
                    string old_site = GetSiteEnglish(old_site_code);
                    string new_site = GetSiteName(new_site_code);
                    in_tree_no = old_site + in_tree_no + "(" + new_site + ")";
                }
                else if (cfg.is_list_mode || tab.site_code != old_site_code)
                {
                    in_tree_no = in_tree_no + "(" + old_site_name + ")";
                }

                if (in_win_time == "")
                {
                    sb.Append("<p>" + in_tree_no + "</p>");
                }
                else
                {
                    sb.Append("<p>"
                        + " <label class='label bg-green inn_tree_no'>" + in_tree_no + "</label>"
                        + " <label>(" + DtmStr(in_win_time) + ")</label>"
                        + "</p>"
                        );
                }
            }

            return sb.ToString();
        }

        private string ProgramInfo(TConfig cfg, TProgram row)
        {
            var itmProgram = row.value;
            string in_l1 = itmProgram.getProperty("in_l1", "");
            string in_name2 = itmProgram.getProperty("in_name2", "");
            string in_weight = itmProgram.getProperty("in_weight", "");
            string in_team_count = itmProgram.getProperty("in_team_count", "");

            if (in_l1 == "團體賽")
            {
                return in_name2 + "(" + in_team_count + ")";
            }
            else
            {
                return in_name2 + " " + in_weight + "(" + in_team_count + ")";
            }
        }

        private string TabTable(TConfig cfg, string table_id)
        {
            return "<table id='" + table_id + "'"
                + " class='table table-hover table-bordered table-rwd rwd rwdtable'"
                + " style='background-color: #fff;' data-toggle='table'"
                + " data-click-to-select='true' data-striped='false'"
                + " data-search='false' data-pagination='false'>";
        }

        private string TabAnchor(TConfig cfg, TTab tab)
        {
            return "<a id='" + tab.a_id + "'"
                + " href='#" + tab.content_id + "'"
                + " class='nav-link'"
                + " role='tab'"
                + " data-toggle='pill'"
                + " aria-controls='pills-home'"
                + " aria-selected='true'"
                + " onclick='StoreTab_Click(this)'>"
                + tab.title
                + "</a>";
        }

        private TPage MapPageModel(TConfig cfg, List<TProgram> rows)
        {
            var result = new TPage
            {
                tabs = new List<TTab>(),
            };

            for (var i = 0; i < rows.Count; i++)
            {
                var row = rows[i];
                var tab = cfg.is_list_mode
                    ? result.tabs.Find(x => x.key == row.key_list)
                    : result.tabs.Find(x => x.code == row.key_site);

                if (tab == null)
                {
                    tab = cfg.is_list_mode
                        ? NewTab_List(cfg, row, result.tabs.Count + 1)
                        : NewTab_Site(cfg, row, result.tabs.Count + 1);

                    if (result.tabs.Count == 0)
                    {
                        tab.active1 = "active";
                        tab.active2 = "fade in active";
                    }
                    result.tabs.Add(tab);
                }
                tab.rows.Add(row);
            }

            return result;
        }

        private TTab NewTab_Site(TConfig cfg, TProgram row, int idx)
        {
            var id = "site_tab_" + idx;

            return new TTab
            {
                code = row.key_site,
                key = row.key_list,
                site_code = row.key_site.ToString(),
                active1 = "",
                active2 = "",
                title = "第" + GetSiteName(row.key_site) + "場地",
                li_id = "li_" + id,
                a_id = "a_" + id,
                content_id = "box_" + id,
                folder_id = "folder_" + id,
                toggle_id = "toggle_" + id,
                table_id = "table_" + id,
                rows = new List<TProgram>(),
            };
        }

        private TTab NewTab_List(TConfig cfg, TProgram row, int idx)
        {
            var id = "site_tab_" + idx;

            return new TTab
            {
                code = idx,
                key = row.key_list,
                active1 = "",
                active2 = "",
                title = row.key_list,
                li_id = "li_" + id,
                a_id = "a_" + id,
                content_id = "box_" + id,
                folder_id = "folder_" + id,
                toggle_id = "toggle_" + id,
                table_id = "table_" + id,
                rows = new List<TProgram>(),
            };
        }

        private string GetSiteEnglish(string code)
        {
            switch (code)
            {
                case "1": return "A";
                case "2": return "B";
                case "3": return "C";
                case "4": return "D";
                case "5": return "E";
                case "6": return "F";
                case "7": return "G";
                case "8": return "H";
                case "9": return "I";
                case "10": return "J";
                default: return "";
            }
        }

        private string GetSiteName(int code)
        {
            return GetSiteName(code.ToString());
        }

        private string GetSiteName(string code)
        {
            switch (code)
            {
                case "1": return "一";
                case "2": return "二";
                case "3": return "三";
                case "4": return "四";
                case "5": return "五";
                case "6": return "六";
                case "7": return "七";
                case "8": return "八";
                case "9": return "九";
                case "10": return "十";
                default: return "";
            }
        }

        private List<TProgram> GetPrograms(TConfig cfg, Item items, Dictionary<string, List<Item>> map1, Dictionary<string, List<Item>> map2)
        {
            var result = new List<TProgram>();
            var count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string id = item.getProperty("id", "");
                string in_l1 = item.getProperty("in_l1", "");
                string in_l2 = item.getProperty("in_l2", "");
                string in_battle_type = item.getProperty("in_battle_type", "");
                string site_code = item.getProperty("site_code", "");
                string in_end_tree_no = item.getProperty("in_end_tree_no", "");

                var row = new TProgram
                {
                    id = id,
                    in_l1 = in_l1,
                    in_l2 = in_l2,
                    value = item,
                };

                row.is_team = in_l1 == "團體組";
                row.is_robin = in_battle_type == "SingleRoundRobin";

                row.key_site = GetIntVal(site_code);
                row.key_list = GetAgeGender(cfg, row);

                if (in_end_tree_no != "")
                {
                    row.rk12_events = QueryEventItems(cfg, id, in_end_tree_no);
                }
                else if (map1.ContainsKey(id))
                {
                    row.rk12_events = map1[id];
                }

                if (map2.ContainsKey(id))
                {
                    row.rk34_events = map2[id];
                }

                result.Add(row);
            }
            return result;
        }

        private string GetAgeGender(TConfig cfg, TProgram row)
        {
            if (row.is_team)
            {
                return row.in_l1;
            }
            else
            {
                return row.in_l2.Replace("個-", "");
            }
        }

        private Dictionary<string, List<Item>> GetEventMap(TConfig cfg, List<Item> items)
        {
            var result = new Dictionary<string, List<Item>>();

            for (int i = 0; i < items.Count; i++)
            {
                var item = items[i];
                var key = item.getProperty("source_id", "");

                var list = default(List<Item>);
                if (result.ContainsKey(key))
                {
                    list = result[key];
                }
                else
                {
                    list = new List<Item>();
                    result.Add(key, list);
                }
                list.Add(item);
            }
            return result;
        }

        private void AppendFightDayMenu(TConfig cfg, Item itmReturn)
        {
            var items = FightDayItems(cfg);
            var count = items.getItemCount();

            var first = "";
            var today = DateTime.Now.ToString("yyyy-MM-dd");
            var has_exist = false;
            var has_today = false;

            for (int i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var in_fight_day = item.getProperty("in_fight_day", "");

                if (i == 0)
                {
                    first = in_fight_day;
                }

                if (today == in_fight_day)
                {
                    has_today = true;
                }

                if (cfg.in_fight_day == in_fight_day)
                {
                    has_exist = true;
                }

                item.setType("inn_day");
                item.setProperty("value", in_fight_day);
                item.setProperty("text", in_fight_day);
                itmReturn.addRelationship(item);
            }

            if (count > 0 && (cfg.in_fight_day == "" || !has_exist))
            {
                if (has_today)
                {
                    cfg.in_fight_day = today;
                }
                else
                {
                    cfg.in_fight_day = first;
                }
                itmReturn.setProperty("in_fight_day", cfg.in_fight_day);
            }
        }

        private List<Item> QueryEventItems(TConfig cfg, string program_id, string in_end_tree_no)
        {
            string sql = @"
                SELECT 
	                t1.source_id
	                , t1.id
	                , t1.in_tree_no
	                , t1.in_win_status
	                , DATEADD(HOUR, 8, t1.in_win_time) AS 'in_win_time'
	                , t1.in_site_move
	                , t2.in_code AS 'site_code'
	                , REPLACE(REPLACE(t2.in_name, N'第', ''), N'場地', '') AS 'site_name'
                FROM
	                IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_SITE t2 WITH(NOLOCK)
	                ON t2.id = t1.in_site
                WHERE
	                t1.source_id = '{#program_id}'
	                AND t1.in_tree_no = '{#in_end_tree_no}'
            ";

            sql = sql.Replace("{#program_id}", program_id)
                .Replace("{#in_end_tree_no}", in_end_tree_no);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            var items = cfg.inn.applySQL(sql);

            var result = new List<Item>();
            MergeItems(cfg, result, items);
            return result;
        }

        private List<Item> FightRank12EventItems(TConfig cfg)
        {
            var result = new List<Item>();
            //冠亞軍(單淘、四柱復活)
            MergeItems(cfg, result, FightRank12EventItems_Top24(cfg));
            //冠亞軍(單循環)
            MergeItems(cfg, result, FightRank12EventItems_Robin(cfg));
            return result;
        }

        private List<Item> FightRank34EventItems(TConfig cfg)
        {
            var result = new List<Item>();
            //三三五五(單淘)
            MergeItems(cfg, result, FightRank34EventItems_TopTwo(cfg));
            //三三五五(四柱復活)
            MergeItems(cfg, result, FightRank34EventItems_TopFour(cfg));
            return result;
        }

        private void MergeItems(TConfig cfg, List<Item> list, Item items)
        {
            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                list.Add(items.getItemByIndex(i));
            }
        }

        //冠亞軍(單淘、四柱復活)
        private Item FightRank12EventItems_Top24(TConfig cfg)
        {
            string sql = @"
                SELECT 
	                t11.source_id
	                , t11.id
	                , t11.in_tree_no
	                , t11.in_win_status
	                , DATEADD(HOUR, 8, t11.in_win_time) AS 'in_win_time'
	                , t11.in_site_move
	                , t11.in_tree_rank
	                , t12.in_code AS 'site_code'
	                , REPLACE(REPLACE(t12.in_name, N'第', ''), N'場地', '') AS 'site_name'
                FROM
	                IN_MEETING_PEVENT t11 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_SITE t12 WITH(NOLOCK)
	                ON t12.id = t11.in_site
                INNER JOIN
                (
	                SELECT
		                t2.source_id
		                , max(t2.in_tree_no) AS 'max_tree_no'
	                FROM
		                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
					INNER JOIN
						IN_MEETING_PEVENT t2 WITH(NOLOCK)
						ON t2.source_id = t1.id
	                WHERE
		                t1.in_meeting = '{#meeting_id}'
		                AND t1.in_fight_day = '{#in_fight_day}'
						AND t1.in_battle_type IN ('TopTwo', 'JudoTopFour')
						AND t2.in_tree_name = 'main'
						AND t2.in_round_code = 2
		                AND ISNULL(in_tree_no, 0) > 0
		                AND ISNULL(in_type, '') IN ('', 'p')
		                AND ISNULL(in_win_status, '') NOT IN ('cancel', 'nofight', 'bypass')
	                GROUP BY
		                t2.source_id
                )t13 
	                ON t13.source_id = t11.source_id
	                AND t13.max_tree_no = t11.in_tree_no
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_fight_day}", cfg.in_fight_day);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        //冠亞軍(單循環)
        private Item FightRank12EventItems_Robin(TConfig cfg)
        {
            string sql = @"
                SELECT 
	                t11.source_id
	                , t11.id
	                , t11.in_tree_no
	                , t11.in_win_status
	                , DATEADD(HOUR, 8, t11.in_win_time) AS 'in_win_time'
	                , t11.in_site_move
	                , t11.in_tree_rank
	                , t12.in_code AS 'site_code'
	                , REPLACE(REPLACE(t12.in_name, N'第', ''), N'場地', '') AS 'site_name'
                FROM
	                IN_MEETING_PEVENT t11 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_SITE t12 WITH(NOLOCK)
	                ON t12.id = t11.in_site
                INNER JOIN
                (
	                SELECT
		                t2.source_id
		                , max(t2.in_tree_no) AS 'max_tree_no'
	                FROM
		                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
					INNER JOIN
						IN_MEETING_PEVENT t2 WITH(NOLOCK)
						ON t2.source_id = t1.id
	                WHERE
		                t1.in_meeting = '{#meeting_id}'
		                AND t1.in_fight_day = '{#in_fight_day}'
						AND t1.in_battle_type = 'SingleRoundRobin'
		                AND ISNULL(in_tree_no, 0) > 0
		                AND ISNULL(in_type, '') IN ('', 'p')
		                AND ISNULL(in_win_status, '') NOT IN ('cancel', 'nofight', 'bypass')
	                GROUP BY
		                t2.source_id
                )t13 
	                ON t13.source_id = t11.source_id
	                AND t13.max_tree_no = t11.in_tree_no
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_fight_day}", cfg.in_fight_day);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        //三三五五(單淘)
        private Item FightRank34EventItems_TopTwo(TConfig cfg)
        {
            string sql = @"
                SELECT 
	                t11.source_id
	                , t11.id
	                , t11.in_tree_id
	                , t11.in_tree_no
	                , t11.in_win_status
	                , DATEADD(HOUR, 8, t11.in_win_time) AS 'in_win_time'
	                , t11.in_site_move
	                , t11.in_tree_rank
	                , t12.in_code AS 'site_code'
	                , REPLACE(REPLACE(t12.in_name, N'第', ''), N'場地', '') AS 'site_name'
                FROM
	                IN_MEETING_PEVENT t11 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_SITE t12 WITH(NOLOCK)
	                ON t12.id = t11.in_site
                INNER JOIN
                (
	                SELECT
		                t2.source_id
		                , t2.id
		                , t2.in_tree_no
	                FROM
		                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
					INNER JOIN
						IN_MEETING_PEVENT t2 WITH(NOLOCK)
						ON t2.source_id = t1.id
	                WHERE
		                t1.in_meeting = '{#meeting_id}'
		                AND t1.in_fight_day = '{#in_fight_day}'
						AND t1.in_battle_type = 'TopTwo'
						AND t2.in_tree_name = 'main'
						AND t2.in_round_code = 4
		                AND ISNULL(in_tree_no, 0) > 0
		                AND ISNULL(in_type, '') IN ('', 'p')
		                AND ISNULL(in_win_status, '') NOT IN ('cancel', 'nofight', 'bypass')
                )t13 
	                ON t13.id = t11.id
				ORDER BY
					t11.source_id
					, t11.in_tree_id
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_fight_day}", cfg.in_fight_day);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        //三三五五(四柱復活)
        private Item FightRank34EventItems_TopFour(TConfig cfg)
        {
            string sql = @"
                SELECT 
	                t11.source_id
	                , t11.id
	                , t11.in_tree_id
	                , t11.in_tree_no
	                , t11.in_win_status
	                , DATEADD(HOUR, 8, t11.in_win_time) AS 'in_win_time'
	                , t11.in_site_move
	                , t11.in_tree_rank
	                , t12.in_code AS 'site_code'
	                , REPLACE(REPLACE(t12.in_name, N'第', ''), N'場地', '') AS 'site_name'
                FROM
	                IN_MEETING_PEVENT t11 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_SITE t12 WITH(NOLOCK)
	                ON t12.id = t11.in_site
                INNER JOIN
                (
	                SELECT
		                t2.source_id
		                , t2.id
		                , t2.in_tree_no
	                FROM
		                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
					INNER JOIN
						IN_MEETING_PEVENT t2 WITH(NOLOCK)
						ON t2.source_id = t1.id
	                WHERE
		                t1.in_meeting = '{#meeting_id}'
		                AND t1.in_fight_day = '{#in_fight_day}'
						AND t1.in_battle_type = 'JudoTopFour'
						AND t2.in_tree_name = 'repechage'
						AND t2.in_round_code = 4
		                AND ISNULL(in_tree_no, 0) > 0
		                AND ISNULL(in_type, '') IN ('', 'p')
		                AND ISNULL(in_win_status, '') NOT IN ('cancel', 'nofight', 'bypass')
                )t13 
	                ON t13.id = t11.id
				ORDER BY
					t11.source_id
					, t11.in_tree_id
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_fight_day}", cfg.in_fight_day);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        private Item RankItems(TConfig cfg)
        {
            string sql = @"
                SELECT
                    t1.id AS 'team_id'
                    , t1.in_stuff_b1
                    , t1.in_current_org
                    , t1.in_team
                    , t1.in_name
                    , t1.in_names
                    , t1.in_sno
                    , t1.in_final_rank
                    , t1.in_sign_no
                    , t1.in_part_id
                    , t1.in_part_code
                    , t1.map_short_org
                    , t1.map_org_name
                    , t1.in_weight_result
                    , t1.in_weight_message
                    , t1.in_check_result
                    , t1.in_medal_dns
                    , t3.in_l1
                FROM 
                    VU_MEETING_PTEAM t1
                LEFT OUTER JOIN
                    IN_MEETING_PEVENT t2 WITH(NOLOCK)
                    ON t2.id = t1.in_event
                INNER JOIN
                    IN_MEETING_PROGRAM t3 WITH(NOLOCK)
                    ON t3.id = t1.source_id
                WHERE
                    t1.in_meeting = '{#meeting_id}'
                    AND t1.source_id = '{#program_id}'
                    AND ISNULL(t1.in_type, '') <> 's'
					AND ISNULL(t1.in_final_rank, 0) > 0
                ORDER BY
                    t1.in_final_rank
                    , t2.in_tree_id
                    , t1.in_foot
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#program_id}", cfg.program_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        private Item MedalItems(TConfig cfg)
        {
            string sql = @"
                SELECT
	                t1.id
	                , t1.in_l1
	                , t1.in_l2
	                , t1.in_l3
	                , t1.in_name2
	                , t1.in_short_name
	                , t1.in_weight
	                , t1.in_team_count
	                , t1.in_battle_type
                    , t1.in_director_status
	                , DATEADD(HOUR, 8, t1.in_director_time) AS 'in_director_time'
                    , t1.in_certificate_status
	                , DATEADD(HOUR, 8, t1.in_certificate_time) AS 'in_certificate_time'
                    , t1.in_medal_status
	                , DATEADD(HOUR, 8, t1.in_medal_time) AS 'in_medal_time'
                    , t1.in_end_tree_no
                FROM
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                WHERE 
	                t1.in_meeting = '{#meeting_id}'
	                AND t1.in_fight_day = '{#in_fight_day}'
	                AND t1.in_team_count > 1
	                AND t1.in_medal_status = N'可頒獎'
                ORDER BY
	                t1.in_certificate_time DESC
            ";


            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_fight_day}", cfg.in_fight_day);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        private Item FightProgramItems(TConfig cfg)
        {
            string orderBy = cfg.is_list_mode
                ? "t1.in_sort_order"
                : "t3.in_code, t2.created_on";

            string sql = @"
                SELECT
	                t1.id
	                , t1.in_l1
	                , t1.in_l2
	                , t1.in_l3
	                , t1.in_name2
	                , t1.in_short_name
	                , t1.in_weight
	                , t1.in_team_count
	                , t1.in_battle_type
                    , t1.in_director_status
	                , DATEADD(HOUR, 8, t1.in_director_time) AS 'in_director_time'
                    , t1.in_certificate_status
	                , DATEADD(HOUR, 8, t1.in_certificate_time) AS 'in_certificate_time'
                    , t1.in_medal_status
	                , DATEADD(HOUR, 8, t1.in_medal_time) AS 'in_medal_time'
                    , t1.in_end_tree_no
	                , t3.in_code AS 'site_code'
	                , t3.in_name AS 'site_name'
                FROM
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_ALLOCATION t2 WITH(NOLOCK)
	                ON t2.in_program = t1.id
                INNER JOIN
	                IN_MEETING_SITE t3 WITH(NOLOCK)
	                ON t3.id = t2.in_site
                WHERE
	                t1.in_meeting = '{#meeting_id}'
	                AND t1.in_fight_day = '{#in_fight_day}'
	                AND t1.in_team_count > 1
                ORDER BY
	                {#orderBy}
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_fight_day}", cfg.in_fight_day)
                .Replace("{#orderBy}", orderBy);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        private Item FightDayItems(TConfig cfg)
        {
            string sql = @"
                SELECT DISTINCT
	                in_fight_day 
                FROM 
	                IN_MEETING_PROGRAM WITH(NOLOCK) 
                WHERE 
	                in_meeting = '{#meeting_id}' 
	                AND ISNULL(in_fight_day, '') <> ''
                ORDER BY 
	                in_fight_day
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }

            public string meeting_id { get; set; }
            public string program_id { get; set; }
            public string in_fight_day { get; set; }
            public string scene { get; set; }

            public bool is_list_mode { get; set; }

            public string SelectCtrl1 { get; set; }
        }

        private class TPage
        {
            public List<TTab> tabs { get; set; }
        }

        private class TTab
        {
            public int code { get; set; }
            public string key { get; set; }
            public string site_code { get; set; }

            public string id { get; set; }
            public string active1 { get; set; }
            public string active2 { get; set; }
            public string title { get; set; }
            public string groups { get; set; }

            public string li_id { get; set; }
            public string a_id { get; set; }
            public string content_id { get; set; }
            public string folder_id { get; set; }
            public string toggle_id { get; set; }
            public string table_id { get; set; }

            public List<TProgram> rows { get; set; }
        }

        private class TProgram
        {
            public string id { get; set; }
            public string in_l1 { get; set; }
            public string in_l2 { get; set; }

            public bool is_team { get; set; }
            public bool is_robin { get; set; }
            public int key_site { get; set; }
            public string key_list { get; set; }

            public Item value { get; set; }
            public List<Item> rk12_events { get; set; }
            public List<Item> rk34_events { get; set; }
        }

        private string DtmStr(string value, string format = "HH:mm")
        {
            if (value == "") return "";

            var dt = GetDtm(value);
            if (dt == DateTime.MinValue) return "";

            return dt.ToString(format);
        }

        private DateTime GetDtm(string value)
        {
            if (value == "") return DateTime.MinValue;

            DateTime result = DateTime.MinValue;
            if (DateTime.TryParse(value, out result))
            {
                return result;
            }
            return DateTime.MinValue;
        }

        private int GetIntVal(string value, int def = 0)
        {
            int result = def;
            Int32.TryParse(value, out result);
            return result;
        }
    }
}
