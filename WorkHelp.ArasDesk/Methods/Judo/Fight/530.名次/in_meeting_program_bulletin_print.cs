﻿using Aras.IOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WorkHelp.ArasDesk.Methods.Judo.Common
{
    public class in_meeting_program_bulletin_print : Item
    {
        public in_meeting_program_bulletin_print(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 
                    名次列印
                輸出: 
                    docx
                重點: 
                    - 需 Xceed.Document.NET.dll
                    - 需 Xceed.Words.NET.dll
                    - 請於 method-config.xml 引用 DLL
                日期: 
                    2020-11-18: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]in_meeting_program_bulletin_print";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, this.dom.InnerXml);

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                meeting_id = itmR.getProperty("meeting_id", ""),
                in_date = itmR.getProperty("in_date", ""),
                mode = itmR.getProperty("mode", ""),
            };

            if (cfg.meeting_id == "")
            {
                itmR.setProperty("error_message", "賽事 id 不得為空白");
                return itmR;
            }

            cfg.isExport = cfg.mode == "word" || cfg.mode == "pdf";
            cfg.IsWMG = cfg.meeting_id == "5B6CCBF34B32402C9CB7E65892EDF69C";

            //取得賽事資料
            cfg.itmMeeting = GetMeeting(cfg, itmR);
            cfg.mt_title = cfg.itmMeeting.getProperty("in_title", "");
            itmR.setProperty("in_title", cfg.mt_title);

            //挑戰賽
            cfg.mt_battle_type = GetMtVariableValue(cfg, "fight_battle_type");
            cfg.isChallenge = cfg.mt_battle_type == "Challenge";

            //取得名次資料
            Item itmRanks = GetRanks(cfg);

            //轉換名次資料
            Dictionary<string, TTable> map = MapRankList(cfg, itmRanks);
            ClearRanks(map);

            if (map.Count > 0)
            {
                map.Last().Value.IsEndTable = true;

                if (cfg.mode == "")
                {
                    AppendDays(cfg, itmR);
                    AppendProgramMenu(cfg, itmR);
                    AppendTables(cfg, map, itmR);
                }
                else if (cfg.mode == "word")
                {
                    ExportWord(cfg, map, itmR);
                }
                else if (cfg.mode == "pdf")
                {
                    ExportPDF(cfg, map, itmR);
                }
            }

            return itmR;
        }

        //組別選單
        private void AppendDays(TConfig cfg, Item itmReturn)
        {
            Item itmEmpty = cfg.inn.newItem();
            itmEmpty.setType("inn_date");
            itmEmpty.setProperty("value", "");
            itmEmpty.setProperty("label", "請選擇");
            itmReturn.addRelationship(itmEmpty);

            string sql = "SELECT DISTINCT in_date_key FROM IN_MEETING_ALLOCATION WITH(NOLOCK) WHERE in_meeting = '" + cfg.meeting_id + "' ORDER BY in_date_key";
            Item itmDays = cfg.inn.applySQL(sql);
            int count = itmDays.getItemCount();

            for (int i = 0; i < count; i++)
            {
                var itmDay = itmDays.getItemByIndex(i);
                var in_date_key = itmDay.getProperty("in_date_key", "");
                var selected = in_date_key == cfg.in_date ? "selected" : "";
                itmDay.setType("inn_date");
                itmDay.setProperty("selected", selected);
                itmDay.setProperty("value", in_date_key);
                itmDay.setProperty("label", in_date_key);
                itmReturn.addRelationship(itmDay);
            }
        }

        //組別選單
        private void AppendProgramMenu(TConfig cfg, Item itmReturn)
        {
            if (cfg.in_date == "") return;

            var sql = @"
                SELECT 
	                t2.id
	                , t2.in_name3 + ' (' + CAST(in_rank_count AS VARCHAR) + ')' AS 'program_display'
                FROM
	                IN_MEETING_ALLOCATION t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PROGRAM t2 WITH(NOLOCK)
	                ON t2.id = t1.in_program
                WHERE
	                t1.in_meeting = '{#meeting_id}'
	                AND t1.in_date_key = '{#in_date}'
                ORDER BY
	                t2.in_medal_sort
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_date}", cfg.in_date);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item items = cfg.inn.applySQL(sql);

            Item itmEmpty = cfg.inn.newItem();
            itmEmpty.setType("inn_program");
            itmEmpty.setProperty("value", "");
            itmEmpty.setProperty("label", "請選擇");
            itmReturn.addRelationship(itmEmpty);

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                item.setType("inn_program");
                item.setProperty("value", item.getProperty("id", ""));
                item.setProperty("label", item.getProperty("program_display", ""));
                itmReturn.addRelationship(item);
            }
        }

        private void ClearRanks(Dictionary<string, TTable> map)
        {
            foreach (var kv in map)
            {
                var table = kv.Value;
                foreach (var kv2 in table.Rows)
                {
                    var row = kv2.Value;
                    switch (row.max_rank)
                    {
                        case 1:
                            row.Rank2.Text = "";
                            row.Rank3.Text = "";
                            row.Rank4.Text = "";
                            row.Rank5.Text = "";
                            row.Rank6.Text = "";
                            row.Rank7.Text = "";
                            row.Rank8.Text = "";
                            break;

                        case 2:
                            row.Rank3.Text = "";
                            row.Rank4.Text = "";
                            row.Rank5.Text = "";
                            row.Rank6.Text = "";
                            row.Rank7.Text = "";
                            row.Rank8.Text = "";
                            break;

                        case 3:
                            row.Rank4.Text = "";
                            row.Rank5.Text = "";
                            row.Rank6.Text = "";
                            row.Rank7.Text = "";
                            row.Rank8.Text = "";
                            break;

                        case 4:
                            row.Rank5.Text = "";
                            row.Rank6.Text = "";
                            row.Rank7.Text = "";
                            row.Rank8.Text = "";
                            break;

                        case 5:
                            row.Rank6.Text = "";
                            row.Rank7.Text = "";
                            row.Rank8.Text = "";
                            break;

                        case 6:
                            row.Rank7.Text = "";
                            row.Rank8.Text = "";
                            break;

                        case 7:
                            row.Rank8.Text = "";
                            break;

                        case 8:
                            break;

                        default:
                            row.Rank7.Text = "";
                            row.Rank8.Text = "";
                            break;
                    }
                }
            }
        }

        #region 匯出 Word

        /// <summary>
        /// 匯出 Word
        /// </summary>
        private void ExportWord(TConfig cfg, Dictionary<string, TTable> map, Item itmReturn)
        {
            TExport export = GetExportInfo(cfg);

            var rpt = new TRptSetting
            {
                FontName = "標楷體",
                FontSize = 11,
                RowCount = 1 + 20,
                BodyRowCount = 20,
                RowStart = 1,
                ColStart = 0,
                CurrentRow = 1,
                Fields = new List<TField>(),
                MeetingTitle = cfg.mt_title,
            };

            rpt.Fields.Add(new TField { Title = "第二階", Property = "left_title", Format = "" });
            rpt.Fields.Add(new TField { Title = "第三階", Property = "row_title", Format = "" });
            rpt.Fields.Add(new TField { Title = "冠軍", Property = "rank1", Format = "" });
            rpt.Fields.Add(new TField { Title = "亞軍", Property = "rank2", Format = "" });
            if (cfg.isChallenge)
            {
                rpt.Fields.Add(new TField { Title = "第三名", Property = "rank3", Format = "" });
                rpt.Fields.Add(new TField { Title = "第四名", Property = "rank4", Format = "" });
            }
            else
            {
                rpt.Fields.Add(new TField { Title = "季軍", Property = "rank3", Format = "" });
                rpt.Fields.Add(new TField { Title = "季軍", Property = "rank4", Format = "" });
            }
            rpt.Fields.Add(new TField { Title = "第五名", Property = "rank5", Format = "" });
            rpt.Fields.Add(new TField { Title = "第五名", Property = "rank6", Format = "" });

            //載入模板
            using (var doc = Xceed.Words.NET.DocX.Load(export.Source))
            {
                foreach (var kv in map)
                {
                    var table = kv.Value;

                    if (table.Rows.Count == 0)
                    {
                        continue;
                    }

                    if (table.Rows.Count > rpt.BodyRowCount)
                    {
                        throw new Exception("量級數量超過預設值，請調整 Word 樣板，並修改參數");
                    }

                    AppendWordTable(cfg, doc, rpt, table);
                }

                doc.Tables[0].Remove();

                //doc.RemoveParagraphAt(0);
                doc.RemoveParagraphAt(0);

                doc.SaveAs(export.File);

            }

            itmReturn.setProperty("xls_name", export.Url);
        }

        private void AppendWordTable(TConfig cfg, Xceed.Words.NET.DocX doc, TRptSetting rpt, TTable table)
        {
            rpt.CurrentRow = rpt.RowStart;

            //插入標題段落
            var p = doc.InsertParagraph();

            p.Append(rpt.MeetingTitle)
               .Font(new Xceed.Document.NET.Font(rpt.FontName))
               .FontSize(25)
               .Bold()
               .Spacing(10);
            //.Color(System.Drawing.Color.Red)
            p.Alignment = Xceed.Document.NET.Alignment.center;

            p = doc.InsertParagraph();
            p.Append(table.Title)
               .Font(new Xceed.Document.NET.Font(rpt.FontName))
               .FontSize(12);
            p.Alignment = Xceed.Document.NET.Alignment.left;

            //取得模板表格
            var template_table = doc.Tables[0];

            var docTable = doc.InsertTable(template_table);

            //資料容器
            Item itmContainer = cfg.inn.newItem();

            var c = System.Environment.NewLine;
            foreach (var rkv in table.Rows)
            {
                var row = rkv.Value;
                itmContainer.setProperty("left_title", table.LeftTitle_Word);
                itmContainer.setProperty("row_title", row.Title_Word + c + row.Title_Weight);
                itmContainer.setProperty("rank1", row.Rank1.Text);
                itmContainer.setProperty("rank2", row.Rank2.Text);
                itmContainer.setProperty("rank3", row.Rank3.Text);
                itmContainer.setProperty("rank4", row.Rank4.Text);
                itmContainer.setProperty("rank5", row.Rank5.Text);
                itmContainer.setProperty("rank6", row.Rank6.Text);

                SetCellValue(docTable, rpt, itmContainer);

                rpt.CurrentRow++;
            }

            for (int i = rpt.RowCount - 1; i >= rpt.CurrentRow; i--)
            {
                docTable.RemoveRow(i);
            }

            if (!table.IsEndTable)
            {
                var pend = doc.InsertParagraph();
                pend.InsertPageBreakAfterSelf();
            }
        }

        //設定資料表格
        private void SetCellValue(Xceed.Document.NET.Table table, TRptSetting rpt, Item item)
        {
            int wsRow = rpt.CurrentRow;
            int wsCol = rpt.ColStart;
            int fdCount = rpt.Fields.Count;

            for (int i = 0; i < fdCount; i++)
            {
                var f = rpt.Fields[i];
                var x = wsCol + i;
                var v = GetCellValue(f, item);

                table.Rows[wsRow].Cells[x].Paragraphs.First().Append(v).Font(rpt.FontName).FontSize(rpt.FontSize);
            }
        }

        //設定資料列
        private string GetCellValue(TField field, Item item)
        {
            if (field.Property == "")
            {
                return "";
            }

            string value = item.getProperty(field.Property);

            return value;

            //switch (field.Format)
            //{
            //    case "center":
            //        break;

            //    case "yyyy/MM/dd":
            //    case "yyyy/MM/dd HH:mm":
            //        break;

            //    case "$ #,##0":
            //        cell.Value = value;
            //        cell.Style.NumberFormat.Format = "$ #,##0";
            //        break;

            //    case "tel":
            //        if (value.StartsWith("09"))
            //        {
            //            cell.Value = value.Replace("-", "");
            //            cell.Style.NumberFormat.Format = "0000-000-000";
            //            cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
            //        }
            //        else
            //        {
            //            cell.Value = "'" + value;
            //            cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
            //        }
            //        break;

            //    case "text":
            //        cell.Value = "'" + value;
            //        cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
            //        break;

            //    default:
            //        break;
            //}
        }

        /// <summary>
        /// 取得匯出設定資訊
        /// </summary>
        private TExport GetExportInfo(TConfig cfg)
        {
            Item itmPath = GetXlsPaths(cfg, "rank_overall_path");

            //比賽名稱
            string in_title = cfg.mt_title;

            string export_path = itmPath.getProperty("export_path", "").TrimEnd('\\');
            string ext_name = ".docx";
            string doc_name = in_title + "_成績總表_" + DateTime.Now.ToString("yyyyMMdd_HHmmss");

            string doc_file = export_path + "\\" + doc_name + ext_name;
            string doc_url = doc_name + ext_name;

            string templateFile = itmPath.getProperty("template_path", "");
            if (cfg.isChallenge)
            {
                templateFile = templateFile.Replace("overall", "challenge");
            }

            return new TExport
            {
                Source = templateFile,
                File = doc_file,
                Url = doc_url,
            };
        }

        #endregion 匯出 Word

        #region 匯出 PDF

        /// <summary>
        /// 匯出 PDF
        /// </summary>
        private void ExportPDF(TConfig cfg, Dictionary<string, TTable> map, Item itmReturn)
        {
            var rpt = new TRptSetting
            {
                WsRow = 4,
                WsCol = 1,
                Heads = new string[] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" },
                SheetCount = 1,
                Sheets = new List<string>(),
                FontName = "標楷體",
                FontSize = 11,
                Fields = new List<TField>(),
                MeetingTitle = cfg.mt_title,
            };

            TExport export = GetPDFExportInfo(cfg);

            Spire.Xls.Workbook workbook = new Spire.Xls.Workbook();
            workbook.LoadFromFile(export.Source);
            Spire.Xls.Worksheet sheetTemplate = workbook.Worksheets[0];

            foreach (var kv in map)
            {
                var table = kv.Value;

                if (table.Rows.Count == 0)
                {
                    continue;
                }

                AppendSheets(cfg, rpt, workbook, sheetTemplate, table);
            }

            //移除樣板 sheet
            sheetTemplate.Remove();
            //輸出 pdf
            workbook.SaveToFile(export.File.Replace("xlsx", "pdf"), Spire.Xls.FileFormat.PDF);

            itmReturn.setProperty("xls_name", export.Url.Replace("xlsx", "pdf"));
        }

        private void AppendSheets(TConfig cfg
            , TRptSetting rpt
            , Spire.Xls.Workbook workbook
            , Spire.Xls.Worksheet sheetTemplate
            , TTable table)
        {

            Spire.Xls.Worksheet sheet = workbook.CreateEmptySheet();
            sheet.CopyFrom(sheetTemplate);
            sheet.Name = table.Title;

            sheet.Range["A1"].Text = table.Head;
            sheet.Range["A1"].Style.Font.FontName = rpt.FontName;

            sheet.Range["A2"].Text = table.Title;
            sheet.Range["A2"].Style.Font.FontName = rpt.FontName;

            int wsRow = rpt.WsRow;
            int wsCol = rpt.WsCol;

            List<TField> fields = new List<TField>();
            //fields.Add(new TField { Title = "第二階", Property = "left_title", Format = "" });
            fields.Add(new TField { Title = "第三階", Property = "row_title", Format = "" });
            fields.Add(new TField { Title = "冠軍", Property = "rank1", Format = "" });
            fields.Add(new TField { Title = "亞軍", Property = "rank2", Format = "" });
            if (cfg.isChallenge)
            {
                fields.Add(new TField { Title = "第三名", Property = "rank3", Format = "" });
                fields.Add(new TField { Title = "第四名", Property = "rank4", Format = "" });
            }
            else
            {
                fields.Add(new TField { Title = "季軍", Property = "rank3", Format = "" });
                fields.Add(new TField { Title = "季軍", Property = "rank4", Format = "" });
            }
            fields.Add(new TField { Title = "第五名", Property = "rank5", Format = "" });
            fields.Add(new TField { Title = "第五名", Property = "rank6", Format = "" });

            int row_start = wsRow;
            int row_end = wsRow + table.Rows.Count;

            //資料容器
            Item itmContainer = cfg.inn.newItem();

            var c = System.Environment.NewLine;

            foreach (var rkv in table.Rows)
            {
                var row = rkv.Value;
                //itmContainer.setProperty("left_title", table.LeftTitle_Word);
                itmContainer.setProperty("row_title", row.Title_Word + c + row.Title_Weight);
                itmContainer.setProperty("rank1", row.Rank1.Text);
                itmContainer.setProperty("rank2", row.Rank2.Text);
                itmContainer.setProperty("rank3", row.Rank3.Text);
                itmContainer.setProperty("rank4", row.Rank4.Text);
                itmContainer.setProperty("rank5", row.Rank5.Text);
                itmContainer.setProperty("rank6", row.Rank6.Text);

                SetItemCell(sheet, rpt, wsRow, wsCol, itmContainer, fields);

                wsRow++;
            }

            string pos_1 = "A" + row_start;
            string pos_2 = "A" + (row_end - 1);

            Spire.Xls.CellRange range = sheet.Range[pos_1 + ":" + pos_2];
            range.Merge();
            range.Text = table.LeftTitle_Word;
            range.Style.Font.FontName = rpt.FontName;

            range.Style.Borders[Spire.Xls.BordersLineType.EdgeTop].LineStyle = Spire.Xls.LineStyleType.Thin;
            range.Style.Borders[Spire.Xls.BordersLineType.EdgeRight].LineStyle = Spire.Xls.LineStyleType.Thin;
            range.Style.Borders[Spire.Xls.BordersLineType.EdgeBottom].LineStyle = Spire.Xls.LineStyleType.Thin;
            range.Style.Borders[Spire.Xls.BordersLineType.EdgeLeft].LineStyle = Spire.Xls.LineStyleType.Thin;

        }

        //設定物件與資料列
        private void SetItemCell(Spire.Xls.Worksheet sheet, TRptSetting rpt, int wsRow, int wsCol, Item item, List<TField> fields)
        {
            for (int i = 0; i < fields.Count; i++)
            {
                var field = fields[i];
                var value = "";
                var position = rpt.Heads[wsCol + i] + wsRow;

                if (field.getValue != null)
                {
                    value = field.getValue(rpt, item, field);
                }
                else if (field.Property != "")
                {
                    value = item.getProperty(field.Property, "");
                }

                Spire.Xls.CellRange range = sheet.Range[position];
                SetBodyCell(range, rpt, value, field.Property);
            }
        }

        //設定資料列
        private void SetBodyCell(Spire.Xls.CellRange range, TRptSetting rpt, string value = "", string format = "")
        {
            switch (format)
            {
                case "center":
                    range.Text = value;
                    range.Style.Font.FontName = rpt.FontName;
                    break;

                case "yyyy/MM/dd":
                case "yyyy/MM/dd HH:mm":
                    range.Text = GetDateTimeVal(value, format);
                    range.Style.Font.FontName = rpt.FontName;
                    break;

                case "$ #,##0":
                    range.Text = value;
                    range.Style.Font.FontName = rpt.FontName;
                    break;

                case "text":
                    range.Text = value;
                    range.Style.Font.FontName = rpt.FontName;
                    break;

                default:
                    range.Text = value;
                    range.Style.Font.FontName = rpt.FontName;
                    break;
            }

            range.Style.Borders[Spire.Xls.BordersLineType.EdgeTop].LineStyle = Spire.Xls.LineStyleType.Thin;
            range.Style.Borders[Spire.Xls.BordersLineType.EdgeRight].LineStyle = Spire.Xls.LineStyleType.Thin;
            range.Style.Borders[Spire.Xls.BordersLineType.EdgeBottom].LineStyle = Spire.Xls.LineStyleType.Thin;
            range.Style.Borders[Spire.Xls.BordersLineType.EdgeLeft].LineStyle = Spire.Xls.LineStyleType.Thin;
        }

        /// <summary>
        /// 取得匯出設定資訊
        /// </summary>
        private TExport GetPDFExportInfo(TConfig cfg)
        {
            Item itmPath = GetXlsPaths(cfg, "rank_pdf_path");

            //比賽名稱
            string in_title = cfg.mt_title;

            string export_path = itmPath.getProperty("export_path", "").TrimEnd('\\');
            string ext_name = ".xlsx";
            string doc_name = in_title + "_成績總表_" + DateTime.Now.ToString("yyyyMMdd_HHmmss");

            string doc_file = export_path + "\\" + doc_name + ext_name;
            string doc_url = doc_name + ext_name;

            return new TExport
            {
                Source = itmPath.getProperty("template_path", ""),
                File = doc_file,
                Url = doc_url,
            };
        }

        #endregion 匯出 PDF

        //附加表格
        private void AppendTables(TConfig cfg, Dictionary<string, TTable> map, Item itmReturn)
        {
            var builder = new StringBuilder();

            foreach (var kv in map)
            {
                var table = kv.Value;
                if (table.Rows.Count > 0)
                {
                    AppendTable(cfg, builder, kv.Value);
                }
            }

            itmReturn.setProperty("inn_tables", builder.ToString());
        }

        //附加分組名單
        private void AppendTable(TConfig cfg, StringBuilder builder, TTable table)
        {
            builder.Append("<div class=\"showsheet_" + table.Name + " page-block\">");
            builder.Append("  <h2 class=\"text-center\">" + table.Head + "</h2>");
            builder.Append("  <h4 class=\"sheet-title\">" + table.Title + "<span> </span> </h4>");

            builder.Append("  <table id=\"" + table.Name + "\" data-toggle='table'  class=\"table table-hover table-bordered table-rwd rwd\">");

            AppendRankTable(cfg, builder, table);

            builder.Append("  </table>");

            //builder.Append("  <script>");
            //builder.Append("      $('#" + table.Name + "').bootstrapTable();");
            //builder.Append("      if($(window).width() <= 768) { $('#" + table.Name + "').bootstrapTable('toggleView');}");
            //builder.Append("  </script>");

            // builder.Append("  <div style='height: 25px;'> </div>");//2020.10.06 補了間隔 讓其可以蓋章
            // builder.Append("  <div class=\"pull-left signature\">" + "裁判簽章" + "</div>");

            builder.Append("</div>");
        }

        //附加名次
        private void AppendRankTable(TConfig cfg, StringBuilder builder, TTable table)
        {
            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            head.Append("<thead>");
            head.Append("  <tr>");
            head.Append("    <th class='text-center mailbox-subject search-field' colspan='2'>級組／名次</th>");
            head.Append("    <th class='text-center mailbox-subject search-field'>冠軍</th>");
            head.Append("    <th class='text-center mailbox-subject search-field'>亞軍</th>");
            if (cfg.isChallenge)
            {
                head.Append("    <th class='text-center mailbox-subject search-field'>第三名</th>");
                head.Append("    <th class='text-center mailbox-subject search-field'>第四名</th>");
            }
            else
            {
                head.Append("    <th class='text-center mailbox-subject search-field'>季軍</th>");
                head.Append("    <th class='text-center mailbox-subject search-field'>季軍</th>");
            }
            head.Append("    <th class='text-center mailbox-subject search-field'>第五名</th>");
            head.Append("    <th class='text-center mailbox-subject search-field'>第五名</th>");
            head.Append("    <th class='text-center mailbox-subject search-field'>第七名</th>");
            head.Append("    <th class='text-center mailbox-subject search-field'>第七名</th>");
            head.Append("  </tr>");
            head.Append("</thead>");

            body.Append("<tbody>");
            foreach (var kv in table.Rows)
            {
                var row = kv.Value;
                var sect = row.Title_Html + "<BR>" + row.Title_Weight;
                var link = "<a href='javascript:;' onclick='goFightTreePage(this)' data-pid='" + row.program_id + "'>" + sect + "</a>";
                body.Append("  <tr>");
                if (row.No == 1)
                {
                    body.Append("    <td rowspan='" + table.Rows.Count + "' class='text-center' style='width: 10px'>" + table.LeftTitle_Html + "</td>");
                }
                body.Append("    <td class='text-center' style='width: 65px'>" + link + "</td>");
                body.Append("    <td class='text-center' style='width: 110px'>" + row.Rank1.Text + "</td>");
                body.Append("    <td class='text-center' style='width: 110px'>" + row.Rank2.Text + "</td>");
                body.Append("    <td class='text-center' style='width: 110px'>" + row.Rank3.Text + "</td>");
                body.Append("    <td class='text-center' style='width: 110px'>" + row.Rank4.Text + "</td>");
                body.Append("    <td class='text-center' style='width: 110px'>" + row.Rank5.Text + "</td>");
                body.Append("    <td class='text-center' style='width: 110px'>" + row.Rank6.Text + "</td>");
                body.Append("    <td class='text-center' style='width: 100px'>" + row.Rank7.Text + "</td>");
                body.Append("    <td class='text-center' style='width: 100px'>" + row.Rank8.Text + "</td>");
                body.Append("  </tr>");

            }
            body.Append("</tbody>");

            builder.Append(head);
            builder.Append(body);
        }

        /// <summary>
        /// 取得賽事資訊
        /// </summary>
        private Item GetMeeting(TConfig cfg, Item itmReturn)
        {
            var sql = "SELECT id, in_title FROM IN_MEETING WITH(NOLOCK) WHERE id = '{#meeting_id}'";
            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);
            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// 取得樣板與匯出路徑
        /// </summary>
        private Item GetXlsPaths(TConfig cfg, string in_name)
        {
            Item itmResult = cfg.inn.newItem();

            //Excel 匯出路徑 (from 原創.設定參數)
            string sql = @"
                SELECT 
                    t2.in_name AS 'variable'
                    , t1.in_name
                    , t1.in_value 
                FROM 
                    In_Variable_Detail t1 WITH(NOLOCK)
                INNER JOIN 
                    In_Variable t2 WITH(NOLOCK) 
                    ON t2.ID = t1.SOURCE_ID 
                WHERE 
                    t2.in_name = N'meeting_excel'  
                    AND t1.in_name IN (N'export_path', N'{#in_name}')
            ";

            sql = sql.Replace("{#in_name}", in_name);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item items = cfg.inn.applySQL(sql);

            if (items.isError() || items.getItemCount() < 1)
            {
                throw new Exception("未設定樣板與匯出路徑 _# " + in_name);
            }

            for (int i = 0; i < items.getItemCount(); i++)
            {
                Item item = items.getItemByIndex(i);
                string xls_name = item.getProperty("in_name", "");
                string xls_value = item.getProperty("in_value", "");

                if (xls_name == "export_path")
                {
                    itmResult.setProperty("export_path", xls_value);
                }
                else
                {
                    itmResult.setProperty("template_path", xls_value);
                }
            }

            return itmResult;
        }

        /// <summary>
        /// 取得名次資訊
        /// </summary>
        private Item GetRanks(TConfig cfg)
        {
            var sql = @"
                SELECT
                    t3.id AS 'program_id'     
                    , ISNULL(t3.in_short_name, t3.in_display) AS 'in_short_name'
                    , t3.in_l1
                    , t3.in_l2
                    , t3.in_l3
                    , t3.in_team_count
                    , t3.in_rank_count
                    , t3.in_name2           AS 'program_name2'
                    , LOWER(t3.in_weight)   AS 'program_weight'
                    , t1.in_current_org
                    , t1.in_short_org
                    , t1.map_short_org
                    , t1.map_org_name
                    , t1.in_show_org
                    , t1.in_team
                    , t1.in_name
                    , t1.in_sign_no
                    , t1.in_section_no
                    , t1.in_sno
                    , t1.in_final_rank
                    , t1.in_show_rank
                FROM
                    VU_MEETING_PTEAM t1
                LEFT OUTER JOIN
                    IN_MEETING_PEVENT t2 WITH(NOLOCK)
                    ON t2.id = t1.in_event
                INNER JOIN
                    IN_MEETING_PROGRAM t3 WITH(NOLOCK)
                    ON t3.id = t1.source_id
                WHERE
                    t1.in_meeting = '{#meeting_id}'
                    AND ISNULL(t3.in_program, '') = ''
                    AND ISNULL(t1.in_final_rank, 0) > 0
                ORDER BY
                    t3.in_sort_order
                    , t1.in_stuff_b1
                    , t1.in_final_rank
                    , t2.in_tree_id
                    , t1.in_foot
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// 取得名次資訊
        /// </summary>
        private Dictionary<string, TTable> MapRankList(TConfig cfg, Item items)
        {
            int count = items.getItemCount();

            Dictionary<string, TTable> map = new Dictionary<string, TTable>();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);

                TTable table = AddAndGetTable(cfg, map, item);

                AddRow(cfg, table, item);
            }

            return map;
        }

        /// <summary>
        /// 附加並回傳 Table
        /// </summary>
        private TTable AddAndGetTable(TConfig cfg, Dictionary<string, TTable> map, Item item)
        {
            string in_l1 = item.getProperty("in_l1", "");
            string in_l2 = item.getProperty("in_l2", "");
            string title = ClearLv2(in_l2);

            var key = "";
            var type = TableEnum.None;

            switch (in_l1)
            {
                case "團體組":
                    key = in_l1;
                    type = TableEnum.Multiple;
                    break;

                case "個人組":
                    key = in_l2;
                    type = TableEnum.Single;
                    break;

                case "格式組":
                    key = in_l2;
                    type = TableEnum.Format;
                    break;

                default:
                    key = in_l2;
                    type = TableEnum.Single;
                    break;
            }

            TTable table = null;

            if (map.ContainsKey(key))
            {
                table = map[key];
            }
            else
            {
                table = new TTable
                {
                    Key = key,
                    Id = (map.Count + 1).ToString(),
                    Name = "rank_" + (map.Count + 1).ToString(),
                    Head = cfg.mt_title,
                    Rows = new Dictionary<string, TRow>(),
                    RowSpan = 1,
                    TableType = type
                };

                switch (table.TableType)
                {
                    case TableEnum.Multiple:
                        table.Title = in_l1;
                        table.Title2 = in_l1;
                        table.LeftTitle = in_l1;
                        table.LeftTitle_Html = string.Join("<br>", in_l1.ToCharArray());
                        table.LeftTitle_Word = string.Join("\n", in_l1.ToCharArray());
                        break;

                    case TableEnum.Single:
                    case TableEnum.Format:
                        table.Title = ClearTitle(in_l1, title);
                        table.Title2 = in_l1;
                        table.LeftTitle = title;
                        if (cfg.IsWMG)
                        {
                            var words = title.Split('(');
                            var text = words[0].Replace("男子組", "").Replace("女子組", "");
                            table.LeftTitle_Html = text;
                        }
                        else
                        {
                            table.LeftTitle_Html = string.Join("<br>", title.ToCharArray());
                        }
                        table.LeftTitle_Word = string.Join("\n", title.ToCharArray());
                        break;

                    default:
                        break;
                }

                map.Add(key, table);
            }
            return table;
        }

        private string ClearLv2(string in_l2)
        {
            if (in_l2 == "") return "";

            return in_l2.Replace("少~", "")
                .Replace("青~", "")
                .Replace("成~", "");

            //in_l2 == "" ? "" : in_l2.Split('-').Last();
        }

        private string ClearTitle(string in_l1, string clear_l2)
        {
            if (in_l1.EndsWith("組") && clear_l2.EndsWith("組"))
            {
                return in_l1.Trim('組') + "-" + clear_l2;
            }
            else
            {
                return in_l1 + "-" + clear_l2;

            }
        }

        /// <summary>
        /// 附加 Row
        /// </summary>
        private void AddRow(TConfig cfg, TTable table, Item item)
        {
            string in_l1 = item.getProperty("in_l1", "");
            string in_l2 = item.getProperty("in_l2", "");
            string in_l3 = item.getProperty("in_l3", "");
            string in_short_name = item.getProperty("in_short_name", "");
            string in_team_count = item.getProperty("in_team_count", "0");
            string in_rank_count = item.getProperty("in_rank_count", "0");
            string in_final_rank = item.getProperty("in_final_rank", "");

            //string program_name2 = item.getProperty("program_name2", "").Replace(in_l2, "");

            string program_name2 = item.getProperty("program_name2", "").Replace(in_l2, "");
            string program_weight = item.getProperty("program_weight", "");
            //+ "(" + in_rank_count + ")";

            var key = "";

            switch (table.TableType)
            {
                case TableEnum.Multiple:
                    key = in_l2;
                    break;

                case TableEnum.Single:
                case TableEnum.Format:
                    key = in_l3;
                    break;

                default:
                    break;
            }

            TRow row = null;
            if (table.Rows.ContainsKey(key))
            {
                row = table.Rows[key];
            }
            else
            {
                row = new TRow
                {
                    Key = key,
                    Title = in_l3,
                    Title_Weight = program_weight,
                    No = table.Rows.Count + 1,
                    Rank1 = new TRank(),
                    Rank2 = new TRank(),
                    Rank3 = new TRank(),
                    Rank4 = new TRank(),
                    Rank5 = new TRank(),
                    Rank6 = new TRank(),
                    Rank7 = new TRank(),
                    Rank8 = new TRank(),
                    program_id = item.getProperty("program_id", ""),
                    in_team_count = in_team_count,
                    in_rank_count = in_rank_count,
                };

                Item itmMaxRank = cfg.inn.applyMethod("in_meeting_program_rank", "<in_team_count>" + in_rank_count + "</in_team_count>");
                row.max_rank = GetIntVal(itmMaxRank.getProperty("max_rank", "0"));

                switch (table.TableType)
                {
                    case TableEnum.Multiple:
                        var c = in_l3.Contains("男") ? '男' : '女';
                        var arr = in_l3.Split(c);

                        //row.Title = in_l3 + "(" + row.in_team_count + ")";
                        row.Title = in_l3;
                        row.Title_Html = string.Join("<br>" + c, arr);
                        row.Title_Word = string.Join("\n" + c, arr);
                        break;

                    case TableEnum.Single:
                    case TableEnum.Format:
                        row.Title = program_name2;//in_l3.Split('：').First();
                        // row.Title_Html = row.Title + "(" + row.in_team_count + ")";
                        // row.Title_Word = row.Title + "(" + row.in_team_count + ")";
                        row.Title_Html = in_l3.Split('：').First() + " (" + in_rank_count + ")";
                        row.Title_Word = row.Title;
                        break;

                    default:
                        break;
                }

                table.Rows.Add(key, row);
            }

            switch (in_final_rank)
            {
                case "1":
                    SetRank(cfg, table, row.Rank1, item);
                    break;

                case "2":
                    SetRank(cfg, table, row.Rank2, item);
                    break;

                case "3":
                    if (!row.Rank3.HasSetted)
                    {
                        SetRank(cfg, table, row.Rank3, item);
                    }
                    else
                    {
                        SetRank(cfg, table, row.Rank4, item);
                    }
                    break;

                case "4":
                    SetRank(cfg, table, row.Rank4, item);
                    break;

                case "5":
                    if (!row.Rank5.HasSetted)
                    {
                        SetRank(cfg, table, row.Rank5, item);
                    }
                    else
                    {
                        SetRank(cfg, table, row.Rank6, item);
                    }
                    break;

                case "6":
                    SetRank(cfg, table, row.Rank6, item);
                    break;

                case "7":
                    if (!row.Rank7.HasSetted)
                    {
                        SetRank(cfg, table, row.Rank7, item);
                    }
                    else
                    {
                        SetRank(cfg, table, row.Rank8, item);
                    }
                    break;

                case "8":
                    SetRank(cfg, table, row.Rank8, item);
                    break;


                default:
                    break;
            }
        }

        //設定名次資料
        private void SetRank(TConfig cfg, TTable table, TRank rank, Item item)
        {
            rank.HasSetted = true;

            rank.Value = item;

            switch (table.TableType)
            {
                case TableEnum.Multiple:
                    rank.Text = GetNameInfo2(cfg, item);
                    break;

                case TableEnum.Single:
                case TableEnum.Format:
                    rank.Text = GetNameInfo1(cfg, item);
                    break;

                default:
                    break;
            }
        }

        //個人組、格式組
        private string GetNameInfo1(TConfig cfg, Item item)
        {
            if (item == null)
            {
                return "&nbsp;";
            }

            string in_name = item.getProperty("in_name", "");
            string map_short_org = GetShowOrg(item);

            if (in_name.Contains("("))
            {
                return in_name;
            }
            else if (cfg.isExport)
            {
                return in_name + Environment.NewLine + "(" + map_short_org + ")";
            }
            else
            {
                return "<span style='color: blue'>" + in_name + "</span><br><span style='font-size: 0.9rem'>" + map_short_org + "</span>";
            }
        }

        //團體組
        private string GetNameInfo2(TConfig cfg, Item item)
        {
            if (item == null)
            {
                return "&nbsp;";
            }

            string map_short_org = GetShowOrg(item);
            string in_team = item.getProperty("in_team", "");
            string in_name = item.getProperty("in_name", "");

            if (in_team == "")
            {
                return map_short_org;
            }
            else
            {
                return map_short_org + "(" + in_team + ")";
            }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string meeting_id { get; set; }
            public string in_date { get; set; }
            public string mode { get; set; }
            public Item itmMeeting { get; set; }
            public string mt_title { get; set; }
            public string mt_battle_type { get; set; }
            public bool isChallenge { get; set; }
            public bool isExport { get; set; }
            public bool IsWMG { get; set; }
        }

        /// <summary>
        /// 表格類型
        /// </summary>
        private enum TableEnum
        {
            /// <summary>
            /// 未設定
            /// </summary>
            None = 0,
            /// <summary>
            /// 團體組
            /// </summary>
            Multiple = 1,
            /// <summary>
            /// 個人組
            /// </summary>
            Single = 2,
            /// <summary>
            /// 格式組
            /// </summary>
            Format = 3,
        }

        /// <summary>
        /// 表格資料模型
        /// </summary>
        private class TTable
        {
            /// <summary>
            /// 鍵
            /// </summary>
            public string Key { get; set; }

            /// <summary>
            /// Table 流水號
            /// </summary>
            public string Id { get; set; }

            /// <summary>
            /// Table 名稱
            /// </summary>
            public string Name { get; set; }

            /// <summary>
            /// Table 標題
            /// </summary>
            public string Title { get; set; }

            /// <summary>
            /// Table 標題
            /// </summary>
            public string Title2 { get; set; }

            /// <summary>
            /// Table 左側標題
            /// </summary>
            public string LeftTitle { get; set; }

            /// <summary>
            /// Table 左側標題
            /// </summary>
            public string LeftTitle_Html { get; set; }

            /// <summary>
            /// Table 左側標題
            /// </summary>
            public string LeftTitle_Word { get; set; }

            /// <summary>
            /// 賽事名稱
            /// </summary>
            public string Head { get; set; }

            /// <summary>
            /// 參賽人數
            /// </summary>
            public string TeamCount { get; set; }

            /// <summary>
            /// 列資料集
            /// </summary>
            public Dictionary<string, TRow> Rows { get; set; }

            /// <summary>
            /// 跨列
            /// </summary>
            public int RowSpan { get; set; }

            /// <summary>
            /// 表格類型
            /// </summary>
            public TableEnum TableType { get; set; }

            /// <summary>
            /// 是否為結束表格
            /// </summary>
            public bool IsEndTable { get; set; }
        }

        /// <summary>
        /// 列模式
        /// </summary>
        private class TRow
        {
            /// <summary>
            /// 鍵
            /// </summary>
            public string Key { get; set; }

            /// <summary>
            /// 列序
            /// </summary>
            public int No { get; set; }

            /// <summary>
            /// 項目名稱
            /// </summary>
            public string Title { get; set; }

            /// <summary>
            /// 項目名稱
            /// </summary>
            public string Title_Html { get; set; }

            /// <summary>
            /// 項目名稱
            /// </summary>
            public string Title_Word { get; set; }

            /// <summary>
            /// 量級
            /// </summary>
            public string Title_Weight { get; set; }

            /// <summary>
            /// 冠軍
            /// </summary>
            public TRank Rank1 { get; set; }

            /// <summary>
            /// 亞軍
            /// </summary>
            public TRank Rank2 { get; set; }

            /// <summary>
            /// 季軍
            /// </summary>
            public TRank Rank3 { get; set; }

            /// <summary>
            /// 第 4 名 or 季軍
            /// </summary>
            public TRank Rank4 { get; set; }

            /// <summary>
            /// 第 5 名
            /// </summary>
            public TRank Rank5 { get; set; }

            /// <summary>
            /// 第 6 名
            /// </summary>
            public TRank Rank6 { get; set; }

            /// <summary>
            /// 第 7 名
            /// </summary>
            public TRank Rank7 { get; set; }

            /// <summary>
            /// 第 8 名
            /// </summary>
            public TRank Rank8 { get; set; }

            public string program_id { get; set; }
            public string in_team_count { get; set; }
            public string in_rank_count { get; set; }
            public int max_rank { get; set; }
        }

        /// <summary>
        /// 名次資料模型
        /// </summary>
        private class TRank
        {
            public string Text { get; set; }

            public Item Value { get; set; }

            public bool HasSetted { get; set; }
        }

        /// <summary>
        /// 匯出資料模型
        /// </summary>
        private class TExport
        {
            /// <summary>
            /// 樣板來源完整路徑
            /// </summary>
            public string Source { get; set; }

            /// <summary>
            /// 檔案名稱
            /// </summary>
            public string File { get; set; }

            /// <summary>
            /// 檔案網址
            /// </summary>
            public string Url { get; set; }
        }

        /// <summary>
        /// 組態
        /// </summary>
        private class TRptSetting
        {
            /// <summary>
            /// 字型名稱
            /// </summary>
            public string FontName { get; set; }

            /// <summary>
            /// 字型大小
            /// </summary>
            public int FontSize { get; set; }

            /// <summary>
            /// 樣板列數( Head 加 Body)
            /// </summary>
            public int RowCount { get; set; }

            /// <summary>
            /// 樣板列數( Body)
            /// </summary>
            public int BodyRowCount { get; set; }

            /// <summary>
            /// 列起始位置
            /// </summary>
            public int RowStart { get; set; }

            /// <summary>
            /// 欄起始位置
            /// </summary>
            public int ColStart { get; set; }

            /// <summary>
            /// 當前列位置
            /// </summary>
            public int CurrentRow { get; set; }

            /// <summary>
            /// 欄位清單
            /// </summary>
            public List<TField> Fields { get; set; }

            /// <summary>
            /// 賽事名稱
            /// </summary>
            public string MeetingTitle { get; set; }

            public int SheetCount { get; set; }

            public List<string> Sheets { get; set; }

            public int WsRow { get; set; }

            public int WsCol { get; set; }

            public string[] Heads { get; set; }

        }

        /// <summary>
        /// 欄位
        /// </summary>
        private class TField
        {
            /// <summary>
            /// 標題
            /// </summary>
            public string Title { get; set; }
            /// <summary>
            /// 屬性
            /// </summary>
            public string Property { get; set; }
            /// <summary>
            /// 格式
            /// </summary>
            public string Format { get; set; }

            /// <summary>
            /// 欄寬
            /// </summary>
            public int Width { get; set; }

            public Func<TRptSetting, Item, TField, string> getValue { get; set; }
        }

        private string GetShowOrg(Item item)
        {
            var in_show_org = item.getProperty("in_show_org", "");
            if (in_show_org != "") return in_show_org;
            return item.getProperty("map_short_org", "");
        }

        private string GetMtVariableValue(TConfig cfg, string in_key)
        {
            var sql = @"
                SELECT 
	                in_key
	                , in_value
                FROM 
	                IN_MEETING_VARIABLE WITH(NOLOCK) 
                WHERE
	                source_id = '{#meeting_id}'
	                AND in_key = '{#in_key}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_key}", in_key);

            var itmResult = cfg.inn.applySQL(sql);
            if (itmResult.isError() || itmResult.getResult() == "") return "";
            return itmResult.getProperty("in_value", "");
        }

        /// <summary>
        /// 轉換為整數
        /// </summary>
        private int GetIntVal(string value, int def = 0)
        {
            int result = def;
            Int32.TryParse(value, out result);
            return result;
        }

        /// <summary>
        /// 轉換為日期時間
        /// </summary>
        private string GetDateTimeVal(string value, string format)
        {
            DateTime result = DateTime.MinValue;
            DateTime.TryParse(value, out result);
            if (result != DateTime.MinValue)
            {
                return result.ToString(format);
            }
            return "";
        }
    }
}