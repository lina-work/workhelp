﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Judo.Fight
{
    public class in_meeting_rank_sumpage_111nmssport : Item
    {
        public in_meeting_rank_sumpage_111nmssport(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
    目的: 晉段報表
    日誌: 
        - 2024-12-11: 創建 (lina)
*/

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "In_Meeting_User_WinEvent2";

            var itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                in_year = itmR.getProperty("in_year", ""),
                in_type = itmR.getProperty("in_type", ""),
                in_status = itmR.getProperty("in_status", ""),
                scene = itmR.getProperty("scene", ""),
                type1_win_count = 4,
                type2_win_count = 5,
                type3_win_count = 4,
                type2_rank_count = 2,
            };

            if (cfg.in_year == "") cfg.in_year = "2024";

            switch (cfg.scene)
            {
                case "page":
                    Page(cfg, itmR);
                    break;

                case "team-modal":
                    Modal(cfg, true, itmR);
                    break;

                case "solo-modal":
                    Modal(cfg, false, itmR);
                    break;

                case "analysis":
                    Analysis(cfg, itmR);
                    break;

                case "remove":
                    RemoveReport(cfg, itmR);
                    break;

                case "update_report_status":
                    UpdateReportStatus(cfg, itmR);
                    break;

                case "edit_win_count":
                    EditWinCount(cfg, itmR);
                    break;

                case "export":
                    ExportXlsx(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void ExportXlsx(TConfig cfg, Item itmReturn)
        {
            cfg.head_color = System.Drawing.Color.FromArgb(232, 232, 234);
            AppendExportInfo(cfg);

            AppendTargetMeetingList(cfg);

            var items = GetExportItem(cfg);
            var count = items.getItemCount();
            if (count <= 0) throw new Exception("查無晉段報表資料");

            var rows = MapReportRows(cfg, items);
            var book = new Spire.Xls.Workbook();

            var sheet = book.CreateEmptySheet();
            sheet.Name = "晉升初段-" + cfg.year;

            SetStrCell(cfg, sheet, "A1:A2", "No.", 12, 0, true, true, HA.C, CT.Head);
            SetStrCell(cfg, sheet, "B1:B2", "姓名", 12, 0, true, true, HA.C, CT.Head);
            SetStrCell(cfg, sheet, "C1:C2", "身分證號", 12, 0, true, true, HA.C, CT.Head);
            SetStrCell(cfg, sheet, "D1:D2", "生日", 12, 0, true, true, HA.C, CT.Head);
            SetStrCell(cfg, sheet, "E1:E2", "性別", 12, 0, true, true, HA.C, CT.Head);
            SetStrCell(cfg, sheet, "F1:F2", "類型", 12, 0, true, true, HA.C, CT.Head);
            SetStrCell(cfg, sheet, "G1:H1", "全中運", 12, 0, true, true, HA.C, CT.Head);
            SetStrCell(cfg, sheet, "I1:J1", "全大運", 12, 0, true, true, HA.C, CT.Head);
            SetStrCell(cfg, sheet, "K1:L1", "全柔錦", 12, 0, true, true, HA.C, CT.Head);
            SetStrCell(cfg, sheet, "M1:N1", "中正盃", 12, 0, true, true, HA.C, CT.Head);

            SetStrCell(cfg, sheet, "G2", "名次", 12, 0, true, false, HA.C, CT.Head);
            SetStrCell(cfg, sheet, "H2", "勝場數", 12, 0, true, false, HA.C, CT.Head);
            SetStrCell(cfg, sheet, "I2", "名次", 12, 0, true, false, HA.C, CT.Head);
            SetStrCell(cfg, sheet, "J2", "勝場數", 12, 0, true, false, HA.C, CT.Head);
            SetStrCell(cfg, sheet, "K2", "名次", 12, 0, true, false, HA.C, CT.Head);
            SetStrCell(cfg, sheet, "L2", "勝場數", 12, 0, true, false, HA.C, CT.Head);
            SetStrCell(cfg, sheet, "M2", "名次", 12, 0, true, false, HA.C, CT.Head);
            SetStrCell(cfg, sheet, "N2", "勝場數", 12, 0, true, false, HA.C, CT.Head);

            SetStrCell(cfg, sheet, "O1:O2", "狀態", 12, 0, true, true, HA.C, CT.Head);


            var ri = 3;
            var no = 1;
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                SetStrCell(cfg, sheet, "A" + ri, no.ToString(), 12, 0, false, false, HA.C);
                SetStrCell(cfg, sheet, "B" + ri, item.getProperty("in_name", ""), 12, 0, false, false, HA.C);
                SetStrCell(cfg, sheet, "C" + ri, item.getProperty("in_sno", ""), 12, 0, false, false, HA.C);
                SetStrCell(cfg, sheet, "D" + ri, item.getProperty("show_birth", ""), 12, 0, false, false, HA.C);
                SetStrCell(cfg, sheet, "E" + ri, item.getProperty("in_gender", ""), 12, 0, false, false, HA.C);
                SetStrCell(cfg, sheet, "F" + ri, item.getProperty("in_type", ""), 12, 0, false, false, HA.C);

                SetStrCell(cfg, sheet, "G" + ri, item.getProperty("in_game_01_rank", ""), 12, 0, false, false, HA.C);
                SetStrCell(cfg, sheet, "H" + ri, item.getProperty("in_game_01_wcnt", ""), 12, 0, false, false, HA.C);

                SetStrCell(cfg, sheet, "I" + ri, item.getProperty("in_game_02_rank", ""), 12, 0, false, false, HA.C);
                SetStrCell(cfg, sheet, "J" + ri, item.getProperty("in_game_02_wcnt", ""), 12, 0, false, false, HA.C);

                SetStrCell(cfg, sheet, "K" + ri, item.getProperty("in_game_11_rank", ""), 12, 0, false, false, HA.C);
                SetStrCell(cfg, sheet, "L" + ri, item.getProperty("in_game_11_wcnt", ""), 12, 0, false, false, HA.C);

                SetStrCell(cfg, sheet, "M" + ri, item.getProperty("in_game_12_rank", ""), 12, 0, false, false, HA.C);
                SetStrCell(cfg, sheet, "N" + ri, item.getProperty("in_game_12_wcnt", ""), 12, 0, false, false, HA.C);

                SetStrCell(cfg, sheet, "O" + ri, item.getProperty("in_status", ""), 12, 0, false, false, HA.C);

                ri++;
                no++;
            }

            SetRangeBorder(sheet, "A1:O" + (ri - 1));

            sheet.Columns[0].ColumnWidth = 6;
            sheet.Columns[1].ColumnWidth = 20;
            sheet.Columns[2].ColumnWidth = 14;
            sheet.Columns[3].ColumnWidth = 13;
            sheet.Columns[4].ColumnWidth = 9;
            sheet.Columns[5].ColumnWidth = 9;

            sheet.Columns[6].ColumnWidth = 9;
            sheet.Columns[7].ColumnWidth = 9;

            sheet.Columns[8].ColumnWidth = 9;
            sheet.Columns[9].ColumnWidth = 9;

            sheet.Columns[10].ColumnWidth = 9;
            sheet.Columns[11].ColumnWidth = 9;

            sheet.Columns[12].ColumnWidth = 9;
            sheet.Columns[13].ColumnWidth = 9;

            sheet.Columns[14].ColumnWidth = 9;

            sheet.FreezePanes(3, 3);

            sheet.PageSetup.PrintTitleRows = "$1:$2";
            sheet.PageSetup.CenterHorizontally = true;

            AppendExportResult(cfg, book, itmReturn);

        }

        private void AppendExportResult(TConfig cfg, Spire.Xls.Workbook book, Item itmReturn)
        {
            if (book.Worksheets.Count > 1) book.Worksheets[0].Remove();
            if (book.Worksheets.Count > 1) book.Worksheets[0].Remove();
            if (book.Worksheets.Count > 1) book.Worksheets[0].Remove();

            //匯出檔名
            var xlsName = "晉段報表"
                + "_" + cfg.year
                + "_" + System.Guid.NewGuid().ToString().Substring(0, 4).ToUpper()
                + "_" + System.DateTime.Now.ToString("yyyyMMddHHmmss");

            xlsName = xlsName.Replace("+", "")
                            .Replace("-", "")
                            .Replace(".", "")
                            .Replace(" ", "");

            string extName = cfg.export_type != "" && cfg.export_type.ToLower() == "pdf" ? ".pdf" : ".xlsx";
            string xls_file = cfg.export_path + xlsName + extName;
            string xls_name = xlsName + extName;

            if (!System.IO.Directory.Exists(cfg.export_path))
            {
                System.IO.Directory.CreateDirectory(cfg.export_path);
            }

            //儲存檔案          
            if (extName == ".pdf")
            {
                book.SaveToFile(xls_file, Spire.Xls.FileFormat.PDF);
            }
            else
            {
                book.SaveToFile(xls_file, Spire.Xls.ExcelVersion.Version2013);
            }

            //下載路徑
            itmReturn.setProperty("xls_name", xls_name);
        }

        private void SetStrCell(TConfig cfg, Spire.Xls.Worksheet sheet
            , string pos
            , string text
            , int size
            , int height = 0
            , bool needBold = false
            , bool needMerge = false
            , HA ha = HA.C
            , CT color = CT.None)
        {
            var range = sheet.Range[pos];
            if (needMerge) range.Merge();
            range.Text = text;
            SetUtlCell(cfg, range, size, height, needBold, ha, color);
        }

        private void SetUtlCell(TConfig cfg, Spire.Xls.CellRange range
            , int size
            , int height
            , bool needBold
            , HA ha = HA.C
            , CT color = CT.None)
        {
            range.Style.Font.FontName = cfg.font_name;
            range.Style.Font.Size = size;

            if (needBold) range.Style.Font.IsBold = true;
            if (height > 0) range.RowHeight = height;

            switch (color)
            {
                case CT.Head: range.Style.Color = cfg.head_color; break;
            }

            range.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;

            switch (ha)
            {
                case HA.C:
                    range.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                    break;
                case HA.L:
                    range.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Left;
                    break;
                case HA.R:
                    range.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Right;
                    break;
            }
        }

        private enum HA
        {
            L = 100,
            C = 200,
            R = 300
        }

        private enum CT
        {
            None = 1,
            Head = 100,
            Blue = 200,
        }

        //設定格線
        private void SetRangeBorder(Spire.Xls.Worksheet sheet, string pos)
        {
            var range = sheet.Range[pos];
            range.BorderInside(Spire.Xls.LineStyleType.Thin, System.Drawing.Color.Black);
            range.BorderAround(Spire.Xls.LineStyleType.Thin, System.Drawing.Color.Black);
        }

        private void AppendExportInfo(TConfig cfg)
        {
            var xls_parm_name = "";
            cfg.itmXlsx = cfg.inn.applyMethod("In_Variable_File", "<in_name>" + xls_parm_name + "</in_name>");
            cfg.template_path = cfg.itmXlsx.getProperty("template_path", "");
            cfg.export_path = cfg.itmXlsx.getProperty("export_path", "");
            cfg.export_type = "xlsx";
            cfg.font_name = "Source Sans Pro";
        }

        private Item GetExportItem(TConfig cfg)
        {
            var cond = cfg.in_type == "" ? "" : "AND t1.in_type = '" + cfg.in_type + "'";

            var sql = @"
                SELECT 
	                t1.*
	                , CONVERT(VARCHAR, DATEADD(HOUR, 8, t1.in_birth), 111) AS 'show_birth'
	                , t2.id AS 'resume_id'
                FROM 
	                IN_RESUME_DEGREE_REPORT t1 WITH(NOLOCK)
	            LEFT OUTER JOIN
	                IN_RESUME t2 WITH(NOLOCK)
	                ON t2.login_name = t1.in_sno
                WHERE
	                t1.in_year = {#year}
	                AND t1.in_degree_lv = '無'
		            AND DATEADD(HOUR, 8, t1.in_birth) <= '{#max_birth}'
	                AND t1.in_status = '可晉段'
		            {#cond}
                ORDER BY
	                t1.in_sno
	                , t1.in_type
            ";

            sql = sql.Replace("{#year}", cfg.year.ToString())
                .Replace("{#max_birth}", cfg.max_birth)
                .Replace("{#cond}", cond);

            return cfg.inn.applySQL(sql);
        }

        private void EditWinCount(TConfig cfg, Item itmReturn)
        {
            var id = itmReturn.getProperty("id", "");
            var no = itmReturn.getProperty("no", "");
            var count = itmReturn.getProperty("count", "");
            var prop = "in_game_" + no + "_wcnt";
            var sql = "UPDATE IN_RESUME_DEGREE_REPORT SET [" + prop + "] = " + count + " WHERE id = '" + id + "'";
            cfg.inn.applySQL(sql);
        }
        private void Modal(TConfig cfg, bool isTeam, Item itmReturn)
        {
            var meeting_id = itmReturn.getProperty("meeting_id", "");
            var resume_id = itmReturn.getProperty("resume_id", "");

            var sql = "SELECT id, UPPER(login_name) AS 'login_name' FROM IN_RESUME WITH(NOLOCK) WHERE id = '" + resume_id + "'";
            var itmResume = cfg.inn.applySQL(sql);

            var in_sno = itmResume.getProperty("login_name", "");

            var items = isTeam
                ? GetTeamModalEventItems(cfg, meeting_id, in_sno)
                : GetSoloModalEventItems(cfg, meeting_id, in_sno);

            var count = items.getItemCount();

            var modal_head = " ";
            if (count > 0) modal_head = items.getItemByIndex(0).getProperty("mt_title", "");
            itmReturn.setProperty("modal_head", modal_head);

            var model_note = " ";
            switch (cfg.in_type)
            {
                case "第1類": model_note = "參加全國段外組<span style='color:red'>連獲4勝</span>(含六都比賽)；連勝，不含敗部及棄權勝"; break;
                case "第2類": model_note = "1年內全國段外組<span style='color:red'>2次</span>前三名，且累勝場數達<span style='color:red'>5勝</span>以上者"; break;
                case "第3類": model_note = "團體勝乙組獲全國前3名之隊伍，以有出賽之隊員，且勝場連勝4勝"; break;
            }
            itmReturn.setProperty("modal_note", model_note);

            itmReturn.setProperty("modal_table", GenerateModalTable(cfg, items));
        }

        private string GenerateModalTable(TConfig cfg, Item items)
        {
            var hCss = "text-center bg-warning";

            var body = new StringBuilder();
            var count = items.getItemCount();
            var lastKey = "";
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var mt_id = item.getProperty("mt_id", "");
                var pg_id = item.getProperty("pg_id", "");
                var in_l1 = item.getProperty("in_l1", "");
                var in_l2 = item.getProperty("in_l2", "").Replace("個-", "").Replace("團-", "");
                var in_l3 = item.getProperty("in_l3", "");
                var in_status = item.getProperty("in_status", "");
                var in_player_name = item.getProperty("in_player_name", "").Split('(').First();
                var opp_name = item.getProperty("opp_name", "").Split('(').First();
                var opp_org = item.getProperty("opp_org", "");
                var opp_status = item.getProperty("opp_status", "");
                var in_target_bypass = item.getProperty("in_target_bypass", "");

                var no_fight = opp_name == "" && in_target_bypass == "1";
                if (no_fight) opp_name = "<span style='color:red'>輪空</span>";

                var key = in_l1 + in_l2 + in_l3;
                var needHead = false;
                if (key != lastKey)
                {
                    needHead = true;
                    lastKey = key;
                }

                var status_text = "未打";
                if (no_fight) status_text = "未打";
                else if (in_status == "1") status_text = "<span style='color: blue'>勝</span>";
                else if (in_status == "0") status_text = "<span style='color: red'>敗</span>";

                var site_name = "第" + item.getProperty("in_site_code", "") + "場地";

                var win_local_time = "<span style='color: blue;'>" + item.getProperty("in_win_local_time", "") + "</span>";

                // var l3_link = in_l3
                //   + " <a href='../pages/public/FightTreeV1.html?meeting_id=" + mt_id + "&program_id=" + pg_id + "' target='_blank'"
                //   + "     rel='noopener noreferrer'>"
                //   + "     <i class='fa fa-link'></i>"
                //   + " </a>";

                var l3_link = in_l3
                  + " <a href='c.aspx?page=in_competition_preview.html&method=in_meeting_program_preview&meeting_id=" + mt_id + "&program_id=" + pg_id + "' target='_blank'"
                  + "     rel='noopener noreferrer'>"
                  + "     <i class='fa fa-link'></i>"
                  + " </a>";

                if (opp_status != "") opp_status = " <BR><span style='color: red;'>" + opp_status + "</span>";
                var opp_display = opp_name + opp_status;

                var tree_name = GetTreeName(cfg, item);

                if (needHead)
                {
                    body.Append("<tr>");
                    body.Append("<th class='" + hCss + "' data-field='in_final_rank'>名次</th>");
                    body.Append("<th class='" + hCss + "' data-field='in_l1'>競賽項目</th>");
                    body.Append("<th class='" + hCss + "' data-field='in_l2'>競賽組別</th>");
                    body.Append("<th class='" + hCss + "' data-field='in_l3'>競賽量級</th>");
                    body.Append("<th class='" + hCss + "' data-field='in_date_key'>比賽日期</th>");
                    body.Append("<th class='" + hCss + "' data-field='in_site_code'>比賽場地</th>");
                    body.Append("<th class='" + hCss + "' data-field='in_tree_no'>場次</th>");
                    body.Append("<th class='" + hCss + "' data-field='in_player_org'>單位</th>");
                    body.Append("<th class='" + hCss + "' data-field='in_player_name'>選手</th>");
                    body.Append("<th class='" + hCss + "' data-field='in_status'>勝敗</th>");
                    body.Append("<th class='" + hCss + "' data-field='in_win_local_time'>時間</th>");
                    body.Append("<th class='" + hCss + "' data-field='opp_name'>對手</th>");
                    body.Append("<th class='" + hCss + "' data-field='opp_org'>對手單位</th>");
                    body.Append("<th class='" + hCss + "' data-field='tree_name'>部別</th>");
                    body.Append("</tr>");
                }

                body.Append("<tr>");
                AddModalTableCol(body, item, "in_final_rank");
                AddModalTableCol(body, item, "in_l1");
                AddModalTableCol(body, in_l2, "in_l2");
                AddModalTableCol(body, l3_link, "in_l3");
                AddModalTableCol(body, item, "in_date_key");
                AddModalTableCol(body, site_name, "in_site_code");
                AddModalTableCol(body, item, "in_tree_no");
                AddModalTableCol(body, item, "in_player_org");
                AddModalTableCol(body, in_player_name, "in_player_name");
                AddModalTableCol(body, status_text, "in_status");
                AddModalTableCol(body, win_local_time, "in_win_local_time");
                AddModalTableCol(body, opp_display, "opp_name");
                AddModalTableCol(body, item, "opp_org");
                AddModalTableCol(body, tree_name, "tree_name");
                body.Append("</tr>");
            }

            var builder = new StringBuilder();
            builder.Append("<table id='modal-data-table' class='table table-bordered table-hover table-striped' data-toggle='table'  data-show-toggle='false'>");
            builder.Append("  <tbody>");
            builder.Append(body);
            builder.Append("  </tbody>");
            builder.Append("</table>");
            return builder.ToString();
        }

        private string GetTreeName(TConfig cfg, Item item)
        {
            var in_tree_name = item.getProperty("in_tree_name", "");
            switch (in_tree_name)
            {
                case "main": return "勝部";
                case "repechage": return "<span style='color: orange'>敗部</span>";
                default: return in_tree_name;
            }
        }

        private void AddModalTableCol(StringBuilder body, string value, string property)
        {
            body.Append("<td class='text-center' data-field='" + property + "'>" + value + "</td>");
        }

        private void AddModalTableCol(StringBuilder body, Item item, string property)
        {
            body.Append("<td class='text-center' data-field='" + property + "'>" + item.getProperty(property, "") + "</td>");
        }

        private void Page(TConfig cfg, Item itmReturn)
        {
            AppendTargetMeetingList(cfg);

            var items = GetReportItem(cfg);
            var rows = MapReportRows(cfg, items);

            var evtTitle = "勝場數";
            if (cfg.in_type == "第3類") evtTitle = "場次數";

            var head = new StringBuilder();
            head.Append("<tr>");
            head.Append("  <th rowspan='2' colspan='1' class='text-center bg-primary'>NO.</th>");
            head.Append("  <th rowspan='2' colspan='1' class='text-center bg-primary'>姓名</th>");
            head.Append("  <th rowspan='2' colspan='1' class='text-center bg-primary'>證號</th>");
            head.Append("  <th rowspan='2' colspan='1' class='text-center bg-primary'>性別</th>");
            head.Append("  <th rowspan='2' colspan='1' class='text-center bg-primary'>生日</th>");
            head.Append("  <th rowspan='2' colspan='1' class='text-center bg-primary'>段別</th>");
            head.Append("  <th rowspan='2' colspan='1' class='text-center bg-primary'>類型</th>");
            head.Append("  <th rowspan='1' colspan='2' class='text-center bg-primary'>全中運</th>");
            head.Append("  <th rowspan='1' colspan='2' class='text-center bg-primary'>全大運</th>");
            head.Append("  <th rowspan='1' colspan='2' class='text-center bg-primary'>全柔錦</th>");
            head.Append("  <th rowspan='1' colspan='2' class='text-center bg-primary'>中正盃</th>");
            head.Append("  <th rowspan='2' colspan='1' class='text-center bg-primary'>功能</th>");
            head.Append("  <th rowspan='2' colspan='1' class='text-center bg-primary'>晉段<BR>狀態</th>");
            head.Append("</tr>");
            head.Append("<tr>");
            head.Append("  <th rowspan='1' colspan='1' class='text-center bg-primary inno-head-fix'>名次</th>");
            head.Append("  <th rowspan='1' colspan='1' class='text-center bg-primary'>" + evtTitle + "</th>");
            head.Append("  <th rowspan='1' colspan='1' class='text-center bg-primary'>名次</th>");
            head.Append("  <th rowspan='1' colspan='1' class='text-center bg-primary'>" + evtTitle + "</th>");
            head.Append("  <th rowspan='1' colspan='1' class='text-center bg-primary'>名次</th>");
            head.Append("  <th rowspan='1' colspan='1' class='text-center bg-primary'>" + evtTitle + "</th>");
            head.Append("  <th rowspan='1' colspan='1' class='text-center bg-primary'>名次</th>");
            head.Append("  <th rowspan='1' colspan='1' class='text-center bg-primary'>" + evtTitle + "</th>");
            head.Append("</tr>");

            var lastSno = "";
            var no = 0;
            var body = new StringBuilder();
            for (var i = 0; i < rows.Count; i++)
            {
                var x = rows[i];
                var trCss = "";
                var needNo = false;
                if (x.in_sno != lastSno)
                {
                    no++;
                    lastSno = x.in_sno;
                    needNo = true;
                    trCss = "class='inno-row-tr'";
                }
                if (i == 0) trCss = "";

                body.Append("<tr " + trCss + ">");
                body.Append("  <td class='text-center'>" + (needNo ? no.ToString() : "") + "</td>");

                body.Append("  <td class='text-center'>" + x.in_name + "</td>");
                body.Append("  <td class='text-center'>" + x.in_sno + "</td>");
                body.Append("  <td class='text-center'>" + x.in_gender + "</td>");
                body.Append("  <td class='text-center'>" + x.show_birth + "</td>");
                body.Append("  <td class='text-center'>" + x.in_degree_lv + "</td>");

                body.Append("  <td class='text-center inno-row-td'>" + x.in_type + "</td>");

                body.Append("  <td class='text-center' data-mtno='01'>" + GetWinRankLink(cfg, x, "01") + "</td>");
                body.Append("  <td class='text-center inno-row-td' data-mtno='01'>" + GetWinCountLink(cfg, x, "01") + "</td>");

                body.Append("  <td class='text-center' data-mtno='02'>" + GetWinRankLink(cfg, x, "02") + "</td>");
                body.Append("  <td class='text-center inno-row-td' data-mtno='02'>" + GetWinCountLink(cfg, x, "02") + "</td>");

                body.Append("  <td class='text-center' data-mtno='11'>" + GetWinRankLink(cfg, x, "11") + "</td>");
                body.Append("  <td class='text-center inno-row-td' data-mtno='11'>" + GetWinCountLink(cfg, x, "11") + "</td>");

                body.Append("  <td class='text-center' data-mtno='12'>" + GetWinRankLink(cfg, x, "12") + "</td>");
                body.Append("  <td class='text-center inno-row-td' data-mtno='12'>" + GetWinCountLink(cfg, x, "12") + "</td>");

                body.Append("  <td class='text-center'>" + GetButtonLink(cfg, x) + "</td>");
                body.Append("  <td class='text-center'>" + GetStatusLink(cfg, x) + "</td>");

                body.Append("</tr>");
            }

            var table_name = "data-table";

            var builder = new StringBuilder();
            builder.Append("<div class='box-body tablenobg' style='position: relative'>");
            builder.AppendLine(GetTableAttribute(table_name));
            builder.Append("<thead>");
            builder.Append(head);
            builder.Append("</thead>");
            builder.Append("<tbody>");
            builder.Append(body);
            builder.Append("</tbody>");
            builder.AppendLine("</table>");
            builder.Append("</div>");

            itmReturn.setProperty("inn_table", builder.ToString());
        }

        private string GetStatusLink(TConfig cfg, TRptRow x)
        {
            var css = x.in_status == "不晉段" ? "bg-danger" : "";

            var value = "<label class='inno-status-" + x.id + " " + css + "'>"
                + x.in_status
                + "</label>";
            return value;
        }

        private string GetButtonLink(TConfig cfg, TRptRow x)
        {
            var btn1 = "<button class='btn btn-sm btn-default' onclick='ChangeStatus(this)'"
                + " data-id='" + x.id + "'"
                + " data-status='可晉段'>可晉段"
                + "</button>";

            var btn2 = " <button class='btn btn-sm btn-default' onclick='ChangeStatus(this)'"
                + " data-id='" + x.id + "'"
                + " data-status='不晉段'>不晉段"
                + "</button>";

            return btn1 + btn2;
        }

        private string GetWinRankLink(TConfig cfg, TRptRow x, string code)
        {
            var rank = x.item.getProperty("in_game_" + code + "_rank", "");
            if (!x.isType2) return rank;

            switch (rank)
            {
                case "1": return "<i class='fa fa-trophy' style='color: gold'></i>";
                case "2": return "<i class='fa fa-trophy' style='color: silver'></i>";
                case "3": return "<i class='fa fa-trophy' style='color: #B87333'></i>";
                default: return rank;
            }
        }

        private string GetWinCountLink(TConfig cfg, TRptRow x, string code)
        {
            var rank = x.item.getProperty("in_game_" + code + "_rank", "");
            var wcnt = x.item.getProperty("in_game_" + code + "_wcnt", "");

            var b1 = rank == "" || rank == "0";
            var b2 = wcnt == "" || wcnt == "0";
            if (b1 && b2) return "";

            var value = wcnt;
            if (!b1 && b2) value = "0";

            var meeting_id = "";
            switch (code)
            {
                case "01": meeting_id = cfg.mt01.id; break;
                case "02": meeting_id = cfg.mt02.id; break;
                case "03": meeting_id = cfg.mt03.id; break;
                case "04": meeting_id = cfg.mt04.id; break;
                case "11": meeting_id = cfg.mt11.id; break;
                case "12": meeting_id = cfg.mt12.id; break;
            }

            var input = "<span class='inno-win-count' onclick='EditWinCount(this)' data-id='" + x.id + "' data-old='" + value + "'>" + value + "</span>";

            // var link1 = " <a href='javascript:;' onclick='FindPlayer(this)'"
            //         + " data-meeting='" + meeting_id + "'"
            //         + " data-name='" + x.in_name + "'"
            //         + "><i class='fa fa-user'></i></a>";

            var link1 = "";

            if (x.isType3)
            {
                return input + link1
                    + " <a href='javascript:;' onclick='FindModalEvent(this)'"
                    + " data-meeting='" + meeting_id + "'"
                    + " data-resume='" + x.resume_id + "'"
                    + " data-type='" + x.in_type + "'"
                    + " data-modal='team-modal'"
                    + "><i class='fa fa-link'></i></a>";
            }
            else
            {
                return input + link1
                    + " <a href='javascript:;' onclick='FindModalEvent(this)'"
                    + " data-meeting='" + meeting_id + "'"
                    + " data-resume='" + x.resume_id + "'"
                    + " data-type='" + x.in_type + "'"
                    + " data-modal='solo-modal'"
                    + "><i class='fa fa-link'></i></a>";
            }
        }

        private string GetTableAttribute(string table_name)
        {
            return "<table id='" + table_name + "'"
                + " class='table table-hover table-striped'"
                + " data-toggle='table'"
                + " data-show-toggle='false'"
                + " data-show-columns='false'"
                + " data-search-align='right'"
                + " data-sort-stable='true'"
                + " data-search='true'"
                + ">";
        }

        private void UpdateReportStatus(TConfig cfg, Item itmReturn)
        {
            var id = itmReturn.getProperty("id", "");
            var in_status = itmReturn.getProperty("in_status", "");
            //if (in_status == "不晉段") in_status = "";
            var sql = "UPDATE IN_RESUME_DEGREE_REPORT SET in_status = '" + in_status + "' WHERE id = '" + id + "'";
            cfg.inn.applySQL(sql);
        }

        //清倉儲
        private void RemoveReport(TConfig cfg, Item itmReturn)
        {
            var sql = "DELETE FROM IN_RESUME_DEGREE_REPORT WHERE in_year = " + cfg.in_year;
            cfg.inn.applySQL(sql);
        }

        //過倉儲
        private void Analysis(TConfig cfg, Item itmReturn)
        {
            AppendTargetMeetingList(cfg);
            AnalysisTypeA(cfg, itmReturn);
            AnalysisTypeB(cfg, itmReturn);
            AnalysisTypeC(cfg, itmReturn);
        }

        private void AnalysisTypeC(TConfig cfg, Item itmReturn)
        {
            //團體勝乙組獲全國前3名之隊伍，以有出賽之隊員，且勝場連勝4勝
            for (var i = 0; i < cfg.meetingList.Count; i++)
            {
                var meeting = cfg.meetingList[i];
                if (meeting.id == "") continue;

                var items = GetTeamPlayerItems(cfg, meeting.id, meeting.title);
                var map = GetTeamEventMap(cfg, meeting.id, meeting.title);

                var count = items.getItemCount();
                if (count <= 0) continue;

                for (var j = 0; j < count; j++)
                {
                    var item = items.getItemByIndex(j);
                    AppendReportItemType3(cfg, meeting, item, map);
                }
            }
        }

        private void AnalysisTypeA(TConfig cfg, Item itmReturn)
        {
            //參加全國段外組連獲4勝。(含六都比賽) 
            //(連勝，不含敗部及棄權勝) 
            for (var i = 0; i < cfg.meetingList.Count; i++)
            {
                var meeting = cfg.meetingList[i];
                if (meeting.id == "") continue;

                var itmPrograms = GetProgramItems(cfg, meeting.id);
                var count = itmPrograms.getItemCount();
                if (count <= 0) continue;

                for (var j = 0; j < count; j++)
                {
                    var itmProgram = itmPrograms.getItemByIndex(j);
                    AnalysisTypeAEvent(cfg, meeting, itmProgram);
                }
            }
        }

        private void AnalysisTypeAEvent(TConfig cfg, TMeeting meeting, Item itmProgram)
        {
            var program_id = itmProgram.getProperty("id", "");
            var team_count = GetInt(itmProgram.getProperty("in_team_count", "0"));
            if (team_count <= 4) return;
            var items = GetEventItemsByProgram(cfg, program_id);
            var count = items.getItemCount();
            if (count <= 0) return;

            var map = new Dictionary<string, TEvt>();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var evt_id = item.getProperty("id", "");
                var winner_no = item.getProperty("in_win_sign_no", "");
                var next_evt = item.getProperty("in_next_win", "");
                if (map.ContainsKey(evt_id)) continue;
                map.Add(evt_id, new TEvt
                {
                    evt_id = evt_id,
                    winner_no = winner_no,
                    wcnt = 0,
                    next_evt = next_evt,
                    item = item,
                });
            }

            var matched = new Dictionary<string, Item>();
            foreach (var kv in map)
            {
                var evt = kv.Value;
                if (evt.winner_no == "" || evt.winner_no == "0") continue;
                evt.wcnt++;
                FindNextWinner(cfg, map, evt, evt.next_evt);
                if (evt.wcnt >= cfg.type1_win_count)
                {
                    if (matched.ContainsKey(evt.winner_no)) continue;
                    evt.item.setProperty("win_count", evt.wcnt.ToString());
                    matched.Add(evt.winner_no, evt.item);
                }
            }

            foreach (var kv in matched)
            {
                var item = kv.Value;
                AppendReportItemType1(cfg, meeting, item);
            }
        }

        private void FindNextWinner(TConfig cfg, Dictionary<string, TEvt> map, TEvt evt, string next_id)
        {
            if (map.ContainsKey(next_id))
            {
                var next = map[next_id];
                if (next.winner_no == evt.winner_no)
                {
                    evt.wcnt++;
                    FindNextWinner(cfg, map, evt, next.next_evt);
                }
            }
        }

        private void AnalysisTypeB(TConfig cfg, Item itmReturn)
        {
            //1年內獲全國段外組2次前3名且累勝場數達 5 勝以上者。
            for (var i = 0; i < cfg.meetingList.Count; i++)
            {
                var meeting = cfg.meetingList[i];
                if (meeting.id == "") continue;
                var items = GetRankItems(cfg, meeting.id, meeting.title);
                var count = items.getItemCount();
                if (count <= 0) continue;

                var map = GetSectEventMap(cfg, meeting.id);
                if (map.Count == 0) continue;

                for (var j = 0; j < count; j++)
                {
                    var item = items.getItemByIndex(j);
                    AppendReportItemType2(cfg, meeting, item, map);
                }
            }
        }

        private void AppendReportItemType1(TConfig cfg, TMeeting meeting, Item item)
        {
            item.setProperty("in_game_" + meeting.px + "_rank", item.getProperty("in_final_rank", "0"));
            item.setProperty("in_game_" + meeting.px + "_wcnt", item.getProperty("win_count", "0"));
            MergeReport(cfg, meeting, item, "第1類");
        }

        private void AppendReportItemType2(TConfig cfg, TMeeting meeting, Item item, Dictionary<string, TSect> map)
        {
            item.setProperty("in_game_" + meeting.px + "_rank", item.getProperty("in_final_rank", "0"));
            item.setProperty("in_game_" + meeting.px + "_wcnt", GetWinEventCount(cfg, item, map));
            MergeReport(cfg, meeting, item, "第2類");
        }

        private void AppendReportItemType3(TConfig cfg, TMeeting meeting, Item item, Dictionary<string, TTeamPlayer> map)
        {
            var in_sno = item.getProperty("in_sno", "");
            if (!map.ContainsKey(in_sno)) return;

            var player = map[in_sno];
            var ecount = player.items.Count;
            if (ecount < cfg.type3_win_count) return;

            item.setProperty("in_game_" + meeting.px + "_rank", item.getProperty("in_final_rank", "0"));
            item.setProperty("in_game_" + meeting.px + "_wcnt", ecount.ToString());
            MergeReport(cfg, meeting, item, "第3類");
        }

        private void MergeReport(TConfig cfg, TMeeting meeting, Item item, string in_type)
        {
            var in_sno = item.getProperty("in_sno", "");
            var in_name = item.getProperty("in_name", "").Split('(').First();
            var in_birth = item.getProperty("in_birth", "").Replace("00:00:00.000", "").Replace("/", "-");
            var in_degree_lv = item.getProperty("history_degree_lv", "");
            var in_degree_id = item.getProperty("history_degree_id", "");
            if (in_degree_lv == "") in_degree_lv = "無";
            if (in_name == "") return;

            var itmNew = cfg.inn.newItem("In_Resume_Degree_Report", "merge");
            itmNew.setAttribute("where", "in_sno = '" + in_sno + "' AND in_type = '" + in_type + "'");

            itmNew.setProperty("in_type", in_type);
            itmNew.setProperty("in_name", in_name);
            itmNew.setProperty("in_gender", item.getProperty("in_gender", ""));
            itmNew.setProperty("in_sno", in_sno);
            itmNew.setProperty("in_birth", in_birth);
            itmNew.setProperty("in_year", cfg.year.ToString());
            itmNew.setProperty("in_degree_lv", in_degree_lv);
            itmNew.setProperty("in_degree_id", in_degree_id);

            var p1 = "in_game_" + meeting.px + "_rank";
            var p2 = "in_game_" + meeting.px + "_wcnt";
            itmNew.setProperty(p1, item.getProperty(p1, ""));
            itmNew.setProperty(p2, item.getProperty(p2, ""));

            itmNew.apply();
        }

        private string GetWinEventCount(TConfig cfg, Item item, Dictionary<string, TSect> map)
        {
            var zero = "0";
            var pg_id = item.getProperty("pg_id", "");
            var in_sign_no = item.getProperty("in_sign_no", "");

            if (pg_id == "") return zero;
            if (in_sign_no == "" || in_sign_no == "0") return zero;
            if (!map.ContainsKey(pg_id)) return zero;

            var sub = map[pg_id];
            if (!sub.map.ContainsKey(in_sign_no)) return zero;
            return sub.map[in_sign_no].Count.ToString();
        }

        private void AppendTargetMeetingList(TConfig cfg)
        {
            cfg.year = GetInt(cfg.in_year);
            cfg.age = 14;
            cfg.max_birth = (cfg.year - cfg.age) + "-12-31";

            cfg.meetingList = new List<TMeeting>
            {
                new TMeeting { px = "01", id = "BCA302C39E264A2B84D062AAA880B022", title = "中華民國113年全國中等學校運動會" },
                new TMeeting { px = "02", id = "9EE567C80DE1463FA736627D59AB61DF", title = "中華民國113年全國大專校運運動會" },
                new TMeeting { px = "03", id = "", title = "" },
                new TMeeting { px = "04", id = "", title = "" },
                new TMeeting { px = "11", id = "56C1317640BE4839B36EACA08466DAFD", title = "中華民國柔道總會113年全國柔道錦標賽" },
                new TMeeting { px = "12", id = "95C1607DDA434EFD9BE95FDC042A49D2", title = "113 年全國中正盃柔道錦標賽" },
            };

            cfg.mt01 = cfg.meetingList[0];
            cfg.mt02 = cfg.meetingList[1];
            cfg.mt03 = cfg.meetingList[2];
            cfg.mt04 = cfg.meetingList[3];
            cfg.mt11 = cfg.meetingList[4];
            cfg.mt12 = cfg.meetingList[5];
        }

        private Dictionary<string, TSect> GetSectEventMap(TConfig cfg, string meeting_id)
        {
            var map = new Dictionary<string, TSect>();
            var items = GetEventItemsByMeeting(cfg, meeting_id);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var pg_id = item.getProperty("pg_id", "");
                var in_sign_no = item.getProperty("in_win_sign_no", "");
                var evt_id = item.getProperty("evt_id", "");
                if (!map.ContainsKey(pg_id))
                {
                    map.Add(pg_id, new TSect
                    {
                        pg_id = pg_id,
                        map = new Dictionary<string, List<TEvt>>()
                    });
                }
                var sect = map[pg_id];
                if (!sect.map.ContainsKey(in_sign_no))
                {
                    sect.map.Add(in_sign_no, new List<TEvt>());
                }
                sect.map[in_sign_no].Add(new TEvt
                {
                    evt_id = evt_id
                });
            }
            return map;
        }

        private class TTeamPlayer
        {
            public string in_sno { get; set; }
            public List<Item> items { get; set; }
        }

        private Dictionary<string, TTeamPlayer> GetTeamEventMap(TConfig cfg, string meeting_id, string title)
        {
            var map = new Dictionary<string, TTeamPlayer>();
            var items = GetTeamEventItems(cfg, meeting_id, title);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var in_sno = item.getProperty("in_player_sno", "");
                if (!map.ContainsKey(in_sno))
                {
                    map.Add(in_sno, new TTeamPlayer
                    {
                        in_sno = in_sno,
                        items = new List<Item>(),
                    });
                }
                map[in_sno].items.Add(item);
            }
            return map;
        }

        private Item GetTeamEventItems(TConfig cfg, string meeting_id, string title)
        {
            var sql = @"
                SELECT 
	                t1.id AS 'pg_id'
	                , t1.in_l1
	                , t1.in_l2
	                , t1.in_l3
	                , t3.id AS 'sub_did'
	                , t3.in_player_org
	                , t3.in_player_name
	                , UPPER(t3.in_player_sno) AS 'in_player_sno'
	                , t3.in_status
	                , t2.in_tree_no
	                , t2.in_fight_id
	                , t2.in_site_code
	                , t2.in_date_key
                FROM 
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PEVENT t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t3 WITH(NOLOCK)
	                ON t3.source_id = t2.id
                WHERE 
	                t1.in_meeting = '{#meeting_id}'
	                AND t1.in_l1 = '團體組' 
	                AND t1.in_l2 NOT LIKE '%甲%'
	                AND t1.in_l2 NOT LIKE '%公開%'
	                AND t1.in_l2 NOT LIKE '%特別%'
	                AND t2.in_type = 's'
	                AND ISNULL(t3.in_player_org, '') <> ''
	                AND ISNULL(t3.in_player_name, '') NOT IN ('', '沒有選手', '第6位')
                ORDER BY
	                t1.in_sort_order
	                , t3.in_player_org
	                , t3.in_player_name
	                , t2.in_tree_no
            ";

            sql = sql.Replace("{#meeting_id}", meeting_id)
                .Replace("{#meeting_title}", title)
                .Replace("{#max_birth}", cfg.max_birth);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        private Item GetTeamPlayerItems(TConfig cfg, string meeting_id, string title)
        {
            var sql = @"
                SELECT
	                t1.id			AS 'pg_id'
	                , t1.in_name2	AS 'pg_name'
	                , t1.in_l1
	                , t1.in_l2
	                , t1.in_l3
	                , t1.in_team_count
	                , t2.in_current_org
	                , t2.in_team
	                , t2.in_final_rank
	                , t3.id AS 'muid'
	                , t3.in_name
	                , UPPER(t3.in_sno) AS 'in_sno'
	                , CONVERT(VARCHAR, DATEADD(HOUR, 8, t3.in_birth), 111) AS 'in_birth'
	                , t3.in_gender
	                , t3.in_weight_status
	                , t11.in_degree	        AS 'resume_degree_lv'
	                , t11.in_degree_id	    AS 'resume_degree_id'
	                , t12.in_sheet_name	    AS 'history_degree_lv'
	                , t12.in_degree_id	    AS 'history_degree_id'
                FROM 
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK) 
                INNER JOIN
	                IN_MEETING_PTEAM t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
	                AND t2.in_type = 'p'
                INNER JOIN
	                IN_MEETING_USER t3 WITH(NOLOCK)
	                ON t3.source_id = t1.in_meeting
	                AND t3.in_l1 = t1.in_l1
	                AND t3.in_l2 = t1.in_l2
	                AND t3.in_l3 = t1.in_l3
	                AND t3.in_index = t2.in_index
	                AND t3.in_creator_sno = t2.in_creator_sno
                INNER JOIN
	                IN_RESUME t11 WITH(NOLOCK)
	                ON t11.login_name = t3.in_sno
                LEFT JOIN
	                AAA_Degree_Record_Fix t12 WITH(NOLOCK)
	                ON t12.in_sno = t3.in_sno
                WHERE 
	                t1.in_meeting = '{#meeting_id}' -- {#meeting_title}
	                AND t1.in_l1 = '團體組' 
	                AND t1.in_l2 NOT LIKE '%甲%'
	                AND t1.in_l2 NOT LIKE '%公開%'
	                AND t1.in_l2 NOT LIKE '%特別%'
	                AND t1.in_team_count > 8
	                AND ISNULL(t2.in_final_rank, 0) BETWEEN 1 AND 3
	                AND ISNULL(t3.in_weight_status, '') = 'on'
		            AND DATEADD(HOUR, 8, t3.in_birth) <= '{#max_birth}'
                ORDER BY
	                t1.in_sort_order
	                , t2.in_final_rank
            ";

            sql = sql.Replace("{#meeting_id}", meeting_id)
                .Replace("{#meeting_title}", title)
                .Replace("{#max_birth}", cfg.max_birth);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        private Item GetRankItems(TConfig cfg, string meeting_id, string title)
        {
            var sql = @"
	            SELECT 
					 t1.id AS 'pg_id'
		             , t3.in_name
		             , UPPER(t3.in_sno) AS 'in_sno'
		             , t3.in_gender
		             , CONVERT(VARCHAR, DATEADD(HOUR, 8, t3.in_birth), 111) AS 'in_birth'
		             , t2.in_final_rank
		             , t1.in_l1
		             , t1.in_l2
		             , t1.in_l3
		             , t3.in_sign_no
					 , t11.in_degree	    AS 'resume_degree_lv'
					 , t11.in_degree_id	    AS 'resume_degree_id'
					 , t12.in_sheet_name	AS 'history_degree_lv'
					 , t12.in_degree_id	    AS 'history_degree_id'
	            FROM
		            IN_MEETING_PROGRAM t1 WITH(NOLOCK)
	            INNER JOIN
		            IN_MEETING_PTEAM t2 WITH(NOLOCK)
		            ON t2.source_id = t1.id
	            INNER JOIN
		            IN_MEETING_USER t3 WITH(NOLOCK)
		            ON t3.source_id = t1.in_meeting
		            AND t3.in_l1 = t1.in_l1
		            AND t3.in_l2 = t1.in_l2
		            AND t3.in_l3 = t1.in_l3
		            AND t3.in_index = t2.in_index
		            AND t3.in_creator_sno = t2.in_creator_sno
				INNER JOIN
					IN_RESUME t11 WITH(NOLOCK)
					ON t11.login_name = t2.in_sno
				LEFT JOIN
					AAA_Degree_Record_Fix t12 WITH(NOLOCK)
					ON t12.in_sno = t2.in_sno
	            WHERE
		            t1.in_meeting = '{#meeting_id}' -- {#meeting_title}
		            AND t1.in_l1 <> '團體組'
		            AND t1.in_l1 <> '格式組'
		            AND t1.in_l2 NOT LIKE '%甲%'
		            AND t1.in_l2 NOT LIKE '%公開%'
		            AND ISNULL(t2.in_final_rank, 0) BETWEEN 1 AND 3
		            AND ISNULL(t2.in_sno, '') <> ''
		            AND DATEADD(HOUR, 8, t3.in_birth) <= '{#max_birth}'
				ORDER BY
					t2.in_sno
					, t12.in_sheet_no
            ";

            sql = sql.Replace("{#meeting_id}", meeting_id)
                .Replace("{#meeting_title}", title)
                .Replace("{#max_birth}", cfg.max_birth);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        private Item GetProgramItems(TConfig cfg, string meeting_id)
        {
            var sql = @"
                SELECT
	                t1.id
	                , t1.in_name
	                , t1.in_l1
	                , t1.in_l2
	                , t1.in_l3
	                , t1.in_team_count
                FROM
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                WHERE
	                t1.in_meeting = '{#meeting_id}'
	                AND t1.in_l1 <> '團體組'
	                AND t1.in_l2 NOT LIKE '%甲%'
	                AND t1.in_l2 NOT LIKE '%公開%'
	                AND t1.in_l2 NOT LIKE '%特別%'
                ORDER BY
	                in_sort_order
            ";

            sql = sql.Replace("{#meeting_id}", meeting_id);

            return cfg.inn.applySQL(sql);
        }

        private Item GetEventItemsByProgram(TConfig cfg, string program_id)
        {
            var sql = @"
                SELECT
	                t2.id
	                , t2.in_tree_name
	                , t2.in_tree_id
	                , t2.in_fight_id
	                , t2.in_tree_no
	                , t2.in_win_status
	                , t2.in_win_sign_no
	                , t2.in_next_win
	                , t2.in_suspend
	                , t3.in_name
	                , UPPER(t3.in_sno) AS 'in_sno'
	                , CONVERT(VARCHAR, DATEADD(HOUR, 8, t11.in_birth), 111) AS 'in_birth'
	                , t11.in_gender
	                , t3.in_sign_no
	                , t3.in_final_rank
	                , t11.in_degree	    AS 'resume_degree_lv'
	                , t11.in_degree_id	AS 'resume_degree_id'
	                , t12.in_sheet_name	AS 'history_degree_lv'
	                , t12.in_degree_id	AS 'history_degree_id'
                FROM
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PEVENT t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                LEFT JOIN
	                IN_MEETING_PTEAM t3 WITH(NOLOCK)
	                ON t3.source_id = t1.id
	                AND t3.in_sign_no = t2.in_win_sign_no
                LEFT JOIN
	                IN_RESUME t11 WITH(NOLOCK)
	                ON t11.login_name = t3.in_sno
                LEFT JOIN
	                AAA_Degree_Record_Fix t12 WITH(NOLOCK)
	                ON t12.in_sno = t3.in_sno
                WHERE
	                t1.id = '{#program_id}'
	                AND ISNULL(t2.in_tree_no, 0) > 0
	                AND t2.in_win_status NOT IN ('bypass', 'cancel', 'nofight')
	                AND t2.in_tree_name NOT IN ('repechage')
                ORDER BY
	                t2.in_tree_no
            ";

            sql = sql.Replace("{#program_id}", program_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        private Item GetEventItemsByMeeting(TConfig cfg, string meeting_id)
        {
            var sql = @"
                SELECT
	                t1.in_meeting
	                , t1.source_id  AS 'pg_id'
	                , t1.id			AS 'evt_id'
	                , t1.id
	                , t1.in_tree_sno
	                , t1.in_tree_name
	                , t1.in_tree_no
	                , t1.in_fight_id
	                , t1.in_win_status
	                , t1.in_win_sign_no
	                , t1.in_win_local_time
	                , t1.in_suspend
                FROM
	                IN_MEETING_PEVENT t1 WITH(NOLOCK)
                WHERE
	                t1.in_meeting = '{#meeting_id}'
	                AND ISNULL(t1.in_tree_no, 0) > 0
	                AND t1.in_win_status NOT IN ('bypass', 'cancel', 'nofight')
                ORDER BY
	                t1.source_id
            ";

            sql = sql.Replace("{#meeting_id}", meeting_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        private Item GetReportItem(TConfig cfg)
        {
            var condType = cfg.in_type == "" ? "" : "AND t1.in_type = '" + cfg.in_type + "'";

            var condStatus = "";
            switch (cfg.in_status)
            {
                case "全部":
                case "":
                    break;
                case "未分析":
                    condStatus = "AND ISNULL(t1.in_status, '') = ''";
                    break;
                default:
                    condStatus = "AND ISNULL(t1.in_status, '') = '" + cfg.in_status + "'";
                    break;
            }

            var sql = @"
                SELECT 
	                t1.*
	                , CONVERT(VARCHAR, DATEADD(HOUR, 8, t1.in_birth), 111) AS 'show_birth'
	                , t2.id AS 'resume_id'
                FROM 
	                IN_RESUME_DEGREE_REPORT t1 WITH(NOLOCK)
	            LEFT OUTER JOIN
	                IN_RESUME t2 WITH(NOLOCK)
	                ON t2.login_name = t1.in_sno
                WHERE
	                t1.in_year = {#year}
	                AND t1.in_degree_lv = '無'
		            AND DATEADD(HOUR, 8, t1.in_birth) <= '{#max_birth}'
		            {#condType}
		            {#condStatus}
                ORDER BY
	                t1.in_sno
	                , t1.in_type
            ";

            sql = sql.Replace("{#year}", cfg.year.ToString())
                .Replace("{#max_birth}", cfg.max_birth)
                .Replace("{#condType}", condType)
                .Replace("{#condStatus}", condStatus);

            return cfg.inn.applySQL(sql);
        }

        private Item GetSoloModalEventItems(TConfig cfg, string meeting_id, string in_sno)
        {
            var sql = @"
                SELECT 
                    t0.id           AS 'mt_id'
                    , t0.in_title   AS 'mt_title'
	                , t1.id         AS 'pg_id'
	                , t1.in_l1
	                , t1.in_l2
	                , t1.in_l3
	                , t3.id AS 'sub_did'
	                , t3.in_status
	                , t2.in_tree_name
	                , t2.in_tree_no
	                , t2.in_win_local
	                , t2.in_win_local_time
	                , t2.in_site_code
	                , t2.in_date_key
					, t3.in_target_bypass
					, t4.in_name        AS 'in_player_name'
					, UPPER(t4.in_sno)  AS 'in_player_sno'
					, t4.in_short_org   AS 'in_player_org'
					, t4.in_final_rank
					, t5.in_name			AS 'opp_name'
					, t5.in_short_org		AS 'opp_org'
					, t5.in_weight_message	AS 'opp_status'
                FROM 
	                IN_MEETING t0 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
	                ON t1.in_meeting = t0.id
                INNER JOIN
	                IN_MEETING_PEVENT t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t3 WITH(NOLOCK)
	                ON t3.source_id = t2.id
				INNER JOIN
					IN_MEETING_PTEAM t4 WITH(NOLOCK) 
					ON t4.source_id = t1.id
					AND t4.in_sign_no = t3.in_sign_no
				LEFT OUTER JOIN
					IN_MEETING_PTEAM t5 WITH(NOLOCK) 
					ON t5.source_id = t1.id
					AND t5.in_sign_no = t3.in_target_no
                WHERE 
	                t1.in_meeting = '{#meeting_id}'
	                AND t1.in_l1 <> '團體組' 
	                AND t1.in_l2 NOT LIKE '%甲%'
	                AND t1.in_l2 NOT LIKE '%公開%'
	                AND t1.in_l2 NOT LIKE '%特別%'
	                AND ISNULL(t2.in_tree_no, 0) > 0
	                AND ISNULL(t2.in_win_status, '') NOT IN ('bypass', 'cancel', 'nofight')
					AND t4.in_sno = '{#in_sno}'
                ORDER BY
	                t1.in_fight_day
	                , t2.in_tree_no
            ";

            sql = sql.Replace("{#meeting_id}", meeting_id)
                .Replace("{#in_sno}", in_sno);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        private Item GetTeamModalEventItems(TConfig cfg, string meeting_id, string in_sno)
        {
            var sql = @"
                SELECT 
                    t0.id           AS 'mt_id'
                    , t0.in_title   AS 'mt_title'
	                , t1.id         AS 'pg_id'
	                , t1.in_l1
	                , t1.in_l2
	                , t1.in_l3
	                , t3.id AS 'sub_did'
	                , t3.in_player_org
	                , t3.in_player_name
	                , UPPER(t3.in_player_sno) AS 'in_player_sno'
	                , t3.in_status
	                , t2.in_tree_no
	                , t2.in_win_local
	                , t2.in_win_local_time
	                , t2.in_site_code
	                , t2.in_date_key
                FROM 
	                IN_MEETING t0 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
	                ON t1.in_meeting = t0.id
                INNER JOIN
	                IN_MEETING_PEVENT t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t3 WITH(NOLOCK)
	                ON t3.source_id = t2.id
                WHERE 
	                t1.in_meeting = '{#meeting_id}'
	                AND t1.in_l1 = '團體組' 
	                AND t1.in_l2 NOT LIKE '%甲%'
	                AND t1.in_l2 NOT LIKE '%公開%'
	                AND t1.in_l2 NOT LIKE '%特別%'
	                AND t2.in_type = 's'
	                AND ISNULL(t3.in_player_sno, '') = '{#in_sno}'
                ORDER BY
	                t1.in_sort_order
	                , t3.in_player_org
	                , t3.in_player_name
	                , t2.in_tree_no
            ";

            sql = sql.Replace("{#meeting_id}", meeting_id)
                .Replace("{#in_sno}", in_sno);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string in_year { get; set; }
            public string in_type { get; set; }
            public string in_status { get; set; }
            public string scene { get; set; }

            public int year { get; set; }
            public int age { get; set; }
            public string max_birth { get; set; }
            public int type1_win_count { get; set; }
            public int type2_win_count { get; set; }
            public int type3_win_count { get; set; }
            public int type2_rank_count { get; set; }
            public List<TMeeting> meetingList { get; set; }

            public TMeeting mt01 { get; set; }
            public TMeeting mt02 { get; set; }
            public TMeeting mt03 { get; set; }
            public TMeeting mt04 { get; set; }
            public TMeeting mt11 { get; set; }
            public TMeeting mt12 { get; set; }

            public System.Drawing.Color head_color { get; set; }

            public Item itmXlsx { get; set; }
            public string template_path { get; set; }
            public string export_path { get; set; }
            public string export_type { get; set; }
            public string font_name { get; set; }
        }

        private class TMeeting
        {
            public string id { get; set; }
            public string px { get; set; }
            public string title { get; set; }
        }

        private class TSect
        {
            public string pg_id { get; set; }
            public Dictionary<string, List<TEvt>> map { get; set; }
        }

        private class TEvt
        {
            public string evt_id { get; set; }
            public string winner_no { get; set; }
            public string next_evt { get; set; }
            public int wcnt { get; set; }
            public Item item { get; set; }
        }

        private class TRptRow
        {
            public string id { get; set; }
            public string in_name { get; set; }
            public string in_sno { get; set; }
            public string in_gender { get; set; }
            public string show_birth { get; set; }
            public string in_degree_id { get; set; }
            public string in_degree_lv { get; set; }
            public string in_type { get; set; }
            public string in_status { get; set; }
            public string resume_id { get; set; }

            public Item item { get; set; }

            public bool matched { get; set; }
            public bool isType1 { get; set; }
            public bool isType2 { get; set; }
            public bool isType3 { get; set; }

            public TRptSub game01 { get; set; }
            public TRptSub game02 { get; set; }
            public TRptSub game03 { get; set; }
            public TRptSub game04 { get; set; }
            public TRptSub game11 { get; set; }
            public TRptSub game12 { get; set; }
        }

        public class TRptSub
        {
            public bool existed { get; set; }
            public int rank { get; set; }
            public int wcnt { get; set; }
        }

        private TRptRow NewRptRow(TConfig cfg, Item item)
        {
            var x = new TRptRow
            {
                id = item.getProperty("id", ""),
                in_name = item.getProperty("in_name", ""),
                in_sno = item.getProperty("in_sno", "").ToUpper(),
                in_gender = item.getProperty("in_gender", ""),
                show_birth = item.getProperty("show_birth", ""),
                in_degree_id = item.getProperty("in_degree_id", ""),
                in_degree_lv = item.getProperty("in_degree_lv", ""),
                in_type = item.getProperty("in_type", ""),
                in_status = item.getProperty("in_status", ""),
                resume_id = item.getProperty("resume_id", ""),
                item = item,
            };
            return x;
        }

        private List<TRptRow> MapReportRows(TConfig cfg, Item items)
        {
            var rows = new List<TRptRow>();
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var x = MapReportRow(cfg, item);
                if (!x.matched) continue;
                rows.Add(x);
            }
            return rows;
        }

        private TRptRow MapReportRow(TConfig cfg, Item item)
        {
            var x = NewRptRow(cfg, item);

            switch (x.in_type)
            {
                case "第1類":
                    x.isType1 = true;
                    x.matched = true;
                    break;
                case "第2類":
                    x.isType2 = true;
                    x.matched = IsType2Matched(cfg, item);
                    break;
                case "第3類":
                    x.isType3 = true;
                    x.matched = IsType3Matched(cfg, item);
                    break;
            }
            return x;
        }

        private bool IsType3Matched(TConfig cfg, Item item)
        {
            var b = false;
            for (var i = 0; i < cfg.meetingList.Count; i++)
            {
                var meeting = cfg.meetingList[i];
                var wcnt_p = "in_game_" + meeting.px + "_wcnt";
                var wcnt_v = item.getProperty(wcnt_p, "0");
                var value = GetInt(wcnt_v);
                if (value >= cfg.type3_win_count)
                {
                    b = true;
                    break;
                }
            }
            return b;
        }

        private bool IsType2Matched(TConfig cfg, Item item)
        {
            int medalCount = 0;
            for (var i = 0; i < cfg.meetingList.Count; i++)
            {
                var meeting = cfg.meetingList[i];
                var rank_p = "in_game_" + meeting.px + "_rank";
                var rank_v = item.getProperty(rank_p, "0");
                var value = GetInt(rank_v);
                if (value >= 1 && value <= 3) medalCount++;
            }
            return medalCount >= cfg.type2_rank_count;
        }

        //private Dictionary<string, TRptRow> MapExportRows(TConfig cfg, Item items)
        //{
        //    var map = new Dictionary<string, TRptRow>();
        //    var count = items.getItemCount();
        //    for (var i = 0; i < count; i++)
        //    {
        //        var item = items.getItemByIndex(i);
        //        var in_sno = item.getProperty("in_sno", "").ToUpper();
        //        if (!map.ContainsKey(in_sno))
        //        {
        //            map.Add(in_sno, NewRptRow(cfg, item));
        //        }
        //        var x = map[in_sno];
        //        if (x.)
        //    }
        //    return map;
        //}


        private int GetInt(string value, int defV = 0)
        {
            if (value == "") return 0;
            int result = defV;
            if (int.TryParse(value, out result)) return result;
            return defV;
        }
    }
}