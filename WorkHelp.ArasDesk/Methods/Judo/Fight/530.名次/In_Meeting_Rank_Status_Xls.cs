﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;
using Spire.Xls;

namespace WorkHelp.ArasDesk.Methods.Judo.Fight
{
    public class In_Meeting_Rank_Status_Xls : Item
    {
        public In_Meeting_Rank_Status_Xls(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 可頒獎列表匯出
                日誌: 
                    - 2022-02-10: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "In_Meeting_Rank_Status_Xls";

            var itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                strIdentityId = inn.getUserAliases(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                scene = itmR.getProperty("scene", ""),
            };


            cfg.itmMeeting = cfg.inn.applySQL("SELECT in_title FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            cfg.mt_title = cfg.itmMeeting.getProperty("in_title", "");

            return itmR;
        }

        private void Export(TConfig cfg, Item itmReturn)
        {
            var exp = ExportInfo(cfg, "rank_path");
            exp.export_Path = exp.export_Path.Replace("meeting_excel", "meeting_medal");
            if (!System.IO.Directory.Exists(exp.export_Path))
            {
                System.IO.Directory.CreateDirectory(exp.export_Path);
            }

            var book = new Spire.Xls.Workbook();
            Spire.Xls.Worksheet sheet = book.CreateEmptySheet();
            sheet.Name = "可頒獎列表";
            sheet.PageSetup.PaperSize = Spire.Xls.PaperSizeType.PaperA4;

            //將所有欄放入單一頁面
            sheet.PageSetup.FitToPagesWide = 1;

            //頁面配置-邊界
            sheet.PageSetup.TopMargin = 0.4;
            sheet.PageSetup.BottomMargin = 0.5;
            sheet.PageSetup.LeftMargin = 0.5;
            sheet.PageSetup.RightMargin = 0.4;


            var rows = GetGroups(cfg);
            SetXlsCell(cfg, sheet, "A1:B1", cfg.mt_title, true, 24, 40, false, false, false);
            SetXlsCell(cfg, sheet, "A2", "量級", true, 18, 32, true, false, false);
            SetXlsCell(cfg, sheet, "B2", "出賽人數", true, 18, 32, true, false, false);


            var ridx = 3;
            for (var i = 0; i< rows.Count; i++)
            {
                var row = rows[i];
                SetXlsCell(cfg, sheet, "A" + ridx, row.in_name3, true, 14, 26, true, false, false);
                SetXlsCell(cfg, sheet, "B" + ridx, row.in_rank_count, true, 14, 26, true, false, false);
                ridx++;
            }

            SetRangeBorder(sheet, "A1" + ":" + "B" + (ridx - 1));

            sheet.Columns[1].ColumnWidth = 30;

            book.Worksheets[0].Remove();
            book.Worksheets[0].Remove();
            book.Worksheets[0].Remove();

            string ext_name = ".xlsx";
            string xls_name = cfg.mt_title + "_可頒獎列表_" + DateTime.Now.ToString("MMdd_HHmmss");

            string xls_file = exp.export_Path + "\\" + xls_name + ext_name;
            string xls_url = xls_name + ext_name;

            book.SaveToFile(xls_file, Spire.Xls.ExcelVersion.Version2013);

            itmReturn.setProperty("xls_name", xls_name + ".xlsx");
        }

        private void SetXlsCell(TConfig cfg
            , Spire.Xls.Worksheet sheet
            , string pos
            , string value
            , bool isMerge = false
            , int fontSize = 0
            , int height = 0
            , bool isBold = false
            , bool isNum = false
            , bool isLeft = false)
        {
            var range = sheet.Range[pos];
            if (isMerge) range.Merge();

            if (isLeft)
            {
                range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Left;
            }
            else
            {
                range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
            }
            range.Style.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;

            range.Text = value;
            range.Style.Font.FontName = isNum ? "Times New Roman" : "標楷體";
            if (fontSize > 0) range.Style.Font.Size = fontSize;
            if (isBold) range.Style.Font.IsBold = true;
            if (height > 0) range.RowHeight = height;
        }

        //設定格線
        private void SetRangeBorder(Spire.Xls.Worksheet sheet, string pos)
        {
            var range = sheet.Range[pos];
            range.BorderInside(Spire.Xls.LineStyleType.Thin, System.Drawing.Color.Black);
            range.BorderAround(Spire.Xls.LineStyleType.Thin, System.Drawing.Color.Black);
        }

        private List<TGroup> GetGroups(TConfig cfg)
        {
            var rows = new List<TGroup>();
            var items = MedalItems(cfg);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var row = new TGroup 
                {
                    in_l1 = item.getProperty("in_l1", ""),
                    in_l2 = item.getProperty("in_l2", ""),
                    in_l3 = item.getProperty("in_l3", ""),
                    in_name2 = item.getProperty("in_name2", ""),
                    in_name3 = item.getProperty("in_name2", ""),
                    in_short_name = item.getProperty("in_short_name", ""),
                    in_team_count = item.getProperty("in_team_count", "0"),
                    in_rank_count = item.getProperty("in_rank_count", "0"),
                };
                rows.Add(row);
            }
            return rows;
        }

        private class TGroup
        {
            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }
            public string in_name2 { get; set; }
            public string in_name3 { get; set; }
            public string in_short_name { get; set; }
            public string in_weight { get; set; }
            public string in_team_count { get; set; }
            public string in_rank_count { get; set; }
        }

        private Item MedalItems(TConfig cfg)
        {
            string sql = @"
                SELECT
	                t1.id
	                , t1.in_l1
	                , t1.in_l2
	                , t1.in_l3
	                , t1.in_name2
	                , t1.in_short_name
	                , t1.in_weight
	                , t1.in_team_count
	                , t1.in_rank_count
	                , t1.in_battle_type
                    , t1.in_director_status
	                , DATEADD(HOUR, 8, t1.in_director_time) AS 'in_director_time'
                    , t1.in_certificate_status
	                , DATEADD(HOUR, 8, t1.in_certificate_time) AS 'in_certificate_time'
                    , t1.in_medal_status
	                , DATEADD(HOUR, 8, t1.in_medal_time) AS 'in_medal_time'
                    , t1.in_end_tree_no
                FROM
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                WHERE 
	                t1.in_meeting = '{#meeting_id}'
	                AND t1.in_fight_day = '{#in_fight_day}'
	                AND t1.in_team_count > 1
	                AND t1.in_medal_status = N'可頒獎'
                ORDER BY
                    t1.in_sort_order
	                --t1.in_certificate_time DESC
            ";


            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_fight_day}", DateTime.Now.ToString("yyyy-MM-dd"));

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        private TExport ExportInfo(TConfig cfg, string in_name)
        {
            Item itmXls = cfg.inn.applyMethod("In_Meeting_Excel_Path", "<in_name>" + in_name + "</in_name>");
            string Export_Path = itmXls.getProperty("export_path", "");
            string Template_Path = itmXls.getProperty("template_path", "");

            TExport result = new TExport
            {
                template_Path = Template_Path,
                export_Path = Export_Path,
            };

            return result;
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }

            public string meeting_id { get; set; }
            public string scene { get; set; }

            public Item itmMeeting { get; set; }
            public string mt_title { get; set; }
        }

        private class TExport
        {
            public string template_Path { get; set; }
            public string export_Path { get; set; }
        }
    }
}
