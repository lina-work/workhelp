﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Judo.Fight
{
    public class in_meeting_rank_statistics : Item
    {
        public in_meeting_rank_statistics(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 名次積分統計(男女)
                日誌: 
                    - 2022-04-10: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]" + "in_meeting_rank_statistics";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                scene = itmR.getProperty("scene", ""),

                rank1_point = 4,
                rank2_point = 2,
                rank3_point = 0.5m,
                rank5_point = 0,
                rank7_point = 0,
                CharSet = GetCharSet(),
            };

            cfg.itmMeeting = cfg.inn.applySQL("SELECT in_title FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            cfg.mt_title = cfg.itmMeeting.getProperty("in_title", "");

            //取得名次資料
            var items = GetScores(cfg);

            //轉換為統計頁面
            var pages = MapPages(cfg, items);

            //匹配出場次數
            MapRankEvents(cfg, pages);

            //計算成績並排序
            RefreshScore(cfg, pages);

            //匯出 EXCEL
            SumXlsx(cfg, pages, itmR);

            return itmR;
        }

        private void MapRankEvents(TConfig cfg, List<TPage> pages)
        {
            Item items = GetRankEvents(cfg);

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                var row = MapRow(cfg, item);

                var page = pages.Find(x => x.key == row.AgeType);
                if (page == null) continue;

                var block = page.BlockList.Find(x => x.key == row.GenderType);
                if (block == null) continue;

                var org = block.orgs.Find(x => x.short_name == row.map_short_org);
                if (org == null) continue;

                if (org.events == null)
                {
                    org.events = new List<TRow>();
                }

                org.events.Add(row);
            }
        }

        private Item GetRankEvents(TConfig cfg)
        {
            string sql = @"
                SELECT 
	                t1.id              AS 'program_id'
	                , t1.in_name2      AS 'program_name2'
	                , t1.in_short_name AS 'program_short_name'
	                , t11.in_name
	                , t11.in_names
	                , t11.in_current_org
	                , t11.map_short_org
	                , t11.in_team
	                , t11.in_final_rank
	                , t11.in_show_rank
	                , t11.in_sign_no
	                , t2.in_tree_name
	                , t2.in_tree_id
	                , t2.in_tree_no
	                , t2.in_tree_sno
	                , t2.in_round
	                , t2.in_round_code
	                , t2.in_bypass_status
	                , t2.in_win_sign_no
	                , t2.in_win_status
	                , t3.in_sign_foot
					, t4.in_name AS 'site_name'
                FROM 
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
	                VU_MEETING_PTEAM t11 WITH(NOLOCK)
	                ON t11.source_id = t1.id
                INNER JOIN
	                IN_MEETING_PEVENT t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t3 WITH(NOLOCK)
	                ON t3.source_id = t2.id
	                AND t3.in_sign_no = t11.in_sign_no
                INNER JOIN
	                IN_MEETING_SITE t4 WITH(NOLOCK)
	                ON t4.id = t2.in_site
                WHERE
	                t1.in_meeting = '{#meeting_id}'
	                AND ISNULL(t11.in_final_rank, 0) > 0
	                AND ISNULL(t2.in_win_status, '') NOT IN ('', 'bypass', 'cancel')
                ORDER BY
	                t1.in_sort_order
	                , t11.in_final_rank
	                , t2.in_tree_no
	                , t3.in_sign_no
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            return cfg.inn.applySQL(sql);
        }

        private void RefreshScore(TConfig cfg, List<TPage> pages)
        {
            foreach (var page in pages)
            {
                foreach (var block in page.BlockList)
                {
                    foreach (var org in block.orgs)
                    {
                        org.w01_score = GetScores(org.sects, 1);
                        org.w02_score = GetScores(org.sects, 2);
                        org.w03_score = GetScores(org.sects, 3);
                        org.w04_score = GetScores(org.sects, 4);
                        org.w05_score = GetScores(org.sects, 5);
                        org.w06_score = GetScores(org.sects, 6);
                        org.w07_score = GetScores(org.sects, 7);
                        org.w08_score = GetScores(org.sects, 8);
                        org.w09_score = GetScores(org.sects, 9);
                        org.w10_score = GetScores(org.sects, 10);

                        org.total_score = 0
                            + org.w01_score
                            + org.w02_score
                            + org.w03_score
                            + org.w04_score
                            + org.w05_score
                            + org.w06_score
                            + org.w07_score
                            + org.w08_score
                            + org.w09_score
                            + org.w10_score;

                        org.rank01_count = GetRankCount(org.rows, 1);
                        org.rank02_count = GetRankCount(org.rows, 2);
                        org.rank03_count = GetRankCount(org.rows, 3);
                        org.rank05_count = GetRankCount(org.rows, 5);
                        org.rank07_count = GetRankCount(org.rows, 7);

                        org.rank01_events = GetRankCount(org.events, 1);
                        org.rank02_events = GetRankCount(org.events, 2);
                        org.rank03_events = GetRankCount(org.events, 3);
                        org.rank05_events = GetRankCount(org.events, 5);
                        org.rank07_events = GetRankCount(org.events, 7);
                    }
                }
            }

            foreach (var page in pages)
            {
                foreach (var block in page.BlockList)
                {
                    block.sorted_orgs = SortedOrgs(block.orgs);
                }
            }
        }

        private List<TOrg> SortedOrgs(List<TOrg> orgs)
        {
            return orgs
                    .OrderByDescending(x => x.total_score)
                    .ThenByDescending(x => x.rank01_count)
                    .ThenByDescending(x => x.rank02_count)
                    .ThenByDescending(x => x.rank03_count)
                    .ThenByDescending(x => x.rank05_count)
                    .ThenByDescending(x => x.rank07_count)
                    .ThenByDescending(x => x.rank01_events)
                    .ThenByDescending(x => x.rank02_events)
                    .ThenByDescending(x => x.rank03_events)
                    .ThenByDescending(x => x.rank05_events)
                    .ThenByDescending(x => x.rank07_events)
                    .ToList();
        }

        private int GetRankCount(List<TRow> rows, int rank)
        {
            if (rows == null || rows.Count == 0) return 0;

            var result = rows.FindAll(x => x.rank == rank);
            if (result == null) return 0;

            return result.Count;
        }

        private decimal GetScores(List<TSect> sects, int weight)
        {
            if (sects == null || sects.Count == 0) return 0;
            var sect = sects.Find(x => x.weight == weight);
            if (sect == null || sect.RowList == null || sect.RowList.Count == 0) return 0;

            return sect.RowList.Sum(x => x.point);
        }

        private void SumXlsx(TConfig cfg, List<TPage> pages, Item itmReturn)
        {
            var exp = ExportInfo(cfg, "");

            //試算表
            Spire.Xls.Workbook workbook = new Spire.Xls.Workbook();

            foreach (var page in pages)
            {
                //團體成績
                RankSumXlsxSheet(cfg, workbook, page);
                //團體成績總冊
                RankSumXlsxSheetAll(cfg, workbook, page);
            }

            ////獎牌出賽場次統計
            //EventSumXlsxSheet(cfg, workbook, pages);

            //獎牌出賽場次清單
            EventListXlsxSheet(cfg, workbook, pages);

            workbook.Worksheets[0].Remove();
            workbook.Worksheets[0].Remove();
            workbook.Worksheets[0].Remove();

            string ext_name = ".xlsx";
            string xls_name = cfg.mt_title + "_團體成績_" + DateTime.Now.ToString("MMdd_HHmmss");
            string xls_file = exp.export_Path + "\\" + xls_name + ext_name;
            string xls_url = xls_name + ext_name;

            workbook.SaveToFile(xls_file, Spire.Xls.ExcelVersion.Version2013);

            itmReturn.setProperty("xls_name", xls_url);
        }

        //獎牌出賽場次清單
        private void EventListXlsxSheet(TConfig cfg, Spire.Xls.Workbook workbook, List<TPage> pages)
        {
            Spire.Xls.Worksheet sheet = workbook.CreateEmptySheet();
            sheet.Name = "出賽清單";
            sheet.PageSetup.PaperSize = Spire.Xls.PaperSizeType.PaperA4;
            sheet.PageSetup.Orientation = Spire.Xls.PageOrientationType.Portrait;//直立

            //頁面配置-邊界
            sheet.PageSetup.TopMargin = 0.5;
            sheet.PageSetup.BottomMargin = 0.5;
            // sheet.PageSetup.LeftMargin = 0.5;
            // sheet.PageSetup.RightMargin = 0.5;

            int mnRow = 2;
            int wsRow = 2;

            List<TField> fields = new List<TField>();
            fields.Add(new TField { ci = 1, title = "學校", property = "map_short_org", css = TCss.Center, width = 12 });
            fields.Add(new TField { ci = 2, title = "選手", property = "in_names", css = TCss.Center, width = 12 });
            fields.Add(new TField { ci = 3, title = "名次", property = "in_final_rank", css = TCss.Center, width = 12 });
            fields.Add(new TField { ci = 4, title = "量級", property = "program_name2", css = TCss.Center, width = 24 });
            fields.Add(new TField { ci = 5, title = "場地", property = "site_name", css = TCss.Center, width = 12 });
            fields.Add(new TField { ci = 6, title = "序號", property = "no", css = TCss.Center, width = 8 });
            fields.Add(new TField { ci = 7, title = "場次", property = "in_tree_no", css = TCss.Center, width = 8 });

            MapCharSet(cfg, fields);


            var fs = fields.First();
            var fe = fields.Last();

            //活動標題
            var mt_mr = sheet.Range[fs.cs + "1" + ":" + fe.cs + "1"];
            mt_mr.Merge();
            mt_mr.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
            mt_mr.Style.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;
            mt_mr.Text = cfg.mt_title;
            mt_mr.Style.Font.Size = 24;
            mt_mr.RowHeight = 35;

            foreach (var page in pages)
            {
                foreach (var block in page.BlockList)
                {
                    var m_mr = sheet.Range[fs.cs + wsRow + ":" + fe.cs + wsRow];
                    m_mr.Merge();
                    m_mr.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                    m_mr.Style.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;
                    m_mr.Text = block.title;
                    m_mr.Style.Font.Size = 16;
                    m_mr.Style.Font.IsBold = true;
                    m_mr.RowHeight = 30;
                    m_mr.Style.Color = System.Drawing.Color.FromArgb(183, 224, 224);
                    wsRow++;

                    //標題列
                    SetHeadRow(sheet, wsRow, fields, need_color: false);
                    wsRow++;

                    foreach (var org in block.sorted_orgs)
                    {
                        int no = 1;
                        string last_names = "";
                        foreach (var evt in org.events)
                        {
                            var item = evt.Value;
                            
                            string in_names = item.getProperty("in_names", "");
                            if (in_names != last_names)
                            {
                                no = 1;
                            }

                            item.setProperty("no", no.ToString()); ;

                            SetItemCell(cfg, sheet, wsRow, item, fields);
                            wsRow++;

                            no++;
                        }
                    }
                }
            }

            //設定格線
            SetRangeBorder(sheet, fields, mnRow, wsRow);

            //設定欄寬
            SetColumnWidth(sheet, fields);
        }

        ////獎牌出賽場次統計
        //private void EventSumXlsxSheet(TConfig cfg, Spire.Xls.Workbook workbook, List<TPage> pages)
        //{
        //    Spire.Xls.Worksheet sheet = workbook.CreateEmptySheet();
        //    sheet.Name = "出賽場次統計";
        //    sheet.PageSetup.PaperSize = Spire.Xls.PaperSizeType.PaperA4;
        //    sheet.PageSetup.Orientation = Spire.Xls.PageOrientationType.Portrait;//直立

        //    //頁面配置-邊界
        //    sheet.PageSetup.TopMargin = 0.5;
        //    sheet.PageSetup.BottomMargin = 0.5;
        //    // sheet.PageSetup.LeftMargin = 0.5;
        //    // sheet.PageSetup.RightMargin = 0.5;

        //    int mnRow = 2;
        //    int wsRow = 2;

        //    List<TField> fields = new List<TField>();
        //    fields.Add(new TField { ci = 1, title = "學校", property = "short_name", css = TCss.Center, width = 11 });
        //    fields.Add(new TField { ci = 2, title = "第01名", property = "rank01_events", css = TCss.Double, width = 5.33 });
        //    fields.Add(new TField { ci = 3, title = "第02名", property = "rank02_events", css = TCss.Double, width = 5.33 });
        //    fields.Add(new TField { ci = 4, title = "第03名", property = "rank03_events", css = TCss.Double, width = 5.33 });
        //    fields.Add(new TField { ci = 5, title = "第05名", property = "rank05_events", css = TCss.Double, width = 5.33 });
        //    fields.Add(new TField { ci = 6, title = "第07名", property = "rank06_events", css = TCss.Double, width = 5.33 });

        //    MapCharSet(cfg, fields);


        //    var fs = fields.First();
        //    var fe = fields.Last();

        //    //活動標題
        //    var mt_mr = sheet.Range[fs.cs + "1" + ":" + fe.cs + "1"];
        //    mt_mr.Merge();
        //    mt_mr.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
        //    mt_mr.Style.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;
        //    mt_mr.Text = cfg.mt_title;
        //    mt_mr.Style.Font.Size = 24;
        //    mt_mr.RowHeight = 35;

        //    //資料容器
        //    Item item = cfg.inn.newItem();

        //    foreach (var page in pages)
        //    {
        //        foreach (var block in page.BlockList)
        //        {
        //            var m_mr = sheet.Range[fs.cs + wsRow + ":" + fe.cs + wsRow];
        //            m_mr.Merge();
        //            m_mr.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
        //            m_mr.Style.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;
        //            m_mr.Text = block.title.Replace("獲獎名單", "");
        //            m_mr.Style.Font.Size = 16;
        //            m_mr.Style.Font.IsBold = true;
        //            m_mr.RowHeight = 30;
        //            m_mr.Style.Color = System.Drawing.Color.FromArgb(183, 224, 224);
        //            wsRow++;

        //            //標題列
        //            SetHeadRow(sheet, wsRow, fields, need_color: false);
        //            wsRow++;

        //            foreach (var org in block.sorted_orgs)
        //            {
        //                item.setProperty("short_name", org.short_name);
        //                item.setProperty("rank01_events", org.rank01_events.ToString());
        //                item.setProperty("rank02_events", org.rank02_events.ToString());
        //                item.setProperty("rank03_events", org.rank03_events.ToString());
        //                item.setProperty("rank05_events", org.rank05_events.ToString());
        //                item.setProperty("rank07_events", org.rank07_events.ToString());

        //                SetItemCell(cfg, sheet, wsRow, item, fields);
        //                wsRow++;
        //            }
        //        }
        //    }

        //    //設定格線
        //    SetRangeBorder(sheet, fields, mnRow, wsRow);

        //    //設定欄寬
        //    SetColumnWidth(sheet, fields);
        //}

        private void RankSumXlsxSheet(TConfig cfg, Spire.Xls.Workbook workbook, TPage page)
        {
            Spire.Xls.Worksheet sheet = workbook.CreateEmptySheet();
            sheet.Name = page.title;
            sheet.PageSetup.PaperSize = Spire.Xls.PaperSizeType.PaperA3;
            sheet.PageSetup.Orientation = Spire.Xls.PageOrientationType.Landscape;

            //頁面配置-邊界
            sheet.PageSetup.TopMargin = 0.5;
            sheet.PageSetup.BottomMargin = 0.5;
            // sheet.PageSetup.LeftMargin = 0.5;
            // sheet.PageSetup.RightMargin = 0.5;

            cfg.ci = 1;
            int mnRow = 2;
            int wsRow = 2;

            //男子
            var m_block = FilterBlockList(page, page.BlockList, GenderEnum.M);
            List<TField> m_fields = new List<TField>();
            AppendBlockCols(cfg, m_block, m_fields);

            cfg.ci++;

            //女子
            var w_block = FilterBlockList(page, page.BlockList, GenderEnum.W);
            List<TField> w_fields = new List<TField>();
            AppendBlockCols(cfg, w_block, w_fields);

            var fs = m_fields.First();
            var fe = w_fields.Last();

            //活動標題
            var mt_mr = sheet.Range[fs.cs + "1" + ":" + fe.cs + "1"];
            mt_mr.Merge();
            mt_mr.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
            mt_mr.Style.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;
            mt_mr.Text = cfg.mt_title;
            mt_mr.Style.Font.Size = 24;
            mt_mr.RowHeight = 35;


            //凍結窗格
            sheet.FreezePanes(4, fe.ci + 1);


            //資料容器
            Item item = cfg.inn.newItem();

            fs = m_fields.First();
            fe = m_fields.Last();

            if (m_block != null)
            {
                var m_mr = sheet.Range[fs.cs + wsRow + ":" + fe.cs + wsRow];
                m_mr.Merge();
                m_mr.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                m_mr.Style.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;
                m_mr.Text = m_block.title;
                m_mr.Style.Font.Size = 16;
                m_mr.Style.Font.IsBold = true;
                m_mr.RowHeight = 30;
                m_mr.Style.Color = System.Drawing.Color.FromArgb(183, 224, 224);
                wsRow++;

                //標題列
                SetHeadRow(sheet, wsRow, m_fields, need_color: true);
                wsRow++;

                var m_orgs = m_block.sorted_orgs;
                for (int i = 0; i < m_orgs.Count; i++)
                {
                    var org = m_orgs[i];
                    RefreshItem(item, org);
                    SetItemCell(cfg, sheet, wsRow, item, m_fields);
                    wsRow++;
                }

                //設定格線
                SetRangeBorder(sheet, m_fields, mnRow, wsRow);

                //設定欄寬
                SetColumnWidth(sheet, m_fields);
            }


            wsRow = mnRow;

            fs = w_fields.First();
            fe = w_fields.Last();

            //女子
            if (w_block != null)
            {
                var w_mr = sheet.Range[fs.cs + wsRow + ":" + fe.cs + wsRow];
                w_mr.Merge();
                w_mr.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                w_mr.Style.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;
                w_mr.Text = w_block.title;
                w_mr.Style.Font.Size = 16;
                w_mr.Style.Font.IsBold = true;
                w_mr.RowHeight = 30;
                w_mr.Style.Color = System.Drawing.Color.FromArgb(247, 203, 242);
                wsRow++;

                //標題列
                SetHeadRow(sheet, wsRow, w_fields, need_color: true);
                wsRow++;

                var w_orgs = w_block.sorted_orgs;
                for (int i = 0; i < w_orgs.Count; i++)
                {
                    var org = w_orgs[i];
                    RefreshItem(item, org);
                    SetItemCell(cfg, sheet, wsRow, item, w_fields);
                    wsRow++;
                }

                //設定格線
                SetRangeBorder(sheet, w_fields, mnRow, wsRow);

                //設定欄寬
                SetColumnWidth(sheet, w_fields);
            }

            sheet.Columns[15].ColumnWidth = 4.22;
        }

        private void RankSumXlsxSheetAll(TConfig cfg, Spire.Xls.Workbook workbook, TPage page)
        {
            Spire.Xls.Worksheet sheet = workbook.CreateEmptySheet();
            sheet.Name = page.title;
            sheet.PageSetup.PaperSize = Spire.Xls.PaperSizeType.PaperA3;
            sheet.PageSetup.Orientation = Spire.Xls.PageOrientationType.Landscape;

            //頁面配置-邊界
            sheet.PageSetup.TopMargin = 0.5;
            sheet.PageSetup.BottomMargin = 0.5;
            // sheet.PageSetup.LeftMargin = 0.5;
            // sheet.PageSetup.RightMargin = 0.5;

            cfg.ci = 1;
            int mnRow = 2;
            int wsRow = 2;

            //男子
            var m_block = FilterBlockList(page, page.BlockList, GenderEnum.M);
            List<TField> m_fields = new List<TField>();
            AppendBlockCols2(cfg, m_block, m_fields);

            cfg.ci++;

            //女子
            var w_block = FilterBlockList(page, page.BlockList, GenderEnum.W);
            List<TField> w_fields = new List<TField>();
            AppendBlockCols2(cfg, w_block, w_fields);

            var fs = m_fields.First();
            var fe = w_fields.Last();

            //活動標題
            var mt_mr = sheet.Range[fs.cs + "1" + ":" + fe.cs + "1"];
            mt_mr.Merge();
            mt_mr.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
            mt_mr.Style.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;
            mt_mr.Text = cfg.mt_title;
            mt_mr.Style.Font.Size = 24;
            mt_mr.RowHeight = 35;


            //凍結窗格
            sheet.FreezePanes(4, fe.ci + 1);


            //資料容器
            Item item = cfg.inn.newItem();

            fs = m_fields.First();
            fe = m_fields.Last();

            if (m_block != null)
            {
                var m_mr = sheet.Range[fs.cs + wsRow + ":" + fe.cs + wsRow];
                m_mr.Merge();
                m_mr.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                m_mr.Style.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;
                m_mr.Text = m_block.title;
                m_mr.Style.Font.Size = 16;
                m_mr.Style.Font.IsBold = true;
                m_mr.RowHeight = 30;
                m_mr.Style.Color = System.Drawing.Color.FromArgb(183, 224, 224);
                wsRow++;

                //標題列
                SetHeadRow(sheet, wsRow, m_fields, need_color: true);
                wsRow++;

                var m_orgs = m_block.sorted_orgs;
                for (int i = 0; i < m_orgs.Count; i++)
                {
                    var org = m_orgs[i];
                    RefreshItem2(item, org);
                    SetItemCell(cfg, sheet, wsRow, item, m_fields);
                    wsRow++;
                }

                //設定格線
                SetRangeBorder(sheet, m_fields, mnRow, wsRow);

                //設定欄寬
                SetColumnWidth(sheet, m_fields);
            }


            wsRow = mnRow;

            fs = w_fields.First();
            fe = w_fields.Last();

            //女子
            if (w_block != null)
            {
                var w_mr = sheet.Range[fs.cs + wsRow + ":" + fe.cs + wsRow];
                w_mr.Merge();
                w_mr.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                w_mr.Style.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;
                w_mr.Text = w_block.title;
                w_mr.Style.Font.Size = 16;
                w_mr.Style.Font.IsBold = true;
                w_mr.RowHeight = 30;
                w_mr.Style.Color = System.Drawing.Color.FromArgb(247, 203, 242);
                wsRow++;

                //標題列
                SetHeadRow(sheet, wsRow, w_fields, need_color: true);
                wsRow++;

                var w_orgs = w_block.sorted_orgs;
                for (int i = 0; i < w_orgs.Count; i++)
                {
                    var org = w_orgs[i];
                    RefreshItem(item, org);
                    SetItemCell(cfg, sheet, wsRow, item, w_fields);
                    wsRow++;
                }

                //設定格線
                SetRangeBorder(sheet, w_fields, mnRow, wsRow);

                //設定欄寬
                SetColumnWidth(sheet, w_fields);
            }

            sheet.Columns[15].ColumnWidth = 4.22;
        }

        private TBlock FilterBlockList(TPage page, List<TBlock> source, GenderEnum g)
        {
            var block = source.Find(x => x.key == g);
            if (block == null) block = new TBlock { sects = new List<TSect>(), orgs = new List<TOrg>(), sorted_orgs = new List<TOrg>() };
            if (block.title == null)
            {
                switch (g)
                {
                    case GenderEnum.M:
                        block.title = page.title + "男生" + "團體組獲獎名單";
                        break;
                    case GenderEnum.W:
                        block.title = page.title + "女生" + "團體組獲獎名單";
                        break;
                    default:
                        block.title = page.title + " " + "團體組獲獎名單";
                        break;
                }
            }

            if (block.orgs == null) block.orgs = new List<TOrg>();
            if (block.sorted_orgs == null) block.sorted_orgs = new List<TOrg>();

            return block;
        }

        private void AppendBlockCols(TConfig cfg, TBlock block, List<TField> fields)
        {
            fields.Add(new TField { ci = cfg.ci, title = "學校", property = "short_name", css = TCss.Center, width = 11 });
            cfg.ci++;

            for (int i = 0; i < block.sects.Count; i++)
            {
                var sect = block.sects[i];
                fields.Add(new TField { ci = cfg.ci, title = "第" + Environment.NewLine + sect.no + Environment.NewLine + "級", property = "score_" + sect.weight, css = TCss.Double, width = 5.5, color = sect.color });
                cfg.ci++;
            }

            fields.Add(new TField { ci = cfg.ci, title = "合計", property = "total_score", css = TCss.Double, width = 7 });
            cfg.ci++;
            fields.Add(new TField { ci = cfg.ci, title = "第" + Environment.NewLine + "05" + Environment.NewLine + "名", property = "rank05_count", css = TCss.Double, width = 4 });
            cfg.ci++;
            fields.Add(new TField { ci = cfg.ci, title = "第" + Environment.NewLine + "07" + Environment.NewLine + "名", property = "rank07_count", css = TCss.Double, width = 4 });
            cfg.ci++;
            fields.Add(new TField { ci = cfg.ci, title = "備註" + Environment.NewLine + "名次", property = "", css = TCss.Double, width = 6 });
            cfg.ci++;

            MapCharSet(cfg, fields);
        }

        private void AppendBlockCols2(TConfig cfg, TBlock block, List<TField> fields)
        {
            fields.Add(new TField { ci = cfg.ci, title = "學校", property = "short_name", css = TCss.Center, width = 11 });
            cfg.ci++;

            var w = 5.5;

            for (int i = 0; i < block.sects.Count; i++)
            {
                var sect = block.sects[i];
                fields.Add(new TField { ci = cfg.ci, title = "第" + Environment.NewLine + sect.no + Environment.NewLine + "級", property = "score_" + sect.weight, css = TCss.Double, width = w, color = sect.color });
                cfg.ci++;
            }

            fields.Add(new TField { ci = cfg.ci, title = "合計", property = "total_score", css = TCss.Double, width = 7 });
            cfg.ci++;
            fields.Add(new TField { ci = cfg.ci, title = "第" + Environment.NewLine + "05" + Environment.NewLine + "名", property = "rank05_count", css = TCss.Double, width = 4 });
            cfg.ci++;
            fields.Add(new TField { ci = cfg.ci, title = "第" + Environment.NewLine + "07" + Environment.NewLine + "名", property = "rank07_count", css = TCss.Double, width = 4 });
            cfg.ci++;

            fields.Add(new TField { ci = 2, title = "第01名", property = "rank01_events", css = TCss.Double, width = w, color = TColor.Color2 });
            cfg.ci++;
            fields.Add(new TField { ci = 3, title = "第02名", property = "rank02_events", css = TCss.Double, width = w, color = TColor.Color2 });
            cfg.ci++;
            fields.Add(new TField { ci = 4, title = "第03名", property = "rank03_events", css = TCss.Double, width = w, color = TColor.Color2 });
            cfg.ci++;
            fields.Add(new TField { ci = 5, title = "第05名", property = "rank05_events", css = TCss.Double, width = w, color = TColor.Color2 });
            cfg.ci++;
            fields.Add(new TField { ci = 6, title = "第07名", property = "rank06_events", css = TCss.Double, width = w, color = TColor.Color2 });
            cfg.ci++;

            fields.Add(new TField { ci = cfg.ci, title = "備註" + Environment.NewLine + "名次", property = "", css = TCss.Double, width = 6 });
            cfg.ci++;

            MapCharSet(cfg, fields);
        }

        private void RefreshItem(Item item, TOrg org)
        {
            item.setProperty("short_name", org.short_name);
            item.setProperty("score_1", org.w01_score.ToString("##.#"));
            item.setProperty("score_2", org.w02_score.ToString("##.#"));
            item.setProperty("score_3", org.w03_score.ToString("##.#"));
            item.setProperty("score_4", org.w04_score.ToString("##.#"));
            item.setProperty("score_5", org.w05_score.ToString("##.#"));
            item.setProperty("score_6", org.w06_score.ToString("##.#"));
            item.setProperty("score_7", org.w07_score.ToString("##.#"));
            item.setProperty("score_8", org.w08_score.ToString("##.#"));
            item.setProperty("score_9", org.w09_score.ToString("##.#"));
            item.setProperty("score_10", org.w10_score.ToString("##.#"));

            item.setProperty("total_score", org.total_score.ToString());
            item.setProperty("total_score", org.total_score.ToString());

            item.setProperty("rank05_count", org.rank05_count.ToString());
            item.setProperty("rank07_count", org.rank07_count.ToString());
        }

        private void RefreshItem2(Item item, TOrg org)
        {
            item.setProperty("short_name", org.short_name);
            item.setProperty("score_1", org.w01_score.ToString("##.#"));
            item.setProperty("score_2", org.w02_score.ToString("##.#"));
            item.setProperty("score_3", org.w03_score.ToString("##.#"));
            item.setProperty("score_4", org.w04_score.ToString("##.#"));
            item.setProperty("score_5", org.w05_score.ToString("##.#"));
            item.setProperty("score_6", org.w06_score.ToString("##.#"));
            item.setProperty("score_7", org.w07_score.ToString("##.#"));
            item.setProperty("score_8", org.w08_score.ToString("##.#"));
            item.setProperty("score_9", org.w09_score.ToString("##.#"));
            item.setProperty("score_10", org.w10_score.ToString("##.#"));

            item.setProperty("total_score", org.total_score.ToString());
            item.setProperty("total_score", org.total_score.ToString());

            item.setProperty("rank05_count", org.rank05_count.ToString());
            item.setProperty("rank07_count", org.rank07_count.ToString());

            item.setProperty("rank01_events", org.rank01_events.ToString());
            item.setProperty("rank02_events", org.rank02_events.ToString());
            item.setProperty("rank03_events", org.rank03_events.ToString());
            item.setProperty("rank05_events", org.rank05_events.ToString());
            item.setProperty("rank07_events", org.rank07_events.ToString());
        }
        #region Xlsx

        private class TExport
        {
            public string template_Path { get; set; }
            public string export_Path { get; set; }
        }

        private class TField
        {
            public string title { get; set; }
            public string property { get; set; }
            public TCss css { get; set; }
            public int ci { get; set; }
            public string cs { get; set; }
            public double width { get; set; }
            public Func<TConfig, TField, Item, string> getValue { get; set; }

            public bool is_merge { get; set; }
            public TColor color { get; set; }
        }

        private enum TColor
        {
            None = 0,
            Color1 = 10,
            Color2 = 20,

        }
        private enum TCss
        {
            None = 0,
            Center = 10,
            Text = 100,
            Number = 200,
            Double = 250,
            Money = 300,
            Day = 400,
        }

        private string[] GetCharSet()
        {
            return new string[] { "", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "AA", "AB", "AC", "AD", "AE" };
        }

        private TExport ExportInfo(TConfig cfg, string in_name)
        {
            Item itmXls = cfg.inn.applyMethod("In_Meeting_Excel_Path", "<in_name>" + in_name + "</in_name>");
            string Export_Path = itmXls.getProperty("export_path", "");
            string Template_Path = itmXls.getProperty("template_path", "");

            TExport result = new TExport
            {
                template_Path = Template_Path,
                export_Path = Export_Path,
            };

            return result;
        }

        System.Drawing.Color Color1 = System.Drawing.Color.FromArgb(197, 195, 157);
        System.Drawing.Color Color2 = System.Drawing.Color.FromArgb(197, 0, 157);

        private void CellNeedColor(TField field, Spire.Xls.CellRange range)
        {
            switch(field.color)
            {
                case TColor.Color1:
                    range.Style.Color = Color1;
                    break;
                case TColor.Color2:
                    range.Style.Color = Color2;
                    break;
            }
        }

        //設定資料列
        private void SetItemCell(TConfig cfg, Spire.Xls.Worksheet sheet, int wsRow, Item item, List<TField> fields)
        {
            foreach (var field in fields)
            {
                var cr = field.cs + wsRow;
                var va = " ";

                if (field.getValue != null)
                {
                    va = field.getValue(cfg, field, item);
                }
                else if (!string.IsNullOrEmpty(field.property))
                {
                    va = item.getProperty(field.property, "");
                }

                SetCell(sheet, cr, va, field);
            }
        }

        //設定資料格
        private void SetCell(Spire.Xls.Worksheet sheet, string cr, string value, TField field)
        {
            var range = sheet.Range[cr];
            range.Style.Font.Size = 14;
            range.RowHeight = 20;

            CellNeedColor(field, range);

            switch (field.css)
            {
                case TCss.None:
                    range.Text = value;
                    break;
                case TCss.Center:
                    range.Text = value;
                    range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                    break;
                case TCss.Text:
                    range.Text = "'" + value;
                    range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                    break;
                case TCss.Number:
                    range.NumberValue = GetIntVal(value);
                    range.NumberFormat = "0";
                    range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Right;
                    break;
                case TCss.Double:
                    var v = GetDblVal(value);
                    if (v <= 0)
                    {

                    }
                    else
                    {
                        range.NumberValue = v;
                        range.NumberFormat = "0";
                        range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Right;
                        range.Style.Font.Color = System.Drawing.Color.Red;
                    }
                    break;
                case TCss.Day:
                    range.Text = GetDateTimeValue(value, "yyyy/MM/dd");
                    range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                    break;
            }
        }

        //設定標題列
        private void SetHead(Spire.Xls.Worksheet sheet, string cr, TField field)
        {
            var range = sheet.Range[cr];

            CellNeedColor(field, range);

            range.Text = field.title;
            range.Style.Font.IsBold = true;
            range.Style.Font.Size = 14;
            range.RowHeight = 60;

            range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
            range.Style.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;
            range.IsWrapText = true;
        }

        //設定格線
        private void SetRangeBorder(Spire.Xls.Worksheet sheet, string pos)
        {
            var range = sheet.Range[pos];
            range.BorderInside(Spire.Xls.LineStyleType.Thin, System.Drawing.Color.Black);
            range.BorderAround(Spire.Xls.LineStyleType.Thin, System.Drawing.Color.Black);
        }

        //設定格線
        private void SetRangeBorder(Spire.Xls.Worksheet sheet, List<TField> fields, int mnRow, int wsRow)
        {
            var mxRow = wsRow - 1;
            if (mxRow < mnRow) mxRow = mnRow;

            var tbl_ps = fields.First().cs + mnRow;
            var tbl_pe = fields.Last().cs + mxRow;
            SetRangeBorder(sheet, tbl_ps + ":" + tbl_pe);
        }

        private void MapCharSet(TConfig cfg, List<TField> fields)
        {
            foreach (var field in fields)
            {
                field.cs = cfg.CharSet[field.ci];
            }
        }

        private void MapCharSetAndIndex(TConfig cfg, List<TField> fields)
        {
            for (int i = 0; i < fields.Count; i++)
            {
                var field = fields[i];
                field.ci = i + 1;
                field.cs = cfg.CharSet[field.ci];
            }
        }

        private void SetHeadRow(Spire.Xls.Worksheet sheet, int wsRow, List<TField> fields, bool need_color)
        {
            foreach (var field in fields)
            {
                string cr = field.cs + wsRow;
                SetHead(sheet, cr, field);
            }
        }

        private void SetColumnWidth(Spire.Xls.Worksheet sheet, List<TField> fields)
        {
            foreach (var field in fields)
            {
                sheet.Columns[field.ci - 1].ColumnWidth = field.width;
            }
        }

        #endregion Xlsx

        private List<TPage> MapPages(TConfig cfg, Item items)
        {
            List<TPage> pages = new List<TPage>();

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                var row = MapRow(cfg, item);

                var page = pages.Find(x => x.key == row.AgeType);
                if (page == null)
                {
                    page = MapPage(cfg, row);
                    pages.Add(page);
                }

                var block = page.BlockList.Find(x => x.key == row.GenderType);
                if (block == null)
                {
                    block = MapBlock(cfg, page, row);
                    page.BlockList.Add(block);
                }

                var org = block.orgs.Find(x => x.short_name == row.map_short_org);
                if (org == null)
                {
                    org = MapOrg(cfg, page, block, block.sects, row);
                    block.orgs.Add(org);
                }

                var sect = org.sects.Find(x => x.weight == row.weight);
                if (sect == null)
                {
                    //異常
                    continue;
                }

                sect.RowList.Add(row);
                org.rows.Add(row);
            }

            return pages;
        }

        private TOrg MapOrg(TConfig cfg, TPage page, TBlock block, List<TSect> sects, TRow row)
        {
            var obj = new TOrg
            {
                short_name = row.map_short_org,
                long_name = row.in_current_org,
                sects = new List<TSect>(),
                rows = new List<TRow>(),

                w01_score = 0,
                w02_score = 0,
                w03_score = 0,
                w04_score = 0,
                w05_score = 0,
                w06_score = 0,
                w07_score = 0,
                w08_score = 0,
                w09_score = 0,
                w10_score = 0,
                w11_score = 0,
                w12_score = 0,

                total_score = 0,

                rank05_count = 0,
                rank07_count = 0,
            };

            foreach (var sect in sects)
            {
                obj.sects.Add(new TSect
                {
                    weight = sect.weight,
                    title = sect.title,
                    need_sum = sect.need_sum,
                    RowList = new List<TRow>(),
                });
            }

            return obj;
        }

        private TBlock MapBlock(TConfig cfg, TPage page, TRow row)
        {
            var obj = new TBlock
            {
                key = row.GenderType,
                title = row.AgeTitle + row.GenderTitle + "團體組獲獎名單",
                orgs = new List<TOrg>(),
            };

            obj.sects = GetSects(cfg, page.key, obj.key);

            return obj;
        }

        private TPage MapPage(TConfig cfg, TRow row)
        {
            var obj = new TPage
            {
                key = row.AgeType,
                title = row.AgeTitle + "團體成績",
                BlockList = new List<TBlock>(),
            };
            return obj;
        }

        private TRow MapRow(TConfig cfg, Item item)
        {
            var obj = new TRow
            {
                map_short_org = item.getProperty("map_short_org", ""),
                in_current_org = item.getProperty("in_current_org", ""),
                in_team = item.getProperty("in_team", ""),
                in_names = item.getProperty("in_names", ""),
                in_final_rank = item.getProperty("in_final_rank", "0"),
                in_show_rank = item.getProperty("in_show_rank", "0"),

                program_id = item.getProperty("program_id", ""),
                program_name2 = item.getProperty("program_name2", ""),
                program_short_name = item.getProperty("program_short_name", ""),

                Value = item,
            };

            if (obj.program_short_name.Contains("男"))
            {
                obj.GenderType = GenderEnum.M;
                obj.GenderTitle = "男生";
            }
            else if (obj.program_short_name.Contains("女"))
            {
                obj.GenderType = GenderEnum.W;
                obj.GenderTitle = "女生";
            }
            else
            {
                obj.GenderType = GenderEnum.None;
                obj.GenderTitle = "";
            }

            var sn = obj.program_short_name;
            var noAgeType = false;

            if (sn.Contains("小"))
            {
                if (sn.Contains("A") || sn.Contains("高"))
                {
                    obj.AgeType = AgeEnum.ElementaryA;
                    obj.AgeTitle = "小A";
                }
                else if (sn.Contains("B") || sn.Contains("中"))
                {
                    obj.AgeType = AgeEnum.ElementaryB;
                    obj.AgeTitle = "小B";
                }
                else if (sn.Contains("C") || sn.Contains("低"))
                {
                    obj.AgeType = AgeEnum.ElementaryC;
                    obj.AgeTitle = "小C";
                }
                else
                {
                    noAgeType = true;
                }
            }
            else if (sn.Contains("國"))
            {
                obj.AgeType = AgeEnum.Junior;
                obj.AgeTitle = "國中";
            }
            else if (sn.Contains("高"))
            {
                obj.AgeType = AgeEnum.High;
                obj.AgeTitle = "高中";
            }
            else if (sn.Contains("一般"))
            {
                obj.AgeType = AgeEnum.CollegeKyuGrade;
                obj.AgeTitle = "一般";
            }
            else if (sn.Contains("公開"))
            {
                obj.AgeType = AgeEnum.CollegeDanGrade;
                obj.AgeTitle = "公開";
            }
            else if (sn.Contains("大"))
            {
                if (sn.Contains("甲"))
                {
                    obj.AgeType = AgeEnum.CollegeDanGrade;
                    obj.AgeTitle = "大甲";
                }
                else if (sn.Contains("乙"))
                {
                    obj.AgeType = AgeEnum.CollegeKyuGrade;
                    obj.AgeTitle = "大乙";
                }
                else if (sn.Contains("未上段"))
                {
                    obj.AgeType = AgeEnum.CollegeKyuGrade;
                    obj.AgeTitle = "大乙";
                }
                else if (sn.Contains("上段"))
                {
                    obj.AgeType = AgeEnum.CollegeDanGrade;
                    obj.AgeTitle = "大甲";
                }
                else
                {
                    noAgeType = true;
                }
            }
            else if (sn.Contains("社"))
            {
                if (sn.Contains("甲"))
                {
                    obj.AgeType = AgeEnum.SocietyDanGrade;
                    obj.AgeTitle = "社甲";
                }
                else if (sn.Contains("乙"))
                {
                    obj.AgeType = AgeEnum.SocietyKyuGrade;
                    obj.AgeTitle = "社乙";
                }
                else if (sn.Contains("未上段"))
                {
                    obj.AgeType = AgeEnum.SocietyKyuGrade;
                    obj.AgeTitle = "社乙";
                }
                else if (sn.Contains("上段"))
                {
                    obj.AgeType = AgeEnum.SocietyDanGrade;
                    obj.AgeTitle = "社甲";
                }
                else
                {
                    noAgeType = true;
                }
            }
            else
            {
                noAgeType = true;
            }

            if (noAgeType)
            {
                obj.AgeType = AgeEnum.None;
                obj.AgeTitle = "";
            }

            if (obj.program_name2.Contains("第十一")) obj.weight = 11;
            else if (obj.program_name2.Contains("第十二")) obj.weight = 12;
            else if (obj.program_name2.Contains("第一")) obj.weight = 1;
            else if (obj.program_name2.Contains("第二")) obj.weight = 2;
            else if (obj.program_name2.Contains("第三")) obj.weight = 3;
            else if (obj.program_name2.Contains("第四")) obj.weight = 4;
            else if (obj.program_name2.Contains("第五")) obj.weight = 5;
            else if (obj.program_name2.Contains("第六")) obj.weight = 6;
            else if (obj.program_name2.Contains("第七")) obj.weight = 7;
            else if (obj.program_name2.Contains("第八")) obj.weight = 8;
            else if (obj.program_name2.Contains("第九")) obj.weight = 9;
            else if (obj.program_name2.Contains("第十")) obj.weight = 10;
            else obj.weight = 0;

            obj.rank = GetIntVal(obj.in_show_rank);

            switch (obj.rank)
            {
                case 1:
                    obj.point = cfg.rank1_point;
                    break;
                case 2:
                    obj.point = cfg.rank2_point;
                    break;
                case 3:
                    obj.point = cfg.rank3_point;
                    break;
                case 5:
                    obj.point = cfg.rank5_point;
                    break;
                case 7:
                    obj.point = cfg.rank7_point;
                    break;
                default:
                    obj.point = 0;
                    break;
            }

            return obj;
        }

        private Item GetScores(TConfig cfg)
        {
            string sql = @"
                SELECT
	                t2.in_stuff_b1
	                , t2.map_short_org
	                , t2.in_current_org
	                , t2.in_team
	                , t2.in_names
	                , t2.in_final_rank 
	                , t2.in_show_rank 
	                , t1.id               AS 'program_id'
	                , t1.in_name2         AS 'program_name2'
	                , t1.in_short_name    AS 'program_short_name'
                FROM 
	                IN_MEETING_PROGRAM t1
                INNER JOIN
	                VU_MEETING_PTEAM t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                WHERE 
	                t1.in_meeting = '{#meeting_id}'
	                AND ISNULL(t2.in_final_rank, 0) > 0
                ORDER BY
	                t1.in_sort_order
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }

            public string meeting_id { get; set; }
            public string scene { get; set; }

            public Item itmMeeting { get; set; }
            public string mt_title { get; set; }

            public decimal rank1_point { get; set; }
            public decimal rank2_point { get; set; }
            public decimal rank3_point { get; set; }
            public decimal rank5_point { get; set; }
            public decimal rank7_point { get; set; }

            public string[] CharSet { get; set; }
            public int ci { get; set; }

        }

        private enum AgeEnum
        {
            /// <summary>
            /// 未定義
            /// </summary>
            None = 0,
            /// <summary>
            /// 小學(高年級)
            /// </summary>
            ElementaryA = 110,
            /// <summary>
            /// 小學(中年級)
            /// </summary>
            ElementaryB = 120,
            /// <summary>
            /// 小學(低年級)
            /// </summary>
            ElementaryC = 130,
            /// <summary>
            /// 初中
            /// </summary>
            Junior = 200,
            /// <summary>
            /// 高中
            /// </summary>
            High = 300,
            /// <summary>
            /// 大專校院-公開組(上段)
            /// </summary>
            CollegeDanGrade = 410,
            /// <summary>
            /// 大專校院-一般組(未上段)
            /// </summary>
            CollegeKyuGrade = 420,
            /// <summary>
            /// 社甲(上段)
            /// </summary>
            SocietyDanGrade = 510,
            /// <summary>
            /// 社乙(未上段)
            /// </summary>
            SocietyKyuGrade = 520,
        }

        private enum GenderEnum
        {
            /// <summary>
            /// 未定義
            /// </summary>
            None = 0,
            /// <summary>
            /// 女
            /// </summary>
            W = 10,
            /// <summary>
            /// 男
            /// </summary>
            M = 20,
        }

        private class TRow
        {
            public string map_short_org { get; set; }
            public string in_current_org { get; set; }
            public string in_team { get; set; }
            public string in_names { get; set; }
            public string in_final_rank { get; set; }
            public string in_show_rank { get; set; }

            public string program_id { get; set; }
            public string program_name2 { get; set; }
            public string program_short_name { get; set; }

            public string event_id { get; set; }
            public string in_tree_name { get; set; }
            public string in_tree_id { get; set; }
            public string in_tree_sno { get; set; }

            public AgeEnum AgeType { get; set; }
            public string AgeTitle { get; set; }

            public GenderEnum GenderType { get; set; }
            public string GenderTitle { get; set; }

            public int weight { get; set; }
            public int rank { get; set; }
            public decimal point { get; set; }

            public Item Value { get; set; }
        }

        private class TPage
        {
            public AgeEnum key { get; set; }
            public string title { get; set; }
            public List<TBlock> BlockList { get; set; }
        }

        private class TBlock
        {
            public GenderEnum key { get; set; }
            public string in_gender { get; set; }
            public string title { get; set; }
            public List<TSect> sects { get; set; }
            public List<TOrg> orgs { get; set; }
            public List<TOrg> sorted_orgs { get; set; }
        }

        private class TOrg
        {
            public string short_name { get; set; }
            public string long_name { get; set; }

            public List<TSect> sects { get; set; }
            public List<TRow> rows { get; set; }
            public List<TRow> events { get; set; }


            public decimal w01_score { get; set; }
            public decimal w02_score { get; set; }
            public decimal w03_score { get; set; }
            public decimal w04_score { get; set; }
            public decimal w05_score { get; set; }
            public decimal w06_score { get; set; }
            public decimal w07_score { get; set; }
            public decimal w08_score { get; set; }
            public decimal w09_score { get; set; }
            public decimal w10_score { get; set; }
            public decimal w11_score { get; set; }
            public decimal w12_score { get; set; }

            public decimal total_score { get; set; }

            public decimal rank01_count { get; set; }
            public decimal rank02_count { get; set; }
            public decimal rank03_count { get; set; }
            public decimal rank05_count { get; set; }
            public decimal rank07_count { get; set; }

            public decimal rank01_events { get; set; }
            public decimal rank02_events { get; set; }
            public decimal rank03_events { get; set; }
            public decimal rank05_events { get; set; }
            public decimal rank07_events { get; set; }
        }

        private class TSect
        {
            public int weight { get; set; }
            public string title { get; set; }
            public string no { get; set; }
            public bool need_sum { get; set; }
            public TColor color { get; set; }

            public List<TRow> RowList { get; set; }
        }

        private class TScore
        {
            /// <summary>
            /// 名次
            /// </summary>
            public int rank { get; set; }
            /// <summary>
            /// 分數
            /// </summary>
            public decimal point { get; set; }
            /// <summary>
            /// 小計
            /// </summary>
            public decimal total { get; set; }
        }
        private List<TSect> GetSects(TConfig cfg, AgeEnum ageType, GenderEnum genderType)
        {
            List<TSect> result = new List<TSect>();

            result.Add(new TSect { weight = 1, title = "第01級", need_sum = true, no = "01"});
            result.Add(new TSect { weight = 2, title = "第02級", need_sum = true, no = "02"});
            result.Add(new TSect { weight = 3, title = "第03級", need_sum = true, no = "03"});
            result.Add(new TSect { weight = 4, title = "第04級", need_sum = true, no = "04"});
            result.Add(new TSect { weight = 5, title = "第05級", need_sum = true, no = "05" });
            result.Add(new TSect { weight = 6, title = "第06級", need_sum = true, no = "06" });
            result.Add(new TSect { weight = 7, title = "第07級", need_sum = true, no = "07" });

            switch (ageType)
            {
                case AgeEnum.ElementaryC://9個量級
                case AgeEnum.ElementaryB://9個量級
                    result.Add(new TSect { weight = 8, title = "第08級", need_sum = true, no = "08", color = TColor.Color1 });
                    result.Add(new TSect { weight = 9, title = "第09級", need_sum = true, no = "09", color = TColor.Color1 });
                    break;
                case AgeEnum.ElementaryA://8個量級
                    result.Add(new TSect { weight = 8, title = "第08級", need_sum = true, no = "08", color = TColor.Color1 });
                    break;
                case AgeEnum.Junior://男10個量級、女9個量級
                    result.Add(new TSect { weight = 8, title = "第08級", need_sum = true, no = "08", color = TColor.Color1 });
                    result.Add(new TSect { weight = 9, title = "第09級", need_sum = true, no = "09", color = TColor.Color1 });
                    if (genderType == GenderEnum.M)
                    {
                        result.Add(new TSect { weight = 10, title = "第10級", need_sum = true, no = "10", color = TColor.Color1 });
                    }
                    break;
                case AgeEnum.High://8個量級
                    result.Add(new TSect { weight = 8, title = "第08級", need_sum = true, no = "08", color = TColor.Color1 });
                    break;
            }

            return result;
        }

        private double GetDblVal(string value, int def = 0)
        {
            double result = def;
            double.TryParse(value, out result);
            return result;
        }

        private int GetIntVal(string value, int def = 0)
        {
            int result = def;
            Int32.TryParse(value, out result);
            return result;
        }

        //轉換日期
        private string GetDateTimeValue(string value, string format, bool add8Hours = true)
        {
            if (value == "") return "";
            DateTime dt = Convert.ToDateTime(value);
            if (add8Hours)
            {
                dt = dt.AddHours(8);
            }
            return dt.ToString(format);
        }
    }
}