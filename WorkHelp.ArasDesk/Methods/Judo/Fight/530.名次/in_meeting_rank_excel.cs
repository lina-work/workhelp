﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Judo.Fight
{
    public class in_meeting_rank_excel : Item
    {
        public in_meeting_rank_excel(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 匯出名次 Excel 表
                位置: In_Competition_Bulletin_N1.html
                日期: 
                    - 2023-06-05 修改(Panda) 東亞青下載名次總表失敗
                    - 2021-10-09 修改 (Lina)
                    - 2020-11-17 創建 (Alan)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]in_meeting_rank_excel";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, this.dom.InnerXml);

            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,

                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,

                sport = "柔道",
                meeting_id = itmR.getProperty("meeting_id", ""),
                in_date = itmR.getProperty("in_date", ""),
                program_id = itmR.getProperty("program_id", ""),
                in_gender = itmR.getProperty("in_gender", ""),
                scene = itmR.getProperty("scene", ""),
                CharSet = GetCharSet(),

                rank01_point = 4,
                rank02_point = 2,
                rank03_point = 0.5,
                rank05_point = 0,
                rank07_point = 0,
            };

            if (cfg.in_gender == "")
            {
                cfg.show_a = true;
                cfg.show_m = true;
                cfg.show_w = true;
            }
            if (cfg.in_gender == "男")
            {
                cfg.show_m = true;
            }
            if (cfg.in_gender == "女")
            {
                cfg.show_w = true;
            }

            cfg.itmEmpty = cfg.inn.newItem();

            cfg.itmMeeting = cfg.inn.applySQL("SELECT in_title FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            cfg.mt_title = cfg.itmMeeting.getProperty("in_title", "");
            cfg.mt_battle_type = GetMtVariableValue(cfg, "fight_battle_type");
            cfg.isChallenge = cfg.mt_battle_type == "Challenge";

            cfg.IsWMG = cfg.meeting_id == "5B6CCBF34B32402C9CB7E65892EDF69C";

            switch (cfg.scene)
            {
                case "sum_page":
                    cfg.IsSumReport = true;
                    SumFunc(cfg, itmR, SumPage);
                    break;

                case "sum_xlsx":
                    cfg.IsSumReport = true;
                    SumFunc(cfg, itmR, SumXlsx);
                    break;

                case "rank_xlsx":
                    cfg.IsSumReport = false;
                    ExportRankXlsx(cfg, itmR);
                    break;
            }

            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

            return itmR;
        }

        #region 名次統計

        private void SumFunc(TConfig cfg, Item itmReturn, Action<TConfig, List<TMedal>, List<TGroup>, Item> action)
        {
            var medals = new List<TMedal>();
            medals.Add(new TMedal { rank = 1, label = "冠軍", ranks = new int[] { 1 }, prop = "rank01_count" });
            medals.Add(new TMedal { rank = 2, label = "亞軍", ranks = new int[] { 2 }, prop = "rank02_count" });
            medals.Add(new TMedal { rank = 3, label = "季軍", ranks = new int[] { 3, 4 }, prop = "rank03_count" });
            medals.Add(new TMedal { rank = 5, label = "第五名", ranks = new int[] { 5, 6 }, prop = "rank05_count" });
            medals.Add(new TMedal { rank = 7, label = "第七名", ranks = new int[] { 7, 8 }, prop = "rank07_count" });

            var items = GetScores(cfg);
            var groups = MapGroup(cfg, items);
            action(cfg, medals, groups, itmReturn);
        }

        private void SumXlsx(TConfig cfg, List<TMedal> medals, List<TGroup> groups, Item itmReturn)
        {
            var exp = ExportInfo(cfg, "");

            //試算表
            Spire.Xls.Workbook workbook = new Spire.Xls.Workbook();

            foreach (var gp in groups)
            {
                var orgs = MapOrgList(cfg, medals, gp);
                SumXlsxSheet(cfg, workbook, medals, gp, orgs);
            }

            workbook.Worksheets[0].Remove();
            workbook.Worksheets[0].Remove();
            workbook.Worksheets[0].Remove();

            string ext_name = ".xlsx";
            string xls_name = cfg.mt_title + "_名次統計_" + DateTime.Now.ToString("MMdd_HHmmss");
            string xls_file = exp.export_Path + "\\" + xls_name + ext_name;
            string xls_url = xls_name + ext_name;

            workbook.SaveToFile(xls_file, Spire.Xls.ExcelVersion.Version2013);

            itmReturn.setProperty("xls_name", xls_url);
        }

        private void SumXlsxSheet(TConfig cfg, Spire.Xls.Workbook workbook, List<TMedal> medals, TGroup gp, List<TOrg> orgs)
        {
            Spire.Xls.Worksheet sheet = workbook.CreateEmptySheet();
            sheet.Name = gp.sheetName;

            int ci = 1;

            List<TField> fields = new List<TField>();

            fields.Add(new TField { ci = ci, title = "序號", property = "no", css = TCss.Center, width = 8 });
            ci++;
            fields.Add(new TField { ci = ci, title = "單位", property = "map_short_org", css = TCss.None, width = 18 });
            ci++;

            foreach (var medal in medals)
            {
                fields.Add(new TField { ci = ci, title = "男子" + Environment.NewLine + medal.label, property = "m_" + medal.prop, css = TCss.Double, width = 6 });
                ci++;
            }
            fields.Add(new TField { ci = ci, title = "男子" + Environment.NewLine + "總分", property = "m_total", css = TCss.Double, width = 5 });
            ci++;

            foreach (var medal in medals)
            {
                fields.Add(new TField { ci = ci, title = "女子" + Environment.NewLine + medal.label, property = "w_" + medal.prop, css = TCss.Double, width = 6 });
                ci++;
            }
            fields.Add(new TField { ci = ci, title = "女子" + Environment.NewLine + "總分", property = "w_total", css = TCss.Double, width = 5 });
            ci++;

            fields.Add(new TField { ci = ci, title = "總積分", property = "total", css = TCss.Double, width = 5 });

            MapCharSet(cfg, fields);

            var fs = fields.First();
            var fe = fields.Last();

            var mt_mr = sheet.Range[fs.cs + "1" + ":" + fe.cs + "1"];
            mt_mr.Merge();
            mt_mr.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
            mt_mr.Text = cfg.mt_title;
            mt_mr.Style.Font.Size = 16;
            mt_mr.RowHeight = 20;

            var rpt_mr = sheet.Range[fs.cs + "2" + ":" + fe.cs + "2"];
            rpt_mr.Merge();
            rpt_mr.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
            rpt_mr.Text = "名次統計";
            rpt_mr.Style.Font.Size = 16;
            rpt_mr.RowHeight = 20;

            int mnRow = 3;
            int wsRow = 3;

            //標題列
            SetHeadRow(sheet, wsRow, fields);
            wsRow++;

            //凍結窗格
            sheet.FreezePanes(wsRow, fields.Last().ci + 1);

            //資料容器
            Item item = cfg.inn.newItem();
            int no = 0;

            foreach (var org in orgs)
            {
                no++;

                item.setProperty("no", no.ToString());
                item.setProperty("map_short_org", org.map_short_org);

                item.setProperty("m_rank01_count", org.m_box.rank01_count.ToString());
                item.setProperty("m_rank02_count", org.m_box.rank02_count.ToString());
                item.setProperty("m_rank03_count", org.m_box.rank03_count.ToString());
                item.setProperty("m_rank05_count", org.m_box.rank05_count.ToString());
                item.setProperty("m_rank06_count", org.m_box.rank07_count.ToString());
                item.setProperty("m_total", org.m_box.total_point.ToString());

                item.setProperty("w_rank01_count", org.w_box.rank01_count.ToString());
                item.setProperty("w_rank02_count", org.w_box.rank02_count.ToString());
                item.setProperty("w_rank03_count", org.w_box.rank03_count.ToString());
                item.setProperty("w_rank05_count", org.w_box.rank05_count.ToString());
                item.setProperty("w_rank06_count", org.w_box.rank07_count.ToString());
                item.setProperty("w_total", org.w_box.total_point.ToString());

                item.setProperty("total", org.o_box.total_point.ToString());

                SetItemCell(cfg, sheet, wsRow, item, fields);

                wsRow++;
            }

            //設定格線
            SetRangeBorder(sheet, fields, mnRow, wsRow);

            //設定欄寬
            SetColumnWidth(sheet, fields);
        }

        private void SumPage(TConfig cfg, List<TMedal> medals, List<TGroup> groups, Item itmReturn)
        {
            StringBuilder builder = new StringBuilder();

            AppendTabHead(cfg, medals, groups, builder);
            AppendTabBody(cfg, medals, groups, builder);

            itmReturn.setProperty("inn_tab", builder.ToString());
            itmReturn.setProperty("in_title", cfg.mt_title);
        }

        private void AppendTabHead(TConfig cfg, List<TMedal> medals, List<TGroup> groups, StringBuilder builder)
        {
            builder.Append("<ul class='nav nav-pills' style='justify-content: start;' id='pills-tab' role='tablist'>");
            foreach (var gp in groups)
            {
                string active = gp.Id == "1" ? " active" : "";
                builder.Append("<li class='nav-item" + active + "'>");
                builder.Append("  <a class='nav-link' data-toggle='pill' id='" + gp.Tab_Id + "'");
                builder.Append("     role='tab' aria-controls='pills-home' href='#" + gp.Div_Id + "' onclick='StoreTab_Click(this)'>");
                builder.Append("  " + gp.sheetName);
                builder.Append("  </a>");
                builder.Append("</li>");
            }
            builder.Append("</ul>");
        }

        private void AppendTabBody(TConfig cfg, List<TMedal> medals, List<TGroup> groups, StringBuilder builder)
        {
            builder.Append("<div class='tab-content page-tab-content' id='pills-tabContent'>");

            foreach (var gp in groups)
            {
                var orgs = MapOrgList(cfg, medals, gp);

                string active = gp.Id == "1" ? " active in" : "";

                builder.Append("<div id='" + gp.Div_Id + "' class='tab-pane " + active + "' role='tabpanel'>");
                builder.Append("  <div class='box-body row container-row'>");
                builder.Append("    <div class='box box-fix'>");
                builder.Append("      <div class='box-body'>");

                AppendTable(cfg, medals, gp, orgs, builder);


                builder.Append("      </div>");
                builder.Append("    </div>");
                builder.Append("  </div>");
                builder.Append("</div>");
            }
            builder.Append("</div>");
        }

        private void AppendTable(TConfig cfg, List<TMedal> medals, TGroup gp, List<TOrg> orgs, StringBuilder builder)
        {
            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            head.Append("<thead>");
            head.Append("<tr>");
            head.Append("<th class='text-center'>&nbsp;</th>");
            head.Append("<th class='text-center'>單位</th>");

            if (cfg.show_m)
            {
                for (int i = 0; i < medals.Count; i++)
                {
                    var medal = medals[i];
                    head.Append("<th class='text-center'>" + "男子<br>" + medal.label + "</th>");
                }
                head.Append("<th class='text-center'>男子<br>總分</th>");
            }

            if (cfg.show_w)
            {
                for (int i = 0; i < medals.Count; i++)
                {
                    var medal = medals[i];
                    head.Append("<th class='text-center'>" + "女子<br>" + medal.label + "</th>");
                }
                head.Append("<th class='text-center'>女子<br>總分</th>");
            }

            if (cfg.show_a)
            {
                head.Append("<th class='text-center'>總積分</th>");
            }


            head.Append("</tr>");
            head.Append("</thead>");


            body.Append("<tbody>");

            for (int i = 0; i < orgs.Count; i++)
            {
                var org = orgs[i];

                var tr_id = gp.Table_Id + "_tr_" + (i + 1);

                body.Append("<tr>");

                var no_wait = org.Teams.Count <= 0;
                if (no_wait)
                {
                    body.Append("<td class='text-center'> &nbsp; </td>");
                }
                else
                {
                    body.Append("<td class='text-center'> <span class='event-btn' onclick='ToggleWaitRow_Click(this)' data-id='" + tr_id + "' data-state='hide'> &nbsp; <i class='fa fa-plus '></i> &nbsp; </span> </td>");
                }

                body.Append("<td class='text-center'>" + org.map_short_org + "</td>");

                if (cfg.show_m)
                {
                    var m_medals = org.medals.FindAll(x => x.gcode == 1);
                    for (int j = 0; j < m_medals.Count; j++)
                    {
                        var medal = m_medals[j];
                        body.Append("<td class='text-cente boy_group'>" + medal.count + "</td>");
                    }
                    body.Append("<td class='text-center gold_group'>" + org.m_box.total_point + "</td>");
                }

                if (cfg.show_w)
                {
                    var w_medals = org.medals.FindAll(x => x.gcode == 2);
                    for (int j = 0; j < w_medals.Count; j++)
                    {
                        var medal = w_medals[j];
                        body.Append("<td class='text-center girl_group'>" + medal.count + "</td>");
                    }
                    body.Append("<td class='text-center gold_group'>" + org.w_box.total_point + "</td>");
                }

                if (cfg.show_a)
                {
                    body.Append("<td class='text-center'>" + org.o_box.total_point + "</td>");
                }
                body.Append("</tr>");


                var colspan = 14;
                if (!cfg.show_a) colspan = 7;


                if (!no_wait)
                {
                    body.Append("<tr id='" + tr_id + "' style='display: none'>");
                    body.Append("<td class='text-center' style='vertical-align: middle'> &nbsp; </td>");
                    body.Append("<td class='text-center' colspan='" + colspan + "'>");
                    body.Append(AppendRanks(cfg, gp, org));
                    body.Append("</td>");

                    body.Append("</tr>");
                }
            }
            body.Append("</tbody>");

            builder.Append("<table id='" + gp.Table_Id + "' class='table table-hover table-bordered table-rwd rwd rwdtable match-table'>");
            builder.Append(head);
            builder.Append(body);
            builder.Append("</table>");
        }

        private StringBuilder AppendRanks(TConfig cfg, TGroup gp, TOrg org)
        {
            StringBuilder builder = new StringBuilder();

            builder.Append("<table class='table table-bordered table-rwd rwd'>");
            builder.Append("  <tbody>");

            var teams = org.Teams
                .OrderBy(x => x.in_gender)
                .ThenBy(x => x.rank).ToList();

            var no = 1;
            for (int i = 0; i < teams.Count; i++)
            {
                var team = teams[i];

                builder.Append("<tr>");
                builder.Append("  <td>" + no + "</td>");
                builder.Append("  <td>" + ProgramName(cfg, team) + "</td>");
                builder.Append("  <td>" + team.player_name + "</td>");
                builder.Append("  <td>名次：" + team.rank + "</td>");
                builder.Append("</tr>");

                no++;
            }

            builder.Append("  </tbody>");
            builder.Append("</table>");

            return builder;
        }

        private string ProgramName(TConfig cfg, TTeam team)
        {
            //return team.pg_name;

            return "<a target='_blank' href='c.aspx?page=In_Competition_Preview.html&method=in_meeting_program_preview"
                + "&meeting_id=" + cfg.meeting_id
                + "&program_id=" + team.pg_id + "'>"
                + team.pg_name
                + "</a>";
        }

        private List<TOrg> MapOrgList(TConfig cfg, List<TMedal> medals, TGroup gp)
        {
            List<TOrg> list = new List<TOrg>();

            var items = gp.Values;

            int count = items.Count;

            for (int i = 0; i < count; i++)
            {
                Item item = items[i];
                string program_short_name = item.getProperty("program_short_name", "");
                string in_stuff_b1 = item.getProperty("in_stuff_b1", "");
                string map_short_org = item.getProperty("map_short_org", "");
                string in_final_rank = item.getProperty("in_final_rank", "0");

                int gcode = program_short_name.Contains("男") ? 1 : 2;
                int final_rank = GetIntVal(in_final_rank);

                var org = list.Find(x => x.map_short_org == map_short_org);

                if (org == null)
                {
                    org = new TOrg
                    {
                        in_stuff_b1 = in_stuff_b1,
                        map_short_org = map_short_org,
                        medals = new List<TMedal>(),
                        Teams = new List<TTeam>(),
                        o_box = new TRankBox(),
                        m_box = new TRankBox(),
                        w_box = new TRankBox(),
                    };

                    for (int g = 1; g <= 2; g++)
                    {
                        for (int j = 0; j < medals.Count; j++)
                        {
                            var medal = medals[j];
                            org.medals.Add(new TMedal
                            {
                                gcode = g,
                                rank = medal.rank,
                                ranks = medal.ranks,
                                label = medal.label,
                                count = 0,
                                items = new List<Item>(),
                            });
                        }
                    }
                    list.Add(org);
                }

                //找到對應性別的獎牌統計
                var rank_medal = org.medals.Find(x => x.gcode == gcode && x.ranks.Contains(final_rank));
                if (rank_medal == null) throw new Exception("異常");

                rank_medal.count++;
                rank_medal.items.Add(item);

                org.Teams.Add(MapTeam(final_rank, item));
            }

            for (int i = 0; i < list.Count; i++)
            {
                var org = list[i];

                for (int j = 0; j < org.medals.Count; j++)
                {
                    var medal = org.medals[j];

                    switch (medal.rank)
                    {
                        case 1:
                            org.o_box.rank01_count += medal.count;
                            if (medal.gcode == 1) org.m_box.rank01_count = medal.count;
                            if (medal.gcode == 2) org.w_box.rank01_count = medal.count;
                            break;
                        case 2:
                            org.o_box.rank02_count += medal.count;
                            if (medal.gcode == 1) org.m_box.rank02_count = medal.count;
                            if (medal.gcode == 2) org.w_box.rank02_count = medal.count;
                            break;
                        case 3:
                            org.o_box.rank03_count += medal.count;
                            if (medal.gcode == 1) org.m_box.rank03_count = medal.count;
                            if (medal.gcode == 2) org.w_box.rank03_count = medal.count;
                            break;
                        case 5:
                            org.o_box.rank05_count += medal.count;
                            if (medal.gcode == 1) org.m_box.rank05_count = medal.count;
                            if (medal.gcode == 2) org.w_box.rank05_count = medal.count;
                            break;
                        case 7:
                            org.o_box.rank07_count += medal.count;
                            if (medal.gcode == 1) org.m_box.rank07_count = medal.count;
                            if (medal.gcode == 2) org.w_box.rank07_count = medal.count;
                            break;
                    }

                    org.o_box.total_count += medal.count;
                    if (medal.gcode == 1) org.m_box.total_count += medal.count;
                    if (medal.gcode == 2) org.w_box.total_count += medal.count;
                }
            }

            foreach (var org in list)
            {
                org.o_box.total_point = GetPoints(cfg, org.o_box);
                org.m_box.total_point = GetPoints(cfg, org.m_box);
                org.w_box.total_point = GetPoints(cfg, org.w_box);
            }

            return list.OrderByDescending(x => x.o_box.total_point)
                .ThenByDescending(x => x.o_box.rank05_count)
                .ThenByDescending(x => x.o_box.rank07_count)
                .ToList();
        }

        private double GetPoints(TConfig cfg, TRankBox bx)
        {
            return bx.rank01_count * cfg.rank01_point
                + bx.rank02_count * cfg.rank02_point
                + bx.rank03_count * cfg.rank03_point
                + bx.rank05_count * cfg.rank05_point
                + bx.rank07_count * cfg.rank07_point;
        }

        private TTeam MapTeam(int rank, Item item)
        {
            string pg_id = item.getProperty("program_id", "");
            string pg_name = item.getProperty("program_name2", "");
            string in_team = item.getProperty("in_team", "");
            string in_names = item.getProperty("in_names", "");
            string in_show_rank = item.getProperty("in_show_rank", "");

            if (in_team != "") in_names = "(" + in_team + "隊)" + in_names;

            TTeam result = new TTeam
            {
                rank = rank,
                pg_id = pg_id,
                pg_name = pg_name,
                player_name = in_names,
            };

            if (pg_name.Contains("男"))
            {
                result.in_gender = "M";
            }
            else if (pg_name.Contains("女"))
            {
                result.in_gender = "W";
            }

            return result;
        }

        private Item GetScores(TConfig cfg)
        {
            string gender_condition = cfg.show_a
                ? ""
                : "AND t1.in_name LIKE N'%" + cfg.in_gender + "%'";

            string sql = @"
                SELECT
	                t2.in_stuff_b1
	                , t2.in_current_org
	                , t2.map_short_org
	                , t2.in_team
	                , t2.in_names
	                , t2.in_final_rank 
	                , t2.in_show_rank 
	                , t1.id               AS 'program_id'
	                , t1.in_name2         AS 'program_name2'
	                , t1.in_short_name    AS 'program_short_name'
	                , t1.in_l1
	                , t1.in_l2
	                , t1.in_l3
                FROM 
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
	                VU_MEETING_PTEAM t2
	                ON t2.source_id = t1.id
	            LEFT OUTER JOIN
	                IN_MEETING_PEVENT t3 WITH(NOLOCK)
	                ON t3.id = t2.in_event
                WHERE 
	                t1.in_meeting = '{#meeting_id}'
	                AND ISNULL(t2.in_final_rank, 0) > 0
	                {#gender_condition}
                ORDER BY
	                t1.in_sort_order
	                , t2.in_final_rank
	                , t3.in_tree_id
	                , t2.in_foot
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#gender_condition}", gender_condition);

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        private class TOrg
        {
            public string in_stuff_b1 { get; set; }
            public string map_short_org { get; set; }
            public List<TMedal> medals { get; set; }

            public TRankBox o_box { get; set; }
            public TRankBox m_box { get; set; }
            public TRankBox w_box { get; set; }

            public List<TTeam> Teams { get; set; }
        }

        private class TMedal
        {
            public int gcode { get; set; }
            public int rank { get; set; }
            public int[] ranks { get; set; }
            public string label { get; set; }
            public string prop { get; set; }
            public int count { get; set; }
            public List<Item> items { get; set; }
        }

        private class TRankBox
        {
            public int rank01_count { get; set; }
            public int rank02_count { get; set; }
            public int rank03_count { get; set; }
            public int rank05_count { get; set; }
            public int rank07_count { get; set; }
            public int total_count { get; set; }
            public double total_point { get; set; }
        }

        private class TTeam
        {
            public int rank { get; set; }
            public string pg_id { get; set; }
            public string pg_name { get; set; }
            public string player_name { get; set; }
            public string in_gender { get; set; }
        }

        #endregion 名次統計

        #region 名次清單

        private void ExportRankXlsx(TConfig cfg, Item itmReturn)
        {
            Item items = GetRankList(cfg);

            if (items.getResult() == "")
            {
                throw new Exception("該組尚無資料再請確認!");
            }

            var exp = ExportInfo(cfg, "rank_path");
            var groups = MapGroup(cfg, items);
            var workbook = new Spire.Xls.Workbook();

            AppendRankListTable(cfg, workbook, items);
            foreach (var gp in groups)
            {
                AppendRankTableType2(cfg, workbook, gp);
                AppendRankTable1233(cfg, workbook, gp);
                AppendRankTableA4(cfg, workbook, gp);
                AppendRankTableA3(cfg, workbook, gp);
                AppendRankTable(cfg, workbook, gp, "男");
                AppendRankTable(cfg, workbook, gp, "女");
            }

            workbook.Worksheets[0].Remove();
            workbook.Worksheets[0].Remove();
            workbook.Worksheets[0].Remove();

            string ext_name = ".xlsx";
            string xls_name = cfg.mt_title + "_" + cfg.sport + "_名次總表_" + DateTime.Now.ToString("MMdd_HHmmss");

            if (cfg.in_date != "")
            {
                xls_name = cfg.itmMeeting.getProperty("in_title", "")
                    + "_" + cfg.sport
                    + "_" + cfg.in_date.Replace("-", "")
                    + "_名次總表_" + DateTime.Now.ToString("MMdd_HHmmss");
            }

            string xls_file = exp.export_Path + "\\" + xls_name + ext_name;
            string xls_url = xls_name + ext_name;

            workbook.SaveToFile(xls_file, Spire.Xls.ExcelVersion.Version2013);

            itmReturn.setProperty("xls_name", xls_name + ".xlsx");
        }

        private void AppendRankListTable(TConfig cfg, Spire.Xls.Workbook workbook, Item items)
        {
            var sheet = workbook.CreateEmptySheet();
            sheet.Name = "名次清單";
            sheet.PageSetup.PaperSize = Spire.Xls.PaperSizeType.PaperA4;

            //頁面配置-邊界
            sheet.PageSetup.TopMargin = 0.4;
            sheet.PageSetup.BottomMargin = 0.5;
            sheet.PageSetup.LeftMargin = 0.5;
            sheet.PageSetup.RightMargin = 0.4;

            //列印標題
            sheet.PageSetup.PrintTitleRows = "$1:$3";

            SetType2Title(cfg, sheet, "A1:H1", cfg.mt_title, true, 28, 42, true);
            SetType2Title(cfg, sheet, "A2:H2", "名次清單", true, 24, 30, false);

            SetType2Title(cfg, sheet, "A3", "比賽日期", false, 12, 16, true);
            SetType2Title(cfg, sheet, "B3", "項目", false, 12, 16, true);
            SetType2Title(cfg, sheet, "C3", "組別", false, 12, 16, true);
            SetType2Title(cfg, sheet, "D3", "級組", false, 12, 16, true);
            SetType2Title(cfg, sheet, "E3", "參賽人數", false, 12, 16, true);
            SetType2Title(cfg, sheet, "F3", "名次", false, 12, 16, true);
            SetType2Title(cfg, sheet, "G3", "姓名", false, 12, 16, true);
            SetType2Title(cfg, sheet, "H3", "單位", false, 12, 16, true);

            var ridx = 4;
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var in_l1 = item.getProperty("in_l1", "");
                var in_l2 = item.getProperty("in_l2", "");
                var in_l3 = item.getProperty("in_l3", "");
                var in_fight_day = item.getProperty("in_fight_day", "");
                var in_rank_count = item.getProperty("program_rank_count", "0");
                var in_final_rank = item.getProperty("in_final_rank", "0");
                var in_name = item.getProperty("in_name", "");
                var org = GetShowOrg(item);
                var in_team = item.getProperty("in_team", "");
                var sect = item.getProperty("program_name3", "");
                var isTeam = in_l1 == "團體組";

                if (isTeam)
                {
                    sect = item.getProperty("program_name3", "");
                    in_name = item.getProperty("in_names", "");
                    //if (in_team != "") org += " " + in_team + "隊";
                }

                SetTypeCellC(cfg, sheet, "A" + ridx, in_fight_day);
                SetTypeCellC(cfg, sheet, "B" + ridx, in_l1);
                SetTypeCellC(cfg, sheet, "C" + ridx, GetAgeSect(sect));
                SetTypeCellC(cfg, sheet, "D" + ridx, sect);
                SetTypeCellN(cfg, sheet, "E" + ridx, GetIntVal(in_rank_count));
                SetTypeCellN(cfg, sheet, "F" + ridx, GetIntVal(in_final_rank));
                SetTypeCellL(cfg, sheet, "G" + ridx, in_name);
                SetTypeCellL(cfg, sheet, "H" + ridx, org);
                ridx++;
            }
            sheet.Columns[0].ColumnWidth = 13;
            sheet.Columns[1].ColumnWidth = 10;
            sheet.Columns[2].ColumnWidth = 10;
            sheet.Columns[3].ColumnWidth = 38;
            sheet.Columns[4].ColumnWidth = 10;
            sheet.Columns[5].ColumnWidth = 10;
            sheet.Columns[6].ColumnWidth = 25;
            sheet.Columns[7].ColumnWidth = 25;

            SetRangeBorder(sheet, "A3:" + "H" + (ridx - 1));
        }

        private string GetAgeSect(string in_l2)
        {
            if (in_l2.Contains("國小")) return "國小組";
            if (in_l2.Contains("國中")) return "國中組";
            if (in_l2.Contains("高中")) return "高中組";
            if (in_l2.Contains("社會大專")) return "社會大專組";
            if (in_l2.Contains("大專")) return "大專組";
            if (in_l2.Contains("社會")) return "社會組";
            if (in_l2.Contains("公開")) return "公開組";
            if (in_l2.Contains("一般")) return "一般組";
            if (in_l2.Contains("混合")) return "混合組";
            if (in_l2.Contains("男")) return "男子組";
            if (in_l2.Contains("女")) return "女子組";
            return "";
        }

        private void AppendRankTableType2(TConfig cfg, Spire.Xls.Workbook workbook, TGroup gp)
        {
            if (cfg.isChallenge)
            {
                AppendRankTableType2C(cfg, workbook, gp);
            }
            else
            {
                AppendRankTableType2T(cfg, workbook, gp);
            }
        }

        private void AppendRankTableType2C(TConfig cfg, Spire.Xls.Workbook workbook, TGroup gp)
        {
            Spire.Xls.Worksheet sheet = workbook.CreateEmptySheet();
            sheet.Name = gp.sheetName + "-" + "名次公告";
            sheet.PageSetup.PaperSize = Spire.Xls.PaperSizeType.PaperA4;

            //橫式列印
            sheet.PageSetup.Orientation = Spire.Xls.PageOrientationType.Landscape;

            //將所有欄放入單一頁面
            //sheet.PageSetup.FitToPagesWide = 1;

            //頁面配置-邊界
            sheet.PageSetup.CenterHorizontally = true;
            sheet.PageSetup.TopMargin = 0.4;
            sheet.PageSetup.BottomMargin = 0.2;
            sheet.PageSetup.LeftMargin = 0.2;
            sheet.PageSetup.RightMargin = 0.2;

            //列印標題
            sheet.PageSetup.PrintTitleRows = "$1:$2";


            var ec = "J";
            SetType2Title(cfg, sheet, "A1:" + ec + "1", cfg.mt_title + " - 名次公告", true, 22, 30, true);

            SetType2Sign(cfg, sheet, "I2", "比賽日期：", 12);
            SetType2SignV(cfg, sheet, ec + "2", cfg.in_date, 12);

            var ridx = 3;
            var rstart = ridx;

            SetType2Head(cfg, sheet, "A" + ridx, "組別");
            SetType2Head(cfg, sheet, "B" + ridx, "項目");
            SetType2Head(cfg, sheet, "C" + ridx, "量級");
            SetType2Head(cfg, sheet, "D" + ridx, "人數");
            SetType2Head(cfg, sheet, "E" + ridx, "第1名");
            SetType2Head(cfg, sheet, "F" + ridx, "第2名");
            SetType2Head(cfg, sheet, "G" + ridx, "第3名");
            SetType2Head(cfg, sheet, "H" + ridx, "第4名");
            SetType2Head(cfg, sheet, "I" + ridx, "第5名");
            SetType2Head(cfg, sheet, "J" + ridx, "第5名");
            ridx++;

            var c = System.Environment.NewLine;
            for (var i = 0; i < gp.programs.Count; i++)
            {
                var program = gp.programs[i];

                SetType2Cell(cfg, sheet, "A" + ridx, program.in_l1);
                SetType2Cell(cfg, sheet, "B" + ridx, program.in_l2.Replace("個-", ""));
                SetType2Cell(cfg, sheet, "C" + ridx, program.in_l3.Split('：')[0] + c + program.weight);
                SetType2Cell(cfg, sheet, "D" + ridx, program.rank_count.ToString());

                var model = GetRankItems(cfg, program.id);

                SetType2Cell(cfg, sheet, "E" + ridx, model.r1, true);
                SetType2Cell(cfg, sheet, "F" + ridx, model.r2, true);
                SetType2Cell(cfg, sheet, "G" + ridx, model.r3, true);
                SetType2Cell(cfg, sheet, "H" + ridx, model.r4, true);
                SetType2Cell(cfg, sheet, "I" + ridx, model.r5, true);
                SetType2Cell(cfg, sheet, "J" + ridx, model.r6, true);

                ridx++;
            }

            var w = 15.3;
            sheet.Columns[1].ColumnWidth = 12;
            sheet.Columns[2].ColumnWidth = 12;
            sheet.Columns[3].ColumnWidth = 8;
            sheet.Columns[4].ColumnWidth = w;
            sheet.Columns[5].ColumnWidth = w;
            sheet.Columns[6].ColumnWidth = w;
            sheet.Columns[7].ColumnWidth = w;
            sheet.Columns[8].ColumnWidth = w;
            sheet.Columns[9].ColumnWidth = w;

            SetRangeBorder(sheet, "A" + rstart + ":" + ec + (ridx - 1));


            var rend = ridx + 1;
            SetType2Sign(cfg, sheet, "E" + rend, "裁判長簽名：", 16);
            sheet.Range["F" + rend].Borders[Spire.Xls.BordersLineType.EdgeBottom].LineStyle = Spire.Xls.LineStyleType.Thin;
            sheet.Range["G" + rend].Borders[Spire.Xls.BordersLineType.EdgeBottom].LineStyle = Spire.Xls.LineStyleType.Thin;

            SetType2Sign(cfg, sheet, "H" + rend, "日期：", 16);
            sheet.Range["I" + rend].Borders[Spire.Xls.BordersLineType.EdgeBottom].LineStyle = Spire.Xls.LineStyleType.Thin;
            sheet.Range["J" + rend].Borders[Spire.Xls.BordersLineType.EdgeBottom].LineStyle = Spire.Xls.LineStyleType.Thin;
        }

        private void AppendRankTableType2T(TConfig cfg, Spire.Xls.Workbook workbook, TGroup gp)
        {
            Spire.Xls.Worksheet sheet = workbook.CreateEmptySheet();
            sheet.Name = gp.sheetName + "-" + "名次公告";
            sheet.PageSetup.PaperSize = Spire.Xls.PaperSizeType.PaperA4;

            //橫式列印
            sheet.PageSetup.Orientation = Spire.Xls.PageOrientationType.Landscape;

            //將所有欄放入單一頁面
            sheet.PageSetup.FitToPagesWide = 1;

            //頁面配置-邊界
            sheet.PageSetup.TopMargin = 0.4;
            sheet.PageSetup.BottomMargin = 0.5;
            sheet.PageSetup.LeftMargin = 0.5;
            sheet.PageSetup.RightMargin = 0.4;

            //列印標題
            sheet.PageSetup.PrintTitleRows = "$1:$2";


            SetType2Title(cfg, sheet, "A1:L1", cfg.mt_title, true, 28, 42, true);
            SetType2Title(cfg, sheet, "A2:L2", cfg.sport + "名次公告", true, 24, 30, false);

            SetType2Sign(cfg, sheet, "K3", "比賽日期：", 12);
            SetType2SignV(cfg, sheet, "L3", cfg.in_date, 12);

            var ridx = 5;
            var rstart = ridx;

            SetType2Head(cfg, sheet, "A" + ridx, "組別");
            SetType2Head(cfg, sheet, "B" + ridx, "項目");
            SetType2Head(cfg, sheet, "C" + ridx, "量級");
            SetType2Head(cfg, sheet, "D" + ridx, "人數");
            SetType2Head(cfg, sheet, "E" + ridx, "第1名");
            SetType2Head(cfg, sheet, "F" + ridx, "第2名");
            SetType2Head(cfg, sheet, "G" + ridx, "第3名");
            SetType2Head(cfg, sheet, "H" + ridx, "第3名");
            SetType2Head(cfg, sheet, "I" + ridx, "第5名");
            SetType2Head(cfg, sheet, "J" + ridx, "第5名");
            SetType2Head(cfg, sheet, "K" + ridx, "第7名");
            SetType2Head(cfg, sheet, "L" + ridx, "第7名");
            ridx++;

            for (var i = 0; i < gp.programs.Count; i++)
            {
                var program = gp.programs[i];

                SetType2Cell(cfg, sheet, "A" + ridx, program.in_l1);
                SetType2Cell(cfg, sheet, "B" + ridx, program.in_l2.Replace("個-", ""));
                SetType2Cell(cfg, sheet, "C" + ridx, program.in_l3.Split('：')[0]);
                SetType2Cell(cfg, sheet, "D" + ridx, program.rank_count.ToString());

                var model = GetRankItems(cfg, program.id);

                SetType2Cell(cfg, sheet, "E" + ridx, model.r1);
                SetType2Cell(cfg, sheet, "F" + ridx, model.r2);
                SetType2Cell(cfg, sheet, "G" + ridx, model.r3);
                SetType2Cell(cfg, sheet, "H" + ridx, model.r4);
                SetType2Cell(cfg, sheet, "I" + ridx, model.r5);
                SetType2Cell(cfg, sheet, "J" + ridx, model.r6);
                SetType2Cell(cfg, sheet, "K" + ridx, model.r7);
                SetType2Cell(cfg, sheet, "L" + ridx, model.r8);

                ridx++;
            }

            var w = 18;
            sheet.Columns[1].ColumnWidth = 12;
            sheet.Columns[4].ColumnWidth = w;
            sheet.Columns[5].ColumnWidth = w;
            sheet.Columns[6].ColumnWidth = w;
            sheet.Columns[7].ColumnWidth = w;
            sheet.Columns[8].ColumnWidth = w;
            sheet.Columns[9].ColumnWidth = w;
            sheet.Columns[10].ColumnWidth = w;
            sheet.Columns[11].ColumnWidth = w;

            SetRangeBorder(sheet, "A" + rstart + ":" + "L" + (ridx - 1));


            var rend = ridx + 2;
            SetType2Sign(cfg, sheet, "H" + rend, "裁判長簽名：", 16);
            SetType2Sign(cfg, sheet, "K" + rend, "日期：", 16);
            sheet.Range["I" + rend].Borders[Spire.Xls.BordersLineType.EdgeBottom].LineStyle = Spire.Xls.LineStyleType.Thin;
            sheet.Range["J" + rend].Borders[Spire.Xls.BordersLineType.EdgeBottom].LineStyle = Spire.Xls.LineStyleType.Thin;
            sheet.Range["L" + rend].Borders[Spire.Xls.BordersLineType.EdgeBottom].LineStyle = Spire.Xls.LineStyleType.Thin;
        }


        private void SetType2Title(TConfig cfg, Spire.Xls.Worksheet sheet, string pos, string value, bool isMerge, int fontSize, int height, bool isBold)
        {
            var range = sheet.Range[pos];
            if (isMerge) range.Merge();
            range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
            range.Style.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;
            range.Text = value;
            range.Style.Font.Size = fontSize;
            range.Style.Font.IsBold = isBold;
            range.RowHeight = height;
            range.Style.Font.FontName = "標楷體";
        }

        private void SetType2Sign(TConfig cfg, Spire.Xls.Worksheet sheet, string pos, string value, int fontSize)
        {
            var range = sheet.Range[pos];
            range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Right;
            range.Style.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;
            range.Text = value;
            range.Style.Font.Size = fontSize;
            range.Style.Font.FontName = "標楷體";
        }

        private void SetType2SignV(TConfig cfg, Spire.Xls.Worksheet sheet, string pos, string value, int fontSize)
        {
            var range = sheet.Range[pos];
            range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Left;
            range.Style.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;
            range.Text = value;
            range.Style.Font.Size = fontSize;
            range.Style.Font.FontName = "標楷體";
        }
        private RankModel GetRankItems(TConfig cfg, string program_id)
        {
            var result = new RankModel();
            var sql = "SELECT id, in_current_org, in_short_org, in_show_org, in_name, in_final_rank FROM IN_MEETING_PTEAM WITH(NOLOCK) WHERE source_id = '" + program_id + "' AND in_final_rank > 0 ORDER BY in_final_rank";
            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var in_final_rank = item.getProperty("in_final_rank", "0");
                switch (in_final_rank)
                {
                    case "1": result.r1 = item; break;
                    case "2": result.r2 = item; break;
                    case "3":
                        if (result.r3 == null)
                        {
                            result.r3 = item;
                        }
                        else
                        {
                            result.r4 = item;
                        }
                        break;
                    case "4": result.r4 = item; break;
                    case "5":
                        if (result.r5 == null)
                        {
                            result.r5 = item;
                        }
                        else
                        {
                            result.r6 = item;
                        }
                        break;
                    case "6": result.r6 = item; break;
                    case "7":
                        if (result.r7 == null)
                        {
                            result.r7 = item;
                        }
                        else
                        {
                            result.r8 = item;
                        }
                        break;
                    case "8": result.r8 = item; break;

                }

            }

            if (result.r1 == null) result.r1 = cfg.inn.newItem();
            if (result.r2 == null) result.r2 = cfg.inn.newItem();
            if (result.r3 == null) result.r3 = cfg.inn.newItem();
            if (result.r4 == null) result.r4 = cfg.inn.newItem();
            if (result.r5 == null) result.r5 = cfg.inn.newItem();
            if (result.r6 == null) result.r6 = cfg.inn.newItem();
            if (result.r7 == null) result.r7 = cfg.inn.newItem();
            if (result.r8 == null) result.r8 = cfg.inn.newItem();

            return result;
        }

        private class RankModel
        {
            public Item r1 { get; set; }
            public Item r2 { get; set; }
            public Item r3 { get; set; }
            public Item r4 { get; set; }
            public Item r5 { get; set; }
            public Item r6 { get; set; }
            public Item r7 { get; set; }
            public Item r8 { get; set; }
        }

        private void SetType2Head(TConfig cfg, Spire.Xls.Worksheet sheet, string pos, string value)
        {
            var range = sheet.Range[pos];
            range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
            range.Style.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;
            range.Text = value;
            range.Style.Font.Size = 14;
            range.Style.Font.FontName = "標楷體";
        }

        private void SetType2Cell(TConfig cfg, Spire.Xls.Worksheet sheet, string pos, string value)
        {
            var range = sheet.Range[pos];
            range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
            range.Style.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;
            range.Text = value;
            range.Style.Font.Size = 12;
            range.Style.Font.FontName = "標楷體";
            range.RowHeight = 50;
        }

        private void SetTypeCellN(TConfig cfg, Spire.Xls.Worksheet sheet, string pos, int value)
        {
            var range = sheet.Range[pos];
            range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
            range.Style.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;
            range.NumberValue = value;
            range.Style.Font.Size = 12;
            range.Style.Font.FontName = "標楷體";
        }

        private void SetTypeCellC(TConfig cfg, Spire.Xls.Worksheet sheet, string pos, string value)
        {
            var range = sheet.Range[pos];
            range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
            range.Style.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;
            range.Text = value;
            range.Style.Font.Size = 12;
            range.Style.Font.FontName = "標楷體";
        }

        private void SetTypeCellL(TConfig cfg, Spire.Xls.Worksheet sheet, string pos, string value)
        {
            var range = sheet.Range[pos];
            range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Left;
            range.Style.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;
            range.Text = value;
            range.Style.Font.Size = 12;
            range.Style.Font.FontName = "標楷體";
        }

        private void SetType2Cell(TConfig cfg, Spire.Xls.Worksheet sheet, string pos, Item item, bool needLine = false)
        {
            string org = item.getProperty("in_short_org", "");
            string org2 = item.getProperty("in_current_org", "");
            string in_show_org = item.getProperty("in_show_org", "");
            if (org == "") org = org2;
            if (in_show_org != "") org = in_show_org;

            string c = needLine ? System.Environment.NewLine : " ";
            string name = item.getProperty("in_name", "");
            string value = org + c + name;

            var range = sheet.Range[pos];
            range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
            range.Style.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;
            range.Text = value;
            range.Style.Font.Size = 12;
            range.Style.Font.FontName = "標楷體";
            range.IsWrapText = true;
            if (needLine)
            {
                range.RowHeight = 33;
            }
            else
            {
                range.RowHeight = 50;
            }
        }

        private void AppendRankTable1233(TConfig cfg, Spire.Xls.Workbook workbook, TGroup gp)
        {
            Spire.Xls.Worksheet sheet = workbook.CreateEmptySheet();
            sheet.Name = gp.sheetName + "1233(A3)";
            sheet.PageSetup.PaperSize = Spire.Xls.PaperSizeType.PaperA3;

            //頁面配置-邊界
            sheet.PageSetup.TopMargin = 0.5;
            // sheet.PageSetup.BottomMargin = 0.5;
            // sheet.PageSetup.LeftMargin = 0.5;
            // sheet.PageSetup.RightMargin = 0.5;

            //列印標題
            sheet.PageSetup.PrintTitleRows = "$1:$2";

            var mt_mr = sheet.Range["A1:G1"];
            mt_mr.Merge();
            mt_mr.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
            mt_mr.Text = cfg.mt_title;
            mt_mr.Style.Font.Size = 16;
            mt_mr.Style.Font.IsBold = true;
            mt_mr.RowHeight = 20;

            var rpt_mr = sheet.Range["A2:G2"];
            rpt_mr.Merge();
            rpt_mr.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
            rpt_mr.Text = "名次清單";
            rpt_mr.Style.Font.Size = 16;
            rpt_mr.RowHeight = 20;

            //男子
            List<TField> fields_m = new List<TField>();
            fields_m.Add(new TField { ci = 1, cs = "A", title = "名次", getValue = RankName, css = TCss.Center, width = 10 });
            fields_m.Add(new TField { ci = 2, cs = "B", title = "單位", getValue = OrgShortName, css = TCss.Center, width = 28 });
            fields_m.Add(new TField { ci = 3, cs = "C", title = "姓名/隊別", getValue = PlayerName, css = TCss.Center, width = 20 });

            var programs_m = gp.programs.FindAll(x => x.name.Contains("男"));
            AppendHalfSheet1233(cfg, sheet, fields_m, programs_m, 14);


            //女子
            List<TField> fields_w = new List<TField>();
            fields_w.Add(new TField { ci = 5, cs = "E", title = "名次", getValue = RankName, css = TCss.Center, width = 10 });
            fields_w.Add(new TField { ci = 6, cs = "F", title = "單位", getValue = OrgShortName, css = TCss.Center, width = 28 });
            fields_w.Add(new TField { ci = 7, cs = "G", title = "姓名/隊別", getValue = PlayerName, css = TCss.Center, width = 20 });

            var programs_w = gp.programs.FindAll(x => x.name.Contains("女"));
            AppendHalfSheet1233(cfg, sheet, fields_w, programs_w, 14);

            if (gp.sheetName == "格式")
            {
                //男子
                List<TField> fields_x = new List<TField>();
                fields_x.Add(new TField { ci = 1, cs = "A", title = "名次", getValue = RankName, css = TCss.Center, width = 10 });
                fields_x.Add(new TField { ci = 2, cs = "B", title = "單位", getValue = OrgShortName, css = TCss.Center, width = 28 });
                fields_x.Add(new TField { ci = 3, cs = "C", title = "姓名/隊別", getValue = PlayerName, css = TCss.Center, width = 20 });

                var programs_x = gp.programs;
                AppendHalfSheet1233(cfg, sheet, fields_x, programs_x, 14);
            }
        }

        private void AppendRankTableA4(TConfig cfg, Spire.Xls.Workbook workbook, TGroup gp)
        {
            Spire.Xls.Worksheet sheet = workbook.CreateEmptySheet();
            sheet.Name = gp.sheetName + "(A4)";
            sheet.PageSetup.PaperSize = Spire.Xls.PaperSizeType.PaperA4;

            //頁面配置-邊界
            sheet.PageSetup.TopMargin = 0.5;
            sheet.PageSetup.LeftMargin = 0.3;
            sheet.PageSetup.RightMargin = 0.3;
            // //將所有欄放入單一頁面
            // sheet.PageSetup.FitToPagesWide = 1;
            // sheet.PageSetup.FitToPagesTall = 0;

            //列印標題
            sheet.PageSetup.PrintTitleRows = "$1:$3";

            var mt_mr = sheet.Range["A1:G1"];
            mt_mr.Merge();
            mt_mr.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
            mt_mr.Text = cfg.mt_title;
            mt_mr.Style.Font.Size = 16;
            mt_mr.Style.Font.IsBold = true;
            mt_mr.RowHeight = 20;

            var rpt_mr = sheet.Range["A2:G2"];
            rpt_mr.Merge();
            rpt_mr.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
            rpt_mr.Text = "名次清單";
            rpt_mr.Style.Font.Size = 16;
            rpt_mr.RowHeight = 20;

            //男子
            List<TField> fields_m = new List<TField>();
            fields_m.Add(new TField { ci = 1, cs = "A", title = "名次", getValue = RankName, css = TCss.Center, width = 6 });
            fields_m.Add(new TField { ci = 2, cs = "B", title = "單位", getValue = OrgShortName, css = TCss.Center, width = 16 });
            fields_m.Add(new TField { ci = 3, cs = "C", title = "姓名/隊別", getValue = PlayerName, css = TCss.Center, width = 21 });

            var programs_m = gp.programs.FindAll(x => x.name.Contains("男"));
            AppendHalfSheetA4(cfg, sheet, fields_m, programs_m, 12);


            //女子
            List<TField> fields_w = new List<TField>();
            fields_w.Add(new TField { ci = 5, cs = "E", title = "名次", getValue = RankName, css = TCss.Center, width = 6 });
            fields_w.Add(new TField { ci = 6, cs = "F", title = "單位", getValue = OrgShortName, css = TCss.Center, width = 16 });
            fields_w.Add(new TField { ci = 7, cs = "G", title = "姓名/隊別", getValue = PlayerName, css = TCss.Center, width = 21 });

            var programs_w = gp.programs.FindAll(x => x.name.Contains("女"));
            AppendHalfSheetA4(cfg, sheet, fields_w, programs_w, 12);


            //格式
            if (gp.sheetName == "格式")
            {
                List<TField> fields_x = new List<TField>();
                fields_x.Add(new TField { ci = 1, cs = "A", title = "名次", getValue = RankName, css = TCss.Center, width = 10 });
                fields_x.Add(new TField { ci = 2, cs = "B", title = "單位", getValue = OrgShortName, css = TCss.Center, width = 28 });
                fields_x.Add(new TField { ci = 3, cs = "C", title = "姓名/隊別", getValue = PlayerName, css = TCss.Center, width = 22 });

                var programs_x = gp.programs;
                AppendHalfSheet(cfg, sheet, fields_x, programs_x, 14);
            }

            sheet.Columns[3].ColumnWidth = 3;
        }

        private void AppendRankTableA3(TConfig cfg, Spire.Xls.Workbook workbook, TGroup gp)
        {
            Spire.Xls.Worksheet sheet = workbook.CreateEmptySheet();
            sheet.Name = gp.sheetName + "(A3)";
            sheet.PageSetup.PaperSize = Spire.Xls.PaperSizeType.PaperA3;

            //頁面配置-邊界
            sheet.PageSetup.TopMargin = 0.5;
            // sheet.PageSetup.BottomMargin = 0.5;
            // sheet.PageSetup.LeftMargin = 0.5;
            // sheet.PageSetup.RightMargin = 0.5;

            //列印標題
            sheet.PageSetup.PrintTitleRows = "$1:$2";

            var mt_mr = sheet.Range["A1:G1"];
            mt_mr.Merge();
            mt_mr.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
            mt_mr.Text = cfg.mt_title;
            mt_mr.Style.Font.Size = 16;
            mt_mr.Style.Font.IsBold = true;
            mt_mr.RowHeight = 20;

            var rpt_mr = sheet.Range["A2:G2"];
            rpt_mr.Merge();
            rpt_mr.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
            rpt_mr.Text = "名次清單";
            rpt_mr.Style.Font.Size = 16;
            rpt_mr.RowHeight = 20;

            //男子
            List<TField> fields_m = new List<TField>();
            // fields_m.Add(new TField { ci = 1, cs = "A", title = "名次", getValue = RankName, css = TCss.Center, width = 6 });
            // fields_m.Add(new TField { ci = 2, cs = "B", title = "單位", getValue = OrgShortName, css = TCss.Center, width = 15 });
            // fields_m.Add(new TField { ci = 3, cs = "C", title = "姓名/隊別", getValue = PlayerName, css = TCss.Center, width = 15 });
            fields_m.Add(new TField { ci = 1, cs = "A", title = "名次", getValue = RankName, css = TCss.Center, width = 10 });
            fields_m.Add(new TField { ci = 2, cs = "B", title = "單位", getValue = OrgShortName, css = TCss.Center, width = 28 });
            fields_m.Add(new TField { ci = 3, cs = "C", title = "姓名/隊別", getValue = PlayerName, css = TCss.Center, width = 20 });

            var programs_m = gp.programs.FindAll(x => x.name.Contains("男"));
            AppendHalfSheet(cfg, sheet, fields_m, programs_m, 14);


            //女子
            List<TField> fields_w = new List<TField>();
            // fields_w.Add(new TField { ci = 5, cs = "E", title = "名次", getValue = RankName, css = TCss.Center, width = 6 });
            // fields_w.Add(new TField { ci = 6, cs = "F", title = "單位", getValue = OrgShortName, css = TCss.Center, width = 15 });
            // fields_w.Add(new TField { ci = 7, cs = "G", title = "姓名/隊別", getValue = PlayerName, css = TCss.Center, width = 15 });
            fields_w.Add(new TField { ci = 5, cs = "E", title = "名次", getValue = RankName, css = TCss.Center, width = 10 });
            fields_w.Add(new TField { ci = 6, cs = "F", title = "單位", getValue = OrgShortName, css = TCss.Center, width = 28 });
            fields_w.Add(new TField { ci = 7, cs = "G", title = "姓名/隊別", getValue = PlayerName, css = TCss.Center, width = 20 });

            var programs_w = gp.programs.FindAll(x => x.name.Contains("女"));
            AppendHalfSheet(cfg, sheet, fields_w, programs_w, 14);


            //格式
            if (gp.sheetName == "格式")
            {
                List<TField> fields_x = new List<TField>();
                fields_x.Add(new TField { ci = 1, cs = "A", title = "名次", getValue = RankName, css = TCss.Center, width = 10 });
                fields_x.Add(new TField { ci = 2, cs = "B", title = "單位", getValue = OrgShortName, css = TCss.Center, width = 28 });
                fields_x.Add(new TField { ci = 3, cs = "C", title = "姓名/隊別", getValue = PlayerName, css = TCss.Center, width = 20 });

                var programs_x = gp.programs;
                AppendHalfSheet(cfg, sheet, fields_x, programs_x, 14);
            }
        }

        private void AppendRankTable(TConfig cfg, Spire.Xls.Workbook workbook, TGroup gp, string in_gender)
        {
            Spire.Xls.Worksheet sheet = workbook.CreateEmptySheet();
            sheet.Name = gp.sheetName + in_gender;

            //將所有欄放入單一頁面
            sheet.PageSetup.FitToPagesWide = 1;
            sheet.PageSetup.FitToPagesTall = 0;
            //頁面配置-邊界
            sheet.PageSetup.TopMargin = 0.5;
            // sheet.PageSetup.BottomMargin = 0.5;
            // sheet.PageSetup.LeftMargin = 0.5;
            // sheet.PageSetup.RightMargin = 0.5;

            //列印標題
            sheet.PageSetup.PrintTitleRows = "$1:$2";

            var mt_mr = sheet.Range["A1:C1"];
            mt_mr.Merge();
            mt_mr.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
            mt_mr.Text = cfg.mt_title;
            mt_mr.Style.Font.Size = 16;
            mt_mr.Style.Font.IsBold = true;
            mt_mr.RowHeight = 20;

            var rpt_mr = sheet.Range["A2:C2"];
            rpt_mr.Merge();
            rpt_mr.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
            rpt_mr.Text = "名次清單";
            rpt_mr.Style.Font.Size = 16;
            rpt_mr.RowHeight = 20;

            List<TField> fields_m = new List<TField>();
            fields_m.Add(new TField { ci = 1, cs = "A", title = "名次", getValue = RankName, css = TCss.Center, width = 12 });
            fields_m.Add(new TField { ci = 2, cs = "B", title = "單位", getValue = OrgShortName, css = TCss.Center, width = 50 });
            fields_m.Add(new TField { ci = 3, cs = "C", title = "姓名/隊別", getValue = PlayerName, css = TCss.Center, width = 24 });

            var programs_m = gp.programs.FindAll(x => x.name.Contains(in_gender));
            AppendHalfSheet(cfg, sheet, fields_m, programs_m, fsize: 16, need_empty_row: false);
        }

        private void AppendHalfSheet1233(TConfig cfg, Spire.Xls.Worksheet sheet, List<TField> fields, List<TProgram> programs, int fsize = 0, bool need_empty_row = true)
        {
            int mnRow = 4;
            int wsRow = 4;

            //資料容器
            Item item = cfg.inn.newItem();

            var fs = fields.First();
            var fe = fields.Last();

            foreach (var program in programs)
            {
                var teams = program.Values;

                var pg_mr = sheet.Range[fs.cs + wsRow + ":" + fe.cs + wsRow];
                pg_mr.Merge();
                pg_mr.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                pg_mr.Text = program.name + " (" + program.rank_count + ")";
                pg_mr.Style.Font.Size = 12;
                pg_mr.RowHeight = 16;

                if (fsize > 0) pg_mr.Style.Font.Size = fsize;
                if (fsize >= 16) pg_mr.RowHeight = 24;

                wsRow++;

                //標題列
                SetHeadRow(sheet, wsRow, fields, 16, need_color: false);
                wsRow++;

                var rank_teams = new List<Item>();
                foreach (var team in teams)
                {
                    string in_show_rank = team.getProperty("in_show_rank", "0");
                    switch (in_show_rank)
                    {
                        case "1":
                        case "2":
                        case "3":
                        case "4":
                            rank_teams.Add(team);
                            break;
                    }
                }

                //維持八列
                var max_row = 4;
                var max_idx = rank_teams.Count - 1;



                for (int i = 0; i < max_row; i++)
                {
                    if (i <= max_idx)
                    {
                        var team = rank_teams[i];
                        SetItemCell(cfg, sheet, wsRow, team, fields, fsize);
                        wsRow++;
                    }
                }

                var rs = wsRow - max_row - 2;
                var re = wsRow - 1;
                SetRangeBorder(sheet, fs.cs + rs + ":" + fe.cs + re);

                wsRow++;
            }

            ////設定格線
            //SetRangeBorder(sheet, fields, mnRow, wsRow);

            //設定欄寬
            SetColumnWidth(sheet, fields);
        }

        private void AppendHalfSheet(TConfig cfg, Spire.Xls.Worksheet sheet, List<TField> fields, List<TProgram> programs, int fsize = 0, bool need_empty_row = true)
        {
            int mnRow = 4;
            int wsRow = 4;

            //資料容器
            Item item = cfg.inn.newItem();

            var fs = fields.First();
            var fe = fields.Last();

            foreach (var program in programs)
            {
                var teams = program.Values;

                var pg_mr = sheet.Range[fs.cs + wsRow + ":" + fe.cs + wsRow];
                pg_mr.Merge();
                pg_mr.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                pg_mr.Text = program.name + " (" + program.rank_count + ")";
                pg_mr.Style.Font.Size = 12;
                pg_mr.RowHeight = 16;

                if (fsize > 0) pg_mr.Style.Font.Size = fsize;
                if (fsize >= 16) pg_mr.RowHeight = 20;

                wsRow++;

                //標題列
                SetHeadRow(sheet, wsRow, fields, fsize, need_color: false);
                wsRow++;

                //foreach (var team in teams)
                //{
                //    SetItemCell(cfg, sheet, wsRow, team, fields);
                //    wsRow++;
                //}


                //維持八列
                var max_row = 8;
                var max_idx = teams.Count - 1;

                if (!need_empty_row)
                {
                    //不維持
                    max_row = teams.Count;
                }

                for (int i = 0; i < max_row; i++)
                {
                    if (i <= max_idx)
                    {
                        var team = teams[i];
                        SetItemCell(cfg, sheet, wsRow, team, fields, fsize);
                    }
                    wsRow++;
                }

                var rs = wsRow - max_row - 2;
                var re = wsRow - 1;
                SetRangeBorder(sheet, fs.cs + rs + ":" + fe.cs + re);

                wsRow++;
            }

            ////設定格線
            //SetRangeBorder(sheet, fields, mnRow, wsRow);

            //設定欄寬
            SetColumnWidth(sheet, fields);
        }


        private void AppendHalfSheetA4(TConfig cfg
            , Spire.Xls.Worksheet sheet
            , List<TField> fields
            , List<TProgram> programs
            , int fsize = 0
            , bool need_empty_row = true)
        {
            int wsRow = 4;

            //資料容器
            Item item = cfg.inn.newItem();

            var fs = fields.First();
            var fe = fields.Last();

            var pcnt = 0;
            foreach (var program in programs)
            {
                var teams = program.Values;

                var pg_mr = sheet.Range[fs.cs + wsRow + ":" + fe.cs + wsRow];
                pg_mr.Merge();
                pg_mr.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                pg_mr.Text = program.name + " (" + program.rank_count + ")";
                pg_mr.Style.Font.Size = 12;
                pg_mr.RowHeight = 16;

                if (fsize > 0) pg_mr.Style.Font.Size = fsize;
                if (fsize >= 16) pg_mr.RowHeight = 20;

                wsRow++;

                //標題列
                SetHeadRow(sheet, wsRow, fields, fsize, need_color: false);
                wsRow++;

                //維持八列
                var max_row = 8;
                var max_idx = teams.Count - 1;

                if (!need_empty_row)
                {
                    //不維持
                    max_row = teams.Count;
                }

                for (int i = 0; i < max_row; i++)
                {
                    if (i <= max_idx)
                    {
                        var team = teams[i];
                        SetItemCell(cfg, sheet, wsRow, team, fields, fsize);
                    }
                    else
                    {
                        SetItemCell(cfg, sheet, wsRow, cfg.itmEmpty, fields, fsize);
                    }

                    sheet.Rows[wsRow - 1].RowHeight = 17.3;
                    wsRow++;
                }

                var rs = wsRow - max_row - 2;
                var re = wsRow - 1;
                SetRangeBorder(sheet, fs.cs + rs + ":" + fe.cs + re);

                wsRow++;
                // pcnt++;
                // if (pcnt == 4)
                // {
                //     pcnt = 0;
                //     wsRow = wsRow + 5;
                // }
            }

            ////設定格線
            //SetRangeBorder(sheet, fields, mnRow, wsRow);

            //設定欄寬
            SetColumnWidth(sheet, fields);
        }

        private string RankName(TConfig cfg, TField field, Item item)
        {
            //string[] rank_arr = new string[] { "", "一", "二", "三", "四", "五", "五", "七", "七", "" };
            return item.getProperty("in_show_rank", "");
        }

        private string OrgLongName(TConfig cfg, TField field, Item item)
        {
            string map_short_org = item.getProperty("map_short_org", "");
            string in_current_org = item.getProperty("in_current_org", "");
            string in_stuff_b1 = item.getProperty("in_stuff_b1", "");
            if (in_current_org == "") in_current_org = map_short_org;

            if (in_stuff_b1 == "")
            {
                return in_current_org;
            }
            else
            {
                return in_current_org.Replace(in_stuff_b1, "");
            }
        }

        private string OrgShortName(TConfig cfg, TField field, Item item)
        {
            string map_short_org = item.getProperty("map_short_org", "");
            string in_current_org = item.getProperty("in_current_org", "");
            string in_show_org = item.getProperty("in_show_org", "");
            string in_stuff_b1 = item.getProperty("in_stuff_b1", "");
            if (in_show_org != "") return in_show_org;
            return map_short_org;
        }

        private string GetShowOrg(Item item)
        {
            var in_show_org = item.getProperty("in_show_org", "");
            if (in_show_org != "") return in_show_org;
            return item.getProperty("map_short_org", "");
        }

        private string PlayerName(TConfig cfg, TField field, Item item)
        {
            return item.getProperty("in_name", "");
        }

        private List<TGroup> MapGroup(TConfig cfg, Item items)
        {
            List<TGroup> result = new List<TGroup>();

            List<TProgram> programs = MapProgram(items);

            foreach (var program in programs)
            {
                program.sheetName = GetSheetName(cfg, program.short_name);

                var gp = result.Find(x => x.sheetName == program.sheetName);

                if (gp == null)
                {
                    gp = new TGroup
                    {
                        sheetName = program.sheetName,
                        programs = new List<TProgram>(),
                        Values = new List<Item>(),

                        Id = (result.Count + 1).ToString(),
                    };

                    gp.Tab_Id = "tab_a_" + gp.Id;
                    gp.Div_Id = "tab_div_" + gp.Id;
                    gp.Table_Id = "tab_div_tb_" + gp.Id;

                    result.Add(gp);
                }

                gp.programs.Add(program);
                gp.Values.AddRange(program.Values);
            }
            return result;
        }

        private List<TProgram> MapProgram(Item items)
        {
            List<TProgram> list = new List<TProgram>();

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string id = item.getProperty("program_id", "");
                string program_name2 = item.getProperty("program_name2", "");
                string program_short_name = item.getProperty("program_short_name", "");
                string program_rank_count = item.getProperty("program_rank_count", "0");

                var entity = list.Find(x => x.id == id);
                if (entity == null)
                {
                    entity = new TProgram
                    {
                        id = id,
                        name = program_name2,
                        short_name = program_short_name,
                        rank_count = program_rank_count,
                        in_l1 = item.getProperty("in_l1", ""),
                        in_l2 = item.getProperty("in_l2", ""),
                        in_l3 = item.getProperty("in_l3", ""),
                        weight = item.getProperty("prgoram_weight", ""),
                        Values = new List<Item>(),
                    };
                    list.Add(entity);
                }

                entity.Values.Add(item);
            }

            return list;
        }

        private string GetSheetName(TConfig cfg, string value)
        {
            //2023-06-05 下載出東亞青出問題，故移除
            //var v2 = value.Replace("男", "").Replace("女", "");
            var v2 = value;
            if (cfg.IsSumReport)
            {
                if (v2.StartsWith("團")) return v2;
                if (v2.StartsWith("格")) return v2;
            }
            else
            {
                if (v2.StartsWith("團")) return "團體賽";
                if (v2.StartsWith("格")) return "格式";
            }

            if (v2.Length == 3)
            {
                if (v2.StartsWith("小"))
                {
                    return "國小";
                }
                else
                {
                    var v3 = v2.Substring(0, 2);
                    switch (v3)
                    {
                        case "國男": return "國中組";
                        case "國女": return "國中組";
                        case "高男": return "高中組";
                        case "高女": return "高中組";
                        default: return v3;
                    }
                }
            }

            if (v2.Length == 2)
            {
                var v3 = v2.Substring(0, 1);

                switch (v3)
                {
                    case "一": return "一般";
                    case "公": return "公開";

                    case "大": return "大專";
                    case "高": return "高中";
                    case "國": return "國中";
                    case "小": return "國小";

                    case "少": return "青少年";
                    case "青": return "青年";
                    case "成": return "成年";
                    case "社": return "社會";
                    default: return "名次總表";
                }
            }

            return "名次總表";
        }

        private Item GetRankList(TConfig cfg)
        {
            string condition = "";

            if (cfg.program_id != "")
            {
                condition = "AND t1.source_id = '" + cfg.program_id + "'";
            }
            else if (cfg.in_date != "")
            {
                condition = "AND t3.in_fight_day = '" + cfg.in_date + "'";
            }

            string sql = @"
                SELECT
                    t1.in_current_org
                    , t1.in_team
                    , t1.in_name
                    , t1.in_names
                    , t1.in_sno
                    , t1.in_final_rank
                    , t1.in_show_rank
                    , t1.in_sign_no
                    , t1.in_section_no
                    , t1.map_short_org
                    , t1.map_org_name
                    , t1.in_stuff_b1
                    , t1.in_show_org
                    , t3.id             AS 'program_id'
                    , t3.in_name        AS 'program_name'
                    , t3.in_name2       AS 'program_name2'
                    , t3.in_name3       AS 'program_name3'
                    , t3.in_display     AS 'program_display'
                    , t3.in_short_name  AS 'program_short_name'
                    , t3.in_sort_order  AS 'program_sort'
                    , t3.in_rank_count  AS 'program_rank_count'
                    , t3.in_l1
                    , t3.in_l2
                    , t3.in_l3
                    , t3.in_fight_day
                    , t3.in_team_count
                    , t3.in_weight AS 'program_weight'
                FROM 
                    VU_MEETING_PTEAM t1
                LEFT OUTER JOIN
                    IN_MEETING_PEVENT t2 WITH(NOLOCK)
                    ON t2.id = t1.in_event
                INNER JOIN
                    IN_MEETING_PROGRAM t3 WITH(NOLOCK)
                    ON t3.id = t1.source_id
                WHERE
                    t1.in_meeting = '{#meeting_id}'
                    {#condition}
	                AND ISNULL(t1.in_final_rank, 0) > 0
                ORDER BY
                    t3.in_sort_order
                    , t1.in_final_rank
                    , t2.in_tree_id
                    , t1.in_foot
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                      .Replace("{#condition}", condition);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        private string GetRankDisplay(string in_final_rank, string[] rank_arr)
        {
            string rank_display = "";

            int numRank = GetIntVal(in_final_rank);

            if (numRank > rank_arr.Length)
            {
                rank_display = "";
            }
            else
            {
                if (rank_arr[numRank] != "")
                {
                    rank_display = "第 " + rank_arr[numRank] + " 名"; ;
                }
            }
            return rank_display;
        }

        private class TGroup
        {
            public string sheetName { get; set; }
            public List<TProgram> programs { get; set; }
            public List<Item> Values { get; set; }

            public string Id { get; set; }
            public string Tab_Id { get; set; }
            public string Div_Id { get; set; }
            public string Table_Id { get; set; }
        }

        private class TProgram
        {
            public string id { get; set; }
            public string name { get; set; }
            public string short_name { get; set; }
            public string rank_count { get; set; }
            public List<Item> Values { get; set; }
            public string sheetName { get; set; }
            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }
            public string weight { get; set; }
        }

        #endregion 名次清單

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }

            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }

            public string sport { get; set; }
            public string meeting_id { get; set; }
            public string in_date { get; set; }
            public string program_id { get; set; }
            public string in_gender { get; set; }
            public string scene { get; set; }

            public bool show_a { get; set; }
            public bool show_m { get; set; }
            public bool show_w { get; set; }

            public Item itmMeeting { get; set; }
            public string mt_title { get; set; }
            public string mt_battle_type { get; set; }

            public string[] CharSet { get; set; }
            public bool IsSumReport { get; set; }

            public double rank01_point { get; set; }
            public double rank02_point { get; set; }
            public double rank03_point { get; set; }
            public double rank05_point { get; set; }
            public double rank07_point { get; set; }

            public Item itmEmpty { get; set; }
            public bool IsWMG { get; set; }
            public bool isChallenge { get; set; }
        }

        #region Xlsx

        private class TExport
        {
            public string template_Path { get; set; }
            public string export_Path { get; set; }
        }

        private class TField
        {
            public string title { get; set; }
            public string property { get; set; }
            public TCss css { get; set; }
            public int ci { get; set; }
            public string cs { get; set; }
            public int width { get; set; }
            public Func<TConfig, TField, Item, string> getValue { get; set; }

            public bool is_merge { get; set; }
        }

        private enum TCss
        {
            None = 0,
            Center = 10,
            Text = 100,
            Double = 200,
        }

        private string[] GetCharSet()
        {
            return new string[] { "", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
        }

        private TExport ExportInfo(TConfig cfg, string in_name)
        {
            Item itmXls = cfg.inn.applyMethod("In_Meeting_Excel_Path", "<in_name>" + in_name + "</in_name>");
            string Export_Path = itmXls.getProperty("export_path", "");
            string Template_Path = itmXls.getProperty("template_path", "");

            TExport result = new TExport
            {
                template_Path = Template_Path,
                export_Path = Export_Path,
            };

            return result;
        }

        //設定資料列
        private void SetItemCell(TConfig cfg, Spire.Xls.Worksheet sheet, int wsRow, Item item, List<TField> fields, int fsize = 0)
        {
            foreach (var field in fields)
            {
                var cr = field.cs + wsRow;
                var va = " ";

                if (field.getValue != null)
                {
                    va = field.getValue(cfg, field, item);
                }
                else if (field.property != "")
                {
                    va = item.getProperty(field.property, "");
                }

                SetCell(sheet, cr, va, field.css, fsize);
            }
        }

        //設定資料格
        private void SetCell(Spire.Xls.Worksheet sheet, string cr, string value, TCss css, int fsizeA = 0)
        {
            var range = sheet.Range[cr];

            var fsize = fsizeA;
            if (fsize == 12)
            {
                if (value.Length >= 11) fsize = 10;
                //else if (value.Length >= 11) fsize = 11;
            }
            if (fsize > 0) range.Style.Font.Size = fsize;
            if (fsize >= 16) range.RowHeight = 20;

            switch (css)
            {
                case TCss.None:
                    range.Text = value;
                    break;
                case TCss.Center:
                    range.Text = value;
                    range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                    break;
                case TCss.Text:
                    range.Text = "'" + value;
                    range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                    break;
                case TCss.Double:
                    var v = GetDblVal(value);
                    if (v <= 0)
                    {

                    }
                    else
                    {
                        range.NumberValue = v;
                        range.NumberFormat = "0";
                        range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Right;
                        range.Style.Font.Color = System.Drawing.Color.Red;
                    }
                    break;
            }
        }

        private void SetMoney(Spire.Xls.Worksheet sheet, string cr, double value)
        {
            var range = sheet.Range[cr];
            range.NumberValue = value;
            range.NumberFormat = "$ #,##0";
        }

        //設定標題列
        private void SetHead(Spire.Xls.Worksheet sheet, string cr, string value, int fsize = 0, bool need_color = true)
        {
            var range = sheet.Range[cr];
            range.Text = value;
            range.Style.Font.IsBold = true;

            if (fsize > 0) range.Style.Font.Size = fsize;
            if (fsize >= 16) range.RowHeight = 20;

            if (need_color)
            {
                range.Style.Font.Color = System.Drawing.Color.White;
                range.Style.Color = System.Drawing.Color.FromArgb(41, 92, 144);//#295C90
            }

            range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
            range.Style.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;
        }

        //設定格線
        private void SetRangeBorder(Spire.Xls.Worksheet sheet, string pos)
        {
            var range = sheet.Range[pos];
            range.BorderInside(Spire.Xls.LineStyleType.Thin, System.Drawing.Color.Black);
            range.BorderAround(Spire.Xls.LineStyleType.Thin, System.Drawing.Color.Black);
        }

        //設定格線
        private void SetRangeBorder(Spire.Xls.Worksheet sheet, List<TField> fields, int mnRow, int wsRow)
        {
            var tbl_ps = fields.First().cs + mnRow;
            var tbl_pe = fields.Last().cs + (wsRow - 1);
            SetRangeBorder(sheet, tbl_ps + ":" + tbl_pe);
        }

        private void MapCharSet(TConfig cfg, List<TField> fields)
        {
            foreach (var field in fields)
            {
                field.cs = cfg.CharSet[field.ci];
            }
        }

        private void MapCharSetAndIndex(TConfig cfg, List<TField> fields)
        {
            for (int i = 0; i < fields.Count; i++)
            {
                var field = fields[i];
                field.ci = i + 1;
                field.cs = cfg.CharSet[field.ci];
            }
        }

        private void SetHeadRow(Spire.Xls.Worksheet sheet, int wsRow, List<TField> fields, int fsize = 0, bool need_color = true)
        {
            foreach (var field in fields)
            {
                string cr = field.cs + wsRow;
                SetHead(sheet, cr, field.title, fsize, need_color);
            }
        }

        private void AutoFit(Spire.Xls.Worksheet sheet, List<TField> fields)
        {
            foreach (var field in fields)
            {
                sheet.Columns[field.ci - 1].AutoFitColumns();
            }
        }

        private void SetColumnWidth(Spire.Xls.Worksheet sheet, List<TField> fields)
        {
            foreach (var field in fields)
            {
                sheet.Columns[field.ci - 1].ColumnWidth = field.width;
            }
        }

        #endregion Xlsx

        private string GetMtVariableValue(TConfig cfg, string in_key)
        {
            var sql = @"
                SELECT 
	                in_key
	                , in_value
                FROM 
	                IN_MEETING_VARIABLE WITH(NOLOCK) 
                WHERE
	                source_id = '{#meeting_id}'
	                AND in_key = '{#in_key}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_key}", in_key);

            var itmResult = cfg.inn.applySQL(sql);
            if (itmResult.isError() || itmResult.getResult() == "") return "";
            return itmResult.getProperty("in_value", "");
        }

        private int GetIntVal(string value, int def = 0)
        {
            int result = def;
            Int32.TryParse(value, out result);
            return result;
        }

        //轉換日期
        private string GetDateTimeValue(string value, string format, bool add8Hours = true)
        {
            if (value == "") return "";
            DateTime dt = Convert.ToDateTime(value);
            if (add8Hours)
            {
                dt = dt.AddHours(8);
            }
            return dt.ToString(format);
        }

        private double GetDblVal(string value, int def = 0)
        {
            double result = def;
            double.TryParse(value, out result);
            return result;
        }
    }
}