﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;
using WorkHelp.ArasDesk.Methods.PLMCTA.Common;

namespace WorkHelp.ArasDesk.Methods.Judo.Fight
{
    public class in_meeting_program_bulletin_coach : Item
    {
        public in_meeting_program_bulletin_coach(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 名次查詢-教練獎狀
                日誌: 
                    2024-06-05: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]in_meeting_program_bulletin_coach";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, this.dom.InnerXml);

            string meeting_id = itmR.getProperty("meeting_id", "");
            string program_id = itmR.getProperty("program_id", "");
            string mode = itmR.getProperty("mode", "");
            if (meeting_id == "")
            {
                itmR.setProperty("error_message", "賽事 id 不得為空白");
                return itmR;
            }

            //取得賽事資料
            Item itmMeeting = GetMeeting(CCO, strMethodName, inn, meeting_id);
            AppendMeeting(CCO, strMethodName, inn, itmMeeting, itmR);

            if (mode != "update")
            {
                AppendLevelOptions(CCO, strMethodName, inn, itmR);
            }
            
            if (mode == "update_coach")
            {
                UpdateCoach(CCO, strMethodName, inn, itmR);
                return itmR;
            }

            if (program_id != "")
            {
                //取得組別資料
                Item itmProgram = GetProgram(CCO, strMethodName, inn, program_id);

                //附加組別
                AppendProgram(CCO, strMethodName, inn, itmProgram, itmR);

                if (mode == "")
                {
                    //附加比賽名次
                    Item itmRanks = GetRanks(CCO, strMethodName, inn, meeting_id, program_id);
                    if (!itmRanks.isError())
                    {
                        AppendRanks(itmRanks, itmR);
                    }
                }
                else if (mode == "edit")
                {
                    string in_battle_type = itmProgram.getProperty("in_battle_type", "");
                    if (in_battle_type.StartsWith("Group"))
                    {
                        AppendChildrenPrograms(CCO, strMethodName, inn, itmProgram, itmR);
                    }
                    else
                    {
                        AppendMainProgramRank(CCO, strMethodName, inn, itmProgram, itmR);
                        AppendSummaryTeams(CCO, strMethodName, inn, itmProgram, itmR);
                    }
                }
                else if (mode == "update")
                {
                    UpdateRank(CCO, strMethodName, inn, itmR);
                }
            }

            return itmR;
        }

        private void UpdateCoach(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            var in_team = itmReturn.getProperty("team_id", "");
            var in_coach_list = itmReturn.getProperty("in_coach_list", "");
            var itmNew = inn.newItem("In_Meeting_PTeam_Coatch", "merge");
            itmNew.setAttribute("where", "in_team = '" + in_team + "'");
            itmNew.setProperty("in_team", in_team);
            itmNew.setProperty("in_coach_list", in_coach_list);
            itmNew.apply();
        }

        //附加人數統計
        private void AppendSummaryTeams(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmProgram, Item itmReturn)
        {
            string sql = "SELECT in_weight_status, in_weight_result FROM IN_MEETING_USER WITH(NOLOCK)"
                + " WHERE source_id = '" + itmProgram.getProperty("in_meeting", "") + "'"
                + " AND in_l1 = N'" + itmProgram.getProperty("in_l1", "") + "'"
                + " AND in_l2 = N'" + itmProgram.getProperty("in_l2", "") + "'"
                + " AND in_l3 = N'" + itmProgram.getProperty("in_l3", "") + "'";

            Item itmMUsers = inn.applySQL(sql);

            int count = itmMUsers.getItemCount();

            int dq_count = 0;
            int off_count = 0;
            int leave_count = 0;

            int weight_count = 0;
            int no_weight_count = 0;

            for (int i = 0; i < count; i++)
            {
                Item itmMUser = itmMUsers.getItemByIndex(i);
                string in_weight_status = itmMUser.getProperty("in_weight_status", "");
                string in_weight_result = itmMUser.getProperty("in_weight_result", "");

                switch (in_weight_status)
                {
                    case "on":
                        weight_count++;
                        break;

                    case "dq":
                        dq_count++;
                        break;

                    case "off":
                        off_count++;
                        break;

                    case "leave":
                        leave_count++;
                        break;

                    default:
                        no_weight_count++;
                        break;
                }
            }

            string draw_count = itmProgram.getProperty("in_team_count", "0");

            string count_msg = "抽籤: " + draw_count
                + "、出賽<span style='color: red'>: " + weight_count + "</span>"
                + "、DQ: " + dq_count
                + "、未到: " + off_count
                + "、請假: " + leave_count
                + "、未過磅: " + no_weight_count
                ;

            itmReturn.setProperty("team_count_msg", count_msg);
        }

        //附加 Children 組別
        private void AppendMainProgramRank(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmProgram, Item itmReturn)
        {
            string meeting_id = itmProgram.getProperty("in_meeting", "");
            string program_id = itmProgram.getProperty("id", "");

            Item itmTeams = GetTeams(CCO, strMethodName, inn, meeting_id, program_id);

            StringBuilder builder = new StringBuilder();

            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            string in_l1 = itmProgram.getProperty("in_l1", "");
            string in_battle_type = itmProgram.getProperty("in_battle_type", "");

            string hide_name = "";
            string hide_team = "";
            string hide_group = in_battle_type.StartsWith("Group") ? "" : "data-visible='false'";

            if (in_l1 == "團體組")
            {
                hide_name = "data-visible='false'";
            }
            else
            {
                hide_team = "data-visible='false'";
            }

            int count = itmTeams.getItemCount();

            head.Append("<thead>");
            head.Append("<tr>");
            head.Append("<th class='text-center' " + hide_name + ">姓名</th>");
            head.Append("<th class='text-center'>單位</th>");
            head.Append("<th class='text-center'" + hide_team + ">隊伍</th>");
            head.Append("<th class='text-center'>組別量級</th>");
            head.Append("<th class='text-center'" + hide_group + ">分組</th>");
            head.Append("<th class='text-center'" + hide_group + ">分組編號</th>");
            head.Append("<th class='text-center'>籤號</th>");
            head.Append("<th class='text-center' data-width='10%'>名次</th>");
            head.Append("<th class='text-center' data-width='10%'>教練</th>");
            head.Append("<th class='text-center' data-width='10%'>功能</th>");
            head.Append("</tr>");
            head.Append("</thead>");


            body.Append("<tbody>");
            for (int i = 0; i < count; i++)
            {
                Item itmTeam = itmTeams.getItemByIndex(i);
                string in_name = itmTeam.getProperty("in_name", "");
                string in_weight_message = itmTeam.getProperty("in_weight_message", "");
                if (in_weight_message != "") in_name += "<label style='color: red'>" + in_weight_message + "</label>";

                body.Append("<tr>");
                body.Append("<td class='ctrl_search'>" + in_name + "</td>");
                body.Append("<td class='ctrl_search'>" + itmTeam.getProperty("map_org_name", "") + "</td>");
                body.Append("<td class='ctrl_search'>" + itmTeam.getProperty("in_team", "") + "</td>");
                body.Append("<td class='text-left'>" + itmProgram.getProperty("in_name3", "") + "</td>");
                body.Append("<td class='text-center'>" + itmProgram.getProperty("in_group_display", "") + "</td>");
                body.Append("<td class='text-center'>" + itmTeam.getProperty("in_part_code", "") + "</td>");
                body.Append("<td>" + itmTeam.getProperty("in_sign_no", "") + "</td>");
                body.Append("<td>" + RankInput(itmTeam) + "</td>");
                body.Append("<td>" + CoachInput(itmTeam) + "</td>");
                body.Append("<td>" + Buttons(CCO, strMethodName, inn, itmTeam) + "</td>");
                body.Append("</tr>");
            }
            body.Append("</tbody>");

            string table_name = "table_" + program_id;

            builder.Append("<div extractor='searchResult'>");

            builder.Append("<table id='" + table_name + "' class='table-bordered rwdtable'"
                + " data-toggle='table'"
                + " data-search-align='right'"
                + " data-search='true'"
                + " data-pagination='true'"
                + " data-page-size='30'"
                + " data-click-to-select='true'"
                + " data-maintain-selected='true'"
                + " data-show-pagination-switch='true'"
                + " data-striped='true'"
                + " data-show-toggle='false'"
                + " data-show-columns='true'"
                + " data-toolbar='#toolbar'"
                + "data-card-view='true'>");
            builder.Append(head);
            builder.Append(body);
            builder.Append("</table>");

            builder.Append("<script>");
            builder.Append("    ih.initSearchBootstrapTable();");
            builder.Append("</script>");

            builder.Append("</div>");

            itmReturn.setProperty("inn_tables", builder.ToString());
        }

        private string Buttons(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmTeam)
        {
            string result = "<button class='btn btn-sm btn-primary'"
                + " data-tid='" + itmTeam.getProperty("team_id", "") + "'"
                + " onclick='ExpMedalWord_Click(this)'"
                + ">下載獎狀</button>";

            return result;
        }

        //附加 Children 組別
        private void AppendChildrenPrograms(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmProgram, Item itmReturn)
        {
            string meeting_id = itmProgram.getProperty("in_meeting", "");
            string program_id = itmProgram.getProperty("id", "");

            Item itmChildrenPrograms = GetChildrenPrograms(CCO, strMethodName, inn, program_id);

            int count = itmChildrenPrograms.getItemCount();

            if (count <= 0)
            {
                return;
            }

            StringBuilder builder = new StringBuilder();

            for (int i = 0; i < count; i++)
            {
                Item itmChildrenProgram = itmChildrenPrograms.getItemByIndex(i);
                string children_program = itmChildrenProgram.getProperty("id", "");
                string in_group_display = itmChildrenProgram.getProperty("in_group_display", "");
                string in_battle_type = itmChildrenProgram.getProperty("in_battle_type", "");

                Item itmTeams = GetTeams(CCO, strMethodName, inn, meeting_id, children_program);

                //附加比賽隊伍
                if (itmTeams.isError())
                {
                    continue;
                }

                AppendRankTable(builder, itmProgram, itmChildrenProgram, itmTeams);
            }

            itmReturn.setProperty("inn_tables", builder.ToString());
        }

        private void AppendRankTable(StringBuilder builder, Item itmParent, Item itmChildren, Item itmTeams)
        {
            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            string in_l1 = itmParent.getProperty("in_l1", "");
            string in_battle_type = itmParent.getProperty("in_battle_type", "");

            string hide_name = "";
            string hide_team = "";
            string hide_group = in_battle_type.StartsWith("Group") ? "" : "data-visible='false'";

            if (in_l1 == "團體組")
            {
                hide_name = "data-visible='false'";
            }
            else
            {
                hide_team = "data-visible='false'";
            }

            int count = itmTeams.getItemCount();

            head.Append("<thead>");
            head.Append("<tr>");
            head.Append("<th class='text-center' " + hide_name + ">姓名</th>");
            head.Append("<th class='text-center'>單位</th>");
            head.Append("<th class='text-center'" + hide_team + ">隊伍</th>");
            head.Append("<th class='text-center'>組別量級</th>");
            head.Append("<th class='text-center'" + hide_group + ">分組</th>");
            head.Append("<th class='text-center'" + hide_group + ">分組編號</th>");
            head.Append("<th class='text-center'>籤號</th>");
            head.Append("<th class='text-center' data-width='10%'>名次</th>");
            head.Append("</tr>");
            head.Append("</thead>");


            body.Append("<tbody>");
            for (int i = 0; i < count; i++)
            {
                Item itmTeam = itmTeams.getItemByIndex(i);
                body.Append("<tr>");
                body.Append("<td class='ctrl_search'>" + itmTeam.getProperty("in_name", "") + "</td>");
                body.Append("<td class='ctrl_search'>" + itmTeam.getProperty("map_org_name", "") + "</td>");
                body.Append("<td class='ctrl_search'>" + itmTeam.getProperty("in_team", "") + "</td>");
                body.Append("<td class='text-left'>" + itmChildren.getProperty("in_name3", "") + "</td>");
                body.Append("<td class='text-center'>" + itmChildren.getProperty("in_group_display", "") + "</td>");
                body.Append("<td class='text-center'>" + itmTeam.getProperty("in_part_code", "") + "</td>");
                body.Append("<td>" + itmTeam.getProperty("in_sign_no", "") + "</td>");
                body.Append("<td>" + RankInput(itmTeam) + "</td>");
                body.Append("</tr>");
            }
            body.Append("</tbody>");

            string table_name = "table_" + itmChildren.getProperty("id", "");

            builder.Append("<div extractor='searchResult'>");

            builder.Append("<table id='" + table_name + "' class='table-bordered rwdtable'"
                + " data-toggle='table'"
                + " data-search-align='right'"
                + " data-search='true'"
                + " data-pagination='true'"
                + " data-click-to-select='true'"
                + " data-maintain-selected='true'"
                + " data-show-pagination-switch='true'"
                + " data-striped='true'"
                + " data-show-toggle='false'"
                + " data-show-columns='true'"
                + " data-toolbar='#toolbar'"
                + "data-card-view='true'>");
            builder.Append(head);
            builder.Append(body);
            builder.Append("</table>");

            builder.Append("<script>");
            builder.Append("    ih.initSearchBootstrapTable();");
            builder.Append("</script>");

            builder.Append("</div>");
        }

        private string RankInput(Item itmTeam)
        {
            string in_final_rank = itmTeam.getProperty("in_final_rank", "");

            if (in_final_rank == "0")
            {
                in_final_rank = "";
            }

            return "<input type='text' class='form-control' value='" + in_final_rank + "' "
                + " style ='text-align: right;'"
                + " onkeyup = \"doUpdateRank('" + itmTeam.getProperty("team_id", "") + "', this)\">";
        }

        private string CoachInput(Item itmTeam)
        {
            string in_coach_list = itmTeam.getProperty("in_coach_list", "");

            return "<input type='text' class='form-control' value='" + in_coach_list + "' "
                + " style ='text-align: right;'"
                + " onkeyup = \"doUpdateCoach('" + itmTeam.getProperty("team_id", "") + "', this)\">";
        }

        private void UpdateRank(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            string sql = "";

            string team_id = itmReturn.getProperty("team_id", "");
            string in_final_rank = itmReturn.getProperty("in_final_rank", "");

            //sql = "UPDATE IN_MEETING_PTEAM SET in_final_rank = '" + in_final_rank + "' WHERE id = '" + team_id + "'";

            sql = "UPDATE IN_MEETING_PTEAM SET in_final_rank = '" + in_final_rank + "', in_show_rank = '" + in_final_rank + "'"
                + " WHERE id = '" + team_id + "'";

            Item itmSQL = inn.applySQL(sql);

            if (itmSQL.isError())
            {
                throw new Exception("更新名次發生錯誤");
            }
        }

        private void AppendMeeting(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmMeeting, Item itmReturn)
        {
            itmReturn.setProperty("in_title", itmMeeting.getProperty("in_title", ""));
        }

        private void AppendProgram(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmProgram, Item itmReturn)
        {
            //itmReturn.setProperty("in_title", itmProgram.getProperty("in_title", ""));
            itmReturn.setProperty("in_l1", itmProgram.getProperty("in_l1", ""));
            itmReturn.setProperty("in_l2", itmProgram.getProperty("in_l2", ""));
            itmReturn.setProperty("in_l3", itmProgram.getProperty("in_l3", ""));
            itmReturn.setProperty("program_display", itmProgram.getProperty("program_display", ""));
        }

        private void AppendLevelOptions(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            string meeting_id = itmReturn.getProperty("meeting_id", "");

            Item itmJson = inn.newItem("In_Meeting");
            itmJson.setProperty("meeting_id", meeting_id);
            itmJson.setProperty("need_id", "1");
            itmJson = itmJson.apply("in_meeting_program_options");
            itmReturn.setProperty("in_level_json", itmJson.getProperty("json", ""));
            itmReturn.setProperty("in_level_count", itmJson.getProperty("total", ""));
        }


        //附加比賽名次
        private void AppendRanks(Item items, Item itmReturn)
        {
            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);

                string in_final_rank = item.getProperty("in_final_rank", "");
                string rank_display = "第 " + in_final_rank + " 名"; ;
                if (in_final_rank == "1")
                {
                    string icon = "<i class='fa fa-trophy' style='color: gold;'></i>";
                    rank_display = icon + rank_display + icon;
                }
                else if (in_final_rank == "0")
                {
                    rank_display = "";
                }

                item.setType("inn_rank");
                item.setProperty("rank_display", rank_display);
                itmReturn.addRelationship(item);
            }
        }

        //取得賽事資訊
        private Item GetMeeting(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string meeting_id)
        {
            string sql = @"SELECT id, in_title FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + meeting_id + "'";
            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);
            return inn.applySQL(sql);
        }

        //取得組別資訊
        private Item GetProgram(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string program_id)
        {
            string sql = @"SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + program_id + "'";
            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);
            return inn.applySQL(sql);
        }

        //取得 Children 組別資訊
        private Item GetChildrenPrograms(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string program_id, string in_group_tag = "")
        {
            string group_condition = in_group_tag == "" ? "" : "AND t1.in_group_tag = N'" + in_group_tag + "'";

            string sql = @"
        SELECT
            t1.*
            , t2.label AS 'program_battle_type'
        FROM
            IN_MEETING_PROGRAM t1 WITH(NOLOCK)
        LEFT OUTER JOIN
        (
            SELECT
                t12.value AS 'value'
                , t12.label_zt AS 'label'
            FROM
                [LIST] t11 WITH(NOLOCK)
            INNER JOIN
                [VALUE] t12 WITH(NOLOCK)
                ON t12.source_id = t11.id
            WHERE
                t11.name = N'In_Meeting_BattleType'
        ) t2
            ON t1.in_battle_type = t2.value
        WHERE
            t1.in_program = '{#program_id}'
            {#group_condition}
        ORDER BY
            t1.in_group_type
            , t1.in_group_id
        ";

            sql = sql.Replace("{#program_id}", program_id)
                .Replace("{#group_condition}", group_condition);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return inn.applySQL(sql);
        }

        //取得賽事組別名次
        private Item GetTeams(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string meeting_id, string program_id)
        {
            string sql = @"
                SELECT
                    t1.id AS 'team_id'
                    , t1.in_current_org
                    , t1.in_team
                    , t1.in_name
                    , t1.in_sno
                    , t1.in_final_rank
                    , t1.in_sign_no
                    , t1.in_part_id
                    , t1.in_part_code
                    , t1.map_short_org
                    , t1.map_org_name
                    , t1.in_weight_result
                    , t1.in_weight_message
                    , t1.in_check_result
                    , t4.in_coach_list
                FROM 
                    VU_MEETING_PTEAM t1
                LEFT OUTER JOIN
                    IN_MEETING_PEVENT t2 WITH(NOLOCK)
                    ON t2.id = t1.in_event
                LEFT OUTER JOIN
                    IN_MEETING_PTEAM_COATCH t4 WITH(NOLOCK)
                    ON t4.in_team = t1.id
                WHERE
                    t1.in_meeting = '{#meeting_id}'
                    AND t1.source_id = '{#program_id}'
                    AND ISNULL(t1.in_type, '') <> 's'
                ORDER BY
                    CASE 
                        WHEN t1.in_final_rank IS NULL THEN 1 
                        WHEN t1.in_final_rank = 0 THEN 999 
                        ELSE 0 END
                    , t1.in_final_rank
                    , t2.in_tree_id
                    , t1.in_foot
                    , t1.in_city_no
                    , t1.in_stuff_b1
                    , t1.in_current_org
                    , t1.in_team
                    , t1.in_sno
            ";

            sql = sql.Replace("{#meeting_id}", meeting_id)
                .Replace("{#program_id}", program_id); ;

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return inn.applySQL(sql);
        }

        //取得賽事組別名次
        private Item GetRanks(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, string meeting_id, string program_id)
        {
            string sql = @"
                SELECT
                    t1.in_current_org
                    , t1.in_name
                    , t1.in_sno
                    , t1.in_final_rank
                    , t1.in_sign_no
                    , t1.map_short_org
                    , t1.map_org_name
                    , t1.in_weight_message
                    , t3.in_display AS 'program_display'
                    , t4.in_coach_list
                FROM 
                    VU_MEETING_PTEAM t1
                LEFT OUTER JOIN
                    IN_MEETING_PEVENT t2 WITH(NOLOCK)
                    ON t2.id = t1.in_event
                INNER JOIN
                    IN_MEETING_PROGRAM t3 WITH(NOLOCK)
                    ON t3.id = t1.source_id
                LEFT OUTER JOIN
                    IN_MEETING_PTEAM_COATCH t4 WITH(NOLOCK)
                    ON t4.in_team = t1.id
                WHERE
                    t1.in_meeting = '{#meeting_id}'
                    AND t1.source_id = '{#program_id}'
                    AND ISNULL(t1.in_final_rank, 0) > 0
                ORDER BY
                    t1.in_final_rank
                    , t2.in_tree_id
                    , t1.in_foot
            ";

            sql = sql.Replace("{#meeting_id}", meeting_id)
                .Replace("{#program_id}", program_id); ;

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            return inn.applySQL(sql);
        }

        private class TNode
        {
            public string Val { get; set; }

            public string Lbl { get; set; }

            public int Sort { get; set; }

            public List<TNode> Nodes { get; set; }
        }

        private int GetIntVal(string value, int def = 0)
        {
            int result = def;
            Int32.TryParse(value, out result);
            return result;
        }
    }
}