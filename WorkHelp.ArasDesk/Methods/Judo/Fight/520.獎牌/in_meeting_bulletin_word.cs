﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Judo.Fight
{
    public class in_meeting_bulletin_word : Item
    {
        public in_meeting_bulletin_word(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
    目的: 下載獎狀
    輸入: meeting_id, team_id
    日期: 
        2022-08-18: 團體賽調整(單位、教練獎狀) (lina)
        2021-11-09: 創建 (lina)
*/

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]in_meeting_bulletin_word";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, this.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                meeting_id = itmR.getProperty("meeting_id", ""),
                program_id = itmR.getProperty("program_id", ""),
                team_id = itmR.getProperty("team_id", ""),
                coach_count = itmR.getProperty("coach_count", ""),
                scene = itmR.getProperty("scene", ""),
            };

            cfg.itmMeeting = cfg.inn.applySQL("SELECT in_title, in_document_number FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            if (cfg.itmMeeting.isError() || cfg.itmMeeting.getResult() == "")
            {
                throw new Exception("賽事資訊錯誤");
            }

            cfg.MeetingTitle = cfg.itmMeeting.getProperty("in_title", "");
            cfg.DocumentNumber = cfg.itmMeeting.getProperty("in_document_number", "");

            switch (cfg.scene)
            {
                case "top5":
                    Top5Players(cfg, itmR);
                    break;

                default:
                    OnePlayer(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void Top5Players(TConfig cfg, Item itmReturn)
        {
            cfg.itmProgram = cfg.inn.applySQL("SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + cfg.program_id + "'");
            if (cfg.itmProgram.isError() || cfg.itmProgram.getResult() == "")
            {
                throw new Exception("組別資訊錯誤");
            }

            string sql = @"
                SELECT t1.* FROM VU_MEETING_PTEAM t1 
                LEFT OUTER JOIN IN_MEETING_PEVENT t2 WITH(NOLOCK)
                ON t2.id = t1.in_event
                WHERE t1.source_id = '{#program_id}'
                AND ISNULL(t1.in_final_rank, 999) > 0 
                AND ISNULL(t1.in_final_rank, 999) <= 7
                ORDER BY t1.in_final_rank, t2.in_tree_id, t1.in_foot
            ";

            sql = sql.Replace("{#program_id}", cfg.program_id);
            Item itmPTeams = cfg.inn.applySQL(sql);
            if (itmPTeams.isError() || itmPTeams.getResult() == "")
            {
                throw new Exception("隊伍資訊錯誤");
            }

            //設定組別資訊
            SetProgram(cfg);

            cfg.WeightDisplay = GetWeightDisplay(cfg);

            var list = new List<TOrgPlayer>();

            var team_count = itmPTeams.getItemCount();
            for (int i = 0; i < team_count; i++)
            {
                var itmPTeam = itmPTeams.getItemByIndex(i);
                var team = MapTeam(cfg, itmPTeam);
                AppendPlayers(cfg, list, team);
            }

            foreach (var entity in list)
            {
                entity.FullName = entity.OrgName + " " + entity.PlayerName;
                entity.RankDisplay = GetRankDisplay(cfg, entity.ShowRank);
            }

            var exp = ExportInfo(cfg, "rank_medal_path", cfg.itmProgram.getProperty("in_name2", ""));

            //載入模板
            using (var doc = Xceed.Words.NET.DocX.Load(exp.template_Path))
            {
                int end_idx = list.Count - 1;
                for (int i = 0; i < list.Count; i++)
                {
                    AppendWordTable(cfg, doc, list[i], i == end_idx);
                }

                doc.Tables[0].Remove();
                doc.RemoveParagraphAt(0);

                doc.SaveAs(exp.File);
            }

            itmReturn.setProperty("xls_name", exp.Url);
        }

        private void OnePlayer(TConfig cfg, Item itmReturn)
        {
            Item itmPTeam = cfg.inn.applySQL("SELECT * FROM VU_MEETING_PTEAM WITH(NOLOCK) WHERE id = '" + cfg.team_id + "'");
            if (itmPTeam.isError() || itmPTeam.getResult() == "")
            {
                throw new Exception("隊伍資訊錯誤");
            }

            cfg.program_id = itmPTeam.getProperty("source_id", "");
            cfg.itmProgram = cfg.inn.applySQL("SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + cfg.program_id + "'");
            if (cfg.itmProgram.isError() || cfg.itmProgram.getResult() == "")
            {
                throw new Exception("組別資訊錯誤");
            }

            //設定組別資訊
            SetProgram(cfg);

            cfg.WeightDisplay = GetWeightDisplay(cfg);

            var list = new List<TOrgPlayer>();
            var team = MapTeam(cfg, itmPTeam);
            AppendPlayers(cfg, list, team);

            if (list.Count == 0)
            {
                var player = new TOrgPlayer
                {
                    OrgName = team.in_short_org,
                    PlayerName = team.in_name,
                    FinalRank = team.in_final_rank,
                    ShowRank = team.in_show_rank,
                };
                list.Add(player);
            }

            foreach (var entity in list)
            {
                entity.FullName = entity.OrgName + " " + entity.PlayerName;
                entity.RankDisplay = GetRankDisplay(cfg, entity.ShowRank);
            }

            TExport exp = ExportInfo(cfg, "rank_medal_path", cfg.InL2 + "_" + team.in_short_org);

            //載入模板
            using (var doc = Xceed.Words.NET.DocX.Load(exp.template_Path))
            {
                int end_idx = list.Count - 1;
                for (int i = 0; i < list.Count; i++)
                {
                    AppendWordTable(cfg, doc, list[i], i == end_idx);
                }

                doc.Tables[0].Remove();
                doc.RemoveParagraphAt(0);
                //doc.RemoveParagraphAt(0);

                doc.SaveAs(exp.File);
            }

            itmReturn.setProperty("xls_name", exp.Url);
        }

        private void AppendWordTable(TConfig cfg, Xceed.Words.NET.DocX doc, TOrgPlayer player, bool is_end)
        {
            DateTime now = DateTime.Now;
            string tw_year = (now.Year - 1911).ToString();
            string mt_title = cfg.MeetingTitle;
            string pname = "";

            if (cfg.IsTeamBattle)
            {
                pname = cfg.itmProgram.getProperty("in_name3", "")
                    + " "
                    + player.RankDisplay;
            }
            else if (cfg.IsKata)
            {
                pname = " 格式 " + cfg.InL2.Replace("格-", "") + "-" + cfg.InL3
                    + " "
                    + player.RankDisplay;
            }
            else
            {
                pname = cfg.itmProgram.getProperty("in_name2", "")
                    + "："
                    + cfg.WeightDisplay
                    + " "
                    + player.RankDisplay;
            }

            //var p = doc.InsertParagraph();

            //取得模板表格
            var template_table = doc.Tables[0];
            if (!cfg.hasAnalysis)
            {
                LoadDocPosition(cfg, template_table);
                cfg.hasAnalysis = true;
            }

            var table = doc.InsertTable(template_table);

            var docNumArr = cfg.DocumentNumber.Split(new char[] { '、' }, StringSplitOptions.RemoveEmptyEntries);
            if (docNumArr == null || docNumArr.Length == 0) docNumArr = new string[] { "", "", "" };
            var docNum1 = docNumArr.Length > 0 ? docNumArr[0] : "";
            var docNum2 = docNumArr.Length > 1 ? docNumArr[1] : "";
            var docNum3 = docNumArr.Length > 2 ? docNumArr[2] : "";


            ResetTableRowCell(table, cfg.docNum1Pos, docNum1);
            ResetTableRowCell(table, cfg.docNum2Pos, docNum2);
            ResetTableRowCell(table, cfg.docNum3Pos, docNum3);


            ResetTableRowCell(table, cfg.playerPos, player.FullName);

            var meetingInfo = "參加" + mt_title.Replace("中華民國柔道總會", "中華民國").Replace("112 ", "112");
            ResetTableRowCell(table, cfg.meetingPos, meetingInfo);

            var sectInfo = "榮獲" + pname;
            ResetTableRowCell(table, cfg.sectPos, sectInfo);

            ResetTableRowCell(table, cfg.yPos, tw_year);
            ResetTableRowCell(table, cfg.mPos, cfg.FightM);
            ResetTableRowCell(table, cfg.dPos, cfg.FightD);

            if (!is_end)
            {
                var pend = doc.InsertParagraph();
                pend.InsertPageBreakAfterSelf();
            }
        }

        private void ResetTableRowCell(Xceed.Document.NET.Table table, TPos pos, string value)
        {
            if (pos == null) return;
            try
            {
                var cell = table.Rows[pos.ri].Cells[pos.ci];
                cell.ReplaceText(pos.pn, value);
            }
            catch
            {

            }
        }

        private void LoadDocPosition(TConfig cfg, Xceed.Document.NET.Table table)
        {
            var rows = table.Rows;
            for (var j = 0; j < rows.Count; j++)
            {
                var row = rows[j];
                var cells = row.Cells;
                for (var k = 0; k < cells.Count; k++)
                {
                    var cell = cells[k];
                    foreach (var p in cell.Paragraphs)
                    {
                        string text = p.Text;
                        if (string.IsNullOrWhiteSpace(text)) continue;
                        cfg.docNum1Pos = AppendPosition(text, "document_number1", j, k, cfg.docNum1Pos);
                        cfg.docNum2Pos = AppendPosition(text, "document_number2", j, k, cfg.docNum2Pos);
                        cfg.docNum3Pos = AppendPosition(text, "document_number3", j, k, cfg.docNum3Pos);
                        cfg.playerPos = AppendPosition(text, "player_name", j, k, cfg.playerPos);
                        cfg.meetingPos = AppendPosition(text, "activity_info", j, k, cfg.meetingPos);
                        cfg.sectPos = AppendPosition(text, "program_info", j, k, cfg.sectPos);
                        cfg.yPos = AppendPosition(text, "y", j, k, cfg.sectPos);
                        cfg.mPos = AppendPosition(text, "m", j, k, cfg.sectPos);
                        cfg.dPos = AppendPosition(text, "d", j, k, cfg.sectPos);
                    }
                }
            }
        }

        private TPos AppendPosition(string text, string name, int ridx, int cidx, TPos old)
        {
            if (text.Contains("$"+ text + "$"))
            {
                return new TPos { ri = ridx, ci = cidx, pn = "[$"+ name + "$]" };
            }
            return old;
        }

        private TExport ExportInfo(TConfig cfg, string in_name, string file_title)
        {
            Item itmXls = cfg.inn.applyMethod("In_Variable_File", "<in_name>" + in_name + "</in_name>");

            var result = new TExport
            {
                template_Path = itmXls.getProperty("template_path", ""),
                export_Path = itmXls.getProperty("export_path", ""),
            };

            string mt_title = cfg.MeetingTitle;

            string ext_name = ".docx";
            string doc_name = mt_title + "_" + file_title + "_獎狀_" + DateTime.Now.ToString("yyyyMMdd_HHmmss");

            string export_path = result.export_Path.TrimEnd('\\');
            string doc_file = export_path + "\\" + doc_name + ext_name;
            string doc_url = doc_name + ext_name;

            result.File = doc_file;
            result.Url = doc_url;

            return result;
        }

        private string GetRankDisplay(TConfig cfg, string in_show_rank)
        {
            switch (in_show_rank)
            {
                case "1": return "冠軍";
                case "2": return "亞軍";
                case "3": return "季軍";
                case "4": return "第四名";
                case "5": return "第五名";
                case "6": return "第六名";
                case "7": return "第七名";
                case "8": return "第八名";
                default: return "無名次";
            }
        }

        private string GetWeightDisplay(TConfig cfg)
        {
            if (cfg.IsTeamBattle) return "";

            string[] arr = cfg.InL3.Split(new char[] { '：' }, StringSplitOptions.RemoveEmptyEntries);
            if (arr == null || arr.Length == 0)
            {
                return "";
            }

            //return arr.Last().ToUpper();
            return arr.Last().Replace("KG", "Kg");
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }

            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string meeting_id { get; set; }
            public string program_id { get; set; }
            public string team_id { get; set; }
            public string coach_count { get; set; }
            public string scene { get; set; }

            public Item itmMeeting { get; set; }
            public Item itmProgram { get; set; }
            public string MeetingTitle { get; set; }
            public string DocumentNumber { get; set; }
            public string InL1 { get; set; }
            public string InL2 { get; set; }
            public string InL3 { get; set; }
            public string InFightDay { get; set; }
            public string FightY { get; set; }
            public string FightM { get; set; }
            public string FightD { get; set; }
            public bool IsTeamBattle { get; set; }
            public bool IsKata { get; set; }
            public string WeightDisplay { get; set; }

            public bool hasAnalysis { get; set; }
            public TPos docNum1Pos { get; set; }
            public TPos docNum2Pos { get; set; }
            public TPos docNum3Pos { get; set; }
            public TPos playerPos { get; set; }
            public TPos meetingPos { get; set; }
            public TPos sectPos { get; set; }
            public TPos yPos { get; set; }
            public TPos mPos { get; set; }
            public TPos dPos { get; set; }
        }

        public class TPos
        {
            public int ri { get; set; }
            public int ci { get; set; }
            public string pn { get; set; }
        }

        private class TExport
        {
            public string template_Path { get; set; }
            public string export_Path { get; set; }
            public string File { get; set; }
            public string Url { get; set; }
        }

        private class TOrgPlayer
        {
            public string OrgName { get; set; }
            public string PlayerName { get; set; }
            public string FullName { get; set; }
            public string FinalRank { get; set; }
            public string ShowRank { get; set; }
            public string RankDisplay { get; set; }
        }

        private class TTeam
        {
            public string in_short_org { get; set; }
            public string in_final_rank { get; set; }
            public string in_show_rank { get; set; }
            public string in_names { get; set; }
            public string in_name { get; set; }
            public string in_team { get; set; }
            public Item item { get; set; }
        }

        private void AppendPlayers2(TConfig cfg, List<TOrgPlayer> list, TTeam team)
        {
            string in_l1 = cfg.itmProgram.getProperty("in_l1", "");
            string in_l2 = cfg.itmProgram.getProperty("in_l2", "");
            string in_l3 = cfg.itmProgram.getProperty("in_l3", "");
            string in_creator_sno = team.item.getProperty("in_creator_sno", "");
            string in_index = team.item.getProperty("in_index", "");

            string sql = @"
                SELECT 
                	* 
                FROM 
                	IN_MEETING_USER WITH(NOLOCK) 
                WHERE 
                	source_id = '{#meeting_id}'
                	AND in_l1 = N'{#in_l1}'
                	AND in_l2 = N'{#in_l2}'
                	AND in_l3 = N'{#in_l3}'
                	AND in_creator_sno = '{#in_creator_sno}'
                	AND in_index = '{#in_index}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_l1}", in_l1)
                .Replace("{#in_l2}", in_l2)
                .Replace("{#in_l3}", in_l3)
                .Replace("{#in_creator_sno}", in_creator_sno)
                .Replace("{#in_index}", in_index);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            Item itmMUser = cfg.inn.applySQL(sql);

            if (!itmMUser.isError() && itmMUser.getResult() != "")
            {
                string nm = itmMUser.getProperty("in_exe_a1", "");
                string org = itmMUser.getProperty("in_exe_a4", "");
                string code = itmMUser.getProperty("in_stuff_b1", "");
                org = org.Replace(code, "");

                var entity = new TOrgPlayer
                {
                    OrgName = org,
                    PlayerName = nm,
                    FinalRank = team.in_final_rank,
                    ShowRank = team.in_show_rank,
                };
                list.Add(entity);
            }

        }

        private void AppendPlayers(TConfig cfg, List<TOrgPlayer> list, TTeam team)
        {
            var nms = team.in_names.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            if (nms != null && nms.Length > 0)
            {
                if (cfg.IsTeamBattle)
                {
                    var orgEntity = new TOrgPlayer
                    {
                        OrgName = team.in_short_org,
                        PlayerName = "",
                        FinalRank = team.in_final_rank,
                        ShowRank = team.in_show_rank,
                    };
                    list.Add(orgEntity);
                }

                if (cfg.IsTeamBattle)
                {
                    var coachEntity = new TOrgPlayer
                    {
                        OrgName = team.in_short_org,
                        PlayerName = "教練",
                        FinalRank = team.in_final_rank,
                        ShowRank = team.in_show_rank,
                    };
                    list.Add(coachEntity);

                    if (cfg.coach_count == "2")
                    {
                        var coachEntity2 = new TOrgPlayer
                        {
                            OrgName = team.in_short_org,
                            PlayerName = "教練",
                            FinalRank = team.in_final_rank,
                            ShowRank = team.in_show_rank,
                        };
                        list.Add(coachEntity2);
                    }

                    if (cfg.coach_count == "3")
                    {
                        var coachEntity31 = new TOrgPlayer
                        {
                            OrgName = team.in_short_org,
                            PlayerName = "教練",
                            FinalRank = team.in_final_rank,
                            ShowRank = team.in_show_rank,
                        };
                        list.Add(coachEntity31);
                        var coachEntity32 = new TOrgPlayer
                        {
                            OrgName = team.in_short_org,
                            PlayerName = "教練",
                            FinalRank = team.in_final_rank,
                            ShowRank = team.in_show_rank,
                        };
                        list.Add(coachEntity32);
                    }
                }

                foreach (var nm in nms)
                {
                    var entity = new TOrgPlayer
                    {
                        OrgName = team.in_short_org,
                        PlayerName = nm,
                        FinalRank = team.in_final_rank,
                        ShowRank = team.in_show_rank,
                    };
                    list.Add(entity);
                }

                if (cfg.IsKata)
                {
                    AppendPlayers2(cfg, list, team);
                }
            }
        }

        private void SetProgram(TConfig cfg)
        {
            cfg.InL1 = cfg.itmProgram.getProperty("in_l1", "");
            cfg.InL2 = cfg.itmProgram.getProperty("in_l2", "");
            cfg.InL3 = cfg.itmProgram.getProperty("in_l3", "");

            cfg.InFightDay = cfg.itmProgram.getProperty("in_fight_day", "");
            if (cfg.InFightDay != "")
            {
                var arr = cfg.InFightDay.Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);
                if (arr.Length == 3)
                {
                    cfg.FightY = arr[0];
                    cfg.FightM = arr[1];
                    cfg.FightD = arr[2];
                }
                else
                {
                    var now = DateTime.Now;
                    cfg.FightY = now.Year.ToString();
                    cfg.FightM = now.Month.ToString().PadLeft(2, '0');
                    cfg.FightD = now.Day.ToString().PadLeft(2, '0');
                }
            }
            else
            {
                var now2 = DateTime.Now;
                cfg.FightY = now2.Year.ToString();
                cfg.FightM = now2.Month.ToString().PadLeft(2, '0');
                cfg.FightD = now2.Day.ToString().PadLeft(2, '0');
            }

            cfg.IsTeamBattle = cfg.InL1.Contains("團體");
            cfg.IsKata = cfg.InL1 == "格式組";
        }

        private TTeam MapTeam(TConfig cfg, Item item)
        {
            var result = new TTeam
            {
                in_short_org = item.getProperty("map_short_org", ""),
                in_final_rank = item.getProperty("in_final_rank", "0"),
                in_show_rank = item.getProperty("in_show_rank", "0"),
                in_names = item.getProperty("in_names", ""),
                in_name = item.getProperty("in_name", ""),
                in_team = item.getProperty("in_team", ""),
                item = item,

            };

            result.in_names = result.in_names.Replace("(" + result.in_team + "隊)", "").Trim();

            return result;
        }
    }
}