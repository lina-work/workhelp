﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Judo.Fight
{
    public class in_meeting_program_score_xls : Item
    {
        public in_meeting_program_score_xls(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
    目的: 晉段測驗成績
    日期:
        - 2022-05-06 大改 (lina)
        - 2022-03-03 調整 (lina)
        - 2022-01-20 創建 (lina)
*/

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]" + "In_Cla_Meeting_Promotion";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),

                meeting_id = itmR.getProperty("itemid", ""),
                in_qry = itmR.getProperty("in_qry", ""),
                scene = itmR.getProperty("scene", ""),
                cmt_sno = itmR.getProperty("cmt_sno", ""),
                cmt_city = itmR.getProperty("cmt_city", ""),

                doc_name = itmR.getProperty("doc_name", ""),
                CharSet = GetCharSet(),
                font_size = 12,
                row_height = 24,
            };

            if (cfg.meeting_id == "")
            {
                cfg.meeting_id = itmR.getProperty("meeting_id", "");
            }

            cfg.itmMeeting = MeetingInfo(cfg, cfg.meeting_id);
            cfg.mt_title = cfg.itmMeeting.getProperty("in_title", "");

            switch (cfg.scene)
            {
                case "clone_from_old"://匯入上一梯次補測人員
                    CloneFromOldEchelon(cfg, itmR);
                    break;

                case "refresh"://更新報名資料
                    Refresh(cfg, itmR);
                    break;

                case "update"://更新晉段測驗成績
                    Update(cfg, itmR);
                    break;

                case "file_modal"://跳窗: 上傳晉段卡照片
                    FileModal(cfg, itmR);
                    break;

                case "new_modal"://跳窗: 新增補測人員
                    NewModal(cfg, itmR);
                    break;

                case "edit_modal"://跳窗: 修改補測人員
                    EditModal(cfg, itmR);
                    break;

                case "import_modal"://跳窗: 調入人員資料
                    ImportModal(cfg, itmR);
                    break;

                case "upload"://上傳晉段卡照片
                    FileUpload(cfg, itmR);
                    break;

                case "merge"://更新補測人員
                    MergeMakeup(cfg, itmR);
                    break;

                case "remove"://移除補測人員
                    RemoveMakeup(cfg, itmR);
                    break;

                case "all_pass"://全部合格
                    AllPass(cfg, itmR);
                    break;

                case "promotion"://登錄晉段紀錄
                    ResumePromotion(cfg, itmR);
                    break;

                case "excel"://下載資料
                    ExportExcel(cfg, itmR);
                    break;

                default:
                    Page(cfg, itmR);
                    break;
            }

            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

            return itmR;
        }

        private Item MeetingInfo(TConfig cfg, string meeting_id)
        {
            var cols = new List<string>
            {
                "id",
                "keyed_name",
                "in_title",
                "in_echelon",
                "in_annual",
                "in_seminar_type",
                "in_meeting_type",
            };

            string sql = "SELECT " + string.Join(", ", cols) + " FROM IN_CLA_MEETING WITH(NOLOCK)"
                + " WHERE id = '" + meeting_id + "'";

            return cfg.inn.applySQL(sql);
        }

        #region 匯入上一梯次補測人員
        private void CloneFromOldEchelon(TConfig cfg, Item itmReturn)
        {
            string echelon_meeting = itmReturn.getProperty("echelon_meeting", "");

            Item itmOldMeeting = MeetingInfo(cfg, echelon_meeting);
            if (itmOldMeeting.isError() || itmOldMeeting.getResult() == "")
            {
                throw new Exception("查無該梯次活動資料");
            }

            string old_meeting_id = itmOldMeeting.getProperty("id", "");

            Item itmMAreas = GetMeetingAreas(cfg, cfg.meeting_id);
            var area_map = MapItem(itmMAreas, "in_area");

            Item itmBelts = GetBelts(cfg);
            var belt_map = MapItem(itmBelts, "label");

            var filter_states = new List<string>
            {
                "不合格(需補測)",
                "未到",
                "請假",
            };

            string states = string.Join(", ", filter_states.Select(x => "N'" + x + "'").ToList());

            string sql = @"
                SELECT 
	                t1.*
                FROM 
	                IN_CLA_MEETING_PROMOTION t1 WITH(NOLOCK)
                LEFT OUTER JOIN
	                IN_CLA_MEETING_USER t2 WITH(NOLOCK)
	                ON t2.id = t1.in_user
                WHERE 
	                t1.source_id = '{#meeting_id}'
	                AND t1.in_score_state IN ({#states})
            ";

            sql = sql.Replace("{#meeting_id}", old_meeting_id)
                .Replace("{#states}", states);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                NewPromotionFromClone(cfg, item, area_map, belt_map);
            }
        }

        private void NewPromotionFromClone(TConfig cfg, Item itmSource, Dictionary<string, Item> area_map, Dictionary<string, Item> belt_map)
        {
            string sql = "";

            string meeting_id = cfg.meeting_id;
            string mt_annual = cfg.itmMeeting.getProperty("in_annual", "");
            string mt_echelon = cfg.itmMeeting.getProperty("in_echelon", "");

            string in_sno = itmSource.getProperty("in_sno", "");
            string in_serial = itmSource.getProperty("in_serial", "");
            int serial = GetIntVal(in_serial) + 1;

            // sql = "SELECT id FROM In_Cla_Meeting_Promotion WITH(NOLOCK)"
            //   + " WHERE source_id = '" + meeting_id + "'"
            //   + " AND in_sno = '" + in_sno + "'";

            // Item itmOld = cfg.inn.applySQL(sql);
            // if (!itmOld.isError() && itmOld.getResult() != "")
            // {
            //     //晉段測驗已有資料
            //     return;
            // }

            Item itmMPromotion = cfg.inn.newItem("In_Cla_Meeting_Promotion", "merge");
            itmMPromotion.setAttribute("where", "source_id = '" + meeting_id + "' AND in_sno = '" + in_sno + "'");

            itmMPromotion.setProperty("in_key", itmSource.getProperty("in_key", ""));
            itmMPromotion.setProperty("in_serial", serial.ToString());
            itmMPromotion.setProperty("in_makeup_yn", "1");
            itmMPromotion.setProperty("in_makeup_note", GetMakeupNote(cfg, itmSource));

            itmMPromotion.setProperty("source_id", meeting_id);
            itmMPromotion.setProperty("related_id", itmSource.getProperty("related_id", ""));
            itmMPromotion.setProperty("in_user", itmSource.getProperty("in_user", ""));

            itmMPromotion.setProperty("in_annual", mt_annual);
            itmMPromotion.setProperty("in_echelon", mt_echelon);

            itmMPromotion.setProperty("in_name", itmSource.getProperty("in_name", ""));
            itmMPromotion.setProperty("in_en_name", itmSource.getProperty("in_en_name", ""));
            itmMPromotion.setProperty("in_sno", itmSource.getProperty("in_sno", ""));
            itmMPromotion.setProperty("in_birth", DateTimeStr(itmSource.getProperty("in_birth", ""), hours: 8));
            itmMPromotion.setProperty("in_gender", itmSource.getProperty("in_gender", ""));
            itmMPromotion.setProperty("in_stuff_c1", itmSource.getProperty("in_stuff_c1", ""));
            itmMPromotion.setProperty("in_stuff_c2", itmSource.getProperty("in_stuff_c2", ""));
            itmMPromotion.setProperty("in_stuff_c1_sno", "");

            itmMPromotion.setProperty("in_gym", itmSource.getProperty("in_gym", ""));
            itmMPromotion.setProperty("in_gym_sno", itmSource.getProperty("in_gym_sno", ""));

            itmMPromotion.setProperty("in_committee", itmSource.getProperty("in_committee", ""));
            itmMPromotion.setProperty("in_committee_sno", itmSource.getProperty("in_committee_sno", ""));
            itmMPromotion.setProperty("in_committee_short", itmSource.getProperty("in_committee_short", ""));

            itmMPromotion.setProperty("in_area", itmSource.getProperty("in_area", ""));
            itmMPromotion.setProperty("in_current_org", itmSource.getProperty("in_current_org", ""));
            itmMPromotion.setProperty("in_degree_area", itmSource.getProperty("in_degree_area", ""));
            //itmMPromotion.setProperty("in_area_sort", itmSource.getProperty("in_area_sort", ""));
            //itmMPromotion.setProperty("in_location", itmSource.getProperty("in_location", ""));
            //itmMPromotion.setProperty("in_place", itmSource.getProperty("in_place", ""));
            //itmMPromotion.setProperty("in_date", itmSource.getProperty("in_date", ""));
            //itmMPromotion.setProperty("in_area_id", itmSource.getProperty("in_area_id", ""));

            itmMPromotion.setProperty("in_l1", itmSource.getProperty("in_l1", ""));
            itmMPromotion.setProperty("in_degree", itmSource.getProperty("in_degree", ""));


            Item itmResult = itmMPromotion.apply();

            if (itmResult.isError())
            {
                throw new Exception("建立 晉段測驗成績 發生錯誤");
            }
        }

        /// <summary>
        /// 取得補測項目
        /// </summary>
        private string GetMakeupNote(TConfig cfg, Item itmSource)
        {
            List<string> result = new List<string>();
            string in_degree = itmSource.getProperty("in_degree", "");
            string in_score_foot = itmSource.getProperty("in_score_foot", "");
            string in_score_poomsae = itmSource.getProperty("in_score_poomsae", "");
            string in_score_sparring = itmSource.getProperty("in_score_sparring", "");
            string in_score_beat = itmSource.getProperty("in_score_beat", "");
            string in_score_state = itmSource.getProperty("in_score_state", "");

            if (in_score_state.Contains("除名"))
            {
                return "除名";
            }
            else if (in_score_state.Contains("未到"))
            {
                return "未到";
            }
            else if (in_score_state.Contains("請假"))
            {
                return "請假";
            }

            switch (in_degree)
            {
                case "1000":
                case "2000":
                case "3000":
                    if (in_score_foot != "O") result.Add("足技");
                    if (in_score_poomsae != "O") result.Add("品勢");
                    if (in_score_sparring != "O") result.Add("對練");
                    if (in_score_beat != "O") result.Add("擊破");
                    break;

                case "4000":
                case "5000":
                case "6000":
                case "7000":
                case "8000":
                case "9000"://全部重考
                    result.Add("足技");
                    result.Add("品勢");
                    result.Add("對練");
                    result.Add("擊破");
                    break;

                default:
                    result.Add("異常");
                    break;
            }

            return string.Join("、", result);
        }
        #endregion 匯入上一梯次補測人員

        #region 下載資料

        private void ExportExcel(TConfig cfg, Item itmReturn)
        {
            Spire.Xls.Workbook workbook = new Spire.Xls.Workbook();

            switch (cfg.doc_name)
            {
                case "測驗成績總表":
                    cfg.ext_type = "xlsx";
                    AppendCmtScoreSheets(cfg, workbook);
                    break;

                case "補測名單":
                    cfg.ext_type = "pdf";
                    AppendMakeupSheets(cfg, workbook);
                    break;

                default:
                    cfg.ext_type = "xlsx";
                    AppendCmtScoreSheets(cfg, workbook);
                    break;
            }

            //移除空 sheet
            workbook.Worksheets[0].Remove();
            workbook.Worksheets[0].Remove();
            workbook.Worksheets[0].Remove();

            Item itmXls = cfg.inn.applyMethod("In_Variable_File", "<in_name></in_name>");
            string export_path = itmXls.getProperty("export_path", "");

            string guid = System.Guid.NewGuid().ToString().Substring(0, 6).ToUpper()
                + "_" + DateTime.Now.ToString("yyyyMMddHHmmssfff");

            string ext_name = "." + cfg.ext_type;
            string xls_name = cfg.itmMeeting.getProperty("in_title", "") + "_" + cfg.doc_name + "_" + guid;
            string xls_file = export_path + "\\" + xls_name + ext_name;

            if (cfg.ext_type == "pdf")
            {
                workbook.SaveToFile(xls_file, Spire.Xls.FileFormat.PDF);
            }
            else
            {
                workbook.SaveToFile(xls_file, Spire.Xls.ExcelVersion.Version2013);
            }

            itmReturn.setProperty("xls_name", xls_name + ext_name);
        }

        //補測名單
        private void AppendMakeupSheets(TConfig cfg, Spire.Xls.Workbook workbook)
        {
            int mt_echelon = GetIntVal(cfg.itmMeeting.getProperty("in_echelon", "0"));

            //梯次只顯示該次與前兩次共三個梯次
            List<TMeeting> list = LastYearsMeeting(cfg, mt_echelon, 2);

            if (list == null || list.Count == 0)
            {
                throw new Exception("查無晉段補測資料");
            }

            Item items = GetMakeupPromotions(cfg, list);
            List<TCommittee> committees = MapCommittees(cfg, items);
            foreach (var cmt in committees)
            {
                cmt.users = new List<TEchUser>();
                foreach (var item in cmt.items)
                {
                    var in_sno = item.getProperty("in_sno", "");
                    var user = cmt.users.Find(x => x.in_sno == in_sno);
                    if (user == null)
                    {
                        user = new TEchUser
                        {
                            in_sno = in_sno,
                            value = item,
                            makeups = new List<string>(),
                        };
                        cmt.users.Add(user);
                    }

                    var echelon_val = item.getProperty("in_echelon", "");
                    var echelon_pro = "ech_" + echelon_val;
                    var makeup_note = GetMakeupNote(cfg, item);

                    user.value.setProperty(echelon_pro, makeup_note);
                    if (makeup_note != "除名")
                    {
                        user.makeups.Add(makeup_note);
                    }
                }
            }

            foreach (var cmt in committees)
            {
                foreach (var user in cmt.users)
                {
                    if (user.makeups.Count >= 3)
                    {
                        user.value.setProperty("inn_memo", "除名");
                    }
                }
            }

            List<TField> fields = new List<TField>();
            fields.Add(new TField { ci = 1, title = "編號", property = "no", css = TCss.Center, width = 5 });
            fields.Add(new TField { ci = 2, title = "段位", property = "in_degree", css = TCss.Center, width = 5 });
            fields.Add(new TField { ci = 3, title = "中文姓名", property = "in_name", css = TCss.Center, width = 10 });
            //fields.Add(new TField { ci = 4, title = "啟蒙教練", property = "in_stuff_c1", css = TCss.Center, width = 10 });
            fields.Add(new TField { ci = 4, title = "所屬教練", property = "in_stuff_c2", css = TCss.Center, width = 10 });

            int ci = 5;
            for (int i = 0; i < list.Count; i++)
            {
                var mt = list[i];
                fields.Add(new TField { ci = ci, title = mt.in_echelon, property = "ech_" + mt.in_echelon, css = TCss.Center, width = 25 });
                ci++;
            }

            fields.Add(new TField { ci = ci, title = "備註", property = "inn_memo", css = TCss.None, width = 10 });

            MapCharSet(cfg, fields);

            for (int i = 0; i < committees.Count; i++)
            {
                TCommittee committee = committees[i];
                AppendMakeupSheet(cfg, workbook, fields, committee);
            }
        }

        //補測名單
        private void AppendMakeupSheet(TConfig cfg, Spire.Xls.Workbook workbook, List<TField> fields, TCommittee committee)
        {
            Spire.Xls.Worksheet sheet = workbook.CreateEmptySheet();
            sheet.Name = committee.city;

            //頁面配置-邊界
            sheet.PageSetup.TopMargin = 0.5;
            sheet.PageSetup.BottomMargin = 0.5;
            sheet.PageSetup.LeftMargin = 0.2;
            sheet.PageSetup.RightMargin = 0.2;

            //將所有欄放入單一頁面
            sheet.PageSetup.FitToPagesWide = 1;
            sheet.PageSetup.FitToPagesTall = 0;

            //列印標題
            sheet.PageSetup.PrintTitleRows = "$1:$2";

            var last_field = fields.Last();

            string sheet_title = cfg.mt_title.Replace("晉段申請", "補測名單")
                + "    " + committee.city;

            //活動標題
            var mt_mr = sheet.Range["A1:" + last_field.cs + "1"];
            mt_mr.Merge();
            mt_mr.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
            mt_mr.Style.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;
            mt_mr.Style.Font.Size = 16;
            //mt_mr.Style.Font.IsBold = true;
            mt_mr.RowHeight = 40;
            mt_mr.Text = sheet_title;


            int mnRow = 2;
            int wsRow = 2;

            //標題列
            SetHeadRow(cfg, sheet, wsRow, fields);
            wsRow++;

            ////凍結窗格
            //sheet.FreezePanes(wsRow, fields.Last().ci + 1);

            int count = committee.users.Count;

            for (int i = 0; i < count; i++)
            {
                var item = committee.users[i].value;
                string in_degree = item.getProperty("in_degree", "");

                item.setProperty("no", (i + 1).ToString());
                item.setProperty("in_degree", in_degree.Replace("000", ""));

                SetItemCell(cfg, sheet, wsRow, item, fields);

                wsRow++;
            }

            //設定格線
            SetRangeBorder(sheet, fields, mnRow, wsRow);

            //設定欄寬
            SetColumnWidth(sheet, fields);
        }


        //測驗成績總表
        private void AppendCmtScoreSheets(TConfig cfg, Spire.Xls.Workbook workbook)
        {
            Item items = GetMeetingPromotions(cfg, filter_makeup: false, order_by_degree: true);
            if (items.isError() || items.getResult() == "")
            {
                return;
            }

            List<TCommittee> list = MapCommittees(cfg, items);

            List<TField> fields = new List<TField>();
            fields.Add(new TField { title = "編號", property = "no", css = TCss.Center, width = 5 });
            fields.Add(new TField { title = "區別", property = "in_degree_area", css = TCss.None, width = 8 });
            fields.Add(new TField { title = "中文姓名", property = "in_name", css = TCss.None, width = 10 });
            fields.Add(new TField { title = "段位", property = "in_l1", css = TCss.None, width = 8 });
            //fields.Add(new TField { title = "啟蒙教練", property = "in_stuff_c1", css = TCss.None, width = 10 });
            fields.Add(new TField { title = "所屬教練", property = "in_stuff_c2", css = TCss.None, width = 10 });
            fields.Add(new TField { title = "所屬道館", property = "in_gym", css = TCss.None, width = 15 });
            fields.Add(new TField { title = "所屬委員會", property = "in_committee_short", css = TCss.None, width = 15 });
            fields.Add(new TField { title = "足技", property = "in_score_foot", css = TCss.Center, width = 5 });
            fields.Add(new TField { title = "型", property = "in_score_poomsae", css = TCss.Center, width = 5 });
            fields.Add(new TField { title = "對練", property = "in_score_sparring", css = TCss.Center, width = 5 });
            fields.Add(new TField { title = "擊破", property = "in_score_beat", css = TCss.Center, width = 5 });
            fields.Add(new TField { title = "補測", property = "makeup_yn", css = TCss.Center, width = 5 });
            fields.Add(new TField { title = "補測項目", property = "in_makeup_note", css = TCss.None, width = 20 });

            MapCharSetAndIndex(cfg, fields);

            foreach (var cmt in list)
            {
                if (cmt.items.Count > 0)
                {
                    AppendCmtScoreSheet(cfg, cmt, fields, workbook);
                }
            }
        }

        //測驗成績總表
        private void AppendCmtScoreSheet(TConfig cfg, TCommittee cmt, List<TField> fields, Spire.Xls.Workbook workbook)
        {
            Spire.Xls.Worksheet sheet = workbook.CreateEmptySheet();
            sheet.Name = cmt.in_short_name;

            //頁面配置-邊界
            // sheet.PageSetup.TopMargin = 0.5;
            // sheet.PageSetup.BottomMargin = 0.5;
            // sheet.PageSetup.LeftMargin = 0.5;
            // sheet.PageSetup.RightMargin = 0.5;

            //列印標題
            sheet.PageSetup.PrintTitleRows = "$1:$2";

            //最先欄
            var fs = fields.First();
            //最後欄
            var fe = fields.Last();

            //活動標題
            var mt_mr = sheet.Range[fs.cs + "1:" + fe.cs + "1"];
            mt_mr.Merge();
            mt_mr.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
            mt_mr.Style.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;
            mt_mr.Text = cfg.mt_title;
            mt_mr.Style.Font.Size = 16;
            //mt_mr.Style.Font.IsBold = true;
            mt_mr.RowHeight = 40;

            int mnRow = 2;
            int wsRow = 2;

            //凍結窗格
            sheet.FreezePanes(wsRow, fe.ci + 1);

            string last_l1 = "";
            for (int i = 0; i < cmt.items.Count; i++)
            {
                var item = cmt.items[i];
                string in_l1 = item.getProperty("in_l1", "");

                if (in_l1 != last_l1)
                {
                    //標題列
                    SetHeadRow(cfg, sheet, wsRow, fields, need_color: true);
                    wsRow++;

                    last_l1 = in_l1;
                }

                string in_makeup_yn = item.getProperty("in_makeup_yn", "");
                string makeup_yn = in_makeup_yn == "1" ? "是" : "";

                item.setProperty("no", (i + 1).ToString());
                item.setProperty("makeup_yn", makeup_yn);

                SetItemCell(cfg, sheet, wsRow, item, fields);

                wsRow++;
            }

            //設定格線
            SetRangeBorder(sheet, fields, mnRow, wsRow);

            //設定欄寬
            SetColumnWidth(sheet, fields);
        }

        //測驗成績總表
        private void AppendScoreSheet(TConfig cfg, Spire.Xls.Workbook workbook)
        {
            Spire.Xls.Worksheet sheet = workbook.CreateEmptySheet();
            sheet.Name = cfg.doc_name;

            //頁面配置-邊界
            // sheet.PageSetup.TopMargin = 0.5;
            // sheet.PageSetup.BottomMargin = 0.5;
            // sheet.PageSetup.LeftMargin = 0.5;
            // sheet.PageSetup.RightMargin = 0.5;

            //列印標題
            sheet.PageSetup.PrintTitleRows = "$1:$2";


            List<TField> fields = new List<TField>();
            fields.Add(new TField { title = "編號", property = "no", css = TCss.Center, width = 5 });
            fields.Add(new TField { title = "區別", property = "in_degree_area", css = TCss.None, width = 8 });
            fields.Add(new TField { title = "中文姓名", property = "in_name", css = TCss.None, width = 10 });
            fields.Add(new TField { title = "段位", property = "in_l1", css = TCss.None, width = 8 });
            //fields.Add(new TField { title = "啟蒙教練", property = "in_stuff_c1", css = TCss.None, width = 10 });
            fields.Add(new TField { title = "所屬教練", property = "in_stuff_c2", css = TCss.None, width = 10 });
            fields.Add(new TField { title = "所屬道館", property = "in_gym", css = TCss.None, width = 15 });
            fields.Add(new TField { title = "所屬委員會", property = "in_committee_short", css = TCss.None, width = 15 });
            fields.Add(new TField { title = "足技", property = "in_score_foot", css = TCss.Center, width = 5 });
            fields.Add(new TField { title = "型", property = "in_score_poomsae", css = TCss.Center, width = 5 });
            fields.Add(new TField { title = "對練", property = "in_score_sparring", css = TCss.Center, width = 5 });
            fields.Add(new TField { title = "擊破", property = "in_score_beat", css = TCss.Center, width = 5 });
            fields.Add(new TField { title = "補測", property = "makeup_yn", css = TCss.Center, width = 5 });
            fields.Add(new TField { title = "補測項目", property = "in_makeup_note", css = TCss.None, width = 20 });

            MapCharSetAndIndex(cfg, fields);

            //最後欄
            var last_field = fields.Last();

            //活動標題
            var mt_mr = sheet.Range["A1:" + last_field.cs + "1"];
            mt_mr.Merge();
            mt_mr.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
            mt_mr.Style.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;
            mt_mr.Text = cfg.mt_title;
            mt_mr.Style.Font.Size = 16;
            //mt_mr.Style.Font.IsBold = true;
            mt_mr.RowHeight = 40;


            int mnRow = 2;
            int wsRow = 2;

            //標題列
            SetHeadRow(cfg, sheet, wsRow, fields);
            wsRow++;

            //凍結窗格
            sheet.FreezePanes(wsRow, last_field.ci + 1);

            Item items = GetMeetingPromotions(cfg);
            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                string in_makeup_yn = item.getProperty("in_makeup_yn", "");
                string makeup_yn = in_makeup_yn == "1" ? "是" : "";

                item.setProperty("no", (i + 1).ToString());
                item.setProperty("makeup_yn", makeup_yn);

                SetItemCell(cfg, sheet, wsRow, item, fields);

                wsRow++;
            }

            //設定格線
            SetRangeBorder(sheet, fields, mnRow, wsRow);

            //設定欄寬
            SetColumnWidth(sheet, fields);
        }

        private class TField
        {
            public string title { get; set; }
            public string property { get; set; }
            public TCss css { get; set; }
            public int ci { get; set; }
            public string cs { get; set; }
            public double width { get; set; }
            public Func<TConfig, TField, Item, string> getValue { get; set; }

            public bool is_merge { get; set; }
        }

        private enum TCss
        {
            None = 0,
            Center = 10,
            Text = 100,
            Number = 200,
            Money = 300,
            Day = 400,
        }

        private string[] GetCharSet()
        {
            return new string[] { "", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
        }

        //設定標題列
        private void SetHeadRow(TConfig cfg, Spire.Xls.Worksheet sheet, int wsRow, List<TField> fields, bool need_color = false, bool is_bold = false)
        {
            foreach (var field in fields)
            {
                string cr = field.cs + wsRow;
                SetHeadCell(cfg, sheet, cr, field.title, need_color, is_bold);
            }

            var row_mr = sheet.Range["A" + wsRow + ":" + fields.Last().cs + wsRow];
            row_mr.RowHeight = cfg.row_height;
            row_mr.Style.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;
        }

        //設定標題列
        private void SetHeadCell(TConfig cfg, Spire.Xls.Worksheet sheet, string cr, string value, bool need_color = false, bool is_bold = false)
        {
            var range = sheet.Range[cr];
            range.Text = value;
            range.Style.Font.IsBold = is_bold;
            range.Style.Font.Size = 12;

            if (need_color)
            {
                range.Style.Font.Color = System.Drawing.Color.White;
                range.Style.Color = System.Drawing.Color.FromArgb(41, 92, 144);//#295C90
            }

            range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
        }

        //設定資料列
        private void SetItemCell(TConfig cfg, Spire.Xls.Worksheet sheet, int wsRow, Item item, List<TField> fields)
        {
            foreach (var field in fields)
            {
                var cr = field.cs + wsRow;
                var va = "";

                if (field.getValue != null)
                {
                    va = field.getValue(cfg, field, item);
                }
                else if (field.property != "")
                {
                    va = item.getProperty(field.property, "");
                }

                SetCell(cfg, sheet, cr, va, field.css);
            }

            var row_mr = sheet.Range["A" + wsRow + ":" + fields.Last().cs + wsRow];
            row_mr.RowHeight = cfg.row_height;
            row_mr.Style.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;
        }

        //設定資料格
        private void SetCell(TConfig cfg, Spire.Xls.Worksheet sheet, string cr, string value, TCss css)
        {
            var range = sheet.Range[cr];
            range.Style.Font.Size = cfg.font_size;

            switch (css)
            {
                case TCss.None:
                    range.Text = value;
                    break;
                case TCss.Center:
                    range.Text = value;
                    range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                    break;
                case TCss.Text:
                    range.Text = "'" + value;
                    range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                    break;
                case TCss.Number:
                    range.NumberValue = GetIntVal(value);
                    range.NumberFormat = "0";
                    range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Right;
                    break;
                case TCss.Money:
                    range.NumberValue = GetIntVal(value);
                    range.NumberFormat = "$ #,##0";
                    break;
                case TCss.Day:
                    range.Text = DateTimeStr(value, "yyyy/MM/dd");
                    range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                    break;
            }
        }

        //設定格線
        private void SetRangeBorder(Spire.Xls.Worksheet sheet, string pos)
        {
            var range = sheet.Range[pos];
            range.BorderInside(Spire.Xls.LineStyleType.Thin, System.Drawing.Color.Black);
            range.BorderAround(Spire.Xls.LineStyleType.Thin, System.Drawing.Color.Black);
        }

        //設定格線
        private void SetRangeBorder(Spire.Xls.Worksheet sheet, List<TField> fields, int mnRow, int wsRow)
        {
            var tbl_ps = fields.First().cs + mnRow;
            var tbl_pe = fields.Last().cs + (wsRow - 1);
            SetRangeBorder(sheet, tbl_ps + ":" + tbl_pe);
        }

        private void MapCharSet(TConfig cfg, List<TField> fields)
        {
            foreach (var field in fields)
            {
                field.cs = cfg.CharSet[field.ci];
            }
        }

        private void MapCharSetAndIndex(TConfig cfg, List<TField> fields)
        {
            for (int i = 0; i < fields.Count; i++)
            {
                var field = fields[i];
                field.ci = i + 1;
                field.cs = cfg.CharSet[field.ci];
            }
        }

        private void AutoFit(Spire.Xls.Worksheet sheet, List<TField> fields)
        {
            foreach (var field in fields)
            {
                sheet.Columns[field.ci - 1].AutoFitColumns();
            }
        }

        private void SetColumnWidth(Spire.Xls.Worksheet sheet, List<TField> fields)
        {
            foreach (var field in fields)
            {
                sheet.Columns[field.ci - 1].ColumnWidth = field.width;
            }
        }
        #endregion 下載資料

        //全部合格
        private void AllPass(TConfig cfg, Item itmReturn)
        {
            string sql = "UPDATE IN_CLA_MEETING_PROMOTION SET"
                + " in_score_state = N'合格'"
                + ", in_score_foot = 'O'"
                + ", in_score_poomsae = 'O'"
                + ", in_score_sparring = 'O'"
                + ", in_score_beat = 'O'"
                + " WHERE source_id = '" + cfg.meeting_id + "'"
                + "";

            if (cfg.cmt_sno != "")
            {
                sql += " AND in_committee_sno = '" + cfg.cmt_sno + "'";
            }

            Item itmResult = cfg.inn.applySQL(sql);

            if (itmResult.isError())
            {
                throw new Exception("執行全部合格發生錯誤");
            }

            //合格後自動登錄晉段紀錄
            ResumePromotion(cfg, itmReturn);

        }

        //登錄晉段紀錄
        private void ResumePromotion(TConfig cfg, Item itmReturn)
        {
            string mt_annual = cfg.itmMeeting.getProperty("in_annual", "");
            string mt_seminar_type = cfg.itmMeeting.getProperty("in_seminar_type", "");
            string mt_date = itmReturn.getProperty("in_date", "");
            string cmt_sno = itmReturn.getProperty("cmt_sno", "");
            string promotion_id = itmReturn.getProperty("promotion_id", "");

            if (mt_annual == "") throw new Exception("未設定晉段民國年度");
            if (mt_seminar_type == "") throw new Exception("未設定晉段國內國際類型");
            if (mt_date == "") throw new Exception("請輸入晉升日期");

            string promotion_day = GetPromotionDay(cfg, mt_annual, mt_date);

            Item items = GetItmPromotions(cfg, cmt_sno, promotion_id);

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                RefreshPromotion(cfg, item, promotion_day);
            }
        }

        private Item GetItmPromotions(TConfig cfg, string cmt_sno, string promotion_id)
        {
            string condition = cmt_sno == ""
                ? "AND t1.id = '" + promotion_id + "'"
                : "AND t1.in_committee_sno = '" + cmt_sno + "'";

            string sql = @"
                SELECT 
	                t1.*
	                , t2.id        AS 'resume_id'
                    , t2.in_degree AS 'resume_degree'
                FROM 
	                IN_CLA_MEETING_PROMOTION t1 WITH(NOLOCK)
                INNER JOIN
	                IN_RESUME t2 WITH(NOLOCK)
	                ON t2.in_sno = t1.in_sno
                WHERE 
	                t1.source_id = '{#meeting_id}'
	                {#condition}
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#condition}", condition);

            return cfg.inn.applySQL(sql);
        }

        private void RefreshPromotion(TConfig cfg, Item item, string promotion_day)
        {
            string resume_id = item.getProperty("resume_id", "");
            string resume_degree = item.getProperty("resume_degree", "");
            string in_l1 = item.getProperty("in_l1", "");

            string in_score_foot = item.getProperty("in_score_foot", "");
            string in_score_poomsae = item.getProperty("in_score_poomsae", "");
            string in_score_sparring = item.getProperty("in_score_sparring", "");
            string in_score_beat = item.getProperty("in_score_beat", "");
            string in_score_state = item.getProperty("in_score_state", "");
            string in_makeup_yn = item.getProperty("in_makeup_yn", "");
            
            bool is_qualified = false;
            string in_note = "";

            switch (in_score_state)
            {
                case "":
                    is_qualified = in_score_foot == "O"
                        && in_score_poomsae == "O"
                        && in_score_sparring == "O"
                        && in_score_beat == "O";

                    if (is_qualified && in_makeup_yn == "1")
                    {
                        in_note = "補測";
                    }
                    break;

                case "合格":
                    is_qualified = true;
                    if (is_qualified && in_makeup_yn == "1")
                    {
                        in_note = "補測";
                    }
                    break;

                case "不合格(除名)":
                case "未到(除名)":
                case "請假(除名)":
                    in_note = "除名";
                    is_qualified = true;
                    break;

                default:
                    is_qualified = false;
                    break;
            }

            string new_degree = GetDegreeValue(cfg, in_l1, is_qualified);

            if (is_qualified)
            {
                //更新晉段紀錄
                MergeResumePromotion(cfg, item, promotion_day, in_note);
            }
            else
            {
                //移除晉段紀錄
                RemoveResumePromotion(cfg, item);
            }

            //更新段位
            UpdResumeDegree(cfg, resume_id, resume_degree, new_degree);
        }

        //更新段位
        private void UpdResumeDegree(TConfig cfg, string resume_id, string resume_degree, string new_degree)
        {
            if (resume_degree == new_degree) return;

            string sql = "UPDATE IN_RESUME SET in_degree = '" + new_degree + "' WHERE id = '" + resume_id + "'";

            Item itmUpd = cfg.inn.applySQL(sql);

            if (itmUpd.isError())
            {
                throw new Exception("更新段位發生錯誤");
            }
        }

        private string GetDegreeValue(TConfig cfg, string in_l1, bool is_qualified)
        {
            var itmNewDgree = cfg.inn.applyMethod("In_Degree_Value", "<in_degree_label>" + in_l1 + "</in_degree_label>");
            var new_dgr_code = itmNewDgree.getProperty("in_degree", "");

            if (is_qualified)
            {
                return new_dgr_code;
            }

            switch (new_dgr_code)
            {
                case "1000": return "";
                case "2000": return "1000";
                case "3000": return "2000";
                case "4000": return "3000";
                case "5000": return "4000";
                case "6000": return "5000";
                case "7000": return "6000";
                case "8000": return "7000";
                case "9000": return "8000";
                default: return "";
            }
        }

        //移除晉段紀錄
        private void RemoveResumePromotion(TConfig cfg, Item item)
        {
            string in_cla_promotion = item.getProperty("id", "");

            Item itmRPro = cfg.inn.newItem("In_Resume_Promotion", "delete");
            itmRPro.setAttribute("where", "in_cla_promotion = '" + in_cla_promotion + "'");
            itmRPro = itmRPro.apply();

            if (itmRPro.isError())
            {
                //throw new Exception("移除晉段紀錄發生錯誤");
            }
        }

        //更新晉段紀錄
        private void MergeResumePromotion(TConfig cfg, Item item, string promotion_day, string in_note)
        {
            string in_cla_promotion = item.getProperty("id", "");
            string in_makeup_yn = item.getProperty("in_makeup_yn", "");
            string resume_id = item.getProperty("related_id", "");

            Item itmRPro = cfg.inn.newItem("In_Resume_Promotion", "merge");
            itmRPro.setAttribute("where", "in_cla_promotion = '" + in_cla_promotion + "'");

            itmRPro.setProperty("source_id", resume_id);
            itmRPro.setProperty("in_cla_meeting", cfg.meeting_id);
            itmRPro.setProperty("in_cla_promotion", in_cla_promotion);
            itmRPro.setProperty("in_echelon", item.getProperty("in_echelon", ""));
            itmRPro.setProperty("in_manager_area", item.getProperty("in_degree_area", ""));
            itmRPro.setProperty("in_date", promotion_day);
            itmRPro.setProperty("in_apply_degree", item.getProperty("in_degree", ""));
            itmRPro.setProperty("in_score_photo", item.getProperty("in_score_photo", ""));
            itmRPro.setProperty("in_note", in_note);

            string in_is_global = "0";
            string in_user = item.getProperty("in_user", "");
            if (in_user != "")
            {
                Item itmMUser = cfg.inn.applySQL("SELECT id, in_is_gl_degree FROM IN_CLA_MEETING_USER WITH(NOLOCK) WHERE id = '" + in_user + "'");
                if (!itmMUser.isError() && itmMUser.getResult() != "")
                {
                    string in_is_gl_degree = itmMUser.getProperty("in_is_gl_degree", "");////@是@否
                    if (in_is_gl_degree == "是")
                    {
                        in_is_global = "1";
                    }
                }
            }

            itmRPro.setProperty("in_is_global", in_is_global);


            itmRPro.setProperty("in_sno", item.getProperty("in_sno", ""));

            itmRPro = itmRPro.apply();

            if (itmRPro.isError())
            {
                throw new Exception("更新晉段紀錄發生錯誤");
            }
        }

        //刪除補測人員
        private void RemoveMakeup(TConfig cfg, Item itmReturn)
        {
            string id = itmReturn.getProperty("id", "");

            string sql = "DELETE FROM IN_CLA_MEETING_PROMOTION"
                + " WHERE id = '" + id + "'"
                + " AND in_makeup_yn = 1";

            Item itmResult = cfg.inn.applySQL(sql);

            if (itmReturn.isError())
            {
                throw new Exception("刪除失敗");
            }
        }

        //跳窗: 新增補測人員
        private void NewModal(TConfig cfg, Item itmReturn)
        {
            itmReturn.setProperty("crud", "create");
        }

        //跳窗: 修改補測人員
        private void EditModal(TConfig cfg, Item itmReturn)
        {
            string id = itmReturn.getProperty("id", "");

            Item itmPromotion = cfg.inn.getItemById("In_Cla_Meeting_Promotion", id);
            if (itmPromotion.isError() || itmPromotion.getResult() == "")
            {
                throw new Exception("查無測驗紀錄");
            }

            string resume_id = itmPromotion.getProperty("related_id", "");
            Item itmResume = cfg.inn.getItemById("In_Resume", resume_id);

            SetUserModal(cfg, itmResume, itmReturn);

            itmReturn.setProperty("in_degree_area", itmPromotion.getProperty("in_degree_area", ""));
            itmReturn.setProperty("in_l1", itmPromotion.getProperty("in_l1", ""));
            itmReturn.setProperty("in_num", itmPromotion.getProperty("in_num", ""));
            itmReturn.setProperty("in_serial", itmPromotion.getProperty("in_serial", ""));
            itmReturn.setProperty("in_makeup_note", itmPromotion.getProperty("in_makeup_note", ""));

            itmReturn.setProperty("in_committee", itmPromotion.getProperty("in_committee", ""));
            itmReturn.setProperty("in_committee_sno", itmPromotion.getProperty("in_committee_sno", ""));
            itmReturn.setProperty("in_committee_short", itmPromotion.getProperty("in_committee_short", ""));

            itmReturn.setProperty("crud", "update");
        }

        //編輯補測人員
        private void MergeMakeup(TConfig cfg, Item itmReturn)
        {
            string mt_title = cfg.itmMeeting.getProperty("in_title", "");
            string mt_annual = cfg.itmMeeting.getProperty("in_annual", "");
            string mt_echelon = cfg.itmMeeting.getProperty("in_echelon", "");

            string id = itmReturn.getProperty("id", "");
            string crud = itmReturn.getProperty("crud", "");
            string resume_id = itmReturn.getProperty("resume_id", "");
            string in_name = itmReturn.getProperty("in_name", "");
            //string in_en_name = itmReturn.getProperty("in_en_name", "");
            string in_sno = itmReturn.getProperty("in_sno", "");
            //string in_area = itmReturn.getProperty("in_area", "");

            string in_gym = itmReturn.getProperty("in_gym", "");
            string in_gym_sno = itmReturn.getProperty("in_gym_sno", "");

            string in_committee = itmReturn.getProperty("in_committee", "");
            string in_committee_short = itmReturn.getProperty("in_committee_short", "");
            string in_committee_sno = itmReturn.getProperty("in_committee_sno", "");

            string in_num = itmReturn.getProperty("in_num", "");
            string in_serial = itmReturn.getProperty("in_serial", "");
            string in_degree_area = itmReturn.getProperty("in_degree_area", "");
            string in_l1 = itmReturn.getProperty("in_l1", "");
            string in_makeup_note = itmReturn.getProperty("in_makeup_note", "");

            if (resume_id == "")
            {
                throw new Exception("人員資料錯誤");
            }

            Item itmResume = cfg.inn.getItemById("In_Resume", resume_id);
            if (itmResume.isError())
            {
                throw new Exception("查無人員資料");
            }

            //過去無報名資料，因此以晉段卡號為鍵值
            string in_key = in_num;
            string sql = "";

            Item itmOld = null;

            if (crud == "create")
            {
                //新增 - 資料防呆
                sql = "SELECT id FROM In_Cla_Meeting_Promotion WITH(NOLOCK)"
                + " WHERE in_key = '" + in_key + "'"
                + " AND in_serial = '" + in_serial + "'"
                + "";

                itmOld = cfg.inn.applySQL(sql);
                if (!itmOld.isError() && itmOld.getResult() != "")
                {
                    throw new Exception("該晉段卡已有第 " + in_serial + " 次測驗紀錄");
                }

                sql = "SELECT id FROM In_Cla_Meeting_Promotion WITH(NOLOCK)"
                   + " WHERE source_id = '" + cfg.meeting_id + "'"
                   + " AND in_sno = '" + in_sno + "'"
                   + "";

                itmOld = cfg.inn.applySQL(sql);
                if (!itmOld.isError() && itmOld.getResult() != "")
                {
                    throw new Exception("【" + mt_title + "】已有 " + in_name + " 報名資料");
                }
            }
            else if (crud == "update")
            {
                //修改 - 資料防呆
                itmOld = cfg.inn.getItemById("In_Cla_Meeting_Promotion", id);
                if (itmOld.isError() || itmOld.getResult() == "")
                {
                    throw new Exception("查無測驗紀錄");
                }
            }
            else
            {
                throw new Exception("參數錯誤");
            }

            Item itmMAreas = GetMeetingAreas(cfg, cfg.meeting_id);
            var area_map = MapItem(itmMAreas, "in_area");

            Item itmBelts = GetBelts(cfg);
            var belt_map = MapItem(itmBelts, "label");


            Item itmMPromotion = cfg.inn.newItem("In_Cla_Meeting_Promotion", "merge");
            if (crud == "create")
            {
                itmMPromotion.setAttribute("where", "in_key = '" + in_key + "' AND in_serial = " + in_serial);
            }
            else if (crud == "update")
            {
                itmMPromotion.setAttribute("where", "id = '" + id + "'");
            }

            itmMPromotion.setProperty("in_key", in_key);
            itmMPromotion.setProperty("in_serial", in_serial);
            itmMPromotion.setProperty("in_num", in_num);

            itmMPromotion.setProperty("source_id", cfg.meeting_id);
            itmMPromotion.setProperty("related_id", resume_id);
            itmMPromotion.setProperty("in_user", "");

            itmMPromotion.setProperty("in_annual", mt_annual);
            itmMPromotion.setProperty("in_echelon", mt_echelon);

            itmMPromotion.setProperty("in_name", itmResume.getProperty("in_name", ""));
            itmMPromotion.setProperty("in_en_name", itmResume.getProperty("in_en_name", ""));
            itmMPromotion.setProperty("in_sno", itmResume.getProperty("in_sno", ""));
            itmMPromotion.setProperty("in_birth", itmResume.getProperty("in_birth", ""));
            itmMPromotion.setProperty("in_gender", itmResume.getProperty("in_gender", ""));
            itmMPromotion.setProperty("in_area", itmResume.getProperty("in_area", ""));

            itmMPromotion.setProperty("in_stuff_c1", itmResume.getProperty("in_stuff_c1", ""));
            //itmMPromotion.setProperty("in_stuff_c1_sno", "");
            itmMPromotion.setProperty("in_stuff_c2", itmResume.getProperty("in_stuff_c2", ""));

            itmMPromotion.setProperty("in_gym", in_gym);
            itmMPromotion.setProperty("in_gym_sno", in_gym_sno);

            itmMPromotion.setProperty("in_committee", in_committee);
            itmMPromotion.setProperty("in_committee_sno", in_committee_sno);
            itmMPromotion.setProperty("in_committee_short", in_committee_short);

            if (area_map.ContainsKey(in_degree_area))
            {
                Item itmMArea = area_map[in_degree_area];
                itmMPromotion.setProperty("in_degree_area", in_degree_area);
                itmMPromotion.setProperty("in_area_sort", itmMArea.getProperty("in_sort_order", ""));
                itmMPromotion.setProperty("in_location", itmMArea.getProperty("in_location", ""));
                itmMPromotion.setProperty("in_place", itmMArea.getProperty("in_place", ""));
                itmMPromotion.setProperty("in_date", DateTimeStr(itmMArea.getProperty("in_date", "")));
                itmMPromotion.setProperty("in_area_id", itmMArea.getProperty("id", ""));
            }
            else
            {
                itmMPromotion.setProperty("in_degree_area", in_degree_area);
                itmMPromotion.setProperty("in_area_sort", "100");
                itmMPromotion.setProperty("in_location", "");
                itmMPromotion.setProperty("in_place", "");
                itmMPromotion.setProperty("in_date", "");
                itmMPromotion.setProperty("in_area_id", "");
            }

            if (belt_map.ContainsKey(in_l1))
            {
                Item itmBelt = belt_map[in_l1];
                itmMPromotion.setProperty("in_l1", in_l1);
                itmMPromotion.setProperty("in_degree", itmBelt.getProperty("value", ""));
            }
            else
            {
                itmMPromotion.setProperty("in_l1", in_l1);
                itmMPromotion.setProperty("in_degree", "0");
            }

            itmMPromotion.setProperty("in_makeup_yn", "1");
            itmMPromotion.setProperty("in_makeup_note", in_makeup_note);

            Item itmResult = itmMPromotion.apply();

            if (itmResult.isError())
            {
                throw new Exception("補測人員新增失敗");
            }
        }

        //載入
        private void ImportModal(TConfig cfg, Item itmReturn)
        {
            string in_sno = itmReturn.getProperty("in_sno", "");
            if (in_sno == "") return;

            Item itmResume = cfg.inn.newItem("In_Resume", "get");
            itmResume.setProperty("in_sno", in_sno);
            itmResume = itmResume.apply();

            SetUserModal(cfg, itmResume, itmReturn);

            itmReturn.setProperty("crud", "create");
        }

        //載入
        private void SetUserModal(TConfig cfg, Item itmResume, Item itmReturn)
        {
            if (itmResume.isError() || itmResume.getResult() == "")
            {
                throw new Exception("查無人員資料");
            }

            itmReturn.setProperty("resume_id", itmResume.getProperty("id", ""));
            itmReturn.setProperty("in_name", itmResume.getProperty("in_name", ""));
            itmReturn.setProperty("in_en_name", itmResume.getProperty("in_en_name", ""));
            itmReturn.setProperty("in_stuff_c1", itmResume.getProperty("in_stuff_c1", ""));
            itmReturn.setProperty("in_stuff_c2", itmResume.getProperty("in_stuff_c2", ""));
            itmReturn.setProperty("inn_sno", itmResume.getProperty("in_sno", ""));

            string in_current_org = itmResume.getProperty("in_current_org", "");
            string in_manager_org = itmResume.getProperty("in_manager_org", "");
            string in_manager_name = itmResume.getProperty("in_manager_name", "");

            Item itmGym = cfg.inn.newItem();
            Item itmCommittee = cfg.inn.newItem();
            Item itmManager = cfg.inn.newItem();

            if (in_manager_org != "")
            {
                itmManager = cfg.inn.newItem("In_Resume", "get");
                itmManager.setProperty("id", in_manager_org);
                itmManager = itmManager.apply();
            }
            else
            {
                string sql = "SELECT TOP 1 * FROM IN_RESUME WITH(NOLOCK)"
                    + " WHERE in_member_type = 'vip_gym'"
                    + " AND in_name = N'" + in_current_org + "'"
                    + " AND in_member_status = '合格會員'";

                itmManager = cfg.inn.applySQL(sql);
            }


            string[] arr = new string[]
            {
                "u_gym",
                "vip_gym",
                "vip_group",
            };

            if (!itmManager.isError() && itmManager.getResult() != "")
            {
                string mgr_member_type = itmManager.getProperty("in_member_type", "");
                if (mgr_member_type == "area_cmt")
                {
                    itmCommittee = itmManager;
                    BindFromCmt(cfg, itmCommittee, in_current_org, itmReturn);
                }
                else if (arr.Contains(mgr_member_type))
                {
                    itmGym = itmManager;
                    BindFromGym(cfg, itmGym, itmReturn);
                }
            }
            else
            {
                itmGym = cfg.inn.newItem("In_Resume", "get");
                itmGym.setProperty("in_current_org", in_current_org);
                itmGym.setProperty("in_member_type", "vip_gym");
                itmGym = itmGym.apply();

                if (!itmGym.isError() && itmGym.getResult() != "")
                {
                    BindFromGym(cfg, itmGym, itmReturn);
                }
                else
                {
                    itmCommittee = cfg.inn.newItem("In_Resume", "get");
                    itmCommittee.setProperty("in_name", in_manager_name);
                    itmCommittee.setProperty("in_member_type", "area_cmt");
                    itmCommittee = itmCommittee.apply();
                    if (!itmCommittee.isError() && itmCommittee.getResult() != "")
                    {
                        BindFromCmt(cfg, itmCommittee, in_current_org, itmReturn);
                    }
                }
            }

            if (itmReturn.getProperty("in_committee", "") == "")
            {
                itmReturn.setProperty("in_committee", in_manager_name);
            }

            if (itmReturn.getProperty("in_gym", "") == "")
            {
                itmReturn.setProperty("in_gym", in_current_org);
            }
        }

        private void BindFromCmt(TConfig cfg, Item itmCommittee, string in_current_org, Item itmReturn)
        {
            itmReturn.setProperty("in_gym", in_current_org);
            itmReturn.setProperty("in_gym_sno", "");

            itmReturn.setProperty("in_committee", itmCommittee.getProperty("in_name", ""));
            itmReturn.setProperty("in_committee_short", itmCommittee.getProperty("in_short_org", ""));
            itmReturn.setProperty("in_committee_sno", itmCommittee.getProperty("in_sno", ""));
        }

        private void BindFromGym(TConfig cfg, Item itmGym, Item itmReturn)
        {
            itmReturn.setProperty("in_gym", itmGym.getProperty("in_name", ""));
            itmReturn.setProperty("in_gym_sno", itmGym.getProperty("in_sno", ""));

            //所屬委員會 resume_id
            string gym_manager_org = itmGym.getProperty("in_manager_org", "");

            Item itmCommittee = cfg.inn.newItem("In_Resume", "get");
            itmCommittee.setProperty("id", gym_manager_org);
            itmCommittee.setProperty("in_member_type", "area_cmt");
            itmCommittee = itmCommittee.apply();

            if (!itmCommittee.isError() && itmCommittee.getResult() != "")
            {
                itmReturn.setProperty("in_committee", itmCommittee.getProperty("in_name", ""));
                itmReturn.setProperty("in_committee_short", itmCommittee.getProperty("in_short_org", ""));
                itmReturn.setProperty("in_committee_sno", itmCommittee.getProperty("in_sno", ""));
            }
        }

        //上傳
        private void FileUpload(TConfig cfg, Item itmReturn)
        {
            string id = itmReturn.getProperty("id", "");
            string in_file = itmReturn.getProperty("in_file", "");

            Item item = cfg.inn.newItem("IN_CLA_MEETING_PROMOTION", "merge");
            item.setAttribute("where", "id = '" + id + "'");
            item.setProperty("in_score_photo", in_file);
            Item itmResult = item.apply();

            if (itmResult.isError())
            {
                throw new Exception("上傳失敗，請稍後再試");
            }
        }

        //跳窗
        private void FileModal(TConfig cfg, Item itmReturn)
        {
            string id = itmReturn.getProperty("id", "");
            Item item = cfg.inn.getItemById("IN_CLA_MEETING_PROMOTION", id);

            if (item.isError())
            {
                return;
            }

            itmReturn.setProperty("in_committee_short", item.getProperty("in_committee_short", ""));
            itmReturn.setProperty("in_name", item.getProperty("in_name", ""));
            itmReturn.setProperty("in_echelon", item.getProperty("in_echelon", ""));
            itmReturn.setProperty("in_serial", item.getProperty("in_serial", ""));
            itmReturn.setProperty("old_file", item.getProperty("in_score_photo", ""));
            itmReturn.setProperty("old_file_ext", " ");
        }

        //刷新資料
        private void Refresh(TConfig cfg, Item itmReturn)
        {
            MergeMeetingPromotions(cfg);
        }

        //更新資料
        private void Update(TConfig cfg, Item itmReturn)
        {
            string promotion_id = itmReturn.getProperty("promotion_id", "");
            string property = itmReturn.getProperty("property", "");
            string value = itmReturn.getProperty("value", "");

            Item itmOld = cfg.inn.newItem("In_Cla_Meeting_Promotion", "merge");
            itmOld.setAttribute("where", "id = '" + promotion_id + "'");

            if (property == "in_score_state" && value == "合格")
            {
                itmOld.setProperty("in_score_foot", "O");
                itmOld.setProperty("in_score_poomsae", "O");
                itmOld.setProperty("in_score_sparring", "O");
                itmOld.setProperty("in_score_beat", "O");
                itmOld.setProperty("in_score_state", "合格");
            }
            else
            {
                itmOld.setProperty(property, value);
                if (value == "X")
                {
                    itmOld.setProperty("in_score_state", "不合格(需補測)");
                }
            }

            Item itmResult = itmOld.apply();

            if (itmResult.isError())
            {
                throw new Exception("更新失敗");
            }

            //清空縣市帳號
            itmReturn.setProperty("cmt_sno", "");

            //登錄晉段紀錄
            ResumePromotion(cfg, itmReturn);
        }

        //頁面
        private void Page(TConfig cfg, Item itmReturn)
        {
            Item itmStates = cfg.inn.applySQL("SELECT * FROM VU_Mt_PromotionState ORDER BY sort_order");
            if (itmStates.isError() || itmStates.getResult() == "")
            {
                return;
            }

            cfg.itmStates = itmStates;
            cfg.PromotionStates = StatusMenuBuilder(itmStates);

            Item items = GetMeetingPromotions(cfg, filter_makeup: false, order_by_degree: true);
            if (items.isError() || items.getResult() == "")
            {
                return;
            }

            List<TCommittee> list = MapCommittees(cfg, items);

            Item itmEmpty = cfg.inn.newItem();
            itmEmpty.setType("inn_cmt");
            itmEmpty.setProperty("value", "");
            itmEmpty.setProperty("text", "請選擇縣市");
            itmReturn.addRelationship(itmEmpty);

            for (int i = 0; i < list.Count; i++)
            {
                var cmt = list[i];
                Item itmCmt = cfg.inn.newItem();
                itmCmt.setType("inn_cmt");
                itmCmt.setProperty("value", cmt.in_sno);
                itmCmt.setProperty("text", cmt.city);
                itmReturn.addRelationship(itmCmt);
            }

            //附加梯次選單
            AppendEchelonMenu(cfg, itmReturn);

            StringBuilder tabs = new StringBuilder();
            StringBuilder body = new StringBuilder();

            for (int i = 0; i < list.Count; i++)
            {
                string active = i == 0 ? "active" : "";
                string active2 = i == 0 ? "active in" : "";

                var cmt = list[i];
                tabs.Append("<li id='" + cmt.ctrl_li_id + "' class='nav-item " + active + "'>");
                tabs.Append("  <a id='" + cmt.ctrl_a_id + "'");
                tabs.Append("	href='#" + cmt.ctrl_pn_id + "'");
                tabs.Append("   class='nav-link inn_city' role='tab' data-toggle='pill'");
                tabs.Append("   data-sno='" + cmt.in_sno + "'");
                tabs.Append("	aria-controls='pills-home' aria-selected='true' onclick='StoreTab_Click(this)'>");
                tabs.Append("	" + cmt.city + "(" + cmt.items.Count + ")");
                tabs.Append("  </a>");
                tabs.Append("</li>");

                body.Append("<div class='tab-pane fade " + active2 + "' id='" + cmt.ctrl_pn_id + "' role='tabpanel' >");
                body.Append("  <div class='container bg-white-hw'>");

                body.Append(GetCmtTable(cfg, cmt));

                body.Append("  </div>");
                body.Append("</div>");
            }

            StringBuilder builder = new StringBuilder();
            builder.Append("<ul class='nav nav-pills' style='justify-content: start ;' id='pills-tab' role='tablist'>");
            builder.Append(tabs);
            builder.Append("</ul>");

            builder.Append("<div class='tab-content' id='pills-tabContent'>");
            builder.Append(body);
            builder.Append("</div>");

            itmReturn.setProperty("in_title", cfg.itmMeeting.getProperty("in_title", ""));
            itmReturn.setProperty("keyed_name", cfg.itmMeeting.getProperty("keyed_name", ""));
            itmReturn.setProperty("inn_tabs", builder.ToString());
        }

        private void AppendEchelonMenu(TConfig cfg, Item itmReturn)
        {
            string sql = @"
                SELECT 
                	id
                	, in_echelon
                	, in_title 
                FROM 
                	IN_CLA_MEETING WITH(NOLOCK)
                WHERE 
                	id <> '{#meeting_id}'
                	AND in_meeting_type = 'degree' 
                	AND ISNULL(in_is_main, 0) = 0
                	AND ISNULL(in_is_template, 0) = 0
                	AND ISNULL(in_echelon, '') NOT IN ('', '198')
                ORDER BY 
                	in_echelon
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            Item items = cfg.inn.applySQL(sql);
            if (items.isError() || items.getResult() == "")
            {
                return;
            }

            Item itmEmpty = cfg.inn.newItem();
            itmEmpty.setType("inn_echelon");
            itmEmpty.setProperty("value", "");
            itmEmpty.setProperty("text", "----");
            itmReturn.addRelationship(itmEmpty);

            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string id = item.getProperty("id", "");
                string in_echelon = item.getProperty("in_echelon", "");
                string in_title = item.getProperty("in_title", "");

                item.setType("inn_echelon");
                item.setProperty("value", id);
                item.setProperty("text", in_echelon);
                itmReturn.addRelationship(item);
            }
        }

        private StringBuilder GetCmtTable(TConfig cfg, TCommittee cmt)
        {
            bool is_makeup = cfg.in_qry != "0";

            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            head.Append("<thead>");
            head.Append("<tr>");
            head.Append("  <th class='mailbox-subject text-center '>No.</th>");
            head.Append("  <th class='mailbox-subject text-center '>區別</th>");
            head.Append("  <th class='mailbox-subject text-center '>姓名</th>");
            head.Append("  <th class='mailbox-subject text-center '>晉段</th>");
            head.Append("  <th class='mailbox-subject text-center '>所屬教練</th>");
            //head.Append("  <th class='mailbox-subject text-center '>所屬委員會</th>");
            head.Append("  <th class='mailbox-subject text-center '>足技</th>");
            head.Append("  <th class='mailbox-subject text-center '>型</th>");
            head.Append("  <th class='mailbox-subject text-center '>對練</th>");
            head.Append("  <th class='mailbox-subject text-center '>擊破</th>");
            head.Append("  <th class='mailbox-subject text-center '>合格</th>");

            if (is_makeup)
            {
                head.Append("  <th class='mailbox-subject text-center '>補測</th>");
            }

            head.Append("  <th class='mailbox-subject text-center '>上傳</th>");
            head.Append("</tr>");
            head.Append("</thead>");

            body.Append("<tbody>");
            string last_l1 = cmt.items[0].getProperty("in_l1", "");
            int count = cmt.items.Count;
            for (int i = 0; i < count; i++)
            {
                Item item = cmt.items[i];
                string id = item.getProperty("id", "");
                string muid = item.getProperty("in_user", "");
                string in_l1 = item.getProperty("in_l1", "");
                string in_name = item.getProperty("in_name", "");
                string in_score_photo = item.getProperty("in_score_photo", "");
                string in_makeup_yn = item.getProperty("in_makeup_yn", "");
                string resume_id = item.getProperty("related_id", "");
                string in_degree_area = item.getProperty("in_degree_area", "");
                string in_stuff_c1 = item.getProperty("in_stuff_c1", "");
                string in_stuff_c2 = item.getProperty("in_stuff_c2", "");
                string in_makeup_note = item.getProperty("in_makeup_note", "");


                string score_photo = "晉段卡<i class='fa fa-cloud-upload'></i>";
                if (in_score_photo != "")
                {
                    score_photo = "晉段卡<i class='fa fa-file-photo-o'></i>";
                }

                string name = in_name;
                if (in_makeup_yn == "1")
                {
                    name = "<a href='javascript:void()' onclick='ShowResume_Click(this)' >"
                        + in_name
                        + "</a>"
                        + " <a href='javascript:void()' onclick='UpdateModal_Click(this)'> (補) </a>";
                }
                else
                {
                    name = "<a href='javascript:void()' onclick='ShowResume_Click(this)' >"
                        + in_name
                        + "</a>"
                        + " <a href='javascript:void()' onclick='ShowVerify_Click(this)' > <i class='fa fa-check-circle-o'><i/> </a>";
                }

                if (in_l1 != last_l1)
                {
                    last_l1 = in_l1;
                    body.Append("<tr>");
                    body.Append("  <td class='mailbox-subject text-center '>No.</td>");
                    body.Append("  <td class='mailbox-subject text-center '>區別</td>");
                    body.Append("  <td class='mailbox-subject text-center '>姓名</td>");
                    body.Append("  <td class='mailbox-subject text-center '>晉段</td>");
                    body.Append("  <td class='mailbox-subject text-center '>所屬教練</td>");
                    body.Append("  <td class='mailbox-subject text-center '>足技</td>");
                    body.Append("  <td class='mailbox-subject text-center '>型</td>");
                    body.Append("  <td class='mailbox-subject text-center '>對練</td>");
                    body.Append("  <td class='mailbox-subject text-center '>擊破</td>");
                    body.Append("  <td class='mailbox-subject text-center '>合格</td>");

                    if (is_makeup)
                    {
                        body.Append("  <td class='mailbox-subject text-center '>補測</td>");
                    }

                    body.Append("  <td class='mailbox-subject text-center '>上傳</td>");
                    body.Append("</tr>");
                }

                body.Append("<tr class='show_all'data-pid='" + id + "'"
                    + " data-muid='" + muid + "'"
                    + " data-rid='" + resume_id + "'"
                    + ">");

                body.Append("  <td class='text-center'> " + (i + 1) + " </td>");
                body.Append("  <td class='text-center'> " + in_degree_area + " </td>");
                body.Append("  <td class='text-center'> " + name + " </td>");
                body.Append("  <td class='text-center'> " + in_l1 + " </td>");
                body.Append("  <td class='text-center'> " + in_stuff_c2 + " </td>");
                body.Append("  <td class='text-center'> " + GetScoreMenu(item, "in_score_foot") + " </td>");
                body.Append("  <td class='text-center'> " + GetScoreMenu(item, "in_score_poomsae") + " </td>");
                body.Append("  <td class='text-center'> " + GetScoreMenu(item, "in_score_sparring") + " </td>");
                body.Append("  <td class='text-center'> " + GetScoreMenu(item, "in_score_beat") + " </td>");
                body.Append("  <td class='text-center'> " + GetStatusMenu(cfg, item, "in_score_state") + " </td>");

                if (is_makeup)
                {
                    body.Append("  <td class='text-left'> " + in_makeup_note + " </td>");
                }

                body.Append("  <td class='text-center'> <a href='javascript:void()' onclick='UploadModal_Click(this)'> " + score_photo + " </a> </td>");
                body.Append("</tr>");
            }
            body.Append("</tbody>");

            string table_name = cmt.ctrl_tbl_id;

            StringBuilder builder = new StringBuilder();
            builder.Append("<div class='box-body tablenobg' style='position: relative'>");
            builder.AppendLine(GetTableAttribute(table_name));
            builder.Append(head);
            builder.Append(body);
            builder.AppendLine("</table>");
            builder.Append("</div>");
            builder.AppendLine("<script>");
            builder.AppendLine("$('#" + table_name + "').bootstrapTable();");
            builder.AppendLine("if ($(window).width() <= 768) { $('#" + table_name + "').bootstrapTable('toggleView'); }");
            builder.AppendLine("</script>");

            return builder;
        }

        private string GetScoreMenu(Item item, string prop)
        {
            string value = item.getProperty(prop, "");

            StringBuilder builder = new StringBuilder();
            builder.Append("<select class='form-control score_menu " + prop + "'");
            builder.Append("  data-value='" + value + "'");
            builder.Append("  onchange=\"doUpdateOption(this, '" + prop + "')\">");
            builder.Append("    <option value=''>--</option>");
            builder.Append("    <option value='O'>O</option>");
            builder.Append("    <option value='X'>X</option>");
            builder.Append("</select>");
            return builder.ToString();
        }

        private string GetStatusMenu(TConfig cfg, Item item, string prop)
        {
            string value = item.getProperty(prop, "");

            var builder = new StringBuilder(cfg.PromotionStates);
            builder.Replace("{#prop}", prop);
            builder.Replace("{#value}", value);

            return builder.ToString();
        }

        private string StatusMenuBuilder(Item items)
        {
            StringBuilder options = new StringBuilder();
            options.Append("    <option value=''>未設定</option>");

            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string value = item.getProperty("value", "");
                options.Append("    <option value='" + value + "'>" + value + "</option>");
            }

            StringBuilder builder = new StringBuilder();
            builder.Append("<select class='form-control score_menu {#prop}'");
            builder.Append("  data-value='{#value}'");
            builder.Append("  onchange=\"doUpdateOption(this, '{#prop}')\">");
            builder.Append(options);
            builder.Append("</select>");
            return builder.ToString();
        }

        private string GetTableAttribute(string table_name)
        {
            return "<table id='" + table_name + "'"
                + " class='table table-hover table-striped'"
                + " data-toggle='table'"
                + " data-show-toggle='true'"
                + " data-show-columns='true'"
                + " data-sort-order='asc'"
                + " data-search-align='left'"
                + " data-sort-stable='true'"
                + " data-search='true'"
                + ">";
        }

        private void MergeMeetingPromotions(TConfig cfg)
        {
            string mt_title = cfg.itmMeeting.getProperty("in_title", "");
            string mt_annual = cfg.itmMeeting.getProperty("in_annual", "");
            string mt_echelon = cfg.itmMeeting.getProperty("in_echelon", "");

            Item itmMUsers = GetMeetingUsers(cfg);

            Item itmMAreas = GetMeetingAreas(cfg, cfg.meeting_id);
            var area_map = MapItem(itmMAreas, "in_area");

            Item itmBelts = GetBelts(cfg);
            var belt_map = MapItem(itmBelts, "label");

            int count = itmMUsers.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item itmMUser = itmMUsers.getItemByIndex(i);

                string in_serial = "1";

                string muid = itmMUser.getProperty("id", "");
                string in_l1 = itmMUser.getProperty("in_l1", "");
                string in_degree_area = itmMUser.getProperty("in_degree_area", "");
                string resume_id = itmMUser.getProperty("resume_id", "");

                Item itmMPromotion = cfg.inn.newItem("In_Cla_Meeting_Promotion", "merge");
                itmMPromotion.setAttribute("where", "in_key = '" + muid + "' AND in_serial = " + in_serial);

                itmMPromotion.setProperty("in_key", muid);
                itmMPromotion.setProperty("in_serial", in_serial);
                itmMPromotion.setProperty("in_makeup_yn", "0");
                itmMPromotion.setProperty("in_makeup_note", "");

                itmMPromotion.setProperty("source_id", cfg.meeting_id);
                itmMPromotion.setProperty("related_id", resume_id);
                itmMPromotion.setProperty("in_user", muid);

                itmMPromotion.setProperty("in_annual", mt_annual);
                itmMPromotion.setProperty("in_echelon", mt_echelon);

                itmMPromotion.setProperty("in_name", itmMUser.getProperty("in_name", ""));
                itmMPromotion.setProperty("in_en_name", itmMUser.getProperty("in_en_name", ""));
                itmMPromotion.setProperty("in_sno", itmMUser.getProperty("in_sno", ""));
                itmMPromotion.setProperty("in_birth", DateTimeStr(itmMUser.getProperty("in_birth", ""), hours: 8));
                itmMPromotion.setProperty("in_gender", itmMUser.getProperty("in_gender", ""));
                itmMPromotion.setProperty("in_stuff_c1", itmMUser.getProperty("in_stuff_c1", ""));
                itmMPromotion.setProperty("in_stuff_c2", itmMUser.getProperty("in_stuff_c2", ""));
                itmMPromotion.setProperty("in_stuff_c1_sno", "");

                itmMPromotion.setProperty("in_gym", itmMUser.getProperty("in_current_org", ""));
                itmMPromotion.setProperty("in_gym_sno", itmMUser.getProperty("in_creator_sno", ""));

                itmMPromotion.setProperty("in_committee", itmMUser.getProperty("in_committee", ""));
                itmMPromotion.setProperty("in_committee_sno", itmMUser.getProperty("committee_sno", ""));
                itmMPromotion.setProperty("in_committee_short", itmMUser.getProperty("committee_short_name", ""));

                itmMPromotion.setProperty("in_area", itmMUser.getProperty("in_area", ""));
                itmMPromotion.setProperty("in_current_org", itmMUser.getProperty("in_current_org", ""));

                if (area_map.ContainsKey(in_degree_area))
                {
                    Item itmMArea = area_map[in_degree_area];
                    itmMPromotion.setProperty("in_degree_area", itmMArea.getProperty("in_area", ""));
                    itmMPromotion.setProperty("in_area_sort", itmMArea.getProperty("in_sort_order", ""));
                    itmMPromotion.setProperty("in_location", itmMArea.getProperty("in_location", ""));
                    itmMPromotion.setProperty("in_place", itmMArea.getProperty("in_place", ""));
                    itmMPromotion.setProperty("in_date", DateTimeStr(itmMArea.getProperty("in_date", "")));
                    itmMPromotion.setProperty("in_area_id", itmMArea.getProperty("id", ""));
                }
                else
                {
                    itmMPromotion.setProperty("in_degree_area", in_degree_area);
                    itmMPromotion.setProperty("in_area_sort", "100");
                    itmMPromotion.setProperty("in_location", "");
                    itmMPromotion.setProperty("in_place", "");
                    itmMPromotion.setProperty("in_date", "");
                    itmMPromotion.setProperty("in_area_id", "");
                }

                if (belt_map.ContainsKey(in_l1))
                {
                    Item itmBelt = belt_map[in_l1];
                    itmMPromotion.setProperty("in_l1", in_l1);
                    itmMPromotion.setProperty("in_degree", itmBelt.getProperty("value", ""));
                }
                else
                {
                    itmMPromotion.setProperty("in_l1", in_l1);
                    itmMPromotion.setProperty("in_degree", "0");
                }

                Item itmResult = itmMPromotion.apply();

                if (itmResult.isError())
                {
                    throw new Exception("建立 晉段測驗成績 發生錯誤");
                }
            }
        }

        private Dictionary<string, Item> MapItem(Item items, string key_pro)
        {
            Dictionary<string, Item> map = new Dictionary<string, Item>();

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string key_val = item.getProperty(key_pro, "");

                if (!map.ContainsKey(key_val))
                {
                    map.Add(key_val, item);
                }
            }

            return map;
        }

        private Item GetBelts(TConfig cfg)
        {
            string sql = "SELECT * FROM VU_DEGREE ORDER BY sort_order";
            return cfg.inn.applySQL(sql);
        }

        private Item GetMeetingAreas(TConfig cfg, string meeting_id)
        {
            string sql = "SELECT * FROM IN_CLA_MEETING_AREA WITH(NOLOCK)"
                + " WHERE source_id = '{#meeting_id}'";

            sql = sql.Replace("{#meeting_id}", meeting_id);

            return cfg.inn.applySQL(sql);
        }

        private Item GetMeetingPromotions(TConfig cfg, bool filter_makeup = false, bool order_by_degree = false)
        {
            string sql = @"
                SELECT
	                t1.*
	                , t2.in_area AS 'in_committee_city'
                FROM 
	                IN_CLA_MEETING_PROMOTION t1 WITH(NOLOCK)
                INNER JOIN
	                IN_RESUME t2 WITH(NOLOCK)
	                ON t2.in_sno = t1.in_committee_sno
                WHERE 
	                t1.source_id = '{#meeting_id}'
            ";

            if (filter_makeup || cfg.in_qry == "1")
            {
                sql += " AND ISNULL(t1.in_makeup_yn, 0) = 1";
            }
            else if (cfg.in_qry == "0")
            {
                sql += " AND ISNULL(t1.in_makeup_yn, 0) = 0";
            }

            if (order_by_degree)
            {
                sql += " ORDER BY t1.in_degree DESC, t1.in_birth";
            }
            else
            {
                sql += " ORDER BY t1.in_area_sort, t1.in_degree DESC, t1.in_birth";
            }

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);
            return cfg.inn.applySQL(sql);
        }

        private Item GetMeetingUsers(TConfig cfg)
        {
            string sql = @"
                SELECT
	                t1.id               AS 'mt_id'
	                , t1.in_annual      AS 'mt_annual'
	                , t1.in_echelon     AS 'mt_echelon'
	                , t2.*
                    , t11.in_sno        AS 'committee_sno'
                    , t11.in_short_org  AS 'committee_short_name'
                    , t12.id            AS 'resume_id'
                FROM
	                IN_CLA_MEETING t1 WITH(NOLOCK)
                INNER JOIN
	                IN_CLA_MEETING_USER t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                INNER JOIN
	                IN_RESUME t11 WITH(NOLOCK)
	                ON t11.in_name = t2.in_committee
                    AND t11.in_member_type = 'area_cmt' 
                    AND t11.in_member_role = 'sys_9999'
                INNER JOIN 
                    IN_RESUME t12 WITH(NOLOCK)
                    ON t12.in_sno = t2.in_sno
                WHERE
	                t1.id = '{#meeting_id}'
	                AND ISNULL(t2.in_ass_ver_result, '') = '1'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            return cfg.inn.applySQL(sql);
        }

        private Item GetMakeupPromotions(TConfig cfg, List<TMeeting> list)
        {
            var meetings = string.Join(", ", list.Select(x => "'" + x.id + "'").ToList());

            var filter_states = new List<string>
            {
                "不合格(需補測)",
                "未到",
                "請假",
            };

            string states = string.Join(", ", filter_states.Select(x => "N'" + x + "'").ToList());

            string sql = @"
                SELECT
	                t1.*
	                , t2.in_area AS 'in_committee_city'
                FROM 
	                IN_CLA_MEETING_PROMOTION t1 WITH(NOLOCK)
                INNER JOIN
	                IN_RESUME t2 WITH(NOLOCK)
	                ON t2.in_sno = t1.in_committee_sno
                WHERE 
	                t1.source_id IN ({#meetings})
                    AND t1.in_score_state IN ({#states})
                ORDER BY 
                    t1.in_degree DESC
                    , t1.in_birth
            ";

            sql = sql.Replace("{#meetings}", meetings)
                .Replace("{#states}", states);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        private List<TMeeting> LastYearsMeeting(TConfig cfg, int mt_echelon, int year)
        {
            List<TMeeting> result = new List<TMeeting>();

            for (int i = mt_echelon - year; i <= mt_echelon; i++)
            {
                result.Add(new TMeeting { id = "", in_title = "", in_echelon = i.ToString() });
            }

            string sql = @"
                SELECT 
	                id
	                , in_title
	                , in_echelon 
                FROM 
	                IN_CLA_MEETING WITH(NOLOCK)
                WHERE 
                    in_meeting_type = 'degree' 
                    AND ISNULL(in_is_main, 0) = 0
                    AND ISNULL(in_is_template, 0) = 0
	                AND in_echelon IN ({#years})
                ORDER BY
	                in_echelon
            ";

            var years = result.Select(x => "'" + x.in_echelon + "'").ToList();

            sql = sql.Replace("{#years}", string.Join(", ", years));

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_echelon = item.getProperty("in_echelon", "0");
                var obj = result.Find(x => x.in_echelon == in_echelon);
                if (obj != null)
                {
                    obj.id = item.getProperty("id", "");
                    obj.in_title = item.getProperty("in_title", "");
                }
            }

            return result;
        }

        private class TMeeting
        {
            public string id { get; set; }
            public string in_title { get; set; }
            public string in_echelon { get; set; }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }

            public string meeting_id { get; set; }
            public string in_qry { get; set; }
            public string scene { get; set; }
            public string cmt_sno { get; set; }
            public string cmt_city { get; set; }

            public Item itmMeeting { get; set; }

            public string mt_title { get; set; }
            public string doc_name { get; set; }
            public string ext_type { get; set; }
            public string[] CharSet { get; set; }
            public int font_size { get; set; }
            public int row_height { get; set; }

            public Item itmStates { get; set; }
            public string PromotionStates { get; set; }
        }

        private class TCommittee
        {
            public string in_name { get; set; }
            public string in_sno { get; set; }
            public string in_short_name { get; set; }
            public string city { get; set; }

            public List<Item> items { get; set; }

            public string ctrl_li_id { get; set; }
            public string ctrl_pn_id { get; set; }
            public string ctrl_a_id { get; set; }
            public string ctrl_tbl_id { get; set; }

            public List<TEchUser> users { get; set; }
        }

        private class TEchUser
        {
            public string in_sno { get; set; }
            public Item value { get; set; }
            public List<TMeeting> meetings { get; set; }
            public List<string> makeups { get; set; }
        }

        private List<TCommittee> MapCommittees(TConfig cfg, Item items)
        {
            List<TCommittee> result = new List<TCommittee>();

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                string in_committee = item.getProperty("in_committee", "");
                string in_committee_sno = item.getProperty("in_committee_sno", "");
                string in_committee_short = item.getProperty("in_committee_short", "");
                string city = item.getProperty("in_committee_city", "");

                TCommittee cmt = result.Find(x => x.in_sno == in_committee_sno);

                if (cmt == null)
                {
                    cmt = new TCommittee
                    {
                        in_name = in_committee,
                        in_sno = in_committee_sno,
                        in_short_name = in_committee_short,
                        city = city,
                        items = new List<Item>(),
                    };

                    cmt.ctrl_li_id = "cmt_li_" + cmt.in_sno;
                    cmt.ctrl_pn_id = "cmt_pn_" + cmt.in_sno;
                    cmt.ctrl_a_id = "cmt_a_" + cmt.in_sno;
                    cmt.ctrl_tbl_id = "cmt_tbl_" + cmt.in_sno;

                    result.Add(cmt);
                }

                cmt.items.Add(item);
            }

            return result.OrderBy(x => x.in_sno).ToList();
        }

        private string GetPromotionDay(TConfig cfg, string mt_annual, string mt_date)
        {
            string result = "";

            int west_year = GetIntVal(mt_annual) + 1911;

            switch (mt_date)
            {
                case "03-30":
                    result = west_year + "-03-29T16:00:00";
                    break;
                case "03-31":
                    result = west_year + "-03-30T16:00:00";
                    break;
                case "06-30":
                    result = west_year + "-06-29T16:00:00";
                    break;
                case "09-30":
                    result = west_year + "-09-29T16:00:00";
                    break;
                case "12-30":
                    result = west_year + "-12-29T16:00:00";
                    break;
                case "12-31":
                    result = west_year + "-12-30T16:00:00";
                    break;
            }

            return result;
        }

        private string DateTimeStr(string value, string format = "yyyy-MM-ddTHH:mm:ss", int hours = 8)
        {
            if (value == "") return "";

            DateTime dt = DateTime.Now;
            if (DateTime.TryParse(value, out dt))
            {
                return dt.AddHours(hours).ToString(format);
            }
            else
            {
                return value;
            }
        }

        private int GetIntVal(string value)
        {
            int result = 0;
            int.TryParse(value, out result);
            return result;
        }
    }
}