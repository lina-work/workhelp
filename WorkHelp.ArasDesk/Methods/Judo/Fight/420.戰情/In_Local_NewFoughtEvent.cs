﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;
using System.Net;

namespace WorkHelp.ArasDesk.Methods.Judo.Fight
{
    public class In_Local_NewFoughtEvent : Item
    {
        public In_Local_NewFoughtEvent(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 推播最新場次
                日誌: 
                    - 2022-02-10: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]" + "In_Local_NewFoughtEvent";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                strIdentityId = inn.getUserAliases(),
            };


            return itmR;
        }

        private void NewFoughtEvents(TConfig cfg)
        {
            //Api
            string api_url = GetVariable(cfg, "api_url");

            string sql = @"
                SELECT TOP 1 
	                id
	                , in_table
	                , REPLACE(CONVERT(VARCHAR(30), DATEADD(HOUR, 8, in_time), 126), 'T', ' ') AS 'in_time'
	                , in_count
                FROM 
	                In_Meeting_LocalNotify WITH(NOLOCK)
            ";

            var itmSetting = cfg.inn.applySQL(sql);
            if (!itmSetting.isError() && itmSetting.getResult() != "")
            {
                string id = itmSetting.getProperty("id", "");
                string in_table = itmSetting.getProperty("in_table", "");
                string in_time = itmSetting.getProperty("in_time", "");
                string in_count = itmSetting.getProperty("in_count", "");

                string last_time = in_time;

                sql = "SELECT TOP " + in_count + " * FROM " + in_table + " WHERE SyncTime > '" + in_time + "' ORDER BY SyncTime ";
                var items = cfg.inn.applySQL(sql);
                var count = items.getItemCount();
                for (int i = 0; i < count; i++)
                {
                    Item item = items.getItemByIndex(i);
                    string site = item.getProperty("site", "");
                    string eround = item.getProperty("eround", "");
                    string eweight = item.getProperty("eweight", "");
                    string win = item.getProperty("win", "");

                    string css = "";
                    string org = "";
                    string name = "";

                    if (win == "B")
                    {
                        css = "紅方";
                        org = item.getProperty("blued", "");
                        name = item.getProperty("bluen", "");
                    }

                    if (win == "W")
                    {
                        css = "白方";
                        org = item.getProperty("whited", "");
                        name = item.getProperty("whiten", "");
                    }

                    string site_name = GetSiteName(site);

                    string message = site_name
                        + " " + eround
                        + " " + eweight
                        + " " + css
                        + " " + org
                        + " " + name
                        ;
                }
            }
        }

        private string GetVariable(TConfig cfg, string in_name)
        {
            Item item = cfg.inn.applySQL("SELECT TOP 1 in_value FROM IN_VARIABLE WITH(NOLOCK) WHERE in_name = '" + in_name + "'");
            if (item.isError() || item.getResult() == "")
            {
                throw new Exception("查無 " + in_name + " 參數資料");
            }
            return item.getProperty("in_value", "").TrimEnd('/');
        }

        //推播
        private string RunNotify(TConfig cfg, string api_url)
        {
            string result = "";

            try
            {
                string fullurl = api_url + "/create?url=";
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, fullurl);

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(fullurl);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                StreamReader tReader = new StreamReader(response.GetResponseStream());

                string rst = tReader.ReadToEnd();
                if (!string.IsNullOrWhiteSpace(rst))
                {
                    result = rst.Replace("\"", "");
                }
            }
            catch (Exception ex)
            {
                result = "";
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, ex.Message);
            }

            return result;
        }

        private string GetSiteName(string value)
        {
            switch(value)
            {
                case "A": return "第一場地";
                case "B": return "第二場地";
                case "C": return "第三場地";
                case "D": return "第四場地";
                case "E": return "第五場地";
                case "F": return "第六場地";
                case "G": return "第七場地";
                case "H": return "第八場地";
                case "I": return "第九場地";
                case "J": return "第十場地";
                case "K": return "第十一場地";
                case "L": return "第十二場地";
                default: return "ERR";
            }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }
        }

        private string GetDtmStr(string value, string format)
        {
            return GetDtmVal(value).ToString(format);
        }

        private DateTime GetDtmVal(string value)
        {
            if (value == "") return DateTime.MinValue;
            DateTime dt = DateTime.MinValue;
            DateTime.TryParse(value, out dt);
            return dt;
        }

    }
}
