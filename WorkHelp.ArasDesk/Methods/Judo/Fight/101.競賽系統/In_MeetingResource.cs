﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Judo.Common
{
    public class In_MeetingResource : Item
    {
        public In_MeetingResource(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
    目的: 花絮集錦設定
    日誌: 
        - 2023-05-12: 創建 (lina)
*/

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "In_MeetingResource";

            var itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);


            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                meeting_id = itmR.getProperty("meeting_id", ""),
                scene = itmR.getProperty("scene", ""),
            };

            switch (cfg.scene)
            {
                case "save":
                    Save(cfg, itmR);
                    break;

                case "page":
                    Page(cfg, itmR);
                    break;

                case "titbits_page":
                    TitbitsPage(cfg, itmR);
                    break;

                case "edit_titbits":
                    TitbitsEdit(cfg, itmR);
                    break;

                case "edit_player":
                    TitbitsPlayerEdit(cfg, itmR);
                    break;

                case "remove_titbits":
                    TitbitsRemove(cfg, itmR);
                    break;

                case "force_remove_titbits":
                    TitbitsRemoveForce(cfg, itmR);
                    break;

                case "broadcast_page":
                    BroadcastPage(cfg, itmR);
                    break;
            }


            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);


            return itmR;
        }

        //賽會廣播
        private void BroadcastPage(TConfig cfg, Item itmReturn)
        {
            if (cfg.meeting_id == "") return;

            var itmMeeting = cfg.inn.applySQL("SELECT * FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            CopyItemValue(itmReturn, itmMeeting, "in_title");

            var sql = @"
				SELECT 
				    t1.*
					, t2.in_title
				FROM 
					IN_MEETING_BROADCAST t1 WITH(NOLOCK)
				INNER JOIN
					IN_MEETING t2 WITH(NOLOCK)
					ON t2.id = t1.meeting_id
				ORDER BY
					t1.created_on DESC
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                item.setType("inn_message");
                item.setProperty("no", (i + 1).ToString());
                itmReturn.addRelationship(item);
            }
        }

        private void TitbitsRemoveForce(TConfig cfg, Item itmReturn)
        {
            var file_id = "E0E4B75FA2A04DA59A83F4C89330ADBA";
            var in_api_path = @"C:\Site\SportApi\photovault\judo\61EBAE60D44A4E9C99C0789CB2C5BC40\E0E4B75FA2A04DA59A83F4C89330ADBA.jpeg";

            try
            {
                var itmFile = cfg.inn.newItem("File", "delete");
                itmFile.setAttribute("where", "id = '" + file_id + "'");
                itmFile = itmFile.apply();

                if (System.IO.File.Exists(in_api_path))
                {
                    System.IO.File.Delete(in_api_path);
                }
            }
            catch (Exception ex)
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, ex.Message);
            }
        }

        private void TitbitsRemove(TConfig cfg, Item itmReturn)
        {
            var id = itmReturn.getProperty("id", "");

            Item itmOld = cfg.inn.applySQL("SELECT * FROM IN_MEETING_TITBITS WITH(NOLOCK) WHERE id = '" + id + "'");
            if (itmOld.isError() || itmOld.getResult() == "")
            {
                throw new Exception("資料異常");
            }

            var in_player_name = itmOld.getProperty("in_player_name", "");
            var file_id = itmOld.getProperty("in_file", "");
            var in_api_path = itmOld.getProperty("in_api_path", "");
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "刪除 " + in_player_name + ": " + file_id + ": " + in_api_path);

            try
            {
                itmOld = cfg.inn.newItem("In_Meeting_Titbits", "delete");
                itmOld.setAttribute("where", "id = '" + id + "'");
                itmOld = itmOld.apply();

                var itmFile = cfg.inn.newItem("File", "delete");
                itmFile.setAttribute("where", "id = '" + file_id + "'");
                itmFile = itmFile.apply();

                if (System.IO.File.Exists(in_api_path))
                {
                    System.IO.File.Delete(in_api_path);
                }
            }
            catch (Exception ex)
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, ex.Message);
            }
        }

        private void TitbitsPlayerEdit(TConfig cfg, Item itmReturn)
        {
            var id = itmReturn.getProperty("id", "");
            var in_player_name = itmReturn.getProperty("in_player_name", "");
            var in_description = itmReturn.getProperty("in_description", "");

            var sql = "UPDATE IN_MEETING_TITBITS SET"
                + "  in_player_name = N'" + in_player_name + "'"
                + ", in_description = N'" + in_description + "'"
                + " WHERE id = '" + id + "'";

            cfg.inn.applySQL(sql);
        }

        private void TitbitsEdit(TConfig cfg, Item itmReturn)
        {
            var id = itmReturn.getProperty("id", "");
            var in_closed = itmReturn.getProperty("in_closed", "");
            var sql = "UPDATE IN_MEETING_TITBITS SET in_closed = '" + in_closed + "' WHERE id = '" + id + "'";
            cfg.inn.applySQL(sql);
        }

        //花絮集錦
        private void TitbitsPage(TConfig cfg, Item itmReturn)
        {
            if (cfg.meeting_id == "") return;

            var itmMeeting = cfg.inn.applySQL("SELECT * FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            CopyItemValue(itmReturn, itmMeeting, "in_title");

            var sql = @"
				SELECT 
				    t1.*
					, t2.filename
					, t2.mimetype
				FROM 
					IN_MEETING_TITBITS t1 WITH(NOLOCK)
				INNER JOIN
					[FILE] t2 WITH(NOLOCK)
					ON t2.ID = t1.in_file
				WHERE 
					t1.in_meeting = '{#meeting_id}'
				ORDER BY
					t1.created_on DESC
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                item.setType("inn_photo");
                item.setProperty("no", (i + 1).ToString());
                itmReturn.addRelationship(item);
            }
        }

        //儲存
        private void Save(TConfig cfg, Item itmReturn)
        {
            var in_type = itmReturn.getProperty("in_type", "");
            var in_property = itmReturn.getProperty("in_property", "");
            var in_value = itmReturn.getProperty("in_value", "");

            if (in_type != "variable")
            {
                var itmOld = cfg.inn.newItem("In_Meeting_Resource", "merge");
                itmOld.setAttribute("where", "in_meeting = '" + cfg.meeting_id + "' AND in_property = '" + in_property + "'");
                CopyItemValue(itmOld, itmReturn, "in_meeting", "meeting_id");
                CopyItemValue(itmOld, itmReturn, "in_type");
                CopyItemValue(itmOld, itmReturn, "in_name");
                CopyItemValue(itmOld, itmReturn, "in_value");
                CopyItemValue(itmOld, itmReturn, "in_property");
                CopyItemValue(itmOld, itmReturn, "in_sort_order");
                itmOld = itmOld.apply();
            }

            var sql = "UPDATE IN_SITE SET [" + in_property + "] = N'" + in_value + "'";
            cfg.inn.applySQL(sql);
        }

        //頁面
        private void Page(TConfig cfg, Item itmReturn)
        {
            var itmSite = cfg.inn.applySQL("SELECT TOP 1 * FROM IN_SITE WITH(NOLOCK)");
            CopyItemValue(itmReturn, itmSite, "in_meeting_playercard");
            CopyItemValue(itmReturn, itmSite, "in_meeting_video");
            CopyItemValue(itmReturn, itmSite, "in_broadcast_count");
            CopyItemValue(itmReturn, itmSite, "in_tidbits_count");

            if (cfg.meeting_id == "") return;
            var itmMeeting = cfg.inn.applySQL("SELECT * FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            CopyItemValue(itmReturn, itmMeeting, "in_title");

            var itmMResources = cfg.inn.applySQL("SELECT * FROM In_Meeting_Resource WITH(NOLOCK) WHERE in_meeting = '" + cfg.meeting_id + "' ORDER BY in_sort_order");
            var count = itmMResources.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var itmMResource = itmMResources.getItemByIndex(i);
                var in_property = itmMResource.getProperty("in_property", "");
                CopyItemValue(itmReturn, itmMResource, in_property, "in_value");
            }
        }

        private void CopyItemValue(Item itmA, Item itmB, string propA, string propB = "")
        {
            if (propB == "")
            {
                itmA.setProperty(propA, itmB.getProperty(propA, ""));
            }
            else
            {
                itmA.setProperty(propA, itmB.getProperty(propB, ""));
            }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }

            public string meeting_id { get; set; }
            public string scene { get; set; }
        }
    }
}