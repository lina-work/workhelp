﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Judo.Fight
{
    public class In_meeting_allocation_national : Item
    {
        public In_meeting_allocation_national(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 場地分配 - 全國運版
                輸入: meeting_id
                日期: 
                    2023-10-19: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]in_meeting_allocation";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "input dom: " + itmR.dom.InnerXml);

            string meeting_id = itmR.getProperty("meeting_id", "");
            string mode = itmR.getProperty("mode", "");
            string menu = itmR.getProperty("menu", "");

            if (meeting_id == "")
            {
                itmR.setProperty("error_message", "賽事 id 不得為空白");
                return itmR;
            }

            if (menu == "no")
            {
                itmR.setProperty("hide_menu", "item_show_0");
            }

            //檢查頁面權限
            Item itmPermit = inn.applyMethod("In_CheckIdentity", "<method>" + strMethodName + "</method><code>" + "ALL" + "</code>");
            bool isMeetingAdmin = itmPermit.getProperty("isMeetingAdmin", "") == "1";
            bool isMeetingLeader = itmPermit.getProperty("isMeetingLeader", "") == "1";

            bool can_view = isMeetingAdmin || isMeetingLeader;
            if (!can_view)
            {
                itmR.setProperty("error_message", "您無權限瀏覽此頁面");
                return itmR;
            }

            FixTreeNo(CCO, strMethodName, inn, itmR);

            return itmR;
        }

        private void FixTreeNo(Aras.Server.Core.CallContext CCO, string strMethodName, Innovator inn, Item itmReturn)
        {
            string meeting_id = itmReturn.getProperty("meeting_id", "");
            string in_date_source = itmReturn.getProperty("in_date", "");
            string in_date_key = GetDateTimeVal(in_date_source, "yyyy-MM-dd");
            string in_sort = itmReturn.getProperty("in_sort", "");

            if (meeting_id == "") throw new Exception("賽事 id 不得為空白");
            if (in_date_source == "") throw new Exception("比賽日期 不得為空白");
            if (in_sort == "") throw new Exception("排列方式 不得為空白");

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                strMethodName = strMethodName,
                inn = inn,
                meeting_id = meeting_id,
                in_date_key = in_date_key,
                in_sort = in_sort,
            };

            //取得賽事資料
            cfg.itmMeeting = inn.applySQL("SELECT id, in_battle_type, in_robin_player, in_battle_repechage FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            cfg.mt_battle_type = cfg.itmMeeting.getProperty("in_battle_type", "");
            cfg.is_challenge = cfg.mt_battle_type == "Challenge";
            cfg.is_quarter = cfg.itmMeeting.getProperty("in_battle_repechage", "").ToLower() == "quarterfinals";
            cfg.robin_player = GetIntVal(cfg.itmMeeting.getProperty("in_robin_player", "0"));

            //分析該日全部組別的賽制(EX: 中正盃  團體賽: 單淘、個人賽: 四柱復活賽)
            cfg.day_battle_type = GetDayBattleType(cfg, cfg.mt_battle_type);

            //更新組別-場地場次序
            FixProgramMatNo(cfg);

            //重設場次編號(每個組別 reset tree_no)
            Item itmData = inn.newItem("In_Meeting_Program");
            itmData.setProperty("meeting_id", cfg.meeting_id);
            itmData.setProperty("program_id", "");
            itmData.setProperty("in_date", in_date_key);
            itmData.apply("in_meeting_pevent_reset");

            //重算獎牌戰類型、賽別時程、出賽人數
            itmData.apply("in_meeting_program_medal");

            string sql_update = "";

            //清空特殊場次編號
            sql_update = "UPDATE IN_MEETING_PEVENT SET in_tree_no = NULL"
                + " WHERE in_meeting = '" + cfg.meeting_id + "'"
                + " AND in_date_key = '" + in_date_key + "'"
                + " AND in_tree_name IN ('rank34', 'rank56', 'rank78', 'sub')";
            inn.applySQL(sql_update);

            //清空敗部決賽場次編號
            sql_update = "UPDATE IN_MEETING_PEVENT SET in_tree_no = NULL"
                + " WHERE in_meeting = '" + cfg.meeting_id + "'"
                + " AND in_date_key = '" + in_date_key + "'"
                + " AND in_tree_name = 'repechage' AND in_round_code = 2";
            inn.applySQL(sql_update);

            //紀錄初始 TreeNo
            sql_update = "UPDATE IN_MEETING_PEVENT SET in_tree_sno = in_tree_no"
                + " WHERE in_meeting = '" + cfg.meeting_id + "'"
                + " AND in_date_key = '" + in_date_key + "'"
                + " AND ISNULL(in_tree_sno, 0) = 0";
            inn.applySQL(sql_update);

            //取得場地資料
            Item itmSites = inn.applySQL("SELECT * FROM IN_MEETING_SITE WITH(NOLOCK)"
                + " WHERE in_meeting = '" + cfg.meeting_id + "' ORDER BY in_code");

            //執行編制
            AllocateTreeNo(cfg, itmSites);

            //補上顯示用場次
            ResetShowSiteSerial(cfg);

            //清除暫存資訊(三階選單)
            ClearCache(cfg, itmReturn);

            //儲存排列模式
            UpdMeetingVariable(cfg, in_sort);
        }

        //修正組別場地場次序
        private void FixProgramMatNo(TConfig cfg)
        {
            string sql = @"
                UPDATE t1 SET
                	t1.in_site_mat = 'MAT ' + CAST(in_code AS varchar) + '-' + CAST(rn AS VARCHAR)
                	, t1.in_site_mat2 = 'MAT ' + CAST(in_code AS varchar) + '-' + RIGHT(REPLICATE('0', 2) + CAST(rn as VARCHAR), 2)
                FROM
                	IN_MEETING_PROGRAM t1
                INNER JOIN
                (
                	SELECT
                		t11.in_program
                		, t12.in_code
                		, ROW_NUMBER() OVER (PARTITION BY t12.in_code ORDER BY t11.created_on) AS 'rn'
                	FROM
                		IN_MEETING_ALLOCATION t11 WITH(NOLOCK)
                	INNER JOIN
                		IN_MEETING_SITE t12 WITH(NOLOCK)
                		ON t12.id = t11.in_site
                	WHERE
                		t11.in_meeting = '{#meeting_id}'
                		AND t11.in_date_key = '{#in_date_key}'
                ) t2 ON t2.in_program = t1.id
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_date_key}", cfg.in_date_key);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            cfg.inn.applySQL(sql);

            sql = @"UPDATE t1 SET 
            	t1.in_fight_day = t2.in_date_key
            	, t1.in_fight_site = t2.in_place
            	, t1.in_site = t2.in_site
            FROM
            	IN_MEETING_PROGRAM t1
            INNER JOIN
            	IN_MEETING_ALLOCATION t2
            	ON t2.in_program = t1.id
            WHERE
            	t2.in_meeting = '{#meeting_id}'
            	AND t2.in_date_key = '{#in_date_key}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_date_key}", cfg.in_date_key);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            cfg.inn.applySQL(sql);
        }

        private void ResetShowSiteSerial(TConfig cfg)
        {
            string sql = @"
                UPDATE t1 SET
                	t1.in_show_site = t1.in_site_code
                	, t1.in_show_serial = t1.in_tree_no
                FROM
                	IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
                	IN_MEETING_SITE t2 WITH(NOLOCK)
                	ON t2.id  = t1.in_site
                WHERE
                	t1.in_meeting = '{#meeting_id}'
                	AND t1.in_date_key = '{#in_date_key}'
                	AND ISNULL(t1.in_tree_no, 0) > 0
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_date_key}", cfg.in_date_key);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            cfg.inn.applySQL(sql);
        }

        //儲存排列模式
        private void UpdMeetingVariable(TConfig cfg, string in_sort)
        {
            Item itmData = cfg.inn.newItem();
            itmData.setType("In_Meeting_Variable");
            itmData.setProperty("meeting_id", cfg.meeting_id);
            itmData.setProperty("scene", "update");
            itmData.setProperty("name", "allocate_mode");
            itmData.setProperty("value", in_sort);
            itmData.apply("In_Meeting_Variable");
        }

        //清除暫存資訊(三階選單)
        private void ClearCache(TConfig cfg, Item itmReturn)
        {
            Item itmData = cfg.inn.newItem();
            itmData.setType("In_Meeting");
            itmData.setProperty("meeting_id", cfg.meeting_id);
            itmData.setProperty("scene", "clear");
            itmData.apply("in_meeting_program_options");

            Item itmData2 = cfg.inn.newItem();
            itmData2.setType("In_Meeting");
            itmData2.setProperty("meeting_id", cfg.meeting_id);
            itmData2.setProperty("scene", "clear");
            itmData2.apply("in_meeting_day_options");
        }

        //執行編制
        private void AllocateTreeNo(TConfig cfg, Item itmSites)
        {
            //更新場次的比賽日期與場地 (如果是自動配發場地 一個量級可能分散在多個場地)
            UpdateDateAndSite(cfg);

            //更新東西側註記
            RunUpdateWestAndEast(cfg);

            int site_count = itmSites.getItemCount();

            for (int i = 0; i < site_count; i++)
            {
                Item itmSite = itmSites.getItemByIndex(i);

                switch (cfg.in_sort)
                {
                    case "allocate08"://打到8強換組
                        UpdateMRTreeNo_08(cfg, itmSite);
                        break;

                    case "allocate16"://打到16強換組
                        UpdateMRTreeNo_16(cfg, itmSite);
                        break;
                }

                if (cfg.is_challenge)
                {
                    //挑戰賽
                    UpdateCTreeNo(cfg, itmSite);
                }
            }
        }

        //打到8強換組
        private void UpdateMRTreeNo_08(TConfig cfg, Item itmSite)
        {
            string site_id = itmSite.getProperty("id", "");

            List<Item> list = new List<Item>();

            //勝部 8 強以前
            AppendList(list, GetSiteEvents(cfg, EventFilterEnum.main_pre_semi, site_id));
            //循環賽第 1 回合
            AppendList(list, GetSiteRobinEvents(cfg, site_id, round: "1"));
            //勝部 4 強
            AppendList(list, GetSiteEvents(cfg, EventFilterEnum.main_semi, site_id));
            //循環賽第 2 回合
            AppendList(list, GetSiteRobinEvents(cfg, site_id, round: "2"));

            //敗部 128 強
            AppendList(list, GetSiteEvents(cfg, EventFilterEnum.rpc_128, site_id));
            //敗部 64 強
            AppendList(list, GetSiteEvents(cfg, EventFilterEnum.rpc_64, site_id));
            //敗部 32 強
            AppendList(list, GetSiteEvents(cfg, EventFilterEnum.rpc_32, site_id));
            //敗部 16 強
            AppendList(list, GetSiteEvents(cfg, EventFilterEnum.rpc_16, site_id));
            //敗部 8 強
            AppendList(list, GetSiteEvents(cfg, EventFilterEnum.rpc_8, site_id));
            //循環賽第 3 回合
            AppendList(list, GetSiteRobinEvents(cfg, site_id, round: "3"));
            //循環賽第 4 回合
            AppendList(list, GetSiteRobinEvents(cfg, site_id, round: "4"));

            if (cfg.is_challenge)
            {
                //七八名
                AppendList(list, GetSiteEvents(cfg, EventFilterEnum.rank78, site_id));
                //循環賽第 5 回合
                AppendList(list, GetSiteRobinEvents(cfg, site_id, round: "5"));
                //敗部 4 強(銅牌戰)
                AppendList(list, GetSiteEvents(cfg, EventFilterEnum.rpc_4, site_id));
                //三四名
                AppendList(list, GetSiteEvents34(cfg, EventFilterEnum.rank34, site_id));
                //五六名(如果出賽人數 = 7，等三四名打完)
                AppendList(list, GetSiteEvents(cfg, EventFilterEnum.rank56, site_id));
                //勝部決賽(金牌戰)
                AppendList(list, GetSiteEvents(cfg, EventFilterEnum.main_final, site_id));

                //此處不排後續挑戰賽與三戰兩勝
            }
            else
            {
                //七八名
                AppendList(list, GetSiteEvents(cfg, EventFilterEnum.rank78, site_id));
                //循環賽第 5 回合
                AppendList(list, GetSiteRobinEvents(cfg, site_id, round: "5"));
                //敗部 4 強(非銅牌戰，如果人數 = 5)
                AppendList(list, GetSiteEvents(cfg, EventFilterEnum.rpc_4_fake, site_id));
                //三戰兩勝-第 1 場
                AppendList(list, GetSiteRobinEvents(cfg, site_id, "11"));
                //分組交叉 3 人 - 三四名
                AppendList(list, GetSiteRobinEvents(cfg, site_id, round: "14"));
                //敗部 4 強(真銅牌戰，如果人數 <> 5)
                AppendList(list, GetSiteEvents(cfg, EventFilterEnum.rpc_4_true, site_id));
                //三四名(如果人數 = 5)
                AppendList(list, GetSiteEvents(cfg, EventFilterEnum.rpc_final, site_id));
                //五六名(如果人數 = 7，等三四名打完)
                AppendList(list, GetSiteEvents(cfg, EventFilterEnum.rank56, site_id));
                //三戰兩勝-第 2 場
                AppendList(list, GetSiteRobinEvents(cfg, site_id, "12"));
                //分組交叉 3 人 - 決賽
                AppendList(list, GetSiteRobinEvents(cfg, site_id, round: "15"));
                //勝部決賽(金牌戰)
                AppendList(list, GetSiteEvents(cfg, EventFilterEnum.main_final, site_id));
                //三戰兩勝-第 3 場
                AppendList(list, GetSiteRobinEvents(cfg, site_id, "13"));
            }


            //全部重編
            ResetTreeNo(cfg, list);
            //單淘不分預賽決賽
            ResetPeriod(cfg);
            //子場次全部重編
            ResetSubTreeNo(cfg);
            //子場次量級重設
            ResetSubSects(cfg);
        }

        //打到16強換組
        private void UpdateMRTreeNo_16(TConfig cfg, Item itmSite)
        {
            string site_id = itmSite.getProperty("id", "");

            List<Item> list = new List<Item>();

            //勝部 16 強以前(含16強)
            AppendList(list, GetSiteEvents(cfg, EventFilterEnum.main_pre_quarterfinals, site_id));
            //勝部 8 強
            AppendList(list, GetSiteEvents(cfg, EventFilterEnum.main_r008, site_id));
            //循環賽第 1 回合
            AppendList(list, GetSiteRobinEvents(cfg, site_id, round: "1"));
            //勝部 4 強
            AppendList(list, GetSiteEvents(cfg, EventFilterEnum.main_semi, site_id));
            //循環賽第 2 回合
            AppendList(list, GetSiteRobinEvents(cfg, site_id, round: "2"));

            //敗部 128 強
            AppendList(list, GetSiteEvents(cfg, EventFilterEnum.rpc_128, site_id));
            //敗部 64 強
            AppendList(list, GetSiteEvents(cfg, EventFilterEnum.rpc_64, site_id));
            //敗部 32 強
            AppendList(list, GetSiteEvents(cfg, EventFilterEnum.rpc_32, site_id));
            //敗部 16 強
            AppendList(list, GetSiteEvents(cfg, EventFilterEnum.rpc_16, site_id));
            //敗部 8 強
            AppendList(list, GetSiteEvents(cfg, EventFilterEnum.rpc_8, site_id));
            //循環賽第 3 回合
            AppendList(list, GetSiteRobinEvents(cfg, site_id, round: "3"));
            //循環賽第 4 回合
            AppendList(list, GetSiteRobinEvents(cfg, site_id, round: "4"));

            if (cfg.is_challenge)
            {
                //七八名
                AppendList(list, GetSiteEvents(cfg, EventFilterEnum.rank78, site_id));
                //循環賽第 5 回合
                AppendList(list, GetSiteRobinEvents(cfg, site_id, round: "5"));
                //敗部 4 強(銅牌戰)
                AppendList(list, GetSiteEvents(cfg, EventFilterEnum.rpc_4, site_id));
                //三四名
                AppendList(list, GetSiteEvents34(cfg, EventFilterEnum.rank34, site_id));
                //五六名(如果出賽人數 = 7，等三四名打完)
                AppendList(list, GetSiteEvents(cfg, EventFilterEnum.rank56, site_id));
                //勝部決賽(金牌戰)
                AppendList(list, GetSiteEvents(cfg, EventFilterEnum.main_final, site_id));

                //此處不排後續挑戰賽與三戰兩勝
            }
            else
            {
                //七八名
                AppendList(list, GetSiteEvents(cfg, EventFilterEnum.rank78, site_id));
                //循環賽第 5 回合
                AppendList(list, GetSiteRobinEvents(cfg, site_id, round: "5"));
                //敗部 4 強(非銅牌戰，如果人數 = 5)
                AppendList(list, GetSiteEvents(cfg, EventFilterEnum.rpc_4_fake, site_id));
                //三戰兩勝-第 1 場
                AppendList(list, GetSiteRobinEvents(cfg, site_id, "11"));
                //敗部 4 強(真銅牌戰，如果人數 <> 5)
                AppendList(list, GetSiteEvents(cfg, EventFilterEnum.rpc_4_true, site_id));
                //三四名(如果人數 = 5)
                AppendList(list, GetSiteEvents(cfg, EventFilterEnum.rpc_final, site_id));
                //五六名(如果人數 = 7，等三四名打完)
                AppendList(list, GetSiteEvents(cfg, EventFilterEnum.rank56, site_id));
                //三戰兩勝-第 2 場
                AppendList(list, GetSiteRobinEvents(cfg, site_id, "12"));
                //勝部決賽(金牌戰)
                AppendList(list, GetSiteEvents(cfg, EventFilterEnum.main_final, site_id));
                //三戰兩勝-第 3 場
                AppendList(list, GetSiteRobinEvents(cfg, site_id, "13"));
            }


            //全部重編
            ResetTreeNo(cfg, list);
            //單淘不分預賽決賽
            ResetPeriod(cfg);
            //子場次全部重編
            ResetSubTreeNo(cfg);
            //子場次量級重設
            ResetSubSects(cfg);
        }

        //全部重編
        private void ResetTreeNo(TConfig cfg, List<Item> list)
        {
            for (int i = 0; i < list.Count; i++)
            {
                int no = i + 1;

                Item item = list[i];
                string event_id = item.getProperty("event_id", "");
                string in_tree_no = no.ToString();

                string sql = "UPDATE IN_MEETING_PEVENT SET"
                    + " in_site_allocate = '1'"
                    + " , in_tree_no = '" + in_tree_no + "'"
                    + " , in_tree_state = '1'"
                    + " WHERE id = '" + event_id + "'";

                Item itmSQL = cfg.inn.applySQL(sql);

                if (itmSQL.isError()) throw new Exception("error");
            }
        }

        //子場次量級重設
        private void ResetSubSects(TConfig cfg)
        {
            string sql = "";
            Item itmSQL = null;

            sql = @"
                UPDATE t1 SET
                	t1.in_sub_sect = t2.in_name
                FROM
                	IN_MEETING_PEVENT t1
                INNER JOIN
                	IN_MEETING_PSECT t2
                	ON t2.IN_PROGRAM = t1.source_id
                	AND t1.in_tree_name = 'sub'
                	AND t2.in_sub_id = t1.in_sub_id
                WHERE
                	t1.in_meeting = '{#meeting_id}'
                	AND t1.in_date_key = '{#in_date_key}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_date_key}", cfg.in_date_key);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            itmSQL = cfg.inn.applySQL(sql);
        }

        //單淘不分預賽決賽
        private void ResetPeriod(TConfig cfg)
        {
            string mt_battle_type = cfg.itmMeeting.getProperty("in_battle_type", "");
            if (mt_battle_type != "TopTwo")
            {
                return;
            }

            string sql = "";
            Item itmSQL = null;

            sql = @"
                UPDATE IN_MEETING_PEVENT SET
	                in_period = 1
                WHERE
	                in_meeting = '{#meeting_id}'
	                AND in_date_key = '{#in_date_key}'
	                AND ISNULL(in_period, 0) <> 1
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_date_key}", cfg.in_date_key);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            itmSQL = cfg.inn.applySQL(sql);
        }

        //子場次全部重編
        private void ResetSubTreeNo(TConfig cfg)
        {
            string sql = "";
            Item itmSQL = null;

            sql = @"
                UPDATE t1 SET
	                t1.in_tree_no = t2.in_tree_no * 100 + t1.in_sub_id
	                , t1.in_period = t2.in_period
	                , t1.in_medal = t2.in_medal
                FROM 
	                IN_MEETING_PEVENT t1 WITH(NOLOCK) 
                INNER JOIN
	                IN_MEETING_PEVENT t2 WITH(NOLOCK) 
	                ON t2.id = t1.in_parent
                WHERE
	                t1.in_meeting = '{#meeting_id}'
	                AND t1.in_tree_name = 'sub'
	                AND t1.in_date_key = '{#in_date_key}'
	                AND ISNULL(t2.in_tree_no, 0) <> 0
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_date_key}", cfg.in_date_key);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            cfg.inn.applySQL(sql);
        }

        //更新東西側註記
        private void RunUpdateWestAndEast(TConfig cfg)
        {
            UpdateWestAndEast_STEP_01(cfg);
            UpdateWestAndEast_STEP_11(cfg);
            UpdateWestAndEast_STEP_12(cfg);
            UpdateWestAndEast_STEP_21(cfg);
            UpdateWestAndEast_STEP_22(cfg);
            UpdateWestAndEast_STEP_31(cfg);

            UpdateWestAndEast_STEP_41(cfg, "1", "w");
            UpdateWestAndEast_STEP_41(cfg, "2", "e");
        }

        private void UpdateWestAndEast_STEP_41(TConfig cfg, string in_code, string in_site_we)
        {
            string sql = @"
                SELECT
	                t1.id
	                , t1.in_code
                FROM
	                IN_MEETING_SITE t1 WITH(NOLOCK)
                WHERE
	                t1.in_meeting = '{#meeting_id}'
	                AND t1.in_code = {#in_code}
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_code}", in_code);

            var itmSite = cfg.inn.applySQL(sql);
            var site_id = itmSite.getProperty("id", "");
            var site_code = itmSite.getProperty("in_code", "");

             sql = @"
                UPDATE t2 SET
	                t2.in_site = '{#site_id}'
	                , t2.in_site_code = '{#site_code}'
                FROM
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PEVENT t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                WHERE
	                t1.in_meeting = '{#meeting_id}'
	                AND t1.in_fight_day = '{#in_date_key}'
	                AND ISNULL(t1.in_site_we, '') <> ''
	                AND ISNULL(t2.in_site_we, '') = '{#in_site_we}'
             ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_date_key}", cfg.in_date_key)
                .Replace("{#in_site_we}", in_site_we)
                .Replace("{#site_id}", site_id)
                .Replace("{#site_code}", site_code);

            cfg.inn.applySQL(sql);
        }

        //更新東西側註記 - 清空東西側
        private void UpdateWestAndEast_STEP_01(TConfig cfg)
        {
            string sql = @"
                UPDATE t2 SET
	                t2.in_site_we = NULL
                FROM
	                IN_MEETING_PEVENT t2 WITH(NOLOCK)
                WHERE 
	                t2.in_meeting = '{#meeting_id}'
	                AND t2.in_date_key = '{#in_date_key}'
	                AND t2.in_tree_name IN ('main', 'repechage')
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_date_key}", cfg.in_date_key);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            cfg.inn.applySQL(sql);
        }

        //更新東西側註記 - 西側 (MAIN)
        private void UpdateWestAndEast_STEP_11(TConfig cfg)
        {
            string sql = @"
                UPDATE t2 SET
	                t2.in_site_we = 'w'
                FROM
	                IN_MEETING_PEVENT t2 WITH(NOLOCK)
                WHERE 
	                t2.in_meeting = '{#meeting_id}'
	                AND t2.in_date_key = '{#in_date_key}'
	                AND t2.in_tree_name = 'main'
	                AND t2.in_round_id <= (t2.in_round_code / 4)
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_date_key}", cfg.in_date_key);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            cfg.inn.applySQL(sql);
        }

        //更新東西側註記 - 東側 (MAIN)
        private void UpdateWestAndEast_STEP_12(TConfig cfg)
        {
            string sql = @"
                UPDATE t2 SET
	                t2.in_site_we = 'e'
                FROM
	                IN_MEETING_PEVENT t2 WITH(NOLOCK)
                WHERE 
	                t2.in_meeting = '{#meeting_id}'
	                AND t2.in_date_key = '{#in_date_key}'
	                AND t2.in_tree_name = 'main'
	                AND t2.in_round_id > (t2.in_round_code / 4)
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_date_key}", cfg.in_date_key);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            cfg.inn.applySQL(sql);
        }

        //更新東西側註記 - 西側 (RPC)
        private void UpdateWestAndEast_STEP_21(TConfig cfg)
        {
            string sql = @"
                UPDATE t2 SET
	                t2.in_site_we = 'w'
                FROM
	                IN_MEETING_PEVENT t2 WITH(NOLOCK)
                WHERE 
	                t2.in_meeting = '{#meeting_id}'
	                AND t2.in_date_key = '{#in_date_key}'
	                AND t2.in_tree_name = 'repechage'
	                AND t2.in_fight_id IN ('R032-01', 'R032-02', 'R016-01', 'R016-02', 'R008-01', 'R004-01')
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_date_key}", cfg.in_date_key);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            cfg.inn.applySQL(sql);
        }

        //更新東西側註記 - 東側 (RPC)
        private void UpdateWestAndEast_STEP_22(TConfig cfg)
        {
            string sql = @"
                UPDATE t2 SET
	                t2.in_site_we = 'e'
                FROM
	                IN_MEETING_PEVENT t2 WITH(NOLOCK)
                WHERE 
	                t2.in_meeting = '{#meeting_id}'
	                AND t2.in_date_key = '{#in_date_key}'
	                AND t2.in_tree_name = 'repechage'
	                AND t2.in_fight_id IN ('R032-03', 'R032-04', 'R016-03', 'R016-04', 'R008-02', 'R004-02')
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_date_key}", cfg.in_date_key);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            cfg.inn.applySQL(sql);
        }

        //更新東西側註記 - 決賽
        private void UpdateWestAndEast_STEP_31(TConfig cfg)
        {
            string sql = @"
                UPDATE t2 SET
	                t2.in_site_we = ''
                FROM
	                IN_MEETING_PEVENT t2 WITH(NOLOCK)
                WHERE 
	                t2.in_meeting = '{#meeting_id}'
	                AND t2.in_date_key = '{#in_date_key}'
	                AND t2.in_fight_id IN ('M002-01', 'R002-01')
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_date_key}", cfg.in_date_key);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            cfg.inn.applySQL(sql);
        }

        //批次更新場地與比賽日期
        private void UpdateDateAndSite(TConfig cfg)
        {
            string sql = @"
                 UPDATE t1 SET
             	    t1.in_date_key = t3.in_date_key
             	    , t1.in_site = t4.id
             	    , t1.in_site_code = t4.in_code
             	    , t1.in_tree_state = 0
                 FROM
             	    IN_MEETING_PEVENT t1
                 INNER JOIN
                    IN_MEETING_PROGRAM t2
                    ON t2.id = t1.source_id
                 INNER JOIN
             	    IN_MEETING_ALLOCATION t3
             	    ON t3.in_program = t2.id
                 INNER JOIN
             	    IN_MEETING_SITE t4
             	    ON t4.id = t3.in_site
                 WHERE
                    t2.in_meeting = '{#meeting_id}'
             	    AND t3.in_date_key = '{#in_date_key}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_date_key}", cfg.in_date_key);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            cfg.inn.applySQL(sql);
        }

        //更新場地序號(挑戰賽)
        private void UpdateCTreeNo(TConfig cfg, Item itmSite)
        {
            string site_id = itmSite.getProperty("id", "");

            List<Item> list = new List<Item>();

            //三戰兩勝-第 1 場
            AppendList(list, GetSiteRobinEvents(cfg, site_id, "11"));
            //挑戰賽 R1
            AppendList(list, GetSiteEvents(cfg, EventFilterEnum.clg_a1, site_id));
            //三戰兩勝-第 2 場
            AppendList(list, GetSiteRobinEvents(cfg, site_id, "12"));
            //挑戰賽 R2
            AppendList(list, GetSiteEvents(cfg, EventFilterEnum.clg_a2, site_id));
            //三戰兩勝-第 2 場
            AppendList(list, GetSiteRobinEvents(cfg, site_id, "13"));
            //挑戰賽 R3
            AppendList(list, GetSiteEvents(cfg, EventFilterEnum.clg_b1, site_id));

            //盟主賽 R1
            AppendList(list, GetSiteEventsByTid(cfg, site_id, "KA01"));
            //盟主賽 R2
            AppendList(list, GetSiteEventsByTid(cfg, site_id, "KB01"));

            string sql = @"
                SELECT 
                	MAX(in_tree_no) AS 'max_tree_no'
                FROM 
                	IN_MEETING_PEVENT WITH(NOLOCK)
                WHERE 
                	in_meeting = '{#meeting_id}' 
                	AND in_site = '{#site_id}'
                	AND in_date_key = '{#in_date_key}'
	                AND in_tree_id NOT IN ('CA01', 'CA02', 'CB01', 'rank56', 'KA01', 'KB01')
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#site_id}", site_id)
                .Replace("{#in_date_key}", cfg.in_date_key);

            int max_tree_no = 0;
            Item itmTreeNo = cfg.inn.applySQL(sql);
            if (!itmTreeNo.isError() && itmTreeNo.getResult() != "")
            {
                string tree_no = itmTreeNo.getProperty("max_tree_no", "0");
                max_tree_no = GetIntVal(tree_no);
            }

            for (int i = 0; i < list.Count; i++)
            {
                int no = i + 1 + max_tree_no;

                Item item = list[i];
                string event_id = item.getProperty("event_id", "");
                string in_site_code = item.getProperty("site_code", "");
                string in_site_no = no.ToString();
                string in_site_id = in_site_code + in_site_no.PadLeft(3, '3');

                sql = "UPDATE IN_MEETING_PEVENT SET"
                    + " in_site_allocate = '1'"
                    + " , in_site_no = '" + in_site_no + "'"
                    + " , in_site_id = '" + in_site_id + "'"
                    + " , in_tree_no = '" + in_site_no + "'"
                    + " , in_tree_state = '1'"
                    + " WHERE id = '" + event_id + "'";

                Item itmSQL = cfg.inn.applySQL(sql);

                if (itmSQL.isError()) throw new Exception("error");
            }
        }

        private string GetDayBattleType(TConfig cfg, string mt_battle_type)
        {
            string result = "";

            string sql = @"
                 SELECT
					DISTINCT t1.in_battle_type AS 'battle_type' 
                 FROM
                    IN_MEETING_PROGRAM t1
                 INNER JOIN
             	    IN_MEETING_ALLOCATION t2
             	    ON t2.in_program = t1.id
                 WHERE
                    t1.in_meeting = '{#meeting_id}'
             	    AND t2.in_date_key = '{#in_date}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_date}", cfg.in_date_key);

            Item items = cfg.inn.applySQL(sql);
            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string battle_type = item.getProperty("battle_type", "");
                if (battle_type.Contains("Robin"))
                {
                    continue;
                }
                else
                {
                    result = battle_type;
                }
            }

            if (result == "")
            {
                result = mt_battle_type;
            }

            return result;
        }

        private void AppendList(List<Item> list, Item items)
        {
            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                list.Add(item);
            }
        }

        private enum EventFilterEnum
        {
            /// <summary>
            /// 勝部-16強賽以前
            /// </summary>
            main_pre_quarterfinals = 5,
            /// <summary>
            /// 勝部-8強賽以前
            /// </summary>
            main_pre_semi = 10,
            /// <summary>
            /// 勝部-準決賽(4強)
            /// </summary>
            main_semi = 20,
            /// <summary>
            /// 勝部-準決賽以前
            /// </summary>
            main_semi_all = 21,
            /// <summary>
            /// 勝部-決賽 (金牌戰)
            /// </summary>
            main_final = 30,

            /// <summary>
            /// 敗部-128強
            /// </summary>
            rpc_128 = 105,
            /// <summary>
            /// 敗部-64強
            /// </summary>
            rpc_64 = 110,
            /// <summary>
            /// 敗部-32強
            /// </summary>
            rpc_32 = 120,
            /// <summary>
            /// 敗部-16強
            /// </summary>
            rpc_16 = 130,
            /// <summary>
            /// 敗部-8強
            /// </summary>
            rpc_8 = 140,

            /// <summary>
            /// 敗部-4強 (銅牌戰)
            /// </summary>
            rpc_4 = 150,

            /// <summary>
            /// 敗部-4強 (銅牌戰-West)
            /// </summary>
            rpc_4W = 151,

            /// <summary>
            /// 敗部-4強 (銅牌戰-East)
            /// </summary>
            rpc_4E = 152,

            /// <summary>
            /// 敗部-4強以前
            /// </summary>
            rpc_4_all = 153,

            /// <summary>
            /// 敗部-4強 (人數不足，非銅牌戰)
            /// </summary>
            rpc_4_fake = 155,

            /// <summary>
            /// 敗部-4強 (真銅牌戰)
            /// </summary>
            rpc_4_true = 156,

            /// <summary>
            /// 敗部-決賽 (三四名戰)
            /// </summary>
            rpc_final = 160,
            /// <summary>
            /// 復活賽-第1場 (3挑戰2)
            /// </summary>
            clg_a1 = 310,
            /// <summary>
            /// 復活賽-第2場 (2挑戰1)
            /// </summary>
            clg_a2 = 320,
            /// <summary>
            /// 復活賽-第3場 (2挑戰1) (原第1名落敗)
            /// </summary>
            clg_b1 = 330,
            /// <summary>
            /// 循環賽
            /// </summary>
            round = 510,
            /// <summary>
            /// 三四名 (只有四人的三四名戰)
            /// </summary>
            rank34 = 540,
            /// <summary>
            /// 五六名
            /// </summary>
            rank56 = 560,
            /// <summary>
            /// 七八名
            /// </summary>
            rank78 = 580,
            /// <summary>
            /// 勝部-8強
            /// </summary>
            main_r008 = 1008,
            /// <summary>
            /// 勝部-16強
            /// </summary>
            main_r016 = 1016,
            /// <summary>
            /// 勝部-32強
            /// </summary>
            main_r032 = 1032,
            /// <summary>
            /// 勝部-64強
            /// </summary>
            main_r064 = 1064,
            /// <summary>
            /// 勝部-128強
            /// </summary>
            main_r128 = 1128,
            /// <summary>
            /// 盟主賽
            /// </summary>
            ka01 = 2100,
            /// <summary>
            /// 盟主賽加賽
            /// </summary>
            kb01 = 2200,
        }

        private Item GetSiteEventsSemi(TConfig cfg, EventFilterEnum filter, string site_id)
        {
            string in_battle_type = "";
            string in_tree_name = "";
            string round_code_filter = "";
            in_battle_type = cfg.day_battle_type;

            string sql = @"
                SELECT
	                t2.id				AS 'site_id'
	                , t2.in_code		AS 'site_code'
	                , t2.in_name		AS 'site_name'
	                , t1.in_date_key
                    , t3.id             AS 'program_id'
	                , t3.in_name3       AS 'program_name'
	                , t3.in_battle_type
	                , t4.id             AS 'event_id'
	                , t4.in_round_code
	                , t4.in_tree_id
                FROM 
	                IN_MEETING_ALLOCATION t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_SITE t2 WITH(NOLOCK)
	                ON t2.id = t1.in_site
                INNER JOIN
	                IN_MEETING_PROGRAM t3 WITH(NOLOCK)
	                ON t3.id = t1.in_program
                INNER JOIN
	                IN_MEETING_PEVENT t4 WITH(NOLOCK)
	                ON t4.source_id = t3.id
                WHERE
	                t1.in_meeting = '{#meeting_id}'
	                AND t1.in_date_key = '{#in_date_key}'
	                AND t1.in_site = '{#site_id}'
	                AND t3.in_battle_type = '{#in_battle_type}'
                    AND ISNULL(t4.in_tree_no, 0) > 0
	                AND (
                        (t4.in_tree_name = N'main' AND t4.in_round_code >= 4)
                        OR
                        (t4.in_tree_name = N'repechage' AND t4.in_round_code > 4)
                    )
                ORDER BY
	                t2.in_code
	                , t1.created_on
	                , t4.in_tree_sort
	                , t4.in_round_code DESC
	                , t4.in_tree_sno
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_date_key}", cfg.in_date_key)
                .Replace("{#site_id}", site_id)
                .Replace("{#in_battle_type}", in_battle_type)
                .Replace("{#in_tree_name}", in_tree_name)
                .Replace("{#round_code_filter}", round_code_filter);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        private Item GetSiteRobinEvents(TConfig cfg, string site_id, string round)
        {
            //五人: 10場、四人: 6場、三人: 3場
            List<string> ors = new List<string>();

            switch (round)
            {
                case "1":
                    ors.Add("t3.in_team_count = 5 AND ISNULL(t4.in_tree_sno, 0) IN (1, 2)");
                    ors.Add("t3.in_team_count = 4 AND ISNULL(t4.in_tree_sno, 0) IN (1, 2)");
                    ors.Add("t3.in_team_count = 3 AND ISNULL(t4.in_tree_sno, 0) IN (1)");
                    break;

                case "2":
                    ors.Add("t3.in_team_count = 5 AND ISNULL(t4.in_tree_sno, 0) IN (3, 4)");
                    ors.Add("t3.in_team_count = 4 AND ISNULL(t4.in_tree_sno, 0) IN (3, 4)");
                    ors.Add("t3.in_team_count = 3 AND ISNULL(t4.in_tree_sno, 0) IN (2)");
                    break;

                case "3":
                    ors.Add("t3.in_team_count = 5 AND ISNULL(t4.in_tree_sno, 0) IN (5, 6)");
                    ors.Add("t3.in_team_count = 4 AND ISNULL(t4.in_tree_sno, 0) IN (5)");
                    ors.Add("t3.in_team_count = 3 AND ISNULL(t4.in_tree_sno, 0) IN (3)");
                    break;

                case "4":
                    ors.Add("t3.in_team_count = 5 AND ISNULL(t4.in_tree_sno, 0) IN (7, 8)");
                    ors.Add("t3.in_team_count = 4 AND ISNULL(t4.in_tree_sno, 0) IN (6)");
                    break;

                case "5":
                    ors.Add("t3.in_team_count = 5 AND ISNULL(t4.in_tree_sno, 0) IN (9, 10)");
                    break;

                case "11":
                    ors.Add("t3.in_team_count = 2 AND in_round = 1");
                    break;

                case "12":
                    ors.Add("t3.in_team_count = 2 AND in_round = 2");
                    break;

                case "13":
                    ors.Add("t3.in_team_count = 2 AND in_round = 3");
                    break;

                case "14":
                    ors.Add("t4.in_tree_id = 'RNK34'");
                    break;

                case "15":
                    ors.Add("t4.in_tree_id = 'RNK12'");
                    break;
            }

            string sql = @"
                SELECT
	                t2.id				AS 'site_id'
	                , t2.in_code		AS 'site_code'
	                , t2.in_name		AS 'site_name'
	                , t1.in_date_key
                    , t3.id             AS 'program_id'
	                , t3.in_name3       AS 'program_name'
	                , t3.in_battle_type
	                , t4.id             AS 'event_id'
	                , t4.in_round_code
	                , t4.in_tree_id
                FROM 
	                IN_MEETING_ALLOCATION t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_SITE t2 WITH(NOLOCK)
	                ON t2.id = t1.in_site
                INNER JOIN
	                IN_MEETING_PROGRAM t3 WITH(NOLOCK)
	                ON t3.id = t1.in_program
                INNER JOIN
	                IN_MEETING_PEVENT t4 WITH(NOLOCK)
	                ON t4.source_id = t3.id
                WHERE
	                t1.in_meeting = '{#meeting_id}'
	                AND t1.in_date_key = '{#in_date_key}'
	                AND t1.in_site = '{#site_id}'
	                AND t3.in_battle_type IN ('SingleRoundRobin', 'Cross')
	                AND t4.in_tree_name IN ('main', 'cross')
	                AND (
                        {#ors}
                    )
                ORDER BY
	                t2.in_code
	                , t1.created_on
	                , t4.in_round_code DESC
	                , t4.in_tree_sno
            ";

            var conds = ors.Select(x => "( " + x + " )");

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_date_key}", cfg.in_date_key)
                .Replace("{#site_id}", site_id)
                .Replace("{#ors}", string.Join(" OR ", conds));

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        private Item GetSiteEventsST(TConfig cfg, string site_id)
        {
            string in_battle_type = "";
            string in_tree_name = "";

            string sql = @"
                SELECT
	                t2.id				AS 'site_id'
	                , t2.in_code		AS 'site_code'
	                , t2.in_name		AS 'site_name'
	                , t1.in_date_key
                    , t3.id             AS 'program_id'
	                , t3.in_name3       AS 'program_name'
	                , t3.in_battle_type
	                , t4.id             AS 'event_id'
	                , t4.in_round_code
	                , t4.in_tree_id
                FROM 
	                IN_MEETING_ALLOCATION t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_SITE t2 WITH(NOLOCK)
	                ON t2.id = t1.in_site
                INNER JOIN
	                IN_MEETING_PROGRAM t3 WITH(NOLOCK)
	                ON t3.id = t1.in_program
                INNER JOIN
	                IN_MEETING_PEVENT t4 WITH(NOLOCK)
	                ON t4.source_id = t3.id
                WHERE
	                t1.in_meeting = '{#meeting_id}'
	                AND t1.in_date_key = '{#in_date_key}'
	                AND t1.in_site = '{#site_id}'
	                AND ISNULL(t4.in_tree_no, 0) > 0
                ORDER BY
	                t2.in_code
	                , t1.created_on
	                , t4.in_round_code DESC
	                , t4.in_tree_sno

            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_date_key}", cfg.in_date_key)
                .Replace("{#site_id}", site_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        private Item GetSiteEvents(TConfig cfg, EventFilterEnum filter, string site_id)
        {
            string in_battle_type = "";
            string in_tree_name = "";
            string round_code_filter = "";

            switch (filter)
            {
                case EventFilterEnum.main_pre_quarterfinals:
                    in_battle_type = cfg.day_battle_type;
                    in_tree_name = "main";
                    round_code_filter = "AND t4.in_round_code > 8 AND ISNULL(t4.in_tree_no, 0) > 0";
                    break;

                case EventFilterEnum.main_pre_semi:
                    in_battle_type = cfg.day_battle_type;
                    in_tree_name = "main";
                    round_code_filter = "AND t4.in_round_code > 4 AND ISNULL(t4.in_tree_no, 0) > 0";
                    break;

                case EventFilterEnum.main_r128:
                    in_battle_type = cfg.day_battle_type;
                    in_tree_name = "main";
                    round_code_filter = "AND t4.in_round_code = 128 AND ISNULL(t4.in_tree_no, 0) > 0";
                    break;

                case EventFilterEnum.main_r064:
                    in_battle_type = cfg.day_battle_type;
                    in_tree_name = "main";
                    round_code_filter = "AND t4.in_round_code = 64 AND ISNULL(t4.in_tree_no, 0) > 0";
                    break;

                case EventFilterEnum.main_r032:
                    in_battle_type = cfg.day_battle_type;
                    in_tree_name = "main";
                    round_code_filter = "AND t4.in_round_code = 32 AND ISNULL(t4.in_tree_no, 0) > 0";
                    break;

                case EventFilterEnum.main_r016:
                    in_battle_type = cfg.day_battle_type;
                    in_tree_name = "main";
                    round_code_filter = "AND t4.in_round_code = 16 AND ISNULL(t4.in_tree_no, 0) > 0";
                    break;

                case EventFilterEnum.main_r008:
                    in_battle_type = cfg.day_battle_type;
                    in_tree_name = "main";
                    round_code_filter = "AND t4.in_round_code = 8 AND ISNULL(t4.in_tree_no, 0) > 0";
                    break;

                case EventFilterEnum.main_semi:
                    in_battle_type = cfg.day_battle_type;
                    in_tree_name = "main";
                    round_code_filter = "AND t4.in_round_code = 4 AND ISNULL(t4.in_tree_no, 0) > 0";
                    break;

                case EventFilterEnum.main_semi_all:
                    in_battle_type = cfg.day_battle_type;
                    in_tree_name = "main";
                    round_code_filter = "AND t4.in_round_code >= 4 AND ISNULL(t4.in_tree_no, 0) > 0";
                    break;

                case EventFilterEnum.main_final:
                    in_battle_type = cfg.day_battle_type;
                    in_tree_name = "main";
                    round_code_filter = "AND t4.in_round_code = 2 AND ISNULL(t4.in_tree_no, 0) > 0";
                    break;

                case EventFilterEnum.rpc_128:
                    in_battle_type = cfg.day_battle_type;
                    in_tree_name = "repechage";
                    round_code_filter = "AND t4.in_round_code = 128 AND ISNULL(t4.in_tree_no, 0) > 0";
                    break;

                case EventFilterEnum.rpc_64:
                    in_battle_type = cfg.day_battle_type;
                    in_tree_name = "repechage";
                    round_code_filter = "AND t4.in_round_code = 64 AND ISNULL(t4.in_tree_no, 0) > 0";
                    break;

                case EventFilterEnum.rpc_32:
                    in_battle_type = cfg.day_battle_type;
                    in_tree_name = "repechage";
                    round_code_filter = "AND t4.in_round_code = 32 AND ISNULL(t4.in_tree_no, 0) > 0";
                    break;

                case EventFilterEnum.rpc_16:
                    in_battle_type = cfg.day_battle_type;
                    in_tree_name = "repechage";
                    round_code_filter = "AND t4.in_round_code = 16 AND ISNULL(t4.in_tree_no, 0) > 0";
                    break;

                case EventFilterEnum.rpc_8:
                    in_battle_type = cfg.day_battle_type;
                    in_tree_name = "repechage";
                    round_code_filter = "AND t4.in_round_code = 8 AND ISNULL(t4.in_tree_no, 0) > 0";
                    break;

                case EventFilterEnum.rpc_4_all:
                    in_battle_type = cfg.day_battle_type;
                    in_tree_name = "repechage";
                    round_code_filter = "AND t4.in_round_code > 4 AND ISNULL(t4.in_tree_no, 0) > 0";
                    break;

                case EventFilterEnum.rpc_4:
                    in_battle_type = cfg.day_battle_type;
                    in_tree_name = "repechage";
                    round_code_filter = "AND t4.in_round_code = 4";
                    break;

                case EventFilterEnum.rpc_4_fake:
                    in_battle_type = cfg.day_battle_type;
                    in_tree_name = "repechage";
                    round_code_filter = "AND ISNULL(t3.in_rank_count, 0) = 5 AND t4.in_round_code = 4";
                    break;

                case EventFilterEnum.rpc_4_true:
                    in_battle_type = cfg.day_battle_type;
                    in_tree_name = "repechage";
                    round_code_filter = "AND ISNULL(t3.in_rank_count, 0) <> 5 AND t4.in_round_code = 4";
                    break;

                case EventFilterEnum.rpc_final:
                    in_battle_type = cfg.day_battle_type;
                    in_tree_name = "repechage";
                    round_code_filter = "AND t4.in_round_code = 2 AND ISNULL(t4.in_tree_no, 0) > 0";
                    break;

                case EventFilterEnum.clg_a1:
                    in_battle_type = "Challenge";
                    in_tree_name = "challenge-a";
                    round_code_filter = "AND t4.in_tree_id = 'CA01'";
                    break;

                case EventFilterEnum.clg_a2:
                    in_battle_type = "Challenge";
                    in_tree_name = "challenge-a";
                    round_code_filter = "AND t4.in_tree_id = 'CA02'";
                    break;

                case EventFilterEnum.clg_b1:
                    in_battle_type = "Challenge";
                    in_tree_name = "challenge-b";
                    break;

                case EventFilterEnum.round:
                    in_battle_type = "SingleRoundRobin";
                    in_tree_name = "main";
                    break;

                case EventFilterEnum.rank34:
                    in_battle_type = cfg.day_battle_type;
                    in_tree_name = "rank34";
                    break;

                case EventFilterEnum.rank56: // 7 取 5
                    in_battle_type = cfg.day_battle_type;
                    in_tree_name = "rank56";
                    round_code_filter = "AND ISNULL(t3.in_rank_count, 0) = 7";
                    break;

                case EventFilterEnum.rank78: // 9 取 7
                    in_battle_type = cfg.day_battle_type;
                    in_tree_name = "rank78";
                    round_code_filter = "AND ISNULL(t3.in_rank_count, 0) = 9";
                    break;
            }

            string sql = @"
                SELECT
	                t4.in_date_key
                    , t3.id             AS 'program_id'
	                , t3.in_name3       AS 'program_name'
	                , t3.in_battle_type
	                , t4.id             AS 'event_id'
	                , t4.in_round_code
	                , t4.in_tree_id
	                , t2.id				AS 'site_id'
	                , t2.in_code		AS 'site_code'
	                , t2.in_name		AS 'site_name'
                FROM 
	                IN_MEETING_PEVENT t4 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PROGRAM t3 WITH(NOLOCK)
	                ON t3.id = t4.source_id
                INNER JOIN
	                IN_MEETING_SITE t2 WITH(NOLOCK)
	                ON t2.id = t4.in_site
                WHERE
	                t4.in_meeting = '{#meeting_id}'
	                AND t4.in_date_key = '{#in_date_key}'
	                AND t4.in_site = '{#site_id}'
	                AND t4.in_tree_name = N'{#in_tree_name}'
	                AND t3.in_battle_type = '{#in_battle_type}'
	                {#round_code_filter}
                ORDER BY
	                t2.in_code
	                , t3.in_site_mat3
	                , t4.in_round_code DESC
	                , t4.in_tree_sno
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_date_key}", cfg.in_date_key)
                .Replace("{#site_id}", site_id)
                .Replace("{#in_battle_type}", in_battle_type)
                .Replace("{#in_tree_name}", in_tree_name)
                .Replace("{#round_code_filter}", round_code_filter);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        private Item GetSiteEvents34(TConfig cfg, EventFilterEnum filter, string site_id)
        {
            string in_battle_type = cfg.day_battle_type;

            string sql = @"
				SELECT
	                t1.id             AS 'event_id'
	                , t1.in_tree_id
	                , t1.in_date_key
	                , t1.in_round_code
                    , t2.id             AS 'program_id'
	                , t2.in_name3       AS 'program_name'
	                , t2.in_battle_type
	                , t11.id			AS 'site_id'
	                , t11.in_code		AS 'site_code'
	                , t11.in_name		AS 'site_name'
                FROM
	                IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PROGRAM t2 WITH(NOLOCK)
	                ON t2.id = t1.source_id
                LEFT OUTER JOIN
	                IN_MEETING_SITE t11 WITH(NOLOCK)
	                ON t11.id = t1.in_site
                WHERE
	                t1.in_meeting = '{#meeting_id}'
	                AND t1.in_site = '{#site_id}'
	                AND t1.in_date_key = '{#in_date_key}'
	                AND t2.in_battle_type = '{#in_battle_type}'
	                AND 
					((t1.in_tree_name = 'repechage' AND t1.in_round_code = 2 AND ISNULL(t2.in_rank_count, 0) >= 4)
						OR
					(t1.in_tree_name = 'rank34'))
                ORDER BY
	                t2.in_sort_order
	                , t1.in_tree_sort
					, t1.in_tree_sno
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#site_id}", site_id)
                .Replace("{#in_date_key}", cfg.in_date_key)
                .Replace("{#in_battle_type}", in_battle_type);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        private Item GetSiteEventsByTid(TConfig cfg, string site_id, string tree_id)
        {
            string in_battle_type = cfg.day_battle_type;

            string sql = @"
				SELECT
	                t1.id             AS 'event_id'
	                , t1.in_tree_id
	                , t1.in_date_key
	                , t1.in_round_code
                    , t2.id             AS 'program_id'
	                , t2.in_name3       AS 'program_name'
	                , t2.in_battle_type
	                , t11.id			AS 'site_id'
	                , t11.in_code		AS 'site_code'
	                , t11.in_name		AS 'site_name'
                FROM
	                IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PROGRAM t2 WITH(NOLOCK)
	                ON t2.id = t1.source_id
                LEFT OUTER JOIN
	                IN_MEETING_SITE t11 WITH(NOLOCK)
	                ON t11.id = t1.in_site
                WHERE
	                t1.in_meeting = '{#meeting_id}'
	                AND t1.in_date_key = '{#in_date_key}'
	                AND t1.in_site = '{#site_id}'
	                AND t1.in_tree_id = '{#tree_id}'
                ORDER BY
	                t2.in_sort_order
	                , t1.in_tree_sort
					, t1.in_tree_sno
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_date_key}", cfg.in_date_key)
                .Replace("{#site_id}", site_id)
                .Replace("{#tree_id}", tree_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public Innovator inn { get; set; }

            public string meeting_id { get; set; }
            //public string site_id { get; set; }
            public string in_date_key { get; set; }
            public string in_sort { get; set; }

            public Item itmMeeting { get; set; }
            public string mt_battle_type { get; set; }
            public bool is_challenge { get; set; }
            public bool is_quarter { get; set; }
            public int robin_player { get; set; }

            public string day_battle_type { get; set; }
            /// <summary>
            /// 字型名稱
            /// </summary>
            public string FontName { get; set; }

            /// <summary>
            /// 字型大小
            /// </summary>
            public int FontSize { get; set; }

            /// <summary>
            /// 列起始位置
            /// </summary>
            public int RowStart { get; set; }

            /// <summary>
            /// 欄起始位置
            /// </summary>
            public int ColStart { get; set; }

            /// <summary>
            /// 當前列位置
            /// </summary>
            public int CurrentRow { get; set; }

            /// <summary>
            /// 賽事名稱
            /// </summary>
            public string Title { get; set; }
        }

        private class TSite
        {
            /// <summary>
            /// 鍵
            /// </summary>
            public string Key { get; set; }

            /// <summary>
            /// 系統編號
            /// </summary>
            public string Id { get; set; }

            /// <summary>
            /// 場地代號
            /// </summary>
            public string Code { get; set; }

            /// <summary>
            /// 場地名稱
            /// </summary>
            public string Name { get; set; }

        }

        private int GetIntVal(string value, int def = 0)
        {
            int result = def;
            int.TryParse(value, out result);
            return result;
        }

        private string GetDateTimeVal(string value, string format, int add_hours = 0)
        {
            DateTime result = DateTime.MinValue;
            DateTime.TryParse(value, out result);
            if (result != DateTime.MinValue)
            {
                if (format == "chinese")
                {
                    return ChineseDateTime(result.AddHours(add_hours));
                }
                else
                {
                    return result.AddHours(add_hours).ToString(format);
                }
            }
            else
            {
                return "";
            }
        }

        /// <summary>
        /// 轉換為民國年月日(星期)
        /// </summary>
        private string ChineseDateTime(DateTime value)
        {
            var y = value.Year - 1911;
            var m = value.Month.ToString().PadLeft(2, '0');
            var d = value.Day.ToString().PadLeft(2, '0');
            var w = "";

            switch (value.DayOfWeek)
            {
                case DayOfWeek.Monday:
                    w = "星期一";
                    break;
                case DayOfWeek.Tuesday:
                    w = "星期二";
                    break;
                case DayOfWeek.Wednesday:
                    w = "星期三";
                    break;
                case DayOfWeek.Thursday:
                    w = "星期四";
                    break;
                case DayOfWeek.Friday:
                    w = "星期五";
                    break;
                case DayOfWeek.Saturday:
                    w = "星期六";
                    break;
                case DayOfWeek.Sunday:
                    w = "星期日";
                    break;
                default:
                    break;
            }

            return y + "年" + m + "月" + d + "日" + "（" + w + "）";
        }
    }
}