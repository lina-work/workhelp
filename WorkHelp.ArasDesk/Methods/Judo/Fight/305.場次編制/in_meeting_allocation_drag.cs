﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Judo.Fight
{
    public class in_meeting_allocation_drag : Item
    {
        public in_meeting_allocation_drag(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 場次拖曳
                日誌: 
                    - 2022-12-23: 扣除盟主 (lina)
                    - 2022-05-19: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]" + "in_meeting_allocation_drag";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                in_fight_day = itmR.getProperty("in_fight_day", ""),
                site_code = itmR.getProperty("site_code", ""),
            };

            cfg.in_fight_day = cfg.in_fight_day.Replace("null", "");
            cfg.site_code = cfg.site_code.Replace("null", "");

            string sql = "";

            Item itmMeeting = cfg.inn.applySQL("SELECT in_title, in_battle_type FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            if (itmMeeting.isError() || itmMeeting.getResult() == "")
            {
                itmMeeting = cfg.inn.newItem();
            }

            itmR.setProperty("in_title", itmMeeting.getProperty("in_title", ""));

            var tomtomorrow = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd");
            var sameDay = false;
            sql = "SELECT DISTINCT in_fight_day FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE in_meeting = '" + cfg.meeting_id + "' ORDER BY in_fight_day";
            Item itmDays = cfg.inn.applySQL(sql);
            if (!itmDays.isError() && itmDays.getResult() != "")
            {
                var day_count = itmDays.getItemCount();

                for (int i = 0; i < day_count; i++)
                {
                    Item itmDay = itmDays.getItemByIndex(i);
                    string in_fight_day = itmDay.getProperty("in_fight_day", "");

                    if (in_fight_day == tomtomorrow)
                    {
                        sameDay = true;
                    }

                    itmDay.setType("inn_day");
                    itmDay.setProperty("value", in_fight_day);
                    itmDay.setProperty("text", in_fight_day);
                    itmR.addRelationship(itmDay);
                }
            }

            if (cfg.in_fight_day == "")
            {
                if (sameDay)
                {
                    cfg.in_fight_day = tomtomorrow;
                }
                else
                {
                    cfg.in_fight_day = itmDays.getItemByIndex(0).getProperty("in_fight_day", "");
                }

                itmR.setProperty("in_fight_day", cfg.in_fight_day);
            }

            if (cfg.site_code == "")
            {
                cfg.site_code = "1";
                itmR.setProperty("site_code", "1");
            }

            sql = @"
                SELECT
                    t1.id                                   AS 'pg_id'
	                , t1.in_short_name
	                , t1.in_weight
	                , t1.in_battle_type
	                , t1.in_team_count
	                , t1.in_rank_count
	                , t2.id                                 AS 'evt_id'
	                , t2.in_tree_name
	                , t2.in_tree_id
	                , t2.in_tree_no
	                , t2.in_tree_sno
	                , t2.in_round_code
	                , t2.in_period
	                , t2.in_medal
	                , t3.in_name                            AS 'site_name'
	                , t3.in_code                            AS 'site_code'
                FROM 
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PEVENT t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                INNER JOIN
	                IN_MEETING_SITE t3 WITH(NOLOCK)
	                ON t3.id = t2.in_site
                WHERE
	                t1.in_meeting = '{#meeting_id}'
	                AND t1.in_fight_day = '{#in_fight_day}'
	                AND t3.in_code = {#site_code}
	                AND ISNULL(t2.in_tree_no, 0) > 0
	                AND ISNULL(t2.in_type, '') <> 's'
                ORDER BY
	                t2.in_tree_no
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_fight_day}", cfg.in_fight_day)
                .Replace("{#site_code}", cfg.site_code);

            Item items = inn.applySQL(sql);

            itmR.setProperty("inn_table", TableContents(itmMeeting, items));

            return itmR;
        }

        private string TableContents(Item itmMeeting, Item items)
        {
            string mt_battle_type = itmMeeting.getProperty("in_battle_type", "");
            //單淘不分決賽、獎牌戰
            bool no_medal = mt_battle_type == "TopTwo";
            no_medal = false;

            StringBuilder table = new StringBuilder();
            table.Append("<table class='table table-hover' id='myTable'>");
            table.Append("  <thead>");
            table.Append("    <tr>");
            table.Append("      <th scope='col' class='text-center'>場地</th>");
            table.Append("      <th scope='col' style='color: red'>新場次</th>");
            table.Append("      <th scope='col'>輪次</th>");
            table.Append("      <th scope='col'>量級</th>");
            table.Append("      <th scope='col'>原場次</th>");
            table.Append("      <th scope='col'>賽程</th>");
            table.Append("      <th scope='col'>獎牌戰</th>");
            table.Append("      <th scope='col'>場地</th>");
            table.Append("    </tr>");
            table.Append("  </thead>");

            table.Append("  <tbody>");

            int no = 1;
            int count = items.getItemCount();

            string last_short_name = count <= 0
                ? ""
                : items.getItemByIndex(0).getProperty("in_short_name", "");

            List<TWeight> map = new List<TWeight>();

            for (int i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var evt = MapEvt(item, no);
                var weight = FindAndAddWeight(map, evt);

                table.Append("    <tr id='" + no + "' style='background-color: " + weight.color + "' >");
                table.Append("      <td class='index'>" + evt.site_name + "</td>");
                table.Append("      <td class='indexInput'>" + InputText(evt) + "</td>");
                table.Append("      <td>" + evt.alias + "</td>");
                table.Append("      <td>" + evt.pg_name + "</td>");
                table.Append("      <td>" + evt.tno + "</td>");
                table.Append("      <td>" + PeriodMenu(evt) + "</td>");
                table.Append("      <td>" + ModalMenu(evt) + "</td>");
                table.Append("      <td>" + SiteMenu(evt) + "</td>");
                table.Append("    </tr>");

                no++;
            }
            table.Append("  </tbody>");

            table.Append("</table>");

            return table.ToString();
        }

        private string SiteMenu(TEvt evt)
        {
            return "<select class='form-control inn_select inn_site' data-tv='" + evt.site_code + "' data-type='s' onchange='DataChange(this)'>"
                + "<option value=''>無</option>"
                + "<option value='1'>一</option>"
                + "<option value='2'>二</option>"
                + "<option value='3'>三</option>"
                + "<option value='4'>四</option>"
                + "<option value='5'>五</option>"
                + "<option value='6'>六</option>"
                + "<option value='7'>七</option>"
                + "<option value='8'>八</option>"
                + "<option value='9'>九</option>"
                + "<option value='10'>十</option>"
                + "</select>";
        }

        private string PeriodMenu(TEvt evt)
        {
            return "<select class='form-control inn_select inn_period' data-tv='" + evt.in_period + "' data-type='p' onchange='DataChange(this)'>"
                + "<option value=''>無</option>"
                + "<option value='1'>預賽</option>"
                + "<option value='4'>決賽</option>"
                + "<option value='8'>加賽</option>"
                + "</select>";
        }

        private string ModalMenu(TEvt evt)
        {
            return "<select class='form-control inn_select inn_medal' data-tv='" + evt.in_medal + "' data-type='m' onchange='DataChange(this)'>"
                + "<option value=''>無</option>"
                + "<option value='1'>銅牌</option>"
                + "<option value='2'>金牌</option>"
                + "<option value='11'>3 vs 2</option>"
                + "<option value='12'>2 vs 1</option>"
                + "<option value='13'>加賽(挑)</option>"
                + "<option value='21'>盟主賽</option>"
                + "<option value='22'>加賽(盟)</option>"
                + "</select>";
        }

        private TWeight FindAndAddWeight(List<TWeight> map, TEvt evt)
        {
            var weight = map.Find(x => x.pg_short == evt.pg_short);
            if (weight == null)
            {
                weight = new TWeight
                {
                    pg_short = evt.pg_short,
                };

                if (map.Count > Colors.Length)
                {
                    weight.color = "white";
                }
                else
                {
                    weight.color = Colors[map.Count];
                }

                map.Add(weight);
            }
            return weight;
        }

        private string[] Colors =
        {
            "#F0F5F7",
            "#E8F5EA",
            "#FCDBDB",
            "#CBE3F5",
            "#FFDDAA",
            "#FFEE99",
            "#FFFFBB",
            "#EEFFBB",
            "#CCFF99",
            "#99FF99",
            "#BBFFEE",
            "#AAFFEE",
            "#99FFFF",
            "#CCEEFF",
            "#CCDDFF",
            "#CCCCFF",
            "#CCBBFF",
            "#D1BBFF",
            "#E8CCFF",
            "#F0BBFF",
            "#FFB3FF",
            "#DDDDDD",
            "#FF88C2",
            "#FF8888",
            "#FFA488",
            "#FFBB66",
            "#FFDD55",
            "#FFFF77",
            "#DDFF77",
            "#BBFF66",
        };

        private string InputText(TEvt evt)
        {
            return "<input type='text' name='tree_no' id='index'"
                    + " data-eid='" + evt.eid + "'"
                    + " data-ono='" + evt.tno + "'"
                    + "onkeyup='Event_KeyUp(this)'"
                    + " value='" + evt.no + "'>";
        }

        private TEvt MapEvt(Item item, int no)
        {
            TEvt evt = new TEvt
            {
                no = no,
                eid = item.getProperty("evt_id", ""),
                tnm = item.getProperty("in_tree_name", ""),
                tno = item.getProperty("in_tree_no", ""),
                otn = item.getProperty("in_tree_sno", ""),
                tid = item.getProperty("in_tree_id", ""),
                rcode = item.getProperty("in_round_code", ""),

                pg_short = item.getProperty("in_short_name", ""),
                pg_weight = item.getProperty("in_weight", ""),
                pg_battle = item.getProperty("in_battle_type", ""),
                pg_tmcnt = item.getProperty("in_team_count", "0"),
                pg_rkcnt = item.getProperty("in_rank_count", "0"),

                site_name = item.getProperty("site_name", ""),
                site_code = item.getProperty("site_code", ""),
                in_period = item.getProperty("in_period", ""),
                in_medal = item.getProperty("in_medal", ""),
            };

            evt.is_robin = evt.pg_battle.Contains("Robin");

            //量級
            evt.pg_name = evt.pg_short + " " + evt.pg_weight + " (" + evt.pg_tmcnt + ")";

            evt.alias = GetAlias(evt);

            return evt;
        }

        private string GetAlias(TEvt evt)
        {
            var tid = evt.tid.ToUpper();
            if (evt.is_robin)
            {
                return GetRobinAlias(evt);
            }

            switch (tid)
            {
                case "CA01": return "3 vs 2";
                case "CA02": return "2 vs 1";
                case "CB01": return "挑戰賽-加賽";
                case "KA01": return "盟主賽";
                case "KB01": return "盟主賽-加賽";
                case "rank78": return "78名";
                case "rank56": return "56名";
                case "rank34": return "34名";
                default: return GetOtherAlias(evt);
            }
        }

        private string GetOtherAlias(TEvt evt)
        {
            switch (evt.tnm)
            {
                case "main":
                    if (evt.rcode == "2")
                    {
                        return "<label style='color: red'>勝部-決賽</label>";
                    }
                    else if (evt.rcode == "4")
                    {
                        return "<label style='color: red'>勝部-準決賽</label>";
                    }
                    else
                    {
                        return evt.rcode + "強";
                    }

                case "repechage":
                    if (evt.rcode == "2")
                    {
                        return "<label style='color: red'>34名</label>";
                    }
                    else if (evt.rcode == "4")
                    {
                        if (evt.tid.EndsWith("1"))
                        {
                            return "<label style='color: red'>銅牌戰(W)</label>";
                        }
                        else if (evt.tid.EndsWith("2"))
                        {
                            return "<label style='color: red'>銅牌戰(E)</label>";
                        }
                        else
                        {
                            return "<label style='color: red'>銅牌戰</label>";
                        }
                    }
                    else
                    {
                        return "RPC " + evt.rcode + "強";
                    }
                default:
                    return "";
            }
        }

        private string GetRobinAlias(TEvt evt)
        {
            return "(循) " + evt.otn + "/" + evt.rcode + "";
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }

            public string meeting_id { get; set; }
            public string in_fight_day { get; set; }
            public string site_code { get; set; }
        }

        private class TEvt
        {
            public int no { get; set; }
            public string eid { get; set; }
            public string weight { get; set; }
            public string alias { get; set; }
            public string tnm { get; set; }
            public string tno { get; set; }
            public string tid { get; set; }
            public string rcode { get; set; }

            public bool is_robin { get; set; }
            public string pg_name { get; set; }
            public string pg_short { get; set; }
            public string pg_weight { get; set; }
            public string pg_battle { get; set; }
            public string pg_tmcnt { get; set; }
            public string pg_rkcnt { get; set; }

            public string site_name { get; set; }
            public string site_code { get; set; }
            public string in_medal { get; set; }
            public string in_period { get; set; }

            public string otn { get; set; }
        }

        private class TWeight
        {
            public string pg_short { get; set; }
            public string color { get; set; }
        }
    }
}