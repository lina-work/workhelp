﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Judo.Fight
{
    public class AAA_Method : Item
    {
        public AAA_Method(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
               目的: 過磅卡控設定
               日期: 
                   - 2021-12-17 創建 (Lina)
           */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]In_Meeting_Weight";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, this.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                InnH = _InnH,

                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),

                meeting_id = itmR.getProperty("meeting_id", ""),
                scene = itmR.getProperty("scene", ""),
            };

            switch (cfg.scene)
            {
                case "update":
                    Update(cfg, itmR);
                    break;

                case "auto_weight"://過磅體重自動推算
                    AutoWeight(cfg, itmR);
                    break;

                case "auto_name"://自動推算簡稱
                    AutoName(cfg, itmR);
                    break;

                default:
                    Page(cfg, itmR);
                    break;
            }

            return itmR;
        }

        //自動推算簡稱
        private void AutoName(TConfig cfg, Item itmReturn)
        {
            var options = OptionTree(cfg);

            int count = options.Count();

            for (int i = 0; i < count; i++)
            {
                var opt1 = options[i];

                for (int j = 0; j < opt1.Children.Count; j++)
                {
                    var opt2 = opt1.Children[j];

                    for (int k = 0; k < opt2.Children.Count; k++)
                    {
                        var opt3 = opt2.Children[k];
                        MapName(opt1, opt2, opt3);

                        UpdateOption(cfg, opt3.id, "in_n1", opt3.in_n1);
                    }
                };
            }
        }

        private void MapName(TOption opt1, TOption opt2, TOption opt3)
        {
            List<string> ns = new List<string>();

            if (opt1.in_value == "團體組")
            {
                ns.Add("團");
            }
            else if (opt1.in_value == "格式組")
            {
                ns.Add("格");
            }
            if (opt1.in_value == "成年組")
            {
                ns.Add("成");
            }
            else if (opt1.in_value == "青年組")
            {
                ns.Add("青");
            }
            else if (opt1.in_value == "青少年組")
            {
                ns.Add("少");
            }



            if (opt2.in_value.Contains("一般"))
            {
                ns.Add("一");
            }
            else if (opt2.in_value.Contains("成人"))
            {
                ns.Add("成");
            }
            else if (opt2.in_value.Contains("大專"))
            {
                ns.Add("大");
            }
            else if (opt2.in_value.Contains("公開"))
            {
                ns.Add("公");
            }
            else if (opt2.in_value.Contains("社會"))
            {
                ns.Add("社");
            }
            else if (opt2.in_value.Contains("青年"))
            {
                ns.Add("青");
            }
            else if (opt2.in_value.Contains("青少年"))
            {
                ns.Add("少");
            }
            else if (opt2.in_value.Contains("高中"))
            {
                ns.Add("高");
            }
            else if (opt2.in_value.Contains("國中"))
            {
                ns.Add("國");
            }
            else if (opt2.in_value.Contains("國小"))
            {
                ns.Add("小");
            }

            if (opt2.in_value.Contains("男"))
            {
                ns.Add("男");
            }
            else if (opt2.in_value.Contains("女"))
            {
                ns.Add("女");
            }

            if (opt2.in_value.Contains("甲"))
            {
                ns.Add("甲");
            }
            else if (opt2.in_value.Contains("乙"))
            {
                ns.Add("乙");
            }
            else if (opt2.in_value.Contains("特別"))
            {
                ns.Add("特");
            }
            else if (opt2.in_value.Contains("A"))
            {
                ns.Add("A");
            }
            else if (opt2.in_value.Contains("B"))
            {
                ns.Add("B");
            }
            else if (opt2.in_value.Contains("C"))
            {
                ns.Add("C");
            }
            else if (opt2.in_value.Contains("高年級組"))
            {
                ns.Add("A");
            }
            else if (opt2.in_value.Contains("中年級組"))
            {
                ns.Add("B");
            }
            else if (opt2.in_value.Contains("低年級組"))
            {
                ns.Add("C");
            }

            if (opt3.in_value.Contains("一")) ns.Add("一");
            else if (opt3.in_value.Contains("二")) ns.Add("二");
            else if (opt3.in_value.Contains("三")) ns.Add("三");
            else if (opt3.in_value.Contains("四")) ns.Add("四");
            else if (opt3.in_value.Contains("五")) ns.Add("五");
            else if (opt3.in_value.Contains("六")) ns.Add("六");
            else if (opt3.in_value.Contains("七")) ns.Add("七");
            else if (opt3.in_value.Contains("八")) ns.Add("八");
            else if (opt3.in_value.Contains("九")) ns.Add("九");
            else if (opt3.in_value.Contains("十")) ns.Add("十");
            else if (opt3.in_value.Contains("十一")) ns.Add("十一");
            else if (opt3.in_value.Contains("十二")) ns.Add("十二");
            else if (opt3.in_value.Contains("投之形")) ns.Add("投");
            else if (opt3.in_value.Contains("柔之形")) ns.Add("柔");
            else if (opt3.in_value.Contains("固之形")) ns.Add("固");
            else if (opt3.in_value.Contains("極之形")) ns.Add("極");
            else if (opt3.in_value.Contains("講道")) ns.Add("講");

            opt3.in_n1 = string.Join("", ns);

        }

        //過磅體重自動推算
        private void AutoWeight(TConfig cfg, Item itmReturn)
        {
            var options = OptionTree(cfg);

            int count = options.Count();

            string[] bypass_arr = new string[] { "團體組", "格式組", "隊職員" };
            for (int i = 0; i < count; i++)
            {
                var opt1 = options[i];
                if (bypass_arr.Contains(opt1.in_value))
                {
                    continue;
                }

                for (int j = 0; j < opt1.Children.Count; j++)
                {
                    var opt2 = opt1.Children[j];
                    opt2.is_elementary = opt2.in_value.Contains("國小");

                    for (int k = 0; k < opt2.Children.Count; k++)
                    {
                        var opt3 = opt2.Children[k];
                        opt3.is_elementary = opt2.is_elementary;

                        var last_opt3 = k == 0
                            ? new TOption { in_value = "0" }
                            : opt2.Children[k - 1];

                        SetWeightValue(opt3, last_opt3);

                        UpdateOptions(cfg, opt3);
                    }
                };
            }
        }

        private void UpdateOptions(TConfig cfg, TOption opt)
        {
            string sql = "UPDATE IN_SURVEY_OPTION SET" 
                + "  in_extend_value2 = N'" + opt.new_extend_value2  + "'"
                + ", in_weight = '" + opt.in_weight  + "'"
                + " WHERE id = '" + opt.id + "'";

            Item itmSQL = cfg.inn.applySQL(sql);

            if (itmSQL.isError())
            {
                throw new Exception("更新失敗");
            }
        }

        private void Update(TConfig cfg, Item itmReturn)
        {
            string id = itmReturn.getProperty("opt_id", "");
            string property = itmReturn.getProperty("property", "");
            string value = itmReturn.getProperty("value", "");
            UpdateOption(cfg, id, property, value);
        }

        private void UpdateOption(TConfig cfg, string id, string in_property, string value)
        {
            string sql = "UPDATE IN_SURVEY_OPTION SET " + in_property + " = N'" + value + "' WHERE id = '" + id + "'";

            Item itmSQL = cfg.inn.applySQL(sql);

            if (itmSQL.isError())
            {
                throw new Exception("更新失敗");
            }
        }

        private void SetWeightValue(TOption opt, TOption lastOpt)
        {
            string value = opt.in_value;

            //SAMPLE 1
            //  第一級：60公斤以下（含60.0公斤）
            //  第六級：90.1公斤至100.0公斤
            //  第七級：100.1公斤以上
            //SAMPLE 2
            //  第一級：-48Kg
            //  第三級：-57Kg
            //  第七級：+78Kg
            if (value.Contains("以下"))
            {
                WeightValue2(opt, lastOpt);
            }
            else if (value.Contains("至"))
            {
                WeightValue2(opt, lastOpt);
            }
            else if (value.Contains("以上"))
            {
                WeightValue2(opt, lastOpt);
            }
            else
            {
                WeightValue1(opt, lastOpt);
            }
        }

        private string GetWeighKg(string value)
        {
            string result = value
                .Replace("公斤", "Kg")
                .Replace("kg", "Kg")
                .Replace("以下", "")
                .Replace("以下", "")
                .Replace("-", "")
                .Replace("+", "");

            //SAMPLE 1
            //  第一級：60公斤以下（含60.0公斤）
            //  第六級：90.1公斤至100.0公斤
            //  第七級：100.1公斤以上

            string op = "-"; 
            if (value.Contains("以下"))
            {
                op = "-";
            }
            else if (value.Contains("以上"))
            {
                op = "+";
            }
            else if (value.Contains("+"))
            {
                op = "+";
            }

            char c = '：';
            if (value.Contains(":")) c = ':';

            string[] arr = value.Split(new char[] { c }, StringSplitOptions.RemoveEmptyEntries);
            if (arr == null)
            {
                return result;
            }
            else if (arr.Length == 1)
            {
                return op + arr[0];
            }
            else
            {
                return op + arr[1];
            }
        }

        private void WeightValue2(TOption opt, TOption lastOpt)
        {
            //SAMPLE 1
            //  第一級：60公斤以下（含60.0公斤）
            //  第六級：90.1公斤至100.0公斤
            //  第七級：100.1公斤以上

            string value = opt.in_value.ToLower();

            opt.weight_end = value;
            opt.new_extend_value2 = value;
            opt.in_weight = GetWeighKg(value);

            opt.weight_start = lastOpt.weight_end + ".1";

            if (value.Contains("以上"))
            {
                var v1 = value.Split('：').Last();
                var v2 = v1.Split(new string[] { "公斤" }, StringSplitOptions.RemoveEmptyEntries).First();
                opt.new_extend_value2 = v2 + "-" + "999";
            }
            else if (value.Contains("以下"))
            {
                var v1 = value.Split('：').Last();
                var v2 = v1.Split(new string[] { "公斤" }, StringSplitOptions.RemoveEmptyEntries).First();
                opt.new_extend_value2 = "0" + "-" + v2;
            }
            else
            {
                var v1 = value.Split('：').Last();
                var arr = v1.Split(new string[] { "至" }, StringSplitOptions.RemoveEmptyEntries);
                var v2 = arr.First().Replace("公斤", "");
                var v3 = arr.Last().Replace("公斤", "").Replace(".0", "");

                if (opt.is_elementary)
                {
                    //國小不可裸磅，max 容許 0.1
                    opt.new_extend_value2 = v2 + "-" + v3 + ".1";
                }
                else
                {
                    opt.new_extend_value2 = v2 + "-" + v3;
                }
            }
        }

        private void WeightValue1(TOption opt, TOption lastOpt)
        {
            string value = opt.in_value.ToLower();
            bool is_first = value.Contains("第一") || lastOpt.in_value == "0";

            opt.weight_start = value;
            opt.weight_end = value;
            opt.new_extend_value2 = value;
            opt.in_weight = GetWeighKg(value);

            string v1 = value;

            if (value.Contains("kg"))
            {
                v1 = value.Replace("kg", "");
            }
            else if (value.Contains("公斤"))
            {
                v1 = value.Replace("公斤", "");
            }
            else
            {
                return;
            }


            opt.weight_start = lastOpt.weight_end + ".1";
            if (is_first)
            {
                opt.weight_start = "0";
            }

            if (v1.Contains("+"))
            {
                //第七級：+100kg
                //100.1-999
                opt.weight_end = "999";
                opt.new_extend_value2 = opt.weight_start + "-" + opt.weight_end;
            }
            else if (v1.Contains('-'))
            {
                //第二級：-66Kg
                //60.1-66
                opt.weight_end = v1.Split('-').Last();

                if (opt.is_elementary)
                {
                    //國小不可裸磅，max 容許 0.1
                    opt.new_extend_value2 = opt.weight_start + "-" + opt.weight_end + ".1";
                }
                else
                {
                    opt.new_extend_value2 = opt.weight_start + "-" + opt.weight_end;
                }
            }
        }

        private void Page(TConfig cfg, Item itmReturn)
        {
            Item itmMeeting = cfg.inn.applySQL("SELECT in_title FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            itmReturn.setProperty("in_title", itmMeeting.getProperty("in_title", ""));

            var options = OptionTree(cfg);

            int count = options.Count();

            int no = 1;

            for (int i = 0; i < count; i++)
            {
                var opt1 = options[i];
                for (int j = 0; j < opt1.Children.Count; j++)
                {
                    var opt2 = opt1.Children[j];
                    for (int k = 0; k < opt2.Children.Count; k++)
                    {
                        var opt3 = opt2.Children[k];
                        opt3.Value.setType("inn_options");
                        opt3.Value.setProperty("no", no.ToString());
                        itmReturn.addRelationship(opt3.Value);

                        no++;
                    }
                };
            }
        }

        private List<TOption> OptionTree(TConfig cfg)
        {
            var itmLv1s = GetSurveyOptions(cfg, "in_l1");
            var l1_opts = MapOption(cfg, itmLv1s);

            var itmLv2s = GetSurveyOptions(cfg, "in_l2");
            var l2_opts = MapOption(cfg, itmLv2s);

            var itmLv3s = GetSurveyOptions(cfg, "in_l3");
            var l3_opts = MapOption(cfg, itmLv3s);

            for (int i = 0; i < l1_opts.Count; i++)
            {
                var op1 = l1_opts[i];
                op1.Children = l2_opts.FindAll(x => x.in_filter == op1.in_value);
                if (op1.Children == null)
                {
                    op1.Children = new List<TOption>();
                }

                for (int j = 0; j < op1.Children.Count; j++)
                {
                    var op2 = op1.Children[j];
                    op2.Children = l3_opts.FindAll(x => x.in_filter == op2.in_value);
                    if (op2.Children == null)
                    {
                        op2.Children = new List<TOption>();
                    }
                }
            }

            return l1_opts;
        }

        private List<TOption> MapOption(TConfig cfg, Item items)
        {
            List<TOption> options = new List<TOption>();

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);

                options.Add(new TOption
                {
                    id = item.getProperty("id", ""),
                    in_filter = item.getProperty("in_filter", ""),
                    in_value = item.getProperty("in_value", ""),
                    Value = item,
                });
            }

            return options;
        }

        private Item GetSurveyOptions(TConfig cfg, string in_property)
        {
            string sql = @"
                SELECT
	                t3.*
                FROM 
	                IN_MEETING_SURVEYS t1 WITH(NOLOCK)
                INNER JOIN
	                IN_SURVEY t2 WITH(NOLOCK)
	                ON t2.id = t1.related_id
                INNER JOIN
	                IN_SURVEY_OPTION t3 WITH(NOLOCK)
	                ON t3.source_id = t1.related_id
                WHERE 
	                t1.source_id = '{#meeting_id}'
	                AND t2.in_property = '{#in_property}'
	                AND t3.in_value <> N'隊職員'
                ORDER BY
	                t3.sort_order
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_property}", in_property);

            return cfg.inn.applySQL(sql); ;
        }

        private class TOption
        {
            public Item Value { get; set; }

            public string id { get; set; }
            public string in_filter { get; set; }
            public string in_value { get; set; }
            public List<TOption> Children { get; set; }

            public string in_weight { get; set; }
            public string new_extend_value2 { get; set; }
            public string weight_start { get; set; }
            public string weight_end { get; set; }

            public bool is_elementary { get; set; }

            public string in_n1 { get; set; }

        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public Innosoft.InnovatorHelper InnH { get; set; }

            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }

            public string meeting_id { get; set; }
            public string scene { get; set; }

            public Item itmMeeting { get; set; }
        }

        private int GetIntVal(string value, int def = 0)
        {
            if (value == "") return 0;

            int result = def;
            Int32.TryParse(value, out result);
            return result;
        }
    }
}