﻿using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.PLMCTA.Common
{
    public class Fix_Meeting_User_WeightTag : Item
    {
        public Fix_Meeting_User_WeightTag(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "Fix_Meeting_User_WeightTag";

            var itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                meeting_id = itmR.getProperty("meeting_id", ""),
                scene = itmR.getProperty("scene", ""),
            };



            return itmR;
        }

        //團體組
        private void FixTeamWeight(TConfig cfg, Item itmReturn)
        {
            var sql = "";

            sql = @"
                UPDATE t1 SET
                	t1.in_team_sect = REPLACE(REPLACE(REPLACE(t2.in_short_name, '團', ''), '五', ''), '三', '')
                FROM
                	IN_MEETING_USER t1 WITH(NOLOCK)
                INNER JOIN
                	IN_MEETING_PROGRAM t2 WITH(NOLOCK)
                	ON t2.in_meeting = t1.source_id
                	AND t2.in_l1 = t1.in_l1
                	AND t2.in_l2 = t1.in_l2
                	AND t2.in_l3 = t1.in_l3
                WHERE
                	t1.source_id = '{#meeting_id}'
                	AND t1.in_l1 = N'團體組'
                	AND t1.{#level_prop} NOT LIKE '%特別%'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#level_prop}", cfg.level_prop);

            cfg.inn.applySQL(sql);


            sql = @"
                UPDATE t1 SET
                	t1.in_team_sect = N'小男A'
                FROM
                	IN_MEETING_USER t1 WITH(NOLOCK)
                WHERE
                	t1.source_id = '{#meeting_id}'
                	AND t1.in_l1 = N'團體組'
                	AND ISNULL(t1.in_team_sect, '') = N'小男'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            cfg.inn.applySQL(sql);


            sql = @"
                UPDATE t1 SET
                	t1.in_team_sect = N'小女A'
                FROM
                	IN_MEETING_USER t1 WITH(NOLOCK)
                WHERE
                	t1.source_id = '{#meeting_id}'
                	AND t1.in_l1 = N'團體組'
                	AND ISNULL(t1.in_team_sect, '') = N'小女'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            cfg.inn.applySQL(sql);
        }

        //個人組
        private void FixSoloWeight(TConfig cfg, Item itmReturn)
        {
            var sql = "";

            sql = @"
                UPDATE t1 SET
                	t1.in_team_sect = t2.in_short_name
                	, t1.in_team_n1 = LEFT(t1.in_l3, 3)
                FROM
                	IN_MEETING_USER t1 WITH(NOLOCK)
                INNER JOIN
                	IN_MEETING_PROGRAM t2 WITH(NOLOCK)
                	ON t2.in_meeting = t1.source_id
                	AND t2.in_l1 = t1.in_l1
                	AND t2.in_l2 = t1.in_l2
                	AND t2.in_l3 = t1.in_l3
                WHERE
                	t1.source_id = '{#meeting_id}'
                	AND t1.in_l1 = N'個人組'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#level_prop}", cfg.level_prop);

            cfg.inn.applySQL(sql);


            // sql = @"
            //     UPDATE t1 SET
            //     	t1.in_team_n1 = REPLACE(REPLACE(t1.in_team_n1, '第', ''), '級', '')
            //     FROM
            //     	IN_MEETING_USER t1 WITH(NOLOCK)
            //     WHERE
            //     	t1.source_id = '{#meeting_id}'
            //     	AND t1.in_l1 = N'個人組'
            // ";

            // sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            // cfg.inn.applySQL(sql);
        }


        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }

            public string meeting_id { get; set; }
            public string level_prop { get; set; }
            public string scene { get; set; }
        }
    }
}