﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Judo.Fight
{
    public class In_Meeting_UpdateWeight : Item
    {
        public In_Meeting_UpdateWeight(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的:更新工作人員檢錄選手體重資訊
                位置:選手檢錄 > 體重登錄
                日期: 2010/10/13 創建 (Alan)
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]In_Meeting_UpdateWeight";

            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            string aml = "";
            string sql = "";
            string strError = "";

            //工作人員ID與姓名
            string staff_id = inn.getUserID();
            Item CurrentUser = inn.getItemById("User", staff_id);
            string staff_name = CurrentUser.getProperty("last_name", "");

            Item itmR = this;
            string meeting_id = this.getProperty("meeting_id", "");
            string in_sno = this.getProperty("in_sno", "");
            string in_weight = this.getProperty("in_weight", "0");
            string in_weight_time = System.DateTime.Now.AddHours(-8).ToString("yyyy/MM/dd HH:mm:ss");
            string mode = this.getProperty("mode", "");
            string l1_value = this.getProperty("l1_value", "");
            string l2_value = this.getProperty("l2_value", "");
            string l3_value = this.getProperty("l3_value", "");

            string sqlfilter = "";
            if (mode == "single")
            {
                sqlfilter = " AND t1.in_l1 = N'" + l1_value + "' AND t1.in_l2 = N'" + l2_value + "' AND t1.in_l3 = N'" + l3_value + "'";
            }

            Item itmSql;
            double weight = 0;
            bool weight_null = false;

            if (in_weight == "" || in_weight == "0")
            {
                weight_null = true;
            }

            if (!weight_null)
            {
                bool result = double.TryParse(in_weight, out weight);
                if (!result || weight > 500 || weight <= 0)
                {
                    throw new Exception("體重值有誤，請確認並輸入數值!");
                }
            }

            bool is_remove_dq = false;
            sql = "SELECT in_is_remove FROM IN_MEETING_PWEIGHT WITH(NOLOCK) WHERE in_meeting = '" + meeting_id + "' AND in_status = 'dq'";
            itmSql = inn.applySQL(sql);
            if (!itmSql.isError() && itmSql.getResult() != "")
            {
                is_remove_dq = itmSql.getProperty("in_is_remove", "") == "1";
            }

            //sql = "SELECT * FROM IN_MEETING_USER WITH(NOLOCK)"
            //    + " WHERE source_id = '" + meeting_id + "'"
            //    + " AND in_sno = N'" + in_sno + "'";

            //itmSql = inn.applySQL(sql);

            //if (itmSql.isError() || itmSql.getResult() == "")
            //{
            //    throw new Exception("該賽事找無此人員，請再確認");
            //}

            //取得該人員參加之所有項目
            sql = @"
                SELECT 
                    t1.id
                    , t1.in_l1
                    , t1.in_l2
                    , t1.in_l3
                    , t1.in_index
                    , t1.in_creator_sno
                    , t1.in_weight_status
                    , t3.in_extend_value2 AS 'weight_class' 
                FROM 
                    IN_MEETING_USER AS t1 WITH(NOLOCK)
                INNER JOIN
                    IN_MEETING_SURVEYS AS t2 WITH(NOLOCK)
                    ON t1.source_id = t2.source_id
                INNER JOIN
                    IN_SURVEY_OPTION AS t3 WITH(NOLOCK)
                    ON t2.related_id = t3.source_id 
                    AND t1.in_l1 = t3.in_grand_filter
                    AND t1.in_l2 = t3.in_filter
                    AND t1.in_l3 = t3.in_value
                WHERE 
                    t1.source_id = '{#meeting_id}' 
                    AND t1.in_sno = N'{#in_sno}'
                    {#sqlfilter}
                ";

            sql = sql.Replace("{#meeting_id}", meeting_id)
                       .Replace("{#in_sno}", in_sno)
                       .Replace("{#sqlfilter}", sqlfilter);

            Item itmMUsers = inn.applySQL(sql);
            int muser_count = itmMUsers.getItemCount();

            string muid = "";
            string in_l1 = "";
            string in_l2 = "";
            string in_l3 = "";
            string old_status = "";
            string weight_class = "";
            string[] words;
            double w_min = 0;
            double w_max = 0;
            string w_check = "0";
            string w_status = "";
            int check_no = 0;

            for (int i = 0; i < muser_count; i++)
            {
                Item itmMUser = itmMUsers.getItemByIndex(i);
                muid = itmMUser.getProperty("id", "");
                in_l1 = itmMUser.getProperty("in_l1", "");
                in_l2 = itmMUser.getProperty("in_l2", "");
                in_l3 = itmMUser.getProperty("in_l3", "");
                weight_class = itmMUser.getProperty("weight_class", "");
                old_status = itmMUser.getProperty("in_weight_status", "");

                if (weight_null)
                {
                    //歸零更新後，不往下處理
                    string sql_upd = "UPDATE IN_MEETING_USER SET in_weight = NULL, in_weight_result = NULL WHERE id = '" + muid + "'";
                    Item itmUpd = inn.applySQL(sql_upd);

                    LinkTeamBattle(inn, itmMUser, meeting_id, muid, in_l1, in_l2, in_l3, "");
                    continue;
                }

                try
                {
                    if (weight_class == "")
                    {
                        w_check = "1";
                        w_status = "on";
                    }
                    else
                    {
                        words = weight_class.Split('-');
                        w_min = Convert.ToDouble(words[0]);
                        w_max = Convert.ToDouble(words[1]);

                        //lina: 2022.07.20: 少融與老師確認後，改為 >= MIN，<= MAX
                        if (weight >= w_min && weight <= w_max)
                        {
                            w_check = "1";
                            w_status = "on";
                        }
                        else
                        {
                            w_check = "0";
                            w_status = "dq";
                            check_no++;
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("競賽項目設定值有誤，請通知系統設定人員!");
                }

                if (old_status == "king")
                {
                    w_status = "king";
                }

                sql = @"UPDATE IN_MEETING_USER SET 
                    in_weight = {#in_weight}
                    , in_weight_createid = '{#staff_id}'
                    , in_weight_createname = N'{#staff_name}'
                    , in_weight_time = N'{#in_weight_time}'
                    , in_weight_result = N'{#w_check}'
                    , in_weight_status = N'{#w_status}'
                    WHERE id = '{#muid}'";

                sql = sql.Replace("{#muid}", muid)
                    .Replace("{#in_weight}", in_weight)
                    .Replace("{#staff_id}", staff_id)
                    .Replace("{#staff_name}", staff_name)
                    .Replace("{#in_weight_time}", in_weight_time)
                    .Replace("{#w_check}", w_check)
                    .Replace("{#w_status}", w_status)
                    .Replace("{#in_sno}", in_sno);

                Item itmSql_update = inn.applySQL(sql);

                if (itmSql_update.isError())
                {
                    throw new Exception("登錄體重失敗，請再確認!");
                }

                if (is_remove_dq)
                {
                    //lina 2021.12.23 移除名單 _# START:
                    Item itmStrawOff = inn.newItem("In_Meeting_PTeam");
                    itmStrawOff.setProperty("meeting_id", meeting_id);
                    itmStrawOff.setProperty("l1_value", in_l1);
                    itmStrawOff.setProperty("l2_value", in_l2);
                    itmStrawOff.setProperty("l3_value", in_l3);
                    itmStrawOff.setProperty("muid", muid);
                    itmStrawOff.setProperty("scene", "no_auto_upgrad");
                    Item itmStrawOffResult = itmStrawOff.apply("in_meeting_straw_off");
                    //lina 2021.12.23 移除名單 _# END.
                }

                if (in_l1 == "團體組")
                {
                    LinkTeamBattle(inn, itmMUser, meeting_id, muid, in_l1, in_l2, in_l3, in_weight);
                }
                else
                {
                    //lina 2020.10.27 成績串接 _# START:
                    Item itmScore = inn.newItem("In_Meeting_PEvent_Detail");
                    itmScore.setProperty("mode", "rollcall");
                    itmScore.setProperty("meeting_id", meeting_id);
                    itmScore.setProperty("in_l1", in_l1);
                    itmScore.setProperty("in_l2", in_l2);
                    itmScore.setProperty("in_l3", in_l3);
                    itmScore.setProperty("in_sno", in_sno);
                    itmScore.setProperty("w_check", w_check);
                    itmScore.setProperty("w_status", w_status);
                    itmScore.setProperty("w_type", "weight");
                    itmScore.setProperty("w_value", in_weight);
                    Item itmScoreResult = itmScore.apply("in_meeting_program_score");
                    //lina 2020.10.27 成績串接 _# END.

                    string program_id = itmScoreResult.getProperty("program_id", "");
                    itmMUser.setType("inn_program_id");
                    itmMUser.setProperty("program_id", program_id);
                    itmR.addRelationship(itmMUser);
                }
            }

            string resultMsg = "";

            itmR.setProperty("check_no", check_no.ToString());

            if (weight_null)
            {
                resultMsg = "<span style='color: rgb(25, 146, 9);font-weight:bold;'>(已清空體重資料)</span>";
            }
            else if (check_no > 0)
            {
                string styleS = "<span style='color: rgb(255, 0, 0);font-weight:bold;'>";
                string styleE = "</span>";
                // resultMsg = styleS +"體重： " + in_weight + " 公斤(與項目限制不符，請下拉至競賽項目查閱)" + styleE;
                resultMsg = styleS + "(體重： " + in_weight + " 公斤與項目限制不符，請下拉至競賽項目查閱)" + styleE;
            }
            else
            {
                // resultMsg = "<span style='color: rgb(25, 146, 9);font-weight:bold;'>體重： " + in_weight + " 公斤    (與項目符合)</span>";
                resultMsg = "<span style='color: rgb(25, 146, 9);font-weight:bold;'>(體重： " + in_weight + " 公斤與項目符合)</span>";
            }

            itmR.setProperty("resultMsg", resultMsg);
            itmR.setProperty("in_weight_time", DateTime.Parse(in_weight_time).AddHours(8).ToString("yyyy/MM/dd HH:mm:ss"));
            itmR.setProperty("in_weight_createname", staff_name);

            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

            return itmR;
        }

        private void LinkTeamBattle(Innovator inn
            , Item itmMUser
            , string meeting_id
            , string muid
            , string in_l1
            , string in_l2
            , string in_l3
            , string in_weight)
        {
            //lina 2022.11.03 團體賽串接 _# START:
            Item itmTeam = inn.newItem("In_Meeting_PTeam");
            itmTeam.setProperty("scene", "weight_link");
            itmTeam.setProperty("meeting_id", meeting_id);
            itmTeam.setProperty("muid", muid);
            itmTeam.setProperty("in_l1", in_l1);
            itmTeam.setProperty("in_l2", in_l2);
            itmTeam.setProperty("in_l3", in_l3);
            itmTeam.setProperty("in_index", itmMUser.getProperty("in_index", ""));
            itmTeam.setProperty("in_creator_sno", itmMUser.getProperty("in_creator_sno", ""));
            itmTeam.setProperty("in_weight", in_weight);
            Item itmScoreResult = itmTeam.apply("in_team_battle_register_org");
            //lina 2022.11.03 團體賽串接 _# END.
        }
    }
}