﻿using Aras.IOM;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;

namespace WorkHelp.ArasDesk.Methods.Judo.Fight
{
    public class In_Meeting_GetCheckByGroup : Item
    {
        public In_Meeting_GetCheckByGroup(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 抓取競賽群組尚未過磅資訊
                日期: 
                    - 2022-11-04 加入 團體賽、個人賽量級差註記異 (lina)
                    - 2022-07-27 加入 +2kg 註記 (lina)
                    - 2021-11-26 備註加入上次過磅紀錄 (lina)
                    - 2020-10-29 創建 (Alan)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]In_Meeting_GetCheckByGroup";

            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            Item itmR = this;

            Item itmPermit = inn.applyMethod("In_CheckIdentity", "<method>" + strMethodName + "</method><code>" + "ALL" + "</code>");
            bool isMeetingAdmin = itmPermit.getProperty("isMeetingAdmin", "") == "1";
            bool isMeetingLeader = itmPermit.getProperty("isMeetingLeader", "") == "1";
            bool can_view = isMeetingAdmin || isMeetingLeader;
            if (!can_view)
            {
                itmR.setProperty("error_message", "您無權限瀏覽此頁面，請重新登入");
                return itmR;
            }

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                meeting_id = itmR.getProperty("meeting_id", ""),
                program_id = itmR.getProperty("program_id", ""),
                in_date = itmR.getProperty("in_date", ""),
                in_l1 = itmR.getProperty("in_l1", ""),
                in_l2 = itmR.getProperty("in_l2", ""),
                in_l3 = itmR.getProperty("in_l3", ""),
                mode = itmR.getProperty("mode", ""),
                scene = itmR.getProperty("scene", ""),
                note_mode = itmR.getProperty("note_mode", ""),
            };

            cfg.NoNote = cfg.note_mode == "no_note";

            if (cfg.meeting_id == "")
            {
                return itmR;
            }

            itmR.setProperty("id", cfg.meeting_id);
            //設定賽會資訊
            AppendMeetingInfo(cfg, itmR);
            //設定三階 id
            SetMeetingSurveyId(cfg);
            //設定賽會參數
            SetMeetingVariable(cfg);

            if (cfg.scene == "export_weight")
            {
                //下載稱量體重表(單一 OR 當日)
                cfg.is_xlsx = true;
                XlsFunc(cfg, itmR);
            }
            else
            {
                cfg.is_xlsx = false;
                PageFunc(cfg, itmR);
            }

            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

            return itmR;
        }

        private void AppendMeetingInfo(TConfig cfg, Item itmReturn)
        {
            cfg.itmMeeting = cfg.inn.applySQL("SELECT in_title, in_language, in_weight_mode FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            if (cfg.itmMeeting.isError() || cfg.itmMeeting.getResult() == "")
            {
                throw new Exception("找無該場賽事資料!");
            }

            cfg.mt_title = cfg.itmMeeting.getProperty("in_title", "");
            cfg.mt_weight_mode = cfg.itmMeeting.getProperty("in_weight_mode", "");
            cfg.is_fight_day = cfg.mt_weight_mode == "fight-day";

            itmReturn.setProperty("in_title", cfg.mt_title);
            itmReturn.setProperty("in_language", cfg.itmMeeting.getProperty("in_language", ""));
        }

        private void SetMeetingSurveyId(TConfig cfg)
        {
            cfg.l1_id = "";
            cfg.l2_id = "";
            cfg.l3_id = "";

            var sql = @"
                SELECT 
                    t1.id,
                    t1.in_questions,
                    t1.in_property,
                    t2.sort_order
                FROM 
                    IN_SURVEY t1 WITH(NOLOCK)
                INNER JOIN 
                    IN_MEETING_SURVEYS t2 WITH(NOLOCK)
                    ON t2.related_id = t1.id 
                WHERE 
                    t2.source_id = '{#meeting_id}' 
                    AND t2.in_surveytype = '1'
                    AND t1.in_property IN ('in_l1', 'in_l2', 'in_l3')
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            var items = cfg.inn.applySQL(sql);
            if (items.isError() || items.getItemCount() <= 0)
            {
                throw new Exception("取得問項資料發生錯誤");
            }
            var count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var id = item.getProperty("id", "");
                var in_property = item.getProperty("in_property", "");
                switch (in_property)
                {
                    case "in_l1": cfg.l1_id = id; break;
                    case "in_l2": cfg.l2_id = id; break;
                    case "in_l3": cfg.l3_id = id; break;
                }
            }
        }
        #region 當日過磅單

        private void XlsFunc(TConfig cfg, Item itmReturn)
        {
            Item itmPrograms = GetDayPrograms(cfg);
            if (itmPrograms.isError() || itmPrograms.getItemCount() <= 0)
            {
                throw new Exception("組別資料錯誤");
            }

            var programs = MapProgram(cfg, itmPrograms);

            var exp = ExportInfo(cfg, "weight_path");
            exp.export_path = exp.export_path.Replace("meeting_excel", "meeting_weight");
            if (cfg.weigh_sort_mode != "")
            {
                exp.template_path = exp.template_path.Replace("weight_template(直立A4).xlsx", "weight_template(直立A4)-sign.xlsx");
            }

            if (!System.IO.Directory.Exists(exp.export_path))
            {
                System.IO.Directory.CreateDirectory(exp.export_path);
            }

            //總表
            CreateALLWeight(cfg, programs, exp, itmReturn);
        }

        private TExport ExportInfo(TConfig cfg, string in_name)
        {
            Item itmXls = cfg.inn.applyMethod("In_Meeting_Excel_Path", "<in_name>weight_path</in_name>");
            string export_path = itmXls.getProperty("export_path", "");
            string template_path = itmXls.getProperty("template_path", "");

            TExport result = new TExport
            {
                template_path = template_path,
                export_path = export_path,
            };

            return result;
        }

        private List<TProgram> MapProgram(TConfig cfg, Item itmPrograms)
        {
            var list = new List<TProgram>();
            var count = itmPrograms.getItemCount();
            for (int i = 0; i < count; i++)
            {
                var itmProgram = itmPrograms.getItemByIndex(i);
                var no = (i + 1).ToString().PadLeft(2, '0');
                var entity = GetNewProgram(cfg, itmProgram, no);
                list.Add(entity);
            }

            return list;
        }

        private TProgram GetNewProgram(TConfig cfg, Item itmProgram, string no)
        {
            var entity = new TProgram
            {
                no = no,
                name = itmProgram.getProperty("in_name", ""),
                name2 = itmProgram.getProperty("in_name2", ""),
                sheet_name = itmProgram.getProperty("in_short_name", ""),
                in_l1 = itmProgram.getProperty("in_l1", ""),
                in_l2 = itmProgram.getProperty("in_l2", ""),
                in_l3 = itmProgram.getProperty("in_l3", ""),
                in_fight_day = itmProgram.getProperty("in_fight_day", ""),
                in_sign_time = itmProgram.getProperty("in_sign_time", ""),
                extv = itmProgram.getProperty("in_extend_value2", ""),
            };

            entity.fight_day = GetDateTime(entity.in_fight_day).Date;
            entity.is_team = entity.in_l1 == "團體組";
            entity.min = 0;
            entity.max = 0;
            entity.max2 = 0;

            if (entity.extv != "")
            {
                var ws = entity.extv.Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);
                entity.min = ws != null && ws.Length > 0 ? GetDblVal(ws[0]) : 0;
                entity.max = ws != null && ws.Length > 1 ? GetDblVal(ws[1]) : 0;
                if (entity.max > 0 && entity.max < 999)
                {
                    entity.max2 = ((int)entity.max) * 1.05;
                }
            }
            return entity;
        }

        private void CreateALLWeight(TConfig cfg, List<TProgram> programs, TExport exp, Item itmReturn)
        {
            cfg.head_color = System.Drawing.Color.FromArgb(237, 237, 237);

            var workbook = new Spire.Xls.Workbook();
            workbook.LoadFromFile(exp.template_path);

            var sheetTemplate = workbook.Worksheets[0];

            for (int i = 0; i < programs.Count; i++)
            {
                var program = programs[i];
                if (program.sheet_name == "")
                {
                    cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "n1-n3 is null: " + program.name);
                    continue;
                }

                CreateWeightXls(cfg, program, workbook, sheetTemplate);
            }

            //移除樣板 sheet
            sheetTemplate.Remove();

            if (cfg.in_date != "")
            {
                AppendWeightSummaySheet(cfg, workbook, programs);
            }

            workbook.ActiveSheetIndex = 0;

            string ext_name = ".xlsx";

            string xls_name = "稱量體重表"
                    + "_" + cfg.mt_title
                    + "_" + DateTime.Now.ToString("_MMdd_HHmmss");

            if (cfg.in_l3 != "" && programs.Count == 1)
            {
                xls_name = "稱量體重表"
                    + "_" + programs[0].name2
                    + "_" + DateTime.Now.ToString("_MMdd_HHmmss");
            }
            else if (cfg.in_date != "")
            {
                xls_name = "稱量體重表"
                    + "_" + cfg.in_date.Replace("-", "") + "(比賽日期)"
                    + "_" + DateTime.Now.ToString("_MMdd_HHmmss");

            }

            string xls_file = exp.export_path + "\\" + xls_name + ext_name;
            string xls_url = xls_name + ext_name;

            workbook.SaveToFile(xls_file, Spire.Xls.ExcelVersion.Version2010);

            itmReturn.setProperty("xls_name", xls_url);
        }

        private void AppendWeightSummaySheet(TConfig cfg, Spire.Xls.Workbook workbook, List<TProgram> programs)
        {
            if (programs.Count == 0) return;

            var listX = new List<TProgram>();
            var listM = new List<TProgram>();
            var listF = new List<TProgram>();
            for (var i = 0; i < programs.Count; i++)
            {
                var p = programs[i];
                if (p.name.Contains("混合")) listX.Add(p);
                else if (p.name.Contains("男")) listM.Add(p);
                else if (p.name.Contains("女")) listF.Add(p);
            }

            var sheet = workbook.CreateEmptySheet();
            sheet.Name = cfg.in_date;
            sheet.MoveSheet(0);

            sheet.PageSetup.CenterHorizontally = true;
            sheet.PageSetup.TopMargin = 0.8;
            sheet.PageSetup.LeftMargin = 0.8;
            sheet.PageSetup.RightMargin = 0.8;
            sheet.PageSetup.BottomMargin = 0.8;

            sheet.PageSetup.PrintTitleRows = "$1:$2";

            var mtPos = "A1:D1";
            sheet.Range[mtPos].Merge();
            sheet.Range[mtPos].RowHeight = 42;
            SetSummaryCell(sheet, "A1", cfg.mt_title, 20);

            var dtVal = GetDateTime(cfg.in_date).AddDays(-1).ToString("yyyy-MM-dd");
            var dtPos = "A2:D2";
            sheet.Range[dtPos].Merge();
            sheet.Range[dtPos].RowHeight = 15;
            SetSummaryCell(sheet, "A2", "過磅日期: " + dtVal, 12);
            sheet.Range["A2"].Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Right;

            cfg.row_index = 3;
            AppendWeightSummayBlock(cfg, sheet, listX, "混合");
            AppendWeightSummayBlock(cfg, sheet, listM, "男子過磅");
            AppendWeightSummayBlock(cfg, sheet, listF, "女子過磅");

            sheet.Columns[0].ColumnWidth = 13;
            sheet.Columns[1].ColumnWidth = 24;
            sheet.Columns[2].ColumnWidth = 29;
            sheet.Columns[3].ColumnWidth = 13;
        }

        private void AppendWeightSummayBlock(TConfig cfg, Spire.Xls.Worksheet sheet, List<TProgram> list, string subTitle)
        {
            if (list == null || list.Count == 0) return;

            var ri = cfg.row_index;
            var rn = ri;

            var posX = "A" + ri + ":" + "D" + ri;
            sheet.Range[posX].Merge();
            sheet.Range[posX].RowHeight = 32;
            SetSummaryCell(sheet, posX, subTitle, 14);
            sheet.Range[posX].Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Left;
            ri++;

            SetSummaryCell(sheet, "A" + ri, "競賽項目", 14);
            SetSummaryCell(sheet, "B" + ri, "競賽組別", 14);
            SetSummaryCell(sheet, "C" + ri, "競賽量級", 14);
            SetSummaryCell(sheet, "D" + ri, "過磅人數", 14);
            ri++;
            var total = 0;
            for (var i = 0; i < list.Count; i++)
            {
                var p = list[i];
                var l2 = p.in_l2.Replace("個-", "").Replace("團-", "");

                SetSummaryCell(sheet, "A" + ri, p.in_l1, 14);
                SetSummaryCell(sheet, "B" + ri, l2, 14);
                SetSummaryCell(sheet, "C" + ri, p.in_l3, 14);
                SetSummaryCell(sheet, "D" + ri, "", 14);
                sheet.Range["D" + ri].NumberValue = p.in_name_count;

                if (i % 2 > 0)
                {
                    SetSummaryColor(cfg, sheet, "A" + ri + ":" + "D" + ri);
                }

                ri++;


                total += p.in_name_count;
            }

            var endPos = "A" + ri + ":" + "C" + ri;
            sheet.Range[endPos].Merge();

            SetSummaryCell(sheet, "A" + ri, "合計", 14);
            SetSummaryCell(sheet, "D" + ri, "", 14);
            sheet.Range["D" + ri].NumberValue = total;
            ri++;

            var pos = "A" + (rn + 1) + ":" + "D" + (ri - 1);
            SetRangeBorder(sheet, pos);

            ri++;
            cfg.row_index = ri;
        }

        private void SetSummaryColor(TConfig cfg, Spire.Xls.Worksheet sheet, string pos)
        {
            var range = sheet.Range[pos];
            range.Style.Color = cfg.head_color;
        }

        private void SetSummaryCell(Spire.Xls.Worksheet sheet, string pos, string value, int fontSize)
        {
            var range = sheet.Range[pos];
            range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
            range.Style.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;
            range.Text = value;
            range.Style.Font.Size = fontSize;
            range.Style.Font.FontName = "標楷體";
        }

        private void CreateWeightXls(TConfig cfg, TProgram program, Spire.Xls.Workbook workbook, Spire.Xls.Worksheet sheetTemplate)
        {
            var itmMUsers = GetMUsers(cfg, program.in_l1, program.in_l2, program.in_l3, program.in_sign_time);
            if (itmMUsers.isError() || itmMUsers.getItemCount() < 0)
            {
                return;
            }

            var fields = new List<TField>();
            if (cfg.need_judo_no)
            {
                fields.Add(new TField { RS = "A", RE = "A", property = "in_judo_no", fontName = "Times New Roman", fontSize = 12, fontBold = true, is_num = true });
            }
            else
            {
                fields.Add(new TField { RS = "A", RE = "A", property = "cno", fontName = "Times New Roman", fontSize = 12, fontBold = true, is_num = true });
            }

            fields.Add(new TField { RS = "B", RE = "B", property = "in_name", fontName = "新細明體", fontSize = 14 });
            fields.Add(new TField { RS = "C", RE = "C", property = "in_short_org", fontName = "新細明體", fontSize = 16 });
            fields.Add(new TField { RS = "D", RE = "D", property = "in_weight", fontName = "Times New Roman", fontSize = 12, is_num = true });
            fields.Add(new TField { RS = "E", RE = "F", property = "space4", fontName = "Times New Roman", fontSize = 12 });
            fields.Add(new TField { RS = "G", RE = "G", property = "in_weight_note", fontName = "Times New Roman", fontSize = 10 });
            if (cfg.need_judo_no)
            {
                fields.Add(new TField { RS = "H", RE = "H", property = "cno", fontName = "Times New Roman", fontSize = 10 });
            }

            var map = GetWeightRecords(cfg, program.in_l1, program.in_l2, program.in_l3);
            SetSectionTitle(cfg, program, itmMUsers);

            var sheet = workbook.CreateEmptySheet();
            sheet.CopyFrom(sheetTemplate);
            sheet.Name = program.sheet_name;


            sheet.Range["A1"].Text = cfg.mt_title;
            sheet.Range["C5"].Text = program.sect_name;

            int wsRow = 8;
            int count = itmMUsers.getItemCount();
            for (int i = 0; i < count; i++)
            {
                var itmMUser = itmMUsers.getItemByIndex(i);
                var x = ConvertMUser(cfg, itmMUser, i);
                itmMUser.setProperty("in_name", x.display_name);

                //取得上次過磅體重
                if (cfg.NoNote)
                {
                    itmMUser.setProperty("in_weight_note", "");
                }
                else
                {
                    itmMUser.setProperty("in_weight_note", GetWeightNote2(cfg, program, x, map, Environment.NewLine));
                }

                bool long_name = x.in_name.Contains("(");
                int length = x.in_name.Length;

                int height = long_name ? 36 : 21;
                if (length >= 12) height = 45;

                itmMUser.setProperty("cno", (i + 1).ToString());

                if (x.in_team != "")
                {
                    itmMUser.setProperty("in_short_org", x.in_short_org + " " + x.in_team);
                }

                int ctRow = wsRow + i;

                sheet.Range["E" + ctRow + ":F" + ctRow].Merge();
                sheet.Rows[ctRow - 1].RowHeight = height;

                for (int j = 0; j < fields.Count; j++)
                {
                    var field = fields[j];
                    string value = "";

                    if (long_name)
                    {
                        value = itmMUser.getProperty(field.property, "").Replace("(", Environment.NewLine + "(");
                    }
                    else
                    {
                        value = itmMUser.getProperty(field.property, "");
                    }

                    var pos = field.RS + ctRow;
                    var range = sheet.Range[pos];

                    if (value == "")
                    {

                    }
                    else if (field.is_num)
                    {
                        range.NumberValue = GetDblVal(value);
                        range.NumberFormat = "#,##0.00";
                    }
                    else
                    {
                        range.Text = value;
                    }

                    range.Text = value;
                    range.Style.Font.FontName = field.fontName;
                    range.Style.Font.Size = field.fontSize;
                    range.Style.Font.IsBold = field.fontBold;
                    range.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                }

                //簽名欄
                var posSign = "E" + ctRow;
                sheet.Range[posSign].Text = x.need_weight;
                sheet.Range[posSign].HorizontalAlignment = Spire.Xls.HorizontalAlignType.Right;
                sheet.Range[posSign].Style.Font.FontName = "Segoe UI Symbol";

                var ri = ctRow;
                if (i % 2 > 0)
                {
                    // if (cfg.need_judo_no)
                    // {
                    //     SetSummaryColor(cfg, sheet, "A" + ri + ":" + "H" + ri);
                    // }
                    // else
                    // {
                    //     SetSummaryColor(cfg, sheet, "A" + ri + ":" + "G" + ri);
                    // }
                }

                var in_name = itmMUser.getProperty("in_name", "");
                var in_short_org = itmMUser.getProperty("in_short_org", "");
                var in_weight_note = itmMUser.getProperty("in_weight_note", "");
                if (in_name.Length >= 6)
                {
                    sheet.Range["B" + ri].Style.Font.Size = 12;
                }
                if (in_short_org.Length > 6)
                {
                    sheet.Range["C" + ri].Style.Font.Size = 14;
                }
                if (in_weight_note.Length > 4)
                {
                    sheet.Range["G" + ri].Style.Font.Size = 8;
                }
            }

            var minRow = wsRow;
            var maxRow = wsRow + count - 1;

            var fs = fields.First();
            var fe = fields.Last();

            var tbl_pos = fs.RS + minRow + ":" + fe.RE + maxRow;
            SetRangeBorder(sheet, tbl_pos);
        }

        private void SetSectionTitle(TConfig cfg, TProgram pg, Item itmMUsers)
        {
            var itmMUser = itmMUsers.getItemByIndex(0);
            var sectName = itmMUser.getProperty("in_section_name");
            var count = itmMUsers.getItemCount();

            pg.in_name_count = count;

            if (!pg.is_team && pg.extv != "")
            {
                var v = pg.extv.Replace("-999", "以上");
                if (v.StartsWith("0-")) v = pg.extv.Replace("0-", "") + "以下";
                sectName += " (" + v + ")";
            }

            sectName = sectName
                .Replace("(不分段級)", "")
                .Replace("(未上段)", "")
                .Replace("(上段)", "")
                .Replace("個-", "")
                .Replace("公斤", "kg");

            if (sectName.Contains("個人組-國小"))
            {
                sectName = sectName.Replace("個人組-", "");
            }

            sectName = sectName + " " + count + "人";

            pg.sect_name = sectName.Replace("：", "：" + Environment.NewLine);

            if (cfg.weigh_sort_mode != "" && pg.max2 > 0 && !pg.is_team)
            {
                pg.sect_name += Environment.NewLine + "5% = " + pg.max2;
            }

        }

        private void SetRangeBorder(Spire.Xls.Worksheet sheet, string pos)
        {
            var range = sheet.Range[pos];
            range.BorderInside(Spire.Xls.LineStyleType.Thin, System.Drawing.Color.Black);
            range.BorderAround(Spire.Xls.LineStyleType.Thin, System.Drawing.Color.Black);

            //lina 2021.11.29: 有 bug，合併儲存格加上格式會有錯亂格式
            // rg.Style.Borders[Spire.Xls.BordersLineType.EdgeTop].LineStyle = Spire.Xls.LineStyleType.Thin;
            // rg.Style.Borders[Spire.Xls.BordersLineType.EdgeRight].LineStyle = Spire.Xls.LineStyleType.Thin;
            // rg.Style.Borders[Spire.Xls.BordersLineType.EdgeBottom].LineStyle = Spire.Xls.LineStyleType.Thin;
            // rg.Style.Borders[Spire.Xls.BordersLineType.EdgeLeft].LineStyle = Spire.Xls.LineStyleType.Thin;
        }

        private Item GetOneProgramItem(TConfig cfg, string in_l1, string in_l2, string in_l3)
        {
            var sql = @"
                SELECT 
                    t1.* 
                	, t2.in_extend_value2
                FROM 
                    IN_MEETING_PROGRAM t1 WITH(NOLOCK) 
                LEFT OUTER JOIN
                	IN_SURVEY_OPTION t2 WITH(NOLOCK)
                	ON t2.source_id = '{#svy_id}'
                	AND ISNULL(t2.in_grand_filter, '') = t1.in_l1
                	AND ISNULL(t2.in_filter, '') = t1.in_l2
                	AND ISNULL(t2.in_value, '') = t1.in_l3
                WHERE 
                    t1.in_meeting = '{#meeting_id}'
                    AND t1.in_l1 = '{#in_l1}'
                    AND t1.in_l2 = '{#in_l2}'
                    AND t1.in_l3 = '{#in_l3}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#svy_id}", cfg.l3_id)
                .Replace("{#in_l1}", in_l1)
                .Replace("{#in_l2}", in_l2)
                .Replace("{#in_l3}", in_l3);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        private Item GetDayPrograms(TConfig cfg)
        {
            var sql = @"
                SELECT 
                    t1.* 
                	, t2.in_extend_value2
                FROM 
                    IN_MEETING_PROGRAM t1 WITH(NOLOCK) 
                LEFT OUTER JOIN
                	IN_SURVEY_OPTION t2 WITH(NOLOCK)
                	ON t2.source_id = '{#svy_id}'
                	AND ISNULL(t2.in_grand_filter, '') = t1.in_l1
                	AND ISNULL(t2.in_filter, '') = t1.in_l2
                	AND ISNULL(t2.in_value, '') = t1.in_l3
                WHERE 
                    t1.in_meeting = '{#meeting_id}'
            ";

            if (cfg.l3_id == "")
            {
                sql = @"
                    SELECT 
                        t1.* 
                    FROM 
                        IN_MEETING_PROGRAM t1 WITH(NOLOCK) 
                    WHERE 
                        t1.in_meeting = '{#meeting_id}'
                ";
                sql = sql.Replace("{#meeting_id}", cfg.meeting_id);
            }
            else
            {
                sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                    .Replace("{#svy_id}", cfg.l3_id);
            }


            if (cfg.in_date != "") sql += " AND t1.in_fight_day = '" + cfg.in_date + "'";

            if (cfg.in_l1 != "") sql += " AND t1.in_l1 = N'" + cfg.in_l1 + "'";
            if (cfg.in_l2 != "") sql += " AND t1.in_l2 = N'" + cfg.in_l2 + "'";
            if (cfg.in_l3 != "") sql += " AND t1.in_l3 = N'" + cfg.in_l3 + "'";
            sql += " ORDER BY t1.in_sort_order";

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            return cfg.inn.applySQL(sql);
        }

        #endregion 當日過磅單

        private void PageFunc(TConfig cfg, Item itmReturn)
        {
            if (cfg.program_id != "" && cfg.in_l1 == "")
            {
                cfg.itmProgram = cfg.inn.applySQL("SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + cfg.program_id + "'");
                if (!cfg.itmProgram.isError() && cfg.itmProgram.getResult() != "")
                {
                    cfg.in_l1 = cfg.itmProgram.getProperty("in_l1", "");
                    cfg.in_l2 = cfg.itmProgram.getProperty("in_l2", "");
                    cfg.in_l3 = cfg.itmProgram.getProperty("in_l3", "");
                    itmReturn.setProperty("l1", cfg.in_l1);
                    itmReturn.setProperty("l2", cfg.in_l2);
                    itmReturn.setProperty("l3", cfg.in_l3);

                    cfg.in_fight_day = cfg.itmProgram.getProperty("in_fight_day", "");
                    cfg.in_weight = cfg.itmProgram.getProperty("in_weight", "").ToLower().Trim();
                    cfg.fight_day = GetDateTime(cfg.in_fight_day);
                }
            }
            else
            {
                cfg.itmProgram = GetProgram(cfg);
                cfg.program_id = cfg.itmProgram.getProperty("id", "");

                cfg.in_fight_day = cfg.itmProgram.getProperty("in_fight_day", "");
                cfg.in_weight = cfg.itmProgram.getProperty("in_weight", "").ToLower().Trim();
                cfg.fight_day = GetDateTime(cfg.in_fight_day);
            }

            cfg.pg_sign_time = cfg.itmProgram.getProperty("in_sign_time", "");
            cfg.has_sign_no = cfg.pg_sign_time != "";
            cfg.has_fight_day = cfg.in_date != "";

            if (cfg.mode == "list")
            {
                ListPage(cfg, itmReturn);
                itmReturn.setProperty("pg_fight_day", cfg.in_fight_day);
            }
            else if (cfg.mode == "related")
            {
                //分組統計
                Item itmSurveyLevel = GetSurveyLevels(cfg);
                AppendGroupingStatistics(cfg, itmSurveyLevel, itmReturn);

                //附加選單資訊
                Item itmJson = cfg.inn.newItem("In_Meeting");
                itmJson.setProperty("meeting_id", cfg.meeting_id);
                itmJson.setProperty("need_id", "1");
                itmJson = itmJson.apply("in_meeting_program_options");
                itmReturn.setProperty("in_level_json", itmJson.getProperty("json", ""));
            }
            else
            {
                //附加比賽日期選單
                AppendFightMenu(cfg, itmReturn);

                //分組統計
                Item itmSurveyLevel = GetSurveyLevels(cfg);
                AppendGroupingStatistics(cfg, itmSurveyLevel, itmReturn);

                //附加選單資訊
                Item itmJson = cfg.inn.newItem("In_Meeting");
                itmJson.setProperty("meeting_id", cfg.meeting_id);
                itmJson.setProperty("need_id", "1");
                itmJson = itmJson.apply("in_meeting_program_options");
                itmReturn.setProperty("in_level_json", itmJson.getProperty("json", ""));
            }
        }

        #region 附加比賽日期選單

        //附加比賽日期選單
        private void AppendFightMenu(TConfig cfg, Item itmReturn)
        {
            Item itmDays = GetFightDays(cfg);
            List<TOpt> day_opts = MapOptList(cfg, itmDays, IsDayMatch, "請選擇比賽日期");
            string day_ctrl = ToSelectCtrl(cfg, day_opts, "date_menu", "FightDay_Change(this)");
            itmReturn.setProperty("inn_fight_days", day_ctrl);

            if (cfg.has_fight_day)
            {
                Item itmPrograms = GetFightPrograms(cfg);
                List<TOpt> program_opts = MapOptList(cfg, itmPrograms, IsProgramMatch, "請選擇組別");
                string program_ctrl = ToSelectCtrl(cfg, program_opts, "program_menu", "FightProgram_Change(this)");
                itmReturn.setProperty("inn_fight_programs", program_ctrl);
            }
        }

        private Item GetFightPrograms(TConfig cfg)
        {
            string sql = @"
                SELECT 
	                t1.id AS 'value'
	                , t1.in_name2 +' (' + CAST(t1.in_team_count AS VARCHAR) + ')' AS 'text'
	                , t1.in_l1 + '@' + t1.in_l2 + '@' + t1.in_l3 AS 'ext'
	                , t1.in_l2 AS 'group'
                FROM 
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
				INNER JOIN
					IN_MEETING_ALLOCATION t2 WITH(NOLOCK)
					ON t2.in_program = t1.id
				INNER JOIN
					IN_MEETING_SITE t3 WITH(NOLOCK)
					ON t3.id = t2.in_site
                WHERE 
	                t1.in_meeting = '{#meeting_id}' 
	                AND t1.in_fight_day = '{#in_fight_day}'
                ORDER BY 
	                t1.in_sort_order
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_fight_day}", cfg.in_date);

            return cfg.inn.applySQL(sql);
        }

        private Item GetFightDays(TConfig cfg)
        {
            string sql = @"
                SELECT DISTINCT
	                in_fight_day   AS 'value'
	                , in_fight_day AS 'text'
                FROM 
	                IN_MEETING_PROGRAM WITH(NOLOCK) 
                WHERE 
	                in_meeting = '{#meeting_id}' 
	                AND ISNULL(in_fight_day, '') <> '' 
                ORDER BY 
	                in_fight_day
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            return cfg.inn.applySQL(sql);
        }

        private string ToSelectCtrl(TConfig cfg, List<TOpt> list, string id, string onchange)
        {
            StringBuilder options = new StringBuilder();

            foreach (var opt in list)
            {
                if (opt.Children != null)
                {
                    options.Append("<optgroup label='" + opt.group + "'>");
                    foreach (var child in opt.Children)
                    {
                        options.Append("<option data-ext='" + child.ext + "'"
                            + " value='" + child.value + "' " + child.selected + ">" + child.text + "</option>");
                    }
                    options.Append("</optgroup>");
                }
                else
                {
                    options.Append("<option data-ext='" + opt.ext + "'"
                        + " value='" + opt.value + "' " + opt.selected + ">" + opt.text + "</option>");
                }
            }

            StringBuilder builder = new StringBuilder();
            builder.Append("<select id='" + id + "' class='form-control' onchange='" + onchange + "'>");
            builder.Append(options);
            builder.Append("</select>");
            return builder.ToString();
        }

        private bool IsDayMatch(TConfig cfg, TOpt opt)
        {
            if (cfg.has_fight_day)
            {
                return opt.value == cfg.in_date;
            }
            else
            {
                return opt.value == DateTime.Now.ToString("yyyy-MM-dd");
            }
        }

        private bool IsProgramMatch(TConfig cfg, TOpt opt)
        {
            return opt.value == cfg.program_id;
        }

        private List<TOpt> MapOptList(TConfig cfg, Item items, Func<TConfig, TOpt, bool> is_match, string def_opt_text)
        {
            List<TOpt> result = new List<TOpt>();

            result.Add(new TOpt
            {
                text = def_opt_text,
                value = "",
                selected = "",
            });

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string gp = item.getProperty("group", "");

                if (gp.Contains("團-"))
                {
                    gp = "團體組";
                }
                else
                {
                    gp = gp.Replace("個-", "");
                }

                if (gp != "")
                {
                    TOpt group = result.Find(x => x.group == gp);
                    if (group == null)
                    {
                        group = new TOpt
                        {
                            group = gp,
                            Children = new List<TOpt>(),
                        };
                        result.Add(group);
                    }

                    TOpt opt = new TOpt
                    {
                        value = item.getProperty("value", ""),
                        text = item.getProperty("text", ""),
                        ext = item.getProperty("ext", ""),
                    };

                    opt.selected = is_match(cfg, opt) ? "selected" : "";

                    group.Children.Add(opt);
                }
                else
                {
                    TOpt opt = new TOpt
                    {
                        value = item.getProperty("value", ""),
                        text = item.getProperty("text", ""),
                        ext = item.getProperty("ext", ""),
                    };

                    opt.selected = is_match(cfg, opt) ? "selected" : "";

                    result.Add(opt);
                }
            }

            return result;
        }

        private class TOpt
        {
            public string group { get; set; }
            public string text { get; set; }
            public string value { get; set; }
            public string ext { get; set; }
            public string selected { get; set; }
            public List<TOpt> Children { get; set; }
        }

        #endregion 附加比賽日期選單

        private class TMUser
        {
            public string no { get; set; }
            public string id { get; set; }
            public string in_stuff_b1 { get; set; }
            public string in_short_org { get; set; }
            public string in_show_org { get; set; }
            public string in_name { get; set; }
            public string in_sno { get; set; }
            public string in_gender { get; set; }
            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }
            public string in_team { get; set; }
            public string in_sign_no { get; set; }
            public string in_judo_no { get; set; }
            public string in_section_no { get; set; }
            public string n2 { get; set; }

            public string sect_full { get; set; }
            public string in_weight { get; set; }
            public string in_weight_result { get; set; }
            public string in_weight_status { get; set; }
            public string weight_class { get; set; }
            public string rollcall { get; set; }

            public string display_name { get; set; }
            public string display_org { get; set; }

            public int group_y { get; set; }
            public int group_n { get; set; }
            public int group_null { get; set; }

            public string need_weight { get; set; }
            public bool yestoday_weighted { get; set; }

        }

        private TMUser ConvertMUser(TConfig cfg, Item itmMUser, int index)
        {
            var x = new TMUser();
            x.no = (index + 1).ToString();
            x.id = itmMUser.getProperty("id", "");
            x.in_name = itmMUser.getProperty("in_name", "");
            x.in_sno = itmMUser.getProperty("in_sno", "").ToUpper();
            x.in_gender = itmMUser.getProperty("in_gender", "");
            x.in_l1 = itmMUser.getProperty("in_l1", "");
            x.in_l2 = itmMUser.getProperty("in_l2", "");
            x.in_l3 = itmMUser.getProperty("in_l3", "");
            x.sect_full = x.in_l1 + x.in_l2 + x.in_l3;
            x.in_weight = itmMUser.getProperty("in_weight", "");
            x.in_weight_result = itmMUser.getProperty("in_weight_result", "");
            x.in_weight_status = itmMUser.getProperty("in_weight_status", "");
            x.weight_class = itmMUser.getProperty("in_extend_value2", "");
            x.rollcall = itmMUser.getProperty("rollcall", "");

            x.in_stuff_b1 = itmMUser.getProperty("in_stuff_b1", "");
            x.in_short_org = itmMUser.getProperty("in_short_org", "");
            x.in_show_org = itmMUser.getProperty("in_show_org", "");
            x.in_team = itmMUser.getProperty("in_team", "");

            x.in_sign_no = itmMUser.getProperty("in_sign_no", "");
            x.in_judo_no = itmMUser.getProperty("in_judo_no", "");
            x.in_section_no = itmMUser.getProperty("in_section_no", "");

            x.n2 = itmMUser.getProperty("in_team_weight_n2", "").ToUpper();

            x.display_name = x.in_name;
            if (x.sect_full.Contains("混合")) x.display_name += "(" + x.in_gender + ")";
            if (x.in_show_org != "") x.display_name += " / " + x.in_show_org;

            //string in_stuff_b1 = itmMUser.getProperty("in_stuff_b1", "");
            //string in_team = itmMUser.getProperty("in_team", "");
            //string in_short_org = itmMUser.getProperty("in_short_org", "");

            var orgWords = new List<string>();
            if (x.in_stuff_b1 != "") orgWords.Add(x.in_stuff_b1);
            if (x.in_short_org != "") orgWords.Add(x.in_short_org);
            if (x.in_team != "") orgWords.Add(x.in_team);
            x.display_org = string.Join(" ", orgWords);

            return x;
        }

        private void ListPage(TConfig cfg, Item itmReturn)
        {
            if (cfg.program_id != "" && cfg.has_sign_no)
            {
                //更新與會者籤號
                FixMUserSignNo(cfg);
            }

            var itmProgram = GetOneProgramItem(cfg, cfg.in_l1, cfg.in_l2, cfg.in_l3);
            var program = GetNewProgram(cfg, itmProgram, "01");

            var itmMUsers = GetMUsers(cfg, program.in_l1, program.in_l2, program.in_l3, program.in_sign_time);
            if (itmMUsers.isError() || itmMUsers.getResult() == "")
            {
                return;
            }

            var map = GetWeightRecords(cfg, cfg.in_l1, cfg.in_l2, cfg.in_l3);

            int count = itmMUsers.getItemCount();
            int group_y = 0;
            int group_n = 0;
            int group_null = 0;

            for (int i = 0; i < count; i++)
            {
                var itmMUser = itmMUsers.getItemByIndex(i);
                itmMUser.setType("inn_groupList");

                var x = ConvertMUser(cfg, itmMUser, i);

                itmMUser.setProperty("in_name", x.display_name);
                itmMUser.setProperty("in_short_org", x.display_org);
                itmMUser.setProperty("in_weight", GetWeightInput(cfg, x));
                itmMUser.setProperty("in_weight_status", GetWeightStatus(cfg, x));
                itmMUser.setProperty("in_weight_result", GetWeightResult(cfg, x));
                itmMUser.setProperty("in_rollcall_result", GetRollResult(cfg, x));
                itmMUser.setProperty("in_check", GetSaveButton(cfg, x));

                //排列序號，抽籤後會變為柔道籤號
                if (cfg.need_judo_no)
                {
                    itmMUser.setProperty("no", (i + 1).ToString());
                    itmMUser.setProperty("in_judo_no", x.in_judo_no);
                    itmMUser.setProperty("in_section_no", x.in_judo_no);
                }
                else if (cfg.has_sign_no)
                {
                    itmMUser.setProperty("no", (i + 1).ToString());
                    if (x.in_sign_no == "0" || x.in_sign_no == "")
                    {
                        itmMUser.setProperty("in_section_no", "-");
                    }
                    else
                    {
                        itmMUser.setProperty("in_section_no", x.in_section_no);
                    }
                }
                else
                {
                    itmMUser.setProperty("no", (i + 1).ToString());
                    itmMUser.setProperty("in_section_no", "-");
                }

                switch (cfg.in_l1)
                {
                    case "團體組": program.is_team = true; break;
                    case "個人組": program.is_solo = true; break;
                }

                //取得上次過磅體重
                itmMUser.setProperty("in_weight_note", GetWeightNoteMain(cfg, program, x, map, "<br>"));
                itmMUser.setProperty("need_weight", x.need_weight);

                itmReturn.addRelationship(itmMUser);
            }

            itmReturn.setProperty("group_c", count.ToString());
            itmReturn.setProperty("group_y", group_y.ToString());
            itmReturn.setProperty("group_n", group_n.ToString());
            itmReturn.setProperty("group_null", group_null.ToString());
        }

        private string GetWeightInput(TConfig cfg, TMUser x)
        {
            return "<input tabindex='" + x.no + "' data-value='" + x.in_weight + "' id='" + x.in_sno + "' style='width:100%' class='eventKD' type='number' value='" + x.in_weight + "' >";
        }


        private string GetWeightStatus(TConfig cfg, TMUser x)
        {
            var menu = GetStatusMenu();

            switch (x.in_weight_status)
            {
                case "on":
                    menu.On.IsSelected = true;
                    break;

                case "leave":
                    menu.Leave.IsSelected = true;
                    break;

                case "off":
                    menu.Off.IsSelected = true;
                    break;

                case "dq":
                    menu.DQ.IsSelected = true;
                    break;

                case "king":
                    menu.King.IsSelected = true;
                    break;

                default:
                    break;
            }

            var builder = new StringBuilder();

            builder.Append("<select id='w_status_" + x.in_sno + "' class='form-control weight-status'"
                + " data-id='" + x.id + "'"
                + " data-old='" + x.in_weight_status + "'"
                + " onchange='WeightStatus_Change(this)'>");
            builder.Append("<option value=''>--</option>");
            builder.Append(GetStatusOption(menu.Leave));
            builder.Append(GetStatusOption(menu.Off));
            builder.Append(GetStatusOption(menu.DQ));
            builder.Append(GetStatusOption(menu.On));
            builder.Append(GetStatusOption(menu.King));
            builder.Append("</select>");

            return builder.ToString();
        }

        private string GetSaveButton(TConfig cfg, TMUser x)
        {
            return "<button class='btn btn-sm btn-default' data-sno='" + x.in_sno + "' onclick='SendWeight(this)' ><i class='fa fa-save'></i></button>";
        }

        private string GetRollResult(TConfig cfg, TMUser x)
        {
            if (x.rollcall == "1")
            {
                return "<font color='green'><b>已檢錄</b></font>";
            }
            else
            {
                return "<font color='darkgoldenrod'><b>未檢錄</b></font>";
            }
        }

        private string GetWeightResult(TConfig cfg, TMUser x)
        {
            var result = "";
            //未設定體重限制
            if (x.weight_class == "")
            {
                if (x.in_weight != "")
                {
                    result = "<font id='" + x.in_sno + "_" + "result" + "'>-</font>";
                }
                else if (x.in_weight_result == "0")
                {
                    result = "<font id='" + x.in_sno + "_" + "result" + "' color='red'><b>未過磅</b></font>";
                }
                else
                {
                    result = "<font id='" + x.in_sno + "_" + "result" + "' color='darkgoldenrod'><b>未過磅</b></font>";
                    x.group_null++;
                }
            }
            else
            {
                if (x.in_weight != "" || x.in_weight_result != "")
                {
                    if (x.in_weight_result == "1")
                    {
                        result = "<font id='" + x.in_sno + "_" + "result" + "' color='green'><b>過磅合格</b></font>";
                        x.group_y++;
                    }
                    else
                    {
                        if (x.in_weight_status == "off")
                        {
                            x.in_weight = "未到";
                        }
                        else if (x.in_weight_status == "leave")
                        {
                            x.in_weight = "請假";
                        }
                        else if (x.in_weight == "")
                        {
                            x.in_weight = "未過磅";
                        }
                        else
                        {
                            x.in_weight = "體重不符";
                        }
                        result = "<font id='" + x.in_sno + "_" + "result" + "' color='red'><b>" + x.in_weight + "</b></font>";
                        x.group_n++;
                    }
                }
                else
                {
                    result = "<font id='" + x.in_sno + "_" + "result" + "' color='darkgoldenrod'><b>未過磅</b></font>";
                    x.group_null++;
                }
            }
            return result;
        }

        private string GetWeightNoteMain(TConfig cfg, TProgram program, TMUser x, Dictionary<string, TPlayer> map, string line_char)
        {
            List<string> list = null;
            if (cfg.is_xlsx)
            {
                list = GetWeightNoteXlsx(cfg, program, x, map, line_char);
            }
            else
            {
                list = GetWeightNotePage(cfg, program, x, map, line_char);
            }
            return string.Join(line_char, list);
        }

        private List<string> GetWeightNoteXlsx(TConfig cfg, TProgram program, TMUser x, Dictionary<string, TPlayer> map, string line_char)
        {
            x.need_weight = "";
            var lines = new List<string>();
            if (!map.ContainsKey(x.in_sno)) return lines;

            TPlayer player = map[x.in_sno];
            if (program.is_team && !player.HasSolo)
            {
                lines.Add("未報個人組");
            }

            var sorted = player.WeightRecords.OrderByDescending(w => w.pg_fight_day).ToList();
            foreach (var w in sorted)
            {
                var fday = w.pg_fight_day;//比賽日期
                var wday = w.show_weight_time;//過磅時間
                if (w.in_weight == "") w.in_weight = w.status;

                if (fday == "")
                {
                    lines.Add(wday + " " + w.pg_short_name + " " + w.in_weight);
                    continue;
                }

                var dt = GetDateTime(fday);
                var dtMin = dt.Date;
                var dtMax1 = dt.AddDays(1).Date;
                var dtMax2 = dt.AddDays(2).Date;
                var dtMax3 = dt.AddDays(3).Date;

                if (program.fight_day == dtMin)
                {
                    //同日過磅
                    lines.Add(w.show_weight_time + " " + w.pg_short_name + " " + w.in_weight);
                }
                else if (program.fight_day >= dtMin && program.fight_day <= dtMax1)
                {
                    CheckYestodayWeightB(cfg, program, x, w, lines);//昨日過磅
                    lines.Add(w.show_weight_time + " " + w.pg_short_name + " " + w.in_weight);
                }
                else if (program.fight_day >= dtMin && program.fight_day <= dtMax2)
                {
                    Check2DayBeforeWeightB(cfg, program, x, w, lines);//兩日前過磅
                    lines.Add(w.show_weight_time + " " + w.pg_short_name + " " + w.in_weight);
                }
                else if (program.fight_day >= dtMin && program.fight_day <= dtMax3)
                {
                    //三日前過磅
                    lines.Add(w.show_weight_time + " " + w.pg_short_name + " " + w.in_weight);
                }
                else
                {
                    lines.Add(w.show_weight_time + " " + w.pg_short_name + " " + w.in_weight);
                }
            }
            return lines;
        }

        private List<string> GetWeightNotePage(TConfig cfg, TProgram program, TMUser x, Dictionary<string, TPlayer> map, string line_char)
        {
            x.need_weight = "應";
            x.yestoday_weighted = false;

            var lines = new List<string>();
            if (!map.ContainsKey(x.in_sno)) return lines;

            TPlayer player = map[x.in_sno];
            if (program.is_team && !player.HasSolo)
            {
                lines.Add("<span style='color: red'>未報個人組</span>");
            }

            var sorted = player.WeightRecords.OrderByDescending(w => w.pg_fight_day).ToList();
            foreach (var w in sorted)
            {
                var fday = w.pg_fight_day;//比賽日期
                var wday = w.show_weight_time;//過磅時間
                if (w.in_weight == "") w.in_weight = w.status;

                if (wday == "")
                {
                    lines.Add(w.w.pg_link);
                }
                else
                {
                    var dt = w.fight_day;
                    var dtMin = dt.Date;
                    var dtMax1 = dt.AddDays(1).Date;
                    var dtMax2 = dt.AddDays(2).Date;
                    var dtMax3 = dt.AddDays(3).Date;

                    var ie = "<i class='fa fa-times-circle inn_red'></i>";
                    var ic = " <i class='fa fa-copy inn_green' data-wv='" + w.in_weight + "' onclick='cloneDayWeight(this)'></i>";

                    if (w.in_weight_status == "off")
                    {
                        lines.Add(GetWeigthSpan(w, "", "", ie));
                    }
                    else if (program.fight_day == dtMin)
                    {
                        lines.Add(GetWeigthSpan(w, "同日過磅", "sday", ic));
                    }
                    else if (program.fight_day >= dtMin && program.fight_day <= dtMax1)
                    {
                        CheckYestodayWeightA(cfg, program, x, w, lines);
                        lines.Add(GetWeigthSpan(w, "昨日過磅", "yday", ic));
                    }
                    else if (program.fight_day >= dtMin && program.fight_day <= dtMax2)
                    {
                        Check2DayBeforeWeightA(cfg, program, x, w, lines);
                        lines.Add(GetWeigthSpan(w, "兩日前過磅", "2Day " + cfg.weigh_even_increase, ic));
                    }
                    else if (program.fight_day >= dtMin && program.fight_day <= dtMax3)
                    {
                        lines.Add(GetWeigthSpan(w, "三日前過磅", "3Day " + cfg.weigh_even_increase, ic));
                    }
                    else
                    {
                        lines.Add(GetWeigthSpan(w, "", "", ic));
                    }
                }
            }
            return lines;
        }

        //兩日前過磅
        private void Check2DayBeforeWeightA(TConfig cfg, TProgram program, TMUser x, TWeight w, List<string> lines)
        {
            if (!x.yestoday_weighted)
            {
                if (program.is_team)
                {
                    x.need_weight = "<span style='color: red' title='要過磅，但有5%寬限'>５％</span><BR>";
                }
                else if (program.max2 <= 0)
                {
                    x.need_weight = "<span style='color: red' title='要過磅'>應</span><BR>";
                }
                else
                {
                    x.need_weight = "<span style='color: red' title='要過磅，但有5%寬限'>５％</span><BR><span>" + program.max2 + "</span>";
                }
            }
        }

        //兩日前過磅
        private void Check2DayBeforeWeightB(TConfig cfg, TProgram program, TMUser x, TWeight w, List<string> lines)
        {
            if (!x.yestoday_weighted)
            {
                if (w.wv <= 0)
                {

                }
                else
                {
                    x.need_weight = "％";
                }
            }
        }

        //昨日前過磅
        private void CheckYestodayWeightB(TConfig cfg, TProgram program, TMUser x, TWeight w, List<string> lines)
        {
            if (program.is_team)
            {
                if (w.wv > 0)
                {
                    x.yestoday_weighted = true;
                    x.need_weight = "Ⅴ";
                }
            }
            else if (w.wv <= 0)
            {
                x.yestoday_weighted = false;
                x.need_weight = "";
            }
            else if (w.wv >= program.min && w.wv <= program.max)
            {
                x.yestoday_weighted = true;
                x.need_weight = "Ⅴ";
            }
            else
            {
                x.yestoday_weighted = true;
                x.need_weight = "△";
            }
        }


        //昨日前過磅
        private void CheckYestodayWeightA(TConfig cfg, TProgram program, TMUser x, TWeight w, List<string> lines)
        {
            var wv = GetDblVal(w.in_weight);
            if (program.is_team)
            {
                if (wv > 0)
                {
                    x.yestoday_weighted = true;
                    x.need_weight = "<i class='fa fa-check' style='color: red' title='昨日體重過磅合格'></i>";
                }
            }
            else if (wv >= program.min && wv <= program.max)
            {
                x.yestoday_weighted = true;
                x.need_weight = "<i class='fa fa-check' style='color: red' title='昨日體重過磅合格'></i>";
            }
            else
            {
                x.yestoday_weighted = true;
                x.need_weight = "<i class='fa fa-warning' style='color: red' title='昨日體重不合格'></i>";
            }
        }

        private string GetWeigthSpan(TWeight w, string title, string sub, string icon)
        {
            var s2 = "<span style='color:red' title='" + title + "' > / " + w.in_weight + "</span>" + icon;
            return w.pg_link + s2;
        }

        private string GetWeightNote2(TConfig cfg, TProgram program, TMUser x, Dictionary<string, TPlayer> map, string line_char)
        {
            string result = GetWeightNoteMain(cfg, program, x, map, line_char);
            if (x.in_weight_status == "leave")
            {
                return "請假" + line_char + result;
            }
            else
            {
                return result;
            }
        }

        /// <summary>
        /// 更新與會者籤號
        /// </summary>
        private void FixMUserSignNo(TConfig cfg)
        {
            string sql = @"
                UPDATE t1 SET
        	        t1.in_sign_no = t2.in_sign_no
        	        , t1.in_section_no = t2.in_section_no
                FROM
        	        IN_MEETING_USER t1 WITH(NOLOCK)
                INNER JOIN
        	        IN_MEETING_PTEAM t2 WITH(NOLOCK)
        	        ON t2.in_meeting = t1.source_id
        	        AND t2.in_team_key = ISNULL(t1.in_l1, '') 
        		        + '-' + ISNULL(t1.in_l2, '') 
        		        + '-' + ISNULL(t1.in_l3, '') 
        		        + '-' + ISNULL(t1.in_index, '')
        		        + '-' + ISNULL(t1.in_creator_sno, '')
                WHERE
        	        t1.source_id = '{#meeting_id}'
        	        AND t2.source_id = '{#program_id}'
        	        AND ISNULL(t2.in_sign_no, '') <> ''
        	        AND ISNULL(t2.in_type, '') IN ('', 'p')
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#program_id}", cfg.program_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql: " + sql);

            var itmSQL = cfg.inn.applySQL(sql);

            if (itmSQL.isError())
            {
                cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "更新與會者籤號與量級序號發生錯誤 _# sql: " + sql);
            }
        }

        //分組統計
        private void AppendGroupingStatistics(TConfig cfg, Item itmSurveyLevel, Item itmReturn)
        {
            string sql = "";

            string meeting_id = itmSurveyLevel.getProperty("meeting_id", "");
            string group_condition = itmSurveyLevel.getProperty("group_condition", "");

            string distinct_cols = itmSurveyLevel.getProperty("distinct_cols", "");

            //計算各分組的人數( in_l1)

            sql = @"
                SELECT 
                	DISTINCT t1.in_l1, t11.sort_order
                FROM 
                	IN_MEETING_USER t1 WITH(NOLOCK) 
                LEFT OUTER JOIN
                	VU_MEETING_SVY_L1 t11 WITH(NOLOCK)
                	ON t11.source_id = t1.source_id
                	AND t11.in_value = t1.in_l1
                WHERE 
                	t1.source_id = '{#meeting_id}' 
                	AND in_l1 NOT IN ('隊職員', '常年會費') 
                	AND in_note_state = 'official' {#group_condition}
                ORDER BY
                	t11.sort_order
            ";

            sql = sql.Replace("{#meeting_id}", meeting_id)
                .Replace("{#group_condition}", group_condition);


            Item l1_list = cfg.inn.applySQL(sql);

            Item l1_all = cfg.inn.newItem("inn_l1_group");
            l1_all.setProperty("inn_l1_name", "請選擇");
            l1_all.setProperty("inn_count", "");
            itmReturn.addRelationship(l1_all);

            //暫將JOIN IN_MEETING_PAY註解    
            // sql = @"SELECT DISTINCT {#distinct_cols} FROM in_meeting_user AS T1 WITH(NOLOCK) 
            //         JOIN IN_MEETING_PAY AS T2 WITH(NOLOCK) 
            //         ON T1.SOURCE_ID = T2.IN_MEETING AND T1.IN_PAYNUMBER = T2.ITEM_NUMBER
            //         WHERE T1.source_id = '{#meeting_id}' AND T1.in_note_state = 'official' {#group_condition} AND T1.in_l1 = N'{#in_l1}'";

            sql = @"SELECT DISTINCT {#distinct_cols} FROM IN_MEETING_USER AS T1 WITH(NOLOCK) 
            WHERE T1.source_id = '{#meeting_id}' AND T1.in_note_state = 'official' {#group_condition} AND T1.in_l1 = N'{#in_l1}'";


            int count = l1_list.getItemCount();
            int total = 0;

            for (int i = 0; i < count; i++)
            {
                Item item = l1_list.getItemByIndex(i);
                string in_l1 = item.getProperty("in_l1", "");

                string temp_sql = sql.Replace("{#meeting_id}", meeting_id)
                    .Replace("{#group_condition}", group_condition)
                    .Replace("{#in_l1}", in_l1)
                    .Replace("{#distinct_cols}", distinct_cols);

                //CCO.Utilities.WriteDebug(strMethodName, "sql: " + temp_sql);

                Item in_l1_result = cfg.inn.applySQL(temp_sql);
                int in_l1_count = in_l1_result.getItemCount();

                Item l1_option = cfg.inn.newItem("inn_l1_group");
                l1_option.setProperty("inn_l1_name", in_l1);
                l1_option.setProperty("inn_count", in_l1_count.ToString());
                itmReturn.addRelationship(l1_option);

                //累加各類組數
                total = total + in_l1_count;
            }

            l1_all.setProperty("inn_count", total.ToString());

            string l1_hide = itmSurveyLevel.getProperty("l1_hide", "");
            string l2_hide = itmSurveyLevel.getProperty("l2_hide", "");
            string l3_hide = itmSurveyLevel.getProperty("l3_hide", "");

            itmReturn.setProperty("l1_hide", l1_hide);
            itmReturn.setProperty("l2_hide", l2_hide);
            itmReturn.setProperty("l3_hide", l3_hide);

            if (l2_hide == "")
            {
                string l2_values = GetSurveyOptionContents(cfg, "in_l2", group_condition);
                itmReturn.setProperty("l2_values", l2_values);
            }

            if (l3_hide == "")
            {
                string l3_values = GetSurveyOptionContents(cfg, "in_l3", group_condition);
                itmReturn.setProperty("l3_values", l3_values);
            }
        }

        private Item GetSurveyLevels(TConfig cfg)
        {
            Item itmResult = cfg.inn.newItem();

            string sql = @"
                SELECT 
                    t1.*
                FROM 
                    IN_SURVEY t1 WITH(NOLOCK)
                INNER JOIN 
                    IN_MEETING_SURVEYS t2 WITH(NOLOCK) ON t2.related_id = t1.id 
                WHERE 
                    t2.source_id = N'{#meeting_id}' 
                    AND t1.in_client_remove = N'0'
                    AND t1.in_property IN (N'in_l1', N'in_l2', N'in_l3')
                ORDER BY 
                    t2.sort_order
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            //CCO.Utilities.WriteDebug(strMethodName, "sql_levels: " + sql);

            Item items = cfg.inn.applySQL(sql);

            if (items.isError() || items.getItemCount() <= 0)
            {
                return itmResult;
            }

            int count = items.getItemCount();

            string level_fields = "";
            string number_key_level = "";
            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_property = item.getProperty("in_property", "").Trim();
                string in_is_nokey = item.getProperty("in_is_nokey", "").Trim();

                if (level_fields != "")
                {
                    level_fields += ",";
                }
                level_fields += in_property;

                if (in_is_nokey == "1")
                {
                    if (number_key_level != "")
                    {
                        number_key_level += ",";
                    }
                    number_key_level += in_property;
                }
            }

            if (level_fields == "")
            {
                level_fields = "in_l1";
            }

            if (number_key_level == "")
            {
                number_key_level = "in_l1";
            }

            string default_cols = "";
            string user_order_by = "";
            string distinct_cols = "";
            string l1_hide = "";
            string l2_hide = "";
            string l3_hide = "";

            if (level_fields.Contains("in_l3"))
            {
                default_cols = "in_l1, in_l2, in_l3";
            }
            else if (level_fields.Contains("in_l2"))
            {
                default_cols = "in_l1, in_l2";
                l3_hide = "item_show_0";

            }
            else if (level_fields.Contains("in_l1"))
            {
                default_cols = "in_l1";
                l2_hide = "item_show_0";
                l3_hide = "item_show_0";
            }
            else
            {
                l1_hide = "item_show_0";
                l2_hide = "item_show_0";
                l3_hide = "item_show_0";
            }

            //定序處理
            if (number_key_level.Contains("in_l3"))
            {
                distinct_cols = "in_l1, in_l2, in_l3, in_index";
                user_order_by = "in_group, in_short_org, in_l1, in_l2, in_l3, in_index, in_name";
            }
            else if (number_key_level.Contains("in_l2"))
            {
                distinct_cols = "in_l1, in_l2, in_index";
                user_order_by = "in_group, in_short_org, in_l1, in_l2, in_index, in_name";
            }
            else if (number_key_level.Contains("in_l1"))
            {
                distinct_cols = "in_l1, in_index";
                user_order_by = "in_group, in_short_org, in_l1, in_index, in_name";
            }

            itmResult.setProperty("meeting_id", cfg.meeting_id);
            //定序欄位
            itmResult.setProperty("number_key_level", number_key_level);
            //預設欄位組合
            itmResult.setProperty("default_cols", default_cols);
            //分組統計欄位組合
            itmResult.setProperty("distinct_cols", distinct_cols);
            //定序欄位組合
            itmResult.setProperty("user_order_by", user_order_by);
            //第一階選單是否隱藏
            itmResult.setProperty("l1_hide", l1_hide);
            //第二階選單是否隱藏
            itmResult.setProperty("l2_hide", l2_hide);
            //第三階選單是否隱藏
            itmResult.setProperty("l3_hide", l3_hide);

            return itmResult;
        }

        private string GetSurveyOptionContents(TConfig cfg, string in_property, string group_condition)
        {
            string sql = "";

            if (in_property == "in_l2")
            {
                sql = @"
            SELECT t3.in_l1 AS 'in_filter', t3.in_l2 AS 'in_value', count(t3.team_index) AS 'in_count' FROM
            (
                SELECT 
                    DISTINCT T1.in_l1, T1.in_l2, T1.in_l3, (T1.in_l1 + '-' + ISNULL(T1.in_l2, '') + '-' + ISNULL(T1.in_l3, '') + '-' + T1.in_index) AS 'team_index' , T4.IN_L2_SORT
                FROM 
                    IN_MEETING_USER AS T1 WITH(NOLOCK)
                LEFT JOIN IN_MEETING_PROGRAM AS T4 WITH(NOLOCK) 
            	ON T1.SOURCE_ID = T4.IN_MEETING and T1.in_l1 = T4.in_l1 and T1.in_l2 = T4.in_l2 and T1.in_l3 = T4.in_l3
                WHERE 
                    T1.source_id = '{#meeting_id}'
                    {#group_condition}
            ) t3
                GROUP BY t3.in_l1, t3.in_l2, t3.in_l2_sort
                ORDER BY t3.in_l2_sort";
            }
            else if (in_property == "in_l3")
            {
                sql = @"
            SELECT t3.in_l1 AS 'in_l1', t3.in_l2 AS 'in_filter', ISNULL(t3.in_l3, '') AS 'in_value', count(t3.team_index) AS 'in_count' FROM
            (
                SELECT 
                    DISTINCT T1.in_l1, T1.in_l2, T1.in_l3, (T1.in_l1 + '-' + ISNULL(T1.in_l2, '') + '-' + ISNULL(T1.in_l3, '') + '-' + T1.in_index) AS 'team_index' , T4.IN_L3_SORT
                FROM 
                    IN_MEETING_USER AS T1 WITH(NOLOCK)
                LEFT JOIN IN_MEETING_PROGRAM AS T4 WITH(NOLOCK) 
            	ON T1.source_id = T4.in_meeting and T1.in_l1 = T4.in_l1 and T1.in_l2 = T4.in_l2 and T1.in_l3 = T4.in_l3
                WHERE 
                    T1.source_id = '{#meeting_id}'
                    {#group_condition}
            ) t3
                GROUP BY t3.in_l1, t3.in_l2, t3.in_l3, t3.in_l3_sort
                ORDER BY t3.in_l3_sort";

            }

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#group_condition}", group_condition);

            Item items = cfg.inn.applySQL(sql);

            if (items.isError() || items.getItemCount() <= 0)
            {
                return "";
            }

            return ConvertSurveyOptionContents(items);

        }

        string GetStatusOption(TStatus status)
        {
            if (status.IsSelected)
            {
                return "<option value='" + status.Value + "' " + status.Css + " selected >" + status.Label + "</option>";
            }
            else
            {
                return "<option value='" + status.Value + "' " + status.Css + " >" + status.Label + "</option>";
            }
        }

        private Item GetProgram(TConfig cfg)
        {
            if (cfg.in_l3 == "")
            {
                return cfg.inn.newItem();
            }

            string sql = "SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK)"
                + " WHERE in_meeting = '" + cfg.meeting_id + "'"
                + " AND in_l1 = N'" + cfg.in_l1 + "'"
                + " AND in_l2 = N'" + cfg.in_l2 + "'"
                + " AND in_l3 = N'" + cfg.in_l3 + "'"
                + "";

            Item itmProgram = cfg.inn.applySQL(sql);

            if (itmProgram.isError() || itmProgram.getResult() == "")
            {
                return cfg.inn.newItem();
            }
            else
            {
                return itmProgram;
            }
        }

        private Item GetMUsers(TConfig cfg, string in_l1, string in_l2, string in_l3, string in_sign_time)
        {
            if (in_sign_time == "")
            {
                cfg.need_judo_no = false;
                return GetMUsersStuffB1(cfg, in_l1, in_l2, in_l3);
            }
            else if (cfg.weigh_sort_mode == "")
            {
                cfg.need_judo_no = false;
                return GetMUsersStuffB1(cfg, in_l1, in_l2, in_l3);
            }
            else
            {
                cfg.need_judo_no = true;
                return GetMUsersJudoNumber(cfg, in_l1, in_l2, in_l3);
            }
        }

        private Item GetMUsersJudoNumber(TConfig cfg, string in_l1, string in_l2, string in_l3)
        {
            string sql = @"
                SELECT 
	                t1.id
	                , t1.in_name
	                , t1.in_sno
	                , t1.in_gender
	                , t1.in_stuff_b1
	                , t1.in_short_org
	                , t1.in_show_org
	                , t1.in_team
	                , t1.in_weight
	                , t1.in_weight_result
	                , t1.in_weight_status
	                , IIF(t1.in_rollcall_time > 0, '1', '0') AS 'rollcall'
	                , t1.in_sign_no
	                , t1.in_judo_no
	                , t1.in_section_no
	                , t1.in_show_no
	                , t1.in_section_name
	                , t1.in_team_weight_n2
	                , t1.in_team_weight_n3
	                , t1.in_l1
	                , t1.in_team_solo
	                , t1.in_team_n2
	                , t1.in_team_n3
	                , t4.in_extend_value2
                FROM 
	                IN_MEETING_USER t1 WITH(NOLOCK)
                JOIN 
	                IN_SURVEY_OPTION t4 WITH(NOLOCK)
	                ON t4.source_id = '{#l3_id}'
	                AND t4.in_grand_filter + t4.in_filter + t4.in_value = t1.in_l1 + t1.in_l2 + t1.in_l3
                WHERE 
	                t1.source_id = '{#meeting_id}' 
	                AND t1.in_l1 = N'{#in_l1}' 
	                AND t1.in_l2 = N'{#in_l2}' 
	                AND t1.in_l3 = N'{#in_l3}' 
                ORDER BY 
                    t1.in_judo_no
	                , t1.in_stuff_b1
	                , t1.in_current_org
	                , t1.in_team
	                , t1.in_gender
	                , t1.in_sno
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                    .Replace("{#l3_id}", cfg.l3_id)
                    .Replace("{#in_l1}", in_l1)
                    .Replace("{#in_l2}", in_l2)
                    .Replace("{#in_l3}", in_l3);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        private Item GetMUsersStuffB1(TConfig cfg, string in_l1, string in_l2, string in_l3)
        {
            string sql = @"
                SELECT 
	                t1.id
	                , t1.in_name
	                , t1.in_sno
	                , t1.in_gender
	                , t1.in_stuff_b1
	                , t1.in_short_org
	                , t1.in_show_org
	                , t1.in_team
	                , t1.in_weight
	                , t1.in_weight_result
	                , t1.in_weight_status
	                , IIF(t1.in_rollcall_time > 0, '1', '0') AS 'rollcall'
	                , t1.in_sign_no
	                , t1.in_judo_no
	                , t1.in_section_no
	                , t1.in_show_no
	                , t1.in_section_name
	                , t1.in_team_weight_n2
	                , t1.in_team_weight_n3
	                , t1.in_l1
	                , t1.in_team_solo
	                , t1.in_team_n2
	                , t1.in_team_n3
	                , t4.in_extend_value2
                FROM 
	                IN_MEETING_USER t1 WITH(NOLOCK)
                JOIN 
	                IN_SURVEY_OPTION t4 WITH(NOLOCK)
	                ON t4.source_id = '{#l3_id}'
	                AND t4.in_grand_filter + t4.in_filter + t4.in_value = t1.in_l1 + t1.in_l2 + t1.in_l3
                WHERE 
	                t1.source_id = '{#meeting_id}' 
	                AND t1.in_l1 = N'{#in_l1}' 
	                AND t1.in_l2 = N'{#in_l2}' 
	                AND t1.in_l3 = N'{#in_l3}' 
                ORDER BY 
                    t1.in_city_no
	                , t1.in_stuff_b1
	                , t1.in_current_org
	                , t1.in_team
	                , t1.in_gender
	                , t1.in_sno
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                    .Replace("{#l3_id}", cfg.l3_id)
                    .Replace("{#in_l1}", in_l1)
                    .Replace("{#in_l2}", in_l2)
                    .Replace("{#in_l3}", in_l3);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        private Dictionary<string, TPlayer> GetWeightRecords(TConfig cfg, string in_l1, string in_l2, string in_l3)
        {
            var map = new Dictionary<string, TPlayer>();

            string sql = @"
		        SELECT
			        t1.id
			        , t1.in_current_org
			        , t1.in_stuff_b1
			        , t1.in_short_org
			        , t1.in_team
			        , t1.in_l1
			        , t1.in_l2
			        , t1.in_l3
			        , t1.in_sno
			        , t1.in_name
			        , t1.in_weight
			        , t1.in_weight_status
			        , t1.in_weight_time
			        , t1.in_team_weight_n2
			        , t1.in_team_weight_n3
			        , t2.current_muid
			        , t3.id            AS 'pg_id'
			        , t3.in_short_name AS 'pg_short_name'
			        , t3.in_fight_day  AS 'pg_fight_day'
			        , t3.in_weight     AS 'pg_weight'
		        FROM
			        IN_MEETING_USER t1
		        INNER JOIN
		        (
			        SELECT 
				        source_id
                        , id AS 'current_muid'
				        , in_sno
			        FROM 
				        IN_MEETING_USER WITH(NOLOCK)
			        WHERE 
				        source_id = '{#meeting_id}' 
				        AND in_l1 = N'{#in_l1}' 
				        AND in_l2 = N'{#in_l2}' 
				        AND in_l3 = N'{#in_l3}' 
		        ) t2 
		            ON t2.source_id = t1.source_id
		            AND t2.in_sno = t1.in_sno
		        INNER JOIN
		            IN_MEETING_PROGRAM t3 WITH(NOLOCK)
		            ON t3.in_meeting = t1.source_id
		            AND t3.in_l1 = t1.in_l1
		            AND t3.in_l2 = t1.in_l2
		            AND t3.in_l3 = t1.in_l3
		        WHERE
			        t1.source_id = '{#meeting_id}' 
                    AND t1.in_l1 <> '格式組'
			    ORDER BY
			        t1.in_city_no
			        , t1.in_stuff_b1
			        , t1.in_current_org
			        , t1.in_team
			        , t1.in_sno
			        , t3.in_fight_day
			        , t1.in_weight_time
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_l1}", in_l1)
                .Replace("{#in_l2}", in_l2)
                .Replace("{#in_l3}", in_l3);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var id = item.getProperty("id", "");
                var current_muid = item.getProperty("current_muid", "");
                var in_sno = item.getProperty("in_sno", "").ToUpper();

                TPlayer player = null;
                if (map.ContainsKey(in_sno))
                {
                    player = map[in_sno];
                }
                else
                {
                    player = new TPlayer
                    {
                        in_sno = in_sno,
                        in_name = item.getProperty("in_name", ""),
                        WeightRecords = new List<TWeight>(),
                        Value = item,
                        HasSolo = false,
                        HasTeam = false,
                    };
                    map.Add(in_sno, player);
                }

                var row_in_l1 = item.getProperty("in_l1", "");
                switch (row_in_l1)
                {
                    case "個人組": player.HasSolo = true; break;
                    case "團體組": player.HasTeam = true; break;
                }

                if (id == current_muid) continue;

                TWeight weight = new TWeight
                {
                    pg_id = item.getProperty("pg_id", ""),
                    pg_short_name = item.getProperty("pg_short_name", ""),
                    pg_fight_day = item.getProperty("pg_fight_day", ""),
                    pg_weight = item.getProperty("pg_weight", "").ToLower().Trim(),
                    in_weight = item.getProperty("in_weight", ""),
                    in_weight_status = item.getProperty("in_weight_status", ""),
                    in_weight_time = item.getProperty("in_weight_time", ""),
                    n2 = item.getProperty("in_team_weight_n2", ""),
                    n3 = item.getProperty("in_team_weight_n3", ""),
                };


                switch (weight.in_weight_status)
                {
                    case "on": weight.status = ""; break;
                    case "off": weight.status = "DNS"; break;
                    case "leave": weight.status = "假"; break;
                    case "dq": weight.status = "DQ"; break;
                    default: weight.status = ""; break;
                }

                if (weight.in_weight_time == "")
                {
                    weight.weight_time = DateTime.MinValue;
                    weight.show_weight_time = "";
                    weight.wv = 0;
                }
                else
                {
                    weight.weight_time = GetDateTime(weight.in_weight_time);
                    weight.show_weight_time = weight.weight_time.AddHours(8).ToString("MM/dd");
                    weight.wv = GetDblVal(weight.in_weight);
                }

                SetWeightRecordLink(cfg, weight);

                player.WeightRecords.Add(weight);
            }

            return map;
        }

        private void SetWeightRecordLink(TConfig cfg, TWeight w)
        {
            if (w.wv > 0)
            {
                w.pg_display = w.show_weight_time + " " + w.pg_short_name + " " + w.pg_weight;
            }
            else if (w.pg_fight_day == "")
            {
                w.pg_display = w.pg_short_name + " " + w.pg_weight;
            }
            else
            {
                w.fight_day = GetDateTime(w.pg_fight_day);
            
                if (cfg.is_fight_day)
                {
                    w.weight_day = w.fight_day;
                }
                else
                {
                    w.weight_day = w.fight_day.AddDays(-1);
                }

                w.pg_display = w.weight_day.ToString("MM/dd") + " " + w.pg_short_name + " " + w.pg_weight;
            }
            w.pg_link = "<span data-id='" + w.pg_id + "' onclick='GoWeightPage(this)'>" + w.pg_display + "</span>";
        }

        //設定賽會參數
        private void SetMeetingVariable(TConfig cfg)
        {
            cfg.weigh_even_days = 0;
            cfg.weigh_even_increase = "";
            cfg.weigh_sort_mode = "";

            var sql = @"
                SELECT
	                in_key
	                , in_value
                FROM
	                IN_MEETING_VARIABLE WITH(NOLOCK)
                WHERE
	                source_id = '{#meeting_id}'
	                AND in_key IN ('weigh_even_days', 'weigh_even_increase', 'weigh_sort_mode')
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var in_key = item.getProperty("in_key", "");
                var in_value = item.getProperty("in_value", "");
                switch (in_key)
                {
                    case "weigh_even_days":
                        cfg.weigh_even_days = GetIntVal(in_value);
                        break;
                    case "weigh_even_increase":
                        cfg.weigh_even_increase = in_value;
                        break;
                    case "weigh_sort_mode":
                        cfg.weigh_sort_mode = in_value;
                        break;
                }
            }

            if (cfg.weigh_even_increase == "")
            {
                cfg.weigh_even_increase = "+5%";
            }

            if (cfg.weigh_even_increase != "")
            {
                cfg.weigh_even_increase = "(" + cfg.weigh_even_increase + ")";
            }

        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }

            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }

            public string meeting_id { get; set; }
            public string program_id { get; set; }
            public string in_date { get; set; }
            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }
            public string in_fight_day { get; set; }
            public string in_weight { get; set; }
            public string mode { get; set; }
            public string scene { get; set; }
            public string note_mode { get; set; }
            public bool NoNote { get; set; }

            public Item itmMeeting { get; set; }
            public Item itmProgram { get; set; }

            public string mt_title { get; set; }
            public string mt_weight_mode { get; set; }
            public bool is_fight_day { get; set; }
            public string pg_sign_time { get; set; }
            public bool has_sign_no { get; set; }
            public bool has_fight_day { get; set; }
            public DateTime fight_day { get; set; }
            public DateTime weight_day { get; set; }

            public bool is_xlsx { get; set; }

            public int weigh_even_days { get; set; }
            public string weigh_even_increase { get; set; }
            public string weigh_sort_mode { get; set; }
            public bool need_judo_no { get; set; }
            public string l1_id { get; set; }
            public string l2_id { get; set; }
            public string l3_id { get; set; }

            public int row_index { get; set; }
            public System.Drawing.Color head_color { get; set; }
        }

        private class TExport
        {
            public string template_path { get; set; }
            public string export_path { get; set; }
        }

        private class TProgram
        {
            public string no { get; set; }
            public string name { get; set; }
            public string name2 { get; set; }
            public string sheet_name { get; set; }
            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }
            public string in_fight_day { get; set; }
            public string in_sign_time { get; set; }

            public DateTime fight_day { get; set; }
            public bool is_team { get; set; }
            public bool is_solo { get; set; }
            public string extv { get; set; }

            public string sect_name { get; set; }
            public int in_name_count { get; set; }

            public bool needCheck { get; set; }
            public double min { get; set; }
            public double max { get; set; }
            public double max2 { get; set; }
        }

        private class TPlayer
        {
            public string in_sno { get; set; }
            public string in_name { get; set; }
            public List<TWeight> WeightRecords { get; set; }
            public Item Value { get; set; }
            public bool HasSolo { get; set; }
            public bool HasTeam { get; set; }
        }

        private class TWeight
        {
            public string pg_id { get; set; }
            public string pg_short_name { get; set; }
            public string pg_fight_day { get; set; }
            public string pg_weight { get; set; }
            public string pg_display { get; set; }
            public string pg_link { get; set; }

            public string in_weight { get; set; }
            public string in_weight_time { get; set; }
            public string in_weight_status { get; set; }

            public string status { get; set; }

            public DateTime fight_day { get; set; }
            public DateTime weight_day { get; set; }
            public DateTime weight_time { get; set; }
            public string show_weight_time { get; set; }

            public string n2 { get; set; }
            public string n3 { get; set; }
            public double wv { get; set; }
        }

        private class TField
        {
            public string title { get; set; }
            public string property { get; set; }
            public string fontName { get; set; }
            public double fontSize { get; set; }
            public bool fontBold { get; set; }
            public bool IsSectionNo { get; set; }
            public string RS { get; set; }
            public string RE { get; set; }
            public bool is_num { get; set; }
        }

        private TStatusMenu GetStatusMenu()
        {
            var result = new TStatusMenu
            {
                Leave = new TStatus { Label = "請假", Value = "leave", Message = "(請假)", Css = "style='background-color: yellow'", NeedRemove = true },
                Off = new TStatus { Label = "DNS", Value = "off", Message = "(DNS)", Css = "style='background-color: orange'", NeedRemove = false, Desc = "未到(Did Not Start)" },
                DQ = new TStatus { Label = "DQ", Value = "dq", Message = "(DQ)", Css = "style='background-color: #F8D7DA'", NeedRemove = false, Desc = "" },
                On = new TStatus { Label = "合格", Value = "on", Message = "", Css = "style='background-color: white'", NeedRemove = false },
                King = new TStatus { Label = "盟主", Value = "king", Message = "(king)", Css = "style='background-color: white'", NeedRemove = true },
            };
            return result;
        }

        private class TStatusMenu
        {
            /// <summary>
            /// 過磅合格
            /// </summary>
            public TStatus On { get; set; }
            /// <summary>
            /// 未到
            /// </summary>
            public TStatus Off { get; set; }
            /// <summary>
            /// 請假
            /// </summary>
            public TStatus Leave { get; set; }
            /// <summary>
            /// 超磅
            /// </summary>
            public TStatus DQ { get; set; }
            /// <summary>
            /// 盟主
            /// </summary>
            public TStatus King { get; set; }
        }

        private class TStatus
        {
            public string Label { get; set; }
            public string Value { get; set; }
            public string Css { get; set; }
            public string Desc { get; set; }
            public bool IsSelected { get; set; }
            public bool NeedRemove { get; set; }
            public string Message { get; set; }
        }

        /// <summary>
        /// 將問項明細轉換為字典內容字串
        /// </summary>
        private string ConvertSurveyOptionContents(Item items)
        {
            Dictionary<string, List<string>> dictionary = new Dictionary<string, List<string>>();

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_filter = item.getProperty("in_filter", "");
                string in_value = item.getProperty("in_value", "");
                string in_count = item.getProperty("in_count", "");

                List<string> list = null;
                if (dictionary.ContainsKey(in_filter))
                {
                    list = dictionary[in_filter];
                }
                else
                {
                    list = new List<string>();
                    dictionary.Add(in_filter, list);
                }

                if (!list.Contains(in_value))
                {
                    list.Add(in_value + "_#" + in_count);
                }
            }

            List<string> list2 = new List<string>();
            foreach (KeyValuePair<string, List<string>> kv in dictionary)
            {
                string key = kv.Key;
                string values = string.Join("@", kv.Value.Where(x => !string.IsNullOrEmpty(x)));
                string row = key + ":" + values;
                list2.Add(row);
            }
            return string.Join("@@", list2.Where(x => !string.IsNullOrEmpty(x)));
        }

        private DateTime GetDateTime(string value)
        {
            if (value == "") return DateTime.MinValue;
            DateTime result = DateTime.MinValue;
            DateTime.TryParse(value, out result);
            return result;
        }

        private int GetIntVal(string value)
        {
            if (value == "" || value == "0") return 0;
            int result = 0;
            int.TryParse(value, out result);
            return result;
        }

        private double GetDblVal(string value)
        {
            if (value == "" || value == "0") return 0;
            double result = 0;
            double.TryParse(value, out result);
            return result;
        }
    }
}