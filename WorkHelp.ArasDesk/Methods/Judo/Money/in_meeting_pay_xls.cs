﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;
using System.Security.Cryptography;

namespace WorkHelp.ArasDesk.Methods.Judo.Common
{
    public class in_meeting_pay_xls : Item
    {
        public in_meeting_pay_xls(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 匯出對帳總表
                輸入: meeting_id
                輸出: 
                    1. 報名總表
                日誌:
                    - 2024-12-31: 增加 item_number 與發票抬頭、統一發票欄位 (lina)
                    - 2022-06-07: 條碼2 不加遮罩 (lina)
                    - 2020-11-11: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]in_meeting_pay_xls";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "input dom: " + itmR.dom.InnerXml);

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                meeting_id = itmR.getProperty("meeting_id", "").Trim(),
            };

            if (cfg.meeting_id == "")
            {
                throw new Exception("賽事 id 不得為空白");
            }

            //取得賽事資料
            cfg.itmMeeting = GetMeeting(cfg);
            //取得競賽項目
            Item itmLv1s = GetMeetingLv1s(cfg);
            //取得所有與會者資料
            Item itmMUsers = GetMeetingAllUsers(cfg);
            //取得所有發票
            Dictionary<string, List<TInvoice>> invMap = GetMeetingInvoiceMap(cfg);
            //取得與會者與繳費單資料
            List<Item> lstMPays = MergerMUsersAndMPays(cfg);
            //取得賽事統計摘要
            GameSummary game_summary = MapToGameSummary(cfg, itmLv1s, itmMUsers, lstMPays, invMap);

            var workbook = new ClosedXML.Excel.XLWorkbook();

            // //報名總表
            AppendSummarySheet(cfg, workbook, game_summary);

            string in_title = cfg.itmMeeting.getProperty("keyed_name", "");

            Item itmPath = GetExcelPath(cfg, "export_path");

            string export_path = itmPath.getProperty("export_path", "").TrimEnd('\\');
            string ext_name = ".xlsx";
            string xls_name = in_title + "_對帳報表_" + DateTime.Now.ToString("MMdd_HHmmss");
            string xls_file = export_path + "\\" + xls_name + ext_name;
            string xls_url = xls_name + ext_name;

            workbook.SaveAs(xls_file);

            itmR.setProperty("xls_name", xls_url);

            return itmR;
        }

        //報名總表
        private void AppendSummarySheet(TConfig cfg, ClosedXML.Excel.XLWorkbook workbook, GameSummary game_summary)
        {
            ClosedXML.Excel.IXLWorksheet sheet = workbook.Worksheets.Add("報名總表");

            //標題列
            List<TField> fields = new List<TField>();
            fields.Add(new TField { title = "序號", property = "no", format = "center", width = 10 });
            fields.Add(new TField { title = "所屬單位", property = "map_current_org", format = "", width = 24, color = true, color_func = RegisterHasError });
            fields.Add(new TField { title = "", property = "", format = "", width = 2 });
            fields.Add(new TField { title = "隊職員數", property = "manager_count", format = "", width = 10 });
            fields.Add(new TField { title = "選手人數", property = "player_count", format = "", width = 10 });
            fields.Add(new TField { title = "項目總計", property = "item_count", format = "", width = 10 });

            foreach (var kv in game_summary.ItemMap)
            {
                var f = kv.Value;
                fields.Add(new TField { title = f.Title, property = f.Property, format = "", width = 10 });
            }

            fields.Add(new TField { title = "報名費用", property = "subtotal", format = "$ #,##0", width = 10 });
            fields.Add(new TField { title = "對帳狀態", property = "pay_status", format = "center", width = 10, color = true, color_func = AmountHasError });
            fields.Add(new TField { title = "未繳", property = "need_amount", format = "$ #,##0", width = 10 });
            fields.Add(new TField { title = "已繳", property = "give_amount", format = "$ #,##0", width = 10 });
            fields.Add(new TField { title = "溢繳", property = "over_amount", format = "$ #,##0", width = 10 });
            fields.Add(new TField { title = "實繳總額", property = "paid_amount", format = "$ #,##0", width = 10 });

            fields.Add(new TField { title = "協助報名者", property = "creator_name", format = "", width = 10 });
            fields.Add(new TField { title = "連絡電話", property = "creator_tel", format = "tel", width = 12 });

            fields.Add(new TField { title = "繳費狀態", property = "pay_bool", format = "center", width = 10 });

            fields.Add(new TField { title = "繳費單號", property = "pay_no", format = "center", width = 14 });
            fields.Add(new TField { title = "虛擬帳號", property = "code_2", format = "text", width = 18 });
            fields.Add(new TField { title = "應繳金額", property = "expect_amount", format = "$ #,##0", width = 10 });
            fields.Add(new TField { title = "實繳金額", property = "real_amount", format = "$ #,##0", width = 10 });
            fields.Add(new TField { title = "退款金額", property = "refund_amount", format = "$ #,##0", width = 10 });
            fields.Add(new TField { title = "最後金額", property = "final_amount", format = "$ #,##0", width = 10 });
            fields.Add(new TField { title = "退款手續費", property = "fee_amount", format = "$ #,##0", width = 10 });

            fields.Add(new TField { title = "發票抬頭", property = "invoice_up", format = "center", width = 18 });
            fields.Add(new TField { title = "統一發票", property = "uniform_numbers", format = "center", width = 18 });

            int merge_col_s = 0;
            int merge_col_e = 12 + game_summary.ItemMap.Count;

            SetHeadCell(sheet.Cell(2, 2), "報名總費用");
            SetBodyCell(sheet.Cell(2, 3), game_summary.TotalAmount.ToString(), "$ #,##0");

            int wsRow = 4;
            int wsCol = 2;

            for (int i = 0; i < fields.Count; i++)
            {
                ClosedXML.Excel.IXLCell cell = sheet.Cell(wsRow, wsCol + i);
                SetHeadCell(cell, fields[i].title);
            }
            wsRow++;

            //凍結窗格
            sheet.SheetView.FreezeRows(wsRow - 1);

            int no = 0;

            //foreach (KeyValuePair<string, TOrg> org_kv in game_summary.OrgMap.OrderBy(x => x.Key))
            foreach (var org_kv in game_summary.OrgMap)
            {
                no++;

                TOrg org = org_kv.Value;

                //資料容器
                Item item = cfg.inn.newItem();
                item.setProperty("no", no.ToString());
                item.setProperty("map_current_org", org.Name);
                item.setProperty("manager_count", org.ManagerMap.Count.ToString());
                item.setProperty("player_count", org.PlayerMap.Count.ToString());
                item.setProperty("item_count", org.PlayItemMap.Count.ToString());
                item.setProperty("subtotal", org.Subtotal.ToString());
                item.setProperty("pay_status", org.PayStatus.ToString());
                item.setProperty("need_amount", org.NeedAmount.ToString());
                item.setProperty("give_amount", org.GiveAmount.ToString());
                item.setProperty("over_amount", org.OverAmount.ToString());
                item.setProperty("paid_amount", org.PaidAmount.ToString());

                item.setProperty("register_error", org.RegisterError ? "1" : "0");
                item.setProperty("amount_error", org.AmountError ? "1" : "0");

                foreach (var kv in game_summary.ItemMap)
                {
                    var k = kv.Key;
                    var f = kv.Value;

                    if (org.SubItemMap.ContainsKey(k))
                    {
                        item.setProperty(f.Property, org.SubItemMap[k].List.Count.ToString());
                    }
                    else
                    {
                        item.setProperty(f.Property, "0");
                    }
                }

                foreach (KeyValuePair<string, TPay> pay_kv in org.PayMap)
                {
                    TPay pay = pay_kv.Value;


                    item.setProperty("creator_name", pay.CreatorName);
                    item.setProperty("creator_tel", pay.CreatorTel);

                    item.setProperty("pay_bool", pay.PayBool);
                    item.setProperty("pay_no", pay.PayNumber);

                    if (pay.HasPayment)
                    {
                        item.setProperty("code_2", pay.Code2);
                        item.setProperty("expect_amount", pay.ExpectAmount.ToString());
                        item.setProperty("real_amount", pay.RealAmount.ToString());
                        item.setProperty("refund_amount", pay.RefundAmount.ToString());
                        item.setProperty("final_amount", pay.FinalAmount.ToString());
                        item.setProperty("fee_amount", pay.FeeAmount.ToString());
                        item.setProperty("invoice_up", GetInvoiceUps(cfg, pay));
                        item.setProperty("uniform_numbers", GetInvoiceNums(cfg, pay));
                    }
                    else
                    {
                        item.setProperty("expect_amount", pay.ExpectAmount.ToString());
                    }

                    SetItemCell(sheet, wsRow, wsCol, fields, item);
                    if (pay.Invoices.Count > 1)
                    {
                        sheet.Row(wsRow).Height = 40;
                    }
                    wsRow++;
                }

                if (org.PayMap.Count == 0)
                {
                    cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "對帳項目異常: " + org.Name);
                    continue;
                }

                if (org.PayMap.Count == 1)
                {
                    continue;
                }

                //跨列處理
                for (int i = merge_col_s; i < merge_col_e; i++)
                {
                    int col_s = wsCol + i;
                    int col_e = wsCol + i;

                    int row_s = wsRow - org.PayMap.Count;
                    int row_e = wsRow - 1;

                    sheet.Range(row_s, col_s, row_e, col_s)
                        .Merge()
                        .Style.Alignment.Vertical = ClosedXML.Excel.XLAlignmentVerticalValues.Center;
                }
            }

            for (int i = 0; i < fields.Count; i++)
            {
                sheet.Column(wsCol + i).Width = fields[i].width;
                //自動調整，時間有點久
                //sheet.Column(wsCol + i).AdjustToContents();
            }
        }
        
        private string GetInvoiceNums(TConfig cfg, TPay pay)
        {
            if (pay.Invoices == null || pay.Invoices.Count == 0) return "";
            var lines = pay.Invoices.Select(x => x.UniformNumbers);
            return string.Join(System.Environment.NewLine, lines);
        }

        private string GetInvoiceUps(TConfig cfg, TPay pay)
        {
            if (pay.Invoices == null || pay.Invoices.Count == 0) return "";
            var lines = pay.Invoices.Select(x => x.InvoiceUp);
            return string.Join(System.Environment.NewLine, lines);
        }
        
        ClosedXML.Excel.XLColor head_bg_color = ClosedXML.Excel.XLColor.FromHtml("#295C90");
        ClosedXML.Excel.XLColor body_er_bg_color = ClosedXML.Excel.XLColor.FromHtml("#FFC7CE");

        //設定物件與資料列
        private void SetItemCell(ClosedXML.Excel.IXLWorksheet sheet, int wsRow, int wsCol, List<TField> fields, Item item)
        {
            for (int i = 0; i < fields.Count; i++)
            {
                TField field = fields[i];
                string field_name = field.property;
                string field_format = field.format;
                ClosedXML.Excel.IXLCell cell = sheet.Cell(wsRow, wsCol + i);

                if (field_name == "")
                {
                    SetBodyCell(cell, "", field_format);
                }
                else
                {
                    string value = item.getProperty(field_name, "");

                    SetBodyCell(cell, value, field_format);

                    if (field.color && field.color_func(item))
                    {
                        cell.Style.Fill.BackgroundColor = body_er_bg_color;
                        cell.Style.Font.FontColor = ClosedXML.Excel.XLColor.Black;
                    }
                }
            }
        }

        //設定資料列
        private void SetBodyCell(ClosedXML.Excel.IXLCell cell, string value, string format)
        {
            switch (format)
            {
                case "center":
                    cell.Value = value;
                    cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    break;

                case "yyyy/MM/dd":
                case "yyyy/MM/dd HH:mm":
                    cell.Value = GetDtm(value, format);
                    cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    break;

                case "$ #,##0":
                    cell.Value = value;
                    cell.Style.NumberFormat.Format = "$ #,##0";
                    break;

                case "tel":
                    if (value.StartsWith("09"))
                    {
                        cell.Value = value.Replace("-", "");
                        cell.Style.NumberFormat.Format = "0000-000-000";
                        cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    }
                    else
                    {
                        cell.Value = "'" + value;
                        cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    }
                    break;

                case "text":
                    cell.Value = "'" + value;
                    cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
                    break;

                default:
                    cell.Value = value;
                    break;
            }
            cell.Style.Border.SetRightBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetTopBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetBottomBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetLeftBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
        }


        //設定標題列
        private void SetHeadCell(ClosedXML.Excel.IXLCell cell, string title)
        {
            cell.Value = title;

            cell.Style.Alignment.Horizontal = ClosedXML.Excel.XLAlignmentHorizontalValues.Center;
            cell.Style.Font.Bold = true;
            cell.DataType = ClosedXML.Excel.XLDataType.Text;
            cell.Style.Fill.BackgroundColor = head_bg_color;
            cell.Style.Font.FontColor = ClosedXML.Excel.XLColor.White;

            cell.Style.Border.SetRightBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetTopBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetBottomBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
            cell.Style.Border.SetLeftBorder(ClosedXML.Excel.XLBorderStyleValues.Thin);
        }

        //資料 Cell 上色
        private void SetBodyCellColor(ClosedXML.Excel.IXLWorksheet sheet, int wsRow, int wsCol_s, int wsCol_e, ClosedXML.Excel.XLColor color)
        {
            for (int i = wsCol_s; i <= wsCol_e; i++)
            {
                ClosedXML.Excel.IXLCell cell = sheet.Cell(wsRow, i);
                cell.Style.Fill.BackgroundColor = color;
            }
        }

        private Item GetExcelPath(TConfig cfg, string in_name)
        {
            Item itmResult = cfg.inn.newItem();

            //Excel 匯出路徑 (from 原創.設定參數)
            string sql = @"
                SELECT 
                    TOP 1 
                    t2.in_name AS 'variable'
                    , t1.in_name
                    , t1.in_value 
                FROM
                    In_Variable_Detail t1 WITH(NOLOCK)
                INNER JOIN 
                    In_Variable t2 WITH(NOLOCK) ON t2.ID = t1.SOURCE_ID 
                WHERE 
                    t2.in_name = N'meeting_excel'  
                    AND t1.in_name = N'{#in_name}'
             ";

            sql = sql.Replace("{#in_name}", in_name);

            //CCO.Utilities.WriteDebug(strMethodName, "sql: " + sql);

            Item item = cfg.inn.applySQL(sql);

            if (item.isError())
            {
                throw new Exception("未設定匯出路徑 _# " + in_name);
            }

            itmResult.setProperty("export_path", item.getProperty("in_value", ""));

            return itmResult;
        }

        /// <summary>
        /// 轉換為報名總表
        /// </summary>
        private GameSummary MapToGameSummary(TConfig cfg
            , Item itmLv1s
            , Item itmMUsers
            , List<Item> lstMPays
            , Dictionary<string, List<TInvoice>> invMap)
        {
            GameSummary result = new GameSummary
            {
                OrgMap = new Dictionary<string, TOrg>(),
                ItemMap = new Dictionary<string, TItem>(),
                TotalAmount = 0,
            };

            int count = 0;

            //所有競賽項目
            count = itmLv1s.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = itmLv1s.getItemByIndex(i);
                AddItem(result.ItemMap, item);
            }

            //所有與會者
            count = itmMUsers.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item item = itmMUsers.getItemByIndex(i);
                TOrg org = AddAndGetOrg(result.OrgMap, item);

                SetSummary(org, item);
            }

            //與會者與繳費單
            count = lstMPays.Count;
            for (int i = 0; i < count; i++)
            {
                Item item = lstMPays[i];
                TOrg org = AddAndGetOrg(result.OrgMap, item);

                if (org.IsNew)
                {
                    org.RegisterError = true;
                }

                AddPay(org.PayMap, item, invMap);
            }

            foreach (var kv in result.OrgMap)
            {
                TOrg org = kv.Value;
                org.Subtotal = org.PlayItemMap.Sum(x => GetInt(x.Value));

                int paidAmount = 0;
                foreach (var pkv in org.PayMap)
                {
                    TPay pay = pkv.Value;
                    if (!pay.HasPayment)
                    {
                        pay.ExpectAmount = pay.TeamMap.Sum(x => x.Value.Amount);
                    }

                    if (pay.PayBool == "已繳費")
                    {
                        paidAmount += pay.ExpectAmount;
                    }
                }

                org.PaidAmount = paidAmount;
                if (org.Subtotal == org.PaidAmount)
                {
                    org.PayStatus = "已繳清";
                    org.GiveAmount = org.PaidAmount;
                }
                else if (org.PaidAmount > org.Subtotal)
                {
                    org.AmountError = true;
                    org.PayStatus = "異常(溢繳)";
                    org.NeedAmount = 0;
                    org.GiveAmount = org.Subtotal;
                    org.OverAmount = org.PaidAmount - org.Subtotal;
                    org.DiffAmount = org.PaidAmount - org.Subtotal;
                }
                else
                {
                    org.AmountError = true;
                    org.PayStatus = "未繳清";
                    org.NeedAmount = org.Subtotal - org.PaidAmount;
                    org.GiveAmount = org.PaidAmount;
                    org.OverAmount = 0;
                    org.DiffAmount = org.Subtotal - org.PaidAmount;
                }
            }

            var cancel_keys = result.OrgMap.Where(x => x.Value.RegisterError && x.Value.DiffAmount == 0).Select(x => x.Key).ToList();

            foreach (var key in cancel_keys)
            {
                if (result.OrgMap.ContainsKey(key))
                {
                    result.OrgMap.Remove(key);
                }
            }

            result.TotalAmount = result.OrgMap.Sum(x => x.Value.Subtotal);

            return result;
        }

        /// <summary>
        /// 報名有問題
        /// </summary>
        private bool RegisterHasError(Item item)
        {
            return item.getProperty("register_error", "") == "1";
        }

        /// <summary>
        /// 金額有問題
        /// </summary>
        private bool AmountHasError(Item item)
        {
            return item.getProperty("amount_error", "") == "1";
        }

        /// <summary>
        /// 附加對帳項目
        /// </summary>
        private void AddPay(Dictionary<string, TPay> map, Item item, Dictionary<string, List<TInvoice>> invMap)
        {
            string key = item.getProperty("pay_key", "");

            TPay entity = null;

            if (map.ContainsKey(key))
            {
                entity = map[key];
            }
            else
            {
                entity = new TPay
                {
                    Key = key,
                    PayBool = item.getProperty("pay_bool", ""),
                    PayNumber = item.getProperty("pay_no", ""),
                    CreatorName = item.getProperty("in_creator", ""),
                    CreatorSno = item.getProperty("in_creator_sno", ""),
                    CreatorTel = item.getProperty("in_creator_tel", ""),
                    CreatorEmail = item.getProperty("in_creator_email", ""),
                    TeamMap = new Dictionary<string, TTeam>(),
                };

                if (invMap.ContainsKey(entity.PayNumber))
                {
                    entity.Invoices = invMap[entity.PayNumber];
                }
                else
                {
                    entity.Invoices = new List<TInvoice>();
                }

                entity.HasPayment = entity.PayNumber != "";

                if (entity.HasPayment)
                {
                    entity.Code2 = GetCodeMask(item.getProperty("in_code_2", ""));
                    entity.ExpectAmount = GetInt(item.getProperty("in_pay_amount_exp"));
                    entity.RealAmount = GetInt(item.getProperty("in_pay_amount_real"));
                    entity.RefundAmount = GetInt(item.getProperty("in_refund_amount"));
                    entity.FeeAmount = GetInt(item.getProperty("in_fee_amount"));
                    entity.FinalAmount = entity.ExpectAmount - entity.RefundAmount;
                }

                map.Add(key, entity);
            }

            AddTeam(entity.TeamMap, item);
        }

        /// <summary>
        /// 附加隊伍
        /// </summary>
        private void AddTeam(Dictionary<string, TTeam> map, Item item)
        {
            string key = GetTeamKey(item);
            TTeam entity = null;
            if (map.ContainsKey(key))
            {
                entity = map[key];
            }
            else
            {
                entity = new TTeam
                {
                    Key = key,
                    Amount = GetInt(item.getProperty("pay_amount", "0")),
                    Players = new List<Item>()
                };
                map.Add(key, entity);
            }

            entity.Players.Add(item);
        }

        private TOrg AddAndGetOrg(Dictionary<string, TOrg> map, Item item)
        {
            string key = item.getProperty("map_current_org", "");

            TOrg entity = null;

            if (map.ContainsKey(key))
            {
                entity = map[key];
                entity.IsNew = false;
            }
            else
            {
                entity = new TOrg
                {
                    Key = key,
                    Name = key,
                    ManagerMap = new Dictionary<string, string>(),
                    PlayerMap = new Dictionary<string, string>(),
                    PlayItemMap = new Dictionary<string, string>(),
                    SubItemMap = new Dictionary<string, TItem>(),
                    PayMap = new Dictionary<string, TPay>(),
                    Subtotal = 0,
                    IsNew = true,
                };
                map.Add(key, entity);
            }
            return entity;
        }

        private void AddItem(Dictionary<string, TItem> map, Item item)
        {
            string key = item.getProperty("in_l1", "");

            if (!map.ContainsKey(key))
            {
                map.Add(key, new TItem
                {
                    Key = key,
                    Title = ClearTitle(key),
                    Property = "in_l1_dynamic_" + map.Count,
                });
            }
        }

        /// <summary>
        /// 設定摘要
        /// </summary>
        private void SetSummary(TOrg entity, Item itmMUser)
        {
            string in_l1 = itmMUser.getProperty("in_l1", "");
            string in_name = itmMUser.getProperty("in_name", "");
            string in_sno = itmMUser.getProperty("in_sno", "");
            string in_expense = itmMUser.getProperty("in_expense", "");

            string team_key = GetTeamKey(itmMUser);

            bool is_player = in_l1 != "隊職員";

            if (!is_player)
            {
                //隊職員人數
                if (!entity.ManagerMap.ContainsKey(in_name))
                {
                    entity.ManagerMap.Add(in_name, in_name);
                }
            }
            else
            {
                //選手人數
                if (!entity.PlayerMap.ContainsKey(in_sno))
                {
                    entity.PlayerMap.Add(in_sno, in_sno);
                }

                //加入競賽項目、組別
                AddSubItemMap(entity, in_l1, team_key);

                //項目總計
                if (!entity.PlayItemMap.ContainsKey(team_key))
                {
                    entity.PlayItemMap.Add(team_key, in_expense);
                }
            }
        }

        /// <summary>
        /// 加入競賽項目、組別
        /// </summary>
        private void AddSubItemMap(TOrg entity, string in_l1, string team_index)
        {
            TItem map = null;
            if (entity.SubItemMap.ContainsKey(in_l1))
            {
                map = entity.SubItemMap[in_l1];
            }
            else
            {
                map = new TItem
                {
                    Key = in_l1,
                    Property = ClearProperty(in_l1),
                    List = new Dictionary<string, string>(),
                };
                entity.SubItemMap.Add(in_l1, map);
            }

            if (!map.List.ContainsKey(team_index))
            {
                map.List.Add(team_index, team_index);
            }
        }

        /// <summary>
        /// 過濾費用
        /// </summary>
        private string ClearTitle(string value)
        {
            if (value == "")
            {
                return "";
            }

            if (value.Contains("/"))
            {
                return value.Substring(0, value.IndexOf("/")).TrimEnd('組') + "組數";
            }
            else
            {
                return value.TrimEnd('組') + "組數";
            }
        }

        /// <summary>
        /// 過濾特殊字元
        /// </summary>
        private string ClearProperty(string value)
        {
            return value.Replace("(", "_").Replace(")", "_").Replace(".", "_").Replace("/", "_");
        }

        private string GetTeamKey(Item item)
        {
            string in_l1 = item.getProperty("in_l1", "");
            string in_l2 = item.getProperty("in_l2", "");
            string in_l3 = item.getProperty("in_l3", "");
            string in_index = item.getProperty("in_index", "");
            string in_creator_sno = item.getProperty("in_creator_sno", "");

            string in_team_key = string.Join("-", new string[]
            {
in_l1,
in_l2,
in_l3,
in_index,
in_creator_sno,
            });

            return in_team_key;
        }


        /// <summary>
        /// 取得賽事資料
        /// </summary>
        private Item GetMeeting(TConfig cfg)
        {
            string aml = @"
                <AML>
                    <Item type='In_Meeting' action='get' id='{#meeting_id}' select='keyed_name'>
                    </Item>
                </AML>
            ".Replace("{#meeting_id}", cfg.meeting_id);

            Item itmMeeting = cfg.inn.applyAML(aml);
            if (itmMeeting.isError())
            {
                throw new Exception("取得賽事資料發生錯誤");
            }
            return itmMeeting;
        }

        /// <summary>
        /// 合併與會者與繳費單
        /// </summary>
        private List<Item> MergerMUsersAndMPays(TConfig cfg)
        {
            List<Item> result = new List<Item>();
            MergerItems(result, GetMeetingUsers(cfg));
            MergerItems(result, GetMeetingPays(cfg));
            return result;
        }

        /// <summary>
        /// 合併資料
        /// </summary>
        private void MergerItems(List<Item> list, Item items)
        {
            int count = items.getItemCount();
            for (int i = 0; i < count; i++)
            {
                list.Add(items.getItemByIndex(i));
            }
        }

        /// <summary>
        /// 取得競賽項目
        /// </summary>
        private Item GetMeetingLv1s(TConfig cfg)
        {
            string sql = @"
                SELECT
                    DISTINCT
                    t4.sort_order
                    , t1.in_l1
                FROM 
                    IN_MEETING_USER t1 WITH(NOLOCK)
                LEFT OUTER JOIN
                    IN_MEETING_SURVEYS t2 WITH(NOLOCK)
                    ON t2.SOURCE_ID = t1.source_id
                INNER JOIN
                    IN_SURVEY t3 WITH(NOLOCK)
                    ON t3.id = t2.related_id
                    AND t3.in_property = 'in_l1'
                INNER JOIN
                    IN_SURVEY_OPTION t4 WITH(NOLOCK)
                    ON t4.source_id = t3.id
                    AND t4.in_value = t1.in_l1
                WHERE 
                    t1.source_id = '{#meeting_id}'
                    AND t1.in_l1 <> N'隊職員'
                ORDER BY
                    t4.sort_order
            ";
            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);
            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);
            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// 取得所有與會者資料
        /// </summary>
        private Item GetMeetingAllUsers(TConfig cfg)
        {
            string sql = @"
                SELECT
                    t1.id
                    , t1.in_l1
                    , t1.in_l2
                    , t1.in_l3
                    , t1.in_index
                    , t1.in_team
                    , t1.in_name
                    , t1.in_sno
                    , t1.in_expense
                    , t1.in_creator_sno
                    , t1.in_current_org AS 'map_current_org'
                FROM 
                    IN_MEETING_USER t1 WITH(NOLOCK)
                WHERE 
                    t1.source_id = '{#meeting_id}'
                ORDER BY 
                    t1.in_current_org
                    , t1.in_l1
                    , t1.in_l2
                    , t1.in_l3
                    , t1.in_index
            ";
            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);
            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);
            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// 取得與會者資料
        /// </summary>
        private Item GetMeetingUsers(TConfig cfg)
        {
            string sql = @"
                SELECT
                    t1.in_creator_sno AS 'pay_key'
                    , ''              AS 'pay_no'
                    , '未確認'         AS 'pay_bool'
                    , t1.in_expense   AS 'pay_amount'
                    , t1.in_l1
                    , t1.in_l2
                    , t1.in_l3
                    , t1.in_index
                    , t1.in_team
                    , t1.in_name
                    , t1.in_sno
                    , t1.in_gender
                    , t1.in_birth
                    , t1.in_creator
                    , t1.in_creator_sno
                    , t2.in_tel   AS 'in_creator_tel'
                    , t2.in_email AS 'in_creator_email'
                    , ''          AS 'in_code_2'
                    , ''          AS 'in_pay_amount_exp'
                    , ''          AS 'in_pay_amount_real'
                    , ''          AS 'in_refund_amount'
                    , t1.in_current_org AS 'map_current_org'
                FROM 
                    IN_MEETING_USER t1 WITH(NOLOCK)
                LEFT OUTER JOIN
                    IN_RESUME t2 WITH(NOLOCK)
                    ON t2.in_sno = t1.in_creator_sno
                WHERE 
                    t1.source_id = '{#meeting_id}'
                    AND t1.in_l1 <> N'隊職員'
                    AND ISNULL(t1.in_paynumber, '') = ''
                ORDER BY 
                    t1.in_current_org
                    , t1.in_l1
                    , t1.in_l2
                    , t1.in_l3
                    , t1.in_index
            ";
            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);
            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);
            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// 取得繳費單資料
        /// </summary>
        private Item GetMeetingPays(TConfig cfg)
        {
            string sql = @"
                SELECT
                    t2.item_number     AS 'pay_key'
                    , t2.item_number   AS 'pay_no'
                    , t2.pay_bool
                    , t1.in_pay_amount AS 'pay_amount'
                    , t1.in_l1
                    , t1.in_l2
                    , t1.in_l3
                    , t1.in_index
                    , '' AS 'in_team'
                    , t1.in_name
                    , t1.in_sno
                    , '' AS 'in_gender'
                    , '' AS 'in_birth'
                    , t1.in_creator
                    , t1.in_creator_sno
                    , t3.in_tel   AS 'in_creator_tel'
                    , t3.in_email AS 'in_creator_email'
                    , t2.in_code_2
                    , t2.in_pay_amount_exp
                    , t2.in_pay_amount_real
                    , t2.in_refund_amount
                    , t2.in_current_org AS 'map_current_org'
                    , t2.in_fee_amount
                FROM 
                    IN_MEETING_NEWS t1 WITH(NOLOCK)
                INNER JOIN
                    IN_MEETING_PAY t2 WITH(NOLOCK)
                    ON t2.id = t1.source_id
                LEFT OUTER JOIN
                    IN_RESUME t3 WITH(NOLOCK)
                    ON t3.in_sno = t2.in_creator_sno
                WHERE 
                    t2.in_meeting = '{#meeting_id}'
                    AND t1.in_l1 <> N'隊職員'
                    AND t2.pay_bool <> N'已取消'
                ORDER BY 
                    t2.item_number
            ";
            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);
            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);
            return cfg.inn.applySQL(sql);
        }
        
        private Dictionary<string, List<TInvoice>> GetMeetingInvoiceMap(TConfig cfg)
        {
            var map = new Dictionary<string, List<TInvoice>>();
            var items = GetMeetingInvoices(cfg);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var key = item.getProperty("pay_no", "");
                var uniform_numbers = item.getProperty("uniform_numbers", "");
                var invoice_up = item.getProperty("invoice_up", "");
                var invoice_amount = item.getProperty("invoice_amount", "");
                var row = new TInvoice
                {
                    InvoiceUp = invoice_up,
                    Amount = GetInt(invoice_amount),
                    UniformNumbers = uniform_numbers,
                };
                if (map.ContainsKey(key))
                {
                    map[key].Add(row);
                }
                else 
                {
                    map.Add(key, new List<TInvoice> { row });
                }
            }
            return map;
        }

        private Item GetMeetingInvoices(TConfig cfg)
        {
            var sql = @"
                SELECT
	                t1.id				AS 'pay_id'
	                , t1.item_number	AS 'pay_no'
	                , t2.uniform_numbers
	                , t2.invoice_up
	                , t2.invoice_amount
	                , t3.in_short_org
                FROM
	                IN_MEETING_PAY t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_INVOICE t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                LEFT OUTER JOIN
	                IN_ORG_MAP t3 WITH(NOLOCK)
	                ON t3.in_stuff_b1 = LEFT(t2.invoice_up, 3)
                WHERE
	                t1.in_meeting = '{#meeting_id}'
                ORDER BY
	                t1.item_number
            ";
            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);
            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);
            return cfg.inn.applySQL(sql);
        }

        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string meeting_id { get; set; }
            public Item itmMeeting { get; set; }
        }

        /// <summary>
        /// 報名總表
        /// </summary>
        private class GameSummary
        {
            /// <summary>
            /// 單位摘要
            /// </summary>
            public Dictionary<string, TOrg> OrgMap { get; set; }

            /// <summary>
            /// 競賽項目
            /// </summary>
            public Dictionary<string, TItem> ItemMap { get; set; }

            /// <summary>
            /// 報名總費用
            /// </summary>
            public int TotalAmount { get; set; }
        }

        /// <summary>
        /// 單位摘要
        /// </summary>
        private class TOrg
        {
            /// <summary>
            /// 鍵
            /// </summary>
            public string Key { get; set; }

            /// <summary>
            /// 單位名稱
            /// </summary>
            public string Name { get; set; }

            /// <summary>
            /// 隊職員人數
            /// </summary>
            public Dictionary<string, string> ManagerMap { get; set; }

            /// <summary>
            /// 選手人數
            /// </summary>
            public Dictionary<string, string> PlayerMap { get; set; }

            /// <summary>
            /// 項目總計
            /// </summary>
            public Dictionary<string, string> PlayItemMap { get; set; }

            /// <summary>
            /// 競賽項目各項統計
            /// </summary>
            public Dictionary<string, TItem> SubItemMap { get; set; }

            /// <summary>
            /// 繳費清單
            /// </summary>
            public Dictionary<string, TPay> PayMap { get; set; }

            /// <summary>
            /// 報名費用
            /// </summary>
            public int Subtotal { get; set; }

            /// <summary>
            /// 未確認費用
            /// </summary>
            public int UnConfirmAmount { get; set; }

            /// <summary>
            /// 未繳費用
            /// </summary>
            public int UnPaidAmount { get; set; }

            /// <summary>
            /// 已繳費用
            /// </summary>
            public int PaidAmount { get; set; }

            /// <summary>
            /// 對帳狀態
            /// </summary>
            public string PayStatus { get; set; }

            ///// <summary>
            ///// 差異費用
            ///// </summary>
            //public int DiffAmount { get; set; }

            /// <summary>
            /// 未給付費用
            /// </summary>
            public int NeedAmount { get; set; }

            /// <summary>
            /// 已給付費用
            /// </summary>
            public int GiveAmount { get; set; }

            /// <summary>
            /// 溢繳費用
            /// </summary>
            public int OverAmount { get; set; }

            /// <summary>
            /// 差異費用
            /// </summary>
            public int DiffAmount { get; set; }

            /// <summary>
            /// 是新增的單位
            /// </summary>
            public bool IsNew { get; set; }

            /// <summary>
            /// 報名有問題(有繳費單，無報名資料)
            /// </summary>
            public bool RegisterError { get; set; }

            /// <summary>
            /// 金額有問題
            /// </summary>
            public bool AmountError { get; set; }
        }

        private class TItem
        {
            public string Key { get; set; }
            public string Title { get; set; }
            public string Property { get; set; }
            public Dictionary<string, string> List { get; set; }
        }

        /// <summary>
        /// 帳務
        /// </summary>
        private class TPay
        {
            /// <summary>
            /// 鍵
            /// </summary>
            public string Key { get; set; }

            /// <summary>
            /// 繳費狀態
            /// </summary>
            public string PayBool { get; set; }

            /// <summary>
            /// 是否有繳費單
            /// </summary>
            public bool HasPayment { get; set; }

            /// <summary>
            /// 繳費單編號
            /// </summary>
            public string PayNumber { get; set; }

            /// <summary>
            /// 虛擬帳號
            /// </summary>
            public string Code2 { get; set; }

            /// <summary>
            /// 應收金額
            /// </summary>
            public int ExpectAmount { get; set; }

            /// <summary>
            /// 實收金額
            /// </summary>
            public int RealAmount { get; set; }

            /// <summary>
            /// 退款金額
            /// </summary>
            public int RefundAmount { get; set; }

            /// <summary>
            /// 最後金額
            /// </summary>
            public int FinalAmount { get; set; }

            /// <summary>
            /// 退款手續費
            /// </summary>
            public int FeeAmount { get; set; }

            /// <summary>
            /// 協助報名者姓名
            /// </summary>
            public string CreatorName { get; set; }

            /// <summary>
            /// 協助報名者身分證號
            /// </summary>
            public string CreatorSno { get; set; }

            /// <summary>
            /// 協助報名者連絡電話
            /// </summary>
            public string CreatorTel { get; set; }

            /// <summary>
            /// 協助報名者 email
            /// </summary>
            public string CreatorEmail { get; set; }

            /// <summary>
            /// 隊伍列表
            /// </summary>
            public Dictionary<string, TTeam> TeamMap { get; set; }

            /// <summary>
            /// 收據列表
            /// </summary>
            public List<TInvoice> Invoices { get; set; }
        }

        /// <summary>
        /// 隊伍
        /// </summary>
        private class TTeam
        {
            /// <summary>
            /// 隊伍鍵
            /// </summary>
            public string Key { get; set; }

            /// <summary>
            /// 報名費用
            /// </summary>
            public int Amount { get; set; }

            /// <summary>
            /// 隊伍列表
            /// </summary>
            public List<Item> Players { get; set; }
        }

        /// <summary>
        /// 收據
        /// </summary>
        private class TInvoice
        {
            /// <summary>
            /// 發票抬頭
            /// </summary>
            public string InvoiceUp { get; set; }
            /// <summary>
            /// 統一編號
            /// </summary>
            public string UniformNumbers { get; set; }
            /// <summary>
            /// 銷帳金額
            /// </summary>
            public int Amount { get; set; }
        }

        private class TField
        {
            public string title { get; set; }
            public string property { get; set; }
            public string format { get; set; }
            public int width { get; set; }
            public bool color { get; set; }
            public Func<Item, bool> color_func { get; set; }
        }

        private string GetDtm(string value, string format, bool bAdd8Hours = false)
        {
            DateTime result = DateTime.MinValue;
            DateTime.TryParse(value, out result);
            if (result == DateTime.MinValue) return "";

            return bAdd8Hours
                ? result.AddHours(8).ToString(format)
                : result.ToString(format);
        }

        private int GetInt(string value, int def = 0)
        {
            int result = def;
            int.TryParse(value, out result);
            return result;
        }

        //in_meeting_pay 條碼2 需加上遮罩
        private string GetCodeMask(string value)
        {
            return value;
        }

        private string GetCodeMask_OLD(string value)
        {
            if (value.Length == 16)
            {
                return value.Substring(0, 4) + "*******" + value.Substring(value.Length - 5, 5);
            }
            return "";
        }
        }
}