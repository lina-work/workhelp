﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Judo.團體組
{
    internal class In_Team_Battle_Draw : Item
    {
        public In_Team_Battle_Draw(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            //System.Diagnostics.Debugger.Break();
            //使用 RNGCryptoServiceProvider 產生由密碼編譯服務供應者 (CSP) 提供的亂數產生器。
            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "In_Team_Battle_Draw";

            var itmR = this;
            var meeting_id = itmR.getProperty("meeting_id", "");
            var value = itmR.getProperty("value", "");
            var wait = itmR.getProperty("wait", "");
            var scene = itmR.getProperty("scene", "").ToUpper();

            var itmMeeting = inn.applySQL("SELECT in_title FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + meeting_id + "'");
            itmR.setProperty("in_title", itmMeeting.getProperty("in_title", ""));

            if (scene != "run")
            {
                return itmR;
            }
            
            var sec = GetInt(value);
            var v1 = sec * 1000 + 300;
            var v2 = v1 + 1300;
            if (wait == "Y")
            {
                v1 = 100;
                v2 = 100;
            }

            var arr = InnSport.Core.Utilities.TUtility.RandomArray(5);
            var no = InnSport.Core.Utilities.TUtility.RandomNo(1, 5);

            var cb = new StringBuilder();
            cb.AppendLine("<style>");
            cb.AppendLine("	#box ");
            cb.AppendLine("	{");
            cb.AppendLine("		width: 220px;");
            cb.AppendLine("		height: 120px;");
            cb.AppendLine("		background-color: white;");
            //cb.AppendLine("		background-image: url('../Images/team-fight/0" + arr[0] +".png');");
            cb.AppendLine("		animation-name: example;");
            cb.AppendLine("		animation-duration: " + sec + "s;");
            cb.AppendLine("     animation-timing-function: linear");
            cb.AppendLine("	}");
            cb.AppendLine("");
            cb.AppendLine("	@keyframes example ");
            cb.AppendLine("	{");
            cb.AppendLine("		0%   { background-image: url('../Images/team-fight/0" + arr[0] + ".png') }");
            cb.AppendLine("		30%  { background-image: url('../Images/team-fight/0" + arr[1] + ".png') }");
            cb.AppendLine("		50%  { background-image: url('../Images/team-fight/0" + arr[2] + ".png') }");
            cb.AppendLine("		75%  { background-image: url('../Images/team-fight/0" + arr[3] + ".png') }");
            cb.AppendLine("		100% { background-image: url('../Images/team-fight/0" + arr[4] + ".png') }");
            cb.AppendLine("	}");
            cb.AppendLine("</style>");
            cb.AppendLine("<div id='box'></div>");

            cb.AppendLine("<div id='sub' class='display:none'>");
            cb.AppendLine("  <h2 style='color: blue'>鋒將位隨機排序</h2>");
            cb.AppendLine("  <img src='../Images/team-fight/0" + arr[0] + ".png' class='bordered' >");
            cb.AppendLine("  <img src='../Images/team-fight/0" + arr[1] + ".png' class='bordered' >");
            cb.AppendLine("  <img src='../Images/team-fight/0" + arr[2] + ".png' class='bordered' >");
            cb.AppendLine("  <img src='../Images/team-fight/0" + arr[3] + ".png' class='bordered' >");
            cb.AppendLine("  <img src='../Images/team-fight/0" + arr[4] + ".png' class='bordered' >");
            cb.AppendLine("  <h2 style='color: blue'>本次抽到的位置: " + no + " </h2>");
            cb.AppendLine("</div>");
            cb.AppendLine("<script>");
            cb.AppendLine("    $('#sub').hide();");
            cb.AppendLine("    setTimeout(() => {");
            cb.AppendLine("        $('#box').hide();");
            cb.AppendLine("        $('#sub').show();");
            cb.AppendLine("    }, " + v1 + ");");
            cb.AppendLine("    setTimeout(() => {");
            cb.AppendLine("        $('#sub').append(`<img src='../Images/team-fight/0" + arr[(no - 1)] + ".png' class='bordered' >`);");
            cb.AppendLine("    }, " + v2 + ");");
            cb.AppendLine("</script>");

            itmR.setProperty("draw_box", cb.ToString());

            return itmR;
        }

        private int GetInt(string value, int defV = 0)
        {
            if (value == "") return 0;
            int result = defV;
            if (int.TryParse(value, out result)) return result;
            return defV;

        }
    }
}