﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Judo.團體組
{
    internal class in_team_battle_span : Item
    {
        public in_team_battle_span(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 團體組-體重寬限檢查
                日誌: 
                    - 2024-11-08: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "in_team_battle_span";

            var itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                meeting_id = itmR.getProperty("meeting_id", ""),
                program_id = itmR.getProperty("program_id", ""),
                scene = itmR.getProperty("scene", ""),
            };

            switch (cfg.scene)
            {
                case "page":
                    AppendMeeting(cfg, itmR);
                    AppendProgramItem(cfg, itmR);
                    Page(cfg, itmR);
                    break;
            }

            return itmR;
        }

        //頁面
        private void Page(TConfig cfg, Item itmReturn)
        {
            var items = GetMeetingUserItems(cfg);
            var tables = GenerateTableContents(cfg, items);
            itmReturn.setProperty("inn_tables", tables);
        }

        private string GenerateTableContents(TConfig cfg, Item items)
        {
            var head = new StringBuilder();
            var body = new StringBuilder();

            var headCss = "mailbox-subject text-center bg-primary";
            head.Append("<tr>");
            head.Append("  <th class='" + headCss + "' data-width='5%' >No.</th>");
            head.Append("  <th class='" + headCss + "' data-width='14%'>單位</th>");
            head.Append("  <th class='" + headCss + "' data-width='14%'>姓名</th>");
            head.Append("  <th class='" + headCss + "' data-width='8%'>團體組<BR>體重</th>");
            head.Append("  <th class='" + headCss + "' data-width='10%'>個人組<BR>級</th>");
            head.Append("  <th class='" + headCss + "' data-width='10%'>個人組<BR>量</th>");
            head.Append("  <th class='" + headCss + "' data-width='8%'>MIN</th>");
            head.Append("  <th class='" + headCss + "' data-width='8%'>MAX</th>");
            head.Append("  <th class='" + headCss + "' data-width='8%'>寬限</th>");
            head.Append("  <th class='" + headCss + "' data-width='15%'>狀態</th>");
            head.Append("</tr>");

            var bodyCss = "mailbox-subject text-center";
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var in_stuff_b1 = item.getProperty("in_stuff_b1", "");
                var in_short_org = item.getProperty("in_short_org", "");
                var in_show_org = item.getProperty("in_show_org", "");
                var in_team = item.getProperty("in_team", "");
                var in_l3 = item.getProperty("in_l3", "");
                var is_span = in_l3 == "高中女子組" || in_l3 == "國中男子組";

                var in_team_n2 = item.getProperty("in_team_n2", "");

                var in_weight = item.getProperty("in_weight", "0");
                var in_min = item.getProperty("in_min", "0");
                var in_max = item.getProperty("in_max", "0");
                var span_max = item.getProperty("span_max", "0");
                var status = "";

                var vw = GetDcm(in_weight);
                var vn = GetDcm(in_min);
                var vx = GetDcm(in_max);
                var vs = GetDcm(span_max);
                
                if (vs >= 999)
                {
                    span_max = "";//無上限
                }

                if (in_team_n2 == "")
                {
                    span_max = "";
                    status = "<span style='color: #C6C6C6'>未報個人組</span>";
                }
                else
                {
                    if (vw <= 0)
                    {
                        in_weight = "";
                        status = "<span style='color: #F1F1F1'>未過磅</span>";
                    }
                    else if (vw >= vn && vw <= vx)
                    {
                        status = "範圍內";
                    }
                    else if (vw > vs)
                    {
                        if (is_span)
                        {
                            status = "<span style='color: red'>超過5%</span>";
                        }
                        else
                        {
                            status = "<span style='color: red'>無5%寬限</span>";
                        }
                    }
                    else if (vw > vx)
                    {
                        status = "<span style='color: #FF9900'>超過MAX，小於5%</span>";
                    }
                }

                if (in_team != "") in_short_org = in_short_org + " " + in_team;
                if (in_stuff_b1 != "") in_short_org = in_stuff_b1 + " " + in_short_org;
                if (in_show_org != "") in_short_org = in_show_org + "<BR>" + in_short_org;

                body.Append("<tr>");
                body.Append("  <td class='" + bodyCss + "'>" + (i + 1) + "</td>");
                body.Append("  <td class='" + bodyCss + "'>" + in_short_org + "</td>");
                body.Append("  <td class='" + bodyCss + "'>" + item.getProperty("in_name", "") + "</td>");
                body.Append("  <td class='" + bodyCss + "'>" + in_weight + "</td>");
                body.Append("  <td class='" + bodyCss + "'>" + item.getProperty("in_team_n2", "") + "</td>");
                body.Append("  <td class='" + bodyCss + "'>" + item.getProperty("in_team_n3", "") + "</td>");
                body.Append("  <td class='" + bodyCss + "'>" + item.getProperty("in_min", "") + "</td>");
                body.Append("  <td class='" + bodyCss + "'>" + item.getProperty("in_max", "") + "</td>");
                body.Append("  <td class='" + bodyCss + "'>" + span_max + "</td>");
                body.Append("  <td class='" + bodyCss + "'>" + status + "</td>");
                body.Append("</tr>");
            }

            var table_name = "data-table";
            var builder = new StringBuilder();
            builder.Append("<div class='box-body tablenobg' style='position: relative'>");
            builder.AppendLine(GetTableAttribute(table_name));
            builder.Append("<thead>");
            builder.Append(head);
            builder.Append("</thead>");
            builder.Append("<tbody>");
            builder.Append(body);
            builder.Append("</tbody>");
            builder.AppendLine("</table>");
            builder.Append("</div>");

            builder.AppendLine("<script>");
            builder.AppendLine("$('#" + table_name + "').bootstrapTable();");
            builder.AppendLine("if ($(window).width() <= 768) { $('#" + table_name + "').bootstrapTable('toggleView'); }");
            builder.AppendLine("</script>");

            return builder.ToString();
        }

        private string GetTableAttribute(string table_name)
        {
            return "<table id='" + table_name + "'"
                + " class='table table-hover table-striped'"
                + " data-toggle='table'"
                + " data-show-toggle='false'"
                + " data-show-columns='false'"
                + " data-search-align='right'"
                + " data-sort-stable='false'"
                + " data-search='false'"
                + ">";
        }


        //附加賽事資訊
        private void AppendMeeting(TConfig cfg, Item itmReturn)
        {
            var sql = "SELECT id, in_title, in_uniform_color FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'";
            cfg.itmMeeting = cfg.inn.applySQL(sql);

            itmReturn.setProperty("mt_title", cfg.itmMeeting.getProperty("in_title", ""));
        }

        //附加組別資訊
        private void AppendProgramItem(TConfig cfg, Item itmReturn)
        {
            var sql = "SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + cfg.program_id + "'";
            cfg.itmProgram = cfg.inn.applySQL(sql);

            itmReturn.setProperty("pg_name", cfg.itmProgram.getProperty("in_name", "").Replace("團-", ""));
        }

        private Item GetMeetingUserItems(TConfig cfg)
        {
            var sql = @"
                SELECT 
                    t2.in_fight_day
                    , t1.in_stuff_b1
                    , t1.in_current_org
                    , t1.in_short_org
                    , t1.in_show_org
                    , t1.in_team
	                , t1.in_name
	                , t1.in_l3
	                , t1.in_weight
	                , t1.in_team_n2
	                , t1.in_team_n3
	                , t3.in_min
	                , t3.in_max
	                , CONVERT(INT, t3.in_max) * 1.05 AS 'span_max'
                FROM 
	                IN_MEETING_USER t1 WITH(NOLOCK) 
                INNER JOIN
	                IN_MEETING_PROGRAM t2 WITH(NOLOCK)
	                ON t2.in_meeting = t1.source_id
	                AND t2.in_l1 = t1.in_l1
	                AND t2.in_l2 = t1.in_l2
	                AND t2.in_l3 = t1.in_l3
                LEFT OUTER JOIN
	                IN_GROUP_WEIGHT t3
	                ON t3.in_group = t1.in_team_n1
	                AND t3.in_level = t1.in_team_n2
                WHERE 
	                t1.source_id = '{#meeting_id}'
	                AND t2.id = '{#program_id}'
                ORDER BY
                    t1.in_city_no
	                , t1.in_stuff_b1
	                , t1.in_current_org
	                , t1.in_team
	                , t1.in_gender
	                , t1.in_sno
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#program_id}", cfg.program_id);

            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string meeting_id { get; set; }
            public string program_id { get; set; }
            public string scene { get; set; }

            public Item itmMeeting { get; set; }
            public Item itmProgram { get; set; }
        }

        private decimal GetDcm(string value, decimal defV = 0)
        {
            if (value == "") return 0;
            decimal result = defV;
            if (decimal.TryParse(value, out result)) return result;
            return defV;
        }

        private int GetInt(string value, int defV = 0)
        {
            if (value == "") return 0;
            int result = defV;
            if (int.TryParse(value, out result)) return result;
            return defV;
        }


    }
}