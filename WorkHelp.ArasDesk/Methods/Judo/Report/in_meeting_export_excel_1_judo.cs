﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Judo.Report
{
    public class in_meeting_export_excel_1_judo : Item
    {
        public in_meeting_export_excel_1_judo(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 柔系列(柔道,台灣柔道,柔術)-產生秩序冊
                日誌: 
                    - 2023-05-04: 校正 三人團體賽制 B隊位置錯位問題 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "in_meeting_export_excel_1_judo";

            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            Item itmR = this;

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                strUserId = inn.getUserID(),
                meeting_id = itmR.getProperty("meeting_id", "925FB369E0D6488F8D618E86FBADA72E"),
                scene = itmR.getProperty("scene", ""),
            };

            if (cfg.meeting_id == "")
            {
                throw new Exception("課程id不得為空白");
            }


            //如果是屬於 ACT_GymOwner 則只能看到自己道館的資料
            string SQL_OnlyMyGym = "";

            string aml = "<AML><Item type='In_Resume' action='get'><in_user_id>{#strUserId}</in_user_id></Item></AML>";
            aml = aml.Replace("{#strUserId}", cfg.strUserId);
            Item Resume = cfg.inn.applyAML(aml);
            if (CCO.Permissions.IdentityListHasId(Aras.Server.Security.Permissions.Current.IdentitiesList, "7AC8B33DA0F246DDA70BB244AB231E29"))
            {
                SQL_OnlyMyGym = " AND [In_Meeting_User].in_creator_sno = '" + Resume.getProperty("in_sno", "") + "'";
            }


            //活動資料
            Item Meeting = GetMeetingItem(cfg);

            //建立 Students 的欄位
            Dictionary<string, Item> StudentFields = new Dictionary<string, Item>();

            int ColIndex = 1;

            var itmTemp = default(Item);
            var ExpenseProperty = ""; //紀錄本賽事的費用層次
            var Meeting_Surveys_Count = Meeting.getRelationships("In_Meeting_Surveys").getItemCount();
            for (int i = 0; i < Meeting_Surveys_Count; i++)
            {
                Item In_Meeting_Survey = Meeting.getRelationships("In_Meeting_Surveys").getItemByIndex(i);
                itmTemp = In_Meeting_Survey.getPropertyItem("related_id");
                itmTemp.setProperty("col_index", (ColIndex++).ToString());
                itmTemp.setProperty("inn_id", In_Meeting_Survey.getProperty("related_id", ""));
                StudentFields.Add(itmTemp.getProperty("inn_id"), itmTemp);

                if (itmTemp.getProperty("in_expense", "") == "1")
                {
                    ExpenseProperty = itmTemp.getProperty("in_property", ""); //可能是 in_l1, in_l2, in_l3
                }
            }

            Meeting_Surveys_Count++;
            itmTemp = inn.newItem("In_Meeting_Surveys");
            itmTemp.setProperty("inn_id", "expense");
            itmTemp.setProperty("in_questions", "金額");
            itmTemp.setProperty("in_request", "0");
            itmTemp.setProperty("col_index", (ColIndex++).ToString());
            itmTemp.setProperty("in_selectoption", "");
            itmTemp.setProperty("in_question_type", "number");
            StudentFields.Add(itmTemp.getProperty("inn_id"), itmTemp);

            //取得 Excel 相關參數
            SetExcelVariable(cfg);

            var wb = new ClosedXML.Excel.XLWorkbook(cfg.template_path);
            var StudentsWS = wb.Worksheet(1);
            var TestWs = wb.Worksheet(2);

            ClosedXML.Excel.IXLCell foundCell;
            ClosedXML.Excel.IXLCell Org_foundCell;


            //先處理 students 的欄位
            int WSRow = 3;
            int WSCol = 0;

            string L1Name = "";
            string L2Name = "";
            string L3Name = "";

            foreach (KeyValuePair<string, Item> StudentField in StudentFields)
            {
                var tmp = StudentField.Value;
                var idx = tmp.getProperty("col_index", "0");
                var prp = tmp.getProperty("in_property", "");
                var qst = tmp.getProperty("in_questions", "");

                WSCol = int.Parse(idx) + 1;

                if (prp == "in_l1") L1Name = qst;
                if (prp == "in_l2") L2Name = qst;
                if (prp == "in_l3") L3Name = qst;
            }

            //算出有幾間群組
            var sql = "SELECT DISTINCT in_current_org AS 'c1' FROM IN_MEETING_USER WITH(NOLOCK)"
                + " WHERE source_id = '" + cfg.meeting_id + "'"
                + " AND in_note_state = 'official'"
                + " AND in_l1 NOT IN (N'常年會費')"
                + SQL_OnlyMyGym
                + " ORDER BY in_current_org ASC";
            Item Orgs = inn.applySQL(sql);
            Dictionary<string, Item> DicOrgs = new Dictionary<string, Item>();

            int index = 0;
            int SheetIndex = 3; //****新頁籤的 初始化次序 *************	
            for (int i = 0; i < Orgs.getItemCount(); i++)
            {
                Item Org = Orgs.getItemByIndex(i);
                string C1 = Org.getProperty("c1", "");

                string OrgName = C1;

                if (!DicOrgs.ContainsKey(OrgName))
                {
                    Org.setProperty("last_row", "3");
                    Org.setProperty("sheet_index", SheetIndex.ToString());//
                    SheetIndex++;
                    //隊職員數	選手人數	參賽項目總計	報名費用
                    Org.setProperty("players", "0");
                    Org.setProperty("stuffs", "0");
                    Org.setProperty("items", "0");
                    Org.setProperty("gameitems", ",");
                    Org.setProperty("expenses", "0");

                    Org.setProperty("stuff_names", ",");
                    Org.setProperty("player_names", ",");

                    Org.setProperty("player_1_names", ","); //隊職員
                    Org.setProperty("player_2_names", ","); //團體組
                    Org.setProperty("player_3_names", ","); //個人組
                    Org.setProperty("player_4_names", ","); //格式組


                    DicOrgs.Add(OrgName, Org);
                }
            }

            //處理學員
            WSCol = 1;
            WSRow = 4;
            //序號 第一層 第二層 姓名
            string[] single_mufields = { "in_index", "in_l1", "in_l2", "in_name" };

            //序號 第一層  姓名~6
            string[] group_fields = { "in_index", "in_l1", "in_l2", "in_name", "in_name1", "in_name2", "in_name3", "in_name4", "in_name5", "in_name6" };

            //計算總費用的方式:同一個費用層次且同一序號,只能算一個費用,例如雙打/2000, 代表 兩個人總共2000

            string CommanExpenseProperty = "";
            if (ExpenseProperty != "in_l1")
            {
                CommanExpenseProperty = "," + ExpenseProperty;
            }

            //更新與會者 Excel 排序欄位
            UpdateMeetingUserExcelOrder(cfg);

            sql = @"
				SELECT 
					id
					, in_name
					, in_sno
					, in_current_org
					, in_show_org
					, in_l1
					, in_l2
					, in_l3
					, in_index
					, in_team
					, in_exe_a1
					, in_excel_order
					, in_excel_order_2
					, in_excel_order_3
				FROM
					IN_MEETING_USER WITH(NOLOCK)
				WHERE
					source_id = '{#meeting_id}'
					AND in_note_state = 'official'
					AND in_l1 NOT IN (N'常年會費')
				ORDER BY
					in_current_org
                	, CAST(in_excel_order   AS INT)
                	, CAST(in_excel_order_2 AS INT)
                	, CAST(in_excel_order_3 AS INT)
					, in_l1 DESC
					, in_l2
					, in_index
					, in_name
			";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            Item MeetingUsers = cfg.inn.applySQL(sql);

            string PreExpenseKey = "";
            //string ThisExpenseKey = "";

            int UserPosIndex = 0;
            string pre_l1 = "";
            string pre_l2 = "";
            string pre_l3 = "";
            string pre_idx = "";
            string gre = "";
            Item student_row;

            //為了確保每一個L2的人名都是唯一
            var DicL2Names = new Dictionary<string, string>();
            var muser = new TMUser();

            for (int i = 0; i < MeetingUsers.getItemCount(); i++)
            {
                WSRow = i + 4;

                Item itmMUser = MeetingUsers.getItemByIndex(i);

                ResetMUser(cfg, muser, itmMUser);

                //ThisExpenseKey = In_Meeting_User.getProperty(ExpenseProperty, "") + In_Meeting_User.getProperty("in_index", "");

                Item DicOrg = DicOrgs[muser.in_current_org];

                //更新統計數字
                int players = int.Parse(DicOrg.getProperty("players", "0"));
                int stuffs = int.Parse(DicOrg.getProperty("stuffs", "0"));

                string stuff_names = DicOrg.getProperty("stuff_names", "");
                string player_names = DicOrg.getProperty("player_names", "");

                string DistinctL2 = muser.in_current_org
                    + muser.in_l1
                    + muser.in_l2
                    + muser.in_l3;

                if (!DicL2Names.ContainsKey(DistinctL2))
                {
                    DicL2Names.Add(DistinctL2, ",");
                }

                string User_Names = DicL2Names[DistinctL2];

                string player_1_names = DicOrg.getProperty("player_1_names", "");
                string player_2_names = DicOrg.getProperty("player_2_names", "");
                string player_3_names = DicOrg.getProperty("player_3_names", "");
                string player_4_names = DicOrg.getProperty("player_4_names", "");


                if (!User_Names.Contains("," + muser.in_name + ","))
                {
                    User_Names += muser.in_name + ",";
                    DicL2Names[DistinctL2] = User_Names;
                }
                else
                {
                    continue;
                }

                UserPosIndex = int.Parse(DicOrg.getProperty("UserPosIndex", "0"));
                int LastRow = DicOrg.getRelationships("student_row").getItemCount();

                //這裡要決定是否跳row, 如果 UserPosIndex=6 或者 與前面的 L2 不相同,都要換Row, 否則就不換row

                if (muser.is_kata)
                {
                    string ddd = muser.in_l1 + muser.in_l3 + muser.in_index;
                    if (LastRow == 0 || UserPosIndex == 7 || muser.in_l1 != pre_l1 || muser.in_l2 != pre_l2 || ddd != gre)
                    {
                        student_row = inn.newItem("student_row");
                        student_row.setProperty("col_1", "0");
                        student_row.setProperty("col_2", muser.in_l1);
                        student_row.setProperty("col_3", muser.in_l2);
                        student_row.setProperty("in_team", muser.in_team);//團體 補上隊別
                        student_row.setProperty("col_ttr", muser.in_l3);

                        DicOrg.addRelationship(student_row);
                        LastRow++;
                        UserPosIndex = 0;
                    }
                }
                else if (muser.is_team)
                {
                    string ddd2 = muser.in_l1 + muser.in_l3 + muser.in_index;
                    if (LastRow == 0 || UserPosIndex == 7 || muser.in_l1 != pre_l1 || muser.in_l2 != pre_l2 || ddd2 != gre)
                    {
                        student_row = inn.newItem("student_row");
                        student_row.setProperty("col_1", "0");
                        student_row.setProperty("col_2", muser.in_l1);
                        student_row.setProperty("col_3", muser.team_sect);
                        student_row.setProperty("in_team", muser.in_team);//團體 補上隊別
                        student_row.setProperty("col_ttr", muser.in_l3);

                        DicOrg.addRelationship(student_row);
                        LastRow++;
                        UserPosIndex = 0;
                    }
                }
                else
                {
                    if (LastRow == 0 || UserPosIndex == 7 || muser.in_l1 != pre_l1 || muser.in_l2 != pre_l2)
                    {
                        student_row = inn.newItem("student_row");
                        student_row.setProperty("col_1", "0");
                        student_row.setProperty("col_2", muser.in_l1);
                        student_row.setProperty("col_3", muser.in_l2);
                        student_row.setProperty("in_team", muser.in_team);//團體 補上隊別
                        student_row.setProperty("col_ttr", muser.in_l3);

                        DicOrg.addRelationship(student_row);
                        LastRow++;
                        UserPosIndex = 0;
                    }
                }

                pre_l1 = muser.in_l1;
                pre_l2 = muser.in_l2;
                pre_l3 = muser.in_l3;
                pre_idx = muser.in_index;
                gre = muser.in_l1 + muser.in_l3 + muser.in_index;

                student_row = DicOrg.getRelationships("student_row").getItemByIndex(LastRow - 1);
                student_row.setProperty("col_" + (UserPosIndex + 4).ToString(), muser.in_name);

                UserPosIndex++;
                DicOrg.setProperty("UserPosIndex", UserPosIndex.ToString());

                //因為是格式組,有被施術者,而且有可能超過7個人的限制而被加到下一排
                if (muser.is_kata)
                {
                    if (muser.in_exe_a1 == "")
                    {
                        continue;
                    }
                    if (!User_Names.Contains("," + muser.in_exe_a1 + ","))
                    {
                        User_Names += muser.in_exe_a1 + ",";
                        DicL2Names[DistinctL2] = User_Names;
                    }
                    else
                    {
                        continue;
                    }

                    if (UserPosIndex == 7)
                    {
                        student_row = inn.newItem("student_row");
                        student_row.setProperty("col_1", "0");
                        student_row.setProperty("col_2", muser.in_l1);
                        student_row.setProperty("col_3", muser.in_l2);
                        student_row.setProperty("col_ttr", muser.in_l3);
                        DicOrg.addRelationship(student_row);
                        LastRow++;
                        UserPosIndex = 0;
                    }

                    student_row.setProperty("col_" + (UserPosIndex + 4).ToString(), muser.in_exe_a1);
                    UserPosIndex++;
                    DicOrg.setProperty("UserPosIndex", UserPosIndex.ToString());
                }
            }


            int CurrentExcelRowIndex = 0;
            foreach (var DicOrgElement in DicOrgs)
            {
                Item DicOrg = DicOrgElement.Value;
                Item student_rows = DicOrg.getRelationships("student_row");

                int b = int.Parse(DicOrg.getProperty("sheet_index", ""));
                int count = int.Parse(DicOrg.getProperty("count", "0"));//中間有幾條

                CurrentExcelRowIndex++;
                foundCell = TestWs.Cell(CurrentExcelRowIndex, 1);
                foundCell.Value = DicOrg.getProperty("c1", "");

                string PreL1 = "隊職員";
                for (int c = 0; c < student_rows.getItemCount(); c++)
                {
                    CurrentExcelRowIndex++;
                    student_row = student_rows.getItemByIndex(c);
                    if (student_row.getProperty("col_2", "") != PreL1)
                    {
                        foundCell = TestWs.Cell(CurrentExcelRowIndex, 1);
                        foundCell.Value = student_row.getProperty("col_2", "") + "：";
                        PreL1 = student_row.getProperty("col_2", "");
                        CurrentExcelRowIndex++;
                    }


                    //個人-國小男生A組/500, 要取得 國小男生A組
                    string L2 = student_row.getProperty("col_3", "");
                    L2 = L2.Split('/')[0];
                    if (L2.Contains("-"))
                        L2 = L2.Split('-')[1];


                    foundCell = TestWs.Cell(CurrentExcelRowIndex, 1);
                    if (student_row.getProperty("col_3", "").Split('-')[0].Contains("團"))
                    {
                        //團體貼出隊別
                        foundCell.Value = L2 + " " + student_row.getProperty("in_team", "");//貼出隊別(第三階+隊別)
                    }
                    else
                    {
                        foundCell.Value = L2;
                    }

                    if (student_row.getProperty("col_3", "").Split('-')[0].Contains("格"))
                    {
                        //格式補上第三階
                        foundCell.Value = L2 + "-" + student_row.getProperty("col_ttr", "");//
                    }


                    for (int k = 4; k <= 10; k++)
                    {
                        foundCell = TestWs.Cell(CurrentExcelRowIndex, k - 2);
                        foundCell.Value = student_row.getProperty("col_" + k.ToString(), "");
                    }
                }

                CurrentExcelRowIndex++;
                CurrentExcelRowIndex++;

            }

            //移除兩個Sheet
            StudentsWS.Delete();

            string xlsName = Meeting.getProperty("in_title", "");

            xlsName = xlsName.Replace("+", "")
                            .Replace("-", "")
                            .Replace(".", "")
                            .Replace(" ", "");

            wb.SaveAs(cfg.export_path + xlsName + "_秩序冊.xlsx");

            itmR.setProperty("xls_name", xlsName + "_秩序冊.xlsx");

            return itmR;
        }

        private void ResetMUser(TConfig cfg, TMUser muser, Item item)
        {
            muser.id = item.getProperty("id", "");
            muser.in_name = item.getProperty("in_name", "");
            muser.in_sno = item.getProperty("in_sno", "");
            muser.in_l1 = item.getProperty("in_l1", "");
            muser.in_l2 = item.getProperty("in_l2", "");
            muser.in_l3 = item.getProperty("in_l3", "");
            muser.in_index = item.getProperty("in_index", "");
            muser.in_team = item.getProperty("in_team", "");
            muser.in_current_org = item.getProperty("in_current_org", "");
            muser.in_exe_a1 = item.getProperty("in_exe_a1", "");

            var in_show_org = item.getProperty("in_show_org", "");
            if (in_show_org != "") muser.in_name += "(" + in_show_org + ")";

            muser.is_team = muser.in_l1 == "團體組";
            muser.is_kata = muser.in_l1 == "格式組";
            muser.team_sect = "";

            if (muser.is_team)
            {
                if (muser.in_l3.Contains("人"))
                {
                    muser.team_sect = muser.in_l3 + "團體";
                }
                else
                {
                    muser.team_sect = muser.in_l3 + muser.in_l2.Replace("團-", "");
                }

                muser.team_sect = muser.team_sect.Replace("賽制", "");
            }
        }

        private class TMUser
        {
            public string id { get; set; }
            public string in_name { get; set; }
            public string in_sno { get; set; }
            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }
            public string in_index { get; set; }
            public string in_team { get; set; }
            public string in_current_org { get; set; }
            public string in_exe_a1 { get; set; }
            public bool is_team { get; set; }
            public bool is_kata { get; set; }
            public string team_sect { get; set; }
        }

        private void SetCellTextValue(ref ClosedXML.Excel.IXLCell Cell, string TextValue)
        {
            DateTime dtTmp;
            double dblTmp;

            if (DateTime.TryParse(TextValue, out dtTmp))
            {
                Cell.Value = "'" + TextValue;
            }
            else if (double.TryParse(TextValue, out dblTmp))
            {
                Cell.Value = "'" + TextValue;
            }
            else
            {
                Cell.Value = TextValue;
            }
            Cell.DataType = ClosedXML.Excel.XLDataType.Text;

        }

        private string GetSheetName(string TheString)
        {
            return TheString;
        }

        private Item GetMeetingItem(TConfig cfg)
        {
            var aml = "<AML>"
                + "<Item type='In_Meeting' action='get' id='" + cfg.meeting_id + "'>"
                + "<Relationships>"
                + "<Item type='In_Meeting_Surveys' action='get' select='related_id(id,in_questions,in_request,in_selectoption,in_question_type,in_property,in_expense,in_is_pkey)'>"
                + "<in_surveytype>1</in_surveytype>"
                + "<in_property condition='in'>'in_l1','in_l2','in_l3'</in_property>"
                + "</Item>"
                + "</Relationships>"
                + "</Item></AML>";

            return cfg.inn.applyAML(aml);
        }

        private void SetExcelVariable(TConfig cfg)
        {
            //開始整理 EXCEL Template
            var aml = "<AML>" +
                "<Item type='In_Variable' action='get'>" +
                "<in_name>meeting_excel</in_name>" +
                "<Relationships>" +
                "<Item type='In_Variable_Detail' action='get'/>" +
                "</Relationships>" +
                "</Item></AML>";

            Item Vairable = cfg.inn.applyAML(aml);

            string Template_Path = "";
            string Export_Path = "";

            for (int i = 0; i < Vairable.getRelationships("In_Variable_Detail").getItemCount(); i++)
            {
                Item In_Variable_Detail = Vairable.getRelationships("In_Variable_Detail").getItemByIndex(i);
                var in_name = In_Variable_Detail.getProperty("in_name", "");
                var in_value = In_Variable_Detail.getProperty("in_value", "");

                if (in_name == "template_1_path")
                {
                    Template_Path = in_value;
                }

                if (in_name == "export_path")
                {
                    Export_Path = in_value;
                }

                if (!Export_Path.EndsWith(@"\"))
                {
                    Export_Path = Export_Path + @"\";
                }
            }

            cfg.template_path = Template_Path;
            cfg.export_path = Export_Path;
        }

        private Item GetSurveyItem(TConfig cfg, string in_property)
        {
            var sql = @"
				SELECT 
					t2.id
				FROM 
					IN_MEETING_SURVEYS t1 WITH(NOLOCK) 
				INNER JOIN 
					IN_SURVEY t2 WITH(NOLOCK) 
					ON t2.id = t1.related_id
				WHERE
					t1.source_id = '{#meeting_id}'
					AND t2.in_property = '{#in_property}'
			";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_property}", in_property);

            return cfg.inn.applySQL(sql);
        }

        private void UpdateMeetingUserExcelOrder(TConfig cfg)
        {
            var sql = "";

            var itmSvyL1 = GetSurveyItem(cfg, "in_l1");
            var svy_id_l1 = itmSvyL1.getProperty("id", "");

            var sql_svy = "SELECT in_value, sort_order, in_label, in_filter FROM IN_SURVEY_OPTION WITH(NOLOCK) WHERE source_id = '{#id}' ORDER BY sort_order";

            //取得題庫下的in_l1跟項次
            Item itmL1Opts = cfg.inn.applySQL(sql_svy.Replace("{#id}", svy_id_l1));

            //用假欄位排序(L1)
            for (int i = 0; i < itmL1Opts.getItemCount(); i++)
            {
                Item item = itmL1Opts.getItemByIndex(i);
                var sort_order = item.getProperty("sort_order", "");
                var in_filter = item.getProperty("in_filter", "");
                var in_value = item.getProperty("in_value", "");

                sql = "UPDATE IN_MEETING_USER SET"
                    + "   in_excel_order = '" + sort_order + "'"
                    + " WHERE source_id = '" + cfg.meeting_id + "'"
                    + " AND in_l1 = N'" + in_filter + "'";

                cfg.inn.applySQL(sql);
            }

            //取得題庫下的in_l2跟項次
            var itmSvyL2 = GetSurveyItem(cfg, "in_l2");
            var svy_id_l2 = itmSvyL2.getProperty("id", "");

            Item itmL2Opts = cfg.inn.applySQL(sql_svy.Replace("{#id}", svy_id_l2));

            //用假欄位排序(L2)
            for (int i = 0; i < itmL2Opts.getItemCount(); i++)
            {
                Item item = itmL2Opts.getItemByIndex(i);
                var sort_order = item.getProperty("sort_order", "");
                var in_filter = item.getProperty("in_filter", "");
                var in_value = item.getProperty("in_value", "");

                sql = "UPDATE IN_MEETING_USER SET"
                    + "   in_excel_order_2 = '" + sort_order + "'"
                    + " WHERE source_id = '" + cfg.meeting_id + "'"
                    + " AND in_l2 = N'" + in_value + "'";

                cfg.inn.applySQL(sql);
            }

            //抓出該課程的問卷(in_l3)
            var itmSvyL3 = GetSurveyItem(cfg, "in_l3");
            var svy_id_l3 = itmSvyL3.getProperty("id", "");

            if (svy_id_l3 != "")
            {
                //取得題庫下的in_l3跟項次
                Item itmL3Opts = cfg.inn.applySQL(sql_svy.Replace("{#id}", svy_id_l3));

                //用假欄位排序(L3)
                for (int i = 0; i < itmL3Opts.getItemCount(); i++)
                {
                    Item item = itmL3Opts.getItemByIndex(i);
                    var sort_order = item.getProperty("sort_order", "");
                    var in_filter = item.getProperty("in_filter", "");
                    var in_value = item.getProperty("in_value", "");

                    sql = "UPDATE IN_MEETING_USER SET"
                        + "   in_excel_order_3 = '" + sort_order + "'"
                        + " WHERE source_id = '" + cfg.meeting_id + "'"
                        + " AND in_l3 = N'" + in_value + "'";

                    cfg.inn.applySQL(sql);
                }
            }
        }

        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string meeting_id { get; set; }
            public string scene { get; set; }
            public string template_path { get; set; }
            public string export_path { get; set; }
        }
    }
}