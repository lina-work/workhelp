﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Judo.Report
{
    public class In_Meeting_User_WinEvent : Item
    {
        public In_Meeting_User_WinEvent(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 勝場數統計
                日誌: 
                    - 2023-11-16: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "In_Meeting_User_WinEvent";

            var itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                meeting_id = itmR.getProperty("meeting_id", ""),
                benchmark_day = itmR.getProperty("benchmark_day", ""),
                scene = itmR.getProperty("scene", ""),
            };

            switch (cfg.scene)
            {
                case "page":
                    Page(cfg, itmR);
                    break;

                case "statistics": //計算勝場數
                    BypassDQ(cfg, itmR);
                    Statistics_Solo(cfg, itmR);
                    Statistics_Team(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void BypassDQ(TConfig cfg, Item itmReturn)
        {
            var sql = @"
                UPDATE t1 SET
                	t1.in_suspend = t2.in_weight_message
                FROM
                	IN_MEETING_PEVENT t1
                INNER JOIN
                	VU_FIGHT_DQ t2 
                	ON t2.event_id = t1.id
                WHERE
                	t1.in_meeting = '{#meeting_id}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            cfg.inn.applySQL(sql);

        }

        //計算勝場數-團體組
        private void Statistics_Team(TConfig cfg, Item itmReturn)
        {
            var items = GetTeamEventDetails(cfg);
            var rows = MapEvnets(cfg, items);
            AnalysisMUser(cfg, rows);

            for (var i = 0; i < rows.Count; i++)
            {
                var row = rows[i];
                if (string.IsNullOrWhiteSpace(row.muid)) continue;

                var sql_upd = "UPDATE [IN_MEETING_USER] SET"
                    + "  [in_win_count] = " + row.in_win_count
                    + ", [in_win_sum] = " + row.in_win_sum
                    + ", [in_final_rank] = '" + row.in_final_rank + "'"
                    + ", [in_is_top3] = " + row.in_is_top3
                    + " WHERE [id] = '" + row.muid + "'";

                cfg.inn.applySQL(sql_upd);
            }
        }

        private void AnalysisMUser(TConfig cfg, List<TMUser> list)
        {
            foreach (var row in list)
            {
                if (row.win_status.Contains("1111111111"))
                {
                    row.in_win_count = 10;
                }
                else if (row.win_status.Contains("111111111"))
                {
                    row.in_win_count = 9;
                }
                else if (row.win_status.Contains("11111111"))
                {
                    row.in_win_count = 8;
                }
                else if (row.win_status.Contains("1111111"))
                {
                    row.in_win_count = 7;
                }
                else if (row.win_status.Contains("111111"))
                {
                    row.in_win_count = 6;
                }
                else if (row.win_status.Contains("11111"))
                {
                    row.in_win_count = 5;
                }
                else if (row.win_status.Contains("1111"))
                {
                    row.in_win_count = 4;
                }
                else if (row.win_status.Contains("111"))
                {
                    row.in_win_count = 3;
                }
                else if (row.win_status.Contains("11"))
                {
                    row.in_win_count = 2;
                }
                else if (row.win_status.Contains("1"))
                {
                    row.in_win_count = 1;
                }
                else
                {
                    row.in_win_count = 0;
                }

                var s = row.win_status.Replace("0", "");
                row.in_win_sum = s.Length;
            }
        }

        private List<TMUser> MapEvnets(TConfig cfg, Item items)
        {
            var result = new List<TMUser>();

            var count = items.getItemCount();
            if (count <= 0) return result;

            var list = new List<TEvt>();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var program_id = item.getProperty("program_id", "");
                var event_id = item.getProperty("event_id", "");
                var sub_tree_no = item.getProperty("sub_tree_no", "");
                var sub_foot = item.getProperty("sub_foot", "");
                var key = event_id + "-" + sub_tree_no;

                var existed = list.Find(x => x.key == key);
                if (existed == null)
                {
                    existed = new TEvt
                    {
                        key = key,
                        program_id = program_id,
                        event_id = event_id,
                        hasFought = false,
                        hasError = false,
                    };
                    list.Add(existed);
                }
                if (sub_foot == "1")
                {
                    existed.itmF1 = item;
                }
                else if (sub_foot == "2")
                {
                    existed.itmF2 = item;
                }
            }

            foreach (var node in list)
            {
                if (node.itmF1 == null || node.itmF2 == null)
                {
                    node.hasError = true;
                    continue;
                }

                var f1_name = node.itmF1.getProperty("in_player_name", "");
                var f1_status = node.itmF1.getProperty("in_status", "");
                var f2_name = node.itmF2.getProperty("in_player_name", "");
                var f2_status = node.itmF2.getProperty("in_status", "");

                if (f1_name == "" || f1_name == "沒有選手") continue;
                if (f2_name == "" || f2_name == "沒有選手") continue;

                if (f1_status == "1")
                {
                    node.hasFought = true;
                }
                else if (f2_status == "1")
                {
                    node.hasFought = true;
                }

                if (node.hasFought)
                {
                    AppendMUser(cfg, result, node.itmF1);
                    AppendMUser(cfg, result, node.itmF2);
                }
            }

            return result;
        }

        private void AppendMUser(TConfig cfg, List<TMUser> list, Item itmFoot)
        {
            var program_id = itmFoot.getProperty("program_id", "");
            var in_sno = itmFoot.getProperty("in_player_sno", "");
            var in_status = itmFoot.getProperty("in_status", "");
            var key = program_id + "-" + in_sno;

            if (in_status == "") in_status = "0";

            var existed = list.Find(x => x.key == key);
            if (existed == null)
            {
                existed = new TMUser
                {
                    key = key,
                    program_id = program_id,
                    in_l1 = itmFoot.getProperty("in_l1", ""),
                    in_l2 = itmFoot.getProperty("in_l2", ""),
                    in_l3 = itmFoot.getProperty("in_l3", ""),
                    in_short_org = itmFoot.getProperty("in_player_org", ""),
                    in_team = itmFoot.getProperty("in_player_team", ""),
                    in_name = itmFoot.getProperty("in_player_name", ""),
                    in_sno = itmFoot.getProperty("in_player_sno", ""),
                    win_status = "",
                    in_win_count = 0,
                    in_win_sum = 0,
                    hasError = false,
                    in_is_top3 = "0",
                };

                existed.muid = GetMUID(cfg, existed);

                existed.itmParentTeam = GetParentTeam(cfg, existed.program_id, existed.in_short_org, existed.in_team);
                if (existed.itmParentTeam.isError() || existed.itmParentTeam.getResult() == "")
                {
                    existed.hasError = true;
                }
                else
                {
                    existed.in_final_rank = existed.itmParentTeam.getProperty("in_final_rank", "0");
                    switch (existed.in_final_rank)
                    {
                        case "1":
                        case "2":
                        case "3":
                            existed.in_is_top3 = "1";
                            break;
                    }
                }
                list.Add(existed);
            }

            existed.win_status += in_status;
        }

        private string GetMUID(TConfig cfg, TMUser row)
        {
            var sql = "SELECT id FROM IN_MEETING_USER WITH(NOLOCK)"
                + " WHERE source_id = '" + cfg.meeting_id + "'"
                + " AND in_l1 = N'" + row.in_l1 + "'"
                + " AND in_l2 = N'" + row.in_l2 + "'"
                + " AND in_l3 = N'" + row.in_l3 + "'"
                + " AND in_sno = '" + row.in_sno + "'";

            var itmResult = cfg.inn.applySQL(sql);
            if (itmResult.isError() || itmResult.getResult() == "") return "";
            return itmResult.getProperty("id", "");
        }

        private class TMUser
        {
            public string muid { get; set; }
            public string key { get; set; }
            public string program_id { get; set; }
            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }
            public string in_short_org { get; set; }
            public string in_team { get; set; }
            public string in_name { get; set; }
            public string in_sno { get; set; }
            public string win_status { get; set; }
            public int in_win_count { get; set; }
            public int in_win_sum { get; set; }
            public bool hasError { get; set; }
            public Item itmParentTeam { get; set; }
            public string in_final_rank { get; set; }
            public string in_is_top3 { get; set; }
        }

        private class TEvt
        {
            public string key { get; set; }
            public string program_id { get; set; }
            public string event_id { get; set; }
            public bool hasFought { get; set; }
            public bool hasError { get; set; }
            public Item itmF1 { get; set; }
            public Item itmF2 { get; set; }
        }


        private Item GetParentTeam(TConfig cfg, string program_id, string in_short_org, string in_team)
        {
            var sql = "SELECT id, in_final_rank FROM IN_MEETING_PTEAM WITH(NOLOCK)"
                + " WHERE source_id = '" + program_id + "'"
                + " AND ISNULL(in_type, '') IN ('', 'p')"
                + " AND in_short_org = N'" + in_short_org + "'"
                + " AND ISNULL(in_team, '') =  '" + in_team + "'";

            return cfg.inn.applySQL(sql);
        }

        //取得勝場與勝利者
        private Item GetTeamEventDetails(TConfig cfg)
        {
            var sql = @"
                SELECT 
	                t1.id               AS 'program_id'
	                , t1.in_l1
	                , t1.in_l2
	                , t1.in_l3
	                , t1.in_team_count
					, t11.id            AS 'event_id'
					, t11.in_tree_no
					, t11.in_fight_id
					, t2.in_tree_no		AS 'sub_tree_no'
					, t3.in_sign_foot	AS 'sub_foot'
	                , t3.in_player_org
	                , t3.in_player_team
	                , t3.in_player_name
	                , t3.in_player_sno
	                , t3.in_status
                FROM
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PEVENT t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t3 WITH(NOLOCK)
	                ON t3.source_id = t2.id
				INNER JOIN IN_MEETING_PEVENT t11 WITH(NOLOCK)
					ON t11.id = t2.in_parent
                WHERE
	                t1.in_meeting = '{#meeting_id}'
	                AND t1.in_l1 = '團體組'
	                --AND t1.in_l2 LIKE '%乙組%'
	                AND ISNULL(t1.in_team_count, 0) > 1
					AND t2.in_tree_name = 'sub'
					AND t2.in_win_status <> 'cancel'
					AND t11.in_win_status NOT IN ('cancel', 'bypass')
                ORDER BY
	                t1.in_sort_order
	                , t11.in_tree_sort
					, t11.in_fight_id DESC
					, t2.in_tree_no
					, t3.in_sign_foot
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        //計算勝場數-個人組
        private void Statistics_Solo(TConfig cfg, Item itmReturn)
        {
            var items = GetSoloWinners(cfg);
            var count = items.getItemCount();
            if (count <= 0) throw new Exception("查無勝場資料");

            //將勝場數歸零
            var sql_upd1 = "UPDATE [IN_MEETING_USER] SET"
                + "  [in_win_count] = 0"
                + ", [in_win_sum] = 0"
                + ", [in_final_rank] = NULL"
                + " WHERE source_id = '" + cfg.meeting_id + "'";

            cfg.inn.applySQL(sql_upd1);

            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var muid = item.getProperty("muid", "");
                var in_tree_name = item.getProperty("in_tree_name", "");
                var in_win_status = item.getProperty("in_win_status", "");
                var in_suspend = item.getProperty("in_suspend", "");
                var in_final_rank = item.getProperty("in_final_rank", "0");

                if (in_win_status == "bypass") continue;
                if (in_win_status == "cancel") continue;
                if (in_suspend != "") continue;

                if (in_tree_name == "main")
                {
                    var sql_upd2 = "UPDATE [IN_MEETING_USER] SET"
                        + "  [in_win_count] = [in_win_count] + 1"
                        + ", [in_win_sum] = [in_win_sum] + 1"
                        + ", [in_final_rank] = '" + in_final_rank + "'"
                        + " WHERE [id] = '" + muid + "'";

                    cfg.inn.applySQL(sql_upd2);
                }
                else
                {
                    var sql_upd3 = "UPDATE [IN_MEETING_USER] SET"
                        + "  [in_win_sum] = [in_win_sum] + 1"
                        + ", [in_final_rank] = '" + in_final_rank + "'"
                        + " WHERE [id] = '" + muid + "'";

                    cfg.inn.applySQL(sql_upd3);
                }
            }
        }

        //取得勝場與勝利者
        private Item GetSoloWinners(TConfig cfg)
        {
            var sql = @"
                SELECT 
	                t1.id AS 'program_id'
	                , t1.in_l1
	                , t1.in_l2
	                , t1.in_l3
	                , t1.in_team_count
	                , t2.in_tree_name
					, t2.in_tree_sno
	                , t2.in_fight_id
	                , t2.in_bypass_foot
	                , t2.in_win_status
	                , t2.in_win_sign_no
	                , t2.in_suspend
	                , t3.in_current_org
	                , t3.in_name
	                , t3.in_index
	                , t3.in_creator_sno
	                , t3.in_team_key
                    , t3.in_final_rank
	                , t4.id AS 'muid'
                FROM
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PEVENT t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                INNER JOIN
	                IN_MEETING_PTEAM t3 WITH(NOLOCK)
	                ON t3.source_id = t1.id
	                AND t3.in_sign_no = t2.in_win_sign_no
	                AND ISNULL(t3.in_type, '') IN ('', 'p')
                INNER JOIN
	                IN_MEETING_USER t4 WITH(NOLOCK)
	                ON t4.source_id = t1.in_meeting
	                AND t4.in_l1 = t1.in_l1
	                AND t4.in_l2 = t1.in_l2
	                AND t4.in_l3 = t1.in_l3
	                AND t4.in_index = t3.in_index
	                AND t4.in_creator_sno = t3.in_creator_sno
                WHERE
	                t1.in_meeting = '{#meeting_id}'
	                AND t1.in_l1 NOT IN (N'隊職員', N'格式組', N'常年會費', N'團體組')
	                AND ISNULL(t1.in_team_count, 0) > 1
	                AND ISNULL(t2.in_type, '') IN ('', 'p')
	                AND ISNULL(t2.in_tree_no, 0) > 0
                ORDER BY
	                t1.in_sort_order
	                , t2.in_tree_sort
					, t2.in_fight_id DESC
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        //頁面
        private void Page(TConfig cfg, Item itmReturn)
        {
            if (cfg.benchmark_day == "")
            {
                cfg.benchmark_day = DateTime.Now.AddYears(-14).ToString("yyyy-MM-dd");
                itmReturn.setProperty("benchmark_day", cfg.benchmark_day);
            }

            AppendMeetingInfo(cfg, itmReturn);
            AppendMeetingMenu(cfg, itmReturn);

            var contents = "";
            var sortmode = itmReturn.getProperty("sortmode", "");

            if (sortmode == "weight")
            {
                //同一個人會出現在不同量級
                contents = GetMeetingUsersTables1(cfg, itmReturn);
            }
            else
            {
                contents = GetMeetingUsersTables2(cfg, itmReturn);
            }

            itmReturn.setProperty("inn_table", contents);
        }

        //用量級觀看
        private string GetMeetingUsersTables1(TConfig cfg, Item itmReturn)
        {
            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            head.Append("<th class='mailbox-subject text-center' data-width='4%' >No.</th>");
            head.Append("<th class='mailbox-subject text-center' data-width='31%' >級組</th>");
            head.Append("<th class='mailbox-subject text-center' data-width='20%' >單位</th>");
            head.Append("<th class='mailbox-subject text-center' data-width='16%' >姓名</th>");
            head.Append("<th class='mailbox-subject text-center' data-width='7%' >生日</th>");
            head.Append("<th class='mailbox-subject text-center' data-width='7%' >性別</th>");
            head.Append("<th class='mailbox-subject text-center' data-width='7%' >勝場</th>");
            head.Append("<th class='mailbox-subject text-center' data-width='8%' >名次</th>");

            var items = GetMeetingUserItems(cfg, itmReturn);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var program_id = item.getProperty("program_id", "");
                var program_name = item.getProperty("in_name3", "");
                var in_team_count = item.getProperty("in_team_count", "0");
                var in_final_rank = item.getProperty("in_final_rank", "");
                if (in_final_rank == "0") in_final_rank = "";

                var text = program_name + " (" + in_team_count + ")";
                var link = "<a href='javascript:;' data-pid='" + program_id + "' onclick='GoFightTree(this)'>" + text + "</a>";

                body.Append("<tr>");

                body.Append("<td class='mailbox-subject text-center'>" + (i + 1) + "</td>");
                body.Append("<td class='mailbox-subject text-left'>" + link + "</td>");
                body.Append("<td class='mailbox-subject text-left'>" + item.getProperty("in_current_org", "") + "</td>");
                body.Append("<td class='mailbox-subject text-left'>" + item.getProperty("in_name", "") + "</td>");
                body.Append("<td class='mailbox-subject text-left'>" + item.getProperty("in_birth", "") + "</td>");
                body.Append("<td class='mailbox-subject text-center'>" + item.getProperty("in_gender", "") + "</td>");
                body.Append("<td class='mailbox-subject text-center'>" + item.getProperty("in_win_count", "") + "</td>");
                body.Append("<td class='mailbox-subject text-center'>" + in_final_rank + "</td>");

                body.Append("</tr>");
            }

            return GenerateTableContents(head, body);
        }

        //用單位觀看
        private string GetMeetingUsersTables2(TConfig cfg, Item itmReturn)
        {
            StringBuilder head = new StringBuilder();
            StringBuilder body = new StringBuilder();

            head.Append("<th class='mailbox-subject text-center' data-width='4%' >No.</th>");
            head.Append("<th class='mailbox-subject text-center' data-width='20%'>單位</th>");
            head.Append("<th class='mailbox-subject text-center' data-width='16%'>姓名</th>");
            head.Append("<th class='mailbox-subject text-center' data-width='7%' >生日</th>");
            head.Append("<th class='mailbox-subject text-center' data-width='7%'>性別</th>");
            head.Append("<th class='mailbox-subject text-center' data-width='7%' >勝場</th>");
            head.Append("<th class='mailbox-subject text-center' data-width='39%'>級組</th>");

            var rows = MapMeetingUsers(cfg, itmReturn);
            for (var i = 0; i < rows.Count; i++)
            {
                var row = rows[i];
                var links = GetLinks(row.nodes);

                body.Append("<tr>");
                body.Append("<td class='mailbox-subject text-center'>" + (i + 1) + "</td>");
                body.Append("<td class='mailbox-subject text-left'>" + row.in_current_org + "</td>");
                body.Append("<td class='mailbox-subject text-left'>" + row.in_name + "</td>");
                body.Append("<td class='mailbox-subject text-left'>" + row.in_birth + "</td>");
                body.Append("<td class='mailbox-subject text-center'>" + row.in_gender + "</td>");
                body.Append("<td class='mailbox-subject text-center'>" + row.max_win_count + "</td>");
                body.Append("<td class='mailbox-subject text-left'>" + links + "</td>");
                body.Append("</tr>");
            }

            return GenerateTableContents(head, body);
        }

        private string GetLinks(List<Item> items)
        {
            if (items == null || items.Count == 0) return "";

            var links = new List<string>();
            foreach (var item in items)
            {
                var program_id = item.getProperty("program_id", "");
                var program_name = item.getProperty("in_name3", "");
                var in_team_count = item.getProperty("in_team_count", "0");
                var in_win_count = item.getProperty("in_win_count", "0");

                var in_final_rank = item.getProperty("in_final_rank", "");
                if (in_final_rank == "" || in_final_rank == "0")
                {
                    in_final_rank = "無名次";
                }
                else
                {
                    in_final_rank = "第 " + in_final_rank + " 名";
                }

                var text = program_name + " (" + in_team_count + ")";
                var link = "<a href='javascript:;' data-pid='" + program_id + "' onclick='GoFightTree(this)'>" + text + "</a>";
                link += ": 勝 " + in_win_count + " 場，" + in_final_rank;
                links.Add(link);
            }

            return string.Join("<br />", links);
        }

        private string GenerateTableContents(StringBuilder head, StringBuilder body)
        {
            string table_name = "data_table";

            StringBuilder builder = new StringBuilder();
            builder.Append("<div class='box-body tablenobg' style='position: relative'>");
            builder.AppendLine(GetTableAttribute(table_name));

            builder.Append("<thead>");
            builder.Append("<tr>");
            builder.Append(head);
            builder.Append("</tr>");
            builder.Append("</thead>");

            builder.Append("<tbody>");
            builder.Append(body);
            builder.Append("</tbody>");

            builder.AppendLine("</table>");
            builder.Append("</div>");

            builder.AppendLine("<script>");
            builder.AppendLine("$('#" + table_name + "').bootstrapTable();");
            builder.AppendLine("if ($(window).width() <= 768) { $('#" + table_name + "').bootstrapTable('toggleView'); }");
            builder.AppendLine("</script>");

            return builder.ToString();
        }

        private string GetTableAttribute(string table_name)
        {
            return "<table id='" + table_name + "'"
                + " class='table table-hover table-striped'"
                + " data-toggle='table'"
                + " data-show-toggle='false'"
                + " data-show-columns='false'"
                + " data-search-align='right'"
                + " data-sort-stable='true'"
                + " data-search='true'"
                + ">";
        }

        private List<TRow> MapMeetingUsers(TConfig cfg, Item itmReturn)
        {
            var result = new List<TRow>();

            var items = GetMeetingUserItems(cfg, itmReturn);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var in_name = item.getProperty("in_name", "");
                var in_sno = item.getProperty("in_sno", "");
                var in_win_count = GetInt(item.getProperty("in_win_count", "0"));

                var key = in_sno + " " + in_name;
                var existed = result.Find(x => x.key == key);

                if (existed == null)
                {
                    existed = new TRow
                    {
                        key = key,
                        in_current_org = item.getProperty("in_current_org", ""),
                        in_name = item.getProperty("in_name", ""),
                        in_sno = item.getProperty("in_sno", ""),
                        in_gender = item.getProperty("in_gender", ""),
                        in_birth = item.getProperty("in_birth", ""),
                        max_win_count = in_win_count,
                        nodes = new List<Item>(),
                    };
                    result.Add(existed);
                }

                existed.nodes.Add(item);

                if (in_win_count > existed.max_win_count)
                {
                    existed.max_win_count = in_win_count;
                }
            }

            return result;
        }

        private Item GetMeetingUserItems(TConfig cfg, Item itmReturn)
        {
            var op = itmReturn.getProperty("operate", "");
            var wc = itmReturn.getProperty("wincount", "");
            var sortmode = itmReturn.getProperty("sortmode", "");
            var rankmode = itmReturn.getProperty("rankmode", "");
            var rankCond = "";

            switch (op)
            {
                case "ge": op = ">="; break;
                case "lt": op = "<="; break;
                case "eq": op = "="; break;
                default: op = ">="; break;
            }

            if (wc == "") wc = "4";

            switch (rankmode)
            {
                case "rank":
                    rankCond = "AND ISNULL(t4.in_final_rank, 0) >= 1";
                    break;

                case "rankw1":
                    rankCond = "AND ISNULL(t4.in_final_rank, 0) >= 1 AND ISNULL(t4.in_win_count, 0) >= 1";
                    break;

                case "norank":
                    rankCond = "AND ISNULL(t4.in_final_rank, 0) <= 0";
                    break;
            }

            var bday = GetDtmVal(cfg.benchmark_day).AddHours(-8).ToString("yyyy-MM-dd HH:mm:ss");

            var orderBy = "t4.in_current_org, t4.in_gender, t4.in_name, t1.in_sort_order";
            if (sortmode == "weight") orderBy = "t1.in_sort_order, t4.in_win_count DESC";

            var sql = @"
                SELECT 
	                t1.id AS 'program_id'
	                , t1.in_l1
	                , t1.in_l2
	                , t1.in_l3
	                , t1.in_name2
	                , t1.in_name3
	                , t1.in_team_count
	                , t4.in_current_org
	                , t4.in_name
	                , t4.in_sno
	                , t4.in_gender
	                , CONVERT(VARCHAR, DATEADD(HOUR, 8, t4.in_birth), 111) AS 'in_birth'
	                , t4.in_win_count
	                , t4.in_final_rank
                FROM
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_USER t4 WITH(NOLOCK)
	                ON t4.source_id = t1.in_meeting
	                AND t4.in_l1 = t1.in_l1
	                AND t4.in_l2 = t1.in_l2
	                AND t4.in_l3 = t1.in_l3
                WHERE
	                t1.in_meeting = '{#meeting_id}'
	                AND t1.in_l1 NOT IN (N'隊職員', N'格式組', N'常年會費')
	                AND ISNULL(t1.in_team_count, 0) > 1
	                AND ISNULL(t4.in_win_count, 0) {#op} {#wc}
	                AND t4.in_birth <= '{#bday}'
	                {#rankCond}
                ORDER BY
	                {#orderBy}
                ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#op}", op)
                .Replace("{#wc}", wc)
                .Replace("{#bday}", bday)
                .Replace("{#rankCond}", rankCond)
                .Replace("{#orderBy}", orderBy);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        private class TRow
        {
            public string key { get; set; }
            public string in_current_org { get; set; }
            public string in_name { get; set; }
            public string in_sno { get; set; }
            public string in_gender { get; set; }
            public string in_birth { get; set; }
            public int max_win_count { get; set; }

            public List<Item> nodes { get; set; }
        }

        private void AppendMeetingInfo(TConfig cfg, Item itmReturn)
        {
            if (cfg.meeting_id == "") return;
            var sql = "SELECT id, in_title FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'";

            var itmMeeting = cfg.inn.applySQL(sql);
            if (itmMeeting.isError() || itmMeeting.getResult() == "") return;

            CopyItemValue(itmReturn, itmMeeting, "in_title");
        }

        private void AppendMeetingMenu(TConfig cfg, Item itmReturn)
        {
            var sql = @"
                SELECT 
	                id
	                , in_title
	                , item_number
	                , in_real_taking
	                , in_meeting_type
	                , in_date_s
	                , in_date_e
                FROM 
	                IN_MEETING t1 WITH(NOLOCK) 
                WHERE 
	                ISNULL(in_is_template, 0) = 0
	                AND in_meeting_type = 'game'
	                AND EXISTS( SELECT * FROM IN_MEETING_PROGRAM t2 WITH(NOLOCK) WHERE t2.in_meeting = t1.id )
                ORDER BY 
	                in_date_s DESC
                ";

            var itmEmpty = cfg.inn.newItem();
            itmEmpty.setType("inn_meeting");
            itmEmpty.setProperty("id", "");
            itmEmpty.setProperty("in_title", "請選擇");
            itmReturn.addRelationship(itmEmpty);

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                item.setType("inn_meeting");
                itmReturn.addRelationship(item);
            }
        }

        private void CopyItemValue(Item itmA, Item itmB, string propA, string propB = "")
        {
            if (propB == "")
            {
                itmA.setProperty(propA, itmB.getProperty(propA, ""));
            }
            else
            {
                itmA.setProperty(propA, itmB.getProperty(propB, ""));
            }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }

            public string meeting_id { get; set; }
            public string scene { get; set; }
            public string benchmark_day { get; set; }
        }

        private DateTime GetDtmVal(string value)
        {
            DateTime result = DateTime.MinValue;
            DateTime.TryParse(value, out result);
            return result;
        }

        private int GetInt(string value, int defV = 0)
        {
            if (value == "") return 0;
            int result = defV;
            if (int.TryParse(value, out result)) return result;
            return defV;
        }
    }
}