﻿using Aras.IOM;
using System;
using System.Collections.Generic;
using System.Linq;
using WorkHelp.ArasDesk.Methods.Judo.種子籤;

namespace WorkHelp.ArasDesk.Methods.Judo.籤號
{
    internal class Fix_Player_Need_Draw : Item
    {
        public Fix_Player_Need_Draw(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
    目的: 已抽籤後加人重抽
    日誌: 
        - 2025-01-09: 創建 (lina)
*/

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "Fix_Player_Need_Draw";

            var itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                meeting_id = itmR.getProperty("meeting_id", ""),
                program_id = itmR.getProperty("program_id", ""),
                muid = itmR.getProperty("muid", ""),
                scene = itmR.getProperty("scene", ""),
            };

            switch (cfg.scene)
            {
                case "sect-menu":
                    AppendSectMenu(cfg, itmR);
                    break;
                case "player-menu":
                    AppendPlayerMenu(cfg, itmR);
                    break;
                case "run":
                    ResetFightSchedule(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void ResetFightSchedule(TConfig cfg, Item itmReturn)
        {
            cfg.createname = "User";
            cfg.createid = "AD30A6D8D3B642F5A2AFED1A4B02BEFA";
            cfg.new_main_no = itmReturn.getProperty("new_main_no", "");
            cfg.target_main_no = itmReturn.getProperty("target_main_no", "");

            cfg.itmProgram = GetProgramItem(cfg);
            cfg.itmMUser = GetMUserItem(cfg);
            cfg.itmPTeam = GetPTeamItem(cfg, cfg.itmProgram, cfg.itmMUser);
            if (cfg.itmPTeam.getResult() == "") throw new Exception("查無選手資料");

            cfg.in_l1 = cfg.itmProgram.getProperty("in_l1", "");
            cfg.in_l2 = cfg.itmProgram.getProperty("in_l2", "");
            cfg.in_l3 = cfg.itmProgram.getProperty("in_l3", "");
            cfg.sect_key = cfg.in_l1 + "-" + cfg.in_l2 + "-" + cfg.in_l3;

            cfg.fight_day = cfg.itmProgram.getProperty("in_fight_day", "");
            cfg.site_code = cfg.itmProgram.getProperty("site_code", "");
            cfg.site_id = cfg.itmProgram.getProperty("site_id", "");
            cfg.new_team_id = cfg.itmPTeam.getProperty("id", "");
            cfg.old_team_count = GetInt(cfg.itmProgram.getProperty("in_team_count", "0"));
            cfg.new_team_count = cfg.old_team_count + 1;
            cfg.new_judo_no = GetNewJudoNo(cfg.new_team_count);
            cfg.new_sign_no = GetNewSignNo(cfg.new_team_count, cfg.new_judo_no);
            //cfg.new_system_count = GetSystemCount(cfg.new_team_count);
            if (cfg.old_team_count <= 0) throw new Exception("組別人數異常");
            if (cfg.new_sign_no <= 0) throw new Exception("組別人數異常");

            cfg.utcNow = System.DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss");
            cfg.new_weight_value1 = itmReturn.getProperty("in_weight", "");
            cfg.new_weight_value2 = GetDcmVal(cfg.new_weight_value1);
            if (cfg.new_weight_value2 <= 0 || cfg.new_weight_value2 >= 300) throw new Exception("體重異常");

            cfg.rpc_tree_id = GetRpcTreeId(cfg.new_team_count);
            if (cfg.rpc_tree_id != "")
            {
                cfg.new_rpc_no = GetInt(itmReturn.getProperty("new_rpc_no", "0"));
                cfg.target_rpc_no = GetInt(itmReturn.getProperty("target_rpc_no", "0"));
                if (cfg.new_rpc_no <= 0) throw new Exception("請輸入敗部場次編號");
                if (cfg.target_rpc_no <= 0) throw new Exception("請輸入敗部目標場次");
            }

            //形變
            if (cfg.new_team_count <= 8) throw new Exception("不調整");

            var needRebuild = false;
            if (cfg.new_team_count == 9) needRebuild = true;
            if (cfg.new_team_count == 17) needRebuild = true;
            if (cfg.new_team_count == 33) needRebuild = true;
            if (cfg.new_team_count == 65) needRebuild = true;

            if (needRebuild)
            {
                RunStepB(cfg, itmReturn);
            }
            else
            {
                RunStepA(cfg, itmReturn);
            }
        }

        private void RunStepB(TConfig cfg, Item itmReturn)
        {
            //紀錄原場次 - NULL 場次
            AppendTreeNoIsNullCmdList(cfg);
            //紀錄原場次 - 非 NULL 場次
            AppendTreeNoNotNullCmdList(cfg);
            //紀錄籤號
            AppendPlayerSignNoCmdList(cfg);

            //清除該組籤號
            var sql1 = "UPDATE IN_MEETING_PTEAM SET "
                + "	 in_sign_no = NULL"
                + ", in_judo_no = NULL"
                + " WHERE in_meeting = '" + cfg.meeting_id + "'"
                + " AND in_team_key LIKE N'%" + cfg.sect_key + "%'"
                ;
            cfg.inn.applySQL(sql1);

            //重建場次
            RebuldEvents(cfg);

            //更新該組場次資料
            var sql2 = "UPDATE IN_MEETING_PEVENT SET "
                + "	 in_site_allocate = '1'"
                + ", in_win_creator = NULL"
                + ", in_win_creator_sno = NULL"
                + ", in_date_key = '" + cfg.fight_day + "'"
                + ", in_site = '" + cfg.site_id + "'"
                + ", in_site_code = " + cfg.site_code
                + ", in_show_site = " + cfg.site_code
                + " WHERE source_id = '" + cfg.program_id + "'"
                ;
            cfg.inn.applySQL(sql2);

            //恢復原場次 - NULL 場次
            for (var i = 0; i < cfg.cmdsA.Count; i++)
            {
                var cmd = cfg.cmdsA[i];
                cfg.inn.applySQL(cmd);
            }
            //恢復原場次 - 非 NULL 場次
            for (var i = 0; i < cfg.cmdsB.Count; i++)
            {
                var cmd = cfg.cmdsB[i];
                cfg.inn.applySQL(cmd);
            }
            //恢復原籤號
            for (var i = 0; i < cfg.cmdsC.Count; i++)
            {
                var cmd = cfg.cmdsC[i];
                cfg.inn.applySQL(cmd);
            }

            FixExpandEvents(cfg);
            FixTeamSignNo(cfg);

            if (cfg.rpc_tree_id != "")
            {
                RunRebuildRpc(cfg, cfg.rpc_tree_id, itmReturn);
            }

            //清除暫存資訊(三階選單)
            //ClearCache(cfg, itmReturn);

            //直接給籤號
            AssignPlayerNo(cfg);
        }

        //修復籤號
        private void FixTeamSignNo(TConfig cfg)
        {
            var svc = new InnSport.Core.Services.Impl.TkdSportService();
            var ns = svc.TkdNoListByPosition(cfg.new_team_count);

            var sql = @"
                SELECT 
	                id
	                , in_judo_no
                FROM 
	                IN_MEETING_PTEAM WITH(NOLOCK)
                WHERE
	                source_id = '{#program_id}'
                ORDER BY
	                in_judo_no
            ";

            sql = sql.Replace("{#program_id}", cfg.program_id);
            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var id = item.getProperty("id", "");
                var in_judo_no = item.getProperty("in_judo_no", "0");
                if (in_judo_no == "" || in_judo_no == "0") continue;

                var judo_no = GetInt(in_judo_no);
                var x = ns.Find(y => y.JodoNo == judo_no);
                if (x == null) continue;

                var sql_upd = "UPDATE IN_MEETING_PTEAM SET"
                    + " in_sign_no = '" + x.TkdNo + "'"
                    + " WHERE id = '" + id + "'"
                    ;

                cfg.inn.applySQL(sql_upd);
            }
        }

        private void FixExpandEvents(TConfig cfg)
        {
            var itmFoot = GetDetailItem(cfg);
            if (itmFoot.getResult() == "") throw new Exception("查無場次#1");

            var sql = "SELECT TOP 1 id, in_tree_sno, in_show_serial FROM IN_MEETING_PEVENT WITH(NOLOCK)"
                + " WHERE in_meeting = '" + cfg.meeting_id + "'"
                + " AND in_date_key = '" + cfg.fight_day + "'"
                + " AND in_site_code = " + cfg.site_code
                + " AND in_tree_no = " + cfg.target_main_no
                ;

            var itmTargetEvent = cfg.inn.applySQL(sql);
            if (itmTargetEvent.getResult() == "")
            {
                itmTargetEvent = cfg.inn.newItem();
            }

            cfg.new_event_id = itmFoot.getProperty("event_id", "");
            cfg.new_tree_id = itmFoot.getProperty("event_tree_id", "");
            cfg.new_detail_id = itmFoot.getProperty("detail_id", "");

            cfg.new_tree_sno = 1;
            cfg.new_show_serial = 1;
            if (itmTargetEvent.getResult() != "")
            {
                var target_tree_sno = GetInt(itmTargetEvent.getProperty("in_tree_sno", "0"));
                var target_show_serial = GetInt(itmTargetEvent.getProperty("in_show_serial", "0")) * 2;
                cfg.new_tree_sno = target_tree_sno + 1;
                cfg.new_show_serial = target_show_serial + 1;
            }


            var sql2 = "UPDATE IN_MEETING_USER SET"
                + "  in_weight = '" + cfg.new_weight_value1 + "'"
                + ", in_weight_createname = '" + cfg.createname + "'"
                + ", in_weight_createid = '" + cfg.createid + "'"
                + ", in_weight_time = '" + cfg.utcNow + "'"
                + ", in_weight_result = '1'"
                + ", in_weight_status = 'on'"
                + " WHERE id = '" + cfg.muid + "'"
                ;

            cfg.inn.applySQL(sql2);

            var sql3 = "UPDATE IN_MEETING_PTEAM SET"
                + "  source_id = '" + cfg.program_id + "'"
                + ", in_check_result = '1'"
                + ", in_check_status = '通過'"
                + ", in_weight_result = '1'"
                + ", in_weight_value = '" + cfg.new_weight_value1 + "'"
                + ", in_weight_message = ''"
                + ", in_not_draw = '0'"
                + " WHERE id = '" + cfg.new_team_id + "'"
                ;

            cfg.inn.applySQL(sql3);

            //該場地所有流水號放大2倍
            var sql5 = "UPDATE IN_MEETING_PEVENT"
                + " SET in_show_serial = in_show_serial * 2"
                + " WHERE in_meeting = '" + cfg.meeting_id + "'"
                + " AND in_date_key = '" + cfg.fight_day + "'"
                + " AND in_site_code = " + cfg.site_code
                + " AND ISNULL(in_show_serial, 0) > 0"
                ;
            cfg.inn.applySQL(sql5);

            //更新該場次資料
            var sql6 = "UPDATE IN_MEETING_PEVENT SET "
                + "	 in_site_allocate = '1'"
                + ", in_win_creator = NULL"
                + ", in_win_creator_sno = NULL"
                + ", in_date_key = '" + cfg.fight_day + "'"
                + ", in_site = '" + cfg.site_id + "'"
                + ", in_site_code = " + cfg.site_code
                + ", in_show_site = " + cfg.site_code
                + ", in_tree_no = " + cfg.new_main_no
                + ", in_show_serial = " + cfg.new_show_serial
                + " WHERE id = '" + cfg.new_event_id + "'"
                ;

            cfg.inn.applySQL(sql6);
        }

        private void RebuldEvents(TConfig cfg)
        {
            var itmData = cfg.inn.newItem("In_Meeting_User");
            itmData.setProperty("muid", cfg.muid);
            itmData.setProperty("in_weight_status", "on");
            itmData.apply("In_Meeting_Weight_Status");
        }

        private void AppendPlayerSignNoCmdList(TConfig cfg)
        {
            cfg.cmdsC = new List<string>();

            var sql = "SELECT id, in_sign_no, in_judo_no FROM IN_MEETING_PTEAM WITH(NOLOCK)"
                + " WHERE source_id = '" + cfg.meeting_id + "'"
                + " AND ISNULL(in_sign_no, '') NOT IN ('', '0')"
                ;
            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var id = item.getProperty("id", "");
                var in_sign_no = item.getProperty("in_sign_no", "");
                var in_judo_no = item.getProperty("in_judo_no", "");

                var cmd = "UPDATE IN_MEETING_PTEAM SET"
                    + " in_sign_no = '" + in_sign_no + "'"
                    + ", in_judo_no = '" + in_judo_no + "'"
                    + " WHERE id = '" + id + "'"
                    ;
                //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, cmd);

                cfg.cmdsC.Add(cmd);
            }
        }

        private void AppendTreeNoNotNullCmdList(TConfig cfg)
        {
            cfg.cmdsB = new List<string>();

            var sql = "SELECT source_id, in_fight_id, in_tree_no, in_show_serial FROM IN_MEETING_PEVENT WITH(NOLOCK)"
                + " WHERE in_meeting = '" + cfg.meeting_id + "'"
                + " AND in_date_key = '" + cfg.fight_day + "'"
                + " AND in_site = '" + cfg.site_id + "'"
                + " AND ISNULL(in_tree_no, 0) > 0"
                ;
            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var source_id = item.getProperty("source_id", "");
                var in_fight_id = item.getProperty("in_fight_id", "");
                var in_tree_no = item.getProperty("in_tree_no", "");
                var in_show_serial = item.getProperty("in_show_serial", "");

                var cmd = "UPDATE IN_MEETING_PEVENT SET"
                    + " in_tree_no = '" + in_tree_no + "'"
                    + ", in_show_serial = '" + in_show_serial + "'"
                    + " WHERE source_id = '" + source_id + "'"
                    + " AND in_fight_id = '" + in_fight_id + "'"
                    ;
                cfg.cmdsB.Add(cmd);
            }
        }

        private void AppendTreeNoIsNullCmdList(TConfig cfg)
        {
            cfg.cmdsA = new List<string>();

            var sql = "SELECT source_id, in_fight_id FROM IN_MEETING_PEVENT WITH(NOLOCK)"
                + " WHERE in_meeting = '" + cfg.meeting_id + "'"
                + " AND in_date_key = '" + cfg.fight_day + "'"
                + " AND in_site = '" + cfg.site_id + "'"
                + " AND ISNULL(in_tree_no, 0) = 0"
                ;
            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var source_id = item.getProperty("source_id", "");
                var in_fight_id = item.getProperty("in_fight_id", "");

                var cmd = "UPDATE IN_MEETING_PEVENT SET in_tree_no = NULL"
                    + " WHERE source_id = '" + source_id + "'"
                    + " AND in_fight_id = '" + in_fight_id + "'"
                    ;
                cfg.cmdsA.Add(cmd);
            }
        }

        private void RunStepA(TConfig cfg, Item itmReturn)
        {
            RunRebuildMain(cfg, itmReturn);

            if (cfg.rpc_tree_id != "")
            {
                RunRebuildRpc(cfg, cfg.rpc_tree_id, itmReturn);
            }

            //清除暫存資訊(三階選單)
            ClearCache(cfg, itmReturn);
        }

        //清除暫存資訊(三階選單)
        private void ClearCache(TConfig cfg, Item itmReturn)
        {
            Item itmData = cfg.inn.newItem();
            itmData.setType("In_Meeting");
            itmData.setProperty("meeting_id", cfg.meeting_id);
            itmData.setProperty("scene", "clear");
            itmData.apply("in_meeting_program_options");

            Item itmData2 = cfg.inn.newItem();
            itmData2.setType("In_Meeting");
            itmData2.setProperty("meeting_id", cfg.meeting_id);
            itmData2.setProperty("scene", "clear");
            itmData2.apply("in_meeting_day_options");
        }

        private void RunRebuildRpc(TConfig cfg, string rpc_tree_id, Item itmReturn)
        {
            var sql = "SELECT * FROM IN_MEETING_PEVENT WITH(NOLOCK)"
                + " WHERE source_id = '" + cfg.program_id + "'"
                + " AND in_tree_id = '" + rpc_tree_id + "'"
                ;
            var itmRpcEvent = cfg.inn.applySQL(sql);
            if (itmRpcEvent.getResult() == "")
            {
                itmReturn.setProperty("inn_tip", "查無敗部場次");
                return;
            }

            sql = "SELECT TOP 1 id, in_tree_sno, in_show_serial FROM IN_MEETING_PEVENT WITH(NOLOCK)"
                + " WHERE in_meeting = '" + cfg.meeting_id + "'"
                + " AND in_date_key = '" + cfg.fight_day + "'"
                + " AND in_site_code = " + cfg.site_code
                + " AND in_tree_no = " + cfg.target_rpc_no
                ;

            var itmTargetEvent = cfg.inn.applySQL(sql);
            if (itmTargetEvent.getResult() == "")
            {
                itmReturn.setProperty("inn_tip", "查無敗部目標場次");
                return;
            }

            var rpc_event_id = itmRpcEvent.getProperty("id", "");
            var rpc_show_serial = GetInt(itmTargetEvent.getProperty("in_show_serial", "0")) + 1;

            FootNoByPass(cfg, rpc_event_id, "1");
            FootNoByPass(cfg, rpc_event_id, "2");

            //更新該場次資料
            var sql1 = "UPDATE IN_MEETING_PEVENT SET "
                + "	 in_site_allocate = '1'"
                + ", in_win_creator = NULL"
                + ", in_win_creator_sno = NULL"
                + ", in_date_key = '" + cfg.fight_day + "'"
                + ", in_site = '" + cfg.site_id + "'"
                + ", in_site_code = " + cfg.site_code
                + ", in_show_site = " + cfg.site_code
                + ", in_tree_no = " + cfg.new_rpc_no
                + ", in_show_serial = " + rpc_show_serial
                + " WHERE id = '" + rpc_event_id + "'"
                ;

            cfg.inn.applySQL(sql1);
        }

        private string GetRpcTreeId(int n)
        {
            switch (n)
            {
                case 9: return "R104";
                case 10: return "R102";
                case 11: return "R103";
                case 12: return "R101";

                case 17: return "R104";
                case 18: return "R102";
                case 19: return "R103";
                case 20: return "R101";

                case 33: return "R104";
                case 34: return "R102";
                case 35: return "R103";
                case 36: return "R101";

                case 65: return "R104";
                case 66: return "R102";
                case 67: return "R103";
                case 68: return "R101";

                default: return "";
            }
        }
        private void RunRebuildMain(TConfig cfg, Item itmReturn)
        {
            var itmFoot = GetDetailItem(cfg);
            if (itmFoot.getResult() == "") throw new Exception("查無場次#2");

            var sql = "SELECT TOP 1 id, in_tree_sno, in_show_serial FROM IN_MEETING_PEVENT WITH(NOLOCK)"
                + " WHERE in_meeting = '" + cfg.meeting_id + "'"
                + " AND in_date_key = '" + cfg.fight_day + "'"
                + " AND in_site_code = " + cfg.site_code
                + " AND in_tree_no = " + cfg.target_main_no
                ;

            var itmTargetEvent = cfg.inn.applySQL(sql);
            if (itmTargetEvent.getResult() == "")
            {
                itmTargetEvent = cfg.inn.newItem();
            }


            cfg.new_event_id = itmFoot.getProperty("event_id", "");
            cfg.new_tree_id = itmFoot.getProperty("event_tree_id", "");
            cfg.new_detail_id = itmFoot.getProperty("detail_id", "");

            cfg.new_tree_sno = 1;
            cfg.new_show_serial = 1;
            if (itmTargetEvent.getResult() != "")
            {
                var target_tree_sno = GetInt(itmTargetEvent.getProperty("in_tree_sno", "0"));
                var target_show_serial = GetInt(itmTargetEvent.getProperty("in_show_serial", "0")) * 2;
                cfg.new_tree_sno = target_tree_sno + 1;
                cfg.new_show_serial = target_show_serial + 1;
            }

            var sql1 = "UPDATE IN_MEETING_PROGRAM SET"
                + "  in_team_count = in_team_count + 1"
                + ", in_event_count = in_event_count + 1"
                + " WHERE id = '" + cfg.program_id + "'"
                ;

            cfg.inn.applySQL(sql1);

            var sql2 = "UPDATE IN_MEETING_USER SET"
                + "  in_weight = '" + cfg.new_weight_value1 + "'"
                + ", in_weight_createname = '" + cfg.createname + "'"
                + ", in_weight_createid = '" + cfg.createid + "'"
                + ", in_weight_time = '" + cfg.utcNow + "'"
                + ", in_weight_result = '1'"
                + ", in_weight_status = 'on'"
                + " WHERE id = '" + cfg.muid + "'"
                ;

            cfg.inn.applySQL(sql2);


            var sql3 = "UPDATE IN_MEETING_PTEAM SET"
                + "  source_id = '" + cfg.program_id + "'"
                + ", in_check_result = '1'"
                + ", in_check_status = '通過'"
                + ", in_weight_result = '1'"
                + ", in_weight_value = '" + cfg.new_weight_value1 + "'"
                + ", in_weight_message = ''"
                + ", in_not_draw = '0'"
                + " WHERE id = '" + cfg.new_team_id + "'"
                ;

            cfg.inn.applySQL(sql3);

            //處理 in_tree_sno
            var sql4 = "UPDATE IN_MEETING_PEVENT"
                + " SET in_tree_sno = in_tree_sno + 1"
                + " WHERE source_id = '" + cfg.program_id + "'"
                + " AND in_tree_sno > " + (cfg.new_tree_sno - 1)
                + " AND ISNULL(in_tree_sno, 0) > 0"
                ;
            cfg.inn.applySQL(sql4);

            //該場地所有流水號放大2倍
            var sql5 = "UPDATE IN_MEETING_PEVENT"
                + " SET in_show_serial = in_show_serial * 2"
                + " WHERE in_meeting = '" + cfg.meeting_id + "'"
                + " AND in_date_key = '" + cfg.fight_day + "'"
                + " AND in_site_code = " + cfg.site_code
                + " AND ISNULL(in_show_serial, 0) > 0"
                ;
            cfg.inn.applySQL(sql5);

            //更新該場次資料
            var sql6 = "UPDATE IN_MEETING_PEVENT SET "
                + "	 in_site_allocate = '1'"
                + ", in_win_creator = NULL"
                + ", in_win_creator_sno = NULL"
                + ", in_date_key = '" + cfg.fight_day + "'"
                + ", in_site = '" + cfg.site_id + "'"
                + ", in_site_code = " + cfg.site_code
                + ", in_show_site = " + cfg.site_code
                + ", in_tree_no = " + cfg.new_main_no
                + ", in_show_serial = " + cfg.new_show_serial
                + " WHERE id = '" + cfg.new_event_id + "'"
                ;

            cfg.inn.applySQL(sql6);

            //直接給籤號
            AssignPlayerNo(cfg);

            //籤腳不輪空
            FootNoByPass(cfg, itmFoot);
            //場次歸零
            EventInitial(cfg, itmFoot);
        }

        //直接給籤號
        private void AssignPlayerNo(TConfig cfg)
        {
            var itmData = cfg.inn.newItem("In_Meeting_PTeam");
            itmData.setProperty("meeting_id", cfg.meeting_id);
            itmData.setProperty("program_id", cfg.program_id);
            itmData.setProperty("team_id", cfg.new_team_id);
            itmData.setProperty("exe_type", "judo_no");
            itmData.setProperty("in_judo_no", cfg.new_judo_no.ToString());
            itmData.apply("in_meeting_draw_update");
        }

        //籤腳不輪空
        private void FootNoByPass(TConfig cfg, Item itmFoot)
        {
            var in_sign_foot = itmFoot.getProperty("in_sign_foot", "");
            FootNoByPass(cfg, cfg.new_event_id, in_sign_foot);
        }

        //籤腳不輪空
        private void FootNoByPass(TConfig cfg, string event_id, string in_sign_foot)
        {
            string opp_foot = in_sign_foot == "1" ? "2" : "1";

            string sql_upd1 = "UPDATE IN_MEETING_PEVENT_DETAIL SET"
                + " in_sign_bypass = '0', in_status = NULL"
                + " WHERE source_id = '" + event_id + "' AND in_sign_foot = '" + in_sign_foot + "'";
            cfg.inn.applySQL(sql_upd1);

            string sql_upd2 = "UPDATE IN_MEETING_PEVENT_DETAIL SET"
                + " in_sign_bypass = '0', in_status = NULL"
                + " WHERE source_id = '" + event_id + "' AND in_sign_foot = '" + opp_foot + "'";
            cfg.inn.applySQL(sql_upd2);

            string sql_upd3 = "UPDATE IN_MEETING_PEVENT SET"
                + " in_bypass_foot = '', in_bypass_status = '1', in_win_status = NULL, in_win_sign_no = NULL, in_win_time = NULL"
                + " WHERE id = '" + event_id + "'";
            cfg.inn.applySQL(sql_upd3);
        }

        //場次歸零
        private void EventInitial(TConfig cfg, Item itmFoot)
        {
            var itmData = cfg.inn.newItem("In_Meeting_PEvent_Detail");
            itmData.setProperty("meeting_id", cfg.meeting_id);
            itmData.setProperty("program_id", cfg.program_id);
            itmData.setProperty("event_id", cfg.new_event_id);
            itmData.setProperty("tree_id", cfg.new_tree_id);
            itmData.setProperty("detail_id", cfg.new_detail_id);
            itmData.setProperty("points_text", "歸零");
            itmData.setProperty("points_type", "Initial");
            itmData.setProperty("team_name", "");
            //itmData.setProperty("target_detail_id", "");
            itmData.setProperty("mode", "score");
            itmData.setProperty("equipment_sno", "");
            itmData.apply("in_meeting_program_score_new");
        }

        private void AppendPlayerMenu(TConfig cfg, Item itmReturn)
        {
            cfg.itmProgram = GetProgramItem(cfg);
            var in_l1 = cfg.itmProgram.getProperty("in_l1", "");
            var in_l2 = cfg.itmProgram.getProperty("in_l2", "");
            var in_l3 = cfg.itmProgram.getProperty("in_l3", "");
            var in_fight_day = cfg.itmProgram.getProperty("in_fight_day", "");
            var site_id = cfg.itmProgram.getProperty("site_id", "");
            var site_name = cfg.itmProgram.getProperty("site_name", "");

            cfg.fight_day = cfg.itmProgram.getProperty("in_fight_day", "");
            cfg.site_code = cfg.itmProgram.getProperty("site_code", "");
            cfg.site_id = cfg.itmProgram.getProperty("site_id", "");

            cfg.old_team_count = GetInt(cfg.itmProgram.getProperty("in_team_count", "0"));
            cfg.new_team_count = cfg.old_team_count + 1;
            cfg.new_judo_no = GetNewJudoNo(cfg.new_team_count);
            cfg.new_sign_no = GetNewSignNo(cfg.new_team_count, cfg.new_judo_no);
            cfg.new_system_count = GetSystemCount(cfg.new_team_count);

            AppendCreatorList(cfg, in_l1, in_l2, in_l3, itmReturn);
            AppendPlayerList(cfg, in_l1, in_l2, in_l3, itmReturn);
            AppendMaxTreeNo(cfg, in_fight_day, site_id, itmReturn);

            itmReturn.setProperty("site_name", site_name);
            itmReturn.setProperty("new_judo_no", cfg.new_judo_no.ToString());
            itmReturn.setProperty("new_system_count", cfg.new_system_count.ToString().PadLeft(3, '0'));
        }

        private Item GetDetailItem(TConfig cfg)
        {
            var sql = @"
                SELECT 
	                t1.id	        AS 'event_id'
                    , t1.in_tree_id AS 'event_tree_id'
	                , t2.id         AS 'detail_id'
	                , t2.in_sign_foot
	                , t2.in_sign_no
                FROM 
	                IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                WHERE 
	                t1.source_id = '{#program_id}'
	                AND t2.in_sign_no = N'{#in_sign_no}'
            ";

            sql = sql.Replace("{#program_id}", cfg.program_id)
                .Replace("{#in_sign_no}", cfg.new_sign_no.ToString());

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        private Item GetPTeamItem(TConfig cfg, Item itmProgram, Item itmMUser)
        {
            var in_l1 = itmProgram.getProperty("in_l1", "");
            var in_l2 = itmProgram.getProperty("in_l2", "");
            var in_l3 = itmProgram.getProperty("in_l3", "");
            var in_name = itmMUser.getProperty("in_name", "");
            var sect_key = in_l1 + "-" + in_l2 + "-" + in_l3;

            var sql = @"
                SELECT 
                    t1.*
                FROM 
	                IN_MEETING_PTEAM t1 WITH(NOLOCK) 
                WHERE 
	                t1.in_meeting = '{#meeting_id}'
					AND t1.source_id IS NULL
					AND t1.in_team_key LIKE N'%{#sect_key}%'
					AND t1.in_name = N'{#in_name}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#sect_key}", sect_key)
                .Replace("{#in_name}", in_name);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        private Item GetMUserItem(TConfig cfg)
        {
            var sql = @"
                SELECT 
                    t1.*
                FROM 
	                IN_MEETING_USER t1 WITH(NOLOCK) 
                WHERE 
	                t1.id = '{#muid}'
            ";
            sql = sql.Replace("{#muid}", cfg.muid);
            return cfg.inn.applySQL(sql);
        }

        private Item GetProgramItem(TConfig cfg)
        {
            var sql = @"
                SELECT 
	                t1.id
	                , t1.in_l1
	                , t1.in_l2
	                , t1.in_l3
	                , t1.in_team_count
	                , t1.in_event_count
	                , t1.in_fight_day
	                , t2.id				AS 'site_id'
	                , t2.in_code		AS 'site_code'
	                , t2.in_name		AS 'site_name'
	                , t2.in_code_en		AS 'site_code_en'
                FROM 
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK) 
                LEFT OUTER JOIN
	                IN_MEETING_SITE t2 WITH(NOLOCK)
	                ON t2.id = t1.in_site
                WHERE 
	                t1.id = '{#program_id}'
            ";
            sql = sql.Replace("{#program_id}", cfg.program_id);
            return cfg.inn.applySQL(sql);
        }

        private void AppendMaxTreeNo(TConfig cfg, string in_fight_day, string in_site, Item itmReturn)
        {
            var sql = @"
                SELECT
	                MAX(in_tree_no) AS 'max_tree_no'
                FROM
	                IN_MEETING_PEVENT WITH(NOLOCK)
                WHERE
	                in_meeting = '{#meeting_id}'
	                AND in_date_key = '{#in_fight_day}'
	                AND in_site = '{#in_site}'
	                AND ISNULL(in_tree_no, 0) > 0
            ";
            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_fight_day}", in_fight_day)
                .Replace("{#in_site}", in_site);

            var itmResult = cfg.inn.applySQL(sql);
            if (itmResult.getResult() != "")
            {
                var noA = itmResult.getProperty("max_tree_no", "0");
                var noB = GetInt(noA) + 1;
                var noC = GetInt(noA) + 2;
                itmReturn.setProperty("max_tree_no", noA);
                itmReturn.setProperty("next_tree_no", noB.ToString());
                itmReturn.setProperty("rpc_tree_no", noC.ToString());
            }
            else
            {
                itmReturn.setProperty("max_tree_no", "0");
                itmReturn.setProperty("next_tree_no", "0");
                itmReturn.setProperty("rpc_tree_no", "0");
            }
        }

        private void AppendPlayerList(TConfig cfg, string in_l1, string in_l2, string in_l3, Item itmReturn)
        {
            var sql = @"
                SELECT 
                    t1.id
                	, t1.in_current_org
                	, t1.in_stuff_b1
                	, t1.in_short_org
                	, t1.in_team
                	, t1.in_name
                	, t1.in_weight
                	, t1.in_weight_status
                FROM
                	IN_MEETING_USER t1 WITH(NOLOCK)
                LEFT OUTER JOIN
                	IN_RESUME t2 WITH(NOLOCK)
                	ON t2.login_name = t1.in_creator_sno
                WHERE
                	t1.source_id = '{#meeting_id}'
                	AND t1.in_l1 = N'{#in_l1}'
                	AND t1.in_l2 = N'{#in_l2}'
                	AND t1.in_l3 = N'{#in_l3}'
                	AND ISNULL(t1.in_weight_status, '') IN ('off', 'leave')
                ORDER BY
                    t1.in_judo_no
	                , t1.in_stuff_b1
	                , t1.in_current_org
	                , t1.in_team
	                , t1.in_gender
	                , t1.in_sno
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_l1}", in_l1)
                .Replace("{#in_l2}", in_l2)
                .Replace("{#in_l3}", in_l3);

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                item.setType("inn_player");
                itmReturn.addRelationship(item);
            }
        }

        private void AppendCreatorList(TConfig cfg, string in_l1, string in_l2, string in_l3, Item itmReturn)
        {
            var sql = @"
                SELECT DISTINCT
                	t1.in_current_org
                	, t1.in_stuff_b1
                	, t1.in_short_org
                	, t1.in_team
                	, t1.in_creator		 	-- '協助報名者'
                	, t1.in_creator_sno		-- '協助帳號'
                	, t2.in_tel				-- '連絡電話'
                	, t2.in_email			-- '電子信箱'
                FROM
                	IN_MEETING_USER t1 WITH(NOLOCK)
                LEFT OUTER JOIN
                	IN_RESUME t2 WITH(NOLOCK)
                	ON t2.login_name = t1.in_creator_sno
                WHERE
                	t1.source_id = '{#meeting_id}'
                	AND t1.in_l1 = N'{#in_l1}'
                	AND t1.in_l2 = N'{#in_l2}'
                	AND t1.in_l3 = N'{#in_l3}'
                ORDER BY
                	t1.in_stuff_b1
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_l1}", in_l1)
                .Replace("{#in_l2}", in_l2)
                .Replace("{#in_l3}", in_l3);

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                item.setType("inn_creator");
                itmReturn.addRelationship(item);
            }
        }

        private void AppendSectMenu(TConfig cfg, Item itmReturn)
        {
            var sql = @"
                SELECT
	                id
	                , in_name
	                , in_team_count
                FROM
	                IN_MEETING_PROGRAM WITH(NOLOCK)
                WHERE
	                in_meeting = '{#meeting_id}'
                ORDER BY
	                in_sort_order
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                item.setType("inn_sect");
                itmReturn.addRelationship(item);
            }
        }

        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string meeting_id { get; set; }
            public string program_id { get; set; }
            public string muid { get; set; }
            public string scene { get; set; }

            public Item itmProgram { get; set; }
            public Item itmMUser { get; set; }
            public Item itmPTeam { get; set; }

            public string createname { get; set; }
            public string createid { get; set; }
            public string new_main_no { get; set; }
            public string target_main_no { get; set; }

            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }
            public string sect_key { get; set; }

            public string fight_day { get; set; }
            public string site_code { get; set; }
            public string site_id { get; set; }
            public string new_team_id { get; set; }
            public string new_event_id { get; set; }
            public string new_detail_id { get; set; }
            public string new_tree_id { get; set; }
            public string rpc_tree_id { get; set; }

            public int new_system_count { get; set; }
            public int old_team_count { get; set; }
            public int new_team_count { get; set; }
            public int new_sign_no { get; set; }
            public int new_judo_no { get; set; }
            public int new_show_serial { get; set; }
            public int new_tree_sno { get; set; }
            public int new_rpc_no { get; set; }
            public int target_rpc_no { get; set; }

            public string new_weight_value1 { get; set; }
            public decimal new_weight_value2 { get; set; }

            public string utcNow { get; set; }

            public List<string> cmdsA { get; set; }
            public List<string> cmdsB { get; set; }
            public List<string> cmdsC { get; set; }
        }

        private int GetNewJudoNo(int n)
        {
            if (n <= 3) return n;
            var svc = new InnSport.Core.Services.Impl.JudoSportService();
            var ns = svc.NoListByOrder(n);
            return ns.Last();
        }

        private int GetNewSignNo(int n, int j)
        {
            var svc = new InnSport.Core.Services.Impl.TkdSportService();
            var ns = svc.TkdNoListByPosition(n);
            var x = ns.Find(y => y.JodoNo == j);
            return x.TkdNo;
        }

        private int GetSystemCount(int n)
        {
            var svc = new InnSport.Core.Services.Impl.TkdSportService();
            var ns = svc.TkdNoListByPosition(n);
            return ns.Count;
        }

        private decimal GetDcmVal(string value, decimal def = 0)
        {
            decimal result = def;
            decimal.TryParse(value, out result);
            return result;
        }

        private int GetInt(string value, int defV = 0)
        {
            if (value == "") return 0;
            int result = defV;
            if (int.TryParse(value, out result)) return result;
            return defV;
        }
    }
}