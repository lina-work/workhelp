﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Judo.Clear
{
    public class In_Delete_Resume2 : Item
    {
        public In_Delete_Resume2(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 徹底移除 Resume 資料
                輸入: 
                    - Resume 物件
                日誌:
                    - 2020.09.16 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]In_Delete_Resume2";

            string aml = "";
            string sql = "";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "resume dom: " + itmR.dom.InnerXml);

            Item itmSQL = null;

            //取得講師履歷
            string resume_id = itmR.getProperty("id", "");
            string resume_name = itmR.getProperty("in_name", "");
            string resume_sno = itmR.getProperty("in_sno", "");
            string resume_photo = itmR.getProperty("in_photo", "");
            CCO.Utilities.WriteDebug(strMethodName, "徹底移除 Resume 資料 _# 開始: " + resume_sno + " " + resume_name);


            //取得使用者資訊
            sql = "SELECT TOP 1 id FROM [USER] WITH(NOLOCK) WHERE [login_name] = '{#in_sno}'";
            sql = sql.Replace("{#in_sno}", resume_sno);
            Item itmUser = inn.applySQL(sql);
            if (itmUser.isError() || itmUser.getResult() == "")
            {
                itmUser = inn.newItem();
            }


            //取得角色資訊
            sql = "SELECT TOP 1 id FROM [IDENTITY] WITH(NOLOCK) WHERE [in_number] = '{#in_sno}'";
            sql = sql.Replace("{#in_sno}", resume_sno);
            Item itmIdentity = inn.applySQL(sql);
            if (itmIdentity.isError() || itmIdentity.getResult() == "")
            {
                itmIdentity = inn.newItem();
            }

            string user_id = itmUser.getProperty("id", "");
            string identity_id = itmIdentity.getProperty("id", "");


            //刪除問卷結果
            sql = @"DELETE FROM [IN_MEETING_SURVEYS_RESULT] WHERE in_participant IN 
                (
                    SELECT id FROM IN_MEETING_USER WITH(NOLOCK) WHERE ISNULL(in_sno, '') = '{#in_sno}'
                )";
            sql = sql.Replace("{#in_sno}", resume_sno);
            inn.applySQL(sql);


            //刪除學員履歷
            sql = @"DELETE FROM [IN_MEETING_RESUME] WHERE [IN_USER] IN 
                (
                    SELECT id FROM IN_MEETING_USER WITH(NOLOCK) WHERE ISNULL(in_sno, '') = '{#in_sno}'
                )";
            sql = sql.Replace("{#in_sno}", resume_sno);
            inn.applySQL(sql);


            //解除講師履歷關聯
            sql = @"UPDATE IN_RESUME SET owned_by_id = '', in_user_id = '', in_photo = '' WHERE id = '{#resume_id}'";
            sql = sql.Replace("{#resume_id}", resume_id);
            inn.applySQL(sql);

            if (user_id != "")
            {
                //解除使用者關聯
                sql = @"UPDATE [USER] SET [IN_RESUME] = '' WHERE [ID] = '{#user_id}'";
                sql = sql.Replace("{#user_id}", user_id);
                inn.applySQL(sql);

                //刪除別名
                sql = @"DELETE FROM [ALIAS] WHERE [SOURCE_ID] = '{#user_id}'";
                sql = sql.Replace("{#user_id}", user_id);
                inn.applySQL(sql);
            }

            if (identity_id != "")
            {
                //刪除別名
                sql = @"DELETE FROM [ALIAS] WHERE [RELATED_ID] = '{#identity_id}'";
                sql = sql.Replace("{#identity_id}", identity_id);
                inn.applySQL(sql);
            }


            //刪除成員角色
            sql = @"DELETE FROM [IN_RESUME_RESUME] WHERE [RELATED_ID] = '{#resume_id}'";
            sql = sql.Replace("{#resume_id}", resume_id);
            inn.applySQL(sql);


            //刪除成員角色
            sql = @"DELETE FROM [IN_RESUME_RESUME] WHERE [SOURCE_ID] = '{#resume_id}'";
            sql = sql.Replace("{#resume_id}", resume_id);
            inn.applySQL(sql);

            if (identity_id != "")
            {
                //刪除 MEMBER
                sql = @"DELETE FROM [MEMBER] WHERE [RELATED_ID] = '{#identity_id}'";
                sql = sql.Replace("{#identity_id}", identity_id);
                inn.applySQL(sql);

                //刪除角色
                sql = @"DELETE FROM [IDENTITY] WHERE [ID] = '{#identity_id}'";
                sql = sql.Replace("{#identity_id}", identity_id);
                inn.applySQL(sql);
            }


            if (user_id != "")
            {
                //刪除使用者
                sql = @"DELETE FROM [USER] WHERE [ID] = '{#user_id}'";
                sql = sql.Replace("{#user_id}", user_id);
                inn.applySQL(sql);
            }


            //刪除講師履歷
            sql = @"DELETE FROM [IN_RESUME] WHERE [ID] = '{#resume_id}'";
            sql = sql.Replace("{#resume_id}", resume_id);
            inn.applySQL(sql);
            

            if (resume_photo != "")
            {
                //刪除大頭照所在
                sql = @"DELETE FROM [LOCATED] WHERE [SOURCE_ID] = '{#resume_photo}'";
                sql = sql.Replace("{#resume_photo}", resume_photo);
                inn.applySQL(sql);
                if (itmSQL.isError())
                {
                    throw new Exception("刪除檔案所在: 發生錯誤 sql = " + sql);
                }

                //刪除大頭照
                sql = @"DELETE FROM [FILE] WHERE [ID] = '{#resume_photo}'";
                sql = sql.Replace("{#resume_photo}", resume_photo);
                inn.applySQL(sql);
            }

            CCO.Utilities.WriteDebug(strMethodName, "徹底移除 Resume 資料 _# 完成: " + resume_sno + " " + resume_name);

            return itmR;
        }
    }
}