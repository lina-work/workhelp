﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Judo.Clear
{
    public class Fix_Resume_Import : Item
    {
        public Fix_Resume_Import(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 修正載入關聯
                日誌: 
                    - 2023-11-28: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "Fix_Resume_Import";

            var itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
            };

            Run(cfg);

            return itmR;
        }

        private void Run(TConfig cfg)
        {
            var itmCreators = GetCreatorResumeList(cfg);
            if (itmCreators.isError() || itmCreators.getResult() == "") return;

            var count = itmCreators.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var itmCreator = itmCreators.getItemByIndex(i);
                var source_id = itmCreator.getProperty("id", "");
                var in_current_org = itmCreator.getProperty("in_current_org", "");

                var itmNodes = GetOrgResumeList(cfg, in_current_org);
                if (itmNodes.isError() || itmNodes.getResult() == "") continue;

                var node_count = itmNodes.getItemCount();
                for (var j = 0; j < node_count; j++)
                {
                    var itmNode = itmNodes.getItemByIndex(j);
                    var related_id = itmNode.getProperty("id", "");
                    MergeResumeResume(cfg, source_id, related_id);
                }
            }
        }

        //成員角色
        private void MergeResumeResume(TConfig cfg, string source_id, string related_id)
        {
            var sql = "SELECT id FROM In_Resume_Resume WITH(NOLOCK)"
                + " WHERE source_id = '" + source_id + "'"
                + " AND related_id = '" + related_id + "'";

            var itmOld = cfg.inn.applySQL(sql);
            if (itmOld.isError()) return;
            if (itmOld.getResult() != "") return;

            Item itmNew = cfg.inn.newItem("In_Resume_Resume", "add");
            itmNew.setProperty("source_id", source_id);
            itmNew.setProperty("related_id", related_id);
            itmNew.setProperty("in_resume_role", "110");
            itmNew.apply();
        }

        private Item GetOrgResumeList(TConfig cfg, string in_current_org)
        {
            var sql = @"
                SELECT 
	                id 
                FROM 
	                IN_RESUME WITH(NOLOCK)
                WHERE 
	                in_current_org = N'{#in_current_org}'
                ORDER BY
	                in_sno
            ";

            sql = sql.Replace("{#in_current_org}", in_current_org);

            return cfg.inn.applySQL(sql);
        }

        private Item GetCreatorResumeList(TConfig cfg)
        {
            var sql = @"
                SELECT DISTINCT
	                t1.in_creator_sno
	                , t2.in_name
	                , t2.in_current_org
	                , t2.id
                FROM 
	                IN_MEETING_USER t1 WITH(NOLOCK)
                INNER JOIN
	                IN_RESUME t2 WITH(NOLOCK)
	                ON t2.login_name = t1.in_creator_sno
                WHERE 
	                ISNULL(t1.in_creator_sno, '') NOT IN ('', 'lwu001')
	                AND t1.in_creator NOT LIKE '%lwu%'
                ORDER BY
	                t1.in_creator_sno
            ";

            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
        }

    }
}
