﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Judo.Common
{
    public class Lock_Meeting_PEvent : Item
    {
        public Lock_Meeting_PEvent(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 保護場次資料
                日誌: 
                    - 2023-02-14: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "Lock_Meeting_PEvent";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,

                meeting_id = itmR.getProperty("meeting_id", ""),
                program_id = itmR.getProperty("program_id", ""),
                in_date = itmR.getProperty("in_date", ""),
                in_l1 = itmR.getProperty("in_l1", ""),
                in_l2 = itmR.getProperty("in_l2", ""),
                in_l3 = itmR.getProperty("in_l3", ""),
                muid = itmR.getProperty("id", ""),
                scene = itmR.getProperty("scene", ""),
            };

            if (cfg.meeting_id == "") cfg.meeting_id = itmR.getProperty("source_id", "");
            if (cfg.in_l1 == "") cfg.in_l1 = itmR.getProperty("l1_value", "");
            if (cfg.in_l2 == "") cfg.in_l2 = itmR.getProperty("l2_value", "");
            if (cfg.in_l3 == "") cfg.in_l3 = itmR.getProperty("l3_value", "");

            var need_lock = false;
            if (cfg.meeting_id != "")
            {
                //附加賽會參數
                Item itmMtVrl = inn.applyMethod("In_Meeting_Variable"
                        , "<meeting_id>" + cfg.meeting_id + "</meeting_id>"
                        + "<name>today_is_fight_day</name>"
                        + "<scene>get</scene>");

                need_lock = itmMtVrl.getProperty("inn_result", "") == "1";
            }

            if (need_lock)
            {
                CheckDay(cfg);
            }
            else if (cfg.muid != "")
            {
                CheckMUser(cfg);
            }

            return itmR;
        }

        private void CheckMUser(TConfig cfg)
        {
            var today = DateTime.Now.ToString("yyyy-MM-dd");
            var model = GetAgeSect(cfg, cfg.in_l1, cfg.in_l2);
            if (!model.need_check) return;

            var day = GetDaySettings(cfg, model);
            if (day == "") return;

            if (day != today)
            {
                throw new Exception(cfg.in_l2.Replace("個-", "") + "只允許在" + day + "進行請假");
            }
        }

        private string GetDaySettings(TConfig cfg, TMTV model)
        {
            var sql = @"
                SELECT
	                in_key
	                , in_value
                FROM
	                IN_MEETING_VARIABLE WITH(NOLOCK)
                WHERE
	                source_id = '{#meeting_id}'
	                AND in_key LIKE 'apply_leave_day%'
	                AND in_value LIKE N'{#key}%'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#key}", model.age_sect);

            var item = cfg.inn.applySQL(sql);
            if (item.isError() || item.getResult() == "")
            {
                return "";
            }
            else
            {
                return item.getProperty("in_value", "").Replace(model.age_sect + ":", "");
            }
        }

        private TMTV GetAgeSect(TConfig cfg, string in_l1, string in_l2)
        {
            var result = new TMTV { need_check = false, age_sect = "" };
            if (in_l1.Contains("隊職員")) return result;
            if (in_l1.Contains("團體")) return result;
            if (in_l1.Contains("格式")) return result;
            if (in_l1.Contains("常年會費")) return result;

            if (in_l2.Contains("國小")) result.age_sect = "國小組";
            else if (in_l2.Contains("國中")) result.age_sect = "國中組";
            else if (in_l2.Contains("高中")) result.age_sect = "高中組";
            else if (in_l2.Contains("大專")) result.age_sect = "大專組";
            else if (in_l2.Contains("社會")) result.age_sect = "社會組";
            else if (in_l2.Contains("公開")) result.age_sect = "公開組";
            else if (in_l2.Contains("一般")) result.age_sect = "一般組";
            else if (in_l2.Contains("成年")) result.age_sect = "成年組";
            else if (in_l2.Contains("青少年")) result.age_sect = "青少年組";
            else if (in_l2.Contains("青年")) result.age_sect = "青年組";
            else result.age_sect = "";

            result.need_check = result.age_sect != "";
            return result;
        }

        private class TMTV
        { 
            public bool need_check { get; set; }
            public string age_sect { get; set; }
        }


        private void CheckDay(TConfig cfg)
        {
            var today = DateTime.Now.ToString("yyyy-MM-dd");
            var msg = "當日資料不開放異動";

            if (cfg.in_date == today)
            {
                throw new Exception(msg);
            }

            var in_fight_day = FightDay(cfg);

            if (in_fight_day == today)
            {
                throw new Exception(msg);
            }
        }

        private string FightDay(TConfig cfg)
        {
            if (cfg.program_id == "")
            {
                return FightDayByLv(cfg);
            }
            else
            {

                return FightDayById(cfg);
            }
        }

        private string FightDayByLv(TConfig cfg)
        {
            string sql = @"
                SELECT
	                t1.in_fight_day
                FROM 
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                WHERE
	                t1.in_meeting = '{#meeting_id}'
	                AND t1.in_l1 = N'{#in_l1}'
	                AND t1.in_l2 = N'{#in_l2}'
	                AND t1.in_l3 = N'{#in_l3}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_l1}", cfg.in_l1)
                .Replace("{#in_l2}", cfg.in_l2)
                .Replace("{#in_l3}", cfg.in_l3)
                ;

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            var item = cfg.inn.applySQL(sql);

            if (!item.isError() && item.getResult() != "")
            {
                return item.getProperty("in_fight_day", "");
            }

            return "";
        }

        private string FightDayById(TConfig cfg)
        {
            string sql = @"
                SELECT
	                t1.in_fight_day
                FROM 
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                WHERE
	                t1.id = '{#program_id}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#program_id}", cfg.program_id)
                ;

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            var item = cfg.inn.applySQL(sql);

            if (!item.isError() && item.getResult() != "")
            {
                return item.getProperty("in_fight_day", "");
            }

            return "";
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }

            public string meeting_id { get; set; }
            public string program_id { get; set; }
            public string in_date { get; set; }
            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }
            public string muid { get; set; }
            
            public string scene { get; set; }

        }
    }
}