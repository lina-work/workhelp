﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Judo.舊報表
{
    public class in_playerid_judo : Item
    {
        public in_playerid_judo(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
            目的:創建[柔道]選手證
            說明:2020.10.29 還原到依著組別出 2020.08.17(因著109年全國柔道錦標賽做在取資料時做了調整並切割字串)
            輸入:
                2020.11.03 用此邏輯出選手證
                T0.隊職員(隊職員單獨出一份)
                T1.社會(匯出前檢查是否報名過大專,高中,國中如果有則不出)(只有報名社會組才出)
                T2.大專(包含社會 但不包含格式組)
                T3.高中(包含社會 但不包含格式組)
                T4.國中(包含社會 但不包含格式組)
                T5.國小(不包含格式組)
                T6.團體(只有報名團體組的才出)
                T7.格式(格式組單獨出一份)
                T8.非以上組別(人數超過1200會出錯)
            輸出:
            位置:
            做法:
            */

            //System.Diagnostics.Debugger.Break();

            Innovator inn = this.getInnovator();
            Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

            string strDatabaseName = inn.getConnection().GetDatabaseName();
            string strMethodName = "[" + strDatabaseName + "]" + "in_playerid_judo";

            Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
            bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

            string aml = "";
            string sql = "";
            string strError = "";
            string strUserId = inn.getUserID();
            //string strIdentityId = inn.getUserAliases();

            _InnH.AddLog(strMethodName, "MethodSteps");
            Item itmR = this;

            string meeting_id = this.getProperty("meeting_id", "");
            //string meeting_id = "BECB34B6550E478E83EEECBFE8B0C323";
            bool open = false;
            Item Resume = this;
            string SQL_OnlyMyGym = "";
            //CCO.Utilities.WriteDebug("in_playerid_judo", "this:" + this.dom.InnerXml);
            //判斷現在的登入者是否為此課程的共同講師 
            aml = "<AML>" +
                    "<Item type='In_Meeting_Resumelist' action='get'>" +
                    "<source_id>" + meeting_id + "</source_id>" +
                    "</Item></AML>";
            Item Meeting_Resumelists = inn.applyAML(aml);

            for (int i = 0; i < Meeting_Resumelists.getItemCount(); i++)
            {
                Item Meeting_Resumelist = Meeting_Resumelists.getItemByIndex(i);
                //判定登入者ID是否存在於該課程共同講師裡面
                if (strUserId == Meeting_Resumelist.getProperty("in_user_id", ""))//此次登入者是共同講師
                {
                    open = true;
                }
                else
                {
                    open = false;
                }
            }

            aml = "<AML>" +
            "<Item type='In_Resume' action='get'>" +
            "<in_user_id>" + strUserId + "</in_user_id>" +
            "</Item></AML>";
            Resume = inn.applyAML(aml);

            if (!open)//不是共同講師走這
            {
                if (CCO.Permissions.IdentityListHasId(Aras.Server.Security.Permissions.Current.IdentitiesList, "7AC8B33DA0F246DDA70BB244AB231E29"))
                {

                    SQL_OnlyMyGym = " and [in_meeting_user].in_group=N'" + Resume.getProperty("in_group", "") + "'";
                }
            }

            if (meeting_id == "")
                throw new Exception("課程id不得為空白");

            bool IsTemplate = false;


            aml = "<AML>" +
                "<Item type='In_Meeting' action='get' id='" + meeting_id + "'>" +
                "<Relationships>" +

                "<Item type='In_Meeting_Surveys' action='get' select='related_id(id,in_questions,in_request,in_selectoption,in_question_type,in_property,in_expense,in_is_pkey)'>" +
                "<in_surveytype>1</in_surveytype>" +
                "<in_property condition='in'>'in_l1','in_l2','in_l3'</in_property>" +
                "</Item>" +


                "</Relationships>" +
            "</Item></AML>";

            //1.取得課程主檔
            Item Meeting = inn.applyAML(aml);


            //賽事的資訊
            string in_title = Meeting.getProperty("in_title", "");//賽事名稱
            string in_address = Meeting.getProperty("in_address", "");//比賽地點
            string in_date_s = Meeting.getProperty("in_date_s", "");//賽事開始日
            string in_date_e = Meeting.getProperty("in_date_e", "");//賽事結束日
                                                                    //轉民國年

            DateTime in_date_s_dt = DateTime.Parse(in_date_s);
            DateTime in_date_e_dt = DateTime.Parse(in_date_e);
            var time_date_s = string.Format("{0}年{1}月{2}", new System.Globalization.TaiwanCalendar().GetYear(in_date_s_dt), in_date_s_dt.Month, in_date_s_dt.Day);
            var time_date_e = string.Format("{2}日", new System.Globalization.TaiwanCalendar().GetYear(in_date_e_dt), in_date_e_dt.Month, in_date_e_dt.Day);


            //throw new Exception("該賽事報名人數過多無法使用此功能");

            //開始整理EXCEL Template
            aml = "<AML>" +
                "<Item type='In_Variable' action='get'>" +
                "<in_name>meeting_excel</in_name>" +
                "<Relationships>" +
                "<Item type='In_Variable_Detail' action='get'/>" +
                "</Relationships>" +
                "</Item></AML>";
            Item Vairable = inn.applyAML(aml);

            string Template_Path = "";
            string Export_Path = "";
            for (int i = 0; i < Vairable.getRelationships("In_Variable_Detail").getItemCount(); i++)
            {
                Item In_Variable_Detail = Vairable.getRelationships("In_Variable_Detail").getItemByIndex(i);
                if (In_Variable_Detail.getProperty("in_name", "") == "template_4_path")
                    Template_Path = In_Variable_Detail.getProperty("in_value", "");

                if (In_Variable_Detail.getProperty("in_name", "") == "export_path")
                    Export_Path = In_Variable_Detail.getProperty("in_value", "");
                if (!Export_Path.EndsWith(@"\"))
                    Export_Path = Export_Path + @"\";
            }

            //Word
            Xceed.Words.NET.DocX doc;//word
            doc = Xceed.Words.NET.DocX.Load(Template_Path);//載入模板
            var docTables = doc.Tables[0];//取得模板第一個表格
                                          //var d = doc.Pictures[0];

            //CCO.Utilities.WriteDebug("in_playerid", "0");


            string output_type = this.getProperty("output_type", "T9");//區分匯出的組別
            string type_name = "";//拚進去的字串


            switch (output_type)
            {
                case "T0":
                    //隊職員單獨出一份

                    type_name = "隊職員";
                    sql = "select * from in_meeting_user where in_l1=N'隊職員' and source_id='" + meeting_id + "' and [In_Meeting_User].in_note_state='official'" + SQL_OnlyMyGym + "order By in_gender desc,in_current_org,in_name,in_excel_order,in_excel_order_2,in_excel_order_3";
                    break;

                case "T1":
                    //出社會組之前判斷有沒有報過大專 高中 國中 有的則不出(只報名社會組的才出) 

                    type_name = "只報社會";

                    sql = "select string_agg(IN_SECTION_NAME, ', ') as in_lx,in_name,in_stuff_b1,in_short_org,in_photo1,in_l1,in_l2,in_l3,in_gender,in_current_org,in_excel_order,in_excel_order_2,in_excel_order_3,in_sign_no,in_section_no from In_Meeting_User where source_id='" + meeting_id + "' and in_l2 LIKE '%社會%' and in_sno NOT IN (select in_sno from in_meeting_user where (in_l2 LIKE '%大專%' or in_l2 LIKE '%高中%' or in_l2 LIKE '%國中%' or in_l2 LIKE '%國小%') and source_id='" + meeting_id + "') group by in_name,in_stuff_b1,in_short_org,in_photo1,in_l1,in_l2,in_l3,in_gender,in_current_org,in_excel_order,in_excel_order_2,in_excel_order_3,in_sign_no,in_section_no order By in_gender desc,in_current_org,in_name,in_excel_order,in_excel_order_2,in_excel_order_3,in_sign_no";


                    break;

                case "T2":
                    //大專可跨社會(出大專有報社會 就一樣把組別拚進去  格式組不包含在這裡)
                    type_name = "大專(含社會)";

                    sql = "select string_agg(IN_SECTION_NAME, ', ') as in_lx,in_name,in_stuff_b1,in_short_org,in_photo1,in_l1,in_l2,in_l3,in_gender,in_current_org,in_excel_order,in_excel_order_2,in_excel_order_3,in_sign_no,in_section_no from In_Meeting_User where source_id='" + meeting_id + "'and (in_l2 LIKE '%社會%' or in_l2 LIKE '%大專%') and in_l1 <> '格式組' and in_sno IN (select in_sno from in_meeting_user where in_l2 LIKE '%大專%' and source_id='" + meeting_id + "') group by in_name,in_stuff_b1,in_short_org,in_photo1,in_l1,in_l2,in_l3,in_gender,in_current_org,in_excel_order,in_excel_order_2,in_excel_order_3,in_sign_no,in_section_no order By in_gender desc,in_current_org,in_name,in_excel_order,in_excel_order_2,in_excel_order_3,in_sign_no";

                    break;

                case "T3":
                    //高中可跨社會(出高中有報社會 就一樣把組別拚進去  格式組不包含在這裡)

                    type_name = "高中(含社會)";

                    sql = "select string_agg(IN_SECTION_NAME, ', ') as in_lx,in_name,in_stuff_b1,in_short_org,in_photo1,in_l1,in_l2,in_l3,in_gender,in_current_org,in_excel_order,in_excel_order_2,in_excel_order_3,in_sign_no,in_section_no from In_Meeting_User where source_id='" + meeting_id + "'and (in_l2 LIKE '%社會%' or in_l2 LIKE '%高中%') and in_l1 <> '格式組' and in_sno IN (select in_sno from in_meeting_user where in_l2 LIKE '%高中%' and source_id='" + meeting_id + "') group by in_name,in_stuff_b1,in_short_org,in_photo1,in_l1,in_l2,in_l3,in_gender,in_current_org,in_excel_order,in_excel_order_2,in_excel_order_3,in_sign_no,in_section_no order By in_gender desc,in_current_org,in_name,in_excel_order,in_excel_order_2,in_excel_order_3,in_sign_no";

                    break;

                case "T4":
                    //國中可跨社會(出國中有報社會 就一樣把組別拚進去  格式組不包含在這裡)

                    type_name = "國中(含社會)";
                    sql = "select string_agg(IN_SECTION_NAME, ', ') as in_lx,in_name,in_stuff_b1,in_short_org,in_photo1,in_l1,in_l2,in_l3,in_gender,in_current_org,in_excel_order,in_excel_order_2,in_excel_order_3,in_sign_no,in_section_no from In_Meeting_User where source_id='" + meeting_id + "'and (in_l2 LIKE '%社會%' or in_l2 LIKE '%國中%') and in_l1 <> '格式組' and in_sno IN (select in_sno from in_meeting_user where in_l2 LIKE '%國中%' and source_id='" + meeting_id + "') group by in_name,in_stuff_b1,in_short_org,in_photo1,in_l1,in_l2,in_l3,in_gender,in_current_org,in_excel_order,in_excel_order_2,in_excel_order_3,in_sign_no,in_section_no order By in_gender desc,in_current_org,in_name,in_excel_order,in_excel_order_2,in_excel_order_3,in_sign_no";

                    break;

                case "T5":
                    //國小不會報社會(格式組不包含在這裡)

                    type_name = "國小";
                    sql = "select string_agg(IN_SECTION_NAME, ', ') as in_lx,in_name,in_stuff_b1,in_short_org,in_photo1,in_l1,in_l2,in_l3,in_gender,in_current_org,in_excel_order,in_excel_order_2,in_excel_order_3,in_sign_no,in_section_no from in_meeting_user where in_l2 LIKE '%國小%' and in_l1 <> '格式組' and source_id='" + meeting_id + "' and [In_Meeting_User].in_note_state='official'" + SQL_OnlyMyGym + " group by in_name,in_stuff_b1,in_short_org,in_photo1,in_l1,in_l2,in_l3,in_gender,in_current_org,in_excel_order,in_excel_order_2,in_excel_order_3,in_sign_no,in_section_no order By in_gender desc,in_current_org,in_name,in_excel_order,in_excel_order_2,in_excel_order_3,in_sign_no";
                    break;

                case "T6":
                    // 2.團體組單獨出一份(只有報團體)

                    type_name = "只報團體";

                    sql = "select string_agg(IN_SECTION_NAME, ', ') as in_lx,in_name,in_stuff_b1,in_short_org,in_photo1,in_l1,in_l2,in_l3,in_gender,in_current_org,in_excel_order,in_excel_order_2,in_excel_order_3,in_sign_no,in_section_no from In_Meeting_User where source_id='" + meeting_id + "' and in_l1=N'團體組' and in_sno NOT IN (select in_sno from in_meeting_user where (in_l1 = N'個人組' or in_l1 = N'格式組') and source_id='" + meeting_id + "') group by in_name,in_stuff_b1,in_short_org,in_photo1,in_l1,in_l2,in_l3,in_gender,in_current_org,in_excel_order,in_excel_order_2,in_excel_order_3,in_sign_no,in_section_no order By in_gender desc,in_current_org,in_name,in_excel_order,in_excel_order_2,in_excel_order_3,in_sign_no";

                    break;

                case "T7":
                    // 4.格式組單獨出一份

                    type_name = "格式";
                    sql = "select string_agg(IN_SECTION_NAME, ', ') as in_lx,in_name,in_stuff_b1,in_short_org,in_photo1,in_l1,in_l2,in_l3,in_gender,in_current_org,in_excel_order,in_excel_order_2,in_excel_order_3,in_sign_no,in_section_no from in_meeting_user where in_l1=N'格式組' and source_id='" + meeting_id + "' and [In_Meeting_User].in_note_state='official'" + SQL_OnlyMyGym + " group by in_name,in_stuff_b1,in_short_org,in_photo1,in_l1,in_l2,in_l3,in_gender,in_current_org,in_excel_order,in_excel_order_2,in_excel_order_3,in_sign_no,in_section_no order By in_gender desc,in_current_org,in_name,in_excel_order,in_excel_order_2,in_excel_order_3,in_sign_no";
                    break;

                case "T8":
                    //沒有以上組別(人數1200以下)

                    type_name = "全部(1200人以下)";
                    sql = "select * from in_meeting_user where source_id='" + meeting_id + "' and [In_Meeting_User].in_note_state='official'" + SQL_OnlyMyGym + "order By in_gender desc,in_current_org,in_name,in_excel_order,in_excel_order_2,in_excel_order_3,in_sign_no";
                    break;
                case "T9":
                    // 4.格式組單獨出一份

                    type_name = "成年";
                    sql = "select string_agg(IN_SECTION_NAME, ', ') as in_lx,in_name,in_stuff_b1,in_short_org,in_photo1,in_l1,in_l2,in_l3,in_gender,in_current_org,in_excel_order,in_excel_order_2,in_excel_order_3,in_sign_no,in_section_no from in_meeting_user where in_l1=N'成年組' and source_id='" + meeting_id + "' and [In_Meeting_User].in_note_state='official'" + SQL_OnlyMyGym + " group by in_name,in_stuff_b1,in_short_org,in_photo1,in_l1,in_l2,in_l3,in_gender,in_current_org,in_excel_order,in_excel_order_2,in_excel_order_3,in_sign_no,in_section_no order By in_gender desc,in_current_org,in_name,in_excel_order,in_excel_order_2,in_excel_order_3,in_sign_no";
                    break;
            }


            /*
            //抓取指定的筆數(2020.10.14 超過1000筆會掛掉)
            sql = "select * from in_meeting_user where source_id='" + meeting_id + "' and [In_Meeting_User].in_note_state='official'" + SQL_OnlyMyGym + "order By in_gender desc,in_current_org,in_name offset 0 row fetch next 1000 rows only";
            */

            CCO.Utilities.WriteDebug("[Judo]" + strMethodName, sql);

            Item Orgs = inn.applySQL(sql);
            string Orgs_count = Orgs.getItemCount().ToString();
            //CCO.Utilities.WriteDebug("in_playerid", "Orgs:" + Orgs.dom.InnerXml);	

            //if(Orgs.getItemCount() > 1200)
            //throw new Exception("該組別報名人數過多無法使用此功能");



            Dictionary<string, Item> DicOrgs = new Dictionary<string, Item>();
            string filename = "";
            string id_1 = "";
            string id_2 = "";
            string id_3 = "";
            string url = @"C:\Aras\Vault\judo\";//讀取大頭照的路徑
            for (int i = 0; i < Orgs.getItemCount(); i++)
            {
                Item Org = Orgs.getItemByIndex(i);

                string in_current_org = Org.getProperty("in_current_org", "");
                string in_name = Org.getProperty("in_name", "");
                string in_l1 = Org.getProperty("in_l1", "");
                string in_l2 = Org.getProperty("in_l2", "");
                string in_l3 = Org.getProperty("in_l3", "");
                string OrgName = "";

                //若是隊職員 則多拚二階進去當key值
                if (in_l1 == "隊職員")
                {
                    OrgName = in_current_org + "_" + in_name + "_" + in_l2;
                }
                else
                {
                    OrgName = in_current_org + "_" + in_name;
                }



                string in_sno = Org.getProperty("in_sno", "");
                string in_lx = Org.getProperty("in_lx", "");
                //CCO.Utilities.WriteDebug("in_playerid", "in_lx:" + in_lx);



                //CCO.Utilities.WriteDebug("in_playerid", "OrgName_158:" + OrgName);
                Item orgnode = inn.newItem();
                orgnode.setProperty("sheet_index", "0");
                orgnode.setProperty("last_row", "3");
                //in_meeting_user
                aml = "<AML>" +
                "<Item type='File' action='get'>" +
                "<id>" + Org.getProperty("in_photo1", "") + "</id>" +
                "</Item></AML>";
                Item Files = inn.applyAML(aml);
                //CCO.Utilities.WriteDebug("in_playerid", "Files:" + Files.dom.InnerXml); 
                if (Files.getItemCount() > 0)
                {
                    filename = Files.getProperty("filename", "");//取得檔名
                }
                orgnode.setProperty("in_photo", Org.getProperty("in_photo1", ""));
                orgnode.setProperty("filename", filename);
                orgnode.setProperty("in_name", Org.getProperty("in_name", ""));
                orgnode.setProperty("in_current_org", Org.getProperty("in_current_org", ""));
                orgnode.setProperty("in_l1", Org.getProperty("in_l1", ""));
                orgnode.setProperty("in_l2", Org.getProperty("in_l2", ""));
                orgnode.setProperty("in_stuff_c1", Org.getProperty("in_stuff_c1", ""));
                orgnode.setProperty("in_stuff_b1", Org.getProperty("in_stuff_b1", ""));
                orgnode.setProperty("in_short_org", Org.getProperty("in_short_org", ""));
                orgnode.setProperty("in_sign_no", Org.getProperty("in_sign_no", ""));
                orgnode.setProperty("in_section_no", Org.getProperty("in_section_no", ""));


                //2020.08.17(應著109年全國柔道錦標賽的新格式)
                if (Org.getProperty("in_l1", "").Contains("團體"))
                {
                    string re_in_l3 = text_check(Org.getProperty("in_l3", ""));

                    if (re_in_l3.Contains('('))
                        re_in_l3 = re_in_l3.Split('(')[0];

                    orgnode.setProperty("in_l3_group", re_in_l3);
                    //orgnode.setProperty("in_l3_group",Org.getProperty("in_section_no","") + re_in_l3);
                    //orgnode.setProperty("in_l3_group",in_lx);
                }
                else if (Org.getProperty("in_l1", "").Contains("個人"))
                {
                    string re_in_l2 = text_check(Org.getProperty("in_l2", ""));

                    if (re_in_l2.Contains('('))
                        re_in_l2 = re_in_l2.Split('(')[0];

                    orgnode.setProperty("in_l3", Org.getProperty("in_section_no", "") + re_in_l2 + "-" + Org.getProperty("in_l3", ""));
                    //orgnode.setProperty("in_l3",in_lx);

                }
                else if (Org.getProperty("in_l1", "").Contains("格式"))
                {

                    string re_in_l2 = text_check(Org.getProperty("in_l2", ""));

                    if (re_in_l2.Contains('('))
                        re_in_l2 = re_in_l2.Split('(')[0];

                    orgnode.setProperty("in_l3", Org.getProperty("in_section_no", "") + re_in_l2 + "-" + Org.getProperty("in_l3", ""));
                    //orgnode.setProperty("in_l3",in_lx);
                }
                else if (Org.getProperty("in_l1", "").Contains("隊職員"))
                {
                    string re_in_l2 = text_check(Org.getProperty("in_l2", ""));

                    if (re_in_l2.Contains('('))
                        re_in_l2 = re_in_l2.Split('(')[0];

                    orgnode.setProperty("in_l3", Org.getProperty("in_l3", ""));
                    //orgnode.setProperty("in_l3",in_lx);

                }
                else
                {
                    string re_in_l2 = text_check(Org.getProperty("in_l2", ""));

                    if (re_in_l2.Contains('('))
                        re_in_l2 = re_in_l2.Split('(')[0];


                    orgnode.setProperty("in_l3", Org.getProperty("in_section_no", "") + Org.getProperty("in_l1", "") + "-" + re_in_l2 + "-" + Org.getProperty("in_l3", ""));
                }


                //in_meeting
                orgnode.setProperty("in_title", in_title);
                orgnode.setProperty("in_address", in_address);
                orgnode.setProperty("in_date_se", time_date_s + "~" + time_date_e);
                orgnode.setProperty("in_lx", in_lx);

                if (!DicOrgs.ContainsKey(OrgName))
                {
                    DicOrgs.Add(OrgName, orgnode);
                }
                else
                {
                    //同個人在一個單位可能被報多次 所以隊職員不處理
                    if (Org.getProperty("in_l1", "") != "隊職員")
                    {
                        string l3 = DicOrgs[OrgName].getProperty("in_l3", "");
                        string l3_group = "";

                        if (Org.getProperty("in_l1", "").Contains("團體"))
                        {

                            string re_in_l2 = text_check(Org.getProperty("in_l2", ""));

                            if (re_in_l2.Contains('('))
                                re_in_l2 = re_in_l2.Split('(')[0];

                            //DicOrgs[OrgName].setProperty("in_l3_group",re_in_l2.Trim('\n'));
                            DicOrgs[OrgName].setProperty("in_l3_group", Org.getProperty("in_section_no", "") + in_lx.Replace("團體組-", ""));
                        }
                        else if (Org.getProperty("in_l1", "").Contains("個人"))
                        {
                            string re_in_l2 = text_check(Org.getProperty("in_l2", ""));

                            if (re_in_l2.Contains('('))
                                re_in_l2 = re_in_l2.Split('(')[0];

                            l3 += "\n" + Org.getProperty("in_section_no", "") + re_in_l2 + "-" + Org.getProperty("in_l3", "");

                            //DicOrgs[OrgName].setProperty("in_l3",l3.Trim('\n'));
                            //l3 += "\n" + in_lx.Replace("個人組-","");
                            DicOrgs[OrgName].setProperty("in_l3", l3.Trim('\n'));
                        }
                        else
                        {
                            string re_in_l2 = text_check(Org.getProperty("in_l2", ""));

                            if (re_in_l2.Contains('('))
                                re_in_l2 = re_in_l2.Split('(')[0];


                            l3 += "\n" + Org.getProperty("in_section_no", "") + Org.getProperty("in_l1", "") + "-" + re_in_l2 + "-" + Org.getProperty("in_l3", "");

                            //DicOrgs[OrgName].setProperty("in_l3",l3.Trim('\n'));
                            //l3 += "\n" + in_lx.Replace("個人組-","");
                            DicOrgs[OrgName].setProperty("in_l3", l3.Trim('\n'));
                        }
                    }
                }
            }


            //CCO.Utilities.WriteDebug("in_playerid", "1");

            foreach (KeyValuePair<string, Item> DicOrg in DicOrgs)
            {
                Item Org = DicOrg.Value;
                //CCO.Utilities.WriteDebug("in_playerid", "Org:" + Org.dom.InnerXml);
                if (!Org.getProperty("in_l1", "").Contains("隊職員"))
                {
                    doc.InsertParagraph();
                    docTables = doc.InsertTable(doc.Tables[0]);

                    string org = Org.getProperty("in_stuff_b1", "") + " " + Org.getProperty("in_short_org", "");

                    //賽事名稱
                    docTables.Rows[1].Cells[0].Paragraphs.First().Append(Org.getProperty("in_title", "")).Font("標楷體").FontSize(14);
                    //選手證
                    docTables.Rows[1].Cells[0].Paragraphs.First().Append("\n" + "選手證").Font("標楷體").FontSize(16).Alignment = Xceed.Document.NET.Alignment.center;
                    //單位(簡稱)
                    docTables.Rows[2].Cells[1].Paragraphs.First().Append(org).Font("標楷體").FontSize(14);
                    //姓名
                    string in_name = Org.getProperty("in_name", "");
                    int fontSize = 14;
                    if (in_name.Length > 9) fontSize = 12;
                    docTables.Rows[3].Cells[1].Paragraphs.First().Append(in_name).Font("標楷體").FontSize(fontSize);
                    /*
                    //取得[教練1]
                    string in_stuff_c1 = Org.getProperty("in_stuff_c1","");
                    string coach = "";

                    if(in_stuff_c1 != "")
                    {
                        coach = in_stuff_c1;
                    }
                    */

                    //沒有第三階就抓第二階
                    string in_lx = "";
                    string in_lx_group = "";

                    string l1_type = "";
                    string in_section_no = Org.getProperty("in_section_no", "");
                    if (Org.getProperty("in_l1", "").Contains("團體"))
                    {
                        l1_type = "in_l3_group";
                    }
                    else
                    {
                        l1_type = "in_l3";
                    }


                    if (Org.getProperty(l1_type, "") == "")
                    {
                        //無第三階 則用 第一+第二階呈現(青少年男子組:限18歲以下 第一級: - 50kg/500)
                        in_lx = Org.getProperty("in_l1", "") + " " + text_check(Org.getProperty("in_l2", ""));//第二階會放金額所以切['/']取前面
                    }
                    else
                    {
                        in_lx_group = Org.getProperty("in_l3_group", "");
                        in_lx = Org.getProperty("in_l3", "");
                    }

                    //2020.08.17(應著109年全國柔道錦標賽的新格式 量級>個人,教練>團體)
                    //x量級(個人)
                    docTables.Rows[4].Cells[1].Paragraphs.First().Append(SectionName(in_lx)).Font("標楷體").FontSize(12);

                    //x教練(團體)
                    docTables.Rows[5].Cells[1].Paragraphs.First().Append(in_lx_group.Replace("團體組-", "")).Font("標楷體").FontSize(12);


                    //CCO.Utilities.WriteDebug("in_playerid", "1111_s:" + Org.getProperty("in_name",""));
                    //有大頭照才切
                    if (Org.getProperty("in_photo", "") != "")
                    {
                        //路徑切出來
                        id_1 = Org.getProperty("in_photo", "").Substring(0, 1);
                        id_2 = Org.getProperty("in_photo", "").Substring(1, 2);
                        id_3 = Org.getProperty("in_photo", "").Substring(3, 29);
                        //CCO.Utilities.WriteDebug("in_playerid", url + id_1 + @"\" + id_2 + @"\" + id_3 + @"\" + Org.getProperty("filename",""));

                        try
                        {
                            Xceed.Document.NET.Image img = doc.AddImage(url + id_1 + @"\" + id_2 + @"\" + id_3 + @"\" + Org.getProperty("filename", ""));//取得路徑圖片
                            Xceed.Document.NET.Picture p = img.CreatePicture(150, 100);

                            docTables.Rows[6].Cells[0].Paragraphs.First().AppendPicture(p);
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                }
            }


            foreach (KeyValuePair<string, Item> DicOrg in DicOrgs)
            {
                Item Org = DicOrg.Value;
                //CCO.Utilities.WriteDebug("in_playerid", "Org:" + Org.dom.InnerXml);
                if (Org.getProperty("in_l1", "").Contains("隊職員"))
                {
                    doc.InsertParagraph();
                    docTables = doc.InsertTable(doc.Tables[0]);

                    string org = Org.getProperty("in_stuff_b1", "") + " " + Org.getProperty("in_short_org", "");

                    //賽事名稱
                    docTables.Rows[1].Cells[0].Paragraphs.First().Append(Org.getProperty("in_title", "")).Font("標楷體").FontSize(16);
                    //選手證
                    docTables.Rows[1].Cells[0].Paragraphs.First().Append("\n" + Org.getProperty("in_l2", "") + "證").Font("標楷體").FontSize(18).Alignment = Xceed.Document.NET.Alignment.center; ;
                    //單位(簡稱)
                    docTables.Rows[2].Cells[1].Paragraphs.First().Append(org).Font("標楷體").FontSize(11);
                    //姓名
                    docTables.Rows[3].Cells[1].Paragraphs.First().Append(Org.getProperty("in_name", "")).Font("標楷體").FontSize(13);


                    //沒有第三階就抓第二階
                    string in_lx = "";
                    if (Org.getProperty("in_l3", "") == "")
                    {
                        in_lx = Org.getProperty("in_l1", "") + " " + text_check(Org.getProperty("in_l2", ""));//第二階會放金額所以切['/']取前面
                    }
                    else
                    {
                        in_lx = Org.getProperty("in_l1", "") + " " + Org.getProperty("in_l3", "");
                    }

                    //量級
                    docTables.Rows[4].Cells[1].Paragraphs.First().Append(in_lx).Font("標楷體").FontSize(10);


                    //CCO.Utilities.WriteDebug("in_playerid", "2222");
                    //有大頭照才切
                    if (Org.getProperty("in_photo", "") != "")
                    {
                        //路徑切出來
                        id_1 = Org.getProperty("in_photo", "").Substring(0, 1);
                        id_2 = Org.getProperty("in_photo", "").Substring(1, 2);
                        id_3 = Org.getProperty("in_photo", "").Substring(3, 29);
                        //CCO.Utilities.WriteDebug("in_playerid", url + id_1 + @"\" + id_2 + @"\" + id_3 + @"\" + Org.getProperty("filename",""));

                        try
                        {
                            Xceed.Document.NET.Image img = doc.AddImage(url + id_1 + @"\" + id_2 + @"\" + id_3 + @"\" + Org.getProperty("filename", ""));//取得路徑圖片
                            Xceed.Document.NET.Picture p = img.CreatePicture(150, 100);
                            docTables.Rows[6].Cells[0].Paragraphs.First().AppendPicture(p);
                        }
                        catch (Exception ex)
                        {

                        }

                    }
                }
            }
            //CCO.Utilities.WriteDebug("in_playerid", "3");
            doc.Tables[0].Remove();
            doc.RemoveParagraphAt(0);
            doc.RemoveParagraphAt(0);



            string xlsName = Meeting.getProperty("in_title", "");
            xlsName = xlsName.Replace("+", "")
                            .Replace("-", "")
                            .Replace(".", "")
                            .Replace(" ", "");
            doc.SaveAs(Export_Path + xlsName + "_選手證(" + type_name + ").docx");
            itmR.setProperty("xls_name", xlsName + "_選手證(" + type_name + ").docx");




            //CCO.Utilities.WriteDebug("in_playerid", "itmR:" + itmR.dom.InnerXml);
            if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

            return itmR;

        }

        private string SectionName(string name)
        {
            string result = "";
            string[] rows = name.Split(new char[] { '\n' });

            foreach (var row in rows)
            {
                if (result != "") result += "\n";

                string[] arr = row.Split(new char[] { '-', '+', '：' });
                for (int i = 0; i < arr.Length - 1; i++)
                {
                    if (arr[i].ToLower().Contains("kg"))
                    {
                        continue;
                    }
                    // if (result.Contains(arr[i]))
                    // {
                    //     continue;
                    // }
                    result += arr[i];
                }
            }
            return result;
        }

        private string text_check(string value)
        {
            //2020.11.03 檢查丟進來的字串 是否包含{-,/} 以便切割
            bool a1 = false;
            bool a2 = false;
            bool a3 = false;

            if (value.Contains('-'))
                a1 = true;

            if (value.Contains('/'))
                a2 = true;

            if (value.Contains('~'))
                a3 = true;


            if (a1 && a2)
            {
                string[] values = value.Split(new char[2] { '-', '/' });
                return values[1];
            }
            else if (a1)
            {
                return value.Split('-')[1];
            }
            else if (a2)
            {
                return value.Split('/')[0];
            }
            else if (a3)
            {
                return value.Split('~')[1];
            }
            else
            {
                return value;
            }

        }
    }
}