﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;
using WorkHelp.ArasDesk.Methods.Rowing.報表;
using System.Reflection;
using System.Security.Policy;
using Spire.Doc;
using System.Collections.Specialized;
using Aras.Server.Core;
using Xceed.Document.NET;

namespace WorkHelp.ArasDesk.Methods.Judo.場地分配
{
    public class In_meeting_allocation3 : Item
    {
        public In_meeting_allocation3(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 場地分配(用時間版本)
                日誌: 
                    - 2024-10-25: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "In_meeting_allocation3";

            var itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                meeting_id = itmR.getProperty("meeting_id", ""),
                scene = itmR.getProperty("scene", ""),
            };

            switch (cfg.scene)
            {
                case "clear-site-sects":
                    ClearSiteSects(cfg, itmR);
                    break;
                case "edit-fight-day":
                    EditFightDay(cfg, itmR);
                    break;
                case "sect-table":
                    SectTable(cfg, itmR);
                    break;
                case "analysis":
                    Analysis(cfg, itmR);
                    break;
                case "allocation":
                    Allocation(cfg, itmR);
                    break;
                case "page":
                    Page(cfg, itmR);
                    break;
                case "fight-minutes":
                    QueryFightMinutes(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void QueryFightMinutes(TConfig cfg, Item itmReturn)
        {
            var sql = @"
                SELECT 
					t1.id
	                , t1.in_name2
	                , t1.in_team_count
					, t1.in_battle_type
	                , t1.in_fight_day
	                , t1.in_fight_time
					, t2.in_event_count_pre
					, t2.in_event_count_total
	                , t1.in_fight_minutes
                FROM 
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
				LEFT JOIN
					AAA_EVENT_COUNT t2 WITH(NOLOCK)
					ON t2.in_type = 'Challenge'
					AND t2.in_team_count = t1.in_team_count
                WHERE
	                t1.in_meeting = '{#meeting_id}'
                ORDER BY
	                t1.in_sort_order
            ";
            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);
            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();

            var head = new StringBuilder();
            var body = new StringBuilder();

            head.AppendLine("<tr>");
            head.AppendLine("    <th class='text-center'>比賽日期</th>");
            head.AppendLine("    <th class='text-center'>組別</th>");
            head.AppendLine("    <th class='text-center'>人數</th>");
            head.AppendLine("    <th class='text-center'>預賽場次</th>");
            head.AppendLine("    <th class='text-center'>每場分鐘</th>");
            head.AppendLine("    <th class='text-center'>預賽時長(分鐘)</th>");
            head.AppendLine("</tr>");

            AppendTable(cfg, head, body, itmReturn);

            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var id = item.getProperty("id", "");
                var in_name2 = item.getProperty("in_name2", "");
                var in_team_count = item.getProperty("in_team_count", "0"); 
                var in_event_count_pre = item.getProperty("in_event_count_pre", "0"); 
                var in_fight_day = item.getProperty("in_fight_day", "");
                var in_fight_time = item.getProperty("in_fight_time", "");

                var words = in_fight_time.Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
                var w1 = words != null && words.Length > 0 ? words[0] : "0";
                var w2 = words != null && words.Length > 1 ? words[1] : "0";
                var fight_time = w1 + (w2 == "30" ? ".5" : "");
                var v1 = GetInt(in_event_count_pre);
                var v2 = GetDcm(fight_time);
                var in_fight_minutes = v1 * v2;

                body.AppendLine("<tr>");
                body.AppendLine("    <td class='text-center'>" + in_fight_day + "</td>");
                body.AppendLine("    <td class='text-center'>" + in_name2 + "</td>");
                body.AppendLine("    <td class='text-center'>" + in_team_count + "</td>");
                body.AppendLine("    <td class='text-center'>" + in_event_count_pre + "</td>");
                body.AppendLine("    <td class='text-center'>" + fight_time + "</td>");
                body.AppendLine("    <td class='text-center'>" + in_fight_minutes + "</td>");
                body.AppendLine("</tr>");
            }
            AppendTable(cfg, head, body, itmReturn);
        }
        
        private void ClearSiteSects(TConfig cfg, Item itmReturn)
        {
            var in_fight_day = itmReturn.getProperty("in_fight_day", "");
            var sql = "UPDATE IN_MEETING_PROGRAM SET in_site_code = NULL WHERE in_meeting = '" + cfg.meeting_id + "' AND in_fight_day = '" + in_fight_day + "'";
            cfg.inn.applySQL(sql);
        }

        private void Allocation(TConfig cfg, Item itmReturn)
        {
            var in_fight_day = itmReturn.getProperty("in_fight_day", "");

            var sql = @"
                SELECT 
	                id
	                , in_fight_day
	                , in_l1
	                , in_l2
	                , in_l3
	                , in_battle_type
	                , in_team_count
	                , in_event_count
	                , in_fight_time
	                , in_fight_minutes
	                , in_site_code
	                , in_site_mat
	                , in_site_mat2
	                , ISNULL(in_site_mat3, in_sort_order) AS 'in_site_mat3'
                FROM 
	                IN_MEETING_PROGRAM WITH(NOLOCK)
                WHERE
	                in_meeting = '{#meeting_id}'
	                AND in_fight_day = '{#in_fight_day}'
                ORDER BY
	                in_sort_order
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_fight_day}", in_fight_day);

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                item.setType("inn_sect");
                itmReturn.addRelationship(item);
            }
        }

        private void EditFightDay(TConfig cfg, Item itmReturn)
        {
            var in_l1 = itmReturn.getProperty("in_l1", "");
            var in_l2 = itmReturn.getProperty("in_l2", "");
            var in_fight_day = itmReturn.getProperty("in_fight_day", "");
            var sql = "UPDATE IN_MEETING_PROGRAM SET in_fight_day = '" + in_fight_day + "' WHERE in_meeting = '" + cfg.meeting_id + "' AND in_l1 = '" + in_l1 + "' AND in_l2 = '" + in_l2 + "'";
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);
            cfg.inn.applySQL(sql);
        }

        private void SectTable(TConfig cfg, Item itmReturn)
        {
            var sql = @"
                SELECT 
	                DISTINCT t1.in_l1, t1.in_l2, t1.in_fight_day, t11.sort_order, t12.sort_order
                FROM 
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
				INNER JOIN
					VU_MEETING_SVY_L1 t11 
					ON t11.source_id = t1.in_meeting
					AND t11.in_value = t1.in_l1
				INNER JOIN
					VU_MEETING_SVY_L2 t12 
					ON t12.source_id = t1.in_meeting
					AND t12.in_filter = t1.in_l1
					AND t12.in_value = t1.in_l2
                WHERE
	                t1.in_meeting = '{#meeting_id}'
	                AND t1.in_l1 <> '格式組'
				ORDER BY
					t11.sort_order
					, t12.sort_order
            ";
            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);
            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();

            var head = new StringBuilder();
            var body = new StringBuilder();

            head.AppendLine("<tr>");
            head.AppendLine("    <th class='text-center'>競賽項目</th>");
            head.AppendLine("    <th class='text-center'>競賽組別</th>");
            head.AppendLine("    <th class='text-center'>比賽日期</th>");
            head.AppendLine("</tr>");

            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                body.AppendLine("<tr>");
                body.AppendLine("    <td class='text-center'>" + item.getProperty("in_l1", "") + "</td>");
                body.AppendLine("    <td class='text-center'>" + item.getProperty("in_l2", "") + "</td>");
                body.AppendLine("    <td class='text-center'>" + GetDayInput(cfg, item) + "</td>");
                body.AppendLine("</tr>");
            }

            AppendTable(cfg, head, body, itmReturn);
        }

        private string GetDayInput(TConfig cfg, Item item)
        {
            return "<input type='text'"
                + " class='text'"
                + " style='text-align: center;'"
                + " value='" + item.getProperty("in_fight_day", "") + "'"
                + " data-l1='" + item.getProperty("in_l1", "") + "'"
                + " data-l2='" + item.getProperty("in_l2", "") + "'"
                + " onkeyup='onInputKeyUp(this)' />";
        }

        private void Analysis(TConfig cfg, Item itmReturn)
        {
            var items = GetDayTotalMinutes(cfg);
            var rows = GetFightDayRows(cfg, items, itmReturn);

            var head = new StringBuilder();
            var body = new StringBuilder();

            head.AppendLine("<tr>");
            head.AppendLine("    <th class='text-center'>比賽日期</th>");
            head.AppendLine("    <th class='text-center'>總分鐘</th>");
            head.AppendLine("    <th class='text-center'>時分</th>");
            head.AppendLine("    <th class='text-center'>場地數</th>");
            head.AppendLine("    <th class='text-center'>每個場地平均</th>");
            head.AppendLine("    <th class='text-center'>平均分鐘</th>");
            head.AppendLine("</tr>");
            for (var i = 0; i < rows.Count; i++)
            {
                var x = rows[i];
                body.AppendLine("<tr>");
                body.AppendLine("    <td class='text-center'>" + x.day + "</td>");
                body.AppendLine("    <td class='text-right'>" + x.total_minutes.ToString("###,##0") + "</td>");
                body.AppendLine("    <td class='text-center'>" + x.display + "</td>");
                body.AppendLine("    <td class='text-center'>" + x.site_count + "</td>");
                body.AppendLine("    <td class='text-center'>" + x.site_display + "</td>");
                body.AppendLine("    <td class='text-center'>" + x.site_minutes + "</td>");
                body.AppendLine("</tr>");
            }

            AppendTable(cfg, head, body, itmReturn);
        }

        private void AppendTable(TConfig cfg, StringBuilder head, StringBuilder body, Item itmReturn)
        {
            var table_name = "data-table";
            var builder = new StringBuilder();
            builder.AppendLine(GetTableAttribute(table_name));
            builder.AppendLine("<thead>");
            builder.Append(head);
            builder.AppendLine("</thead>");
            builder.AppendLine("<tbody>");
            builder.Append(body);
            builder.AppendLine("</tbody>");
            builder.AppendLine("</table>");
            builder.AppendLine("<script>");
            builder.AppendLine("$('#" + table_name + "').bootstrapTable();");
            builder.AppendLine("</script>");

            itmReturn.setProperty("inn_table", builder.ToString());
        }

        private string GetTableAttribute(string table_name)
        {
            return "<table id='" + table_name + "'"
                + " class='table table-hover table-striped'"
                + " data-toggle='table'"
                + " data-show-toggle='false'"
                + " data-show-columns='false'"
                + " data-search-align='right'"
                + " data-sort-stable='false'"
                + " data-search='false'"
                + ">";
        }

        private List<TFightDay> GetFightDayRows(TConfig cfg, Item items, Item itmReturn)
        {
            var rows = new List<TFightDay>();
            var site_count = GetInt(itmReturn.getProperty("site_count", "1"));
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var in_fight_day = item.getProperty("in_fight_day", "");
                if (in_fight_day == "") continue;
                var x = new TFightDay
                {
                    day = in_fight_day,
                    total_minutes = GetInt(item.getProperty("total_minutes", "0")),
                    site_count = site_count,
                };
                var h = x.total_minutes / 60;
                var m = x.total_minutes % 60;
                x.display = h.ToString().PadLeft(2, '0') + ":" + m.ToString().PadLeft(2, '0');

                var a = x.total_minutes / site_count;
                var b = x.total_minutes % site_count;
                if (b > 0) a = a + 1;

                x.site_minutes = a;

                var h2 = a / 60;
                var m2 = a % 60;
                if (m2 > 0)
                {
                    x.site_display = "約 " + h2 + " 個小時又 " + m2 + " 分鐘";
                }
                else
                {
                    x.site_display = "約 " + h2 + " 個小時";
                }

                rows.Add(x);
            }
            return rows;
        }

        private class TFightDay
        {
            public string day { get; set; }
            public int total_minutes { get; set; }
            public string display { get; set; }
            public int site_count { get; set; }
            public int site_minutes { get; set; }
            public string site_display { get; set; }
        }

        private Item GetDayTotalMinutes(TConfig cfg)
        {
            var sql = @"
                SELECT 
	                in_fight_day
	                , SUM(in_fight_minutes) AS 'total_minutes'
                FROM 
	                IN_MEETING_PROGRAM WITH(NOLOCK)
                WHERE
	                in_meeting = '{#meeting_id}'
	                AND in_l1 <> '格式組'
                GROUP BY
	                in_fight_day
                ORDER BY
	                in_fight_day
            ";
            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);
            return cfg.inn.applySQL(sql);
        }

        private void Page(TConfig cfg, Item itmReturn)
        {
            AppendMeetingInfo(cfg);
            var fight_days = GetMeetingVariable(cfg, "fight_days");
            var fight_site_count = GetMeetingVariable(cfg, "fight_site_count");

            itmReturn.setProperty("in_title", cfg.mt_title);
            itmReturn.setProperty("fight_site_count", fight_site_count);

            AppendDayMenu(cfg, fight_days, itmReturn);

        }

        private string GetMeetingVariable(TConfig cfg, string in_key)
        {
            var sql = "SELECT in_key, in_value FROM IN_MEETING_VARIABLE WITH(NOLOCK) WHERE source_id = '" + cfg.meeting_id + "'"
                + " AND in_key = '" + in_key + "'";
            var itmResult = cfg.inn.applySQL(sql);
            if (itmResult.isError() || itmResult.getResult() == "") return "";
            return itmResult.getProperty("in_value", "");
        }

        //賽事資訊
        private void AppendMeetingInfo(TConfig cfg)
        {
            if (cfg.meeting_id == "") return;

            cfg.itmMeeting = cfg.inn.applySQL("SELECT in_title, in_weight_mode FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            if (cfg.itmMeeting.isError() || cfg.itmMeeting.getResult() == "")
            {
                throw new Exception("查無 賽事資料");
            }
            cfg.mt_title = cfg.itmMeeting.getProperty("in_title", "");
        }

        private void AppendDayMenu(TConfig cfg, string fight_days, Item itmReturn)
        {
            var itmEmpty = cfg.inn.newItem();
            itmEmpty.setType("inn_day");
            itmEmpty.setProperty("value", "");
            itmEmpty.setProperty("text", "請選擇");
            itmReturn.addRelationship(itmEmpty);

            if (fight_days == "") return;

            var days = fight_days.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            for (var i = 0; i < days.Length; i++)
            {
                var day = days[i];
                var itmDay = cfg.inn.newItem();
                itmDay.setType("inn_day");
                itmDay.setProperty("value", day);
                itmDay.setProperty("text", day);
                itmReturn.addRelationship(itmDay);
            }

        }

        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string meeting_id { get; set; }
            public string scene { get; set; }

            public Item itmMeeting { get; set; }
            public string mt_title { get; set; }
        }

        private decimal GetDcm(string value, int defV = 0)
        {
            if (value == "") return 0;
            decimal result = defV;
            if (decimal.TryParse(value, out result)) return result;
            return defV;
        }

        private int GetInt(string value, int defV = 0)
        {
            if (value == "") return 0;
            int result = defV;
            if (int.TryParse(value, out result)) return result;
            return defV;
        }
    }
}