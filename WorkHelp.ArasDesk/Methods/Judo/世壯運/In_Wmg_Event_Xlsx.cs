﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;
using WorkHelp.ArasDesk.Methods.Rowing.報表;
using System.Reflection;

namespace WorkHelp.ArasDesk.Methods.Judo.世壯運
{
    public class In_Wmg_Event_Xlsx : Item
    {
        public In_Wmg_Event_Xlsx(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: World Master Games 場次匯出
                日誌: 
                    - 2024-09-30: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "In_Wmg_Event_Xlsx";

            var itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                meeting_id = itmR.getProperty("meeting_id", ""),
                program_id = itmR.getProperty("program_id", ""),
                event_id = itmR.getProperty("event_id", ""),
                in_fight_day = itmR.getProperty("in_fight_day", ""),
                scene = itmR.getProperty("scene", ""),
            };

            cfg.head_color = System.Drawing.Color.FromArgb(232, 232, 234);
            cfg.blue_color = System.Drawing.Color.FromArgb(192, 230, 245);

            switch (cfg.scene)
            {
                case "export_one_event":
                    cfg.is_one_event = true;
                    ExportDayEvents(cfg, itmR);
                    break;
                case "export_day_events":
                    cfg.is_one_event = false;
                    ExportDayEvents(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void ExportDayEvents(TConfig cfg, Item itmReturn)
        {
            AppendMeetingInfo(cfg);
            AppendExportInfo(cfg);

            var items = GetEventItems(cfg);
            var count = items.getItemCount();
            if (count <= 0) throw new Exception("查無場次資料");

            var book = new Spire.Xls.Workbook();

            var sheet = book.CreateEmptySheet();
            sheet.Name = cfg.is_one_event ? "場次" : "當日場次";

            ////將所有欄放入單一頁面
            //sheet.PageSetup.FitToPagesWide = 1;
            //sheet.PageSetup.FitToPagesTall = 0;

            //sheet.PageSetup.TopMargin = 0.4;
            //sheet.PageSetup.LeftMargin = 0.3;
            //sheet.PageSetup.RightMargin = 0.3;
            //sheet.PageSetup.BottomMargin = 0.6;

            var ri = 1;
            SetStrCell(cfg, sheet, "A" + ri, "日期", 12, 0, true, false, HA.C, CT.Head);
            SetStrCell(cfg, sheet, "B" + ri, "場地", 12, 0, true, false, HA.C, CT.Head);
            SetStrCell(cfg, sheet, "C" + ri, "場次", 12, 0, true, false, HA.C, CT.Head);
            SetStrCell(cfg, sheet, "D" + ri, "級組", 12, 0, true, false, HA.C, CT.Head);

            SetStrCell(cfg, sheet, "E" + ri, "白方單位", 12, 0, true, false, HA.C, CT.Head);
            SetStrCell(cfg, sheet, "F" + ri, "白方選手", 12, 0, true, false, HA.C, CT.Head);
            SetStrCell(cfg, sheet, "G" + ri, "白方指導", 12, 0, true, false, HA.C, CT.Head);
            SetStrCell(cfg, sheet, "H" + ri, "白方得分", 12, 0, true, false, HA.C, CT.Head);

            SetStrCell(cfg, sheet, "I" + ri, "VS", 12, 0, true, false, HA.C, CT.Head);

            SetStrCell(cfg, sheet, "J" + ri, "藍方得分", 12, 0, true, false, HA.C, CT.Head);
            SetStrCell(cfg, sheet, "K" + ri, "藍方指導", 12, 0, true, false, HA.C, CT.Head);
            SetStrCell(cfg, sheet, "L" + ri, "藍方選手", 12, 0, true, false, HA.C, CT.Head);
            SetStrCell(cfg, sheet, "M" + ri, "藍方單位", 12, 0, true, false, HA.C, CT.Head);

            SetStrCell(cfg, sheet, "N" + ri, "勝方", 12, 0, true, false, HA.C, CT.Head);
            SetStrCell(cfg, sheet, "O" + ri, "時間", 12, 0, true, false, HA.C, CT.Head);
            ri++;

            var first = default(TEvt);
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var evt = MapEvent(cfg, item);
                var f1 = MapFoot(cfg, item, "foot1");
                var f2 = MapFoot(cfg, item, "foot2");
                var winner = f1.win ? "白方" : (f2.win ? "藍方" : "");

                if (i == 0) first = evt;

                SetStrCell(cfg, sheet, "A" + ri, evt.in_fight_day, 12, 0, false, false, HA.C, CT.None);
                SetStrCell(cfg, sheet, "B" + ri, evt.site_name, 12, 0, false, false, HA.C, CT.None);
                SetStrCell(cfg, sheet, "C" + ri, evt.in_tree_no, 12, 0, false, false, HA.C, CT.None);
                SetStrCell(cfg, sheet, "D" + ri, evt.sect_name, 12, 0, false, false, HA.C, CT.None);

                SetStrCell(cfg, sheet, "E" + ri, f1.org, 12, 0, false, false, HA.C, CT.None);
                SetStrCell(cfg, sheet, "F" + ri, f1.name, 12, 0, false, false, HA.C, CT.None);
                SetStrCell(cfg, sheet, "G" + ri, f1.shido, 12, 0, false, false, HA.C, CT.None);
                SetStrCell(cfg, sheet, "H" + ri, f1.points, 12, 0, false, false, HA.C, CT.None);

                SetStrCell(cfg, sheet, "I" + ri, "vs", 12, 0, false, false, HA.C, CT.None);

                SetStrCell(cfg, sheet, "J" + ri, f2.points, 12, 0, false, false, HA.C, CT.None);
                SetStrCell(cfg, sheet, "K" + ri, f2.shido, 12, 0, false, false, HA.C, CT.None);
                SetStrCell(cfg, sheet, "L" + ri, f2.name, 12, 0, false, false, HA.C, CT.None);
                SetStrCell(cfg, sheet, "M" + ri, f2.org, 12, 0, false, false, HA.C, CT.None);

                SetStrCell(cfg, sheet, "N" + ri, winner, 12, 0, false, false, HA.C, CT.None);
                SetStrCell(cfg, sheet, "O" + ri, item.getProperty("in_win_local_time", ""), 12, 0, false, false, HA.C, CT.None);
                ri++;
            }

            SetRangeBorder(sheet, "A1:O" + (ri - 1));

            sheet.Columns[0].ColumnWidth = 12;
            sheet.Columns[1].ColumnWidth = 13;
            sheet.Columns[2].ColumnWidth = 7;
            sheet.Columns[3].ColumnWidth = 36;

            sheet.Columns[4].ColumnWidth = 16;
            sheet.Columns[5].ColumnWidth = 16;
            sheet.Columns[6].ColumnWidth = 9;
            sheet.Columns[7].ColumnWidth = 9;

            sheet.Columns[8].ColumnWidth = 6;

            sheet.Columns[9].ColumnWidth = 9;
            sheet.Columns[10].ColumnWidth = 9;
            sheet.Columns[11].ColumnWidth = 16;
            sheet.Columns[12].ColumnWidth = 16;

            sheet.Columns[13].ColumnWidth = 8;
            sheet.Columns[14].ColumnWidth = 10;

            var book_title = cfg.in_fight_day.Replace("-", "");
            if (cfg.is_one_event)
            {
                book_title += "-" + first.event_no;//20240901-A1
            }

            AppendExportResult(cfg, book, book_title, itmReturn);
        }

        private void SetStrCell(TConfig cfg, Spire.Xls.Worksheet sheet
            , string pos
            , string text
            , int size
            , int height = 0
            , bool needBold = false
            , bool needMerge = false
            , HA ha = HA.C
            , CT color = CT.None)
        {
            var range = sheet.Range[pos];
            if (needMerge) range.Merge();
            range.Text = text;
            SetUtlCell(cfg, range, size, height, needBold, ha, color);
        }

        private void SetUtlCell(TConfig cfg, Spire.Xls.CellRange range
            , int size
            , int height
            , bool needBold
            , HA ha = HA.C
            , CT color = CT.None)
        {
            range.Style.Font.FontName = cfg.font_name;
            range.Style.Font.Size = size;

            if (needBold) range.Style.Font.IsBold = true;
            if (height > 0) range.RowHeight = height;

            switch (color)
            {
                case CT.Head: range.Style.Color = cfg.head_color; break;
                case CT.Blue: range.Style.Color = cfg.blue_color; break;
            }

            range.VerticalAlignment = Spire.Xls.VerticalAlignType.Center;

            switch (ha)
            {
                case HA.C:
                    range.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                    break;
                case HA.L:
                    range.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Left;
                    break;
                case HA.R:
                    range.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Right;
                    break;
            }
        }

        private enum HA
        {
            L = 100,
            C = 200,
            R = 300
        }

        private enum CT
        {
            None = 1,
            Head = 100,
            Blue = 200,
        }

        //設定格線
        private void SetRangeBorder(Spire.Xls.Worksheet sheet, string pos)
        {
            var range = sheet.Range[pos];
            range.BorderInside(Spire.Xls.LineStyleType.Thin, System.Drawing.Color.Black);
            range.BorderAround(Spire.Xls.LineStyleType.Thin, System.Drawing.Color.Black);
        }

        private void AppendExportResult(TConfig cfg, Spire.Xls.Workbook book, string book_title, Item itmReturn)
        {
            if (book.Worksheets.Count > 1) book.Worksheets[0].Remove();
            if (book.Worksheets.Count > 1) book.Worksheets[0].Remove();
            if (book.Worksheets.Count > 1) book.Worksheets[0].Remove();

            //匯出檔名
            var xlsName = cfg.mt_title
                + "_" + book_title
                + "_" + System.Guid.NewGuid().ToString().Substring(0, 4).ToUpper()
                + "_" + System.DateTime.Now.ToString("yyyyMMddHHmmss");

            xlsName = xlsName.Replace("+", "")
                            .Replace("-", "")
                            .Replace(".", "")
                            .Replace(" ", "");

            string extName = cfg.export_type != "" && cfg.export_type.ToLower() == "pdf" ? ".pdf" : ".xlsx";
            string xls_file = cfg.export_path + xlsName + extName;
            string xls_name = xlsName + extName;

            if (!System.IO.Directory.Exists(cfg.export_path))
            {
                System.IO.Directory.CreateDirectory(cfg.export_path);
            }

            //儲存檔案          
            if (extName == ".pdf")
            {
                book.SaveToFile(xls_file, Spire.Xls.FileFormat.PDF);
            }
            else
            {
                book.SaveToFile(xls_file, Spire.Xls.ExcelVersion.Version2013);
            }

            //下載路徑
            itmReturn.setProperty("xls_name", xls_name);
        }

        //賽事資訊
        private void AppendMeetingInfo(TConfig cfg)
        {
            if (cfg.meeting_id == "") return;

            cfg.itmMeeting = cfg.inn.applySQL("SELECT in_title, in_weight_mode FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'");
            if (cfg.itmMeeting.isError() || cfg.itmMeeting.getResult() == "")
            {
                throw new Exception("查無 賽事資料");
            }
            cfg.mt_title = cfg.itmMeeting.getProperty("in_title", "");
        }

        private void AppendExportInfo(TConfig cfg)
        {
            var xls_parm_name = "";
            cfg.itmXlsx = cfg.inn.applyMethod("In_Variable_File", "<in_name>" + xls_parm_name + "</in_name>");
            cfg.template_path = cfg.itmXlsx.getProperty("template_path", "");
            cfg.export_path = cfg.itmXlsx.getProperty("export_path", "");
            cfg.export_type = "xlsx";
            cfg.font_name = "Source Sans Pro";
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string meeting_id { get; set; }
            public string program_id { get; set; }
            public string event_id { get; set; }
            public string in_fight_day { get; set; }
            public string scene { get; set; }

            public Item itmMeeting { get; set; }
            public string mt_title { get; set; }

            public Item itmXlsx { get; set; }
            public string template_path { get; set; }
            public string export_path { get; set; }
            public string export_type { get; set; }
            public string font_name { get; set; }

            public System.Drawing.Color head_color { get; set; }
            public System.Drawing.Color blue_color { get; set; }

            public bool is_one_event { get; set; }
        }

        private class TEvt 
        {
            public string in_fight_day { get; set; }
            public string site_name { get; set; }
            public string site_code { get; set; }
            public string in_tree_no { get; set; }
            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }
            public string in_sub_sect { get; set; }

            public bool is_team { get; set; }
            public bool is_sub { get; set; }

            public string event_no { get; set; }
            public string sect_name { get; set; }
        }

        private class TFoot
        {
            public string org { get; set; }
            public string team { get; set; }
            public string name { get; set; }
            public string no { get; set; }
            public string points { get; set; }
            public string shido { get; set; }
            public string status { get; set; }
            public bool win { get; set; }
        }

        private TEvt MapEvent(TConfig cfg, Item item)
        {
            var x = new TEvt
            {
                in_fight_day = item.getProperty("pg_fight_day", ""),
                site_name = item.getProperty("site_name", ""),
                site_code = item.getProperty("site_code", ""),
                in_tree_no = item.getProperty("in_tree_no", ""),
                in_l1 = item.getProperty("in_l1", ""),
                in_l2 = item.getProperty("in_l2", ""),
                in_l3 = item.getProperty("in_l3", ""),
                in_sub_sect = item.getProperty("in_sub_sect", ""),
            };

            x.event_no = GetEventNumber(cfg, x.site_code, x.in_tree_no);
            x.is_team = x.in_l1 == "團體組";
            x.is_sub = x.in_sub_sect != "";

            var sect_name = x.in_l2.Replace("個-", "") + x.in_l3;
            if (x.is_team)
            {
                if (x.in_l2 == x.in_l3)
                {
                    sect_name = x.in_l2.Replace("團-", "").Replace("賽制-", "賽");
                }
                else
                {
                    sect_name = x.in_l2.Replace("團-", "").Replace("賽制-", "賽") + "-" + x.in_l3;
                }
                if (x.is_sub)
                {
                    sect_name += "-" + x.in_sub_sect;
                }
            }
            x.sect_name = sect_name;

            return x;
        }

        private TFoot MapFoot(TConfig cfg, Item item, string prefix)
        {
            var x = new TFoot
            {
                org = item.getProperty(prefix + "_" + "org", ""),
                name = item.getProperty(prefix + "_" + "name", ""),
                team = item.getProperty(prefix + "_" + "team", ""),
                no = item.getProperty(prefix + "_" + "judo_no", ""),
                points = item.getProperty(prefix + "_" + "points", ""),
                shido = item.getProperty(prefix + "_" + "correct", ""),
                status = item.getProperty(prefix + "_" + "status", ""),
            };

            if (x.points == "") x.points = "0";

            if (x.shido == "") x.shido = "0";
            if (x.shido != "") x.shido = "S" + x.shido;

            if (x.status == "1") x.win = true;

            var in_win_time = item.getProperty("in_win_time", "");
            if (in_win_time == "")
            {
                x.points = "";
                x.shido = "";
                x.win = false;
            }

            return x;
        }

        private string GetEventNumber(TConfig cfg, string code, string number)
        {
            switch (code)
            {
                case "1": return "A" + number;
                case "2": return "B" + number;
                case "3": return "C" + number;
                case "4": return "D" + number;
                case "5": return "E" + number;
                case "6": return "F" + number;
                case "7": return "G" + number;
                case "8": return "H" + number;
                case "9": return "I" + number;
                case "10": return "J" + number;
                default: return code + "-" + number;
            }
        }

        private Item GetEventItems(TConfig cfg)
        {
            if (cfg.event_id == "")
            {
                return GetDayEventItems(cfg);
            }
            else
            {
                return GetOneEventItem(cfg);
            }
        }

        private Item GetOneEventItem(TConfig cfg)
        {
            var sql = @"
                SELECT 
	                *
                FROM 
	                VU_FightTreeEventDetails 
                WHERE 
	                event_id = '{#event_id}'
	                AND	foot1_foot = 1
            ";

            sql = sql.Replace("{#event_id}", cfg.event_id);

            return cfg.inn.applySQL(sql);
        }

        private Item GetDayEventItems(TConfig cfg)
        {
            var sql = @"
                SELECT 
	                * 
                FROM 
	                VU_FightTreeEventDetails 
                WHERE
	                in_meeting = '{#meeting_id}'
	                AND pg_fight_day = '{#in_fight_day}'
	                AND foot1_foot = 1
                    AND in_win_status NOT IN ('cancel', 'bypass')
                ORDER BY
	                site_code
	                , in_tree_no
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_fight_day}", cfg.in_fight_day);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }
    }
}