﻿using Aras.IOM;
using System;
using System.Collections.Generic;
using System.Text;

namespace WorkHelp.ArasDesk.Methods.Judo.世壯運
{
    internal class In_Wmg_Register_Merge : Item
    {
        public In_Wmg_Register_Merge(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 世壯運併級
                日誌: 
                    - 2024-08-19: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "In_Wmg_Register_Merge";

            var itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                meeting_id = itmR.getProperty("meeting_id", ""),
                scene = itmR.getProperty("scene", ""),
            };

            switch (cfg.scene)
            {
                case "page":
                    Page(cfg, itmR);
                    break;
                case "sect-update":
                    SectUpdate(cfg, itmR);
                    break;
                case "weight-update":
                    WeightUpdate(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void WeightUpdate(TConfig cfg, Item itmReturn)
        {
            var user_id = cfg.inn.getUserID();
            var itmCurrentUser = cfg.inn.applySQL("SELECT id, last_name FROM [USER] WITH(NOLOCK) WHERE id = ' " + user_id + "'");

            var muid = itmReturn.getProperty("id", "");

            var model = MapSurveyOption(cfg, muid);
            model.in_weight_createid = user_id;
            model.in_weight_createname = itmCurrentUser.getProperty("last_name", "");
            model.in_weight_time = System.DateTime.Now.AddHours(-8).ToString("yyyy/MM/dd HH:mm:ss");
            model.in_weight = itmReturn.getProperty("in_weight", "0");
            model.new_weight = GetDcmVal(model.in_weight);

            if (model.new_weight <= 0)
            {
                RollbackWeight(cfg, model, itmReturn);
                return;
            }

            if (model.has_ranges)
            {
                if (model.new_weight >= model.min && model.new_weight <= model.max)
                {
                    model.in_weight_result = "1";
                    model.in_weight_result = "on";
                }
                else
                {
                    model.in_weight_result = "0";
                    model.in_weight_result = "dq";
                }
            }
            else
            {
                model.in_weight_result = "1";
                model.in_weight_result = "on";
            }

            EditWeight(cfg, model, itmReturn);
        }

        //更新體重
        private void EditWeight(TConfig cfg, TSvyOpt model, Item itmReturn)
        {
            var sql = @"UPDATE IN_MEETING_USER SET"
                + "  in_weight = " + model.in_weight
                + ", in_weight_createid = '" + model.in_weight_createid + "'"
                + ", in_weight_createname = '" + model.in_weight_createname + "'"
                + ", in_weight_time = '" + model.in_weight_time + "'"
                + ", in_weight_result = '" + model.in_weight_result + "'"
                + ", in_weight_status = '" + model.in_weight_status + "'"
                + " WHERE id = '" + model.id + "'";

            cfg.inn.applySQL(sql);

            itmReturn.setProperty("weight_result", "rollback");
        }

        //復歸體重
        private void RollbackWeight(TConfig cfg, TSvyOpt model, Item itmReturn)
        {
            var sql = "UPDATE IN_MEETING_USER SET"
                + "  in_weight = NULL"
                + ", in_weight_createid = NULL"
                + ", in_weight_createname = NULL"
                + ", in_weight_time = NULL"
                + ", in_weight_result = NULL"
                + ", in_weight_status = NULL"
                + " WHERE id = '" + model.id + "'";

            cfg.inn.applySQL(sql);

            itmReturn.setProperty("weight_result", "rollback");
        }

        private void SectUpdate(TConfig cfg, Item itmReturn)
        {
            var value = itmReturn.getProperty("value", "");
            var list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<TRow>>(value);
            if (list == null || list.Count == 0)
            {
                throw new Exception("資料不可為空值");
            }

            for (var i = 0; i < list.Count; i++)
            {
                var x = list[i];
                var itmData = cfg.inn.newItem();
                itmData.setType("In_Meeting_User");
                itmData.setProperty("meeting_id", cfg.meeting_id);
                itmData.setProperty("muid", x.id);
                itmData.setProperty("in_l1", x.in_l1);
                itmData.setProperty("in_l2", x.in_l2);
                itmData.setProperty("in_l3", x.in_l3);
                itmData.setProperty("in_team", "");
                itmData.setProperty("scene", "change_weight");
                itmData.apply("In_Meeting_User_Merge");
            }
        }

        private void Page(TConfig cfg, Item itmReturn)
        {
            cfg.MAgeList = new List<TAge>();
            cfg.FAgeList = new List<TAge>();
            var contents = new StringBuilder();
            AppendWeightAgeTable(cfg, cfg.MAgeList, "男", "M", contents);
            AppendWeightAgeTable(cfg, cfg.FAgeList, "女", "F", contents);
            itmReturn.setProperty("inn_tables", contents.ToString());
        }

        private void AppendWeightAgeTable(TConfig cfg, List<TAge> ageList, string in_gender, string gcode, StringBuilder builder)
        {
            var weights = GetWeights(cfg, in_gender, gcode);
            var items = GetSoloGenderItems(cfg, in_gender);
            AppendSectMUserItem(cfg, weights, items, in_gender, gcode);

            var w1 = weights[0];
            ageList.Add(w1.A1);
            ageList.Add(w1.A2);
            ageList.Add(w1.A3);
            ageList.Add(w1.A4);
            ageList.Add(w1.A5);
            ageList.Add(w1.A6);
            ageList.Add(w1.A7);
            ageList.Add(w1.A8);
            ageList.Add(w1.A9);

            var head = new StringBuilder();
            var body = new StringBuilder();

            var headCss = "text-center bg-primary";
            head.Append(@"<th class='" + headCss + "'>量級＼分組</th>");
            for (int i = 0; i < ageList.Count; i++)
            {
                var age = ageList[i];
                head.Append("<th class='" + headCss + "'>" + age.in_l2.Replace("(", "<br>(") + "</th>");
            }

            for (var i = 0; i < weights.Count; i++)
            {
                var w = weights[i];
                body.Append("<tr>");
                body.Append("<td class='text-center'>" + w.in_l3 + "</td>");
                body.Append(GetWeightAgeTdContent(cfg, w, w.A1));
                body.Append(GetWeightAgeTdContent(cfg, w, w.A2));
                body.Append(GetWeightAgeTdContent(cfg, w, w.A3));
                body.Append(GetWeightAgeTdContent(cfg, w, w.A4));
                body.Append(GetWeightAgeTdContent(cfg, w, w.A5));
                body.Append(GetWeightAgeTdContent(cfg, w, w.A6));
                body.Append(GetWeightAgeTdContent(cfg, w, w.A7));
                body.Append(GetWeightAgeTdContent(cfg, w, w.A8));
                body.Append(GetWeightAgeTdContent(cfg, w, w.A9));
                body.Append("</tr>");
            }

            var id = "data-table-" + gcode;
            builder.Append("<div>");
            builder.Append("<h3>個人組-" + in_gender + "子組</h3>");
            builder.Append(GetTableAttribute(id));
            builder.Append("<thead>");
            builder.Append("  <tr>");
            builder.Append(head);
            builder.Append("  </tr>");
            builder.Append("</thead>");

            builder.Append("<tbody>");
            builder.Append(body);
            builder.Append("</tbody>");

            builder.Append("</table>");
            builder.Append("</div>");
        }

        private string GetWeightAgeTdContent(TConfig cfg, TWeight weight, TAge age)
        {
            var content = GetWeightAgeTdPlayers(cfg, weight, age);
            return "<td class='text-center inno-drag-td' data-l1='" + weight.in_l1 + "' data-l2='" + age.in_l2 + "' data-l3='" + weight.in_l3 + "'>" + content + "</td>";
        }

        private string GetWeightAgeTdPlayers(TConfig cfg, TWeight weight, TAge age)
        {
            if (age == null || age.rows == null || age.rows.Count == 0) return "";

            var css = "inno-wmg-td";
            var contents = new StringBuilder();
            for (var i = 0; i < age.rows.Count; i++)
            {
                var x = age.rows[i];
                var dq = x.in_weight_status != "" && x.in_weight_status != "on" ? "bg-red" : "";
                var wtext = x.in_weight == "" ? "未磅" : x.in_weight;
                var wspan = "<span class='inno-user-weight' data-id='" + x.id + "'>" + wtext + "</span>";
                contents.Append("<div class='inno-user' data-id='" + x.id + "' data-name='" + x.in_name + "' data-l2='" + x.in_l2 + "' data-l3='" + x.in_l3 + "'>");
                contents.Append("<table class='table inno-wmg-table " + dq + "'>");
                contents.Append("<tr>");
                contents.Append("  <td colspan='2' class='" + css + "'>" + x.in_name + "</td>");
                contents.Append("</tr>");

                contents.Append("<tr>");
                contents.Append("  <td colspan='1' class='" + css + "'>" + x.in_age + "</td>");
                contents.Append("  <td colspan='1' class='" + css + "'>" + wspan + "</td>");
                contents.Append("</tr>");

                contents.Append("<tr>");
                contents.Append("  <td colspan='1' class='" + css + " bg-gray'>" + x.in_reg_l2 + "</td>");
                contents.Append("  <td colspan='1' class='" + css + "'>" + x.in_reg_l3 + "</td>");
                contents.Append("</tr>");

                contents.Append("</table>");
                contents.Append("</div>");
            }
            return contents.ToString();
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string meeting_id { get; set; }
            public string scene { get; set; }
            public List<TAge> MAgeList { get; set; }
            public List<TAge> FAgeList { get; set; }
        }

        private class TWeight
        {
            public string in_l1 { get; set; }
            public string in_l3 { get; set; }
            public TAge A1 { get; set; }
            public TAge A2 { get; set; }
            public TAge A3 { get; set; }
            public TAge A4 { get; set; }
            public TAge A5 { get; set; }
            public TAge A6 { get; set; }
            public TAge A7 { get; set; }
            public TAge A8 { get; set; }
            public TAge A9 { get; set; }
        }

        private class TAge
        {
            public string gcode { get; set; }
            public string acode { get; set; }
            public string code { get; set; }
            public string in_l2 { get; set; }
            public List<TRow> rows { get; set; }
        }

        private class TRow
        {
            public string id { get; set; }
            public string in_name { get; set; }
            public string in_gender { get; set; }
            public string in_sno { get; set; }
            public string reg_birth { get; set; }
            public string in_age { get; set; }
            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }
            public string in_index { get; set; }
            public string in_current_org { get; set; }
            public string in_reg_l2 { get; set; }
            public string in_reg_l3 { get; set; }
            public string in_weight { get; set; }
            public string in_weight_status { get; set; }
            public string in_weight_result { get; set; }

            public string old_l2 { get; set; }
            public string old_l3 { get; set; }
        }


        private string GetTableAttribute(string table_name)
        {
            return "<table id='" + table_name + "'"
                + " class='table table-hover table-bordered table-rwd rwd rwdtable'"
                + " data-toggle='table'"
                + " data-show-toggle='false'"
                + " data-show-columns='false'"
                + " data-search-align='left'"
                + " data-sort-stable='true'"
                + " data-search='false'"
                + ">";
        }

        private void AppendSectMUserItem(TConfig cfg, List<TWeight> weights, Item items, string in_gender, string gcode)
        {
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var in_l1 = item.getProperty("in_l1", "");
                var in_l2 = item.getProperty("in_l2", "").ToUpper();
                var in_l3 = item.getProperty("in_l3", "").ToLower();
                var weight = weights.Find(x => x.in_l3 == in_l3);
                var age = default(TAge);
                var code = GetOnlyCode(in_l2, in_gender, gcode);
                switch (code)
                {
                    case "1": age = weight.A1; break;
                    case "2": age = weight.A2; break;
                    case "3": age = weight.A3; break;
                    case "4": age = weight.A4; break;
                    case "5": age = weight.A5; break;
                    case "6": age = weight.A6; break;
                    case "7": age = weight.A7; break;
                    case "8": age = weight.A8; break;
                    case "9": age = weight.A9; break;
                }
                if (age != null)
                {
                    age.rows.Add(NewMUserRow(cfg, item));
                }
            }
        }

        private TRow NewMUserRow(TConfig cfg, Item item)
        {
            var result = new TRow
            {
                id = item.getProperty("id", ""),
                in_name = item.getProperty("in_name", ""),
                in_sno = item.getProperty("in_sno", ""),
                in_gender = item.getProperty("in_gender", ""),
                reg_birth = item.getProperty("reg_birth", ""),
                in_age = item.getProperty("in_age", ""),
                in_l1 = item.getProperty("in_l1", ""),
                in_l2 = item.getProperty("in_l2", ""),
                in_l3 = item.getProperty("in_l3", ""),
                in_index = item.getProperty("in_index", ""),
                in_current_org = item.getProperty("in_current_org", ""),
                in_reg_l2 = item.getProperty("in_reg_l2", ""),
                in_reg_l3 = item.getProperty("in_reg_l3", ""),
                in_weight = item.getProperty("in_weight", ""),
                in_weight_status = item.getProperty("in_weight_status", ""),
                in_weight_result = item.getProperty("in_weight_result", ""),
            };

            if (result.in_name.Contains("書弘"))
            {
                result.in_name = "書弘·拉告";
            }
            else
            {
                if (result.in_name.Length > 2)
                {
                    result.in_name = result.in_name.Substring(0, 3);
                }
            }
            return result;
        }

        private string GetOnlyCode(string in_l2, string in_gender, string gcode)
        {
            var arr = in_l2.Split(new char[] { '(' }, StringSplitOptions.RemoveEmptyEntries);
            var word = in_gender + "子組" + gcode;
            var code = arr[0].Replace(word, "");
            return code;
        }

        private List<TWeight> GetWeights(TConfig cfg, string in_gender, string gcode)
        {
            var list = new List<TWeight>();
            var items = GetSectGenderItems(cfg, in_gender);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var in_l1 = item.getProperty("in_grand_filter", "");
                var in_l2 = item.getProperty("in_filter", "").ToUpper();
                var in_l3 = item.getProperty("in_value", "").ToLower();
                var existed = list.Find(x => x.in_l3 == in_l3);
                if (existed == null)
                {
                    existed = NewWeight(cfg, in_l1, in_l3, gcode);
                    list.Add(existed);
                }
                var age = default(TAge);
                var code = GetOnlyCode(in_l2, in_gender, gcode);
                switch (code)
                {
                    case "1": age = existed.A1; break;
                    case "2": age = existed.A2; break;
                    case "3": age = existed.A3; break;
                    case "4": age = existed.A4; break;
                    case "5": age = existed.A5; break;
                    case "6": age = existed.A6; break;
                    case "7": age = existed.A7; break;
                    case "8": age = existed.A8; break;
                    case "9": age = existed.A9; break;
                }
                if (age != null)
                {
                    age.in_l2 = in_l2;
                }
            }
            return list;
        }

        private TWeight NewWeight(TConfig cfg, string in_l1, string in_l3, string gcode)
        {
            return new TWeight
            {
                in_l1 = in_l1,
                in_l3 = in_l3,
                A1 = new TAge { gcode = gcode, acode = "1", code = gcode + "1", in_l2 = "", rows = new List<TRow>() },
                A2 = new TAge { gcode = gcode, acode = "2", code = gcode + "2", in_l2 = "", rows = new List<TRow>() },
                A3 = new TAge { gcode = gcode, acode = "3", code = gcode + "3", in_l2 = "", rows = new List<TRow>() },
                A4 = new TAge { gcode = gcode, acode = "4", code = gcode + "4", in_l2 = "", rows = new List<TRow>() },
                A5 = new TAge { gcode = gcode, acode = "5", code = gcode + "5", in_l2 = "", rows = new List<TRow>() },
                A6 = new TAge { gcode = gcode, acode = "6", code = gcode + "6", in_l2 = "", rows = new List<TRow>() },
                A7 = new TAge { gcode = gcode, acode = "7", code = gcode + "7", in_l2 = "", rows = new List<TRow>() },
                A8 = new TAge { gcode = gcode, acode = "8", code = gcode + "8", in_l2 = "", rows = new List<TRow>() },
                A9 = new TAge { gcode = gcode, acode = "9", code = gcode + "9", in_l2 = "", rows = new List<TRow>() },
            };
        }

        private Item GetSectGenderItems(TConfig cfg, string in_gender)
        {
            var sql = @"
                SELECT
	                t1.in_value
	                , t1.in_filter
	                , t1.in_grand_filter
                FROM
	                VU_MEETING_SVY_L3 t1
                INNER JOIN
	                VU_MEETING_SVY_L2 t2
	                ON t2.source_id = t1.source_id
	                AND t2.in_filter = t1.in_grand_filter
	                AND t2.in_value = t1.in_filter
                WHERE
	                t1.source_id = '{#meeting_id}'
	                AND t1.in_grand_filter = '個人組'
	                AND t1.in_filter LIKE '%{#in_gender}%'
                ORDER BY
	                t2.sort_order
	                , t1.sort_order
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_gender}", in_gender);

            return cfg.inn.applySQL(sql);
        }
        private Item GetSoloGenderItems(TConfig cfg, string in_gender)
        {
            var sql = @"
                SELECT
	                t1.id
	                , t1.in_name
	                , t1.in_l1
	                , t1.in_l2
	                , t1.in_l3
	                , t1.in_index
	                , t1.in_age
	                , t1.in_gender
	                , t1.in_current_org
                    , UPPER(t1.in_sno_display) AS 'in_sno'
	                , CONVERT(VARCHAR, DATEADD(HOUR, 8, t1.in_birth), 111) AS 'reg_birth'
	                , t1.in_reg_l2
	                , t1.in_reg_l3
	                , t1.in_weight
	                , t1.in_weight_status
	                , t1.in_weight_result
                FROM
	                IN_MEETING_USER t1 WITH(NOLOCK)
                WHERE
	                source_id = '{#meeting_id}'
	                AND in_l1 = '個人組'
	                AND in_gender = '{#in_gender}'
	            ORDER BY
	                in_reg_l2
	                , REPLACE(in_reg_l3, '+', '-1')
	                , in_age
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_gender}", in_gender);

            return cfg.inn.applySQL(sql);
        }

        private TSvyOpt MapSurveyOption(TConfig cfg, string muid)
        {
            var model = new TSvyOpt { has_ranges = false };
            var item = GetMUserSurveyOption(cfg, muid);
            if (item.isError() || item.getResult() == "") throw new Exception("查無與會者");
            model.id = item.getProperty("id", "");
            model.in_name = item.getProperty("in_name", "");
            model.in_sno = item.getProperty("in_sno", "");
            model.in_gender = item.getProperty("in_gender", "");
            model.in_l1 = item.getProperty("in_l1", "");
            model.in_l2 = item.getProperty("in_l2", "");
            model.in_l3 = item.getProperty("in_l3", "");
            model.in_index = item.getProperty("in_index", "");
            model.in_current_org = item.getProperty("in_current_org", "");
            model.in_ranges = item.getProperty("in_ranges", "");
            if (model.in_ranges != "" && model.in_ranges.Contains("-"))
            {
                var ranges = model.in_ranges.Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);
                model.range_s = ranges != null && ranges.Length > 0 ? ranges[0] : "";
                model.range_e = ranges != null && ranges.Length > 1 ? ranges[1] : "";
                if (model.range_s != "" && model.range_e != "")
                {
                    model.min = GetDcmVal(model.range_s);
                    model.max = GetDcmVal(model.range_e);
                    model.has_ranges = model.max > model.min;
                }
            }
            return model;
        }

        private Item GetMUserSurveyOption(TConfig cfg, string muid)
        {
            var sql = @"
                SELECT
	                t1.id
	                , t1.in_name
	                , t1.in_sno
	                , t1.in_l1
	                , t1.in_l2
	                , t1.in_l3
	                , t1.in_index
	                , t1.in_current_org
	                , t2.in_ranges
                FROM
	                IN_MEETING_USER t1 WITH(NOLOCK)
                LEFT OUTER JOIN
	                VU_MEETING_SVY_L3 t2
	                ON t2.source_id = t1.source_id
	                AND t2.in_grand_filter = t1.in_l1
	                AND t2.in_filter = t1.in_l2
	                AND t2.in_value = t1.in_l3
                WHERE
	                t1.source_id = '{#meeting_id}'
	                AND t1.id = '{#muid}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#muid}", muid);

            return cfg.inn.applySQL(sql);
        }

        private class TSvyOpt
        {
            public bool has_ranges { get; set; }
            public string id { get; set; }
            public string in_name { get; set; }
            public string in_sno { get; set; }
            public string in_gender { get; set; }
            public string in_current_org { get; set; }
            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }
            public string in_index { get; set; }
            public string in_ranges { get; set; }
            public string range_s { get; set; }
            public string range_e { get; set; }
            public decimal min { get; set; }
            public decimal max { get; set; }
            public decimal new_weight { get; set; }

            public string in_weight { get; set; }
            public string in_weight_createid { get; set; }
            public string in_weight_createname { get; set; }
            public string in_weight_time { get; set; }
            public string in_weight_result { get; set; }
            public string in_weight_status { get; set; }
        }

        private decimal GetDcmVal(string value, decimal def = 0)
        {
            decimal result = def;
            decimal.TryParse(value, out result);
            return result;
        }
    }
}