﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Judo.世壯運
{
    internal class In_Meeting_Wmg_Sect : Item
    {
        public In_Meeting_Wmg_Sect(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;

            /*
                目的: 世壯運-組別對照表
                日誌: 
                    - 2024-12-27: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "In_Meeting_Wmg_Sect";

            var itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                meeting_id = itmR.getProperty("meeting_id", ""),
                scene = itmR.getProperty("scene", ""),
            };

            switch (cfg.scene)
            {
                case "merge":
                    Merge(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void Merge(TConfig cfg, Item itmReturn)
        {
            var in_l1 = itmReturn.getProperty("in_l1", "");
            var in_l2 = itmReturn.getProperty("in_l2", "");
            var in_l3 = itmReturn.getProperty("in_l3", "");
            var in_fight_l1 = itmReturn.getProperty("in_fight_l1", "");
            var in_fight_l2 = itmReturn.getProperty("in_fight_l2", "");
            var in_fight_l3 = itmReturn.getProperty("in_fight_l3", "");

            var itmNew = cfg.inn.newItem("In_Meeting_Wmg_Sect", "merge");

            itmNew.setAttribute("where", "in_meeting = '" + cfg.meeting_id + "'"
                + " AND in_l1 = '" + in_l1 + "'"
                + " AND in_l2 = '" + in_l2 + "'"
                + " AND in_l3 = '" + in_l3 + "'"
                );

            itmNew.setProperty("in_meeting", cfg.meeting_id);
            itmNew.setProperty("in_l1", in_l1);
            itmNew.setProperty("in_l2", in_l2);
            itmNew.setProperty("in_l2", in_l3);
            itmNew.setProperty("in_fight_l1", in_fight_l1);
            itmNew.setProperty("in_fight_l2", in_fight_l2);
            itmNew.setProperty("in_fight_l2", in_fight_l3);
            itmNew = itmNew.apply();
        }

        private void CopyItemValue(Item itmA, Item itmB, string propA, string propB = "")
        {
            if (propB == "")
            {
                itmA.setProperty(propA, itmB.getProperty(propA, ""));
            }
            else
            {
                itmA.setProperty(propA, itmB.getProperty(propB, ""));
            }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string strUserId { get; set; }
            public string strIdentityId { get; set; }

            public string meeting_id { get; set; }
            public string scene { get; set; }
        }

    }
}
