﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Judo.世壯運
{
    internal class In_Wmg_Register_List : Item
    {
        public In_Wmg_Register_List(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 世界壯年運動會報名清單
                日誌: 
                    - 2024-08-14: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "In_Wmg_Register_List";

            var itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                meeting_id = itmR.getProperty("meeting_id", ""),
                scene = itmR.getProperty("scene", ""),
            };

            switch (cfg.scene)
            {
                case "page":
                    Page(cfg, itmR);
                    break;
            }

            return itmR;
        }

        //頁面
        private void Page(TConfig cfg, Item itmReturn)
        {
            var items = GetMeetingUserItems(cfg, itmReturn);
            var sects = MapSects(cfg, items);
            var map = GetCountMap(cfg);

            var contents = new StringBuilder();

            AppendStatisticsTable(cfg, map, contents);

            for (var i = 0; i < sects.Count; i++)
            {
                var sect = sects[i];
                if (sect.is_team)
                {
                    AppendTeamSectTable(cfg, sect, contents);
                }
                else
                {
                    AppendSoloSectTable(cfg, sect, contents);
                }
            }

            AppendCountTable(cfg, sects, contents);

            itmReturn.setProperty("inn_tables", contents.ToString());
        }

        private void AppendStatisticsTable(TConfig cfg, TCountMap map, StringBuilder contents)
        {
            var head = new StringBuilder();
            var body = new StringBuilder();

            var headCss = "text-center bg-primary";
            head.Append("<th class='" + headCss + "'>項目</th>");
            head.Append("<th class='" + headCss + "'>數量</th>");

            body.Append("<tr><td>項目數</td><td>" + map.item_count+ "</td></tr>");
            body.Append("<tr><td>選手數</td><td>" + map.player_count+ "</td></tr>");
            body.Append("<tr><td>男子選手</td><td>" + map.gender_m_count + "</td></tr>");
            body.Append("<tr><td>女子選手</td><td>" + map.gender_w_count + "</td></tr>");
            body.Append("<tr><td>單位數</td><td>" + map.item_count + "</td></tr>");

            var id = "data-table-group";
            contents.Append("<div>");
            contents.Append("<h3>統計資訊</h3>");
            contents.Append(GetTableAttribute(id));
            contents.Append("<thead>");
            contents.Append("  <tr>");
            contents.Append(head);
            contents.Append("  </tr>");
            contents.Append("</thead>");
            contents.Append("<tbody>");
            contents.Append(body);
            contents.Append("</tbody>");
            contents.Append("</table>");
            contents.Append("</div>");
        }

        private void AppendCountTable(TConfig cfg, List<TSect> sects, StringBuilder contents)
        {
            var head = new StringBuilder();
            var body = new StringBuilder();

            var headCss = "text-center bg-primary";
            head.Append("<th class='" + headCss + "'>序號</th>");
            head.Append("<th class='" + headCss + "'>組別</th>");
            head.Append("<th class='" + headCss + "'>分級</th>");
            head.Append("<th class='" + headCss + "'>報名人數</th>");

            var no = 1;
            for (var i = 0; i < sects.Count; i++)
            {
                var sect = sects[i];
                body.Append("<tr>");
                body.Append("<td class='text-center'>" + no + "</td>");
                if (sect.is_team)
                {
                    body.Append("<td>" + sect.in_l1 + "</td>");
                    body.Append("<td>" + sect.in_l3 + "</td>");
                }
                else
                {
                    body.Append("<td>" + sect.in_l2 + "</td>");
                    body.Append("<td>" + sect.in_l3 + "</td>");
                }
                body.Append("<td class='text-center'>" + sect.teams.Count + "</td>");
                body.Append("</tr>");
                no++;
            }

            var id = "data-table-group";
            contents.Append("<div>");
            contents.Append("<h3>級組摘要</h3>");
            contents.Append(GetTableAttribute(id));
            contents.Append("<thead>");
            contents.Append("  <tr>");
            contents.Append(head);
            contents.Append("  </tr>");
            contents.Append("</thead>");
            contents.Append("<tbody>");
            contents.Append(body);
            contents.Append("</tbody>");
            contents.Append("</table>");
            contents.Append("</div>");
        }

        private void AppendSoloSectTable(TConfig cfg, TSect sect, StringBuilder contents)
        {
            var head = new StringBuilder();
            var body = new StringBuilder();

            var headCss = "text-center bg-primary";
            head.Append("<th class='" + headCss + "'>序號</th>");
            head.Append("<th class='" + headCss + "'>姓名</th>");
            head.Append("<th class='" + headCss + "'>身分證字號</th>");
            head.Append("<th class='" + headCss + "'>性別</th>");
            head.Append("<th class='" + headCss + "'>西元生日</th>");
            head.Append("<th class='" + headCss + "'>年齡</th>");
            head.Append("<th class='" + headCss + "'>所屬單位</th>");
            head.Append("<th class='" + headCss + "'>分組</th>");
            head.Append("<th class='" + headCss + "'>量級</th>");
            head.Append("<th class='" + headCss + "'>併級前量級</th>");

            var no = 1;
            for (var i = 0; i < sect.teams.Count; i++)
            {
                var team = sect.teams[i];
                for (var j = 0; j < team.items.Count; j++)
                {
                    var item = team.items[j];
                    var in_l2_old = item.getProperty("in_l2_old", "");
                    var in_weight_note = item.getProperty("in_weight_note", "");

                    body.Append("<tr>");
                    body.Append("<td class='text-center'>" + no + "</td>");
                    body.Append("<td>" + item.getProperty("in_name", "") + "</td>");
                    body.Append("<td>" + item.getProperty("in_sno", "") + "</td>");
                    body.Append("<td>" + item.getProperty("in_gender", "") + "</td>");
                    body.Append("<td>" + item.getProperty("reg_birth", "") + "</td>");
                    body.Append("<td>" + item.getProperty("in_age", "") + "</td>");
                    body.Append("<td>" + item.getProperty("in_current_org", "") + "</td>");
                    body.Append("<td>" + GetSectCode(item) + "</td>");
                    body.Append("<td>" + item.getProperty("in_l3", "") + "</td>");
                    if (in_l2_old != "")
                    {
                        body.Append("<td>" + in_weight_note + "</td>");
                    }
                    else
                    {
                        body.Append("<td>&nbsp;</td>");
                    }
                    body.Append("</tr>");
                    no++;
                }
            }

            var id = "data-table-" + sect.no;
            contents.Append("<div>");
            contents.Append("<h3>" + sect.in_l2 + " " + sect.in_l3 + "</h3>");

            contents.Append(GetTableAttribute(id));

            contents.Append("<thead>");
            contents.Append("  <tr>");
            contents.Append(head);
            contents.Append("  </tr>");
            contents.Append("</thead>");

            contents.Append("<tbody>");
            contents.Append(body);
            contents.Append("</tbody>");

            contents.Append("</table>");
            contents.Append("</div>");
        }

        private void AppendTeamSectTable(TConfig cfg, TSect sect, StringBuilder contents)
        {
            var head = new StringBuilder();
            var body = new StringBuilder();

            var headCss = "text-center bg-primary";
            head.Append("<th class='" + headCss + "'>序號</th>");
            head.Append("<th class='" + headCss + "'>所屬單位</th>");
            head.Append("<th class='" + headCss + "'>姓名</th>");
            head.Append("<th class='" + headCss + "'>身分證字號</th>");
            head.Append("<th class='" + headCss + "'>性別</th>");
            head.Append("<th class='" + headCss + "'>西元生日</th>");
            head.Append("<th class='" + headCss + "'>年齡</th>");

            for (var i = 0; i < sect.teams.Count; i++)
            {
                var team = sect.teams[i];
                for (var j = 0; j < team.items.Count; j++)
                {
                    var item = team.items[j];
                    var in_current_org = item.getProperty("in_current_org", "");
                    var in_team = item.getProperty("in_team", "");
                    var org = in_current_org + " " + in_team;

                    body.Append("<tr>");
                    if (j == 0)
                    {
                        body.Append("<td class='text-center'>" + (i + 1) + "</td>");
                        body.Append("<td>" + org + "</td>");
                    }
                    else
                    {
                        body.Append("<td>&nbsp;</td>");
                        body.Append("<td>&nbsp;</td>");
                    }
                    body.Append("<td>" + item.getProperty("in_name", "") + "</td>");
                    body.Append("<td>" + item.getProperty("in_sno", "") + "</td>");
                    body.Append("<td>" + item.getProperty("in_gender", "") + "</td>");
                    body.Append("<td>" + item.getProperty("reg_birth", "") + "</td>");
                    body.Append("<td>" + item.getProperty("in_age", "") + "</td>");
                    body.Append("</tr>");
                }
            }


            var id = "data-table-" + sect.no;
            contents.Append("<div>");
            contents.Append("<h3>" + sect.in_l3 + "</h3>");

            contents.Append(GetTableAttribute(id));

            contents.Append("<thead>");
            contents.Append("  <tr>");
            contents.Append(head);
            contents.Append("  </tr>");
            contents.Append("</thead>");

            contents.Append("<tbody>");
            contents.Append(body);
            contents.Append("</tbody>");

            contents.Append("</table>");
            contents.Append("</div>");
        }

        private string GetSectCode(Item item)
        {
            var in_l2 = item.getProperty("in_l2", "");
            var arr = in_l2.Split('(');
            if (arr == null || arr.Length == 1) return in_l2;
            var code = arr[0].Replace("男子組", "").Replace("女子組", "");
            return code;
        }
        private string GetTableAttribute(string table_name)
        {
            return "<table id='" + table_name + "'"
                + " class='table table-hover table-bordered table-rwd rwd rwdtable'"
                + " data-toggle='table'"
                + " data-show-toggle='false'"
                + " data-show-columns='false'"
                + " data-search-align='left'"
                + " data-sort-stable='true'"
                + " data-search='false'"
                + ">";
        }

        private Item GetMeetingUserItems(TConfig cfg, Item itmReturn)
        {
            var sql = @"
                SELECT 
	                t1.in_name
                    , UPPER(t1.in_sno_display) AS 'in_sno'
	                , t1.in_gender
	                , CONVERT(VARCHAR, DATEADD(HOUR, 8, t1.in_birth), 111) AS 'reg_birth'
	                , t1.in_age
                    , t1.in_l1
					, t1.in_l2
					, t1.in_l3
					, t1.in_index
					, t1.in_creator_sno
					, t1.in_current_org
					, t1.in_team
	                , t1.in_expense
	                , t1.in_paynumber
	                , ISNULL(CONVERT(VARCHAR, DATEADD(HOUR, 8, t1.in_paytime), 111), '') AS 'in_paytime'
	                , ISNULL(t21.pay_bool, '') AS 'pay_bool'
	                , t1.in_l2_old
	                , t1.in_weight_note
                FROM 
	                IN_MEETING_USER t1 WITH(NOLOCK) 
                INNER JOIN 
	                VU_MEETING_SVY_L1 t11 
	                ON t11.source_id = t1.source_id 
	                AND t11.in_value = t1.in_l1
                INNER JOIN 
	                VU_MEETING_SVY_L2 t12 
	                ON t12.source_id = t1.source_id 
	                AND t12.in_filter = t1.in_l1
	                AND t12.in_value = t1.in_l2
                INNER JOIN 
	                VU_MEETING_SVY_L3 t13 
	                ON t13.source_id = t1.source_id 
	                AND t13.in_grand_filter = t1.in_l1
	                AND t13.in_filter = t1.in_l2
	                AND t13.in_value = t1.in_l3
                LEFT OUTER JOIN 
	                IN_MEETING_PAY t21 WITH(NOLOCK) 
	                ON t21.in_meeting = t1.source_id 
	                AND t21.item_number = t1. in_paynumber
                WHERE 
	                t1.source_id = '{#meeting_id}'
	                AND t1.in_l1 NOT IN ('隊職員')
                ORDER BY 
	                t11.sort_order
	                , t12.sort_order
	                , t13.sort_order
	                , t1.in_index
	                , t1.in_stuff_b1
	                , t1.in_team
	                , t1.in_birth
            ";
            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);
            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string meeting_id { get; set; }
            public string scene { get; set; }
        }

        private class TSect
        {
            public int no { get; set; }
            public string key { get; set; }
            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }
            public bool is_team { get; set; }

            public List<TTeam> teams { get; set; }
        }

        private class TTeam
        {
            public string key { get; set; }
            public string in_index { get; set; }
            public string in_creator_sno { get; set; }
            public List<Item> items { get; set; }
        }

        private class TCountMap
        {
            public string org_count { get; set; }
            public string player_count { get; set; }
            public string gender_m_count { get; set; }
            public string gender_w_count { get; set; }
            public string item_count { get; set; }
        }

        private TCountMap GetCountMap(TConfig cfg)
        {
            var map = new TCountMap { org_count = "", player_count="", gender_m_count = "", gender_w_count = "", item_count = "" };
            var sql = "SELECT * FROM [VU_MEETING_STATISTICS] WHERE source_id = '" + cfg.meeting_id + "'";
            var items = cfg.inn.applySQL(sql);
            var count = 0;
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var in_type = item.getProperty("in_type", "");
                var in_count = item.getProperty("in_count", "");
                switch (in_type)
                {
                    case "player_count": map.player_count = in_count; break;
                    case "gender_m_count": map.gender_m_count = in_count; break;
                    case "gender_w_count": map.gender_w_count = in_count; break;
                    case "item_count": map.item_count = in_count; break;
                    case "org_count": map.org_count = in_count; break;
                }
            }
            return map;
        }

        private List<TSect> MapSects(TConfig cfg, Item items)
        {
            var sects = new List<TSect>();
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var in_l1 = item.getProperty("in_l1", "");
                var in_l2 = item.getProperty("in_l2", "");
                var in_l3 = item.getProperty("in_l3", "");
                var in_index = item.getProperty("in_index", "");
                var in_creator_sno = item.getProperty("in_creator_sno", "");
                var sect_key = string.Join("-", new string[] { in_l1, in_l2, in_l3 });
                var team_key = string.Join("-", new string[] { in_l1, in_l2, in_l3, in_index, in_creator_sno });

                var sect = sects.Find(x => x.key == sect_key);
                if (sect == null)
                {
                    sect = new TSect
                    {
                        no = sects.Count + 1,
                        key = sect_key,
                        in_l1 = in_l1,
                        in_l2 = in_l2,
                        in_l3 = in_l3,
                        is_team = in_l1 == "團體組",
                        teams = new List<TTeam>()
                    };
                    sects.Add(sect);
                }

                var team = sect.teams.Find(x => x.key == team_key);
                if (team == null)
                {
                    team = new TTeam
                    {
                        key = team_key,
                        in_index = in_index,
                        in_creator_sno = in_creator_sno,
                        items = new List<Item>()
                    };
                    sect.teams.Add(team);
                }
                team.items.Add(item);
            }
            return sects;
        }
    }
}