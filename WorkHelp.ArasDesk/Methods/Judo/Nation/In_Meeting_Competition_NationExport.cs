﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Judo.Common
{
    public class In_Meeting_Competition_NationExport : Item
    {
        public In_Meeting_Competition_NationExport(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
    目的: 國際版對戰表
    日誌: 
        - 2023-06-14: 創建 (lina)
*/

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "In_Meeting_Competition_NationExport";

            var itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                meeting_id = itmR.getProperty("meeting_id", ""),
                scene = itmR.getProperty("scene", ""),
                facade = itmR.getProperty("facade", ""),
                refresh_tree_no = false,
                font_name = "Calibri",
            };

            switch (cfg.scene)
            {
                case "nation_draw_sheet"://國際版對戰表
                    ExportNationDrawSheet(cfg, itmR);
                    break;
            }

            return itmR;
        }

        //國際版對戰表
        private void ExportNationDrawSheet(TConfig cfg, Item itmReturn)
        {
            SetExportInfo(cfg, itmReturn);

            var itmPrograms = GetProgramItems(cfg, itmReturn);
            var programs = MapPrograms(cfg, itmPrograms);

            var book = new Spire.Xls.Workbook();
            book.LoadFromFile(cfg.template_path);

            var default_sheet_count = book.Worksheets.Count;

            if (cfg.facade == "report")
            {
                AppendEventSheets(cfg, book);
            }

            for (int i = 0; i < programs.Count; i++)
            {
                var program = programs[i];
                AppendDrawSheet(cfg, program, book);
            }

            if (book.Worksheets.Count == default_sheet_count)
            {
                throw new Exception("查無資料");
            }

            //移除樣板 Sheet
            var mx = default_sheet_count - 1;
            for (int i = mx; i >= 0; i--)
            {
                book.Worksheets.RemoveAt(i);
            }

            var xlsName1 = cfg.mt_title + "_DrawSheet_Step1_" + DateTime.Now.ToString("MMdd_HHmmss");
            var xlsFile1 = cfg.export_path + xlsName1 + ".xlsx";
            book.SaveToFile(xlsFile1);

            //由於 OvalShapes CopyFrom 時期無法複製，因此增加中繼檔處理
            ResetSheetsForTreeNo(cfg, xlsFile1, programs, itmReturn);
        }

        private List<TProgram> MapPrograms(TConfig cfg, Item itmPrograms)
        {
            var result = new List<TProgram>();
            var count = itmPrograms.getItemCount();
            for (int i = 0; i < count; i++)
            {
                var itmProgram = itmPrograms.getItemByIndex(i);
                var program = MapOneProgram(cfg, itmProgram);
                program.map = MapEventList(cfg, program);

                if (program.isRobin)
                {
                    program.players = MapPlayerList(cfg, program);
                }

                result.Add(program);
            }
            return result;
        }

        private void ResetSheetsForTreeNo(TConfig cfg, string fileSource, List<TProgram> programs, Item itmReturn)
        {
            var book = new Spire.Xls.Workbook();
            book.LoadFromFile(fileSource);

            if (cfg.facade == "report")
            {
                ResetEventSheetsTitleBox(cfg, book);
            }

            for (int i = 0; i < programs.Count; i++)
            {
                var program = programs[i];
                var sheet = book.Worksheets[program.in_short_name];
                if (sheet == null) continue;

                ResetDrawSheetTitleBox(cfg, sheet, program, program.map);

                if (!cfg.refresh_tree_no) continue;

                switch (program.sheetKey)
                {
                    case "2":
                        ResetTreeNo002Robin(cfg, sheet, program, program.map);
                        break;

                    case "3":
                        ResetTreeNo003Robin(cfg, sheet, program, program.map);
                        break;

                    case "3_Cross":
                        ResetTreeNo003Cross(cfg, sheet, program, program.map);
                        break;

                    case "4":
                        ResetTreeNo004Robin(cfg, sheet, program, program.map);
                        break;

                    case "4_Cross":
                        ResetTreeNo004Cross(cfg, sheet, program, program.map);
                        break;

                    case "5":
                        ResetTreeNo005Robin(cfg, sheet, program, program.map);
                        break;

                    case "5_Cross":
                        ResetTreeNo005Cross(cfg, sheet, program, program.map);
                        break;

                    case "8":
                        ResetTreeNo008(cfg, sheet, program, program.map);
                        break;

                    case "16":
                        ResetTreeNo016(cfg, sheet, program, program.map);
                        break;

                    case "32":
                        ResetTreeNo032(cfg, sheet, program, program.map);
                        break;
                }
            }

            // //適合頁面
            // book.ConverterSetting.SheetFitToPage = true;

            var xlsName2 = cfg.mt_title + "_DrawSheet_" + DateTime.Now.ToString("MMdd_HHmmss");
            var xlsFile2 = cfg.export_path + xlsName2 + "." + cfg.export_type;

            if (cfg.export_type == "pdf")
            {
                book.SaveToFile(xlsFile2, Spire.Xls.FileFormat.PDF);
            }
            else
            {
                book.SaveToFile(xlsFile2, Spire.Xls.ExcelVersion.Version2013);
            }

            itmReturn.setProperty("xls_name", xlsName2 + "." + cfg.export_type);
        }

        #region Event Report

        private void AppendGenderReport(TConfig cfg, Spire.Xls.Workbook book, List<TMUser> rows, TRptConfig rptCfg)
        {
            var key = "gender_sample";
            var sheetTemplate = book.Worksheets[key];

            var sheet = book.CreateEmptySheet();
            sheet.CopyFrom(sheetTemplate);
            sheet.Name = "gender_report";

            var nations = rows.GroupBy(x => x.in_short_org).ToList();

            var list_m = rows.FindAll(x => x.in_gender == rptCfg.m.gender);
            var list_w = rows.FindAll(x => x.in_gender == rptCfg.w.gender);
            if (list_m == null) list_m = new List<TMUser>();
            if (list_w == null) list_w = new List<TMUser>();

            var r1 = rows.Count + " competitors from " + nations.Count + " nations";

            var txtTitle = sheet.TextBoxes.AddTextBox(6, 3, 36, 500);
            txtTitle.HAlignment = Spire.Xls.CommentHAlignType.Center;
            txtTitle.VAlignment = Spire.Xls.CommentVAlignType.Center;
            txtTitle.HtmlString = rptCfg.format1.Replace("{#w}", "22").Replace("{#v}", r1);
            txtTitle.Line.Visible = false;

            SetCellStr(cfg, sheet, "C9", rptCfg.m.key, false);
            SetCellNum(cfg, sheet, "D9", list_m.Count, false);
            SetCellNum(cfg, sheet, "H9", list_w.Count, false);
            SetCellStr(cfg, sheet, "I9", rptCfg.w.key, false);
            sheet["C9"].Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Left;
            sheet["I9"].Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Right;

            //SetCellStr(cfg, sheet, "C9", rptCfg.m.key + " " + list_m.Count);
            //SetCellStr(cfg, sheet, "I9", rptCfg.w.key + " " + list_w.Count);
            // sheet["C9"].Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Left;
            // sheet["I9"].Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Right;

            var w_model = Calculate(list_w.Count, rows.Count, 500);
            var txtGenderW = sheet.TextBoxes.AddTextBox(10, 3, 36, 500);
            txtGenderW.HAlignment = Spire.Xls.CommentHAlignType.Right;
            txtGenderW.VAlignment = Spire.Xls.CommentVAlignType.Center;
            txtGenderW.HtmlString = rptCfg.format1.Replace("{#w}", "16").Replace("{#v}", w_model.percent);
            txtGenderW.Fill.ForeColor = rptCfg.w.color1;
            txtGenderW.Line.Visible = true;

            var m_model = Calculate(list_m.Count, rows.Count, 500);
            var txtGenderM = sheet.TextBoxes.AddTextBox(10, 3, 36, m_model.width);
            txtGenderM.HAlignment = Spire.Xls.CommentHAlignType.Left;
            txtGenderM.VAlignment = Spire.Xls.CommentVAlignType.Center;
            txtGenderM.HtmlString = rptCfg.format1.Replace("{#w}", "16").Replace("{#v}", m_model.percent);
            txtGenderM.Fill.ForeColor = rptCfg.m.color1;
            txtGenderM.Line.Visible = true;

            var ridx = 13;
            var cs = new string[] { "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M" };
            var l1_kvs = rows.GroupBy(x => x.in_l1).ToDictionary(o => o.Key, o => o.ToList());
            if (l1_kvs.Count > 0)
            {
                foreach (var l1_kv in l1_kvs)
                {
                    SetGenderWeightCount(cfg, sheet, list_m, l1_kv.Key, ridx, rptCfg.m, true);
                    ridx += 3;

                    SetGenderWeightCount(cfg, sheet, list_w, l1_kv.Key, ridx, rptCfg.w, false);
                    ridx += 3;
                }
            }

            MergeCell(cfg, sheet, "C", "D", ridx + 0, ridx + 0, "Schedule", rptCfg);
            MergeCell(cfg, sheet, "C", "D", ridx + 1, ridx + 1, "Date", rptCfg);
            MergeCell(cfg, sheet, "C", "D", ridx + 2, ridx + 2, "Start", rptCfg);
            MergeCell(cfg, sheet, "C", "D", ridx + 3, ridx + 3, "Finals", rptCfg);

            var dts = cfg.in_date_s.Date;
            var dte = cfg.in_date_e.Date;
            var cidx = 1;
            for (var i = dts; i <= dte; i = i.AddDays(1))
            {
                var c = cs[cidx + 1];
                SetCellStr(cfg, sheet, c + (ridx + 0), "Day " + cidx, rptCfg.color1, true);
                SetCellStr(cfg, sheet, c + (ridx + 1), i.ToString("dd MMMM"), rptCfg.color1, false);
                SetCellStr(cfg, sheet, c + (ridx + 2), "10:30", rptCfg.color1, false);
                SetCellStr(cfg, sheet, c + (ridx + 3), "17:00", rptCfg.color1, false);
                cidx++;
            }

            var range = sheet.Range["C" + (ridx + 0) + ":" + cs[cidx] + (ridx + 3)];
            range.BorderInside(Spire.Xls.LineStyleType.Thin, System.Drawing.Color.Black);
            range.BorderAround(Spire.Xls.LineStyleType.Thin, System.Drawing.Color.Black);
        }

        private void MergeCell(TConfig cfg, Spire.Xls.Worksheet sheet, string cs, string ce, int rs, int re, string text, TRptConfig rptCfg)
        {
            var mr = sheet.Range[cs + rs + ":" + ce + re];
            mr.Merge();
            mr.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Right;
            mr.Text = text;
            mr.Style.Font.FontName = cfg.font_name;
            mr.Style.Font.IsBold = true;
            mr.Style.Color = rptCfg.color1;
        }

        private void SetGenderWeightCount(TConfig cfg, Spire.Xls.Worksheet sheet, List<TMUser> source, string in_l1, int ridx, TGenderConfig gcfg, bool need_l1)
        {
            var in_l2 = in_l1 + " " + gcfg.key;
            var l3s = GetLevel3(cfg, in_l1, in_l2);

            var cs = l3s.Count == 7
                ? new string[] { "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M" }
                : new string[] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M" };

            if (need_l1)
            {
                SetCellStr(cfg, sheet, cs[0] + ridx, in_l1, true);
            }

            var total = 0;
            for (var i = 0; i < l3s.Count; i++)
            {
                var in_l3 = l3s[i];
                var p1 = cs[i + 1] + (ridx + 0);
                var p2 = cs[i + 1] + (ridx + 1);
                var list = source.FindAll(x => x.in_l3 == in_l3);
                var cnt = list == null ? 0 : list.Count;

                SetCellStr(cfg, sheet, p1, in_l3, gcfg.color2, true);
                SetCellNum(cfg, sheet, p2, cnt, gcfg.color2, false);

                total += cnt;
            }

            var gp1 = cs[l3s.Count + 1] + (ridx + 0);
            var gp2 = cs[l3s.Count + 1] + (ridx + 1);
            SetCellStr(cfg, sheet, gp1, gcfg.key, gcfg.color2, true);
            SetCellNum(cfg, sheet, gp2, total, gcfg.color2, false);

            var ps = cs[1] + (ridx + 0);
            var pe = cs[l3s.Count + 1] + (ridx + 1);
            var range = sheet.Range[ps + ":" + pe];
            range.BorderInside(Spire.Xls.LineStyleType.Thin, System.Drawing.Color.Black);
            range.BorderAround(Spire.Xls.LineStyleType.Thin, System.Drawing.Color.Black);
        }

        private void SetCellNum(TConfig cfg, Spire.Xls.Worksheet sheet, string pos, int value, bool isBold = false)
        {
            var col = sheet[pos];
            col.NumberValue = value;
            col.Style.Font.FontName = cfg.font_name;
            col.Style.Font.IsBold = isBold;
        }

        private void SetCellNum(TConfig cfg, Spire.Xls.Worksheet sheet, string pos, int value, System.Drawing.Color color, bool isBold = false)
        {
            var col = sheet[pos];
            col.NumberValue = value;
            col.Style.Font.FontName = cfg.font_name;
            col.Style.Font.IsBold = isBold;
            col.Style.Color = color;
        }

        private void SetCellStr(TConfig cfg, Spire.Xls.Worksheet sheet, string pos, string value, bool isBold = false)
        {
            var col = sheet[pos];
            col.Text = value;
            col.Style.Font.FontName = cfg.font_name;
            col.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
            col.Style.Font.IsBold = isBold;
        }

        private void SetCellStr(TConfig cfg, Spire.Xls.Worksheet sheet, string pos, string value, System.Drawing.Color color, bool isBold = false)
        {
            var col = sheet[pos];
            col.Text = value;
            col.Style.Font.FontName = cfg.font_name;
            col.Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
            col.Style.Font.IsBold = isBold;
            col.Style.Color = color;
        }

        private void AppendEventSheets(TConfig cfg, Spire.Xls.Workbook book)
        {
            var rows = GetMeetingUsers(cfg);

            var rptCfg = new TRptConfig
            {
                color1 = System.Drawing.ColorTranslator.FromHtml("#E7E7E7"),
                format1 = @"<Font Style=""FONT-WEIGHT: bold;FONT-FAMILY: Calibri;FONT-SIZE: {#w}pt;COLOR: #000000;TEXT-ALIGN: center;"">{#v}</Font>",
                m = new TGenderConfig
                {
                    gender = "男",
                    key = "Men",
                    color1 = System.Drawing.ColorTranslator.FromHtml("#6DB5E5"),
                    color2 = System.Drawing.ColorTranslator.FromHtml("#95CEEE"),
                },
                w = new TGenderConfig
                {
                    gender = "女",
                    key = "Women",
                    color1 = System.Drawing.ColorTranslator.FromHtml("#F5CDE1"),
                    color2 = System.Drawing.ColorTranslator.FromHtml("#F5CDE1"),
                }
            };

            AppendGenderReport(cfg, book, rows, rptCfg);
        }

        private class TRptConfig
        {
            public System.Drawing.Color color1 { get; set; }
            public string format1 { get; set; }
            public TGenderConfig m { get; set; }
            public TGenderConfig w { get; set; }
        }

        private class TGenderConfig
        {
            public string gender { get; set; }
            public string key { get; set; }
            public System.Drawing.Color color1 { get; set; }
            public System.Drawing.Color color2 { get; set; }
        }

        private List<string> GetLevel3(TConfig cfg, string in_l1, string in_l2)
        {
            var result = new List<string>();

            var sql = @"
                SELECT 
	                t3.in_grand_filter
	                , t3.in_filter
	                , t3.in_value
                FROM 
	                IN_MEETING_SURVEYS t1 WITH(NOLOCK)
                INNER JOIN
	                IN_SURVEY t2 WITH(NOLOCK)
	                ON t2.id = t1.related_id
                INNER JOIN
	                IN_SURVEY_OPTION t3 WITH(NOLOCK)
	                ON t3.source_id = t1.related_id
                WHERE
	                t1.source_id = '{#meeting_id}'
	                AND t2.in_property = 'in_l3'
	                AND t3.in_grand_filter = N'{#in_l1}'
	                AND t3.in_filter = N'{#in_l2}'
                ORDER BY
	                t3.sort_order
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_l1}", in_l1)
                .Replace("{#in_l2}", in_l2);

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var in_value = item.getProperty("in_value", "");
                result.Add(in_value);
            }

            return result;
        }

        private TGender Calculate(int count, int total_count, int total_width)
        {
            if (count <= 0) return new TGender { percent = "00.00%", width = 0 };

            var d1 = ((decimal)count * 100) / (decimal)total_count;
            var d2 = decimal.Round(d1);
            return new TGender { percent = d2.ToString() + "%", width = (int)(d2 * 5) };
        }

        private class TGender
        {
            public int width { get; set; }
            public string percent { get; set; }
        }

        private List<TMUser> GetMeetingUsers(TConfig cfg)
        {
            var result = new List<TMUser>();

            var sql = @"
                SELECT 
	                in_name
	                , in_gender
	                , in_short_org
	                , in_l1
	                , in_l2
	                , in_l3
	                , REPLACE(in_l3, ' kg', '') AS 'weight'
                FROM 
	                IN_MEETING_USER WITH(NOLOCK)
                WHERE
	                source_id = '{#meeting_id}'
	                AND in_l1 NOT IN (N'隊職員', '常年會費', 'Delegation')
                ORDER BY
	                in_l1
	                , in_l2
	                , in_l3
	
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            var items = cfg.inn.applySQL(sql);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var row = new TMUser
                {
                    in_name = item.getProperty("in_name", ""),
                    in_gender = item.getProperty("in_gender", ""),
                    in_short_org = item.getProperty("in_short_org", ""),
                    in_l1 = item.getProperty("in_l1", ""),
                    in_l2 = item.getProperty("in_l2", ""),
                    in_l3 = item.getProperty("in_l3", ""),
                    weight = item.getProperty("weight", ""),
                };

                if (row.in_l3.Contains("+"))
                {
                    row.w = GetInt(row.in_l3.Replace("+", "")) + 1000;
                }
                else
                {
                    row.w = GetInt(row.in_l3.Replace("-", ""));
                }

                result.Add(row);
            }

            return result;
        }

        private class TMUser
        {
            public string in_name { get; set; }
            public string in_gender { get; set; }
            public string in_short_org { get; set; }
            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }
            public string weight { get; set; }
            public int w { get; set; }
        }

        #endregion Event Report


        private void AppendDrawSheet(TConfig cfg, TProgram program, Spire.Xls.Workbook book)
        {
            if (program.map == null || program.map.Count == 0) return;

            var sheetTemplate = book.Worksheets[program.sheetKey];

            var sheet = book.CreateEmptySheet();
            sheet.CopyFrom(sheetTemplate);
            sheet.Name = program.in_short_name;

            sheet.PageSetup.FitToPagesWide = 1;
            sheet.PageSetup.FitToPagesTall = 1;
            // sheet.PageSetup.TopMargin = 0.3;
            // sheet.PageSetup.LeftMargin = 0.5;
            // sheet.PageSetup.RightMargin = 0.5;
            // sheet.PageSetup.BottomMargin = 0.5;
            sheet.PageSetup.TopMargin = 0.1;
            sheet.PageSetup.LeftMargin = 0.0;
            sheet.PageSetup.RightMargin = 0.0;
            sheet.PageSetup.BottomMargin = 0.1;

            switch (program.sheetKey)
            {
                case "2":
                    program.LeftColumnOffset = 75;
                    program.TopRowOffset = 50;
                    ExportPlayers002Robin(cfg, sheet, program, program.map);
                    ResetWinStatus002Robin(cfg, sheet, program, program.map);
                    break;

                case "3":
                    program.LeftColumnOffset = 75;
                    program.TopRowOffset = 50;
                    ExportPlayers003Robin(cfg, sheet, program, program.map);
                    ResetWinStatus003Robin(cfg, sheet, program, program.map);
                    break;

                case "3_Cross":
                    program.LeftColumnOffset = 75;
                    program.TopRowOffset = 50;
                    ExportPlayers003Robin(cfg, sheet, program, program.map);
                    ResetWinStatus003Cross(cfg, sheet, program, program.map);
                    break;

                case "4":
                    program.LeftColumnOffset = 50;
                    program.TopRowOffset = 50;
                    ExportPlayers004Robin(cfg, sheet, program, program.map);
                    ResetWinStatus004Robin(cfg, sheet, program, program.map);
                    break;

                case "4_Cross":
                    program.LeftColumnOffset = 150;
                    program.TopRowOffset = 100;
                    ExportPlayers004Cross(cfg, sheet, program, program.map);
                    ResetWinStatus004Cross(cfg, sheet, program, program.map);
                    break;

                case "5":
                    program.LeftColumnOffset = 75;
                    program.TopRowOffset = 50;
                    ExportPlayers005Robin(cfg, sheet, program, program.map);
                    ResetWinStatus005Robin(cfg, sheet, program, program.map);
                    break;

                case "5_Cross":
                    program.LeftColumnOffset = 75;
                    program.TopRowOffset = 75;
                    ExportPlayers005Cross(cfg, sheet, program, program.map);
                    ResetWinStatus005Cross(cfg, sheet, program, program.map);
                    break;

                case "8":
                    program.LeftColumnOffset = 75;
                    program.TopRowOffset = 100;
                    ExportPlayers008(cfg, sheet, program, program.map);
                    ResetWinStatus008(cfg, sheet, program, program.map);
                    break;

                case "16":
                    program.LeftColumnOffset = 75;
                    program.TopRowOffset = 50;
                    ExportPlayers016(cfg, sheet, program, program.map);
                    ResetWinStatus016(cfg, sheet, program, program.map);
                    break;

                case "32":
                    program.LeftColumnOffset = 75;
                    program.TopRowOffset = 25;
                    ExportPlayers032(cfg, sheet, program, program.map);
                    ResetWinStatus032(cfg, sheet, program, program.map);
                    break;
            }
        }

        #region WinStatus

        private void ResetWinStatus003Cross(TConfig cfg, Spire.Xls.Worksheet sheet, TProgram program, Dictionary<string, TEvt> map)
        {
            SetWinStatus(cfg, sheet, program, map, "CRS03-01", "I15", "I16");
            SetWinStatus(cfg, sheet, program, map, "CRS03-02", "I19", "I20");
            SetWinStatus(cfg, sheet, program, map, "CRS03-03", "I23", "I24");

            SetRepechagePlayers(cfg, sheet, program, map, "RNK12", "C28", "C30");
            SetWinStatus(cfg, sheet, program, map, "RNK12", "F29", "F30");
        }

        private void ResetWinStatus004Cross(TConfig cfg, Spire.Xls.Worksheet sheet, TProgram program, Dictionary<string, TEvt> map)
        {
            SetWinStatus(cfg, sheet, program, map, "CRS04-01", "F6", "F7");
            SetWinStatus(cfg, sheet, program, map, "CRS04-02", "F10", "F11");

            SetWinStatus(cfg, sheet, program, map, "RNK12", "I8", "I9");
            SetWinStatus(cfg, sheet, program, map, "RNK34", "F16", "F17");
        }

        private void ResetWinStatus005Cross(TConfig cfg, Spire.Xls.Worksheet sheet, TProgram program, Dictionary<string, TEvt> map)
        {
            SetWinStatus(cfg, sheet, program, map, "CRS05-01", "I16", "I17");
            SetWinStatus(cfg, sheet, program, map, "CRS05-02", "I20", "I21");
            SetWinStatus(cfg, sheet, program, map, "CRS05-03", "I24", "I25");
            SetWinStatus(cfg, sheet, program, map, "CRS05-04", "O28", "O29");

            SetRepechagePlayers(cfg, sheet, program, map, "RNK12", "O20", "");
            SetRepechagePlayers(cfg, sheet, program, map, "RNK34", "O32", "O36");

            SetWinStatus(cfg, sheet, program, map, "RNK12", "O24", "O25");
            SetWinStatus(cfg, sheet, program, map, "RNK34", "O34", "O35");
        }

        private void ResetWinStatus002Robin(TConfig cfg, Spire.Xls.Worksheet sheet, TProgram program, Dictionary<string, TEvt> map)
        {
            SetWinStatus(cfg, sheet, program, map, "RBN2-01", "I15", "I16");
            SetWinStatus(cfg, sheet, program, map, "RBN2-02", "I19", "I20");
            SetWinStatus(cfg, sheet, program, map, "RBN2-03", "I23", "I24");
        }

        private void ResetWinStatus003Robin(TConfig cfg, Spire.Xls.Worksheet sheet, TProgram program, Dictionary<string, TEvt> map)
        {
            SetWinStatus(cfg, sheet, program, map, "RBN3-01", "I15", "I16");
            SetWinStatus(cfg, sheet, program, map, "RBN3-02", "I19", "I20");
            SetWinStatus(cfg, sheet, program, map, "RBN3-03", "I23", "I24");
        }

        private void ResetWinStatus004Robin(TConfig cfg, Spire.Xls.Worksheet sheet, TProgram program, Dictionary<string, TEvt> map)
        {
            SetWinStatus(cfg, sheet, program, map, "RBN4-01", "I15", "I16");
            SetWinStatus(cfg, sheet, program, map, "RBN4-02", "I19", "I20");
            SetWinStatus(cfg, sheet, program, map, "RBN4-03", "I23", "I24");
            SetWinStatus(cfg, sheet, program, map, "RBN4-04", "I27", "I28");
            SetWinStatus(cfg, sheet, program, map, "RBN4-05", "I31", "I32");
            SetWinStatus(cfg, sheet, program, map, "RBN4-06", "I35", "I36");
        }

        private void ResetWinStatus005Robin(TConfig cfg, Spire.Xls.Worksheet sheet, TProgram program, Dictionary<string, TEvt> map)
        {
            SetWinStatus(cfg, sheet, program, map, "RBN5-01", "I15", "I16");
            SetWinStatus(cfg, sheet, program, map, "RBN5-02", "I19", "I20");
            SetWinStatus(cfg, sheet, program, map, "RBN5-03", "I23", "I24");
            SetWinStatus(cfg, sheet, program, map, "RBN5-04", "I27", "I28");
            SetWinStatus(cfg, sheet, program, map, "RBN5-05", "I31", "I32");
            SetWinStatus(cfg, sheet, program, map, "RBN5-06", "I35", "I36");
            SetWinStatus(cfg, sheet, program, map, "RBN5-07", "I39", "I40");
            SetWinStatus(cfg, sheet, program, map, "RBN5-08", "I43", "I44");
            SetWinStatus(cfg, sheet, program, map, "RBN5-09", "I47", "I48");
            SetWinStatus(cfg, sheet, program, map, "RBN5-10", "I51", "I52");
        }

        private void ResetWinStatus008(TConfig cfg, Spire.Xls.Worksheet sheet, TProgram program, Dictionary<string, TEvt> map)
        {
            SetWinStatus(cfg, sheet, program, map, "M008-01", "F6", "F7");
            SetWinStatus(cfg, sheet, program, map, "M008-02", "F10", "F11");
            SetWinStatus(cfg, sheet, program, map, "M008-03", "F14", "F15");
            SetWinStatus(cfg, sheet, program, map, "M008-04", "F18", "F19");

            SetWinStatus(cfg, sheet, program, map, "M004-01", "G8", "G9");
            SetWinStatus(cfg, sheet, program, map, "M004-02", "G16", "G17");

            SetWinStatus(cfg, sheet, program, map, "M002-01", "G12", "G13");


            SetRepechagePlayers(cfg, sheet, program, map, "R008-01", "C22", "C24");
            SetRepechagePlayers(cfg, sheet, program, map, "R008-02", "C28", "C30");

            SetRepechagePlayers(cfg, sheet, program, map, "R004-01", "", "F27");
            SetRepechagePlayers(cfg, sheet, program, map, "R004-02", "", "F33");

            SetWinStatus(cfg, sheet, program, map, "R008-01", "F23", "F24");
            SetWinStatus(cfg, sheet, program, map, "R008-02", "F29", "F30");

            SetWinStatus(cfg, sheet, program, map, "R004-01", "G24", "G26");
            SetWinStatus(cfg, sheet, program, map, "R004-02", "G30", "G32");
        }

        private void ResetWinStatus016(TConfig cfg, Spire.Xls.Worksheet sheet, TProgram program, Dictionary<string, TEvt> map)
        {
            SetWinStatus(cfg, sheet, program, map, "M016-01", "F6", "F7");
            SetWinStatus(cfg, sheet, program, map, "M016-02", "F8", "F9");
            SetWinStatus(cfg, sheet, program, map, "M016-03", "F12", "F13");
            SetWinStatus(cfg, sheet, program, map, "M016-04", "F14", "F15");
            SetWinStatus(cfg, sheet, program, map, "M016-05", "F18", "F19");
            SetWinStatus(cfg, sheet, program, map, "M016-06", "F20", "F21");
            SetWinStatus(cfg, sheet, program, map, "M016-07", "F24", "F25");
            SetWinStatus(cfg, sheet, program, map, "M016-08", "F26", "F27");

            SetWinStatus(cfg, sheet, program, map, "M008-01", "G7", "G8");
            SetWinStatus(cfg, sheet, program, map, "M008-02", "G13", "G14");
            SetWinStatus(cfg, sheet, program, map, "M008-03", "G19", "G20");
            SetWinStatus(cfg, sheet, program, map, "M008-04", "G25", "G26");

            SetWinStatus(cfg, sheet, program, map, "M004-01", "H10", "H11");
            SetWinStatus(cfg, sheet, program, map, "M004-02", "H22", "H23");

            SetWinStatus(cfg, sheet, program, map, "M002-01", "H16", "H17");


            SetRepechagePlayers(cfg, sheet, program, map, "R008-01", "C30", "C32");
            SetRepechagePlayers(cfg, sheet, program, map, "R008-02", "C36", "C38");

            SetRepechagePlayers(cfg, sheet, program, map, "R004-01", "", "F35");
            SetRepechagePlayers(cfg, sheet, program, map, "R004-02", "", "F41");

            SetWinStatus(cfg, sheet, program, map, "R008-01", "F31", "F32");
            SetWinStatus(cfg, sheet, program, map, "R008-02", "F37", "F38");

            SetWinStatus(cfg, sheet, program, map, "R004-01", "G32", "G34");
            SetWinStatus(cfg, sheet, program, map, "R004-02", "G38", "G40");
        }

        private void ResetWinStatus032(TConfig cfg, Spire.Xls.Worksheet sheet, TProgram program, Dictionary<string, TEvt> map)
        {
            SetWinStatus(cfg, sheet, program, map, "M032-01", "F6", "F7");
            SetWinStatus(cfg, sheet, program, map, "M032-02", "F8", "F9");
            SetWinStatus(cfg, sheet, program, map, "M032-03", "F10", "F11");
            SetWinStatus(cfg, sheet, program, map, "M032-04", "F12", "F13");
            SetWinStatus(cfg, sheet, program, map, "M032-05", "F16", "F17");
            SetWinStatus(cfg, sheet, program, map, "M032-06", "F18", "F19");
            SetWinStatus(cfg, sheet, program, map, "M032-07", "F20", "F21");
            SetWinStatus(cfg, sheet, program, map, "M032-08", "F22", "F23");
            SetWinStatus(cfg, sheet, program, map, "M032-09", "F26", "F27");
            SetWinStatus(cfg, sheet, program, map, "M032-10", "F28", "F29");
            SetWinStatus(cfg, sheet, program, map, "M032-11", "F30", "F31");
            SetWinStatus(cfg, sheet, program, map, "M032-12", "F32", "F33");
            SetWinStatus(cfg, sheet, program, map, "M032-13", "F36", "F37");
            SetWinStatus(cfg, sheet, program, map, "M032-14", "F38", "F39");
            SetWinStatus(cfg, sheet, program, map, "M032-15", "F40", "F41");
            SetWinStatus(cfg, sheet, program, map, "M032-16", "F42", "F43");

            SetWinStatus(cfg, sheet, program, map, "M016-01", "G7", "G8");
            SetWinStatus(cfg, sheet, program, map, "M016-02", "G11", "G12");
            SetWinStatus(cfg, sheet, program, map, "M016-03", "G17", "G18");
            SetWinStatus(cfg, sheet, program, map, "M016-04", "G21", "G22");
            SetWinStatus(cfg, sheet, program, map, "M016-05", "G27", "G28");
            SetWinStatus(cfg, sheet, program, map, "M016-06", "G31", "G32");
            SetWinStatus(cfg, sheet, program, map, "M016-07", "G37", "G38");
            SetWinStatus(cfg, sheet, program, map, "M016-08", "G41", "G42");

            SetWinStatus(cfg, sheet, program, map, "M008-01", "H9", "H10");
            SetWinStatus(cfg, sheet, program, map, "M008-02", "H19", "H20");
            SetWinStatus(cfg, sheet, program, map, "M008-03", "H29", "H30");
            SetWinStatus(cfg, sheet, program, map, "M008-04", "H39", "H40");

            SetWinStatus(cfg, sheet, program, map, "M004-01", "I13", "I15");
            SetWinStatus(cfg, sheet, program, map, "M004-02", "I33", "I35");

            SetWinStatus(cfg, sheet, program, map, "M002-01", "I23", "I25");


            SetRepechagePlayers(cfg, sheet, program, map, "R008-01", "B46", "B48");
            SetRepechagePlayers(cfg, sheet, program, map, "R008-02", "B51", "B53");

            SetRepechagePlayers(cfg, sheet, program, map, "R004-01", "", "F49");
            SetRepechagePlayers(cfg, sheet, program, map, "R004-02", "", "F54");

            SetWinStatus(cfg, sheet, program, map, "R008-01", "F47", "F48");
            SetWinStatus(cfg, sheet, program, map, "R008-02", "F52", "F53");

            SetWinStatus(cfg, sheet, program, map, "R004-01", "G48", "G49");
            SetWinStatus(cfg, sheet, program, map, "R004-02", "G53", "G54");
        }

        private void SetRepechagePlayers(TConfig cfg, Spire.Xls.Worksheet sheet, TProgram program, Dictionary<string, TEvt> map, string in_fight_id, string c1, string c2)
        {
            var evt = FindEvt(cfg, map, in_fight_id);
            if (c1 != "")
            {
                sheet[c1].Text = GetWinnerName(cfg, program, evt, evt.f1);
            }
            if (c2 != "")
            {
                sheet[c2].Text = GetWinnerName(cfg, program, evt, evt.f2);
            }
        }

        private void SetWinStatus(TConfig cfg, Spire.Xls.Worksheet sheet, TProgram program, Dictionary<string, TEvt> map, string in_fight_id, string c1, string c2)
        {
            var evt = FindEvt(cfg, map, in_fight_id);
            var winner = GetWinnerFoot(cfg, evt);
            sheet[c1].Text = GetWinnerName(cfg, program, evt, winner);
            sheet[c2].Text = GetFoughtInfo(cfg, program, evt);
        }

        private string GetFoughtInfo(TConfig cfg, TProgram program, TEvt evt)
        {
            if (!evt.has_fought) return "";
            var f1_p = evt.f1.Value.getProperty("in_points", "0").PadLeft(2, '0');
            var f1_s = evt.f1.Value.getProperty("in_correct_count", "0");
            var f2_p = evt.f2.Value.getProperty("in_points", "0").PadLeft(2, '0'); ;
            var f2_s = evt.f2.Value.getProperty("in_correct_count", "0");
            var time = GetWinLocalTime(cfg, program, evt);
            return f1_p + " s" + f1_s + "/" + f2_p + " s" + f2_s + "[" + time + "]";
        }

        private string GetWinLocalTime(TConfig cfg, TProgram program, TEvt evt)
        {
            var result = "";
            var in_win_local_time = evt.Value.getProperty("in_win_local_time", "");
            if (in_win_local_time == "") return "00:00";

            if (in_win_local_time.Contains("GS"))
            {
                //需拆開
                var a = in_win_local_time.Replace("GS", "").Trim().Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
                var b1 = a.Length > 0 ? GetInt(a[0]) : 0;
                var b2 = a.Length > 1 ? GetInt(a[1]) : 0;
                var gsSeconds = b1 * 60 + b2;
                int total = program.fightSeconds + gsSeconds;
                int s = total % 60;
                int m = (total - s) / 60;
                result = m.ToString().PadLeft(2, '0') + ":" + s.ToString().PadLeft(2, '0');
            }
            else
            {
                var a = in_win_local_time.Replace("GS", "").Trim().Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
                var b1 = a.Length > 0 ? GetInt(a[0]) : 0;
                var b2 = a.Length > 1 ? GetInt(a[1]) : 0;
                var sec1 = b1 * 60 + b2;
                var sec2 = program.fightSeconds - sec1;
                var c1 = sec2 / 60;
                var c2 = sec2 % 60;
                result = c1.ToString().PadLeft(2, '0') + ":" + c2.ToString().PadLeft(2, '0');
            }
            return result;
        }

        private string GetWinnerName(TConfig cfg, TProgram program, TEvt evt, TDtl dtl)
        {
            if (dtl.og == "") return "";
            var in_family_name = dtl.Value.getProperty("in_family_name", "");
            var in_given_name = dtl.Value.getProperty("in_given_name", "");
            if (in_given_name == "") return in_family_name + " (" + dtl.og + ")";
            return in_family_name + " " + in_given_name[0] + ". (" + dtl.og + ")";
        }

        private TDtl GetWinnerFoot(TConfig cfg, TEvt evt)
        {
            if (evt.win_sign_no == evt.f1_sign_no)
            {
                return evt.f1;
            }
            else if (evt.win_sign_no == evt.f2_sign_no)
            {
                return evt.f2;
            }
            else
            {
                return new TDtl { nm = "", no = "", og = "", sign = "", sno = "", Value = cfg.inn.newItem() };
            }
        }
        #endregion WinStatus

        private void ExportPlayers002Robin(TConfig cfg, Spire.Xls.Worksheet sheet, TProgram program, Dictionary<string, TEvt> map)
        {
            var player1 = FindPlayer(cfg, program.players, "1");
            var player2 = FindPlayer(cfg, program.players, "2");

            SetRobinBox(cfg, sheet, program, player1, new List<int> { 7, 15, 19, 23 });
            SetRobinBox(cfg, sheet, program, player2, new List<int> { 8, 16, 20, 24 });
        }

        private void ExportPlayers003Robin(TConfig cfg, Spire.Xls.Worksheet sheet, TProgram program, Dictionary<string, TEvt> map)
        {
            var player1 = FindPlayer(cfg, program.players, "1");
            var player2 = FindPlayer(cfg, program.players, "2");
            var player3 = FindPlayer(cfg, program.players, "3");

            SetRobinBox(cfg, sheet, program, player1, new List<int> { 7, 15, 19 });
            SetRobinBox(cfg, sheet, program, player2, new List<int> { 8, 16, 23 });
            SetRobinBox(cfg, sheet, program, player3, new List<int> { 9, 20, 24 });
        }

        private void ExportPlayers004Robin(TConfig cfg, Spire.Xls.Worksheet sheet, TProgram program, Dictionary<string, TEvt> map)
        {
            var player1 = FindPlayer(cfg, program.players, "1");
            var player2 = FindPlayer(cfg, program.players, "2");
            var player3 = FindPlayer(cfg, program.players, "3");
            var player4 = FindPlayer(cfg, program.players, "4");

            SetRobinBox(cfg, sheet, program, player1, new List<int> { 7, 15, 23, 31 });
            SetRobinBox(cfg, sheet, program, player2, new List<int> { 8, 16, 27, 35 });
            SetRobinBox(cfg, sheet, program, player3, new List<int> { 9, 19, 24, 36 });
            SetRobinBox(cfg, sheet, program, player4, new List<int> { 10, 20, 28, 32 });
        }

        private void ExportPlayers005Robin(TConfig cfg, Spire.Xls.Worksheet sheet, TProgram program, Dictionary<string, TEvt> map)
        {
            var player1 = FindPlayer(cfg, program.players, "1");
            var player2 = FindPlayer(cfg, program.players, "2");
            var player3 = FindPlayer(cfg, program.players, "3");
            var player4 = FindPlayer(cfg, program.players, "4");
            var player5 = FindPlayer(cfg, program.players, "5");

            SetRobinBox(cfg, sheet, program, player1, new List<int> { 7, 15, 23, 35, 47 });
            SetRobinBox(cfg, sheet, program, player2, new List<int> { 8, 16, 27, 39, 51 });
            SetRobinBox(cfg, sheet, program, player3, new List<int> { 9, 19, 28, 36, 43 });
            SetRobinBox(cfg, sheet, program, player4, new List<int> { 10, 20, 31, 40, 48 });
            SetRobinBox(cfg, sheet, program, player5, new List<int> { 11, 24, 32, 44, 52 });
        }

        private void SetRobinBox(TConfig cfg, Spire.Xls.Worksheet sheet, TProgram program, TDtl player, List<int> list)
        {
            foreach (var ri in list)
            {
                sheet["E" + ri].Text = player.nm;
                sheet["F" + ri].Text = player.og;
                SetFlagPicture(cfg, sheet, program, ri, 4, player.og);
            }
        }

        private void ExportPlayers004Cross(TConfig cfg, Spire.Xls.Worksheet sheet, TProgram program, Dictionary<string, TEvt> map)
        {
            var player1 = FindPlayer(cfg, program.players, "1");
            var player2 = FindPlayer(cfg, program.players, "2");
            var player3 = FindPlayer(cfg, program.players, "3");
            var player4 = FindPlayer(cfg, program.players, "4");


            sheet["C6"].Text = player1.nm;
            sheet["D6"].Text = player1.og;
            SetFlagPicture(cfg, sheet, program, 6, 2, player1.og);

            sheet["C7"].Text = player3.nm;
            sheet["D7"].Text = player3.og;
            SetFlagPicture(cfg, sheet, program, 7, 2, player3.og);


            sheet["C10"].Text = player2.nm;
            sheet["D10"].Text = player2.og;
            SetFlagPicture(cfg, sheet, program, 10, 2, player2.og);

            sheet["C11"].Text = player4.nm;
            sheet["D11"].Text = player4.og;
            SetFlagPicture(cfg, sheet, program, 11, 2, player4.og);

            //A2 vs B2
            var evt = FindEvt(cfg, map, "RNK34");
            sheet["C16"].Text = evt.f1.nm;
            sheet["D16"].Text = evt.f1.og;
            SetFlagPicture(cfg, sheet, program, 16, 2, evt.f1.og);

            sheet["C17"].Text = evt.f2.nm;
            sheet["D17"].Text = evt.f2.og;
            SetFlagPicture(cfg, sheet, program, 17, 2, evt.f2.og);
        }

        private void ExportPlayers005Cross(TConfig cfg, Spire.Xls.Worksheet sheet, TProgram program, Dictionary<string, TEvt> map)
        {
            var player1 = FindPlayer(cfg, program.players, "1");
            var player2 = FindPlayer(cfg, program.players, "2");
            var player3 = FindPlayer(cfg, program.players, "3");
            var player4 = FindPlayer(cfg, program.players, "4");
            var player5 = FindPlayer(cfg, program.players, "5");

            // A Group
            sheet["E7"].Text = player1.nm;
            sheet["F7"].Text = player1.og;
            SetFlagPicture(cfg, sheet, program, 7, 4, player1.og);

            sheet["E8"].Text = player3.nm;
            sheet["F8"].Text = player3.og;
            SetFlagPicture(cfg, sheet, program, 8, 4, player3.og);

            sheet["E9"].Text = player5.nm;
            sheet["F9"].Text = player5.og;
            SetFlagPicture(cfg, sheet, program, 9, 4, player5.og);

            // B Group
            sheet["J12"].Text = player2.nm;
            sheet["O12"].Text = player2.og;
            SetFlagPicture(cfg, sheet, program, 12, 9, player2.og);

            sheet["J13"].Text = player4.nm;
            sheet["O13"].Text = player4.og;
            SetFlagPicture(cfg, sheet, program, 13, 9, player4.og);

            // Event 1: 1-3
            sheet["E16"].Text = player1.nm;
            sheet["F16"].Text = player1.og;
            SetFlagPicture(cfg, sheet, program, 16, 4, player1.og);
            sheet["E17"].Text = player3.nm;
            sheet["F17"].Text = player3.og;
            SetFlagPicture(cfg, sheet, program, 17, 4, player3.og);

            // Event 3: 1-5
            sheet["E20"].Text = player1.nm;
            sheet["F20"].Text = player1.og;
            SetFlagPicture(cfg, sheet, program, 20, 4, player1.og);
            sheet["E21"].Text = player5.nm;
            sheet["F21"].Text = player5.og;
            SetFlagPicture(cfg, sheet, program, 21, 4, player5.og);

            // Event 4: 3-5
            sheet["E24"].Text = player3.nm;
            sheet["E24"].Text = player3.og;
            SetFlagPicture(cfg, sheet, program, 24, 4, player3.og);
            sheet["E25"].Text = player5.nm;
            sheet["F25"].Text = player5.og;
            SetFlagPicture(cfg, sheet, program, 25, 4, player5.og);

            // Event 2: 2-4
            sheet["E28"].Text = player2.nm;
            sheet["F28"].Text = player2.og;
            SetFlagPicture(cfg, sheet, program, 28, 4, player2.og);
            sheet["E29"].Text = player4.nm;
            sheet["F29"].Text = player4.og;
            SetFlagPicture(cfg, sheet, program, 29, 4, player4.og);
        }

        private void ExportPlayers008(TConfig cfg, Spire.Xls.Worksheet sheet, TProgram program, Dictionary<string, TEvt> map)
        {
            var ridx = 6;
            for (var i = 1; i <= 4; i++)
            {
                var key = "M008-" + i.ToString().PadLeft(2, '0');
                var evt = FindEvt(cfg, map, key);

                var r1 = ridx + 0;
                var r2 = ridx + 1;

                sheet["C" + r1].Text = evt.f1.nm;
                sheet["D" + r1].Text = evt.f1.og;
                SetFlagPicture(cfg, sheet, program, r1, 2, evt.f1.og);

                sheet["C" + r2].Text = evt.f2.nm;
                sheet["D" + r2].Text = evt.f2.og;
                SetFlagPicture(cfg, sheet, program, r2, 2, evt.f2.og);

                ridx += 4;
            }
        }

        private void ExportPlayers016(TConfig cfg, Spire.Xls.Worksheet sheet, TProgram program, Dictionary<string, TEvt> map)
        {
            var ridx = 6;
            for (var i = 1; i <= 8; i++)
            {
                var key = "M016-" + i.ToString().PadLeft(2, '0');
                var evt = FindEvt(cfg, map, key);

                var r1 = ridx + 0;
                var r2 = ridx + 1;

                sheet["C" + r1].Text = evt.f1.nm;
                sheet["D" + r1].Text = evt.f1.og;
                SetFlagPicture(cfg, sheet, program, r1, 2, evt.f1.og);

                sheet["C" + r2].Text = evt.f2.nm;
                sheet["D" + r2].Text = evt.f2.og;
                SetFlagPicture(cfg, sheet, program, r2, 2, evt.f2.og);

                if (i % 2 == 0)
                {
                    ridx += 4;
                }
                else
                {
                    ridx += 2;
                }
            }
        }

        private void ExportPlayers032(TConfig cfg, Spire.Xls.Worksheet sheet, TProgram program, Dictionary<string, TEvt> map)
        {
            var ridx = 6;
            for (var i = 1; i <= 16; i++)
            {
                var key = "M032-" + i.ToString().PadLeft(2, '0');
                var evt = FindEvt(cfg, map, key);

                var r1 = ridx + 0;
                var r2 = ridx + 1;

                sheet["C" + r1].Text = evt.f1.nm;
                sheet["D" + r1].Text = evt.f1.og;
                SetFlagPicture(cfg, sheet, program, r1, 2, evt.f1.og);

                sheet["C" + r2].Text = evt.f2.nm;
                sheet["D" + r2].Text = evt.f2.og;
                SetFlagPicture(cfg, sheet, program, r2, 2, evt.f2.og);

                if (i % 4 == 0)
                {
                    ridx += 4;
                }
                else
                {
                    ridx += 2;
                }
            }
        }

        private void ResetTreeNo003Cross(TConfig cfg, Spire.Xls.Worksheet sheet, TProgram program, Dictionary<string, TEvt> map)
        {
            if (sheet.OvalShapes == null || sheet.OvalShapes.Count <= 0) return;

            var shapes = sheet.OvalShapes;
            for (var i = 0; i < shapes.Count; i++)
            {
                var shape = shapes[i];
                var text = shape.Text;
                if (string.IsNullOrWhiteSpace(text)) continue;

                var pos = "";
                var evt = default(TEvt);
                switch (text)
                {
                    case "1": evt = FindEvt(cfg, map, "CRS03-01"); pos = "I7"; break;
                    case "2": evt = FindEvt(cfg, map, "CRS03-02"); pos = "J7"; break;
                    case "3": evt = FindEvt(cfg, map, "CRS03-03"); pos = "J8"; break;
                    case "4": evt = FindEvt(cfg, map, "RNK12"); pos = ""; break;

                    default: evt = FindEvt(cfg, map, ""); pos = ""; break;
                }

                shape.Text = evt.tno;
                if (pos != "")
                {
                    sheet[pos].Text = evt.tno;
                    sheet[pos].Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                }
            }
        }

        private void ResetTreeNo004Cross(TConfig cfg, Spire.Xls.Worksheet sheet, TProgram program, Dictionary<string, TEvt> map)
        {
            if (sheet.OvalShapes == null || sheet.OvalShapes.Count <= 0) return;

            var shapes = sheet.OvalShapes;
            for (var i = 0; i < shapes.Count; i++)
            {
                var shape = shapes[i];
                var text = shape.Text;
                if (string.IsNullOrWhiteSpace(text)) continue;

                var pos = "";
                var evt = default(TEvt);
                switch (text)
                {
                    case "1": evt = FindEvt(cfg, map, "CRS04-01"); pos = ""; break;
                    case "2": evt = FindEvt(cfg, map, "CRS04-02"); pos = ""; break;
                    case "3": evt = FindEvt(cfg, map, "RNK34"); pos = ""; break;
                    case "4": evt = FindEvt(cfg, map, "RNK12"); pos = ""; break;

                    default: evt = FindEvt(cfg, map, ""); pos = ""; break;
                }

                shape.Text = evt.tno;
                if (pos != "")
                {
                    sheet[pos].Text = evt.tno;
                    sheet[pos].Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                }
            }
        }

        private void ResetTreeNo005Cross(TConfig cfg, Spire.Xls.Worksheet sheet, TProgram program, Dictionary<string, TEvt> map)
        {
            if (sheet.OvalShapes == null || sheet.OvalShapes.Count <= 0) return;

            var shapes = sheet.OvalShapes;
            for (var i = 0; i < shapes.Count; i++)
            {
                var shape = shapes[i];
                var text = shape.Text;
                if (string.IsNullOrWhiteSpace(text)) continue;

                var pos = "";
                var evt = default(TEvt);
                switch (text)
                {
                    case "1": evt = FindEvt(cfg, map, "CRS05-01"); pos = "I7"; break;
                    case "2": evt = FindEvt(cfg, map, "CRS05-04"); pos = "R12"; break;
                    case "3": evt = FindEvt(cfg, map, "CRS05-02"); pos = "J7"; break;
                    case "4": evt = FindEvt(cfg, map, "CRS05-03"); pos = "J8"; break;
                    case "5": evt = FindEvt(cfg, map, "RNK34"); pos = ""; break;
                    case "6": evt = FindEvt(cfg, map, "RNK12"); pos = ""; break;

                    default: evt = FindEvt(cfg, map, ""); pos = ""; break;
                }

                shape.Text = evt.tno;
                if (pos != "")
                {
                    sheet[pos].Text = evt.tno;
                    sheet[pos].Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                }
            }
        }

        private void ResetTreeNo002Robin(TConfig cfg, Spire.Xls.Worksheet sheet, TProgram program, Dictionary<string, TEvt> map)
        {
            if (sheet.OvalShapes == null || sheet.OvalShapes.Count <= 0) return;

            var shapes = sheet.OvalShapes;
            for (var i = 0; i < shapes.Count; i++)
            {
                var shape = shapes[i];
                var text = shape.Text;
                if (string.IsNullOrWhiteSpace(text)) continue;

                var pos = "";
                var evt = default(TEvt);
                switch (text)
                {
                    case "1": evt = FindEvt(cfg, map, "RBN2-01"); pos = "I7"; break;
                    case "2": evt = FindEvt(cfg, map, "RBN2-02"); pos = "G8"; break;
                    case "3": evt = FindEvt(cfg, map, "RBN2-03"); pos = "J7"; break;

                    default: evt = FindEvt(cfg, map, ""); pos = ""; break;
                }

                shape.Text = evt.tno;
                if (pos != "")
                {
                    sheet[pos].Text = evt.tno;
                    sheet[pos].Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                }
            }
        }

        private void ResetTreeNo003Robin(TConfig cfg, Spire.Xls.Worksheet sheet, TProgram program, Dictionary<string, TEvt> map)
        {
            if (sheet.OvalShapes == null || sheet.OvalShapes.Count <= 0) return;

            var shapes = sheet.OvalShapes;
            for (var i = 0; i < shapes.Count; i++)
            {
                var shape = shapes[i];
                var text = shape.Text;
                if (string.IsNullOrWhiteSpace(text)) continue;

                var pos = "";
                var evt = default(TEvt);
                switch (text)
                {
                    case "1": evt = FindEvt(cfg, map, "RBN3-01"); pos = "I7"; break;
                    case "2": evt = FindEvt(cfg, map, "RBN3-02"); pos = "J7"; break;
                    case "3": evt = FindEvt(cfg, map, "RBN3-03"); pos = "J8"; break;

                    default: evt = FindEvt(cfg, map, ""); pos = ""; break;
                }

                shape.Text = evt.tno;
                if (pos != "")
                {
                    sheet[pos].Text = evt.tno;
                    sheet[pos].Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                }
            }
        }

        private void ResetTreeNo004Robin(TConfig cfg, Spire.Xls.Worksheet sheet, TProgram program, Dictionary<string, TEvt> map)
        {
            if (sheet.OvalShapes == null || sheet.OvalShapes.Count <= 0) return;

            var shapes = sheet.OvalShapes;
            for (var i = 0; i < shapes.Count; i++)
            {
                var shape = shapes[i];
                var text = shape.Text;
                if (string.IsNullOrWhiteSpace(text)) continue;

                var pos = "";
                var evt = default(TEvt);
                switch (text)
                {
                    case "1": evt = FindEvt(cfg, map, "RBN4-01"); pos = "I7"; break;
                    case "2": evt = FindEvt(cfg, map, "RBN4-02"); pos = "K9"; break;
                    case "3": evt = FindEvt(cfg, map, "RBN4-03"); pos = "J7"; break;
                    case "4": evt = FindEvt(cfg, map, "RBN4-04"); pos = "K8"; break;
                    case "5": evt = FindEvt(cfg, map, "RBN4-05"); pos = "K7"; break;
                    case "6": evt = FindEvt(cfg, map, "RBN4-06"); pos = "J8"; break;

                    default: evt = FindEvt(cfg, map, ""); pos = ""; break;
                }

                shape.Text = evt.tno;
                if (pos != "")
                {
                    sheet[pos].Text = evt.tno;
                    sheet[pos].Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                }
            }
        }

        private void ResetTreeNo005Robin(TConfig cfg, Spire.Xls.Worksheet sheet, TProgram program, Dictionary<string, TEvt> map)
        {
            if (sheet.OvalShapes == null || sheet.OvalShapes.Count <= 0) return;

            var shapes = sheet.OvalShapes;
            for (var i = 0; i < shapes.Count; i++)
            {
                var shape = shapes[i];
                var text = shape.Text;
                if (string.IsNullOrWhiteSpace(text)) continue;

                var pos = "";
                var evt = default(TEvt);
                switch (text)
                {
                    case "1": evt = FindEvt(cfg, map, "RBN5-01"); pos = "I7"; break;
                    case "2": evt = FindEvt(cfg, map, "RBN5-02"); pos = "K9"; break;
                    case "3": evt = FindEvt(cfg, map, "RBN5-03"); pos = "L7"; break;
                    case "4": evt = FindEvt(cfg, map, "RBN5-04"); pos = "J8"; break;
                    case "5": evt = FindEvt(cfg, map, "RBN5-05"); pos = "L10"; break;
                    case "6": evt = FindEvt(cfg, map, "RBN5-06"); pos = "J7"; break;
                    case "7": evt = FindEvt(cfg, map, "RBN5-07"); pos = "K8"; break;
                    case "8": evt = FindEvt(cfg, map, "RBN5-08"); pos = "L9"; break;
                    case "9": evt = FindEvt(cfg, map, "RBN5-09"); pos = "K7"; break;
                    case "10": evt = FindEvt(cfg, map, "RBN5-10"); pos = "L8"; break;

                    default: evt = FindEvt(cfg, map, ""); pos = ""; break;
                }

                shape.Text = evt.tno;
                if (pos != "")
                {
                    sheet[pos].Text = evt.tno;
                    sheet[pos].Style.HorizontalAlignment = Spire.Xls.HorizontalAlignType.Center;
                }
            }
        }

        private void ResetTreeNo008(TConfig cfg, Spire.Xls.Worksheet sheet, TProgram program, Dictionary<string, TEvt> map)
        {
            if (sheet.OvalShapes == null || sheet.OvalShapes.Count <= 0) return;

            var shapes = sheet.OvalShapes;
            for (var i = 0; i < shapes.Count; i++)
            {
                var shape = shapes[i];
                var text = shape.Text;
                if (string.IsNullOrWhiteSpace(text)) continue;

                var evt = default(TEvt);
                switch (text)
                {
                    case "1": evt = FindEvt(cfg, map, "M008-01"); break;
                    case "2": evt = FindEvt(cfg, map, "M008-02"); break;
                    case "3": evt = FindEvt(cfg, map, "M008-03"); break;
                    case "4": evt = FindEvt(cfg, map, "M008-04"); break;

                    case "5": evt = FindEvt(cfg, map, "R008-01"); break;
                    case "6": evt = FindEvt(cfg, map, "R008-02"); break;

                    case "7": evt = FindEvt(cfg, map, "M004-01"); break;
                    case "8": evt = FindEvt(cfg, map, "M004-02"); break;

                    case "9": evt = FindEvt(cfg, map, "R004-01"); break;
                    case "10": evt = FindEvt(cfg, map, "R004-02"); break;

                    case "11": evt = FindEvt(cfg, map, "M002-01"); break;

                    default: evt = FindEvt(cfg, map, ""); break;
                }

                shape.Text = evt.tno;
            }
        }

        private void ResetTreeNo016(TConfig cfg, Spire.Xls.Worksheet sheet, TProgram program, Dictionary<string, TEvt> map)
        {
            if (sheet.OvalShapes == null || sheet.OvalShapes.Count <= 0) return;

            var shapes = sheet.OvalShapes;
            for (var i = 0; i < shapes.Count; i++)
            {
                var shape = shapes[i];
                var text = shape.Text;
                if (string.IsNullOrWhiteSpace(text)) continue;

                var evt = default(TEvt);
                switch (text)
                {
                    case "1": evt = FindEvt(cfg, map, "M016-01"); break;
                    case "2": evt = FindEvt(cfg, map, "M016-02"); break;
                    case "3": evt = FindEvt(cfg, map, "M016-03"); break;
                    case "4": evt = FindEvt(cfg, map, "M016-04"); break;
                    case "5": evt = FindEvt(cfg, map, "M016-05"); break;
                    case "6": evt = FindEvt(cfg, map, "M016-06"); break;
                    case "7": evt = FindEvt(cfg, map, "M016-07"); break;
                    case "8": evt = FindEvt(cfg, map, "M016-08"); break;

                    case "9": evt = FindEvt(cfg, map, "M008-01"); break;
                    case "10": evt = FindEvt(cfg, map, "M008-02"); break;
                    case "11": evt = FindEvt(cfg, map, "M008-03"); break;
                    case "12": evt = FindEvt(cfg, map, "M008-04"); break;

                    case "13": evt = FindEvt(cfg, map, "R008-01"); break;
                    case "14": evt = FindEvt(cfg, map, "R008-02"); break;

                    case "15": evt = FindEvt(cfg, map, "M004-01"); break;
                    case "16": evt = FindEvt(cfg, map, "M004-02"); break;

                    case "17": evt = FindEvt(cfg, map, "R004-01"); break;
                    case "18": evt = FindEvt(cfg, map, "R004-02"); break;

                    case "19": evt = FindEvt(cfg, map, "M002-01"); break;

                    default: evt = FindEvt(cfg, map, ""); break;
                }

                shape.Text = evt.tno;
            }
        }

        private void ResetTreeNo032(TConfig cfg, Spire.Xls.Worksheet sheet, TProgram program, Dictionary<string, TEvt> map)
        {
            if (sheet.OvalShapes == null || sheet.OvalShapes.Count <= 0) return;

            var shapes = sheet.OvalShapes;
            for (var i = 0; i < shapes.Count; i++)
            {
                var shape = shapes[i];
                var text = shape.Text;
                if (string.IsNullOrWhiteSpace(text)) continue;

                var evt = default(TEvt);
                switch (text)
                {
                    case "1": evt = FindEvt(cfg, map, "M032-01"); break;
                    case "2": evt = FindEvt(cfg, map, "M032-02"); break;
                    case "3": evt = FindEvt(cfg, map, "M032-03"); break;
                    case "4": evt = FindEvt(cfg, map, "M032-04"); break;
                    case "5": evt = FindEvt(cfg, map, "M032-05"); break;
                    case "6": evt = FindEvt(cfg, map, "M032-06"); break;
                    case "7": evt = FindEvt(cfg, map, "M032-07"); break;
                    case "8": evt = FindEvt(cfg, map, "M032-08"); break;
                    case "9": evt = FindEvt(cfg, map, "M032-09"); break;
                    case "10": evt = FindEvt(cfg, map, "M032-10"); break;
                    case "11": evt = FindEvt(cfg, map, "M032-11"); break;
                    case "12": evt = FindEvt(cfg, map, "M032-12"); break;
                    case "13": evt = FindEvt(cfg, map, "M032-13"); break;
                    case "14": evt = FindEvt(cfg, map, "M032-14"); break;
                    case "15": evt = FindEvt(cfg, map, "M032-15"); break;
                    case "16": evt = FindEvt(cfg, map, "M032-16"); break;

                    case "17": evt = FindEvt(cfg, map, "M016-01"); break;
                    case "18": evt = FindEvt(cfg, map, "M016-02"); break;
                    case "19": evt = FindEvt(cfg, map, "M016-03"); break;
                    case "20": evt = FindEvt(cfg, map, "M016-04"); break;
                    case "21": evt = FindEvt(cfg, map, "M016-05"); break;
                    case "22": evt = FindEvt(cfg, map, "M016-06"); break;
                    case "23": evt = FindEvt(cfg, map, "M016-07"); break;
                    case "24": evt = FindEvt(cfg, map, "M016-08"); break;

                    case "25": evt = FindEvt(cfg, map, "M008-01"); break;
                    case "26": evt = FindEvt(cfg, map, "M008-02"); break;
                    case "27": evt = FindEvt(cfg, map, "M008-03"); break;
                    case "28": evt = FindEvt(cfg, map, "M008-04"); break;

                    case "29": evt = FindEvt(cfg, map, "R008-01"); break;
                    case "30": evt = FindEvt(cfg, map, "R008-02"); break;

                    case "31": evt = FindEvt(cfg, map, "M004-01"); break;
                    case "32": evt = FindEvt(cfg, map, "M004-02"); break;

                    case "33": evt = FindEvt(cfg, map, "R004-01"); break;
                    case "34": evt = FindEvt(cfg, map, "R004-02"); break;

                    case "35": evt = FindEvt(cfg, map, "M002-01"); break;

                    default: evt = FindEvt(cfg, map, ""); break;
                }

                shape.Text = evt.tno;
            }
        }

        private void ResetEventSheetsTitleBox(TConfig cfg, Spire.Xls.Workbook book)
        {
            var sheet1 = book.Worksheets["gender_report"];
            ResetDrawSheetTitleBox(cfg, sheet1, "Event Report", cfg.draw_sheet_sub_title, "Event", "Report");
        }

        private void ResetDrawSheetTitleBox(TConfig cfg, Spire.Xls.Worksheet sheet, TProgram program, Dictionary<string, TEvt> map)
        {
            string title = cfg.mt_title;
            string sub = cfg.draw_sheet_sub_title;
            string weight = program.in_l3;
            string grade = program.in_l1 + " (" + program.in_team_count + ")";
            ResetDrawSheetTitleBox(cfg, sheet, title, sub, weight, grade);
        }

        //處理標題
        private void ResetDrawSheetTitleBox(TConfig cfg, Spire.Xls.Worksheet sheet, string title, string sub, string weight, string grade)
        {
            if (sheet.RectangleShapes == null || sheet.RectangleShapes.Count <= 0) return;

            var format = @"<Font Style=""FONT-WEIGHT: bold;FONT-FAMILY: Calibri;FONT-SIZE: {#w}pt;COLOR: #000000;TEXT-ALIGN: left;"">{#v}</Font>";
            //{#v}
            var shapes = sheet.RectangleShapes;
            for (var i = 0; i < shapes.Count; i++)
            {
                var shape = shapes[i];
                var text = ((Spire.Xls.Core.IPrstGeomShape)shape).Text;
                switch (text)
                {
                    case "title":
                        ((Spire.Xls.Core.IPrstGeomShape)shape).HtmlString = format.Replace("{#w}", "18").Replace("{#v}", title);
                        break;

                    case "address":
                        ((Spire.Xls.Core.IPrstGeomShape)shape).HtmlString = format.Replace("{#w}", "14").Replace("{#v}", sub);
                        break;

                    case "weight":
                        ((Spire.Xls.Core.IPrstGeomShape)shape).HtmlString = format.Replace("{#w}", "18").Replace("{#v}", weight);
                        break;

                    case "grade":
                        ((Spire.Xls.Core.IPrstGeomShape)shape).HtmlString = format.Replace("{#w}", "12").Replace("{#v}", grade);
                        break;
                }
            }
        }

        private void SetFlagPicture(TConfig cfg, Spire.Xls.Worksheet sheet, TProgram program, int topRow, int leftColumn, string og)
        {
            var fileName = @"C:\site\judo\Images\flagsXlsx\" + og + ".png";
            if (!System.IO.File.Exists(fileName)) return;

            var picture = sheet.Pictures.Add(topRow, leftColumn, fileName);
            picture.Width = 30;
            picture.Height = 16;
            picture.LeftColumnOffset = program.LeftColumnOffset;
            picture.TopRowOffset = program.TopRowOffset;
            picture.Line.ForeColor = System.Drawing.Color.Black;
        }

        private TEvt FindEvt(TConfig cfg, Dictionary<string, TEvt> map, string in_fight_id)
        {
            var result = default(TEvt);
            if (in_fight_id != "" && map.ContainsKey(in_fight_id))
            {
                result = map[in_fight_id];
            }
            else
            {
                result = new TEvt { Value = cfg.inn.newItem(), tno = "", f1_sign_no = "", f2_sign_no = "", win_status = "", has_fought = false, win_sign_no = "-999" };
            }

            if (result.f1 == null)
            {
                result.f1 = NewDetail(cfg, cfg.inn.newItem());
            }

            if (result.f2 == null)
            {
                result.f2 = NewDetail(cfg, cfg.inn.newItem());
            }

            return result;
        }

        private TDtl FindPlayer(TConfig cfg, Dictionary<string, TDtl> map, string in_sign_no)
        {
            if (map.ContainsKey(in_sign_no))
            {
                return map[in_sign_no];
            }
            else
            {
                return new TDtl
                {
                    no = in_sign_no,
                    sno = in_sign_no,
                    sign = in_sign_no,
                    og = "",
                    nm = "",
                    Value = cfg.inn.newItem(),
                };
            }
        }

        private void SetExportInfo(TConfig cfg, Item itmReturn)
        {
            var sql = @"SELECT in_title, in_battle_repechage, in_uniform_color, in_date_s, in_date_e FROM IN_MEETING WITH(NOLOCK) WHERE id = '" + cfg.meeting_id + "'";
            cfg.itmMeeting = cfg.inn.applySQL(sql);
            if (cfg.itmMeeting.isError() || cfg.itmMeeting.getResult() == "")
            {
                throw new Exception("找無該場賽事資料!");
            }

            cfg.mt_title = cfg.itmMeeting.getProperty("in_title", "");
            cfg.mt_battle_repechage = cfg.itmMeeting.getProperty("in_battle_repechage", "").ToLower();
            cfg.in_uniform_color = cfg.itmMeeting.getProperty("in_uniform_color", "");
            cfg.in_date_s = GetDtm(cfg.itmMeeting.getProperty("in_date_s", ""), 8);
            cfg.in_date_e = GetDtm(cfg.itmMeeting.getProperty("in_date_e", ""), 8);

            var target_name = "competition_nation";
            var itmXls = cfg.inn.applyMethod("In_Meeting_Excel_Path", "<in_name>" + target_name + "</in_name>");
            cfg.export_path = itmXls.getProperty("export_path", "");
            cfg.template_path = itmXls.getProperty("template_path", "");
            if (cfg.template_path == "")
            {
                throw new Exception("查無報表樣板!");
            }

            //道服顏色資訊
            cfg.itmColor = cfg.inn.newItem("In_Meeting");
            cfg.itmColor.setProperty("in_uniform_color", cfg.in_uniform_color);
            cfg.itmColor = cfg.itmColor.apply("in_meeting_uniform_color");

            cfg.export_type = itmReturn.getProperty("export_type", "").ToLower();

            cfg.draw_sheet_sub_title = GetMeetingVariable(cfg, "draw_sheet_sub_title");
            cfg.refresh_tree_no = GetMeetingVariable(cfg, "draw_sheet_tree_no") == "allocate";
        }

        private Item GetProgramItems(TConfig cfg, Item itmReturn)
        {
            var in_date = itmReturn.getProperty("in_date", "");
            var in_l1 = itmReturn.getProperty("in_l1", "");
            var in_l2 = itmReturn.getProperty("in_l2", "");
            var in_l3 = itmReturn.getProperty("in_l3", "");
            var program_id = itmReturn.getProperty("program_id", "");

            var sql = "";
            if (program_id != "")
            {
                sql = "SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + program_id + "'";
            }
            else if (in_date != "")
            {
                sql = "SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE in_meeting = '" + cfg.meeting_id + "'"
                    + " AND ISNULL(in_fight_day, '') = '" + in_date + "'"
                    + " ORDER BY in_sort_order";
            }
            else
            {
                sql = "SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE in_meeting = '" + cfg.meeting_id + "'"
                    + " AND ISNULL(in_l1, '') = N'" + in_l1 + "'"
                    + " AND ISNULL(in_l2, '') = N'" + in_l2 + "'"
                    + " AND ISNULL(in_l3, '') = N'" + in_l3 + "'"
                    + " ORDER BY in_sort_order";
            }


            var items = cfg.inn.applySQL(sql);

            if (items.isError() || items.getResult() == "")
            {
                throw new Exception("查無組別資料!");
            }

            return items;
        }

        private void CopyItemValue(Item itmA, Item itmB, string propA, string propB = "")
        {
            if (propB == "")
            {
                itmA.setProperty(propA, itmB.getProperty(propA, ""));
            }
            else
            {
                itmA.setProperty(propA, itmB.getProperty(propB, ""));
            }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }

            public string meeting_id { get; set; }
            public string scene { get; set; }
            public string facade { get; set; }

            public bool refresh_tree_no { get; set; }

            public Item itmMeeting { get; set; }
            public Item itmColor { get; set; }


            public string mt_title { get; set; }
            public string mt_battle_repechage { get; set; }
            public string in_uniform_color { get; set; }

            public string export_path { get; set; }
            public string template_path { get; set; }
            public string export_type { get; set; }
            public string draw_sheet_sub_title { get; set; }

            public string font_name { get; set; }

            public DateTime in_date_s { get; set; }
            public DateTime in_date_e { get; set; }
        }

        private class TProgram
        {
            public string id { get; set; }
            public string in_name { get; set; }
            public string in_short_name { get; set; }
            public string in_weight { get; set; }
            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }
            public string in_fight_day { get; set; }
            public string in_battle_type { get; set; }
            public string in_round_code { get; set; }
            public string in_team_count { get; set; }
            public string in_sign_time { get; set; }
            public string in_fight_time { get; set; }

            public string in_place { get; set; }

            public Item Value { get; set; }

            public string sheetKey { get; set; }
            public int teamCount { get; set; }
            public bool isRobin { get; set; }
            public string signTime { get; set; }

            public int fightSeconds { get; set; }

            public Dictionary<string, TEvt> map { get; set; }
            public Dictionary<string, TDtl> players { get; set; }

            public int LeftColumnOffset { get; set; }
            public int TopRowOffset { get; set; }

        }

        private TProgram MapOneProgram(TConfig cfg, Item itmProgram)
        {
            var result = new TProgram
            {
                id = itmProgram.getProperty("id", ""),
                in_name = itmProgram.getProperty("in_name", ""),
                in_short_name = itmProgram.getProperty("in_short_name", ""),
                in_weight = itmProgram.getProperty("in_weight", ""),
                in_l1 = itmProgram.getProperty("in_l1", ""),
                in_l2 = itmProgram.getProperty("in_l2", ""),
                in_l3 = itmProgram.getProperty("in_l3", ""),
                in_fight_day = itmProgram.getProperty("in_fight_day", ""),
                in_battle_type = itmProgram.getProperty("in_battle_type", ""),
                in_round_code = itmProgram.getProperty("in_round_code", "0"),
                in_team_count = itmProgram.getProperty("in_team_count", "0"),
                in_sign_time = itmProgram.getProperty("in_sign_time", ""),
                in_fight_time = itmProgram.getProperty("in_fight_time", "4:00"),
                Value = itmProgram,
            };

            result.in_place = GetProgramPlace(cfg, result.id);
            if (result.in_short_name == "")
            {
                result.in_short_name = result.id;
            }

            result.teamCount = GetInt(result.in_team_count);
            result.signTime = GetDtmStr(result.in_sign_time, 8, "dd.MM.yyyy");
            result.fightSeconds = GetFightSeconds(result.in_fight_time);

            var btype = result.in_battle_type;

            if (btype.Contains("SingleRoundRobin"))
            {
                result.isRobin = true;
                result.sheetKey = result.in_team_count;
            }
            else if (btype == "Cross")
            {
                result.isRobin = true;
                result.sheetKey = result.in_team_count + "_Cross";
            }
            else
            {
                result.sheetKey = result.in_round_code;
            }

            return result;
        }

        private int GetFightSeconds(string in_fight_time)
        {
            var df = 4 * 60;
            if (in_fight_time == "") return df;
            var arr = in_fight_time.Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
            if (arr.Length != 2) return df;

            var m = GetInt(arr[0]);
            var s = GetInt(arr[1]);
            return m * 60 + s;
        }

        private string GetProgramPlace(TConfig cfg, string program_id)
        {
            var item = cfg.inn.applySQL("SELECT TOP 1 in_place FROM IN_MEETING_ALLOCATION WITH(NOLOCK) WHERE in_program = '" + program_id + "'");
            if (item.isError() || item.getResult() == "")
            {
                return "";
            }
            else
            {
                return item.getProperty("in_place", "");
            }
        }

        private class TEvt
        {
            public string tno { get; set; }
            public string win_status { get; set; }
            public string win_sign_no { get; set; }
            public string f1_sign_no { get; set; }
            public string f2_sign_no { get; set; }
            public bool has_fought { get; set; }
            public TDtl f1 { get; set; }
            public TDtl f2 { get; set; }
            public Item Value { get; set; }
        }

        private class TDtl
        {
            public string no { get; set; }
            public string og { get; set; }
            public string nm { get; set; }
            public string sno { get; set; }
            public string sign { get; set; }
            public Item Value { get; set; }
        }

        private Dictionary<string, TEvt> MapEventList(TConfig cfg, TProgram program)
        {
            var result = new Dictionary<string, TEvt>();
            var itmDetails = GetMPEventDetails(cfg, program);
            var count = itmDetails.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var itmDetail = itmDetails.getItemByIndex(i);
                var in_fight_id = itmDetail.getProperty("in_fight_id", "");
                var in_tree_no = itmDetail.getProperty("in_tree_no", "");
                var in_sign_no = itmDetail.getProperty("in_sign_no", "");
                var in_sign_foot = itmDetail.getProperty("in_sign_foot", "");

                var in_win_status = itmDetail.getProperty("in_win_status", "");
                var in_win_time = itmDetail.getProperty("in_win_time", "");
                var in_win_sign_no = itmDetail.getProperty("in_win_sign_no", "");

                if (in_fight_id == "") continue;

                var evt = default(TEvt);
                if (result.ContainsKey(in_fight_id))
                {
                    evt = result[in_fight_id];
                }
                else
                {
                    evt = new TEvt
                    {
                        tno = in_tree_no,
                        Value = itmDetail,
                        win_status = in_win_status,
                        win_sign_no = in_win_sign_no,
                        f1_sign_no = "",
                        f2_sign_no = "",
                        has_fought = false,
                    };

                    if (evt.win_sign_no == "" || evt.win_sign_no == "0")
                    {
                        evt.win_sign_no = "-999";
                    }

                    switch (evt.win_status)
                    {
                        case "bypass":
                        case "cancel":
                        case "nofight":
                            evt.has_fought = false;
                            break;
                        default:
                            evt.has_fought = in_win_time != "";
                            break;
                    }

                    result.Add(in_fight_id, evt);
                }

                if (in_sign_foot == "1")
                {
                    evt.f1 = NewDetail(cfg, itmDetail);
                    evt.f1_sign_no = evt.f1.sign;
                }
                else if (in_sign_foot == "2")
                {
                    evt.f2 = NewDetail(cfg, itmDetail);
                    evt.f2_sign_no = evt.f2.sign;
                }
            }
            return result;
        }

        private TDtl NewDetail(TConfig cfg, Item itmDetail)
        {
            var result = new TDtl
            {
                no = itmDetail.getProperty("in_section_no", ""),
                og = itmDetail.getProperty("map_short_org", ""),
                nm = itmDetail.getProperty("in_name", ""),
                sign = itmDetail.getProperty("in_sign_no", ""),
                Value = itmDetail,
            };

            return result;
        }

        private Item GetMPEventDetails(TConfig cfg, TProgram program)
        {
            string sql = @"
                SELECT
                    t1.id
                    , t1.in_tree_name
                    , t1.in_tree_id
                    , t1.in_fight_id
                    , t1.in_tree_no
                    , t1.in_win_status
                    , t1.in_win_time
                    , t1.in_win_sign_no
                    , t1.in_win_local_time
                    , t2.in_sign_foot
                    , t2.in_sign_no
                    , t2.in_sign_bypass
                    , t2.in_points
                    , t2.in_correct_count
                    , t2.in_status
                    , t3.in_name AS 'site_name'
                    , t3.in_code AS 'site_code'
                    , t11.in_section_no
                    , t11.in_name
                    , t11.in_team
                    , t11.map_short_org
                    , t11.in_sign_time
                    , t11.in_weight_value
                    , t11.in_weight_message
                    , t11.in_final_rank
                    , t11.in_final_count
                    , t11.in_final_points
					, t12.in_family_name
					, t12.in_given_name
                FROM
                    IN_MEETING_PEVENT t1 WITH(NOLOCK)
                INNER JOIN
                    IN_MEETING_PEVENT_DETAIL t2 WITH(NOLOCK)
                    ON t2.source_id = t1.id
                LEFT OUTER JOIN
                    IN_MEETING_SITE t3 WITH(NOLOCK)
                    ON t3.id = t1.in_site
                LEFT OUTER JOIN
                    VU_MEETING_PTEAM  t11 WITH(NOLOCK)
                    ON t11.source_id = t1.source_id
                    AND t11.in_sign_no = t2.in_sign_no
                LEFT OUTER JOIN
                    IN_MEETING_USER  t12 WITH(NOLOCK)
                    ON t12.source_id = t11.in_meeting
                    AND ISNULL(t12.in_l1, '') + '-' + ISNULL(t12.in_l2, '') + '-' + ISNULL(t12.in_l3, '')+ '-' + ISNULL(t12.in_index, '')+ '-' + ISNULL(t12.in_creator, '')
					= t11.in_team_key
                WHERE
                    t1.source_id = '{#program_id}'
                ORDER BY
                      t1.in_tree_sort
                    , t1.in_tree_id
                    , t1.in_tree_no
                    , t2.in_sign_foot
            ";

            sql = sql.Replace("{#program_id}", program.id);

            return cfg.inn.applySQL(sql);
        }

        //分組交叉參數判斷
        private bool NeedCrossTop(TConfig cfg, string in_key)
        {
            var result = GetMeetingVariable(cfg, in_key);
            return result == "cross";
        }

        //取得賽會參數
        private string GetMeetingVariable(TConfig cfg, string in_key)
        {
            var sql = "SELECT TOP 1 in_value FROM IN_MEETING_VARIABLE WITH(NOLOCK) WHERE source_id = '" + cfg.meeting_id + "' AND in_key = '" + in_key + "'";
            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);
            var itmV = cfg.inn.applySQL(sql);
            if (itmV.isError() || itmV.getResult() == "")
            {
                return "";
            }
            else
            {
                return itmV.getProperty("in_value", "");
            }
        }

        private Dictionary<string, TDtl> MapPlayerList(TConfig cfg, TProgram program)
        {
            var result = new Dictionary<string, TDtl>();
            var items = GetPlayerItems(cfg, program);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var in_sign_no = item.getProperty("in_sign_no", "");
                var in_judo_no = item.getProperty("in_judo_no", "");
                var in_section_no = item.getProperty("in_section_no", "");
                var in_short_org = item.getProperty("in_short_org", "");
                var in_names = item.getProperty("in_names", "");

                if (!result.ContainsKey(in_sign_no))
                {
                    result.Add(in_sign_no, new TDtl
                    {
                        no = in_sign_no,
                        sno = in_judo_no,
                        og = in_short_org,
                        nm = in_names,
                        Value = item
                    });
                }
            }
            return result;
        }

        private Item GetPlayerItems(TConfig cfg, TProgram program)
        {
            string sql = @"
                SELECT
	                in_sign_no
	                , in_judo_no
                    , in_section_no
	                , in_short_org
	                , in_names
                FROM
	                IN_MEETING_PTEAM WITH(NOLOCK)
                WHERE
	                source_id = '{#program_id}'
	                AND ISNULL(in_sign_no, '') NOT IN ('', '0')
                ORDER BY
	                in_sign_no

            ";

            sql = sql.Replace("{#program_id}", program.id);

            return cfg.inn.applySQL(sql);
        }

        private string GetDtmStr(string value, int hours, string format)
        {
            if (value == "") return "";

            var result = GetDtm(value, hours);
            if (result == DateTime.MinValue) return "";

            return result.ToString(format);
        }

        private DateTime GetDtm(string value, int hours)
        {
            if (value == "") return DateTime.MinValue;

            DateTime result = DateTime.MinValue;
            if (DateTime.TryParse(value, out result))
            {
                return result.AddHours(hours);
            }
            return DateTime.MinValue;
        }

        private int GetInt(string value, int defV = 0)
        {
            if (value == "") return 0;
            int result = defV;
            if (int.TryParse(value, out result))
            {
                return result;
            }
            return defV;
        }
    }
}