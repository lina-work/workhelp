﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Judo.ParaTT
{
    public class In_Meeting_User_ImportNew : Item
    {
        public In_Meeting_User_ImportNew(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
    目的: 新版與會者匯入
    日誌: 
        - 2023-03-09: 與會者改為可分開匯入 (lina)
        - 2022-10-03: 創建 (lina)
*/

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "In_Meeting_User_ImportNew";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,

                meeting_id = itmR.getProperty("meeting_id", ""),
                need_rebuild_meeting = itmR.getProperty("need_rebuild_meeting", ""),
                site_count = itmR.getProperty("site_count", "1"),
                draw_sheet_mode = itmR.getProperty("draw_sheet_mode", ""),
                scene = itmR.getProperty("scene", ""),
            };

            switch (cfg.scene)
            {
                case "import":
                    CacheStep1(cfg, itmR);
                    Import(cfg, itmR);
                    break;

                case "rebuild":
                    Rebuild(cfg, itmR);
                    break;

                case "fight":
                    Fight(cfg, itmR);
                    break;

                    //case "day_menu":
                    //    DayMenu(cfg, itmR);
                    //    break;

                    //case "fix_rank": //修正準決賽名次
                    //    FixOther(cfg, itmR);
                    //    CacheStep2(cfg, itmR);
                    //    break;

                    //case "refresh_rank":
                    //    RefreshRank(cfg, itmR);
                    //    break;
            }

            return itmR;
        }

        private void RefreshRank(TConfig cfg, Item itmReturn)
        {
            ClearProgramRranks(cfg);

            var items = GetProgramRrankItems(cfg);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var team_id = item.getProperty("team_id", "");
                var org = item.getProperty("dptname", "").Trim();
                var player = item.getProperty("ptname", "").Trim();
                var rank = item.getProperty("rank", "");

                if (org == "" || player == "")
                {
                    continue;
                }

                if (team_id == "")
                {
                    team_id = GetRankTeamId(cfg, item);
                }

                if (team_id == "")
                {
                    throw new Exception(org + " " + player + " 系統查無資料");
                }

                var sql_upd = "UPDATE IN_MEETING_PTEAM SET"
                    + "  in_final_rank = '" + rank + "'"
                    + ", in_show_rank = '" + rank + "'"
                    + " WHERE id = '" + team_id + "'";

                cfg.inn.applySQL(sql_upd);
            }
        }

        private string GetRankTeamId(TConfig cfg, Item item)
        {
            var program_id = item.getProperty("program_id", "");
            var org = item.getProperty("dptname", "");
            var player = item.getProperty("ptname", "");

            var sql = @"
                SELECT TOP 1
	                id
                FROM
	                IN_MEETING_PTEAM WITH(NOLOCK)
                WHERE
	                source_id = '{#program_id}'
	                AND in_current_org = N'{#org}'
	                AND in_name = N'{#player}'
            ";

            sql = sql.Replace("{#program_id}", program_id)
                .Replace("{#org}", org)
                .Replace("{#player}", player);

            var itmData = cfg.inn.applySQL(sql);
            if (!itmData.isError() && itmData.getResult() != "")
            {
                return itmData.getProperty("id", "");
            }
            else
            {
                return "";
            }
        }

        private Item GetProgramRrankItems(TConfig cfg)
        {
            var sql = @"
	            SELECT  
		            t3.id               AS 'program_id'
		            , t3.in_name
		            , t3.in_program_no
		            , t1.Aplseq
		            , t1.Dptname
		            , t1.Ptname
		            , t1.Rank
		            , t4.id             AS 'team_id'
	            FROM 
		            In_Local_Rank t1 WITH(NOLOCK)
	            INNER JOIN
		            IN_MEETING t2 WITH(NOLOCK)
		            ON t2.item_number = t1.MNumber
	            INNER JOIN
		            IN_MEETING_PROGRAM t3 WITH(NOLOCK)
		            ON t3.in_meeting = t2.id
		            AND t3.in_l1 = t1.Item
		            AND t3.in_l2 = t1.EGrade
		            AND t3.in_l3 =t1.EWeight
	            LEFT OUTER JOIN
		            IN_MEETING_PTEAM t4 WITH(NOLOCK)
		            ON t4.source_id = t3.id
		            AND t4.in_team_index = t1.Aplseq
	            WHERE
		            t2.id = '{#meeting_id}'
	            ORDER BY
		            t3.in_sort_order
		            , t1.rank
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            return cfg.inn.applySQL(sql);
        }

        private void ClearProgramRranks(TConfig cfg)
        {
            var sql = @"
                UPDATE IN_MEETING_PTEAM SET
	                  in_final_rank = NULL
	                , in_show_rank = NULL
                WHERE
	                id IN
                (
	                SELECT DISTINCT 
		                t3.id
	                FROM 
		                IN_LOCAL_RANK t1 WITH(NOLOCK)
	                INNER JOIN
		                IN_MEETING t2 WITH(NOLOCK)
		                ON t2.item_number = t1.MNumber
	                INNER JOIN
		                IN_MEETING_PROGRAM t3 WITH(NOLOCK)
		                ON t3.in_meeting = t2.id
	                WHERE
		                t2.id = '{#meeting_id}'
                )
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            cfg.inn.applySQL(sql);
        }

        private void CacheStep1(TConfig cfg, Item itmReturn)
        {
            if (cfg.need_rebuild_meeting == "1") return;

            var sql = "";

            sql = "UPDATE IN_MEETING_ALLOCATION SET in_meeting = NULL WHERE in_meeting = '" + cfg.meeting_id + "'";
            cfg.inn.applySQL(sql);

            sql = "UPDATE IN_MEETING_PROGRAM SET in_meeting = NULL WHERE in_meeting = '" + cfg.meeting_id + "'";
            cfg.inn.applySQL(sql);

            sql = "UPDATE IN_MEETING_PTEAM SET in_meeting = NULL WHERE in_meeting = '" + cfg.meeting_id + "'";
            cfg.inn.applySQL(sql);

            sql = "UPDATE IN_MEETING_PEVENT SET in_meeting = NULL WHERE in_meeting = '" + cfg.meeting_id + "'";
            cfg.inn.applySQL(sql);

            sql = "UPDATE IN_MEETING_USER SET source_id = NULL WHERE source_id = '" + cfg.meeting_id + "'";
            cfg.inn.applySQL(sql);
        }

        private void CacheStep2(TConfig cfg, Item itmReturn)
        {
            var sql = "";

            //檢查是否有緩存資料
            sql = "SELECT TOP 1 id FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE in_meeting IS NULL";
            var itmData = cfg.inn.applySQL(sql);
            if (itmData.isError() || itmData.getResult() == "") return;

            sql = "UPDATE IN_MEETING_ALLOCATION SET in_meeting = '" + cfg.meeting_id + "' WHERE in_meeting IS NULL";
            cfg.inn.applySQL(sql);

            sql = "UPDATE IN_MEETING_PROGRAM SET in_meeting = '" + cfg.meeting_id + "' WHERE in_meeting IS NULL";
            cfg.inn.applySQL(sql);

            sql = "UPDATE IN_MEETING_PTEAM SET in_meeting = '" + cfg.meeting_id + "' WHERE in_meeting IS NULL";
            cfg.inn.applySQL(sql);

            sql = "UPDATE IN_MEETING_PEVENT SET in_meeting = '" + cfg.meeting_id + "' WHERE in_meeting IS NULL";
            cfg.inn.applySQL(sql);

            sql = "UPDATE IN_MEETING_USER SET source_id = '" + cfg.meeting_id + "' WHERE source_id IS NULL";
            cfg.inn.applySQL(sql);

            sql = @"
                UPDATE t1 SET
                	t1.IN_SORT_ORDER = rn * 100
                FROM
                	IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
                (
                	SELECT
                		id
                		, ROW_NUMBER() OVER (ORDER BY in_l1, in_l2, in_sort_order) AS 'rn' 
                	FROM
                		IN_MEETING_PROGRAM WITH(NOLOCK)
                	WHERE
                		in_meeting = '{#meeting_id}'
                ) t2 ON t2.id = t1.id
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);
            cfg.inn.applySQL(sql);
        }

        private void DayMenu(TConfig cfg, Item itmReturn)
        {
            string sql = @"
                SELECT DISTINCT
	                in_day AS 'in_day'
                FROM
	                IN_MEETING_USER_IMPORTNEW WITH(NOLOCK)
                WHERE
	                in_meeting = '{#meeting_id}'
                ORDER BY
	                in_day
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql + ": " + count.ToString());

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_day = item.getProperty("in_day", "");
                string in_fight_day = GetFightDay(cfg, in_day);
                item.setProperty("in_date", in_fight_day);
                item.setType("inn_day");
                itmReturn.addRelationship(item);
            }
        }

        private void Fight(TConfig cfg, Item itmReturn)
        {
            //賽會參數設定
            RebuildMtVariable(cfg, itmReturn);
            //過磅狀態設定
            RebuildMtWeight(cfg, itmReturn);
            //清除賽程組別資料
            ClearAllPrograms(cfg, itmReturn);

            var itmDays = GetFightDayItems(cfg, itmReturn);
            var count = itmDays.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var itmDay = itmDays.getItemByIndex(i);
                var in_day = itmDay.getProperty("in_day", "");
                var in_fight_day = GetFightDay(cfg, in_day);
                NewPrograms(cfg, in_day, in_fight_day);
                //更新隊伍籤號
                UpdateTeamDrawNo(cfg, in_fight_day);
            }

            //建立場地分配
            CreateAllocation(cfg, itmReturn);
            //建立競賽連結
            CreateQrCode(cfg, itmReturn);
        }

        //更新隊伍籤號
        private void UpdateTeamDrawNo(TConfig cfg, string in_fight_day)
        {
            var sql = @"
                UPDATE t1 SET
                	t1.in_sign_no = t11.in_draw_no
                	, t1.in_section_no = t11.in_draw_no
                	, t1.in_team_index = t11.in_index
                FROM
                	IN_MEETING_PTEAM t1 WITH(NOLOCK)
                INNER JOIN
                	IN_MEETING_PROGRAM t2 WITH(NOLOCK)
                	ON t2.id = t1.source_id
                INNER JOIN
                	IN_MEETING_USER t11 WITH(NOLOCK)
                	ON t11.source_id = t2.in_meeting
                	AND t11.in_l1 = t2.in_l1
                	AND t11.in_l2 = t2.in_l2
                	AND t11.in_l3 = t2.in_l3
                	AND t11.in_sno = t1.in_sno
                WHERE
                	t11.source_id = '{#meeting_id}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id)
                .Replace("{#in_fight_day}", in_fight_day);

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            cfg.inn.applySQL(sql);
        }

        private void NewPrograms(TConfig cfg, string in_day, string in_fight_day)
        {
            var items = cfg.inn.applySQL("SELECT DISTINCT in_meeting, in_program_no, in_l1, in_l2, in_l3, in_section_name FROM IN_MEETING_USER_IMPORTNEW WITH(NOLOCK) WHERE in_day = '" + in_day + "' ORDER BY in_meeting, in_program_no");
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var in_meeting = item.getProperty("in_meeting", "");
                var in_l1 = item.getProperty("in_l1", "");
                var in_l2 = item.getProperty("in_l2", "");
                var in_l3 = item.getProperty("in_l3", "");
                var in_program_no = item.getProperty("in_program_no", "");
                var in_section_name = item.getProperty("in_section_name", "");
                var in_sort_order = (i + 1) * 100;

                var itmMUsers = cfg.inn.applySQL("SELECT DISTINCT in_l1, in_l2, in_l3, in_index, in_creator_sno FROM IN_MEETING_USER WITH(NOLOCK) WHERE source_id = '" + in_meeting + "' AND in_l1 = N'" + in_l1 + "' AND in_l2 = N'" + in_l2 + "' AND in_l3 = N'" + in_l3 + "'");
                var team_count = itmMUsers.getItemCount();

                var itmProgram = cfg.inn.newItem("In_Meeting_Program", "merge");
                itmProgram.setAttribute("where", "in_meeting = '" + in_meeting + "' AND in_l1 = N'" + in_l1 + "' AND in_l2 = N'" + in_l2 + "' AND in_l3 = N'" + in_l3 + "'");
                itmProgram.setProperty("in_meeting", in_meeting);
                itmProgram.setProperty("in_l1", in_l1);
                itmProgram.setProperty("in_l2", in_l2);
                itmProgram.setProperty("in_l3", in_l3);
                itmProgram.setProperty("in_battle_type", "Cross");
                itmProgram.setProperty("in_display", in_l3);
                itmProgram.setProperty("in_name", in_l1 + "-" + in_l2 + "-" + in_l3);
                itmProgram.setProperty("in_name2", in_l2 + "-" + in_l3);
                itmProgram.setProperty("in_name3", in_section_name);
                itmProgram.setProperty("in_short_name", GetProgramShortName(in_l1, in_l2, in_l3));
                itmProgram.setProperty("in_weight", in_l3);
                itmProgram.setProperty("in_fight_day", in_fight_day);
                itmProgram.setProperty("in_team_yn", in_l1.Contains("雙") ? "1" : "0");

                itmProgram.setProperty("in_l1_sort", "100");
                itmProgram.setProperty("in_l2_sort", "100");
                itmProgram.setProperty("in_l3_sort", "100");
                itmProgram.setProperty("in_sort_order", in_sort_order.ToString());

                itmProgram.setProperty("in_team_count", team_count.ToString());
                itmProgram.setProperty("in_rank_count", team_count.ToString());
                itmProgram.setProperty("in_register_count", team_count.ToString());
                itmProgram.setProperty("in_round_code", GetProgramRoundCode(team_count));
                itmProgram.setProperty("in_round_count", GetProgramRoundCount(team_count));

                itmProgram.setProperty("in_event_count", GetEventCount(team_count));

                itmProgram.setProperty("in_program_no", in_program_no);
                itmProgram.setProperty("in_rank_type", "Default");
                itmProgram.setProperty("in_is_sync", "1");

                itmProgram = itmProgram.apply();

                NewProgramTeam(cfg, itmProgram);
            }
        }

        private string GetProgramShortName(string in_l1, string in_l2, string in_l3)
        {
            var n1 = "";
            var n2 = "";

            if (in_l2.Contains("混")) n1 = "混";
            else if (in_l2.Contains("男")) n1 = "男";
            else if (in_l2.Contains("女")) n1 = "女";

            if (in_l1.Contains("雙")) n2 = "雙";
            else if (in_l1.Contains("個")) n2 = "單";
            else if (in_l1.Contains("單")) n2 = "單";
            else n2 = "單";

            return n1 + n2 + "-" + in_l3; 
        }

        private void NewProgramTeam(TConfig cfg, Item itmProgram)
        {
            var itmData = cfg.inn.newItem();
            itmData.setType("In_Meeting_Program");
            itmData.setProperty("meeting_id", itmProgram.getProperty("in_meeting", ""));
            itmData.setProperty("program_id", itmProgram.getProperty("id", ""));
            itmData.setProperty("scene", "rebuild");
            itmData.apply("in_meeting_draw_team_program");
        }

        private string GetEventCount(int team_count)
        {
            switch (team_count)
            {
                case 1: return "0";
                case 2: return "1";
                case 3: return "3";
                case 4: return "6";
                case 5: return "10";
                case 6: return "10";
                case 7: return "13";
                case 8: return "16";
                case 9: return "18";
                case 10: return "19";
                case 11: return "22";
                case 12: return "25";
                case 13: return "26";
                case 14: return "29";
                case 15: return "32";
                case 16: return "35";
                default: return "0";
            }
        }

        private string GetProgramRoundCode(int team_count)
        {
            switch (team_count)
            {
                case 1: return "0";
                case 2: return "2";
                case 3: return "4";
                case 4: return "4";
                case 5: return "8";
                case 6: return "8";
                case 7: return "8";
                case 8: return "8";
                case 9: return "16";
                case 10: return "16";
                case 11: return "16";
                case 12: return "16";
                case 13: return "16";
                case 14: return "16";
                case 15: return "16";
                case 16: return "16";
                default: return "0";
            }
        }

        private string GetProgramRoundCount(int team_count)
        {
            switch (team_count)
            {
                case 1: return "0";
                case 2: return "1";
                case 3: return "2";
                case 4: return "2";
                case 5: return "3";
                case 6: return "3";
                case 7: return "3";
                case 8: return "3";
                case 9: return "4";
                case 10: return "4";
                case 11: return "4";
                case 12: return "4";
                case 13: return "4";
                case 14: return "4";
                case 15: return "4";
                case 16: return "4";
                default: return "0";
            }
        }


        private Item GetFightDayItems(TConfig cfg, Item itmReturn)
        {
            string sql = @"
                SELECT
	                DISTINCT in_day
                FROM
	                IN_MEETING_USER_IMPORTNEW t1 WITH(NOLOCK)
                ORDER BY
	                in_day
            ";

            return cfg.inn.applySQL(sql);
        }

        //修正其他
        private void FixOther(TConfig cfg, Item itmReturn)
        {
            FixSectCode(cfg, itmReturn);

            //修正第七名
            FixRepecahgeRank57(cfg, itmReturn);

            ClosePrograms(cfg, itmReturn);
            //FixSemiFinalRank(cfg, itmReturn);
        }

        //修正第七名
        private void FixRepecahgeRank57(TConfig cfg, Item itmReturn)
        {
            var sql = @"
                UPDATE t11 SET
                	 t11.in_tree_rank = 'rank57'
                   , t11.in_tree_rank_ns = '0,7' 
                   , t11.in_tree_rank_nss = '0,7' 
                FROM
                	IN_MEETING_PEVENT t11 WITH(NOLOCK)
                INNER JOIN
                (
                	SELECT DISTINCT
                		t1.in_program_no
                		, t2.id
                		, t2.in_team_count
                	FROM 
                		IN_MEETING_USER_IMPORTNEW t1 WITH(NOLOCK)
                	INNER JOIN
                		IN_MEETING_PROGRAM t2 WITH(NOLOCK)
                		ON t2.in_meeting = t1.in_meeting
                		AND t2.in_program_no = t1.in_program_no
                ) t12 ON t12.id = t11.source_id
                WHERE
                    t11.in_meeting = '{#meeting_id}'
                	AND t12.in_team_count >= 8
                	AND t11.in_fight_id IN ('R008-01', 'R008-02') 
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            cfg.inn.applySQL(sql);
        }

        //將人數少於等於 1 者關閉
        private void ClosePrograms(TConfig cfg, Item itmReturn)
        {
            string sql = @"
                UPDATE t1 SET
                	t1.in_team_count = 0
                FROM
                	IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                WHERE
                	t1.in_meeting = '{#meeting_id}'
                	AND ISNULL(t1.in_team_count, 0) <= 1
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            cfg.inn.applySQL(sql);
        }

        //修正組別代碼
        private void FixSectCode(TConfig cfg, Item itmReturn)
        {
            string sql = @"
                UPDATE t1 SET
                	t1.in_program_no = t2.in_program_no
                FROM
                	IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                INNER JOIN
                (
                	SELECT DISTINCT
						in_meeting
						, in_program_no
						, in_l1, in_l2
						, in_l3 
					FROM 
						In_Meeting_User_ImportNew WITH(NOLOCK)
                ) t2 ON t2.in_meeting = t1.in_meeting
                    AND t2.in_l1 = t1.in_l1
                    AND t2.in_l2 = t1.in_l2
                    AND t2.in_l3 = t1.in_l3
                WHERE
                	t1.in_meeting = '{#meeting_id}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            cfg.inn.applySQL(sql);
        }

        // //修正準決賽名次
        // private void FixSemiFinalRank(TConfig cfg, Item itmReturn)
        // {
        //     string sql = @"
        //         UPDATE IN_MEETING_PEVENT SET
        //             in_tree_rank = 'rank23'
        //             , in_tree_rank_ns = '2,3'
        //             , in_tree_rank_nss = '2,3'
        //         where in_meeting = '{#meeting_id}'
        //         AND in_tree_name = 'main'
        //         AND in_round_code = 4
        //         AND ISNULL(in_robin_key, '') = ''
        //     ";

        //     sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

        //     cfg.inn.applySQL(sql);
        // }

        //建立競賽連結
        private void CreateQrCode(TConfig cfg, Item itmReturn)
        {
            Item itmData = cfg.inn.newItem("In_Meeting");
            itmData.setProperty("meeting_id", cfg.meeting_id);
            itmData.apply("in_qrcode_create");
        }

        //建立場地分配
        private void CreateAllocation(TConfig cfg, Item itmReturn)
        {
            //建立場地分配
            var sql = "SELECT * FROM IN_MEETING_SITE WITH(NOLOCK) WHERE in_meeting = '{#meeting_id}' AND in_code = 1";
            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);
            var itmSite = cfg.inn.applySQL(sql);
            if (itmSite.isError() || itmSite.getResult() == "")
            {
                return;
            }

            //清除舊場地分配
            cfg.inn.applySQL("DELETE FROM IN_MEETING_ALLOCATION WHERE in_meeting = '" + cfg.meeting_id + "'");

            //建立主分類
            MergeAllocateCategory(cfg);

            //建立場地分配
            MergeAllocateFight(cfg, itmSite);
        }

        private void MergeAllocateFight(TConfig cfg, Item itmSiteSrc)
        {
            List<string> pgs = new List<string>();

            string sql = @"
                SELECT DISTINCT
					in_program_no
					, in_l1
					, in_l2
					, in_l3
					, in_fight_day
                FROM 
                    In_Meeting_User WITH(NOLOCK) 
                WHERE 
                    source_id = '{#meeting_id}'
                ORDER BY
					in_program_no
					, in_l1
					, in_l2
					, in_l3
					, in_fight_day
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_l1 = item.getProperty("in_l1", "");
                string in_l2 = item.getProperty("in_l2", "");
                string in_l3 = item.getProperty("in_l3", "");
                string in_fight_day = item.getProperty("in_fight_day", "");

                string sql_qry1 = "SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK)"
                    + " WHERE in_meeting = '" + cfg.meeting_id + "'"
                    + " AND in_l1 = N'" + in_l1 + "'"
                    + " AND in_l2 = N'" + in_l2 + "'"
                    + " AND in_l3 = N'" + in_l3 + "'";

                Item itmProgram = cfg.inn.applySQL(sql_qry1);
                if (itmProgram.isError() || itmProgram.getResult() == "")
                {
                    continue;
                }

                string sql_qry2 = @"SELECT TOP 1 t2.id FROM IN_MEETING_USER_IMPORTNEW t1 WITH(NOLOCK)"
                    + " LEFT OUTER JOIN IN_MEETING_SITE t2 WITH(NOLOCK)"
                    + " ON t2.in_meeting = t1.in_meeting"
                    + " AND CAST(t2.in_code AS VARCHAR) = t1.in_site_code"
                    + " WHERE t1.in_meeting = '" + cfg.meeting_id + "'"
                    + " AND t1.in_l1 = N'" + in_l1 + "'"
                    + " AND t1.in_l2 = N'" + in_l2 + "'"
                    + " AND t1.in_l3 = N'" + in_l3 + "'";

                Item itmSite = cfg.inn.applySQL(sql_qry2);
                if (itmSite.isError() || itmSite.getResult() == "")
                {
                    itmSite = itmSiteSrc;
                }


                if (in_fight_day == "")
                {
                    continue;
                }


                string site_id = itmSite.getProperty("id", "");
                string program_id = itmProgram.getProperty("id", "");

                //補刀 (比賽日期)
                cfg.inn.applySQL("UPDATE IN_MEETING_PROGRAM SET in_fight_day = '" + in_fight_day + "' WHERE id = '" + program_id + "'");

                Item itmNew = cfg.inn.newItem("In_Meeting_Allocation", "add");
                itmNew.setProperty("in_meeting", cfg.meeting_id);
                itmNew.setProperty("in_date", in_fight_day);
                itmNew.setProperty("in_date_key", in_fight_day);
                itmNew.setProperty("in_site", site_id);
                itmNew.setProperty("in_program", program_id);
                itmNew.setProperty("in_type", "fight");
                itmNew.setProperty("in_l1", "");
                itmNew.setProperty("in_l2", "");
                itmNew = itmNew.apply();

                pgs.Add(program_id);
            }

            DateTime dt = DateTime.Now.AddDays(-1).Date;

            foreach (var pg in pgs)
            {
                string sql_upd = "UPDATE IN_MEETING_ALLOCATION SET created_on = '" + dt.ToString("yyyy-MM-dd HH:mm:00") + "' WHERE in_meeting = '" + cfg.meeting_id + "' AND in_program = '" + pg + "'";
                cfg.inn.applySQL(sql_upd);
                dt = dt.AddMinutes(10);
            }
        }

        private void MergeAllocateCategory(TConfig cfg)
        {
            string sql = @"
                SELECT DISTINCT
					in_program_no
					, in_l1
					, in_l2
					, in_fight_day
                FROM 
                    In_Meeting_User WITH(NOLOCK) 
                WHERE 
                    source_id = '{#meeting_id}'
                ORDER BY
					in_program_no
					, in_l1
					, in_l2
					, in_fight_day
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_l1 = item.getProperty("in_l1", "");
                string in_l2 = item.getProperty("in_l2", "");
                string in_fight_day = item.getProperty("in_fight_day", "");

                if (in_fight_day == "")
                {
                    continue;
                }

                Item itmNew = cfg.inn.newItem("In_Meeting_Allocation", "add");
                itmNew.setProperty("in_meeting", cfg.meeting_id);
                itmNew.setProperty("in_date", in_fight_day);
                itmNew.setProperty("in_date_key", in_fight_day);
                itmNew.setProperty("in_type", "category");
                itmNew.setProperty("in_l1", in_l1);
                itmNew.setProperty("in_l2", in_l2);
                itmNew = itmNew.apply();
            }
        }

        //清除所有比賽資料
        private void ClearAllPrograms(TConfig cfg, Item itmReturn)
        {
            var sql = "";
            var itmSQL = default(Item);

            sql = @"
                SELECT
	                DISTINCT t1.id
                FROM
	                IN_MEETING_PROGRAM t1 WITH(NOLOCK)
                WHERE
	                t1.in_meeting = '{#meeting_id}'
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            var itmPrograms = cfg.inn.applySQL(sql);
            int count = itmPrograms.getItemCount();
            for (int i = 0; i < count; i++)
            {
                Item itmProgram = itmPrograms.getItemByIndex(i);
                string program_id = itmProgram.getProperty("id", "");

                //刪除隊伍
                sql = "DELETE FROM IN_MEETING_PTEAM WHERE source_id = '" + program_id + "'";
                cfg.inn.applySQL(sql);

                //刪除明細
                sql = @"
                    DELETE FROM 
                        IN_MEETING_PEVENT_DETAIL 
                    WHERE 
                        source_id IN
                        (
                            SELECT id FROM IN_MEETING_PEVENT WITH(NOLOCK) WHERE source_id = '{#program_id}'
                        )
                ";
                sql = sql.Replace("{#program_id}", program_id);
                cfg.inn.applySQL(sql);

                //刪除場次
                sql = @"DELETE FROM IN_MEETING_PEVENT WHERE source_id = '{#program_id}'";
                sql = sql.Replace("{#program_id}", program_id);
                cfg.inn.applySQL(sql);
            }

            //刪除無歸屬隊伍
            sql = "DELETE FROM IN_MEETING_PTEAM WHERE in_meeting = '" + cfg.meeting_id + "'";
            cfg.inn.applySQL(sql);

            //移除場地分配
            sql = "UPDATE t1 SET t1.in_program_name = t2.in_name"
                 + " FROM IN_MEETING_ALLOCATION t1 INNER JOIN IN_MEETING_PROGRAM t2 ON t2.id = t1.in_program"
                 + " WHERE t1.in_meeting = '" + cfg.meeting_id + "'";
            cfg.inn.applySQL(sql);

            //移除場地分配
            sql = "UPDATE t1 SET t1.in_program = NULL"
                 + " FROM IN_MEETING_ALLOCATION t1 INNER JOIN IN_MEETING_PROGRAM t2 ON t2.id = t1.in_program"
                 + " WHERE t1.in_meeting = '" + cfg.meeting_id + "'"
                 + " AND ISNULL(t1.in_program_name, '') <> ''";
            cfg.inn.applySQL(sql);

            //移除團體量級
            sql = "UPDATE t1 SET t1.in_program = NULL"
                 + " FROM IN_MEETING_PSECT t1 INNER JOIN IN_MEETING_PROGRAM t2 ON t2.id = t1.in_program"
                 + " WHERE t1.in_meeting = '" + cfg.meeting_id + "'"
                 + " AND ISNULL(t1.in_program_name, '') <> ''";
            cfg.inn.applySQL(sql);

            //刪除組別
            sql = "DELETE FROM IN_MEETING_PROGRAM WHERE in_meeting = '" + cfg.meeting_id + "'";
            cfg.inn.applySQL(sql);

            // //刪除場地
            // sql = "DELETE FROM IN_MEETING_SITE WHERE in_meeting = '" + meeting_id + "'";
            // itmSQL = inn.applySQL(sql);

            sql = "UPDATE IN_MEETING SET in_draw_status = '', in_draw_file = '', in_site_mode = '' WHERE id = '" + cfg.meeting_id + "'";
            cfg.inn.applySQL(sql);
        }

        //賽會參數設定
        private void RebuildMtVariable(TConfig cfg, Item itmReturn)
        {
            string need_rank57 = itmReturn.getProperty("need_rank57", "1");

            cfg.inn.applySQL("DELETE FROM In_Meeting_Variable WHERE source_id = '" + cfg.meeting_id + "'");

            RebuildMtVariable(cfg, "fight_site_address", "板樹體育館", "100");
            RebuildMtVariable(cfg, "fight_site_count", cfg.site_count, "200");
            RebuildMtVariable(cfg, "fight_battle_type", "Cross", "300");
            RebuildMtVariable(cfg, "fight_robin_player", "5", "400");
            RebuildMtVariable(cfg, "allocate_mode", "cycle", "500");
            RebuildMtVariable(cfg, "line_color", "red", "600");
            RebuildMtVariable(cfg, "need_rank78", "0", "700");
            RebuildMtVariable(cfg, "need_rank56", "1", "800");
            RebuildMtVariable(cfg, "need_rank57", need_rank57, "900");
            RebuildMtVariable(cfg, "draw_sheet_mode", cfg.draw_sheet_mode, "1000");

        }

        private void RebuildMtVariable(TConfig cfg, string in_key, string in_value, string sort_order)
        {
            Item itmNew = cfg.inn.newItem("In_Meeting_Variable", "add");
            itmNew.setProperty("source_id", cfg.meeting_id);
            itmNew.setProperty("in_key", in_key);
            itmNew.setProperty("in_value", in_value);
            itmNew.setProperty("sort_order", sort_order);
            itmNew.apply();
        }

        //過磅狀態設定
        private void RebuildMtWeight(TConfig cfg, Item itmReturn)
        {
            cfg.inn.applySQL("DELETE FROM In_Meeting_PWeight WHERE in_meeting = '" + cfg.meeting_id + "'");
            RebuildMtWeight(cfg, "off", "0");
            RebuildMtWeight(cfg, "leave", "0");
            RebuildMtWeight(cfg, "dq", "0");
        }

        private void RebuildMtWeight(TConfig cfg, string in_status, string in_is_remove)
        {
            Item itmNew = cfg.inn.newItem("In_Meeting_PWeight", "add");
            itmNew.setProperty("in_meeting", cfg.meeting_id);
            itmNew.setProperty("in_status", in_status);
            itmNew.setProperty("in_is_remove", in_is_remove);
            itmNew.apply();
        }

        private void Rebuild(TConfig cfg, Item itmReturn)
        {
            string aml = "<AML><Item type='In_Meeting' action='get' id='" + cfg.meeting_id + "' select='*'/></AML>";
            cfg.itmMeeting = cfg.inn.applyAML(aml);

            MergeMtMuser(cfg);

            var svy_model = GetMtSvy(cfg);
            RebuildInL1(cfg, svy_model.l1_id);
            RebuildInL2(cfg, svy_model.l2_id);
            RebuildInL3(cfg, svy_model.l3_id);
        }

        private void MergeMtMuser(TConfig cfg)
        {
            //刪除舊與會者資料
            string sql_del = "DELETE FROM IN_MEETING_USER WHERE source_id = '{#meeting_id}'";
            sql_del = sql_del.Replace("{#meeting_id}", cfg.meeting_id);
            cfg.inn.applySQL(sql_del);

            string sql = "SELECT * FROM In_Meeting_User_ImportNew WITH(NOLOCK)"
                + " WHERE in_meeting = '" + cfg.meeting_id + "' ORDER BY in_player_no";

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                //與會者資訊
                Item applicant = NewMUser(cfg, item);
                //建立與會者
                applicant.apply("add");
            }

            //重算正取人數
            cfg.itmMeeting.apply("In_Update_MeetingOnMUEdit");
        }

        private Item NewMUser(TConfig cfg, Item item)
        {
            var applicant = cfg.inn.newItem("In_Meeting_User");

            applicant.setProperty("source_id", cfg.meeting_id);

            //所屬單位
            applicant.setProperty("in_current_org", item.getProperty("in_current_org", ""));

            //單位簡稱
            applicant.setProperty("in_short_org", item.getProperty("in_current_org", ""));

            //姓名
            applicant.setProperty("in_name", item.getProperty("in_name", ""));

            //身分證號
            applicant.setProperty("in_sno", item.getProperty("in_player_no", ""));

            //性別
            applicant.setProperty("in_gender", GetGender(cfg, item));

            //西元生日
            applicant.getProperty("in_birth", "1990-01-01");

            //電子信箱
            applicant.setProperty("in_email", "na@na.n");

            //手機號碼
            applicant.setProperty("in_tel", "");

            //協助報名者姓名
            applicant.setProperty("in_creator", "lwu001");

            //協助報名者身分號
            applicant.setProperty("in_creator_sno", "lwu001");

            //所屬群組
            applicant.setProperty("in_group", "原創"); //F103277376測試者


            //預設身分均為選手
            applicant.setProperty("in_role", "player");

            //正取
            applicant.setProperty("in_note_state", "official");

            //組別
            applicant.setProperty("in_gameunit", item.getProperty("in_section_name", ""));

            //組名
            applicant.setProperty("in_section_name", item.getProperty("in_section_name", ""));

            //組別編號
            applicant.setProperty("in_program_no", item.getProperty("in_program_no", ""));

            //捐款金額
            applicant.setProperty("in_expense", "0");

            //序號
            Item itmIndex = MaxIndexItem(cfg, applicant);
            applicant.setProperty("in_index", item.getProperty("in_player_no", ""));

            //競賽項目
            applicant.setProperty("in_l1", item.getProperty("in_l1", ""));

            //競賽組別
            applicant.setProperty("in_l2", item.getProperty("in_l2", ""));

            //競賽分級
            applicant.setProperty("in_l3", item.getProperty("in_l3", ""));

            //非實名制
            applicant.setProperty("in_mail", item.getProperty("in_player_no", ""));

            //報名時間
            applicant.setProperty("in_regdate", System.DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss"));

            //繳費單號
            applicant.setProperty("in_paynumber", "");

            //籤號
            applicant.setProperty("in_draw_no", item.getProperty("in_draw_no", ""));

            //比賽日期
            applicant.setProperty("in_fight_day", GetFightDay(cfg, item.getProperty("in_day", "")));

            return applicant;
        }

        private string GetGender(TConfig cfg, Item item)
        {
            string in_l2 = item.getProperty("in_l2", "");
            return GetGender(cfg, in_l2);
        }

        private string GetGender(TConfig cfg, string value)
        {
            if (value.Contains("男")) return "男";
            if (value.Contains("女")) return "女";
            return "";
        }

        //取得該組當前最大序號
        private Item MaxIndexItem(TConfig cfg, Item applicant)
        {
            string sql = "";

            string in_l1 = applicant.getProperty("in_l1", "");
            string in_l2 = applicant.getProperty("in_l2", "");
            string in_l3 = applicant.getProperty("in_l3", "");
            string in_l4 = applicant.getProperty("in_l4", "");

            sql = "SELECT MAX(in_index) AS 'max_idx' FROM In_Meeting_User WITH(NOLOCK) WHERE source_id = '" + cfg.meeting_id + "'"
                + " AND in_l1 = N'" + in_l1 + "'";

            //cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, "sql_index: " + sql);

            Item itmResult = cfg.inn.applySQL(sql);

            if (itmResult.isError() || itmResult.getResult() == "")
            {
                itmResult = cfg.inn.newItem();
                itmResult.setProperty("new_idx", "00001");
            }
            else
            {
                var max_idx = itmResult.getProperty("max_idx", "1");
                var new_idx = GetInt(max_idx) + 1;
                applicant.setProperty("new_idx", new_idx.ToString("00000"));

            }

            return itmResult;
        }

        private void RebuildInL1(TConfig cfg, string id)
        {
            cfg.inn.applySQL("DELETE FROM IN_SURVEY_OPTION WHERE source_id = '" + id + "'");

            string sql = @"
                SELECT DISTINCT
                    in_l1
                FROM 
                    IN_MEETING_USER WITH(NOLOCK) 
                WHERE 
                    source_id = '{#meeting_id}'
                ORDER BY
                    in_l1
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();

            string in_selectoption = "";

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_l1 = item.getProperty("in_l1", "");
                int sort_order = (i + 1) * 100;

                Item itmNew = cfg.inn.newItem("In_Survey_Option", "merge");
                itmNew.setAttribute("where", "source_id='" + id + "' AND in_value=N'" + in_l1 + "'");
                itmNew.setProperty("source_id", id);
                itmNew.setProperty("in_label", in_l1);
                itmNew.setProperty("in_value", in_l1);
                itmNew.setProperty("sort_order", sort_order.ToString());
                itmNew = itmNew.apply();

                in_selectoption += "@" + in_l1;
            }

            cfg.inn.applySQL("UPDATE IN_SURVEY SET in_selectoption = N'" + in_selectoption + "' WHERE id = '" + id + "'");
        }

        private void RebuildInL2(TConfig cfg, string id)
        {
            cfg.inn.applySQL("DELETE FROM IN_SURVEY_OPTION WHERE source_id = '" + id + "'");

            string sql = @"
                SELECT DISTINCT
                    in_l1
                    , in_l2
                FROM 
                    IN_MEETING_USER WITH(NOLOCK) 
                WHERE 
                    source_id = '{#meeting_id}'
                ORDER BY
                    in_l1
                    , in_l2
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_l1 = item.getProperty("in_l1", "");
                string in_l2 = item.getProperty("in_l2", "");
                int sort_order = (i + 1) * 100;

                Item itmNew = cfg.inn.newItem("In_Survey_Option", "merge");
                itmNew.setAttribute("where", "source_id='" + id + "' AND in_filter = N'" + in_l1 + "' AND in_value=N'" + in_l2 + "'");
                itmNew.setProperty("source_id", id);
                itmNew.setProperty("in_filter", in_l1);
                itmNew.setProperty("in_label", in_l2);
                itmNew.setProperty("in_value", in_l2);
                itmNew.setProperty("sort_order", sort_order.ToString());
                itmNew = itmNew.apply();
            }
        }

        private void RebuildInL3(TConfig cfg, string id)
        {
            cfg.inn.applySQL("DELETE FROM IN_SURVEY_OPTION WHERE source_id = '" + id + "'");

            string sql = @"
                SELECT DISTINCT
                    in_program_no
                    , in_l1
                    , in_l2
                    , in_l3
                FROM 
                    IN_MEETING_USER WITH(NOLOCK) 
                WHERE 
                    source_id = '{#meeting_id}'
                ORDER BY
                    in_program_no
                    , in_l1
                    , in_l2
                    , in_l3
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);
            Item items = cfg.inn.applySQL(sql);
            int count = items.getItemCount();

            int weight_idx = 0;
            int last_code = 10000;
            string last_key = "";

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string in_l1 = item.getProperty("in_l1", "");
                string in_l2 = item.getProperty("in_l2", "");
                string in_l3 = item.getProperty("in_l3", "");

                string key = in_l1 + "-" + in_l2;

                int sort_order = last_code + (i + 1) * 100;
                if (key != last_key)
                {
                    last_key = key;
                    last_code += 10000;
                }
                if (in_l3.Contains("以上"))
                {
                    sort_order += 3000;
                }

                var itmShortName = cfg.inn.applyMethod("In_Meeting_Program_ShortName"
                    , "<in_l1>" + in_l1 + "</in_l1><in_l2>" + in_l2 + "</in_l2><in_l3>" + in_l3 + "</in_l3>");

                var in_n1 = itmShortName.getProperty("result", "");
                var in_weight = itmShortName.getProperty("weight", "");

                Item itmNew = cfg.inn.newItem("In_Survey_Option", "merge");
                itmNew.setAttribute("where", "source_id='" + id + "' AND in_filter = N'" + in_l2 + "' AND in_value=N'" + in_l3 + "'");
                itmNew.setProperty("source_id", id);
                itmNew.setProperty("in_grand_filter", in_l1);
                itmNew.setProperty("in_filter", in_l2);
                itmNew.setProperty("in_label", in_l3);
                itmNew.setProperty("in_value", in_l3);
                itmNew.setProperty("in_weight", in_weight);
                itmNew.setProperty("in_n1", in_n1);
                itmNew.setProperty("sort_order", sort_order.ToString());
                itmNew = itmNew.apply();

                if (in_l3.Contains("以上"))
                {
                    weight_idx = 0;
                }
                else
                {
                    weight_idx++;
                }
            }
        }

        private TMtSvy GetMtSvy(TConfig cfg)
        {
            TMtSvy result = new TMtSvy { l1_id = "", l2_id = "", l3_id = "" };

            string sql = @"
                SELECT
	                t2.id
	                , t2.in_property
	                , t2.in_questions
                FROM
	                IN_MEETING_SURVEYS t1 WITH(NOLOCK)
                INNER JOIN
	                IN_SURVEY t2 WITH(NOLOCK)
	                ON t2.id = t1.related_id
                WHERE
	                t1.source_id = '{#meeting_id}'
	                AND t2.in_property IN ('in_l1', 'in_l2', 'in_l3')
                ORDER BY
	                t2.in_property 
            ";

            sql = sql.Replace("{#meeting_id}", cfg.meeting_id);

            Item items = cfg.inn.applySQL(sql);

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);
                string id = item.getProperty("id", "");
                string in_property = item.getProperty("in_property", "");
                switch (in_property)
                {
                    case "in_l1": result.l1_id = id; break;
                    case "in_l2": result.l2_id = id; break;
                    case "in_l3": result.l3_id = id; break;
                }
            }
            return result;
        }

        //匯入與會者
        private void Import(TConfig cfg, Item itmReturn)
        {
            //重建場地資料
            RebuildMeetingSites(cfg, itmReturn.getProperty("site_count", "0"));

            var value = itmReturn.getProperty("value", "");
            if (value == "") throw new Exception("無匯入資料");

            var list = MapList(cfg, value);
            if (list == null || list.Count == 0) throw new Exception("無匯入資料");

            //刪除資料
            cfg.inn.applySQL("DELETE FROM In_Meeting_User_ImportNew");

            for (var i = 0; i < list.Count; i++)
            {
                var row = list[i];
                //var lv = GetLevel(row.c02);

                Item itmNew = cfg.inn.newItem("In_Meeting_User_ImportNew", "add");
                itmNew.setProperty("in_no", row.c00);
                itmNew.setProperty("in_meeting", cfg.meeting_id);
                itmNew.setProperty("in_l1", row.c06);
                itmNew.setProperty("in_l2", row.c04);
                itmNew.setProperty("in_l3", row.c05);
                itmNew.setProperty("in_section_name", row.c03);
                itmNew.setProperty("in_program_no", row.c02);

                itmNew.setProperty("in_player_no", row.c07);
                itmNew.setProperty("in_current_org", row.c08);
                itmNew.setProperty("in_name", row.c09);
                itmNew.setProperty("in_draw_no", row.c10);
                itmNew.setProperty("in_city", "");
                itmNew.setProperty("in_day", row.c11);
                itmNew.apply();
            }
        }

        //重建場地資料
        private void RebuildMeetingSites(TConfig cfg, string value)
        {
            if (cfg.need_rebuild_meeting != "1") return;

            var max = GetInt(value);
            if (max <= 0) throw new Exception("場地數量錯誤");

            //清除所有場地資料
            Item itmData = cfg.inn.newItem();
            itmData.setType("In_Meeting");
            itmData.setProperty("meeting_id", cfg.meeting_id);
            itmData.setProperty("mode", "remove");
            itmData.apply("in_meeting_site");

            for (var i = 1; i <= max; i++)
            {
                var code = i.ToString();
                var row = SiteCodeRow(i);

                var itmNew = cfg.inn.newItem("In_Meeting_Site", "add");
                itmNew.setProperty("in_meeting", cfg.meeting_id);
                itmNew.setProperty("in_code", code);
                //itmNew.setProperty("in_code_en", row.w1);
                itmNew.setProperty("in_name", "第" + row.w2 + "場地");
                itmNew.setProperty("in_rows", "1");
                itmNew.setProperty("in_cols", value);
                itmNew.setProperty("in_row_index", "0");
                itmNew.setProperty("in_col_index", (i - 1).ToString());
                itmNew.apply();
            }
        }

        private TSP SiteCodeRow(int code)
        {
            switch (code)
            {
                case 1: return new TSP { w1 = "A", w2 = "一" };
                case 2: return new TSP { w1 = "B", w2 = "二" };
                case 3: return new TSP { w1 = "C", w2 = "三" };
                case 4: return new TSP { w1 = "D", w2 = "四" };
                case 5: return new TSP { w1 = "E", w2 = "五" };
                case 6: return new TSP { w1 = "F", w2 = "六" };
                case 7: return new TSP { w1 = "G", w2 = "七" };
                case 8: return new TSP { w1 = "H", w2 = "八" };
                case 9: return new TSP { w1 = "I", w2 = "九" };
                case 10: return new TSP { w1 = "J", w2 = "十" };
                case 11: return new TSP { w1 = "K", w2 = "十一" };
                case 12: return new TSP { w1 = "L", w2 = "十二" };
                default: return new TSP { w1 = "ERR", w2 = "ERR" };
            }
        }

        private class TSP
        {
            public string w1 { get; set; }
            public string w2 { get; set; }
        }

        private List<TRow> MapList(TConfig cfg, string value)
        {
            try
            {
                var list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<TRow>>(value);
                return list;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private TLevel GetLevel(string value)
        {
            //公開男生組  希羅式 第一級60公斤以下(含60.0公斤)
            //高中部男生組  自由式 第三級：61.1 公斤以上至 65.0 公斤以下

            var result = new TLevel { in_l1 = "", in_l2 = "", in_l3 = "" };

            if (value.Contains("希羅式")) result.in_l1 = "希羅式";
            else if (value.Contains("自由式")) result.in_l1 = "自由式";
            else result.in_l1 = "個人組";


            result.in_l2 = value.Substring(0, value.IndexOf(result.in_l1));
            result.in_l3 = value.Replace(result.in_l1, "").Replace(result.in_l2, "");

            return result;
        }

        private class TLevel
        {
            public string in_l1 { get; set; }
            public string in_l2 { get; set; }
            public string in_l3 { get; set; }
        }

        private class TRow
        {
            public string c00 { get; set; }
            public string c01 { get; set; }
            public string c02 { get; set; }
            public string c03 { get; set; }
            public string c04 { get; set; }
            public string c05 { get; set; }
            public string c06 { get; set; }
            public string c07 { get; set; }
            public string c08 { get; set; }
            public string c09 { get; set; }
            public string c10 { get; set; }
            public string c11 { get; set; }
            public string c12 { get; set; }
            public string c13 { get; set; }
        }

        private class TMtSvy
        {
            public string l1_id { get; set; }
            public string l2_id { get; set; }
            public string l3_id { get; set; }
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }

            public string meeting_id { get; set; }
            public string need_rebuild_meeting { get; set; }
            public string site_count { get; set; }
            public string draw_sheet_mode { get; set; }
            public string scene { get; set; }

            public Item itmMeeting { get; set; }
        }

        private string GetFightDay(TConfig cfg, string value)
        {
            string code = value;
            if (code.Length <= 4)
            {
                code = DateTime.Now.Year + code.PadLeft(4, '0');
            }
            if (code.Length != 8)
            {
                return "";
            }

            string y = code.Substring(0, 4);
            string m = code.Substring(4, 2);
            string d = code.Substring(6, 2);
            return y + "-" + m + "-" + d;
        }

        private DateTime GetDtmVal(string value)
        {
            DateTime result = DateTime.MinValue;
            DateTime.TryParse(value, out result);
            return result;
        }

        private int GetInt(string value, int def = 0)
        {
            int result = def;
            int.TryParse(value, out result);
            return result;
        }
    }
}