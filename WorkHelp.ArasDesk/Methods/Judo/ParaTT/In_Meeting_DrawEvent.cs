﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;

namespace WorkHelp.ArasDesk.Methods.Judo.ParaTT
{
    public class In_Meeting_DrawEvent : Item
    {
        public In_Meeting_DrawEvent(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 帕拉桌球籤表對照表建立
                日誌: 
                    - 2023-05-30: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "In_Meeting_DrawEvent";

            Item itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            TConfig cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,

                meeting_id = itmR.getProperty("meeting_id", ""),
                program_id = itmR.getProperty("program_id", ""),
                scene = itmR.getProperty("scene", ""),
            };


            switch (cfg.scene)
            {
                case "create":
                    CreateEvents(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void CreateEvents(TConfig cfg, Item itmReturn)
        {
            //取得量級設定
            var pg = new TProgram();
            pg.itmProgram = cfg.inn.applySQL("SELECT * FROM IN_MEETING_PROGRAM WITH(NOLOCK) WHERE id = '" + cfg.program_id + "'");
            pg.in_team_count = pg.itmProgram.getProperty("in_team_count", "");
            pg.in_fight_day = pg.itmProgram.getProperty("in_fight_day", "");

            //新賽制
            pg.new_battle_type = "Cross";

            //清除場次資料
            RemoveEvents(cfg);

            //重設量級賽制
            ResetBattleType(cfg, pg.new_battle_type);

            switch (pg.new_battle_type)
            {
                case "Cross"://分組交叉賽制
                    pg.is_robin = true;
                    pg.is_cross = true;
                    break;
            }

            //取得籤表場次對照表
            pg.itmDrawEvents = GetMeetingDrawEventsByRobin(cfg, pg.in_team_count);

            pg.EventMap = new Dictionary<string, string>();

            //重設量級場次
            ResetEvents(cfg, pg);

            //重設場次連線
            ResetEventLinks(cfg, pg);
        }

        //重設場次連線
        private void ResetEventLinks(TConfig cfg, TProgram pg)
        {
            var count = pg.itmDrawEvents.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = pg.itmDrawEvents.getItemByIndex(i);
                var in_fight_id = item.getProperty("in_fight_id", "");
                var in_next_win_tid = item.getProperty("in_next_win_tid", "");
                var in_next_win_foot = item.getProperty("in_next_win_foot", "");
                var in_next_lose_tid = item.getProperty("in_next_lose_tid", "");
                var in_next_lose_foot = item.getProperty("in_next_lose_foot", "");

                var eid = GetEventId(cfg, pg, in_fight_id);
                if (eid == "") continue;

                //勝出 Link
                UpdateEventWLink(cfg, pg, eid, in_next_win_tid, in_next_win_foot);
                //敗出 Link
                UpdateEventLLink(cfg, pg, eid, in_next_lose_tid, in_next_lose_foot);
            }
        }

        private void UpdateEventWLink(TConfig cfg, TProgram pg, string eid, string next_tid, string next_foot)
        {
            if (next_tid == "") return;

            var next_id = GetEventId(cfg, pg, next_tid);
            if (next_id == "" || next_foot == "") return;

            var sql = "UPDATE IN_MEETING_PEVENT SET"
                + "  in_next_win = '" + next_id + "'"
                + ", in_next_foot_win = '" + next_foot + "'"
                + " WHERE id = '" + eid + "'";

            cfg.inn.applySQL(sql);
        }

        private void UpdateEventLLink(TConfig cfg, TProgram pg, string eid, string next_tid, string next_foot)
        {
            if (next_tid == "") return;

            var next_id = GetEventId(cfg, pg, next_tid);
            if (next_id == "" || next_foot == "") return;

            var sql = "UPDATE IN_MEETING_PEVENT SET"
                + "  in_next_lose = '" + next_id + "'"
                + ", in_next_foot_lose = '" + next_foot + "'"
                + " WHERE id = '" + eid + "'";

            cfg.inn.applySQL(sql);
        }

        private string GetEventId(TConfig cfg, TProgram pg, string key)
        {
            if (pg.EventMap.ContainsKey(key))
            {
                return pg.EventMap[key];
            }
            return "";
        }

        //重設量級場次
        private void ResetEvents(TConfig cfg, TProgram pg)
        {
            var itmSite = cfg.inn.applySQL("SELECT TOP 1 id FROM IN_MEETING_SITE WITH(NOLOCK) WHERE in_meeting = '" + cfg.meeting_id + "' AND in_code = 1");
            var count = pg.itmDrawEvents.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = pg.itmDrawEvents.getItemByIndex(i);
                var in_tree_name = item.getProperty("in_tree_name", "");
                var in_round_code = item.getProperty("in_round_code", "0");
                var in_round_id = item.getProperty("in_round_id", "0");
                var in_fight_id = item.getProperty("in_fight_id", "0");
                var in_link_type = item.getProperty("in_link_type", "");
                var in_f1 = item.getProperty("in_f1", "");
                var in_f2 = item.getProperty("in_f2", "");


                Item itmNew = cfg.inn.newItem("In_Meeting_PEvent", "add");
                itmNew.setProperty("in_meeting", cfg.meeting_id);
                itmNew.setProperty("source_id", cfg.program_id);
                itmNew.setProperty("in_date_key", pg.in_fight_day);

                itmNew.setProperty("in_tree_name", in_tree_name);
                itmNew.setProperty("in_tree_sort", item.getProperty("in_tree_sort", ""));

                itmNew.setProperty("in_tree_id", item.getProperty("in_tree_id", ""));
                itmNew.setProperty("in_tree_no", item.getProperty("in_tree_no", ""));
                itmNew.setProperty("in_tree_rank", item.getProperty("in_rank_name", ""));
                itmNew.setProperty("in_tree_rank_ns", item.getProperty("in_rank_real", ""));
                itmNew.setProperty("in_tree_rank_nss", item.getProperty("in_rank_show", ""));
                itmNew.setProperty("in_tree_alias", item.getProperty("in_alias", ""));

                itmNew.setProperty("in_round", item.getProperty("in_round", ""));
                itmNew.setProperty("in_round_id", item.getProperty("in_round_id", ""));
                itmNew.setProperty("in_round_code", item.getProperty("in_round_code", ""));
                itmNew.setProperty("in_fight_id", in_fight_id);

                itmNew.setProperty("in_period", "1");
                itmNew.setProperty("sort_order", item.getProperty("in_tree_no", ""));

                itmNew.setProperty("in_tree_id", item.getProperty("in_tree_id", ""));
                itmNew.setProperty("in_tree_sno", item.getProperty("in_tree_no", ""));
                itmNew.setProperty("in_round", item.getProperty("in_round", ""));
                itmNew.setProperty("in_robin_round", item.getProperty("in_round", ""));
               
                itmNew.setProperty("in_sub_id", item.getProperty("in_sub_id", "0"));
                itmNew.setProperty("in_sub_sect", item.getProperty("in_sub_sect", ""));

                itmNew.setProperty("in_site", itmSite.getProperty("id", ""));
                itmNew.setProperty("in_site_code", "1");
                itmNew.setProperty("in_show_site", itmSite.getProperty("id", ""));
                itmNew.setProperty("in_show_serial", item.getProperty("in_tree_no", ""));

                itmNew.setProperty("in_robin_name", item.getProperty("in_sub_sect", ""));
                itmNew.setProperty("in_robin_key", in_f1 + "," + in_f2);
                itmNew.setProperty("in_detail_ns", in_f1 + "," + in_f2);

                itmNew = itmNew.apply();

                var key = in_fight_id;
                var eid = itmNew.getProperty("id", "");

                pg.EventMap.Add(key, eid);

                AppendEventDetail(cfg, eid, "1", "2", in_link_type, in_f1, in_f2);
                AppendEventDetail(cfg, eid, "2", "1", in_link_type, in_f2, in_f1);
            }
        }

        private void AppendEventDetail(TConfig cfg, string eid, string in_sign_foot, string in_target_foot, string in_link_type, string n1, string n2)
        {
            Item itmDetail = cfg.inn.newItem("In_Meeting_PEvent_Detail", "add");
            itmDetail.setProperty("source_id", eid);
            itmDetail.setProperty("in_sign_foot", in_sign_foot);
            itmDetail.setProperty("in_target_foot", in_target_foot);

            switch(in_link_type)
            {
                case "no":
                    itmDetail.setProperty("in_sign_no", n1);
                    itmDetail.setProperty("in_target_no", n2);
                    break;
            }

            itmDetail = itmDetail.apply();
        }

        private string GetNewBattleType(TConfig cfg, int team_count)
        {
            var sql = "SELECT TOP 1 * FROM IN_MEETING_DRAWTEAM WITH(NOLOCK) WHERE " + team_count + " BETWEEN in_count_s AND in_count_e";
            var itmDrawTeam = cfg.inn.applySQL(sql);
            return itmDrawTeam.getProperty("in_battle_type", "");

        }

        //重設量級賽制
        private void ResetBattleType(TConfig cfg, string new_battle_type)
        {
            var sql_upd = "UPDATE IN_MEETING_PROGRAM SET in_battle_type = '" + new_battle_type + "' WHERE id = '" + cfg.program_id + "'";
            cfg.inn.applySQL(sql_upd);
        }

        /// <summary>
        /// 清除場次資料
        /// </summary>
        private void RemoveEvents(TConfig cfg)
        {
            Item itmData = cfg.inn.newItem("In_Meeting_Program");
            itmData.setProperty("program_id", cfg.program_id);
            itmData.apply("in_meeting_program_remove_all");
        }

        private Item GetMeetingDrawEventsByRobin(TConfig cfg, string in_team_count)
        {
            string sql = @"
                SELECT 
	                * 
                FROM 
	                IN_MEETING_DRAWEVENT WITH(NOLOCK) 
                WHERE 
	                in_team_count = {#in_team_count}
                ORDER BY
	                in_sort_order
            ";

            sql = sql.Replace("{#in_team_count}", in_team_count);

            return cfg.inn.applySQL(sql);
        }

        private Item GetMeetingDrawEventsByTree(TConfig cfg, int sys_count)
        {
            string sql = @"
                SELECT 
	                * 
                FROM 
	                IN_MEETING_DRAWEVENT WITH(NOLOCK) 
                WHERE 
	                in_team_count <= {#sys_count}
                    AND in_group IN ('main', 'repechage', 'rank')
                ORDER BY
	                in_sort_order
            ";

            sql = sql.Replace("{#sys_count}", sys_count.ToString());

            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string scene { get; set; }

            public string meeting_id { get; set; }
            public string program_id { get; set; }

        }

        private class TProgram
        {
            public Item itmProgram { get; set; }
            public string in_team_count { get; set; }
            public string in_fight_day { get; set; }

            public string new_battle_type { get; set; }

            public Item itmDrawEvents { get; set; }

            public Dictionary<string, string> EventMap { get; set; }

            public int team_count { get; set; }
            public bool is_robin { get; set; }
            public bool is_cross { get; set; }
        }

        private int GetRoundCount(int value)
        {
            var arr = InnSport.Core.Utilities.TUtility.Cardinarity(value);
            return arr[0];
        }

        private int GetInt(string value)
        {
            if (value == "" || value == "0") return 0;
            int result = 0;
            if (int.TryParse(value, out result))
            {
                return result;
            }
            return 0;
        }
    }
}