﻿using Aras.IOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WorkHelp.ArasDesk.Methods.Judo.退費
{
    public class In_Meeting_RefundQuery : Item
    {
        public In_Meeting_RefundQuery(IServerConnection arg) : base(arg) { }

        /// <summary>
        /// 編程啟動點 (Code 在此撰寫)
        /// </summary>
        public Item Run()
        {
            Aras.Server.Core.CallContext CCO = ((Aras.Server.Core.IOMConnection)serverConnection).CCO;
            Aras.Server.Core.IContextState RequestState = CCO.RequestState;
            /*
                目的: 退費查詢與退費統計
                日誌: 
                    - 2024-09-16: 創建 (lina)
            */

            //System.Diagnostics.Debugger.Break();

            var inn = this.getInnovator();
            var strDatabaseName = inn.getConnection().GetDatabaseName();
            var strMethodName = "[" + strDatabaseName + "]" + "In_Meeting_RefundQuery";

            var itmR = this;
            //CCO.Utilities.WriteDebug(strMethodName, "dom: " + itmR.dom.InnerXml);

            var cfg = new TConfig
            {
                CCO = CCO,
                inn = inn,
                strDatabaseName = strDatabaseName,
                strMethodName = strMethodName,
                meeting_type = itmR.getProperty("meeting_type", ""),
                meeting_id = itmR.getProperty("meeting_id", ""),
                scene = itmR.getProperty("scene", ""),
            };

            switch (cfg.scene)
            {
                case "page":
                    Page(cfg, itmR);
                    break;
                case "menu":
                    GetMenu(cfg, itmR);
                    break;
                case "report":
                    GetReport(cfg, itmR);
                    break;
            }

            return itmR;
        }

        private void GetReport(TConfig cfg, Item itmReturn)
        {
            var pages = GetReportPages(cfg);
            var sorted = pages.OrderBy(x => x.sort).ToList(); ;
            var headCss = "text-center bg-primary";
            var bodyCss = "text-center";

            var contents = new StringBuilder();

            for (var i = 0; i < sorted.Count; i++)
            {
                var page = sorted[i];
                var head = new StringBuilder();
                var body = new StringBuilder();

                head.Append("<tr>");
                head.Append("    <th class='" + headCss + "'>" + "活動" + "</th>");
                head.Append("    <th class='" + headCss + "'>" + "活動結束日" + "</th>");
                head.Append("    <th class='" + headCss + "'>" + "申請中" + "</th>");
                head.Append("    <th class='" + headCss + "'>" + "審核通過" + "</th>");
                head.Append("    <th class='" + headCss + "'>" + "不通過" + "</th>");
                head.Append("    <th class='" + headCss + "'>" + "其他狀態" + "</th>");
                head.Append("</tr>");

                for (var j = 0; j < page.meetings.Count; j++)
                {
                    var mt = page.meetings[j];
                    body.Append("<tr>");
                    body.Append("    <td class='text-left'>" + mt.mt_title + "</td>");
                    body.Append("    <td class='" + bodyCss + "'>" + mt.mt_end_date + "</td>");
                    body.Append("    <td class='" + bodyCss + "'>" + CountCell(cfg, mt, mt.listA) + "</td>");
                    body.Append("    <td class='" + bodyCss + "'>" + CountCell(cfg, mt, mt.listB) + "</td>");
                    body.Append("    <td class='" + bodyCss + "'>" + CountCell(cfg, mt, mt.listC) + "</td>");
                    body.Append("    <td class='" + bodyCss + "'>" + CountCell(cfg, mt, mt.listD) + "</td>");
                    body.Append("</tr>");
                }

                var tableId = "data-table-" + (i + 1);
                contents.Append("<div>");
                contents.Append("<h3>" + page.key + "</h3>");
                contents.Append(GetTableAttribute("tableId"));
                contents.Append("<thead>");
                contents.Append(head);
                contents.Append("</thead>");
                contents.Append("<tbody>");
                contents.Append(body);
                contents.Append("</tbody>");
                contents.Append("</table>");
                contents.Append("</div>");
            }

            itmReturn.setProperty("inn_tables", contents.ToString());
        }

        private string CountCell(TConfig cfg, TRefundMeeting mt, List<TRefundRow> list)
        {
            if (list.Count == 0) return "0";
            return "<span class='text-danger' onclick='GoRefundQuery(this)' data-mode='" + mt.mode + "' data-id='" + mt.mt_id + "' >" 
                + list.Count 
            + "</span>";
        }

        private List<TRefundPage> GetReportPages(TConfig cfg)
        {
            var pages = new List<TRefundPage>();
            var items = GetRefundItemsAll(cfg);
            var count = items.getItemCount();
            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var row = MapRefundRow(cfg, item);
                var key = row.type1 + "-" + row.type2 + "-" + row.type3;
                key = key.TrimEnd('-');
                var page = pages.Find(x => x.key == key);
                if (page == null)
                {
                    page = new TRefundPage
                    {
                        key = key,
                        sort = row.sort1 * 1000000 + row.sort2 * 1000 + row.sort3,
                        meetings = new List<TRefundMeeting>(),
                    };
                    pages.Add(page);
                }
                var meeting = page.meetings.Find(x => x.mt_id == row.mt_id);
                if (meeting == null)
                {
                    meeting = new TRefundMeeting
                    {
                        mt_id = row.mt_id,
                        mt_title = row.mt_title,
                        mt_end_date = row.mt_end_date,
                        listA = new List<TRefundRow>(),
                        listB = new List<TRefundRow>(),
                        listC = new List<TRefundRow>(),
                        listD = new List<TRefundRow>(),
                        mode = row.mode,
                    };
                    page.meetings.Add(meeting);
                }

                switch (row.in_cancel_status)
                {
                    case "退費申請中": meeting.listA.Add(row); break;
                    case "退費申請通過": meeting.listB.Add(row); break;
                    case "退費申請不通過": meeting.listC.Add(row); break;
                    default: meeting.listD.Add(row); break;
                }
            }
            return pages;
        }

        private class TRefundPage
        {
            public string key { get; set; }
            public int sort { get; set; }
            public List<TRefundMeeting> meetings { get; set; }
        }

        private class TRefundMeeting
        {
            public string mt_id { get; set; }
            public string mt_title { get; set; }
            public string mt_end_date { get; set; }
            public List<TRefundRow> listA { get; set; }
            public List<TRefundRow> listB { get; set; }
            public List<TRefundRow> listC { get; set; }
            public List<TRefundRow> listD { get; set; }
            public string mode { get; set; }
        }

        private class TRefundRow
        {
            public int sort1 { get; set; }
            public int sort2 { get; set; }
            public int sort3 { get; set; }
            public string type1 { get; set; }
            public string type2 { get; set; }
            public string type3 { get; set; }
            public string mt_id { get; set; }
            public string mt_title { get; set; }
            public string mt_type { get; set; }
            public string mt_end_date { get; set; }
            public string item_number { get; set; }
            public string in_code_2 { get; set; }
            public string in_cancel_status { get; set; }
            public string mode { get; set; }
        }

        private TRefundRow MapRefundRow(TConfig cfg, Item item)
        {
            var isSeminar = item.getProperty("in_meeting", "") == "";
            var type1 = isSeminar ? "課程" : "賽事";
            var type2 = item.getProperty("mt_type", "");
            var type3 = item.getProperty("in_seminar_label", "");

            var sort1 = isSeminar ? 200 : 100;
            var sort2 = 999;
            var sort3 = GetInt(item.getProperty("in_seminar_label", "999"), 999);

            switch (type2)
            {
                case "registry": type2 = "註冊"; sort2 = 100; break;
                case "game": type2 = "比賽"; sort2 = 200; break;
                case "payment": type2 = "會費"; sort2 = 300; break;
                case "seminar": type2 = "講習"; sort2 = 400; break;
                case "degree": type2 = "晉段"; sort2 = 500; break;
                case "merch": type2 = "周邊"; sort2 = 600; break;
                case "merge": type2 = "合併"; sort2 = 700; break;
                case "vote": type2 = "投票"; sort2 = 800; break;
                case "join": type2 = "入會"; sort2 = 900; break;
                case "other": type2 = "其他"; sort2 = 950; break;
            }

            var x = new TRefundRow
            {
                sort1 = sort1,
                sort2 = sort2,
                sort3 = sort3,
                type1 = type1,
                type2 = type2,
                type3 = type3,
                mt_id = item.getProperty("mt_id", ""),
                mt_title = item.getProperty("mt_title", ""),
                mt_type = item.getProperty("mt_type", ""),
                mt_end_date = item.getProperty("date_end", ""),
                item_number = item.getProperty("item_number", ""),
                in_code_2 = item.getProperty("in_code_2", ""),
                in_cancel_status = item.getProperty("in_cancel_status", ""),
                mode = isSeminar ? "cla" : "",
            };

            return x;
        }

        private Item GetRefundItemsAll(TConfig cfg)
        {
            var sql = @"
                SELECT
                    *
	                , CONVERT(VARCHAR, DATEADD(HOUR, 8, t101.in_date_e), 111) AS 'date_end'
                FROM
                (
                    SELECT DISTINCT
                        ISNULL(t11.in_title, t12.in_title)		AS 'mt_title'
                        , ISNULL(t11.id, t12.id)				AS 'mt_id'
                        , ISNULL(t11.in_date_e, t12.in_date_e)	AS 'in_date_e'
                        , ISNULL(t11.in_meeting_type, t12.in_meeting_type) AS 'mt_type'
                        , t12.in_seminar_type
                        , t1.id
                        , t1.item_number
                        , t1.in_code_2
                        , t1.in_current_org
                        , t1.in_creator
                        , t1.in_creator_sno
                        , t1.in_pay_amount_exp
                        , t1.in_pay_date_exp
                        , t1.pay_bool
                        , t1.in_refund_amount
                        , t1.in_write_note
                        , t2.in_cancel_status
                        , t1.in_meeting
                        , t21.label_zt      AS 'in_seminar_label'
                        , t21.sort_order    AS 'in_seminar_sort'
                    FROM
                        IN_MEETING_PAY t1 WITH(NOLOCK)
                    INNER JOIN
                        IN_MEETING_NEWS t2 WITH(NOLOCK)
                        ON t2.source_id = t1.id
                    LEFT OUTER JOIN
                        IN_MEETING_USER t3 WITH(NOLOCK)
                        ON t3.id = t2.in_muid
                    LEFT OUTER JOIN
                        IN_MEETING t11 WITH(NOLOCK)
                        ON t11.id = t1.in_meeting
                    LEFT OUTER JOIN
                        IN_CLA_MEETING t12 WITH(NOLOCK)
                        ON t12.id = t1.in_cla_meeting
                    LEFT OUTER JOIN
                        VU_MT_SEMINARTYPE t21 WITH(NOLOCK)
                        ON t21.value = t12.in_seminar_type
                    WHERE
                        t1.pay_bool = '已繳費'
                        AND t2.in_cancel_status LIKE N'%退費申請%'
                ) t101
                WHERE
                    t101.in_date_e >= DATEADD(year, -1, GETUTCDATE())
                ORDER BY 
                    t101.in_date_e DESC
            ";
            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);
            return cfg.inn.applySQL(sql);
        }

        //取得項目名稱 Group
        private void GetMenu(TConfig cfg, Item itmReturn)
        {
            string sql = "";
            if (cfg.meeting_type == "cla")
            {
                sql = @"
                    SELECT '' AS id , '請選擇' AS in_title, 'IMT-99999999' AS 'item_number', '0' AS 'in_meeting_type', '0' AS 'in_seminar_type', '2100-12-31' AS 'in_date_e'
                    UNION ALL
                    SELECT id, in_title, item_number, in_meeting_type, in_seminar_type, in_date_e FROM IN_CLA_MEETING WITH(NOLOCK) 
                    WHERE in_title NOT LIKE N'%範本%' AND in_title NOT LIKE N'%主場%'
                    AND DATEADD(month, -6, in_state_time_start) < '{#dateNow}' 
                    AND DATEADD(month, 6, in_state_time_end) > '{#dateNow}'
                    AND in_meeting_type NOT IN ('other')
                    ORDER BY in_meeting_type, in_seminar_type, in_date_e DESC
                ";
            }
            else
            {
                sql = @"
                    SELECT '' AS id , '請選擇' AS in_title, 'IMT-99999999' AS 'item_number', '2100-12-31' AS 'in_date_e'
                    UNION ALL
                    SELECT id, in_title, item_number, in_date_e FROM IN_MEETING WITH(NOLOCK) 
                    WHERE DATEADD(month, -6, in_state_time_start) < '{#dateNow}' 
                    AND DATEADD(month, 6, in_state_time_end) > '{#dateNow}'
                    AND in_title not like '%範本%'
                    AND in_meeting_type NOT IN ('registry', 'payment')
                    ORDER BY in_date_e DESC
                ";
            }

            sql = sql.Replace("{#dateNow}", DateTime.Now.ToString("yyyy/MM/dd"));

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            Item items = cfg.inn.applySQL(sql);

            if (items.isError() || items.getItemCount() <= 0)
            {
                throw new Exception("找無項目");
            }

            int count = items.getItemCount();

            for (int i = 0; i < count; i++)
            {
                Item item = items.getItemByIndex(i);

                item.setType("in_meeting_title");
                item.setProperty("id", item.getProperty("id", ""));
                item.setProperty("in_title", item.getProperty("in_title", ""));
                itmReturn.addRelationship(item);
            }
        }

        //頁面
        private void Page(TConfig cfg, Item itmReturn)
        {
            var items = GetRefundItems(cfg);
            var count = items.getItemCount();

            var body = new StringBuilder();
            var headCss = "text-center";
            var bodyCss = "text-center";

            for (var i = 0; i < count; i++)
            {
                var item = items.getItemByIndex(i);
                var no = i + 1;
                var amount = ToMoney(item.getProperty("in_pay_amount_exp", "0"));
                var refund = ToMoney(item.getProperty("in_refund_amount", "0"));

                body.Append("<tr>");
                body.Append("    <td class='" + bodyCss + "'>" + no + "</td>");
                body.Append("    <td class='" + bodyCss + "'>" + item.getProperty("item_number", "") + "</td>");
                body.Append("    <td class='" + bodyCss + "'>" + item.getProperty("in_code_2", "") + "</td>");
                body.Append("    <td class='" + bodyCss + "'>" + amount + "</td>");
                body.Append("    <td class='" + bodyCss + "'>" + item.getProperty("pay_bool", "") + "</td>");
                body.Append("    <td class='text-left'>" + item.getProperty("in_current_org", "") + "</td>");
                body.Append("    <td class='" + bodyCss + "'>" + item.getProperty("in_creator", "") + "</td>");
                body.Append("    <td class='" + bodyCss + "'>" + item.getProperty("in_cancel_status", "") + "</td>");
                body.Append("    <td class='" + bodyCss + "'>" + refund + "</td>");
                body.Append("    <td class='text-left'>" + item.getProperty("in_write_note", "") + "</td>");
                body.Append("</tr>");
            }

            var head = new StringBuilder();
            head.Append("<tr>");
            head.Append("    <th class='" + headCss + "'>" + "No." + "</th>");
            head.Append("    <th class='" + headCss + "'>" + "繳費單號" + "</th>");
            head.Append("    <th class='" + headCss + "'>" + "條碼2" + "</th>");
            head.Append("    <th class='" + headCss + "'>" + "應繳金額" + "</th>");
            head.Append("    <th class='" + headCss + "'>" + "繳費狀態" + "</th>");
            head.Append("    <th class='" + headCss + "'>" + "所屬單位" + "</th>");
            head.Append("    <th class='" + headCss + "'>" + "協助報名者" + "</th>");
            head.Append("    <th class='" + headCss + "'>" + "申請狀態" + "</th>");
            head.Append("    <th class='" + headCss + "'>" + "退費金額" + "</th>");
            head.Append("    <th class='" + headCss + "'>" + "銷帳備註" + "</th>");
            head.Append("</tr>");

            var contents = new StringBuilder();
            contents.Append(GetTableAttribute("data-table"));
            contents.Append("<thead>");
            contents.Append(head);
            contents.Append("</thead>");
            contents.Append("<tbody>");
            contents.Append(body);
            contents.Append("</tbody>");
            contents.Append("</table>");

            itmReturn.setProperty("inn_table", contents.ToString());
        }

        private Item GetRefundItems(TConfig cfg)
        {
            var column = "in_meeting";
            if (cfg.meeting_type == "cla") column = "in_cla_meeting";

            var sql = @"
                SELECT DISTINCT
	                ISNULL(t11.in_title, t12.in_title)	AS 'mt_title'
	                , ISNULL(t11.id, t12.id)			AS 'mt_id'
	                , t1.id
	                , t1.item_number
	                , t1.in_code_2
	                , t1.in_current_org
	                , t1.in_creator
	                , t1.in_creator_sno
	                , t1.in_pay_amount_exp
	                , t1.in_pay_date_exp
	                , t1.pay_bool
	                , t1.in_refund_amount
	                , t1.in_write_note
	                , t2.in_cancel_status
                FROM
	                IN_MEETING_PAY t1 WITH(NOLOCK)
                INNER JOIN
	                IN_MEETING_NEWS t2 WITH(NOLOCK)
	                ON t2.source_id = t1.id
                LEFT OUTER JOIN
	                IN_MEETING_USER t3 WITH(NOLOCK)
	                ON t3.id = t2.in_muid
                LEFT OUTER JOIN
	                IN_MEETING t11 WITH(NOLOCK)
	                ON t11.id = t1.in_meeting
                LEFT OUTER JOIN
	                IN_CLA_MEETING t12 WITH(NOLOCK)
	                ON t12.id = t1.in_cla_meeting
                WHERE
	                t1.{#column} = '{#meeting_id}'
	                AND t1.pay_bool = '已繳費'
	                AND t2.in_cancel_status LIKE N'%退費申請%'
                ORDER BY
                    t1.item_number
            ";

            sql = sql.Replace("{#column}", column)
                .Replace("{#meeting_id}", cfg.meeting_id);

            cfg.CCO.Utilities.WriteDebug(cfg.strMethodName, sql);

            return cfg.inn.applySQL(sql);
        }

        /// <summary>
        /// Method 組態
        /// </summary>
        private class TConfig
        {
            public Aras.Server.Core.CallContext CCO { get; set; }
            public Innovator inn { get; set; }
            public string strDatabaseName { get; set; }
            public string strMethodName { get; set; }
            public string meeting_type { get; set; }
            public string meeting_id { get; set; }
            public string scene { get; set; }
        }

        private string GetTableAttribute(string table_name)
        {
            return "<table id='" + table_name + "'"
                + " class='table table-hover table-bordered table-rwd rwd rwdtable inno-table'"
                + " data-toggle='table'"
                + " data-show-toggle='false'"
                + " data-show-columns='false'"
                + " data-search-align='left'"
                + " data-sort-stable='true'"
                + " data-search='false'"
                + ">";
        }

        private string ToMoney(string value)
        {
            int amount = 0;

            if (Int32.TryParse(value, out amount))
            {
                return amount.ToString("###,##0");
            }
            else
            {
                return value;
            }
        }

        private int GetInt(string value, int def = 0)
        {
            int result = def;
            int.TryParse(value, out result);
            return result;
        }
    }
}